﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Dit.Framework.UI;

namespace EquipView
{
    /// <summary>
    /// UserControl1.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class UcrlEquipView : System.Windows.Controls.UserControl
    {
        public class ImgMoveInfo
        {
            public double Current { get; set; }
            public double ImgStart { get; set; }
            public double ImgEnd { get; set; }
            public double Stroke { get; set; }
            public ImgMoveInfo()
            {
                Current = ImgStart = ImgEnd = Stroke = 0;
            }
            public void SetStroke(double imgStart, double imgEnd, double stroke)
            {
                ImgStart = imgStart;
                ImgEnd = imgEnd;
                Stroke = stroke;
            }
        }


        #region 축
        private ImgMoveInfo _CstLoaderARotation = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderAUpDown = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderBRotation = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderBUpDown = new ImgMoveInfo();
        private ImgMoveInfo _LoaderAX = new ImgMoveInfo();
        private ImgMoveInfo _LoaderBX = new ImgMoveInfo();
        private ImgMoveInfo _LoaderAY = new ImgMoveInfo();
        private ImgMoveInfo _LoaderBY = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRB1Y = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRB2Y = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRB1X = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRB2X = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRB1Y = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRB2Y = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRB1T = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRB2T = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRA1Y = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRA2Y = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRA1X = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRA2X = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRA1Y = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRA2Y = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRA1T = new ImgMoveInfo();
        private ImgMoveInfo _Sub_LoaderTRA2T = new ImgMoveInfo();
        private ImgMoveInfo _PreAlignX = new ImgMoveInfo();
        private ImgMoveInfo _FineAlignX = new ImgMoveInfo();
        private ImgMoveInfo _IRCutStageA1Y = new ImgMoveInfo();
        private ImgMoveInfo _IRCutStageA2Y = new ImgMoveInfo();
        private ImgMoveInfo _IRCutStageB1Y = new ImgMoveInfo();
        private ImgMoveInfo _IRCutStageB2Y = new ImgMoveInfo();
        private ImgMoveInfo _LaserHeadX = new ImgMoveInfo();
        private ImgMoveInfo _LaserHeadZ = new ImgMoveInfo();
        private ImgMoveInfo _AfterIRCutTRAY = new ImgMoveInfo();
        private ImgMoveInfo _AfterIRCutTRBY = new ImgMoveInfo();
        private ImgMoveInfo _BreakTableB1Y = new ImgMoveInfo();
        private ImgMoveInfo _BreakTableB2Y = new ImgMoveInfo();





        private ImgMoveInfo _Sub_BreakTableB1T = new ImgMoveInfo();
        private ImgMoveInfo _Sub_BreakTableB2T = new ImgMoveInfo();
        private ImgMoveInfo _BreakTableA1Y = new ImgMoveInfo();
        private ImgMoveInfo _BreakTableA2Y = new ImgMoveInfo();




        private ImgMoveInfo _Sub_BreakTableA1T = new ImgMoveInfo();
        private ImgMoveInfo _Sub_BreakTableA2T = new ImgMoveInfo();
        private ImgMoveInfo _BreakingHead1X = new ImgMoveInfo();
        private ImgMoveInfo _BreakingHead2X = new ImgMoveInfo();
        private ImgMoveInfo _BreakingHead1Z = new ImgMoveInfo();
        private ImgMoveInfo _BreakingHead2Z = new ImgMoveInfo();
        private ImgMoveInfo _BreakAlign1X = new ImgMoveInfo();
        private ImgMoveInfo _BreakAlign2X = new ImgMoveInfo();
        private ImgMoveInfo _BreakAlign1Z = new ImgMoveInfo();
        private ImgMoveInfo _BreakAlign2Z = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRB1Y = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRB2Y = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRA1Y = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRA2Y = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRA1Y = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRA2Y = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRA1T = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRA2T = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRB1Y = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRB2Y = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRB1T = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRB2T = new ImgMoveInfo();
        private ImgMoveInfo _InspCameraX = new ImgMoveInfo();
        private ImgMoveInfo _InspCameraZ = new ImgMoveInfo();
        private ImgMoveInfo _InspStageA1Y = new ImgMoveInfo();
        private ImgMoveInfo _InspStageA2Y = new ImgMoveInfo();
        private ImgMoveInfo _InspStageB1Y = new ImgMoveInfo();
        private ImgMoveInfo _InspStageB2Y = new ImgMoveInfo();
        private ImgMoveInfo _UnloaderAX = new ImgMoveInfo();
        private ImgMoveInfo _UnloaderBX = new ImgMoveInfo();
        private ImgMoveInfo _UnloaderAY = new ImgMoveInfo();
        private ImgMoveInfo _UnloaderBY = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderARotation = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderAUpDown = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderBRotation = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderBUpDown = new ImgMoveInfo();
        #endregion

        #region 실린더

        private ImgMoveInfo _CstLoaderBL_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderBR_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderAL_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderAR_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderA2L_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderA2R_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRB1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRB2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRA1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LoaderTRA2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderB_Tilt_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstLoaderA_Tilt_Cylinder = new ImgMoveInfo();

        private ImgMoveInfo _LaserHead1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LaserHead2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LaserShutter_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _IRCutStageA_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _IRCutStageB_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BreakTableA_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BreakTableB_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BreakTRB1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BreakTRB2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BreakTRA1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BreakTRA2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LaserDummyShutter1_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _LaserDummyShutter2_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _DummyTank_Cylinder = new ImgMoveInfo();

        private ImgMoveInfo _Buffer_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderBL_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderBR_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderAL_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderAR_Grip_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRB1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRA1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRB2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _AfterInspTRA2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderB_Tilt_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _CstUnloaderA_Tilt_Cylinder = new ImgMoveInfo();

        private ImgMoveInfo _BreakingHead1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BreakingHead2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRB1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRA1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRB2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _BeforeInspTRA2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _InspA1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _InspA2_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _InspB1_UpDown_Cylinder = new ImgMoveInfo();
        private ImgMoveInfo _InspB2_UpDown_Cylinder = new ImgMoveInfo();
        #endregion




        public UcrlEquipView()
        {
            InitializeComponent();
            
            //////////////////////////////////////////////////////////////////////////
            // Start-End만 설정하면 아래 Motor Move는 변경 X
            // Start, End에 따라 이동 방향 결정
            //////////////////////////////////////////////////////////////////////////

            #region 축
            _CstLoaderARotation.SetStroke(-90, -180, 200);
            _CstLoaderAUpDown.SetStroke(GridCstLoaderACYlinder.Margin.Top, GridCstLoaderACYlinder.Margin.Top + 45, 400);
            _CstLoaderBRotation.SetStroke(-90, -180, 200);
            _CstLoaderBUpDown.SetStroke(GridCstLoaderBCYlinder.Margin.Top, GridCstLoaderBCYlinder.Margin.Top + 45, 400);

            _LoaderAX.SetStroke(GridLoaderA.Margin.Top, GridLoaderA.Margin.Top - 145, 850);
            _LoaderBX.SetStroke(GridLoaderB.Margin.Top, GridLoaderB.Margin.Top + 145, 850);

            _LoaderAY.SetStroke(GridLoaderA_Copy.Margin.Left, GridLoaderA_Copy.Margin.Left + 45, 500);
            _LoaderBY.SetStroke(GridLoaderB_Copy.Margin.Left, GridLoaderB_Copy.Margin.Left + 45, 500);

            _LoaderTRB1Y.SetStroke(GridLoaderTRB1.Margin.Left, GridLoaderTRB1.Margin.Left - 60, 1400);
            _LoaderTRB2Y.SetStroke(GridLoaderTRB2.Margin.Left, GridLoaderTRB2.Margin.Left - 60, 1400);
            _Sub_LoaderTRB1Y.SetStroke(GridLoaderTRB1.Margin.Left, GridLoaderTRB1.Margin.Left + 30, 1400);
            _Sub_LoaderTRB2Y.SetStroke(GridLoaderTRB2.Margin.Left, GridLoaderTRB2.Margin.Left + 30, 1400);
            _LoaderTRB1X.SetStroke(GridLoaderTRB1.Margin.Top, GridLoaderTRB1.Margin.Top + 9, 30);
            _LoaderTRB2X.SetStroke(GridLoaderTRB2.Margin.Top, GridLoaderTRB2.Margin.Top + 9, 30);
            _Sub_LoaderTRB1T.SetStroke(0, 90, 250);
            _Sub_LoaderTRB2T.SetStroke(0, 90, 250);


            _LoaderTRA1Y.SetStroke(GridLoaderTRA1.Margin.Left, GridLoaderTRA1.Margin.Left - 60, 1400);
            _LoaderTRA2Y.SetStroke(GridLoaderTRA2.Margin.Left, GridLoaderTRA2.Margin.Left - 60, 1400);
            _LoaderTRA1X.SetStroke(GridLoaderTRA1.Margin.Top, GridLoaderTRA1.Margin.Top + 9, 30);
            _LoaderTRA2X.SetStroke(GridLoaderTRA2.Margin.Top, GridLoaderTRA2.Margin.Top + 9, 30);
            _Sub_LoaderTRA1Y.SetStroke(GridLoaderTRA1.Margin.Left, GridLoaderTRA1.Margin.Left + 30, 1400);
            _Sub_LoaderTRA2Y.SetStroke(GridLoaderTRA2.Margin.Left, GridLoaderTRA2.Margin.Left + 30, 1400);
            _Sub_LoaderTRA1T.SetStroke(0, 90, 250);
            _Sub_LoaderTRA2T.SetStroke(0, 90, 250);


            _PreAlignX.SetStroke(GridPreAlign.Margin.Top, GridPreAlign.Margin.Top - 132, 1000);
            _FineAlignX.SetStroke(GridFineAlign.Margin.Top, GridFineAlign.Margin.Top - 132, 1000);
            _IRCutStageA1Y.SetStroke(GridProcessTableA1.Margin.Left, GridProcessTableA1.Margin.Left - 40, 1350);
            _IRCutStageA2Y.SetStroke(GridProcessTableA2.Margin.Left, GridProcessTableA2.Margin.Left - 40, 1350);
            _IRCutStageB1Y.SetStroke(GridProcessTableB1.Margin.Left, GridProcessTableB1.Margin.Left - 40, 1350);
            _IRCutStageB2Y.SetStroke(GridProcessTableB2.Margin.Left, GridProcessTableB2.Margin.Left - 40, 1350);
            _LaserHeadX.SetStroke(GridLaserHead.Margin.Top, GridLaserHead.Margin.Top - 140, 1150);
            _LaserHeadZ.SetStroke(GridLaserHeaderZCylinder.Margin.Top, GridLaserHeaderZCylinder.Margin.Top + 45, 100);
            _AfterIRCutTRAY.SetStroke(GridBreakTRA.Margin.Left, GridBreakTRA.Margin.Left - 20, 450);
            _AfterIRCutTRBY.SetStroke(GridBreakTRB.Margin.Left, GridBreakTRA.Margin.Left - 20, 450);
            _BreakTableB1Y.SetStroke(GridBreakTableB1.Margin.Left, GridBreakTableB1.Margin.Left - 50, 1350);
            _BreakTableB2Y.SetStroke(GridBreakTableB2.Margin.Left, GridBreakTableB2.Margin.Left - 50, 1350);
            _Sub_BreakTableB1T.SetStroke(0, 90, 250);
            _Sub_BreakTableB2T.SetStroke(0, 90, 250);
            _BreakTableA1Y.SetStroke(GridBreakTableA1.Margin.Left, GridBreakTableA1.Margin.Left - 50, 1350);
            _BreakTableA2Y.SetStroke(GridBreakTableA2.Margin.Left, GridBreakTableA2.Margin.Left - 50, 1350);
            _Sub_BreakTableA1T.SetStroke(0, 90, 250);
            _Sub_BreakTableA2T.SetStroke(0, 90, 250);
            _BreakingHead1X.SetStroke(GridBreakingHead1.Margin.Top, GridBreakingHead1.Margin.Top - 150, 800);
            _BreakingHead2X.SetStroke(GridBreakingHead2.Margin.Top, GridBreakingHead2.Margin.Top - 150, 800);
            _BreakingHead1Z.SetStroke(GridBreakingheadZ1Cylinder.Margin.Top, GridBreakingheadZ1Cylinder.Margin.Top + 45, 100);
            _BreakingHead2Z.SetStroke(GridBreakingheadZ2Cylinder.Margin.Top, GridBreakingheadZ2Cylinder.Margin.Top + 45, 100);
            _BreakAlign1X.SetStroke(GridBreakAlign1.Margin.Top, GridBreakAlign1.Margin.Top - 150, 1100);
            _BreakAlign2X.SetStroke(GridBreakAlign2.Margin.Top, GridBreakAlign2.Margin.Top - 150, 1100);
            _BreakAlign1Z.SetStroke(GridBreakingAlignZCylinder.Margin.Top, GridBreakingAlignZCylinder.Margin.Top + 45, 100);
            _BreakAlign2Z.SetStroke(GridBreakingAlignZCylinder.Margin.Top, GridBreakingAlignZCylinder.Margin.Top + 45, 100);
            _BeforeInspTRA1Y.SetStroke(GridBeforeInspTRA1.Margin.Left, GridBeforeInspTRA1.Margin.Left - 80, 1400);
            _BeforeInspTRA2Y.SetStroke(GridBeforeInspTRA2.Margin.Left, GridBeforeInspTRA2.Margin.Left - 80, 1400);
            _BeforeInspTRB1Y.SetStroke(GridBeforeInspTRB1.Margin.Left, GridBeforeInspTRB1.Margin.Left - 80, 1400);
            _BeforeInspTRB2Y.SetStroke(GridBeforeInspTRB2.Margin.Left, GridBeforeInspTRB2.Margin.Left - 80, 1400);
            _AfterInspTRA1Y.SetStroke(GridAfterInspTRA1.Margin.Left, GridAfterInspTRA1.Margin.Left + 30, 850);
            _AfterInspTRA2Y.SetStroke(GridAfterInspTRA2.Margin.Left, GridAfterInspTRA2.Margin.Left + 30, 850);
            _AfterInspTRA1T.SetStroke(0, 90, 250);
            _AfterInspTRA2T.SetStroke(0, 90, 250);
            _AfterInspTRB1Y.SetStroke(GridAfterInspTRB1.Margin.Left, GridAfterInspTRB1.Margin.Left + 30, 850);
            _AfterInspTRB2Y.SetStroke(GridAfterInspTRB2.Margin.Left, GridAfterInspTRB2.Margin.Left + 30, 850);
            _AfterInspTRB1T.SetStroke(0, 90, 250);
            _AfterInspTRB2T.SetStroke(0, 90, 250);
            _InspCameraX.SetStroke(GridInspCamera.Margin.Top, GridInspCamera.Margin.Top - 160, 1050);
            _InspCameraZ.SetStroke(GridInspectionCamreraZ.Margin.Top, GridInspectionCamreraZ.Margin.Top - 40, 100);
            _InspStageA1Y.SetStroke(GridInspectionStageA1.Margin.Left, GridInspectionStageA1.Margin.Left - 90, 1350);
            _InspStageA2Y.SetStroke(GridInspectionStageA2.Margin.Left, GridInspectionStageA2.Margin.Left - 90, 1350);
            _InspStageB1Y.SetStroke(GridInspectionStageB1.Margin.Left, GridInspectionStageB1.Margin.Left - 90, 1350);
            _InspStageB2Y.SetStroke(GridInspectionStageB2.Margin.Left, GridInspectionStageB2.Margin.Left - 90, 1350);
            _UnloaderAX.SetStroke(GridUnloaderA.Margin.Top, GridUnloaderA.Margin.Top - 145, 850);
            _UnloaderBX.SetStroke(GridUnloaderB.Margin.Top, GridUnloaderB.Margin.Top + 145, 850);
            _UnloaderAY.SetStroke(GridUnloaderA_Copy.Margin.Left, GridUnloaderA_Copy.Margin.Left - 45, 500);
            _UnloaderBY.SetStroke(GridUnloaderB_Copy.Margin.Left, GridUnloaderB_Copy.Margin.Left - 45, 500);
            _CstUnloaderARotation.SetStroke(90, 180, 250);
            _CstUnloaderAUpDown.SetStroke(GridCstUnloaderACYlinder.Margin.Top, GridCstUnloaderACYlinder.Margin.Top - 45, 400);
            _CstUnloaderBRotation.SetStroke(90, 180, 250);
            _CstUnloaderBUpDown.SetStroke(GridCstUnloaderBCYlinder.Margin.Top, GridCstUnloaderBCYlinder.Margin.Top - 45, 400);

            #endregion

            #region 실린더
            _CstLoaderBL_Grip_Cylinder.SetStroke(lblCstLoaderBL_Grip_Cylinder.Margin.Left, lblCstLoaderBL_Grip_Cylinder.Margin.Left + 4, 50);
            _CstLoaderBR_Grip_Cylinder.SetStroke(lblCstLoaderBR_Grip_Cylinder.Margin.Left, lblCstLoaderBR_Grip_Cylinder.Margin.Left - 4, 50);
            _CstLoaderAL_Grip_Cylinder.SetStroke(lblCstLoaderAL_Grip_Cylinder.Margin.Left, lblCstLoaderAL_Grip_Cylinder.Margin.Left + 4, 50);
            _CstLoaderAR_Grip_Cylinder.SetStroke(lblCstLoaderAR_Grip_Cylinder.Margin.Left, lblCstLoaderAR_Grip_Cylinder.Margin.Left - 4, 50);
            _LoaderTRB1_UpDown_Cylinder.SetStroke(lblLoaderTRB1_UpDown_Cylinder.Margin.Top, lblLoaderTRB1_UpDown_Cylinder.Margin.Top + 5, 50);
            _LoaderTRB2_UpDown_Cylinder.SetStroke(lblLoaderTRB2_UpDown_Cylinder.Margin.Top, lblLoaderTRB2_UpDown_Cylinder.Margin.Top + 5, 50);
            _LoaderTRA1_UpDown_Cylinder.SetStroke(lblLoaderTRA1_UpDown_Cylinder.Margin.Top, lblLoaderTRA1_UpDown_Cylinder.Margin.Top + 5, 50);
            _LoaderTRA2_UpDown_Cylinder.SetStroke(lblLoaderTRA2_UpDown_Cylinder.Margin.Top, lblLoaderTRA2_UpDown_Cylinder.Margin.Top + 5, 50);
            _CstLoaderA_Tilt_Cylinder.SetStroke(lblCstLoaderA_Tilt_Cylinder.Margin.Top, lblCstLoaderA_Tilt_Cylinder.Margin.Top + 5, 50);
            _CstLoaderB_Tilt_Cylinder.SetStroke(lblCstLoaderB_Tilt_Cylinder.Margin.Top, lblCstLoaderB_Tilt_Cylinder.Margin.Top + 5, 50);

            _LaserHead1_UpDown_Cylinder.SetStroke(lblLaserHead1_UpDown_Cylinder.Margin.Top, lblLaserHead1_UpDown_Cylinder.Margin.Top + 5, 50);
            _LaserHead2_UpDown_Cylinder.SetStroke(lblLaserHead2_UpDown_Cylinder.Margin.Top, lblLaserHead2_UpDown_Cylinder.Margin.Top + 5, 50);
            _LaserShutter_Cylinder.SetStroke(lblLaserShutter_Cylinder.Margin.Left, lblLaserShutter_Cylinder.Margin.Left - 5, 50);
            _IRCutStageA_UpDown_Cylinder.SetStroke(lblIRCutStageA_UpDown_Cylinder.Margin.Top, lblIRCutStageA_UpDown_Cylinder.Margin.Top + 5, 50);
            _IRCutStageB_UpDown_Cylinder.SetStroke(lblIRCutStageB_UpDown_Cylinder.Margin.Top, lblIRCutStageB_UpDown_Cylinder.Margin.Top + 5, 50);
            _BreakTableA_UpDown_Cylinder.SetStroke(lblBreakTableA_UpDown_Cylinder.Margin.Top, lblBreakTableA_UpDown_Cylinder.Margin.Top + 5, 50);
            _BreakTableB_UpDown_Cylinder.SetStroke(lblBreakTableB_UpDown_Cylinder.Margin.Top, lblBreakTableB_UpDown_Cylinder.Margin.Top + 5, 50);
            _BreakTRB1_UpDown_Cylinder.SetStroke(lblBreakTRB1_UpDown_Cylinder.Margin.Top, lblBreakTRB1_UpDown_Cylinder.Margin.Top + 5, 50);
            _BreakTRB2_UpDown_Cylinder.SetStroke(lblBreakTRB2_UpDown_Cylinder.Margin.Top, lblBreakTRB2_UpDown_Cylinder.Margin.Top + 5, 50);
            _BreakTRA1_UpDown_Cylinder.SetStroke(lblBreakTRA1_UpDown_Cylinder.Margin.Top, lblBreakTRA1_UpDown_Cylinder.Margin.Top + 5, 50);
            _BreakTRA2_UpDown_Cylinder.SetStroke(lblBreakTRA2_UpDown_Cylinder.Margin.Top, lblBreakTRA2_UpDown_Cylinder.Margin.Top + 5, 50);
            _LaserDummyShutter1_Cylinder.SetStroke(lblLaserDummyShutter1_Cylinder.Margin.Left, lblLaserDummyShutter1_Cylinder.Margin.Left - 6, 50);
            _LaserDummyShutter2_Cylinder.SetStroke(lblLaserDummyShutter2_Cylinder.Margin.Left, lblLaserDummyShutter2_Cylinder.Margin.Left - 6, 50);
            _DummyTank_Cylinder.SetStroke(GridDummyTankLid.Margin.Left, GridDummyTankLid.Margin.Left + 17, 50);

            _Buffer_UpDown_Cylinder.SetStroke(lblBuffer_UpDown_Cylinder.Margin.Top, lblBuffer_UpDown_Cylinder.Margin.Top + 5, 50);
            _CstUnloaderBL_Grip_Cylinder.SetStroke(lblCstUnloaderBL_Grip_Cylinder.Margin.Left, lblCstUnloaderBL_Grip_Cylinder.Margin.Left + 4, 50);
            _CstUnloaderBR_Grip_Cylinder.SetStroke(lblCstUnloaderBR_Grip_Cylinder.Margin.Left, lblCstUnloaderBR_Grip_Cylinder.Margin.Left - 4, 50);
            _CstUnloaderAL_Grip_Cylinder.SetStroke(lblCstUnloaderAL_Grip_Cylinder.Margin.Left, lblCstUnloaderAL_Grip_Cylinder.Margin.Left + 4, 50);
            _CstUnloaderAR_Grip_Cylinder.SetStroke(lblCstUnloaderAR_Grip_Cylinder.Margin.Left, lblCstUnloaderAR_Grip_Cylinder.Margin.Left - 4, 50);
            _AfterInspTRB1_UpDown_Cylinder.SetStroke(lblAfterInspTRB1_UpDown_Cylinder.Margin.Top, lblAfterInspTRB1_UpDown_Cylinder.Margin.Top + 5, 50);
            _AfterInspTRA1_UpDown_Cylinder.SetStroke(lblAfterInspTRA1_UpDown_Cylinder.Margin.Top, lblAfterInspTRA1_UpDown_Cylinder.Margin.Top + 5, 50);
            _AfterInspTRB2_UpDown_Cylinder.SetStroke(lblAfterInspTRB2_UpDown_Cylinder.Margin.Top, lblAfterInspTRB2_UpDown_Cylinder.Margin.Top + 5, 50);
            _AfterInspTRA2_UpDown_Cylinder.SetStroke(lblAfterInspTRA2_UpDown_Cylinder.Margin.Top, lblAfterInspTRA2_UpDown_Cylinder.Margin.Top + 5, 50);
            _CstUnloaderA_Tilt_Cylinder.SetStroke(lblCstUnloaderA_Tilt_Cylinder.Margin.Top, lblCstLoaderA_Tilt_Cylinder.Margin.Top + 5, 50);
            _CstUnloaderB_Tilt_Cylinder.SetStroke(lblCstUnloaderB_Tilt_Cylinder.Margin.Top, lblCstLoaderB_Tilt_Cylinder.Margin.Top + 5, 50);



            //_BreakingHead1_UpDown_Cylinder.SetStroke(lblBreakingHead1_UpDown_Cylinder.Margin.Top, lblBreakingHead1_UpDown_Cylinder.Margin.Top + 10, 50);
            //_BreakingHead2_UpDown_Cylinder.SetStroke(lblBreakingHead2_UpDown_Cylinder.Margin.Top, lblBreakingHead2_UpDown_Cylinder.Margin.Top + 10, 50);
            _BeforeInspTRB1_UpDown_Cylinder.SetStroke(lblBeforeInspTRB1_UpDown_Cylinder.Margin.Top, lblBeforeInspTRB1_UpDown_Cylinder.Margin.Top + 5, 50);
            _BeforeInspTRA1_UpDown_Cylinder.SetStroke(lblBeforeInspTRA1_UpDown_Cylinder.Margin.Top, lblBeforeInspTRA1_UpDown_Cylinder.Margin.Top + 5, 50);
            _BeforeInspTRB2_UpDown_Cylinder.SetStroke(lblBeforeInspTRB2_UpDown_Cylinder.Margin.Top, lblBeforeInspTRB2_UpDown_Cylinder.Margin.Top + 5, 50);
            _BeforeInspTRA2_UpDown_Cylinder.SetStroke(lblBeforeInspTRA2_UpDown_Cylinder.Margin.Top, lblBeforeInspTRA2_UpDown_Cylinder.Margin.Top + 5, 50);
            //_InspA1_UpDown_Cylinder.SetStroke(lblInspStageA1_UpDown_Cylinder.Margin.Top, lblInspStageA1_UpDown_Cylinder.Margin.Top + 10, 50);
            //_InspA2_UpDown_Cylinder.SetStroke(lblInspStageA2_UpDown_Cylinder.Margin.Top, lblInspStageA2_UpDown_Cylinder.Margin.Top + 10, 50);
            //_InspB1_UpDown_Cylinder.SetStroke(lblInspStageB1_UpDown_Cylinder.Margin.Top, lblInspStageB1_UpDown_Cylinder.Margin.Top + 10, 50);
            //_InspB2_UpDown_Cylinder.SetStroke(lblInspStageB2_UpDown_Cylinder.Margin.Top, lblInspStageB2_UpDown_Cylinder.Margin.Top + 10, 50);
            #endregion


        }
        #region 축

        public double CstLoaderBRotation { get { return _CstLoaderBRotation.Current; } set { CalculateRotate(BorderCstLoaderB, _CstLoaderBRotation, value); } }
        public double CstLoaderBUpDown { get { return _CstLoaderBUpDown.Current; } set { CalculatePosition(GridCstLoaderBCYlinder, _CstLoaderBUpDown, value, Orientation.Vertical); } }
        public double CstLoaderARotation { get { return _CstLoaderARotation.Current; } set { CalculateRotate(BorderCstLoaderA, _CstLoaderARotation, value); } }
        public double CstLoaderAUpDown { get { return _CstLoaderAUpDown.Current; } set { CalculatePosition(GridCstLoaderACYlinder, _CstLoaderAUpDown, value, Orientation.Vertical); } }
        public double LoaderAX { get { return _LoaderAX.Current; } set { CalculatePosition(GridLoaderA, _LoaderAX, value, Orientation.Vertical); } }
        public double LoaderBX { get { return _LoaderBX.Current; } set { CalculatePosition(GridLoaderB, _LoaderBX, value, Orientation.Vertical); } }
        public double LoaderAY { get { return _LoaderAY.Current; } set { CalculatePosition(GridLoaderA_Copy, _LoaderAY, value, Orientation.Horizontal); } }
        public double LoaderBY { get { return _LoaderBY.Current; } set { CalculatePosition(GridLoaderB_Copy, _LoaderBY, value, Orientation.Horizontal); } }
        public double LoaderTRB1Y { get { return _LoaderTRB1Y.Current; } set { CalculatePosition(GridLoaderTRB1, _LoaderTRB1Y, value, Orientation.Horizontal); } }
        public double LoaderTRB2Y { get { return _LoaderTRB2Y.Current; } set { CalculatePosition(GridLoaderTRB2, _LoaderTRB2Y, value, Orientation.Horizontal); } }
        public double LoaderTRB1X { get { return _LoaderTRB1X.Current; } set { CalculatePosition(GridLoaderTRB1, _LoaderTRB1X, value, Orientation.Vertical); } }
        public double LoaderTRB2X { get { return _LoaderTRB2X.Current; } set { CalculatePosition(GridLoaderTRB2, _LoaderTRB2X, value, Orientation.Vertical); } }

        public double Sub_LoaderTRB1Y { get { return _Sub_LoaderTRB1Y.Current; } set { CalculatePosition(BorderLoaderTRB1, _Sub_LoaderTRB1Y, value, Orientation.Horizontal); } }
        public double Sub_LoaderTRB2Y { get { return _Sub_LoaderTRB2Y.Current; } set { CalculatePosition(BorderLoaderTRB2, _Sub_LoaderTRB2Y, value, Orientation.Horizontal); } }
        public double Sub_LoaderTRB1T { get { return _Sub_LoaderTRB1T.Current; } set { CalculateRotate(BorderLoaderTRB1, _Sub_LoaderTRB1T, value); } }
        public double Sub_LoaderTRB2T { get { return _Sub_LoaderTRB2T.Current; } set { CalculateRotate(BorderLoaderTRB2, _Sub_LoaderTRB2T, value); } }

        public double LoaderTRA1Y { get { return _LoaderTRA1Y.Current; } set { CalculatePosition(GridLoaderTRA1, _LoaderTRA1Y, value, Orientation.Horizontal); } }
        public double LoaderTRA2Y { get { return _LoaderTRA2Y.Current; } set { CalculatePosition(GridLoaderTRA2, _LoaderTRA2Y, value, Orientation.Horizontal); } }
        public double LoaderTRA1X { get { return _LoaderTRA1X.Current; } set { CalculatePosition(GridLoaderTRA1, _LoaderTRA1X, value, Orientation.Vertical); } }
        public double LoaderTRA2X { get { return _LoaderTRA2X.Current; } set { CalculatePosition(GridLoaderTRA2, _LoaderTRA2X, value, Orientation.Vertical); } }
        public double Sub_LoaderTRA1Y { get { return _Sub_LoaderTRA1Y.Current; } set { CalculatePosition(GridLoaderTRA1, _Sub_LoaderTRA1Y, value, Orientation.Horizontal); } }
        public double Sub_LoaderTRA2Y { get { return _Sub_LoaderTRA2Y.Current; } set { CalculatePosition(GridLoaderTRA2, _Sub_LoaderTRA2Y, value, Orientation.Horizontal); } }
        public double Sub_LoaderTRA1T { get { return _Sub_LoaderTRA1T.Current; } set { CalculateRotate(BorderLoaderTRA1, _Sub_LoaderTRA1T, value); } }
        public double Sub_LoaderTRA2T { get { return _Sub_LoaderTRA2T.Current; } set { CalculateRotate(BorderLoaderTRA2, _Sub_LoaderTRA2T, value); } }
        public double PreAlignX { get { return _PreAlignX.Current; } set { CalculatePosition(GridPreAlign, _PreAlignX, value, Orientation.Vertical); } }
        public double FineAlignX { get { return _FineAlignX.Current; } set { CalculatePosition(GridFineAlign, _FineAlignX, value, Orientation.Vertical); } }
        public double IRCutStageA1Y { get { return _IRCutStageA1Y.Current; } set { CalculatePosition(GridProcessTableA1, _IRCutStageA1Y, value, Orientation.Horizontal); } }
        public double IRCutStageA2Y { get { return _IRCutStageA2Y.Current; } set { CalculatePosition(GridProcessTableA2, _IRCutStageA2Y, value, Orientation.Horizontal); } }
        public double IRCutStageB1Y { get { return _IRCutStageB2Y.Current; } set { CalculatePosition(GridProcessTableB1, _IRCutStageB2Y, value, Orientation.Horizontal); } }
        public double IRCutStageB2Y { get { return _IRCutStageB1Y.Current; } set { CalculatePosition(GridProcessTableB2, _IRCutStageB1Y, value, Orientation.Horizontal); } }
        public double LaserHeadX { get { return _LaserHeadX.Current; } set { CalculatePosition(GridLaserHead, _LaserHeadX, value, Orientation.Vertical); } }
        public double LaserHeadZ { get { return _LaserHeadZ.Current; } set { CalculatePosition(GridLaserHeaderZCylinder, _LaserHeadZ, value, Orientation.Vertical); } }
        public double AfterIRCutTRAY { get { return _AfterIRCutTRAY.Current; } set { CalculatePosition(GridBreakTRA, _AfterIRCutTRAY, value, Orientation.Horizontal); } }
        public double AfterIRCutTRBY { get { return _AfterIRCutTRBY.Current; } set { CalculatePosition(GridBreakTRB, _AfterIRCutTRBY, value, Orientation.Horizontal); } }
        public double BreakTableB1Y { get { return _BreakTableB1Y.Current; } set { CalculatePosition(GridBreakTableB1, _BreakTableB1Y, value, Orientation.Horizontal); } }
        public double BreakTableB2Y { get { return _BreakTableB2Y.Current; } set { CalculatePosition(GridBreakTableB2, _BreakTableB2Y, value, Orientation.Horizontal); } }
        public double Sub_BreakTableB1T { get { return _Sub_BreakTableB1T.Current; } set { CalculateRotate(BorderBreakTableB1, _Sub_BreakTableB1T, value); } }
        public double Sub_BreakTableB2T { get { return _Sub_BreakTableB2T.Current; } set { CalculateRotate(BorderBreakTableB2, _Sub_BreakTableB2T, value); } }
        public double BreakTableA1Y { get { return _BreakTableA1Y.Current; } set { CalculatePosition(GridBreakTableA1, _BreakTableA1Y, value, Orientation.Horizontal); } }
        public double BreakTableA2Y { get { return _BreakTableA2Y.Current; } set { CalculatePosition(GridBreakTableA2, _BreakTableA2Y, value, Orientation.Horizontal); } }
        public double Sub_BreakTableA1T { get { return _Sub_BreakTableA1T.Current; } set { CalculateRotate(BorderBreakTableA1, _Sub_BreakTableA1T, value); } }
        public double Sub_BreakTableA2T { get { return _Sub_BreakTableA2T.Current; } set { CalculateRotate(BorderBreakTableA2, _Sub_BreakTableA2T, value); } }
        public double BreakingHead1X { get { return _BreakingHead1X.Current; } set { CalculatePosition(GridBreakingHead1, _BreakingHead1X, value, Orientation.Vertical); } }
        public double BreakingHead2X { get { return _BreakingHead2X.Current; } set { CalculatePosition(GridBreakingHead2, _BreakingHead2X, value, Orientation.Vertical); } }
        public double BreakingHead1Z { get { return _BreakingHead1Z.Current; } set { CalculatePosition(GridBreakingheadZ1Cylinder, _BreakingHead1Z, value, Orientation.Vertical); } }
        public double BreakingHead2Z { get { return _BreakingHead2Z.Current; } set { CalculatePosition(GridBreakingheadZ2Cylinder, _BreakingHead2Z, value, Orientation.Vertical); } }
        public double BreakAlign1X { get { return _BreakAlign1X.Current; } set { CalculatePosition(GridBreakAlign1, _BreakAlign1X, value, Orientation.Vertical); } }
        public double BreakAlign2X { get { return _BreakAlign2X.Current; } set { CalculatePosition(GridBreakAlign2, _BreakAlign2X, value, Orientation.Vertical); } }
        public double BreakAlign1Z { get { return _BreakAlign1Z.Current; } set { CalculatePosition(GridBreakingAlignZCylinder, _BreakAlign1Z, value, Orientation.Vertical); } }
        public double BreakAlign2Z { get { return _BreakAlign2Z.Current; } set { CalculatePosition(GridBreakingAlignZCylinder, _BreakAlign2Z, value, Orientation.Vertical); } }
        public double BeforeInspTRA1Y { get { return _BeforeInspTRA1Y.Current; } set { CalculatePosition(GridBeforeInspTRA1, _BeforeInspTRA1Y, value, Orientation.Horizontal); } }
        public double BeforeInspTRA2Y { get { return _BeforeInspTRA2Y.Current; } set { CalculatePosition(GridBeforeInspTRA2, _BeforeInspTRA2Y, value, Orientation.Horizontal); } }
        public double BeforeInspTRB1Y { get { return _BeforeInspTRB1Y.Current; } set { CalculatePosition(GridBeforeInspTRB1, _BeforeInspTRB1Y, value, Orientation.Horizontal); } }
        public double BeforeInspTRB2Y { get { return _BeforeInspTRB2Y.Current; } set { CalculatePosition(GridBeforeInspTRB2, _BeforeInspTRB2Y, value, Orientation.Horizontal); } }
        public double AfterInspTRA1Y { get { return _AfterInspTRA1Y.Current; } set { CalculatePosition(GridAfterInspTRA1, _AfterInspTRA1Y, value, Orientation.Horizontal); } }
        public double AfterInspTRA2Y { get { return _AfterInspTRA2Y.Current; } set { CalculatePosition(GridAfterInspTRA2, _AfterInspTRA2Y, value, Orientation.Horizontal); } }
        public double AfterInspTRA1T { get { return _AfterInspTRA1T.Current; } set { CalculateRotate(BorderAfterInspTRA1, _AfterInspTRA1T, value); } }
        public double AfterInspTRA2T { get { return _AfterInspTRA2T.Current; } set { CalculateRotate(BorderAfterInspTRA2, _AfterInspTRA2T, value); } }
        public double AfterInspTRB1Y { get { return _AfterInspTRB1Y.Current; } set { CalculatePosition(GridAfterInspTRB1, _AfterInspTRB1Y, value, Orientation.Horizontal); } }
        public double AfterInspTRB2Y { get { return _AfterInspTRB2Y.Current; } set { CalculatePosition(GridAfterInspTRB2, _AfterInspTRB2Y, value, Orientation.Horizontal); } }
        public double AfterInspTRB1T { get { return _AfterInspTRB1T.Current; } set { CalculateRotate(BorderAfterInspTRB1, _AfterInspTRB1T, value); } }
        public double AfterInspTRB2T { get { return _AfterInspTRB2T.Current; } set { CalculateRotate(BorderAfterInspTRB2, _AfterInspTRB2T, value); } }
        public double InspCameraX { get { return _InspCameraX.Current; } set { CalculatePosition(GridInspCamera, _InspCameraX, value, Orientation.Vertical); } }
        public double InspCameraZ { get { return _InspCameraZ.Current; } set { CalculatePosition(GridInspectionCamreraZ, _InspCameraZ, value, Orientation.Vertical); } }
        public double InspStageA1Y { get { return _InspStageA1Y.Current; } set { CalculatePosition(GridInspectionStageA1, _InspStageA1Y, value, Orientation.Horizontal); } }
        public double InspStageA2Y { get { return _InspStageA2Y.Current; } set { CalculatePosition(GridInspectionStageA2, _InspStageA2Y, value, Orientation.Horizontal); } }
        public double InspStageB1Y { get { return _InspStageB1Y.Current; } set { CalculatePosition(GridInspectionStageB1, _InspStageB1Y, value, Orientation.Horizontal); } }
        public double InspStageB2Y { get { return _InspStageB2Y.Current; } set { CalculatePosition(GridInspectionStageB2, _InspStageB2Y, value, Orientation.Horizontal); } }
        public double UnloaderAX { get { return _UnloaderAX.Current; } set { CalculatePosition(GridUnloaderA, _UnloaderAX, value, Orientation.Vertical); } }
        public double UnloaderBX { get { return _UnloaderBX.Current; } set { CalculatePosition(GridUnloaderB, _UnloaderBX, value, Orientation.Vertical); } }
        public double UnloaderAY { get { return _UnloaderAY.Current; } set { CalculatePosition(GridUnloaderA_Copy, _UnloaderAY, value, Orientation.Horizontal); } }
        public double UnloaderBY { get { return _UnloaderBY.Current; } set { CalculatePosition(GridUnloaderB_Copy, _UnloaderBY, value, Orientation.Horizontal); } }
        public double CstUnloaderBRotation { get { return _CstUnloaderBRotation.Current; } set { CalculateRotate(BorderCstUnloaderB, _CstUnloaderBRotation, value); } }
        public double CstUnloaderBUpDown { get { return _CstUnloaderBUpDown.Current; } set { CalculatePosition(GridCstUnloaderBCYlinder, _CstUnloaderBUpDown, value, Orientation.Vertical); } }
        public double CstUnloaderARotation { get { return _CstUnloaderARotation.Current; } set { CalculateRotate(BorderCstUnloaderA, _CstUnloaderARotation, value); } }
        public double CstUnloaderAUpDown { get { return _CstUnloaderAUpDown.Current; } set { CalculatePosition(GridCstUnloaderACYlinder, _CstUnloaderAUpDown, value, Orientation.Vertical); } }
        #endregion

        #region 실린더 
        public double CstLoaderBL_Grip_Cylinder { get { return _CstLoaderBL_Grip_Cylinder.Current; } set { CalculatePosition(lblCstLoaderBL_Grip_Cylinder, _CstLoaderBL_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double CstLoaderBR_Grip_Cylinder { get { return _CstLoaderBR_Grip_Cylinder.Current; } set { CalculatePosition(lblCstLoaderBR_Grip_Cylinder, _CstLoaderBR_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double CstLoaderAL_Grip_Cylinder { get { return _CstLoaderAL_Grip_Cylinder.Current; } set { CalculatePosition(lblCstLoaderAL_Grip_Cylinder, _CstLoaderAL_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double CstLoaderAR_Grip_Cylinder { get { return _CstLoaderAR_Grip_Cylinder.Current; } set { CalculatePosition(lblCstLoaderAR_Grip_Cylinder, _CstLoaderAR_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double LoaderTRB1_UpDown_Cylinder { get { return _LoaderTRB1_UpDown_Cylinder.Current; } set { CalculatePosition(lblLoaderTRB1_UpDown_Cylinder, _LoaderTRB1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double LoaderTRB2_UpDown_Cylinder { get { return _LoaderTRB2_UpDown_Cylinder.Current; } set { CalculatePosition(lblLoaderTRB2_UpDown_Cylinder, _LoaderTRB2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double LoaderTRA1_UpDown_Cylinder { get { return _LoaderTRA1_UpDown_Cylinder.Current; } set { CalculatePosition(lblLoaderTRA1_UpDown_Cylinder, _LoaderTRA1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double LoaderTRA2_UpDown_Cylinder { get { return _LoaderTRA2_UpDown_Cylinder.Current; } set { CalculatePosition(lblLoaderTRA2_UpDown_Cylinder, _LoaderTRA2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double CstLoaderA_Tilt_Cylinder { get { return _CstLoaderA_Tilt_Cylinder.Current; } set { CalculatePosition(lblCstLoaderA_Tilt_Cylinder, _CstLoaderA_Tilt_Cylinder, value, Orientation.Vertical); } }
        public double CstLoaderB_Tilt_Cylinder { get { return _CstLoaderB_Tilt_Cylinder.Current; } set { CalculatePosition(lblCstLoaderB_Tilt_Cylinder, _CstLoaderB_Tilt_Cylinder, value, Orientation.Vertical); } }

        public double LaserHead1_UpDown_Cylinder { get { return _LaserHead1_UpDown_Cylinder.Current; } set { CalculatePosition(lblLaserHead1_UpDown_Cylinder, _LaserHead1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double LaserHead2_UpDown_Cylinder { get { return _LaserHead2_UpDown_Cylinder.Current; } set { CalculatePosition(lblLaserHead2_UpDown_Cylinder, _LaserHead2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double LaserShutter_Cylinder { get { return _LaserShutter_Cylinder.Current; } set { CalculatePosition(lblLaserShutter_Cylinder, _LaserShutter_Cylinder, value, Orientation.Horizontal); } }
        public double IRCutStageA_UpDown_Cylinder { get { return _IRCutStageA_UpDown_Cylinder.Current; } set { CalculatePosition(lblIRCutStageA_UpDown_Cylinder, _IRCutStageA_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double IRCutStageB_UpDown_Cylinder { get { return _IRCutStageA_UpDown_Cylinder.Current; } set { CalculatePosition(lblIRCutStageB_UpDown_Cylinder, _IRCutStageA_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BreakTableA_UpDown_Cylinder { get { return _BreakTableA_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakTableA_UpDown_Cylinder, _BreakTableA_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BreakTalbeB_UpDown_Cylinder { get { return _BreakTableB_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakTableB_UpDown_Cylinder, _BreakTableB_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BreakTRB1_UpDown_Cylinder { get { return _BreakTRB1_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakTRB1_UpDown_Cylinder, _BreakTRB1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BreakTRB2_UpDown_Cylinder { get { return _BreakTRB2_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakTRB2_UpDown_Cylinder, _BreakTRB2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BreakTRA1_UpDown_Cylinder { get { return _BreakTRA1_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakTRA1_UpDown_Cylinder, _BreakTRA1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BreakTRA2_UpDown_Cylinder { get { return _BreakTRA2_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakTRA2_UpDown_Cylinder, _BreakTRA2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double LaserDummyShutter1_Cylinder { get { return _LaserDummyShutter1_Cylinder.Current; } set { CalculatePosition(lblLaserDummyShutter1_Cylinder, _LaserDummyShutter1_Cylinder, value, Orientation.Horizontal); } }
        public double LaserDummyShutter2_Cylinder { get { return _LaserDummyShutter2_Cylinder.Current; } set { CalculatePosition(lblLaserDummyShutter2_Cylinder, _LaserDummyShutter2_Cylinder, value, Orientation.Horizontal); } }
        public double DummyTank_Cylinder { get { return _DummyTank_Cylinder.Current; } set { CalculatePosition(GridDummyTankLid, _DummyTank_Cylinder, value, Orientation.Horizontal); } }

        public double Buffer_UpDown_Cylinder { get { return _Buffer_UpDown_Cylinder.Current; } set { CalculatePosition(lblBuffer_UpDown_Cylinder, _Buffer_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double CstUnloaderBL_Grip_Cylinder { get { return _CstUnloaderBL_Grip_Cylinder.Current; } set { CalculatePosition(lblCstUnloaderBL_Grip_Cylinder, _CstUnloaderBL_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double CstUnloaderBR_Grip_Cylinder { get { return _CstUnloaderBR_Grip_Cylinder.Current; } set { CalculatePosition(lblCstUnloaderBR_Grip_Cylinder, _CstUnloaderBR_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double CstUnloaderAL_Grip_Cylinder { get { return _CstUnloaderAL_Grip_Cylinder.Current; } set { CalculatePosition(lblCstUnloaderAL_Grip_Cylinder, _CstUnloaderAL_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double CstUnloaderAR_Grip_Cylinder { get { return _CstUnloaderAR_Grip_Cylinder.Current; } set { CalculatePosition(lblCstUnloaderAR_Grip_Cylinder, _CstUnloaderAR_Grip_Cylinder, value, Orientation.Horizontal); } }
        public double AfterInspTRB1_UpDown_Cylinder { get { return _AfterInspTRB1_UpDown_Cylinder.Current; } set { CalculatePosition(lblAfterInspTRB1_UpDown_Cylinder, _AfterInspTRB1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double AfterInspTRA1_UpDown_Cylinder { get { return _AfterInspTRA1_UpDown_Cylinder.Current; } set { CalculatePosition(lblAfterInspTRA1_UpDown_Cylinder, _AfterInspTRA1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double AfterInspTRB2_UpDown_Cylinder { get { return _AfterInspTRB2_UpDown_Cylinder.Current; } set { CalculatePosition(lblAfterInspTRB2_UpDown_Cylinder, _AfterInspTRB2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double AfterInspTRA2_UpDown_Cylinder { get { return _AfterInspTRA2_UpDown_Cylinder.Current; } set { CalculatePosition(lblAfterInspTRA2_UpDown_Cylinder, _AfterInspTRA2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double CstUnloaderA_Tilt_Cylinder { get { return _CstUnloaderA_Tilt_Cylinder.Current; } set { CalculatePosition(lblCstUnloaderA_Tilt_Cylinder, _CstUnloaderA_Tilt_Cylinder, value, Orientation.Vertical); } }
        public double CstUnloaderB_Tilt_Cylinder { get { return _CstUnloaderB_Tilt_Cylinder.Current; } set { CalculatePosition(lblCstUnloaderB_Tilt_Cylinder, _CstUnloaderB_Tilt_Cylinder, value, Orientation.Vertical); } }

        //public double BreakingHead1_UpDown_Cylinder { get { return _BreakingHead1_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakingHead1_UpDown_Cylinder, _BreakingHead1_UpDown_Cylinder, value, Orientation.Vertical); } }
        //public double BreakingHead2_UpDown_Cylinder { get { return _BreakingHead2_UpDown_Cylinder.Current; } set { CalculatePosition(lblBreakingHead1_UpDown_Cylinder, _BreakingHead2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BeforeInspTRB1_UpDown_Cylinder { get { return _BeforeInspTRB1_UpDown_Cylinder.Current; } set { CalculatePosition(lblBeforeInspTRB1_UpDown_Cylinder, _BeforeInspTRB1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BeforeInspTRA1_UpDown_Cylinder { get { return _BeforeInspTRA1_UpDown_Cylinder.Current; } set { CalculatePosition(lblBeforeInspTRA1_UpDown_Cylinder, _BeforeInspTRA1_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BeforeInspTRB2_UpDown_Cylinder { get { return _BeforeInspTRB2_UpDown_Cylinder.Current; } set { CalculatePosition(lblBeforeInspTRB2_UpDown_Cylinder, _BeforeInspTRB2_UpDown_Cylinder, value, Orientation.Vertical); } }
        public double BeforeInspTRA2_UpDown_Cylinder { get { return _BeforeInspTRA2_UpDown_Cylinder.Current; } set { CalculatePosition(lblBeforeInspTRA2_UpDown_Cylinder, _BeforeInspTRA2_UpDown_Cylinder, value, Orientation.Vertical); } }
        //public double InspA1_UpDown_Cylinder { get { return _InspA1_UpDown_Cylinder.Current; } set { CalculatePosition(lblInspStageA1_UpDown_Cylinder, _InspA1_UpDown_Cylinder, value, Orientation.Vertical); } }
        //public double InspA2_UpDown_Cylinder { get { return _InspA2_UpDown_Cylinder.Current; } set { CalculatePosition(lblInspStageA2_UpDown_Cylinder, _InspA2_UpDown_Cylinder, value, Orientation.Vertical); } }
        //public double InspB1_UpDown_Cylinder { get { return _InspB1_UpDown_Cylinder.Current; } set { CalculatePosition(lblInspStageB1_UpDown_Cylinder, _InspB1_UpDown_Cylinder, value, Orientation.Vertical); } }
        //public double InspB2_UpDown_Cylinder { get { return _InspB2_UpDown_Cylinder.Current; } set { CalculatePosition(lblInspStageB2_UpDown_Cylinder, _InspB2_UpDown_Cylinder, value, Orientation.Vertical); } }
        #endregion

        #region 버큠
        public Brush LoaderA { get { return lblLoaderA_Copy1.Background; } set { lblLoaderA_Copy1.Background = value; } }
        public Brush LoaderB { get { return lblLoaderB_Copy1.Background; } set { lblLoaderB_Copy1.Background = value; } }
        public Brush LoaderTRA1 { get { return lblLoderTRA1_Copy.Background; } set { lblLoderTRA1_Copy.Background = value; } }
        public Brush LoaderTRA2 { get { return lblLoderTRA2_Copy.Background; } set { lblLoderTRA2_Copy.Background = value; } }
        public Brush LoaderTRB1 { get { return lblLoderTRB1_Copy.Background; } set { lblLoderTRB1_Copy.Background = value; } }
        public Brush LoaderTRB2 { get { return lblLoderTRB2_Copy.Background; } set { lblLoderTRB2_Copy.Background = value; } }
        public Brush ProcessTableA1_1 { get { return lblProcessTableA1_1_Copy.Background; } set { lblProcessTableA1_1_Copy.Background = value; } }
        public Brush ProcessTableA2_1 { get { return lblProcessTableA2_1_Copy.Background; } set { lblProcessTableA2_1_Copy.Background = value; } }
        public Brush ProcessTableB1_1 { get { return lblProcessTableB1_1_Copy.Background; } set { lblProcessTableB1_1_Copy.Background = value; } }
        public Brush ProcessTableB2_1 { get { return lblProcessTableB2_1_Copy.Background; } set { lblProcessTableB2_1_Copy.Background = value; } }
        public Brush ProcessTableA1_2 { get { return lblProcessTableA1_2_Copy.Background; } set { lblProcessTableA1_2_Copy.Background = value; } }
        public Brush ProcessTableA2_2 { get { return lblProcessTableA2_2_Copy.Background; } set { lblProcessTableA2_2_Copy.Background = value; } }
        public Brush ProcessTableB1_2 { get { return lblProcessTableB1_2_Copy.Background; } set { lblProcessTableB1_2_Copy.Background = value; } }
        public Brush ProcessTableB2_2 { get { return lblProcessTableB2_2_Copy.Background; } set { lblProcessTableB2_2_Copy.Background = value; } }
        public Brush BreakTRA { get { return lblBreakTRA1_Copy.Background; } set { lblBreakTRA1_Copy.Background = value; } }
        public Brush BreakTRB { get { return lblBreakTRB1_Copy.Background; } set { lblBreakTRB1_Copy.Background = value; } }
        public Brush BreakTRA_ { get { return lblBreakTRA2_Copy.Background; } set { lblBreakTRA2_Copy.Background = value; } }
        public Brush BreakTRB_ { get { return lblBreakTRB2_Copy.Background; } set { lblBreakTRB2_Copy.Background = value; } }
        public Brush BreakTableA1 { get { return lblBreakTableA1_Copy.Background; } set { lblBreakTableA1_Copy.Background = value; } }
        public Brush BreakTableA2 { get { return lblBreakTableA2_Copy.Background; } set { lblBreakTableA2_Copy.Background = value; } }
        public Brush BreakTableB1 { get { return lblBreakTableB1_Copy.Background; } set { lblBreakTableB1_Copy.Background = value; } }
        public Brush BreakTableB2 { get { return lblBreakTableB2_Copy.Background; } set { lblBreakTableB2_Copy.Background = value; } }
        public Brush BeforeInspTRA1 { get { return lblBeforeInspTRA1_Copy.Background; } set { lblBeforeInspTRA1_Copy.Background = value; } }
        public Brush BeforeInspTRA2 { get { return lblBeforeInspTRA2_Copy.Background; } set { lblBeforeInspTRA2_Copy.Background = value; } }
        public Brush BeforeInspTRB1 { get { return lblBeforeInspTRB1_Copy.Background; } set { lblBeforeInspTRB1_Copy.Background = value; } }
        public Brush BeforeInspTRB2 { get { return lblBeforeInspTRB2_Copy.Background; } set { lblBeforeInspTRB2_Copy.Background = value; } }
        public Brush InspStageA1 { get { return lblInspStageA1_Copy.Background; } set { lblInspStageA1_Copy.Background = value; } }
        public Brush InspStageA2 { get { return lblInspStageA2_Copy.Background; } set { lblInspStageA2_Copy.Background = value; } }
        public Brush InspStageB1 { get { return lblInspStageB1_Copy.Background; } set { lblInspStageB1_Copy.Background = value; } }
        public Brush InspStageB2 { get { return lblInspStageB2_Copy.Background; } set { lblInspStageB2_Copy.Background = value; } }
        public Brush AfterInspTRA1 { get { return lblAfterInspTRA1_Copy.Background; } set { lblAfterInspTRA1_Copy.Background = value; } }
        public Brush AfterInspTRA2 { get { return lblAfterInspTRA2_Copy.Background; } set { lblAfterInspTRA2_Copy.Background = value; } }
        public Brush AfterInspTRB1 { get { return lblAfterInspTRB1_Copy.Background; } set { lblAfterInspTRB1_Copy.Background = value; } }
        public Brush AfterInspTRB2 { get { return lblAfterInspTRB2_Copy.Background; } set { lblAfterInspTRB2_Copy.Background = value; } }
        public Brush UnloaderA { get { return lblUnloaderA_Copy1.Background; } set { lblUnloaderA_Copy1.Background = value; } }
        public Brush UnloaderB { get { return lblUnloaderB_Copy1.Background; } set { lblUnloaderB_Copy1.Background = value; } }
        #endregion

        #region 글래스
        public Brush LoaderA_glass { get { return lblLoaderA_Copy2.Background; } set { lblLoaderA_Copy2.Background = value; } }
        public Brush LoaderB_glass { get { return lblLoaderB_Copy2.Background; } set { lblLoaderB_Copy2.Background = value; } }
        public Brush LoaderTRA1_glass { get { return lblLoderTRA1_Copy1.Background; } set { lblLoderTRA1_Copy1.Background = value; } }
        public Brush LoaderTRA2_glass { get { return lblLoderTRA2_Copy1.Background; } set { lblLoderTRA2_Copy1.Background = value; } }
        public Brush LoaderTRB1_glass { get { return lblLoderTRB1_Copy1.Background; } set { lblLoderTRB1_Copy1.Background = value; } }
        public Brush LoaderTRB2_glass { get { return lblLoderTRB2_Copy1.Background; } set { lblLoderTRB2_Copy1.Background = value; } }
        public Brush ProcessTableA1_1_glass { get { return lblProcessTableA1_1_Copy1.Background; } set { lblProcessTableA1_1_Copy1.Background = value; } }
        public Brush ProcessTableA2_1_glass { get { return lblProcessTableA2_1_Copy1.Background; } set { lblProcessTableA2_1_Copy1.Background = value; } }
        public Brush ProcessTableB1_1_glass { get { return lblProcessTableB1_1_Copy1.Background; } set { lblProcessTableB1_1_Copy1.Background = value; } }
        public Brush ProcessTableB2_1_glass { get { return lblProcessTableB2_1_Copy1.Background; } set { lblProcessTableB2_1_Copy1.Background = value; } }
        public Brush ProcessTableA1_2_glass { get { return lblProcessTableA1_2_Copy1.Background; } set { lblProcessTableA1_2_Copy1.Background = value; } }
        public Brush ProcessTableA2_2_glass { get { return lblProcessTableA2_2_Copy1.Background; } set { lblProcessTableA2_2_Copy1.Background = value; } }
        public Brush ProcessTableB1_2_glass { get { return lblProcessTableB1_2_Copy1.Background; } set { lblProcessTableB1_2_Copy1.Background = value; } }
        public Brush ProcessTableB2_2_glass { get { return lblProcessTableB2_2_Copy1.Background; } set { lblProcessTableB2_2_Copy1.Background = value; } }
        public Brush BreakTRA_glass { get { return lblBreakTRA1_Copy1.Background; } set { lblBreakTRA1_Copy1.Background = value; } }
        public Brush BreakTRB_glass { get { return lblBreakTRB1_Copy1.Background; } set { lblBreakTRB1_Copy1.Background = value; } }
        public Brush BreakTRA_glass_ { get { return lblBreakTRA2_Copy1.Background; } set { lblBreakTRA2_Copy1.Background = value; } }
        public Brush BreakTRB_glass_ { get { return lblBreakTRB2_Copy1.Background; } set { lblBreakTRB2_Copy1.Background = value; } }
        public Brush BreakTableA1_glass { get { return lblBreakTableA1_Copy1.Background; } set { lblBreakTableA1_Copy1.Background = value; } }
        public Brush BreakTableA2_glass { get { return lblBreakTableA2_Copy1.Background; } set { lblBreakTableA2_Copy1.Background = value; } }
        public Brush BreakTableB1_glass { get { return lblBreakTableB1_Copy1.Background; } set { lblBreakTableB1_Copy1.Background = value; } }
        public Brush BreakTableB2_glass { get { return lblBreakTableB2_Copy1.Background; } set { lblBreakTableB2_Copy1.Background = value; } }
        public Brush BeforeInspTRA1_glass { get { return lblBeforeInspTRA1_Copy1.Background; } set { lblBeforeInspTRA1_Copy1.Background = value; } }
        public Brush BeforeInspTRA2_glass { get { return lblBeforeInspTRA2_Copy1.Background; } set { lblBeforeInspTRA2_Copy1.Background = value; } }
        public Brush BeforeInspTRB1_glass { get { return lblBeforeInspTRB1_Copy1.Background; } set { lblBeforeInspTRB1_Copy1.Background = value; } }
        public Brush BeforeInspTRB2_glass { get { return lblBeforeInspTRB2_Copy1.Background; } set { lblBeforeInspTRB2_Copy1.Background = value; } }
        public Brush InspStageA1_glass { get { return lblInspStageA1_Copy1.Background; } set { lblInspStageA1_Copy1.Background = value; } }
        public Brush InspStageA2_glass { get { return lblInspStageA2_Copy1.Background; } set { lblInspStageA2_Copy1.Background = value; } }
        public Brush InspStageB1_glass { get { return lblInspStageB1_Copy1.Background; } set { lblInspStageB1_Copy1.Background = value; } }
        public Brush InspStageB2_glass { get { return lblInspStageB2_Copy1.Background; } set { lblInspStageB2_Copy1.Background = value; } }
        public Brush AfterInspTRA1_glass { get { return lblAfterInspTRA1_Copy1.Background; } set { lblAfterInspTRA1_Copy1.Background = value; } }
        public Brush AfterInspTRA2_glass { get { return lblAfterInspTRA2_Copy1.Background; } set { lblAfterInspTRA2_Copy1.Background = value; } }
        public Brush AfterInspTRB1_glass { get { return lblAfterInspTRB1_Copy1.Background; } set { lblAfterInspTRB1_Copy1.Background = value; } }
        public Brush AfterInspTRB2_glass { get { return lblAfterInspTRB2_Copy1.Background; } set { lblAfterInspTRB2_Copy1.Background = value; } }
        public Brush UnloaderA_glass { get { return lblUnloaderA_Copy2.Background; } set { lblUnloaderA_Copy2.Background = value; } }
        public Brush UnloaderB_glass { get { return lblUnloaderB_Copy2.Background; } set { lblUnloaderB_Copy2.Background = value; } }
        #endregion

        private void CalculatePosition(Grid moveObj, ImgMoveInfo moveInfo, double value, Orientation moveAxis, Border sensor = null)
        {
            CalculatePosition((object)moveObj, moveInfo, value, moveAxis);
            if (sensor != null)
                sensor.Background = moveInfo.Current == moveInfo.Stroke ? Brushes.Red : Brushes.Blue;
        }
        private void CalculatePosition(Border moveObj, ImgMoveInfo moveInfo, double value, Orientation moveAxis, Border sensor = null)
        {
            CalculatePosition((object)moveObj, moveInfo, value, moveAxis);
            if (sensor != null)
                sensor.Background = moveInfo.Current == moveInfo.Stroke ? Brushes.Red : Brushes.Blue;
        }
        private void CalculatePosition(object moveObj, ImgMoveInfo moveInfo, double value, Orientation moveAxis)
        {
            if (double.IsNaN(value)) return;

            moveInfo.Current = value;
            double scalePosi = moveInfo.ImgStart + (moveInfo.ImgEnd - moveInfo.ImgStart) * (moveInfo.Current / moveInfo.Stroke);
            (moveObj as FrameworkElement).Margin = new Thickness()
            {
                Left = moveAxis == Orientation.Horizontal ? scalePosi : (moveObj as FrameworkElement).Margin.Left,
                Right = 0,
                Top = moveAxis == Orientation.Vertical ? scalePosi : (moveObj as FrameworkElement).Margin.Top,
                Bottom = 0,
            };
        }
        private void CalculateRotate(Border moveObj, ImgMoveInfo moveInfo, double value, Border sensor = null)
        {
            moveInfo.Current = value;
            double scaleAngle = moveInfo.ImgStart + (moveInfo.ImgEnd - moveInfo.ImgStart) * (moveInfo.Current / moveInfo.Stroke);
            moveObj.RenderTransform = new RotateTransform(scaleAngle);

            if (sensor != null)
                sensor.Background = moveInfo.Current == moveInfo.Stroke ? Brushes.Red : Brushes.Blue;
        }
        
    }
}
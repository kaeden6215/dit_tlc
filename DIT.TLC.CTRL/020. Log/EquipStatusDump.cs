﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public enum EM_LOG_TP
    {
        Centering = 0,
        Motor,
        Alarm,
        Normal,
    }
    public class LogList
    {
        public string Name { get; set; }
        public float StateValue { get; set; }
        public bool State { get; set; }

        public LogList(string name, float value)
        {
            this.Name = name;
            this.StateValue = value;
        }
        public LogList(string name, bool state)
        {
            this.Name = name;
            this.State = state;
        }
    }
    public static class EquipStatusDump
    {
        public static void LogEquipState(Equipment equip, EM_LOG_TP logType)
        {
            if (logType == EM_LOG_TP.Alarm)
            {
                LogMotorsPosition(equip);
                //LogGrabberVacuumState(equip);
                //LogSettingState(equip);
                //LogGlassSensorState(equip);
            }
            else if (logType == EM_LOG_TP.Centering)
            {
                LogMotorsPosition(equip);
                //LogGrabberVacuumState(equip);
                //LogSettingState(equip);
                //LogGlassSensorState(equip);
            }
            else if (logType == EM_LOG_TP.Motor)
            {
                LogMotorsPosition(equip);
               // LogGrabberVacuumState(equip);
                //LogSettingState(equip);
                //LogGlassSensorState(equip);
            }
            else if (logType == EM_LOG_TP.Normal)
            {
                LogMotorsPosition(equip);
                //LogGrabberVacuumState(equip);
                //LogSettingState(equip);
                //LogGlassSensorState(equip);
            }
        }
        public static void LogMotorsPosition(Equipment equip)
        {            
            Logger.Log.AppendLine(LogLevel.NoLog, "[LogMotorsPosition]");
            //foreach (ServoMotorControl motor in equip.Motors)
            //{
            //    Logger.Log.AppendLine(LogLevel.NoLog, string.Format("{0} - Pos : {1}, Stress : {2}, ServoError : {3}, HomeBit : {4}",
            //        motor.Name, motor.XF_CurrMotorPosition, motor.XF_CurrMotorStress.vFloat, motor.IsServoError, motor.XB_StatusHomeCompleteBit.vBit));
            //}            
        }
        public static void LogGrabberVacuumState(Equipment equip)
        {
            Logger.Log.AppendLine(LogLevel.NoLog, "[LogGrabberVacuumState]");
        }
         
    }
}

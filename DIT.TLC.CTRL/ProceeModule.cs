﻿using Dit.Framework.Comm;
using Dit.Framework.PLC;
using DIT.TLC.CTRL._050._Struct._520._Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DIT.TLC.CTRL
{

    public class ProceeModule
    {
        public LaserCuttingStageUnit IRCutStage_B { get; set; }
        public LaserCuttingStageUnit IRCutStage_A { get; set; }
        public FineAlginProxy FineAlign { get; set; }
        //Break Align??? 아래 A,B 있음
        public BreakAlignProxy BreakAlign { get; set; }
        public LaserCuttingHeadUnit LaserHead { get; set; }
        public BreakingHeadUnit BreakingHead { get; set; }

        public MasureUnit Masure { get; set; }

        public AfterIRCutTransferUnit AfterIRCutTransfer_A { get; set; }
        public AfterIRCutTransferUnit AfterIRCutTransfer_B { get; set; }


        //BreakAlign A1A2 B1B2 필요?
        public BreakAlignProxy BreakAlign_B { get; set; }
        public BreakAlignProxy BreakAlign_A { get; set; }

        public BreakStageUnit BreakStage_B { get; set; }
        public BreakStageUnit BreakStage_A { get; set; }

        public ConvertA2D DLMBlowerPressureValue { get; internal set; }
        public ConvertA2D PanelInnerTempValue { get; internal set; }

        public bool IsDoorRelease
        {
            get
            {
                return Door04.IsOnOff == true || Door05.IsOnOff == true || Door06.IsOnOff == true ||
                    Door07.IsOnOff == true || Door08.IsOnOff == true;
            }
        }
        public bool IsDoorOpen
        {
            get
            {
                return DoorOpen04.IsOnOff == true || DoorOpen05.IsOnOff == true || DoorOpen06.IsOnOff == true;
            }
        }
        public bool IsDoorKeyBoxOn
        {
            get
            {
                return DoorKeyBox04.XB_OnOff.vBit == false || DoorKeyBox05.XB_OnOff.vBit == false || DoorKeyBox06.XB_OnOff.vBit == false;
            }
        }
        public void DoorOpenClose(Equipment equip, bool open)
        {
            Door04.OnOff(equip, open);
            Door05.OnOff(equip, open);
            Door06.OnOff(equip, open);
            Door07.OnOff(equip, open);
            Door08.OnOff(equip, open);
        }

        public bool IsHomeComplete
        {
            get
            {
                return
                    IRCutStage_B.IsHomeComplete &&
                    IRCutStage_A.IsHomeComplete &&
                    FineAlign.IsHomeComplete &&
                    BreakAlign.IsHomeComplete &&
                    LaserHead.IsHomeComplete &&
                    BreakingHead.IsHomeComplete &&
                    Masure.IsHomeComplete &&
                    AfterIRCutTransfer_A.IsHomeComplete &&
                    AfterIRCutTransfer_B.IsHomeComplete &&
                    BreakAlign_B.IsHomeComplete &&
                    BreakAlign_A.IsHomeComplete &&
                    BreakStage_B.IsHomeComplete &&
                    BreakStage_A.IsHomeComplete;
            }
        }


        #region CUTING UMAC
        public Sensor MCOff1                                                         /**/    = new Sensor(false);
        public Sensor MCOff2                                                         /**/    = new Sensor(false);
        public Sensor EmergencyStopEnableGripSwitch                                  /**/    = new Sensor();
        public Sensor SafetyEnableGripSwitchOn                                       /**/    = new Sensor();

        public OffCheckSwitch Door04                                                 /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door05                                                 /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door06                                                 /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door07                                                 /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door08                                                 /**/    = new OffCheckSwitch();


        public Sensor ProcEmergencyStop4                                             /**/    = new Sensor();
        public Switch ChillerMCOff                                                   /**/    = new Switch();
        public Switch Safety이중화CP1                                                /**/    = new Switch();
        public Switch Safety이중화CP2                                                /**/    = new Switch();

        public Sensor DoorKeyBox04                                                   /**/    = new Sensor();
        public Sensor DoorKeyBox05                                                   /**/    = new Sensor();
        public Sensor DoorKeyBox06                                                   /**/    = new Sensor();


        public Sensor DoorOpen04                                                      /**/    = new Sensor(false);
        public Sensor DoorOpen05                                                      /**/    = new Sensor(false);
        public Sensor DoorOpen06                                                      /**/    = new Sensor(false);
 

        public Sensor LaserAlarm                                                     /**/    = new Sensor();
        public Sensor LaserOn                                                        /**/    = new Sensor();
        public Switch CpBoxTempResetSwitch                                           /**/    = new Switch();



        public Switch DLMBlowerPressureSwitch                                        /**/    = new Switch();
        public Sensor Process판넬화재Alarm                                           /**/    = new Sensor();
        public Sensor Process판넬온도Alarm                                           /**/    = new Sensor();
        public Sensor CpBoxOpenDetectBJub                                            /**/    = new Sensor();

        public Sensor LaserLeak                                                      /**/    = new Sensor(false);
        public Sensor ChillerLeak                                                    /**/    = new Sensor(false);
        public Sensor DummyTankDetect                                                /**/    = new Sensor();

        public Sensor ProcessFanStopAlarm01                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm02                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm03                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm04                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm05                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm06                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm07                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm08                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm09                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm10                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm11                                          /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm12                                          /**/    = new Sensor();


        public Sensor IonizerRun1                                                    /**/    = new Sensor();
        public Sensor IonizerAlarm1_1                                                /**/    = new Sensor();
        public Sensor IonizerAlarm1_2                                                /**/    = new Sensor();
        public Sensor IonizerRun2                                                    /**/    = new Sensor();
        public Sensor IonizerAlarm2_1                                                /**/    = new Sensor();
        public Sensor IonizerAlarm2_2                                                /**/    = new Sensor();
        public Sensor IonizerRun3                                                    /**/    = new Sensor();
        public Sensor IonizerAlarm3_1                                                /**/    = new Sensor();
        public Sensor IonizerAlarm3_2                                                /**/    = new Sensor();
        public Sensor IonizerRun4                                                    /**/    = new Sensor();
        public Sensor IonizerAlarm4_1                                                /**/    = new Sensor();
        public Sensor IonizerAlarm4_2                                                /**/    = new Sensor();

        public SwitchOneWay MotorControl                                             /**/    = new SwitchOneWay();
        public SwitchOneWay LaserStopInternal                                        /**/    = new SwitchOneWay();
        public SwitchOneWay LaserStopExternal                                        /**/    = new SwitchOneWay();
        public SwitchOneWay LaserEmergencyStop                                       /**/    = new SwitchOneWay();
        public SwitchOneWay InnerLamp                                                /**/    = new SwitchOneWay();
        public SwitchOneWay LaserOnDisplayBoard                                      /**/    = new SwitchOneWay();


        public SwitchOneWay EziServoY축Control                                       /**/    = new SwitchOneWay();
        public SwitchOneWay EziServoX축Control                                       /**/    = new SwitchOneWay();
        public SwitchOneWay EziServoT축Control                                       /**/    = new SwitchOneWay();

        public SwitchOneWay IonizerRunCmd1                                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd1                                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd1                                       /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerRunCmd2                                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd2                                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd2                                       /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerRunCmd3                                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd3                                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd3                                       /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerRunCmd4                                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd4                                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd4                                       /**/    = new SwitchOneWay();
        #endregion
        public ProceeModule()
        {
            IRCutStage_B = new LaserCuttingStageUnit()             /**/{ Name = "컷팅 테이블 - B",             /**/UnitPosi = EmUnitPosi.BCol, UnitNo = 10, ShareMem = (VirtualShare)GG.EQ_DATA };
            IRCutStage_A = new LaserCuttingStageUnit()             /**/{ Name = "컷팅 테이블 - A",             /**/UnitPosi = EmUnitPosi.ACol, UnitNo = 11, ShareMem = (VirtualShare)GG.EQ_DATA };
            FineAlign = new FineAlginProxy()                        /**/{ Name = "Fine 얼라이먼트",            /**/UnitPosi = EmUnitPosi.None, UnitNo = 12, ShareMem = (VirtualShare)GG.EQ_DATA };
            LaserHead = new LaserCuttingHeadUnit()                  /**/{ Name = "레이져 해더",                /**/UnitPosi = EmUnitPosi.None, UnitNo = 13, ShareMem = (VirtualShare)GG.EQ_DATA };
            BreakingHead = new BreakingHeadUnit()                   /**/{ Name = "브레이킹 해더  ",            /**/UnitPosi = EmUnitPosi.None, UnitNo = 14, ShareMem = (VirtualShare)GG.EQ_DATA };

            Masure = new MasureUnit()                               /**/{ Name = "측정",                       /**/UnitPosi = EmUnitPosi.None, UnitNo = 15, ShareMem = (VirtualShare)GG.EQ_DATA };
            AfterIRCutTransfer_B = new AfterIRCutTransferUnit()    /**/{ Name = "After IRCut 트랜스퍼 - B",    /**/UnitPosi = EmUnitPosi.BCol, UnitNo = 16, ShareMem = (VirtualShare)GG.EQ_DATA };
            AfterIRCutTransfer_A = new AfterIRCutTransferUnit()    /**/{ Name = "After IRCut 트랜스퍼 - A",    /**/UnitPosi = EmUnitPosi.ACol, UnitNo = 17, ShareMem = (VirtualShare)GG.EQ_DATA };

            BreakAlign_B = new BreakAlignProxy()                   /**/{ Name = "Break 얼라이먼트 - B",        /**/UnitPosi = EmUnitPosi.BCol, UnitNo = 18, ShareMem = (VirtualShare)GG.EQ_DATA };
            BreakAlign_A = new BreakAlignProxy()                   /**/{ Name = "Break 얼라이먼트 - A",        /**/UnitPosi = EmUnitPosi.ACol, UnitNo = 19, ShareMem = (VirtualShare)GG.EQ_DATA };

            BreakStage_B = new BreakStageUnit()                    /**/{ Name = "브레이킹 테이블 - B",         /**/UnitPosi = EmUnitPosi.BCol, UnitNo = 20, ShareMem = (VirtualShare)GG.EQ_DATA };
            BreakStage_A = new BreakStageUnit()                    /**/{ Name = "브레이킹 테이블 - A",         /**/UnitPosi = EmUnitPosi.ACol, UnitNo = 21, ShareMem = (VirtualShare)GG.EQ_DATA };
            BreakAlign = new BreakAlignProxy()                      /**/{ Name = "브레이킹 얼라이먼트",        /**/UnitPosi = EmUnitPosi.None, UnitNo = 22, ShareMem = (VirtualShare)GG.EQ_DATA };
        }

        public void LogicWorking(Equipment equip)
        {

            IRCutStage_B.LogicWorking(equip);
            IRCutStage_A.LogicWorking(equip);
            FineAlign.LogicWorking(equip);
            LaserHead.LogicWorking(equip); //jys:: motor
            BreakingHead.LogicWorking(equip); //jys:: motor

            //Masure.LogicWorking(equip);
            AfterIRCutTransfer_B.LogicWorking(equip);
            AfterIRCutTransfer_A.LogicWorking(equip);

            BreakStage_B.LogicWorking(equip);
            BreakStage_A.LogicWorking(equip);
            BreakAlign.LogicWorking(equip); //jys:: motor
            SatusLogicWorking(equip);

        }


        public void SatusLogicWorking(Equipment equip)
        {
            //로더부 알람. 처리. 


            if (Door04.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0020_DOOR_4_OPEN);
            if (Door05.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0021_DOOR_5_OPEN);
            if (Door06.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0022_DOOR_6_OPEN);
            if (Door07.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0023_DOOR_7_OPEN);
            if (Door08.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0024_DOOR_8_OPEN);
            if (ProcEmergencyStop4.IsOnOff == true)                                     /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0003_EMS_4);

            if (ChillerMCOff.IsOnOff == true)                                           /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0314_PROC_CHILLER_MC_DOWN);
            if (MCOff1.IsOnOff == true && MCOff2.IsOnOff == true)                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0047_PROC_MC_DOWN);
            if (MCOff1.IsOnOff != MCOff2.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0049_PROC_MC_ERROR);

            if (LaserLeak.IsOnOff == true)                                              /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0394_PROC_LASER_LEACK);
            if (ChillerLeak.IsOnOff == true)                                            /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0395_PROC_CHILLER_LEACK);
            if (Process판넬화재Alarm.IsOnOff == true)                                   /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0401_PROC_CONTROL_BOX_SMOKE_HEAVY);
            if (Process판넬온도Alarm.IsOnOff == true)                                   /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0403_PROC_CONTROL_BOX_TEMP_WARNS);
            if (CpBoxOpenDetectBJub.IsOnOff)                                            /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0029_PROC_CONTROL_BOX_DOOR_OPEN);
            if (DummyTankDetect.IsOnOff)                                                /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0493_PROC_DUMMY_TANK_1_LOST);


            if (ProcessFanStopAlarm01.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0404_PROC_CONTROL_BOX_FAN_1);
            if (ProcessFanStopAlarm02.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0405_PROC_CONTROL_BOX_FAN_2);
            if (ProcessFanStopAlarm03.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0406_PROC_CONTROL_BOX_FAN_3);
            if (ProcessFanStopAlarm04.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0407_PROC_CONTROL_BOX_FAN_4);
            if (ProcessFanStopAlarm05.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0408_PROC_CONTROL_BOX_FAN_5);
            if (ProcessFanStopAlarm06.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0409_PROC_CONTROL_BOX_FAN_6);
            if (ProcessFanStopAlarm07.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0410_PROC_CONTROL_BOX_FAN_7);
            if (ProcessFanStopAlarm08.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0411_PROC_CONTROL_BOX_FAN_8);
            if (ProcessFanStopAlarm09.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0412_PROC_CONTROL_BOX_FAN_9);
            if (ProcessFanStopAlarm10.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0413_PROC_CONTROL_BOX_FAN_10);
            if (ProcessFanStopAlarm11.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0414_PROC_CONTROL_BOX_FAN_11);
            if (ProcessFanStopAlarm12.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0415_PROC_CONTROL_BOX_FAN_12);


            if (IonizerAlarm1_1.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0568_IONIZER_1_ALARM);
            if (IonizerAlarm1_2.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0568_IONIZER_1_ALARM);
            if (IonizerAlarm2_1.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0569_IONIZER_2_ALARM);
            if (IonizerAlarm2_2.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0569_IONIZER_2_ALARM);
            if (IonizerAlarm3_1.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0570_IONIZER_3_ALARM);
            if (IonizerAlarm3_2.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0570_IONIZER_3_ALARM);
            if (IonizerAlarm4_1.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0571_IONIZER_4_ALARM);
            if (IonizerAlarm4_2.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0571_IONIZER_4_ALARM);

            //수치 변환 필요. 
            if (PanelInnerTempValue.Value > 40)
            {
                equip.SafetyMgr.GpsMainMccbTripCmd(0);
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0207_LD_CONTROL_BOX_TEMP_HEAVY);
            }
        }
    }
}

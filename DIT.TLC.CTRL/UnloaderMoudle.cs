﻿using Dit.Framework.Comm;
using Dit.Framework.PLC;
using DIT.TLC.CTRL._050._Struct._520._Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DIT.TLC.CTRL
{

    public class UnloaderMoudle
    {

        public BeforeInspUnLoaderTransferUnit BeforeInspUnloaderTransfer_B { get; set; }
        public BeforeInspUnLoaderTransferUnit BeforeInspUnloaderTransfer_A { get; set; }


        public InspStageUnit InspectionStage_B { get; set; }
        public InspStageUnit InspectionStage_A { get; set; }

        public CellBufferUnit CellBuffer { get; set; }
        public InspCameraUnit InspCamera { get; set; }

        public AfterInspUnLoaderTransferUnit AfterInspUnloaderTransfer_B { get; set; }
        public AfterInspUnLoaderTransferUnit AfterInspUnloaderTransfer_A { get; set; }

        public UnloaderUnit Unloader { get; set; }
        public CstUnloaderUint CstUnloader_B { get; set; }
        public CstUnloaderUint CstUnloader_A { get; set; }

        public bool IsDoorRelease
        {
            get
            {
                return Door09.IsOnOff == true || Door10.IsOnOff == true || Door11.IsOnOff == true;
            }
        }
        public bool IsDoorOpen
        {
            get
            {
                return DoorOpen09.IsOnOff == true || DoorOpen10.IsOnOff == true || DoorOpen11.IsOnOff == true;
            }
        }
        public bool IsDoorKeyBoxOn
        {
            get
            {
                return DoorKeyBox07.XB_OnOff == false || DoorKeyBox08.XB_OnOff == false || DoorKeyBox09.XB_OnOff == false;
            }
        }
        public void DoorOpenClose(Equipment equip, bool open)
        {
            Door09.OnOff(equip, open);
            Door10.OnOff(equip, open);
            Door11.OnOff(equip, open);
        }

        public ConvertA2D PanelInnerTempValue { get; internal set; }
        public ConvertA2D PcBoxTempValue { get; internal set; }

        public bool IsHomeComplete
        {
            get
            {
                return
                BeforeInspUnloaderTransfer_B.IsHomeComplete &&
                BeforeInspUnloaderTransfer_A.IsHomeComplete &&
                InspectionStage_B.IsHomeComplete &&
                InspectionStage_A.IsHomeComplete &&
                CellBuffer.IsHomeComplete &&
                InspCamera.IsHomeComplete &&
                AfterInspUnloaderTransfer_B.IsHomeComplete &&
                AfterInspUnloaderTransfer_A.IsHomeComplete &&
                Unloader.IsHomeComplete &&
                CstUnloader_B.IsHomeComplete &&
                CstUnloader_A.IsHomeComplete;
            }
        }


        #region ULD AJIN

        public Switch CpBoxResetSwitch                                                     /**/    = new Switch();
        public Switch GpsMainMccbTripSwitch                                                /**/    = new Switch();

        public Sensor ULDLiferMuting1_1                                                    /**/    = new Sensor();
        public Sensor ULDLiferMuting1_2                                                    /**/    = new Sensor();
        public Sensor ULDLiferMuting2_1                                                    /**/    = new Sensor();
        public Sensor ULDLiferMuting2_2                                                    /**/    = new Sensor();

        public Sensor ULDEmergencyStop5                                                    /**/    = new Sensor();
        public Sensor ULDEmergencyStop6                                                    /**/    = new Sensor();
        public Sensor ULDEmergencyStop7                                                    /**/    = new Sensor();
        public Switch EmergencyStopEnalbeGripSwitch3                                       /**/    = new Switch();
        public Switch SafteyEnableGripSwitchOn3                                            /**/    = new Switch();
        public OffCheckSwitch Door09                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door10                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door11                                                       /**/    = new OffCheckSwitch();


        public Sensor DoorOpen09                                                           /**/    = new Sensor(false);
        public Sensor DoorOpen10                                                           /**/    = new Sensor(false);
        public Sensor DoorOpen11                                                           /**/    = new Sensor();


        public OffCheckSwitch DoorKeyBox07                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch DoorKeyBox08                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch DoorKeyBox09                                                       /**/    = new OffCheckSwitch();



        public Sensor MCOff1                                                                /**/    = new Sensor(false);
        public Sensor MCOff2                                                                /**/    = new Sensor(false);
        public Sensor ControlSelectULD                                                     /**/    = new Sensor();
        public Sensor Cell투입Hand출동방지                                                  /**/    = new Sensor();
        public Switch ULDResetSwitchUserA                                                  /**/    = new Switch();
        public Switch ULDResetSwitchUserB                                                  /**/    = new Switch();
        public Sensor ULDIRCUT충돌방지Y1_Y3                                                 /**/    = new Sensor();
        public Sensor ULDIRCUT충돌방지Y2_Y4                                                 /**/    = new Sensor();

        //public Sensor BufferVacuumPressureSwitch                                           /**/    = new Sensor();
        //public Sensor BufferUpCylinder                                                     /**/    = new Sensor();
        //public Sensor BufferDownCylinder                                                   /**/    = new Sensor();


        public Switch Cell투입HandPressureSwitch1                                          /**/    = new Switch();
        public Switch Cell투입HandPressureSwitch2                                          /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith1                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith2                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith3                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith4                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith5                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith6                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith7                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith8                                      /**/    = new Switch();


        public Sensor CpBoxOpenDetectBJub                                                      /**/    = new Sensor();
        public Sensor ULDFanStopAlarm1                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm2                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm3                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm4                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm5                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm6                                                     /**/    = new Sensor();
        public Sensor ULD판넬화재알람                                                       /**/    = new Sensor();
        public Sensor ULD판넬온도알람                                                       /**/    = new Sensor();

        public Sensor PCRack온도알람                                                        /**/    = new Sensor();
        public Sensor PCRack화재알람                                                        /**/    = new Sensor();



        public SwitchOneWay ULDMotorControlOn                                              /**/ = new SwitchOneWay();

        public SwitchOneWay ULDAResetSwitchLamp                                            /**/ = new SwitchOneWay();
        public SwitchOneWay ULDBResetSwitchLamp                                            /**/ = new SwitchOneWay();
        public SwitchOneWay ULDACstMutingSwitchLamp                                        /**/ = new SwitchOneWay();
        public SwitchOneWay ULDBCstMutingSwitchLamp                                        /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampR                                                     /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampY                                                     /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampG                                                     /**/ = new SwitchOneWay();
        public SwitchOneWay ControlSelectULDCMD                                            /**/ = new SwitchOneWay();
        public SwitchOneWay CpBoxTempResetSWLamp                                           /**/ = new SwitchOneWay();
        public SwitchOneWay InnerLamp                                                      /**/ = new SwitchOneWay();
        public SwitchOneWay Buzzer                                                     /**/ = new SwitchOneWay();
        public SwitchOneWay 음성Buzzer                                                     /**/ = new SwitchOneWay();






        #endregion

        public UnloaderMoudle()
        {
            BeforeInspUnloaderTransfer_B = new BeforeInspUnLoaderTransferUnit() /**/{ Name = "검사 전 트랜스퍼 - B",          /**/ UnitPosi = EmUnitPosi.BCol, UnitNo = 36, ShareMem = (VirtualShare)GG.EQ_DATA };
            BeforeInspUnloaderTransfer_A = new BeforeInspUnLoaderTransferUnit() /**/{ Name = "검사 전 트랜스퍼 - A",          /**/ UnitPosi = EmUnitPosi.ACol, UnitNo = 37, ShareMem = (VirtualShare)GG.EQ_DATA };

            InspCamera = new InspCameraUnit()                                    /**/{ Name = "검사 카메라",                  /**/ UnitPosi = EmUnitPosi.None, UnitNo = 30, ShareMem = (VirtualShare)GG.EQ_DATA };
            Unloader = new UnloaderUnit()                                        /**/{ Name = "셀 언로더",                    /**/ UnitPosi = EmUnitPosi.None, UnitNo = 31, ShareMem = (VirtualShare)GG.EQ_DATA };

            CstUnloader_B = new CstUnloaderUint()                               /**/{ Name = "카셋트 언로더 - B",             /**/ UnitPosi = EmUnitPosi.BCol, UnitNo = 32, ShareMem = (VirtualShare)GG.EQ_DATA, Port = new PortInfo(EmUnitPosi.BCol, "B PORT") };
            CstUnloader_A = new CstUnloaderUint()                               /**/{ Name = "카셋트 언로더 - A",             /**/ UnitPosi = EmUnitPosi.ACol, UnitNo = 33, ShareMem = (VirtualShare)GG.EQ_DATA, Port = new PortInfo(EmUnitPosi.ACol, "A PORT") };
            InspectionStage_B = new InspStageUnit()                             /**/{ Name = "검사 테이블 - B",               /**/ UnitPosi = EmUnitPosi.BCol, UnitNo = 34, ShareMem = (VirtualShare)GG.EQ_DATA };
            InspectionStage_A = new InspStageUnit()                             /**/{ Name = "검사 테이블 - A",               /**/ UnitPosi = EmUnitPosi.ACol, UnitNo = 35, ShareMem = (VirtualShare)GG.EQ_DATA };

            AfterInspUnloaderTransfer_B = new AfterInspUnLoaderTransferUnit()   /**/{ Name = "검사 후 트랜스퍼 - B",           /**/ UnitPosi = EmUnitPosi.BCol, UnitNo = 36, ShareMem = (VirtualShare)GG.EQ_DATA };
            AfterInspUnloaderTransfer_A = new AfterInspUnLoaderTransferUnit()   /**/{ Name = "검사 후 트랜스퍼 - A",           /**/ UnitPosi = EmUnitPosi.ACol, UnitNo = 37, ShareMem = (VirtualShare)GG.EQ_DATA };

            CellBuffer = new CellBufferUnit()                                    /**/{ Name = "컷팅 테이블 - B",               /**/ UnitPosi = EmUnitPosi.None, UnitNo = 38, ShareMem = (VirtualShare)GG.EQ_DATA };
        }

        public void LogicWorking(Equipment equip)
        {
            BeforeInspUnloaderTransfer_B.LogicWorking(equip); //jys:: motor
            BeforeInspUnloaderTransfer_A.LogicWorking(equip); //jys:: motor

            InspCamera.LogicWorking(equip); //jys:: motor

            AfterInspUnloaderTransfer_B.LogicWorking(equip);
            AfterInspUnloaderTransfer_A.LogicWorking(equip);

            InspectionStage_B.LogicWorking(equip); //jys:: motor
            InspectionStage_A.LogicWorking(equip); //jys:: motor

            Unloader.LogicWorking(equip);

            CstUnloader_B.LogicWorking(equip);
            CstUnloader_A.LogicWorking(equip);

            SatusLogicWorking(equip);
        }

        public void SatusLogicWorking(Equipment equip)
        {
            //로더부 알람. 처리. 

            if (Door09.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0025_DOOR_9_OPEN);
            if (Door10.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0026_DOOR_10_OPEN);
            if (Door11.IsOnOff == true)                                                 /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0027_DOOR_11_OPEN);



            if (ULDEmergencyStop5.IsOnOff == true)                                     /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0004_EMS_5);
            if (ULDEmergencyStop6.IsOnOff == true)                                     /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0005_EMS_6);
            if (ULDEmergencyStop7.IsOnOff == true)                                     /**/ AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0006_EMS_7);

            if (MCOff1.IsOnOff == true && MCOff2.IsOnOff == true)                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0048_ULD_MC_DOWN);
            if (MCOff1.IsOnOff != MCOff2.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0050_ULD_MC_ERROR);
            if (CpBoxOpenDetectBJub.IsOnOff)                                            /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0030_ULD_CONTROL_BOX_DOOR_OPEN);


            if (ULDFanStopAlarm1.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0612_UNLOADER_CONTROL_BOX_FAN_1);
            if (ULDFanStopAlarm2.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0613_UNLOADER_CONTROL_BOX_FAN_2);
            if (ULDFanStopAlarm3.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0614_UNLOADER_CONTROL_BOX_FAN_3);
            if (ULDFanStopAlarm4.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0615_UNLOADER_CONTROL_BOX_FAN_4);
            if (ULDFanStopAlarm5.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0616_UNLOADER_CONTROL_BOX_FAN_5);
            if (ULDFanStopAlarm6.IsOnOff)                                       /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0617_UNLOADER_CONTROL_BOX_FAN_6);

            if (ULD판넬화재알람.IsOnOff == true)                                /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0609_ULD_CONTROL_BOX_SMOKE_HEAVY);
            if (ULD판넬온도알람.IsOnOff == true)                                /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0610_ULD_CONTROL_BOX_TEMP_WARING);

            if (PCRack온도알람.IsOnOff)                                         /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0013_PC_RACK_TEMP_LIGHT);
            if (PCRack화재알람.IsOnOff)                                         /**/AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0011_PC_SMOKE);
            
            //수치 변환 필요. 
            if (PanelInnerTempValue.Value > 40)
            {
                equip.SafetyMgr.GpsMainMccbTripCmd(0);
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0611_ULD_CONTROL_BOX_TEMP_HEAVY);
            }
            if (PcBoxTempValue.Value > 40)
            {
                equip.SafetyMgr.GpsMainMccbTripCmd(0);
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0012_PC_RACK_TEMP_HEAVY);
            }

        }
    }
}

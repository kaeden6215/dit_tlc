﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class EqpSystemParamSettingManager
    {
        public static string PathOfXml = Path.Combine(GG.StartupPath, "Setting", "EqpSystemParamSettingManager.Xml");

        public SystemParamSetting LstEqpSysParam;
        public EqpSystemParamSettingManager()
        {
            LstEqpSysParam = new SystemParamSetting();
        }
        public void Save()
        {
            XmlFileManager<SystemParamSetting>.TrySaveXml(PathOfXml, LstEqpSysParam);
        }
        public void Load()
        {
            if (XmlFileManager<SystemParamSetting>.TryLoadData(PathOfXml, out LstEqpSysParam) == false)
            {
                LstEqpSysParam = new SystemParamSetting();
            }
        }
    }
}

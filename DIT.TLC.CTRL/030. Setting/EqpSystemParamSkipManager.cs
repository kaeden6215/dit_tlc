﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class EqpSystemParamSkipManager
    {
        public static string PathOfXml = Path.Combine(GG.StartupPath, "Setting", "EqpSystemParamSkipManager.Xml");

        public SystemParamSkip Params;
        public EqpSystemParamSkipManager()
        {
            Params = new SystemParamSkip();
        }
        public void Save()
        {
            XmlFileManager<SystemParamSkip>.TrySaveXml(PathOfXml, Params);
        }
        public void Load()
        {
            if (XmlFileManager<SystemParamSkip>.TryLoadData(PathOfXml, out Params) == false)
            {
                Params = new SystemParamSkip();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DIT.TLC.CTRL
{
    public class UserInfoManager
    {
        public static string PathOfXml = Path.Combine(GG.StartupPath, "Setting", "UserInfo.Xml");

        public List<UserInfo> LstUserInfos = new List<UserInfo>();
        public UserInfo GetUserInfo(string userID, string password)
        {
            return LstUserInfos.FirstOrDefault(f => f.UserID == userID && f.Password == password);
        }

        public UserInfo Login(string userid, string password)
        {
            return LstUserInfos.FirstOrDefault(f => f.UserID == userid && f.Password == password);
        }


        public void Save()
        {
            XmlFileManager<List<UserInfo>>.TrySaveXml(PathOfXml, LstUserInfos);
        }
        public void Load()
        {
            XmlFileManager<List<UserInfo>>.TryLoadData(PathOfXml, out LstUserInfos);
            if (LstUserInfos == null)
                LstUserInfos = new List<UserInfo>();
        }
    }
}

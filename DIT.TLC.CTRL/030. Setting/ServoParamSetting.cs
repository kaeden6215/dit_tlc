﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmUmacParam
    {
        AxisNo,
        PositionRate,
        SpeedRate,
        MoveTimeOut,
        InPosition,
        DefauleVelocity,
        DefaultAcceleration,
        Home1Velocity,
        Home2Velocity,
        HomeAcceleration,
        JogSpeed,
        StepSpeed,
        PtpSpeed,
        SWLimitLow,
        SWLimitHigh,
        AccelationTime
    }    

    [Serializable]
    public class ServoParam
    {
        public int ParamNo { get; set; }
        public string ParamValue { get; set; }
    }

    [Serializable]
    public class ServoParamSetting
    {
        public string XmlPath { get; set; }
        public List<ServoParam> LstServoParam { get; set; }

        public ServoParamSetting()
        {
            LstServoParam = new List<ServoParam>();
        }

        public static ServoParamSetting Load(string path)
        {
            ServoParamSetting setting = new ServoParamSetting();
            XmlFileManager<ServoParamSetting>.TryLoadData(path, out setting);

            if (setting != null)
                setting.XmlPath = path;
            return setting;
        }
        public static bool Save(string path, ServoParamSetting setting)
        {
            return XmlFileManager<ServoParamSetting>.TrySaveXml(path, setting);
        }

        public void SetParamVlaue(int paramNo, string value)
        {
            LstServoParam[paramNo].ParamValue = value;
        }
        public string GetParamStringVlaue(int paramNo)
        {
            return LstServoParam[paramNo].ParamValue;
        }
        public int GetParamIntVlaue(int paramNo)
        {
            return int.Parse(LstServoParam[paramNo].ParamValue);
        }
        public double GetParamDoubleVlaue(int paramNo)
        {
            return double.Parse(LstServoParam[paramNo].ParamValue);
        }

        public void DefaultCreate(ServoMotorControl cstRotationAxis)
        {
            if (cstRotationAxis.MotorType == EM_MOTOR_TYPE.Ajin)
            {
                foreach (var vv in Enum.GetValues(typeof(EmAjinParam)))
                    LstServoParam.Add(new ServoParam() { ParamNo = (int)vv, ParamValue = string.Empty });

            }
            else if (cstRotationAxis.MotorType == EM_MOTOR_TYPE.Umac)
            {
                foreach (var vv in Enum.GetValues(typeof(EmUmacParam)))
                    LstServoParam.Add(new ServoParam() { ParamNo = (int)vv, ParamValue = string.Empty });
            }
            else if (cstRotationAxis.MotorType == EM_MOTOR_TYPE.EZi)
            {
                foreach (var vv in Enum.GetValues(typeof(EmEziParam)))
                    LstServoParam.Add(new ServoParam() { ParamNo = (int)vv, ParamValue = string.Empty });
            }
        }

        public void Save()
        {
            ServoParamSetting.Save(XmlPath, this);
        }

        public bool ToEziMotorSetting(out EziMotorSetting ret)
        {
            ret = new EziMotorSetting();

            try
            {
                ret.AxisNo                = GetParamIntVlaue((int)EmEziParam.AxisNo               );
                ret.PulsePerRevolution    = GetParamIntVlaue((int)EmEziParam.PulsePerRevolution   );
                ret.AxisMaxSpeed          = GetParamIntVlaue((int)EmEziParam.AxisMaxSpeed         );
                ret.AxisStartSpeed        = GetParamIntVlaue((int)EmEziParam.AxisStartSpeed       );
                ret.AxisAccTime           = GetParamIntVlaue((int)EmEziParam.AxisAccTime          );
                ret.AxisDecTime           = GetParamIntVlaue((int)EmEziParam.AxisDecTime          );
                ret.SpeedOverride         = GetParamIntVlaue((int)EmEziParam.SpeedOverride        );
                ret.JogSpeed              = GetParamIntVlaue((int)EmEziParam.JogSpeed             );
                ret.JogStartSpeed         = GetParamIntVlaue((int)EmEziParam.JogStartSpeed        );
                ret.JogAccDecTime         = GetParamIntVlaue((int)EmEziParam.JogAccDecTime        );
                ret.SWLimitPlusValue      = GetParamIntVlaue((int)EmEziParam.SWLimitPlusValue     );
                ret.SWLimitMinusValue     = GetParamIntVlaue((int)EmEziParam.SWLimitMinusValue    );
                ret.SWLimitStopMethod     = GetParamIntVlaue((int)EmEziParam.SWLimitStopMethod    );
                ret.HWLimitStopMethod     = GetParamIntVlaue((int)EmEziParam.HWLimitStopMethod    );
                ret.LimitSensorLogic      = GetParamIntVlaue((int)EmEziParam.LimitSensorLogic     );
                ret.OrgSpeed              = GetParamIntVlaue((int)EmEziParam.OrgSpeed             );
                ret.OrgSearchSpeed        = GetParamIntVlaue((int)EmEziParam.OrgSearchSpeed       );
                ret.OrgAccDecTime         = GetParamIntVlaue((int)EmEziParam.OrgAccDecTime        );
                ret.OrgMethod             = GetParamIntVlaue((int)EmEziParam.OrgMethod            );
                ret.OrgDir                = GetParamIntVlaue((int)EmEziParam.OrgDir               );
                ret.OrgOffSet             = GetParamIntVlaue((int)EmEziParam.OrgOffset            );
                ret.OrgPositionSet        = GetParamIntVlaue((int)EmEziParam.OrgPositionSet       );
                ret.OrgSensorLogic        = GetParamIntVlaue((int)EmEziParam.OrgSensorLogic       );
                ret.PositionLoopGain      = GetParamIntVlaue((int)EmEziParam.PositionLoopGain     );
                ret.InposValue            = GetParamIntVlaue((int)EmEziParam.InposValue           );
                ret.PosTrackingLimit      = GetParamIntVlaue((int)EmEziParam.PosTrackingLimit     );
                ret.MotionDir             = GetParamIntVlaue((int)EmEziParam.MotionDir            );
                ret.LimitSensorDir        = GetParamIntVlaue((int)EmEziParam.LimitSensorDir       );
                ret.OrgTorqueRatio        = GetParamIntVlaue((int)EmEziParam.OrgTorqueRatio       );
                ret.PosErrorOverflowLimit = GetParamIntVlaue((int)EmEziParam.PosErrorOverflowLimit);
                ret.BrakeDelayTime        = GetParamIntVlaue((int)EmEziParam.BrakeDelayTime       );
                ret.RunCurrent            = GetParamIntVlaue((int)EmEziParam.RunCurrent           );
                ret.BoostCurrent          = GetParamIntVlaue((int)EmEziParam.BoostCurrent         );
                ret.StopCurrent           = GetParamIntVlaue((int)EmEziParam.StopCurrent);
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
        public bool ToAjinMotorSetting(out AjinMotorSetting ret)
        {
            ret = new AjinMotorSetting();

            try
            {
                ret.AxisNo = GetParamIntVlaue((int)EmAjinParam.AxisNo);
                ret.PulseOutMethod = (AXT_MOTION_PULSE_OUTPUT)GetParamIntVlaue((int)EmAjinParam.PulseOutput);
                ret.EncoderMethod = (AXT_MOTION_EXTERNAL_COUNTER_INPUT)GetParamIntVlaue((int)EmAjinParam.EncInput);
                ret.UseInposition = (AXT_MOTION_LEVEL_MODE)GetParamIntVlaue((int)EmAjinParam.Inposition);
                ret.AlarmStopSignal = (AXT_MOTION_LEVEL_MODE)GetParamIntVlaue((int)EmAjinParam.Alarm);
                ret.NegLimitType = (AXT_MOTION_LEVEL_MODE)GetParamIntVlaue((int)EmAjinParam.MinusEndLimit);
                ret.PosLimitType = (AXT_MOTION_LEVEL_MODE)GetParamIntVlaue((int)EmAjinParam.PlusEndLimit);
                ret.MinSpeed = GetParamDoubleVlaue((int)EmAjinParam.MinVelocitu);
                ret.MaxSpeed = GetParamDoubleVlaue((int)EmAjinParam.MaxVelocitu);
                ret.HomeSignalSensor = (AXT_MOTION_HOME_DETECT)GetParamIntVlaue((int)EmAjinParam.HomeSignal);
                ret.IsHomeSignalNormalOpen = GetParamIntVlaue((int)EmAjinParam.HomeLevel) == 1 ? true : false;
                ret.HomeDirection = (AXT_MOTION_MOVE_DIR)GetParamIntVlaue((int)EmAjinParam.HomeDirection);
                ret.IsZPhaseNormalOpen = GetParamIntVlaue((int)EmAjinParam.ZPhaseLevel) == 1 ? true : false;
                ret.UseFindZPhaseType = (uint)GetParamIntVlaue((int)EmAjinParam.HomeZPhaseType);
                ret.StopMode           = (AXT_MOTION_STOPMODE)GetParamIntVlaue((int)EmAjinParam.StopMode);
                ret.StopModeLevel      = (AXT_MOTION_LEVEL_MODE)GetParamIntVlaue((int)EmAjinParam.StopLevel);
                ret.HomeVel1           = GetParamDoubleVlaue((int)EmAjinParam.HomeVel1);
                ret.HomeVel2           = GetParamDoubleVlaue((int)EmAjinParam.HomeVel2);
                ret.HomeVel3           = GetParamDoubleVlaue((int)EmAjinParam.HomeVel3);
                ret.HomeVelLast        = GetParamDoubleVlaue((int)EmAjinParam.HomeVelLast);
                ret.HomeAcc1           = GetParamDoubleVlaue((int)EmAjinParam.HomeAccelation1);
                ret.HomeAcc2           = GetParamDoubleVlaue((int)EmAjinParam.HomeAccelation2);
                ret.HomeClearTime      = GetParamDoubleVlaue((int)EmAjinParam.HomeClearTime);
                ret.HomeOffset         = GetParamDoubleVlaue((int)EmAjinParam.HomeOffset);
                ret.SoftLimitNeg       = GetParamDoubleVlaue((int)EmAjinParam.SWMinusLimit);
                ret.SoftLimitPos       = GetParamDoubleVlaue((int)EmAjinParam.SWPlusLimit);
                ret.Pulse              = GetParamIntVlaue((int)EmAjinParam.Pulse);
                ret.Unit               = GetParamDoubleVlaue((int)EmAjinParam.Unit);
                ret.InitPos            = GetParamDoubleVlaue((int)EmAjinParam.InitPosition);
                ret.InitSpeed          = GetParamDoubleVlaue((int)EmAjinParam.InitVelocity);
                ret.InitAcc            = GetParamDoubleVlaue((int)EmAjinParam.InitAccelation);
                ret.InitDec            = GetParamDoubleVlaue((int)EmAjinParam.InitDecelation);
                ret.AbsRelMode         = (AXT_MOTION_ABSREL)GetParamIntVlaue((int)EmAjinParam.AbsRelMode);
                ret.ProfileMode        = (AXT_MOTION_PROFILE_MODE)GetParamIntVlaue((int)EmAjinParam.VelProfileMode);
                ret.IsUseSoftLimit     = (AXT_USE)GetParamIntVlaue((int)EmAjinParam.ServoOnLevel);
                ret.SoftLimitStopMode  = (AXT_MOTION_STOPMODE)GetParamIntVlaue((int)EmAjinParam.AlarmResetLevel);
                ret.SoftLimitSource    = (AXT_MOTION_SELECTION)GetParamIntVlaue((int)EmAjinParam.EncoderType);
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
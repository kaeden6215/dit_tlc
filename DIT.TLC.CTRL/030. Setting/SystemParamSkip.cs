﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class SystemParamSkip
    {
        public bool IsSimulationMode { get; set; }
        public bool IsDistributionMode { get; set; }

        public bool IsProcessASkipMode { get; set; }
        public bool IsProcessBSkipMode { get; set; }
        public bool IsBreakingASkipMode { get; set; }
        public bool IsBreakingBSkipMode { get; set; }
        public bool IsLaserSkipMode { get; set; }
        public bool IsBreakingMotionSkipMode { get; set; }
        public bool IsInspectionSkipUseMode { get; set; }
        public bool IsBreakingAlignSkipMode { get; set; }
        public bool IsMcrSkipMode { get; set; }
        public bool IsTiltMeasureSkipMode { get; set; }
        public bool IsBcrSkipMode { get; set; }
        public bool IsBufferPickerSkipMode { get; set; }
        public bool IsLoaderInputSkipAMode { get; set; }
        public bool IsLoaderInputSkipBMode { get; set; }
        public bool IsUnloaderInputSkipAMode { get; set; }
        public bool IsUnloaderInputSkipBMode { get; set; }
        public bool IsLaserMeasureSkipMode { get; set; }
        public bool IsMutingSkipMode { get; set; }
        public bool IsGrabSwitchSkipMode { get; set; }
        public bool IsTeachSkipMode { get; set; }
        public bool IsDoorSkipMode { get; set; }
        public bool IsMcDownSkipMode { get; set; }
        public bool IsInterlockUse { get; set; }
        public SystemParamSkip()
        {
            IsSimulationMode = false;
            IsDistributionMode = false;
            IsProcessASkipMode = false;
            IsProcessBSkipMode = false;
            IsBreakingASkipMode = false;
            IsBreakingBSkipMode = false;
            IsLaserSkipMode = false;
            IsBreakingMotionSkipMode = false;
            IsInspectionSkipUseMode = false;
            IsBreakingAlignSkipMode = false;
            IsMcrSkipMode = false;
            IsTiltMeasureSkipMode = false;
            IsBcrSkipMode = false;
            IsBufferPickerSkipMode = false;
            IsLoaderInputSkipAMode = false;
            IsLoaderInputSkipBMode = false;
            IsUnloaderInputSkipAMode = false;
            IsUnloaderInputSkipBMode = false;
            IsLaserMeasureSkipMode = false;
            IsMutingSkipMode = false;
            IsGrabSwitchSkipMode = false;
            IsTeachSkipMode = false;
            IsDoorSkipMode = false;
            IsMcDownSkipMode = false;
        }
    }
}

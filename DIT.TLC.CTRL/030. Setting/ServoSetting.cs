﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using Dit.Framework.Xml;

namespace DIT.TLC.CTRL
{
    public class ServoPosiInfo : ICloneable
    {
        public int No;
        public string Name;
        public float Position;
        public float Speed;
        public float Accel;
        public bool IsSetting;

        public ServoMotorControl Servo = null;

        public ServoPosiInfo()
        {
            No = 0;
            Position = 0;
            Speed = 0;
            Accel = 0;
        }

        public object Clone()
        {
            return new ServoPosiInfo()
            {
                No = this.No,
                Name = this.Name,
                Position = this.Position,
                Speed = this.Speed,
                Accel = this.Accel,
                IsSetting = this.IsSetting
            };
        }
    }

    public class ServoSetting
    {
        public string XmlPath { get; set; }
        /**
         * [관리항목]
         * 원점 이동속도
         * 조그 속도
         * 정의된 위치/속도 * 7개
        */

        public ServoPosiInfo[] LstServoPosiInfo = new ServoPosiInfo[0];
        public ServoSetting()
        {
        }
        public bool Load()
        {
            if (File.Exists(XmlPath) == false) return false;

            foreach (string line in File.ReadAllLines(XmlPath, Encoding.Unicode))
            {
                string[] items = line.Split(',');

                if (items.Length != 5) continue;

                ServoPosiInfo info = new ServoPosiInfo()
                {
                    No = int.Parse(items[0]),
                    Name = items[1],
                    Position = float.Parse(items[2]),
                    Speed = float.Parse(items[3]),
                    Accel = float.Parse(items[4]),
                };

                if (LstServoPosiInfo[info.No].No == info.No && LstServoPosiInfo[info.No].Name == info.Name && LstServoPosiInfo[info.No].IsSetting == false)
                {
                    LstServoPosiInfo[info.No].Position = info.Position;
                    LstServoPosiInfo[info.No].Speed = info.Speed;
                    LstServoPosiInfo[info.No].Accel = info.Accel;
                    LstServoPosiInfo[info.No].IsSetting = true;
                }
                else
                    return false;
            }

            return true;
        }
        public bool Save()
        {
            string str = string.Empty;
            foreach (var info in LstServoPosiInfo)
            {
                str += string.Format("{0},{1},{2},{3},{4}", info.No, info.Name, info.Position, info.Speed, info.Accel);
                str += Environment.NewLine;
            }
            File.WriteAllText(XmlPath, str, Encoding.Unicode);
            return true;
        }
    }
}
﻿using Dit.Framework.Comm;
using Dit.Framework.PLC;
using DIT.TLC.CTRL._050._Struct._520._Unit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{

    public class Equipment
    {
        public LoaderModule LD = new LoaderModule();
        public ProceeModule PROC = new ProceeModule();
        public UnloaderMoudle UD = new UnloaderMoudle();

        public PMacProxy PMac                                                           /**/ = new PMacProxy();
        public BeamPositionerProxy BeamProxy                                            /**/ = new BeamPositionerProxy("COM4");  // 4
        public PowerMeterProxy PowerProxy                                               /**/ = new PowerMeterProxy("COM5");  // 5
        public McrPcProxy Mcr1stProxy                                                   /**/ = new McrPcProxy("COM6", GG.StartupPath + @"\setting\"); // A : 6,   B : 7
        public McrPcProxy Mcr2ndProxy                                                   /**/ = new McrPcProxy("COM7", GG.StartupPath + @"\setting\"); // A : 6,   B : 7
        public FixedBarcodeProxy LdAFixedBarcProxy                                      /**/ = new FixedBarcodeProxy("COM12"); // LD A : 12, LD B : 13, ULD A : 16, ULD B : 17



        public FixedBarcodeProxy LdBFixedBarcProxy                                      /**/ = new FixedBarcodeProxy("COM13"); // LD A : 12, LD B : 13, ULD A : 16, ULD B : 17
        public HandyBarcodeProxy LdHandyBarcProxy                                       /**/ = new HandyBarcodeProxy("COM14"); // LD : 14, ULD : 18
        public TiltSensorProxy LdTiltProxy                                              /**/ = new TiltSensorProxy("COM15"); // LD : 15, ULD : 19
        public FixedBarcodeProxy UldAFixedBarcProxy                                     /**/ = new FixedBarcodeProxy("COM16"); // LD A : 12, LD B : 13, ULD A : 16, ULD B : 17
        public FixedBarcodeProxy UldBFixedBarcProxy                                     /**/ = new FixedBarcodeProxy("COM17"); // LD A : 12, LD B : 13, ULD A : 16, ULD B : 17
        public HandyBarcodeProxy UldHandyBarcProxy                                      /**/ = new HandyBarcodeProxy("COM18"); // LD : 14, ULD : 18
        public TiltSensorProxy UldTiltProxy                                             /**/ = new TiltSensorProxy("COM19"); // LD : 15, ULD : 19

        public LightControlProxy LightProxy                                             /**/ = new LightControlProxy(); // 몇번인지..

        public LaserProxy LaserProxy                                                    /**/ = new LaserProxy("192.168.0.2", 23);  // 아이피 포트 변경필요
        public SEMProxy SemGpsProxy                                                     /**/ = new SEMProxy("126.100.100.120", 502);
        public SEMProxy SemUpsProxy                                                     /**/ = new SEMProxy("126.100.100.121", 502);

        public AlignPcProxy AlignPc                                                     /**/ = new AlignPcProxy("10.1.20.156", 8119);
        public InspPcProxy InspPc                                                       /**/ = new InspPcProxy("10.1.20.156", 9119);
        public SafetyManger SafetyMgr = new SafetyManger();
        public EquipSettingMgr SettingMgr = new EquipSettingMgr();
        public AlarmManager AlarmMgr = AlarmManager.Instance;

        public WorkInfo WorkInfos { get; set; }
        public EmEquipRunMode RunMode { get; set; }

        public bool IsHeavyAlarm { get; set; }
        public bool IsLightAlarm { get; set; }
        public bool IsUnusedAlarm { get; set; }
        public bool IsUseInterLockOff { get; set; }
        public bool IsUseDoorInterLockOff { get; set; }
        public bool IsInnerWorkOnClose { get; set; }
        public bool IsDoorRelease
        {
            get { return LD.IsDoorRelease || PROC.IsDoorRelease || UD.IsDoorRelease; }
        }
        public bool IsDoorOpen
        {
            get { return LD.IsDoorOpen || PROC.IsDoorOpen || UD.IsDoorOpen; }
        }
        public bool IsInnerWorkOn
        {
            get
            {
                return IsDoorRelease;
            }
        }
        public void DoorOpenClose(bool open)
        {
            LD.DoorOpenClose(this, open);
            PROC.DoorOpenClose(this, open);
            UD.DoorOpenClose(this, open);
        }
        public bool IsPause { get; set; }
        public bool IsImmediatStop { get; set; }

        public Thread _worker = null;
        public bool _running = false;

        public EqpRecipe CurrentRecipe { get; set; }
        public EqpRecipeManager EqpRecipeMgr { get; set; }
        public BreakRecipeManager BreakRecipeMgr { get; set; }
        public ProcessRecipeManager ProcessRecipeMgr { get; set; }
        public UserInfoManager UserInfoMgr { get; set; }

        // 파라미터
        public EqpSystemParamSettingManager EqpSysParamMgr { get; set; }
        public EqpSystemParamSkipManager EqpSysMgr { get; set; }



        private void InitalizeAlarmList()
        {
            #region  모터_알람
           // PMac.HEAVY_ERROR                                                        /**/= EM_AL_LST.AL_0530_PMAC_HEAVY_ERROR;

            int iPos = 0;
            foreach (ServoMotorControl svr in new List<ServoMotorControl> {  LD.CstLoader_A.CstRotationAxis, LD.CstLoader_A.CstUpDownAxis,
                        LD.CstLoader_B.CstRotationAxis,         LD.CstLoader_B.CstUpDownAxis,
                        LD.Loader.X1Axis,                       LD.Loader.X2Axis,                           LD.Loader.Y1Axis,                       LD.Loader.Y2Axis,
                        LD.LoaderTransfer_A.X1Axis,             LD.LoaderTransfer_A.X2Axis,                 LD.LoaderTransfer_A.YAxis,
                        LD.LoaderTransfer_A.SubT1Axis,          LD.LoaderTransfer_A.SubT2Axis,              LD.LoaderTransfer_A.SubY1Axis,          LD.LoaderTransfer_A.SubY2Axis,
                        LD.LoaderTransfer_B.X1Axis,             LD.LoaderTransfer_B.X2Axis,                 LD.LoaderTransfer_B.YAxis,
                        LD.LoaderTransfer_B.SubY1Axis,          LD.LoaderTransfer_B.SubY2Axis,              LD.LoaderTransfer_B.SubT1Axis,          LD.LoaderTransfer_B.SubT2Axis,
                        LD.PreAlign.XAxis,                      PROC.FineAlign.XAxis,                       PROC.LaserHead.XAxis,                   PROC.LaserHead.ZAxis,
                        PROC.IRCutStage_A.YAxis,                PROC.IRCutStage_B.YAxis,
                        PROC.AfterIRCutTransfer_A.YAxis,        PROC.AfterIRCutTransfer_B.YAxis,
                        PROC.BreakingHead.X1Axis,               PROC.BreakingHead.X2Axis,                   PROC.BreakingHead.Z1Axis,               PROC.BreakingHead.Z2Axis,
                        PROC.BreakStage_A.SubT1Axis,            PROC.BreakStage_A.SubT2Axis,                PROC.BreakStage_A.SubX1Axis,            PROC.BreakStage_A.SubX2Axis,        PROC.BreakStage_A.SubY1Axis,         PROC.BreakStage_A.SubY2Axis,           PROC.BreakStage_A.YAxis,
                        PROC.BreakStage_B.SubT1Axis,            PROC.BreakStage_B.SubT2Axis,                PROC.BreakStage_B.SubX1Axis,            PROC.BreakStage_B.SubX2Axis,        PROC.BreakStage_B.SubY1Axis,         PROC.BreakStage_B.SubY2Axis,           PROC.BreakStage_B.YAxis,
                        PROC.BreakAlign.XAxis,                  PROC.BreakAlign.ZAxis,                    
                        UD.BeforeInspUnloaderTransfer_A.Y1Axis, UD.BeforeInspUnloaderTransfer_B.Y1Axis,     UD.InspectionStage_A.YAxis,             UD.InspectionStage_B.YAxis,
                        UD.InspCamera.XAxis,                    UD.InspCamera.ZAxis,
                        UD.AfterInspUnloaderTransfer_A.T1Axis,  UD.AfterInspUnloaderTransfer_A.T2Axis,      UD.AfterInspUnloaderTransfer_B.T1Axis,  UD.AfterInspUnloaderTransfer_B.T2Axis,
                        UD.AfterInspUnloaderTransfer_A.Y2Axis,  UD.AfterInspUnloaderTransfer_B.Y2Axis,      UD.Unloader.X1Axis,                     UD.Unloader.X2Axis,
                        UD.Unloader.Y1Axis, UD.Unloader.Y2Axis, UD.CstUnloader_A.CstRotationAxis,           UD.CstUnloader_B.CstRotationAxis })
            {
                svr.PLUS_LIMIT_ERROR                /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0900_CST_LOADER_A_ROTATION_PLUS_LIMIT_ERROR                 + (12 * iPos));
                svr.MINUS_LIMIT_ERROR               /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0901_CST_LOADER_A_ROTATION_MINUS_LIMIT_ERROR                + (12 * iPos));
                svr.MOTOR_SERVO_ON_ERROR            /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0902_CST_LOADER_A_ROTATION_MOTOR_SERVO_ON_ERROR             + (12 * iPos));
                svr.CRITICAL_POSITION_ERROR         /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0903_CST_LOADER_A_ROTATION_CRITICAL_POSITION_ERROR          + (12 * iPos));
                svr.DRIVE_FAULT_ERROR               /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0904_CST_LOADER_A_ROTATION_DRIVE_FAULT_ERROR                + (12 * iPos));
                svr.OVER_CURRENT_ERROR              /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0905_CST_LOADER_A_ROTATION_OVER_CURRENT_ERROR               + (12 * iPos));
                svr.ECALL_ERROR                     /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0906_CST_LOADER_A_ROTATION_ECALL_ERROR                      + (12 * iPos));
                svr.MOVE_OVERTIME_ERROR             /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0907_CST_LOADER_A_ROTATION_MOVE_OVERTIME_ERROR              + (12 * iPos));
                svr.BUFFER_RESET_ERROR              /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0908_CST_LOADER_A_ROTATION_BUFFER_RESET_ERROR               + (12 * iPos));
                svr.MOTOR_IS_NOT_MOVING_ERROR       /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0909_CST_LOADER_A_ROTATION_MOTOR_IS_NOT_MOVING_ERROR        + (12 * iPos));
                svr.MESSAGE_ACK_ERROR               /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0910_CST_LOADER_A_ROTATION_MESSAGE_ACK_ERROR                + (12 * iPos));
                svr.MESSAGE_MOTION_COMPLETE_ERROR   /**/= (EM_AL_LST)((int)EM_AL_LST.AL_0911_CST_LOADER_A_ROTATION_MESSAGE_MOTION_COMPLETE_ERROR    + (12 * iPos));
            }
            #endregion


            #region  바큠_알람
           LD.Loader.Vaccum1.ON_TIME_OUT_ERROR                                          /**/= EM_AL_LST.AL_0260_LD_A_VACCUM_ON_TIME_OUT_ERROR;
           LD.Loader.Vaccum1.OFF_TIME_OUT_ERROR                                         /**/= EM_AL_LST.AL_0261_LD_A_VACCUM_OFF_TIME_OUT_ERROR;
           LD.Loader.Vaccum2.ON_TIME_OUT_ERROR                                          /**/= EM_AL_LST.AL_0262_LD_B_VACCUM_ON_TIME_OUT_ERROR;
           LD.Loader.Vaccum2.OFF_TIME_OUT_ERROR                                         /**/= EM_AL_LST.AL_0263_LD_B_VACCUM_OFF_TIME_OUT_ERROR;

            LD.LoaderTransfer_A.Vaccum1.ON_TIME_OUT_ERROR                                /**/= EM_AL_LST.AL_0270_LD_TR_A_X1_VACCUM_ON_TIME_OUT_ERROR;
            LD.LoaderTransfer_A.Vaccum1.OFF_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0271_LD_TR_A_X1_VACCUM_OFF_TIME_OUT_ERROR;
            LD.LoaderTransfer_A.Vaccum2.ON_TIME_OUT_ERROR                                /**/= EM_AL_LST.AL_0272_LD_TR_A_X2_VACCUM_ON_TIME_OUT_ERROR;
            LD.LoaderTransfer_A.Vaccum2.OFF_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0273_LD_TR_A_X2_VACCUM_OFF_TIME_OUT_ERROR;
            LD.LoaderTransfer_B.Vaccum1.ON_TIME_OUT_ERROR                                /**/= EM_AL_LST.AL_0274_LD_TR_B_X1_VACCUM_ON_TIME_OUT_ERROR;
            LD.LoaderTransfer_B.Vaccum1.OFF_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0275_LD_TR_B_X1_VACCUM_OFF_TIME_OUT_ERROR;
            LD.LoaderTransfer_B.Vaccum2.ON_TIME_OUT_ERROR                                /**/= EM_AL_LST.AL_0276_LD_TR_B_X2_VACCUM_ON_TIME_OUT_ERROR;
            LD.LoaderTransfer_B.Vaccum2.OFF_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0277_LD_TR_B_X2_VACCUM_OFF_TIME_OUT_ERROR;

           PROC.IRCutStage_A.Vacuum1Ch1.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0467_PROC_STAGE_A_1CH1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_A.Vacuum1Ch2.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0468_PROC_STAGE_A_1CH2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_A.Vacuum2Ch1.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0475_PROC_STAGE_A_2CH1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_A.Vacuum2Ch2.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0476_PROC_STAGE_A_2CH2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_A.Vacuum1Ch1.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0471_PROC_STAGE_A_1CH1_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.IRCutStage_A.Vacuum1Ch2.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0472_PROC_STAGE_A_1CH2_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.IRCutStage_A.Vacuum2Ch1.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0479_PROC_STAGE_A_2CH1_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.IRCutStage_A.Vacuum2Ch2.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0480_PROC_STAGE_A_2CH2_VACCUM_OFF_TIME_OUT_ERROR;

           PROC.IRCutStage_B.Vacuum1Ch1.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0469_PROC_STAGE_B_1CH1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_B.Vacuum1Ch2.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0470_PROC_STAGE_B_1CH2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_B.Vacuum2Ch1.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0477_PROC_STAGE_B_2CH1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_B.Vacuum2Ch2.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0478_PROC_STAGE_B_2CH2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.IRCutStage_B.Vacuum1Ch1.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0473_PROC_STAGE_B_1CH1_VACCUM_OFF_TIME_OUT_ERROR; 
           PROC.IRCutStage_B.Vacuum1Ch2.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0474_PROC_STAGE_B_1CH2_VACCUM_OFF_TIME_OUT_ERROR; 
           PROC.IRCutStage_B.Vacuum2Ch1.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0481_PROC_STAGE_B_2CH1_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.IRCutStage_B.Vacuum2Ch2.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0482_PROC_STAGE_B_2CH2_VACCUM_OFF_TIME_OUT_ERROR;


           PROC.AfterIRCutTransfer_A.Vaccum1.ON_TIME_OUT_ERROR                          /**/= EM_AL_LST.AL_0450_PROC_IRCUT_TR_A1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.AfterIRCutTransfer_A.Vaccum2.ON_TIME_OUT_ERROR                          /**/= EM_AL_LST.AL_0451_PROC_IRCUT_TR_A2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.AfterIRCutTransfer_B.Vaccum1.ON_TIME_OUT_ERROR                          /**/= EM_AL_LST.AL_0452_PROC_IRCUT_TR_B1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.AfterIRCutTransfer_B.Vaccum2.ON_TIME_OUT_ERROR                          /**/= EM_AL_LST.AL_0453_PROC_IRCUT_TR_B2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.AfterIRCutTransfer_A.Vaccum1.OFF_TIME_OUT_ERROR                         /**/= EM_AL_LST.AL_0454_PROC_IRCUT_TR_A1_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.AfterIRCutTransfer_A.Vaccum2.OFF_TIME_OUT_ERROR                         /**/= EM_AL_LST.AL_0455_PROC_IRCUT_TR_A2_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.AfterIRCutTransfer_B.Vaccum1.OFF_TIME_OUT_ERROR                         /**/= EM_AL_LST.AL_0456_PROC_IRCUT_TR_B1_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.AfterIRCutTransfer_B.Vaccum2.OFF_TIME_OUT_ERROR                         /**/= EM_AL_LST.AL_0457_PROC_IRCUT_TR_B2_VACCUM_OFF_TIME_OUT_ERROR;


           PROC.BreakStage_A.Vaccum1.ON_TIME_OUT_ERROR                                  /**/= EM_AL_LST.AL_0504_PROC_BREAK_STAGE_A1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.BreakStage_A.Vaccum1.OFF_TIME_OUT_ERROR                                 /**/= EM_AL_LST.AL_0505_PROC_BREAK_STAGE_A2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.BreakStage_A.Vaccum2.ON_TIME_OUT_ERROR                                  /**/= EM_AL_LST.AL_0506_PROC_BREAK_STAGE_B1_VACCUM_ON_TIME_OUT_ERROR;
           PROC.BreakStage_A.Vaccum2.OFF_TIME_OUT_ERROR                                 /**/= EM_AL_LST.AL_0507_PROC_BREAK_STAGE_B2_VACCUM_ON_TIME_OUT_ERROR;
           PROC.BreakStage_B.Vaccum1.ON_TIME_OUT_ERROR                                  /**/= EM_AL_LST.AL_0508_PROC_BREAK_STAGE_A1_VACCUM_OFF_TIME_OUT_ERROR;                               
           PROC.BreakStage_B.Vaccum1.OFF_TIME_OUT_ERROR                                 /**/= EM_AL_LST.AL_0509_PROC_BREAK_STAGE_A2_VACCUM_OFF_TIME_OUT_ERROR;                               
           PROC.BreakStage_B.Vaccum2.ON_TIME_OUT_ERROR                                  /**/= EM_AL_LST.AL_0510_PROC_BREAK_STAGE_B1_VACCUM_OFF_TIME_OUT_ERROR;
           PROC.BreakStage_B.Vaccum2.OFF_TIME_OUT_ERROR                                 /**/= EM_AL_LST.AL_0511_PROC_BREAK_STAGE_B2_VACCUM_OFF_TIME_OUT_ERROR;                               
           
          

           UD.BeforeInspUnloaderTransfer_A.Vaccum1.ON_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0632_ULD_BEFORE_TR_A1_VACCUM_ON_TIME_OUT_ERROR;
           UD.BeforeInspUnloaderTransfer_A.Vaccum2.ON_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0633_ULD_BEFORE_TR_A2_VACCUM_ON_TIME_OUT_ERROR;
           UD.BeforeInspUnloaderTransfer_B.Vaccum1.ON_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0634_ULD_BEFORE_TR_B1_VACCUM_ON_TIME_OUT_ERROR;
           UD.BeforeInspUnloaderTransfer_B.Vaccum2.ON_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0635_ULD_BEFORE_TR_B2_VACCUM_ON_TIME_OUT_ERROR;
           UD.BeforeInspUnloaderTransfer_A.Vaccum1.OFF_TIME_OUT_ERROR                   /**/= EM_AL_LST.AL_0636_ULD_BEFORE_TR_A1_VACCUM_OFF_TIME_OUT_ERROR;
           UD.BeforeInspUnloaderTransfer_A.Vaccum2.OFF_TIME_OUT_ERROR                   /**/= EM_AL_LST.AL_0637_ULD_BEFORE_TR_A2_VACCUM_OFF_TIME_OUT_ERROR;
           UD.BeforeInspUnloaderTransfer_B.Vaccum1.OFF_TIME_OUT_ERROR                   /**/= EM_AL_LST.AL_0638_ULD_BEFORE_TR_B1_VACCUM_OFF_TIME_OUT_ERROR;
           UD.BeforeInspUnloaderTransfer_B.Vaccum2.OFF_TIME_OUT_ERROR                   /**/= EM_AL_LST.AL_0639_ULD_BEFORE_TR_B2_VACCUM_OFF_TIME_OUT_ERROR;
           
           UD.InspectionStage_A.Vaccum1.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0650_ULD_INSPSTAGE_A1_VACCUM_ON_TIME_OUT_ERROR;
           UD.InspectionStage_A.Vaccum2.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0651_ULD_INSPSTAGE_A2_VACCUM_ON_TIME_OUT_ERROR;
           UD.InspectionStage_B.Vaccum1.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0652_ULD_INSPSTAGE_B1_VACCUM_ON_TIME_OUT_ERROR;
           UD.InspectionStage_B.Vaccum2.ON_TIME_OUT_ERROR                               /**/= EM_AL_LST.AL_0653_ULD_INSPSTAGE_B2_VACCUM_ON_TIME_OUT_ERROR;
           UD.InspectionStage_A.Vaccum1.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0654_ULD_INSPSTAGE_A1_VACCUM_OFF_TIME_OUT_ERROR;
           UD.InspectionStage_A.Vaccum2.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0655_ULD_INSPSTAGE_A2_VACCUM_OFF_TIME_OUT_ERROR;
           UD.InspectionStage_B.Vaccum1.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0656_ULD_INSPSTAGE_B1_VACCUM_OFF_TIME_OUT_ERROR;
           UD.InspectionStage_B.Vaccum2.OFF_TIME_OUT_ERROR                              /**/= EM_AL_LST.AL_0657_ULD_INSPSTAGE_B2_VACCUM_OFF_TIME_OUT_ERROR;
           
           UD.AfterInspUnloaderTransfer_A.Vaccum1.ON_TIME_OUT_ERROR                     /**/= EM_AL_LST.AL_0676_ULD_AFTER_TR_A1_VACCUM_ON_TIME_OUT_ERROR;
           UD.AfterInspUnloaderTransfer_A.Vaccum2.ON_TIME_OUT_ERROR                     /**/= EM_AL_LST.AL_0677_ULD_AFTER_TR_A2_VACCUM_ON_TIME_OUT_ERROR;
           UD.AfterInspUnloaderTransfer_B.Vaccum1.ON_TIME_OUT_ERROR                     /**/= EM_AL_LST.AL_0678_ULD_AFTER_TR_B1_VACCUM_ON_TIME_OUT_ERROR;
           UD.AfterInspUnloaderTransfer_B.Vaccum2.ON_TIME_OUT_ERROR                     /**/= EM_AL_LST.AL_0679_ULD_AFTER_TR_B2_VACCUM_ON_TIME_OUT_ERROR;
           UD.AfterInspUnloaderTransfer_A.Vaccum1.OFF_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0680_ULD_AFTER_TR_A1_VACCUM_OFF_TIME_OUT_ERROR;
           UD.AfterInspUnloaderTransfer_A.Vaccum2.OFF_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0681_ULD_AFTER_TR_A2_VACCUM_OFF_TIME_OUT_ERROR;
           UD.AfterInspUnloaderTransfer_B.Vaccum1.OFF_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0682_ULD_AFTER_TR_B1_VACCUM_OFF_TIME_OUT_ERROR;
           UD.AfterInspUnloaderTransfer_B.Vaccum2.OFF_TIME_OUT_ERROR                    /**/= EM_AL_LST.AL_0683_ULD_AFTER_TR_B2_VACCUM_OFF_TIME_OUT_ERROR;
           
           UD.Unloader.Vaccum1.ON_TIME_OUT_ERROR                                        /**/= EM_AL_LST.AL_0694_ULD_A_VACCUM_ON_TIME_OUT_ERROR;
           UD.Unloader.Vaccum2.ON_TIME_OUT_ERROR                                        /**/= EM_AL_LST.AL_0695_ULD_B_VACCUM_ON_TIME_OUT_ERROR;
           UD.Unloader.Vaccum1.OFF_TIME_OUT_ERROR                                       /**/= EM_AL_LST.AL_0696_ULD_A_VACCUM_OFF_TIME_OUT_ERROR;
           UD.Unloader.Vaccum2.OFF_TIME_OUT_ERROR                                       /**/= EM_AL_LST.AL_0697_ULD_B_VACCUM_OFF_TIME_OUT_ERROR;

           UD.CellBuffer.Vaccum1.ON_TIME_OUT_ERROR                                      /**/= EM_AL_LST.AL_0740_ULD_BUFFER_VACCUM_ON_TIME_OUT_ERROR;
           UD.CellBuffer.Vaccum1.OFF_TIME_OUT_ERROR                                     /**/= EM_AL_LST.AL_0741_ULD_BUFFER_VACCUM_OFF_TIME_OUT_ERROR;

            #endregion


            #region  센터링_실린더_알람               
            LD.CstLoader_A.TiltCylinder.AlcdBackwardTimeOut[0]                                      /***/= EM_AL_LST.AL_0251_LD_CST_A_TILT_DOWN_TIME_OUT_ERROR;
            LD.CstLoader_B.TiltCylinder.AlcdBackwardTimeOut[1]                                      /***/= EM_AL_LST.AL_0252_LD_CST_B_TILT_DOWN_TIME_OUT_ERROR;
            LD.CstLoader_A.TiltCylinder.AlcdForwardTimeOut[0]                                       /***/= EM_AL_LST.AL_0249_LD_CST_A_TILT_UP_TIME_OUT_ERROR;
            LD.CstLoader_B.TiltCylinder.AlcdForwardTimeOut[1]                                       /***/= EM_AL_LST.AL_0250_LD_CST_B_TILT_UP_TIME_OUT_ERROR;

            LD.LoaderTransfer_A.UpDown1Cylinder.AlcdBackwardTimeOut[0]                              /***/= EM_AL_LST.AL_0278_LD_TR_A_X1_PIKER_UP_TIME_OUT_ERROR;
            LD.LoaderTransfer_A.UpDown2Cylinder.AlcdBackwardTimeOut[1]                              /***/= EM_AL_LST.AL_0280_LD_TR_A_X2_PIKER_UP_TIME_OUT_ERROR;    
            LD.LoaderTransfer_A.UpDown1Cylinder.AlcdForwardTimeOut[0]                               /***/= EM_AL_LST.AL_0279_LD_TR_A_X1_PIKER_DOWN_TIME_OUT_ERROR;
            LD.LoaderTransfer_A.UpDown2Cylinder.AlcdForwardTimeOut[1]                               /***/= EM_AL_LST.AL_0281_LD_TR_A_X2_PIKER_DOWN_TIME_OUT_ERROR;
            LD.LoaderTransfer_B.UpDown1Cylinder.AlcdBackwardTimeOut[0]                              /***/= EM_AL_LST.AL_0282_LD_TR_B_X1_PIKER_UP_TIME_OUT_ERROR;
            LD.LoaderTransfer_B.UpDown2Cylinder.AlcdBackwardTimeOut[1]                              /***/= EM_AL_LST.AL_0284_LD_TR_B_X2_PIKER_UP_TIME_OUT_ERROR; 
            LD.LoaderTransfer_B.UpDown1Cylinder.AlcdForwardTimeOut[0]                               /***/= EM_AL_LST.AL_0283_LD_TR_B_X1_PIKER_DOWN_TIME_OUT_ERROR;
            LD.LoaderTransfer_B.UpDown2Cylinder.AlcdForwardTimeOut[1]                               /***/= EM_AL_LST.AL_0285_LD_TR_B_X2_PIKER_DOWN_TIME_OUT_ERROR;

            PROC.AfterIRCutTransfer_A.UpDown1Cylinder.AlcdBackwardTimeOut[0]                        /***/= EM_AL_LST.AL_0442_PROC_IRCUT_TR_A1_PIKER_UP_TIME_OUT_ERROR;
            PROC.AfterIRCutTransfer_A.UpDown2Cylinder.AlcdBackwardTimeOut[1]                        /***/= EM_AL_LST.AL_0443_PROC_IRCUT_TR_A2_PIKER_UP_TIME_OUT_ERROR;
            PROC.AfterIRCutTransfer_A.UpDown1Cylinder.AlcdForwardTimeOut[0]                         /***/= EM_AL_LST.AL_0446_PROC_IRCUT_TR_A1_PIKER_DOWN_TIME_OUT_ERROR; 
            PROC.AfterIRCutTransfer_A.UpDown2Cylinder.AlcdForwardTimeOut[1]                         /***/= EM_AL_LST.AL_0447_PROC_IRCUT_TR_A2_PIKER_DOWN_TIME_OUT_ERROR; 
            PROC.AfterIRCutTransfer_B.UpDown1Cylinder.AlcdForwardTimeOut[0]                         /***/= EM_AL_LST.AL_0448_PROC_IRCUT_TR_B1_PIKER_DOWN_TIME_OUT_ERROR;
            PROC.AfterIRCutTransfer_B.UpDown2Cylinder.AlcdForwardTimeOut[1]                         /***/= EM_AL_LST.AL_0449_PROC_IRCUT_TR_B2_PIKER_DOWN_TIME_OUT_ERROR;
            PROC.AfterIRCutTransfer_B.UpDown1Cylinder.AlcdBackwardTimeOut[0]                        /***/= EM_AL_LST.AL_0444_PROC_IRCUT_TR_B1_PIKER_UP_TIME_OUT_ERROR;
            PROC.AfterIRCutTransfer_B.UpDown2Cylinder.AlcdBackwardTimeOut[1]                        /***/= EM_AL_LST.AL_0445_PROC_IRCUT_TR_B2_PIKER_UP_TIME_OUT_ERROR;

            UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.AlcdBackwardTimeOut[0]                   /***/= EM_AL_LST.AL_0624_ULD_BEFORE_TR_A1_PIKER_UP_TIME_OUT_ERROR;
            UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.AlcdBackwardTimeOut[1]                   /***/= EM_AL_LST.AL_0625_ULD_BEFORE_TR_A2_PIKER_UP_TIME_OUT_ERROR;
            UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.AlcdForwardTimeOut[0]                    /***/= EM_AL_LST.AL_0628_ULD_BEFORE_TR_A1_PIKER_DOWN_TIME_OUT_ERROR;
            UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.AlcdForwardTimeOut[1]                    /***/= EM_AL_LST.AL_0629_ULD_BEFORE_TR_A2_PIKER_DOWN_TIME_OUT_ERROR;
            UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.AlcdForwardTimeOut[0]                    /***/= EM_AL_LST.AL_0630_ULD_BEFORE_TR_B1_PIKER_DOWN_TIME_OUT_ERROR;
            UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.AlcdForwardTimeOut[1]                    /***/= EM_AL_LST.AL_0631_ULD_BEFORE_TR_B2_PIKER_DOWN_TIME_OUT_ERROR;
            UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.AlcdBackwardTimeOut[0]                   /***/= EM_AL_LST.AL_0626_ULD_BEFORE_TR_B1_PIKER_UP_TIME_OUT_ERROR;
            UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.AlcdBackwardTimeOut[1]                   /***/= EM_AL_LST.AL_0627_ULD_BEFORE_TR_B2_PIKER_UP_TIME_OUT_ERROR;
            
            UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.AlcdBackwardTimeOut[0]                   /***/= EM_AL_LST.AL_0668_ULD_AFTER_TR_A1_PIKER_UP_TIME_OUT_ERROR;
            UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.AlcdBackwardTimeOut[1]                   /***/= EM_AL_LST.AL_0669_ULD_AFTER_TR_A2_PIKER_UP_TIME_OUT_ERROR;
            UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.AlcdForwardTimeOut[0]                    /***/= EM_AL_LST.AL_0670_ULD_AFTER_TR_B1_PIKER_UP_TIME_OUT_ERROR;
            UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.AlcdForwardTimeOut[1]                    /***/= EM_AL_LST.AL_0671_ULD_AFTER_TR_B2_PIKER_UP_TIME_OUT_ERROR;
            UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.AlcdForwardTimeOut[0]                    /***/= EM_AL_LST.AL_0672_ULD_AFTER_TR_A1_PIKER_DOWN_TIME_OUT_ERROR;
            UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.AlcdForwardTimeOut[1]                    /***/= EM_AL_LST.AL_0673_ULD_AFTER_TR_A2_PIKER_DOWN_TIME_OUT_ERROR;
            UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.AlcdBackwardTimeOut[0]                   /***/= EM_AL_LST.AL_0674_ULD_AFTER_TR_B1_PIKER_DOWN_TIME_OUT_ERROR;
            UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.AlcdBackwardTimeOut[1]                   /***/= EM_AL_LST.AL_0675_ULD_AFTER_TR_B2_PIKER_DOWN_TIME_OUT_ERROR;
            
            UD.CstUnloader_A.TiltCylinder.AlcdBackwardTimeOut[0]                                    /***/= EM_AL_LST.AL_0720_ULD_CST_TILT_A_UP_TIME_OUT_ERROR;
            UD.CstUnloader_A.TiltCylinder.AlcdBackwardTimeOut[1]                                    /***/= EM_AL_LST.AL_0721_ULD_CST_TILT_B_UP_TIME_OUT_ERROR;
            UD.CstUnloader_B.TiltCylinder.AlcdForwardTimeOut[0]                                     /***/= EM_AL_LST.AL_0722_ULD_CST_TILT_A_DOWN_TIME_OUT_ERROR;
            UD.CstUnloader_B.TiltCylinder.AlcdForwardTimeOut[1]                                     /***/= EM_AL_LST.AL_0723_ULD_CST_TILT_B_DOWN_TIME_OUT_ERROR;
            #endregion
            //
            //UD.CstUnloader_A.TiltCylinder.AlcdBackwardTimeOut[0]                                    /***/= EM_AL_LST.AL_0173_FRONT_CENTERING1_UP_ERROR;
            //UD.CstUnloader_A.TiltCylinder.AlcdBackwardTimeOut[1]                                    /***/= EM_AL_LST.AL_0174_FRONT_CENTERING2_UP_ERROR;
            //UD.CstUnloader_B.TiltCylinder.AlcdForwardTimeOut[0]                                     /***/= EM_AL_LST.AL_0175_FRONT_CENTERING1_DOWN_ERROR;
            //UD.CstUnloader_B.TiltCylinder.AlcdForwardTimeOut[1]                                     /***/= EM_AL_LST.AL_0176_FRONT_CENTERING2_DOWN_ERROR;
            //#endregion
            //
            #region  DOOR_알람
            LD.Door01.OFF_TIME_OUT_ERROR                                    /***/= EM_AL_LST.AL_0017_DOOR_1_OPEN;
            LD.Door02.OFF_TIME_OUT_ERROR                                    /***/= EM_AL_LST.AL_0018_DOOR_2_OPEN;
            LD.Door03.OFF_TIME_OUT_ERROR                                    /***/= EM_AL_LST.AL_0019_DOOR_3_OPEN;
            PROC.Door04.OFF_TIME_OUT_ERROR                                  /***/= EM_AL_LST.AL_0020_DOOR_4_OPEN;
            PROC.Door05.OFF_TIME_OUT_ERROR                                  /***/= EM_AL_LST.AL_0021_DOOR_5_OPEN;
            PROC.Door06.OFF_TIME_OUT_ERROR                                  /***/= EM_AL_LST.AL_0022_DOOR_6_OPEN;
            PROC.Door07.OFF_TIME_OUT_ERROR                                  /***/= EM_AL_LST.AL_0023_DOOR_7_OPEN;
            PROC.Door08.OFF_TIME_OUT_ERROR                                  /***/= EM_AL_LST.AL_0024_DOOR_8_OPEN;
            UD.Door09.OFF_TIME_OUT_ERROR                                    /***/= EM_AL_LST.AL_0025_DOOR_9_OPEN;
            UD.Door10.OFF_TIME_OUT_ERROR                                    /***/= EM_AL_LST.AL_0026_DOOR_10_OPEN;
            UD.Door11.OFF_TIME_OUT_ERROR                                    /***/= EM_AL_LST.AL_0027_DOOR_11_OPEN;
            
            //
            // LD.DoorKeyBox01.OFF_TIME_OUT_ERROR                           /***/= EM_AL_LST.AL_0010_EQP_TOP_DOOR_01_OPEN_ERROR;
            // LD.DoorKeyBox02.OFF_TIME_OUT_ERROR                           /***/= EM_AL_LST.AL_0011_EQP_TOP_DOOR_02_OPEN_ERROR;
            // LD.DoorKeyBox03.OFF_TIME_OUT_ERROR                           /***/= EM_AL_LST.AL_0012_EQP_TOP_DOOR_03_OPEN_ERROR;
            // PROC.DoorKeyBox04.OFF_TIME_OUT_ERROR                            /***/= EM_AL_LST.AL_0010_EQP_TOP_DOOR_01_OPEN_ERROR;
            // PROC.DoorKeyBox05.OFF_TIME_OUT_ERROR                            /***/= EM_AL_LST.AL_0011_EQP_TOP_DOOR_02_OPEN_ERROR;
            // PROC.DoorKeyBox06.OFF_TIME_OUT_ERROR                            /***/= EM_AL_LST.AL_0012_EQP_TOP_DOOR_03_OPEN_ERROR;
            // UD.DoorKeyBox07.OFF_TIME_OUT_ERROR                            /***/= EM_AL_LST.AL_0010_EQP_TOP_DOOR_01_OPEN_ERROR;
            // UD.DoorKeyBox08.OFF_TIME_OUT_ERROR                            /***/= EM_AL_LST.AL_0011_EQP_TOP_DOOR_02_OPEN_ERROR;
            // UD.DoorKeyBox09.OFF_TIME_OUT_ERROR                            /***/= EM_AL_LST.AL_0012_EQP_TOP_DOOR_03_OPEN_ERROR;
            //
            //
            //
            #endregion



        }

        public bool IsHomeComplete
        {
            get { return LD.IsHomeComplete || PROC.IsHomeComplete || UD.IsHomeComplete; }
        }

        public bool IsGrapSwithOn { get { return LD.SafteyEnableGripSwitchOn1.IsOnOff || PROC.SafetyEnableGripSwitchOn.IsOnOff || UD.SafteyEnableGripSwitchOn3.IsOnOff; } }

        public BaseUnit GetUnit(EmUnitPosi posi, EmUnitType unitType)
        {
            if (posi == EmUnitPosi.ACol && unitType == EmUnitType.CstLoader)
            {
                return LD.CstLoader_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.CstLoader)
            {
                return LD.CstLoader_B;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.CstLoader)
            {
                return LD.CstLoader_B;
            }
            else if (posi == EmUnitPosi.ACol && unitType == EmUnitType.LoaderTransfer)
            {
                return LD.LoaderTransfer_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.LoaderTransfer)
            {
                return LD.LoaderTransfer_B;
            }
            else if (posi == EmUnitPosi.ACol && unitType == EmUnitType.AfterIRCutTransfer)
            {
                return PROC.AfterIRCutTransfer_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.AfterIRCutTransfer)
            {
                return PROC.AfterIRCutTransfer_B;
            }

            else if (posi == EmUnitPosi.ACol && unitType == EmUnitType.BeforeInspUnLoaderTransfer)
            {
                return UD.BeforeInspUnloaderTransfer_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.BeforeInspUnLoaderTransfer)
            {
                return UD.BeforeInspUnloaderTransfer_A;
            }
            else if (posi == EmUnitPosi.ACol && unitType == EmUnitType.InspStage)
            {
                return UD.InspectionStage_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.InspStage)
            {
                return UD.InspectionStage_B;
            }
            else if (posi == EmUnitPosi.ACol && unitType == EmUnitType.AfterInspUnLoaderTransfer)
            {
                return UD.AfterInspUnloaderTransfer_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.AfterInspUnLoaderTransfer)
            {
                return UD.AfterInspUnloaderTransfer_B;
            }
            else if (posi == EmUnitPosi.ACol && unitType == EmUnitType.LaserCuttingStage)
            {
                return PROC.IRCutStage_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.LaserCuttingStage)
            {
                return PROC.IRCutStage_B;
            }
            else if (unitType == EmUnitType.LaserCuttingHead)
            {
                return PROC.LaserHead;
            }
            else if (unitType == EmUnitType.FineAlign)
            {
                return PROC.FineAlign;
            }
            else if (unitType == EmUnitType.BreakHead)
            {
                return PROC.BreakingHead;
            }
            else if (unitType == EmUnitType.BreakAlign)
            {
                return PROC.BreakAlign;
            }
            else if (unitType == EmUnitType.InspCamera)
            {
                return UD.InspCamera;
            }
            else if (posi == EmUnitPosi.ACol && unitType == EmUnitType.BreakStage)
            {
                return PROC.BreakStage_A;
            }
            else if (posi == EmUnitPosi.BCol && unitType == EmUnitType.BreakStage)
            {
                return PROC.BreakStage_B;
            }

            return null;
        }

        public Equipment()
        {
            CurrentRecipe = new EqpRecipe();

            LD = new LoaderModule();
            PROC = new ProceeModule();
            UD = new UnloaderMoudle();

            EqpRecipeMgr = new EqpRecipeManager();
            BreakRecipeMgr = new BreakRecipeManager();
            ProcessRecipeMgr = new ProcessRecipeManager();
            UserInfoMgr = new UserInfoManager();

            EqpSysParamMgr = new EqpSystemParamSettingManager();
            EqpSysMgr = new EqpSystemParamSkipManager();

            WorkInfos = new WorkInfo();
            RunMode = EmEquipRunMode.Stop;


            //LD.CstLoader PIO 연결 
            LD.CstLoader_A.PioSend.LowerPioRecv = LD.Loader.PioRecvA;
            LD.CstLoader_B.PioSend.LowerPioRecv = LD.Loader.PioRecvB;

            //LD.Loader
            LD.Loader.PioRecvA.UpperPioSend = LD.CstLoader_A.PioSend;
            LD.Loader.PioRecvB.UpperPioSend = LD.CstLoader_B.PioSend;

            LD.Loader.PioSendA.LowerPioRecv = LD.LoaderTransfer_A.PioRecv;
            LD.Loader.PioSendB.LowerPioRecv = LD.LoaderTransfer_B.PioRecv;

            //LoaderTransfer
            LD.LoaderTransfer_A.PioRecv.UpperPioSend = LD.Loader.PioSendA;
            LD.LoaderTransfer_B.PioRecv.UpperPioSend = LD.Loader.PioSendB;

            LD.LoaderTransfer_A.PioSend.LowerPioRecv = PROC.IRCutStage_A.PioRecv;
            LD.LoaderTransfer_B.PioSend.LowerPioRecv = PROC.IRCutStage_B.PioRecv;

            //IRCutStage
            PROC.IRCutStage_A.PioRecv.UpperPioSend = LD.LoaderTransfer_A.PioSend;
            PROC.IRCutStage_B.PioRecv.UpperPioSend = LD.LoaderTransfer_B.PioSend;

            PROC.IRCutStage_A.PioSend.LowerPioRecv = PROC.AfterIRCutTransfer_A.PioRecv;
            PROC.IRCutStage_B.PioSend.LowerPioRecv = PROC.AfterIRCutTransfer_B.PioRecv;

            //AfterIRCutTransfer
            PROC.AfterIRCutTransfer_A.PioRecv.UpperPioSend = PROC.IRCutStage_A.PioSend;
            PROC.AfterIRCutTransfer_B.PioRecv.UpperPioSend = PROC.IRCutStage_B.PioSend;

            PROC.AfterIRCutTransfer_A.PioSend.LowerPioRecv = PROC.BreakStage_A.PioRecv;
            PROC.AfterIRCutTransfer_B.PioSend.LowerPioRecv = PROC.BreakStage_B.PioRecv;

            //BreakStage
            PROC.BreakStage_A.PioRecv.UpperPioSend = PROC.AfterIRCutTransfer_A.PioSend;
            PROC.BreakStage_B.PioRecv.UpperPioSend = PROC.AfterIRCutTransfer_B.PioSend;

            PROC.BreakStage_A.PioSend.LowerPioRecv = UD.BeforeInspUnloaderTransfer_A.PioRecv;
            PROC.BreakStage_B.PioSend.LowerPioRecv = UD.BeforeInspUnloaderTransfer_B.PioRecv;

            //BeforeInspUnloaderTransfer
            UD.BeforeInspUnloaderTransfer_A.PioRecv.UpperPioSend = PROC.BreakStage_A.PioSend;
            UD.BeforeInspUnloaderTransfer_B.PioRecv.UpperPioSend = PROC.BreakStage_B.PioSend;

            UD.BeforeInspUnloaderTransfer_A.PioSend.LowerPioRecv = UD.InspectionStage_A.PioRecv;
            UD.BeforeInspUnloaderTransfer_B.PioSend.LowerPioRecv = UD.InspectionStage_B.PioRecv;

            //InspectionStage
            UD.InspectionStage_A.PioRecv.UpperPioSend = UD.BeforeInspUnloaderTransfer_A.PioSend;
            UD.InspectionStage_B.PioRecv.UpperPioSend = UD.BeforeInspUnloaderTransfer_B.PioSend;

            UD.InspectionStage_A.PioSend.LowerPioRecv = UD.AfterInspUnloaderTransfer_A.PioRecv;
            UD.InspectionStage_B.PioSend.LowerPioRecv = UD.AfterInspUnloaderTransfer_B.PioRecv;

            //AfterInspUnloaderTransfer
            UD.AfterInspUnloaderTransfer_A.PioRecv.UpperPioSend = UD.InspectionStage_A.PioSend;
            UD.AfterInspUnloaderTransfer_B.PioRecv.UpperPioSend = UD.InspectionStage_B.PioSend;

            UD.AfterInspUnloaderTransfer_A.PioSend.LowerPioRecv = UD.Unloader.PioRecvA;
            UD.AfterInspUnloaderTransfer_B.PioSend.LowerPioRecv = UD.Unloader.PioRecvB;

            //Unloader
            UD.Unloader.PioRecvA.UpperPioSend = UD.AfterInspUnloaderTransfer_A.PioSend;
            UD.Unloader.PioRecvB.UpperPioSend = UD.AfterInspUnloaderTransfer_B.PioSend;

            UD.Unloader.PioSendA.LowerPioRecv = UD.CstUnloader_A.PioRecv;
            UD.Unloader.PioSendB.LowerPioRecv = UD.CstUnloader_B.PioRecv;

            UD.CstUnloader_A.PioRecv.UpperPioSend = UD.Unloader.PioSendA;
            UD.CstUnloader_B.PioRecv.UpperPioSend = UD.Unloader.PioSendB;

        }

        public void StartLogic()
        {
            _running = true;
            _worker = new Thread(new ThreadStart(Working));
            _worker.Start();
        }
        public void StopLogic()
        {
            _running = false;
            _worker.Join();
        }

        public void BuzzserStop()
        {

        }

        private Stopwatch _swInterval = new Stopwatch();
        public double ScanTick = 0;
        public double ScanTime = 0;
        public double MaxScanTick = 0;
        public double MaxScanTime = 0;
        public void Working()
        {
            while (_running)
            {
                _swInterval.Stop();

                ScanTick = Math.Round(_swInterval.ElapsedTicks / 10000f, 3);
                ScanTime = _swInterval.ElapsedMilliseconds;
                if (MaxScanTick < ScanTick) MaxScanTick = ScanTick;
                if (MaxScanTime < ScanTime) MaxScanTime = ScanTime;

                _swInterval.Restart();
                LogicWorking();
                Thread.Sleep(1);
            }
        }

        public List<ServoMotorControl> GetServoMotors(EM_MOTOR_POSI posi, EM_MOTOR_TYPE type)
        {
            List<ServoMotorControl> motors = new List<ServoMotorControl>()
                    {
                LD.CstLoader_A.CstRotationAxis, LD.CstLoader_A.CstUpDownAxis,
                LD.CstLoader_B.CstRotationAxis, LD.CstLoader_B.CstUpDownAxis,

                LD.Loader.X1Axis,LD.Loader.X2Axis,
                LD.Loader.Y1Axis,LD.Loader.Y2Axis,

                LD.LoaderTransfer_A.YAxis,
                LD.LoaderTransfer_A.X1Axis, LD.LoaderTransfer_A.X2Axis,
                LD.LoaderTransfer_A.SubY1Axis, LD.LoaderTransfer_A.SubY2Axis,
                LD.LoaderTransfer_A.SubT1Axis, LD.LoaderTransfer_A.SubT2Axis,

                LD.LoaderTransfer_B.YAxis,
                LD.LoaderTransfer_B.X1Axis, LD.LoaderTransfer_B.X2Axis,
                LD.LoaderTransfer_B.SubY1Axis, LD.LoaderTransfer_B.SubY2Axis,
                LD.LoaderTransfer_B.SubT1Axis, LD.LoaderTransfer_B.SubT2Axis,

                LD.PreAlign.XAxis,
                PROC.FineAlign.XAxis,

                PROC.IRCutStage_A.YAxis, PROC.IRCutStage_B.YAxis,
                PROC.LaserHead.XAxis, PROC.LaserHead.ZAxis,

                PROC.AfterIRCutTransfer_A.YAxis, PROC.AfterIRCutTransfer_B.YAxis,


                PROC.BreakStage_A.YAxis,
                PROC.BreakStage_A.SubX1Axis, PROC.BreakStage_A.SubX2Axis,
                PROC.BreakStage_A.SubY1Axis, PROC.BreakStage_A.SubY2Axis,
                PROC.BreakStage_A.SubT1Axis, PROC.BreakStage_A.SubT2Axis,

                PROC.BreakStage_B.YAxis,
                PROC.BreakStage_B.SubX1Axis, PROC.BreakStage_B.SubX2Axis,
                PROC.BreakStage_B.SubY1Axis, PROC.BreakStage_B.SubY2Axis,
                PROC.BreakStage_B.SubT1Axis, PROC.BreakStage_B.SubT2Axis,

                PROC.BreakingHead.X1Axis, PROC.BreakingHead.X2Axis,
                PROC.BreakingHead.Z1Axis, PROC.BreakingHead.Z2Axis,

                PROC.BreakAlign.XAxis, PROC.BreakAlign.ZAxis ,


                UD.BeforeInspUnloaderTransfer_A.Y1Axis, UD.BeforeInspUnloaderTransfer_B.Y1Axis ,

                UD.AfterInspUnloaderTransfer_A.Y2Axis, UD.AfterInspUnloaderTransfer_A.T1Axis, UD.AfterInspUnloaderTransfer_A.T2Axis,
                UD.AfterInspUnloaderTransfer_B.Y2Axis, UD.AfterInspUnloaderTransfer_B.T1Axis,  UD.AfterInspUnloaderTransfer_B.T2Axis,

                UD.InspCamera.XAxis, UD.InspCamera.ZAxis,

                UD.InspectionStage_A.YAxis, UD.InspectionStage_B.YAxis,

                UD.Unloader.X1Axis, UD.Unloader.X2Axis, UD.Unloader.Y1Axis, UD.Unloader.Y2Axis,

                UD.CstUnloader_A.CstRotationAxis, UD.CstUnloader_A.CstUpDownAxis,
                UD.CstUnloader_B.CstRotationAxis, UD.CstUnloader_B.CstUpDownAxis
                    };

            return motors.Where(f => (f.MotorPosi == posi || posi == EM_MOTOR_POSI.ALL) && (f.MotorType == type || type == EM_MOTOR_TYPE.ALL)).OrderBy(f => f.OutterAxisNo).ToList();
        }
        public void SetAddress()
        {
            int axisNo = 1;

            LD.CstLoader_A.CstRotationAxis              /**/ = new CstLoaderRotationUpAxisServo(       /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Lifter_R1, "LD.CstLoader_A.CstRotationAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.CstLoader_A.CstUpDownAxis                /**/ = new CstLoaderUpDownAxisServo(           /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Lifter_Z1, "LD.CstLoader_A.CstUpDownAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };

            LD.CstLoader_B.CstRotationAxis              /**/ = new CstLoaderRotationUpAxisServo(       /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Lifter_R2, "LD.CstLoader_B.CstRotationAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.CstLoader_B.CstUpDownAxis                /**/ = new CstLoaderUpDownAxisServo(           /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Lifter_Z2, "LD.CstLoader_B.CstUpDownAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };

            LD.Loader.X1Axis                            /**/= new LoaderXAxisServo(                    /**/axisNo++, SAxisNoMgr.AJIN_취출_HAND_X1, "LD.LoadUnit.X1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD, AxisPosi = EmAxisPosi.Aixs1 };
            LD.Loader.X2Axis                            /**/= new LoaderXAxisServo(                    /**/axisNo++, SAxisNoMgr.AJIN_취출_HAND_X2, "LD.LoadUnit.X2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD, AxisPosi = EmAxisPosi.Aixs2 };
            LD.Loader.Y1Axis                            /**/= new LoaderYAxisServo(                    /**/axisNo++, SAxisNoMgr.AJIN_취출_HAND_Y1, "LD.LoadUnit.Y1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.Loader.Y2Axis                            /**/= new LoaderYAxisServo(                    /**/axisNo++, SAxisNoMgr.AJIN_취출_HAND_Y2, "LD.LoadUnit.Y2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };

            LD.LoaderTransfer_A.YAxis                   /**/= new LoaderTransferYAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_Y1, "LD.LoaderTransfer_A.YAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_A.X1Axis                  /**/= new LoaderTransferXAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_X1, "LD.LoaderTransfer_A.X1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_A.X2Axis                  /**/= new LoaderTransferXAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_X2, "LD.LoaderTransfer_A.X2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_A.SubY1Axis               /**/= new LoaderTransferSubYAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_Y3, "LD.LoaderTransfer_A.SubY1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_A.SubY2Axis               /**/= new LoaderTransferSubYAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_Y4, "LD.LoaderTransfer_A.SubY2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_A.SubT1Axis               /**/= new LoaderTransferSubTAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_T1, "LD.LoaderTransfer_A.SubT1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_A.SubT2Axis               /**/= new LoaderTransferSubTAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_T2, "LD.LoaderTransfer_A.SubT2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };

            LD.LoaderTransfer_B.YAxis                   /**/= new LoaderTransferYAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_Y2, "LD.LoaderTransfer_B.YAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_B.X1Axis                  /**/= new LoaderTransferXAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_X3, "LD.LoaderTransfer_B.X1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_B.X2Axis                  /**/= new LoaderTransferXAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_X4, "LD.LoaderTransfer_B.X2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_B.SubY1Axis               /**/= new LoaderTransferSubYAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_Y5, "LD.LoaderTransfer_B.SubY1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_B.SubY2Axis               /**/= new LoaderTransferSubYAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_Y6, "LD.LoaderTransfer_B.SubY2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_B.SubT1Axis               /**/= new LoaderTransferSubTAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_T3, "LD.LoaderTransfer_B.SubT1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };
            LD.LoaderTransfer_B.SubT2Axis               /**/= new LoaderTransferSubTAxisServo(            /**/axisNo++, SAxisNoMgr.AJIN_LD_TR_T4, "LD.LoaderTransfer_B.SubT2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };

            LD.PreAlign.XAxis                           /**/ = new PreAlignXAxisServo(                 /**/axisNo++, SAxisNoMgr.AJIN_PRE_ALIGN_X1, "LD.PreAlign.XAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.LD };

            PROC.FineAlign.XAxis                        /**/ = new FineAlignXAxisServo(                /**/axisNo++, SAxisNoMgr.UMAC_FINE_ALIGN_X1, "PROC.FineAlgin.XAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };


            PROC.IRCutStage_A.YAxis                     /**/= new LaserCuttingStageYAxisServo(         /**/axisNo++, SAxisNoMgr.UMAC_가공_STAGE_Y1, "PROC.IRCutStage_A.YAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.IRCutStage_B.YAxis                     /**/= new LaserCuttingStageYAxisServo(         /**/axisNo++, SAxisNoMgr.UMAC_가공_STAGE_Y2, "PROC.IRCutStage_B.YAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.LaserHead.XAxis                        /**/ = new LaserCuttingHeadXAxisServo(         /**/axisNo++, SAxisNoMgr.UMAC_가공_HEAD_X1, "PROC.LaserHead.XAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.LaserHead.ZAxis                        /**/ = new LaserCuttingHeadZAxisServo(         /**/axisNo++, SAxisNoMgr.UMAC_가공_HEAD_Z1, "PROC.LaserHead.ZAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };

            PROC.AfterIRCutTransfer_A.YAxis             /**/= new AfterIRCutTransferYAxisServo(                /**/axisNo++, SAxisNoMgr.AJIN_에프터_IR_CUT_TR_Y1, "PROC.AfterIRCutTransfer_A.YAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.AfterIRCutTransfer_B.YAxis             /**/= new AfterIRCutTransferYAxisServo(                /**/axisNo++, SAxisNoMgr.AJIN_에프터_IR_CUT_TR_Y2, "PROC.AfterIRCutTransfer_B.YAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.PROC };

            PROC.BreakStage_A.YAxis                     /**/= new BreakUnitYAxisServo(                 /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_STAGE_Y1, "PROC.BreakStage_A.YAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_A.SubX1Axis                 /**/= new BreakUnitSubXAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_X1, "PROC.BreakStage_A.SubX1Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_A.SubX2Axis                 /**/= new BreakUnitSubXAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_X2, "PROC.BreakStage_A.SubX2Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_A.SubY1Axis                 /**/= new BreakUnitSubYAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_Y3, "PROC.BreakStage_A.SubY1Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_A.SubY2Axis                 /**/= new BreakUnitSubYAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_Y4, "PROC.BreakStage_A.SubY2Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_A.SubT1Axis                 /**/= new BreakUnitSubTAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_T1, "PROC.BreakStage_A.SubT1Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_A.SubT2Axis                 /**/= new BreakUnitSubTAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_T2, "PROC.BreakStage_A.SubT2Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };


            PROC.BreakStage_B.YAxis                     /**/= new BreakUnitYAxisServo(                 /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_STAGE_Y2, "PROC.BreakStage_B.YAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_B.SubX1Axis                 /**/= new BreakUnitSubXAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_X3, "PROC.BreakStage_B.SubX1Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_B.SubX2Axis                 /**/= new BreakUnitSubXAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_X4, "PROC.BreakStage_B.SubX2Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_B.SubY1Axis                 /**/= new BreakUnitSubYAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_Y5, "PROC.BreakStage_B.SubY1Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_B.SubY2Axis                 /**/= new BreakUnitSubYAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_Y6, "PROC.BreakStage_B.SubY2Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_B.SubT1Axis                 /**/= new BreakUnitSubTAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_T3, "PROC.BreakStage_B.SubT1Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakStage_B.SubT2Axis                 /**/= new BreakUnitSubTAxisServo(              /**/axisNo++, SAxisNoMgr.EZI_BREAKING_STAGE_T4, "PROC.BreakStage_B.SubT2Axis") { MotorType = EM_MOTOR_TYPE.EZi, MotorPosi = EM_MOTOR_POSI.PROC };

            PROC.BreakingHead.X1Axis                    /**/= new BreakingHeadXAxisServo(              /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_HEAD_X1, "PROC.BreakingHead.X1Axis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakingHead.X2Axis                    /**/= new BreakingHeadXAxisServo(              /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_HEAD_X2, "PROC.BreakingHead.X2Axis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakingHead.Z1Axis                    /**/= new BreakingHeadZAxisServo(              /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_HEAD_Z1, "PROC.BreakingHead.Z1Axis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakingHead.Z2Axis                    /**/= new BreakingHeadZAxisServo(              /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_HEAD_Z2, "PROC.BreakingHead.Z2Axis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };

            PROC.BreakAlign.XAxis                       /**/= new BreakAlignXAxisServo(                /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_JIG_X1, "PROC.BreakAlign.XAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };
            PROC.BreakAlign.ZAxis                       /**/= new BreakAlignZAxisServo(                /**/axisNo++, SAxisNoMgr.UMAC_BREAKING_JIG_Z1, "PROC.BreakAlign.ZAxis") { MotorType = EM_MOTOR_TYPE.Umac, MotorPosi = EM_MOTOR_POSI.PROC };

            UD.BeforeInspUnloaderTransfer_A.Y1Axis      /**/= new BeforeInspUnloaderTransferYAxisServo(/**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_Y1, "UD.UnloaderTransfer_A.Y1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.BeforeInspUnloaderTransfer_B.Y1Axis      /**/= new BeforeInspUnloaderTransferYAxisServo(/**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_Y2, "UD.UnloaderTransfer_B.Y1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };

            UD.AfterInspUnloaderTransfer_A.Y2Axis       /**/= new AfterInspUnloaderTransferYAxisServo( /**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_Y3, "UD.UnloaderTransfer_A.Y2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.AfterInspUnloaderTransfer_A.T1Axis       /**/= new AfterInspUnloaderTransferTAxisServo( /**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_T1, "UD.UnloaderTransfer_A.T1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.AfterInspUnloaderTransfer_A.T2Axis       /**/= new AfterInspUnloaderTransferTAxisServo( /**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_T2, "UD.UnloaderTransfer_A.T2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };

            UD.AfterInspUnloaderTransfer_B.Y2Axis       /**/= new AfterInspUnloaderTransferYAxisServo( /**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_Y4, "UD.UnloaderTransfer_B.Y2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.AfterInspUnloaderTransfer_B.T1Axis       /**/= new AfterInspUnloaderTransferTAxisServo( /**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_T3, "UD.UnloaderTransfer_B.T1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.AfterInspUnloaderTransfer_B.T2Axis       /**/= new AfterInspUnloaderTransferTAxisServo( /**/axisNo++, SAxisNoMgr.AJIN_ULD_TR_T4, "UD.UnloaderTransfer_B.T2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };

            UD.InspCamera.XAxis                         /**/ = new InspCameraXAxisServo(               /**/axisNo++, SAxisNoMgr.AJIN_AOI_Camera_X1, "UD.InspCamera.XAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.InspCamera.ZAxis                         /**/ = new InspCameraZAxisServo(               /**/axisNo++, SAxisNoMgr.AJIN_AOI_Camera_Z1, "UD.InspCamera.ZAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };

            UD.InspectionStage_A.YAxis                  /**/ = new InspStageYAxisServo(                /**/axisNo++, SAxisNoMgr.AJIN_AOI_Stage_Y1, "UD.InspectionStage_A.YAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.InspectionStage_B.YAxis                  /**/ = new InspStageYAxisServo(                /**/axisNo++, SAxisNoMgr.AJIN_AOI_Stage_Y2, "UD.InspectionStage_B.YAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };

            UD.Unloader.X1Axis                          /**/= new UnLoaderXAxisServo(                  /**/axisNo++, SAxisNoMgr.AJIN_투입_HAND_X1, "UnloadUnit.X1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.Unloader.X2Axis                          /**/= new UnLoaderXAxisServo(                  /**/axisNo++, SAxisNoMgr.AJIN_투입_HAND_X2, "UnloadUnit.X2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.Unloader.Y1Axis                          /**/= new UnLoaderYAxisServo(                  /**/axisNo++, SAxisNoMgr.AJIN_투입_HAND_Y1, "UnloadUnit.Y1Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.Unloader.Y2Axis                          /**/= new UnLoaderYAxisServo(                  /**/axisNo++, SAxisNoMgr.AJIN_투입_HAND_Y2, "UnloadUnit.Y2Axis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };

            UD.CstUnloader_A.CstRotationAxis            /**/= new CstUnloaderRotationUpAxisServo(      /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Stage_R3, "UD.CstUnloader_A.CstRotationAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.CstUnloader_A.CstUpDownAxis              /**/= new CstUnloaderUpDownAxisServo(          /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Lifter_Z3, "UD.CstUnloader_A.CstUpDownAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.CstUnloader_B.CstRotationAxis            /**/= new CstUnloaderRotationUpAxisServo(      /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Stage_R4, "UD.CstUnloader_B.CstRotationAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };
            UD.CstUnloader_B.CstUpDownAxis              /**/= new CstUnloaderUpDownAxisServo(          /**/axisNo++, SAxisNoMgr.AJIN_Cassette_Lifter_Z4, "UD.CstUnloader_B.CstUpDownAxis") { MotorType = EM_MOTOR_TYPE.Ajin, MotorPosi = EM_MOTOR_POSI.UD };


            //PMac.YB_EquipMode                       /**/ = AddressMgr.GetAddress("PMAC_YB_CheckMode", 0);
            //PMac.YB_CheckAlarmStatus                /**/ = AddressMgr.GetAddress("PMAC_YB_CheckAlarmStatus", 0);
            //PMac.YB_UpperInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_UpperInterfaceWorking", 0);
            //PMac.YB_LowerInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_LowerInterfaceWorking", 0);
            //PMac.XB_PmacReady                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacReady", 0);
            //PMac.XB_PmacAlive                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacAlive", 0);
            //PMac.XB_PmacHeavyAlarm                  /**/ = AddressMgr.GetAddress("PMAC_XB_PmacMidAlarm", 0);
            //PMac.YB_PinUpMotorInterlockDisable      /**/ = AddressMgr.GetAddress("PMAC_YB_PinUpMotorInterlock", 0);
            //PMac.XB_PinUpMotorInterlockDisableAck   /**/ = AddressMgr.GetAddress("PMAC_XB_PinUpMotorInterlockAck", 0);
            //PMac.YB_ReviewTimerOverCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_ReviewTimerOverCmd", 0);
            //PMac.XB_ReviewTimerOverCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_ReviewTimerOverCmdAck", 0);
            //PMac.YB_PmacResetCmd                    /**/ = AddressMgr.GetAddress("PMAC_YB_PmacResetCmd", 0);
            //PMac.XB_PmacResetCmdAck                 /**/ = AddressMgr.GetAddress("PMAC_XB_PmacResetCmdAck", 0);



            LD.CstLoader_B.InitializeInterLock();
            LD.CstLoader_A.InitializeInterLock();
            LD.Loader.InitializeInterLock();
            LD.LoaderTransfer_B.InitializeInterLock();
            LD.LoaderTransfer_A.InitializeInterLock();
            LD.PreAlign.InitializeInterLock();



            PROC.FineAlign.InitializeInterLock();
            PROC.IRCutStage_B.InitializeInterLock();
            PROC.IRCutStage_A.InitializeInterLock();
            PROC.LaserHead.InitializeInterLock();

            PROC.AfterIRCutTransfer_B.InitializeInterLock();
            PROC.AfterIRCutTransfer_A.InitializeInterLock();

            PROC.BreakStage_B.InitializeInterLock();
            PROC.BreakStage_A.InitializeInterLock();

            PROC.BreakingHead.InitializeInterLock();
            PROC.BreakAlign.InitializeInterLock();

            UD.BeforeInspUnloaderTransfer_A.InitializeInterLock();
            UD.BeforeInspUnloaderTransfer_B.InitializeInterLock();

            UD.InspCamera.InitializeInterLock();
            UD.InspectionStage_B.InitializeInterLock();
            UD.InspectionStage_A.InitializeInterLock();

            UD.AfterInspUnloaderTransfer_B.InitializeInterLock();
            UD.AfterInspUnloaderTransfer_A.InitializeInterLock();

            UD.Unloader.InitializeInterLock();
            UD.CstUnloader_B.InitializeInterLock();
            UD.CstUnloader_A.InitializeInterLock();
            #region PMAC

            List<ServoMotorControl> motors = GetServoMotors(EM_MOTOR_POSI.ALL, EM_MOTOR_TYPE.ALL);

            //PMac.YB_EquipMode                       /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 0);
            //PMac.YB_CheckAlarmStatus                /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 1);
            //PMac.YB_UpperInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 2);
            //PMac.YB_LowerInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 3);

            //PMac.XB_PmacReady                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 0);
            //PMac.XB_PmacAlive                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 1);
            //PMac.XB_ReviewUsingPmac               /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 2);

            //PMac.XB_PmacHeavyAlarm                /**/ = AddressMgr.GetAddress("PMAC_XB_PmacMidAlarm", 0);
            //PMac.YB_PinUpMotorInterlockOffCmd     /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 0);
            //PMac.XB_PinUpMotorInterlockOffCmdAck  /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 0);
            //PMac.YB_PmacResetCmd                    /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 1);
            //PMac.XB_PmacResetCmdAck                 /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 1);
            //PMac.YB_ImmediateStopCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 2);
            //PMac.XB_ImmediateStopCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 2);
            //PMac.YB_PmacValueSave                   /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 3);
            //PMac.XB_PmacValueSaveAck                /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 3);
            //PMac.YB_ReviewTimerOverCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 4);
            //PMac.XB_ReviewTimerOverCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 4);

            //ScanX1.YF_TriggerStartPosi            /**/ = AddressMgr.GetAddress("TriggerStartPosCmd", 0);
            //ScanX1.XF_TriggerStartPosiAck         /**/ = AddressMgr.GetAddress("TriggerStartPosCmdAck", 0);            
            //ScanX1.YF_TriggerEndPosi              /**/ = AddressMgr.GetAddress("TriggerEndPosCmd", 0);
            //ScanX1.XF_TriggerEndPosiAck           /**/ = AddressMgr.GetAddress("TriggerEndPosCmdAck", 0);

            for (int jPos = 0; jPos < motors.Count; jPos++)
            {
                string axisStr = GG.TestMode == true ? motors[jPos].InnerAxisNo.ToString("0#") : motors[jPos].OutterAxisNo.ToString("0#");
                string motorType = motors[jPos].MotorType.ToString();

                motors[jPos].XB_StatusHomeCompleteBit                       /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_StatusHomeCompleteBit", motorType, axisStr), 0);
                motors[jPos].XB_StatusHomeInPosition                        /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_StatusHomeInPosition", motorType, axisStr), 0);
                motors[jPos].XB_StatusMotorMoving                           /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_StatusMotorMoving", motorType, axisStr), 0);
                motors[jPos].XB_StatusMotorInPosition                       /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_StatusMotorInPosition", motorType, axisStr), 0);
                motors[jPos].XB_StatusNegativeLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_StatusNegativeLimitSet", motorType, axisStr), 0);
                motors[jPos].XB_StatusPositiveLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_StatusPositiveLimitSet", motorType, axisStr), 0);
                motors[jPos].XB_StatusMotorServoOn                          /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_ErrMotorServoOn", motorType, axisStr), 0);
                motors[jPos].XB_ErrFatalFollowingError                      /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_ErrFatalFollowingError", motorType, axisStr), 0);
                motors[jPos].XB_ErrAmpFaultError                            /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_ErrAmpFaultError", motorType, axisStr), 0);
                motors[jPos].XB_ErrI2TAmpFaultError                         /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_ErrI2TAmpFaultError", motorType, axisStr), 0);

                motors[jPos].XI_CurrMotorLow                                /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XI_CurrMotorLow", motorType, axisStr), 0);
                motors[jPos].XI_CurrMotorHight                              /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XI_CurrMotorHight", motorType, axisStr), 0);
                motors[jPos].XI_CurrMotorStatus1                            /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XI_CurrMotorStatus1", motorType, axisStr), 0);


                motors[jPos].XF_CurrMotorPositionPlcAddr                    /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XF_CurrentMotorPosition", motorType, axisStr), 0);
                motors[jPos].XF_CurrMotorSpeed                              /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XF_CurrentMotorSpeed", motorType, axisStr), 0);
                motors[jPos].XF_CurrMotorActualLoad                         /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XI_CurrentMotorActualLoad", motorType, axisStr), 0);

                motors[jPos].YB_HomeCmd                                     /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YB_HomeCmd", motorType, axisStr), 0);
                motors[jPos].XB_HomeCmdAck                                  /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_HomeCmdAck", motorType, axisStr), 0);

                motors[jPos].YB_MotorStopCmd                                /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YB_MotorStopCmd", motorType, axisStr), 0);
                motors[jPos].XB_MotorStopCmdAck                             /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_MotorStopCmdAck", motorType, axisStr), 0);

                motors[jPos].YB_ServoOnOffCmd                               /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YB_ServoOnOffCmd", motorType, axisStr), 0);
                motors[jPos].XB_ServoOnOffCmdAck                            /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_ServoOnOffCmdAck", motorType, axisStr), 0);
                motors[jPos].YB_ServoOnOff                                  /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YB_ServoOnOff", motorType, axisStr), 0);

                motors[jPos].YB_MotorJogMinusMove                           /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YB_MinusJogCmd", motorType, axisStr), 0);
                motors[jPos].YB_MotorJogPlusMove                            /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YB_PlusJogCmd", motorType, axisStr), 0);
                motors[jPos].YF_MotorJogSpeedCmdPlcAddr                            /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YF_JogSpeed", motorType, axisStr), 0);
                motors[jPos].XF_MotorJogSpeedCmdAckPlcAddr                         /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XF_JogSpeedAck", motorType, axisStr), 0);

                motors[jPos].YB_PTPMoveCmd                                  /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YB_PTPMoveCmd", motorType, axisStr), 0);
                motors[jPos].XB_PTPMoveCmdAck                               /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XB_PTPMoveCmdAck", motorType, axisStr), 0);


                motors[jPos].YF_PTPMoveSpeed                                /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YF_PTPMoveSpeed", motorType, axisStr), 0);
                motors[jPos].XF_PTPMoveSpeedAck                             /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XF_PTPMoveSpeedAck", motorType, axisStr), 0);

                motors[jPos].YF_PTPMovePositionPlcAddr                      /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YF_PTPMovePosition", motorType, axisStr), 0);
                motors[jPos].XF_PTPMovePositionAckPlcAddr                   /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XF_PTPMovePositionAck", motorType, axisStr), 0);

                motors[jPos].YF_PTPMoveAccel                                /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_YF_PTPMoveAccel", motorType, axisStr), 0);
                motors[jPos].XF_PTPMoveAccelAck                             /**/  = AddressMgr.GetAddress(string.Format("{0}_Axis{1}_XF_PTPMoveAccelAck", motorType, axisStr), 0);


            }


            #endregion


            #region LOADER IN START                   
            LD.CstLoader_A.LightCurtain.MutingBtnSwitch.XB_OnOff           /**/= AddressMgr.GetAddress("L/D_A열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK", 0);
            LD.CstLoader_B.LightCurtain.MutingBtnSwitch.XB_OnOff           /**/= AddressMgr.GetAddress("L/D_B열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK", 0);

            LD.CpBoxResetSwitch.XB_OnOff                                   /**/= AddressMgr.GetAddress("CP_BOX_RESET_SWITCH_FEEDBACK", 0);
            LD.GpsMainMccbTrip.XB_OnOff                                    /**/= AddressMgr.GetAddress("GPS_MAIN_MCCB_TRIP_FEEDBACK", 0);

            LD.LDEmergencyStop1.XB_OnOff                                   /**/= AddressMgr.GetAddress("#1_L/D_EMERGENCY_STOP_FEEDBACK_(L/D_제어_모니터_부)", 0);
            LD.LDEmergencyStop2.XB_OnOff                                   /**/= AddressMgr.GetAddress("#2_L/D_EMERGENCY_STOP_FEEDBACK_(L/D_CASSETTE_투입_부)", 0);
            LD.LDEmergencyStop3.XB_OnOff                                   /**/= AddressMgr.GetAddress("#3_L/D_EMERGENCY_STOP_FEEDBACK_(L/D_CP-BOX)", 0);
            LD.EmergencyStopEnableGripSwitch1.XB_OnOff                     /**/= AddressMgr.GetAddress("#1_EMERGENCY_STOP_FEEDBACK_ENABLE_GRIP_SW", 0);
            LD.SafteyEnableGripSwitchOn1.XB_OnOff                          /**/= AddressMgr.GetAddress("#1_SAFETY_ENABLE_GRIP_SW_ON_FEEDBACK", 0);

            LD.Door01.XB_OnOff                                             /**/= AddressMgr.GetAddress("#1_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(L/D_MAINTENANCE_DOOR)", 0);
            LD.Door02.XB_OnOff                                             /**/= AddressMgr.GetAddress("#2_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(L/D_CASSETTE_LIFTER_DOOR)", 0);
            LD.Door03.XB_OnOff                                             /**/= AddressMgr.GetAddress("#3_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(PRE_ALIGN부_DOOR)", 0);

            LD.MCOff1.XB_OnOff                                           /**/= AddressMgr.GetAddress("L/D_MC#1_OFF_FEEDBACK", 0);
            LD.MCOff2.XB_OnOff                                           /**/= AddressMgr.GetAddress("L/D_MC#2_OFF_FEEDBACK", 0);

            LD.ModeSelectSwitchAuto.XB_OnOff                               /**/= AddressMgr.GetAddress("MODE_SELECT_SW_AUTO", 0);
            LD.ModeSelectSwitchTeach.XB_OnOff                              /**/= AddressMgr.GetAddress("MODE_SELECT_SW_TEACH", 0);

            LD.ControlSelectLD.XB_OnOff                                    /**/= AddressMgr.GetAddress("CONTROL_SELECT_L/D_FEEDBACK", 0);

            LD.Cell취출Hand출동방지.XB_OnOff                               /**/= AddressMgr.GetAddress("CELL_취출_HAND_충돌방지_센서", 0);
            LD.CstLoader_A.LightCurtain.ResetBtn.XB_OnOff            /**/= AddressMgr.GetAddress("L/D_A열_RESET_SWITCH_FEEDBACK", 0);
            LD.CstLoader_B.LightCurtain.ResetBtn.XB_OnOff            /**/= AddressMgr.GetAddress("L/D_B열_RESET_SWITCH_FEEDBACK", 0);

            LD.CstLoader_A.LightCurtain.XB_MutingOnOff                     /**/= AddressMgr.GetAddress("L/D_A열_투입/배출_MUTING_ON_FEEDBACK", 0);
            LD.CstLoader_B.LightCurtain.XB_MutingOnOff                     /**/= AddressMgr.GetAddress("L/D_B열_투입/배출_MUTING_ON_FEEDBACK", 0);

            LD.CstLoader_A.LDCstDetectOGamJi.XB_OnOff                      /**/= AddressMgr.GetAddress("#1-1_L/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK", 0);
            LD.CstLoader_A.LDCstDetectExist.XB_OnOff                       /**/= AddressMgr.GetAddress("#1-2_L/D_CASSETTE_DETECT_SENSOR_FEEDBACK", 0);

            LD.CstLoader_B.LDCstDetectOGamJi.XB_OnOff                      /**/= AddressMgr.GetAddress("#2-1_L/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK", 0);
            LD.CstLoader_B.LDCstDetectExist.XB_OnOff                       /**/= AddressMgr.GetAddress("#2-2_L/D_CASSETTE_DETECT_SENSOR_FEEDBACK", 0);

            LD.CstLoader_A.CstGripCylinder.XB_ForwardCompleteP1            /**/= AddressMgr.GetAddress("#1-1_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_A.CstGripCylinder.XB_BackwardCompleteP1           /**/= AddressMgr.GetAddress("#1-1_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_A.CstGripCylinder.XB_ForwardCompleteP2            /**/= AddressMgr.GetAddress("#1-2_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_A.CstGripCylinder.XB_BackwardCompleteP2           /**/= AddressMgr.GetAddress("#1-2_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_B.CstGripCylinder.XB_ForwardCompleteP1            /**/= AddressMgr.GetAddress("#2-1_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_B.CstGripCylinder.XB_BackwardCompleteP1           /**/= AddressMgr.GetAddress("#2-1_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_B.CstGripCylinder.XB_ForwardCompleteP2            /**/= AddressMgr.GetAddress("#2-2_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_B.CstGripCylinder.XB_BackwardCompleteP2           /**/= AddressMgr.GetAddress("#2-2_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);

            LD.LoaderTransfer_A.UpDown1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.LoaderTransfer_A.UpDown1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.LoaderTransfer_A.UpDown2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.LoaderTransfer_A.UpDown2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.LoaderTransfer_B.UpDown1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.LoaderTransfer_B.UpDown1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.LoaderTransfer_B.UpDown2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.LoaderTransfer_B.UpDown2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            LD.CstLoader_A.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_A.TiltCylinder.XB_ForwardComplete                 /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_B.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LD.CstLoader_B.TiltCylinder.XB_ForwardComplete                 /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            LD.MainCDAPressurSwitch1.XB_OnOff                               /**/= AddressMgr.GetAddress("#1_MAIN_CDA_PRESSURE_SW_FEEDBACK", 0);
            LD.MainCDAPressurSwitch2.XB_OnOff                               /**/= AddressMgr.GetAddress("#2_MAIN_CDA_PRESSURE_SW_FEEDBACK", 0);
            LD.MainCDAPressurSwitch3.XB_OnOff                               /**/= AddressMgr.GetAddress("#3_MAIN_CDA_PRESSURE_SW_FEEDBACK", 0);
            LD.MainVacuumPressurSwitch4.XB_OnOff                            /**/= AddressMgr.GetAddress("#4_MAIN_CDA_PRESSURE_SW_FEEDBACK", 0);

            LD.Loader.Vaccum1.XB_OnOff                                      /**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_PRESSURE_SW_FEEDBACK", 0);
            LD.Loader.Vaccum2.XB_OnOff                                      /**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_PRESSURE_SW_FEEDBACK", 0);

            LD.LoaderTransfer_A.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            LD.LoaderTransfer_A.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            LD.LoaderTransfer_B.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            LD.LoaderTransfer_B.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);

            LD.MainAirFlowmeter1.XB_OnOff                                   /**/= AddressMgr.GetAddress("#1_MAIN_AIR_FLOWMETER_PRESSURE_SW_FEEDBACK", 0);
            LD.MainAirFlowmeter2.XB_OnOff                                   /**/= AddressMgr.GetAddress("#2_MAIN_AIR_FLOWMETER_PRESSURE_SW_FEEDBACK", 0);
            LD.MainAirFlowmeter3.XB_OnOff                                   /**/= AddressMgr.GetAddress("#3_MAIN_AIR_FLOWMETER_PRESSURE_SW_FEEDBACK", 0);

            LD.DoorOpen01.XB_OnOff                                        /**/= AddressMgr.GetAddress("#1_SAFETY_DOOR_SW_OPEN_FEEDBACK_(L/D_MAINTENANCE_DOOR)", 0);
            LD.DoorOpen02.XB_OnOff                                        /**/= AddressMgr.GetAddress("#1_SAFETY_DOOR_SW_OPEN_FEEDBACK_(L/D_CASSETTE_LIFTER_DOOR)", 0);
            LD.DoorOpen03.XB_OnOff                                        /**/= AddressMgr.GetAddress("#1_SAFETY_DOOR_SW_OPEN_FEEDBACK_(PRE_ALIGN부_DOOR)", 0);

            LD.DoorKeyBox01.XB_OnOff                                        /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#1", 0);
            LD.DoorKeyBox02.XB_OnOff                                        /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#2", 0);
            LD.DoorKeyBox03.XB_OnOff                                        /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#3", 0);

            LD.CstLoader_A.LightCurtain.XB_Warning_Detect_BJub              /**/= AddressMgr.GetAddress("L/D_A_열_LIGHT_CURTAIN_감지", 0);
            LD.CstLoader_B.LightCurtain.XB_Warning_Detect_BJub              /**/= AddressMgr.GetAddress("L/D_B_열_LIGHT_CURTAIN_감지", 0);
            LD.OffDelayTimer감지.XB_OnOff                                   /**/= AddressMgr.GetAddress("OFF_DELAY_TIMER_감지", 0);
            LD.LDFanStopAlarm1.XB_OnOff                                     /**/= AddressMgr.GetAddress("#1_L/D_FAN_STOP_ALARM", 0);
            LD.LDFanStopAlarm2.XB_OnOff                                     /**/= AddressMgr.GetAddress("#2_L/D_FAN_STOP_ALARM", 0);
            LD.LDFanStopAlarm3.XB_OnOff                                     /**/= AddressMgr.GetAddress("#3_L/D_FAN_STOP_ALARM", 0);
            LD.LDFanStopAlarm4.XB_OnOff                                     /**/= AddressMgr.GetAddress("#4_L/D_FAN_STOP_ALARM", 0);
            LD.LDFanStopAlarm5.XB_OnOff                                     /**/= AddressMgr.GetAddress("#5_L/D_FAN_STOP_ALARM", 0);
            LD.LDFanStopAlarm6.XB_OnOff                                     /**/= AddressMgr.GetAddress("#6_L/D_FAN_STOP_ALARM", 0);
            LD.CtrlBoxOpenDetectBJub.XB_OnOff                                   /**/= AddressMgr.GetAddress("L/D_전장판넬_OPEN_감지", 0);
            LD.EFUAlarm.XB_OnOff                                            /**/= AddressMgr.GetAddress("EFU_ALARM_FEEDBACK", 0);
            LD.LDCpBoxFireAlarm.XB_OnOff                                    /**/= AddressMgr.GetAddress("L/D부_판넬_화재_알람", 0);
            LD.LDCpBoxTempAlarm.XB_OnOff                                    /**/= AddressMgr.GetAddress("L/D부_판넬_온도_알람", 0);
            //  LOADER IN END 
            #endregion

            #region LOADER OUT START
            // LOADER OUT START
            LD.LDMotorControlOn.YB_OnOff                                   /**/= AddressMgr.GetAddress("LD_MOTOR_CONTROL_ON_CMD", 0);
            LD.CpBoxResetSwitch.YB_OnOff                                   /**/= AddressMgr.GetAddress("CP_BOX_RESET_SWITCH_LAMP_CMD", 0);
            LD.CstLoader_A.LightCurtain.ResetBtnLamp.YB_OnOff            /**/= AddressMgr.GetAddress("L/D_A열_RESET_SWITCH_LAMP_CMD", 0);
            LD.CstLoader_B.LightCurtain.ResetBtnLamp.YB_OnOff            /**/= AddressMgr.GetAddress("L/D_B열_RESET_SWITCH_LAMP_CMD", 0);

            LD.CstLoader_A.LightCurtain.MutingBtnSwitch.YB_OnOff           /**/= AddressMgr.GetAddress("L/D_A열_CASSETTE_MUTING_SWITCH_LAMP_CMD", 0);
            LD.CstLoader_B.LightCurtain.MutingBtnSwitch.YB_OnOff           /**/= AddressMgr.GetAddress("L/D_B열_CASSETTE_MUTING_SWITCH_LAMP_CMD", 0);
            LD.SafteyReset.YB_OnOff                                        /**/= AddressMgr.GetAddress("안전회로_RESET", 0);

            LD.TowerLampR.YB_OnOff                                         /**/= AddressMgr.GetAddress("#1_TOWER_LAMP_R", 0);
            LD.TowerLampY.YB_OnOff                                         /**/= AddressMgr.GetAddress("#1_TOWER_LAMP_Y", 0);
            LD.TowerLampG.YB_OnOff                                         /**/= AddressMgr.GetAddress("#1_TOWER_LAMP_G", 0);
            LD.Buzzer.YB_OnOff                                             /**/= AddressMgr.GetAddress("SYSTEM_BUZZER", 0);
            LD.ControlSelectLD.YB_OnOff                                    /**/= AddressMgr.GetAddress("CONTROL_SELECT_L/D_CMD", 0);

            LD.CstLoader_A.CstInOutBuzzer.YB_OnOff                        /**/= AddressMgr.GetAddress("L/D_A열_CASSETTE_투입/배출_BUZZER", 0);
            LD.CstLoader_B.CstInOutBuzzer.YB_OnOff                        /**/= AddressMgr.GetAddress("L/D_B열_CASSETTE_투입/배출_BUZZER", 0);

            //LD.CstLoader_A.MutingLamp.YB_OnOff                          /**/= AddressMgr.GetAddress("L/D_A열_CASSETTE_투입/배출_MUTING_LAMP_CMD", 0);
            //LD.CstLoader_B.MutingLamp.YB_OnOff                          /**/= AddressMgr.GetAddress("L/D_B열_CASSETTE_투입/배출_MUTING_LAMP_CMD", 0);
            LD.ModelSelectSwAutoTeachLockingSol.YB_OnOff                  /**/= AddressMgr.GetAddress("MODE_SELECT_SW_AUTO/TEACH_LOCKING_SOL", 0);


            LD.CstLoader_A.CstGripCylinder.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_GRIP_CMD", 0);
            LD.CstLoader_A.CstGripCylinder.YB_BackwardCmd                 /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_UNGRIP_CMD", 0);

            LD.CstLoader_B.CstGripCylinder.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_GRIP_CMD", 0);
            LD.CstLoader_B.CstGripCylinder.YB_BackwardCmd                 /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_UNGRIP_CMD", 0);


            LD.LoaderTransfer_A.UpDown1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_UP_CMD", 0);
            LD.LoaderTransfer_A.UpDown1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_DOWN_CMD", 0);
            LD.LoaderTransfer_A.UpDown2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_UP_CMD", 0);
            LD.LoaderTransfer_A.UpDown2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_DOWN_CMD", 0);

            LD.LoaderTransfer_B.UpDown1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_UP_CMD", 0);
            LD.LoaderTransfer_B.UpDown1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_DOWN_CMD", 0);
            LD.LoaderTransfer_B.UpDown2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_UP_CMD", 0);
            LD.LoaderTransfer_B.UpDown2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_DOWN_CMD", 0);

            LD.CstLoader_A.TiltCylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            LD.CstLoader_A.TiltCylinder.YB_ForwardCmd                      /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);

            LD.CstLoader_B.TiltCylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            LD.CstLoader_B.TiltCylinder.YB_ForwardCmd                      /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);

            LD.Loader.Vaccum1.YB_On                                        /**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_VACUUM_ON_CMD", 0);
            LD.Loader.Vaccum1.YB_Off                                       /**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_VACUUM_OFF_CMD", 0);
            LD.Loader.Blower1.YB_OnOff                                     /**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_파기_AIR_ON_CMD", 0);

            LD.Loader.Vaccum2.YB_On                                        /**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_VACUUM_ON_CMD", 0);
            LD.Loader.Vaccum2.YB_Off                                       /**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_VACUUM_OFF_CMD", 0);
            LD.Loader.Blower2.YB_OnOff                                     /**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_파기_AIR_ON_CMD", 0);

            LD.LoaderTransfer_A.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            LD.LoaderTransfer_A.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_VACUUM_OFF_CMD", 0);
            LD.LoaderTransfer_A.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_파기_AIR_ON_CMD", 0);

            LD.LoaderTransfer_A.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            LD.LoaderTransfer_A.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_VACUUM_OFF_CMD", 0);
            LD.LoaderTransfer_A.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_파기_AIR_ON_CMD", 0);


            LD.LoaderTransfer_B.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            LD.LoaderTransfer_B.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_VACUUM_OFF_CMD", 0);
            LD.LoaderTransfer_B.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_파기_AIR_ON_CMD", 0);

            LD.LoaderTransfer_B.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            LD.LoaderTransfer_B.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_VACUUM_OFF_CMD", 0);
            LD.LoaderTransfer_B.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_파기_AIR_ON_CMD", 0);

            LD.음성Buzzer1.YB_OnOff                                        /**/= AddressMgr.GetAddress("음성_BUZZER_DOOR_CMD#1", 0);
            LD.음성Buzzer2.YB_OnOff                                        /**/= AddressMgr.GetAddress("음성_BUZZER_DOOR_CMD#2", 0);

            LD.Door01.YB_OnOff                                              /**/= AddressMgr.GetAddress("#1_SAFETY_DOOR_SW_OPEN_CMD", 0);
            LD.Door02.YB_OnOff                                              /**/= AddressMgr.GetAddress("#2_SAFETY_DOOR_SW_OPEN_CMD", 0);
            LD.Door03.YB_OnOff                                              /**/= AddressMgr.GetAddress("#3_SAFETY_DOOR_SW_OPEN_CMD", 0);

            LD.CstLoader_A.LightCurtain.YB_MutingOnOffCMD1                  /**/= AddressMgr.GetAddress("#1-1_L/D_A_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);
            LD.CstLoader_A.LightCurtain.YB_MutingOnOffCMD2                  /**/= AddressMgr.GetAddress("#1-2_L/D_A_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);
            LD.CstLoader_B.LightCurtain.YB_MutingOnOffCMD1                  /**/= AddressMgr.GetAddress("#2-1_L/D_B_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);
            LD.CstLoader_B.LightCurtain.YB_MutingOnOffCMD2                  /**/= AddressMgr.GetAddress("#2-2_L/D_B_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);

            LD.GpsMainMccbTrip.YB_OnOff                                     /**/= AddressMgr.GetAddress("GPS_MAIN_MCCB_TRIP_CMD", 0);
            LD.CstLoader_A.LightCurtain.YB_Reset                            /**/= AddressMgr.GetAddress("L/D_LIGHT_CURTAIN_A열_RESET", 0);
            LD.CstLoader_B.LightCurtain.YB_Reset                            /**/= AddressMgr.GetAddress("L/D_LIGHT_CURTAIN_B열_RESET", 0);

            LD.InnerLamp.YB_OnOff                                           /**/= AddressMgr.GetAddress("설비_내부_형광등_1_ON/OFF", 0);
            // LOADER OUT END 
            #endregion
            #region LOADER ANLOG
            // LOADER UNIT ANALOG VALUE START
            LD.Loader.HandPressureValue1                                    /**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_PRESSURE_DATA", 0);
            LD.Loader.HandPressureValue2                                    /**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_PRESSURE_DATA", 0);

            LD.LoaderTransfer_A.HandPressureValue1                         /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_PRESSURE_DATA", 0);
            LD.LoaderTransfer_A.HandPressureValue2                         /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_PRESSURE_DATA", 0);

            LD.LoaderTransfer_B.HandPressureValue1                         /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_PRESSURE_DATA", 0);
            LD.LoaderTransfer_B.HandPressureValue2                         /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_PRESSURE_DATA", 0);

            LD.MainCDAPressurValuie1                                        /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("#1_MAIN_CDA_PRESSURE_DATA", 0) };
            LD.MainCDAPressurValuie2                                        /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("#2_MAIN_CDA_PRESSURE_DATA", 0) };
            LD.MainCDAPressurValuie3                                        /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("#3_MAIN_CDA_PRESSURE_DATA", 0) };
            LD.MainAirFlowmeterValue1                                       /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("#1_MAIN_AIR_FLOWMETER_DATA", 0) };
            LD.MainAirFlowmeterValue2                                       /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("#2_MAIN_AIR_FLOWMETER_DATA", 0) };
            LD.MainAirFlowmeterValue3                                       /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("#3_MAIN_AIR_FLOWMETER_DATA", 0) };

            LD.PanelInnerTempValue                                          /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("L/D부_판넬_온도", 0) };
            // LOADER UNIT OUT END

            #endregion



            // PROCESS UNIT START
            PROC.MCOff1.XB_OnOff                                                     /**/= AddressMgr.GetAddress("가공부_MC#1_OFF_FEEDBACK", 0);
            PROC.MCOff2.XB_OnOff                                                     /**/= AddressMgr.GetAddress("가공부_MC#2_OFF_FEEDBACK", 0);

            PROC.EmergencyStopEnableGripSwitch.XB_OnOff                              /**/= AddressMgr.GetAddress("#2_EMERGENCY_STOP_FEEDBACK_ENABLE_GRIP_SW", 0);
            PROC.SafetyEnableGripSwitchOn.XB_OnOff                                   /**/= AddressMgr.GetAddress("#2_SAFETY_ENABLE_GRIP_SW_ON_FEEDBACK", 0);

            PROC.Door04.XB_OnOff                                                     /**/= AddressMgr.GetAddress("#4_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(CUTTING부_DOOR)", 0);
            PROC.Door05.XB_OnOff                                                     /**/= AddressMgr.GetAddress("#5_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(AFTER_IR_CUT_TR_DOOR)", 0);
            PROC.Door06.XB_OnOff                                                     /**/= AddressMgr.GetAddress("#6_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(BREAKING부_DOOR)", 0);
            PROC.Door07.XB_OnOff                                                     /**/= AddressMgr.GetAddress("#7_SAFETY_DOOR_SW_OPEN_FEEDBACK_(LASER_OPTIC)", 0);
            PROC.Door08.XB_OnOff                                                     /**/= AddressMgr.GetAddress("#8_SAFETY_DOOR_SW_OPEN_FEEDBACK_(LASER_HEAD_OPTIC)", 0);
            PROC.LaserAlarm.XB_OnOff                                                 /**/= AddressMgr.GetAddress("LASER_ALARM", 0);
            PROC.LaserOn.XB_OnOff                                                    /**/= AddressMgr.GetAddress("LASER_ON_FEEDBACK", 0);


            PROC.ProcEmergencyStop4.XB_OnOff                                         /**/= AddressMgr.GetAddress("#4_EMERGENCY_STOP_FEEDBACK_(가공부_CP-BOX)", 0);
            PROC.ChillerMCOff.XB_OnOff                                               /**/= AddressMgr.GetAddress("CHILLER_MC_OFF_FEEDBACK", 0);
            PROC.Safety이중화CP1.XB_OnOff                                            /**/= AddressMgr.GetAddress("SAFETY_이중화_CP#1", 0);
            PROC.Safety이중화CP2.XB_OnOff                                            /**/= AddressMgr.GetAddress("SAFETY_이중화_CP#2", 0);

            PROC.LaserHead.UpDown1Cylinder.XB_BackwardComplete                       /**/= AddressMgr.GetAddress("LASER_HEAD_UP_CYLINDER_SENSOR_FEEDBACK#1", 0);
            PROC.LaserHead.UpDown1Cylinder.XB_ForwardComplete                        /**/= AddressMgr.GetAddress("LASER_HEAD_DOWN_CYLINDER_SENSOR_FEEDBACK#1", 0);
            PROC.LaserHead.UpDown2Cylinder.XB_BackwardComplete                       /**/= AddressMgr.GetAddress("LASER_HEAD_UP_CYLINDER_SENSOR_FEEDBACK#2", 0);
            PROC.LaserHead.UpDown2Cylinder.XB_ForwardComplete                        /**/= AddressMgr.GetAddress("LASER_HEAD_DOWN_CYLINDER_SENSOR_FEEDBACK#2", 0);

            PROC.LaserLeak.XB_OnOff                                                  /**/= AddressMgr.GetAddress("LASER_LEAK_SENSOR", 0);
            PROC.ChillerLeak.XB_OnOff                                                  /**/= AddressMgr.GetAddress("CHILLER_LEAK_SENSOR", 0);
            PROC.BreakingHead.HeaderX1X2충돌방지.XB_OnOff                            /**/= AddressMgr.GetAddress("BREAKING_HEAD_X1_X2_충돌방지_센서", 0);
            PROC.DLMBlowerPressureSwitch.XB_OnOff                                    /**/= AddressMgr.GetAddress("DLM_BLOWER_PRESSURE_SW_FEEDBACK", 0);

            PROC.Process판넬화재Alarm.XB_OnOff                                       /**/= AddressMgr.GetAddress("가공부_판넬_화재_알람", 0);
            PROC.Process판넬온도Alarm.XB_OnOff                                       /**/= AddressMgr.GetAddress("가공부_판넬_온도_알람", 0);
            PROC.CpBoxOpenDetectBJub.XB_OnOff                                        /**/= AddressMgr.GetAddress("가공부_전장판넬_OPEN_감지", 0);

            PROC.DummyTankDetect.XB_OnOff                                            /**/= AddressMgr.GetAddress("DUMMY_TANK_DETECT", 0);


            PROC.LaserHead.LaserShutterCylinder.XB_BackwardComplete                  /**/= AddressMgr.GetAddress("LASER_SHUTTER_OPEN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.LaserHead.LaserShutterCylinder.XB_ForwardComplete                   /**/= AddressMgr.GetAddress("LASER_SHUTTER_CLOSE_CYLINDER_SENSOR_FEEDBACK", 0);

            PROC.IRCutStage_A.BrushUpDownCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#1_가공_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.IRCutStage_A.BrushUpDownCylinder.XB_ForwardComplete               /**/= AddressMgr.GetAddress("#1_가공_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.IRCutStage_B.BrushUpDownCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#2_가공_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.IRCutStage_B.BrushUpDownCylinder.XB_ForwardComplete               /**/= AddressMgr.GetAddress("#2_가공_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            PROC.AfterIRCutTransfer_A.UpDown1Cylinder.XB_BackwardComplete           /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_A.UpDown1Cylinder.XB_ForwardComplete            /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_A.UpDown2Cylinder.XB_BackwardComplete           /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_A.UpDown2Cylinder.XB_ForwardComplete            /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);


            PROC.AfterIRCutTransfer_B.UpDown1Cylinder.XB_BackwardComplete           /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_B.UpDown1Cylinder.XB_ForwardComplete            /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_B.UpDown2Cylinder.XB_BackwardComplete           /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_B.UpDown2Cylinder.XB_ForwardComplete            /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);


            PROC.BreakStage_A.BrushUpDownCylinder.XB_ForwardComplete               /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.BreakStage_A.BrushUpDownCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.BreakStage_B.BrushUpDownCylinder.XB_ForwardComplete               /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.BreakStage_B.BrushUpDownCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);



            PROC.LaserHead.DummyShutterCylinder1.XB_BackwardComplete                 /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_OPEN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.LaserHead.DummyShutterCylinder1.XB_ForwardComplete                  /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_CLOSE_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.LaserHead.DummyShutterCylinder2.XB_BackwardComplete                 /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_OPEN_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.LaserHead.DummyShutterCylinder2.XB_ForwardComplete                  /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_CLOSE_CYLINDER_SENSOR_FEEDBACK", 0);

            PROC.LaserHead.DummyTankCylinder.XB_BackwardComplete                     /**/= AddressMgr.GetAddress("DUMMY_TANK_투입_CYLINDER_SENSOR_FEEDBACK", 0);
            PROC.LaserHead.DummyTankCylinder.XB_ForwardComplete                      /**/= AddressMgr.GetAddress("DUMMY_TANK_배출_CYLINDER_SENSOR_FEEDBACK", 0);

            PROC.IRCutStage_A.Vacuum1Ch1.XB_OnOff                                  /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            PROC.IRCutStage_A.Vacuum1Ch2.XB_OnOff                                  /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);
            PROC.IRCutStage_A.Vacuum2Ch1.XB_OnOff                                  /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            PROC.IRCutStage_A.Vacuum2Ch2.XB_OnOff                                  /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);

            PROC.IRCutStage_B.Vacuum1Ch1.XB_OnOff                                  /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            PROC.IRCutStage_B.Vacuum1Ch2.XB_OnOff                                  /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);
            PROC.IRCutStage_B.Vacuum2Ch1.XB_OnOff                                  /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            PROC.IRCutStage_B.Vacuum2Ch2.XB_OnOff                                  /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);


            PROC.BreakStage_A.Vaccum1.XB_OnOff                                     /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);
            PROC.BreakStage_A.Vaccum2.XB_OnOff                                     /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);
            PROC.BreakStage_B.Vaccum1.XB_OnOff                                     /**/= AddressMgr.GetAddress("#3_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);
            PROC.BreakStage_B.Vaccum2.XB_OnOff                                     /**/= AddressMgr.GetAddress("#4_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);


            PROC.AfterIRCutTransfer_A.Vaccum1.XB_OnOff                             /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_A.Vaccum2.XB_OnOff                             /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_B.Vaccum1.XB_OnOff                             /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);
            PROC.AfterIRCutTransfer_B.Vaccum2.XB_OnOff                             /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);


            PROC.ProcessFanStopAlarm01.XB_OnOff                                     /**/= AddressMgr.GetAddress("#1_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm02.XB_OnOff                                     /**/= AddressMgr.GetAddress("#2_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm03.XB_OnOff                                     /**/= AddressMgr.GetAddress("#3_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm04.XB_OnOff                                     /**/= AddressMgr.GetAddress("#4_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm05.XB_OnOff                                     /**/= AddressMgr.GetAddress("#5_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm06.XB_OnOff                                     /**/= AddressMgr.GetAddress("#6_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm07.XB_OnOff                                     /**/= AddressMgr.GetAddress("#7_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm08.XB_OnOff                                     /**/= AddressMgr.GetAddress("#8_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm09.XB_OnOff                                     /**/= AddressMgr.GetAddress("#9_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm10.XB_OnOff                                     /**/= AddressMgr.GetAddress("#10_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm11.XB_OnOff                                     /**/= AddressMgr.GetAddress("#11_가공부_FAN_STOP_ALARM", 0);
            PROC.ProcessFanStopAlarm12.XB_OnOff                                     /**/= AddressMgr.GetAddress("#12_가공부_FAN_STOP_ALARM", 0);

            PROC.IonizerRun1.XB_OnOff                                               /**/= AddressMgr.GetAddress("#1_IONIZER_RUN_FEEDBACK", 0);
            PROC.IonizerAlarm1_1.XB_OnOff                                           /**/= AddressMgr.GetAddress("#1_IONIZER_ALARM_FEEDBACK", 0);
            PROC.IonizerAlarm1_2.XB_OnOff                                           /**/= AddressMgr.GetAddress("#1_IONIZER_ALARM_FEEDBACK", 0);
            PROC.IonizerRun2.XB_OnOff                                               /**/= AddressMgr.GetAddress("#2_IONIZER_RUN_FEEDBACK", 0);
            PROC.IonizerAlarm2_1.XB_OnOff                                           /**/= AddressMgr.GetAddress("#2_IONIZER_ALARM_FEEDBACK", 0);
            PROC.IonizerAlarm2_2.XB_OnOff                                           /**/= AddressMgr.GetAddress("#2_IONIZER_ALARM_FEEDBACK", 0);
            PROC.IonizerRun3.XB_OnOff                                               /**/= AddressMgr.GetAddress("#3_IONIZER_RUN_FEEDBACK", 0);
            PROC.IonizerAlarm3_1.XB_OnOff                                           /**/= AddressMgr.GetAddress("#3_IONIZER_ALARM_FEEDBACK", 0);
            PROC.IonizerAlarm3_2.XB_OnOff                                           /**/= AddressMgr.GetAddress("#3_IONIZER_ALARM_FEEDBACK", 0);
            PROC.IonizerRun4.XB_OnOff                                               /**/= AddressMgr.GetAddress("#4_IONIZER_RUN_FEEDBACK", 0);
            PROC.IonizerAlarm4_1.XB_OnOff                                           /**/= AddressMgr.GetAddress("#4_IONIZER_ALARM_FEEDBACK", 0);
            PROC.IonizerAlarm4_2.XB_OnOff                                           /**/= AddressMgr.GetAddress("#4_IONIZER_ALARM_FEEDBACK", 0);


            PROC.MotorControl.YB_OnOff                                              /**/= AddressMgr.GetAddress("가공부_MOTOR_CONTROL_ON_CMD", 0);

            PROC.LaserStopInternal.YB_OnOff                                         /**/= AddressMgr.GetAddress("LASER_STOP(INTERNAL_INTERLOCK)_CMD", 0);
            PROC.LaserStopExternal.YB_OnOff                                         /**/= AddressMgr.GetAddress("LASER_STOP(EXTERNAL_INTERLOCK)_CMD", 0);
            PROC.LaserEmergencyStop.YB_OnOff                                        /**/= AddressMgr.GetAddress("LASER_EMERGENCY_STOP", 0);



            PROC.LaserHead.LaserShutterCylinder.YB_BackwardCmd                      /**/= AddressMgr.GetAddress("LASER_SHUTTER_OPEN_CMD", 0);
            PROC.LaserHead.LaserShutterCylinder.YB_ForwardCmd                       /**/= AddressMgr.GetAddress("LASER_SHUTTER_CLOSE_CMD", 0);

            PROC.IRCutStage_A.BrushUpDownCylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_BRUSH_DOWN_CMD", 0);
            PROC.IRCutStage_A.BrushUpDownCylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_BRUSH_UP_CMD", 0);
            PROC.IRCutStage_B.BrushUpDownCylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_BRUSH_DOWN_CMD", 0);
            PROC.IRCutStage_B.BrushUpDownCylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_BRUSH_UP_CMD", 0);

            PROC.AfterIRCutTransfer_A.UpDown1Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            PROC.AfterIRCutTransfer_A.UpDown1Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_DOWN_CMD", 0);
            PROC.AfterIRCutTransfer_A.UpDown2Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            PROC.AfterIRCutTransfer_A.UpDown2Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_DOWN_CMD", 0);


            PROC.AfterIRCutTransfer_B.UpDown1Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            PROC.AfterIRCutTransfer_B.UpDown1Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_DOWN_CMD", 0);
            PROC.AfterIRCutTransfer_B.UpDown2Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            PROC.AfterIRCutTransfer_B.UpDown2Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_DOWN_CM", 0);


            PROC.BreakStage_A.BrushUpDownCylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_UP_CMD", 0);
            PROC.BreakStage_A.BrushUpDownCylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_DOWN_CMD", 0);
            PROC.BreakStage_B.BrushUpDownCylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_UP_CMD", 0);
            PROC.BreakStage_B.BrushUpDownCylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_DOWN_CMD", 0);


            PROC.LaserHead.DummyShutterCylinder1.YB_BackwardCmd                    /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_OPEN_CMD", 0);
            PROC.LaserHead.DummyShutterCylinder1.YB_ForwardCmd                     /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_CLOSE_CMD", 0);

            PROC.LaserHead.DummyShutterCylinder2.YB_BackwardCmd                    /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_OPEN_CMD", 0);
            PROC.LaserHead.DummyShutterCylinder2.YB_ForwardCmd                     /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_CLOSE_CMD", 0);

            PROC.LaserHead.DummyTankCylinder.YB_BackwardCmd                        /**/= AddressMgr.GetAddress("DUMMY_TANK_투입_CMD", 0);
            PROC.LaserHead.DummyTankCylinder.YB_ForwardCmd                         /**/= AddressMgr.GetAddress("DUMMY_TANK_배출_CMD", 0);

            PROC.LaserHead.UpDown1Cylinder.YB_BackwardCmd                          /**/= AddressMgr.GetAddress("LASER_HEAD_UP_CYLINDER_CMD", 0);
            PROC.LaserHead.UpDown1Cylinder.YB_ForwardCmd                           /**/= AddressMgr.GetAddress("LASER_HEAD_DOWN_CYLINDER_CMD", 0);


            PROC.IRCutStage_A.Vacuum1Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_A.Blower1Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            PROC.IRCutStage_A.Vacuum1Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_A.Blower1Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);

            PROC.IRCutStage_A.Vacuum2Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_A.Blower2Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            PROC.IRCutStage_A.Vacuum2Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_A.Blower2Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);


            PROC.IRCutStage_B.Vacuum1Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_B.Blower1Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            PROC.IRCutStage_B.Vacuum1Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_B.Blower1Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);

            PROC.IRCutStage_B.Vacuum2Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_B.Blower2Ch1.YB_OnOff                                /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            PROC.IRCutStage_B.Vacuum2Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            PROC.IRCutStage_B.Blower2Ch2.YB_OnOff                                /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);

            PROC.AfterIRCutTransfer_A.Vaccum1.YB_On                             /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);
            PROC.AfterIRCutTransfer_A.Vaccum1.YB_Off                            /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_VACUUM_OFF_CMD", 0);
            PROC.AfterIRCutTransfer_A.Blower1.YB_OnOff                          /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);

            PROC.AfterIRCutTransfer_A.Vaccum2.YB_On                             /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);
            PROC.AfterIRCutTransfer_A.Vaccum2.YB_Off                            /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_VACUUM_OFF_CMD", 0);
            PROC.AfterIRCutTransfer_A.Blower2.YB_OnOff                          /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);

            PROC.AfterIRCutTransfer_B.Vaccum1.YB_On                             /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);
            PROC.AfterIRCutTransfer_B.Vaccum1.YB_Off                            /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_VACUUM_OFF_CMD", 0);
            PROC.AfterIRCutTransfer_B.Blower1.YB_OnOff                          /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);

            PROC.AfterIRCutTransfer_B.Vaccum2.YB_On                             /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);
            PROC.AfterIRCutTransfer_B.Vaccum2.YB_Off                            /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_VACUUM_OFF_CMD", 0);
            PROC.AfterIRCutTransfer_B.Blower2.YB_OnOff                          /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);

            PROC.DLMBlowerPressureSwitch.YB_OnOff                                /**/= AddressMgr.GetAddress("DLM_BLOWER_ON/OFF_CMD", 0);

            PROC.BreakStage_A.Blower1.YB_OnOff                                  /**/= AddressMgr.GetAddress("#1_BREAKING_BLOWER_ON_CMD", 0);
            PROC.BreakStage_B.Blower1.YB_OnOff                                  /**/= AddressMgr.GetAddress("#2_BREAKING_BLOWER_ON_CMD", 0);


            PROC.BreakStage_A.Vaccum1.YB_OnOff                             /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            PROC.BreakStage_A.Blower1.YB_OnOff                             /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_파기_AIR_ON_CMD", 0);
            PROC.BreakStage_A.Vaccum2.YB_OnOff                             /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            PROC.BreakStage_A.Blower2.YB_OnOff                             /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_파기_AIR_ON_CMD", 0);

            PROC.BreakStage_B.Vaccum1.YB_OnOff                             /**/= AddressMgr.GetAddress("#3_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            PROC.BreakStage_B.Blower1.YB_OnOff                             /**/= AddressMgr.GetAddress("#3_BREAKING_STAGE_파기_AIR_ON_CMD", 0);
            PROC.BreakStage_B.Vaccum2.YB_OnOff                             /**/= AddressMgr.GetAddress("#4_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            PROC.BreakStage_B.Blower2.YB_OnOff                             /**/= AddressMgr.GetAddress("#4_BREAKING_STAGE_파기_AIR_ON_CMD", 0);


            PROC.Door04.YB_OnOff                                            /**/= AddressMgr.GetAddress("#4_SAFETY_DOOR_SW_OPEN_CMD", 0);
            PROC.Door05.YB_OnOff                                            /**/= AddressMgr.GetAddress("#5_SAFETY_DOOR_SW_OPEN_CMD", 0);
            PROC.Door06.YB_OnOff                                            /**/= AddressMgr.GetAddress("#6_SAFETY_DOOR_SW_OPEN_CMD", 0);
            PROC.Door07.YB_OnOff                                            /**/= AddressMgr.GetAddress("#7_#8_SAFETY_DOOR_SW_OPEN_CMD", 0);
            PROC.Door08.YB_OnOff                                            /**/= AddressMgr.GetAddress("#7_#8_SAFETY_DOOR_SW_OPEN_CMD", 0);
            PROC.InnerLamp.YB_OnOff                                         /**/= AddressMgr.GetAddress("설비_내부_형광등_2_ON/OFF", 0);
            PROC.LaserOnDisplayBoard.YB_OnOff                               /**/= AddressMgr.GetAddress("LASER_ON_전광판_ON/OFF", 0);

            PROC.LaserHead.LaserInterferometerReset1.YB_OnOff               /**/= AddressMgr.GetAddress("LASER_INTERFEROMETER(변위센서)_RESET", 0);
            PROC.LaserHead.LaserInterferometerReset2.YB_OnOff               /**/= AddressMgr.GetAddress("LASER_INTERFEROMETER(변위센서)_RESET", 0);

            // CHILLER_MC_ON_CMD
            PROC.EziServoY축Control.YB_OnOff                                /**/= AddressMgr.GetAddress("EZI_SERVO_Y축_CONTROL_OFF_CMD", 0);
            PROC.EziServoX축Control.YB_OnOff                                /**/= AddressMgr.GetAddress("EZI_SERVO_X축_CONTROL_OFF_CMD", 0);
            PROC.EziServoT축Control.YB_OnOff                                /**/= AddressMgr.GetAddress("EZI_SERVO_T축_CONTROL_OFF_CMD", 0);

            PROC.IonizerRunCmd1.YB_OnOff                                    /**/= AddressMgr.GetAddress("#1_IONIZER_RUN_CMD", 0);
            PROC.IonizerAirRunCmd1.YB_OnOff                                 /**/= AddressMgr.GetAddress("#1_IONIZER_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd1.YB_OnOff                                /**/= AddressMgr.GetAddress("#1_IONIZER_AIR_STOP_CMD_SOL", 0);

            PROC.IonizerRunCmd2.YB_OnOff                                    /**/= AddressMgr.GetAddress("#2_IONIZER_RUN_CMD", 0);
            PROC.IonizerAirRunCmd2.YB_OnOff                                 /**/= AddressMgr.GetAddress("#2_IONIZER_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd2.YB_OnOff                                /**/= AddressMgr.GetAddress("#2_IONIZER_AIR_STOP_CMD_SOL", 0);

            PROC.IonizerRunCmd3.YB_OnOff                                    /**/= AddressMgr.GetAddress("#3_IONIZER_RUN_CMD", 0);
            PROC.IonizerAirRunCmd3.YB_OnOff                                 /**/= AddressMgr.GetAddress("#3_IONIZER_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd3.YB_OnOff                                /**/= AddressMgr.GetAddress("#3_IONIZER_AIR_STOP_CMD_SOL", 0);

            PROC.IonizerRunCmd4.YB_OnOff                                    /**/= AddressMgr.GetAddress("#4_IONIZER_RUN_CMD", 0);
            PROC.IonizerAirRunCmd4.YB_OnOff                                 /**/= AddressMgr.GetAddress("#4_IONIZER_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd4.YB_OnOff                                /**/= AddressMgr.GetAddress("#4_IONIZER_AIR_STOP_CMD_SOL", 0);

            PROC.DoorKeyBox04.XB_OnOff                                      /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#4", 0);
            PROC.DoorKeyBox05.XB_OnOff                                      /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#5", 0);
            PROC.DoorKeyBox06.XB_OnOff                                      /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#6", 0);

            PROC.DoorOpen04.XB_OnOff                                      /**/= AddressMgr.GetAddress("#4_SAFETY_DOOR_SW_OPEN_FEEDBACK_(CUTTING부_DOOR)", 0);
            PROC.DoorOpen05.XB_OnOff                                      /**/= AddressMgr.GetAddress("#5_SAFETY_DOOR_SW_OPEN_FEEDBACK_(AFTER_IR_CUT_T/R_DOOR)", 0);
            PROC.DoorOpen06.XB_OnOff                                      /**/= AddressMgr.GetAddress("#6_SAFETY_DOOR_SW_OPEN_FEEDBACK_(BREAKING부_DOOR)", 0);



            /////////////////////////////////////////////////////////////////////////////

            PROC.IRCutStage_A.Vacuum1Ch1Value                            /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_1CH_PRESSURE_DATA", 0);
            PROC.IRCutStage_A.Vacuum1Ch2Value                            /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_2CH_PRESSURE_DATA", 0);
            PROC.IRCutStage_A.Vacuum2Ch1Value                            /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_1CH_PRESSURE_DATA", 0);
            PROC.IRCutStage_A.Vacuum2Ch2Value                            /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_2CH_PRESSURE_DATA", 0);

            PROC.IRCutStage_B.Vacuum1Ch1Value                            /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_1CH_PRESSURE_DATA", 0);
            PROC.IRCutStage_B.Vacuum1Ch2Value                            /**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_2CH_PRESSURE_DATA", 0);
            PROC.IRCutStage_B.Vacuum2Ch1Value                            /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_1CH_PRESSURE_DATA", 0);
            PROC.IRCutStage_B.Vacuum2Ch2Value                            /**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_2CH_PRESSURE_DATA", 0);

            PROC.BreakStage_A.Vaccum1Value                                 /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_PRESSURE_DATA", 0);
            PROC.BreakStage_A.Vaccum2Value                                 /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_PRESSURE_DATA", 0);
            PROC.BreakStage_B.Vaccum1Value                                 /**/= AddressMgr.GetAddress("#3_BREAKING_STAGE_PRESSURE_DATA", 0);
            PROC.BreakStage_B.Vaccum2Value                                 /**/= AddressMgr.GetAddress("#4_BREAKING_STAGE_PRESSURE_DATA", 0);

            PROC.AfterIRCutTransfer_A.Vaccum1Value                              /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_PRESSURE_DATA", 0);
            PROC.AfterIRCutTransfer_A.Vaccum2Value                              /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_PRESSURE_DATA", 0);
            PROC.AfterIRCutTransfer_B.Vaccum1Value                              /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_PRESSURE_DATA", 0);
            PROC.AfterIRCutTransfer_B.Vaccum2Value                              /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_PRESSURE_DATA", 0);

            PROC.LaserHead.LaserInterferometerIRValue                       /**/= AddressMgr.GetAddress("LASER_INTERFEROMETER(변위센서)_IR", 0);
            PROC.LaserHead.LaserInterferometerBKValue                       /**/= AddressMgr.GetAddress("LASER_INTERFEROMETER(변위센서)_BK", 0);

            PROC.DLMBlowerPressureValue                                     /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("DLM_BLOWER_PRESSURE_DATA", 0) };
            PROC.PanelInnerTempValue                                        /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("가공부_판넬_온도", 0) };

            // PROCESS UNIT END


            //181118 HANEW 수정
            // UNLOAD UNIT START
            UD.CstUnloader_A.LightCurtain.MutingBtnSwitch.XB_OnOff           /**/= AddressMgr.GetAddress("UL/D_A열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK", 0);
            UD.CstUnloader_B.LightCurtain.MutingBtnSwitch.XB_OnOff           /**/= AddressMgr.GetAddress("UL/D_B열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK", 0);

            UD.CstUnloader_A.LightCurtain.XB_MutingOnOff                            /**/= AddressMgr.GetAddress("UL/D_A열_투입/배출_MUTING_ON_FEEDBACK", 0);
            UD.CstUnloader_B.LightCurtain.XB_MutingOnOff                            /**/= AddressMgr.GetAddress("UL/D_B열_투입/배출_MUTING_ON_FEEDBACK", 0);
            //확인 필요.                                                     

            UD.DoorKeyBox07.XB_OnOff                                                /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#7", 0);
            UD.DoorKeyBox08.XB_OnOff                                                /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#8", 0);
            UD.DoorKeyBox09.XB_OnOff                                                /**/= AddressMgr.GetAddress("DOOR_KEY_BOX_FEEDBACK#9", 0);


            UD.ULDEmergencyStop5.XB_OnOff                                           /**/= AddressMgr.GetAddress("#5_EMERGENCY_STOP_FEEDBACK_(UL/D_제어_모니터_부)", 0);
            UD.ULDEmergencyStop6.XB_OnOff                                           /**/= AddressMgr.GetAddress("#6_EMERGENCY_STOP_FEEDBACK_(UL/D_CASSETTE_투입_부)", 0);
            UD.ULDEmergencyStop7.XB_OnOff                                           /**/= AddressMgr.GetAddress("#7_EMERGENCY_STOP_FEEDBACK_(UL/D_CP-BOX)", 0);
            UD.EmergencyStopEnalbeGripSwitch3.XB_OnOff                              /**/= AddressMgr.GetAddress("#3_EMERGENCY_STOP_FEEDBACK_ENABLE_GRIP_SW", 0);
            UD.SafteyEnableGripSwitchOn3.XB_OnOff                                   /**/= AddressMgr.GetAddress("#3_SAFETY_ENABLE_GRIP_SW_ON_FEEDBACK", 0);

            UD.Door09.XB_OnOff                                                      /**/= AddressMgr.GetAddress("#9_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(검사_카메라_DOOR)", 0);
            UD.Door10.XB_OnOff                                                      /**/= AddressMgr.GetAddress("#10_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(UL/D_CASSETTE_LIFTER_DOOR)", 0);
            UD.Door11.XB_OnOff                                                      /**/= AddressMgr.GetAddress("#11_SAFETY_DOOR_LOCK_RELEASE_FEEDBACK_(UL/D_MAINTENANCE_DOOR)", 0);

            UD.MCOff1.XB_OnOff                                                   /**/= AddressMgr.GetAddress("UL/D_MC#1_OFF_FEEDBACK", 0);
            UD.MCOff2.XB_OnOff                                                   /**/= AddressMgr.GetAddress("UL/D_MC#2_OFF_FEEDBACK", 0);

            UD.ControlSelectULD.XB_OnOff                                            /**/= AddressMgr.GetAddress("CONTROL_SELECT_UL/D_FEEDBACK", 0);

            UD.CstUnloader_A.LightCurtain.XB_Warning_Detect_BJub                    /**/= AddressMgr.GetAddress("UL/D_A_열_LIGHT_CURTAIN_감지", 0);
            UD.CstUnloader_B.LightCurtain.XB_Warning_Detect_BJub                    /**/= AddressMgr.GetAddress("UL/D_B_열_LIGHT_CURTAIN_감지", 0);

            UD.CstUnloader_A.LightCurtain.ResetBtn.XB_OnOff                   /**/= AddressMgr.GetAddress("UL/D_A열_RESET_SWITCH_FEEDBACK", 0);
            UD.CstUnloader_B.LightCurtain.ResetBtn.XB_OnOff                   /**/= AddressMgr.GetAddress("UL/D_B열_RESET_SWITCH_FEEDBACK", 0);

            UD.Cell투입Hand출동방지.XB_OnOff                                          /**/= AddressMgr.GetAddress("CELL_투입_HAND_충돌방지_센서", 0);

            UD.ULDIRCUT충돌방지Y1_Y3.XB_OnOff                                         /**/= AddressMgr.GetAddress("UL/D_A열_CELL_T/R_충돌방지_센서(Y1-Y3)", 0);
            UD.ULDIRCUT충돌방지Y2_Y4.XB_OnOff                                         /**/= AddressMgr.GetAddress("UL/D_B열_CELL_T/R_충돌방지_센서(Y2-Y4)", 0);
            UD.CellBuffer.Vaccum1.XB_OnOff                                            /**/= AddressMgr.GetAddress("BUFFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.CellBuffer.BufferCylinder.XB_BackwardComplete                          /**/= AddressMgr.GetAddress("BUFFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CellBuffer.BufferCylinder.XB_ForwardComplete                           /**/= AddressMgr.GetAddress("BUFFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            UD.CstUnloader_A.ULDCstDetectOGamJi.XB_OnOff                              /**/= AddressMgr.GetAddress("#1-1_UL/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_A.ULDCstDetectExist.XB_OnOff                               /**/= AddressMgr.GetAddress("#1-2_UL/D_CASSETTE_DETECT_SENSOR_FEEDBACK", 0);


            UD.CstUnloader_B.ULDCstDetectOGamJi.XB_OnOff                              /**/= AddressMgr.GetAddress("#2-1_UL/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_B.ULDCstDetectExist.XB_OnOff                               /**/= AddressMgr.GetAddress("#2-2_UL/D_CASSETTE_DETECT_SENSOR_FEEDBACK", 0);

            UD.CstUnloader_A.CstGripCylinder.XB_ForwardCompleteP1                      /**/= AddressMgr.GetAddress("#1-1_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_A.CstGripCylinder.XB_BackwardCompleteP1                     /**/= AddressMgr.GetAddress("#1-1_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_A.CstGripCylinder.XB_ForwardCompleteP2                      /**/= AddressMgr.GetAddress("#1-2_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_A.CstGripCylinder.XB_BackwardCompleteP2                     /**/= AddressMgr.GetAddress("#1-2_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_B.CstGripCylinder.XB_ForwardCompleteP1                      /**/= AddressMgr.GetAddress("#2-1_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_B.CstGripCylinder.XB_BackwardCompleteP1                     /**/= AddressMgr.GetAddress("#2-1_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_B.CstGripCylinder.XB_ForwardCompleteP2                      /**/= AddressMgr.GetAddress("#2-2_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_B.CstGripCylinder.XB_BackwardCompleteP2                     /**/= AddressMgr.GetAddress("#2-2_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);

            UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            UD.CstUnloader_A.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_A.TiltCylinder.XB_ForwardComplete                 /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_B.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.CstUnloader_B.TiltCylinder.XB_ForwardComplete                /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            UD.Unloader.Vaccum1.XB_OnOff                                              /**/= AddressMgr.GetAddress("#1_CELL_투입_HAND_PRESSURE_SW_FEEDBACK", 0);
            UD.Unloader.Vaccum2.XB_OnOff                                              /**/= AddressMgr.GetAddress("#2_CELL_투입_HAND_PRESSURE_SW_FEEDBACK", 0);

            UD.BeforeInspUnloaderTransfer_A.Vaccum1.XB_OnOff                          /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_A.Vaccum2.XB_OnOff                          /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_B.Vaccum1.XB_OnOff                          /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.BeforeInspUnloaderTransfer_B.Vaccum2.XB_OnOff                          /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_A.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_A.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_B.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            UD.AfterInspUnloaderTransfer_B.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);

            UD.InspectionStage_A.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("#1_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);
            UD.InspectionStage_A.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("#2_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);
            UD.InspectionStage_B.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("#3_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);
            UD.InspectionStage_B.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("#4_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);

            UD.CpBoxOpenDetectBJub.XB_OnOff                                      /**/= AddressMgr.GetAddress("UL/D_전장판넬_OPEN_감지", 0);
            UD.ULDFanStopAlarm1.XB_OnOff                                     /**/= AddressMgr.GetAddress("#1_UL/D_FAN_STOP_ALARM", 0);
            UD.ULDFanStopAlarm2.XB_OnOff                                     /**/= AddressMgr.GetAddress("#2_UL/D_FAN_STOP_ALARM", 0);
            UD.ULDFanStopAlarm3.XB_OnOff                                     /**/= AddressMgr.GetAddress("#3_UL/D_FAN_STOP_ALARM", 0);
            UD.ULDFanStopAlarm4.XB_OnOff                                     /**/= AddressMgr.GetAddress("#4_UL/D_FAN_STOP_ALARM", 0);
            UD.ULDFanStopAlarm5.XB_OnOff                                     /**/= AddressMgr.GetAddress("#5_UL/D_FAN_STOP_ALARM", 0);
            UD.ULDFanStopAlarm6.XB_OnOff                                     /**/= AddressMgr.GetAddress("#6_UL/D_FAN_STOP_ALARM", 0);
            UD.ULD판넬화재알람.XB_OnOff                                       /**/= AddressMgr.GetAddress("UL/D부_판넬_화재_알람", 0);
            UD.ULD판넬온도알람.XB_OnOff                                       /**/= AddressMgr.GetAddress("UL/D부_판넬_온도_알람", 0);
            UD.PCRack온도알람.XB_OnOff                                       /**/= AddressMgr.GetAddress("PC_RACK_온도_알람", 0);
            UD.PCRack화재알람.XB_OnOff                                       /**/= AddressMgr.GetAddress("PC_RACK_화재_알람", 0);

            UD.DoorOpen09.XB_OnOff                                     /**/= AddressMgr.GetAddress("#9_SAFETY_DOOR_SW_OPEN_FEEDBACK_(검사_카메라_DOOR)", 0);
            UD.DoorOpen10.XB_OnOff                                       /**/= AddressMgr.GetAddress("#10_SAFETY_DOOR_SW_OPEN_FEEDBACK_(UL/D_CASSETTE_LIFTER_DOOR)", 0);
            UD.DoorOpen11.XB_OnOff                                       /**/= AddressMgr.GetAddress("#11_SAFETY_DOOR_SW_OPEN_FEEDBACK_(UL/D_MAINTENANCE_DOOR)", 0);






            //OUT 
            UD.ULDMotorControlOn.YB_OnOff                                   /**/= AddressMgr.GetAddress("UL/D_MOTOR_CONTROL_ON_CMD", 0);
            UD.Buzzer.YB_OnOff                                              /**/= AddressMgr.GetAddress("UL/D_SYSTEM_BUZZER", 0);

            UD.CstUnloader_A.LightCurtain.MutingBtnSwitch.YB_OnOff                    /**/= AddressMgr.GetAddress("UL/D_A열_CASSETTE_MUTING_SWITCH_LAMP_CMD", 0);
            UD.CstUnloader_B.LightCurtain.MutingBtnSwitch.YB_OnOff                    /**/= AddressMgr.GetAddress("UL/D_B열_CASSETTE_MUTING_SWITCH_LAMP_CMD", 0);

            UD.TowerLampR.YB_OnOff                                                    /**/= AddressMgr.GetAddress("#2_TOWER_LAMP_R", 0);
            UD.TowerLampY.YB_OnOff                                                    /**/= AddressMgr.GetAddress("#2_TOWER_LAMP_Y", 0);
            UD.TowerLampG.YB_OnOff                                                    /**/= AddressMgr.GetAddress("#2_TOWER_LAMP_G", 0);

            UD.ControlSelectULDCMD.YB_OnOff                                           /**/= AddressMgr.GetAddress("CONTROL_SELECT_UL/D_CMD", 0);

            UD.CstUnloader_A.CstInOutBuzzer.YB_OnOff                                  /**/= AddressMgr.GetAddress("UL/D_A열_CASSETTE_투입/배출_대기_BUZZER", 0);
            UD.CstUnloader_B.CstInOutBuzzer.YB_OnOff                                  /**/= AddressMgr.GetAddress("UL/D_B열_CASSETTE_투입/배출_대기_BUZZER", 0);

            UD.CstUnloader_A.LightCurtain.ResetBtnLamp.YB_OnOff                     /**/= AddressMgr.GetAddress("UL/D_A열_RESET_SWITCH_LAMP_CMD", 0);
            UD.CstUnloader_B.LightCurtain.ResetBtnLamp.YB_OnOff                     /**/= AddressMgr.GetAddress("UL/D_B열_RESET_SWITCH_LAMP_CMD", 0);

            UD.CstUnloader_A.MutingLamp.YB_OnOff                                      /**/= AddressMgr.GetAddress("UL/D_A열_CASSETTE_MUTING_LAMP_CMD", 0);
            UD.CstUnloader_B.MutingLamp.YB_OnOff                                      /**/= AddressMgr.GetAddress("UL/D_B열_CASSETTE_MUTING_LAMP_CMD", 0);


            UD.CellBuffer.BufferCylinder.YB_BackwardCmd                               /**/= AddressMgr.GetAddress("BUFFER_UP_CMD", 0);
            UD.CellBuffer.BufferCylinder.YB_ForwardCmd                                /**/= AddressMgr.GetAddress("BUFFER_DOWN_CMD", 0);

            UD.CstUnloader_A.CstGripCylinder.YB_ForwardCmd                            /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_GRIP_CMD", 0);
            UD.CstUnloader_A.CstGripCylinder.YB_BackwardCmd                           /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_UNGRIP_CMD", 0);

            UD.CstUnloader_B.CstGripCylinder.YB_ForwardCmd                            /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_GRIP_CMD", 0);
            UD.CstUnloader_B.CstGripCylinder.YB_BackwardCmd                           /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_UNGRIP_CMD", 0);


            UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_UP_CMD", 0);
            UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_DOWN_CMD", 0);
            UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_UP_CMD", 0);
            UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_DOWN_CMD", 0);

            UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_UP_CMD", 0);
            UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_DOWN_CMD", 0);
            UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_UP_CMD", 0);
            UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_DOWN_CMD", 0);

            UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_UP_CMD", 0);
            UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_DOWN_CMD", 0);
            UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_UP_CMD", 0);
            UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_DOWN_CMD", 0);

            UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_UP_CMD", 0);
            UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_DOWN_CMD", 0);
            UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_UP_CMD", 0);
            UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_DOWN_CMD", 0);

            UD.CstUnloader_A.TiltCylinder.YB_BackwardCmd                               /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            UD.CstUnloader_A.TiltCylinder.YB_ForwardCmd                              /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);

            UD.CstUnloader_B.TiltCylinder.YB_BackwardCmd                               /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            UD.CstUnloader_B.TiltCylinder.YB_ForwardCmd                              /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);

            UD.Unloader.Vaccum1.YB_On                                                 /**/= AddressMgr.GetAddress("#1_CELL_투입_HAND_VACUUM_ON_CMD", 0);
            UD.Unloader.Vaccum1.YB_Off                                                /**/= AddressMgr.GetAddress("#1_CELL_투입_HAND_VACUUM_OFF_CMD", 0);
            UD.Unloader.Blower1.YB_OnOff                                              /**/= AddressMgr.GetAddress("#1_CELL_투입_HAND_파기_AIR_ON_CMD", 0);

            UD.Unloader.Vaccum2.YB_On                                                 /**/= AddressMgr.GetAddress("#2_CELL_투입_HAND_VACUUM_ON_CMD", 0);
            UD.Unloader.Vaccum2.YB_Off                                                /**/= AddressMgr.GetAddress("#2_CELL_투입_HAND_VACUUM_OFF_CMD", 0);
            UD.Unloader.Blower2.YB_OnOff                                              /**/= AddressMgr.GetAddress("#2_CELL_투입_HAND_파기_AIR_ON_CMD", 0);

            UD.BeforeInspUnloaderTransfer_A.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.BeforeInspUnloaderTransfer_A.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.BeforeInspUnloaderTransfer_A.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);

            UD.BeforeInspUnloaderTransfer_A.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.BeforeInspUnloaderTransfer_A.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.BeforeInspUnloaderTransfer_A.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);


            UD.BeforeInspUnloaderTransfer_B.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.BeforeInspUnloaderTransfer_B.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.BeforeInspUnloaderTransfer_B.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);

            UD.BeforeInspUnloaderTransfer_B.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.BeforeInspUnloaderTransfer_B.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.BeforeInspUnloaderTransfer_B.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);

            UD.AfterInspUnloaderTransfer_A.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.AfterInspUnloaderTransfer_A.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.AfterInspUnloaderTransfer_A.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);

            UD.AfterInspUnloaderTransfer_A.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.AfterInspUnloaderTransfer_A.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.AfterInspUnloaderTransfer_A.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);

            UD.AfterInspUnloaderTransfer_B.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.AfterInspUnloaderTransfer_B.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.AfterInspUnloaderTransfer_B.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);

            UD.AfterInspUnloaderTransfer_B.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            UD.AfterInspUnloaderTransfer_B.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_VACUUM_OFF_CMD", 0);
            UD.AfterInspUnloaderTransfer_B.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);


            UD.InspectionStage_A.Vaccum1.YB_On                                        /**/= AddressMgr.GetAddress("#1_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            UD.InspectionStage_A.Vaccum1.YB_Off                                       /**/= AddressMgr.GetAddress("#1_INSPECTION_STAGE_VACUUM_OFF_CMD", 0);
            UD.InspectionStage_A.Blower1.YB_OnOff                                     /**/= AddressMgr.GetAddress("#1_INSPECTION_STAGE_파기_AIR_ON_CMD", 0);

            UD.InspectionStage_A.Vaccum2.YB_On                                        /**/= AddressMgr.GetAddress("#2_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            UD.InspectionStage_A.Vaccum2.YB_Off                                       /**/= AddressMgr.GetAddress("#2_INSPECTION_STAGE_VACUUM_OFF_CMD", 0);
            UD.InspectionStage_A.Blower2.YB_OnOff                                     /**/= AddressMgr.GetAddress("#2_INSPECTION_STAGE_파기_AIR_ON_CMD", 0);

            UD.InspectionStage_B.Vaccum1.YB_On                                        /**/= AddressMgr.GetAddress("#3_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            UD.InspectionStage_B.Vaccum1.YB_Off                                       /**/= AddressMgr.GetAddress("#3_INSPECTION_STAGE_VACUUM_OFF_CMD", 0);
            UD.InspectionStage_B.Blower1.YB_OnOff                                     /**/= AddressMgr.GetAddress("#3_INSPECTION_STAGE_파기_AIR_ON_CMD", 0); ;

            UD.InspectionStage_B.Vaccum2.YB_On                                        /**/= AddressMgr.GetAddress("#4_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            UD.InspectionStage_B.Vaccum2.YB_Off                                       /**/= AddressMgr.GetAddress("#4_INSPECTION_STAGE_VACUUM_OFF_CMD", 0);
            UD.InspectionStage_B.Blower2.YB_OnOff                                     /**/= AddressMgr.GetAddress("#4_INSPECTION_STAGE_파기_AIR_ON_CMD", 0);

            UD.CellBuffer.Vaccum1.YB_On                                               /**/= AddressMgr.GetAddress("BUFFER_1CH_VACUUM_ON_CMD", 0);
            UD.CellBuffer.Vaccum1.YB_Off                                              /**/= AddressMgr.GetAddress("BUFFER_1CH_VACUUM_OFF_CMD", 0);
            UD.CellBuffer.Blower1.YB_OnOff                                            /**/= AddressMgr.GetAddress("BUFFER_1CH_파기_AIR_ON_CMD", 0);

            UD.Door09.YB_OnOff                                                        /**/= AddressMgr.GetAddress("#9_SAFETY_DOOR_SW_OPEN_CMD", 0);
            UD.Door10.YB_OnOff                                                        /**/= AddressMgr.GetAddress("#10_SAFETY_DOOR_SW_OPEN_CMD", 0);
            UD.Door11.YB_OnOff                                                        /**/= AddressMgr.GetAddress("#11_SAFETY_DOOR_SW_OPEN_CMD", 0);

            UD.CstUnloader_A.LightCurtain.YB_MutingOnOffCMD1                          /**/= AddressMgr.GetAddress("#1-1_UL/D_A_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);
            UD.CstUnloader_A.LightCurtain.YB_MutingOnOffCMD2                          /**/= AddressMgr.GetAddress("#1-2_UL/D_A_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);
            UD.CstUnloader_B.LightCurtain.YB_MutingOnOffCMD1                          /**/= AddressMgr.GetAddress("#2-1_UL/D_B_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);
            UD.CstUnloader_B.LightCurtain.YB_MutingOnOffCMD2                          /**/= AddressMgr.GetAddress("#2-2_UL/D_B_열_투입/배출_SAFETY_MUTING_ON/OFF_CMD", 0);

            UD.CstUnloader_A.LightCurtain.YB_Reset                                    /**/= AddressMgr.GetAddress("UL/D_LIGHT_CURTAIN_A열_RESET", 0);
            UD.CstUnloader_B.LightCurtain.YB_Reset                                    /**/= AddressMgr.GetAddress("UL/D_LIGHT_CURTAIN_B열_RESET", 0);

            UD.InnerLamp.YB_OnOff                                                     /**/= AddressMgr.GetAddress("설비_내부_형광등_3_ON/OFF", 0);

            UD.음성Buzzer.YB_OnOff                                                    /**/= AddressMgr.GetAddress("음성_BUZZER_DOOR_CMD#3", 0);
            UD.음성Buzzer.YB_OnOff                                                    /**/= AddressMgr.GetAddress("음성_BUZZER_DOOR_CMD#4", 0);
            //UNLOAD UNIT END 


            UD.PanelInnerTempValue                                                  /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("UL/D부_판넬_온도", 0) };
            UD.PcBoxTempValue                                                       /**/= new ConvertA2D() { XF_Value = AddressMgr.GetAddress("PC_RACK_온도", 0) };
        }

        //설비 투입 Flow
        public EmUnitPosi GetProcssFlow(PortInfo port)
        {
            return port.Cst.OutCount % 2 == 0 ? EmUnitPosi.ACol : EmUnitPosi.BCol;
        }

        public bool ChangeMode(EmEquipRunMode mode)
        {
            if (mode == EmEquipRunMode.Start)
            {
                if (LD.ModeSelectSwitchAuto.XB_OnOff == false)
                {
                    InterLockMgr.AddInterLock("TEACH 모드에서 시작이 불가 합니다.");
                    return false;
                }
            }

            RunMode = mode;
            return true;
        }

        public void LoadSetting()
        {
            SettingMgr.Load();
            EqpRecipeMgr.Load();
            BreakRecipeMgr.Load();
            ProcessRecipeMgr.Load();
            UserInfoMgr.Load();

            EqpSysParamMgr.Load();
            EqpSysMgr.Load();
        }


        public void LogicWorking()
        {
            if (GG.TestMode == false)
            {
                VirtualUMacAsync umac = GG.UMAC as VirtualUMacAsync;
                VirtualAjinDirect ajin = GG.AJIN as VirtualAjinDirect;

                foreach (PlcAddr addr in umac.LstReadAddr) GG.UMAC.ReadFromPLC(addr, addr.Length);
                //foreach (PlcAddr addr in ajin.LstReadAddr) GG.AJIN.ReadFromPLC(addr, addr.Length);

                SafetyMgr.LogicWorking(this);

                LD.LogicWorking(this);
                PROC.LogicWorking(this);
                UD.LogicWorking(this);
                EquipStatusCheck();

                foreach (PlcAddr addr in umac.LstWriteAddr) GG.UMAC.WriteToPLC(addr, addr.Length);
                //foreach (PlcAddr addr in ajin.LstWriteAddr) GG.AJIN.WriteToPLC(addr, addr.Length);
            }
            else
            {

                SafetyMgr.LogicWorking(this);

                LD.LogicWorking(this);
                PROC.LogicWorking(this);
                UD.LogicWorking(this);
                EquipStatusCheck();


            }
        }

        private void EquipStatusCheck()
        {
            //if (IsHeavyAlarm == true)
            //    ChangeMode(EmEquipRunMode.Pause);

            if (IsDoorRelease == false)
                IsInnerWorkOnClose = false;
        }
    }
}
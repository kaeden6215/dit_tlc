﻿using Dit.Framework.Comm;
using Dit.Framework.PLC;
using DIT.TLC.CTRL._050._Struct._520._Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace DIT.TLC.CTRL
{
    public class LoaderModule
    {
        public CstLoaderUint CstLoader_B { get; set; }
        public CstLoaderUint CstLoader_A { get; set; }

        public LoaderUnit Loader { get; set; }

        public LoaderTransferUnit LoaderTransfer_B { get; set; }
        public LoaderTransferUnit LoaderTransfer_A { get; set; }
        public PreAlignProxy PreAlign { get; set; }


        public bool IsHomeComplete
        {
            get
            {
                return
                  CstLoader_A.IsHomeComplete &&
                  CstLoader_A.IsHomeComplete &&
                  Loader.IsHomeComplete &&
                  LoaderTransfer_A.IsHomeComplete &&
                  LoaderTransfer_B.IsHomeComplete &&
                  PreAlign.IsHomeComplete;
            }
        }


        public ConvertA2D MainCDAPressurValuie1 { get; internal set; }
        public ConvertA2D MainCDAPressurValuie2 { get; internal set; }
        public ConvertA2D MainCDAPressurValuie3 { get; internal set; }
        public ConvertA2D MainAirFlowmeterValue1 { get; internal set; }
        public ConvertA2D MainAirFlowmeterValue2 { get; internal set; }
        public ConvertA2D MainAirFlowmeterValue3 { get; internal set; }
        public ConvertA2D PanelInnerTempValue { get; internal set; }

        public bool IsDoorRelease
        {
            get
            {
                return Door01.IsOnOff == true || Door02.IsOnOff == true || Door03.IsOnOff == true;
            }
        }
        public bool IsDoorOpen
        {
            get
            {
                return DoorOpen01.IsOnOff == true || DoorOpen02.IsOnOff == true || DoorOpen03.IsOnOff == true;
            }
        }
        public bool IsDoorKeyBoxOn
        {
            get
            {
                return DoorKeyBox01.XB_OnOff.vBit == false || DoorKeyBox02.XB_OnOff.vBit == false || DoorKeyBox03.XB_OnOff.vBit == false;
            }
        }
        public void DoorOpenClose(Equipment equip, bool open)
        {
            Door01.OnOff(equip, open);
            Door02.OnOff(equip, open);
            Door03.OnOff(equip, open);
        }
        #region LD AJIN

        public Switch CpBoxResetSwitch                                                    /**/    = new Switch();
        public Switch GpsMainMccbTrip                                                     /**/    = new Switch();


        public Sensor LDLiferMuting1_1                                                    /**/    = new Sensor();
        public Sensor LDLiferMuting1_2                                                    /**/    = new Sensor();
        public Sensor LDLiferMuting2_1                                                    /**/    = new Sensor();
        public Sensor LDLiferMuting2_2                                                    /**/    = new Sensor();

        public Sensor LDEmergencyStop1                                                    /**/    = new Sensor();
        public Sensor LDEmergencyStop2                                                    /**/    = new Sensor();
        public Sensor LDEmergencyStop3                                                    /**/    = new Sensor();
        public Switch EmergencyStopEnableGripSwitch1                                      /**/    = new Switch();
        public Switch SafteyEnableGripSwitchOn1                                           /**/    = new Switch();

        public OffCheckSwitch Door01                                                      /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door02                                                      /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door03                                                      /**/    = new OffCheckSwitch();


        public Sensor DoorOpen01                                                      /**/    = new Sensor();
        public Sensor DoorOpen02                                                      /**/    = new Sensor(false);
        public Sensor DoorOpen03                                                      /**/    = new Sensor(false);

        public Sensor MainAirFlowmeter1                                                   /**/    = new Sensor();
        public Sensor MainAirFlowmeter2                                                   /**/    = new Sensor();
        public Sensor MainAirFlowmeter3                                                   /**/    = new Sensor();

        public Sensor DoorKeyBox01                                                        /**/    = new Sensor();
        public Sensor DoorKeyBox02                                                        /**/    = new Sensor();
        public Sensor DoorKeyBox03                                                        /**/    = new Sensor();

        public Sensor MCOff1                                                            /**/    = new Sensor(false);
        public Sensor MCOff2                                                            /**/    = new Sensor(false);
        public Sensor ModeSelectSwitchAuto                                                /**/    = new Sensor();
        public Sensor ModeSelectSwitchTeach                                               /**/    = new Sensor();
        public Switch ControlSelectLD                                                     /**/    = new Switch();
        public Sensor Cell취출Hand출동방지                                                /**/    = new Sensor();
        public Switch LDResetSwitchUserA                                                  /**/    = new Switch();
        public Switch LDResetSwitchUserB                                                  /**/    = new Switch();



        public Switch MainCDAPressurSwitch1                                               /**/    = new Switch();
        public Switch MainCDAPressurSwitch2                                               /**/    = new Switch();
        public Switch MainCDAPressurSwitch3                                               /**/    = new Switch();
        public Switch MainVacuumPressurSwitch4                                            /**/    = new Switch();


        public Sensor OffDelayTimer감지                                                   /**/    = new Sensor();
        public Sensor LDFanStopAlarm1                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm2                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm3                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm4                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm5                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm6                                                     /**/    = new Sensor();
        public Sensor CtrlBoxOpenDetectBJub                                               /**/    = new Sensor();
        public Sensor EFUAlarm                                                            /**/    = new Sensor();
        public Sensor LDCpBoxFireAlarm                                                    /**/    = new Sensor();
        public Sensor LDCpBoxTempAlarm                                                    /**/    = new Sensor();

        public SwitchOneWay LDMotorControlOn                    /**/ = new SwitchOneWay();

        public SwitchOneWay LDAResetSwitchLamp                  /**/ = new SwitchOneWay();
        public SwitchOneWay LDBResetSwitchLamp                  /**/ = new SwitchOneWay();
        public SwitchOneWay LDACstMutingSwitchLamp              /**/ = new SwitchOneWay();
        public SwitchOneWay LDBCstMutingSwitchLamp              /**/ = new SwitchOneWay();

        public SwitchOneWay SafteyReset                         /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampR                          /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampY                          /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampG                          /**/ = new SwitchOneWay();
        public SwitchOneWay Buzzer                              /**/ = new SwitchOneWay();
        public SwitchOneWay 음성Buzzer1                          /**/ = new SwitchOneWay();
        public SwitchOneWay 음성Buzzer2                          /**/ = new SwitchOneWay();
        public SwitchOneWay ControlSelectLDCMD                  /**/ = new SwitchOneWay();
        public SwitchOneWay ModelSelectSwAutoTeachLockingSol    /**/ = new SwitchOneWay();
        public SwitchOneWay CpBoxTempResetSWLamp                /**/ = new SwitchOneWay();
        public SwitchOneWay InnerLamp                           /**/ = new SwitchOneWay();


        #endregion

        public LoaderModule()
        {
            CstLoader_B        /**/= new CstLoaderUint()      /**/ { Name = "카셋트 - B",        /**/ UnitPosi = EmUnitPosi.BCol,  /**/UnitNo = 0, ShareMem = (VirtualShare)GG.EQ_DATA, Port = new PortInfo(EmUnitPosi.BCol, "B PORT") };
            CstLoader_A        /**/= new CstLoaderUint()      /**/ { Name = "카셋트 - A",        /**/ UnitPosi = EmUnitPosi.ACol,  /**/UnitNo = 1, ShareMem = (VirtualShare)GG.EQ_DATA, Port = new PortInfo(EmUnitPosi.ACol, "A PORT") };
            Loader              /**/= new LoaderUnit()         /**/ { Name = "셀로더",              /**/ UnitPosi = EmUnitPosi.None, /**/UnitNo = 2, ShareMem = (VirtualShare)GG.EQ_DATA };

            LoaderTransfer_B   /**/= new LoaderTransferUnit() /**/ { Name = "로더 트랜스퍼 - B", /**/ UnitPosi = EmUnitPosi.BCol,  /**/UnitNo = 3, ShareMem = (VirtualShare)GG.EQ_DATA };
            LoaderTransfer_A   /**/= new LoaderTransferUnit() /**/ { Name = "로더 트랜스퍼 - A", /**/ UnitPosi = EmUnitPosi.ACol,  /**/UnitNo = 4, ShareMem = (VirtualShare)GG.EQ_DATA };

            PreAlign            /**/= new PreAlignProxy()      /**/ { Name = "PRE 얼라인먼트",      /**/ UnitPosi = EmUnitPosi.None, /**/UnitNo = 5, ShareMem = (VirtualShare)GG.EQ_DATA };
        }


        public void LogicWorking(Equipment equip)
        {
            CstLoader_B.LogicWorking(equip);
            CstLoader_A.LogicWorking(equip);

            Loader.LogicWorking(equip);

            LoaderTransfer_B.LogicWorking(equip);
            LoaderTransfer_A.LogicWorking(equip);

            PreAlign.LogicWorking(equip);


            SatusLogicWorking(equip);
        }



        public void SatusLogicWorking(Equipment equip)
        {
            //로더부 알람. 처리. 

            if (GpsMainMccbTrip.IsOnOff == false) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0014_GPS_MAIN_MCCB_TRIP);

            if (LDEmergencyStop1.IsOnOff == true) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0000_EMS_1);

            if (LDEmergencyStop2.IsOnOff == true)
                if (LDEmergencyStop3.IsOnOff == true) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0002_EMS_3);

            if (EmergencyStopEnableGripSwitch1.IsOnOff == true)
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0007_GRIP_EMS_1);

            if (MCOff1.IsOnOff == true && MCOff1.IsOnOff == true)
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0046_LD_MC_DOWN);

            if (MCOff1.IsOnOff != MCOff1.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0045_LD_MC_ERROR);

            if (OffDelayTimer감지.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0015_GPS_MOMENTARY_BLACKOUT);

            if (LDFanStopAlarm1.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0209_LD_CONTROL_BOX_FAN_1);
            if (LDFanStopAlarm2.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0210_LD_CONTROL_BOX_FAN_2);
            if (LDFanStopAlarm3.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0211_LD_CONTROL_BOX_FAN_3);
            if (LDFanStopAlarm4.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0212_LD_CONTROL_BOX_FAN_4);
            if (LDFanStopAlarm5.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0213_LD_CONTROL_BOX_FAN_5);
            if (LDFanStopAlarm6.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0214_LD_CONTROL_BOX_FAN_6);

            if (CtrlBoxOpenDetectBJub.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0028_LD_CONTROL_BOX_DOOR_OPEN);
            if (EFUAlarm.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0032_LD_EFU_ALARM);
            if (LDCpBoxFireAlarm.IsOnOff)
            {
                equip.SafetyMgr.GpsMainMccbTripCmd(0);
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0206_LD_CONTROL_BOX_SMOKE_HEAVY);
            }
            if (LDCpBoxTempAlarm.IsOnOff) AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0208_LD_CONTROL_BOX_TEMP_WARN);
            if (PanelInnerTempValue.Value > 40)
            {
                equip.SafetyMgr.GpsMainMccbTripCmd(0);
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0207_LD_CONTROL_BOX_TEMP_HEAVY);
            }

            //Console.WriteLine("온도 " + PanelInnerTempValue.Value.ToString());          
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using Dit.Framework.Log;


namespace DIT.TLC.CTRL
{
    public class PlcTimerEx : PlcTimer
    {
        public PlcTimerEx(string name)
            : base(name)
        {
        }
        public override void Start(int interval)
        {
            Logger.Log.AppendLine(LogLevel.Info, "{0} DELAY = {1}s", this.Name, interval);
            base.Start(interval);
        }
        public override void Start(int intervalSec, int intervalMilli)
        {
            Logger.Log.AppendLine(LogLevel.Info, "{0} DELAY = {1}s {2}ms", this.Name, intervalSec, intervalMilli);
            base.Start(intervalSec, intervalMilli);
        }
    }
}

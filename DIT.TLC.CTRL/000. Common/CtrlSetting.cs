﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DitCim.Lib;

namespace DIT.TLC.CTRL
{
    [Serializable]
    public class EquipSetting
    {

        public string CurrRecipeName { get; set; }
        public EquipSetting()
        {
        }

    }

    public class EquipSettingMgr
    {

        public static string PathOfXml = Path.Combine(GG.StartupPath, "Setting", "Dit.Tlc.Setting.ini");
        public EquipSetting Setting;
        public EquipSettingMgr()
        {
            Setting = new EquipSetting();
        }
        public void Save()
        {
            XmlFileManager<EquipSetting>.TrySaveXml(PathOfXml, Setting);
        }
        public void Load()
        {
            if (XmlFileManager<EquipSetting>.TryLoadData(PathOfXml, out Setting) == false)
            {
                Setting = new EquipSetting();
            }
        }
    }
}



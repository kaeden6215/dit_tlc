﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;

namespace DitCim.Common
{
    public static class UserExtensionEx
    {  
        public static bool IsRunningProcess(string procName)
        {
            Process[] process = Process.GetProcessesByName(procName);

            if (process.Length < 1)
                return false;

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace DIT.TLC.CTRL
{
    public static class XmlFileManager<TState> where TState : class
    {
        // 정적 메서드
        public static bool TrySaveXml(string xmlFilePath, TState data)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(TState));
                using (TextWriter textWriter = new StreamWriter(xmlFilePath))
                {
                    xmlSerializer.Serialize(textWriter, data);
                    textWriter.Close();
                }

                return true;
            }
            catch (Exception ex )
            {
                return false;
            }
        }
        public static bool TryLoadData(string xmlFilePath, out TState data)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(TState));
                using (TextReader textReader = new StreamReader(xmlFilePath))
                {
                    data = (TState)xmlSerializer.Deserialize(textReader);
                    textReader.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                data = null;
                return false;
            }
        }

        // 정적 메서드
        public static bool TrySaveToXmlText(TState data, out string text)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(TState));
                using (StringWriter sw = new StringWriter())
                {
                    xmlSerializer.Serialize(sw, data);
                    text = sw.ToString();
                }

                return true;
            }
            catch (Exception)
            {
                text = null;

                return false;
            }
        }
        public static bool TryLoadFromXmlText(string text, out TState data)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(TState));
                using (StringReader sr = new StringReader(text))
                {
                    data = (TState)xmlSerializer.Deserialize(sr);
                }

                return true;
            }
            catch (Exception)
            {
                data = null;
                return false;
            }
        }

        // 정적 메서드
        public static byte[] SerializeBin(TState data)
        {
            BinaryFormatter binFmt = new BinaryFormatter();
            using (MemoryStream fs = new MemoryStream())
            {
                binFmt.Serialize(fs, data);
                return fs.ToArray();
            }
        }
        public static TState DeserializeBin(byte[] bin)
        {
            TState p;
            BinaryFormatter binFmt = new BinaryFormatter(); ;
            using (MemoryStream rdr = new MemoryStream(bin, false))
            {
                p = (TState)binFmt.Deserialize(rdr);
            }
            return p;
        }
    }
}
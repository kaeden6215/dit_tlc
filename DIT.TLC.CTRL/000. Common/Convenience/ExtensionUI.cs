﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Dit.Framework.UI;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public class ExtensionUI
    {
        public static void ChangeEnabledControl(Control parent, bool isEnable, Type[] selectedType = null)
        {
            List<Control> childControls = new List<Control>();
            GetAllControl(parent, ref childControls);

            foreach (Control control in childControls)
            {                
                if(selectedType != null) // 지정된 타입이 있으면 해당 타입만 Enable변경.
                {
                    object type = selectedType.FirstOrDefault(item => item == control.GetType());
                    if(type != null)
                        control.Enabled = isEnable;                
                }                
                else
                    control.Enabled = isEnable;
            }
        }
        public static void AddClickEventLog(Control parent)
        {
            List<Control> childControls = new List<Control>();
            GetAllControl(parent, ref childControls);
            foreach (Control control in childControls)
            {
                if (control.GetType() == typeof(ButtonLabel) ||
                    control.GetType() == typeof(Button))
                {
                    control.Click += new EventHandler(SomeButton_Click);
                }
                if (control.GetType() == typeof(ButtonDelay))
                {
                    ButtonDelay bd = control as ButtonDelay;
                    bd.Click += new EventHandler(SomeButton_Click);
                    bd.DelayClick += new EventHandler(SomeDelayButton_Click);
                }
            }
        }
        public static void BindColorToControl(Control control, bool data, 
            bool isTrueOn = true, Color? trueColor = null, Color? falseColor = null)
        {
            Color TrueColor = trueColor ?? Color.FromArgb(255, 100, 100);
            Color FalseColor = falseColor ?? Color.Transparent;
            control.BackColor = !(data ^ isTrueOn) ? TrueColor : FalseColor;
        }

        public static DateTime logoutTime = DateTime.Now;
        private static void SomeButton_Click(Object sender, EventArgs e)
        {
            logoutTime = DateTime.Now;
            Control control = (Control)sender;
            Logger.Log.AppendLine(LogLevel.Info, "Name:{0} Text:{1} 버튼 클릭", control.Name, control.Text);
        }
        private static void SomeDelayButton_Click(Object sender, EventArgs e)
        {
            logoutTime = DateTime.Now;
            Control control = (Control)sender;
            Logger.Log.AppendLine(LogLevel.Info, "Name:{0} Text:{1} 버튼 딜레이클릭", control.Name, control.Text);
        }
        private static void GetAllControl(Control c, ref List<Control> list)
        {
            foreach (Control control in c.Controls)
            {
                list.Add(control);
                if (control.HasChildren)
                {
                    GetAllControl(control, ref list);
                }
            }
        }
    }
}

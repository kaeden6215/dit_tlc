﻿using Dit.Framework.Comm;
using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public partial class FrmMain : Form
    {
        

        public FrmMain()
        {
            InitializeComponent();

            Initailzie(); 
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);            
        }

        private void Initailzie()
        {
            //ExtensionUI.AddClickEventLog(this);
          //  CheckForIllegalCrossThreadCalls = false;
            GG.Equip = new Equipment();

            if (GG.TestMode)
            {
                Logger.Log = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, lstLogger);
                Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "AlarmLog"), "CLIENT", 500, 1024 * 1024 * 20,  true, lstLogger);
                Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "HSMS", 500, 1024 * 1024 * 20,  true, lstLogger);
            }
            else
            {
                System.IO.DirectoryInfo Di = new System.IO.DirectoryInfo("E:/DitCtrl");
                if (Di.Exists)
                {
                    Logger.Log = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "CLIENT", 500, 1024 * 1024 * 20,  true, lstLogger);
                    Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "CLIENT", 500, 1024 * 1024 * 20,  true, lstLogger);
                    Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "HSMS", 500, 1024 * 1024 * 20,  true, lstLogger);
                }
                else
                {
                    Logger.Log = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20,  true, lstLogger);
                    Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "AlarmLog"), "CLIENT", 500, 1024 * 1024 * 20,  true, lstLogger);
                    Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "HSMS", 500, 1024 * 1024 * 20,  true, lstLogger);
                }
            }


            if (false == GG.TestMode)
            {
                //GG.CCLINK = (IVirtualMem)new VirtualCCLink(81, -1,  "CCLINK");
                //GG.CCLINK = (IVirtualMem)new VirtualCCLinkEx(81, 0xff, -1, "CCLINK");
                //GG.PMAC = (IVirtualMem)(new VirtualUMac("PMAC", Properties.Settings.Default.PmacIP, Properties.Settings.Default.PmacPort));
                //GG.HSMS = (IVirtualMem)new VirtualCCLinkEx(151, 255, 0, "CCLINK") { IsDirect = true };
                //GG.MACRO = (IVirtualMem)new VirtualShare("DIT.PLC.MACRO_MEM.S", 102400);
                //GG.MEM_DIT = (IVirtualMem)new VirtualShare("DIT.CTRL.SHARE.MEM", 102400);
            }
            else
            {
                GG.CCLINK = (IVirtualMem)new VirtualMem("CCLINK");
                GG.PMAC = (IVirtualMem)new VirtualMem("PMAC");
                //GG.HSMS = (IVirtualMem)new VirtualCCLinkEx(151, 255, 0, "CCLINK") { IsDirect = true };
                GG.HSMS = (IVirtualMem)new VirtualShare("DIT.CTRL.HSMS.MEM", 102400);
                GG.MEM_DIT = (IVirtualMem)new VirtualShare("DIT.CTRL.SHARE.MEM", 102400);
            }

            int cclinkResult = GG.CCLINK.Open();
            int pmacResult = GG.PMAC.Open();
            int hsmsResult = GG.HSMS.Open();
            int ditMemResult = GG.MEM_DIT.Open();

            AddressMgr.Load(GG.ADDRESS_CC_LINK,             /**/GG.CCLINK);
            AddressMgr.Load(GG.ADDRESS_PMAC,                /**/GG.PMAC);
            AddressMgr.Load(GG.ADDRESS_PIO,                 /**/GG.CCLINK);

            GG.Equip.SetAddress();


            GG.Equip.StartLogic();


            ServoMotorControl[] motors = GG.Equip.GetServoMotors();

            for (int jPos = 0; jPos < motors.Length; jPos++)
            {
                Button btn = new Button() { Text = motors[jPos].Name, Width = 100, Height = 50, Tag = motors[jPos] };
                btn.Click += new EventHandler(btnServoMotor_Click);

                flPnlServo.Controls.Add(btn);
            }

            //_equip.Initalize();

            //_equip.PMac.Initailize();
            //_equip.ReviPc.Initailize(GG.MEM_DIT);
            //_equip.InspPc.Initailize(GG.MEM_DIT);
            //_equip.MacroPc.Initailize(GG.MACRO);
            //_equip.HsmsPc.Initailize(GG.HSMS);
            //_equip.InitalizeAlarmList();
            //_equip.UpdateDelaySetting();
            //_equip.UpdateCimDelaySetting();

            //_logicWorker.Equip = _equip;
            //_logicWorker.Start();

            //btnGlassInfo_Click(btnLoadingGlassInfo, null);

            //tmrUiUpdate.Start();

            //if (GG.TestMode == false)
            //{
            //    this.SetDesktopBounds(0, 0, 384, 1080);
            //    buttonExpanding.Text = "Details";
            //    _isExpending = false;
            //}
            //else
            //    FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;

            //ChangeView(ref pnlEquipDraw);

            //_equip.IsLongTest = false;
            //_equip.IsCycleStop = EmCycleStop.None;

            //_frmOperatorCall = new FrmOperatorCall(_equip);

            //_tabCtrl_Operator = new FrmOperator(_equip);
            //_tabCtrl_Operator.FormBorderStyle = FormBorderStyle.None;
            //_tabCtrl_Operator.Name = "FrmOpMain";
            //_tabCtrl_Operator.TopLevel = false;
            //pnlOperator.Controls.Add(_tabCtrl_Operator);
            //_tabCtrl_Operator.Parent = pnlOperator;
            //_tabCtrl_Operator.Text = string.Empty;
            //_tabCtrl_Operator.ControlBox = false;
            //_tabCtrl_Operator.Show();
            //InitMotorDataGridView(dgvMotorState);
            //InitMotorDataGridView(dgvMotorState1);

            //_equip.ADC.Adc1.ErrorReset();
            //_equip.ADC.Temperature.ErrorReset();


            ////30일 이전 데이터 삭제
            //_equip.DacEquipData.DeleteDatabaseInfo(-30);

            //tabCtrl_MainMenu.SelectedIndex = 1;
        }

        private void btnServoMotor_Click(object sender, EventArgs e)
        {
            //ServoMotorControl motor = ((Button)sender).Tag as ServoMotorControl;
            //ucrlServo1.SetInstance(GG.Equip , motor, motor.Name);
        }

        private void button1_Click(object sender, EventArgs e)
        {
         
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class WorkInfo
    {
        public string WorkName { get; set; }
        public string RecipeName { get; set; }
        public string Process { get; set; }
        public string CellSize { get; set; }
        public string StartTime { get; set; }
        public string WorkTime { get; set; }
        public string WorkCount { get; set; }
        public string TackTime { get; set; }

        public WorkInfo()
        {
            WorkName = "0";
            RecipeName = "0";
            Process = "0";
            CellSize = "0";
            StartTime = "0";
            WorkTime = "0";
            WorkCount = "0";
            TackTime = "0";
        }
    }
}

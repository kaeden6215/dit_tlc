﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class PanelInfoSet : ICloneable
    {
        public EmUnitPosi FlowUnitPosi { get; set; }
        public EmCellPosi CellPosi { get; set; }

        public PortInfo LDPort { get; set; }
        public PortInfo ULDPort { get; set; }

        public bool Exists1 { get { return Panel1.Exists; } }
        public bool Exists2 { get { return Panel2.Exists; } }

        public PanelInfo Panel1 { get; set; }
        public PanelInfo Panel2 { get; set; }

        public PanelInfoSet()
        {
            FlowUnitPosi = EmUnitPosi.ACol;
            Panel1 = new PanelInfo() { Exists = false, PanelNo = "emptry" };
            Panel2 = new PanelInfo() { Exists = false, PanelNo = "emptry" };
        }

        public object Clone()
        {
            PanelInfoSet copy = new PanelInfoSet();

            copy.FlowUnitPosi = this.FlowUnitPosi;
            copy.CellPosi = this.CellPosi;
            copy.LDPort = this.LDPort;
            copy.ULDPort = this.ULDPort;
            copy.FlowUnitPosi = this.FlowUnitPosi;
            copy.FlowUnitPosi = this.FlowUnitPosi;
            copy.Panel1 = this.Panel1;
            copy.Panel2 = this.Panel2;
            return copy;
        }
        public object ClearPanel()
        {
            
            this.Panel1 = new PanelInfo();
            this.Panel2 = new PanelInfo();
            return null;
        }
    }

    [Serializable]
    public class PanelInfo : ICloneable
    {
        public string PanelNo { get; set; }
        public string PanelID { get; set; }
        public bool Exists { get; set; }

        public int Index { get; set; }
        public int SlotRow { get; set; }
        public int SlotCol { get; set; }


        public AlignResult PreAlignResult { get; set; }
        public AlignResult FineAlignResult {get;set;}
        public AlignResult BreakAlignResult { get; set; }
        public McrResult McrResult { get; set; }
        public InspResult InspResult { get; set; }

        //CIM 에서 사용 
        public DateTime Start = DateTime.Now;
        public DateTime End = DateTime.Now;

        public PanelInfo()
        {
            PanelNo = string.Empty;
            Exists = false;
        }
        public object Clone()
        {
            PanelInfo info = new PanelInfo();
            info.Start                       /**/ = this.Start;
            info.End                         /**/ = this.End;
            return info;
        }

        public PanelInfo BackUpPanelInfo()
        {
            PanelInfo backup = new PanelInfo();

            backup.PanelNo = this.PanelNo;
            backup.PanelID = this.PanelID;
            backup.Exists = this.Exists;
            backup.Index = this.Index;
            backup.SlotRow = this.SlotRow;
            backup.SlotCol = this.SlotCol;
            backup.PreAlignResult = this.PreAlignResult;
            backup.FineAlignResult = this.FineAlignResult;
            backup.BreakAlignResult = this.BreakAlignResult;
            backup.McrResult = this.McrResult;

            return backup;
        }
    }
}

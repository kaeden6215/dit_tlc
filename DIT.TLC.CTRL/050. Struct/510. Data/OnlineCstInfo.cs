﻿namespace DIT.TLC.CTRL
{
    public class OnlineCassetteInfoInfo
    {
        public string PPID { get; internal set; }
        public OnlineCassetteInfoInfo()
        {
            PPID = string.Empty;
        }
    }
}
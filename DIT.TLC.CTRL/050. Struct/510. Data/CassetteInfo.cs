﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{

    public enum EmPortStatus
    {
        EMPTY,
        IDLE,
        WAIT,
        BUSY,
    }

    public enum EMCasstteStatus
    {
        LOADING_START,
        MESURE_START,
        CELLOUT_START,
        UNLOADING_START,
        COMPLETE,
        NONE,
    }

    public class PortInfo
    {
        public EmUnitPosi UnitPosi = EmUnitPosi.None;

        public EmPortStatus Status;
        public CassetteInfo Cst = new CassetteInfo();
        public PortInfo ULDPort = null;
        public string Name { get; set; }

        public PortInfo(EmUnitPosi unitPosi, string name)
        {
            UnitPosi = unitPosi;
            Status = EmPortStatus.EMPTY;
            Name = name;
        }
        public void ChangePortStatus(EmPortStatus status)
        {
            Status = status;
        }
    }

    public class CassetteInfo
    {
        public List<PanelInfo> LstPanel { get; set; }

        public int SlotCount { get; set; }
        public int CellCount { get; set; }

        public int InCount { get; set; }
        public int OutCount { get; set; }

        public bool IsTiltSuccess { get; set; }
        private EMCasstteStatus _cstStatus = EMCasstteStatus.NONE;
        public EMCasstteStatus CstStatus { get { return _cstStatus; } }

        public string PPID { get { return OnlineCstInfo == null ? string.Empty : OnlineCstInfo.PPID; } }
        public OnlineCassetteInfoInfo OnlineCstInfo { get; set; }

        public CassetteInfo()
        {
            _cstStatus = EMCasstteStatus.LOADING_START;

            LstPanel = new List<PanelInfo>();
            SlotCount = 160;
            for (int iPos = 0; iPos < SlotCount; iPos++)
                LstPanel.Add(new PanelInfo() { PanelNo = "empty" });
        }
        public PanelInfoSet GetOutPanelInfoSet(PortInfo fromPort, PortInfo toPort, EqpRecipe rcp)
        {
            int axis1Posi = OutCount / 2;
            int axis2Posi = OutCount / 2 + rcp.CassetteCellCount / 2;

            PanelInfoSet set = new PanelInfoSet();
            set.LDPort = fromPort;
            set.ULDPort = toPort;
            set.FlowUnitPosi = OutCount / 2 % 2 == 0 ? EmUnitPosi.ACol : EmUnitPosi.BCol;

            if (rcp.CstType == emCstType.Cst1)
            {
                set.Panel1 = LstPanel[axis1Posi];
                set.CellPosi = set.Panel1.Exists ? EmCellPosi.One_1 : EmCellPosi.None;
            }
            else
            {
                set.Panel1 = LstPanel[axis1Posi];
                set.Panel2 = LstPanel[axis2Posi];
                set.CellPosi = set.Panel1.Exists && set.Panel2.Exists ? EmCellPosi.Two :
                               set.Panel1.Exists ? EmCellPosi.One_1 :
                               set.Panel2.Exists ? EmCellPosi.One_2 : EmCellPosi.None;
            }
            return set;
        }

        //카셋트의 수량으로 위치 계산
        public int GetCellOutColPosi(EmAxisPosi axisNo, EqpRecipe rcp)
        {
            int maxCol = rcp.GetCstMaxCol;
            int axis1Col = (int)Math.Truncate(1f * OutCount / rcp.CassetteMaxHeightCount);
            int axis2Col = maxCol / 2 + axis1Col;

            if (axisNo == EmAxisPosi.Aixs1)
            {
                return axis1Col;
            }
            else
            {
                if (rcp.CstType == emCstType.Cst1)
                    return 0;
                else
                    return axis2Col;
            }
        }
        public int GetCellOutRowPosi(EqpRecipe rcp)
        {
            int maxCol = rcp.CassetteCellCount / rcp.CassetteMaxHeightCount;
            int row = OutCount % rcp.CassetteMaxHeightCount;
            return row;
        }
        public int GetCellInColPosi(EmAxisPosi axisNo, EqpRecipe rcp)
        {
            int maxCol = rcp.CassetteCellCount / rcp.CassetteMaxHeightCount;
            int axis1Col = (int)Math.Truncate(1f * InCount % rcp.CassetteMaxHeightCount);
            int axis2Col = maxCol / 2 + axis1Col;

            if (axisNo == EmAxisPosi.Aixs1)
            {
                return axis1Col;
            }
            else
            {
                if (rcp.CstType == emCstType.Cst1)
                    return 0;
                else
                    return axis2Col;
            }
        }
        public int GetCellInRowPosi(EqpRecipe rcp)
        {
            int maxCol = rcp.CassetteCellCount / rcp.CassetteMaxHeightCount;
            int row = InCount % rcp.CassetteMaxHeightCount;
            return row;
        }



        public void SetInPanelInfoSet(EqpRecipe rcp)
        {
            PanelInfoSet set = new PanelInfoSet();
            if (rcp.Name != PPID)
            {
                InterLockMgr.AddInterLock("설비에 설정된 PPID와 카셋트의 PPID가 상이 합니다.");
                set.Panel1 = new PanelInfo();
                set.Panel2 = new PanelInfo();
            }

            int slot = OutCount / 2;
            LstPanel[slot + 0] = set.Panel1;
            LstPanel[slot + SlotCount / 2] = set.Panel2;
        }
        public void CellOutComplete(PanelInfoSet pnlSet)
        {
            LstPanel[pnlSet.Panel1.Index] = new PanelInfo();
            LstPanel[pnlSet.Panel2.Index] = new PanelInfo();

            if (pnlSet.CellPosi == EmCellPosi.One_1 || pnlSet.CellPosi == EmCellPosi.One_2)
                OutCount += 1;
            if (pnlSet.CellPosi == EmCellPosi.Two)
                OutCount += 2;
        }
        public void CellInComplete(PanelInfoSet pnlSet)
        {
            LstPanel[pnlSet.Panel1.Index] = pnlSet.Panel1;
            LstPanel[pnlSet.Panel2.Index] = pnlSet.Panel2;

            if (pnlSet.CellPosi == EmCellPosi.One_1 || pnlSet.CellPosi == EmCellPosi.One_2)
                InCount += 1;
            if (pnlSet.CellPosi == EmCellPosi.Two)
                InCount += 2;
        }

        public void FillCassette(EqpRecipe rcp, int cellCnt, OnlineCassetteInfoInfo cstInfo)
        {
            //카셋트 정보 채우기. 
            SlotCount = rcp.CassetteCellCount;
            CellCount = cellCnt;
            OnlineCstInfo = cstInfo;

            int firstSlot = 0;
            int secendSlot = 0;


            string pnlNo = DateTime.Now.ToString("yyyyMMddHHmmss");
            int div = (int)Math.Ceiling(SlotCount / 2f);
            for (int iPos = 0; iPos < SlotCount / 2; iPos++)
            {
                firstSlot = iPos;
                secendSlot = div + iPos;

                //앞단
                if (firstSlot < Math.Ceiling(CellCount / 2f))
                {
                    LstPanel[firstSlot].PanelNo = string.Format("{0}_{1}", pnlNo, iPos);
                    LstPanel[firstSlot].Exists = true;
                    LstPanel[firstSlot].Index = firstSlot;
                }
                else
                {
                    LstPanel[firstSlot].PanelNo = "empty";
                    LstPanel[firstSlot].Exists = false;
                    LstPanel[firstSlot].Index = firstSlot;
                }

                //중간 다음
                if ((secendSlot) < div + Math.Truncate(CellCount / 2f))
                {
                    LstPanel[secendSlot].PanelNo = string.Format("{0}_{1}", pnlNo, iPos);
                    LstPanel[secendSlot].Exists = true;
                    LstPanel[secendSlot].Index = secendSlot;
                }
                else
                {
                    LstPanel[secendSlot].PanelNo = "empty";
                    LstPanel[secendSlot].Exists = false;
                    LstPanel[secendSlot].Index = secendSlot;
                }

            }
        }
        public void ChangeCassetteStatus(Equipment equip, EMCasstteStatus status)
        {
            //인터락 고민 필요.  두개 동시에 카셋트가 진행이 가능한가 ?? 
            _cstStatus = status;
        }

        internal PanelInfoSet GetOutPanelInfoSet(PortInfo port, object toPort, EqpRecipe currentRecipe)
        {
            throw new NotImplementedException();
        }
    }
}

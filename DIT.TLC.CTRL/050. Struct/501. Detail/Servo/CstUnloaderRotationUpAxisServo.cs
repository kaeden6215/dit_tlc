﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstUnloaderRotationUpAxisServo : ServoMotorControl
    {
        public static int A_Cst_Insert_Stand     {get;set;}
        public static int A_Cst_Insert           {get;set;}
        public static int A_Cst_Tilt_Measure     {get;set;}
        public static int A_Cell_Insert_Start    {get;set;}
        public static int A_Cst_Emission         {get;set;}
        public static int B_Cst_Insert_Stand     {get;set;}
        public static int B_Cst_Insert           {get;set;}
        public static int B_Cst_Tilt_Measure     {get;set;}
        public static int B_Cell_Insert_Start    {get;set;}
        public static int B_Cst_Emission         {get;set;}

        public CstUnloaderRotationUpAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;


            //A_Cst_Insert_Stand                    = 1;
            //A_Cst_Insert                          = 2;
            //A_Cst_Tilt_Measure                    = 3;
            //A_Cell_Insert_Start                   = 4;
            //A_Cst_Emission                        = 5;
            //B_Cst_Insert_Stand                    = 6;
            //B_Cst_Insert                          = 7;
            //B_Cst_Tilt_Measure                    = 8;
            //B_Cell_Insert_Start                   = 9;
            //B_Cst_Emission                        = 10;


            //base.MoveActionName[0] = "StandByBottomL_Z";                      // A 리프트(Z1) : 카세트 투입 대기
            //base.MoveActionName[1] = "LoadingBottomL_Z";                      // A 리프트(Z1) : 카세트 투입    
            //base.MoveActionName[2] = "MeasureL_Z";                            // A 리프트(Z1) : 카세트 틸트 측정
            //base.MoveActionName[3] = "CellContactL_Z";                        // A 리프트(Z1) : 셀 투입 시작   
            //base.MoveActionName[4] = "UnloadingTopL_Z";                       // A 리프트(Z1) : 카세트 배출    
            //base.MoveActionName[5] = "StandByBottomR_Z";                      // B 리프트(Z2) : 카세트 투입 대기
            //base.MoveActionName[6] = "LoadingBottomR_Z";                      // B 리프트(Z2) : 카세트 투입    
            //base.MoveActionName[7] = "MeasureR_Z";                            // B 리프트(Z2) : 카세트 틸트 측정
            //base.MoveActionName[8] = "CellContactR_Z";                        // B 리프트(Z2) : 셀 투입 시작   
            //base.MoveActionName[9] = "UnloadingTopR_Z";                       // B 리프트(Z2) : 카세트 배출    
        }

    }
}

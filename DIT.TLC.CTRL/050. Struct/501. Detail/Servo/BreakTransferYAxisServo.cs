﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public class BreakTransferYAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "ScanXServo.ini");

        public static int After_IR_Load_Gentry_Y1 { get; set; }

        public static int After_IR_UnLoad_Gentry_Y2 { get; set; }
        //
        public BreakTransferYAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            After_IR_Load_Gentry_Y1 = 1;
            After_IR_UnLoad_Gentry_Y2 = 2;

            
            //base.MoveActionName[0] = "Load_Y1";                       //After IR 이재기 겐트리 : 셀 로드 위치(Y1) 
            //base.MoveActionName[1] = "UnLoad_Y1";                     //After IR 이재기 겐트리 : 셀 로드 위치(Y2) 

        }
        
    }
}
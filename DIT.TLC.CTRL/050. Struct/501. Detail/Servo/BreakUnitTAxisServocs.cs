﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class BreakUnitTAxisServocs : ServoMotorControl
    {

       public static int T1_Load_Pause    {get;set;}
       public static int T2_Load_Pause    {get;set;}
       public static int T3_Load_Pause    {get;set;}
       public static int T4_Load_Pause    {get;set;}


        public BreakUnitTAxisServocs(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            //T1_Load_Pause = 1;
            //T2_Load_Pause = 2;
            //T3_Load_Pause = 3;
            //T4_Load_Pause = 4;


            //base.MoveActionName[0] = "Load_T1";         // T1 : 대기 위치(T1)
            //base.MoveActionName[1] = "Load_T2";         // T2 : 대기 위치(T2)
            //base.MoveActionName[2] = "Load_T3";         // T3 : 대기 위치(T3)
            //base.MoveActionName[3] = "Load_T4";         // T4 : 대기 위치(T4)

        }

    }
}


















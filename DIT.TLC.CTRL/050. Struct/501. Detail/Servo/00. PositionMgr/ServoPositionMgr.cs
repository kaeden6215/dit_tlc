﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class ServoPositionMgr
    {

        public static bool LoadPosition(Equipment equip)
        {
            int errCnt = 0;


            // TOP B열
            // BOT A열
            #region CstLoader
            //CstRotationAxis 설정 
            equip.LD.TopCstLoader.CstRotationAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopCstLoaderCstRotationAxisPosi.xml"); ;
            equip.LD.TopCstLoader.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "B열 AA", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopCstLoader.CstRotationAxis},
                 new ServoPosiInfo() {No = 1, Name = "B열 BB", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopCstLoader.CstRotationAxis},
                 new ServoPosiInfo() {No = 2, Name = "B열 CC", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopCstLoader.CstRotationAxis},
             };
            if (equip.LD.TopCstLoader.CstRotationAxis.Setting.Load() == false) errCnt++;


            equip.LD.BotCstLoader.CstRotationAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotCstLoaderCstRotationAxisPosi.xml"); ;
            equip.LD.BotCstLoader.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "A열 AA", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotCstLoader.CstRotationAxis},
                     new ServoPosiInfo() {No = 1, Name = "A열 BB", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotCstLoader.CstRotationAxis},
                     new ServoPosiInfo() {No = 2, Name = "A열 CC", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotCstLoader.CstRotationAxis},
             };
            if (equip.LD.BotCstLoader.CstRotationAxis.Setting.Load() == false) errCnt++;


            //CstUpDownAxis 설정 
            equip.LD.TopCstLoader.CstUpDownAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopCstLoaderCstUpDownAxisPosi.xml"); ;
            equip.LD.TopCstLoader.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "B열 AA(Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopCstLoader.CstUpDownAxis},
                 new ServoPosiInfo() {No = 1, Name = "B열 BB(Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopCstLoader.CstUpDownAxis},
                 new ServoPosiInfo() {No = 2, Name = "B열 CC(Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopCstLoader.CstUpDownAxis},
             };
            if (equip.LD.TopCstLoader.CstUpDownAxis.Setting.Load() == false) errCnt++;
            

            equip.LD.BotCstLoader.CstUpDownAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotCstLoaderCstUpDownAxisPosi.xml"); ;
            equip.LD.BotCstLoader.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "A열 AA(Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotCstLoader.CstUpDownAxis},
                     new ServoPosiInfo() {No = 1, Name = "A열 BB(Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotCstLoader.CstUpDownAxis},
                     new ServoPosiInfo() {No = 2, Name = "A열 CC(Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotCstLoader.CstUpDownAxis},
             };
            if (equip.LD.BotCstLoader.CstUpDownAxis.Setting.Load() == false) errCnt++;
            #endregion

            #region Loader
            //Loader.X1Axis 설정 
            equip.LD.Loader.X1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "LoaderX1AxisPosi.xml"); ;
            equip.LD.Loader.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (X1) : A 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (X1) : B 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (X1) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
             };
            if (equip.LD.Loader.X1Axis.Setting.Load() == false) errCnt++;

            //Loader.X2Axis 설정
            equip.LD.Loader.X2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "LoaderX2AxisPosi.xml"); ;
            equip.LD.Loader.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "취출 이재기 (X2) : A 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
                     new ServoPosiInfo() {No = 1, Name = "취출 이재기 (X2) : B 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
                     new ServoPosiInfo() {No = 2, Name = "취출 이재기 (X2) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
             };
            if (equip.LD.Loader.X2Axis.Setting.Load() == false) errCnt++;

            //Loader.Y1Axis 설정 
            equip.LD.Loader.Y1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "LoaderY1AxisPosi.xml"); ;
            equip.LD.Loader.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (Y1) : A 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (Y1) : B 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (Y1) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 3, Name = "취출 이재기 (Y1) : 2mm 후퇴 속도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
             };
            if (equip.LD.Loader.Y1Axis.Setting.Load() == false) errCnt++;

            //Loader.Y2Axis 설정
            equip.LD.Loader.Y2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "LoaderY2AxisPosi.xml"); ;
            equip.LD.Loader.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                     new ServoPosiInfo() {No = 0, Name = "취출 이재기 (Y2) : A 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                     new ServoPosiInfo() {No = 1, Name = "취출 이재기 (Y2) : B 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                     new ServoPosiInfo() {No = 2, Name = "취출 이재기 (Y2) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                     new ServoPosiInfo() {No = 3, Name = "취출 이재기 (Y2) : 2mm 후퇴 속도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
             };
            if (equip.LD.Loader.Y2Axis.Setting.Load() == false) errCnt++;
            #endregion

            #region LoaderTransfer
            //TopLoaderTransfer.X1Axis 설정 
            equip.LD.TopLoaderTransfer.X1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopLoaderTransferX1AxisPosi.xml"); ;
            equip.LD.TopLoaderTransfer.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
             {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X1) : B 로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X1) : B 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X1) : B 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X1Axis},
                 new ServoPosiInfo() {No = 3, Name = "로더 이재기 (X1) : PreAlign X1", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X1Axis},
             };
            if (equip.LD.TopLoaderTransfer.X1Axis.Setting.Load() == false) errCnt++;

            //BotLoaderTransfer.X1Axis 설정 
            equip.LD.BotLoaderTransfer.X1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotLoaderTransferX1AxisPosi.xml"); ;
            equip.LD.BotLoaderTransfer.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
             {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X1) : A 로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X1) : A 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X1) : A 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X1Axis},
                 new ServoPosiInfo() {No = 3, Name = "로더 이재기 (X1) : PreAlign X1", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X1Axis},
             };
            if (equip.LD.BotLoaderTransfer.X1Axis.Setting.Load() == false) errCnt++;

            //TopLoaderTransfer.X2Axis 설정
            equip.LD.TopLoaderTransfer.X2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopLoaderTransferX2AxisPosi.xml"); ;
            equip.LD.TopLoaderTransfer.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X2) : B 로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X2Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X2) : B 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X2Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X2) : B 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X2Axis},
                 new ServoPosiInfo() {No = 3, Name = "로더 이재기 (X2) : PreAlign X2", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.X2Axis},
             };
            if (equip.LD.TopLoaderTransfer.X2Axis.Setting.Load() == false) errCnt++;

            //BotLoaderTransfer.X2Axis 설정
            equip.LD.BotLoaderTransfer.X2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotLoaderTransferX2AxisPosi.xml"); ;
            equip.LD.BotLoaderTransfer.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X2) : A 로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X2Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X2) : A 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X2Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X2) : A 스테이지 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X2Axis},
                 new ServoPosiInfo() {No = 3, Name = "로더 이재기 (X2) : PreAlign X2", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.X2Axis},
             };
            if (equip.LD.BotLoaderTransfer.X2Axis.Setting.Load() == false) errCnt++;

            //TopLoaderTransfer.YAxis 설정 
            equip.LD.TopLoaderTransfer.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopLoaderTransferYAxisPosi.xml"); ;
            equip.LD.TopLoaderTransfer.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (Y1) : B 로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (Y1) : B 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (Y1) : PreAlign Y1", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.YAxis},
             };
            if (equip.LD.TopLoaderTransfer.YAxis.Setting.Load() == false) errCnt++;

            //BotLoaderTransfer.YAxis 설정 
            equip.LD.BotLoaderTransfer.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotLoaderTransferYAxisPosi.xml"); ;
            equip.LD.BotLoaderTransfer.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (Y1) : A 로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (Y1) : A 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (Y1) : PreAlign Y1", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.YAxis},
             };
            if (equip.LD.BotLoaderTransfer.YAxis.Setting.Load() == false) errCnt++;

            //TopLoaderTransfer.SubT1Axis 설정 
            equip.LD.TopLoaderTransfer.SubT1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopLoaderTransferSubT1AxisPosi.xml"); ;
            equip.LD.TopLoaderTransfer.SubT1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (T1) : B 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.SubT1Axis},
             };
            if (equip.LD.TopLoaderTransfer.SubT1Axis.Setting.Load() == false) errCnt++;

            //BotLoaderTransfer.SubT1Axis 설정 
            equip.LD.BotLoaderTransfer.SubT1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotLoaderTransferSubT1AxisPosi.xml"); ;
            equip.LD.BotLoaderTransfer.SubT1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (T1) : A 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.SubT1Axis},
             };
            if (equip.LD.BotLoaderTransfer.SubT1Axis.Setting.Load() == false) errCnt++;

            //TopLoaderTransfer.SubT2Axis 설정 
            equip.LD.TopLoaderTransfer.SubT2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopLoaderTransferSubT2AxisPosi.xml"); ;
            equip.LD.TopLoaderTransfer.SubT2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (T2) : B 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.TopLoaderTransfer.SubT2Axis},
             };
            if (equip.LD.TopLoaderTransfer.SubT2Axis.Setting.Load() == false) errCnt++;

            //BotLoaderTransfer.SubT2Axis 설정 
            equip.LD.BotLoaderTransfer.SubT2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotLoaderTransferSubT2AxisPosi.xml"); ;
            equip.LD.BotLoaderTransfer.SubT2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (T2) : A 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.BotLoaderTransfer.SubT2Axis},
             };
            if (equip.LD.BotLoaderTransfer.SubT2Axis.Setting.Load() == false) errCnt++;
            #endregion

            //#region IRCutStage
            //TopIRCutStage.YAxis 설정 0
            equip.PROC.TopIRCutStage.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopIRCutStageYAxisPosi.xml"); ;
            equip.PROC.TopIRCutStage.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[8]
            {
                 new ServoPosiInfo() {No = 0, Name = "B 가공 테이블 : 셀 로드 위치 (Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "B 가공 테이블 : 셀 언로드 위치 (Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "B 가공 테이블1: 레이져 샷 (Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 3, Name = "B 가공 테이블1: 카메라 확인 (Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 4, Name = "B 가공 테이블2: 레이져 샷 (Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 5, Name = "B 가공 테이블2: 카메라 확인 (Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 6, Name = "B Fine Align 티칭 위치 (Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 7, Name = "LDS 측정 Y:  LDS 기준 측정위치(Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopIRCutStage.YAxis},
            };
            if (equip.PROC.TopIRCutStage.YAxis.Setting.Load() == false) errCnt++;

            //BotIRCutStage.YAxis 설정 1
            equip.PROC.BotIRCutStage.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotIRCutStageYAxisPosi.xml"); ;
            equip.PROC.BotIRCutStage.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[7]
            {
                 new ServoPosiInfo() {No = 0, Name = "A 가공 테이블 : 셀 로드 위치 (Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "A 가공 테이블 : 셀 언로드 위치 (Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "A 가공 테이블1: 레이져 샷 (Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 3, Name = "A 가공 테이블1: 카메라 확인 (Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 4, Name = "A 가공 테이블2: 레이져 샷 (Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 5, Name = "A 가공 테이블2: 카메라 확인 (Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotIRCutStage.YAxis},
                 new ServoPosiInfo() {No = 6, Name = "A Fine Align 티칭 위치 (Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotIRCutStage.YAxis},
             };
            if (equip.PROC.BotIRCutStage.YAxis.Setting.Load() == false) errCnt++;

            //LaserHead.XAxis 설정
            equip.PROC.LaserHead.XAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "LaserHeadXAxisPosi.xml"); ;
            equip.PROC.LaserHead.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[5]
            {
                 new ServoPosiInfo() {No = 0, Name = "A 가공 테이블1: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 1, Name = "A 가공 테이블2: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 3, Name = "B 가공 테이블1: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 4, Name = "B 가공 테이블2: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 5, Name = "파워미터 측정 위치 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
            };
            if (equip.PROC.LaserHead.XAxis.Setting.Load() == false) errCnt++;

            //LaserHead.ZAxis 설정
            equip.PROC.LaserHead.ZAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "LaserHeadZAxisPosi.xml"); ;
            equip.PROC.LaserHead.ZAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                 new ServoPosiInfo() {No = 0, Name = "파워미터 측정 위치 (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.ZAxis},
            };
            if (equip.PROC.LaserHead.ZAxis.Setting.Load() == false) errCnt++;

            //FineAlgin.XAxis 설정
            equip.PROC.FineAlgin.XAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "FineAlginXAxisPosi.xml"); ;
            equip.PROC.FineAlgin.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[7]
            {
                 new ServoPosiInfo() {No = 0, Name = "A 가공 테이블1: 카메라 확인 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 new ServoPosiInfo() {No = 1, Name = "A 가공 테이블2: 카메라 확인 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 new ServoPosiInfo() {No = 2, Name = "B 가공 테이블1: 카메라 확인 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 new ServoPosiInfo() {No = 3, Name = "B 가공 테이블2: 카메라 확인 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 new ServoPosiInfo() {No = 4, Name = "A Fine Align 티칭 위치 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 new ServoPosiInfo() {No = 5, Name = "B Fine Align 티칭 위치 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 new ServoPosiInfo() {No = 6, Name = "LDS 측정 X:  LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
            };
            if (equip.PROC.FineAlgin.XAxis.Setting.Load() == false) errCnt++;
            ///////////////////////////////////////////////////////////



            //TopAfterIRCutTransfer.YAxis 설정
            equip.PROC.TopAfterIRCutTransfer.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopAfterIRCutTransferYAxisPosi.xml"); ;
            equip.PROC.TopAfterIRCutTransfer.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "After IR 이재기 겐트리 : 셀 로드 위치(Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopAfterIRCutTransfer.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "After IR 이재기 겐트리 : 셀 언로드 위치(Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopAfterIRCutTransfer.YAxis},
            };
            if (equip.PROC.TopAfterIRCutTransfer.YAxis.Setting.Load() == false) errCnt++;

            //BotAfterIRCutTransfer.YAxis 설정
            equip.PROC.BotAfterIRCutTransfer.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotAfterIRCutTransferYAxisPosi.xml"); ;
            equip.PROC.BotAfterIRCutTransfer.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "After IR 이재기 겐트리 : 셀 로드 위치(Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotAfterIRCutTransfer.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "After IR 이재기 겐트리 : 셀 언로드 위치(Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotAfterIRCutTransfer.YAxis},
            };
            if (equip.PROC.BotAfterIRCutTransfer.YAxis.Setting.Load() == false) errCnt++;

            // Equipment에서 추가 시켜줘야함....
            //TopAfterIRCutTransfer.XAxis 설정
            //equip.PROC.TopAfterIRCutTransfer.XAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopAfterIRCutTransferXAxisPosi.xml"); ;
            //equip.PROC.TopAfterIRCutTransfer.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            //{
            //   new ServoPosiInfo() { No = 0, Name = "A After IR 이재기 X1 : 셀 로드 위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopAfterIRCutTransfer.XAxis },
            //   new ServoPosiInfo() { No = 1, Name = "B After IR 이재기 X1 : 셀 로드 위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopAfterIRCutTransfer.XAxis },
            //   new ServoPosiInfo() { No = 3, Name = "A After IR 이재기 X1 : 셀 언로드 위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopAfterIRCutTransfer.XAxis },
            //   new ServoPosiInfo() { No = 4, Name = "B After IR 이재기 X1 : 셀 언로드 위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopAfterIRCutTransfer.XAxis },
            //};
            //if (equip.PROC.TopAfterIRCutTransfer.Setting.Load() == false) errCnt++;

            //BotAfterIRCutTransfer.XAxis 설정
            //equip.PROC.BotAfterIRCutTransfer.XAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotAfterIRCutTransferXAxisPosi.xml"); ;
            //equip.PROC.BotAfterIRCutTransfer.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            //{
            //     new ServoPosiInfo() {No = 0, Name = "A After IR 이재기 X2 : 셀 로드 위치(X2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotAfterIRCutTransfer.XAxis},
            //     new ServoPosiInfo() {No = 1, Name = "B After IR 이재기 X2 : 셀 로드 위치(X2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotAfterIRCutTransfer.XAxis},
            //     new ServoPosiInfo() {No = 3, Name = "A After IR 이재기 X2 : 셀 언로드 위치(X2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotAfterIRCutTransfer.XAxis},
            //     new ServoPosiInfo() {No = 4, Name = "B After IR 이재기 X2 : 셀 언로드 위치(X2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotAfterIRCutTransfer.XAxis},
            //};
            //if (equip.PROC.BotAfterIRCutTransfer.XAxis.Setting.Load() == false) errCnt++;

            #region BreakingHead
            //BreakingHead.Z1Axis 설정
            equip.PROC.BreakingHead.Z1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BreakingHeadZ1AxisPosi.xml"); ;
            equip.PROC.BreakingHead.Z1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "Z1: 비젼 확인 위치 (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z1Axis},
                 new ServoPosiInfo() {No = 1, Name = "Z1: 브레이킹 스테이지 위치 (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z1Axis},
            };
            if (equip.PROC.BreakingHead.Z1Axis.Setting.Load() == false) errCnt++;

            //BreakingHead.Z2Axis 설정
            equip.PROC.BreakingHead.Z2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BreakingHeadZ2AxisPosi.xml"); ;
            equip.PROC.BreakingHead.Z2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "Z2: 비젼 확인 위치 (Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z2Axis},
                 new ServoPosiInfo() {No = 1, Name = "Z2: 브레이킹 스테이지 위치 (Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z2Axis},
            };
            if (equip.PROC.BreakingHead.Z2Axis.Setting.Load() == false) errCnt++;

            //BreakingHead.Z1Axis 설정
            //equip.PROC.BreakingHead.Z3Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BreakingHeadZ3AxisPosi.xml"); ;
            //equip.PROC.BreakingHead.Z3Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //     new ServoPosiInfo() {No = 0, Name = "Z1: 비젼 확인 위치 (Z3)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z3Axis},
            //     new ServoPosiInfo() {No = 1, Name = "Z1: 브레이킹 스테이지 위치 (Z3)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z3Axis},
            //};
            //if (equip.PROC.BreakingHead.Z3Axis.Setting.Load() == false) errCnt++;

            //BreakingHead.Z2Axis 설정
            //equip.PROC.BreakingHead.Z4Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BreakingHeadZ4AxisPosi.xml"); ;
            //equip.PROC.BreakingHead.Z4Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //     new ServoPosiInfo() {No = 0, Name = "Z2: 비젼 확인 위치 (Z4)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z4Axis},
            //     new ServoPosiInfo() {No = 1, Name = "Z2: 브레이킹 스테이지 위치 (Z4)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z4Axis},
            //};
            //if (equip.PROC.BreakingHead.Z4Axis.Setting.Load() == false) errCnt++;
            #endregion




            // 브레이크 유닛 (Y, T축) 8번축, 35 36 37 38번축 없음-------------------------------
            // BotBreakStage.YAxis 설정
            equip.PROC.BotBreakStage.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotBreakStageYAxis.xml"); ;
            equip.PROC.BotBreakStage.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 테이블: 셀 로드 위치 (Y1)",         Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotBreakStage.YAxis},
                     new ServoPosiInfo() {No = 0, Name = "A 테이블: 셀 언로드 위치 (Y1)",       Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotBreakStage.YAxis},
                     new ServoPosiInfo() {No = 0, Name = "A Align Cam 기준 위치(Y1)",           Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotBreakStage.YAxis},
                     new ServoPosiInfo() {No = 0, Name = "LDS 측정 Y: LDS 기준 측정위치(Y1)",   Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BotBreakStage.YAxis},
            };
            if (equip.PROC.BotBreakStage.YAxis.Setting.Load() == false) errCnt++;

            // TopBreakStage.YAxis 설정
            equip.PROC.TopBreakStage.YAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopBreakStageYAxis.xml"); ;
            equip.PROC.TopBreakStage.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "B 테이블: 셀 로드 위치(Y2)",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopBreakStage.YAxis},
                     new ServoPosiInfo() {No = 0, Name = "B 테이블: 셀 언로드 위치(Y2)",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopBreakStage.YAxis},
                     new ServoPosiInfo() {No = 0, Name = "B Align Cam 기준 위치(Y2)",       Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.TopBreakStage.YAxis},
            };
            if (equip.PROC.TopBreakStage.YAxis.Setting.Load() == false) errCnt++;


            //--------------------------------------------------------------

            #region unloadertr
            //T축 필요
            // BotUnloaderTransfer.Y1Axis 설정
            equip.UD.BotUnloaderTransfer.Y1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotUnloaderTransfer.Y1Axis.xml"); ;
            equip.UD.BotUnloaderTransfer.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y1) : A 스테이지", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotUnloaderTransfer.Y1Axis},
            };
            if (equip.UD.BotUnloaderTransfer.Y1Axis.Setting.Load() == false) errCnt++;

            // TopUnloaderTransfer.Y2Axis 설정
            equip.UD.TopUnloaderTransfer.Y2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopUnloaderTransferY2Axis.xml"); ;
            equip.UD.TopUnloaderTransfer.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y2) : B 스테이지", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopUnloaderTransfer.Y2Axis},
            };
            if (equip.UD.TopUnloaderTransfer.Y2Axis.Setting.Load() == false) errCnt++;

            // BotUnloaderTransfer.Y1Axis 설정
            equip.UD.BotUnloaderTransfer.Y1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotUnloaderTransfer.Y1Axis.xml"); ;
            equip.UD.BotUnloaderTransfer.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y1) : 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotUnloaderTransfer.Y1Axis},
            };
            if (equip.UD.BotUnloaderTransfer.Y1Axis.Setting.Load() == false) errCnt++;

            // TopUnloaderTransfer.Y2Axis 설정
            equip.UD.TopUnloaderTransfer.Y2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopUnloaderTransferY2Axis.xml"); ;
            equip.UD.TopUnloaderTransfer.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y2) : 언로딩", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopUnloaderTransfer.Y2Axis},
            };
            if (equip.UD.TopUnloaderTransfer.Y2Axis.Setting.Load() == false) errCnt++;
            #endregion

            #region 카메라유닛 MCR 없음 -------------------------------------------
            // InspCamera.XAxis 설정
            equip.UD.InspCamera.XAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "InspCameraXAxisPosi.xml"); ;
            equip.UD.InspCamera.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[8]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카메라 이재기 (X1) : 인스펙션 1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카메라 이재기 (X1) : 인스펙션 2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 3, Name = "B 카메라 이재기 (X1) : 인스펙션 1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 4, Name = "B 카메라 이재기 (X1) : 인스펙션 2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 6, Name = "A Insp Align 티칭 위치1 (X1)",      Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 7, Name = "A Insp Align 티칭 위치2 (X1)",      Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 8, Name = "B Insp Align 티칭 위치3 (X1)",      Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 9, Name = "B Insp Align 티칭 위치4 (X1)",      Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
            };
            if (equip.UD.InspCamera.XAxis.Setting.Load() == false) errCnt++;

            // InspCamera.ZAxis 설정
            equip.UD.InspCamera.ZAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "InspCameraZAxisPosi.xml"); ;
            equip.UD.InspCamera.ZAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카메라 이재기 (Z1) : 인스펙션 1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카메라 이재기 (Z1) : 인스펙션 2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
                     new ServoPosiInfo() {No = 0, Name = "B 카메라 이재기 (Z1) : 인스펙션 1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
                     new ServoPosiInfo() {No = 1, Name = "B 카메라 이재기 (Z1) : 인스펙션 2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
            };
            if (equip.UD.InspCamera.ZAxis.Setting.Load() == false) errCnt++;

            // BotUnloaderTransfer.Y1Axis 설정
            equip.UD.BotUnloaderTransfer.Y1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotUnloaderTransfer.Y1Axis.xml"); ;
            equip.UD.BotUnloaderTransfer.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 언로딩 이재기 (Y1) : 인스펙션", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotUnloaderTransfer.Y1Axis},
            };

            // TopUnloaderTransfer.Y2Axis 설정
            equip.UD.TopUnloaderTransfer.Y2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopUnloaderTransferY2Axis.xml"); ;
            equip.UD.TopUnloaderTransfer.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "B 언로딩 이재기 (Y2) : 인스펙션", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopUnloaderTransfer.Y2Axis},
            };
            //-------------------------------------------------------------------
            #endregion

            #region CstUnloader
            //CstUDRotationAxis 설정 
            equip.UD.TopCstUnloader.CstRotationAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopCstUnloaderCstRotationAxisPosi.xml"); ;
            equip.UD.TopCstUnloader.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "B열 AA", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopCstUnloader.CstRotationAxis},
                 new ServoPosiInfo() {No = 1, Name = "B열 BB", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopCstUnloader.CstRotationAxis},
                 new ServoPosiInfo() {No = 2, Name = "B열 CC", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopCstUnloader.CstRotationAxis},
             };
            if (equip.UD.TopCstUnloader.CstRotationAxis.Setting.Load() == false) errCnt++;


            equip.UD.BotCstUnloader.CstRotationAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotCstUnloaderCstRotationAxisPosi.xml"); ;
            equip.UD.BotCstUnloader.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "A열 AA", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotCstUnloader.CstRotationAxis},
                     new ServoPosiInfo() {No = 1, Name = "A열 BB", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotCstUnloader.CstRotationAxis},
                     new ServoPosiInfo() {No = 2, Name = "A열 CC", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotCstUnloader.CstRotationAxis},
             };
            if (equip.UD.BotCstUnloader.CstRotationAxis.Setting.Load() == false) errCnt++;

            //CstUDUpDownAxis 설정 
            equip.UD.TopCstUnloader.CstUpDownAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "TopCstUnloaderCstUpDownAxisPosi.xml"); ;
            equip.UD.TopCstUnloader.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "B열 AA(Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopCstUnloader.CstUpDownAxis},
                 new ServoPosiInfo() {No = 1, Name = "B열 BB(Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopCstUnloader.CstUpDownAxis},
                 new ServoPosiInfo() {No = 2, Name = "B열 CC(Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.TopCstUnloader.CstUpDownAxis},
             };
            if (equip.UD.TopCstUnloader.CstUpDownAxis.Setting.Load() == false) errCnt++;


            equip.UD.BotCstUnloader.CstUpDownAxis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "BotCstUnloaderCstUpDownAxisPosi.xml"); ;
            equip.UD.BotCstUnloader.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "A열 AA(Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotCstUnloader.CstUpDownAxis},
                     new ServoPosiInfo() {No = 1, Name = "A열 BB(Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotCstUnloader.CstUpDownAxis},
                     new ServoPosiInfo() {No = 2, Name = "A열 CC(Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BotCstUnloader.CstUpDownAxis},
             };
            if (equip.UD.BotCstUnloader.CstUpDownAxis.Setting.Load() == false) errCnt++;
            #endregion

            #region Unloder
            //Unloader.X1Axis 설정 
            equip.UD.Unloader.X1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "UnloaderX1AxisPosi.xml"); ;
            equip.UD.Unloader.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "투입 이재기 (X1) : A 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "투입 이재기 (X1) : B 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "투입 이재기 (X1) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
             };
            if (equip.UD.TopCstUnloader.CstUpDownAxis.Setting.Load() == false) errCnt++;

            //Unloader.X2Axis 설정
            equip.UD.Unloader.X2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "UnloaderX2AxisPosi.xml"); ;
            equip.UD.Unloader.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "투입 이재기 (X2) : A 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 1, Name = "투입 이재기 (X2) : B 카세트 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 2, Name = "투입 이재기 (X2) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
             };
            if (equip.UD.BotCstUnloader.CstUpDownAxis.Setting.Load() == false) errCnt++;

            //Unloader.Y1Axis 설정 
            equip.UD.Unloader.Y1Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "UnloaderY1AxisPosi.xml"); ;
            equip.UD.Unloader.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
             {
                 new ServoPosiInfo() {No = 0, Name = "투입 이재기 (Y1) : A 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 1, Name = "투입 이재기 (Y1) : B 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 2, Name = "투입 이재기 (Y1) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 3, Name = "투입 이재기 (Y1) : 2mm 후퇴 속도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
             };
            if (equip.UD.TopCstUnloader.CstUpDownAxis.Setting.Load() == false) errCnt++;

            //Unloader.Y2Axis 설정
            equip.UD.Unloader.Y2Axis.Setting.XmlPath = Path.Combine(Application.StartupPath, "Setting", "UnloaderY2AxisPosi.xml"); ;
            equip.UD.Unloader.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                     new ServoPosiInfo() {No = 0, Name = "투입 이재기 (Y2) : A 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                     new ServoPosiInfo() {No = 1, Name = "투입 이재기 (Y2) : B 카세트 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                     new ServoPosiInfo() {No = 2, Name = "투입 이재기 (Y2) : 언로딩 이재기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                     new ServoPosiInfo() {No = 3, Name = "투입 이재기 (Y2) : 2mm 후퇴 속도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
             };
            if (equip.UD.BotCstUnloader.CstUpDownAxis.Setting.Load() == false) errCnt++;
            #endregion

            return errCnt <= 0;
        }
    }
}

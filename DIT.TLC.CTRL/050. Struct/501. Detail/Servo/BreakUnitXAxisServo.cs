﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class BreakUnitXAxisServo : ServoMotorControl
    {
        public static int A_Vision_Align_X1  {get;set;}
        public static int A_Vision_Align_X2  {get;set;}
        public static int B_Vision_Align_X3  {get;set;}
        public static int B_Vision_Align_X4  {get;set;}
        public static int LDS_Break_Measure_X { get; set; }

        public BreakUnitXAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            //A_Vision_Align_X1            = 1;
            //A_Vision_Align_X2            = 2;
            //B_Vision_Align_X3            = 3;
            //B_Vision_Align_X4            = 4;
            //LDS_Break_Measure_X           = 5;


            //base.MoveActionName[0] = "VISION_ALIGN_X1";                     // A Align Cam 기준 위치(X1)
            //base.MoveActionName[1] = "VISION_ALIGN_X2";                     // A Align Cam 기준 위치(X2)
            //base.MoveActionName[2] = "VISION_ALIGN_X3";                     // B Align Cam 기준 위치(X3)
            //base.MoveActionName[3] = "VISION_ALIGN_X4";                     // B Align Cam 기준 위치(X4)
            //base.MoveActionName[4] = "LDS_Break_X";                         // LDS 측정 X : LDS 기준 측정위치(X1)
        }
    }
}
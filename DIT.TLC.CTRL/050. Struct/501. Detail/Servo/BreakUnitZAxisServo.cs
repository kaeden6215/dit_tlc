﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    //Breaking 유닛
    public class BreakUnitZAxisServo : ServoMotorControl
    {
        public static int Z1_Vision_Check  {get;set;}
        public static int Z1_Stage         {get;set;}
        public static int Z2_Vision_Check  {get;set;}
        public static int Z2_Stage         {get;set;}
        public static int Z3_Vision_Check  {get;set;}
        public static int Z3_Stage         {get;set;}
        public static int Z4_Vision_Check  {get;set;}
        public static int Z4_Stage         {get; set;}


        public BreakUnitZAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;


            // Z1_Vision_Check       = 1;
            // Z1_Stage              = 2;
            // Z2_Vision_Check       = 3;
            // Z2_Stage              = 4;
            // Z3_Vision_Check       = 5;
            // Z3_Stage              = 6;
            // Z4_Vision_Check       = 7;
            // Z4_Stage              = 8;


            //base.MoveActionName[0] = "VISION_Z1";               // Z1 : 비젼 확인 위치(Z1)       
            //base.MoveActionName[1] = "BREAK_STAGE_Z1";          // Z1 : 브레이킹 스테이지 위치(Z1)  
            //base.MoveActionName[2] = "VISION_Z2";               // Z2 : 비젼 확인 위치(Z2)       
            //base.MoveActionName[3] = "BREAK_STAGE_Z2";          // Z2 : 브레이킹 스테이지 위치(Z2)  
            //base.MoveActionName[4] = "VISION_Z3";               // Z3 : 비젼 확인 위치(Z3)       
            //base.MoveActionName[5] = "BREAK_STAGE_Z3";          // Z3 : 브레이킹 스테이지 위치(Z3)    
            //base.MoveActionName[6] = "VISION_Z4";               // Z4 : 비젼 확인 위치(Z4)       
            //base.MoveActionName[7] = "BREAK_STAGE_Z4";          // Z4 : 브레이킹 스테이지 위치(Z4)     

        }

    }
}
















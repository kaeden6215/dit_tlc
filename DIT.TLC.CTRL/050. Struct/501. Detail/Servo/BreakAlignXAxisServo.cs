﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public class BreakAlignXAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "ScanXServo.ini");


        private bool _isLoadingOnPosition = false;


        public static int A_After_IR_Load_X1 { get; set; }
        public static int A_After_IR_Load_X2 { get; set; }
        public static int B_After_IR_Load_X1 { get; set; }
        public static int B_After_IR_Load_X2 { get; set; }

        public static int A_After_IR_UnLoad_X1 { get; set; }
        public static int A_After_IR_UnLoad_X2 { get; set; }
        public static int B_After_IR_UnLoad_X1 { get; set; }
        public static int B_After_IR_UnLoad_X2 { get; set; }

        public BreakAlignXAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo,  name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            //A_After_IR_Load_X1 = 1;
            //A_After_IR_Load_X2 = 2;
            //B_After_IR_Load_X1 = 3;
            //B_After_IR_Load_X2 = 4;
            //A_After_IR_UnLoad_X1 = 5;
            //A_After_IR_UnLoad_X2 = 6;
            //B_After_IR_UnLoad_X1 = 7;
            //B_After_IR_UnLoad_X2 = 8;

            //base.MoveActionName[0] = "Load_X1_Left";
            //base.MoveActionName[1] = "Load_X2_Left";
            //base.MoveActionName[2] = "Load_X1_Right";
            //base.MoveActionName[3] = "Load_X2_Right";
            //base.MoveActionName[4] = "UnLoad_X1_Left";
            //base.MoveActionName[5] = "UnLoad_X2_Left";
            //base.MoveActionName[4] = "UnLoad_X1_Right";
            //base.MoveActionName[5] = "UnLoad_X2_Right";
        }
    }
}




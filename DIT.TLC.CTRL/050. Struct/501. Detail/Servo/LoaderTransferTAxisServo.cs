﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public class LoaderTransferTAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "LoaderTransferTAxisServo .ini");

        public static int Loader_Laoding_Y1 { get; set; }
        public static int Loader_UnLaoding_Y1 { get; set; }
        public static int Loader_A_Piker_Loading_MY { get; set; } // MINIY
        public static int Loader_A_Piker_UnLoading_MY { get; set; }
        public static int Loader_B_Piker_Loading_MY { get; set; } // MINIY
        public static int Loader_B_Piker_UnLoading_MY { get; set; }
        public static int Loader_PreAlign_Y1 { get; set; }


        private bool _isLoadingOnPosition = false;

        public LoaderTransferTAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            //Loader_Laoding_Y1                       = 1;
            //Loader_UnLaoding_Y1                     = 2;
            //Loader_A_Piker_Loading_MY               = 3;
            //Loader_A_Piker_UnLoading_MY             = 4;
            //Loader_B_Piker_Loading_MY               = 5;
            //Loader_B_Piker_UnLoading_MY             = 6;
            //Loader_PreAlign_Y1                      = 7;

            //base.MoveActionName[0] = "Y1_Laod";                   //   로더 이재기(Y1) : 로딩
            //base.MoveActionName[1] = "Y1_Unlaod";                //   로더 이재기(Y1) : 언로딩
            //base.MoveActionName[2] = "Picker_L_Y_Load";          //   로더 이재기(MY1) : A 피커 로딩
            //base.MoveActionName[3] = "Picker_L_Y_Unload";        //   로더 이재기(MY1) : A 피커 언로딩
            //base.MoveActionName[4] = "Picker_R_Y_Load";          //   로더 이재기(MY2) : B 피커 로딩
            //base.MoveActionName[5] = "Picker_R_Y_Unload";        //   로더 이재기(MY2) : B 피커 언로딩
            //base.MoveActionName[6] = "Picker_PreAlignY1";        //   로더 이재기(Y1) : PreAlign Y1F
        }

    }
}
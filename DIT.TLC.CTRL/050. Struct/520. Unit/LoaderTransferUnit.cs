﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{

    public enum EmLoaderTrnasferUnitCmd
    {
        S0000_WAIT,
        S1000_RECV_START,
        S2000_RECV_ING,
        S3000_RECV_ING,
        S4000,
        S5000,
        S6000,
        S7000,
        S8000,
        S9000,
        S0900,
        S0100
    }
    public class LoaderTransferUnit : BaseUnit
    {
        public LoaderTransferYAxisServo YAxis { get; set; }


        //Align 보정 처리..
        public LoaderTransferXAxisServo X1Axis { get; set; }
        public LoaderTransferXAxisServo X2Axis { get; set; }


        public LoaderTransferXAxisServo SubY1Axis { get; set; }
        public LoaderTransferXAxisServo SubY2Axis { get; set; }


        public LoaderTransferTAxisServo SubT1Axis { get; set; }
        public LoaderTransferTAxisServo SubT2Axis { get; set; }
        


        public Cylinder UpDonw1Cylinder = new Cylinder();
        public Cylinder UpDonw2Cylinder = new Cylinder();


        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();

        public EmLoaderTrnasferUnitCmd StepCmd { get; set; }

        public PioSendStep TPioSend { get; set; }
        public PioSendStep BPioSend { get; set; }
        public PioRecvStep PioRecv { get; set; }
        public PlcAddr HandPressureValue1 { get; internal set; }
        public PlcAddr HandPressureValue2 { get; internal set; }

        public override void LogicWorking(Equipment equip)
        {
            YAxis.LogicWorking(equip);
            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);

            SubY1Axis.LogicWorking(equip);
            SubY2Axis.LogicWorking(equip);

            SubT1Axis.LogicWorking(equip);
            SubT2Axis.LogicWorking(equip);

            UpDonw1Cylinder.LogicWorking(equip);
            UpDonw2Cylinder.LogicWorking(equip);
            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);

        }
    }
}

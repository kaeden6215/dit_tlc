﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmCellPosi
    {
        One_1,
        One_2,
        Two,
        None
    }


    public class PioSendStep
    {

        public PanelInfoSet SendPanelInfoSet { get; set; }
        public PioRecvStep LowerPioRecv { get; set; }

        public string   Name { get; set; }       

        public bool YSendAble { get; set; }
        public bool YSendStart { get; set; }
        public bool YSendComplete { get; set; }

        public bool XRecvAble { get { return LowerPioRecv.YRecvAble; } }
        public bool XRecvStart { get { return LowerPioRecv.YRecvStart; } }
        public bool XRecvComplete { get { return LowerPioRecv.YRecvComplete; } }
        public PioSendStep()
        {
            Name = string.Empty;
        }
    }
}

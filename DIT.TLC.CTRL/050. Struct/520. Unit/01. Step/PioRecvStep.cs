﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class PioRecvStep
    {
        public BaseUnit SendUnit { get; set; }
        public PioSendStep UpperPioSend { get; set; }

        public PanelInfoSet RecvPanelInfoSet { get {
                                
                return UpperPioSend.SendPanelInfoSet.Clone() as PanelInfoSet; } }

        public string Name { get; set; }

        public bool YRecvAble { get; set; }
        public bool YRecvStart { get; set; }
        public bool YRecvComplete { get; set; }

        public bool XSendAble { get { return UpperPioSend.YSendAble; } }
        public bool XSendStart { get { return UpperPioSend.YSendStart; } }
        public bool XSendComplete { get { return UpperPioSend.YSendComplete; } }

        public PioRecvStep()
        {
            Name = string.Empty;
        }
    }
}

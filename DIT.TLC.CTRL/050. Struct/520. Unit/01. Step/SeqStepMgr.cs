﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmStepSwitch
    {
        CstLoadLogicWorking,
        CstUnloadLogicWorking,
        CstMeasureSeqLogicWorking,
        CstCellInOutLogicWorking,


        LoaderCellLdLogicWorking,
        LoaderCellUdLogicWorking,


        LoaderTrCellLdLogicWorking,
        LoaderTrCellPreAlignLogicWorking,
        LoaderTrCellMcrLogicWorking,
        LoaderTrCellUdLogicWorking,


        IRCutStageCellLdLogicWorking,
        IRCutStageCellFinAlingLogicWorking,
        IRCutStageCellFienAlignLogicWorking,
        IRCutStageCellUldLogicWorking,
        IRCutStageHomeSeqLogicWorking,


        BreakStageHomeLogicWorking,


        AfterIRCutTrCellLdLogicWorking,
        AfterIRCutTrCellUldLogicWorking,
        AfterIRCutTrHomeSeqLogicWorking,

        CstExistPortHommingLogicWorking,
        CstCommandHommingLogicWorking,
        
        AfterInspTrCellUldLogicWorking,
        BeforeInspTrCellLdLogicWorking,
        BeforeInspTrCellUldLogicWorking,
        BeforeInspTrHomeSeqLogicWorking,
        AfterInspTrCellLdLogicWorking,
        LoaderTrHomeLogicWorking,


        BreakStageCellLdLogicWorking,
        BreakStageBreakAlignLogicWorking,
        BreakStageBreakingnLogicWorking,
        BreakStageCellUldLogicWorking,

        LoaderPreAlignLogicWorking,
        LoaderFineAlignLogicWoorking,
        ProcBreakAlignLogicWorking,

        BreakingHeadLogicWorking,
        ProcLaserCuttingLogicWorking,

        InspStageCellLdLogicWorking,
        InspStageCellInspLogicWorking,
        InspStageCellUldLogicWorking,

        UDInspCameraLogicWorking,
    }

    public class SeqStepMgr
    {
        //CST 로더 

        static bool[] _AColEnabelStep = new bool[100];
        static bool[] _BColEnabelStep = new bool[100];
        static bool[] _NoneEnabelStep = new bool[100];

        public static bool StepSwitch(EmUnitPosi unitPosi, EmStepSwitch step)
        {
            return true;

            if (unitPosi == EmUnitPosi.ACol)
                return _AColEnabelStep[(int)step];
            else if (unitPosi == EmUnitPosi.BCol)
                return _BColEnabelStep[(int)step];
            else if (unitPosi == EmUnitPosi.None)
                return _NoneEnabelStep[(int)step];
            else
                return false;
        }

        public static void SetStepSwitch(EmUnitPosi unitPosi, EmStepSwitch step, bool enable)
        {
            if (unitPosi == EmUnitPosi.ACol)
                _AColEnabelStep[(int)step] = enable;
            else if (unitPosi == EmUnitPosi.BCol)
                _BColEnabelStep[(int)step] = enable;
            else if (unitPosi == EmUnitPosi.None)
                _NoneEnabelStep[(int)step] = enable;
        }
    }
}

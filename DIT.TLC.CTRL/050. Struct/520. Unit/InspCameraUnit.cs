﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class InspCameraUnit : BaseUnit
    {
        public InspCameraXAxisServo XAxis { get; set; }
        public InspCameraZAxisServo ZAxis { get; set; }        
        
        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);
            ZAxis.LogicWorking(equip);
        }
    }
}

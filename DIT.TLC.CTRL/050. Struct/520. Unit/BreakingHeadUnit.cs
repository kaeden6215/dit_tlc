﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class BreakingHeadUnit : BaseUnit
    {
        public Cylinder HeaderUpDown                                                  /**/   = new Cylinder();
        public Sensor HeaderX1X2충돌방지                                             /**/    = new Sensor();
        
        public BreakUnitXAxisServo X1Axis { get; set; }
        public BreakUnitXAxisServo X2Axis { get; set; }
        
        public BreakUnitZAxisServo Z1Axis { get; set; }
        public BreakUnitZAxisServo Z2Axis { get; set; }


        public override void LogicWorking(Equipment equip)
        {
            HeaderUpDown.LogicWorking(equip);
            HeaderX1X2충돌방지.LogicWorking(equip);

            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
            Z1Axis.LogicWorking(equip);
            Z2Axis.LogicWorking(equip);            
        }
    }
}

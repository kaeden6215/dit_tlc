﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    //아진보드
    public enum EmCstLoaderUpDownAxisServo
    {
        Cst_Loading             /**/= 0,
        Cst_Measure             /**/= 1,
        Cst_CellOut             /**/= 2,
        
        //계산된 위치값
        Cal_Cst_CellOut_Wait    /**/= 3,  //Loader Y축이 전진 가능
        Cal_Cst_CellOut         /**/= 4,  //Loader Y축이 후진 가능
    }
    public class CstLoaderUpDownAxisServo : ServoMotorControl
    {
        public CstLoaderUpDownAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {
            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 400;
            SoftSpeedLimit = 250;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }

        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmCstLoaderUpDownAxisServo.Cal_Cst_CellOut_Wait == posiNo || (int)EmCstLoaderUpDownAxisServo.Cal_Cst_CellOut == posiNo)
            {
                PortInfo port = opt as PortInfo;
                if (port == null)
                {
                    InterLockMgr.AddInterLock(" Cst_Loader_CalCellOutPosi 이동 명령시, Port 정보가 없습니다. ");
                    return null;
                }

                /*계산 처리*/
                int iCol = 0;
                int iRow = 0;   // 기존값 0 잠시 변경
                iCol = Math.DivRem(port.Cst.OutCount, (int)recp.CassetteMaxHeightCount, out iRow);

                ServoPosiInfo info = GetCalPosition(EmCstLoaderUpDownAxisServo.Cst_CellOut, recp, null);
                float position = (float)(info.Position + (iCol * recp.CassetteCellOutWaitHitch));

                ServoPosiInfo target = new ServoPosiInfo();

                if ((int)EmCstLoaderUpDownAxisServo.Cal_Cst_CellOut_Wait == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("CELL OUT 대기 위치 {0} SLOT, Out Cell {1}  ", iRow, port.Cst.OutCount),
                        Position = position + iRow * 20 + (float)recp.CassetteCellOutWaitHitch,
                        Speed = info.Speed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmCstLoaderUpDownAxisServo.Cal_Cst_CellOut == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("CELL OUT 위치 {0} SLOT, Out Cell {1}  ", iRow, port.Cst.OutCount),
                        Position = position + iRow * 20 - (float)recp.CassetteCellOutUpHitch,
                        Speed = info.Speed,
                        Accel = info.Accel,
                    };
                }
                return target;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }


        public bool IsInPosition(EmCstLoaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmCstLoaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmCstLoaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmCstLoaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}


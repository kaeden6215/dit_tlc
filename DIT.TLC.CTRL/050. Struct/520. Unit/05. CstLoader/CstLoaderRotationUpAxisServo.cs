﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    //아진보드
    public enum EmCstLoaderRotationUpAxisServo
    {
        R_0dgr      /**/= 0,
        R_90dgr     /**/= 1,
    }

    public class CstLoaderRotationUpAxisServo : ServoMotorControl
    {
        public CstLoaderRotationUpAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 10; // control 1당 0.1도 이동임.
            SoftMinusLimit = 0;
            SoftPlusLimit = 250;
            SoftSpeedLimit = 83;
            SoftJogSpeedLimit = 83;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }

        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            return base.GetCalPosition(posiNo, recp, opt);
        }
        public bool IsInPosition(EmCstLoaderRotationUpAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return info == null ? false : IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmCstLoaderRotationUpAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmCstLoaderRotationUpAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmCstLoaderRotationUpAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}


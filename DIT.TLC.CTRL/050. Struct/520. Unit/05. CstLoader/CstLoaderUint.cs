﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstLoaderUint : BaseUnit
    {
        public enum EmCstLoaderHomeStep
        {
            H0000,
            H0010,
            H0020,

            //카셋트가 있을때. HOME 동작
            H1000_CST_PORT_EXIST,
            H1010,
            H1030,
            H3010,
            H1050,
            H3030,


            H3000_AXIS_HOME_START,
            H2010,
            H2030,
            H2040,
            H2050,
            H2060,


            //HOME 동작 완료
            H5999_HOME_COMPLETE,
            H1070,
            H2000_CST_PORT_EMTYP,
            H3020,
            H3040,
            H6000,
            H3050,
            H3060,
            H5010,
            H5020,
            H5030,
            H6010,
            H3080,
            H5000,
            H3025,
            H3070,
        }
        public enum EmCstLoaderStep
        {
            S0000_CST_WAIT,

            //카셋트 로드
            S1000_CST_TO_LIFT_WAIT,
            S1010_CST_TO_LIFT_START,
            S1999_CST_TO_LIFT_END,

            //카셋트 틸트 측정
            S2010_CST_MEASURE_START,
            S2020_MEASURE_POS_MOVE,
            S2030_TILT_CYLINDER_FORWARD_CHECK,
            S2040_TILT_SENSOR_RESULT_CHECK,
            S2050_FIXED_BARCODE_CMD_SEND,
            S2060_FIXED_BARCODE_RESULT_WAIT,
            S2070_PPID_MATCHING_CHECK,
            S2080_TILT_CYLINDER_BACKWARD,
            S2090_TILT_CYLINDER_BACKWARD_CHECK,
            S2100_CST_MEASURE_COMPLETE,
            S2999_CEL_CST_TO_LD_END,

            //카셋트 CELL OUT
            S5010_CEL_CST_CELL_INOUT_START,
            S5020_,
            S5030_,

            //카셋트_언로드
            S6010_CEL_CST_TO_ULD_WAIT,
            S5040_,
            S5050_,
            S5060_,
            S5070_,
            S5015_,
            S5080_,
            S5100_,
            S5999_,
            S6030_,
            S6020_,
            S6080_,
            S6040_,
            S1000_CST_LD_WAIT,
            S1010_,
            S1020_,
            S1050_,
            S5045_,
            S1060_,
            S5090_,
            S5110_,
            S5130_,
        }

        public PortInfo Port                                                        /**/   = new PortInfo(EmUnitPosi.None, "");

        public Cylinder TiltCylinder                                                /**/   = new Cylinder();

        public Sensor LDCstDetectOGamJi                                             /**/   = new Sensor();  //오 투입 감지 센서
        public Sensor LDCstDetectExist                                              /**/   = new Sensor();  //카셋트 유무 확인

        public CylinderTwo CstGripCylinder                                          /**/   = new CylinderTwo();

        public SwitchOneWay CstInOutBuzzer                                          /**/   = new SwitchOneWay();
        //public SwitchOneWay MutingLamp                                              /**/   = new SwitchOneWay();

        //public Switch SafetyMuttingOnOffSwitch1                                     /**/   = new Switch();
        //public Switch SafetyMuttingOnOffSwitch2                                     /**/   = new Switch();
        //public SwitchOneWay LightCurtainReset                                       /**/   = new SwitchOneWay();

        public LightCurtainProxy LightCurtain                                      /**/   = new LightCurtainProxy();


        public CstLoaderRotationUpAxisServo CstRotationAxis { get; set; }
        public CstLoaderUpDownAxisServo CstUpDownAxis { get; set; }

        public CstLoaderUint()
        {
        }

        private PlcTimerEx _tmrTilteReadWait = new PlcTimerEx("틸트 측정 대기 타이머");
        private PlcTimerEx _tmrLoderVaccumTimeOver = new PlcTimerEx("로더 버큠 타입 오버 타이머");

        public void InitializeInterLock()
        {
            CstRotationAxis.CheckStartMoveInterLockFunc = CstRotationAxis_CheckStartMoveInterLockFunc;
            CstUpDownAxis.CheckStartMoveInterLockFunc = CstUpDownAxis_CheckStartMoveInterLockFunc;

            TiltCylinder.CheckStartMoveInterLockFunc = TiltCylinder_CheckStartMoveInterLock;
            CstGripCylinder.CheckStartMoveInterLockFunc = CstGripCylinder_CheckStartMoveInterLockFunc;


            SetStepSwith("HomeSeqLogicWorking");
            SetStepSwith("CstLoadLogicWorking");
            SetStepSwith("CstMeasureSeqLogicWorking");
            SetStepSwith("CstCellInOutLogicWorking");
            SetStepSwith("CstUnloadLogicWorking");

        }
        public override void LogicWorking(Equipment equip)
        {
            TiltCylinder.LogicWorking(equip);

            LDCstDetectOGamJi.LogicWorking(equip);
            LDCstDetectExist.LogicWorking(equip);

            CstGripCylinder.LogicWorking(equip);
            CstRotationAxis.LogicWorking(equip);

            CstInOutBuzzer.LogicWorking(equip);

            CstRotationAxis.LogicWorking(equip);
            CstUpDownAxis.LogicWorking(equip);

            LightCurtain.LogicWorking(equip);
            SatusLogicWorking(equip);


            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;
            if (GG.ExceptCstLoaderUnloader)
            {
                IsHomeComplete = true;
                return;
            }
            else
            {
                SeqLogicWorking(equip);
            }
        }

        public void SatusLogicWorking(Equipment equip)
        {
            if (GG.TestMode == true)
                if (LightCurtain.XB_Warning_Detect_BJub.vBit == false)
                {
                    if (UnitPosi == EmUnitPosi.ACol)
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0201_LD_CST_A_MUTING);
                    else if (UnitPosi == EmUnitPosi.BCol)
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0202_LD_CST_B_MUTING);


                    equip.ChangeMode(EmEquipRunMode.Pause);
                }
        }



        //구동 시퀀스 구성. 
        public EmCstLoaderStep SeqStep = EmCstLoaderStep.S0000_CST_WAIT;
        private int _cellVaccumOnTryCount = 0;
        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                IsHomeComplete = false;

                SeqStep = EmCstLoaderStep.S0000_CST_WAIT;
                HomeStep = EmCstLoaderHomeStep.H0000;

                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                if (LightCurtain.XB_Warning_Detect_BJub.vBit == false)
                {
                    if (CstRotationAxis.IsMoving == true)
                        CstRotationAxis.MoveStop(equip);
                    if (CstUpDownAxis.IsMoving == true)
                        CstUpDownAxis.MoveStop(equip);
                }
                //일시 정지. 
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Start)
            { }


            if (this.IsHomeComplete == false)
            {
                if (equip.LD.Loader.IsHomeComplete == true)
                {
                    if (GetStepSwith("HomeSeqLogicWorking"))  /**/HomeSeqLogicWorking(equip);
                }
                return;
            }

            if (equip.IsHomeComplete == false) return;

            if (GetStepSwith("CstLoadLogicWorking"))        /**/ CstLoadLogicWorking(equip);
            if (GetStepSwith("CstMeasureSeqLogicWorking"))  /**/ CstMeasureSeqLogicWorking(equip);
            if (GetStepSwith("CstCellInOutLogicWorking"))   /**/ CstCellInOutLogicWorking(equip);
            if (GetStepSwith("CstUnloadLogicWorking"))      /**/ CstUnloadLogicWorking(equip);
        }
        public void CstLoadLogicWorking(Equipment equip)
        {

            if (SeqStep == EmCstLoaderStep.S0000_CST_WAIT)
            {
                if (LightCurtain.XB_MutingOnOff.vBit == false)
                {
                    if (LDCstDetectExist.IsOnOff == true && LDCstDetectOGamJi.IsOnOff == false)
                    {
                        if (GG.TestMode == true)
                        {
                            Port.Cst.OutCount = 0;
                            Port.Cst.FillCassette(equip.CurrentRecipe, 59, new OnlineCassetteInfoInfo());
                            Port.ChangePortStatus(EmPortStatus.BUSY);
                            Port.Cst.ChangeCassetteStatus(equip, EMCasstteStatus.CELLOUT_START);
                        }
                        SeqStep = EmCstLoaderStep.S1010_;
                    }
                    else if (LDCstDetectExist.IsOnOff == true && LDCstDetectOGamJi.IsOnOff == true)
                    {
                        //카셋트 투입 오류..
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S1010_)
            {
                //if (MutingBtnSwitch.IsOnOff == false && SafetyMuttingOnOffSwitch1.IsOnOff == false && SafetyMuttingOnOffSwitch2.IsOnOff == false)
                if (LightCurtain.MutingBtnSwitch.IsOnOff == false)
                {
                    SeqStep = EmCstLoaderStep.S1020_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S1020_)
            {
                if ( /*투입 버튼 확인*/true)
                {
                    if (CstGripCylinder.Forward(equip) == true)
                        SeqStep = EmCstLoaderStep.S1050_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S1050_)
            {
                if (CstGripCylinder.IsForward == true)
                {
                    if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmCstLoaderStep.S1999_CST_TO_LIFT_END;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S1999_CST_TO_LIFT_END)
            {
                SeqStep = EmCstLoaderStep.S2010_CST_MEASURE_START;
            }
        }

        string CstID = string.Empty;
        public void CstMeasureSeqLogicWorking(Equipment equip)
        {
            /* */
            if (SeqStep == EmCstLoaderStep.S2010_CST_MEASURE_START)
            {
                if (CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true &&
                    CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true && CstGripCylinder.IsForward == true)
                {
                    SeqStep = EmCstLoaderStep.S2020_MEASURE_POS_MOVE;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S2020_MEASURE_POS_MOVE)
            {
                if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstLoaderUpDownAxisServo.Cst_Measure, equip.CurrentRecipe, null) == true)
                {
                    if (TiltCylinder.Forward(equip) == true)
                    {
                        SeqStep = EmCstLoaderStep.S2030_TILT_CYLINDER_FORWARD_CHECK;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S2030_TILT_CYLINDER_FORWARD_CHECK)
            {
                if (TiltCylinder.IsForward == true)
                {
                    SeqStep = EmCstLoaderStep.S2040_TILT_SENSOR_RESULT_CHECK;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S2040_TILT_SENSOR_RESULT_CHECK)
            {
                if (this.UnitPosi == EmUnitPosi.ACol)
                {
                    if (equip.LdTiltProxy.IsReadComplete == true)
                    {
                        if (equip.LdTiltProxy.IsReadSuccess)
                        {
                            if (equip.LdTiltProxy.GetReadJudge[0] == true && equip.LdTiltProxy.GetReadJudge[1] == true)
                            {
                                Port.Cst.IsTiltSuccess = true;
                                SeqStep = EmCstLoaderStep.S2050_FIXED_BARCODE_CMD_SEND;
                            }
                            else
                            {
                                Port.Cst.IsTiltSuccess = false;
                                //알람 처리
                            }
                        }
                        else
                        {
                            //알람 처리. 
                            Port.Cst.IsTiltSuccess = false;
                            return;
                        }
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.BCol)
                {
                    if (equip.LdTiltProxy.IsReadComplete == true)
                    {
                        if (equip.LdTiltProxy.IsReadSuccess)
                        {
                            if (equip.LdTiltProxy.GetReadJudge[2] == true && equip.LdTiltProxy.GetReadJudge[3] == true)
                            {
                                Port.Cst.IsTiltSuccess = true;
                                SeqStep = EmCstLoaderStep.S2050_FIXED_BARCODE_CMD_SEND;
                            }
                            else
                            {
                                Port.Cst.IsTiltSuccess = false;
                                //알람 처리
                            }
                        }
                        else
                        {
                            //알람 처리. 
                            Port.Cst.IsTiltSuccess = false;
                            return;
                        }
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.None)
                {
                    //알람 처리.
                    return;
                }
                //CIM 추가 필요. 
            }
            else if (SeqStep == EmCstLoaderStep.S2050_FIXED_BARCODE_CMD_SEND)
            {
                if (this.UnitPosi == EmUnitPosi.ACol)
                {
                    if (!equip.LdAFixedBarcProxy.SendCmd())
                    {
                        //알람 처리.
                    }
                    else
                    {
                        SeqStep = EmCstLoaderStep.S2060_FIXED_BARCODE_RESULT_WAIT;
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.BCol)
                {
                    if (!equip.LdBFixedBarcProxy.SendCmd())
                    {
                        //알람 처리.
                    }
                    else
                    {
                        SeqStep = EmCstLoaderStep.S2060_FIXED_BARCODE_RESULT_WAIT;
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.None)
                {
                    //알람 처리.
                }
            }
            else if (SeqStep == EmCstLoaderStep.S2060_FIXED_BARCODE_RESULT_WAIT)
            {
                if (this.UnitPosi == EmUnitPosi.ACol)
                {
                    if (equip.LdAFixedBarcProxy.IsReadComplete)
                    {
                        if (!equip.LdAFixedBarcProxy.IsReadSuccess)
                        {
                            //알람 처리.
                        }
                        else
                        {
                            CstID = equip.LdAFixedBarcProxy.ReadData;
                            SeqStep = EmCstLoaderStep.S2070_PPID_MATCHING_CHECK;
                        }
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.BCol)
                {
                    if (equip.LdBFixedBarcProxy.IsReadComplete)
                    {
                        if (!equip.LdBFixedBarcProxy.IsReadSuccess)
                        {
                            //알람 처리.
                        }
                        else
                        {
                            CstID = equip.LdBFixedBarcProxy.ReadData;
                            SeqStep = EmCstLoaderStep.S2070_PPID_MATCHING_CHECK;
                        }
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.None)
                {
                    //알람 처리.
                }
            }
            else if (SeqStep == EmCstLoaderStep.S2070_PPID_MATCHING_CHECK)
            {
                /*
                string hostPPID = ????;
                if (string.Compare(equip.CurrentRecipe.Name.Substring(4, 3), hostPPID.Substring(2, 3)) != 0)
                {
                    //알람 처리.
                }
                */ // JICHA [20181122] - 바코드로 읽은 카세트ID를 호스트에 보고하고, 호스트에서 받은 PPID와 설비에 적용 된 레시피 이름에서 Cell Type을 비교하여 다르면 알람.

                /*
                int MaxCellCount = 0;
                int CstWidthCount = equip.CurrentRecipe.CstType == emCstType.Cst1 ? 1 :
                                    equip.CurrentRecipe.CstType == emCstType.Cst2 ? 2 :
                                    equip.CurrentRecipe.CstType == emCstType.Cst4 ? 4 : 8;
                if (MaxCellCount > (equip.CurrentRecipe.CassetteMaxHeightCount * CstWidthCount))
                {
                    //알람 처리.
                }
                */ // JICHA [20181122] - S3F215로 받아온 셀의 갯수가 레시피에 설정 된 카세트의 셀 갯수(가로*세로)보다 크면 알람 처리 추가 필요.

                SeqStep = EmCstLoaderStep.S2080_TILT_CYLINDER_BACKWARD;
            }
            else if (SeqStep == EmCstLoaderStep.S2080_TILT_CYLINDER_BACKWARD)
            {
                if (TiltCylinder.Backward(equip) == true)
                {
                    SeqStep = EmCstLoaderStep.S2090_TILT_CYLINDER_BACKWARD_CHECK;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S2090_TILT_CYLINDER_BACKWARD_CHECK)
            {
                if (TiltCylinder.IsBackward == true)
                {
                    if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstLoaderUpDownAxisServo.Cst_CellOut, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmCstLoaderStep.S2100_CST_MEASURE_COMPLETE;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S2100_CST_MEASURE_COMPLETE)
            {
                if (Port.Cst.IsTiltSuccess)
                {
                    SeqStep = EmCstLoaderStep.S5010_CEL_CST_CELL_INOUT_START;
                }
                else
                {
                    SeqStep = EmCstLoaderStep.S6010_CEL_CST_TO_ULD_WAIT;
                }
            }
        }
        public void CstCellInOutLogicWorking(Equipment equip)
        {
            if (Port.Status != EmPortStatus.BUSY) return;

            /* */
            if (SeqStep == EmCstLoaderStep.S5010_CEL_CST_CELL_INOUT_START)
            {
                if (CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true &&
                    CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cst_CellOut, equip.CurrentRecipe, null) == true &&
                    CstGripCylinder.IsForward == true && TiltCylinder.IsBackward == true)
                {

                    //포트 상태를 변경함. 
                    Port.Cst.ChangeCassetteStatus(equip, EMCasstteStatus.CELLOUT_START);

                    SeqStep = EmCstLoaderStep.S5015_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5015_)
            {
                _cellVaccumOnTryCount = 0;
                SeqStep = EmCstLoaderStep.S5020_;
            }
            else if (SeqStep == EmCstLoaderStep.S5020_)
            {
                if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstLoaderUpDownAxisServo.Cal_Cst_CellOut_Wait, equip.CurrentRecipe, Port) == true)
                {
                    _cellVaccumOnTryCount++;

                    if (equip.CurrentRecipe.Name != Port.Cst.PPID && GG.TestMode == false)
                    {
                        InterLockMgr.AddInterLock("설비에 설정된 PPID와 카셋트의 PPID가 상이 합니다.");
                    }
                    else
                    {
                        //임시 배출  위치 
                        PanelInfoSet pnlSet = Port.Cst.GetOutPanelInfoSet(Port, equip.UD.CstUnloader_A.Port, equip.CurrentRecipe);

                        PioSend.SendPanelInfoSet = pnlSet;
                        PioSend.YSendAble = true;

                        SeqStep = EmCstLoaderStep.S5040_;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5040_)
            {
                if (PioSend.XRecvAble == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    PioSend.YSendStart = true;
                    SeqStep = EmCstLoaderStep.S5045_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5045_)
            {
                //카셋트에 CST1 티입일 경위 한 X축를 회피위치로 이동 필요. //방안 고민. 
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.LD.Loader.X12AxisPtpMoveCmdSync(equip, EmLoaderXAxisServo.Cal_Cell_Loading_PnlSet, equip.CurrentRecipe, PioSend.SendPanelInfoSet, PioSend.SendPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmCstLoaderStep.S5050_;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5050_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.LD.Loader.Y12AxisPtpMoveCmdSync(equip, EmLoaderYAxisServo.Cal_Cell_Loading, equip.CurrentRecipe, PioSend.SendPanelInfoSet, PioSend.SendPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmCstLoaderStep.S5060_;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5060_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstLoaderUpDownAxisServo.Cal_Cst_CellOut, equip.CurrentRecipe, Port) == true)
                    {
                        SeqStep = EmCstLoaderStep.S5070_;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5070_)
            {

                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.LD.Loader.Y12AxisIsInPosition(EmLoaderYAxisServo.Cal_Cell_Loading, equip.CurrentRecipe, PioSend.SendPanelInfoSet, PioSend.SendPanelInfoSet.CellPosi) == true)
                    {
                        if (equip.LD.Loader.VaccumOnOff(equip, true, PioSend.SendPanelInfoSet.CellPosi) == true)
                        {
                            _tmrLoderVaccumTimeOver.Start(3);
                            SeqStep = EmCstLoaderStep.S5080_;
                        }
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5080_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cal_Cst_CellOut, equip.CurrentRecipe, Port) == true)
                    {
                        if (equip.LD.Loader.Y12AxisIsInPosition(EmLoaderYAxisServo.Cal_Cell_Loading, equip.CurrentRecipe, PioSend.SendPanelInfoSet, PioSend.SendPanelInfoSet.CellPosi) == true)
                        {
                            if (_tmrLoderVaccumTimeOver)
                            {
                                _tmrLoderVaccumTimeOver.Stop();

                                if (_cellVaccumOnTryCount < 3)
                                {
                                    SeqStep = EmCstLoaderStep.S5015_;
                                }
                                else
                                {
                                    //알람 처리. 
                                    return;
                                }
                            }

                            if (equip.LD.Loader.IsVaccumOn(PioSend.SendPanelInfoSet.CellPosi) == true)
                            {
                                _tmrLoderVaccumTimeOver.Stop();
                                SeqStep = EmCstLoaderStep.S5090_;
                            }
                        }
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5090_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    Port.Cst.CellOutComplete(PioSend.SendPanelInfoSet);
                    SeqStep = EmCstLoaderStep.S5100_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5100_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.LD.Loader.Y12AxisPtpMoveCmdSync(equip, EmLoaderYAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, PioSend.SendPanelInfoSet, PioSend.SendPanelInfoSet.CellPosi) == true)
                    {
                        PioSend.YSendComplete = true;
                        SeqStep = EmCstLoaderStep.S5110_;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5110_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true && PioSend.XRecvComplete == true)
                {
                    PioSend.YSendAble = false;
                    PioSend.YSendStart = false;
                    PioSend.YSendComplete = false;
                    SeqStep = EmCstLoaderStep.S5130_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5130_)
            {
                if (PioSend.XRecvAble == false && PioSend.XRecvStart == false && PioSend.XRecvComplete == false)
                {
                    if (Port.Cst.OutCount < Port.Cst.CellCount)
                    {
                        //다음 글라스 처리. 
                        SeqStep = EmCstLoaderStep.S5015_;
                    }
                    else
                    {
                        //카셋트 배출 위치로 이동. 
                        SeqStep = EmCstLoaderStep.S5999_;
                    }
                }
            }
            else if (SeqStep == EmCstLoaderStep.S5999_)
            {
                if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstLoaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true)
                {
                    SeqStep = EmCstLoaderStep.S6010_CEL_CST_TO_ULD_WAIT;
                }
            }
        }
        public void CstUnloadLogicWorking(Equipment equip)
        {
            if (SeqStep == EmCstLoaderStep.S6010_CEL_CST_TO_ULD_WAIT)
            {
                if (CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true &&
                    CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true &&
                    CstGripCylinder.IsForward == true && TiltCylinder.IsBackward == true)
                {
                    SeqStep = EmCstLoaderStep.S6020_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S6020_)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstLoaderRotationUpAxisServo.R_0dgr, equip.CurrentRecipe, null) == true)
                {
                    SeqStep = EmCstLoaderStep.S6030_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S6030_)
            {
                if (LDCstDetectExist.IsOnOff == false && LDCstDetectOGamJi.IsOnOff == false)
                {
                    SeqStep = EmCstLoaderStep.S6040_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S6040_)
            {
                if (LightCurtain.MutingBtnSwitch.IsOnOff == false)  //&& SafetyMuttingOnOffSwitch1.IsOnOff == false && SafetyMuttingOnOffSwitch2.IsOnOff == false)
                {
                    SeqStep = EmCstLoaderStep.S6080_;
                }
            }
            else if (SeqStep == EmCstLoaderStep.S6080_)
            {
                SeqStep = EmCstLoaderStep.S0000_CST_WAIT;
            }
        }

        //HOME 시퀀스 구성. 
        public EmCstLoaderHomeStep HomeStep = EmCstLoaderHomeStep.H1010;
        public PlcTimerEx _delayAixsHome = new PlcTimerEx("Cst Loader Home");
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmCstLoaderHomeStep.H0000)
            {
                if (this.IsHomeComplete == false)
                {
                    HomeStep = EmCstLoaderHomeStep.H0010;
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H0010)
            {
                if (TiltCylinder.IsBackward == false)
                    TiltCylinder.Backward(equip);

                HomeStep = EmCstLoaderHomeStep.H0020;
            }
            else if (HomeStep == EmCstLoaderHomeStep.H0020)
            {
                if (TiltCylinder.IsBackward == true)
                {
                    if (LDCstDetectExist.IsOnOff == true && LDCstDetectOGamJi.IsOnOff == false)
                    {
                        HomeStep = EmCstLoaderHomeStep.H1000_CST_PORT_EXIST;
                    }
                    else
                    {
                        HomeStep = EmCstLoaderHomeStep.H2000_CST_PORT_EMTYP;
                    }
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H1000_CST_PORT_EXIST)
            {
                if (CstGripCylinder.IsForward != true)
                {
                    if (CstGripCylinder.Forward(equip))
                    {
                        HomeStep = EmCstLoaderHomeStep.H1010;
                    }
                }
                else
                {
                    HomeStep = EmCstLoaderHomeStep.H1010;
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H1010)
            {
                if (CstGripCylinder.IsForward == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H3000_AXIS_HOME_START;
                }
            }
            // 점프 


            else if (HomeStep == EmCstLoaderHomeStep.H2000_CST_PORT_EMTYP)
            {
                if (CstGripCylinder.IsBackward == false)
                {
                    if (CstGripCylinder.Backward(equip))
                    {
                        HomeStep = EmCstLoaderHomeStep.H2010;
                    }
                }
                else
                {
                    HomeStep = EmCstLoaderHomeStep.H2010;
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H2010)
            {
                if (CstGripCylinder.IsBackward == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H3000_AXIS_HOME_START;
                }
            }
            // 점프 


            else if (HomeStep == EmCstLoaderHomeStep.H3000_AXIS_HOME_START)
            {
                if (CstUpDownAxis.GoHome(equip) == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H3010;
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H3010)
            {
                if (CstUpDownAxis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H3020;
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H3020)
            {
                if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstLoaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true)
                {
                    if (CstRotationAxis.GoHome(equip) == true)
                    {
                        HomeStep = EmCstLoaderHomeStep.H3030;
                    }
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H3030)
            {
                if (CstRotationAxis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H3040;
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H3040)
            {
                if (CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true && CstRotationAxis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H3060;
                }
            }
            else if (HomeStep == EmCstLoaderHomeStep.H3060)
            {
                if (LDCstDetectExist.IsOnOff == false && LDCstDetectOGamJi.IsOnOff == false)
                {
                    HomeStep = EmCstLoaderHomeStep.H5000;
                }
                else if (LDCstDetectExist.IsOnOff == true && LDCstDetectOGamJi.IsOnOff == false)
                {
                    HomeStep = EmCstLoaderHomeStep.H6000;
                }
            }
            //점프. 

            //카셋트가 없을때. 로딩 위치.
            else if (HomeStep == EmCstLoaderHomeStep.H5000)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstLoaderRotationUpAxisServo.R_0dgr, equip.CurrentRecipe, null) == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H5999_HOME_COMPLETE;
                }
            }
            //점프. 

            //카셋트가 있을때. 로딩 위치.
            else if (HomeStep == EmCstLoaderHomeStep.H6000)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true)
                {
                    HomeStep = EmCstLoaderHomeStep.H5999_HOME_COMPLETE;
                }
            }

            //점프
            else if (HomeStep == EmCstLoaderHomeStep.H5999_HOME_COMPLETE)
            {
                HomeStep = EmCstLoaderHomeStep.H0000;
                this.IsHomeComplete = true;
            }
        }

        //각 장치별 인터락
        public bool CstRotationAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (LDCstDetectExist.IsOnOff == true && LDCstDetectOGamJi.IsOnOff == true)
            {
                InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, 카셋트가 방향이 잘못되었습니다. ");
                return true;
            }

            if (LDCstDetectExist.IsOnOff == true && CstGripCylinder.IsForward != true)
            {
                InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Grip 실린더가 그랩 상태가 아닙니다.");
                return true;
            }

            if (CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) != true &&
                CstUpDownAxis.IsHomeOnPosition() != true &&
                CstUpDownAxis.IsPositiveLimit != true)
            {
                InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, 카셋트 Up Down축이 로드/언로드 포지션이 아닙니다.");
                return true;
            }

            if (TiltCylinder.IsBackward != true)
            {
                InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Tilt 실린더가 후진 상태가 아닙니다. ");
                return true;
            }

            //if (svMoveType != emServoMoveType.HOME &&
            //    equip.LD.Loader.Y1Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y1Axis.IsInPosition(EmLoaderYAxisServo.Cell_B_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y1Axis.IsHomeOnPosition() != true)
            //{
            //    InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Loader Y1가 ULD,HOME 위치가 아닙니다.");
            //    return true;
            //}

            //if (svMoveType != emServoMoveType.HOME &&
            //    equip.LD.Loader.Y2Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y2Axis.IsInPosition(EmLoaderYAxisServo.Cell_B_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y2Axis.IsHomeOnPosition() != true)
            //{
            //    InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Loader Y2가 ULD,HOME 위치가 아닙니다.");
            //    return true;
            //}

            return false;
        }
        public bool CstGripCylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect idirect)
        {
            if (CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_0dgr, equip.CurrentRecipe, null) != true)
            {
                InterLockMgr.AddInterLock("Grip 실린더 이동 불가, Cst Rotation축 0도 위치가 아닙니다. ");
                return true;
            }

            if (CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) != false)
            {
                InterLockMgr.AddInterLock("Grip 실린더 이동 불가, 카셋트 업다운축 로드/언로드 위치가 아닙니다. ");
                return true;
            }
            return false;
        }
        public bool CstUpDownAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (svMoveType != emServoMoveType.HOME &&
                CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) != true &&
                CstRotationAxis.XB_StatusHomeInPosition.vBit == true)
            {
                InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, 카셋트 Rotation축이 90 dgr 포지션이 아닙니다.");
                return true;
            }

            if (LDCstDetectExist.IsOnOff == true && CstGripCylinder.IsForward != true)
            {
                InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, Grip 실린더가 그랩 상태가 아닙니다.");
                return true;
            }

            if (TiltCylinder.IsBackward != true)
            {
                InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, Tilt 실린더가 후진 상태가 아닙니다. ");
                return true;
            }

            //if (svMoveType != emServoMoveType.HOME &&
            //    equip.LD.Loader.Y1Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y1Axis.IsInPosition(EmLoaderYAxisServo.Cell_B_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y1Axis.IsHomeOnPosition() != true)
            //{
            //    InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, Loader Y1가 ULD , HOME 위치가 아닙니다.");
            //    return true;
            //}

            //if (svMoveType != emServoMoveType.HOME &&
            //    equip.LD.Loader.Y2Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y2Axis.IsInPosition(EmLoaderYAxisServo.Cell_B_Unloading, equip.CurrentRecipe, null) != true &&
            //    equip.LD.Loader.Y2Axis.IsHomeOnPosition() != true)
            //{
            //    InterLockMgr.AddInterLock("Cst UpDown축 이동 불가,  Loader Y2가 ULD , HOME 위치가 아닙니다.");
            //    return true;
            //}

            return false;
        }
        public bool TiltCylinder_CheckStartMoveInterLock(Equipment equip, EmCylinderDirect idirect)
        {
            if (CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) != true)
            {
                InterLockMgr.AddInterLock("Tilt 실린더 이동 불가, 카셋트 Rotation축이 90 dgr 포지션이 아닙니다.");
                return true;
            }

            if (CstUpDownAxis.IsInPosition(EmCstLoaderUpDownAxisServo.Cst_Measure, equip.CurrentRecipe, null) == false)
            {
                InterLockMgr.AddInterLock("Tilt 실린더 이동 불가, Loader Up Down 축이 Tilt 측정위치가 아닙니다. ");
                return true;
            }
            return false;
        }
    }



}

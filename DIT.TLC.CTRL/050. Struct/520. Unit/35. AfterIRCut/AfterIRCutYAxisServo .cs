﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public enum EmAfterIRCutYAxisServo
    {
        AfterIR_Y_CellLD     /**/= 0,
        AfterIR_Y_CellULD    /**/= 1,
    }

    public class AfterIRCutYAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "ScanXServo.ini");
        public AfterIRCutYAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 5;
            SoftSpeedLimit = 10;
            SoftJogSpeedLimit = 10;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 10;
        }
        public bool IsInPosition(EmAfterIRCutYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmAfterIRCutYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmAfterIRCutYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
    }
}
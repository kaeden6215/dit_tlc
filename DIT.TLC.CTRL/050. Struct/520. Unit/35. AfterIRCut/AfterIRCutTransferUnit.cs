﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using Dit.Framework.Comm;

namespace DIT.TLC.CTRL
{
    public class AfterIRCutTransferUnit : BaseUnit
    {

        public enum EmAfterIRCutTrHomeStep
        {
            H0000,
            H0010,
            H0020,
            H0030,
            H0040,
            H0050,

            H5999_HOME_COMPLETE
        }

        public enum EmBreakTransferCmd
        {
            S1010_CELL_LOADING_START,
            S1020_,
            S1030_,
            S1040_,
            S1050_,
            S2010_CELL_UNLOAD_START,
            S2020_,
            S2030_,
            S2040_,
            S2050_,
            S1010_,
            S1060_,
            S1070_,
            S1080_,
            S1090_,
            S1100_,
            S2070_,
            S2060_,
            S2120_,
            S2110_,
            S2100_,
            S2090_,
            S2080_,
        }

        public AfterIRCutTransferYAxisServo YAxis { get; set; }

        public Cylinder UpDown1Cylinder = new Cylinder();
        public Cylinder UpDown2Cylinder = new Cylinder();

        public Switch2Cmd1Sensor Vaccum1                    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1                         /**/= new SwitchOneWay();
        public Switch2Cmd1Sensor Vaccum2                    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower2                         /**/= new SwitchOneWay();


        public EmBreakTransferCmd SeqStep { get; set; }

        public PioSendStep PioSendA { get; set; }
        public PioRecvStep PioRecvA { get; set; }

        public PioSendStep PioSendB { get; set; }
        public PioRecvStep PioRecvB { get; set; }

        public PlcAddr Vaccum1Value { get; set; }
        public PlcAddr Vaccum2Value { get; set; }

        private LaserCuttingStageUnit IRCuttingState = null;
        private BreakStageUnit BreakStage = null;


        public AfterIRCutTransferUnit()
        {
            SeqStep = EmBreakTransferCmd.S1010_CELL_LOADING_START;
        }
        public void InitializeInterLock()
        {
            if (IRCuttingState == null)
                IRCuttingState = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;

            if (BreakStage == null)
                BreakStage = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.BreakStage) as BreakStageUnit;

            Vaccum1.Simulaotr = GG.VacuumSimul;
            Vaccum2.Simulaotr = GG.VacuumSimul;


            YAxis.CheckStartMoveInterLockFunc = YAxis_CheckStartMoveInterLockFunc;
            UpDown1Cylinder.CheckStartMoveInterLockFunc = UpDown1Cylinder_CheckStartMoveInterLockFunc;
            UpDown2Cylinder.CheckStartMoveInterLockFunc = UpDown2Cylinder_CheckStartMoveInterLockFunc;

            SetStepSwith( "HomeSeqLogicWorking");
            SetStepSwith( "CellLdLogicWorking");
            SetStepSwith( "CellUldLogicWorking");
        }

        public override void LogicWorking(Equipment equip)
        {
            base.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            

            YAxis.LogicWorking(equip);
            UpDown1Cylinder.LogicWorking(equip);
            UpDown2Cylinder.LogicWorking(equip);
            Vaccum1.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            if (GG.NotUseStep == true) return;
            SeqLogicWorking(equip);
        }

        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                SeqStep = EmBreakTransferCmd.S1010_CELL_LOADING_START;

                IsHomeComplete = false;
                HomeStep = EmAfterIRCutTrHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (GetStepSwith( "HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                return;
            }

            //전체 설비가 홈이 되었을때.  시퀀스 들어감. 
            if (equip.IsHomeComplete == false) return;
            

            if (GetStepSwith( "CellLdLogicWorking"))         /**/CellLdLogicWorking(equip);
            if (GetStepSwith( "CellUldLogicWorking"))        /**/CellUldLogicWorking(equip);
                 
        }


        //HOME 시퀀스 구성. 
        public EmAfterIRCutTrHomeStep HomeStep = EmAfterIRCutTrHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmAfterIRCutTrHomeStep.H0000)
            {
                if (IsExitsPanel == true)
                {
                    //공압  On/Off로 
                    if (IsVaccumOn(PanelSet.CellPosi) == true)
                    {
                        HomeStep = EmAfterIRCutTrHomeStep.H0010;
                    }
                    else
                    {
                        InterLockMgr.AddInterLock("셀 위치와 버큠이 상이함", "AFTER IR CUT TR의 CELL 위치 {0}", PanelSet.CellPosi);
                    }
                }
                else
                {
                    HomeStep = EmAfterIRCutTrHomeStep.H0010;
                }
            }
            else if (HomeStep == EmAfterIRCutTrHomeStep.H0010)
            {
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) != true)
                {
                    if (UpDownCylinder_Backward(equip, EmCellPosi.Two) == true)
                    {
                        HomeStep = EmAfterIRCutTrHomeStep.H0020;
                    }
                }
                else
                {
                    HomeStep = EmAfterIRCutTrHomeStep.H0020;
                }
            }
            else if (HomeStep == EmAfterIRCutTrHomeStep.H0020)
            {
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) == true)
                {
                    if (YAxis.GoHome(equip) == true)
                    {
                        HomeStep = EmAfterIRCutTrHomeStep.H0030;
                    }
                }
            }
            else if (HomeStep == EmAfterIRCutTrHomeStep.H0030)
            {
                if (YAxis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmAfterIRCutTrHomeStep.H5999_HOME_COMPLETE;                 
                }
            }
            else if (HomeStep == EmAfterIRCutTrHomeStep.H5999_HOME_COMPLETE)
            {
                IsHomeComplete = true;
            }
        }


        public void CellLdLogicWorking(Equipment equip)
        {
            if (SeqStep == EmBreakTransferCmd.S1010_CELL_LOADING_START)
            {
                //홈완료 되었을때 확인 
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) == true)
                {
                    if (YAxis.PtpMoveCmd(equip, EmAfterIRCutYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmBreakTransferCmd.S1010_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1010_)
            {
                if (YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
                {
                    PioRecv.YRecvAble = true;
                    SeqStep = EmBreakTransferCmd.S1020_;
                }                
            }
            else if (SeqStep == EmBreakTransferCmd.S1020_)
            {
                if (PioRecv.XSendAble == true )
                {
                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;
                    SeqStep = EmBreakTransferCmd.S1030_;
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1030_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (UpDownCylinder_Forward(equip, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmBreakTransferCmd.S1040_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1040_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsUpDownCylinderIsForward(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        if (VaccumOnOff(equip, true, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmBreakTransferCmd.S1050_;
                        }
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1050_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsVaccumOn(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        if (IRCuttingState.VacuumOnOff(equip, false, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmBreakTransferCmd.S1060_;
                        }
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1060_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IRCuttingState.IsVaccumOff(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmBreakTransferCmd.S1070_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1070_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (UpDownCylinder_Backward(equip, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmBreakTransferCmd.S1080_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1080_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsUpDownCylinderIsBackward(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        this.PanelSet = PioRecv.RecvPanelInfoSet;

                        PioRecv.YRecvAble = true;
                        PioRecv.YRecvStart = true;
                        PioRecv.YRecvComplete = true;

                        SeqStep = EmBreakTransferCmd.S1090_;

                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1090_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true && PioRecv.XSendComplete == true)
                {

                    PioRecv.YRecvAble = false;
                    PioRecv.YRecvStart = false;
                    PioRecv.YRecvComplete = false;
                    SeqStep = EmBreakTransferCmd.S1100_;
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S1100_)
            {
                if (PioRecv.XSendAble == false && PioRecv.XSendStart == false && PioRecv.XSendComplete == false)
                {
                    SeqStep = EmBreakTransferCmd.S2010_CELL_UNLOAD_START;
                }
            }
        }
        public void CellUldLogicWorking(Equipment equip)
        {
            if (SeqStep == EmBreakTransferCmd.S2010_CELL_UNLOAD_START)
            {
                //동시 이동 시작
                YAxis.PtpMoveCmd(equip, EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null);
                SeqStep = EmBreakTransferCmd.S2020_;
            }
            else if (SeqStep == EmBreakTransferCmd.S2020_)
            {
                if (YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true)
                {
                    PioSend.YSendAble = true;
                    SeqStep = EmBreakTransferCmd.S2030_;
                }

            }
            else if (SeqStep == EmBreakTransferCmd.S2030_)
            {
                if (PioSend.XRecvAble == true)
                {
                    PioSend.SendPanelInfoSet = this.PanelSet;
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    SeqStep = EmBreakTransferCmd.S2040_;
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2040_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    if (UpDownCylinder_Forward(equip, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmBreakTransferCmd.S2050_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2050_)
            {
                if (IsUpDownCylinderIsForward(this.PanelSet.CellPosi) == true)
                {
                    if ( BreakStage.VaccumOnOff(equip, true, this.PanelSet.CellPosi))
                    {
                        SeqStep = EmBreakTransferCmd.S2060_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2060_)
            {
                if (BreakStage.IsVaccumOn(this.PanelSet.CellPosi) == true)
                {
                    if (VaccumOnOff(equip, false, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmBreakTransferCmd.S2070_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2070_)
            {
                //tr바큠체크
                if (IsVaccumOff(this.PanelSet.CellPosi) == true)
                {
                    if (UpDownCylinder_Backward(equip, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmBreakTransferCmd.S2080_;
                    }
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2080_)
            {
                if (IsUpDownCylinderIsBackward(this.PanelSet.CellPosi) == true)
                {
                    SeqStep = EmBreakTransferCmd.S2090_;
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2090_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    PioSend.YSendComplete = true;
                    SeqStep = EmBreakTransferCmd.S2100_;
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2100_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true && PioSend.XRecvComplete == true)
                {
                    PioSend.YSendAble = false;
                    PioSend.YSendStart = false;
                    PioSend.YSendComplete = false;

                    SeqStep = EmBreakTransferCmd.S2110_;
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2110_)
            {
                if (PioSend.XRecvAble == false && PioSend.XRecvStart == false && PioSend.XRecvComplete == false)
                {
                    this.PanelSet.ClearPanel();

                    SeqStep = EmBreakTransferCmd.S2120_;
                }
            }
            else if (SeqStep == EmBreakTransferCmd.S2120_)
            {
                SeqStep = EmBreakTransferCmd.S1010_CELL_LOADING_START;
            }
        }

        //각 장치별 인터락        
        public bool YAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage1 YAxis 이동 불가, 배큠1이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage YAxis 이동 불가, 배큠2이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }

            if (UpDown1Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage YAxis 이동 불가, {0}LaserCutStage의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            if (UpDown2Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage YAxis 이동 불가, {0}LaserCutStage의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }
        public bool UpDown1Cylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect direct)
        {
            LaserCuttingStageUnit IRCutStage = equip.GetUnit(this.UnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;
            BreakStageUnit breakingStage = equip.GetUnit(this.UnitPosi, EmUnitType.BreakStage) as BreakStageUnit;



            //LD
            if (IRCutStage.YAxis.IsInPosition(EmLaserCuttingStageYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true &&
                YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                
            }
            else
            {
                InterLockMgr.AddInterLock(string.Format("{0}AfterIRCutTransferUnit  UpDown1Cylinder이동 불가, Panel 로딩/언로딩 위치가 아닙니다.", UnitPosi));
                return true;
            }

            //ULD
            if (breakingStage.YAxis.IsInPosition(EmBreakUnitYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                     YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            else
            {
                //인터락 처리.  다시 정리 필요함. 
                InterLockMgr.AddInterLock(string.Format("{0}AfterIRCutTransferUnit  UpDown1Cylinder이동 불가, Panel 로딩/언로딩 위치가 아닙니다.", UnitPosi));
                return true;
            }
        }
        public bool UpDown2Cylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect direct)
        {
            LaserCuttingStageUnit IRCutStage = equip.GetUnit(this.UnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;
            BreakStageUnit breakingStage = equip.GetUnit(this.UnitPosi, EmUnitType.BreakStage) as BreakStageUnit;


            //LD
            if (IRCutStage.YAxis.IsInPosition(EmLaserCuttingStageYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true &&
                YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }

            //ULD
            else if (breakingStage.YAxis.IsInPosition(EmBreakUnitYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                     YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            else
            {
                //인터락 처리.  다시 정리 필요함. 
                InterLockMgr.AddInterLock(string.Format("{0}AfterIRCutTransferUnit  UpDown2Cylinder이동 불가, Panel 로딩/언로딩 위치가 아닙니다.", UnitPosi));
                return true;
            }
        }

        public bool UpDownCylinder_Forward(Equipment equip, EmCellPosi cellPosi)
        {

            if (cellPosi == EmCellPosi.One_1)
            {
                UpDown1Cylinder.Forward(equip);
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                UpDown2Cylinder.Forward(equip);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                UpDown1Cylinder.Forward(equip);
                UpDown2Cylinder.Forward(equip);
            }

            return true;
        }
        public bool UpDownCylinder_Backward(Equipment equip, EmCellPosi cellPosi)
        {

            if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown1Cylinder.Backward(equip);
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown2Cylinder.Backward(equip);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return UpDown1Cylinder.Backward(equip) && UpDown2Cylinder.Backward(equip);
            }

            return true;
        }
        public bool IsUpDownCylinderIsForward(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown1Cylinder.IsForward;
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown2Cylinder.IsForward;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return UpDown1Cylinder.IsForward &&
                UpDown2Cylinder.IsForward;
            }
            else
            {
                return false;
            }
        }
        public bool IsUpDownCylinderIsBackward(EmCellPosi cellPosi)
        {

            if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown1Cylinder.IsBackward;
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown2Cylinder.IsBackward;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return UpDown1Cylinder.IsBackward &&
                UpDown2Cylinder.IsBackward;
            }
            else
            {
                return false;
            }
        }

        public bool Y12AxisPtpMoveCmd(Equipment equip, EmAfterIRCutYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            return true;
        }
        public bool Y12AxisIsInPosition(EmAfterIRCutYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            //
            if (cellPosi == EmCellPosi.One_1)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            return true;
        }

        public bool VaccumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vaccum1.OnOff(equip, onOff);
                if (onOff == false) Blower1.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vaccum2.OnOff(equip, onOff);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vaccum1.OnOff(equip, onOff);
                Vaccum2.OnOff(equip, onOff);

                if (onOff == false)
                {
                    Blower1.OnOff(equip, true, 500);
                    Blower2.OnOff(equip, true, 500);
                }
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == false && Vaccum2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }

    }
}

﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public class AfterIRCutXAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "ScanXServo.ini");
        

        public AfterIRCutXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {
            

            SoftMinusLimit = 0;
            SoftPlusLimit = 1350;
            SoftSpeedLimit = 1200;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

        }

        //public bool IsInPosition(EmLoaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        //{
        //    ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
        //    return IsInPosition(info);
        //}
        //public ServoPosiInfo GetCalPosition(EmLoaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        //{
        //    return GetCalPosition((int)posiNo, recp, opt);
        //}
    }
}




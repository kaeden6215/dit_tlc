﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class MasureUnit : BaseUnit
    {         
        public UnLoaderXAxisServo X1Axis { get; set; }
        public override void LogicWorking(Equipment equip)
        {
            X1Axis.LogicWorking(equip);        
        }
    }
}

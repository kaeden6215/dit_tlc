﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public enum EmBreakAlignXAxisServo
    {
        BreakAling_A_1st        /**/ = 0,
        BreakAling_B_1st        /**/ = 1,

        Flatness_A_1_tb         /**/ = 2,
        Flatness_A_2_tb         /**/ = 3,
        Flatness_B_1_tb         /**/ = 4,
        Flatness_B_2_tb         /**/ = 5,

        Jig_Aling               /**/ = 6,
        FlatnessZ               /**/ = 7,

        //께산된 위치. 
        Cal_BreakAlign_A_2nd /**/= 8,
        Cal_BreakAlign_B_2nd /**/= 9,
    }
    public class BreakAlignXAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(GG.StartupPath, "Setting", "ScanXServo.ini");

        public BreakAlignXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            SoftMinusLimit = 0;
            SoftPlusLimit = 1100;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmBreakAlignXAxisServo.Cal_BreakAlign_A_2nd == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmBreakAlignXAxisServo.BreakAling_A_1st].Clone();
                info.Position += 50;

                return info;
            }
            else if ((int)EmBreakAlignXAxisServo.Cal_BreakAlign_B_2nd == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmBreakAlignXAxisServo.BreakAling_B_1st].Clone();
                info.Position += 50;

                return info;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }
        public bool IsInPosition(EmBreakAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmBreakAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmBreakAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmBreakAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}




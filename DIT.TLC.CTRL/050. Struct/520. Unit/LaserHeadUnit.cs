﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public class LaserHeadUnit
    {
        public Cylinder UnDownCylinder1             /**/= new Cylinder();
        public Cylinder UnDownCylinder2             /**/= new Cylinder();
        public Cylinder LaserShutterCylinder        /**/= new Cylinder();
        public Cylinder DummyShutterCylinder1       /**/= new Cylinder();
        public Cylinder DummyShutterCylinder2       /**/= new Cylinder();

        public Cylinder DummyTankCylinder           /**/= new Cylinder();
        public PlcAddr LaserInterferometerIRValue { get; internal set; }
        public PlcAddr LaserInterferometerBKValue { get; internal set; }

        public SwitchOneWay LaserInterferometerReset1 = new SwitchOneWay();
        public SwitchOneWay LaserInterferometerReset2 = new SwitchOneWay();
    }
}

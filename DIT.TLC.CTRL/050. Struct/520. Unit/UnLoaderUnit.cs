﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class UnloaderUnit : BaseUnit
    {
        public CassetteInfo InEmptyCst { get; set; }
        public CassetteInfo OutCst { get; set; }

        public UnLoaderXAxisServo X1Axis { get; set; }
        public UnLoaderXAxisServo X2Axis { get; set; }
        public UnLoaderYAxisServo Y1Axis { get; set; }
        public UnLoaderYAxisServo Y2Axis { get; set; }


        public Switch2Cmd1Sensor Vaccum1    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1         /**/= new SwitchOneWay();

        public Switch2Cmd1Sensor Vaccum2    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower2         /**/= new SwitchOneWay();
         

        public PlcAddr HandPressureValue1 { get; set; }
        public PlcAddr HandPressureValue2 { get; set; }


        public override void LogicWorking(Equipment equip)
        {
            Y1Axis.LogicWorking(equip);
            Y2Axis.LogicWorking(equip);

            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
            Vaccum1.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower2.LogicWorking(equip);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmPreAlignXAxisServo
    {
        PreAlign_A_1st /**/= 0,
        PreAlign_B_1st /**/= 1,

        //께산된 위치. 
        Cal_PreAlign_A_2nd /**/= 2,
        Cal_PreAlign_B_2nd /**/= 3,

        Cal_Mcr_Read_A_1st   /**/= 4,
        Cal_Mcr_Read_A_2nd   /**/= 5,

        Cal_Mcr_Read_B_1st   /**/= 6,
        Cal_Mcr_Read_B_2nd   /**/= 7,
    }

    public class PreAlignXAxisServo : ServoMotorControl
    {
        public PreAlignXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 1000;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        public bool IsInPosition(EmPreAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmPreAlignXAxisServo.Cal_PreAlign_A_2nd == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmPreAlignXAxisServo.PreAlign_A_1st].Clone();
                info.Position += 50;

                return info;
            }
            else if ((int)EmPreAlignXAxisServo.Cal_PreAlign_B_2nd == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmPreAlignXAxisServo.PreAlign_B_1st].Clone();
                info.Position += 50;

                return info;
            }
            else if ((int)EmPreAlignXAxisServo.Cal_Mcr_Read_A_1st == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmPreAlignXAxisServo.PreAlign_A_1st].Clone();
                info.Position += 13;
                return info;
            }            
            else if ((int)EmPreAlignXAxisServo.Cal_Mcr_Read_B_1st == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmPreAlignXAxisServo.PreAlign_B_1st].Clone();
                info.Position += 13;
                return info;
            }            
            else
            {
                return base.GetCalPosition(posiNo, recp, opt); ;
            }
        }

        public ServoPosiInfo GetCalPosition(EmPreAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmPreAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmPreAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

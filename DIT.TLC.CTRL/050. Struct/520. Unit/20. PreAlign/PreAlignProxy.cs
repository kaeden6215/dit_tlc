﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class PreAlignProxy : BaseUnit
    {
        public enum EmPreAlignStep
        {
            S1000_PRE_ALIGN_WAIT,
            S1000_PRE_ALIGN_START,
            S1010_,
            S1020_,
            S1030_,
            S1040_,
            S1050_,
            S1060_,
            S1070_,
            S1005_,
            S1999_ALIGN_COMPLETE,
            S2000_MCR_XAIS_MOVE,
            S2010_MCR_START_WAIT,
            S2020_1ST_MCR_START,
            S2030_,
            S2040_,
            S2050_,
            S2090_,
            S2060_2ND_MCR_START,
            S2070_,
            S2080_,
            S2999_MCR_COMPLETE,
            S0000_PRE_MOVE_XAXIS,
        }

        public enum EmPreAlignHomeStep
        {
            H0000,
            H1010,
            H1020,
            H5999_HOME_COMPLETE,
        }

        public PreAlignXAxisServo XAxis { get; set; }

        public PreAlignProxy()
        { }

        public void InitializeInterLock()
        {
            XAxis.CheckStartMoveInterLockFunc = XAxis_CheckStartMoveInterLockFunc;

            SetStepSwith("HomeSeqLogicWorking");
            SetStepSwith("PreAlignLogicWorking");
            SetStepSwith("McrLogicWorking");
        }
        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;            
            SeqLogicWorking(equip);
        }

        public EmPreAlignStep SeqStep { get; set; }

        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                SeqStep = EmPreAlignStep.S1000_PRE_ALIGN_WAIT;


                IsHomeComplete = false;
                HomeStep = EmPreAlignHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (GetStepSwith("HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                return;
            }

            if (equip.IsHomeComplete == false) return;


            if (GetStepSwith("PreAlignLogicWorking")) PreAlignLogicWorking(equip);
            if (GetStepSwith("McrLogicWorking")) McrLogicWorking(equip);
        }

        //메소드 - HOME 시퀀스 구성. 
        public EmPreAlignHomeStep HomeStep = EmPreAlignHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmPreAlignHomeStep.H0000)
            {
                HomeStep = EmPreAlignHomeStep.H1010;
            }
            else if (HomeStep == EmPreAlignHomeStep.H1010)
            {
                XAxis.GoHome(equip);
                HomeStep = EmPreAlignHomeStep.H1020;
            }
            else if (HomeStep == EmPreAlignHomeStep.H1020)
            {
                if (XAxis.IsHomeOnPosition())
                {
                    HomeStep = EmPreAlignHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmPreAlignHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true;
            }
        }


        //      레디 받으면 이동하고있어야됨
        //      READY 확인하고 START
        //      X축 움직일수 잇을떄랑 못움직일때ㅐ


        //메소드 - 얼리안 시퀀스. 
        private int _alignTryCount = 0;
        private bool _isAlignComplete = false;
        public PanelInfoSet AlignPnlSet = null;
        private EmUnitPosi AlignUnitPosi = EmUnitPosi.None;

        private AlignResult _pnl1Result = new AlignResult();
        private AlignResult _pnl2Result = new AlignResult();
        private PlcTimerEx plcTmrAlignWait = new PlcTimerEx("PRE ALING 대가 타이머");
        private int _msgInx = 0;

        public bool MoveAlignXAxis(PanelInfoSet pSet, EmUnitPosi unitPosi)
        {
            if (SeqStep == EmPreAlignStep.S1000_PRE_ALIGN_WAIT)
            {
                AlignPnlSet = pSet;
                AlignUnitPosi = unitPosi;

                _isAlignComplete = false;
                SeqStep = EmPreAlignStep.S0000_PRE_MOVE_XAXIS;
                return true;
            }
            else
                return false;
        }
        public bool IsAlingReady(EmUnitPosi unitPosi)
        {
            if (SeqStep == EmPreAlignStep.S1000_PRE_ALIGN_WAIT)
                return true;
            else
                return false;
        }
        public bool StartAlign(PanelInfoSet pSet, EmUnitPosi unitPosi)
        {
            if (SeqStep == EmPreAlignStep.S1000_PRE_ALIGN_WAIT)
            {
                AlignPnlSet = pSet;
                AlignUnitPosi = unitPosi;

                _isAlignComplete = false;
                SeqStep = EmPreAlignStep.S1000_PRE_ALIGN_START;
                return true;
            }
            else
            {
                if (AlignPnlSet == pSet)
                    return true;
                else
                    return false;
            }
        }
        public bool IsAlignComplete(PanelInfoSet info, EmUnitPosi unitPosi, out AlignResult pnl1Result, out AlignResult pnl2Result)
        {
            pnl1Result = new AlignResult();
            pnl2Result = new AlignResult();
            if (_isAlignComplete == true && _pnl1Result.RecvComplete == true && _pnl1Result.RecvComplete == true)
            {
                pnl1Result = _pnl1Result;
                pnl2Result = _pnl2Result;
                return true;
            }
            else
            {
                return false;
            }
        }
        private void PreAlignLogicWorking(Equipment equip)
        {
            if (SeqStep == EmPreAlignStep.S0000_PRE_MOVE_XAXIS)
            {
                if (AlignUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.PreAlign_A_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmPreAlignStep.S1000_PRE_ALIGN_WAIT;
                }
                else if (AlignUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.PreAlign_B_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmPreAlignStep.S1000_PRE_ALIGN_WAIT;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmPreAlignStep.S1000_PRE_ALIGN_WAIT)
            {
                _alignTryCount = 0;
            }
            else if (SeqStep == EmPreAlignStep.S1000_PRE_ALIGN_START)
            {
                _alignTryCount++; ;
                SeqStep = EmPreAlignStep.S1005_;
            }
            else if (SeqStep == EmPreAlignStep.S1005_)
            {
                if (AlignUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.PreAlign_A_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmPreAlignStep.S1010_;
                }
                else if (AlignUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.PreAlign_B_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmPreAlignStep.S1010_;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmPreAlignStep.S1010_)
            {
                LoaderTransferUnit irState = equip.GetUnit(AlignUnitPosi, EmUnitType.LoaderTransfer) as LoaderTransferUnit;

                //1번 얼라인 마크.
                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.PreAlign, EmCameraIndex.Cam1st, EmAlignIndex.Grap1St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.PreAlign, EmCameraIndex.Cam2nd, EmAlignIndex.Grap1St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                SeqStep = EmPreAlignStep.S1020_;
            }
            else if (SeqStep == EmPreAlignStep.S1020_)
            {
                SeqStep = EmPreAlignStep.S1030_;
            }
            else if (SeqStep == EmPreAlignStep.S1030_)
            {
                if (AlignUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.Cal_PreAlign_A_2nd, equip.CurrentRecipe, null) == true)
                        SeqStep = EmPreAlignStep.S1040_;
                }
                else if (AlignUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.Cal_PreAlign_B_2nd, equip.CurrentRecipe, null) == true)
                        SeqStep = EmPreAlignStep.S1040_;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmPreAlignStep.S1040_)
            {
                LoaderTransferUnit irState = equip.GetUnit(AlignUnitPosi, EmUnitType.LoaderTransfer) as LoaderTransferUnit;

                //2번 얼라인 마크.
                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.PreAlign, EmCameraIndex.Cam1st, EmAlignIndex.Grap2St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.PreAlign, EmCameraIndex.Cam2nd, EmAlignIndex.Grap2St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                plcTmrAlignWait.Start(10);   //설정치 추가 필요

                SeqStep = EmPreAlignStep.S1050_;
            }
            else if (SeqStep == EmPreAlignStep.S1050_)
            {
                if (equip.AlignPc.IsAlignComplete(equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.PreAlign, EmCameraIndex.Cam1st, EmAlignIndex.Grap2St, out _pnl1Result) == true &&
                    equip.AlignPc.IsAlignComplete(equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.PreAlign, EmCameraIndex.Cam2nd, EmAlignIndex.Grap2St, out _pnl2Result) == true)
                {
                    plcTmrAlignWait.Stop();
                    SeqStep = EmPreAlignStep.S1060_;
                }

                if (plcTmrAlignWait)
                {
                    plcTmrAlignWait.Stop();
                    //알람 처리.
                }
            }
            else if (SeqStep == EmPreAlignStep.S1060_)
            {
                if (_pnl1Result.Result == EmAlignResult.Success || _pnl2Result.Result == EmAlignResult.Success) //결과 확인 
                {
                    SeqStep = EmPreAlignStep.S1070_;
                }
                else
                {
                    if (_alignTryCount < 3)
                    {
                        SeqStep = EmPreAlignStep.S1000_PRE_ALIGN_START;
                    }
                    else
                    {
                        //알람 처리.
                    }
                }
            }
            else if (SeqStep == EmPreAlignStep.S1070_)
            {
                _isAlignComplete = true;
                SeqStep = EmPreAlignStep.S1999_ALIGN_COMPLETE;
            }
            else if (SeqStep == EmPreAlignStep.S1999_ALIGN_COMPLETE)
            {
                SeqStep = EmPreAlignStep.S2000_MCR_XAIS_MOVE;
            }
        }


        private int _mcrTryCount = 0;
        private bool _isMcrComplete = true;
        public McrResult _mcr1stResult = null;
        public McrResult _mcr2stResult = null;
        private void McrLogicWorking(Equipment equip)
        {
            if (SeqStep == EmPreAlignStep.S2000_MCR_XAIS_MOVE)
            {
                if (AlignUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.Cal_Mcr_Read_A_1st, equip.CurrentRecipe, null))
                        SeqStep = EmPreAlignStep.S2010_MCR_START_WAIT;
                }
                else if (AlignUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmPreAlignXAxisServo.Cal_Mcr_Read_B_1st, equip.CurrentRecipe, null))
                        SeqStep = EmPreAlignStep.S2010_MCR_START_WAIT;
                }
            }
            else if (SeqStep == EmPreAlignStep.S2010_MCR_START_WAIT)
            {
            }
            else if (SeqStep == EmPreAlignStep.S2020_1ST_MCR_START)
            {
                _mcr1stResult = new McrResult();
                _mcr2stResult = new McrResult();
                _isMcrComplete = false;
                _mcrTryCount++; ;
                SeqStep = EmPreAlignStep.S2020_1ST_MCR_START;
            }
            else if (SeqStep == EmPreAlignStep.S2030_)
            {
                //1번 얼라인 마트.
                equip.Mcr1stProxy.SendCmd();
                equip.Mcr2ndProxy.SendCmd();

                SeqStep = EmPreAlignStep.S2040_;
            }
            else if (SeqStep == EmPreAlignStep.S2040_)
            {
                if (equip.Mcr1stProxy.IsReadComplete == true && equip.Mcr2ndProxy.IsReadComplete == true)
                {
                    if (equip.Mcr1stProxy.IsReadSuccess && equip.Mcr2ndProxy.IsReadSuccess) //결과 확인 
                    {
                        _mcr1stResult.PanelID = equip.Mcr1stProxy.ReadData;
                        _mcr2stResult.PanelID = equip.Mcr2ndProxy.ReadData;

                        SeqStep = EmPreAlignStep.S2050_;
                    }
                    else
                    {
                        if (_alignTryCount < 3)
                        {
                            SeqStep = EmPreAlignStep.S2020_1ST_MCR_START;
                        }
                        else
                        {
                            //알람 처리. 
                        }
                    }
                }
            }
            else if (SeqStep == EmPreAlignStep.S2050_)
            {
                _isMcrComplete = true;
                SeqStep = EmPreAlignStep.S2999_MCR_COMPLETE;
            }
            else if (SeqStep == EmPreAlignStep.S2999_MCR_COMPLETE)
            {

            }
        }

        //메소드 - 각 장치별 인터락
        public bool XAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            return false;
        }
        public bool StartMcr(PanelInfoSet panelSet, EmUnitPosi unitPosi)
        {
            if (SeqStep == EmPreAlignStep.S2010_MCR_START_WAIT)
            {
                SeqStep = EmPreAlignStep.S2020_1ST_MCR_START;
                _isMcrComplete = false;
                return true;
            }
            else
            {
                //알람 처리
                return false;
            }
        }
        public bool IsMcrComplete(PanelInfoSet panelSet, EmUnitPosi unitPosi, out McrResult pnl1Result, out McrResult pnl2Result)
        {
            pnl1Result = new McrResult();
            pnl2Result = new McrResult();
            if (GG.ExceptCstLoaderUnloader) return true;

            if (SeqStep == EmPreAlignStep.S2999_MCR_COMPLETE && _isMcrComplete == true)
            {
                pnl1Result = _mcr1stResult;
                pnl2Result = _mcr2stResult;
                return true;
            }
            else
            {
                pnl1Result = null;
                pnl2Result = null;
                return false;
            }
        }
        public void ResetPreAlignMcr()
        {
            SeqStep = EmPreAlignStep.S1000_PRE_ALIGN_WAIT;
        }
    }
}

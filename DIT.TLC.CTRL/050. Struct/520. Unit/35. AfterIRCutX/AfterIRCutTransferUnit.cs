﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using Dit.Framework.Comm;

namespace DIT.TLC.CTRL
{
    public class AfterIRCutTransferUnit : BaseUnit
    {
        public enum EmBreakTransferUnitCmd
        {
            S000_WAIT,
            S0100,
            S0200,
            S1500,
            S0400,
            S0500,
            S0600,
            S0700,
            S0800,
            S0900,
            S1000,
            S2000,
            S3000,
            S3100,
            S3200,
            S1100,
            S0210,
            S1200,
            S1300,

        }

        public AfterIRCutXAxisServo XAxis { get; set; }
        public AfterIRCutYAxisServo YAxis { get; set; }

        public Cylinder UnDownCylinder1 = new Cylinder();
        public Cylinder UnDownCylinder2 = new Cylinder();

        public Switch2Cmd1Sensor Vaccum1                    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1                         /**/= new SwitchOneWay();
        public Switch2Cmd1Sensor Vaccum2                    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower2                         /**/= new SwitchOneWay();


        public EmBreakTransferUnitCmd StepCmd { get; set; }

        public PioSendStep TPioSend { get; set; }
        public PioRecvStep TPioRecv { get; set; }

        public PioSendStep BPioSend { get; set; }
        public PioRecvStep BPioRecv { get; set; }
        public PlcAddr Vaccum1Value { get; set; }
        public PlcAddr Vaccum2Value { get; set; }

        public AfterIRCutTransferUnit()
        {
            StepCmd = EmBreakTransferUnitCmd.S000_WAIT;
        }


        public override void LogicWorking(Equipment equip)
        {
            base.LogicWorking(equip);

            YAxis.LogicWorking(equip);
            UnDownCylinder1.LogicWorking(equip);
            UnDownCylinder2.LogicWorking(equip);
            Vaccum1.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower2.LogicWorking(equip);




            LoaderCmdLogicWorking(equip);
            LoaderAutoLogicWorking();
        }
        public void LoaderCmdLogicWorking(Equipment equip)
        {
            if (equip.RunMode == EmEquipRunMode.Manual)
                return;

            //if (StepCmd == EmBreakTransferUnitCmd.S000_WAIT)
            //{
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0100)
            //{
            //    if (IsExitsPanel)
            //    {
            //        //PNL 있을때. 
            //    }
            //    else
            //    {
            //        //PNL 없을때.
            //        StepCmd = EmBreakTransferUnitCmd.S0200;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0200)
            //{
            //    if (true) //어떤 조건으로 이동 상하 반송 순서 조정
            //    {
            //        StepCmd = EmBreakTransferUnitCmd.S0210;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0210)
            //{
            //    if (true) //어떤 조건으로 이동 상하 반송 순서 조정
            //    {
            //        StepCmd = EmBreakTransferUnitCmd.S1000;//상 반송
            //    }
            //    else
            //    {
            //        StepCmd = EmBreakTransferUnitCmd.S2000; //하 반송
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1000)
            //{
            //    if (XAxis.IsMoveOnPosition(BreakTransferXAxisServo.TLoadingPosiNo) == false)
            //        XAxis.MovePosition(equip, BreakTransferXAxisServo.TLoadingPosiNo);

            //    StepCmd = EmBreakTransferUnitCmd.S1100;
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1100)
            //{
            //    if (XAxis.IsMoveOnPosition(BreakTransferXAxisServo.TLoadingPosiNo) == false)
            //    {
            //        YAxis.MovePosition(equip, BreakTransferXAxisServo.TLoadingPosiNo);
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1100)
            //{
            //    if (YAxis.IsMoveOnPosition(BreakTransferXAxisServo.TLoadingPosiNo) == false)
            //    {
            //        TPioRecv.StartPioRecv(equip, this);
            //        StepCmd = EmBreakTransferUnitCmd.S1200;
            //    }
            //}

            //else if (StepCmd == EmBreakTransferUnitCmd.S1200)
            //{
            //    if (TPioRecv.IsRecvStarted())
            //    {
            //        ZAxis.Forward(equip);
            //        StepCmd = EmBreakTransferUnitCmd.S1300;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1300)
            //{
            //    if (ZAxis.IsForward)
            //    {
            //        Vaccum.OnOff(equip, true);
            //        StepCmd = EmBreakTransferUnitCmd.S1500;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1500)
            //{
            //    if (ZAxis.IsForward && Vaccum.IsOnOff == true)
            //    {
            //        TProcessStage.Vaccum.OnOff(equip, false);
            //        StepCmd = EmBreakTransferUnitCmd.S1500;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1500)
            //{
            //    if (TProcessStage.Vaccum.IsOnOff == true)
            //    {

            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1500)
            //{
            //    if (XAxis.IsMoveOnPosition(ProcessTransferXAxis.LoadingPosiNo) == true)
            //    {
            //        this.PioRecv.StartPioRecv(equip, this);
            //        StepCmd = EmBreakTransferUnitCmd.S0400;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0400)
            //{
            //    if (this.PioRecv.IsRecvStarted())
            //    {
            //        StepCmd = EmBreakTransferUnitCmd.S0500;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0500)
            //{
            //    if (LoaderTransferXAxisUnloadPosi == -1)
            //    {
            //        //인터락 처리 필요함. 
            //        return;
            //    }

            //    if (LoaderTransfer.XAxis.IsMoveOnPosition(LoaderTransferXAxisUnloadPosi)
            //        && LoaderTransfer.YAxis.IsMoveOnPosition(LoaderTransferYAxisUnloadPosi)
            //        && LoaderTransfer.ZAxis.IsBackward) // STATGE별 코드 값이 다름. 
            //    {
            //        LoaderTransfer.ZAxis.Forward(equip);

            //        StepCmd = EmBreakTransferUnitCmd.S0600;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0600)
            //{
            //    if (LoaderTransfer.ZAxis.IsForward == true)
            //    {
            //        Vaccum.OnOff(equip, true);
            //        StepCmd = EmBreakTransferUnitCmd.S0700;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0700)
            //{
            //    if (Vaccum.IsOnOff == true)
            //    {
            //        LoaderTransfer.Vaccum.OnOff(equip, false);
            //        StepCmd = EmBreakTransferUnitCmd.S0800;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0800)
            //{
            //    if (Vaccum.IsOnOff == true || LoaderTransfer.Vaccum.IsOnOff == false)
            //    {
            //        LoaderTransfer.ZAxis.Backward(equip);
            //        StepCmd = EmBreakTransferUnitCmd.S0900;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S0900)
            //{
            //    if (LoaderTransfer.ZAxis.IsBackward == true || LoaderTransfer.Vaccum.IsOnOff == false)
            //    {
            //        this.PioRecv.PioComplete();

            //        if (XAxis.MovePosition(equip, ProcessTransferXAxis.ProcessPosiNo) == false) return;

            //        StepCmd = EmBreakTransferUnitCmd.S1000;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S1000)
            //{
            //    if (XAxis.IsMoveOnPosition(ProcessTransferXAxis.ProcessPosiNo) == true)
            //    {
            //        //Process 처리.                 
            //        StepCmd = EmBreakTransferUnitCmd.S2000;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S2000)
            //{
            //    //Process 완료 대기. 
            //    StepCmd = EmBreakTransferUnitCmd.S3000;
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S3000)  //언로딩 위치로 이동. 
            //{
            //    if (XAxis.MovePosition(equip, ProcessTransferXAxis.UnloadingPosiNo) == false) return;
            //    StepCmd = EmBreakTransferUnitCmd.S3100;
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S3100)
            //{
            //    if (XAxis.IsMoveOnPosition(ProcessTransferXAxis.UnloadingPosiNo) == true)
            //    {
            //        this.PioSend.StartPioSend(equip, this);
            //        StepCmd = EmBreakTransferUnitCmd.S3200;
            //    }
            //}
            //else if (StepCmd == EmBreakTransferUnitCmd.S3200)
            //{
            //    if (this.PioSend.IsSendComplete())
            //    {
            //        //PNL 없는지 확인하고, 다음 포지션 확인 후. 다음 동작 처리
            //        if (true)
            //        {
            //            StepCmd = EmBreakTransferUnitCmd.S0100;
            //        }
            //    }

            //}
        }
        public void LoaderAutoLogicWorking()
        {
        }

    }
}

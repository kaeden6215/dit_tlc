﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class UnLoaderTransferUnit : BaseUnit
    {

        public UnloaderTransferYAxisServo Y1Axis { get; set; }
        public UnloaderTransferYAxisServo Y2Axis { get; set; }

        public UnloaderTransferTAxisServo T1Axis { get; set; }
        public UnloaderTransferTAxisServo T2Axis { get; set; }

        public Cylinder UpDonw1Cylinder = new Cylinder();
        public Cylinder UpDonw2Cylinder = new Cylinder(); 
     

        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();


        public PlcAddr HandPressureValue1 { get; internal set; }
        public PlcAddr HandPressureValue2 { get; internal set; }


        public override void LogicWorking(Equipment equip)
        {
            Y1Axis.LogicWorking(equip);
            Y2Axis.LogicWorking(equip);

            T1Axis.LogicWorking(equip);
            T2Axis.LogicWorking(equip);
            UpDonw1Cylinder.LogicWorking(equip);

            UpDonw2Cylinder.LogicWorking(equip);
            

            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);
        }
    }
}


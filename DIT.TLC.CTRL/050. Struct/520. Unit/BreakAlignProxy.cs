﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class BreakAlignProxy : BaseUnit
    {
        public BreakAlignXAxisServo XAxis { get; set; }
        public BreakAlignZAxisServo ZAxis { get; set; }

        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);
            ZAxis.LogicWorking(equip);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class PioSendStep
    {

        public BaseUnit RecvUnit { get; set; }
        public PioRecvStep PioRecv { get; set; }

        
        public bool SendAble { get; set; }
        public bool SendStart { get; set; }
        public bool SendComplete { get; set; }
                

        //판넬 하류로 배출 PIO 
        private EmSendPio _sendPio = EmSendPio.DW_000_WAIT;

        public PioSendStep(BaseUnit recvUnit, PioRecvStep pioRecv)
        {
            RecvUnit = recvUnit;
            PioRecv = pioRecv;
        }

        public void StartPioSend(Equipment equip, BaseUnit unit)
        {
        }
        public virtual void PioPnlSendLogicWorking()
        {
            if (_sendPio == EmSendPio.DW_000_WAIT)
            {
            }
            else if (_sendPio == EmSendPio.DW_010_SEND_START)
            {
                this.SendAble = true;
                this.SendStart = false;
                this.SendComplete = false;

                _sendPio = EmSendPio.DW_020;
            }
            else if (_sendPio == EmSendPio.DW_020)
            {
                if (PioRecv.RecvAble == true && PioRecv.RecvStart == true && PioRecv.RecvComplete == false)
                {
                    this.SendAble = true;
                    this.SendStart = true;
                    this.SendComplete = false;

                    OnSendActionStart(RecvUnit);

                    _sendPio = EmSendPio.DW_030;
                }
            }
            else if (_sendPio == EmSendPio.DW_030)
            {
                if (PioRecv.RecvAble == true && PioRecv.RecvStart == true && PioRecv.RecvComplete == false)
                {
                    if (IsSendComplete() == true)
                    {
                        this.SendAble = true;
                        this.SendStart = true;
                        this.SendComplete = true;
                        _sendPio = EmSendPio.DW_040;
                    }
                }
            }
            else if (_sendPio == EmSendPio.DW_040)
            {
                if (PioRecv.RecvAble == true && PioRecv.RecvStart == true && PioRecv.RecvComplete == true)
                {
                    this.SendAble = false;
                    this.SendStart = false;
                    this.SendComplete = false;
                    _sendPio = EmSendPio.DW_050;
                }
            }
            else if (_sendPio == EmSendPio.DW_050)
            {
                if (PioRecv.RecvAble == false && PioRecv.RecvStart == false && PioRecv.RecvComplete == false)
                {
                    _sendPio = EmSendPio.DW_000_WAIT;
                }
            }
        }
        public virtual bool IsSendComplete()
        {
            return false;
        }
        public virtual void OnSendActionStart(BaseUnit recvUnit)
        {
            //물류 정보 복사 필요.
        }

        public bool IsReadyPosition()
        {
            return false;
        }
    }
}

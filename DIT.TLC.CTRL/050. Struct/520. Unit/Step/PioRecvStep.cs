﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class PioRecvStep
    {
        public BaseUnit SendUnit { get; set; }
        public PioSendStep PioSend { get; set; }

        
        public bool RecvAble { get; set; }
        public bool RecvStart { get; set; }
        public bool RecvComplete { get; set; }

        public PanelInfo CurrPanelInfo
        {
            /** ShareMem 처리 예정.  **/
            get;
            set;
        }



        //판넬 상류에서 배입 PIO
        private EmRecvPio _recvPio = EmRecvPio.UP_000_WAIT;

        public PioRecvStep(BaseUnit sendUnit, PioSendStep pioSend)
        {
            SendUnit = sendUnit;
            PioSend = pioSend;
        }

        public void StartPioRecv(Equipment equip, BaseUnit unit)
        {
        }

        public virtual void StartActionCmd(EmLoaderUnitCmd cmd)
        {

        }
        public virtual bool IsRecvStarted()
        {
            return false;
        }
        public virtual void PioPnlRecvLogicWorking()
        {
            if (_recvPio == EmRecvPio.UP_000_WAIT)
            {

            }
            else if (_recvPio == EmRecvPio.UP_010_RECV_START)
            {
                this.RecvAble = true;
                this.RecvStart = false;
                this.RecvComplete = false;

                _recvPio = EmRecvPio.UP_020;
            }
            else if (_recvPio == EmRecvPio.UP_020)
            {
                if (PioSend.SendAble == true && PioSend.SendStart == false && PioSend.SendComplete == false)
                {
                    this.RecvAble = true;
                    this.RecvStart = true;
                    this.RecvComplete = false;

                    _recvPio = EmRecvPio.UP_030;
                }
            }
            else if (_recvPio == EmRecvPio.UP_030)
            {
                if (PioSend.SendAble == true && PioSend.SendStart == true && PioSend.SendComplete == false)
                {
                    OnRecvActionStart(SendUnit);
                    _recvPio = EmRecvPio.UP_040;
                }
            }
            else if (_recvPio == EmRecvPio.UP_040)
            {
                if (PioSend.SendAble == true && PioSend.SendStart == true && PioSend.SendComplete == true)
                {
                    if (IsRecvActionComplete() == true)
                    {
                        this.RecvAble = true;
                        this.RecvStart = true;
                        this.RecvComplete = true;
                        _recvPio = EmRecvPio.UP_050;
                    }
                }
            }
            else if (_recvPio == EmRecvPio.UP_050)
            {
                if (PioSend.SendAble == false && PioSend.SendStart == false && PioSend.SendComplete == false)
                {
                    this.RecvAble = false;
                    this.RecvStart = false;
                    this.RecvComplete = false;

                    _recvPio = EmRecvPio.UP_060;
                }
            }
            else if (_recvPio == EmRecvPio.UP_060)
            {
                _recvPio = EmRecvPio.UP_000_WAIT;
            }
        }
        public virtual void OnRecvActionStart(BaseUnit sendUint)
        {
            //this.CurrPanelInfo = sendUint.CurrPanelInfo;
        }
        public virtual bool IsRecvActionComplete()
        {
            return false;
        }


        public PanelInfo RecvPanelInfo { get; set; }

        internal void PioComplete()
        {
            throw new NotImplementedException();
        }
    }
}

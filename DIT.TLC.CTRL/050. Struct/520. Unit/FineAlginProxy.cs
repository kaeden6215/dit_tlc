﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class FineAlginProxy : BaseUnit
    {
        public FineAlignXAxisServo XAxis { get; set; }

        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class ServoParamMgr
    {
        public static bool LoadParam(Equipment equip)
        {
            int errCnt = 0;

            //CstLoader_B
            if (LoadServoParam(equip, equip.LD.CstLoader_B.CstRotationAxis, Path.Combine(GG.StartupPath, "Setting", "CstLoader_BCstRotationAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.CstLoader_B.CstUpDownAxis, Path.Combine(GG.StartupPath, "Setting", "CstLoader_BCstUpDownAxisParam.xml"))) errCnt++;

            //CstLoader_A
            if (LoadServoParam(equip, equip.LD.CstLoader_A.CstRotationAxis, Path.Combine(GG.StartupPath, "Setting", "CstLoader_ACstRotationAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.CstLoader_A.CstUpDownAxis, Path.Combine(GG.StartupPath, "Setting", "CstLoader_ACstUpDownAxisParam.xml"))) errCnt++;

            //Loader
            if (LoadServoParam(equip, equip.LD.Loader.X1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderX1AxisPosi.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.Loader.X2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderX2AxisPosi.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.Loader.Y1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderY1AxisPosi.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.Loader.Y2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderY2AxisPosi.xml"))) errCnt++;

            //LoaderTransfer_B
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_B.X1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BX1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_B.X2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BX2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_B.YAxis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BYAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_B.SubY1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubY1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_B.SubY2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubY2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_B.SubT1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubT1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_B.SubT2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubT2AxisParam.xml"))) errCnt++;


            //LoaderTransfer_A
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_A.X1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_AX1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_A.X2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_AX2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_A.YAxis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_AYAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_A.SubY1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubY1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_A.SubY2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubY2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_A.SubT1Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubT1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.LD.LoaderTransfer_A.SubT2Axis, Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubT2AxisParam.xml"))) errCnt++;

            //PreAlign
            if (LoadServoParam(equip, equip.LD.PreAlign.XAxis, Path.Combine(GG.StartupPath, "Setting", "PreAlignXAxisParam.xml"))) errCnt++;

            //FineAlgin
            if (LoadServoParam(equip, equip.PROC.FineAlign.XAxis, Path.Combine(GG.StartupPath, "Setting", "FineAlginXAxisParam.xml"))) errCnt++;


            //IRCutStage_B
            if (LoadServoParam(equip, equip.PROC.IRCutStage_B.YAxis, Path.Combine(GG.StartupPath, "Setting", "IRCutStage_BYAxisParam.xml"))) errCnt++;

            //IRCutStage_A
            if (LoadServoParam(equip, equip.PROC.IRCutStage_A.YAxis, Path.Combine(GG.StartupPath, "Setting", "IRCutStage_AYAxisParam.xml"))) errCnt++;

            //LaserHead
            if (LoadServoParam(equip, equip.PROC.LaserHead.XAxis, Path.Combine(GG.StartupPath, "Setting", "LaserHeadXAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.LaserHead.ZAxis, Path.Combine(GG.StartupPath, "Setting", "LaserHeadZAxisParam.xml"))) errCnt++;

            //AfterIRCutTransfer_B
            if (LoadServoParam(equip, equip.PROC.AfterIRCutTransfer_B.YAxis, Path.Combine(GG.StartupPath, "Setting", "AfterIRCutTransfer_BYAxisParam.xml"))) errCnt++;

            //AfterIRCutTransfer_A
            if (LoadServoParam(equip, equip.PROC.AfterIRCutTransfer_A.YAxis, Path.Combine(GG.StartupPath, "Setting", "AfterIRCutTransfer_AYAxisParam.xml"))) errCnt++;

            //AfterIRCutTransfer_B
            if (LoadServoParam(equip, equip.PROC.BreakStage_B.YAxis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_BYAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakStage_B.SubY1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubY1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakStage_B.SubY2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubY2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakStage_B.SubX1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubX1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakStage_B.SubX2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubX2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakStage_B.SubT1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubT1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakStage_B.SubT2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubT2AxisParam.xml"))) errCnt++;

            //BreakStage_A
             if (LoadServoParam(equip, equip.PROC.BreakStage_A.YAxis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_AYAxisParam.xml"))) errCnt++;
             if (LoadServoParam(equip, equip.PROC.BreakStage_A.SubY1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubY1AxisParam.xml"))) errCnt++;
             if (LoadServoParam(equip, equip.PROC.BreakStage_A.SubY2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubY2AxisParam.xml"))) errCnt++;
             if (LoadServoParam(equip, equip.PROC.BreakStage_A.SubX1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubX1AxisParam.xml"))) errCnt++;
             if (LoadServoParam(equip, equip.PROC.BreakStage_A.SubX2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubX2AxisParam.xml"))) errCnt++;
             if (LoadServoParam(equip, equip.PROC.BreakStage_A.SubT1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubT1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakStage_A.SubT2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubT2AxisParam.xml"))) errCnt++;

            //BreakingHead
             if (LoadServoParam(equip, equip.PROC.BreakingHead.X1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakingHeadX1AxisParam.xml"))) errCnt++;
             if (LoadServoParam(equip, equip.PROC.BreakingHead.X2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakingHeadX2AxisParam.xml"))) errCnt++;
             if (LoadServoParam(equip, equip.PROC.BreakingHead.Z1Axis, Path.Combine(GG.StartupPath, "Setting", "BreakingHeadZ1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakingHead.Z2Axis, Path.Combine(GG.StartupPath, "Setting", "BreakingHeadZ2AxisParam.xml"))) errCnt++;

            //BreakAlign
            if (LoadServoParam(equip, equip.PROC.BreakAlign.XAxis, Path.Combine(GG.StartupPath, "Setting", "BreakAlignXAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.PROC.BreakAlign.ZAxis, Path.Combine(GG.StartupPath, "Setting", "BreakAlignZAxisParam.xml"))) errCnt++;

                    //AfterInspUnloaderTransfer_B
            if (LoadServoParam(equip, equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis, Path.Combine(GG.StartupPath, "Setting", "BeforeInspUnloaderTransfer_BY1AxisParam.xml"))) errCnt++;
               
            //AfterInspUnloaderTransfer_A
            if (LoadServoParam(equip, equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis, Path.Combine(GG.StartupPath, "Setting", "BeforeInspUnloaderTransfer_AY1AxisParam.xml"))) errCnt++;

            //InspCamera
            if (LoadServoParam(equip, equip.UD.InspCamera.XAxis, Path.Combine(GG.StartupPath, "Setting", "InspCameraXAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.InspCamera.ZAxis, Path.Combine(GG.StartupPath, "Setting", "InspCameraZAxisParam.xml"))) errCnt++;

            //InspectionStage_B
            if (LoadServoParam(equip, equip.UD.InspectionStage_B.YAxis, Path.Combine(GG.StartupPath, "Setting", "InspectionStage_BYAxisParam.xml"))) errCnt++;

            //InspectionStage_A
            if (LoadServoParam(equip, equip.UD.InspectionStage_A.YAxis, Path.Combine(GG.StartupPath, "Setting", "InspectionStage_AYAxisParam.xml"))) errCnt++;

            //AfterInspUnloaderTransfer_B
            if (LoadServoParam(equip, equip.UD.AfterInspUnloaderTransfer_B.Y2Axis, Path.Combine(GG.StartupPath, "Setting", "AfterInspUnloaderTransfer_BY2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.AfterInspUnloaderTransfer_B.T1Axis, Path.Combine(GG.StartupPath, "Setting", "AfterInspUnloaderTransfer_BT1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.AfterInspUnloaderTransfer_B.T2Axis, Path.Combine(GG.StartupPath, "Setting", "AfterInspUnloaderTransfer_BT2AxisParam.xml"))) errCnt++;

            //AfterInspUnloaderTransfer_A
            if (LoadServoParam(equip, equip.UD.AfterInspUnloaderTransfer_A.Y2Axis, Path.Combine(GG.StartupPath, "Setting", "AfterInspUnloaderTransfer_AY2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.AfterInspUnloaderTransfer_A.T1Axis, Path.Combine(GG.StartupPath, "Setting", "AfterInspUnloaderTransfer_AT1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.AfterInspUnloaderTransfer_A.T2Axis, Path.Combine(GG.StartupPath, "Setting", "AfterInspUnloaderTransfer_AT2AxisParam.xml"))) errCnt++;

            //Unloader
            if (LoadServoParam(equip, equip.UD.Unloader.X1Axis, Path.Combine(GG.StartupPath, "Setting", "UnloaderX1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.Unloader.X2Axis, Path.Combine(GG.StartupPath, "Setting", "UnloaderX2AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.Unloader.Y1Axis, Path.Combine(GG.StartupPath, "Setting", "UnloaderY1AxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.Unloader.Y2Axis, Path.Combine(GG.StartupPath, "Setting", "UnloaderY2AxisParam.xml"))) errCnt++;

            //CstUnloader_B
            if (LoadServoParam(equip, equip.UD.CstUnloader_B.CstRotationAxis, Path.Combine(GG.StartupPath, "Setting", "CstUnloader_BCstRotationAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.CstUnloader_B.CstUpDownAxis, Path.Combine(GG.StartupPath, "Setting", "CstUnloader_BCstUpDownAxisParam.xml"))) errCnt++;

            //CstUnloader_A
            if (LoadServoParam(equip, equip.UD.CstUnloader_A.CstRotationAxis, Path.Combine(GG.StartupPath, "Setting", "CstUnloader_ACstRotationAxisParam.xml"))) errCnt++;
            if (LoadServoParam(equip, equip.UD.CstUnloader_A.CstUpDownAxis, Path.Combine(GG.StartupPath, "Setting", "CstUnloader_ACstUpDownAxisParam.xml"))) errCnt++;


            return errCnt <= 0;
        }

        public static bool LoadServoParam(Equipment equip, ServoMotorControl servo, string xmlPath)
        {
            //CstRotationAxis 설정             
            servo.ParamSetting = ServoParamSetting.Load(xmlPath);
            if (servo.ParamSetting == null)
            {
                servo.ParamSetting = new ServoParamSetting();
                servo.ParamSetting.DefaultCreate(servo);
                servo.ParamSetting.XmlPath = xmlPath;

                /*알람 처리*/
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0054_);
                return false;
            }
            return true;
        }
    }
}

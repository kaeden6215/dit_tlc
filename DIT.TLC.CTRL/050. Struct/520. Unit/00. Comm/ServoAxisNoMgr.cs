﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class SAxisNoMgr
    {
        public readonly static int UMAC_가공_STAGE_Y1 = 1;
        public readonly static int UMAC_가공_STAGE_Y2 = 2;
        public readonly static int UMAC_가공_HEAD_X1 = 3;
        public readonly static int UMAC_FINE_ALIGN_X1 = 4;
        public readonly static int UMAC_BREAKING_STAGE_Y1 = 5;
        public readonly static int UMAC_BREAKING_STAGE_Y2 = 6;
        public readonly static int UMAC_BREAKING_HEAD_X1 = 7;
        public readonly static int UMAC_BREAKING_HEAD_X2 = 8;
        public readonly static int UMAC_BREAKING_JIG_X1 = 9;

        public readonly static int UMAC_SPARE = 10;

        public readonly static int UMAC_가공_HEAD_Z1 = 13;
        public readonly static int UMAC_BREAKING_HEAD_Z1 = 14;
        public readonly static int UMAC_BREAKING_HEAD_Z2 = 15;
        public readonly static int UMAC_BREAKING_JIG_Z1 = 16;

        public readonly static int AJIN_취출_HAND_X1 = 31;
        public readonly static int AJIN_취출_HAND_X2 = 32;
        public readonly static int AJIN_LD_TR_Y1 = 33;
        public readonly static int AJIN_LD_TR_Y2 = 34;
        public readonly static int AJIN_PRE_ALIGN_X1 = 35;
        public readonly static int AJIN_Cassette_Lifter_Z1 = 4;
        public readonly static int AJIN_Cassette_Lifter_Z2 = 5;
        public readonly static int AJIN_Cassette_Lifter_R1 = 6;
        public readonly static int AJIN_Cassette_Lifter_R2 = 7;
        public readonly static int AJIN_취출_HAND_Y1 = 8;
        public readonly static int AJIN_취출_HAND_Y2 = 9;
        public readonly static int AJIN_LD_TR_X1 = 10;
        public readonly static int AJIN_LD_TR_Y3 = 11;
        public readonly static int AJIN_LD_TR_X2 = 12;
        public readonly static int AJIN_LD_TR_X3 = 13;
        public readonly static int AJIN_LD_TR_X4 = 14;
        public readonly static int AJIN_LD_TR_Y4 = 15;
        public readonly static int AJIN_LD_TR_Y5 = 16;
        public readonly static int AJIN_LD_TR_Y6 = 17;
        public readonly static int AJIN_LD_TR_T1 = 0;
        public readonly static int AJIN_LD_TR_T2 = 1;
        public readonly static int AJIN_LD_TR_T3 = 2;
        public readonly static int AJIN_LD_TR_T4 = 3;
        public readonly static int AJIN_에프터_IR_CUT_TR_Y1 = 36;
        public readonly static int AJIN_에프터_IR_CUT_TR_Y2 = 37;
        public readonly static int AJIN_ULD_TR_Y1 = 38;
        public readonly static int AJIN_ULD_TR_Y2 = 39;
        public readonly static int AJIN_ULD_TR_Y3 = 40;
        public readonly static int AJIN_ULD_TR_Y4 = 41;
        public readonly static int AJIN_AOI_Camera_X1 = 42;
        public readonly static int AJIN_투입_HAND_X1 = 43;
        public readonly static int AJIN_투입_HAND_X2 = 44;
        public readonly static int AJIN_ULD_TR_T1 = 18;
        public readonly static int AJIN_ULD_TR_T2 = 19;
        public readonly static int AJIN_ULD_TR_T3 = 20;
        public readonly static int AJIN_ULD_TR_T4 = 21;
        public readonly static int AJIN_SPARE = 99;
        public readonly static int AJIN_AOI_Stage_Y1 = 22;
        public readonly static int AJIN_AOI_Stage_Y2 = 23;
        public readonly static int AJIN_AOI_Camera_Z1 = 24;
        public readonly static int AJIN_투입_HAND_Y1 = 25;
        public readonly static int AJIN_투입_HAND_Y2 = 26;
        public readonly static int AJIN_Cassette_Stage_R3 = 27;
        public readonly static int AJIN_Cassette_Stage_R4 = 28;
        public readonly static int AJIN_Cassette_Lifter_Z3 = 29;
        public readonly static int AJIN_Cassette_Lifter_Z4 = 30;


        public readonly static int EZI_BREAKING_STAGE_T1 = 10;
        public readonly static int EZI_BREAKING_STAGE_T2 = 11;
        public readonly static int EZI_BREAKING_STAGE_T3 = 12;
        public readonly static int EZI_BREAKING_STAGE_T4 = 13;

        public readonly static int EZI_BREAKING_STAGE_Y3 = 2;
        public readonly static int EZI_BREAKING_STAGE_Y4 = 3;
        public readonly static int EZI_BREAKING_STAGE_Y5 = 4;
        public readonly static int EZI_BREAKING_STAGE_Y6 = 5;

        public readonly static int EZI_BREAKING_STAGE_X1 = 6;
        public readonly static int EZI_BREAKING_STAGE_X2 = 7;
        public readonly static int EZI_BREAKING_STAGE_X3 = 8;
        public readonly static int EZI_BREAKING_STAGE_X4 = 9;




    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class MasureUnit : BaseUnit
    {         
        public UnLoaderXAxisServo X1Axis { get; set; }

        public void InitializeInterLock()
        {
            X1Axis.CheckStartMoveInterLockFunc = X1Axis_CheckStartMoveInterLockFunc;
        }
        public override void LogicWorking(Equipment equip)
        {
            X1Axis.LogicWorking(equip);        
        }


        //각 장치별 인터락
        public bool X1Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class ServoPositionMgr
    {
        public static bool LoadPosition(Equipment equip)
        {
            int errCnt = 0;


            // TOP B열
            // BOT A열
            #region CstLoader
            //CstRotationAxis 설정 

            equip.LD.CstLoader_A.CstRotationAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstLoader_ACstRotationAxisPosi.xml");
            equip.LD.CstLoader_A.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카세트:  0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_A.CstRotationAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카세트: 90도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_A.CstRotationAxis},
             };
            if (equip.LD.CstLoader_A.CstRotationAxis.Setting.Load() == false) errCnt++;

            equip.LD.CstLoader_B.CstRotationAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstLoader_BCstRotationAxisPosi.xml");
            equip.LD.CstLoader_B.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
             {
                 new ServoPosiInfo() {No = 0, Name = "B 카세트:  0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_B.CstRotationAxis},
                 new ServoPosiInfo() {No = 1, Name = "B 카세트: 90도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_B.CstRotationAxis},
             };
            if (equip.LD.CstLoader_B.CstRotationAxis.Setting.Load() == false) errCnt++;

            //CstUpDownAxis 설정 
            equip.LD.CstLoader_A.CstUpDownAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstLoader_ACstUpDownAxisPosi.xml");
            equip.LD.CstLoader_A.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카세트(Z): Cst LD",    Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_A.CstUpDownAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카세트(Z): 측정",        Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_A.CstUpDownAxis},
                     new ServoPosiInfo() {No = 2, Name = "A 카세트(Z): Cell 배출",    Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_A.CstUpDownAxis},
             };
            if (equip.LD.CstLoader_A.CstUpDownAxis.Setting.Load() == false) errCnt++;

            equip.LD.CstLoader_B.CstUpDownAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstLoader_BCstUpDownAxisPosi.xml");
            equip.LD.CstLoader_B.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "B 카세트(Z): Cst LD",  Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_B.CstUpDownAxis},
                 new ServoPosiInfo() {No = 1, Name = "B 카세트(Z): 측정",       Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_B.CstUpDownAxis},
                 new ServoPosiInfo() {No = 2, Name = "B 카세트(Z): Cell 배출",  Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.CstLoader_B.CstUpDownAxis},
             };
            if (equip.LD.CstLoader_B.CstUpDownAxis.Setting.Load() == false) errCnt++;
            #endregion

            #region Loader
            //Loader.X1Axis 설정 
            equip.LD.Loader.X1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderX1AxisPosi.xml");
            equip.LD.Loader.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[6]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (X1) : A 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (X1) : A 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (X1) : B 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
                 new ServoPosiInfo() {No = 3, Name = "취출 이재기 (X1) : B 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
                 new ServoPosiInfo() {No = 4, Name = "취출 이재기 (X1) : A열", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
                 new ServoPosiInfo() {No = 5, Name = "취출 이재기 (X1) : B열", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X1Axis},
             };
            if (equip.LD.Loader.X1Axis.Setting.Load() == false) errCnt++;

            //Loader.X2Axis 설정
            equip.LD.Loader.X2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderX2AxisPosi.xml");
            equip.LD.Loader.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[6]
            {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (X2) : A 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (X2) : A 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (X2) : B 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
                 new ServoPosiInfo() {No = 3, Name = "취출 이재기 (X2) : B 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
                 new ServoPosiInfo() {No = 4, Name = "취출 이재기 (X2) : A열", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
                 new ServoPosiInfo() {No = 5, Name = "취출 이재기 (X2) : B열", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.X2Axis},
             };
            if (equip.LD.Loader.X2Axis.Setting.Load() == false) errCnt++;

            //Loader.Y1Axis 설정 
            equip.LD.Loader.Y1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderY1AxisPosi.xml");
            equip.LD.Loader.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[6]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (Y1) : A 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (Y1) : B 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (Y1) : Cst 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 3, Name = "취출 이재기 (Y1) : A Cell UL/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 4, Name = "취출 이재기 (Y1) : B Cell UL/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
                 new ServoPosiInfo() {No = 5, Name = "취출 이재기 (Y1) : 2mm 후퇴 속도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y1Axis},
             };
            if (equip.LD.Loader.Y1Axis.Setting.Load() == false) errCnt++;

            //Loader.Y2Axis 설정
            equip.LD.Loader.Y2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderY2AxisPosi.xml");
            equip.LD.Loader.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[6]
            {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (Y2) : A 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                 new ServoPosiInfo() {No = 1, Name = "취출 이재기 (Y2) : B 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                 new ServoPosiInfo() {No = 2, Name = "취출 이재기 (Y2) : Cst 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                 new ServoPosiInfo() {No = 3, Name = "취출 이재기 (Y2) : Cell UL/D Start", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                 new ServoPosiInfo() {No = 4, Name = "취출 이재기 (Y2) : Cell UL/D End", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
                 new ServoPosiInfo() {No = 5, Name = "취출 이재기 (Y1) : 2mm 후퇴 속도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.Loader.Y2Axis},
            };
            if (equip.LD.Loader.Y2Axis.Setting.Load() == false) errCnt++;
            #endregion

            #region LoaderTransfer
            //LoaderTransfer_A.X1Axis 설정 
            equip.LD.LoaderTransfer_A.X1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_AX1AxisPosi.xml");
            equip.LD.LoaderTransfer_A.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X1) : A1 Cell L/D",       Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X1) : A1 Pre Align",      Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X1) : A1 Laser Stage",    Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.X1Axis},
             };
            if (equip.LD.LoaderTransfer_A.X1Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_A.X2Axis 설정
            equip.LD.LoaderTransfer_A.X2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_AX2AxisPosi.xml");
            equip.LD.LoaderTransfer_A.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X2) : A2 Cell L/D",       Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.X2Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X2) : A2 Pre Align",        Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.X2Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X2) : A2 Laser Stage",      Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.X2Axis},
             };
            if (equip.LD.LoaderTransfer_A.X2Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_B.X1Axis 설정 
            equip.LD.LoaderTransfer_B.X1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BX1AxisPosi.xml");
            equip.LD.LoaderTransfer_B.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X3) : B1 Cell L/D",       Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X3) : B1 Pre Align",      Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X3) : B1 Laser Stage",    Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.X1Axis},
             };
            if (equip.LD.LoaderTransfer_B.X1Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_B.X2Axis 설정
            equip.LD.LoaderTransfer_B.X2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BX2AxisPosi.xml");
            equip.LD.LoaderTransfer_B.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                 new ServoPosiInfo() {No = 0, Name = "로더 이재기 (X4) : B2 Cell L/D",       Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.X2Axis},
                 new ServoPosiInfo() {No = 1, Name = "로더 이재기 (X4) : B2 Pre Align",    Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.X2Axis},
                 new ServoPosiInfo() {No = 2, Name = "로더 이재기 (X4) : B2 Laser Stage",  Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.X2Axis},
             };
            if (equip.LD.LoaderTransfer_B.X2Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_A.YAxis 설정 
            equip.LD.LoaderTransfer_A.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_AYAxisPosi.xml");
            equip.LD.LoaderTransfer_A.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
             {
                 new ServoPosiInfo() {No = 0, Name = "로더 TR (Y1) : Cell L/D",              Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "로더 TR (Y1) : Vision Align",     Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "로더 TR (Y1) : MCR Reading",    Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.YAxis},
                 new ServoPosiInfo() {No = 3, Name = "로더 TR (Y1) : Cell UL/D",           Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.YAxis},
             };
            if (equip.LD.LoaderTransfer_A.YAxis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_B.YAxis 설정 
            equip.LD.LoaderTransfer_B.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BYAxisPosi.xml");
            equip.LD.LoaderTransfer_B.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
             {
                 new ServoPosiInfo() {No = 0, Name = "로더 TR (Y2) : Cell L/D",           Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "로더 TR (Y2) : Vision Align", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "로더 TR (Y2) : MCR Reading",   Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.YAxis},
                 new ServoPosiInfo() {No = 3, Name = "로더 TR (Y2) : Cell UL/D",          Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.YAxis},
             };
            if (equip.LD.LoaderTransfer_B.YAxis.Setting.Load() == false) errCnt++;

            //  //BOT SUB  YY
            //  equip.LD.LoaderTransfer_A.SubY1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubY1AxisPosi.xml");
            //  equip.LD.LoaderTransfer_A.SubY1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            //   {
            //       new ServoPosiInfo() {No = 0, Name = "취출 이재기 (A-sub Y1) : 정밀 보정", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.SubY1Axis},
            //   };
            //  if (equip.LD.LoaderTransfer_A.SubY1Axis.Setting.Load() == false) errCnt++;
            //  
            //  equip.LD.LoaderTransfer_A.SubY2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubY2AxisPosi.xml");
            //  equip.LD.LoaderTransfer_A.SubY2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            //   {
            //       new ServoPosiInfo() {No = 0, Name = "취출 이재기 (A-sub Y2) : 정밀 보정", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.SubY2Axis},
            //   };
            //  if (equip.LD.LoaderTransfer_A.SubY2Axis.Setting.Load() == false) errCnt++;
            //  
            //  //TOP SUB YY
            //  equip.LD.LoaderTransfer_B.SubY1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubY1AxisPosi.xml");
            //  equip.LD.LoaderTransfer_B.SubY1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            //   {
            //       new ServoPosiInfo() {No = 0, Name = "취출 이재기 (B-sub Y1) : 정밀 보정", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.SubY1Axis},
            //   };
            //  if (equip.LD.LoaderTransfer_B.SubY1Axis.Setting.Load() == false) errCnt++;
            //  
            //  equip.LD.LoaderTransfer_B.SubY2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubY2AxisPosi.xml");
            //  equip.LD.LoaderTransfer_B.SubY2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            //   {
            //       new ServoPosiInfo() {No = 0, Name = "취출 이재기 (B-sub Y2) : 정밀 보정", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.SubY2Axis},
            //   };
            //  if (equip.LD.LoaderTransfer_B.SubY2Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_A.SubT1Axis 설정 
            equip.LD.LoaderTransfer_A.SubT1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubT1AxisPosi.xml");
            equip.LD.LoaderTransfer_A.SubT1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (A-sub T1) : A 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.SubT1Axis},
             };
            if (equip.LD.LoaderTransfer_A.SubT1Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_B.SubT1Axis 설정 
            equip.LD.LoaderTransfer_B.SubT1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubT1AxisPosi.xml");
            equip.LD.LoaderTransfer_B.SubT1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (B-sub T1) : B 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.SubT1Axis},
             };
            if (equip.LD.LoaderTransfer_B.SubT1Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_A.SubT2Axis 설정 
            equip.LD.LoaderTransfer_A.SubT2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_ASubT2AxisPosi.xml");
            equip.LD.LoaderTransfer_A.SubT2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (A-sub T2) : A 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_A.SubT2Axis},
             };
            if (equip.LD.LoaderTransfer_A.SubT2Axis.Setting.Load() == false) errCnt++;

            //LoaderTransfer_B.SubT2Axis 설정 
            equip.LD.LoaderTransfer_B.SubT2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LoaderTransfer_BSubT2AxisPosi.xml");
            equip.LD.LoaderTransfer_B.SubT2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
             {
                 new ServoPosiInfo() {No = 0, Name = "취출 이재기 (B-sub T2) : B 피커 0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.LoaderTransfer_B.SubT2Axis},
             };
            if (equip.LD.LoaderTransfer_B.SubT2Axis.Setting.Load() == false) errCnt++;
            #endregion

            equip.LD.PreAlign.XAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "PreAlignXAxis.xml");
            equip.LD.PreAlign.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "Pre Align (X1) : A열 Align ", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.PreAlign.XAxis},
                 new ServoPosiInfo() {No = 1, Name = "Pre Align (X1) : B열 Align ", Position = 0, Speed = 0, Accel = 0, Servo = equip.LD.PreAlign.XAxis},
            };
            if (equip.LD.PreAlign.XAxis.Setting.Load() == false) errCnt++;

            //#region IRCutStage

            //IRCutStage_A.YAxis 설정 1
            equip.PROC.IRCutStage_A.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "IRCutStage_AYAxisPosi.xml");
            equip.PROC.IRCutStage_A.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                 new ServoPosiInfo() {No = 0, Name = "A 가공 테이블 : Cell L/D (Y1)",         Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "A 가공 테이블 : Cell UL/D (Y1)",        Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "A 가공 테이블 : 레이저 컷팅 (Y1)",           Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 3, Name = "LDS 측정 1: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 4, Name = "LDS 측정 2: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 5, Name = "LDS 측정 3: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 6, Name = "LDS 측정 4: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 7, Name = "LDS 측정 5: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 8, Name = "LDS 측정 6: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 9, Name = "LDS 측정 7: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 10, Name = "LDS 측정 8: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 //new ServoPosiInfo() {No = 11, Name = "LDS 측정 9: 평탄도 측정 (Y1)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
                 new ServoPosiInfo() {No = 3, Name = "A 가공 테이블 : Fine Align (Y1)",             Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_A.YAxis},
             };
            if (equip.PROC.IRCutStage_A.YAxis.Setting.Load() == false) errCnt++;

            //IRCutStage_B.YAxis 설정 0
            equip.PROC.IRCutStage_B.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "IRCutStage_BYAxisPosi.xml");
            equip.PROC.IRCutStage_B.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                 new ServoPosiInfo() {No = 0, Name = "B 가공 테이블 : Cell L/D (Y2)",             Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "B 가공 테이블 : Cell UL/D (Y2)",            Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 new ServoPosiInfo() {No = 2, Name = "B 가공 테이블 : 레이저 컷팅 (Y2)",         Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 3, Name = "LDS 측정 1: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 4, Name = "LDS 측정 2: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 5, Name = "LDS 측정 3: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 6, Name = "LDS 측정 4: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 7, Name = "LDS 측정 5: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 8, Name = "LDS 측정 6: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 9, Name = "LDS 측정 7: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 10, Name = "LDS 측정 8: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 //new ServoPosiInfo() {No = 11, Name = "LDS 측정 9: 평탄도 측정 (Y2)",          Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
                 new ServoPosiInfo() {No = 3, Name = "B 가공 테이블 : Fine Align (Y2)",             Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.IRCutStage_B.YAxis},
            };
            if (equip.PROC.IRCutStage_B.YAxis.Setting.Load() == false) errCnt++;

            //FineAlgin.XAxis 설정
            equip.PROC.FineAlign.XAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "FineAlginXAxisPosi.xml");
            equip.PROC.FineAlign.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "A Fine Align 티칭 위치 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlign.XAxis},
                 new ServoPosiInfo() {No = 1, Name = "B Fine Align 티칭 위치 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlign.XAxis},
                 //new ServoPosiInfo() {No = 2, Name = "LDS 측정 1: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 3, Name = "LDS 측정 2: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 4, Name = "LDS 측정 3: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 5, Name = "LDS 측정 4: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 6, Name = "LDS 측정 5: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 7, Name = "LDS 측정 6: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 8, Name = "LDS 측정 7: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 9, Name = "LDS 측정 8: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
                 //new ServoPosiInfo() {No = 10, Name = "LDS 측정 9: LDS 기준 측정위치(X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.FineAlgin.XAxis},
            };
            if (equip.PROC.FineAlign.XAxis.Setting.Load() == false) errCnt++;


            //LaserHead.XAxis 설정
            equip.PROC.LaserHead.XAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LaserHeadXAxisPosi.xml");
            equip.PROC.LaserHead.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[5]
            {
                 new ServoPosiInfo() {No = 0, Name = "A 가공 테이블1: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 1, Name = "A 가공 테이블2: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 2, Name = "B 가공 테이블1: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 3, Name = "B 가공 테이블2: 레이져 샷 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
                 new ServoPosiInfo() {No = 4, Name = "파워미터 측정 위치 (X1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.XAxis},
            };
            if (equip.PROC.LaserHead.XAxis.Setting.Load() == false) errCnt++;

            //LaserHead.ZAxis 설정
            equip.PROC.LaserHead.ZAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "LaserHeadZAxisPosi.xml");
            equip.PROC.LaserHead.ZAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "파워미터 측정 위치 (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.ZAxis},
                 new ServoPosiInfo() {No = 1, Name = "Laser Cutting (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.LaserHead.ZAxis},
            };
            if (equip.PROC.LaserHead.ZAxis.Setting.Load() == false) errCnt++;


            ///////////////////////////////////////////////////////////
            //AfterIRCutTransfer_A.YAxis 설정
            equip.PROC.AfterIRCutTransfer_A.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterIRCutTransfer_AYAxisPosi.xml");
            equip.PROC.AfterIRCutTransfer_A.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "After IR 이재기 겐트리 : Cell L/D(Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.AfterIRCutTransfer_A.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "After IR 이재기 겐트리 : Cell UL/D(Y1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.AfterIRCutTransfer_A.YAxis},
            };
            if (equip.PROC.AfterIRCutTransfer_A.YAxis.Setting.Load() == false) errCnt++;

            //AfterIRCutTransfer_B.YAxis 설정
            equip.PROC.AfterIRCutTransfer_B.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterIRCutTransfer_BYAxisPosi.xml");
            equip.PROC.AfterIRCutTransfer_B.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "After IR 이재기 겐트리 : Cell L/D(Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.AfterIRCutTransfer_B.YAxis},
                 new ServoPosiInfo() {No = 1, Name = "After IR 이재기 겐트리 : Cell UL/D(Y2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.AfterIRCutTransfer_B.YAxis},
            };
            if (equip.PROC.AfterIRCutTransfer_B.YAxis.Setting.Load() == false) errCnt++;



            // BreakStage_A.YAxis 설정
            equip.PROC.BreakStage_A.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_AYAxis.xml");
            equip.PROC.BreakStage_A.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 테이블: Cell L/D (Y1)",         Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.YAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 테이블: Cell UL/D (Y1)",       Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.YAxis},
                     new ServoPosiInfo() {No = 2, Name = "A 테이블: Breaking (Y1)",   Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.YAxis},
                     new ServoPosiInfo() {No = 3, Name = "A Align Cam 기준 위치(Y1)",           Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.YAxis},
            };
            if (equip.PROC.BreakStage_A.YAxis.Setting.Load() == false) errCnt++;

            // BreakStage_B.YAxis 설정
            equip.PROC.BreakStage_B.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_BYAxis.xml");
            equip.PROC.BreakStage_B.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                     new ServoPosiInfo() {No = 0, Name = "B 테이블: Cell L/D(Y2)",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.YAxis},
                     new ServoPosiInfo() {No = 1, Name = "B 테이블: Cell UL/D(Y2)",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.YAxis},
                     new ServoPosiInfo() {No = 2, Name = "B 테이블: Breaking(Y2)",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.YAxis},
                     new ServoPosiInfo() {No = 3, Name = "B Align Cam 기준 위치(Y2)",       Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.YAxis},
            };
            if (equip.PROC.BreakStage_B.YAxis.Setting.Load() == false) errCnt++;



            //// A- SUB 1
            //equip.PROC.BreakStage_A.SubX1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubX1Axis.xml");
            //equip.PROC.BreakStage_A.SubX1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (A-sub X1): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubX1Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (A-sub X1): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubX1Axis},
            //};
            //if (equip.PROC.BreakStage_A.SubX1Axis.Setting.Load() == false) errCnt++;
            //
            //// A- SUB 2
            //equip.PROC.BreakStage_A.SubX2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubX2Axis.xml");
            //equip.PROC.BreakStage_A.SubX2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (A-sub X2): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubX2Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (A-sub X2): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubX2Axis},
            //};
            //if (equip.PROC.BreakStage_A.SubX2Axis.Setting.Load() == false) errCnt++;
            //
            //// B- SUB 1
            //equip.PROC.BreakStage_B.SubX1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubX1Axis.xml");
            //equip.PROC.BreakStage_B.SubX1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (B-sub X1): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubX1Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (B-sub X1): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubX1Axis},
            //};
            //if (equip.PROC.BreakStage_B.SubX1Axis.Setting.Load() == false) errCnt++;
            //
            //// B- SUB 2
            //equip.PROC.BreakStage_B.SubX2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubX2Axis.xml");
            //equip.PROC.BreakStage_B.SubX2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (B-sub X2): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubX2Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (B-sub X2): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubX2Axis},
            //};
            //if (equip.PROC.BreakStage_B.SubX2Axis.Setting.Load() == false) errCnt++;
            //
            //// A- SUB 1  YYYYYYYYYY
            //equip.PROC.BreakStage_A.SubY1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubY1Axis.xml");
            //equip.PROC.BreakStage_A.SubY1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (A-sub Y1): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubY1Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (A-sub Y1): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubY1Axis},
            //};
            //if (equip.PROC.BreakStage_A.SubY1Axis.Setting.Load() == false) errCnt++;
            //
            //// A- SUB 2 YYYYYYYY
            //equip.PROC.BreakStage_A.SubY2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubY2Axis.xml");
            //equip.PROC.BreakStage_A.SubY2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (A-sub Y2): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubY2Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (A-sub Y2): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubY2Axis},
            //};
            //if (equip.PROC.BreakStage_A.SubY2Axis.Setting.Load() == false) errCnt++;
            //
            //// B- SUB 1
            //equip.PROC.BreakStage_B.SubY1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubY1Axis.xml");
            //equip.PROC.BreakStage_B.SubY1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (B-sub Y1): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubY1Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (B-sub Y1): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubY1Axis},
            //};
            //if (equip.PROC.BreakStage_B.SubY1Axis.Setting.Load() == false) errCnt++;
            //
            //// B- SUB 2
            //equip.PROC.BreakStage_B.SubY2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubY2Axis.xml");
            //equip.PROC.BreakStage_B.SubY2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (B-sub Y2): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubY2Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (B-sub Y2): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubY2Axis},
            //};
            //if (equip.PROC.BreakStage_B.SubY2Axis.Setting.Load() == false) errCnt++;
            //
            //// A- SUB 1  ttttttttt
            //equip.PROC.BreakStage_A.SubT1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubT1Axis.xml");
            //equip.PROC.BreakStage_A.SubT1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (A-sub T1): 대기",              Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubT1Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (A-sub T1): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubT1Axis},
            //};
            //if (equip.PROC.BreakStage_A.SubT1Axis.Setting.Load() == false) errCnt++;
            //
            //// A- SUB 2 tttttttt
            //equip.PROC.BreakStage_A.SubT2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_ASubT2Axis.xml");
            //equip.PROC.BreakStage_A.SubT2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (A-sub T2): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubT2Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (A-sub T2): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_A.SubT2Axis},
            //};
            //if (equip.PROC.BreakStage_A.SubT2Axis.Setting.Load() == false) errCnt++;
            //
            //// B- SUB 1
            //equip.PROC.BreakStage_B.SubT1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubT1Axis.xml");
            //equip.PROC.BreakStage_B.SubT1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (B-sub T1): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubT1Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (B-sub T1): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubT1Axis},
            //};
            //if (equip.PROC.BreakStage_B.SubY1Axis.Setting.Load() == false) errCnt++;
            //
            //// B- SUB 2
            //equip.PROC.BreakStage_B.SubT2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakStage_BSubT2Axis.xml");
            //equip.PROC.BreakStage_B.SubT2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            //{
            //         new ServoPosiInfo() {No = 0, Name = "테이블 (B-sub T2): 대기",      Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubT2Axis},
            //         new ServoPosiInfo() {No = 1, Name = "테이블 (B-sub T2): 정밀 위치 보정",    Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakStage_B.SubT2Axis},
            //};
            //if (equip.PROC.BreakStage_B.SubT2Axis.Setting.Load() == false) errCnt++;
            //--------------------------------------------------------------

            #region BreakingHead
            //A
            equip.PROC.BreakingHead.X1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakingHeadX1AxisPosi.xml");
            equip.PROC.BreakingHead.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "X1: 1 Breaking", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "X1: 2 Breaking", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.X1Axis},
            };
            if (equip.PROC.BreakingHead.X1Axis.Setting.Load() == false) errCnt++;

            //B
            equip.PROC.BreakingHead.X2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakingHeadX2AxisPosi.xml");
            equip.PROC.BreakingHead.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                 new ServoPosiInfo() {No = 0, Name = "X2: 1 Breaking", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.X2Axis},
                 new ServoPosiInfo() {No = 1, Name = "X2: 2 Breaking", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.X2Axis},
            };
            if (equip.PROC.BreakingHead.X2Axis.Setting.Load() == false) errCnt++;


            //BreakingHead.Z1Axis 설정
            equip.PROC.BreakingHead.Z1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakingHeadZ1AxisPosi.xml");
            equip.PROC.BreakingHead.Z1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                 new ServoPosiInfo() {No = 0, Name = "Z1: 비젼 확인 위치 (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z1Axis},
                 new ServoPosiInfo() {No = 1, Name = "Z1: 브레이킹 스테이지 위치 1 (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z1Axis},
                 new ServoPosiInfo() {No = 2, Name = "Z1: 브레이킹 스테이지 위치 3 (Z1)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z1Axis},
            };
            if (equip.PROC.BreakingHead.Z1Axis.Setting.Load() == false) errCnt++;

            //BreakingHead.Z2Axis 설정
            equip.PROC.BreakingHead.Z2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakingHeadZ2AxisPosi.xml");
            equip.PROC.BreakingHead.Z2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                 new ServoPosiInfo() {No = 0, Name = "Z2: 비젼 확인 위치 (Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z2Axis},
                 new ServoPosiInfo() {No = 1, Name = "Z2: 브레이킹 스테이지 위치 1 (Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z2Axis},
                 new ServoPosiInfo() {No = 2, Name = "Z2: 브레이킹 스테이지 위치 3 (Z2)", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakingHead.Z2Axis},
            };
            if (equip.PROC.BreakingHead.Z2Axis.Setting.Load() == false) errCnt++;

            //Breaking Align  X1
            equip.PROC.BreakAlign.XAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakAlignXAxisPosi.xml");
            equip.PROC.BreakAlign.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[8]
            {
                 new ServoPosiInfo() {No = 0, Name = "X1: A열 Align 측정 위치", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
                 new ServoPosiInfo() {No = 1, Name = "X1: B열 Align 측정 위치", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
                 new ServoPosiInfo() {No = 2, Name = "X1: A-1 Breaking Table 평탄도 측정", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
                 new ServoPosiInfo() {No = 3, Name = "X1: A-2 Breaking Table 평탄도 측정", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
                 new ServoPosiInfo() {No = 4, Name = "X1: B-1 Breaking Table 평탄도 측정", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
                 new ServoPosiInfo() {No = 5, Name = "X1: B-2 Breaking Table 평탄도 측정", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
                 new ServoPosiInfo() {No = 6, Name = "X1: Jig 측정 위치", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
                 new ServoPosiInfo() {No = 7, Name = "X1: 평탄도 Z축", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.XAxis},
            };
            if (equip.PROC.BreakAlign.XAxis.Setting.Load() == false) errCnt++;

            //Breaking Align  Z1
            equip.PROC.BreakAlign.ZAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BreakAlignZAxisPosi.xml");
            equip.PROC.BreakAlign.ZAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                 new ServoPosiInfo() {No = 0, Name = "Z1: 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.ZAxis},
                 new ServoPosiInfo() {No = 1, Name = "Z1: Breaking 위치", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.ZAxis},
                 new ServoPosiInfo() {No = 2, Name = "Z1: Align Z축", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.ZAxis},
                 new ServoPosiInfo() {No = 3, Name = "Z1: Jig 측정 위치", Position = 0, Speed = 0, Accel = 0, Servo = equip.PROC.BreakAlign.ZAxis},
            };
            if (equip.PROC.BreakAlign.ZAxis.Setting.Load() == false) errCnt++;
            #endregion

            #region unloadertr
            // 2Head
            // BotUnloaderTransfer.Y1Axis 설정
            equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BeforeUnlTrAY1Axis.xml");
            equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y1) : TR1 L/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis},
                     new ServoPosiInfo() {No = 1, Name = "언로딩 이재기 (Y1) : TR1 검사 스테이지", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis},
            };
            if (equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis.Setting.Load() == false) errCnt++;

            // BotUnloaderTransfer.Y2Axis 설정
            equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterUnlTrAY2Axis.xml");
            equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y1) : TR2 검사 스테이지", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_A.Y2Axis},
                     new ServoPosiInfo() {No = 1, Name = "언로딩 이재기 (Y1) : TR2 UL/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_A.Y2Axis},
            };
            if (equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.Setting.Load() == false) errCnt++;

            //INSPSTAGE.YAxis 설정
            equip.UD.InspectionStage_A.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "InspStageAYAxis.xml");
            equip.UD.InspectionStage_A.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "INSP STAGE A(Y1) : Cell L/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_A.YAxis},
                     new ServoPosiInfo() {No = 1, Name = "INSP STAGE A (Y1) : Cell UL/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_A.YAxis},
                     new ServoPosiInfo() {No = 2, Name = "INSP STAGE A (Y1) : Cell INSP", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_A.YAxis},
            };
            if (equip.UD.InspectionStage_A.YAxis.Setting.Load() == false) errCnt++;

            //INSPSTAGE.YAxis 설정
            equip.UD.InspectionStage_B.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "InspStageAYAxis.xml");
            equip.UD.InspectionStage_B.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "INSP STAGE A(Y1) : Cell L/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_B.YAxis},
                     new ServoPosiInfo() {No = 1, Name = "INSP STAGE A (Y1) : Cell UL/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_B.YAxis},
                     new ServoPosiInfo() {No = 2, Name = "INSP STAGE A (Y1) : Cell INSP", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_B.YAxis},
            };
            if (equip.UD.InspectionStage_B.YAxis.Setting.Load() == false) errCnt++;

            // TopUnloaderTransfer.Y1Axis 설정
            equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "BeforeUnlTrBY1Axis.xml");
            equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y2) : TR1 L/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis},
                     new ServoPosiInfo() {No = 1, Name = "언로딩 이재기 (Y2) : TR1 검사 스테이지", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis},
            };
            if (equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis.Setting.Load() == false) errCnt++;

            // TopUnloaderTransfer.Y2Axis 설정
            equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterUnlTrBY2Axis.xml");
            equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (Y2) : TR2 검사 스테이지", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_B.Y2Axis},
                     new ServoPosiInfo() {No = 1, Name = "언로딩 이재기 (Y2) : TR2 UL/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_B.Y2Axis},
            };
            if (equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.Setting.Load() == false) errCnt++;

            // BotUnloaderTransfer.T1Axis 설정
            equip.UD.AfterInspUnloaderTransfer_A.T1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterUnlTrAT1Axis.xml");
            equip.UD.AfterInspUnloaderTransfer_A.T1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (T1) : 피커    0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_A.T1Axis},
            };
            if (equip.UD.AfterInspUnloaderTransfer_A.T1Axis.Setting.Load() == false) errCnt++;

            // BotUnloaderTransfer.T2Axis 설정
            equip.UD.AfterInspUnloaderTransfer_A.T2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterUnlTrAT2Axis.xml");
            equip.UD.AfterInspUnloaderTransfer_A.T2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (T2) : 피커    0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_A.T2Axis},
            };
            if (equip.UD.AfterInspUnloaderTransfer_A.T2Axis.Setting.Load() == false) errCnt++;

            // TopUnloaderTransfer.T1Axis 설정
            equip.UD.AfterInspUnloaderTransfer_B.T1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterUnlTrBT1Axis.xml");
            equip.UD.AfterInspUnloaderTransfer_B.T1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (T3) : 피커    0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_B.T1Axis},
            };
            if (equip.UD.AfterInspUnloaderTransfer_B.T1Axis.Setting.Load() == false) errCnt++;

            // TopUnloaderTransfer.T2Axis 설정
            equip.UD.AfterInspUnloaderTransfer_B.T2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "AfterUnlTrBT2Axis.xml");
            equip.UD.AfterInspUnloaderTransfer_B.T2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[1]
            {
                     new ServoPosiInfo() {No = 0, Name = "언로딩 이재기 (T4) : 피커    0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.AfterInspUnloaderTransfer_B.T2Axis},
            };
            if (equip.UD.AfterInspUnloaderTransfer_B.T2Axis.Setting.Load() == false) errCnt++;
            #endregion

            #region 카메라유닛 MCR 없음 -------------------------------------------
            // InspCamera.XAxis 설정
            equip.UD.InspCamera.XAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "InspCameraXAxisPosi.xml");
            equip.UD.InspCamera.XAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[8]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카메라 이재기 (X1) : 하부 Start", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카메라 이재기 (X1) : 상부 Start", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 2, Name = "B 카메라 이재기 (X1) : 하부 Start", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 3, Name = "B 카메라 이재기 (X1) : 상부 Start", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 4, Name = "A 카메라 이재기 (X1) : 하부 End", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 5, Name = "A 카메라 이재기 (X1) : 상부 End", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 6, Name = "B 카메라 이재기 (X1) : 하부 End", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
                     new ServoPosiInfo() {No = 7, Name = "B 카메라 이재기 (X1) : 상부 End", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.XAxis},
            };
            if (equip.UD.InspCamera.XAxis.Setting.Load() == false) errCnt++;

            // InspCamera.ZAxis 설정
            equip.UD.InspCamera.ZAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "InspCameraZAxisPosi.xml");
            equip.UD.InspCamera.ZAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[4]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카메라 이재기 (Z1) : A1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카메라 이재기 (Z1) : A2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
                     new ServoPosiInfo() {No = 2, Name = "B 카메라 이재기 (Z1) : B1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
                     new ServoPosiInfo() {No = 3, Name = "B 카메라 이재기 (Z1) : B2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspCamera.ZAxis},
            };
            if (equip.UD.InspCamera.ZAxis.Setting.Load() == false) errCnt++;

            // // InspStage.Y1Axis 설정
            // equip.UD.InspectionStage_A.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "InspectionStageAYAxisPosi.xml");
            // equip.UD.InspectionStage_A.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            // {
            //          new ServoPosiInfo() {No = 0, Name = "A 검사 스테이지 (Y1) : Cell L/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_A.YAxis},
            //          new ServoPosiInfo() {No = 1, Name = "A 검사 스테이지 (Y1) : Cell UL/d", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_A.YAxis},
            // };
            // if (equip.UD.InspectionStage_A.YAxis.Setting.Load() == false) errCnt++;
            // 
            // // InspStage.Y1Axis 설정
            // equip.UD.InspectionStage_B.YAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "InspectionStageBYAxisPosi.xml");
            // equip.UD.InspectionStage_B.YAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            // {
            //          new ServoPosiInfo() {No = 0, Name = "B 검사 스테이지 (Y2) : Cell L/D", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_B.YAxis},
            //          new ServoPosiInfo() {No = 1, Name = "B 검사 스테이지 (Y2) : Cell UL/d", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.InspectionStage_B.YAxis},
            // };
            // if (equip.UD.InspectionStage_A.YAxis.Setting.Load() == false) errCnt++;
            //-------------------------------------------------------------------
            #endregion

            #region Unloder
            //Unloader.X1Axis 설정 
            //Unloader.X1Axis 설정 
            equip.UD.Unloader.X1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "UnloaderX1AxisPosi.xml");
            equip.UD.Unloader.X1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[7]
             {
                 new ServoPosiInfo() {No = 0, Name = "투입 이재기 (X1) : A 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 1, Name = "투입 이재기 (X1) : A 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 2, Name = "투입 이재기 (X1) : B 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 3, Name = "투입 이재기 (X1) : B 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 4, Name = "투입 이재기 (X1) : A열",    Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 5, Name = "투입 이재기 (X1) : B열", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
                 new ServoPosiInfo() {No = 6, Name = "투입 이재기 (X1) : 버퍼 1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X1Axis},
             };
            if (equip.UD.Unloader.X1Axis.Setting.Load() == false) errCnt++;

            //Unloader.X2Axis 설정
            equip.UD.Unloader.X2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "UnloaderX2AxisPosi.xml");
            equip.UD.Unloader.X2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[7]
            {
                     new ServoPosiInfo() {No = 0, Name = "투입 이재기 (X2) : A 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 1, Name = "투입 이재기 (X2) : A 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 2, Name = "투입 이재기 (X2) : B 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 3, Name = "투입 이재기 (X2) : B 대기", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 4, Name = "투입 이재기 (X2) : A열",    Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 5, Name = "투입 이재기 (X2) : B열", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
                     new ServoPosiInfo() {No = 6, Name = "투입 이재기 (X2) : 버퍼 2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.X2Axis},
             };
            if (equip.UD.Unloader.X2Axis.Setting.Load() == false) errCnt++;

            //Unloader.Y1Axis 설정 
            equip.UD.Unloader.Y1Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "UnloaderY1AxisPosi.xml");
            equip.UD.Unloader.Y1Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[6]
             {
                 new ServoPosiInfo() {No = 0, Name = "투입 이재기 (Y1) : A 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 1, Name = "투입 이재기 (Y1) : B 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 2, Name = "투입 이재기 (Y1) : Cst 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 3, Name = "투입 이재기 (Y1) : Cell UL/D Start", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 4, Name = "투입 이재기 (Y1) : Cell UL/D End", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
                 new ServoPosiInfo() {No = 5, Name = "투입 이재기 (Y1) : 버퍼 1", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y1Axis},
             };
            if (equip.UD.Unloader.Y1Axis.Setting.Load() == false) errCnt++;

            //Unloader.Y2Axis 설정
            equip.UD.Unloader.Y2Axis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "UnloaderY2AxisPosi.xml");
            equip.UD.Unloader.Y2Axis.Setting.LstServoPosiInfo = new ServoPosiInfo[6]
            {
                 new ServoPosiInfo() {No = 0, Name = "투입 이재기 (Y2) : A 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                 new ServoPosiInfo() {No = 1, Name = "투입 이재기 (Y2) : B 안쪽", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                 new ServoPosiInfo() {No = 2, Name = "투입 이재기 (Y2) : Cst 중간", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                 new ServoPosiInfo() {No = 3, Name = "투입 이재기 (Y2) : Cell UL/D Start", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                 new ServoPosiInfo() {No = 4, Name = "투입 이재기 (Y2) : Cell UL/D End", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
                 new ServoPosiInfo() {No = 5, Name = "투입 이재기 (Y2) : 버퍼 2", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.Unloader.Y2Axis},
             };
            if (equip.UD.Unloader.Y2Axis.Setting.Load() == false) errCnt++;
            #endregion


            #region CstUnloader
            //CstUDRotationAxis 설정 

            equip.UD.CstUnloader_A.CstRotationAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstUnloader_ACstRotationAxisPosi.xml");
            equip.UD.CstUnloader_A.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카세트:  0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_A.CstRotationAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카세트: 90도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_A.CstRotationAxis},
             };
            if (equip.UD.CstUnloader_A.CstRotationAxis.Setting.Load() == false) errCnt++;

            equip.UD.CstUnloader_B.CstRotationAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstUnloader_BCstRotationAxisPosi.xml");
            equip.UD.CstUnloader_B.CstRotationAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[2]
             {
                 new ServoPosiInfo() {No = 0, Name = "B 카세트:  0도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_B.CstRotationAxis},
                 new ServoPosiInfo() {No = 1, Name = "B 카세트: 90도", Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_B.CstRotationAxis},
             };
            if (equip.UD.CstUnloader_B.CstRotationAxis.Setting.Load() == false) errCnt++;

            //CstUDUpDownAxis 설정 

            equip.UD.CstUnloader_A.CstUpDownAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstUnloader_ACstUpDownAxisPosi.xml");
            equip.UD.CstUnloader_A.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
            {
                     new ServoPosiInfo() {No = 0, Name = "A 카세트(Z): Cst LD",  Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_A.CstUpDownAxis},
                     new ServoPosiInfo() {No = 1, Name = "A 카세트(Z): 측정",       Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_A.CstUpDownAxis},
                     new ServoPosiInfo() {No = 2, Name = "A 카세트(Z): Cell 배출",  Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_A.CstUpDownAxis},
             };
            if (equip.UD.CstUnloader_A.CstUpDownAxis.Setting.Load() == false) errCnt++;

            equip.UD.CstUnloader_B.CstUpDownAxis.Setting.XmlPath = Path.Combine(GG.StartupPath, "Setting", "CstUnloader_BCstUpDownAxisPosi.xml");
            equip.UD.CstUnloader_B.CstUpDownAxis.Setting.LstServoPosiInfo = new ServoPosiInfo[3]
             {
                 new ServoPosiInfo() {No = 0, Name = "B 카세트(Z): Cst LD",  Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_B.CstUpDownAxis},
                 new ServoPosiInfo() {No = 1, Name = "B 카세트(Z): 측정",       Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_B.CstUpDownAxis},
                 new ServoPosiInfo() {No = 2, Name = "B 카세트(Z): Cell 배출",  Position = 0, Speed = 0, Accel = 0, Servo = equip.UD.CstUnloader_B.CstUpDownAxis},
             };
            if (equip.UD.CstUnloader_B.CstUpDownAxis.Setting.Load() == false) errCnt++;



            #endregion
            return errCnt <= 0;
        }
    }
}

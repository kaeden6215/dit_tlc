﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{

    public enum EmRecvPio
    {
        UP_000_WAIT,
        UP_010_RECV_START,
        UP_020,
        UP_030,
        UP_040,
        UP_050,
        UP_060,
    }
    public enum EmSendPio
    {
        DW_000_WAIT,
        DW_010_SEND_START,
        DW_020,
        DW_030,
        DW_040,
        DW_050,
    }
    public enum EmUnitPosi
    {
        ACol,
        BCol,
        None
    }
    public enum EmPanelPosi
    {
        Bot,
        Top
    }
    public enum EmProcStatus
    {
        Idle,
        Execute,
        Pause,
    }
    public enum EmUnitType
    {
        CstLoader,
        Loader,
        LoaderTransfer,

        AfterIRCutTransfer,
        LaserCuttingStage,
        BreakStage,
        BeforeInspUnLoaderTransfer,
        InspStage,
        AfterInspUnLoaderTransfer,
        LaserCuttingHead,
        FineAlign,
        BreakAlign,
        BreakHead,
        InspCamera,
    }

    public class BaseUnit
    {
        public EmUnitPosi UnitPosi { get; set; }
        public EmProcStatus ProcStatus { get; set; }
        public string Name { get; set; }
        public int UnitNo { get; set; }
        public VirtualShare ShareMem { get; set; }
        public PlcAddr StartAddr { get { return new PlcAddr(PlcMemType.S, UnitNo * 1000); } }

        public PioRecvStep PioRecv { get; set; }
        public PioSendStep PioSend { get; set; }

        public PanelInfoSet PanelSet { get; set; }
        public bool IsHomeComplete { get; set; }

        //public Dictionary<string, bool> ADicStepSwitch = new Dictionary<string, bool>();
        public Dictionary<string, bool> DicStepSwitch = new Dictionary<string, bool>();
        public bool GetStepSwith(string stepName)
        {
            if (DicStepSwitch.ContainsKey(stepName))
                return DicStepSwitch[stepName];
            else
                return false;
        }
        public void SetStepSwith(string stepName, bool value = false)
        {
            if (stepName.ToUpper().Contains("HOME"))
                value = true;

            if (DicStepSwitch.ContainsKey(stepName) == false)
                DicStepSwitch.Add(stepName, value);
            else
                DicStepSwitch[stepName] = value;
        }

        public bool IsExitsPanel
        {
            get { return PanelSet.Exists1 | PanelSet.Exists2; }
        }
        public bool IsExitsPanel1 { get { return PanelSet.Exists1; } }
        public bool IsExitsPanel2 { get { return PanelSet.Exists2; } }

        public Equipment EquipMain { get; set; }

        public Queue<double> QueTactTime = new Queue<double>(20);
        public List<double> LstTactTime = new List<double>(20);
        public DateTime LastTactWriteTime = PcDateTime.Now;

        public string HomeStepStr { get; set; }
        public string SeqStepStr { get; set; }

        public BaseUnit()
        {
            PanelSet = new PanelInfoSet();
            HomeStepStr = string.Empty;
            SeqStepStr = string.Empty;

            PioRecv = new PioRecvStep();
            PioSend = new PioSendStep();

        }


        public void AddTactTime(double tact)
        {
            LastTactWriteTime = DateTime.Now;
            QueTactTime.Enqueue(tact);
        }

        public void Initizie(BaseUnit sender01, BaseUnit sender02, BaseUnit recver01, BaseUnit recver02, PlcAddr startAddr, IVirtualMem shareMem)
        {
        }

        public virtual void LogicWorking(Equipment equip)
        {
        }

        public PlcAddr PNL_NO = new PlcAddr(PlcMemType.S, 00000, 0, 30);
        public PlcAddr PNL_ID = new PlcAddr(PlcMemType.S, 00030, 0, 30);

        public void WriteCellData(PanelInfo info)
        {
            ShareMem.VirSetAscii(PNL_NO + StartAddr, info.PanelNo);
            ShareMem.VirSetAscii(PNL_ID + StartAddr, info.PanelID);
        }
        public PanelInfo ReadCellData()
        {
            PanelInfo info = new PanelInfo();
            info.PanelNo = ShareMem.VirGetAsciiTrim(PNL_NO + StartAddr);
            info.PanelNo = ShareMem.VirGetAsciiTrim(PNL_ID + StartAddr);
            return info;
        }
    }
}

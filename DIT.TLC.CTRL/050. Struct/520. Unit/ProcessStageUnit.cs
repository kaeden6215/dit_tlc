﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmProcessStageUnitCmd
    {
        S000_WAIT,
        S0100,
        S0200,
        S0300,
        S0400,
        S0500,
        S0600,
        S0700,
        S0800,
        S0900,
        S1000,
        S2000,
        S3000,
        S3100,
        S3200,

    }

    public enum UnitPosiCode
    {
        TT,
        TB,
        BT,
        BB
    }

    public class ProcessStageUnit : BaseUnit
    {

        public UnitPosiCode PosiCode { get; set; }
        public EmProcessStageUnitCmd StepCmd { get; set; }
        
        public ProcessStateXAxisServo YAxis { get; set; }

        public LoaderTransferUnit LoaderTransfer { get; set; }
        public Switch Vaccum1Ch1                    /**/= new Switch();
        public Switch Vaccum1Ch2                    /**/= new Switch();
        public Switch Vaccum2Ch1                    /**/= new Switch();
        public Switch Vaccum2Ch2                    /**/= new Switch();

        public SwitchOneWay Blower1Ch1                         /**/= new SwitchOneWay();
        public SwitchOneWay Blower1Ch2                         /**/= new SwitchOneWay();
        public SwitchOneWay Blower2Ch1                         /**/= new SwitchOneWay();
        public SwitchOneWay Blower2Ch2                         /**/= new SwitchOneWay();

        public Cylinder BrushUpDownCylinder     /**/= new Cylinder();
        public PlcAddr Vaccum1Ch1Value { get; internal set; }
        public PlcAddr Vaccum1Ch2Value { get; internal set; }
        public PlcAddr Vaccum2Ch1Value { get; internal set; }
        public PlcAddr Vaccum2Ch2Value { get; internal set; }


        public PioSendStep PioSend { get; set; }
        public PioRecvStep PioRecv { get; set; }
        

        public ProcessStageUnit()
        {
            StepCmd = EmProcessStageUnitCmd.S000_WAIT;
        }

        public override void LogicWorking(Equipment equip)
        {
            base.LogicWorking(equip);

            YAxis.LogicWorking(equip);

            LoaderCmdLogicWorking(equip);
            LoaderAutoLogicWorking();
        }
        public void LoaderCmdLogicWorking(Equipment equip)
        {
            if (equip.RunMode == EmEquipRunMode.Manual)
                return;

            //if (StepCmd == EmProcessStageUnitCmd.S000_WAIT)
            //{
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S0100)
            //{
            //    if (IsExitsPanel)
            //    { //PNL 있을때. 

            //    }
            //    else
            //    { //PNL 없을때.
            //        StepCmd = EmProcessStageUnitCmd.S0200;
            //    }
            //}
          
            //else if (StepCmd == EmProcessStageUnitCmd.S0400)
            //{
            //    if (this.PioRecv.IsRecvStarted())
            //    {
            //        StepCmd = EmProcessStageUnitCmd.S0500;
            //    }
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S0500)
            //{
                
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S0600)
            //{
            //    if (LoaderTransfer.ZAxis.IsForward == true)
            //    {
            //        Vaccum.OnOff(equip, true);
            //        StepCmd = EmProcessStageUnitCmd.S0700;
            //    }
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S0700)
            //{
            //    if (Vaccum.IsOnOff == true)
            //    {
            //        LoaderTransfer.Vaccum.OnOff(equip, false);
            //        StepCmd = EmProcessStageUnitCmd.S0800;
            //    }
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S0800)
            //{
            //    if (Vaccum.IsOnOff == true || LoaderTransfer.Vaccum.IsOnOff == false)
            //    {
            //        LoaderTransfer.ZAxis.Backward(equip);
            //        StepCmd = EmProcessStageUnitCmd.S0900;
            //    }
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S0900)
            //{
            //    if (LoaderTransfer.ZAxis.IsBackward == true || LoaderTransfer.Vaccum.IsOnOff == false)
            //    {
            //        this.PioRecv.PioComplete();

            //        if (XAxis.MovePosition(equip, ProcessTransferXAxis.ProcessPosiNo) == false) return;

            //        StepCmd = EmProcessStageUnitCmd.S1000;
            //    }
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S1000)
            //{
            //    if (XAxis.IsMoveOnPosition(ProcessTransferXAxis.ProcessPosiNo) == true)
            //    {
            //        //Process 처리.                 
            //        StepCmd = EmProcessStageUnitCmd.S2000;
            //    }
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S2000)
            //{
            //    //Process 완료 대기. 
            //    StepCmd = EmProcessStageUnitCmd.S3000;
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S3000)  //언로딩 위치로 이동. 
            //{
            //    if (XAxis.MovePosition(equip, ProcessTransferXAxis.UnloadingPosiNo) == false) return;
            //    StepCmd = EmProcessStageUnitCmd.S3100;
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S3100)
            //{
            //    if (XAxis.IsMoveOnPosition(ProcessTransferXAxis.UnloadingPosiNo) == true)
            //    {
            //        this.PioSend.StartPioSend(equip, this);
            //        StepCmd = EmProcessStageUnitCmd.S3200;
            //    }
            //}
            //else if (StepCmd == EmProcessStageUnitCmd.S3200)
            //{
            //    if (this.PioSend.IsSendComplete())
            //    {
            //        //PNL 없는지 확인하고, 다음 포지션 확인 후. 다음 동작 처리
            //        if (true)
            //        {
            //            StepCmd = EmProcessStageUnitCmd.S0100;
            //        }
            //    }

            //}
        }
        public void LoaderAutoLogicWorking()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class InspCameraUnit : BaseUnit
    {
        public enum EmInspCameraStep
        {
            S1000_INSP_CAMERA_WAIT,
            S1010_INSP_READY_SEND,
            S1020_INSP_READY_RESP_WAIT,
            S1030_INSP_START_POS_MOVE,
            S1040_INSP_START_POS_MOVE_WAIT,
            S1050_INSP_READY_END,
            S1060_INSP_START_SEND,
            S1070_INSP_START_RESP_WAIT,
            S1080_INSP_END_POS_MOVE_WAIT,
            S1090_INSP_END,
            S1200_INSP_RESULT_START,
            S1200_INSP_RESULT_WAIT,
            S1999_INSP_CAMERA_COMPLETE,
        }
        public enum EmInspCamHomeStep
        {
            H0000,
            H1010,
            H1020,
            H1030,
            H1040,
            H5999_HOME_COMPLETE,
        }

       // public CassetteInfo Cstinfo                                                       /**/   = new CassetteInfo();


        public InspCameraXAxisServo XAxis { get; set; }
        public InspCameraZAxisServo ZAxis { get; set; }
        public EmInspCameraStep SeqStep { get; set; }
        private bool _isInspCameraComplete = false;
        private PanelInfoSet _InspCameraReadPnlSet = null;

        private InspResult _pnl1Result = new InspResult();
        private InspResult _pnl2Result = new InspResult();
        private EmUnitPosi _InspCameraUnitPosi = EmUnitPosi.None;
        private EmPanelPosi _InspPanelPosi = EmPanelPosi.Top;

        private PlcTimerEx plcTmrInspWait = new PlcTimerEx("Insp 응답 대기 타이머");

        public void InitializeInterLock()
        {
            XAxis.CheckStartMoveInterLockFunc = XAxis_CheckStartMoveInterLockFunc;
            ZAxis.CheckStartMoveInterLockFunc = ZAxis_CheckStartMoveInterLockFunc;

            SetStepSwith( "InspCamHomeSeqLogicWorking");
            SetStepSwith( "InspCameraLogicWorking");
        }

        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);
            ZAxis.LogicWorking(equip);


            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;
            if (GG.ExceptCstLoaderUnloader)
            {
                IsHomeComplete = true;
                return;
            }
            else
            {
                SeqLogicWorking(equip);
            }
        }

        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요.         
                IsHomeComplete = false;
                SeqStep = EmInspCameraStep.S1000_INSP_CAMERA_WAIT;
                HomeStep = EmInspCamHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (GetStepSwith( "InspCamHomeSeqLogicWorking")) InspCamHomeSeqLogicWorking(equip);
                return;
            }

            if (equip.IsHomeComplete == false) return;

            if (GetStepSwith( "InspCameraLogicWorking")) InspCameraLogicWorking(equip);
        }

        //HOME 시퀀스 구성. 
        public EmInspCamHomeStep HomeStep = EmInspCamHomeStep.H1010;
        public void InspCamHomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmInspCamHomeStep.H0000)
            {
                this.IsHomeComplete = false;
                HomeStep = EmInspCamHomeStep.H1010;
            }
            else if (HomeStep == EmInspCamHomeStep.H1010)
            {
                ZAxis.GoHome(equip);
                HomeStep = EmInspCamHomeStep.H1020;
            }
            else if (HomeStep == EmInspCamHomeStep.H1020)
            {
                if (ZAxis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmInspCamHomeStep.H1030;
                }
            }
            else if (HomeStep == EmInspCamHomeStep.H1030)
            {
                XAxis.GoHome(equip);
                HomeStep = EmInspCamHomeStep.H1040;
            }
            else if (HomeStep == EmInspCamHomeStep.H1040)
            {
                if (XAxis.IsHomeOnPosition())
                {
                    HomeStep = EmInspCamHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmInspCamHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true; 
            }
        }

        private void InspCameraLogicWorking(Equipment equip)
        {
            if (SeqStep == EmInspCameraStep.S1000_INSP_CAMERA_WAIT)
            {

            }
            else if (SeqStep == EmInspCameraStep.S1010_INSP_READY_SEND)
            {
                if (equip.InspPc.SendInspReady(0, equip.CurrentRecipe.CellType, "LOTID", "CELLID1", "CELLID2", equip.CurrentRecipe.BreakLeft1.X,
                    equip.CurrentRecipe.BreakLeft1.Y, equip.CurrentRecipe.BreakLeftY, equip.CurrentRecipe.BreakLeft2.X, equip.CurrentRecipe.BreakLeft2.Y,
                    equip.CurrentRecipe.BreakLeftY, 1, DateTime.Now.ToString("yyyyMMddHHmmss")) == false)
                {
                    //알람 처리 - 인스펙션 명령 전송 실패 알람 추가 필요.
                    return;
                }
                plcTmrInspWait.Start(10); // 응답 대기 시간 설정 추가 필요.
                SeqStep = EmInspCameraStep.S1020_INSP_READY_RESP_WAIT;
            }
            else if (SeqStep == EmInspCameraStep.S1020_INSP_READY_RESP_WAIT)
            {
                if (!equip.InspPc.IsRecvRespInspReady)
                {
                    if (plcTmrInspWait)
                    {
                        plcTmrInspWait.Stop();
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0752_INSP_RESPONSE_FAULT);
                        return;
                    }
                    return;
                }
                plcTmrInspWait.Stop();
                SeqStep = EmInspCameraStep.S1030_INSP_START_POS_MOVE;
            }
            else if (SeqStep == EmInspCameraStep.S1030_INSP_START_POS_MOVE)
            {
                if (_InspCameraUnitPosi == EmUnitPosi.ACol)
                {
                    if (ZAxis.PtpMoveCmd(equip, EmInspCameraZAxisServo.A_1_InspZ1, equip.CurrentRecipe, null) == false)
                    {
                        //알람 처리 - Inspection Z축 이동 실패 알람 추가 필요.
                        return;
                    }
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_A_Upper_Start : EmInspCameraXAxisServo.Rcp_A_Lower_Start;
                    if (XAxis.PtpMoveCmd(equip, PosiNo, equip.CurrentRecipe, null) == false)
                    {
                        //알람 처리 - Inspection X축 이동 실패 알람 추가 필요.
                        return;
                    }
                    plcTmrInspWait.Start(10); // 타임 아웃 설정값 추가 필요.
                    SeqStep = EmInspCameraStep.S1040_INSP_START_POS_MOVE_WAIT;
                }
                else if (_InspCameraUnitPosi == EmUnitPosi.BCol)
                {
                    if (ZAxis.PtpMoveCmd(equip, EmInspCameraZAxisServo.B_1_InspZ1, equip.CurrentRecipe, null) == false)
                    {
                        //알람 처리 - Inspection Z축 이동 실패 알람 추가 필요.
                        return;
                    }
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_B_Upper_Start : EmInspCameraXAxisServo.Rcp_B_Lower_Start;
                    if (XAxis.PtpMoveCmd(equip, PosiNo, equip.CurrentRecipe, null) == false)
                    {
                        //알람 처리 - Inspection X축 이동 실패 알람 추가 필요.
                        return;
                    }
                    plcTmrInspWait.Start(10); // 타임 아웃 설정값 추가 필요.
                    SeqStep = EmInspCameraStep.S1040_INSP_START_POS_MOVE_WAIT;
                }
                else if (_InspCameraUnitPosi == EmUnitPosi.None)
                {
                    //알람 처리
                    return;
                }
            }
            else if (SeqStep == EmInspCameraStep.S1040_INSP_START_POS_MOVE_WAIT)
            {
                if (_InspCameraUnitPosi == EmUnitPosi.ACol)
                {
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_A_Upper_Start : EmInspCameraXAxisServo.Rcp_A_Lower_Start;
                    if (ZAxis.IsInPosition(EmInspCameraZAxisServo.A_1_InspZ1, equip.CurrentRecipe, null) == true && 
                        XAxis.IsInPosition(PosiNo, equip.CurrentRecipe, null) == true)
                    {
                        plcTmrInspWait.Stop();
                        SeqStep = EmInspCameraStep.S1050_INSP_READY_END;
                    }

                    if (plcTmrInspWait)
                    {
                        plcTmrInspWait.Stop();
                        //알람 처리
                        return;
                    }
                }
                else if (_InspCameraUnitPosi == EmUnitPosi.BCol)
                {
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_B_Upper_Start : EmInspCameraXAxisServo.Rcp_B_Lower_Start;
                    if (ZAxis.IsInPosition(EmInspCameraZAxisServo.B_1_InspZ1, equip.CurrentRecipe, null) == true &&
                        XAxis.IsInPosition(PosiNo, equip.CurrentRecipe, null) == true)
                    {
                        plcTmrInspWait.Stop();
                        SeqStep = EmInspCameraStep.S1050_INSP_READY_END;
                    }

                    if (plcTmrInspWait)
                    {
                        plcTmrInspWait.Stop();
                        //알람 처리
                        return;
                    }
                }
                else if (_InspCameraUnitPosi == EmUnitPosi.None)
                {
                    //알람 처리
                    return;
                }
            }
            else if (SeqStep == EmInspCameraStep.S1050_INSP_READY_END)
            {

            }
            else if (SeqStep == EmInspCameraStep.S1060_INSP_START_SEND)
            {
                if (equip.InspPc.SendGrabStart(0, (int)_InspPanelPosi, (int)_InspCameraUnitPosi) == false)
                {
                    // 알람 처리.
                    return;
                }
                else
                {
                    plcTmrInspWait.Start(10); // 응답 대기 시간 설정 추가 필요.
                    SeqStep = EmInspCameraStep.S1070_INSP_START_RESP_WAIT;
                }
            }
            else if (SeqStep == EmInspCameraStep.S1070_INSP_START_RESP_WAIT)
            {
                if (equip.InspPc.IsRecvRespInspGrabStart == false)
                {
                    if (plcTmrInspWait)
                    {
                        plcTmrInspWait.Stop();
                        // 알람 처리.
                        return;
                    }
                    return;
                }

                plcTmrInspWait.Stop();
                if (_InspCameraUnitPosi == EmUnitPosi.ACol)
                {
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_A_Upper_End : EmInspCameraXAxisServo.Rcp_A_Lower_End;
                    if (XAxis.PtpMoveCmd(equip, PosiNo, equip.CurrentRecipe, null) == false)
                    {
                        //알람 처리
                        return;
                    }
                }
                else if (_InspCameraUnitPosi == EmUnitPosi.BCol)
                {
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_B_Upper_End : EmInspCameraXAxisServo.Rcp_B_Lower_End;
                    if (XAxis.PtpMoveCmd(equip, PosiNo, equip.CurrentRecipe, null) == false)
                    {
                        //알람 처리
                        return;
                    }
                }
                else
                {
                    //알람 처리. 
                    return;
                }
                SeqStep = EmInspCameraStep.S1080_INSP_END_POS_MOVE_WAIT;
            }
            else if (SeqStep == EmInspCameraStep.S1080_INSP_END_POS_MOVE_WAIT)
            {
                if (_InspCameraUnitPosi == EmUnitPosi.ACol)
                {
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_A_Upper_End : EmInspCameraXAxisServo.Rcp_A_Lower_End;
                    if (XAxis.IsInPosition(PosiNo, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmInspCameraStep.S1090_INSP_END;
                    }
                }
                else if (_InspCameraUnitPosi == EmUnitPosi.BCol)
                {
                    EmInspCameraXAxisServo PosiNo = _InspPanelPosi == EmPanelPosi.Top ? EmInspCameraXAxisServo.Rcp_B_Upper_End : EmInspCameraXAxisServo.Rcp_B_Lower_End;
                    if (XAxis.IsInPosition(PosiNo, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmInspCameraStep.S1090_INSP_END;
                    }
                }
                else if (_InspCameraUnitPosi == EmUnitPosi.None)
                {
                    // 알람 처리.
                    return;
                }
            }
            else if (SeqStep == EmInspCameraStep.S1090_INSP_END)
            {
                if (_InspPanelPosi == EmPanelPosi.Top)
                {
                    SeqStep = EmInspCameraStep.S1999_INSP_CAMERA_COMPLETE;
                }
                else if (_InspPanelPosi == EmPanelPosi.Bot)
                {
                    SeqStep = EmInspCameraStep.S1200_INSP_RESULT_START;
                }
            }
            else if (SeqStep == EmInspCameraStep.S1200_INSP_RESULT_START)
            {
                plcTmrInspWait.Start(10); // 응답 대기 시간 설정 추가 필요.
                SeqStep = EmInspCameraStep.S1200_INSP_RESULT_WAIT;
            }
            else if (SeqStep == EmInspCameraStep.S1200_INSP_RESULT_WAIT)
            {
                if (equip.InspPc.IsRecvInspEnd1 == false || equip.InspPc.IsRecvInspEnd2 == false)
                {
                    if (plcTmrInspWait)
                    {
                        plcTmrInspWait.Stop();
                        //알람 처리
                        return;
                    }
                    return;
                }

                plcTmrInspWait.Stop();

                _pnl1Result = equip.InspPc.inspResult1;
                _pnl2Result = equip.InspPc.inspResult2;

                SeqStep = EmInspCameraStep.S1999_INSP_CAMERA_COMPLETE;
            }
            else if (SeqStep == EmInspCameraStep.S1999_INSP_CAMERA_COMPLETE)
            {
                _isInspCameraComplete = true;
            }
        }


        //각 장치별 인터락
        public bool XAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            return false;
        }
        public bool ZAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            return false;
        }

        public bool InspCameraReady(EmUnitPosi unitPosi, EmPanelPosi panelPosi)
        {
            if (GG.ExceptCstLoaderUnloader) return true;

            _InspCameraUnitPosi = unitPosi;
            _InspPanelPosi = panelPosi;
            if (SeqStep == EmInspCameraStep.S1000_INSP_CAMERA_WAIT)
            {
                SeqStep = EmInspCameraStep.S1010_INSP_READY_SEND;
                return true;                
            }                
            else
                return false;
        }
        public bool InspCameraStart(PanelInfoSet pSet, EmUnitPosi unitPosi, EmPanelPosi panelPosi)
        {
            if (GG.ExceptCstLoaderUnloader) return true;

            _InspCameraUnitPosi = unitPosi;
            _InspPanelPosi = panelPosi;
            if (SeqStep == EmInspCameraStep.S1050_INSP_READY_END)
            {
                _isInspCameraComplete = false;
                _InspCameraUnitPosi = unitPosi;
                SeqStep = EmInspCameraStep.S1060_INSP_START_SEND;
                return true;
            }
            else
                return false;
        }
        public bool IsInspCameraComplete(EmUnitPosi unitPosi, PanelInfoSet panelSet, EmPanelPosi panelPosi, out InspResult pnl1Result, out InspResult pnl2Result)
        {
            pnl1Result = new InspResult();
            pnl2Result = new InspResult();

            _InspCameraUnitPosi = unitPosi;
            _InspPanelPosi = panelPosi;
            if (GG.ExceptCstLoaderUnloader) return true;

            if (SeqStep == EmInspCameraStep.S1999_INSP_CAMERA_COMPLETE && _isInspCameraComplete == true)
            {
                pnl1Result = _pnl1Result;
                pnl2Result = _pnl2Result;
                SeqStep = EmInspCameraStep.S1000_INSP_CAMERA_WAIT;
                return true;
            }
            else
            {
                pnl1Result = null;
                pnl2Result = null;
                return false;
            }
        }
        public void ResetInspCamera()
        {
            SeqStep = EmInspCameraStep.S1000_INSP_CAMERA_WAIT;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmInspCameraZAxisServo
    {
        A_1_InspZ1 /**/ = 0,
        A_2_InspZ1 /**/ = 1,
        B_1_InspZ1 /**/ = 2,
        B_2_InspZ1 /**/ = 3
    }

    public class InspCameraZAxisServo : ServoMotorControl
    {
        public InspCameraZAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 15;
            SoftSpeedLimit = 100;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 10;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            return base.GetCalPosition(posiNo, recp, opt);
        }
        public bool IsInPosition(EmInspCameraZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmInspCameraZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmInspCameraZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmInspCameraZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

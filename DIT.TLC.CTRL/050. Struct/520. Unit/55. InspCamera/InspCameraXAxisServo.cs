﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmInspCameraXAxisServo
    {
        A_Lower_Start       /**/= 0,
        A_Upper_Start       /**/= 1,
        B_Lower_Start       /**/= 2,
        B_Upper_Start       /**/= 3,

        A_Lower_End         /**/= 4,
        A_Upper_End         /**/= 5,
        B_Lower_End         /**/= 6,
        B_Upper_End         /**/= 7,

        Rcp_A_Lower_Start   /**/= 8,
        Rcp_A_Upper_Start   /**/= 9,
        Rcp_B_Lower_Start   /**/= 10,
        Rcp_B_Upper_Start   /**/= 11,

        Rcp_A_Lower_End     /**/= 12,
        Rcp_A_Upper_End     /**/= 13,
        Rcp_B_Lower_End     /**/= 14,
        Rcp_B_Upper_End     /**/= 15,
    }
    public class InspCameraXAxisServo : ServoMotorControl
    {
        public InspCameraXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 1050;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmInspCameraXAxisServo.Rcp_A_Lower_Start == posiNo || (int)EmInspCameraXAxisServo.Rcp_A_Upper_Start == posiNo ||
                (int)EmInspCameraXAxisServo.Rcp_B_Lower_Start == posiNo || (int)EmInspCameraXAxisServo.Rcp_B_Upper_Start == posiNo ||
                (int)EmInspCameraXAxisServo.Rcp_A_Lower_End == posiNo || (int)EmInspCameraXAxisServo.Rcp_A_Upper_End == posiNo ||
                (int)EmInspCameraXAxisServo.Rcp_B_Lower_End == posiNo || (int)EmInspCameraXAxisServo.Rcp_B_Upper_End == posiNo)
            {
                PortInfo port = opt as PortInfo;
                if (port == null)
                {
                    InterLockMgr.AddInterLock("Insp_Stage_Y_Rep 이동 명령시, Port 정보가 없습니다. ");
                    return null;
                }

                ServoPosiInfo info = GetCalPosition(EmInspCameraXAxisServo.Rcp_A_Lower_Start, recp, null);
                ServoPosiInfo target = new ServoPosiInfo();
                if ((int)EmInspCameraXAxisServo.Rcp_A_Lower_Start == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP A CELL LEFT BOT X 위치 {0}  ", recp.ALeftBot.X),
                        Position = (float)recp.ALeftBot.X,
                        Speed = (float)recp.AXSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspCameraXAxisServo.Rcp_A_Upper_Start == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP A CELL LEFT TOP X 위치 {0}  ", recp.ALeftTop.X),
                        Position = (float)recp.ALeftTop.Y,
                        Speed = (float)recp.AXSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspCameraXAxisServo.Rcp_B_Lower_Start == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP B CELL LEFT BOT X위치 {0}  ", recp.BLeftBot.X),
                        Position = (float)recp.BLeftBot.X,
                        Speed = (float)recp.BXSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspCameraXAxisServo.Rcp_B_Upper_Start == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP B CELL LEFT TOP X 위치 {0}  ", recp.BLeftTop.X),
                        Position = (float)recp.BLeftTop.X,
                        Speed = (float)recp.BXSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspCameraXAxisServo.Rcp_A_Lower_End == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP A CELL RIGHT BOT X 위치 {0}  ", recp.ARightBot.X),
                        Position = (float)recp.ARightBot.X,
                        Speed = (float)recp.AXSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspCameraXAxisServo.Rcp_A_Upper_End == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP A CELL RIGHT TOP X 위치 {0}  ", recp.ARightTop.X),
                        Position = (float)recp.ARightTop.Y,
                        Speed = (float)recp.AXSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspCameraXAxisServo.Rcp_B_Lower_End == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP B CELL RIGHT BOT X 위치 {0}  ", recp.BRightBot.X),
                        Position = (float)recp.BRightBot.X,
                        Speed = (float)recp.BXSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspCameraXAxisServo.Rcp_B_Upper_End == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP B CELL RIGHT TOP X 위치 {0}  ", recp.BRightTop.X),
                        Position = (float)recp.BRightTop.X,
                        Speed = (float)recp.BXSpeed,
                        Accel = info.Accel,
                    };
                }
                return target;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }
        public bool IsInPosition(EmInspCameraXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmInspCameraXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmInspCameraXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmInspCameraXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class BreakingHeadUnit : BaseUnit
    {
        public enum BreakingHeadHomeStep
        {
            H0000,
            H0010,
            H0020,
            H0030,
            H0040,
            H0050,

            H5999_HOME_COMPLETE
        }

        public enum EmBreakingHeadStep
        {
            S1000_BREAK_HEAD_WAIT,
            S1000_BREAK_HEAD_START,
            S1010_,
            S1020_,
            S1030_,
            S1040_,
            S1050_,
            S1060_,
            S1070_,
            S1005_,
            S1999_BREAK_HEAD_COMPLETE,
            S1080_,
            S1090_,
        }


        public Sensor HeaderX1X2충돌방지                                             /**/   = new Sensor();
        public BreakingHeadXAxisServo X1Axis { get; set; }
        public BreakingHeadXAxisServo X2Axis { get; set; }

        public BreakingHeadZAxisServo Z1Axis { get; set; }
        public BreakingHeadZAxisServo Z2Axis { get; set; }

        public EmBreakingHeadStep SeqStep { get; set; }

        private PanelInfoSet _breakingReadPnlSet = null;
        private EmUnitPosi _breakingUnitPosi = EmUnitPosi.None;

        private bool _isBreakingComplete = false;
        public int BreakingCount { get; set; }

        public BreakingHeadUnit()
        {
            BreakingCount = 0;
        }

        public void InitializeInterLock()
        {
            X1Axis.CheckStartMoveInterLockFunc = X1Axis_CheckStartMoveInterLockFunc;
            X2Axis.CheckStartMoveInterLockFunc = X2Axis_CheckStartMoveInterLockFunc;

            Z1Axis.CheckStartMoveInterLockFunc = Z1Axis_CheckStartMoveInterLockFunc;
            Z2Axis.CheckStartMoveInterLockFunc = Z2Axis_CheckStartMoveInterLockFunc;

            SetStepSwith( "HomeSeqLogicWorking");
            SetStepSwith( "BreakingLogicWorking");
        }
        public override void LogicWorking(Equipment equip)
        {
            HeaderX1X2충돌방지.LogicWorking(equip);
            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
            Z1Axis.LogicWorking(equip);
            Z2Axis.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;
            if (GG.ExceptCstLoaderUnloader)
            {
                IsHomeComplete = true;
                return;
            }
            else
            {
                SeqLogicWorking(equip);
            }
        }

        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                IsHomeComplete = false;

                SeqStep = EmBreakingHeadStep.S1000_BREAK_HEAD_WAIT;
                HomeStep = BreakingHeadHomeStep.H0000;

                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Start)
            { }


            if (this.IsHomeComplete == false)
            {
                if (GetStepSwith( "HomeSeqLogicWorking"))  HomeSeqLogicWorking(equip);
                return;
            }

            if (equip.IsHomeComplete == false) return;

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            if (GetStepSwith( "BreakingLogicWorking")) BreakingLogicWorking(equip);
                        
        }

        //HOME 시퀀스 구성. 
        public BreakingHeadHomeStep HomeStep = BreakingHeadHomeStep.H0000;
        public PlcTimerEx _delayAixsHome = new PlcTimerEx("Cst Loader Home");

        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == BreakingHeadHomeStep.H0000)
            {
                if (this.IsHomeComplete == false)
                {
                    HomeStep = BreakingHeadHomeStep.H0010;
                }
            }
            else if (HomeStep == BreakingHeadHomeStep.H0010)
            {
                if (Z1Axis.GoHome(equip) == true)
                {
                    if (Z2Axis.GoHome(equip) == true)
                    {
                        HomeStep = BreakingHeadHomeStep.H0020;
                    }
                }
            }
            else if (HomeStep == BreakingHeadHomeStep.H0020)
            {
                if (Z1Axis.IsHomeOnPosition() == true && Z2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = BreakingHeadHomeStep.H0030;
                }
            }
            else if (HomeStep == BreakingHeadHomeStep.H0030)
            {
                if (X1Axis.GoHome(equip) == true)
                {
                    if (X2Axis.GoHome(equip) == true)
                    {
                        HomeStep = BreakingHeadHomeStep.H0040;
                    }
                }
            }
            else if (HomeStep == BreakingHeadHomeStep.H0040)
            {
                if (X1Axis.IsHomeOnPosition() == true && X2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = BreakingHeadHomeStep.H0050;
                }
            }
            else if (HomeStep == BreakingHeadHomeStep.H0050)
            {
                HomeStep = BreakingHeadHomeStep.H5999_HOME_COMPLETE;
                IsHomeComplete = true;
            }
        }


        private void BreakingLogicWorking(Equipment equip)
        {
            if (SeqStep == EmBreakingHeadStep.S1000_BREAK_HEAD_WAIT)
            {

            }
            else if (SeqStep == EmBreakingHeadStep.S1000_BREAK_HEAD_START)
            {
                SeqStep = EmBreakingHeadStep.S1005_;
            }
            else if (SeqStep == EmBreakingHeadStep.S1005_)
            {
                if (_breakingUnitPosi == EmUnitPosi.ACol)
                {
                    if(X1Axis.PtpMoveCmdSync(equip, EmBreakingHeadXAxisServo.Breaking_1st, equip.CurrentRecipe, null) == true)
                    SeqStep = EmBreakingHeadStep.S1010_;
                }
                else if (_breakingUnitPosi == EmUnitPosi.BCol)
                {
                    if(X2Axis.PtpMoveCmdSync(equip, EmBreakingHeadXAxisServo.Breaking_1st, equip.CurrentRecipe, null) == true)

                    SeqStep = EmBreakingHeadStep.S1010_;
                }
                else
                {
                    //알람 처리. 
                }

            }
            else if (SeqStep == EmBreakingHeadStep.S1010_)
            {
                if (_breakingUnitPosi == EmUnitPosi.ACol)
                {
                    if(Z1Axis.PtpMoveCmdSync(equip, EmBreakingHeadZAxisServo.BreakDown_Stage1_Stage2, equip.CurrentRecipe, null) == true)
                    SeqStep = EmBreakingHeadStep.S1020_;
                }
                else if (_breakingUnitPosi == EmUnitPosi.BCol)
                {
                    if(Z2Axis.PtpMoveCmdSync(equip, EmBreakingHeadZAxisServo.BreakDown_Stage1_Stage2, equip.CurrentRecipe, null) == true)
                    SeqStep = EmBreakingHeadStep.S1020_;
                }
                else
                {
                    //알람처리
                }
            }
            else if (SeqStep == EmBreakingHeadStep.S1020_)
            {
                //1번 얼라인 마트.
                //equip.AlignPc.SendAlignStart(    )   
                SeqStep = EmBreakingHeadStep.S1030_;

            }
            else if (SeqStep == EmBreakingHeadStep.S1030_)
            {
                //equip.AlignPc.IsAlignComplete( );
                SeqStep = EmBreakingHeadStep.S1040_;
            }
            else if (SeqStep == EmBreakingHeadStep.S1040_)
            {
                if (_breakingUnitPosi == EmUnitPosi.ACol)
                {
                    if(X1Axis.PtpMoveCmdSync(equip, EmBreakingHeadXAxisServo.Breaking_2nd, equip.CurrentRecipe, null) == true)
                    SeqStep = EmBreakingHeadStep.S1050_;
                }
                else if (_breakingUnitPosi == EmUnitPosi.BCol)
                {
                    if(X2Axis.PtpMoveCmdSync(equip, EmBreakingHeadXAxisServo.Breaking_2nd, equip.CurrentRecipe, null) == true)
                    SeqStep = EmBreakingHeadStep.S1050_;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmBreakingHeadStep.S1050_)
            {
                if (_breakingUnitPosi == EmUnitPosi.ACol)
                {
                    if(Z1Axis.PtpMoveCmdSync(equip, EmBreakingHeadZAxisServo.BreakDown_Stage3_Stage4, equip.CurrentRecipe, null) == true)
                    SeqStep = EmBreakingHeadStep.S1060_;
                }
                else if (_breakingUnitPosi == EmUnitPosi.BCol)
                {
                    if(Z2Axis.PtpMoveCmdSync(equip, EmBreakingHeadZAxisServo.BreakDown_Stage3_Stage4, equip.CurrentRecipe, null) == true)
                    SeqStep = EmBreakingHeadStep.S1060_;
                }
                else
                {
                    //알람처리
                }
            }
            else if (SeqStep == EmBreakingHeadStep.S1060_)
            {
                //1번 얼라인 마트.
                //equip.AlignPc.SendAlignStart( )                 
                SeqStep = EmBreakingHeadStep.S1070_;
            }
            else if (SeqStep == EmBreakingHeadStep.S1070_)
            {
                //equip.AlignPc.IsAlignComplete( );
                SeqStep = EmBreakingHeadStep.S1080_;
            }
            else if (SeqStep == EmBreakingHeadStep.S1080_)
            {
                if (true) //결과 확인 
                {
                    SeqStep = EmBreakingHeadStep.S1090_;
                }
                else
                {
                    SeqStep = EmBreakingHeadStep.S1000_BREAK_HEAD_START;
                }
            }
            else if (SeqStep == EmBreakingHeadStep.S1090_)
            {
                //결과 담아주고.
                _isBreakingComplete = true;
                SeqStep = EmBreakingHeadStep.S1999_BREAK_HEAD_COMPLETE;
            }
            else if (SeqStep == EmBreakingHeadStep.S1999_BREAK_HEAD_COMPLETE)
            {

            }
        }
        //각 장치별 인터락
        public bool X1Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //if (equip.PROC.BreakingHead.Z1Axis.IsInPosition(EmBreakingHeadZAxisServo.CalStandbyPos, equip.CurrentRecipe, null) != true)
            //{
            //    InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0}BreakingHead  Z1Axis가 Up 상태가 아닙니다.", UnitPosi));
            //    return true;
            //}

            return false;
        }
        public bool X2Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //if (equip.PROC.BreakingHead.Z2Axis.IsInPosition(EmBreakingHeadZAxisServo.CalStandbyPos, equip.CurrentRecipe, null) != true)
            //{
            //    InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0}BreakingHead  Z1Axis가 Up 상태가 아닙니다.", UnitPosi));
            //    return true;
            //}

            return false;
        }

        public bool Z1Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //스테이지 Y축 추가 필요. 

            //이동 위치가 넘어와야 함.            
            //if (equip.PROC.BreakingHead.X1Axis.IsInPosition(EmBreakingHeadXAxisServo.Breaking_1st, equip.CurrentRecipe, null) != true &&
            //    equip.PROC.BreakingHead.X1Axis.IsInPosition(EmBreakingHeadXAxisServo.Breaking_2nd, equip.CurrentRecipe, null) != true && 
            //    equip.PROC.BreakingHead.X1Axis.IsHomeOnPosition())
            //{
            //    InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  ZAxis 이동 불가, {0}BreakingHead  X1Axis가 Breaking 위치가 아닙니다.", UnitPosi));
            //    return true;
            //}
            return false;
        }
        public bool Z2Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //스테이지 Y축 추가 필요. 

            //if (equip.PROC.BreakingHead.X2Axis.IsInPosition(EmBreakingHeadXAxisServo.Breaking_1st, equip.CurrentRecipe, null) != true &&
            //   equip.PROC.BreakingHead.X2Axis.IsInPosition(EmBreakingHeadXAxisServo.Breaking_2nd, equip.CurrentRecipe, null) != true &&
            //   equip.PROC.BreakingHead.X2Axis.IsHomeOnPosition() != true)
            //{
            //    InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  ZAxis 이동 불가, {0}BreakingHead  X2Axis가 Breaking 위치가 아닙니다.", UnitPosi));
            //    return true;
            //}

            return false;
        }


        public bool BreakingtReady(EmUnitPosi unitPosi)
        {
            if (GG.ExceptCstLoaderUnloader) return true;

            if (SeqStep == EmBreakingHeadStep.S1000_BREAK_HEAD_WAIT)
                return true;
            else
                return false;
        }
        public bool BreakingtStart(PanelInfoSet pSet, EmUnitPosi unitPosi)
        {
            if (GG.ExceptCstLoaderUnloader) return true;
            if (SeqStep == EmBreakingHeadStep.S1000_BREAK_HEAD_WAIT)
            {
                _isBreakingComplete = true;
                _breakingUnitPosi = unitPosi;
                SeqStep = EmBreakingHeadStep.S1000_BREAK_HEAD_START;
                return true;
            }
            else
                return false;
        }
        public bool IsBreakingtComplete(EmUnitPosi unitPosi, PanelInfoSet panelSet)
        {
            if (GG.ExceptCstLoaderUnloader) return true;
            if (SeqStep == EmBreakingHeadStep.S1999_BREAK_HEAD_COMPLETE && _isBreakingComplete == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void ResetLaserCuttingHead()
        {
            SeqStep = EmBreakingHeadStep.S1000_BREAK_HEAD_WAIT;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmBreakUnitSubXAxisServo
    {
        Cal_StandbyPos      /**/= 0,
        Cal_PrecisionPos    /**/= 1,
    }

    public class BreakUnitSubXAxisServo : ServoMotorControl
    {
        public BreakUnitSubXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 16000; // ezi : 1000um / 16000pulse, 1um/16pulse
            SoftMinusLimit = 0;
            SoftPlusLimit = 5;
            SoftSpeedLimit = 10;
            SoftJogSpeedLimit = 10;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 10;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmBreakUnitSubXAxisServo.Cal_StandbyPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            else if ((int)EmBreakUnitSubXAxisServo.Cal_PrecisionPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            else
                return null;
        }
        public bool IsInPosition(EmBreakUnitSubXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmBreakUnitSubXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmBreakUnitSubXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
    }
}
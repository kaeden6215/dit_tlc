﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmBreakStageHomeStep
    {
        H0000,
        H0010,
        H0020,
        H0030,
        H0040,
        H0050,
        H0060,
        H0070,
        H0080,

        H5999_HOME_COMPLETE
    }
    public enum EmBreakStageStep
    {
        S0000_CELL_LOAD_START,  //로딩
        S0010_,
        S0020_,
        S0030_,
        S0040_,
        S0050_,
        S0060_,
        S0070_,
        S0080_,

        S1000_BREAK_ALIGN_STRART,
        S1010_,
        S1020_,
        S1030_,
        S1040_,

        S2000_BREAK_STRART,
        S2010_,
        S2020_,
        S2030_,

        S7000_CELL_UNLOAD_START,  //언로딩      
        S7010_,
        S7020_,
        S7030_,
        S7040_,
        S7050_,
        S7060_,
        S7070_,
        S7080_,
    }

    public class BreakStageUnit : BaseUnit
    {
        public BreakUnitYAxisServo YAxis { get; set; }

        public BreakUnitSubXAxisServo SubX1Axis { get; set; }
        public BreakUnitSubXAxisServo SubX2Axis { get; set; }

        public BreakUnitSubYAxisServo SubY1Axis { get; set; }
        public BreakUnitSubYAxisServo SubY2Axis { get; set; }

        public BreakUnitSubTAxisServo SubT1Axis { get; set; }
        public BreakUnitSubTAxisServo SubT2Axis { get; set; }

        public PlcAddr Vaccum1Value { get; set; }
        public PlcAddr Vaccum2Value { get; set; }


        public Switch Vaccum1                              /**/= new Switch();
        public Switch Vaccum2                              /**/= new Switch();
        public SwitchOneWay Blower1                        /**/ = new SwitchOneWay();
        public SwitchOneWay Blower2                        /**/ = new SwitchOneWay();
        public Cylinder BrushUpDownCylinder                /**/= new Cylinder();
        public EmBreakStageStep StepCmd = EmBreakStageStep.S0000_CELL_LOAD_START;

        public BreakAlignProxy BreakAlign = null;
        public BreakingHeadUnit BreakHead = null;
        public BeforeInspUnLoaderTransferUnit BefInspUnloderTr = null;
        public void InitializeInterLock()
        {
            if (BreakAlign == null)
                BreakAlign = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.BreakAlign) as BreakAlignProxy;

            if (BreakHead == null)
                BreakHead = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.BreakHead) as BreakingHeadUnit;

            if (BefInspUnloderTr == null)
                BefInspUnloderTr = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.BeforeInspUnLoaderTransfer) as BeforeInspUnLoaderTransferUnit;

            Vaccum1.Simulaotr = GG.VacuumSimul;
            Vaccum2.Simulaotr = GG.VacuumSimul;



            YAxis.CheckStartMoveInterLockFunc = YAxis_CheckStartMoveInterLockFunc;

            SubX1Axis.CheckStartMoveInterLockFunc = SubXYTAxis_CheckStartMoveInterLockFunc;
            SubX2Axis.CheckStartMoveInterLockFunc = SubXYTAxis_CheckStartMoveInterLockFunc;

            SubY1Axis.CheckStartMoveInterLockFunc = SubXYTAxis_CheckStartMoveInterLockFunc;
            SubY2Axis.CheckStartMoveInterLockFunc = SubXYTAxis_CheckStartMoveInterLockFunc;

            SubT1Axis.CheckStartMoveInterLockFunc = SubXYTAxis_CheckStartMoveInterLockFunc;
            SubT2Axis.CheckStartMoveInterLockFunc = SubXYTAxis_CheckStartMoveInterLockFunc;


            SetStepSwith( "HomeSeqLogicWorking");
            SetStepSwith( "McrLogicWorking");
            SetStepSwith( "CellLdLogicWorking");
            SetStepSwith( "BreakAlignLogicWorking");
            SetStepSwith( "BreakingnLogicWorking");
            SetStepSwith( "CellUldLogicWorking");
        }
        public override void LogicWorking(Equipment equip)
        {
            YAxis.LogicWorking(equip);
            SubX1Axis.LogicWorking(equip);
            SubX2Axis.LogicWorking(equip);
            SubY1Axis.LogicWorking(equip);
            SubY2Axis.LogicWorking(equip);
            SubT1Axis.LogicWorking(equip);
            SubT2Axis.LogicWorking(equip);

            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            BrushUpDownCylinder.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            SeqLogicWorking(equip);
        }


        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = StepCmd.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                IsHomeComplete = false;
                StepCmd =EmBreakStageStep.S0000_CELL_LOAD_START;
                HomeStep = EmBreakStageHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (equip.GetUnit(UnitPosi, EmUnitType.AfterIRCutTransfer).IsHomeComplete == true &&
                    equip.GetUnit(UnitPosi, EmUnitType.BeforeInspUnLoaderTransfer).IsHomeComplete == true &&
                    equip.PROC.BreakingHead.IsHomeComplete == true)
                {
                    if (GetStepSwith( "HomeSeqLogicWorking"))  HomeSeqLogicWorking(equip);
                }
                return;
            }

            if (equip.IsHomeComplete == false) return; 

            if (GetStepSwith( "CellLdLogicWorking"))         /**/CellLdLogicWorking(equip);
            if (GetStepSwith( "BreakAlignLogicWorking"))     /**/BreakAlignLogicWorking(equip);
            if (GetStepSwith( "BreakingnLogicWorking"))      /**/BreakingnLogicWorking(equip);
            if (GetStepSwith( "CellUldLogicWorking"))        /**/CellUldLogicWorking(equip);
            
        }

        //HOME 시퀀스 구성. 
        public EmBreakStageHomeStep HomeStep = EmBreakStageHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmBreakStageHomeStep.H0000)
            {
                if (IsExitsPanel == true)
                {
                    //공압  On/Off로 
                    if (IsVaccumOn(PanelSet.CellPosi) == true)
                    {
                        HomeStep = EmBreakStageHomeStep.H0010;
                    }
                    else
                    {
                        InterLockMgr.AddInterLock("셀 위치와 버큠이 상이함", "LADER TR의 CELL 위치 {0}", PanelSet.CellPosi);
                    }
                }
                else
                {
                    HomeStep = EmBreakStageHomeStep.H0010;
                }
            }
            else if (HomeStep == EmBreakStageHomeStep.H0010)
            {
                HomeStep = EmBreakStageHomeStep.H0020;
            }
            else if (HomeStep == EmBreakStageHomeStep.H0020)
            {
                if (BrushUpDownCylinder.IsBackward != true)
                {
                    if (BrushUpDownCylinder.Backward(equip) == true)
                    {
                        HomeStep = EmBreakStageHomeStep.H0030;
                    }
                }
                else
                {
                    HomeStep = EmBreakStageHomeStep.H0030;
                }
            }
            else if (HomeStep == EmBreakStageHomeStep.H0030)
            {
                if (BrushUpDownCylinder.IsBackward == true)
                {
                    if (YAxis.GoHome(equip) == true)
                    {
                        HomeStep = EmBreakStageHomeStep.H0040;
                    }
                }
            }
            else if (HomeStep == EmBreakStageHomeStep.H0040)
            {
                if (YAxis.IsHomeOnPosition() == true)
                {
                    //SubY1Axis.GoHome(equip);
                    //SubY2Axis.GoHome(equip);
                    //SubX1Axis.GoHome(equip);
                    //SubX2Axis.GoHome(equip);
                    HomeStep = EmBreakStageHomeStep.H0050;
                }
            }
            else if (HomeStep == EmBreakStageHomeStep.H0050)
            {
                //if (SubY1Axis.IsHomeOnPosition() == true && SubY2Axis.IsHomeOnPosition() == true &&
                //    SubX1Axis.IsHomeOnPosition() == true && SubX2Axis.IsHomeOnPosition() == true)
                {
                    //SubT1Axis.GoHome(equip);
                    //SubT2Axis.GoHome(equip);
                    HomeStep = EmBreakStageHomeStep.H0060;
                }
            }
            else if (HomeStep == EmBreakStageHomeStep.H0060)
            {
                //if (SubT1Axis.IsHomeOnPosition() == true && SubT2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmBreakStageHomeStep.H5999_HOME_COMPLETE; 
                }
            }
            else if (HomeStep == EmBreakStageHomeStep.H5999_HOME_COMPLETE)
            {
                IsHomeComplete = true;
            }
        }

        public void CellLdLogicWorking(Equipment equip)
        {

            //글라스가 있을경우. 넘어감. 추가 필요. 
            if (StepCmd == EmBreakStageStep.S0000_CELL_LOAD_START)
            {
                if (YAxis.PtpMoveCmdSync(equip, EmBreakUnitYAxisServo.Cell_Loading, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    PioRecv.YRecvAble = true;
                    StepCmd = EmBreakStageStep.S0010_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S0010_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;

                    StepCmd = EmBreakStageStep.S0020_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S0020_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true && PioRecv.XSendComplete == true)
                {
                    this.PanelSet = PioRecv.RecvPanelInfoSet;

                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;
                    PioRecv.YRecvComplete = true;
                    StepCmd = EmBreakStageStep.S0030_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S0030_)
            {
                if (PioRecv.XSendAble == false && PioRecv.XSendStart == false && PioRecv.XSendComplete == false)
                {
                    PioRecv.YRecvAble = false;
                    PioRecv.YRecvStart = false;
                    PioRecv.YRecvComplete = false;
                    StepCmd = EmBreakStageStep.S0040_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S0040_)
            {
                StepCmd = EmBreakStageStep.S1000_BREAK_ALIGN_STRART;
            }
        }
        public void BreakAlignLogicWorking(Equipment equip)
        {
            if (StepCmd == EmBreakStageStep.S1000_BREAK_ALIGN_STRART)
            {
                if (YAxis.PtpMoveCmd(equip, EmBreakUnitYAxisServo.BreakAlignCamPos, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    BreakAlign.IsAlignReady(this.UnitPosi);
                    StepCmd = EmBreakStageStep.S1010_;
                }
            }

            else if (StepCmd == EmBreakStageStep.S1010_)
            {
                if (YAxis.IsInPosition(EmBreakUnitYAxisServo.BreakAlignCamPos, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    if (BreakAlign.AlignStart(this.PanelSet, this.UnitPosi) == true)
                        StepCmd = EmBreakStageStep.S1030_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S1030_)
            {
                AlignResult pnl1Result = null;
                AlignResult pnl2Result = null;
                if (BreakAlign.IsBrerakAlignComplete(this.PanelSet, this.UnitPosi, out pnl1Result, out pnl2Result) == true)
                {
                    this.PanelSet.Panel1.BreakAlignResult = pnl1Result;
                    this.PanelSet.Panel2.BreakAlignResult = pnl2Result;

                    equip.PROC.BreakAlign.ResetBreakAlign();
                    StepCmd = EmBreakStageStep.S1040_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S1040_)
            {
                StepCmd = EmBreakStageStep.S2000_BREAK_STRART;
            }
        }
        public void BreakingnLogicWorking(Equipment equip)
        {
            if (StepCmd == EmBreakStageStep.S2000_BREAK_STRART)
            {
                if (YAxis.PtpMoveCmd(equip, EmBreakUnitYAxisServo.Breaking, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    //SubX1Axis.PtpMoveCmd(equip, EmBreakUnitSubXAxisServo.Cal_PrecisionPos, equip.CurrentRecipe, this.PanelSet);
                    //SubX2Axis.PtpMoveCmd(equip, EmBreakUnitSubXAxisServo.Cal_PrecisionPos, equip.CurrentRecipe, this.PanelSet);

                    //SubY1Axis.PtpMoveCmd(equip, EmBreakUnitSubYAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet);
                    //SubY2Axis.PtpMoveCmd(equip, EmBreakUnitSubYAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet);

                    //SubT1Axis.PtpMoveCmd(equip, EmBreakUnitSubTAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet);
                    //SubT2Axis.PtpMoveCmd(equip, EmBreakUnitSubTAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet);

                    BreakHead.BreakingtReady(this.UnitPosi);

                    StepCmd = EmBreakStageStep.S2010_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S2010_)
            {
                if (YAxis.IsInPosition(EmBreakUnitYAxisServo.Breaking, equip.CurrentRecipe, this.PanelSet) == true
                    //&&SubX1Axis.IsInPosition(EmBreakUnitSubXAxisServo.Cal_PrecisionPos, equip.CurrentRecipe, this.PanelSet) == true &&
                    //SubX2Axis.IsInPosition(EmBreakUnitSubXAxisServo.Cal_PrecisionPos, equip.CurrentRecipe, this.PanelSet) == true &&

                    //SubY1Axis.IsInPosition(EmBreakUnitSubYAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet) == true &&
                    //SubY2Axis.IsInPosition(EmBreakUnitSubYAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet) == true &&

                    //SubT1Axis.IsInPosition(EmBreakUnitSubTAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet) == true &&
                    //SubT2Axis.IsInPosition(EmBreakUnitSubTAxisServo.PrecisionPos, equip.CurrentRecipe, this.PanelSet) == true
                    )
                {
                    if (BreakHead.BreakingtStart(this.PanelSet, this.UnitPosi))
                        StepCmd = EmBreakStageStep.S2020_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S2020_)
            {
                if (BreakHead.IsBreakingtComplete(this.UnitPosi, this.PanelSet) == true)
                {
                    equip.PROC.BreakingHead.ResetLaserCuttingHead();
                    StepCmd = EmBreakStageStep.S2030_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S2030_)
            {
                StepCmd = EmBreakStageStep.S7000_CELL_UNLOAD_START;
            }
        }
        public void CellUldLogicWorking(Equipment equip)
        {
            if (StepCmd == EmBreakStageStep.S7000_CELL_UNLOAD_START)
            {
                if (YAxis.PtpMoveCmdSync(equip, EmBreakUnitYAxisServo.Cell_Unloading, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    StepCmd = EmBreakStageStep.S7010_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S7010_)
            {
                PioSend.SendPanelInfoSet = this.PanelSet;
                PioSend.YSendAble = true;
                StepCmd = EmBreakStageStep.S7020_;
            }
            else if (StepCmd == EmBreakStageStep.S7020_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;

                    StepCmd = EmBreakStageStep.S7030_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S7030_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true && PioSend.XRecvComplete == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    PioSend.YSendComplete = true;

                    StepCmd = EmBreakStageStep.S7040_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S7040_)
            {
                if (PioSend.XRecvAble == false && PioSend.XRecvStart == false && PioSend.XRecvComplete == false)
                {
                    this.PanelSet.ClearPanel();

                    PioSend.YSendAble = false;
                    PioSend.YSendStart = false;
                    PioSend.YSendComplete = false;

                    StepCmd = EmBreakStageStep.S7050_;
                }
            }
            else if (StepCmd == EmBreakStageStep.S7050_)
            {
                StepCmd = EmBreakStageStep.S0000_CELL_LOAD_START;
            }
        }

        //각 장치별 인터락
        public bool YAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            AfterIRCutTransferUnit afterIRCutTr = equip.GetUnit(this.UnitPosi, EmUnitType.AfterIRCutTransfer) as AfterIRCutTransferUnit;
            BeforeInspUnLoaderTransferUnit beforeInspUnLoaderTr = equip.GetUnit(this.UnitPosi, EmUnitType.BeforeInspUnLoaderTransfer) as BeforeInspUnLoaderTransferUnit;


            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage YAxis 이동 불가, 배큠1이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage YAxis 이동 불가, 배큠2이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }

            if (afterIRCutTr.UpDown1Cylinder.IsBackward != true && afterIRCutTr.YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null))
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0} AfterIRCutTransferUnit의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (afterIRCutTr.UpDown2Cylinder.IsBackward != true && afterIRCutTr.YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null))
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0} AfterIRCutTransferUnit의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (equip.PROC.BreakingHead.Z1Axis.IsInPosition(EmBreakingHeadZAxisServo.CalStandbyPos, equip.CurrentRecipe, null) != true &&
                equip.PROC.BreakingHead.Z1Axis.IsHomeOnPosition())
            {
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0} BreakingHead  Z1Axis가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (equip.PROC.BreakingHead.Z2Axis.IsInPosition(EmBreakingHeadZAxisServo.CalStandbyPos, equip.CurrentRecipe, null) != true &&
                equip.PROC.BreakingHead.Z2Axis.IsHomeOnPosition())
            {
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0} BreakingHead  Z2Axis가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (beforeInspUnLoaderTr.UpDown1Cylinder.IsBackward != true && beforeInspUnLoaderTr.Y1Axis.IsInPosition(EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0} BeforeInspUnLoaderTr의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (beforeInspUnLoaderTr.UpDown2Cylinder.IsBackward != true && beforeInspUnLoaderTr.Y1Axis.IsInPosition(EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0} BeforeInspUnLoaderTr의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }
        public bool SubXYTAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            AfterIRCutTransferUnit afterIRCutTr = equip.GetUnit(this.UnitPosi, EmUnitType.AfterIRCutTransfer) as AfterIRCutTransferUnit;
            BeforeInspUnLoaderTransferUnit beforeInspUnLoaderTr = equip.GetUnit(this.UnitPosi, EmUnitType.BeforeInspUnLoaderTransfer) as BeforeInspUnLoaderTransferUnit;


            if (afterIRCutTr.UpDown1Cylinder.IsBackward != true && afterIRCutTr.YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  SubX/Y/T 이동 불가, {0} AfterIRCutTransferUnit의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (afterIRCutTr.UpDown2Cylinder.IsBackward != true && afterIRCutTr.YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  SubX/Y/T 이동 불가, {0} AfterIRCutTransferUnit의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (equip.PROC.BreakingHead.Z1Axis.IsInPosition(EmBreakingHeadZAxisServo.CalStandbyPos, equip.CurrentRecipe, null) != true &&
                equip.PROC.BreakingHead.Z1Axis.IsHomeOnPosition())

            {
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  SubX/Y/T 이동 불가, {0} BreakingHead  Z1Axis가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }


            if (equip.PROC.BreakingHead.Z2Axis.IsInPosition(EmBreakingHeadZAxisServo.CalStandbyPos, equip.CurrentRecipe, null) != true &&
                equip.PROC.BreakingHead.Z2Axis.IsHomeOnPosition())
            {
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  SubX/Y/T 이동 불가, {0} BreakingHead  Z2Axis가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (beforeInspUnLoaderTr.UpDown1Cylinder.IsBackward != true && beforeInspUnLoaderTr.Y1Axis.IsInPosition(EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  SubX/Y/T 이동 불가, {0} BeforeInspUnLoaderTr의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (beforeInspUnLoaderTr.UpDown2Cylinder.IsBackward != true && beforeInspUnLoaderTr.Y1Axis.IsInPosition(EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}BreakCutStage  YAxis 이동 불가, {0} BeforeInspUnLoaderTr의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }


        public bool VaccumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vaccum1.OnOff(equip, onOff);
                if (onOff == false) Blower1.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vaccum2.OnOff(equip, onOff);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vaccum1.OnOff(equip, onOff);
                Vaccum2.OnOff(equip, onOff);

                if (onOff == false)
                {
                    Blower1.OnOff(equip, true, 500);
                    Blower2.OnOff(equip, true, 500);
                }
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == false && Vaccum2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmBreakingHeadZAxisServo
    {
        Jig_Align                   /**/= 0,
        BreakDown_Stage1_Stage2     /**/= 1,
        BreakDown_Stage3_Stage4     /**/= 2,

        CalStandbyPos               /**/= 4,
    }

    //Breaking 유닛
    public class BreakingHeadZAxisServo : ServoMotorControl
    {
        public BreakingHeadZAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            SoftMinusLimit = 0;
            SoftPlusLimit = 100;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmBreakingHeadZAxisServo.CalStandbyPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            return base.GetCalPosition(posiNo, recp, opt);
        }
        public bool IsInPosition(EmBreakingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmBreakingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmBreakingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmBreakingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}
















﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmBreakUnitSubTAxisServo
    {
        BreakStage_StandbyPos   /**/= 0,
        PrecisionPos            /**/= 1,
    }
    //Breaking 유닛
    public class BreakUnitSubTAxisServo : ServoMotorControl
    {
        public BreakUnitSubTAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 10000 / 1.27F; // ezi : 1.27도 / 10000pulse
            SoftMinusLimit = 0;
            SoftPlusLimit = 5;
            SoftSpeedLimit = 0;
            SoftJogSpeedLimit = 5;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 10;

        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmBreakUnitSubTAxisServo.BreakStage_StandbyPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            else if ((int)EmBreakUnitSubTAxisServo.PrecisionPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            else
                return null;
             
        }
        public bool IsInPosition(EmBreakUnitSubTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmBreakUnitSubTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmBreakUnitSubTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
    }
}
















﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmBreakUnitSubYAxisServo
    {
        StandbyPos    /**/= 0,
        PrecisionPos  /**/= 1,
    }

    public class BreakUnitSubYAxisServo : ServoMotorControl
    {
        public BreakUnitSubYAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 16000; // ezi : 1mm / 16000pulse, 1um/16pulse
            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30; 
            
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmBreakUnitSubYAxisServo.StandbyPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            else if ((int)EmBreakUnitSubYAxisServo.PrecisionPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            else
                return null;
        }
        public bool IsInPosition(EmBreakUnitSubYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmBreakUnitSubYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmBreakUnitSubYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
    }
}

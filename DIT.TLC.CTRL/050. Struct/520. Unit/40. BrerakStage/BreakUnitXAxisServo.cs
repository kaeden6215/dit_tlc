﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class BreakUnitXAxisServo : ServoMotorControl
    {
        public static int BreakStage_Bot_SubX1_StandbyPos { get; set; }
        public static int BreakStage_Bot_SubX1_PrecisionPos { get; set; }
        public static int BreakStage_Bot_SubX2_StandbyPos { get; set; }
        public static int BreakStage_Bot_SubX2_PrecisionPos { get; set; }

        public static int BreakStage_Top_SubX1_StandbyPos { get; set; }
        public static int BreakStage_Top_SubX1_PrecisionPos { get; set; }
        public static int BreakStage_Top_SubX2_StandbyPos { get; set; }
        public static int BreakStage_Top_SubX2_PrecisionPos { get; set; }

        public static int BreakHead_X1_1Breaking { get; set; }
        public static int BreakHead_X1_2Breaking { get; set; }
        public static int BreakHead_X2_1Breaking { get; set; }
        public static int BreakHead_X2_2Breaking { get; set; }

        public BreakUnitXAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            //A_Vision_Align_X1            = 1;
            //A_Vision_Align_X2            = 2;
            //B_Vision_Align_X3            = 3;
            //B_Vision_Align_X4            = 4;
            //LDS_Break_Measure_X           = 5;


            //base.MoveActionName[0] = "VISION_ALIGN_X1";                     // A Align Cam 기준 위치(X1)
            //base.MoveActionName[1] = "VISION_ALIGN_X2";                     // A Align Cam 기준 위치(X2)
            //base.MoveActionName[2] = "VISION_ALIGN_X3";                     // B Align Cam 기준 위치(X3)
            //base.MoveActionName[3] = "VISION_ALIGN_X4";                     // B Align Cam 기준 위치(X4)
            //base.MoveActionName[4] = "LDS_Break_X";                         // LDS 측정 X : LDS 기준 측정위치(X1)
        }
    }
}
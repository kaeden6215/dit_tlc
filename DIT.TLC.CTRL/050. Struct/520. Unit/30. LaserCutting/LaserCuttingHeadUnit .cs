﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using System.Runtime.InteropServices;   // C# 에서 DLLImport를 사용하려면 추가 해야 함.....
using System.Management;                // C# 에서 DLLImport를 사용하려면 추가 해야 함.....

namespace DIT.TLC.CTRL
{
    public class LaserCuttingHeadUnit : BaseUnit
    {
        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_Initialize();
        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_Deinitialize();


        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_PMAC_Open();
        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_PMAC_Close();
        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_Excom_Open();
        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_Excom_Close();


        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetDisplayWinHandle(int nHandle);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetDXFFilePath(StringBuilder strFilePath);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetMotionHeadBlock(int nTableNo, StringBuilder strFilePath);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetMotionTailBlock(int nTableNo, StringBuilder strFilePath);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetZPositions(double dT1Z1, double dT1Z2, double dT2Z1, double dT2Z2);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetEncoderScales(double dXScale, double dYScale, double dZScale);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetMovingWay(int nMovingWayNo);

        //[DllImport("LaserCuttingModule.dll")]
        //private static extern int SetMotionNo(int nT1No, int T2No);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetFidInfo(int nTableNo, double dLX1, double dLY1, double dLX2, double dLY2, double dRX1, double dRY1, double dRX2, double dRY2, bool nLaserUse);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_MakeMotionFile(int nTableNo);

        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_MakeExcomFile(int nTableNo);


        [DllImport("LaserCuttingModule.dll")]
        private static extern int NANODll_SetStartMotion(int nMotionNo);

        public enum EmLaserCuttingHeadStep
        {
            S1000_LASER_CUTTING_HEAD_WAIT,
            S1000_LASER_CUTTING_HEAD_START,
            S1010_,
            S1011_,
            S1012_,
            S1013_,
            S1014_,
            S1015_,
            S1016_,
            S1017_,
            S1018_,
            S1019_,
            S1020_,
            S1025_,
            S1030_,
            S1040_,
            S1050_,
            S1060_,
            S1070_,
            S1005_,
            S1999_LASER_CUTTING_COMPLETE,
            S1080_,
        }

        public enum EmLaserCuttingHeadHomeStep
        {
            H0000,
            H1010,
            H1020,
            H5999_HOME_COMPLETE,
        }

        public Cylinder UpDown1Cylinder             /**/= new Cylinder();
        public Cylinder UpDown2Cylinder             /**/= new Cylinder();
        public Cylinder LaserShutterCylinder        /**/= new Cylinder();
        public Cylinder DummyShutterCylinder1       /**/= new Cylinder();
        public Cylinder DummyShutterCylinder2       /**/= new Cylinder();

        public Cylinder DummyTankCylinder           /**/= new Cylinder();

        public PlcAddr LaserInterferometerIRValue { get; set; }
        public PlcAddr LaserInterferometerBKValue { get; set; }

        public SwitchOneWay LaserInterferometerReset1 = new SwitchOneWay();
        public SwitchOneWay LaserInterferometerReset2 = new SwitchOneWay();

        public LaserCuttingHeadXAxisServo XAxis { get; set; }
        public LaserCuttingHeadZAxisServo ZAxis { get; set; }

        public EmLaserCuttingHeadStep SeqStep { get; set; }

        private bool _isLaserCuttingComplete = false;
        private PanelInfoSet _cutiingReadPnlSet = null;
        private EmUnitPosi _cutiingUnitPosi = EmUnitPosi.None;

        class MotorPosXY
        {
            public double X;
            public double Y;
        }

        private bool InitLCM()
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_Initialize();
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private void CloseLCM()
        {
            if (GG.TestMode == true) return;

            NANODll_Deinitialize();
        }

        private bool OpenPmacForCut()
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_PMAC_Open();
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool ClosePmacForCut()
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_PMAC_Close();
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool OpenExcomForCut()
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_Excom_Open();
            }
            catch (Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool CloseExcomForCut()
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_Excom_Close();
            }
            catch (Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool SetDisplayWinHandleForCut(IntPtr ptrHandle)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_SetDisplayWinHandle((int)ptrHandle);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool SetDxfFileForCut(string dxfFileName)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                StringBuilder FilePath = new StringBuilder(dxfFileName);
                ReturnValue = NANODll_SetDXFFilePath(FilePath);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool SetMotionHeadBlockForCut(EmUnitPosi unitPosi, string fileName)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                StringBuilder strFilePath = new StringBuilder(fileName);
                ReturnValue = NANODll_SetMotionHeadBlock(unitPosi == EmUnitPosi.ACol ? 0 : 1, strFilePath);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool SetMotionTailBlockForCut(EmUnitPosi unitPosi, string fileName)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                StringBuilder strFilePath = new StringBuilder(fileName);
                ReturnValue = NANODll_SetMotionTailBlock(unitPosi == EmUnitPosi.ACol ? 0 : 1, strFilePath);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool SetZPositionsForCut(double table1Z1, double table1Z2, double table2Z1, double table2Z2)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_SetZPositions(table1Z1, table1Z2, table2Z1, table2Z2);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool SetEncoderScalesForCut(double scaleX, double scaleY, double scaleZ)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_SetEncoderScales(scaleX, scaleY, scaleZ);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        public enum EmCutMovingType
        {
            TypeZ       /**/ = 0, // 첫 번째 셀 Z모양으로 가공 후, 두 번째 셀 Z모양으로 가공
            Typeㅁ      /**/ = 1, // 첫 번째 셀 RT -> LT -> LB -> RB 순서로 가공 후, 두 번째 셀 LB -> RB -> RT -> LT 순서로 가공
            Typeㄷ      /**/ = 2, // 두 번째 셀 RT -> LT -> 첫 번째 셀 RT -> LT 가공 후, 첫 번째 셀 LB -> RB -> 두 번째 셀 LB -> RB 순서로 가공
        }
        private bool SetMovingWayForCut(EmCutMovingType movingType)
        {
            if (GG.TestMode == true) return true;

            int CutMovingType = movingType == EmCutMovingType.TypeZ ? 0 : movingType == EmCutMovingType.Typeㅁ ? 1 : movingType == EmCutMovingType.Typeㄷ ? 2 : 3;
            if (CutMovingType == 3) { return false; }
            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_SetMovingWay(CutMovingType);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool SetFidInfoForCut(EmUnitPosi unitPosi, MotorPosXY leftPos1, MotorPosXY leftPos2, MotorPosXY rightPos1, MotorPosXY rightPos2)
        {
            if (GG.TestMode == true) return true;

            double LX1 = leftPos1.X, LX2 = leftPos2.X, RX1 = rightPos1.X, RX2 = rightPos2.X;
            double LY1 = leftPos1.Y, LY2 = leftPos2.Y, RY1 = rightPos1.Y, RY2 = rightPos2.Y;
            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_SetFidInfo(UnitPosi == EmUnitPosi.ACol ? 0 : 1, LX1, LY1, LX2, LY2, RX1, RY1, RX2, RY2, true);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool MakeMotionFileForCut(EmUnitPosi unitPosi)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_MakeMotionFile(unitPosi == EmUnitPosi.ACol ? 0 : 1);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        private bool MakeExcomFileForCut(EmUnitPosi unitPosi)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            try
            {
                ReturnValue = NANODll_MakeExcomFile(unitPosi == EmUnitPosi.ACol ? 0 : 1);
            }
            catch(Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        public enum EmCutCmd
        {
            None       /**/ = 0,
            Start      /**/ = 1,
            Stop      /**/ = 2, 
        } // JICHA [20181130] - Motion No는 예상만으로 만든 부분.. 확인하여 수정 필요.
        private bool StartCut(EmCutCmd CutCmd)
        {
            if (GG.TestMode == true) return true;

            int ReturnValue = 1;
            int MotionNo = CutCmd == EmCutCmd.Start ? 1 : CutCmd == EmCutCmd.Stop ? 2 : 0;
            try
            {
                ReturnValue = NANODll_SetStartMotion(MotionNo);
            }
            catch (Exception ex)
            {
                ReturnValue = 2;
            }
            return ReturnValue == 0 ? true : false;
        }

        public void InitializeInterLock()
        {
            XAxis.CheckStartMoveInterLockFunc = XAxis_CheckStartMoveInterLockFunc;
            ZAxis.CheckStartMoveInterLockFunc = ZAxis_CheckStartMoveInterLockFunc;
            
            SetStepSwith( "HomeSeqLogicWorking");
            SetStepSwith( "LaserCuttingLogicWorking");

            if (GG.TestMode == false)
            {
                bool Result = InitLCM();
                if (Result == false)
                {
                    // JICHA [20181128] - 지금은 Return 값이 0으로 고정이나 추후 협의에 따라 변경될 수 있음.
                }
            }
        }

        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);
            ZAxis.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;
            /*
            if (GG.ExceptCstLoaderUnloader)
            {
                IsHomeComplete = true;
                return;
            }
            else
            */ // JICHA [20181130] - 시뮬레이션 Test차 임시로 막음.
            {
                SeqLogicWorking(equip);
            }
        }

        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요.         
                IsHomeComplete = false;
                SeqStep = EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_WAIT;
                HomeStep = EmLaserCuttingHeadHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (GetStepSwith( "HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                return;
            }

            if (equip.IsHomeComplete == false) return;

            /*if (GetStepSwith( "LaserCuttingLogicWorking"))*/ LaserCuttingLogicWorking(equip); // JICHA [20181130] - 시뮬레이션 Test차 임시로 막음.
        }

        //HOME 시퀀스 구성. 
        public EmLaserCuttingHeadHomeStep HomeStep = EmLaserCuttingHeadHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmLaserCuttingHeadHomeStep.H0000)
            {
                HomeStep = EmLaserCuttingHeadHomeStep.H1010;
            }
            else if (HomeStep == EmLaserCuttingHeadHomeStep.H1010)
            {
                XAxis.GoHome(equip);
                ZAxis.GoHome(equip);
                HomeStep = EmLaserCuttingHeadHomeStep.H1020;
            }
            else if (HomeStep == EmLaserCuttingHeadHomeStep.H1020)
            {
                if (XAxis.IsHomeOnPosition() && ZAxis.IsHomeOnPosition())
                {
                    HomeStep = EmLaserCuttingHeadHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmLaserCuttingHeadHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true;
                HomeStep = EmLaserCuttingHeadHomeStep.H0000;
            }
        }

        private void LaserCuttingLogicWorking(Equipment equip)
        {
            if (SeqStep == EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_WAIT)
            {

            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_START)
            {
                SeqStep = EmLaserCuttingHeadStep.S1005_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1005_)
            {
                if (_cutiingUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmd(equip, EmLaserCuttingHeadXAxisServo.Laser_A_1Shot, equip.CurrentRecipe, null) == true)
                        SeqStep = EmLaserCuttingHeadStep.S1010_;
                }
                else if (_cutiingUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmd(equip, EmLaserCuttingHeadXAxisServo.Laser_B_1Shot, equip.CurrentRecipe, null) == true)
                        SeqStep = EmLaserCuttingHeadStep.S1010_;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1010_)
            {
                if (ZAxis.PtpMoveCmd(equip, EmLaserCuttingHeadZAxisServo.LaserCutting, equip.CurrentRecipe, null) == true)
                    SeqStep = EmLaserCuttingHeadStep.S1011_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1011_)
            {
                if (SetMotionHeadBlockForCut(_cutiingUnitPosi, equip.CurrentRecipe.DxfPath) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1014_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1012_)
            {
                if (SetMotionTailBlockForCut(_cutiingUnitPosi, equip.CurrentRecipe.DxfPath) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1013_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1013_)
            {
                double Z1 = 0, Z2 = 0, Z3 = 0, Z4 = 0;
                if (SetZPositionsForCut(Z1, Z2, Z3, Z4) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1014_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1014_)
            {
                double scaleX = 0, scaleY = 0, scaleZ = 0;
                if (SetEncoderScalesForCut(scaleX, scaleY, scaleZ) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1015_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1015_)
            {
                if (SetDxfFileForCut(equip.CurrentRecipe.DxfPath) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1016_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1016_)
            {
                if (SetMovingWayForCut(EmCutMovingType.Typeㄷ) == false) // JICHA [20181128] - 가공 순서는 ㄷ자로 결정 된 것이 아니니, 확인되면 반드시 수정 필요.
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1017_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1017_)
            {
                int TableNo = _cutiingUnitPosi == EmUnitPosi.ACol ? 0 : _cutiingUnitPosi == EmUnitPosi.BCol ? 1 : -1;
                if (TableNo == -1) { /*알람 처리*/ return; }

                MotorPosXY Left1 = new MotorPosXY();
                MotorPosXY Left2 = new MotorPosXY();
                MotorPosXY Right1 = new MotorPosXY();
                MotorPosXY Right2 = new MotorPosXY();

                if (_cutiingUnitPosi == EmUnitPosi.ACol)
                {
                    Left1.X = _cutiingReadPnlSet.Panel1.FineAlignResult.X; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.
                    Left1.Y = _cutiingReadPnlSet.Panel1.FineAlignResult.Y; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.
                    Left2.X = _cutiingReadPnlSet.Panel2.FineAlignResult.X; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.
                    Left2.Y = _cutiingReadPnlSet.Panel2.FineAlignResult.Y; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.

                    Right1.X = -9999.0; Right1.Y = -9999.0;
                    Right2.X = -9999.0; Right2.Y = -9999.0;
                }
                else if (_cutiingUnitPosi == EmUnitPosi.BCol)
                {
                    Left1.X = -9999.0; Left1.Y = -9999.0;
                    Left2.X = -9999.0; Left2.Y = -9999.0;

                    Right1.X = _cutiingReadPnlSet.Panel1.FineAlignResult.X; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.
                    Right1.Y = _cutiingReadPnlSet.Panel1.FineAlignResult.Y; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.
                    Right2.X = _cutiingReadPnlSet.Panel2.FineAlignResult.X; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.
                    Right2.Y = _cutiingReadPnlSet.Panel2.FineAlignResult.Y; // JICHA [20181128] - 비젼에서 받을 모터 위치 값으로 수정 필요.
                }
                else if (_cutiingUnitPosi == EmUnitPosi.None)
                {
                    //알람 처리.
                    return;
                }

                if (SetFidInfoForCut(this.UnitPosi, Left1, Left2, Right1, Right2) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1018_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1018_)
            {
                if (MakeMotionFileForCut(this.UnitPosi) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1019_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1019_)
            {
                if (MakeExcomFileForCut(this.UnitPosi) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1020_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1020_)
            {
                if (_cutiingUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.IsInPosition(EmLaserCuttingHeadXAxisServo.Laser_A_1Shot, equip.CurrentRecipe, null) == true &&
                        ZAxis.IsInPosition(EmLaserCuttingHeadZAxisServo.LaserCutting, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmLaserCuttingHeadStep.S1025_;
                    }
                }
                else if (_cutiingUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.IsInPosition(EmLaserCuttingHeadXAxisServo.Laser_B_1Shot, equip.CurrentRecipe, null) == true &&
                        ZAxis.IsInPosition(EmLaserCuttingHeadZAxisServo.LaserCutting, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmLaserCuttingHeadStep.S1025_;
                    }
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1025_)
            {
                // laser Start
                if (StartCut(EmCutCmd.Start) == false)
                {
                    //알람 처리
                    return;
                }
                SeqStep = EmLaserCuttingHeadStep.S1030_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1030_)
            {
                // laser Complete - 여기서 Laser Cut 끝났는지 확인하면 될 듯.
                if (_cutiingUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.IsMoving == false && equip.PROC.IRCutStage_A.YAxis.IsMoving == false) // JICHA [20181130] - 임시로 이렇게 Cutting이 끝났는지 확인.
                    {
                        SeqStep = EmLaserCuttingHeadStep.S1080_;
                    }
                }
                else if (_cutiingUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.IsMoving == false && equip.PROC.IRCutStage_B.YAxis.IsMoving == false) // JICHA [20181130] - 임시로 이렇게 Cutting이 끝났는지 확인.
                    {
                        SeqStep = EmLaserCuttingHeadStep.S1080_;
                    }
                }
                else
                {
                    //알람 처리.
                    return;
                }
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1040_)
            {
                if (_cutiingUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmLaserCuttingHeadXAxisServo.Laser_A_2Shot, equip.CurrentRecipe, null) == true)
                        SeqStep = EmLaserCuttingHeadStep.S1050_;
                }
                else if (_cutiingUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmLaserCuttingHeadXAxisServo.Laser_B_2Shot, equip.CurrentRecipe, null) == true)
                        SeqStep = EmLaserCuttingHeadStep.S1050_;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1050_)
            {
                //1번 얼라인 마트.
                //equip.AlignPc.SendAlignStart(    )                 
                SeqStep = EmLaserCuttingHeadStep.S1060_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1060_)
            {
                //equip.AlignPc.IsAlignComplete( );
                SeqStep = EmLaserCuttingHeadStep.S1070_;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1070_)
            {
                if (true) //결과 확인 
                {
                    SeqStep = EmLaserCuttingHeadStep.S1080_;
                }
                else
                {
                    SeqStep = EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_START;
                }
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1080_)
            {
                //결과 담아주고.

                _isLaserCuttingComplete = true;
                SeqStep = EmLaserCuttingHeadStep.S1999_LASER_CUTTING_COMPLETE;
            }
            else if (SeqStep == EmLaserCuttingHeadStep.S1999_LASER_CUTTING_COMPLETE)
            {

            }
        }

        //각 장치별 인터락
        public bool XAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //if (equip.PROC.LaserHead.ZAxis.IsInPosition(EmLaserCuttingHeadZAxisServo.Cal_StandbyPos, equip.CurrentRecipe, null) != true)
            //{
            //    //인터락 처리.
            //    InterLockMgr.AddInterLock(string.Format("{0}LaserHead   Z축 이동 불가, 레이저헤드 Z축이 대기 위치가 아닙니다.", UnitPosi));
            //    return true;
            //}
            return false;
        }
        public bool ZAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            return false;
        }

        public bool LaserCutReady(EmUnitPosi unitPosi)
        {
            if (SeqStep == EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_WAIT)
                return true;
            else
                return false;
        }

        public bool LaserCutStart(PanelInfoSet pSet, EmUnitPosi unitPosi)
        {
            if (SeqStep == EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_WAIT)
            {
                _isLaserCuttingComplete = false;
                _cutiingReadPnlSet = pSet;
                _cutiingUnitPosi = unitPosi;
                SeqStep = EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_START;
                return true;
            }
            else
                return false;
        }

        public bool IsLaserCutComplete(EmUnitPosi unitPosi, PanelInfoSet panelSet)
        {
            if (SeqStep == EmLaserCuttingHeadStep.S1999_LASER_CUTTING_COMPLETE && _isLaserCuttingComplete == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ResetLaserCuttingHead()
        {
            SeqStep = EmLaserCuttingHeadStep.S1000_LASER_CUTTING_HEAD_WAIT;
        }
    }
}

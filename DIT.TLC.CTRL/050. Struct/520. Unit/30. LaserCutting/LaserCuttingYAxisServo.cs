﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class LaserCuttingYAxisServo : ServoMotorControl
    {
        public static int IRCutStage_A_Y1_CellLD { get; set; }
        public static int IRCutStage_A_Y1_CellULD { get; set; }
        public static int IRCutStage_A_Y1_LaserCutting { get; set; }
        public static int IRCutStage_A_LDS_Flatness1 { get; set; }
        public static int IRCutStage_A_LDS_Flatness2 { get; set; }
        public static int IRCutStage_A_LDS_Flatness3 { get; set; }
        public static int IRCutStage_A_LDS_Flatness4 { get; set; }
        public static int IRCutStage_A_LDS_Flatness5 { get; set; }
        public static int IRCutStage_A_LDS_Flatness6 { get; set; }
        public static int IRCutStage_A_LDS_Flatness7 { get; set; }
        public static int IRCutStage_A_LDS_Flatness8 { get; set; }
        public static int IRCutStage_A_LDS_Flatness9 { get; set; }
        public static int IRCutStage_A_FineAlignTeaching { get; set; }

        public static int IRCutStage_B_Y2_CellLD { get; set; }
        public static int IRCutStage_B_Y2_CellULD { get; set; }
        public static int IRCutStage_B_Y2_LaserCutting { get; set; }
        public static int IRCutStage_B_LDS_Flatness1 { get; set; }
        public static int IRCutStage_B_LDS_Flatness2 { get; set; }
        public static int IRCutStage_B_LDS_Flatness3 { get; set; }
        public static int IRCutStage_B_LDS_Flatness4 { get; set; }
        public static int IRCutStage_B_LDS_Flatness5 { get; set; }
        public static int IRCutStage_B_LDS_Flatness6 { get; set; }
        public static int IRCutStage_B_LDS_Flatness7 { get; set; }
        public static int IRCutStage_B_LDS_Flatness8 { get; set; }
        public static int IRCutStage_B_LDS_Flatness9 { get; set; }
        public static int IRCutStage_B_FineAlignTeaching { get; set; }

        public LaserCuttingYAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            //ULD_A_UnLodering_Y1                 = 1;
            //ULD_A_UnLodering_Y2                 = 2;
            //ULD_A_Cst_Inner_Y1                  = 3;
            //ULD_A_Cst_Inner_Y2                  = 4;
            //ULD_B_UnLodering_Y1                 = 5;
            //ULD_B_UnLodering_Y2                 = 6;
            //ULD_B_Cst_Inner_Y1                  = 7;
            //ULD_B_Cst_Inner_Y2                  = 8;
            //ULD_Cst_Mid_Y1                      = 9;
            //ULD_Cst_Mid_Y2                      = 10;
            //ULD_Buffer_Y1                       = 11;
            //ULD_Buffer_Y2                       = 12;


            //base.MoveActionName[0]  = "Y1_TR_L";                        // 언로더(Y1) : A 언로딩 이재기
            //base.MoveActionName[1]  = "Y2_TR_L";                        // 언로더(Y2) : A 언로딩 이재기
            //base.MoveActionName[2]  = "Y1_CST_L";                       // 언로더(Y1) : A 카세트 안쪽
            //base.MoveActionName[3]  = "Y2_CST_L";                       // 언로더(Y2) : A 카세트 안쪽  
            //base.MoveActionName[4]  = "Y1_TR_R";                        // 언로더(Y1) : B 언로딩 이재기
            //base.MoveActionName[5]  = "Y2_TR_R ";                       // 언로더(Y2) : B 언로딩 이재기
            //base.MoveActionName[6]  = "Y1_CST_R";                       // 언로더(Y1) : B 카세트 안쪽  
            //base.MoveActionName[7]  = "Y2_CST_R";                       // 언로더(Y2) : B 카세트 안쪽  
            //base.MoveActionName[8]  = "Y1_CST_Mid";                     // 언로더(Y1) : 카세트 중간    
            //base.MoveActionName[9]  = "Y2_CST_Mid";                     // 언로더(Y2) : 카세트 중간    
            //base.MoveActionName[10] = "Y1_BUFFER";                      // 언로더(Y1) : 버퍼          
            //base.MoveActionName[11] = "Y2_BUFFER";                      // 언로더(Y2) : 버퍼


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmLaserCuttingHeadXAxisServo
    {
        Laser_A_1Shot       /**/= 0,
        Laser_A_2Shot       /**/= 1,
        Laser_B_1Shot       /**/= 2,
        Laser_B_2Shot       /**/= 3,
        PowerMeterMeasure   /**/= 4,

        Cal_StandbyPos      /**/= 5,
    }

    public class LaserCuttingHeadXAxisServo : ServoMotorControl
    {
        public LaserCuttingHeadXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 1150;
            SoftSpeedLimit = 1200;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            return base.GetCalPosition(posiNo, recp, opt);
        }
        public bool IsInPosition(EmLaserCuttingHeadXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmLaserCuttingHeadXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmLaserCuttingHeadXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmLaserCuttingHeadXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

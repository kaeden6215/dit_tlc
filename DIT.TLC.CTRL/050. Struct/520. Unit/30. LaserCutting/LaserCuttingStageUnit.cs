﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmProcessStageHomeStep
    {
        H0000,
        H0010,
        H0020,
        H0030,
        H0040,
        H0050,

        H5999_HOME_COMPLETE
    }

    public enum EmProcessStageUnitCmd
    {
        S000_CELL_LOAD_STRAT,
        S0010_,
        S0020_,
        S1030_,

        S03010_CELL_UNLOAD_START,
        S01020_,
        S01030_,
        S02010_,
        S03040_,
        S03010_,
        S03020_,
        S03030_,
        S1000_FINE_ALIGN_START,
        S02030_,
        S03050_,
        S03060_,
        S0030_,
        S0040_,
        S01040_,
        S02010_CELL_IR_CUT_START,
        S02020_,
    }

    public enum UnitPosiCode
    {
        TT,
        TB,
        BT,
        BB
    }

    public class LaserCuttingStageUnit : BaseUnit
    {
        public UnitPosiCode PosiCode { get; set; }
        public EmProcessStageUnitCmd StepCmd { get; set; }

        public LaserCuttingStageYAxisServo YAxis { get; set; }

        public Switch Vacuum1Ch1                                /**/= new Switch();
        public Switch Vacuum1Ch2                                /**/= new Switch();
        public Switch Vacuum2Ch1                                /**/= new Switch();
        public Switch Vacuum2Ch2                                /**/= new Switch();

        public SwitchOneWay Blower1Ch1                          /**/= new SwitchOneWay();
        public SwitchOneWay Blower1Ch2                          /**/= new SwitchOneWay();
        public SwitchOneWay Blower2Ch1                          /**/= new SwitchOneWay();
        public SwitchOneWay Blower2Ch2                          /**/= new SwitchOneWay();

        public Cylinder BrushUpDownCylinder                     /**/= new Cylinder();
        public PlcAddr Vacuum1Ch1Value { get; internal set; }
        public PlcAddr Vacuum1Ch2Value { get; internal set; }
        public PlcAddr Vacuum2Ch1Value { get; internal set; }
        public PlcAddr Vacuum2Ch2Value { get; internal set; }

        private LaserCuttingHeadUnit LaserCuttingHead = null;
        private FineAlginProxy FineAlign = null;
        public LaserCuttingStageUnit()
        {
            //StepCmd = EmProcessStageUnitCmd.S000_WAIT;
        }
        public void InitializeInterLock()
        {
            if (LaserCuttingHead == null) LaserCuttingHead = GG.Equip.GetUnit(EmUnitPosi.None, EmUnitType.LaserCuttingHead) as LaserCuttingHeadUnit;
            if (FineAlign == null) FineAlign = GG.Equip.GetUnit(EmUnitPosi.None, EmUnitType.FineAlign) as FineAlginProxy;

            Vacuum1Ch1.Simulaotr = GG.VacuumSimul;
            Vacuum1Ch2.Simulaotr = GG.VacuumSimul;
            Vacuum2Ch1.Simulaotr = GG.VacuumSimul;
            Vacuum2Ch2.Simulaotr = GG.VacuumSimul;


            YAxis.CheckStartMoveInterLockFunc = YAxis_CheckStartMoveInterLockFunc;


            SetStepSwith("HomeSeqLogicWorking");
            SetStepSwith("CellLdLogicWorking");
            SetStepSwith("CellFinAlingLogicWorking");
            SetStepSwith("CellLaserCuttingLogicWorking");
            SetStepSwith("CellUldLogicWorking");
        }

        public override void LogicWorking(Equipment equip)
        {
            base.LogicWorking(equip);

            YAxis.LogicWorking(equip);
            Vacuum1Ch1.LogicWorking(equip);
            Vacuum1Ch2.LogicWorking(equip);
            Vacuum2Ch1.LogicWorking(equip);
            Vacuum2Ch2.LogicWorking(equip);
            Blower1Ch1.LogicWorking(equip);
            Blower1Ch2.LogicWorking(equip);
            Blower2Ch1.LogicWorking(equip);
            Blower2Ch2.LogicWorking(equip);
            BrushUpDownCylinder.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            LoaderSeqLogicWorking(equip);
        }

        public void LoaderSeqLogicWorking(Equipment equip)
        {
            SeqStepStr = StepCmd.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                IsHomeComplete = false;
                StepCmd = EmProcessStageUnitCmd.S000_CELL_LOAD_STRAT;
                HomeStep = EmProcessStageHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {

                if (equip.GetUnit(UnitPosi, EmUnitType.LoaderTransfer).IsHomeComplete == true && equip.GetUnit(UnitPosi, EmUnitType.AfterIRCutTransfer).IsHomeComplete == true)
                {
                    if (GetStepSwith("HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                }
                return;
            }

            if (equip.IsHomeComplete == false) return;

            if (GetStepSwith("CellLdLogicWorking"))             /**/CellLdLogicWorking(equip);
            if (GetStepSwith("CellFinAlingLogicWorking"))       /**/CellFineAlignLogicWorking(equip);
            if (GetStepSwith("CellLaserCuttingLogicWorking"))   /**/CellLaserCuttingLogicWorking(equip);
            if (GetStepSwith("CellUldLogicWorking"))            /**/CellUldLogicWorking(equip);
        }


        //HOME 시퀀스 구성. 
        public EmProcessStageHomeStep HomeStep = EmProcessStageHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmProcessStageHomeStep.H0000)
            {
                if (IsExitsPanel == true)
                {
                    //공압  On/Off로 
                    if (IsVaccumOn(PanelSet.CellPosi) == true)
                    {
                        HomeStep = EmProcessStageHomeStep.H0010;
                    }
                    else
                    {
                        InterLockMgr.AddInterLock("셀 위치와 버큠이 상이함", "LADER TR의 CELL 위치 {0}", PanelSet.CellPosi);
                    }
                }
                else
                {
                    HomeStep = EmProcessStageHomeStep.H0010;
                }
            }
            else if (HomeStep == EmProcessStageHomeStep.H0010)
            {
                if (YAxis.GoHome(equip) == true)
                {
                    HomeStep = EmProcessStageHomeStep.H0020;
                }
            }
            else if (HomeStep == EmProcessStageHomeStep.H0020)
            {
                if (YAxis.IsHomeOnPosition() == true)
                {
                    if (YAxis.PtpMoveCmd(equip, EmLaserCuttingStageYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
                    {
                        HomeStep = EmProcessStageHomeStep.H0030;
                    }
                }
            }
            else if (HomeStep == EmProcessStageHomeStep.H0030)
            {
                if (YAxis.IsInPosition(EmLaserCuttingStageYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
                {
                    HomeStep = EmProcessStageHomeStep.H5999_HOME_COMPLETE;
                    IsHomeComplete = true;
                }
            }
            else if (HomeStep == EmProcessStageHomeStep.H5999_HOME_COMPLETE)
            {
                IsHomeComplete = true;
            }
        }

        public bool FirstExecute = true;
        public bool ExecuteEnd = false;
        public void CellLdLogicWorking(Equipment equip)
        {
            
            //글라스가 있을경우. 넘어감. 추가 필요. 


            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // JICHA [20181130] - Laser Cutting을 위한 임시 작업.
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            if (VacuumOnOff(equip, true, EmCellPosi.One_1) && VacuumOnOff(equip, true, EmCellPosi.One_2))
            {
                if (IsVaccumOn(EmCellPosi.One_1) && IsVaccumOn(EmCellPosi.One_2))
                {
                    if (FirstExecute)
                    {
                        FirstExecute = false;
                        StepCmd = EmProcessStageUnitCmd.S1000_FINE_ALIGN_START;
                    }
                }
            }
            return;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            // JICHA [20181130] - Laser Cutting을 위한 임시 작업 여기까지.
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            if (StepCmd == EmProcessStageUnitCmd.S000_CELL_LOAD_STRAT)
            {
                if (YAxis.PtpMoveCmdSync(equip, EmLaserCuttingStageYAxisServo.Cell_Loading, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    PioRecv.YRecvAble = true;
                    StepCmd = EmProcessStageUnitCmd.S0010_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S0010_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;

                    StepCmd = EmProcessStageUnitCmd.S0020_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S0020_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true && PioRecv.XSendComplete == true)
                {
                    this.PanelSet = PioRecv.RecvPanelInfoSet;

                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;
                    PioRecv.YRecvComplete = true;
                    StepCmd = EmProcessStageUnitCmd.S0030_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S0030_)
            {
                if (PioRecv.XSendAble == false && PioRecv.XSendStart == false && PioRecv.XSendComplete == false)
                {
                    PioRecv.YRecvAble = false;
                    PioRecv.YRecvStart = false;
                    PioRecv.YRecvComplete = false;
                    StepCmd = EmProcessStageUnitCmd.S0040_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S0040_)
            {
                StepCmd = EmProcessStageUnitCmd.S1000_FINE_ALIGN_START;

            }
        }
        public void CellFineAlignLogicWorking(Equipment equip)
        {
            if (StepCmd == EmProcessStageUnitCmd.S1000_FINE_ALIGN_START)
            {
                if (YAxis.PtpMoveCmd(equip, EmLaserCuttingStageYAxisServo.Fine_Align, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    FineAlign.MoveAlignXAxis(this.PanelSet, this.UnitPosi);
                    StepCmd = EmProcessStageUnitCmd.S01020_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S01020_)
            {
                if (YAxis.IsInPosition(EmLaserCuttingStageYAxisServo.Fine_Align, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    if (FineAlign.IsAlignReady(this.UnitPosi) == true)
                        if (FineAlign.StartAlign(this.PanelSet, this.UnitPosi) == true)
                            StepCmd = EmProcessStageUnitCmd.S01030_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S01030_)
            {
                AlignResult pnl1Result = null;
                AlignResult pnl2Result = null;
                if (FineAlign.IsAlignComplete(this.PanelSet, this.UnitPosi, out pnl1Result, out pnl2Result) == true)
                {
                    this.PanelSet.Panel1.FineAlignResult = pnl1Result;
                    this.PanelSet.Panel2.FineAlignResult = pnl2Result;

                    // 파인얼라인 Check.
                    equip.PROC.FineAlign.ResetFineAlign();
                    StepCmd = EmProcessStageUnitCmd.S01040_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S01040_)
            {
                StepCmd = EmProcessStageUnitCmd.S02010_CELL_IR_CUT_START;
            }
        }
        public void CellLaserCuttingLogicWorking(Equipment equip)
        {
            if (StepCmd == EmProcessStageUnitCmd.S02010_CELL_IR_CUT_START)
            {
                if (YAxis.PtpMoveCmd(equip, EmLaserCuttingStageYAxisServo.Laser_Cutting, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    LaserCuttingHead.LaserCutReady(this.UnitPosi);
                    StepCmd = EmProcessStageUnitCmd.S02010_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S02010_)
            {
                if (YAxis.IsInPosition(EmLaserCuttingStageYAxisServo.Laser_Cutting, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    LaserCuttingHead.LaserCutStart(this.PanelSet, this.UnitPosi);
                    StepCmd = EmProcessStageUnitCmd.S02020_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S02020_)
            {
                if (LaserCuttingHead.IsLaserCutComplete(this.UnitPosi, this.PanelSet) == true)
                {
                    equip.PROC.LaserHead.ResetLaserCuttingHead();
                    StepCmd = EmProcessStageUnitCmd.S02030_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S02030_)
            {
                StepCmd = EmProcessStageUnitCmd.S03010_CELL_UNLOAD_START;
            }
        }
        public void CellUldLogicWorking(Equipment equip)
        {
            if (StepCmd == EmProcessStageUnitCmd.S03010_CELL_UNLOAD_START)
            {
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                // JICHA [20181130] - Laser Cutting을 위한 임시 작업.
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                if (YAxis.PtpMoveCmdSync(equip, EmLaserCuttingStageYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true && ExecuteEnd == false)
                {
                    VacuumOnOff(equip, false, EmCellPosi.One_1);
                    VacuumOnOff(equip, false, EmCellPosi.One_2);
                    ExecuteEnd = true;
                }
                return;
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                // JICHA [20181130] - Laser Cutting을 위한 임시 작업 여기까지.
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                if (YAxis.PtpMoveCmdSync(equip, EmLaserCuttingStageYAxisServo.Cell_Unloading, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    StepCmd = EmProcessStageUnitCmd.S03020_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S03020_)
            {
                PioSend.SendPanelInfoSet = this.PanelSet;
                PioSend.YSendAble = true;
                StepCmd = EmProcessStageUnitCmd.S03030_;
            }
            else if (StepCmd == EmProcessStageUnitCmd.S03030_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;

                    StepCmd = EmProcessStageUnitCmd.S03040_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S03040_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true && PioSend.XRecvComplete == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    PioSend.YSendComplete = true;

                    StepCmd = EmProcessStageUnitCmd.S03050_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S03050_)
            {
                if (PioSend.XRecvAble == false && PioSend.XRecvStart == false && PioSend.XRecvComplete == false)
                {
                    this.PanelSet.ClearPanel();

                    PioSend.YSendAble = false;
                    PioSend.YSendStart = false;
                    PioSend.YSendComplete = false;

                    StepCmd = EmProcessStageUnitCmd.S03060_;
                }
            }
            else if (StepCmd == EmProcessStageUnitCmd.S03060_)
            {
                StepCmd = EmProcessStageUnitCmd.S000_CELL_LOAD_STRAT;
            }
        }

        //각 장치별 인터락
        public bool YAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            LoaderTransferUnit loaderTr = equip.GetUnit(this.UnitPosi, EmUnitType.LoaderTransfer) as LoaderTransferUnit;
            AfterIRCutTransferUnit afterIRCutTr = equip.GetUnit(this.UnitPosi, EmUnitType.AfterIRCutTransfer) as AfterIRCutTransferUnit;

            if (IsExitsPanel1 == true && (Vacuum1Ch1.IsOnOff != true || Vacuum1Ch2.IsOnOff != true))
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage YAxis 이동 불가, 배큠1이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (IsExitsPanel2 == true && (Vacuum2Ch1.IsOnOff != true || Vacuum2Ch2.IsOnOff != true))
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage YAxis 이동 불가, 배큠2이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }

            // 로딩트랜스퍼 실린더
            if (loaderTr.UpDown1Cylinder.IsBackward != true && loaderTr.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage  YAxis 이동 불가, {0}LoaderTransfer의  UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            if (loaderTr.UpDown2Cylinder.IsBackward != true && loaderTr.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage  YAxis 이동 불가, {0}LoaderTransfer의  UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            // 레이저헤드 Z축
            if (equip.PROC.LaserHead.ZAxis.IsInPosition(EmLaserCuttingHeadZAxisServo.Cal_StandbyPos, equip.CurrentRecipe, null) != true &&
                equip.PROC.LaserHead.ZAxis.IsHomeOnPosition())
            {
                InterLockMgr.AddInterLock(string.Format("LaserHead  ZAxis 이동 불가, {0}LaserHead  ZAxis축이 대기상태가 아닙니다."));
                return true;
            }

            // After IR Cut TR Y축
            if (afterIRCutTr.UpDown1Cylinder.IsBackward != true && afterIRCutTr.YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage  YAxis 이동 불가, {0}AfterIRCutTransfer의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            if (afterIRCutTr.UpDown2Cylinder.IsBackward != true && afterIRCutTr.YAxis.IsInPosition(EmAfterIRCutYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LaserCutStage  YAxis 이동 불가, {0}AfterIRCutTransfer의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }


        public bool VacuumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vacuum1Ch1.OnOff(equip, onOff);
                Vacuum1Ch2.OnOff(equip, onOff);
                if (onOff == false)
                {
                    Blower1Ch1.OnOff(equip, true, 500);
                    Blower1Ch2.OnOff(equip, true, 500);
                }
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vacuum2Ch1.OnOff(equip, onOff);
                Vacuum2Ch2.OnOff(equip, onOff);
                if (onOff == false)
                {
                    Blower2Ch1.OnOff(equip, true, 500);
                    Blower2Ch2.OnOff(equip, true, 500);
                }
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vacuum1Ch1.OnOff(equip, onOff);
                Vacuum1Ch2.OnOff(equip, onOff);
                Vacuum2Ch1.OnOff(equip, onOff);
                Vacuum2Ch2.OnOff(equip, onOff);

                if (onOff == false)
                {
                    Blower1Ch1.OnOff(equip, true, 500);
                    Blower1Ch2.OnOff(equip, true, 500);
                    Blower2Ch1.OnOff(equip, true, 500);
                    Blower2Ch2.OnOff(equip, true, 500);
                }
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vacuum1Ch1.IsOnOff == true && Vacuum1Ch2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vacuum2Ch1.IsOnOff == true && Vacuum2Ch2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vacuum1Ch1.IsOnOff == true && Vacuum1Ch2.IsOnOff == true && Vacuum2Ch1.IsOnOff == true && Vacuum2Ch2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vacuum1Ch1.IsOnOff == false && Vacuum1Ch2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vacuum2Ch1.IsOnOff == false && Vacuum2Ch2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vacuum1Ch1.IsOnOff == false && Vacuum1Ch2.IsOnOff == false && Vacuum2Ch1.IsOnOff == false && Vacuum2Ch2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
    }
}

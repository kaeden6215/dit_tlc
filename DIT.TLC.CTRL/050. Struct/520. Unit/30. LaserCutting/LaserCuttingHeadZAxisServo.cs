﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmLaserCuttingHeadZAxisServo
    {
        PowermeterMeasure /**/= 0,
        LaserCutting      /**/= 1,
        Cal_StandbyPos   /**/= 2,
    }

    public class LaserCuttingHeadZAxisServo : ServoMotorControl
    {
        public LaserCuttingHeadZAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 100;
            SoftSpeedLimit = 50;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmLaserCuttingHeadZAxisServo.Cal_StandbyPos == posiNo)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 10, Accel = 10 };
            }
            else
                return base.GetCalPosition(posiNo, recp, opt);
        }
        public bool IsInPosition(EmLaserCuttingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmLaserCuttingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmLaserCuttingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmLaserCuttingHeadZAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

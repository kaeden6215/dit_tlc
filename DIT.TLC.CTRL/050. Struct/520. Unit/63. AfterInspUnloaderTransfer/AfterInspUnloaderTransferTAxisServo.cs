﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public enum EmAfterInspUnloaderTransferTAxisServo
    {
        Piker_0dgr /**/= 0,

        //계산된 위치
        Cal_90rdgr /**/= 1,
        Cal_90dgr  /**/= 2,
        Cal_180dgr /**/= 3,

        Cal_Cell_Unloading, //레시피별 턴 위치.
    }
    public class AfterInspUnloaderTransferTAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(GG.StartupPath, "Setting", "AfterInspUnlTransferTAxisServo .ini");

        public AfterInspUnloaderTransferTAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {
            PcToCtrllerPositionScale = 10;
            SoftMinusLimit = 0;
            SoftPlusLimit = 0;
            SoftSpeedLimit = 0;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        { 

            if (posiNo == (int)EmAfterInspUnloaderTransferTAxisServo.Cal_90rdgr)
            {
                ServoPosiInfo piker0dgrPosi = base.GetCalPosition((int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, recp, opt);
                piker0dgrPosi.Position -= 90;
                return piker0dgrPosi;
            }
            else if (posiNo == (int)EmAfterInspUnloaderTransferTAxisServo.Cal_90dgr)
            {
                ServoPosiInfo piker0dgrPosi = base.GetCalPosition((int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, recp, opt);
                piker0dgrPosi.Position += 90;
                return piker0dgrPosi;
            }
            else if (posiNo == (int)EmAfterInspUnloaderTransferTAxisServo.Cal_180dgr)
            {
                ServoPosiInfo piker0dgrPosi = base.GetCalPosition((int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, recp, opt);
                piker0dgrPosi.Position += 180;
                return piker0dgrPosi;
            } 
            else if (posiNo == (int)EmAfterInspUnloaderTransferTAxisServo.Cal_Cell_Unloading)
            {
                PanelInfoSet pSet = opt as PanelInfoSet;
                if (pSet != null)
                {
                    ServoPosiInfo piker0dgrPosi = base.GetCalPosition((int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, recp, opt);
                    if (AxisPosi == EmAxisPosi.Aixs1)
                    {
                        //pSet.Panel1.PreAlignResult.Angle
                        piker0dgrPosi.Position += 0;
                    }
                    else
                    {
                        //pSet.Panel2.PreAlignResult.Angle
                        piker0dgrPosi.Position += 0;
                    }
                    return piker0dgrPosi;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            } 
        }
        public bool IsInPosition(EmAfterInspUnloaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmAfterInspUnloaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmAfterInspUnloaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmAfterInspUnloaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}
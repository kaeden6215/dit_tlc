﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public class AfterInspUnloaderTransferXAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "LoaderTransferXAxisServo.ini");

        public static int Loader_Laoding_X1 { get; set; }
        public static int Loader_Laoding_X2 { get; set; }
        public static int Loader_A_Stage_Unloding_X1 { get; set; }
        public static int Loader_A_Stage_Unloding_X2 { get; set; }
        public static int Loader_B_Stage_Unloding_X1 { get; set; }
        public static int Loader_B_Stage_Unloding_X2 { get; set; }
        public static int Loader_PreAlign_X1 { get; set; }
        public static int Loader_PreAlign_X2 { get; set; }

        
        public AfterInspUnloaderTransferXAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
         base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            //Loader_Laoding_X1                   = 1;
            //Loader_Laoding_X2                   = 2;
            //Loader_A_Stage_Unloding_X1          = 3;
            //Loader_A_Stage_Unloding_X2          = 4;
            //Loader_B_Stage_Unloding_X1          = 5;
            //Loader_B_Stage_Unloding_X2          = 6;
            //Loader_PreAlign_X1                  = 7;
            //Loader_PreAlign_X2                  = 8;


            //base.MoveActionName[0] = "X1_Loading";              //   로더 이재기(X1) : 로딩
            //base.MoveActionName[1] = "X2_Loading";              //   로더 이재기(X2) : 로딩
            //base.MoveActionName[2] = "X1_Unload_L";             //   로더 이재기(X1) : A 스테이지 언로딩
            //base.MoveActionName[3] = "X2_Unload_L";             //   로더 이재기(X2) : A 스테이지 언로딩
            //base.MoveActionName[4] = "X2_Unload_R";             //   로더 이재기(X1) : B 스테이지 언로딩
            //base.MoveActionName[5] = "X1_Unload_R";             //   로더 이재기(X2) : B 스테이지 언로딩
            //base.MoveActionName[6] = "Picker_PreAlignX1";       //   로더 이재기(X1) : PreAlign X1
            //base.MoveActionName[7] = "Picker_PreAlignX2";       //   로더 이재기(X2) : PreAlign X2
        }
        
    }
}
﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class AfterInspUnLoaderTransferUnit : BaseUnit
    {
        public enum EmAfterInspTrHomeStep
        {
            H0000,
            H0010,
            H0020,
            H0030,
            H0040,
            H0050,

            H5999_HOME_COMPLETE
        }

        public enum EmAfterInspTransferCmd
        {
            S1010_CELL_LOADING_START,
            S1020_,
            S1030_,
            S1040_,
            S1050_,
            S1060_,
            S1070_,
            S1080_,
            S1090_,
            S1100_,
            S1110_,

            S2010_CELL_UNLOAD_START,
            S2020_,
            S2030_,
            S2040_,
            S2050_,
            S2060_,
            S2070_,
            S2080_,
            S2090_,
            S2100_,
            S2110_,
            S2120_,
        }

        public AfterInspUnloaderTransferYAxisServo Y2Axis { get; set; }

        public AfterInspUnloaderTransferTAxisServo T1Axis { get; set; }
        public AfterInspUnloaderTransferTAxisServo T2Axis { get; set; }

        public Cylinder UpDown1Cylinder = new Cylinder();
        public Cylinder UpDown2Cylinder = new Cylinder();


        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();




        public PlcAddr HandPressureValue1 { get; internal set; }
        public PlcAddr HandPressureValue2 { get; internal set; }

        private InspStageUnit InspState = null;


        public AfterInspUnLoaderTransferUnit()
        {

        }


        public void InitializeInterLock()
        {
            if (InspState == null)
                InspState = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.InspStage) as InspStageUnit;

            Vaccum1.Simulaotr = GG.VacuumSimul;
            Vaccum2.Simulaotr = GG.VacuumSimul;

            Y2Axis.CheckStartMoveInterLockFunc = Y2Axis_CheckStartMoveInterLockFunc;
            T1Axis.CheckStartMoveInterLockFunc = T1Axis_CheckStartMoveInterLockFunc;
            T2Axis.CheckStartMoveInterLockFunc = T2Axis_CheckStartMoveInterLockFunc;


            SetStepSwith("HomeSeqLogicWorking");
            SetStepSwith("CellLdLogicWorking");
            SetStepSwith("CellUldLogicWorking");
        }

        public override void LogicWorking(Equipment equip)
        {
            Y2Axis.LogicWorking(equip);

            T1Axis.LogicWorking(equip);
            T2Axis.LogicWorking(equip);

            UpDown1Cylinder.LogicWorking(equip);
            UpDown2Cylinder.LogicWorking(equip);


            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            SeqLogicWorking(equip);
        }
        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();


            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                SeqStep = EmAfterInspTransferCmd.S1010_CELL_LOADING_START;

                IsHomeComplete = false;
                HomeStep = EmAfterInspTrHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (equip.UD.InspCamera.IsHomeComplete == true)
                {
                    if (GetStepSwith("HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                }
                return;
            }

            //전체 설비가 홈이 되었을때.  시퀀스 들어감. 
            if (equip.IsHomeComplete == false) return;
          
            if (GetStepSwith("CellLdLogicWorking")) CellLdLogicWorking(equip);
            if (GetStepSwith("CellUldLogicWorking")) CellUldLogicWorking(equip);
             
        }

        public EmAfterInspTrHomeStep HomeStep = EmAfterInspTrHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmAfterInspTrHomeStep.H0000)
            {
                if (IsExitsPanel == true)
                {
                    //공압  On/Off로 
                    if (IsVaccumOn(PanelSet.CellPosi) == true)
                    {
                        HomeStep = EmAfterInspTrHomeStep.H0010;
                    }
                    else
                    {
                        InterLockMgr.AddInterLock("셀 위치와 버큠이 상이함", "UNLOADER AFTER TR의 CELL 위치 {0}", PanelSet.CellPosi);
                    }
                }
                else
                {
                    HomeStep = EmAfterInspTrHomeStep.H0010;
                }
            }
            else if (HomeStep == EmAfterInspTrHomeStep.H0010)
            {
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) != true)
                {
                    if (UpDownCylinder_Backward(equip, EmCellPosi.Two) == true)
                    {
                        HomeStep = EmAfterInspTrHomeStep.H0020;
                    }
                }
                else
                {
                    HomeStep = EmAfterInspTrHomeStep.H0020;
                }
            }
            else if (HomeStep == EmAfterInspTrHomeStep.H0020)
            {
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) == true)
                {
                    if (Y2Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmAfterInspTrHomeStep.H0030;
                    }
                }
            }
            else if (HomeStep == EmAfterInspTrHomeStep.H0030)
            {
                if (Y2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmAfterInspTrHomeStep.H0040;
                }
            }

            else if (HomeStep == EmAfterInspTrHomeStep.H0040)
            {
                //if (T1Axis.IsHomeOnPosition() != true || T2Axis.IsHomeOnPosition() != true)
                //{
                //    T1Axis.GoHome(equip);
                //    T2Axis.GoHome(equip);
                //    HomeStep = EmAfterInspTrHomeStep.H0050;
                //}
                //else
                {
                    HomeStep = EmAfterInspTrHomeStep.H0050;
                }
            }
            else if (HomeStep == EmAfterInspTrHomeStep.H0050)
            {
                //if (T1Axis.IsHomeOnPosition() == true && T2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmAfterInspTrHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmAfterInspTrHomeStep.H5999_HOME_COMPLETE)
            {
                IsHomeComplete = true;
            }
        }

        public EmAfterInspTransferCmd SeqStep { get; set; }
        public void CellLdLogicWorking(Equipment equip)
        {
            if (SeqStep == EmAfterInspTransferCmd.S1010_CELL_LOADING_START)
            {
                //홈완료 되었을때 확인 
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) == true)
                {
                    if (PioRecv.XSendAble == true)
                    {
                        if (Y2Axis.PtpMoveCmd(equip, EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
                        {
                            //동시 이동 시작
                            //T1Axis.PtpMoveCmd(equip, EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, equip.CurrentRecipe, null);
                            //T2Axis.PtpMoveCmd(equip, EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, equip.CurrentRecipe, null);
                            SeqStep = EmAfterInspTransferCmd.S1020_;
                        }
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1020_)
            {
                if (Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true
                    //&& T1Axis.IsInPosition(EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, equip.CurrentRecipe, null) == true &&
                    //T2Axis.IsInPosition(EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr, equip.CurrentRecipe, null) == true
                    )
                {
                    PioRecv.YRecvAble = true;
                    SeqStep = EmAfterInspTransferCmd.S1030_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1030_)
            {
                if (PioRecv.XSendAble == true)
                {
                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;
                    SeqStep = EmAfterInspTransferCmd.S1040_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1040_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (UpDownCylinder_Forward(equip, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmAfterInspTransferCmd.S1050_;
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1050_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsUpDownCylinderIsForward(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        if (VaccumOnOff(equip, true, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmAfterInspTransferCmd.S1060_;
                        }
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1060_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsVaccumOn(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        if (InspState.VaccumOnOff(equip, false, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmAfterInspTransferCmd.S1070_;
                        }
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1070_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (InspState.IsVaccumOff(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmAfterInspTransferCmd.S1080_;
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1080_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (UpDownCylinder_Backward(equip, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmAfterInspTransferCmd.S1090_;
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1090_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsUpDownCylinderIsBackward(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        this.PanelSet = PioRecv.RecvPanelInfoSet;

                        PioRecv.YRecvAble = true;
                        PioRecv.YRecvStart = true;
                        PioRecv.YRecvComplete = true;

                        SeqStep = EmAfterInspTransferCmd.S1100_;

                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1100_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true && PioRecv.XSendComplete == true)
                {

                    PioRecv.YRecvAble = false;
                    PioRecv.YRecvStart = false;
                    PioRecv.YRecvComplete = false;
                    SeqStep = EmAfterInspTransferCmd.S1110_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S1110_)
            {
                if (PioRecv.XSendAble == false && PioRecv.XSendStart == false && PioRecv.XSendComplete == false)
                {
                    SeqStep = EmAfterInspTransferCmd.S2010_CELL_UNLOAD_START;
                }
            }
        }
        public void CellUldLogicWorking(Equipment equip)
        {
            if (SeqStep == EmAfterInspTransferCmd.S2010_CELL_UNLOAD_START)
            {
                //동시 이동 시작
                Y2Axis.PtpMoveCmd(equip, EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null);
                //T12AxisPtpMoveCmd(equip, EmAfterInspUnloaderTransferTAxisServo.Cal_Cell_Unloading, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi);
                SeqStep = EmAfterInspTransferCmd.S2020_;
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2020_)
            {
                if (Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true
                    //&& T12AxisIsInPosition(EmAfterInspUnloaderTransferTAxisServo.Cal_Cell_Unloading, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi) == true
                    )
                {
                    PioSend.YSendAble = true;
                    PioSend.SendPanelInfoSet = this.PanelSet;
                    SeqStep = EmAfterInspTransferCmd.S2030_;
                }

            }
            else if (SeqStep == EmAfterInspTransferCmd.S2030_)
            {
                if (PioSend.XRecvAble == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    SeqStep = EmAfterInspTransferCmd.S2040_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2040_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    if (UpDownCylinder_Forward(equip, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmAfterInspTransferCmd.S2050_;
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2050_)
            {
                if (IsUpDownCylinderIsForward(this.PanelSet.CellPosi) == true)
                {
                    if (equip.UD.Unloader.VaccumOnOff(equip, true, this.PanelSet.CellPosi))
                    {
                        SeqStep = EmAfterInspTransferCmd.S2060_;
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2060_)
            {
                if (equip.UD.Unloader.IsVaccumOn(this.PanelSet.CellPosi) == true)
                {
                    if (VaccumOnOff(equip, false, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmAfterInspTransferCmd.S2070_;
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2070_)
            {
                //tr바큠체크
                if (IsVaccumOff(this.PanelSet.CellPosi) == true)
                {
                    if (UpDownCylinder_Backward(equip, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmAfterInspTransferCmd.S2080_;
                    }
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2080_)
            {
                if (IsUpDownCylinderIsBackward(this.PanelSet.CellPosi) == true)
                {
                    SeqStep = EmAfterInspTransferCmd.S2090_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2090_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    PioSend.YSendComplete = true;
                    SeqStep = EmAfterInspTransferCmd.S2100_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2100_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true && PioSend.XRecvComplete == true)
                {
                    PioSend.YSendAble = false;
                    PioSend.YSendStart = false;
                    PioSend.YSendComplete = false;

                    SeqStep = EmAfterInspTransferCmd.S2110_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2110_)
            {
                if (PioSend.XRecvAble == false && PioSend.XRecvStart == false && PioSend.XRecvComplete == false)
                {
                    this.PanelSet.ClearPanel();

                    SeqStep = EmAfterInspTransferCmd.S2120_;
                }
            }
            else if (SeqStep == EmAfterInspTransferCmd.S2120_)
            {
                SeqStep = EmAfterInspTransferCmd.S1010_CELL_LOADING_START;
            }
        }
        public void LoaderCmdLogicWorking(Equipment equip)
        {
            if (equip.RunMode == EmEquipRunMode.Stop)
                return;

            if (equip.RunMode == EmEquipRunMode.Pause)
                return;


        }


        //public bool SubX1Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        //{
        //    CstLoaderUint cstLoaderUnit = (CstLoaderUint)equip.GetUnit(this.UnitPosi, EmUnitType.CstLoader);
        //    AfterIRCutTransferUnit afterIRCutTransferUnit = (AfterIRCutTransferUnit)equip.GetUnit(this.UnitPosi, EmUnitType.AfterIRCutTransfer);
        //
        //    return false;
        //}


        //각 장치별 인터락
        public bool Y2Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //공압
            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} UnloaderTransfer Y2Axis 이동 불가, 배큠1이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} UnloaderTransfer Y2Axis 이동 불가, 배큠2이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }


            if (UpDown1Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderTransfer  Y2Axis 이동 불가, {0}UnloaderTransfer의  UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            if (UpDown2Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderTransfer  Y2Axis 이동 불가, {0}UnloaderTransfer의  UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }

        public bool T1Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //셀이 있을 경우 공압확인
            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} UnloaderTransfer Y2Axis 이동 불가, 배큠1이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            //업다운 실린더 up
            if (UpDown1Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderTransfer  T1Axis이동 불가, {0}UnloaderTransfer의  UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            return false;
        }
        public bool T2Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //셀이 있을 경우 공압 확인
            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} UnloaderTransfer Y2Axis 이동 불가, 배큠2이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (UpDown2Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderTransfer  T2Axis이동 불가, {0}UnloaderTransfer의  UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            return false;
        }


        public bool UpDown1Cylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect direct)
        {

            InspStageUnit inspStage = equip.GetUnit(this.UnitPosi, EmUnitType.InspStage) as InspStageUnit;

            //인터락 처리. 
            if (equip.UD.InspectionStage_A.YAxis.IsInPosition(EmInspStageYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true &&
                Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            //언로딩위치일떄
            else if (equip.UD.Unloader.X1Axis.IsInPosition(EmUnLoaderXAxisServo.Cell_A_Loading, equip.CurrentRecipe, null) == true &&
                Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            else
            {
                InterLockMgr.AddInterLock(string.Format("{0} AfterInspUnloaderTransfer  UpDown1Cylinder 전/후진 불가, {0}AfterInspUnloaderTransfer의 UpDown1Cylinder가 UP 위치가 아닙니다.", UnitPosi));
                return true;
            }
            // T축 is무빙이 없을떄.

            return false;
        }
        public bool UpDown2Cylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect direct)
        {
            InspStageUnit inspStage = equip.GetUnit(this.UnitPosi, EmUnitType.InspStage) as InspStageUnit;

            //인터락 처리. 
            if (equip.UD.InspectionStage_B.YAxis.IsInPosition(EmInspStageYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true &&
                Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            //언로딩위치일떄
            else if (equip.UD.Unloader.X2Axis.IsInPosition(EmUnLoaderXAxisServo.Cell_A_Loading, equip.CurrentRecipe, null) == true &&
                Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            else
            {
                InterLockMgr.AddInterLock(string.Format("{0} AfterInspUnloaderTransfer  UpDown2Cylinder 전/후진 불가, {0}AfterInspUnloaderTransfer의 UpDown2Cylinder가 UP 위치가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }

        public bool UpDownCylinder_Forward(Equipment equip, EmCellPosi cellPosi)
        {

            if (cellPosi == EmCellPosi.One_1)
            {
                UpDown1Cylinder.Forward(equip);
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                UpDown2Cylinder.Forward(equip);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                UpDown1Cylinder.Forward(equip);
                UpDown2Cylinder.Forward(equip);
            }

            return true;
        }
        public bool UpDownCylinder_Backward(Equipment equip, EmCellPosi cellPosi)
        {

            if (cellPosi == EmCellPosi.One_1)
            {
                UpDown1Cylinder.Backward(equip);
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                UpDown2Cylinder.Backward(equip);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                UpDown1Cylinder.Backward(equip);
                UpDown2Cylinder.Backward(equip);
            }

            return true;
        }
        public bool IsUpDownCylinderIsForward(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown1Cylinder.IsForward;
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown2Cylinder.IsForward;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return UpDown1Cylinder.IsForward &&
                UpDown2Cylinder.IsForward;
            }
            else
            {
                return false;
            }
        }
        public bool IsUpDownCylinderIsBackward(EmCellPosi cellPosi)
        {

            if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown1Cylinder.IsBackward;
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown2Cylinder.IsBackward;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return UpDown1Cylinder.IsBackward &&
                UpDown2Cylinder.IsBackward;
            }
            else
            {
                return false;
            }
        }

        public bool Y12AxisPtpMoveCmd(Equipment equip, EmAfterInspUnloaderTransferYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            return true;
        }
        public bool Y12AxisIsInPosition(EmAfterInspUnloaderTransferYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            //
            if (cellPosi == EmCellPosi.One_1)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            return true;
        }

        public bool VaccumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vaccum1.OnOff(equip, onOff);
                if (onOff == false) Blower1.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vaccum2.OnOff(equip, onOff);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vaccum1.OnOff(equip, onOff);
                Vaccum2.OnOff(equip, onOff);

                if (onOff == false) Blower1.OnOff(equip, true, 500);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == false && Vaccum2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }


        public bool T12AxisPtpMoveCmd(Equipment equip, EmAfterInspUnloaderTransferTAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return T1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return T2Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool t1Reulst = T1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
                bool t2Reulst = T2Axis.PtpMoveCmd(equip, posiNo, recp, opt);

                return t1Reulst && t2Reulst;
            }
            else
                return false;
        }
        public bool T12AxisPtpMoveCmdSync(Equipment equip, EmAfterInspUnloaderTransferTAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return T1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return T2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool x1Reulst = T1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                bool x2Reulst = T2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);

                return x1Reulst && x2Reulst;
            }
            else
                return false;
        }
        public bool T12AxisIsInPosition(EmAfterInspUnloaderTransferTAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return T1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return T2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return T1Axis.IsInPosition(posiNo, recp, opt) && T2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }

    }
}


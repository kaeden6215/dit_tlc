﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public enum EmLoaderTransferSubYAxisServo
    {
        Cal_Home,
        Cal_Cell_Align,
        Cal_Cell_Unloading_Align,
    }

    public class LoaderTransferSubYAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(GG.StartupPath, "Setting", "LoaderTransferYAxisServo.ini");

        public LoaderTransferSubYAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 30;
            SoftSpeedLimit = 100;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if (posiNo == (int)EmLoaderTransferSubYAxisServo.Cal_Home)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 5, Accel = 100 };
            }
            else if (posiNo == (int)EmLoaderTransferSubYAxisServo.Cal_Cell_Align)
            {
                return new ServoPosiInfo() { Position = 0, Speed = 5, Accel = 100 };
            }
            else if (posiNo == (int)EmLoaderTransferSubYAxisServo.Cal_Cell_Unloading_Align) //언로딩 보정 위치 
            {
                PanelInfoSet pSet = opt as PanelInfoSet;
                if (pSet != null)
                {
                    if (AxisPosi == EmAxisPosi.Aixs1)
                    {
                        return new ServoPosiInfo() { Position = (float)pSet.Panel1.PreAlignResult.Y, Speed = 5, Accel = 100 }; //위치 계산 필요. 
                    }
                    else
                    {
                        return new ServoPosiInfo() { Position = (float)pSet.Panel2.PreAlignResult.Y, Speed = 5, Accel = 100 }; //위치 계산 필요. 
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        public bool IsInPosition(EmLoaderTransferSubYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmLoaderTransferSubYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmLoaderTransferSubYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmLoaderTransferSubYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}
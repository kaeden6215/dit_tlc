﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmLoaderTrHomeStep
    {
        H0000,
        H0010,
        H0020,
        H0030,
        H0040,
        H0050,

        H5999_HOME_COMPLETE
    }

    public enum EmLoaderTrStep
    {
        S4100_,
        S1000_CELL_LOAD_START,
        S1010_,
        S1020_,
        S1030_,
        S1040_,
        S1050_,
        S1060_,
        S1070_,
        S1080_,
        S1090_,

        S2010_ALIGN_START,
        S2020_,
        S2030_,
        S2040_,
        S2050_,

        S3010_MCR_START,
        S3020_,
        S3030_,

        S4010_TR_ULD_START,
        S4020_,
        S4030_,
        S4040_,
        S4080_,
        S4060_,
        s4070_,

        s4060_,
        S0110_,
        S0610_,
        S0620_,
        S0620_HomePos,
        S1100_,
        S4090_,
        S4110_,
        S4120_,
        S4050_,
        S4130_,
    }
    public class LoaderTransferUnit : BaseUnit
    {
        //Align 보정 처리..
        public LoaderTransferYAxisServo YAxis { get; set; }
        public LoaderTransferXAxisServo X1Axis { get; set; }
        public LoaderTransferXAxisServo X2Axis { get; set; }

        public LoaderTransferSubYAxisServo SubY1Axis { get; set; }
        public LoaderTransferSubYAxisServo SubY2Axis { get; set; }
        public LoaderTransferSubTAxisServo SubT1Axis { get; set; }
        public LoaderTransferSubTAxisServo SubT2Axis { get; set; }

        public Cylinder UpDown1Cylinder = new Cylinder();
        public Cylinder UpDown2Cylinder = new Cylinder();


        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();

        public PlcAddr HandPressureValue1 { get; internal set; }
        public PlcAddr HandPressureValue2 { get; internal set; }
        public LaserCuttingStageUnit LaserCuttingStage = null;
        public AfterIRCutTransferUnit AfterIRCutTransfer = null;

        public LoaderTransferUnit()
        {
        }


        private PlcTimerEx _tmrLoderTrVaccumTimeOver = new PlcTimerEx("로더TR 버큠 타입 오버 타이머");
        private PlcTimerEx _tmrIRCutStageVaccumTimeOver = new PlcTimerEx("IRCutStage 버큠 타입 오버 타이머");

        public void InitializeInterLock()
        {

            if (LaserCuttingStage == null)
                LaserCuttingStage = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;

            if (AfterIRCutTransfer == null)
                AfterIRCutTransfer = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.AfterIRCutTransfer) as AfterIRCutTransferUnit;

            if (LaserCuttingStage == null)
                LaserCuttingStage = GG.Equip.GetUnit(this.UnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;

            Vaccum1.Simulaotr = GG.VacuumSimul;
            Vaccum2.Simulaotr = GG.VacuumSimul;



            YAxis.CheckStartMoveInterLockFunc = YAxis_CheckStartMoveInterLockFunc;
            X1Axis.CheckStartMoveInterLockFunc = X1Axis_CheckStartMoveInterLockFunc;
            X2Axis.CheckStartMoveInterLockFunc = X2Axis_CheckStartMoveInterLockFunc;
            SubY1Axis.CheckStartMoveInterLockFunc = SubYT1Axis_CheckStartMoveInterLockFunc;
            SubY2Axis.CheckStartMoveInterLockFunc = SubYT2Axis_CheckStartMoveInterLockFunc;
            SubT1Axis.CheckStartMoveInterLockFunc = SubYT1Axis_CheckStartMoveInterLockFunc;
            SubT2Axis.CheckStartMoveInterLockFunc = SubYT2Axis_CheckStartMoveInterLockFunc;

            UpDown1Cylinder.CheckStartMoveInterLockFunc = UpDown1Cylinder_CheckStartMoveInterLockFunc;
            UpDown2Cylinder.CheckStartMoveInterLockFunc = UpDown2Cylinder_CheckStartMoveInterLockFunc;

            SetStepSwith( "LoaderHomeSeqLogicWorking");
            SetStepSwith( "CellLdLogicWorking");
            SetStepSwith( "CellPreAlignLogicWorking");
            SetStepSwith( "CellMcrLogicWorking");
            SetStepSwith( "CellUdLogicWorking");
        }

        public override void LogicWorking(Equipment equip)
        {

            YAxis.LogicWorking(equip);
            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);

            SubY1Axis.LogicWorking(equip);
            SubY2Axis.LogicWorking(equip);

            SubT1Axis.LogicWorking(equip);
            SubT2Axis.LogicWorking(equip);

            UpDown1Cylinder.LogicWorking(equip);
            UpDown2Cylinder.LogicWorking(equip);
            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            SeqLogicWorking(equip);
        }

        //구동 시퀀스 구성.                 
        public EmLoaderTrStep SeqStep = EmLoaderTrStep.S1000_CELL_LOAD_START;
        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                IsHomeComplete = false;
                SeqStep = EmLoaderTrStep.S1000_CELL_LOAD_START;
                HomeStep = EmLoaderTrHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (GetStepSwith( "LoaderHomeSeqLogicWorking"))      /**/HomeSeqLogicWorking(equip);
                return;
            }

            if (equip.IsHomeComplete == false) return;

            if (GetStepSwith( "CellLdLogicWorking"))                 /**/CellLdLogicWorking(equip);
            if (GetStepSwith( "CellPreAlignLogicWorking"))           /**/CellPreAlignLogicWorking(equip);
            if (GetStepSwith( "CellMcrLogicWorking"))                /**/CellMcrLogicWorking(equip);
            if (GetStepSwith( "CellUdLogicWorking"))                 /**/CellUdLogicWorking(equip);
            
        }


        //HOME 시퀀스 구성. 
        public EmLoaderTrHomeStep HomeStep = EmLoaderTrHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmLoaderTrHomeStep.H0000)
            {
                if (IsExitsPanel == true)
                {
                    //공압  On/Off로 
                    if (IsVaccumOn(PanelSet.CellPosi) == true)
                    {
                        HomeStep = EmLoaderTrHomeStep.H0010;
                    }
                    else
                    {
                        InterLockMgr.AddInterLock("셀 위치와 버큠이 상이함", "LADER TR의 CELL 위치 {0}", PanelSet.CellPosi);
                    }
                }
                else
                {
                    HomeStep = EmLoaderTrHomeStep.H0010;
                }
            }
            else if (HomeStep == EmLoaderTrHomeStep.H0010)
            {
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) == false)
                {
                    if (UpDownCylinder_Backward(equip, EmCellPosi.Two) == true)
                    {
                        HomeStep = EmLoaderTrHomeStep.H0020;
                    }
                }
                else
                {
                    HomeStep = EmLoaderTrHomeStep.H0020;
                }
            }
            else if (HomeStep == EmLoaderTrHomeStep.H0020)
            {
                if (IsUpDownCylinderIsBackward(EmCellPosi.Two) == true)
                {
                    if (YAxis.GoHome(equip) == true)
                    {
                        HomeStep = EmLoaderTrHomeStep.H0030;
                    }
                }
            }
            else if (HomeStep == EmLoaderTrHomeStep.H0030)
            {
                if (YAxis.IsHomeOnPosition() == true)
                {
                    X1Axis.GoHome(equip);
                    X2Axis.GoHome(equip);
                    HomeStep = EmLoaderTrHomeStep.H0040;
                }
            }
            else if (HomeStep == EmLoaderTrHomeStep.H0040)
            {
                if (X1Axis.IsHomeOnPosition() == true && X2Axis.IsHomeOnPosition() == true)
                {

                    //임시
                    //SubY1Axis.GoHome(equip);
                    //SubY2Axis.GoHome(equip);
                    //SubT1Axis.GoHome(equip);
                    //SubT2Axis.GoHome(equip);
                    HomeStep = EmLoaderTrHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmLoaderTrHomeStep.H0050)
            {
                //임시

                //if (SubY1Axis.IsHomeOnPosition() == true && SubY2Axis.IsHomeOnPosition() == true &&
                //SubT1Axis.IsHomeOnPosition() == true && SubT2Axis.IsHomeOnPosition() == true
                //)
                {
                    HomeStep = EmLoaderTrHomeStep.H5999_HOME_COMPLETE;
                    IsHomeComplete = true;
                }
            }
            else if (HomeStep == EmLoaderTrHomeStep.H5999_HOME_COMPLETE)
            {
                IsHomeComplete = true;
            }
        }


        public void CellLdLogicWorking(Equipment equip)
        {
            Console.WriteLine(this.UnitPosi );
            if (SeqStep == EmLoaderTrStep.S1000_CELL_LOAD_START)
            {
                //홈완료 되었을때 확인 
                if (IsUpDownCylinderIsBackward(this.PanelSet.CellPosi) == true)
                {
                    if (YAxis.PtpMoveCmd(equip, EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true)
                    {
                        //동시 이동 시작
                        X12AxisPtpMoveCmd(equip, EmLoaderTransferXAxisServo.Cell_Loading, equip.CurrentRecipe, null, EmCellPosi.Two);

                        //임시
                        //SubT1Axis.PtpMoveCmd(equip, EmLoaderTransferSubTAxisServo.Piker_0dgr, equip.CurrentRecipe, null);
                        //SubT2Axis.PtpMoveCmd(equip, EmLoaderTransferSubTAxisServo.Piker_0dgr, equip.CurrentRecipe, null);
                        //SubY1Axis.PtpMoveCmd(equip, EmLoaderTransferSubYAxisServo.Cal_Home, equip.CurrentRecipe, null);
                        //SubY1Axis.PtpMoveCmd(equip, EmLoaderTransferSubYAxisServo.Cal_Home, equip.CurrentRecipe, null);


                        SeqStep = EmLoaderTrStep.S1010_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1010_)
            {
                if (YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                    X12AxisIsInPosition(EmLoaderTransferXAxisServo.Cell_Loading, equip.CurrentRecipe, null, EmCellPosi.Two) == true
                    //임시
                    //&& SubT1Axis.IsInPosition(EmLoaderTransferSubTAxisServo.Piker_0dgr, equip.CurrentRecipe, null) == true
                    //&& SubT2Axis.IsInPosition(EmLoaderTransferSubTAxisServo.Piker_0dgr, equip.CurrentRecipe, null) == true
                    //&& SubY1Axis.IsInPosition(EmLoaderTransferSubYAxisServo.Cal_Home, equip.CurrentRecipe, null) == true
                    //&& SubY1Axis.IsInPosition(EmLoaderTransferSubYAxisServo.Cal_Home, equip.CurrentRecipe, null) == true
                    )
                {
                    PioRecv.YRecvAble = true;
                    SeqStep = EmLoaderTrStep.S1020_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1020_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;
                    SeqStep = EmLoaderTrStep.S1030_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1030_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (UpDownCylinder_Forward(equip, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmLoaderTrStep.S1040_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1040_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsUpDownCylinderIsForward(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        if (VaccumOnOff(equip, true, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmLoaderTrStep.S1050_;
                        }
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1050_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsVaccumOn(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        if (equip.LD.Loader.VaccumOnOff(equip, false, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmLoaderTrStep.S1060_;
                        }
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1060_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (equip.LD.Loader.IsVaccumOff(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmLoaderTrStep.S1070_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1070_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (UpDownCylinder_Backward(equip, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmLoaderTrStep.S1080_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1080_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    if (IsUpDownCylinderIsBackward(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        this.PanelSet = PioRecv.RecvPanelInfoSet;

                        PioRecv.YRecvAble = true;
                        PioRecv.YRecvStart = true;
                        PioRecv.YRecvComplete = true;

                        SeqStep = EmLoaderTrStep.S1090_;

                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1090_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true && PioRecv.XSendComplete == true)
                {
                    PioRecv.YRecvAble = false;
                    PioRecv.YRecvStart = false;
                    PioRecv.YRecvComplete = false;
                    SeqStep = EmLoaderTrStep.S1100_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S1100_)
            {
                if (PioRecv.XSendAble == false && PioRecv.XSendStart == false && PioRecv.XSendComplete == false)
                {
                    SeqStep = EmLoaderTrStep.S2010_ALIGN_START;
                }
            }
        }
        public void CellPreAlignLogicWorking(Equipment equip)
        {
            if (SeqStep == EmLoaderTrStep.S2010_ALIGN_START)
            {
                if (YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                    X12AxisIsInPosition(EmLoaderTransferXAxisServo.Cell_Loading, equip.CurrentRecipe, null, EmCellPosi.Two) == true)
                {
                    YAxis.PtpMoveCmd(equip, EmLoaderTransferYAxisServo.Pre_Align, equip.CurrentRecipe, null);
                    X12AxisPtpMoveCmd(equip, EmLoaderTransferXAxisServo.Pre_Align, equip.CurrentRecipe, null, EmCellPosi.Two); //충돌 가능서 있음. 

                    SubT12AxisPtpMoveCmd(equip, EmLoaderTransferSubTAxisServo.Cal_Cell_Align, equip.CurrentRecipe, null, this.PanelSet.CellPosi);                    

                    equip.LD.PreAlign.MoveAlignXAxis(this.PanelSet, this.UnitPosi);
                    SeqStep = EmLoaderTrStep.S2020_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S2020_)
            {
                if (YAxis.IsInPosition(EmLoaderTransferYAxisServo.Pre_Align, equip.CurrentRecipe, null) == true &&
                    X12AxisIsInPosition(EmLoaderTransferXAxisServo.Pre_Align, equip.CurrentRecipe, null, EmCellPosi.Two) == true
                    //&& SubY12AxisIsInPosition(EmLoaderTransferSubYAxisServo.Cal_Cell_Align, equip.CurrentRecipe, null, this.PanelSet.CellPosi) == true 
                    )
                {
                    if (equip.LD.PreAlign.IsAlingReady(this.UnitPosi) == true)
                    {
                        equip.LD.PreAlign.StartAlign(this.PanelSet, this.UnitPosi);
                        SeqStep = EmLoaderTrStep.S2030_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S2030_)
            {
                AlignResult pnl1Result = new AlignResult();
                AlignResult pnl2Result = new AlignResult();
                if (equip.LD.PreAlign.IsAlignComplete(this.PanelSet, this.UnitPosi, out pnl1Result, out pnl2Result) == true)
                {
                    this.PanelSet.Panel1.PreAlignResult = pnl1Result;
                    this.PanelSet.Panel2.PreAlignResult = pnl2Result;

                    // 프리얼라인 Check.
                    SeqStep = EmLoaderTrStep.S2040_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S2040_)
            {
                SeqStep = EmLoaderTrStep.S3010_MCR_START;
            }
        }
        public void CellMcrLogicWorking(Equipment equip)
        {
            if (SeqStep == EmLoaderTrStep.S3010_MCR_START)
            {
                if (YAxis.PtpMoveCmdSync(equip, EmLoaderTransferYAxisServo.Cal_Mcr_Read, equip.CurrentRecipe, null) == true)
                {
                    equip.LD.PreAlign.StartMcr(this.PanelSet, this.UnitPosi);
                    SeqStep = EmLoaderTrStep.S3020_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S3020_)
            {
                McrResult pnl1Result = null;
                McrResult pnl2Result = null;

                if (equip.LD.PreAlign.IsMcrComplete(this.PanelSet, this.UnitPosi, out pnl1Result, out pnl2Result) == true)
                {
                    this.PanelSet.Panel1.McrResult = pnl1Result;
                    this.PanelSet.Panel2.McrResult = pnl2Result;

                    equip.LD.PreAlign.ResetPreAlignMcr();
                    SeqStep = EmLoaderTrStep.S3030_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S3030_)
            {
                return; // JICHA [20181130] - Laser Cutting을 위한 임시 작업
                SeqStep = EmLoaderTrStep.S4010_TR_ULD_START;
            }
        }
        public void CellUdLogicWorking(Equipment equip)
        {
            if (SeqStep == EmLoaderTrStep.S4010_TR_ULD_START)
            {
                //동시 이동 시작
                YAxis.PtpMoveCmd(equip, EmLoaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null);
                X12AxisPtpMoveCmd(equip, EmLoaderTransferXAxisServo.Cell_Unloading, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi);

                //SubT12AxisPtpMoveCmd(equip, EmLoaderTransferSubTAxisServo.Cal_Cell_Unloading_Align, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi);
                //SubY12AxisPtpMoveCmd(equip, EmLoaderTransferSubYAxisServo.Cal_Cell_Unloading_Align, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi);

                SeqStep = EmLoaderTrStep.S4020_;
            }
            else if (SeqStep == EmLoaderTrStep.S4020_)
            {

                if (YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true &&
                  X12AxisIsInPosition(EmLoaderTransferXAxisServo.Cell_Unloading, equip.CurrentRecipe, null, this.PanelSet.CellPosi) == true
                  //&& SubT12AxisIsInPosition(EmLoaderTransferSubTAxisServo.Cal_Cell_Unloading_Align, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi) == true
                  //&& SubY12AxisIsInPosition(EmLoaderTransferSubYAxisServo.Cal_Cell_Unloading_Align, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi)
                  )
                {
                    PioSend.YSendAble = true;
                    SeqStep = EmLoaderTrStep.S4030_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4030_)
            {
                if (PioSend.XRecvAble == true)
                {
                    PioSend.SendPanelInfoSet = this.PanelSet;
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    SeqStep = EmLoaderTrStep.S4040_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4040_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    if (UpDownCylinder_Forward(equip, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmLoaderTrStep.S4050_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4050_)
            {
                if (IsUpDownCylinderIsForward(this.PanelSet.CellPosi) == true)
                {
                    if (LaserCuttingStage.VacuumOnOff(equip, true, this.PanelSet.CellPosi))
                    {
                        SeqStep = EmLoaderTrStep.S4060_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4060_)
            {
                if (LaserCuttingStage.IsVaccumOn(this.PanelSet.CellPosi) == true)
                {
                    if (VaccumOnOff(equip, false, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmLoaderTrStep.s4070_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.s4070_)
            {
                //tr바큠체크
                if (IsVaccumOff(this.PanelSet.CellPosi) == true)
                {
                    if (UpDownCylinder_Backward(equip, this.PanelSet.CellPosi) == true)
                    {
                        SeqStep = EmLoaderTrStep.S4080_;
                    }
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4080_)
            {
                if (IsUpDownCylinderIsBackward(this.PanelSet.CellPosi) == true)
                {
                    SeqStep = EmLoaderTrStep.S4090_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4090_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    PioSend.YSendComplete = true;
                    SeqStep = EmLoaderTrStep.S4100_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4100_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true && PioSend.XRecvComplete == true)
                {
                    PioSend.YSendAble = false;
                    PioSend.YSendStart = false;
                    PioSend.YSendComplete = false;

                    SeqStep = EmLoaderTrStep.S4110_;
                }
            }
            else if (SeqStep == EmLoaderTrStep.S4110_)
            {
                if (PioSend.XRecvAble == false && PioSend.XRecvStart == false && PioSend.XRecvComplete == false)
                {
                    this.PanelSet.ClearPanel();

                    SeqStep = EmLoaderTrStep.S4130_;
                }
            } 
            else if (SeqStep == EmLoaderTrStep.S4130_)
            {
                SeqStep = EmLoaderTrStep.S1000_CELL_LOAD_START;
            }
        }


        //각 장치별 인터락
        public bool X1Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (UpDown1Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  X1Axis이동 불가, {0}LoaderTransfer의  UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            return false;
        }
        public bool X2Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (UpDown2Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  X2Axis이동 불가, {0}LoaderTransfer의  UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            return false;
        }
        public bool YAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  YAxis 이동 불가, 배큠이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  YAxis 이동 불가, 배큠이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }

            if (UpDown1Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  YAxis 이동 불가, {0}LoaderTransfer의  UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (UpDown2Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  YAxis 이동 불가, {0}LoaderTransfer의  UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }

        public bool SubYT1Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (UpDown1Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  SubYT1Axis이동 불가, {0}LoaderTransfer의  UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            return false;
        }
        public bool SubYT2Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (UpDown2Cylinder.IsBackward != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  SubYT2Axis이동 불가, {0}LoaderTransfer의  UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }
            return false;
        }
        public bool UpDown1Cylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect direct)
        {
            LaserCuttingStageUnit IRCutStage = equip.GetUnit(this.UnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;



            if (IRCutStage == null)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("LoaderTransferUnit의 A/B Col [{0}] 설정이 안돼어 있음. ", UnitPosi));
                return true;
            }

            //LD
            if (equip.LD.Loader.X1Axis.IsInPosition(EmLoaderXAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) == true &&
                equip.LD.Loader.Y1Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) == true &&
                YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                X1Axis.IsInPosition(EmLoaderTransferXAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }

            //ULD
            else if (IRCutStage.YAxis.IsInPosition(EmLaserCuttingStageYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                X1Axis.IsInPosition(EmLoaderTransferXAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            else
            {
                //인터락 처리.  다시 정리 필요함. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  UpDown1Cylinder이동 불가, Panel 로딩/언로딩 위치가 아닙니다.", UnitPosi));
                return true;
            }
        }
        public bool UpDown2Cylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect direct)
        {
            LaserCuttingStageUnit IRCutStage = (UnitPosi == EmUnitPosi.BCol) ? equip.PROC.IRCutStage_B :
                                               (UnitPosi == EmUnitPosi.ACol) ? equip.PROC.IRCutStage_A : null;
            //LD
            if (equip.LD.Loader.X1Axis.IsInPosition(EmLoaderXAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) == true &&
                equip.LD.Loader.Y1Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) == true &&
                YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                X2Axis.IsInPosition(EmLoaderTransferXAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            //ULD
            else if (IRCutStage.YAxis.IsInPosition(EmLaserCuttingStageYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) == true &&
                X2Axis.IsInPosition(EmLoaderTransferXAxisServo.Cell_Unloading, equip.CurrentRecipe, null) == true && direct == EmCylinderDirect.Forward)
            {
                return false;
            }
            else
            {
                //인터락 처리.  다시 정리 필요함. 
                InterLockMgr.AddInterLock(string.Format("{0}LoaderTransfer  UpDown2Cylinder이동 불가, Panel 로딩/언로딩 위치가 아닙니다.", UnitPosi));
                return true;
            }
        }


        //동작
        public bool UpDownCylinder_Forward(Equipment equip, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown1Cylinder.Forward(equip);
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown2Cylinder.Forward(equip);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool result1 = UpDown1Cylinder.Forward(equip);
                bool result2 = UpDown2Cylinder.Forward(equip);
                return result1 && result2;
            }
            return false;
        }
        public bool UpDownCylinder_Backward(Equipment equip, EmCellPosi cellPosi)
        {
            bool result1 = UpDown1Cylinder.Backward(equip);
            bool result2 = UpDown2Cylinder.Backward(equip);
            return result1 && result2;
        }
        //신호
        public bool IsUpDownCylinderIsForward(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown1Cylinder.IsForward;
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                return UpDown2Cylinder.IsForward;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return UpDown1Cylinder.IsForward && UpDown2Cylinder.IsForward;
            }
            else
            {
                return false;
            }
        }
        public bool IsUpDownCylinderIsBackward(EmCellPosi cellPosi)
        {
            return UpDown1Cylinder.IsBackward && UpDown2Cylinder.IsBackward;
        }

        public bool X12AxisPtpMoveCmd(Equipment equip, EmLoaderTransferXAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return X1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return X2Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool x1Reulst = X1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
                bool x2Reulst = X2Axis.PtpMoveCmd(equip, posiNo, recp, opt);

                return x1Reulst && x2Reulst;
            }
            else
                return false;
        }
        public bool X12AxisPtpMoveCmdSync(Equipment equip, EmLoaderTransferXAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool x1Reulst = X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                bool x2Reulst = X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);

                return x1Reulst && x2Reulst;
            }
            else
                return false;
        }
        public bool X12AxisIsInPosition(EmLoaderTransferXAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return X1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return X2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return X1Axis.IsInPosition(posiNo, recp, opt) && X2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }

        public bool SubY12AxisPtpMoveCmd(Equipment equip, EmLoaderTransferSubYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return SubY1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return SubY2Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool x1Reulst = SubY1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
                bool x2Reulst = SubY2Axis.PtpMoveCmd(equip, posiNo, recp, opt);

                return x1Reulst && x2Reulst;
            }
            else
                return false;
        }
        public bool SubY12AxisPtpMoveCmdSync(Equipment equip, EmLoaderTransferSubYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return SubY1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return SubY2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool x1Reulst = SubY1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                bool x2Reulst = SubY2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);

                return x1Reulst && x2Reulst;
            }
            else
                return false;
        }
        public bool SubY12AxisIsInPosition(EmLoaderTransferSubYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return SubY1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return SubY2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return SubY1Axis.IsInPosition(posiNo, recp, opt) && SubY2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }


        public bool SubT12AxisPtpMoveCmd(Equipment equip, EmLoaderTransferSubTAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return SubT1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return SubT2Axis.PtpMoveCmd(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool t1Reulst = SubT1Axis.PtpMoveCmd(equip, posiNo, recp, opt);
                bool t2Reulst = SubT2Axis.PtpMoveCmd(equip, posiNo, recp, opt);

                return t1Reulst && t2Reulst;
            }
            else
                return false;
        }
        public bool SubT12AxisPtpMoveCmdSync(Equipment equip, EmLoaderTransferSubTAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return SubT1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return SubT2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool x1Reulst = SubT1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                bool x2Reulst = SubT2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);

                return x1Reulst && x2Reulst;
            }
            else
                return false;
        }
        public bool SubT12AxisIsInPosition(EmLoaderTransferSubTAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return SubT1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return SubT2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return SubT1Axis.IsInPosition(posiNo, recp, opt) && SubT2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }



        public bool VaccumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vaccum1.OnOff(equip, onOff);
                if (onOff == false) Blower1.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vaccum2.OnOff(equip, onOff);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vaccum1.OnOff(equip, onOff);
                Vaccum2.OnOff(equip, onOff);

                if (onOff == false)
                {
                    Blower1.OnOff(equip, true, 500);
                    Blower2.OnOff(equip, true, 500);
                }
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == false && Vaccum2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }


    }
}

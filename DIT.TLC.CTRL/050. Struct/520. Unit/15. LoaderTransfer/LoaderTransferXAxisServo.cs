﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public enum EmLoaderTransferXAxisServo
    {
        Cell_Loading            /**/= 0,
        Pre_Align               /**/= 1,
        Cell_Unloading          /**/= 2,
        Cell_Unloading_Align    /**/= 3 
    }

    public class LoaderTransferXAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(GG.StartupPath, "Setting", "LoaderTransferXAxisServo.ini");

        public LoaderTransferXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 30;
            SoftSpeedLimit = 100;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if (posiNo == (int)EmLoaderTransferXAxisServo.Cell_Unloading_Align)
            {
                PanelInfoSet pSet = opt as PanelInfoSet;
                ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);

                if (AxisPosi == EmAxisPosi.Aixs1)
                {
                    info.Position += (float)pSet.Panel1.PreAlignResult.X;
                    return info;
                }
                else
                {
                    info.Position += (float)pSet.Panel1.PreAlignResult.X;
                    return info;
                }
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }
        public bool IsInPosition(EmLoaderTransferXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmLoaderTransferXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmLoaderTransferXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmLoaderTransferXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{

    public enum EmLoaderTransferTAxisServo
    {
        LoaderTr_Piker_90rdgr /**/= 0,
        LoaderTr_Piker_0dgr   /**/= 1,
        LoaderTr_Piker_90dgr  /**/= 2,
        LoaderTr_Piker_180dgr /**/= 3,
    }

    public class LoaderTransferTAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "LoaderTransferTAxisServo .ini");


        
        public LoaderTransferTAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;             
        }
        public bool IsInPosition(EmLoaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmLoaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmLoaderTransferTAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
    }
}
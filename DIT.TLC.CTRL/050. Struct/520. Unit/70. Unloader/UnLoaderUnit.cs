﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class UnloaderUnit : BaseUnit
    {
        public enum EmCellUnloaderHomeStep
        {
            H0000,
            H1010,
            H1020,
            H1040,
            H1050,
            H1060,
            H1070,
            H5999_HOME_COMPLETE,
            H1005,
            H1030,
            H1080,
            H1090,
            H1110,
            H1120,
        }
        public enum EmCellUnloaderStep
        {
            S0000_CELL_LOAD_START,  //로딩
            S0010_,
            S0015_,
            S0030_,
            S0040_,
            S0050_,
            S0060_,
            S0070_,
            S0080_,


            S7000_CELL_UNLOAD_START,  //언로딩      
            S7010_,
            S7020_,
            S7030_,
            S7040_,
            S7050_,
            S7060_,
            S7070_,
            S7080_,
            S0020_,
        }

        public CassetteInfo InEmptyCst { get; set; }
        public CassetteInfo OutCst { get; set; }

        public UnLoaderXAxisServo X1Axis { get; set; }
        public UnLoaderXAxisServo X2Axis { get; set; }
        public UnLoaderYAxisServo Y1Axis { get; set; }
        public UnLoaderYAxisServo Y2Axis { get; set; }


        public Switch2Cmd1Sensor Vaccum1    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1         /**/= new SwitchOneWay();

        public Switch2Cmd1Sensor Vaccum2    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower2         /**/= new SwitchOneWay();


        public PlcAddr HandPressureValue1 { get; set; }
        public PlcAddr HandPressureValue2 { get; set; }

        public PioSendStep PioSendA { get; set; }
        public PioRecvStep PioRecvA { get; set; }

        public PioSendStep PioSendB { get; set; }
        public PioRecvStep PioRecvB { get; set; }


        public UnloaderUnit()
        {
            PioSendA = new PioSendStep();
            PioSendB = new PioSendStep();

            PioRecvA = new PioRecvStep();
            PioRecvB = new PioRecvStep();

        }
        public void InitializeInterLock()
        {
            Vaccum1.Simulaotr = GG.VacuumSimul;
            Vaccum2.Simulaotr = GG.VacuumSimul;

            X1Axis.CheckStartMoveInterLockFunc = X12Axis_CheckStartMoveInterLockFunc;
            X2Axis.CheckStartMoveInterLockFunc = X12Axis_CheckStartMoveInterLockFunc;
            Y1Axis.CheckStartMoveInterLockFunc = Y12Axis_CheckStartMoveInterLockFunc;
            Y2Axis.CheckStartMoveInterLockFunc = Y12Axis_CheckStartMoveInterLockFunc;


            SetStepSwith( "HomeSeqLogicWorking");
            SetStepSwith( "CellLdLogicWorking");
            SetStepSwith( "CellUdLogicWorking");
        }

        public override void LogicWorking(Equipment equip)
        {
            Y1Axis.LogicWorking(equip);
            Y2Axis.LogicWorking(equip);

            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
            Vaccum1.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            LoaderSeqLogicWorking(equip);

            XYMoveingInterLock(equip);
        }

        //구동 시퀀스 구성.         
        public void LoaderSeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요.         
                IsHomeComplete = false;
                SeqStep = EmCellUnloaderStep.S0000_CELL_LOAD_START;
                HomeStep = EmCellUnloaderHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                //TR이 먼저 HOME 위치 이동이 필요함. 
                if (equip.UD.AfterInspUnloaderTransfer_A.IsHomeComplete == true && equip.UD.AfterInspUnloaderTransfer_B.IsHomeComplete == true)
                {
                    if (GetStepSwith( "HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                }
                return;
            }

            if (equip.IsHomeComplete == false) return;


            if (GetStepSwith( "CellLdLogicWorking")) CellLdLogicWorking(equip);
            if (GetStepSwith( "CellUdLogicWorking")) CellUdLogicWorking(equip);


        }


        //HOME 시퀀스 구성. 
        public EmCellUnloaderHomeStep HomeStep = EmCellUnloaderHomeStep.H1010;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmCellUnloaderHomeStep.H0000)
            {
                //if (equip.LD.LoaderTransfer_A.IsHomeComplete == true && equip.LD.LoaderTransfer_B.IsHomeComplete == true)
                {
                    HomeStep = EmCellUnloaderHomeStep.H1010; ;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1010)
            {
                if (this.IsExitsPanel == true)
                {
                    if (this.IsVaccumOn(PanelSet.CellPosi) == true)
                    {
                        HomeStep = EmCellUnloaderHomeStep.H1020;
                    }
                    else
                    {
                        InterLockMgr.AddInterLock("셀 위치와 버큠이 상이함", "CELL 위치 {0}", PanelSet.CellPosi);
                    }
                }
                else
                {
                    HomeStep = EmCellUnloaderHomeStep.H1020;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1020)
            {
                if (Y1Axis.IsHomeOnPosition() == false)
                {
                    if (Y1Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellUnloaderHomeStep.H1030;
                    }
                }
                else
                {
                    HomeStep = EmCellUnloaderHomeStep.H1030;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1030)
            {
                if (Y1Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellUnloaderHomeStep.H1040;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1040)
            {
                if (Y2Axis.IsHomeOnPosition() == false)
                {
                    if (Y2Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellUnloaderHomeStep.H1050;
                    }
                }
                else
                {
                    HomeStep = EmCellUnloaderHomeStep.H1050;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1050)
            {
                if (Y1Axis.IsHomeOnPosition() == true && Y2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellUnloaderHomeStep.H1060;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1060)
            {
                if (X1Axis.IsHomeOnPosition() == false)
                {
                    if (X1Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellUnloaderHomeStep.H1070;
                    }
                }
                else
                {
                    HomeStep = EmCellUnloaderHomeStep.H1070;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1070)
            {
                if (X1Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellUnloaderHomeStep.H1080;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1080)
            {
                if (X2Axis.IsHomeOnPosition() == false)
                {
                    if (X2Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellUnloaderHomeStep.H1090;
                    }
                }
                else
                {
                    HomeStep = EmCellUnloaderHomeStep.H1090;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1090)
            {
                if (X1Axis.IsHomeOnPosition() == true &&
                    X2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellUnloaderHomeStep.H1110;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1110)
            {
                if (X1Axis.PtpMoveCmd(equip, EmUnLoaderXAxisServo.Cell_A_Loading, equip.CurrentRecipe, null) == true)
                {
                    if (X2Axis.PtpMoveCmd(equip, EmUnLoaderXAxisServo.Cell_B_Loading, equip.CurrentRecipe, null) == true)
                    {
                        HomeStep = EmCellUnloaderHomeStep.H1120;
                    }
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H1120)
            {
                if (X1Axis.IsInPosition(EmUnLoaderXAxisServo.Cell_A_Loading, equip.CurrentRecipe, null) == true &&
                    X2Axis.IsInPosition(EmUnLoaderXAxisServo.Cell_B_Loading, equip.CurrentRecipe, null) == true)
                {
                    HomeStep = EmCellUnloaderHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmCellUnloaderHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true;
            }
        }

        //구동 시퀀스 구성.       
        public EmCellUnloaderStep SeqStep = EmCellUnloaderStep.S0000_CELL_LOAD_START;
        private PanelInfoSet _firstPSet = null;
        private PioRecvStep _recvPio = null;
        public void CellLdLogicWorking(Equipment equip)
        {


            //글라스가 있을경우. 넘어감. 추가 필요. 
            if (SeqStep == EmCellUnloaderStep.S0000_CELL_LOAD_START)
            {
                _firstPSet = GetFirstPanelInfoSet();
                SeqStep = EmCellUnloaderStep.S0010_;
            }
            else if (SeqStep == EmCellUnloaderStep.S0010_)
            {
                if (_firstPSet != null)
                {
                    if (X12AxisPtpMoveCmdSync(equip, EmUnLoaderXAxisServo.Cal_Cell_Load_PnlSet, equip.CurrentRecipe, _firstPSet, EmCellPosi.Two) == true)
                    {
                        _firstPSet = null;
                        SeqStep = EmCellUnloaderStep.S0015_;
                    }
                }
                else
                {
                    SeqStep = EmCellUnloaderStep.S0015_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S0015_)
            {
                if (PioRecvA.XSendAble == true)
                {
                    _recvPio = PioRecvA;
                    SeqStep = EmCellUnloaderStep.S0020_;
                }
                else if (PioRecvB.XSendAble == true)
                {
                    _recvPio = PioRecvB;
                    SeqStep = EmCellUnloaderStep.S0020_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S0020_)
            {
                if (_recvPio.XSendAble == true)
                {
                    if (X12AxisPtpMoveCmdSync(equip, EmUnLoaderXAxisServo.Cal_Cell_Load_PnlSet, equip.CurrentRecipe, _recvPio.RecvPanelInfoSet, EmCellPosi.Two) == true)
                    {
                        _recvPio.YRecvAble = true;
                        _recvPio.YRecvStart = true;

                        SeqStep = EmCellUnloaderStep.S0030_;
                    }
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S0030_)
            {
                if (_recvPio.XSendAble == true && _recvPio.XSendStart == true)
                {
                    _recvPio.YRecvAble = true;
                    _recvPio.YRecvStart = true;

                    SeqStep = EmCellUnloaderStep.S0040_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S0040_)
            {
                if (_recvPio.XSendAble == true && _recvPio.XSendStart == true && _recvPio.XSendComplete == true)
                {
                    this.PanelSet = _recvPio.RecvPanelInfoSet;

                    _recvPio.YRecvAble = true;
                    _recvPio.YRecvStart = true;
                    _recvPio.YRecvComplete = true;
                    SeqStep = EmCellUnloaderStep.S0050_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S0050_)
            {
                if (_recvPio.XSendAble == false && _recvPio.XSendStart == false && _recvPio.XSendComplete == false)
                {
                    _recvPio.YRecvAble = false;
                    _recvPio.YRecvStart = false;
                    _recvPio.YRecvComplete = false;
                    SeqStep = EmCellUnloaderStep.S0060_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S0060_)
            {
                SeqStep = EmCellUnloaderStep.S7000_CELL_UNLOAD_START;

            }
        }
        private PanelInfoSet GetFirstPanelInfoSet()
        {
            return null;
        }
        public void BufferLogicWorking(Equipment equip)
        {
            //if (StepCmd == EmCellUnloaderStep.S1000_INSP_STRART)
            //{
            //    if (YAxis.PtpMoveCmd(equip, EmInspStageYAxisServo.Cell_Insp, equip.CurrentRecipe, this.PanelSet) == true)
            //    {
            //        InspectionCamere = equip.GetUnit(this.UnitPosi, EmUnitType.InspCamera) as InspCameraUnit;
            //        InspectionCamere.InspCameraReady(this.UnitPosi, this.PanelSet);
            //        StepCmd = EmCellUnloaderStep.S01020_;
            //    }
            //}

            //else if (StepCmd == EmCellUnloaderStep.S01020_)
            //{
            //    if (YAxis.IsInPosition(EmInspStageYAxisServo.Cell_Insp, equip.CurrentRecipe, this.PanelSet) == true)
            //    {
            //        InspectionCamere.InspCameraStart(this.UnitPosi, this.PanelSet);
            //        StepCmd = EmCellUnloaderStep.S01030_;
            //    }
            //}
            //else if (StepCmd == EmCellUnloaderStep.S01030_)
            //{
            //    if (InspectionCamere.IsInspCameraComplete(this.UnitPosi, this.PanelSet) == true)
            //    {
            //        StepCmd = EmCellUnloaderStep.S01040_;
            //    }
            //}
            //else if (StepCmd == EmCellUnloaderStep.S01040_)
            //{
            //    StepCmd = EmCellUnloaderStep.S02010_CELL_UNLOAD_START;
            //}
        }
        public void CellUdLogicWorking(Equipment equip)
        {
            PioSendStep sendPio = true ? PioSendA : PioSendB;


            if (SeqStep == EmCellUnloaderStep.S7000_CELL_UNLOAD_START)
            {
                if (X12AxisPtpMoveCmdSync(equip, EmUnLoaderXAxisServo.Cal_Cell_Unload_PnlSet, equip.CurrentRecipe, this.PanelSet, EmCellPosi.Two) == true)
                {
                    if (GG.ExceptCstLoaderUnloader == true)
                    {
                        SeqStep = EmCellUnloaderStep.S7050_;
                    }
                    else
                    {
                        SeqStep = EmCellUnloaderStep.S7010_;
                    }
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S7010_)
            {
                sendPio.SendPanelInfoSet = this.PanelSet;
                sendPio.YSendAble = true;
                SeqStep = EmCellUnloaderStep.S7020_;
            }
            else if (SeqStep == EmCellUnloaderStep.S7020_)
            {
                if (sendPio.XRecvAble == true && sendPio.XRecvStart == true)
                {
                    sendPio.YSendAble = true;
                    sendPio.YSendStart = true;

                    SeqStep = EmCellUnloaderStep.S7030_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S7030_)
            {
                if (sendPio.XRecvAble == true && sendPio.XRecvStart == true && sendPio.XRecvComplete == true)
                {
                    sendPio.YSendAble = true;
                    sendPio.YSendStart = true;
                    sendPio.YSendComplete = true;

                    SeqStep = EmCellUnloaderStep.S7040_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S7040_)
            {
                if (sendPio.XRecvAble == false && sendPio.XRecvStart == false && sendPio.XRecvComplete == false)
                {
                    this.PanelSet.ClearPanel();

                    sendPio.YSendAble = false;
                    sendPio.YSendStart = false;
                    sendPio.YSendComplete = false;

                    SeqStep = EmCellUnloaderStep.S7050_;
                }
            }
            else if (SeqStep == EmCellUnloaderStep.S7050_)
            {
                SeqStep = EmCellUnloaderStep.S0000_CELL_LOAD_START;
            }
        }


        //각 장치별 인터락
        public bool X12Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}Unloader의 X1Axis 이동 불가, 배큠이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}Unloader의 X1Axis 이동 불가, 배큠이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }



            if (equip.UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  X1Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_A의 UpDown1Cylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }
            if (equip.UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  X1Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_A의 UpDown2Cylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }

            if (equip.UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  X1Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_B의 UpDown1Cylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }
            if (equip.UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  X1Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_B의 UpDown2Cylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }
        public bool Y12Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (IsExitsPanel1 == true && (Vaccum1.IsOnOff != true && Vaccum2.IsOnOff != true))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}Unloader의 X1Axis 이동 불가, 배큠이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }

            if (IsExitsPanel2 == true && (Vaccum1.IsOnOff != true && Vaccum2.IsOnOff != true))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Unloader의 Y2Axis 이동 불가, 배큠이 ON되어 있지 않습니다.");
                return true;
            }


            if (equip.UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  Y1Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_A의 UpDown1Cylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }
            if (equip.UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  Y1Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_A의 UpDownCylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }

            if (equip.UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  Y2Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_B의 UpDown1Cylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }
            if (equip.UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.IsBackward != true &&
                equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock(string.Format("{0}UnloaderUnit  Y2Axis 전/후진 불가, {0}AfterInspUnloaderTransfer_B의 UpDown2Cylinder가  UP 위치가 아닙니다.", UnitPosi));
                return true;
            }


            return false;
        }

        public void XYMoveingInterLock(Equipment equip)
        {
            if (X1Axis.XF_CurrMotorPosition + X2Axis.XF_CurrMotorPosition + 50 > X1Axis.SoftPlusLimit)
            {
                if (X1Axis.PtpMoveCmdStep != 0)
                    X1Axis.MoveStop(equip);

                if (X2Axis.PtpMoveCmdStep != 0)
                    X2Axis.MoveStop(equip);
            }
        }


        //메소드 - 축 이동 및 위치 확인 
        public bool Y12AxisPtpMoveCmdSync(Equipment equip, EmUnLoaderYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Y1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Y2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {

                bool y1Reulst = Y1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                bool y2Reulst = Y2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);

                return y1Reulst && y2Reulst;
            }
            else
                return false;
        }
        public bool Y12AxisIsInPosition(EmUnLoaderYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Y1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Y2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Y1Axis.IsInPosition(posiNo, recp, opt) && Y2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }

        public bool X12AxisPtpMoveCmdSync(Equipment equip, EmUnLoaderXAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                ServoPosiInfo info = X1Axis.GetCalPosition(posiNo, equip.CurrentRecipe, opt);

                if (X1Axis.XF_CurrMotorPosition - info.Position > 0)
                {
                    return X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt) && X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                }
                else
                {
                    return X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt) && X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                }
            }
            else
                return false;
        }
        public bool X12AxisIsInPosition(EmUnLoaderXAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return X1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return X2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return X1Axis.IsInPosition(posiNo, recp, opt) && X2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }

        public bool VaccumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vaccum1.OnOff(equip, onOff);
                if (onOff == false) Blower1.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vaccum2.OnOff(equip, onOff);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vaccum1.OnOff(equip, onOff);
                Vaccum2.OnOff(equip, onOff);

                if (onOff == false)
                {
                    Blower1.OnOff(equip, true, 500);
                    Blower2.OnOff(equip, true, 500);
                }
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.None)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == false && Vaccum2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }

    }
}
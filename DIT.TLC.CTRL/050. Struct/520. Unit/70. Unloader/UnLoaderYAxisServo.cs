﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmUnLoaderYAxisServo
    {
        Cst_A_Inner          /**/= 0,
        Cst_B_Inner          /**/= 1,
        Cst_Middle           /**/= 2,

        Cell_A_Loading       /**/= 3,
        Cell_B_Loading       /**/= 4,

        //계산된 위치         
        Cal_Cell_UnLoading            /**/= 5,
        Cal_Cell_Loading_PnlSet       /**/= 7,
    }
    public class UnLoaderYAxisServo : ServoMotorControl
    {
        public UnLoaderYAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 500;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmUnLoaderYAxisServo.Cal_Cell_UnLoading == posiNo)
            {
                PanelInfoSet pSet = opt as PanelInfoSet;
                if (pSet == null)
                {
                    InterLockMgr.AddInterLock("Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다 ");
                    return null;
                }

                if (pSet.LDPort.UnitPosi == EmUnitPosi.None)
                {
                    InterLockMgr.AddInterLock("Loader X축 이동 불가", "{0}, Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다", pSet.LDPort.UnitPosi);
                    return null;
                }

                int cstAInnerNo = (int)(pSet.LDPort.UnitPosi == EmUnitPosi.ACol ? EmUnLoaderYAxisServo.Cst_A_Inner : EmUnLoaderYAxisServo.Cst_B_Inner);
                ServoPosiInfo cstAInnerNoPosi = base.GetCalPosition(cstAInnerNo, recp, opt);

                return cstAInnerNoPosi;
            }
            else if ((int)EmUnLoaderYAxisServo.Cal_Cell_Loading_PnlSet == posiNo)
            {
                PortInfo LdPort = null;

                PanelInfoSet pSet = opt as PanelInfoSet;
                LdPort = pSet.LDPort;


                int cellLoadingNo = (int)(LdPort.UnitPosi == EmUnitPosi.ACol ? EmUnLoaderYAxisServo.Cell_A_Loading : EmUnLoaderYAxisServo.Cell_B_Loading);
                ServoPosiInfo cellUnloadingPosi = base.GetCalPosition(cellLoadingNo, recp, opt);

                return cellUnloadingPosi;
            }
            else
                return base.GetCalPosition(posiNo, recp, opt);
        }
        public bool IsInPosition(EmUnLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmUnLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmUnLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmUnLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmUnLoaderXAxisServo
    {
        Cst_A_Middle          /**/= 0,
        Cst_A_Wait            /**/= 1,
        Cst_B_Middle          /**/= 2,
        Cst_B_Wait            /**/= 3,

        Cell_A_Loading        /**/= 4,
        Cell_B_Loading        /**/= 5,

        //계산된 위치..
        Cal_Cell_Unload_PnlSet    /**/= 6,
        Cal_Cell_Load_PnlSet    /**/= 7,

        Cal_Unloader_Outside    /**/= 7, //회피 위치.

    }
    public class UnLoaderXAxisServo : ServoMotorControl
    {
        public UnLoaderXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {
            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 850;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmUnLoaderXAxisServo.Cal_Cell_Load_PnlSet == posiNo)
            {
                PanelInfoSet pSet = opt as PanelInfoSet;
                if (pSet == null)
                {
                    InterLockMgr.AddInterLock("UnLoaderXAxisServo, Cal_Cell_Load_PnlSet 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다 ");
                    return null;
                }

                int unloadNo = (int)(pSet.FlowUnitPosi == EmUnitPosi.ACol ? EmUnLoaderXAxisServo.Cell_A_Loading
                    : EmUnLoaderXAxisServo.Cell_B_Loading);
                ServoPosiInfo cellLoadingPosi = base.GetCalPosition(unloadNo, recp, opt);

                return cellLoadingPosi;
            }
            else if ((int)EmUnLoaderXAxisServo.Cal_Cell_Unload_PnlSet == posiNo)
            {
                PanelInfoSet pSet = opt as PanelInfoSet;
                if (pSet == null)
                {
                    InterLockMgr.AddInterLock("Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다 ");
                    return null;
                }

                if (pSet.ULDPort.UnitPosi == EmUnitPosi.None)
                {
                    InterLockMgr.AddInterLock("Loader X축 이동 불가", "{0}, Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다", pSet.ULDPort.UnitPosi);
                    return null;
                }


                int unloadNo = (int)(pSet.ULDPort.UnitPosi == EmUnitPosi.ACol ? EmLoaderXAxisServo.Cst_A_Middle : EmLoaderXAxisServo.Cst_B_Middle);  //계산이 필요함. 
                ServoPosiInfo cellUnloadingPosi = base.GetCalPosition(unloadNo, recp, opt);

                if (AxisPosi == EmAxisPosi.Aixs1)
                {
                    int slotCol = pSet.ULDPort.Cst.GetCellOutColPosi(this.AxisPosi, recp);
                    cellUnloadingPosi.Position -= (int)(recp.CassetteWidth * (slotCol + 1));
                }
                else
                {
                    int slotCol = pSet.ULDPort.Cst.GetCellOutColPosi(this.AxisPosi, recp) - recp.GetCstMaxCol / 2;
                    cellUnloadingPosi.Position -= (int)(recp.CassetteWidth * (slotCol + 1));
                }

                return cellUnloadingPosi;
            }
            else
                return base.GetCalPosition(posiNo, recp, opt);
        }
        public bool IsInPosition(EmUnLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmUnLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmUnLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmUnLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}






























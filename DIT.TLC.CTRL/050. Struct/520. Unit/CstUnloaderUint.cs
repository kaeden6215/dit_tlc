﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstUnloaderUint : BaseUnit
    {
        public CassetteInfo InCst { get; set; }
        public CassetteInfo OutEmptyCst { get; set; }

        public Cylinder TiltCylinder = new Cylinder();

        public Sensor ULDCasstteDetect_1                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_2                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_3                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_4                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_5                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_6                                                  /**/    = new Sensor();

        public CylinderTwo CasstteGripCylinder                                            /**/    = new CylinderTwo();

        public CstLoaderRotationUpAxisServo CstRotationAxis { get; set; }
        public CstLoaderRotationDownAxisServo CstUpDownAxis { get; set; }

        //MUTING
        public Switch MutingSwitch                                                      /**/    = new Switch();
        public Sensor LiftMutingSwitch1                                                 /**/    = new Sensor();
        public Sensor LiftMutingSwitch2                                                 /**/    = new Sensor();
        
        public Switch ResetSwitch                                                       /**/    = new Switch();

        public SwitchOneWay  Buzzer                                                     /**/     = new SwitchOneWay();

        public SwitchOneWay MutingLamp                                                  /**/   = new SwitchOneWay();
        //LIFT
        //TILT 센서
        public Switch Tilt { get; set; }
        public override void LogicWorking(Equipment equip)
        {
            TiltCylinder.LogicWorking(equip);
            ULDCasstteDetect_1.LogicWorking(equip);
            ULDCasstteDetect_2.LogicWorking(equip);
            ULDCasstteDetect_3.LogicWorking(equip);
            ULDCasstteDetect_4.LogicWorking(equip);
            ULDCasstteDetect_5.LogicWorking(equip);
            ULDCasstteDetect_6.LogicWorking(equip);
            CasstteGripCylinder.LogicWorking(equip);
            

            CstRotationAxis.LogicWorking(equip);
            CstUpDownAxis.LogicWorking(equip);

            MutingSwitch.LogicWorking(equip);
            LiftMutingSwitch1.LogicWorking(equip);
            LiftMutingSwitch2.LogicWorking(equip);

            ResetSwitch.LogicWorking(equip);
            Buzzer.LogicWorking(equip);

            MutingLamp.LogicWorking(equip);
        }

        internal bool IsSlotPosiont(Equipment equip)
        {
            throw new NotImplementedException();
        }

        internal bool CstSlotDown(Equipment equip)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CameraUnit : BaseUnit
    {
        public CameraXAxisServo X1Axis { get; set; }
        public CameraZAxisServo Z1Axis { get; set; }        
        
        public override void LogicWorking(Equipment equip)
        {
            X1Axis.LogicWorking(equip);
            Z1Axis.LogicWorking(equip);
        }
    }
}

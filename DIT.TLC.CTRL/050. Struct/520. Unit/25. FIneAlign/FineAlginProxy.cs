﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class FineAlginProxy : BaseUnit
    {
        public enum EmFineAlignStep
        {
            S1000_FINE_ALIGN_WAIT,
            S1000_FINE_ALIGN_START,
            S1010_,
            S1020_,
            S1030_,
            S1040_,
            S1050_,
            S1060_,
            S1070_,
            S1005_,
            S1999_ALIGN_COMPLETE,
            S0000_FIN_MOVE_XAXIS,
        }
        public enum EmFineAlignHomeStep
        {
            H0000,
            H1010,
            H1020,
            H5999_HOME_COMPLETE,
        }

        public FineAlignXAxisServo XAxis { get; set; }
        public EmFineAlignStep SeqStep = EmFineAlignStep.S1000_FINE_ALIGN_WAIT;

        private bool _isAlignComplete = false;
        private EmUnitPosi AlignUnitPosi = EmUnitPosi.None;

        public void InitializeInterLock()
        {
            XAxis.CheckStartMoveInterLockFunc = XAxis_CheckStartMoveInterLockFunc;

            SetStepSwith("HomeSeqLogicWorking");
            SetStepSwith("FineAlignLogicWorking");
        }
        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;            
            SeqLogicWorking(equip);
        }
        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요.         
                IsHomeComplete = false;
                HomeStep = EmFineAlignHomeStep.H0000;
                SeqStep = EmFineAlignStep.S1000_FINE_ALIGN_WAIT;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (GetStepSwith("HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                return;
            }

            if (equip.IsHomeComplete == false) return;
            if (GetStepSwith("FineAlignLogicWorking")) FineAlignLogicWorking(equip);
        }


        //HOME 시퀀스 구성. 
        public EmFineAlignHomeStep HomeStep = EmFineAlignHomeStep.H0000;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmFineAlignHomeStep.H0000)
            {
                HomeStep = EmFineAlignHomeStep.H1010;
            }
            else if (HomeStep == EmFineAlignHomeStep.H1010)
            {
                if (XAxis.GoHome(equip) == true)
                {
                    HomeStep = EmFineAlignHomeStep.H1020;
                }
            }
            else if (HomeStep == EmFineAlignHomeStep.H1020)
            {
                if (XAxis.IsHomeOnPosition())
                {
                    HomeStep = EmFineAlignHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmFineAlignHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true;
            }
        }

        private int _alignTryCount = 0;
        public PanelInfoSet AlignPnlSet = null;
        private AlignResult _pnl1Result = new AlignResult();
        private AlignResult _pnl2Result = new AlignResult();
        private PlcTimerEx plcTmrAlignWait = new PlcTimerEx("FIN ALING 대가 타이머");
        private int _msgInx = 0;

        public bool MoveAlignXAxis(PanelInfoSet pSet, EmUnitPosi unitPosi)
        {
            if (SeqStep == EmFineAlignStep.S1000_FINE_ALIGN_WAIT)
            {
                AlignPnlSet = pSet;
                AlignUnitPosi = unitPosi;

                _isAlignComplete = false;
                SeqStep = EmFineAlignStep.S0000_FIN_MOVE_XAXIS;
                return true;
            }
            else
                return false;
        }
        private void FineAlignLogicWorking(Equipment equip)
        {
            if (SeqStep == EmFineAlignStep.S0000_FIN_MOVE_XAXIS)
            {
                if (AlignUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmFineAlignXAxisServo.FineAlign_A_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmFineAlignStep.S1000_FINE_ALIGN_WAIT;
                }
                else if (AlignUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmFineAlignXAxisServo.FineAlign_B_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmFineAlignStep.S1000_FINE_ALIGN_WAIT;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmFineAlignStep.S1000_FINE_ALIGN_WAIT)
            {
                _alignTryCount = 0;
            }
            else if (SeqStep == EmFineAlignStep.S1000_FINE_ALIGN_START)
            {
                _alignTryCount++;
                SeqStep = EmFineAlignStep.S1005_;
            }
            else if (SeqStep == EmFineAlignStep.S1005_)
            {
                if (AlignUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmFineAlignXAxisServo.FineAlign_A_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmFineAlignStep.S1010_;
                }
                else if (AlignUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmFineAlignXAxisServo.FineAlign_B_1st, equip.CurrentRecipe, null) == true)
                        SeqStep = EmFineAlignStep.S1010_;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmFineAlignStep.S1010_)
            {
                LaserCuttingStageUnit irState = equip.GetUnit(AlignUnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;

                //1번 얼라인 마트.
                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.FineAlign, EmCameraIndex.Cam1st, EmAlignIndex.Grap1St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.FineAlign, EmCameraIndex.Cam2nd, EmAlignIndex.Grap1St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                SeqStep = EmFineAlignStep.S1020_;
            }
            else if (SeqStep == EmFineAlignStep.S1020_)
            {
                SeqStep = EmFineAlignStep.S1030_;
            }
            else if (SeqStep == EmFineAlignStep.S1030_)
            {
                if (AlignUnitPosi == EmUnitPosi.ACol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmFineAlignXAxisServo.Cal_FineAlign_A_2nd, equip.CurrentRecipe, null) == true)
                        SeqStep = EmFineAlignStep.S1040_;
                }
                else if (AlignUnitPosi == EmUnitPosi.BCol)
                {
                    if (XAxis.PtpMoveCmdSync(equip, EmFineAlignXAxisServo.Cal_FineAlign_B_2nd, equip.CurrentRecipe, null) == true)
                        SeqStep = EmFineAlignStep.S1040_;
                }
                else
                {
                    //알람 처리. 
                }
            }
            else if (SeqStep == EmFineAlignStep.S1040_)
            {
                LaserCuttingStageUnit irState = equip.GetUnit(AlignUnitPosi, EmUnitType.LaserCuttingStage) as LaserCuttingStageUnit;

                //1번 얼라인 마트.
                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.FineAlign, EmCameraIndex.Cam1st, EmAlignIndex.Grap2St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                equip.AlignPc.SendAlignStart(0, equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.FineAlign, EmCameraIndex.Cam2nd, EmAlignIndex.Grap2St,
                    XAxis.XF_CurrMotorPosition, irState.YAxis.XF_CurrMotorPosition, 1);

                plcTmrAlignWait.Start(10);   //설정치 추가 필요

                SeqStep = EmFineAlignStep.S1050_;
            }
            else if (SeqStep == EmFineAlignStep.S1050_)
            {
                if (equip.AlignPc.IsAlignComplete(equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.FineAlign, EmCameraIndex.Cam1st, EmAlignIndex.Grap2St, out _pnl1Result) == true &&
                  equip.AlignPc.IsAlignComplete(equip.CurrentRecipe.CellType, equip.CurrentRecipe.Name, EmAlignType.FineAlign, EmCameraIndex.Cam2nd, EmAlignIndex.Grap2St, out _pnl2Result) == true)
                {
                    plcTmrAlignWait.Stop();
                    SeqStep = EmFineAlignStep.S1060_;
                }

                if (plcTmrAlignWait)
                {

                    plcTmrAlignWait.Stop();
                    //알람 처리.
                }
            }
            else if (SeqStep == EmFineAlignStep.S1060_)
            {
                if (_pnl1Result.Result == EmAlignResult.Success && _pnl2Result.Result == EmAlignResult.Success) //결과 확인 
                {
                    SeqStep = EmFineAlignStep.S1070_;
                }
                else
                {
                    if (_alignTryCount < 3)
                    {
                        SeqStep = EmFineAlignStep.S1000_FINE_ALIGN_START;
                    }
                    else
                    {
                        //알람 처리.
                    }
                }
            }
            else if (SeqStep == EmFineAlignStep.S1070_)
            {
                _isAlignComplete = true;
                SeqStep = EmFineAlignStep.S1999_ALIGN_COMPLETE;
            }
            else if (SeqStep == EmFineAlignStep.S1999_ALIGN_COMPLETE)
            {

            }
        }

        //각 장치별 인터락
        public bool XAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            return false;
        }

        public bool IsAlignReady(EmUnitPosi unitPosi)
        {   
            if (SeqStep == EmFineAlignStep.S1000_FINE_ALIGN_WAIT)
                return true;
            else
                return false;
        }
        public bool StartAlign(PanelInfoSet pSet, EmUnitPosi unitPosi)
        {   
            if (SeqStep == EmFineAlignStep.S1000_FINE_ALIGN_WAIT)
            {
                AlignPnlSet = pSet;
                AlignUnitPosi = unitPosi;

                _isAlignComplete = false;
                SeqStep = EmFineAlignStep.S1000_FINE_ALIGN_START;
                return true;
            }
            else
                return false;
        }
        public bool IsAlignComplete(PanelInfoSet info, EmUnitPosi unitPosi, out AlignResult pnl1Result, out AlignResult pnl2Result)
        {
            pnl1Result = new AlignResult();
            pnl2Result = new AlignResult();
            
            if (SeqStep == EmFineAlignStep.S1999_ALIGN_COMPLETE && _isAlignComplete == true)
            {
                pnl1Result = _pnl1Result;
                pnl2Result = _pnl2Result;

                return true;
            }
            else
            {
                pnl1Result = null;
                pnl2Result = null;
                return false;
            }
        }
        public void ResetFineAlign()
        {

            SeqStep = EmFineAlignStep.S1000_FINE_ALIGN_WAIT;

        }
    }
}

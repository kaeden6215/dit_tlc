﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmFineAlignXAxisServo
    {
        FineAlign_A_1st/**/ = 0,
        FineAlign_B_1st/**/ = 1,

        //께산된 위치. 
        Cal_FineAlign_A_2nd /**/= 2,
        Cal_FineAlign_B_2nd /**/= 3,
    }

    public class FineAlignXAxisServo : ServoMotorControl
    {
        public FineAlignXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {  

            SoftMinusLimit = 0;
            SoftPlusLimit = 1000;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30; 
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmFineAlignXAxisServo.Cal_FineAlign_A_2nd == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmFineAlignXAxisServo.FineAlign_A_1st].Clone();
                info.Position += 50;

                return info;
            }
            else if ((int)EmFineAlignXAxisServo.Cal_FineAlign_B_2nd == posiNo)
            {
                ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[(int)EmFineAlignXAxisServo.FineAlign_B_1st].Clone();
                info.Position += 50;

                return info;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt); ;
            }
        }
        public bool IsInPosition(EmFineAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmFineAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmFineAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmFineAlignXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

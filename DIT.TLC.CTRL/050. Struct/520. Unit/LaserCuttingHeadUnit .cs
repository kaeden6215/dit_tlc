﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public class LaserCuttingHeadUnit : BaseUnit
    {
        public Cylinder UnDownCylinder1             /**/= new Cylinder();
        public Cylinder UnDownCylinder2             /**/= new Cylinder();
        public Cylinder LaserShutterCylinder        /**/= new Cylinder();
        public Cylinder DummyShutterCylinder1       /**/= new Cylinder();
        public Cylinder DummyShutterCylinder2       /**/= new Cylinder();

        public Cylinder DummyTankCylinder           /**/= new Cylinder();

        public PlcAddr LaserInterferometerIRValue { get; set; }
        public PlcAddr LaserInterferometerBKValue { get; set; }

        public SwitchOneWay LaserInterferometerReset1 = new SwitchOneWay();
        public SwitchOneWay LaserInterferometerReset2 = new SwitchOneWay();

        public LaserCuttingXAxisServo XAxis { get; set; }
        public LaserCuttingZAxisServo ZAxis { get; set; }

        public override void LogicWorking(Equipment equip)
        {
            UnDownCylinder1.LogicWorking(equip);
            UnDownCylinder2.LogicWorking(equip);
            LaserShutterCylinder.LogicWorking(equip);
            DummyShutterCylinder1.LogicWorking(equip);
            DummyShutterCylinder2.LogicWorking(equip);

            DummyTankCylinder.LogicWorking(equip);
            LaserInterferometerReset1.LogicWorking(equip);
            LaserInterferometerReset2.LogicWorking(equip);
            XAxis.LogicWorking(equip);
            ZAxis.LogicWorking(equip);
        }

    }
}

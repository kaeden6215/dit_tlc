﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._520._Unit
{
    public class CellBufferUnit : BaseUnit
    {
        public Cylinder BufferCylinder = new Cylinder();
        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();

        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();

        public PlcAddr HandPressureValue1 { get; internal set; }

        public void InitializeInterLock()
        {
            BufferCylinder.CheckStartMoveInterLockFunc = BufferCylinder_CheckStartMoveInterLockFunc;
        }

        public override void LogicWorking(Equipment equip)
        {
            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);

            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;
            IsHomeComplete = true;
            return;

        }
        
        public bool BufferCylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect idirect)
        {   
            return false;
        }
    }
}

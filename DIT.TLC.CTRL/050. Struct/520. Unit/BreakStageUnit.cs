﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public class BreakStageUnit : BaseUnit
    {
        public BreakUnitYAxisServo YAxis { get; set; }

        public BreakUnitXAxisServo SubX1Axis { get; set; }
        public BreakUnitXAxisServo SubX2Axis { get; set; }

        public BreakUnitYAxisServo SubY1Axis { get; set; }
        public BreakUnitYAxisServo SubY2Axis { get; set; }

        public BreakUnitTAxisServo SubT1Axis { get; set; }
        public BreakUnitTAxisServo SubT2Axis { get; set; }

        public PlcAddr Vaccum1Value { get; set; }
        public PlcAddr Vaccum2Value { get; set; }

        public Switch Vaccum1                              /**/= new Switch();
        public Switch Vaccum2                              /**/= new Switch();
        public SwitchOneWay Blower1                        /**/ = new SwitchOneWay();
        public SwitchOneWay Blower2                        /**/ = new SwitchOneWay();
        public Cylinder BrushUnDownCylinder                /**/= new Cylinder();

        public override void LogicWorking(Equipment equip)
        {
            YAxis.LogicWorking(equip);
            SubX1Axis.LogicWorking(equip);
            SubX2Axis.LogicWorking(equip);
            SubY1Axis.LogicWorking(equip);
            SubY2Axis.LogicWorking(equip);
            SubT1Axis.LogicWorking(equip);
            SubT2Axis.LogicWorking(equip);

            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            BrushUnDownCylinder.LogicWorking(equip);
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public enum EmLoaderYAxisServo
    {
        Cst_A_Inner             /**/= 0,
        Cst_B_Inner             /**/= 1,
        Cst_Middle              /**/= 2,

        Cell_A_Unloading        /**/= 3,
        Cell_B_Unloading        /**/= 4,

        //계산된 위치         
        Cal_Cell_Loading                /**/= 5,
        Cal_Cell_Unloading_PnlSet       /**/= 7,
    }

    public class LoaderYAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(GG.StartupPath, "Setting", "ScanXServo.ini");

        public LoaderYAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 500;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmLoaderYAxisServo.Cal_Cell_Loading == posiNo)
            {
                PanelInfoSet pSet = opt as PanelInfoSet;
                if (pSet == null)
                {
                    InterLockMgr.AddInterLock("Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다 ");
                    return null;
                }

                if (pSet.LDPort.UnitPosi == EmUnitPosi.None)
                {
                    InterLockMgr.AddInterLock("Loader X축 이동 불가", "{0}, Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다", pSet.LDPort.UnitPosi);
                    return null;
                }

                int cstAInnerNo = (int)(pSet.LDPort.UnitPosi == EmUnitPosi.ACol ? EmLoaderYAxisServo.Cst_A_Inner : EmLoaderYAxisServo.Cst_B_Inner);
                ServoPosiInfo cstAInnerNoPosi = base.GetCalPosition(cstAInnerNo, recp, opt);

                return cstAInnerNoPosi;
            }
            else if ((int)EmLoaderYAxisServo.Cal_Cell_Unloading_PnlSet == posiNo)
            {
                PortInfo LdPort = null;

                if (opt is PanelInfoSet)
                {
                    PanelInfoSet pSet = opt as PanelInfoSet;
                    LdPort = pSet.LDPort;
                }
                else if (opt is PortInfo)
                {
                    LdPort = opt as PortInfo;
                }

                int cellUnloadingNo = (int)(LdPort.UnitPosi == EmUnitPosi.ACol ? EmLoaderYAxisServo.Cell_A_Unloading : EmLoaderYAxisServo.Cell_B_Unloading);
                ServoPosiInfo cellUnloadingPosi = base.GetCalPosition(cellUnloadingNo, recp, opt);

                return cellUnloadingPosi;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }
        public bool IsInPosition(EmLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmLoaderYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }

    }
}



















﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmLoaderUnitCmd
    {
        S0010_LOADING_START,
        S0020_,
        S1030_,
        S1060_,
        S2010_UD_START,
        S2060_,
        S2070_,
        S2090_,
        S0015_,
        S0030_,
        S0510_,
        S1050_,
        S2020,
        S1070_,
        S1080_,
        S0500_SIMUL_LOADING,
        S0040_,
        S0520_,
        S0530_,
        S2065_,
        S0512_,
        S0515_
    }

    public enum EmLoaderStep
    {
        LD_0000_WAIT,
    }
    public class LoaderUnit : BaseUnit
    {
        public enum EmCellLoaderStep
        {
            S0000_CELL_WAIT
        }
        public enum EmCellLoaderHomeStep
        {
            H0000,
            H1010,
            H1020,
            H1040,
            H1050,
            H1060,
            H1070,
            H5999_HOME_COMPLETE,
            H1005,
            H1030,
            H1080,
            H1090,
            H1110,
            H1120,
        }

        public LoaderYAxisServo Y1Axis { get; set; }
        public LoaderYAxisServo Y2Axis { get; set; }

        public LoaderXAxisServo X1Axis { get; set; }
        public LoaderXAxisServo X2Axis { get; set; }

        public EmLoaderUnitCmd SeqStep { get; set; }

        public Switch2Cmd1Sensor Vaccum1    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1         /**/= new SwitchOneWay();

        public Switch2Cmd1Sensor Vaccum2    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower2         /**/= new SwitchOneWay();


        public PioSendStep PioSendA { get; set; }
        public PioRecvStep PioRecvA { get; set; }

        public PioSendStep PioSendB { get; set; }
        public PioRecvStep PioRecvB { get; set; }

        public PlcAddr HandPressureValue1 { get; set; }
        public PlcAddr HandPressureValue2 { get; set; }

        public PlcTimer _tmrPlcSimul = new PlcTimer("Simul");

        public LoaderUnit()
        {
            SeqStep = EmLoaderUnitCmd.S0010_LOADING_START;
            PioSendA = new PioSendStep() { Name = "A" };
            PioSendB = new PioSendStep() { Name = "B" };

            PioRecvA = new PioRecvStep() { Name = "A" };
            PioRecvB = new PioRecvStep() { Name = "B" };

        }
        public void InitializeInterLock()
        {
            Vaccum1.Simulaotr = GG.VacuumSimul;
            Vaccum2.Simulaotr = GG.VacuumSimul;

            Y1Axis.CheckStartMoveInterLockFunc = Y12Axis_CheckStartMoveInterLockFunc;
            Y2Axis.CheckStartMoveInterLockFunc = Y12Axis_CheckStartMoveInterLockFunc;
            X1Axis.CheckStartMoveInterLockFunc = X12Axis_CheckStartMoveInterLockFunc;
            X2Axis.CheckStartMoveInterLockFunc = X12Axis_CheckStartMoveInterLockFunc;

            SetStepSwith("LoaderHomeSeqLogicWorking");
            SetStepSwith("LoaderCellLdLogicWorking");
            SetStepSwith("LoaderCellUdLogicWorking");
        }
        public void StartActionCmd(EmLoaderUnitCmd cmd)
        {
        }
        public override void LogicWorking(Equipment equip)
        {

            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            base.LogicWorking(equip);

            Y1Axis.LogicWorking(equip);
            Y2Axis.LogicWorking(equip);

            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
            Vaccum1.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            LoaderSeqLogicWorking(equip);
            XYMoveingInterLock(equip);
        }

        //구동 시퀀스 구성.         
        public void LoaderSeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요.         
                IsHomeComplete = false;
                HomeStep = EmCellLoaderHomeStep.H0000;
                SeqStep = EmLoaderUnitCmd.S0010_LOADING_START;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                //TR이 먼저 HOME 위치 이동이 필요함. 
                if (equip.LD.LoaderTransfer_A.IsHomeComplete == true && equip.LD.LoaderTransfer_B.IsHomeComplete == true)
                {
                    if (GetStepSwith("LoaderHomeSeqLogicWorking"))        /**/LoaderHomeSeqLogicWorking(equip);
                }
                return;
            }

            if (equip.IsHomeComplete == false) return;


            if (GetStepSwith("LoaderCellLdLogicWorking"))               /**/LoaderCellLdLogicWorking(equip);
            if (GetStepSwith("LoaderCellUdLogicWorking"))               /**/LoaderCellUdLogicWorking(equip);
        }
        private int _simulatorPanelNo = 0;
        public void LoaderCellLdLogicWorking(Equipment equip)
        {

            PortInfo port = equip.LD.CstLoader_A.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_A.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? equip.LD.CstLoader_A.Port :
                            equip.LD.CstLoader_B.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_B.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? equip.LD.CstLoader_B.Port :
                            null;

            PioRecvStep cstLoaderPioRecv = equip.LD.CstLoader_A.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_A.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? PioRecvA :
                                           equip.LD.CstLoader_B.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_B.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? PioRecvB :
                                           null;

            if (GG.ExceptCstLoaderUnloader == true)
            {
                port = equip.LD.CstLoader_A.Port;
                cstLoaderPioRecv = PioRecvA;
            }

            if (SeqStep == EmLoaderUnitCmd.S0010_LOADING_START)
            {
                if (GG.ExceptCstLoaderUnloader == true)
                {
                    SeqStep = EmLoaderUnitCmd.S0500_SIMUL_LOADING;

                }
                else
                {
                    if (equip.LD.CstLoader_A.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_A.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START &&
                        equip.LD.CstLoader_B.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_B.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START)
                    {
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_5003_);
                        return;
                    }

                    if (port != null && cstLoaderPioRecv != null)
                    {
                        SeqStep = EmLoaderUnitCmd.S0020_;
                    }
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S0020_)
            {
                SeqStep = EmLoaderUnitCmd.S0030_;
            }
            else if (SeqStep == EmLoaderUnitCmd.S0030_)
            {
                if (X12AxisPtpMoveCmdSync(equip, EmLoaderXAxisServo.Cal_Cell_Loading_Port, equip.CurrentRecipe, port, EmCellPosi.Two) == true)
                {
                    cstLoaderPioRecv.YRecvAble = true;
                    cstLoaderPioRecv.YRecvStart = true;
                    SeqStep = EmLoaderUnitCmd.S0040_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S0040_)
            {
                if (cstLoaderPioRecv.XSendAble == true && cstLoaderPioRecv.XSendStart == true && cstLoaderPioRecv.XSendComplete == true)
                {
                    this.PanelSet = cstLoaderPioRecv.RecvPanelInfoSet;

                    cstLoaderPioRecv.YRecvAble = true;
                    cstLoaderPioRecv.YRecvStart = true;
                    cstLoaderPioRecv.YRecvComplete = true;

                    SeqStep = EmLoaderUnitCmd.S1050_;

                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S1050_)
            {
                if (cstLoaderPioRecv.XSendAble == false && cstLoaderPioRecv.XSendStart == false && cstLoaderPioRecv.XSendComplete == false)
                {
                    cstLoaderPioRecv.YRecvAble = false;
                    cstLoaderPioRecv.YRecvStart = false;
                    cstLoaderPioRecv.YRecvComplete = false;

                    SeqStep = EmLoaderUnitCmd.S1060_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S1060_)
            {
                if (cstLoaderPioRecv.XSendAble == false && cstLoaderPioRecv.XSendStart == false && cstLoaderPioRecv.XSendComplete == false &&
                         cstLoaderPioRecv.YRecvAble == false && cstLoaderPioRecv.YRecvStart == false && cstLoaderPioRecv.YRecvComplete == false)
                {
                    SeqStep = EmLoaderUnitCmd.S1070_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S1070_)
            {
                if (Y12AxisIsInPosition(EmLoaderYAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, this.PanelSet, EmCellPosi.Two) == true)
                {
                    SeqStep = EmLoaderUnitCmd.S2010_UD_START;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S0500_SIMUL_LOADING)
            {
                _tmrPlcSimul.Start(3);
                SeqStep = EmLoaderUnitCmd.S0512_;
            }
            else if (SeqStep == EmLoaderUnitCmd.S0512_)
            {
                if (_tmrPlcSimul)
                {
                    _tmrPlcSimul.Stop();
                    SeqStep = EmLoaderUnitCmd.S0515_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S0515_)
            {
                if (X12AxisPtpMoveCmdSync(equip, EmLoaderXAxisServo.Cal_Cell_Loading_Port, equip.CurrentRecipe, port, EmCellPosi.Two) == true)
                {
                    this.PanelSet = new PanelInfoSet()
                    {
                        ULDPort = equip.UD.CstUnloader_A.Port,
                        CellPosi = EmCellPosi.Two,
                        FlowUnitPosi = _simulatorPanelNo % 2 == 0 ? EmUnitPosi.ACol : EmUnitPosi.BCol,
                        //FlowUnitPosi = EmUnitPosi.ACol,
                        LDPort = equip.LD.CstLoader_A.Port,
                        Panel1 = new PanelInfo() { Exists = true, PanelNo = (_simulatorPanelNo + 00).ToString() },
                        Panel2 = new PanelInfo() { Exists = true, PanelNo = ((_simulatorPanelNo++) + 80).ToString() },
                    };

                    SeqStep = EmLoaderUnitCmd.S0510_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S0510_)
            {
                if (VaccumOnOff(equip, true, this.PanelSet.CellPosi) == true)
                {
                    SeqStep = EmLoaderUnitCmd.S0520_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S0520_)
            {
                if (IsVaccumOn(this.PanelSet.CellPosi) == true)
                {
                    SeqStep = EmLoaderUnitCmd.S0530_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S0530_)
            {
                if (Y12AxisPtpMoveCmdSync(equip, EmLoaderYAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, this.PanelSet, EmCellPosi.Two) == true)
                {
                    SeqStep = EmLoaderUnitCmd.S2010_UD_START;
                }
            }
        }
        public void LoaderCellUdLogicWorking(Equipment equip)
        {
            PortInfo port = equip.LD.CstLoader_A.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_A.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? equip.LD.CstLoader_A.Port :
                         equip.LD.CstLoader_B.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_B.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? equip.LD.CstLoader_B.Port :
                         null;

            PioRecvStep cstLoaderPioRecv = equip.LD.CstLoader_A.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_A.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? PioRecvA :
                                  equip.LD.CstLoader_B.Port.Status == EmPortStatus.BUSY && equip.LD.CstLoader_B.Port.Cst.CstStatus == EMCasstteStatus.CELLOUT_START ? PioRecvB :
                                  null;

            PioSendStep pioSend = (this.PanelSet.FlowUnitPosi == EmUnitPosi.ACol) ? PioSendA : PioSendB;

            if (SeqStep == EmLoaderUnitCmd.S2010_UD_START)
            {
                if (Y12AxisIsInPosition(EmLoaderYAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, this.PanelSet, EmCellPosi.Two) == true)
                {
                    pioSend.SendPanelInfoSet = this.PanelSet;
                    _tmrPlcSimul.Start(3);
                    SeqStep = EmLoaderUnitCmd.S2060_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S2060_)
            {
                if (_tmrPlcSimul)
                {
                    _tmrPlcSimul.Stop();
                    X1Axis.PtpMoveCmd(equip, EmLoaderXAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, this.PanelSet);
                    X2Axis.PtpMoveCmd(equip, EmLoaderXAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, this.PanelSet);
                    SeqStep = EmLoaderUnitCmd.S2065_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S2065_)
            {
                if (X12AxisIsInPosition(EmLoaderXAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, this.PanelSet, EmCellPosi.Two) == true)
                {
                    pioSend.YSendAble = true;
                    pioSend.YSendStart = true;

                    SeqStep = EmLoaderUnitCmd.S2070_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S2070_)
            {
                if (pioSend.XRecvAble == true && pioSend.XRecvStart == true && pioSend.XRecvComplete == true)
                {
                    pioSend.YSendAble = true;
                    pioSend.YSendStart = true;
                    pioSend.YSendComplete = true;
                    SeqStep = EmLoaderUnitCmd.S2090_;
                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S2090_)
            {
                if (pioSend.XRecvAble == false && pioSend.XRecvStart == false && pioSend.XRecvComplete == false)
                {
                    pioSend.YSendAble = false;
                    pioSend.YSendStart = false;
                    pioSend.YSendComplete = false;

                    this.PanelSet.ClearPanel();

                    SeqStep = EmLoaderUnitCmd.S0010_LOADING_START;

                }
            }
            else if (SeqStep == EmLoaderUnitCmd.S2090_)
            {
                EmLoaderXAxisServo xAxisCellUnLoadingPosi = this.PanelSet.FlowUnitPosi == EmUnitPosi.ACol ? EmLoaderXAxisServo.Cell_A_Unloading : EmLoaderXAxisServo.Cell_B_Unloading;
                if (Y12AxisIsInPosition(EmLoaderYAxisServo.Cal_Cell_Unloading_PnlSet, equip.CurrentRecipe, this.PanelSet, EmCellPosi.Two) == true &&
                    X12AxisIsInPosition(xAxisCellUnLoadingPosi, equip.CurrentRecipe, this.PanelSet, this.PanelSet.CellPosi) == true &&
                    IsVaccumOff(this.PanelSet.CellPosi) == true)
                {
                    SeqStep = EmLoaderUnitCmd.S0010_LOADING_START;
                }
            }
        }

        //HOME 시퀀스 구성. 
        public EmCellLoaderHomeStep HomeStep = EmCellLoaderHomeStep.H1010;
        public void LoaderHomeSeqLogicWorking(Equipment equip)
        {

            if (HomeStep == EmCellLoaderHomeStep.H0000)
            {
                //if (equip.LD.LoaderTransfer_A.IsHomeComplete == true && equip.LD.LoaderTransfer_B.IsHomeComplete == true)
                {
                    HomeStep = EmCellLoaderHomeStep.H1010; ;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1010)
            {
                if (this.IsExitsPanel == true)
                {
                    if (this.IsVaccumOn(PanelSet.CellPosi) == true)
                    {
                        HomeStep = EmCellLoaderHomeStep.H1020;
                    }
                    else
                    {
                        InterLockMgr.AddInterLock("셀 위치와 버큠이 상이함", "CELL 위치 {0}", PanelSet.CellPosi);
                    }
                }
                else
                {
                    HomeStep = EmCellLoaderHomeStep.H1020;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1020)
            {
                if (Y1Axis.IsHomeOnPosition() == false)
                {
                    if (Y1Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellLoaderHomeStep.H1030;
                    }
                }
                else
                {
                    HomeStep = EmCellLoaderHomeStep.H1030;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1030)
            {
                if (Y1Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellLoaderHomeStep.H1040;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1040)
            {
                if (Y2Axis.IsHomeOnPosition() == false)
                {
                    if (Y2Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellLoaderHomeStep.H1050;
                    }
                }
                else
                {
                    HomeStep = EmCellLoaderHomeStep.H1050;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1050)
            {
                if (Y1Axis.IsHomeOnPosition() == true && Y2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellLoaderHomeStep.H1060;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1060)
            {
                if (X1Axis.IsHomeOnPosition() == false)
                {
                    if (X1Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellLoaderHomeStep.H1070;
                    }
                }
                else
                {
                    HomeStep = EmCellLoaderHomeStep.H1070;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1070)
            {
                if (X1Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellLoaderHomeStep.H1080;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1080)
            {
                if (X2Axis.IsHomeOnPosition() == false)
                {
                    if (X2Axis.GoHome(equip) == true)
                    {
                        HomeStep = EmCellLoaderHomeStep.H1090;
                    }
                }
                else
                {
                    HomeStep = EmCellLoaderHomeStep.H1090;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1090)
            {
                if (X1Axis.IsHomeOnPosition() == true &&
                    X2Axis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCellLoaderHomeStep.H1110;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1110)
            {
                if (X1Axis.PtpMoveCmd(equip, EmLoaderXAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) == true)
                {
                    if (X2Axis.PtpMoveCmd(equip, EmLoaderXAxisServo.Cell_B_Unloading, equip.CurrentRecipe, null) == true)
                    {
                        HomeStep = EmCellLoaderHomeStep.H1120;
                    }
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H1120)
            {
                if (X1Axis.IsInPosition(EmLoaderXAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) == true &&
                    X2Axis.IsInPosition(EmLoaderXAxisServo.Cell_B_Unloading, equip.CurrentRecipe, null) == true)
                {
                    HomeStep = EmCellLoaderHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmCellLoaderHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true;
            }
        }


        //각 장치별 인터락
        public bool X12Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis 이동 불가, 배큠이 ON되어 있지 않습니다.");
                return true;
            }

            if (Y1Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) != true &&
                Y1Axis.IsHomeOnPosition() != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis 이동 불가, Y1Axis 축이 셀 언로드/홈 위치가 아닙니다.");
                return true;
            }

            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis 이동 불가, 배큠이 ON되어 있지 않습니다.");
                return true;
            }

            if (Y2Axis.IsInPosition(EmLoaderYAxisServo.Cell_A_Unloading, equip.CurrentRecipe, null) != true &&
                Y2Axis.IsHomeOnPosition() != true)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis 이동 불가, Y1Axis 축이 셀 언로드위치가 아닙니다.");
                return true;
            }


            if (equip.LD.LoaderTransfer_A.UpDown1Cylinder.IsBackward != true && equip.LD.LoaderTransfer_A.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis 이동 불가, LoaderTransfer_A의 UpDown1Cylinder가 Up 상태가 아닙니다.");
                return true;
            }
            if (equip.LD.LoaderTransfer_A.UpDown2Cylinder.IsBackward != true && equip.LD.LoaderTransfer_A.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis  이동 불가, LoaderTransfer_A의 UpDown2Cylinder가 Up 상태가 아닙니다.");
                return true;
            }

            if (equip.LD.LoaderTransfer_B.UpDown1Cylinder.IsBackward != true && equip.LD.LoaderTransfer_B.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis 이동 불가, LoaderTransfer_B의 UpDown1Cylinder가 Up 상태가 아닙니다.");
                return true;
            }

            if (equip.LD.LoaderTransfer_B.UpDown2Cylinder.IsBackward != true && equip.LD.LoaderTransfer_B.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Loader의 X1Axis  이동 불가, LoaderTransfer_B의 UpDown2Cylinder가 Up 상태가 아닙니다.");
                return true;
            }

            return false;
        }
        public bool Y12Axis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //if (svMoveType == emServoMoveType.PTP)
            //{
            //    //고민 필요. 
            //    if (equip.LD.CstLoader_A.Port.Status == EmPortStatus.BUSY)
            //    {
            //        if (equip.LD.CstLoader_A.CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) != true)
            //        {
            //            InterLockMgr.AddInterLock("Loader의 YAxis 이동 불가,  CstLoader A열의 RotationAxis 축이 90도가 아닙니다.");
            //            return true;
            //        }
            //    }

            //    if (equip.LD.CstLoader_B.Port.Status == EmPortStatus.BUSY)
            //    {
            //        if (equip.LD.CstLoader_B.CstRotationAxis.IsInPosition(EmCstLoaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) != true)
            //        {
            //            InterLockMgr.AddInterLock("Loader의 YAxis 이동 불가,  CstLoader B열의 RotationAxis 축이 90도가 아닙니다.");
            //            return true;
            //        }
            //    }
            //}

            //if (svMoveType  != emServoMoveType.HOME &&
            //    X1Axis.IsInPosition(EmLoaderXAxisServo.Cal_Cell_Loading_PnlSet, equip.CurrentRecipe, this.PanelSet) != true &&
            //    X1Axis.IsInPosition(EmLoaderXAxisServo.Cell_A_Unloading, equip.CurrentRecipe, this.PanelSet) != true &&
            //    X1Axis.IsInPosition(EmLoaderXAxisServo.Cell_B_Unloading, equip.CurrentRecipe, this.PanelSet) != true)
            //{
            //    InterLockMgr.AddInterLock("Loader의 YAxis 이동 불가,  X1Axis의 로딩 언로딩 위치가 아닙니다.");
            //}

            //if (svMoveType != emServoMoveType.HOME &&
            //    X2Axis.IsInPosition(EmLoaderXAxisServo.Cal_Cell_Loading_PnlSet, equip.CurrentRecipe, this.PanelSet) != true &&
            //    X2Axis.IsInPosition(EmLoaderXAxisServo.Cell_A_Unloading, equip.CurrentRecipe, this.PanelSet) != true &&
            //    X2Axis.IsInPosition(EmLoaderXAxisServo.Cell_B_Unloading, equip.CurrentRecipe, this.PanelSet) != true)
            //{
            //    InterLockMgr.AddInterLock("Loader의 YAxis 이동 불가,  X1Axis의 로딩 언로딩 위치가 아닙니다.");
            //}


            if (equip.LD.LoaderTransfer_A.UpDown1Cylinder.IsBackward != true && equip.LD.LoaderTransfer_A.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                InterLockMgr.AddInterLock("Loader의 Y1Axis  이동 불가, LoaderTransfer_A의  UpDown1Cylinder가 Up 상태가 아닙니다.");
                return true;
            }
            if (equip.LD.LoaderTransfer_B.UpDown1Cylinder.IsBackward != true && equip.LD.LoaderTransfer_B.YAxis.IsInPosition(EmLoaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null))
            {
                InterLockMgr.AddInterLock("Loader의 Y1Axis 이동 불가, LoaderTransfer_B의  UpDown1Cylinder가 Up 상태가 아닙니다.");
                return true;
            }

            //고민 필요. 
            return false;
        }


        //메소드 - 축 이동 및 위치 확인 
        public bool Y12AxisPtpMoveCmdSync(Equipment equip, EmLoaderYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Y1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Y2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                bool y1Reulst = Y1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                bool y2Reulst = Y2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                return y1Reulst && y2Reulst;
            }
            else
                return false;
        }
        public bool Y12AxisIsInPosition(EmLoaderYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Y1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Y2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Y1Axis.IsInPosition(posiNo, recp, opt) && Y2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }

        public bool X12AxisPtpMoveCmdSync(Equipment equip, EmLoaderXAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                ServoPosiInfo info = X1Axis.GetCalPosition(posiNo, equip.CurrentRecipe, opt);

                if (X1Axis.XF_CurrMotorPosition - info.Position > 0)
                {
                    return X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt) && X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                }
                else
                {
                    return X2Axis.PtpMoveCmdSync(equip, posiNo, recp, opt) && X1Axis.PtpMoveCmdSync(equip, posiNo, recp, opt);
                }
            }
            else
                return false;
        }
        public bool X12AxisIsInPosition(EmLoaderXAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return X1Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return X2Axis.IsInPosition(posiNo, recp, opt);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return X1Axis.IsInPosition(posiNo, recp, opt) && X2Axis.IsInPosition(posiNo, recp, opt);
            }
            else
                return false;
        }


        public void XYMoveingInterLock(Equipment equip)
        {
            if (X1Axis.XF_CurrMotorPosition + X2Axis.XF_CurrMotorPosition + 100 > X1Axis.SoftPlusLimit)
            {
                X1Axis.MoveStop(equip);
                X2Axis.MoveStop(equip);
            }
        }

        public bool VaccumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vaccum1.OnOff(equip, onOff);
                if (onOff == false) Blower1.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vaccum2.OnOff(equip, onOff);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vaccum1.OnOff(equip, onOff);
                Vaccum2.OnOff(equip, onOff);

                if (onOff == false)
                {
                    Blower1.OnOff(equip, true, 500);
                    Blower2.OnOff(equip, true, 500);
                }
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.None)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == false && Vaccum2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public enum EmLoaderXAxisServo
    {
        Cst_A_Middle  /**/= 0,
        Cst_A_Wait    /**/= 1,
        Cst_B_Middle  /**/= 2,
        Cst_B_Wait    /**/= 3,

        Cell_A_Unloading    /**/= 4,
        Cell_B_Unloading    /**/= 5,

        //계산된 위치..
        Cal_Cell_Loading_PnlSet    /**/= 6,  //포트 및 레시피 정보가 필요함. 
        Cal_Cell_Loading_Port     /**/= 7,  //포트 및 레시피 정보가 필요함. 
        Cal_Cell_Unloading_PnlSet     /**/= 8,  //포트 및 레시피 정보가 필요함. 
        Cal_Loader_Outside  /**/= 9, //회피 위치.
    }

    public class LoaderXAxisServo : ServoMotorControl
    {
        public LoaderXAxisServo(int innerAxisNo, int outterAxisNo, string name) :
        base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 850;
            SoftSpeedLimit = 1000;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;
        }

        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmLoaderXAxisServo.Cal_Cell_Loading_PnlSet == posiNo || (int)EmLoaderXAxisServo.Cal_Cell_Loading_Port == posiNo)
            {
                PortInfo ldPort = null;

                if ((int)EmLoaderXAxisServo.Cal_Cell_Loading_PnlSet == posiNo)
                {
                    PanelInfoSet pSet = opt as PanelInfoSet;
                    if (pSet == null)
                    {
                        InterLockMgr.AddInterLock("Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다 ");
                        return null;
                    }
                    ldPort = pSet.LDPort;
                }
                if ((int)EmLoaderXAxisServo.Cal_Cell_Loading_Port == posiNo)
                {
                    ldPort = opt as PortInfo;
                    if (ldPort == null)
                    {
                        InterLockMgr.AddInterLock("Cal_Cell_Loading_Port 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다 ");
                        return null;
                    }                   
                }



                if (ldPort.UnitPosi == EmUnitPosi.None)
                {
                    InterLockMgr.AddInterLock("Loader X축 이동 불가", "{0}, Cal_Cell_Loading 이동 명령시, Port 정보가 없습니다, 이동이 불가 합니다", ldPort.UnitPosi);
                    return null;
                }


                int loadNo = (int)(ldPort.UnitPosi == EmUnitPosi.ACol ? EmLoaderXAxisServo.Cst_A_Middle : EmLoaderXAxisServo.Cst_B_Middle);  //계산이 필요함. 
                ServoPosiInfo cellLoadingPosi = base.GetCalPosition(loadNo, recp, opt);

                if (AxisPosi == EmAxisPosi.Aixs1)
                {
                    int slotCol = ldPort.Cst.GetCellOutColPosi(this.AxisPosi, recp);
                    cellLoadingPosi.Position -= 150; //(int)(recp.CassetteWidth * (slotCol + 1));
                }
                else
                {
                    int slotCol = ldPort.Cst.GetCellOutColPosi(this.AxisPosi, recp) - recp.GetCstMaxCol / 2;
                    cellLoadingPosi.Position -= 300; //  (int)(recp.CassetteWidth * (slotCol + 1));
                }


                return cellLoadingPosi;
            }
            else if ((int)EmLoaderXAxisServo.Cal_Cell_Unloading_PnlSet == posiNo)
            {
                
                PanelInfoSet pSet = opt as PanelInfoSet;
                
                int cellUnloadingNo = (int)(pSet.FlowUnitPosi  == EmUnitPosi.ACol ? EmLoaderXAxisServo.Cell_A_Unloading : EmLoaderXAxisServo.Cell_B_Unloading);
                ServoPosiInfo cellUnloadingPosi = base.GetCalPosition(cellUnloadingNo, recp, opt);

                return cellUnloadingPosi;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }
        public bool IsInPosition(EmLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmLoaderXAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }

    }
}








﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class LoaderTAxisServo : ServoMotorControl
    {
        public static int Loader_A_Piker_0 { get; set; }    //  T
        public static int Loader_B_Piker_0 { get; set; }    //  T

        public LoaderTAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            Loader_A_Piker_0 = 1;
            Loader_B_Piker_0 = 2;


            //base.MoveActionName[0] = "Picker_L_0";          //   로더 이재기(T1) : 피커 0도
            //base.MoveActionName[1] = "Picker_R_0";          //   로더 이재기(T2) : 피커 0도

        }
    }
}
               
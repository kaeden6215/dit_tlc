﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstUnloaderUint : BaseUnit
    {
        public enum EmCstUnloaderHomeStep
        {
            H0000,
            H0010,
            H0020,


            //카셋트가 있을때. HOME 동작
            H1000_CST_PORT_EXIST,
            H1010,
            H1030,
            H3010,
            H1050,
            H3030,


            H3000_AXIS_HOME_START,
            H2010,
            H2030,
            H2040,
            H2050,
            H2060,


            //HOME 동작 완료
            H5999_HOME_COMPLETE,
            H1070,
            H2000_CST_PORT_EMTYP,
            H3020,
            H3040,
            H6000,
            H3050,
            H3060,
            H5010,
            H5020,
            H5030,
            H6010,
            H3080,
            H5000,
            H3025,
            H3070,
        }
        public enum EmCstUnloaderStep
        {
            S0000_CST_WAIT,

            //카셋트 로드
            S1000_CST_TO_LIFT_WAIT,
            S1010_CST_TO_LIFT_START,
            S1999_CST_TO_LIFT_END,

            //카셋트 틸트 측정
            S2010_CST_MEASURE_START,
            S2020_,
            S2030_,
            S2040_,
            S2050_,
            S2060_,
            S2070_,
            S2999_CEL_CST_TO_LD_END,

            //카셋트 CELL OUT
            S5010_CEL_CST_CELL_INOUT_START,
            S5020_,
            S5030_,

            //카셋트_언로드
            S6010_CEL_CST_TO_ULD_WAIT,
            S5040_,
            S5050_,
            S5055_,
            S5070_,
            S5015_,
            S5080_,
            S5110_,
            S5999_,
            S6060_,
            S6050_,
            S6080_,
            S6070_,
            S1000_CST_LD_WAIT,
            S1010_,
            S1020_,
            S1050_,
            S5045_,
            S1060_,
            S2080_,
            S5090_,
            S5100_,
            S5130_,
            S6020_,
            S6030_,
            S6040_,
            S5060_,
            T000,
            T010,
            T020,
            T030,
        }

        public PortInfo Port = new PortInfo(EmUnitPosi.None, "");
        public Cylinder TiltCylinder = new Cylinder();

        public Sensor ULDCstDetectOGamJi                                                  /**/    = new Sensor();
        public Sensor ULDCstDetectExist                                                   /**/    = new Sensor();

        public TiltSensorProxy TiltSensor { get; set; }
        public CylinderTwo CstGripCylinder                                            /**/    = new CylinderTwo();

        public CstUnloaderRotationUpAxisServo CstRotationAxis { get; set; }
        public CstUnloaderUpDownAxisServo CstUpDownAxis { get; set; }

        //MUTING 
        public SwitchOneWay CstInOutBuzzer                                             /**/    = new SwitchOneWay();
        public SwitchOneWay MutingLamp                                                 /**/    = new SwitchOneWay();
        //public ToggleRespSensor MutingBtnSwitch                                              /**/    = new ToggleRespSensor();
        //public ToggleRespSensor ResetBtnSwitch                                               /**/    = new ToggleRespSensor();
        //public Switch SafetyMutingSwitch1                                              /**/    = new Switch();
        //public Switch SafetyMutingSwitch2                                              /**/    = new Switch();
        //public SwitchOneWay LightCurtainReset                                           /**/   = new SwitchOneWay();

        public LightCurtainProxy LightCurtain                                      /**/   = new LightCurtainProxy();

        public CstUnloaderUint()
        {
            TiltSensor = new TiltSensorProxy("COM19");
        }

        public void InitializeInterLock()
        {
            CstRotationAxis.CheckStartMoveInterLockFunc = CstRotationAxis_CheckStartMoveInterLockFunc;
            CstUpDownAxis.CheckStartMoveInterLockFunc = CstUpDownAxis_CheckStartMoveInterLockFunc;
            CstGripCylinder.CheckStartMoveInterLockFunc = CstGripCylinder_CheckStartMoveInterLockFunc;
            TiltCylinder.CheckStartMoveInterLockFunc = TiltCylinder_CheckStartMoveInterLockFunc;


            SetStepSwith("CstTest");
            SetStepSwith("HomeSeqLogicWorking");
            SetStepSwith("CstLoadLogicWorking");
            SetStepSwith("CstMeasureSeqLogicWorking");
            SetStepSwith("CstCellInOutLogicWorking");
            SetStepSwith("CstUnloadLogicWorking");
        }

        public override void LogicWorking(Equipment equip)
        {
            TiltCylinder.LogicWorking(equip);
            ULDCstDetectOGamJi.LogicWorking(equip);
            ULDCstDetectExist.LogicWorking(equip);
            CstGripCylinder.LogicWorking(equip);

            CstRotationAxis.LogicWorking(equip);
            CstUpDownAxis.LogicWorking(equip);

            CstInOutBuzzer.LogicWorking(equip);
            MutingLamp.LogicWorking(equip);


            //if (UnitPosi == EmUnitPosi.ACol)
            LightCurtain.LogicWorking(equip);


            SatusLogicWorking(equip);


            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;
            if (UnitPosi == EmUnitPosi.ACol)
            {
                SeqLogicWorking(equip);
            }
            else
            {
                IsHomeComplete = true;
            }
        }

        public void SatusLogicWorking(Equipment equip)
        {
            if (GG.TestMode == false)
            {
                if (LightCurtain.XB_Warning_Detect_BJub.vBit == false)
                {
                    if (UnitPosi == EmUnitPosi.ACol)
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0602_MUTING_UNLOADER_A);
                    else if (UnitPosi == EmUnitPosi.BCol)
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0603_MUTING_UNLOADER_B);

                    equip.ChangeMode(EmEquipRunMode.Pause);
                }
            }

        }

        //구동 시퀀스 구성. 
        public EmCstUnloaderStep SeqStep = EmCstUnloaderStep.S0000_CST_WAIT;
        private int _cellVaccumOnTryCount = 0;
        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = SeqStep.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요. 
                IsHomeComplete = false;

                SeqStep = EmCstUnloaderStep.S0000_CST_WAIT;                
                HomeStep = EmCstUnloaderHomeStep.H0000;
                
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                if (LightCurtain.XB_Warning_Detect_BJub.vBit == false)
                {
                    if (CstRotationAxis.IsMoving == true)
                        CstRotationAxis.MoveStop(equip);
                    if (CstUpDownAxis.IsMoving == true)
                        CstUpDownAxis.MoveStop(equip);
                }
                //일시 정지. 
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Start)
            { }

            if (this.IsHomeComplete == false)
            {
                //if (equip.UD.Unloader.IsHomeComplete == true )
                {
                    if (GetStepSwith("HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);
                }
                return;
            }

            if (equip.IsHomeComplete == false) return;
            if (GetStepSwith("CstTest")) CstTest(equip);


            //if (GetStepSwith("CstLoadLogicWorking"))        /**/CstLoadLogicWorking(equip);
            //if (GetStepSwith("CstMeasureSeqLogicWorking"))  /**/ CstMeasureSeqLogicWorking(equip);
            //if (GetStepSwith("CstCellInOutLogicWorking"))   /**/ CstCellInOutLogicWorking(equip);
            //if (GetStepSwith("CstUnloadLogicWorking"))      /**/ CstUnloadLogicWorking(equip);
        }
        private PlcTimer _ptmrMove = new PlcTimer();
        public void CstTest(Equipment equip)
        {
            if (CstUpDownAxis.IsHomeOnPosition() == false) return;

            if (SeqStep == EmCstUnloaderStep.S0000_CST_WAIT)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstUnloaderRotationUpAxisServo.R_0dgr, equip.CurrentRecipe, null) == true)
                {
                    _ptmrMove.Start(3);
                    SeqStep = EmCstUnloaderStep.T010;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.T010)
            {
                if (_ptmrMove)
                {
                    _ptmrMove.Stop();
                    SeqStep = EmCstUnloaderStep.T020;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.T020)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true)
                {
                    _ptmrMove.Start(3);
                    SeqStep = EmCstUnloaderStep.T030;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.T030)
            {
                if (_ptmrMove)
                {
                    _ptmrMove.Stop();
                    SeqStep = EmCstUnloaderStep.S0000_CST_WAIT;
                }
            }
        }


        public void CstLoadLogicWorking(Equipment equip)
        {
            /* */
            if (SeqStep == EmCstUnloaderStep.S0000_CST_WAIT)
            {
                if (LightCurtain.XB_MutingOnOff.vBit == false)
                {
                    if (ULDCstDetectExist.IsOnOff == true && ULDCstDetectOGamJi.IsOnOff == false)
                    {
                        if (GG.TestMode == true)
                        {
                            Port.Cst.InCount = 0;
                            Port.Cst.OutCount = 0;
                            Port.Cst.FillCassette(equip.CurrentRecipe, 0, new OnlineCassetteInfoInfo());
                            Port.ChangePortStatus(EmPortStatus.BUSY);
                            Port.Cst.ChangeCassetteStatus(equip, EMCasstteStatus.CELLOUT_START);
                        }
                        SeqStep = EmCstUnloaderStep.S1010_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S1010_)
            {
                if (LightCurtain.XB_MutingOnOff.vBit == true && LightCurtain.XB_Warning_Detect_BJub == true)
                {
                    SeqStep = EmCstUnloaderStep.S1020_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S1020_)
            {
                if ( /*투입 버튼 확인*/true)
                {
                    if (CstGripCylinder.Forward(equip) == true)
                        SeqStep = EmCstUnloaderStep.S1050_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S1050_)
            {
                if (CstGripCylinder.IsForward == true)
                {
                    if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmCstUnloaderStep.S1999_CST_TO_LIFT_END;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S1999_CST_TO_LIFT_END)
            {
                SeqStep = EmCstUnloaderStep.S2010_CST_MEASURE_START;
            }
        }
        public void CstMeasureSeqLogicWorking(Equipment equip)
        {
            /* */
            if (SeqStep == EmCstUnloaderStep.S2010_CST_MEASURE_START)
            {
                if (CstRotationAxis.IsInPosition(EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true &&
                    CstUpDownAxis.IsInPosition(EmCstUnloaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true && CstGripCylinder.IsForward == true)
                {
                    SeqStep = EmCstUnloaderStep.S2020_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S2020_)
            {
                if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstUnloaderUpDownAxisServo.Cst_Measure, equip.CurrentRecipe, null) == true)
                {
                    if (TiltCylinder.Forward(equip) == true)
                    {
                        SeqStep = EmCstUnloaderStep.S2040_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S2040_)
            {
                if (TiltCylinder.IsForward == true)
                {
                    if (TiltSensor.SendCmd() == true)
                    {
                        SeqStep = EmCstUnloaderStep.S2050_;
                    }
                    else
                    {
                        //알람 처리. 
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S2050_)
            {
                if (this.UnitPosi == EmUnitPosi.ACol)
                {
                    if (TiltSensor.IsReadComplete == true)
                    {
                        if (TiltSensor.IsReadSuccess)
                        {
                            if (TiltSensor.GetReadJudge[0] == true && TiltSensor.GetReadJudge[1] == true)
                            {
                                Port.Cst.IsTiltSuccess = true;
                                //정상 처리. 
                            }
                            else
                            {
                                Port.Cst.IsTiltSuccess = false;
                                //알람 처리
                            }

                            SeqStep = EmCstUnloaderStep.S2060_;
                        }
                        else
                        {
                            //알람 처리. 
                            Port.Cst.IsTiltSuccess = false;
                            SeqStep = EmCstUnloaderStep.S2060_;
                        }
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.BCol)
                {
                    if (TiltSensor.IsReadComplete == true)
                    {
                        if (TiltSensor.IsReadSuccess)
                        {
                            if (TiltSensor.GetReadJudge[2] == true && TiltSensor.GetReadJudge[3] == true)
                            {
                                Port.Cst.IsTiltSuccess = true;
                                //정상 처리. 
                            }
                            else
                            {
                                Port.Cst.IsTiltSuccess = false;
                                //알람 처리
                            }

                            SeqStep = EmCstUnloaderStep.S2060_;
                        }
                        else
                        {
                            //알람 처리. 
                            Port.Cst.IsTiltSuccess = false;
                            SeqStep = EmCstUnloaderStep.S2060_;
                        }
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.None)
                {
                    //알람 처리.
                }
                //바코드 리더 추가 필요, CIM 추가 필요. 
            }
            else if (SeqStep == EmCstUnloaderStep.S2060_)
            {
                if (TiltCylinder.Backward(equip) == true)
                {
                    SeqStep = EmCstUnloaderStep.S2070_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S2070_)
            {
                if (TiltCylinder.IsBackward == true)
                {
                    if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstUnloaderUpDownAxisServo.Cst_CellOut, equip.CurrentRecipe, null) == true)
                    {
                        SeqStep = EmCstUnloaderStep.S2080_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S2080_)
            {
                if (Port.Cst.IsTiltSuccess)
                {
                    SeqStep = EmCstUnloaderStep.S5010_CEL_CST_CELL_INOUT_START;
                }
                else
                {
                    SeqStep = EmCstUnloaderStep.S6010_CEL_CST_TO_ULD_WAIT;
                }
            }
        }
        public void CstCellInOutLogicWorking(Equipment equip)
        {
            if (Port.Status != EmPortStatus.BUSY) return;


            /* */
            if (SeqStep == EmCstUnloaderStep.S5010_CEL_CST_CELL_INOUT_START)
            {
                if (CstRotationAxis.IsInPosition(EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true &&
                    CstUpDownAxis.IsInPosition(EmCstUnloaderUpDownAxisServo.Cst_CellOut, equip.CurrentRecipe, null) == true &&
                    CstGripCylinder.IsForward == true && TiltCylinder.IsBackward == true)
                {

                    //포트 상태를 변경함. 
                    Port.Cst.ChangeCassetteStatus(equip, EMCasstteStatus.CELLOUT_START);

                    SeqStep = EmCstUnloaderStep.S5015_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5015_)
            {
                SeqStep = EmCstUnloaderStep.S5020_;
            }
            else if (SeqStep == EmCstUnloaderStep.S5020_)
            {
                if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstUnloaderUpDownAxisServo.Cal_Cst_CellIn_Wiat, equip.CurrentRecipe, Port) == true)
                {
                    PioRecv.YRecvAble = true;
                    SeqStep = EmCstUnloaderStep.S5040_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5040_)
            {
                if (PioRecv.XSendAble == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    this.PanelSet = PioRecv.RecvPanelInfoSet;

                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;
                    SeqStep = EmCstUnloaderStep.S5045_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5045_)
            {
                //카셋트에 CST1 티입일 경위 한 X축를 회피위치로 이동 필요. //방안 고민. 
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.UD.Unloader.X12AxisPtpMoveCmdSync(equip, EmUnLoaderXAxisServo.Cal_Cell_Unload_PnlSet, equip.CurrentRecipe, PioRecv.RecvPanelInfoSet, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmCstUnloaderStep.S5050_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5050_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.UD.Unloader.Y12AxisPtpMoveCmdSync(equip, EmUnLoaderYAxisServo.Cal_Cell_UnLoading, equip.CurrentRecipe, PioRecv.RecvPanelInfoSet, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        if (equip.UD.Unloader.VaccumOnOff(equip, false, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmCstUnloaderStep.S5055_;
                        }
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5055_)
            {
                if (equip.UD.Unloader.IsVaccumOff(PioRecv.RecvPanelInfoSet.CellPosi) == true)
                {
                    SeqStep = EmCstUnloaderStep.S5060_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5060_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstUnloaderUpDownAxisServo.Cal_Cst_CellIn, equip.CurrentRecipe, Port) == true)
                    {
                        SeqStep = EmCstUnloaderStep.S5070_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5070_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.UD.Unloader.Y12AxisIsInPosition(EmUnLoaderYAxisServo.Cal_Cell_UnLoading, equip.CurrentRecipe, PioRecv.RecvPanelInfoSet, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        SeqStep = EmCstUnloaderStep.S5080_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5080_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (CstUpDownAxis.IsInPosition(EmCstUnloaderUpDownAxisServo.Cal_Cst_CellIn, equip.CurrentRecipe, Port) == true)
                    {
                        if (equip.UD.Unloader.Y12AxisIsInPosition(EmUnLoaderYAxisServo.Cal_Cell_UnLoading, equip.CurrentRecipe, PioRecv.RecvPanelInfoSet, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                        {
                            SeqStep = EmCstUnloaderStep.S5090_;
                        }
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5090_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    Port.Cst.CellInComplete(PioRecv.RecvPanelInfoSet);
                    equip.UD.Unloader.PanelSet.ClearPanel();

                    SeqStep = EmCstUnloaderStep.S5100_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5100_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)  //로더 X축은 셀 추출 위치에서 대기,
                {
                    if (equip.UD.Unloader.Y12AxisPtpMoveCmdSync(equip, EmUnLoaderYAxisServo.Cal_Cell_Loading_PnlSet, equip.CurrentRecipe, PioRecv.RecvPanelInfoSet, PioRecv.RecvPanelInfoSet.CellPosi) == true)
                    {
                        PioRecv.YRecvComplete = true;
                        SeqStep = EmCstUnloaderStep.S5110_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5110_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true && PioRecv.XSendComplete == true)
                {


                    PioRecv.YRecvAble = false;
                    PioRecv.YRecvStart = false;
                    PioRecv.YRecvComplete = false;
                    SeqStep = EmCstUnloaderStep.S5130_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5130_)
            {
                if (PioRecv.XSendAble == false && PioRecv.XSendStart == false && PioRecv.XSendComplete == false)
                {
                    if (Port.Cst.InCount < 160) //Port.Cst.CellCount)
                    {
                        //다음 글라스 처리. 
                        SeqStep = EmCstUnloaderStep.S5015_;
                    }
                    else
                    {
                        //카셋트 배출 위치로 이동. 
                        SeqStep = EmCstUnloaderStep.S5999_;
                    }
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S5999_)
            {
                if (CstUpDownAxis.PtpMoveCmdSync(equip, EmCstUnloaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true)
                {
                    SeqStep = EmCstUnloaderStep.S6010_CEL_CST_TO_ULD_WAIT;
                }
            }
        }
        public void CstUnloadLogicWorking(Equipment equip)
        {
            if (SeqStep == EmCstUnloaderStep.S6010_CEL_CST_TO_ULD_WAIT)
            {
                if (CstRotationAxis.IsInPosition(EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true &&
                    CstUpDownAxis.IsInPosition(EmCstUnloaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) == true &&
                    CstGripCylinder.IsForward == true && TiltCylinder.IsBackward == true)
                {
                    SeqStep = EmCstUnloaderStep.S6020_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S6020_)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstUnloaderRotationUpAxisServo.R_0dgr, equip.CurrentRecipe, null) == true)
                {
                    SeqStep = EmCstUnloaderStep.S6030_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S6030_)
            {
                if (ULDCstDetectExist.IsOnOff == false && ULDCstDetectOGamJi.IsOnOff == false)
                {
                    SeqStep = EmCstUnloaderStep.S6040_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S6040_)
            {
                if (LightCurtain.XB_MutingOnOff.vBit == false && LightCurtain.XB_Warning_Detect_BJub.vBit == false)  //&& SafetyMuttingOnOffSwitch1.IsOnOff == false && SafetyMuttingOnOffSwitch2.IsOnOff == false)
                {
                    SeqStep = EmCstUnloaderStep.S6080_;
                }
            }
            else if (SeqStep == EmCstUnloaderStep.S6080_)
            {
                SeqStep = EmCstUnloaderStep.S0000_CST_WAIT;
            }
        }

        //HOME 시퀀스 구성. 
        public EmCstUnloaderHomeStep HomeStep = EmCstUnloaderHomeStep.H0000;
        public PlcTimerEx _delayAixsHome = new PlcTimerEx("Cst Unloader Home");
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmCstUnloaderHomeStep.H0000)
            {
                if (this.IsHomeComplete == false)
                {
                    HomeStep = EmCstUnloaderHomeStep.H0010;
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H0010)
            {
                if (TiltCylinder.IsBackward == false)
                    TiltCylinder.Backward(equip);

                HomeStep = EmCstUnloaderHomeStep.H0020;
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H0020)
            {
                if (TiltCylinder.IsBackward == true)
                {
                    if (ULDCstDetectExist.IsOnOff == true && ULDCstDetectOGamJi.IsOnOff == false)
                    {
                        HomeStep = EmCstUnloaderHomeStep.H1000_CST_PORT_EXIST;
                    }
                    else
                    {
                        HomeStep = EmCstUnloaderHomeStep.H2000_CST_PORT_EMTYP;
                    }
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H1000_CST_PORT_EXIST)
            {
                if (CstGripCylinder.IsForward != true)
                {
                    if (CstGripCylinder.Forward(equip))
                    {
                        HomeStep = EmCstUnloaderHomeStep.H1010;
                    }
                }
                else
                {
                    HomeStep = EmCstUnloaderHomeStep.H1010;
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H1010)
            {
                if (CstGripCylinder.IsForward == true)
                {
                    HomeStep = EmCstUnloaderHomeStep.H3000_AXIS_HOME_START;
                }
            }
            // 점프 


            else if (HomeStep == EmCstUnloaderHomeStep.H2000_CST_PORT_EMTYP)
            {
                if (CstGripCylinder.IsBackward == false)
                {
                    if (CstGripCylinder.Backward(equip))
                    {
                        HomeStep = EmCstUnloaderHomeStep.H2010;
                    }
                }
                else
                {
                    HomeStep = EmCstUnloaderHomeStep.H2010;
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H2010)
            {
                if (CstGripCylinder.IsBackward == true)
                {
                    HomeStep = EmCstUnloaderHomeStep.H3000_AXIS_HOME_START;
                }
            }
            // 점프 


            else if (HomeStep == EmCstUnloaderHomeStep.H3000_AXIS_HOME_START)
            {
                if (CstUpDownAxis.GoHome(equip) == true)
                {
                    HomeStep = EmCstUnloaderHomeStep.H3010;
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H3010)
            {
                if (CstUpDownAxis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCstUnloaderHomeStep.H3020;
                    _delayAixsHome.Start(0, 100);
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H3020)
            {
                if (_delayAixsHome)
                {
                    _delayAixsHome.Stop();

                    if (CstRotationAxis.GoHome(equip) == true)
                    {
                        HomeStep = EmCstUnloaderHomeStep.H3030;
                    }
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H3030)
            {
                if (CstRotationAxis.IsHomeOnPosition() == true)
                {
                    _delayAixsHome.Start(0, 500);
                    HomeStep = EmCstUnloaderHomeStep.H3040;
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H3040)
            {
                if (_delayAixsHome)
                {
                    _delayAixsHome.Stop();
                    HomeStep = EmCstUnloaderHomeStep.H3050;

                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H3050)
            {
                if (CstUpDownAxis.IsHomeOnPosition() == true && CstRotationAxis.IsHomeOnPosition() == true)
                {
                    HomeStep = EmCstUnloaderHomeStep.H3060;
                }
            }
            else if (HomeStep == EmCstUnloaderHomeStep.H3060)
            {
                if (ULDCstDetectExist.IsOnOff == false && ULDCstDetectOGamJi.IsOnOff == false)
                {
                    HomeStep = EmCstUnloaderHomeStep.H5000;
                }
                else if (ULDCstDetectExist.IsOnOff == true && ULDCstDetectOGamJi.IsOnOff == false)
                {
                    HomeStep = EmCstUnloaderHomeStep.H6000;
                }
            }
            //점프. 

            //카셋트가 없을때. 로딩 위치.
            else if (HomeStep == EmCstUnloaderHomeStep.H5000)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstUnloaderRotationUpAxisServo.R_0dgr, equip.CurrentRecipe, null) == true)
                {
                    HomeStep = EmCstUnloaderHomeStep.H5999_HOME_COMPLETE;
                }
            }
            //점프. 

            //카셋트가 있을때. 로딩 위치.
            else if (HomeStep == EmCstUnloaderHomeStep.H6000)
            {
                if (CstRotationAxis.PtpMoveCmdSync(equip, EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) == true)
                {
                    HomeStep = EmCstUnloaderHomeStep.H5999_HOME_COMPLETE;
                }
            }

            //점프
            else if (HomeStep == EmCstUnloaderHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true;
            }
        }




        //각 장치별 인터락
        public bool CstRotationAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            //if (equip.UD.Unloader.Y1Axis.IsInPosition(EmUnLoaderYAxisServo.Cell_A_Loading, equip.CurrentRecipe, null) != false
            //    && equip.UD.Unloader.Y2Axis.IsInPosition(EmUnLoaderYAxisServo.Cell_B_Loading, equip.CurrentRecipe, null) != false)
            //{
            //    //인터락 처리. 
            //    InterLockMgr.AddInterLock(string.Format("{0}CstUnloaderUnit  CstRotationAxis 불가, {0}Unloader의 Y1Axis/Y2Axis가  ULD 위치입니다.", UnitPosi));
            //    return true;
            //}

            //if (svMoveType != emServoMoveType.HOME && ULDCstDetectExist.IsOnOff == false )
            //{
            //    InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, 카셋트가 없습니다. ");
            //    return true;
            //}

            if (ULDCstDetectExist.IsOnOff == false && ULDCstDetectExist.IsOnOff == true)
            {
                InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, 카셋트가 방향이 잘못되었습니다. ");
                return true;
            }

            if (CstGripCylinder.IsForward != true)
            {
                if (ULDCstDetectExist.IsOnOff == true)
                {
                    InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Grip 실린더가 그랩 상태가 아닙니다.");
                    return true;
                }
            }

            if (TiltCylinder.IsBackward != true)
            {
                InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Tilt 실린더가 후진 상태가 아닙니다. ");
                return true;
            }

            //if (svMoveType != emServoMoveType.HOME &&
            //    equip.UD.Unloader.Y1Axis.IsInPosition(EmUnLoaderYAxisServo.Cell_A_Loading, equip.CurrentRecipe, null) != true &&
            //    equip.UD.Unloader.Y1Axis.IsInPosition(EmUnLoaderYAxisServo.Cell_B_Loading, equip.CurrentRecipe, null) != true &&
            //    equip.UD.Unloader.Y1Axis.IsHomeOnPosition() != true)
            //{
            //    InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Loader Y1축이 ULD/홈 포지션이 아닙니다.");
            //    return true;
            //}

            //if (svMoveType != emServoMoveType.HOME &&
            //    equip.UD.Unloader.Y2Axis.IsInPosition(EmUnLoaderYAxisServo.Cell_A_Loading, equip.CurrentRecipe, null) != true &&
            //    equip.UD.Unloader.Y2Axis.IsInPosition(EmUnLoaderYAxisServo.Cell_B_Loading, equip.CurrentRecipe, null) != true &&
            //              equip.UD.Unloader.Y2Axis.IsHomeOnPosition() != true)
            //{
            //    InterLockMgr.AddInterLock("Cst Rotation축 이동 불가, Loader Y2축이 ULD/홈 포지션이 아닙니다.");
            //    return true;
            //}



            //if (equip.UD.CstUnloader_A.lift
            //{
            //    //인터락 처리. 
            //    InterLockMgr.AddInterLock(string.Format("{0}CstUnloaderUnit  CstRotationAxis 불가, {0}Unloader의 Y1Axis/Y2Axis가  ULD 위치입니다.", UnitPosi));
            //    return true;
            //}

            return false;
        }
        public bool CstGripCylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect idirect)
        {
            if (CstRotationAxis.IsInPosition(EmCstUnloaderRotationUpAxisServo.R_0dgr, equip.CurrentRecipe, null) != true)
            {
                InterLockMgr.AddInterLock("Grip 실린더 이동 불가, Cst Rotation축 0도 위치가 아닙니다. ");
                return true;
            }

            if (CstUpDownAxis.IsInPosition(EmCstUnloaderUpDownAxisServo.Cst_Loading, equip.CurrentRecipe, null) != false)
            {
                InterLockMgr.AddInterLock("Grip 실린더 이동 불가, 카셋트 업다운축 로드/언로드 위치가 아닙니다. ");
                return true;
            }

            if (TiltCylinder.IsBackward != true)
            {
                InterLockMgr.AddInterLock("Grip 실린더 이동 불가, Tilt 실린더가 후진 상태가 아닙니다. ");
                return true;
            }

            return false;
        }
        public bool CstUpDownAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            if (svMoveType != emServoMoveType.HOME && CstRotationAxis.IsInPosition(EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) != true)
            {
                InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, 카셋트 Rotation축이 90 dgr 포지션이 아닙니다.");
                return true;
            }

            if (CstGripCylinder.IsForward != true)
            {
                if (ULDCstDetectExist.IsOnOff == true)
                {
                    InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, Grip 실린더가 그랩 상태가 아닙니다.");
                    return true;
                }
            }

            if (TiltCylinder.IsBackward != true)
            {
                InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, Tilt 실린더가 후진 상태가 아닙니다. ");
                return true;
            }

            return false;
        }
        public bool TiltCylinder_CheckStartMoveInterLockFunc(Equipment equip, EmCylinderDirect idirect)
        {
            if (CstRotationAxis.IsInPosition(EmCstUnloaderRotationUpAxisServo.R_90dgr, equip.CurrentRecipe, null) != true)
            {
                InterLockMgr.AddInterLock("Tilt 실린더 이동 불가, 카셋트 Rotation축이 90 dgr 포지션이 아닙니다.");
                return true;
            }

            if (CstGripCylinder.IsForward != true)
            {
                InterLockMgr.AddInterLock("Cst UpDown축 이동 불가, Grip 실린더가 그랩 상태가 아닙니다.");
                return true;
            }

            if (CstUpDownAxis.IsInPosition(EmCstUnloaderUpDownAxisServo.Cst_Measure, equip.CurrentRecipe, null) == false)
            {
                //인터락 처리. 
                InterLockMgr.AddInterLock("Tilt 실린더 이동 불가, Unloader Up Down 축이 Tilt 측정위치가 아닙니다. ");
                return true;
            }

            return false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstUnloaderRotationDownAxisServo : ServoMotorControl
    {
        public static int A_Lower_Cst_Insert     {get; set;}
        public static int A_Lower_Cst_Move       {get; set;}
        public static int A_Upper_Cst_Emission   {get; set;}
        public static int A_Upper_Cst_Move       {get; set;}
        public static int B_Lower_Cst_Insert     {get; set;}
        public static int B_Lower_Cst_Move       {get; set;}
        public static int B_Upper_Cst_Emission   {get; set;}
        public static int B_Upper_Cst_Move       {get; set;}

        public CstUnloaderRotationDownAxisServo(int innerAxisNo, int outterAxisNo, string name, int posiCount, int acsBuffer) :
            base(innerAxisNo, outterAxisNo, name)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;


            //A_Lower_Cst_Insert                    = 1;
            //A_Lower_Cst_Move                      = 2;
            //A_Upper_Cst_Emission                  = 3;
            //A_Upper_Cst_Move                      = 4;
            //B_Lower_Cst_Insert                    = 5;
            //B_Lower_Cst_Move                      = 6;
            //B_Upper_Cst_Emission                  = 7;
            //B_Upper_Cst_Move                      = 8;


            //base.MoveActionName[0] = "LoadingBottomL_T";                         //A 하부 카세트(T2) : 카세트 투입
            //base.MoveActionName[1] = "UnloadingBottomL_T";                       //A 하부 카세트(T2) : 리프트 이동
            //base.MoveActionName[2] = "UnloadingTopL_T";                          //A 상부 카세트(T1) : 카세트 배출
            //base.MoveActionName[3] = "LoadingTopL_T";                            //A 상부 카세트(T1) : 카세트 이동
            //base.MoveActionName[4] = "LoadingBottomR_T";                         //B 하부 카세트(T4) : 카세트 투입
            //base.MoveActionName[5] = "UnloadingBottomR_T";                       //B 하부 카세트(T4) : 리프트 이동
            //base.MoveActionName[6] = "UnloadingTopR_T";                          //B 상부 카세트(T3) : 카세트 배출
            //base.MoveActionName[7] = "LoadingTopR_T";                            //B 상부 카세트(T3) : 카세트 이동

        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmCstUnloaderUpDownAxisServo
    {
        Cst_Loading             /**/= 0,
        Cst_Measure             /**/= 1,
        Cst_CellOut             /**/= 2,
        
        //계산된 위치값
        Cal_Cst_CellIn_Wiat    /**/= 3,  //Loader Y축이 전진 가능
        Cal_Cst_CellIn         /**/= 4,  //Loader Y축이 후진 가능
    }

    public class CstUnloaderUpDownAxisServo : ServoMotorControl
    {
        public CstUnloaderUpDownAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 400;
            SoftSpeedLimit = 250;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

        }

        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmCstUnloaderUpDownAxisServo.Cal_Cst_CellIn_Wiat == posiNo || (int)EmCstUnloaderUpDownAxisServo.Cal_Cst_CellIn == posiNo)
            {
                PortInfo port = opt as PortInfo;
                if (port == null)
                {
                    InterLockMgr.AddInterLock(" Cst_Loader_CalCellOutPosi 이동 명령시, Port 정보가 없습니다. ");
                    return null;
                }

                /*계산 처리*/
                int iCol = 0;
                int iRow = 0;   // 기존값 0 잠시 변경
                iCol = Math.DivRem(port.Cst.OutCount, (int)recp.CassetteMaxHeightCount, out iRow);

                ServoPosiInfo info = GetCalPosition(EmCstUnloaderUpDownAxisServo.Cst_CellOut, recp, null);
                float position = (float)(info.Position + (iCol * recp.CassetteCellOutWaitHitch));

                ServoPosiInfo target = new ServoPosiInfo();

                if ((int)EmCstUnloaderUpDownAxisServo.Cal_Cst_CellIn_Wiat == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("CELL OUT 대기 위치 {0} SLOT, Out Cell {1}  ", iRow, port.Cst.OutCount),
                        Position = position + iRow * 20 + (float)recp.CassetteCellOutWaitHitch,
                        Speed = info.Speed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmCstUnloaderUpDownAxisServo.Cal_Cst_CellIn == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("CELL OUT 위치 {0} SLOT, Out Cell {1}  ", iRow, port.Cst.OutCount),
                        Position = position + iRow * 20 - (float)recp.CassetteCellOutUpHitch,
                        Speed = info.Speed,
                        Accel = info.Accel,
                    };
                }
                return target;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }
        public bool IsInPosition(EmCstUnloaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmCstUnloaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmCstUnloaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmCstUnloaderUpDownAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}



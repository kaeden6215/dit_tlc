﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstUnloaderUint : BaseUnit
    {
        public PortInfo Port = new PortInfo();
        public Cylinder TiltCylinder = new Cylinder();

        public Sensor ULDCasstteDetect_1                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_2                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_3                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_4                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_5                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_6                                                  /**/    = new Sensor();

        public CylinderTwo CasstteGripCylinder                                            /**/    = new CylinderTwo();

        public CstUnloaderRotationUpAxisServo CstRotationAxis { get; set; }
        public CstUnloaderUpDownAxisServo CstUpDownAxis { get; set; }

        //MUTING 
        public SwitchOneWay CstInOutBuzzer                                             /**/    = new SwitchOneWay();
        public SwitchOneWay MutingLamp                                                 /**/    = new SwitchOneWay();
        public SensorResp MutingBtnSwitch                                              /**/    = new SensorResp();
        public SensorResp ResetBtnSwitch                                               /**/    = new SensorResp();
        public Switch SafetyMutingSwitch1                                              /**/    = new Switch();
        public Switch SafetyMutingSwitch2                                              /**/    = new Switch();
        public SwitchOneWay LightCurtainReset                                           /**/   = new SwitchOneWay();

        //LIFT
        //TILT 센서
        public Switch Tilt { get; set; }

        public void InitializeInterLock()
        {
            CstRotationAxis.CheckStartMoveInterLockFunc = CstRotationAxis_CheckStartMoveInterLockFunc;
            CstUpDownAxis.CheckStartMoveInterLockFunc = CstUpDownAxis_CheckStartMoveInterLockFunc; 
        }

        public override void LogicWorking(Equipment equip)
        {
            TiltCylinder.LogicWorking(equip);
            ULDCasstteDetect_1.LogicWorking(equip);
            ULDCasstteDetect_2.LogicWorking(equip);
            ULDCasstteDetect_3.LogicWorking(equip);
            ULDCasstteDetect_4.LogicWorking(equip);
            ULDCasstteDetect_5.LogicWorking(equip);
            ULDCasstteDetect_6.LogicWorking(equip);
            CasstteGripCylinder.LogicWorking(equip);


            CstRotationAxis.LogicWorking(equip);
            CstUpDownAxis.LogicWorking(equip);

            CstInOutBuzzer.LogicWorking(equip);
            MutingLamp.LogicWorking(equip);
            MutingBtnSwitch.LogicWorking(equip);
            ResetBtnSwitch.LogicWorking(equip);
            SafetyMutingSwitch1.LogicWorking(equip);
            SafetyMutingSwitch2.LogicWorking(equip);
            LightCurtainReset.LogicWorking(equip);

        }
        public void LoaderCmdLogicWorking(Equipment equip)
        {

            if (equip.RunMode == EmEquipRunMode.Manual)
                return;
        }
        internal bool IsSlotPosiont(Equipment equip)
        {
            throw new NotImplementedException();
        }
        internal bool CstSlotDown(Equipment equip)
        {
            throw new NotImplementedException();
        }



        //각 장치별 인터락
        public bool CstRotationAxis_CheckStartMoveInterLockFunc(Equipment equip)
        {
            return false;
        }
        public bool CstUpDownAxis_CheckStartMoveInterLockFunc(Equipment equip)
        {
            return false;
        }
    }
}

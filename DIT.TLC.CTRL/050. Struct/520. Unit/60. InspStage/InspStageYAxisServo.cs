﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public enum EmInspStageYAxisServo
    {
        Cell_Loading        /**/= 0,
        Cell_Unloading      /**/= 1,
        Cell_Insp           /**/= 2,

        Rcp_A_Cell_Insp_Upper /**/= 3,
        Rcp_A_Cell_Insp_Lower /**/= 4,

        Rcp_B_Cell_Insp_Upper /**/= 5,
        Rcp_B_Cell_Insp_Lower /**/= 6,
    }

    public class InspStageYAxisServo : ServoMotorControl
    {
        public InspStageYAxisServo(int innerAxisNo, int outterAxisNo, string name) :
            base(innerAxisNo, outterAxisNo, name)
        {

            PcToCtrllerPositionScale = 1000;
            SoftMinusLimit = 0;
            SoftPlusLimit = 250;
            SoftSpeedLimit = 500;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 100;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

        }
        protected override ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if ((int)EmInspStageYAxisServo.Rcp_A_Cell_Insp_Upper == posiNo || (int)EmInspStageYAxisServo.Rcp_A_Cell_Insp_Lower == posiNo ||
                (int)EmInspStageYAxisServo.Rcp_B_Cell_Insp_Upper == posiNo || (int)EmInspStageYAxisServo.Rcp_B_Cell_Insp_Lower == posiNo)
            {
                PortInfo port = opt as PortInfo;
                if (port == null)
                {
                    InterLockMgr.AddInterLock("Insp_Stage_Y_Rep 이동 명령시, Port 정보가 없습니다. ");
                    return null;
                }

                ServoPosiInfo info = GetCalPosition(EmInspStageYAxisServo.Rcp_A_Cell_Insp_Upper, recp, null);
                ServoPosiInfo target = new ServoPosiInfo();
                if ((int)EmInspStageYAxisServo.Rcp_A_Cell_Insp_Upper == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP A CELL LEFT TOP Y 위치 {0}  ", recp.ALeftTop.Y),
                        Position = (float)recp.ALeftTop.Y,
                        Speed = (float)recp.AYSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspStageYAxisServo.Rcp_A_Cell_Insp_Lower == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP A CELL LEFT BOT Y 위치 {0}  ", recp.ALeftBot.Y),
                        Position = (float)recp.ALeftBot.Y,
                        Speed = (float)recp.AYSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspStageYAxisServo.Rcp_B_Cell_Insp_Upper == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP B CELL LEFT TOP Y 위치 {0}  ", recp.BLeftTop.Y),
                        Position = (float)recp.BLeftTop.Y,
                        Speed = (float)recp.BYSpeed,
                        Accel = info.Accel,
                    };
                }
                else if ((int)EmInspStageYAxisServo.Rcp_B_Cell_Insp_Lower == posiNo)
                {
                    target = new ServoPosiInfo()
                    {
                        Name = string.Format("INSP B CELL LEFT BOT Y 위치 {0}  ", recp.BLeftBot.Y),
                        Position = (float)recp.BLeftBot.Y,
                        Speed = (float)recp.BYSpeed,
                        Accel = info.Accel,
                    };
                }
                return target;
            }
            else
            {
                return base.GetCalPosition(posiNo, recp, opt);
            }
        }
        public bool IsInPosition(EmInspStageYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        public ServoPosiInfo GetCalPosition(EmInspStageYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return GetCalPosition((int)posiNo, recp, opt);
        }
        public bool PtpMoveCmd(Equipment equip, EmInspStageYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmd(equip, (int)posiNo, recp, opt);
        }
        public bool PtpMoveCmdSync(Equipment equip, EmInspStageYAxisServo posiNo, EqpRecipe recp, object opt)
        {
            return PtpMoveCmdSync(equip, (int)posiNo, recp, opt);
        }
    }
}

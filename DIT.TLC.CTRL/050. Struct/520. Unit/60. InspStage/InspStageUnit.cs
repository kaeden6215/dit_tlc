﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public class InspStageUnit : BaseUnit
    {
        public enum EmInspStageHomeStep
        {
            H0000,
            H1010,
            H1020,
            H5999_HOME_COMPLETE,
        }

        public enum EmInspStageUnitCmd
        {
            S000_CELL_LOAD_STRAT,
            S0010_,
            S0020_,
            S0030_,
            S0040_,

            S1000_INSP_START,
            S01020_,
            S01030_,
            S01040_,
            S01050_,
            S01060_,
            S01070_,
            S01080_,

            S02010_CELL_UNLOAD_START,
            S02010_,
            S02020_,
            S02030_,
            S02040_,
            S02050_,
            S02060_,


        }

        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();


        public InspStageYAxisServo YAxis { get; set; }

        public Cylinder UpDown1Cylinder = new Cylinder();
        public Cylinder UpDown2Cylinder = new Cylinder();

        public PlcAddr HandPressureValue1 { get; internal set; }
        public PlcAddr HandPressureValue2 { get; internal set; }

        public EmInspStageUnitCmd StepCmd { get; set; }

        private InspCameraUnit InspectionCamera = null;

        public void InitializeInterLock()
        {
            if (InspectionCamera == null) InspectionCamera = GG.Equip.GetUnit(EmUnitPosi.None, EmUnitType.InspCamera) as InspCameraUnit;

            Vaccum1.Simulaotr = GG.VacuumSimul;
            Vaccum2.Simulaotr = GG.VacuumSimul;

            YAxis.CheckStartMoveInterLockFunc = YAxis_CheckStartMoveInterLockFunc;

            SetStepSwith( "HomeSeqLogicWorking");
            SetStepSwith( "CellLdLogicWorking");
            SetStepSwith( "CellInspectionLogicWorking");
            SetStepSwith( "CellUldLogicWorking");

        }
        public override void LogicWorking(Equipment equip)
        {

            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);
            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);

            YAxis.LogicWorking(equip);

            //안전 관련 절대 삭제 금지. 
            if (GG.NotUseStep == true) return;

            SeqLogicWorking(equip);
        }



        public void SeqLogicWorking(Equipment equip)
        {
            SeqStepStr = StepCmd.ToString();
            HomeStepStr = HomeStep.ToString();

            if (equip.RunMode == EmEquipRunMode.Stop)
            {
                //Step 초기화 필요.     
                StepCmd = EmInspStageUnitCmd.S000_CELL_LOAD_STRAT;
                 
                IsHomeComplete = false;
                HomeStep = EmInspStageHomeStep.H0000;
                return;
            }
            else if (equip.RunMode == EmEquipRunMode.Pause)
            {
                //일시 정지. 
                return;
            }

            if (this.IsHomeComplete == false)
            {
                if (equip.GetUnit(UnitPosi, EmUnitType.BeforeInspUnLoaderTransfer).IsHomeComplete && equip.GetUnit(UnitPosi, EmUnitType.AfterInspUnLoaderTransfer).IsHomeComplete)
                    if (GetStepSwith( "HomeSeqLogicWorking")) HomeSeqLogicWorking(equip);

                return;
            }

            if (equip.IsHomeComplete == false) return;

            if (GetStepSwith( "CellLdLogicWorking")) CellLdLogicWorking(equip);
            if (GetStepSwith( "CellInspectionLogicWorking")) CellInspectionLogicWorking(equip);
            if (GetStepSwith( "CellUldLogicWorking")) CellUldLogicWorking(equip);

        }

        InspResult pnl1Result = null;
        InspResult pnl2Result = null;
        public void CellLdLogicWorking(Equipment equip)
        {

            //글라스가 있을경우. 넘어감. 추가 필요. 
            if (StepCmd == EmInspStageUnitCmd.S000_CELL_LOAD_STRAT)
            {
                if (YAxis.PtpMoveCmdSync(equip, EmInspStageYAxisServo.Cell_Loading, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    PioRecv.YRecvAble = true;
                    StepCmd = EmInspStageUnitCmd.S0010_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S0010_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true)
                {
                    this.PanelSet = PioRecv.RecvPanelInfoSet;

                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;

                    StepCmd = EmInspStageUnitCmd.S0020_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S0020_)
            {
                if (PioRecv.XSendAble == true && PioRecv.XSendStart == true && PioRecv.XSendComplete == true)
                {


                    PioRecv.YRecvAble = true;
                    PioRecv.YRecvStart = true;
                    PioRecv.YRecvComplete = true;
                    StepCmd = EmInspStageUnitCmd.S0030_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S0030_)
            {
                if (PioRecv.XSendAble == false && PioRecv.XSendStart == false && PioRecv.XSendComplete == false)
                {
                    PioRecv.YRecvAble = false;
                    PioRecv.YRecvStart = false;
                    PioRecv.YRecvComplete = false;
                    StepCmd = EmInspStageUnitCmd.S0040_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S0040_)
            {
                StepCmd = EmInspStageUnitCmd.S1000_INSP_START;

            }
        }
        public void CellInspectionLogicWorking(Equipment equip)
        {
            if (StepCmd == EmInspStageUnitCmd.S1000_INSP_START)
            {
                EmInspStageYAxisServo PosiNo = UnitPosi == EmUnitPosi.ACol ? EmInspStageYAxisServo.Rcp_A_Cell_Insp_Upper : EmInspStageYAxisServo.Rcp_B_Cell_Insp_Upper;
                if (YAxis.PtpMoveCmd(equip, PosiNo, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    InspectionCamera = equip.GetUnit(this.UnitPosi, EmUnitType.InspCamera) as InspCameraUnit;
                    InspectionCamera.InspCameraReady(this.UnitPosi, EmPanelPosi.Top);
                    StepCmd = EmInspStageUnitCmd.S01020_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S01020_)
            {
                EmInspStageYAxisServo PosiNo = UnitPosi == EmUnitPosi.ACol ? EmInspStageYAxisServo.Rcp_A_Cell_Insp_Upper : EmInspStageYAxisServo.Rcp_B_Cell_Insp_Upper;
                if (YAxis.IsInPosition(PosiNo, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    if (InspectionCamera.InspCameraStart(this.PanelSet, this.UnitPosi, EmPanelPosi.Top))
                        StepCmd = EmInspStageUnitCmd.S01030_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S01030_)
            {
                if (InspectionCamera.IsInspCameraComplete(this.UnitPosi, this.PanelSet, EmPanelPosi.Bot, out pnl1Result, out pnl2Result) == true)
                {
                    equip.UD.InspCamera.ResetInspCamera();
                    StepCmd = EmInspStageUnitCmd.S01040_;
                }
            }
            if (StepCmd == EmInspStageUnitCmd.S01040_)
            {
                EmInspStageYAxisServo PosiNo = UnitPosi == EmUnitPosi.ACol ? EmInspStageYAxisServo.Rcp_A_Cell_Insp_Lower : EmInspStageYAxisServo.Rcp_B_Cell_Insp_Lower;
                if (YAxis.PtpMoveCmd(equip, PosiNo, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    InspectionCamera = equip.GetUnit(this.UnitPosi, EmUnitType.InspCamera) as InspCameraUnit;
                    InspectionCamera.InspCameraReady(this.UnitPosi, EmPanelPosi.Bot);
                    StepCmd = EmInspStageUnitCmd.S01050_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S01050_)
            {
                EmInspStageYAxisServo PosiNo = UnitPosi == EmUnitPosi.ACol ? EmInspStageYAxisServo.Rcp_A_Cell_Insp_Lower : EmInspStageYAxisServo.Rcp_B_Cell_Insp_Lower;
                if (YAxis.IsInPosition(PosiNo, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    if (InspectionCamera.InspCameraStart(this.PanelSet, this.UnitPosi, EmPanelPosi.Bot))
                        StepCmd = EmInspStageUnitCmd.S01060_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S01060_)
            {
                if (InspectionCamera.IsInspCameraComplete(this.UnitPosi, this.PanelSet, EmPanelPosi.Bot, out pnl1Result, out pnl2Result) == true)
                {
                    equip.UD.InspCamera.ResetInspCamera();
                    StepCmd = EmInspStageUnitCmd.S01070_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S01070_)
            {
                if (pnl1Result.Result != 1 || pnl1Result.Judgement != 1)
                {
                    if (pnl1Result.Result != 1)
                    {
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0758_INSPECTION_FAIL);
                    }
                    if (pnl1Result.Judgement != 1)
                    {
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0759_INSPECTION_NG);
                    }
                    return;
                }
                if (pnl2Result.Result != 1 || pnl2Result.Judgement != 1)
                {
                    if (pnl2Result.Result != 1)
                    {
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0758_INSPECTION_FAIL);
                    }
                    if (pnl2Result.Judgement != 1)
                    {
                        AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_0759_INSPECTION_NG);
                    }
                    return;
                }

                this.PanelSet.Panel1.InspResult = pnl1Result;
                this.PanelSet.Panel2.InspResult = pnl2Result;
                StepCmd = EmInspStageUnitCmd.S01080_;
            }
            else if (StepCmd == EmInspStageUnitCmd.S01080_)
            {
                StepCmd = EmInspStageUnitCmd.S02010_CELL_UNLOAD_START;
            }
        }
        public void CellUldLogicWorking(Equipment equip)
        {
            if (StepCmd == EmInspStageUnitCmd.S02010_CELL_UNLOAD_START)
            {
                if (YAxis.PtpMoveCmd(equip, EmInspStageYAxisServo.Cell_Unloading, equip.CurrentRecipe, this.PanelSet) == true)
                {
                    StepCmd = EmInspStageUnitCmd.S02020_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S02020_)
            {
                PioSend.SendPanelInfoSet = this.PanelSet;
                PioSend.YSendAble = true;
                StepCmd = EmInspStageUnitCmd.S02030_;
            }
            else if (StepCmd == EmInspStageUnitCmd.S02030_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;

                    StepCmd = EmInspStageUnitCmd.S02040_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S02040_)
            {
                if (PioSend.XRecvAble == true && PioSend.XRecvStart == true && PioSend.XRecvComplete == true)
                {
                    PioSend.YSendAble = true;
                    PioSend.YSendStart = true;
                    PioSend.YSendComplete = true;

                    StepCmd = EmInspStageUnitCmd.S02050_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S02050_)
            {
                if (PioSend.XRecvAble == false && PioSend.XRecvStart == false && PioSend.XRecvComplete == false)
                {
                    this.PanelSet.ClearPanel();

                    PioSend.YSendAble = false;
                    PioSend.YSendStart = false;
                    PioSend.YSendComplete = false;

                    StepCmd = EmInspStageUnitCmd.S02060_;
                }
            }
            else if (StepCmd == EmInspStageUnitCmd.S02060_)
            {
                StepCmd = EmInspStageUnitCmd.S000_CELL_LOAD_STRAT;
            }
        }

        //각 장치별 인터락
        public bool YAxis_CheckStartMoveInterLockFunc(Equipment equip, float position, float speed, emServoMoveType svMoveType)
        {
            BeforeInspUnLoaderTransferUnit beforeInspUnLoaderTr = equip.GetUnit(this.UnitPosi, EmUnitType.BeforeInspUnLoaderTransfer) as BeforeInspUnLoaderTransferUnit;
            AfterInspUnLoaderTransferUnit afterInspUnLoaderTr = equip.GetUnit(this.UnitPosi, EmUnitType.AfterInspUnLoaderTransfer) as AfterInspUnLoaderTransferUnit;


            if (IsExitsPanel1 == true && Vaccum1.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} InspStage YAxis 이동 불가, 배큠1이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }
            if (IsExitsPanel2 == true && Vaccum2.IsOnOff != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} InspStage YAxis 이동 불가, 배큠2이 ON되어 있지 않습니다.", UnitPosi));
                return true;
            }


            if (beforeInspUnLoaderTr.UpDown1Cylinder.IsBackward != true && beforeInspUnLoaderTr.Y1Axis.IsInPosition(EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} InspStage  YAxis 이동 불가, {0} BeforeInspUnLoaderTr의의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (beforeInspUnLoaderTr.UpDown2Cylinder.IsBackward != true && beforeInspUnLoaderTr.Y1Axis.IsInPosition(EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} InspStage  YAxis 이동 불가, {0} BeforeInspUnLoaderTr의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (afterInspUnLoaderTr.UpDown1Cylinder.IsBackward != true && afterInspUnLoaderTr.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} InspStage  YAxis 이동 불가, {0} AfterInspUnLoaderTr의 UpDown1Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            if (afterInspUnLoaderTr.UpDown2Cylinder.IsBackward != true && afterInspUnLoaderTr.Y2Axis.IsInPosition(EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading, equip.CurrentRecipe, null) != true)
            {
                //인터락 처리.                 
                InterLockMgr.AddInterLock(string.Format("{0} InspStage  YAxis 이동 불가, {0} AfterInspUnLoaderTr의 UpDown2Cylinder가 Up 상태가 아닙니다.", UnitPosi));
                return true;
            }

            return false;
        }


        public bool Y12AxisPtpMoveCmd(Equipment equip, EmInspStageYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            return true;
        }
        public bool Y12AxisIsInPosition(EmInspStageYAxisServo posiNo, EqpRecipe recp, object opt, EmCellPosi cellPosi)
        {
            //
            if (cellPosi == EmCellPosi.One_1)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            else if (cellPosi == EmCellPosi.One_1)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                //Axis1 대기 위치. 대기 위치. 
            }
            return true;
        }

        public bool VaccumOnOff(Equipment equip, bool onOff, EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                Vaccum1.OnOff(equip, onOff);
                if (onOff == false) Blower1.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                Vaccum2.OnOff(equip, onOff);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                Vaccum1.OnOff(equip, onOff);
                Vaccum2.OnOff(equip, onOff);

                if (onOff == false) Blower1.OnOff(equip, true, 500);
                if (onOff == false) Blower2.OnOff(equip, true, 500);
            }
            else
            {
                throw new Exception("구현 오류");
            }

            return true;
        }
        public bool IsVaccumOn(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == true;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == true && Vaccum2.IsOnOff == true;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }
        public bool IsVaccumOff(EmCellPosi cellPosi)
        {
            if (cellPosi == EmCellPosi.One_1)
            {
                return Vaccum1.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.One_2)
            {
                return Vaccum2.IsOnOff == false;
            }
            else if (cellPosi == EmCellPosi.Two)
            {
                return Vaccum1.IsOnOff == false && Vaccum2.IsOnOff == false;
            }
            else
            {
                throw new Exception("구현 오류");
            }
        }

        //HOME 시퀀스 구성. 
        public EmInspStageHomeStep HomeStep = EmInspStageHomeStep.H1010;
        public void HomeSeqLogicWorking(Equipment equip)
        {
            if (HomeStep == EmInspStageHomeStep.H0000)
            {
                this.IsHomeComplete = false;
                if (this.UnitPosi == EmUnitPosi.ACol)
                {
                    if (equip.UD.AfterInspUnloaderTransfer_A.IsHomeComplete && equip.UD.BeforeInspUnloaderTransfer_A.IsHomeComplete)
                    {
                        HomeStep = EmInspStageHomeStep.H1010;
                    }
                }
                else if (this.UnitPosi == EmUnitPosi.BCol)
                {
                    if (equip.UD.AfterInspUnloaderTransfer_B.IsHomeComplete && equip.UD.BeforeInspUnloaderTransfer_B.IsHomeComplete)
                    {
                        HomeStep = EmInspStageHomeStep.H1010;
                    }
                }
            }
            else if (HomeStep == EmInspStageHomeStep.H1010)
            {
                YAxis.GoHome(equip);
                HomeStep = EmInspStageHomeStep.H1020;
            }
            else if (HomeStep == EmInspStageHomeStep.H1020)
            {
                if (YAxis.IsHomeOnPosition())
                {
                    HomeStep = EmInspStageHomeStep.H5999_HOME_COMPLETE;
                }
            }
            else if (HomeStep == EmInspStageHomeStep.H5999_HOME_COMPLETE)
            {
                this.IsHomeComplete = true; 
            }
        }
    }
}

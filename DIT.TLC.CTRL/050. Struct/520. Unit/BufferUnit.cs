﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._520._Unit
{
    public class BufferUnit : BaseUnit
    {

        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();

        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();

        public PlcAddr HandPressureValue1 { get; internal set; }

        public override void LogicWorking(Equipment equip)
        {
            Vaccum1.LogicWorking(equip);
            Vaccum2.LogicWorking(equip);

            Blower1.LogicWorking(equip);
            Blower2.LogicWorking(equip);            
        }
    }
}

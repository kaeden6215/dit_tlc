﻿using Dit.Framework.Log;
using Dit.FrameworkSingle.Net.SNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace DIT.TLC.CTRL
{
    public class LaserProxy
    {
        public SNetClinetSession TcpSession { get; set; }

        private Timer TmrCmdSender = new Timer(100);
        private Queue<string> queCmd = new Queue<string>(1000);
        //private Queue<string> queSendOrder = new Queue<string>(1000);

        //통신용 메모리 버퍼.
        private byte[] AllReciveDataBuffer = new byte[819200];
        public string IpAddress { get; set; }
        public int Port { get; set; }

        public enum ParameterName
        {
            ALERTFLASH,
            ALERTW,
            AUTOIP,
            BT1,
            BT2,
            BURST,
            BURSTMAX,
            BURST_SEC,
            CGSYNC,
            D1C,
            D1H,
            D2C,
            D2H,
            D3C,
            D3H,
            DHCP,
            DNS,
            EM,     // ExternalMode
            EP,     // UseEnhancedProtocol
            EXIT,
            F,      // AlarmList
            FACK,   // AlarmClear
            FV,
            GATEWAY,
            GUI,
            H1DP,
            H1H,
            H1T,
            H2DP,
            H2H,
            H2T,
            H3DP,
            H3H,
            H3T,
            HB,     // HeartBeat
            HH,
            HOSTNAME,
            HSN,
            HSV,
            IP,     // IP Address
            IPMAX,
            IPMIN,
            K,
            LASTIP,
            LM,
            LOCK,
            LOCKOUT,
            LRRB,
            MAC,
            MAINT,
            MAXIRPOWER,
            MOH,
            PATT,
            PATTSET,    // IR Power
            PM,     // PulseMode
            POUT,
            PRENABLE,   // CloseLoop
            PSCODE,
            PSID,
            QUIT,
            RL,     // RFPercentLevel
            RLMAX,
            RMAINT,
            RR,
            RRAMP,
            RRAMPSET,   // RepetitionRate
            RRD,        // AOM2 Divisor
            RREFFMAX,
            RRMAX,
            RRMIN,
            S,      // OpenShutter
            SCI,    // ShutterControlInput
            SCOI,   // ShutterControlOutput
            SH,
            SMOD,   // ShutterOperationMode
            START,
            STATE,  // Laser State
            STOP,
            SUBNET,
            SYNC1DT,
            SYNC1DW,
            SYNC2DT,
            SYNC2DW,
            TATT,       // Transmission
            TEST,
            TIME,
            TimeZone,
            USB,
            W,
            WF,
            WT,
            PD7,
            EndNum
        }

        //private const string defCmdGetAll = "?ALL\r?PD7P\r?D1H\r?D2H\r?D3H\r\n"; // >>1.6Ver
        private const string defCmdGetAll = "?ALL\r?PD7P\r\n"; // >>1.3Ver

        public class LaserParameterValues
        {
            public int AlertFlash { get; set; }
            public int AlertW { get; set; }
            public int AutoIP { get; set; }
            public double BT1 { get; set; }
            public double BT2 { get; set; }
            public int Burst { get; set; }
            public int BurstMax { get; set; }
            public int BurstSec { get; set; }
            public int CgSync { get; set; }
            public double D1C { get; set; }
            public double D1H { get; set; }
            public double D2C { get; set; }
            public double D2H { get; set; }
            public double D3C { get; set; }
            public double D3H { get; set; }
            public int DHCP { get; set; }
            public int DNS { get; set; }
            public int EM { get; set; } // ExternalMode
            public int EP { get; set; } // EnhancedProtocol
            public string F { get; set; }
            public string FV { get; set; }
            public string Gateway { get; set; }
            public string GUI { get; set; }
            public double H1DP { get; set; }
            public double H1H { get; set; }
            public double H1T { get; set; }
            public double H2DP { get; set; }
            public double H2H { get; set; }
            public double H2T { get; set; }
            public double H3DP { get; set; }
            public double H3H { get; set; }
            public double H3T { get; set; }
            public int HB { get; set; }
            public double HH { get; set; }
            public string HostName { get; set; }
            public string HSN { get; set; }
            public string HSV { get; set; }
            public string IP { get; set; }
            public string IPMax { get; set; }
            public string IPMin { get; set; }
            public int K { get; set; }
            public string LastIP { get; set; }
            public string LM { get; set; }
            public string Lock { get; set; }
            public string Lockout { get; set; } // JICHA [20181030] - 메뉴얼상 set할 때는 int인데, 받을 때는 string.. 테스트할 때 확인해볼 것.
            public string LRRB { get; set; }
            public string MAC { get; set; }
            public int Maint { get; set; }
            public double MaxIRPower { get; set; }
            public double MOH { get; set; }
            public double PATT { get; set; }
            public double PATTSET { get; set; } // IR Power
            public int PM { get; set; } // PulseMode
            public double POut { get; set; }
            public int PRENABLE { get; set; } // CloseLoopStatus
            public int PsCode { get; set; }
            public int PsID { get; set; }
            public double RL { get; set; } // RFPercentLevel
            public double RLMax { get; set; }
            public int RMaint { get; set; }
            public double RR { get; set; }
            public double RRAmp { get; set; }
            public double RRAmpSet { get; set; } // RepetitionRate
            public int RRD { get; set; } // AOM2Divisor
            public double RREffMax { get; set; }
            public double RRMax { get; set; }
            public double RRMin { get; set; }
            public int S { get; set; } // ShutterOpenStatus
            public int SCI { get; set; } // ShutterControlInput
            public int SCOI { get; set; } // ShutterControlOutput
            public string SH { get; set; }
            public int SMOD { get; set; } // ShutterOperationMode
            public int State { get; set; }
            public string Subnet { get; set; }
            public int Sync1DT { get; set; }
            public int Sync1DW { get; set; }
            public int Sync2DT { get; set; }
            public int Sync2DW { get; set; }
            public double TATT { get; set; } // Transmission
            public string TEST { get; set; } // 메뉴얼에는 없으나, ?ALL에 대한 응답에는 들어가 있는 항목.
            public string Time { get; set; }
            public string TimeZone { get; set; }
            public string USB { get; set; }
            public string W { get; set; }
            public double WF { get; set; }
            public double WT { get; set; }
            public double PD7 { get; set; }
        }

        public class LaserParameterRecvState
        {
            public bool IsRecvCompleteAlertFlash { get; set; }
            public bool IsRecvCompleteAlertW { get; set; }
            public bool IsRecvCompleteAutoIP { get; set; }
            public bool IsRecvCompleteBT1 { get; set; }
            public bool IsRecvCompleteBT2 { get; set; }
            public bool IsRecvCompleteBurst { get; set; }
            public bool IsRecvCompleteBurstMax { get; set; }
            public bool IsRecvCompleteBurstSec { get; set; }
            public bool IsRecvCompleteCgSync { get; set; }
            public bool IsRecvCompleteD1C { get; set; }
            public bool IsRecvCompleteD1H { get; set; }
            public bool IsRecvCompleteD2C { get; set; }
            public bool IsRecvCompleteD2H { get; set; }
            public bool IsRecvCompleteD3C { get; set; }
            public bool IsRecvCompleteD3H { get; set; }
            public bool IsRecvCompleteDHCP { get; set; }
            public bool IsRecvCompleteDNS { get; set; }
            public bool IsRecvCompleteEM { get; set; } // ExternalMode
            public bool IsRecvCompleteEP { get; set; } // EnhancedProtocol
            public bool IsRecvCompleteF { get; set; }
            public bool IsRecvCompleteFV { get; set; }
            public bool IsRecvCompleteGateway { get; set; }
            public bool IsRecvCompleteGUI { get; set; }
            public bool IsRecvCompleteH1DP { get; set; }
            public bool IsRecvCompleteH1H { get; set; }
            public bool IsRecvCompleteH1T { get; set; }
            public bool IsRecvCompleteH2DP { get; set; }
            public bool IsRecvCompleteH2H { get; set; }
            public bool IsRecvCompleteH2T { get; set; }
            public bool IsRecvCompleteH3DP { get; set; }
            public bool IsRecvCompleteH3H { get; set; }
            public bool IsRecvCompleteH3T { get; set; }
            public bool IsRecvCompleteHB { get; set; } // HeartBeat
            public bool IsRecvCompleteHH { get; set; }
            public bool IsRecvCompleteHostName { get; set; }
            public bool IsRecvCompleteHSN { get; set; }
            public bool IsRecvCompleteHSV { get; set; }
            public bool IsRecvCompleteIP { get; set; }
            public bool IsRecvCompleteIPMax { get; set; }
            public bool IsRecvCompleteIPMin { get; set; }
            public bool IsRecvCompleteK { get; set; }
            public bool IsRecvCompleteLastIP { get; set; }
            public bool IsRecvCompleteLM { get; set; }
            public bool IsRecvCompleteLock { get; set; }
            public bool IsRecvCompleteLockout { get; set; }
            public bool IsRecvCompleteLRRB { get; set; }
            public bool IsRecvCompleteMAC { get; set; }
            public bool IsRecvCompleteMaint { get; set; }
            public bool IsRecvCompleteMaxIRPower { get; set; }
            public bool IsRecvCompleteMOH { get; set; }
            public bool IsRecvCompletePATT { get; set; }
            public bool IsRecvCompletePATTSET { get; set; } // IR Power
            public bool IsRecvCompletePM { get; set; } // Pulse Mode
            public bool IsRecvCompletePOut { get; set; }
            public bool IsRecvCompletePRENABLE { get; set; } // CloseLoopStatus
            public bool IsRecvCompletePsCode { get; set; }
            public bool IsRecvCompletePsID { get; set; }
            public bool IsRecvCompleteRL { get; set; } // RF Percent Level
            public bool IsRecvCompleteRLMax { get; set; }
            public bool IsRecvCompleteRMaint { get; set; }
            public bool IsRecvCompleteRR { get; set; }
            public bool IsRecvCompleteRRAMP { get; set; }
            public bool IsRecvCompleteRRAMPSET { get; set; } // RepetitionRate
            public bool IsRecvCompleteRRD { get; set; } // AOM2Divisor
            public bool IsRecvCompleteRREFFMAX { get; set; }
            public bool IsRecvCompleteRRMAX { get; set; }
            public bool IsRecvCompleteRRMIN { get; set; }
            public bool IsRecvCompleteS { get; set; } // Shutter Open Status
            public bool IsRecvCompleteSCI { get; set; } // Shutter Control Input
            public bool IsRecvCompleteSCOI { get; set; } // Shutter Control Output
            public bool IsRecvCompleteSH { get; set; }
            public bool IsRecvCompleteSMOD { get; set; } // Shutter Operation Mode
            public bool IsRecvCompleteState { get; set; }
            public bool IsRecvCompleteSubnet { get; set; }
            public bool IsRecvCompleteSync1DT { get; set; }
            public bool IsRecvCompleteSync1DW { get; set; }
            public bool IsRecvCompleteSync2DT { get; set; }
            public bool IsRecvCompleteSync2DW { get; set; }
            public bool IsRecvCompleteTATT { get; set; } // Transmission
            public bool IsRecvCompleteTEST { get; set; }
            public bool IsRecvCompleteTime { get; set; }
            public bool IsRecvCompleteTimeZone { get; set; }
            public bool IsRecvCompleteUSB { get; set; }
            public bool IsRecvCompleteW { get; set; }
            public bool IsRecvCompleteWF { get; set; }
            public bool IsRecvCompleteWT { get; set; }
            public bool IsRecvCompletePD7 { get; set; }
        }

        public LaserParameterValues _laserParameterValues = new LaserParameterValues();
        public LaserParameterRecvState _laserParameterRecvState = new LaserParameterRecvState();
        private string[] _sLaserParamValue = new string[(int)ParameterName.EndNum];

        public LaserProxy(string ip, int port)
        {
            TcpSession = new SNetClinetSession();
            TcpSession.IpAddress = ip;
            TcpSession.Port = port;

            TcpSession.OnConnect += TcpSession_OnConnect;
            TcpSession.OnClose += TcpSession_OnClose;
            TcpSession.OnError += TcpSession_OnError;
            TcpSession.OnReceive += TcpSession_OnReceive;
            TcpSession.Spliter = new byte[] { 0x0D };
        }

        private void TcpSession_OnReceive(object sender, SNetReceiveEventArgs e)
        {
            int nTemp = 0;
            double dTemp = 0;
            string oriValue = Encoding.ASCII.GetString(e.Buffer), sTemp = string.Empty;
            oriValue = oriValue.Substring(1, oriValue.Length-1);
            if (oriValue.Contains("HyperNX> ") && oriValue.Contains("AUTOIP")) oriValue = oriValue.Substring(9, oriValue.Length - 9);
            string[] aryDivideValue = oriValue.Split('=');

            string[] strTemp = new string[(int)ParameterName.EndNum];
            bool[] bTemp = new bool[(int)ParameterName.EndNum];
            ParameterName Ind = new ParameterName();
            for (Ind = ParameterName.ALERTFLASH; Ind <= ParameterName.EndNum; Ind++)  // Ver 1.8.4 기준 85개
            {
                if (string.Compare(aryDivideValue[0], Ind.ToString()) == 0)
                {
                    _sLaserParamValue[(int)Ind] = aryDivideValue[1];
                    bTemp[(int)Ind] = true;
                    break;
                }
            }

            switch (Ind)
            {
                case ParameterName.ALERTFLASH: {    if (!int.TryParse(_sLaserParamValue[(int)ParameterName.ALERTFLASH], out nTemp)) { nTemp = 0; } _laserParameterValues.AlertFlash = nTemp; _laserParameterRecvState.IsRecvCompleteAlertFlash = true; } break;
                case ParameterName.ALERTW: {        if (!int.TryParse(_sLaserParamValue[(int)ParameterName.ALERTW], out nTemp)) { nTemp = 0; } _laserParameterValues.AlertW = nTemp; _laserParameterRecvState.IsRecvCompleteAlertW = true; } break;
                case ParameterName.AUTOIP: {        if (!int.TryParse(_sLaserParamValue[(int)ParameterName.AUTOIP], out nTemp)) { nTemp = 0; } _laserParameterValues.AutoIP = nTemp; _laserParameterRecvState.IsRecvCompleteAutoIP = true; } break;
                case ParameterName.BT1: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.BT1], out dTemp)) { dTemp = 0; } _laserParameterValues.BT1 = dTemp; _laserParameterRecvState.IsRecvCompleteBT1 = true; } break;
                case ParameterName.BT2: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.BT2], out dTemp)) { dTemp = 0; } _laserParameterValues.BT2 = dTemp; _laserParameterRecvState.IsRecvCompleteBT2 = true; } break;
                case ParameterName.BURST: {         if (!int.TryParse(_sLaserParamValue[(int)ParameterName.BURST], out nTemp)) { nTemp = 0; } _laserParameterValues.Burst = nTemp; _laserParameterRecvState.IsRecvCompleteBurst = true; } break;
                case ParameterName.BURSTMAX: {      if (!int.TryParse(_sLaserParamValue[(int)ParameterName.BURSTMAX], out nTemp)) { nTemp = 0; } _laserParameterValues.BurstMax = nTemp; _laserParameterRecvState.IsRecvCompleteBurstMax = true; } break;
                case ParameterName.BURST_SEC: {     if (!int.TryParse(_sLaserParamValue[(int)ParameterName.BURST_SEC], out nTemp)) { nTemp = 0; } _laserParameterValues.BurstSec = nTemp; _laserParameterRecvState.IsRecvCompleteBurstSec = true; } break;
                case ParameterName.CGSYNC: {        if (!int.TryParse(_sLaserParamValue[(int)ParameterName.CGSYNC], out nTemp)) { nTemp = 0; } _laserParameterValues.CgSync = nTemp; _laserParameterRecvState.IsRecvCompleteCgSync = true; } break;
                case ParameterName.D1C: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.D1C], out dTemp)) { dTemp = 0; } _laserParameterValues.D1C = dTemp; _laserParameterRecvState.IsRecvCompleteD1C = true; } break;
                case ParameterName.D1H: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.D1H], out dTemp)) { nTemp = 0; } _laserParameterValues.D1H = dTemp; _laserParameterRecvState.IsRecvCompleteD1H = true; } break;
                case ParameterName.D2C: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.D2C], out dTemp)) { nTemp = 0; } _laserParameterValues.D2C = dTemp; _laserParameterRecvState.IsRecvCompleteD2C = true; } break;
                case ParameterName.D2H: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.D2H], out dTemp)) { nTemp = 0; } _laserParameterValues.D2H = dTemp; _laserParameterRecvState.IsRecvCompleteD2H = true; } break;
                case ParameterName.D3C: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.D3C], out dTemp)) { nTemp = 0; } _laserParameterValues.D3C = dTemp; _laserParameterRecvState.IsRecvCompleteD3C = true; } break;
                case ParameterName.D3H: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.D3H], out dTemp)) { nTemp = 0; } _laserParameterValues.D3H = dTemp; _laserParameterRecvState.IsRecvCompleteD3H = true; } break;
                case ParameterName.DHCP: {          if (!int.TryParse(_sLaserParamValue[(int)ParameterName.DHCP], out nTemp)) { nTemp = 0; } _laserParameterValues.DHCP = nTemp; _laserParameterRecvState.IsRecvCompleteDHCP = true; } break;
                case ParameterName.DNS: {           if (!int.TryParse(_sLaserParamValue[(int)ParameterName.DNS], out nTemp)) { nTemp = 0; } _laserParameterValues.DNS = nTemp; _laserParameterRecvState.IsRecvCompleteDNS = true; } break;
                case ParameterName.EM: {            if (!int.TryParse(_sLaserParamValue[(int)ParameterName.EM], out nTemp)) { nTemp = 0; } _laserParameterValues.EM = nTemp; _laserParameterRecvState.IsRecvCompleteEM = true; } break;
                case ParameterName.EP: {            if (!int.TryParse(_sLaserParamValue[(int)ParameterName.EP], out nTemp)) { nTemp = 0; } _laserParameterValues.EP = nTemp; _laserParameterRecvState.IsRecvCompleteEP = true; } break;
                case ParameterName.F: {             _laserParameterValues.F = _sLaserParamValue[(int)ParameterName.F]; _laserParameterRecvState.IsRecvCompleteF = true; } break;
                case ParameterName.FV: {            _laserParameterValues.FV = _sLaserParamValue[(int)ParameterName.FV]; _laserParameterRecvState.IsRecvCompleteFV = true; } break;
                case ParameterName.GATEWAY: {       _laserParameterValues.Gateway = _sLaserParamValue[(int)ParameterName.GATEWAY]; _laserParameterRecvState.IsRecvCompleteGateway = true; } break;
                case ParameterName.GUI: {           _laserParameterValues.GUI = _sLaserParamValue[(int)ParameterName.GUI]; _laserParameterRecvState.IsRecvCompleteGUI = true; } break;
                case ParameterName.H1DP: {          if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H1DP], out dTemp)) { dTemp = 0; } _laserParameterValues.H1DP = dTemp; _laserParameterRecvState.IsRecvCompleteH1DP = true; } break;
                case ParameterName.H1H: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H1H], out dTemp)) { dTemp = 0; } _laserParameterValues.H1H = dTemp; _laserParameterRecvState.IsRecvCompleteH1H = true; } break;
                case ParameterName.H1T: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H1T], out dTemp)) { dTemp = 0; } _laserParameterValues.H1T = dTemp; _laserParameterRecvState.IsRecvCompleteH1T = true; } break;
                case ParameterName.H2DP: {          if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H2DP], out dTemp)) { dTemp = 0; } _laserParameterValues.H2DP = dTemp; _laserParameterRecvState.IsRecvCompleteH2DP = true; } break;
                case ParameterName.H2H: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H2H], out dTemp)) { dTemp = 0; } _laserParameterValues.H2H = dTemp; _laserParameterRecvState.IsRecvCompleteH2H = true; } break;
                case ParameterName.H2T: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H2T], out dTemp)) { dTemp = 0; } _laserParameterValues.H2T = dTemp; _laserParameterRecvState.IsRecvCompleteH2T = true; } break;
                case ParameterName.H3DP: {          if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H3DP], out dTemp)) { dTemp = 0; } _laserParameterValues.H3DP = dTemp; _laserParameterRecvState.IsRecvCompleteH2DP = true; } break;
                case ParameterName.H3H: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H3H], out dTemp)) { dTemp = 0; } _laserParameterValues.H3H = dTemp; _laserParameterRecvState.IsRecvCompleteH3H = true; } break;
                case ParameterName.H3T: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.H3T], out dTemp)) { dTemp = 0; } _laserParameterValues.H3T = dTemp; _laserParameterRecvState.IsRecvCompleteH3T = true; } break;
                case ParameterName.HB: {            if (!int.TryParse(_sLaserParamValue[(int)ParameterName.HB], out nTemp)) { nTemp = 0; } _laserParameterValues.HB = nTemp; _laserParameterRecvState.IsRecvCompleteHB = true; } break;
                case ParameterName.HH: {            if (!double.TryParse(_sLaserParamValue[(int)ParameterName.HH], out dTemp)) { dTemp = 0; } _laserParameterValues.HH = dTemp; _laserParameterRecvState.IsRecvCompleteHH = true; } break;
                case ParameterName.HOSTNAME: {      _laserParameterValues.HostName = _sLaserParamValue[(int)ParameterName.HOSTNAME]; _laserParameterRecvState.IsRecvCompleteHostName = true; } break;
                case ParameterName.HSN: {           _laserParameterValues.HSN = _sLaserParamValue[(int)ParameterName.HSN]; _laserParameterRecvState.IsRecvCompleteHSN = true; } break;
                case ParameterName.HSV: {           _laserParameterValues.HSV = _sLaserParamValue[(int)ParameterName.HSV]; _laserParameterRecvState.IsRecvCompleteHSV = true; } break;
                case ParameterName.IP: {            _laserParameterValues.IP = _sLaserParamValue[(int)ParameterName.IP]; _laserParameterRecvState.IsRecvCompleteIP = true; } break;
                case ParameterName.IPMAX: {         _laserParameterValues.IPMax = _sLaserParamValue[(int)ParameterName.IPMAX]; _laserParameterRecvState.IsRecvCompleteIPMax = true; } break;
                case ParameterName.IPMIN: {         _laserParameterValues.IPMin = _sLaserParamValue[(int)ParameterName.IPMIN]; _laserParameterRecvState.IsRecvCompleteIPMin = true; } break;
                case ParameterName.K: {             if (!int.TryParse(_sLaserParamValue[(int)ParameterName.K], out nTemp)) { nTemp = 0; } _laserParameterValues.K = nTemp; } break;
                case ParameterName.LASTIP: {        _laserParameterValues.LastIP = _sLaserParamValue[(int)ParameterName.LASTIP]; _laserParameterRecvState.IsRecvCompleteLastIP = true; } break;
                case ParameterName.LM: {            _laserParameterValues.LM = _sLaserParamValue[(int)ParameterName.LM]; _laserParameterRecvState.IsRecvCompleteLM = true; } break;
                case ParameterName.LOCK: {          _laserParameterValues.Lock = _sLaserParamValue[(int)ParameterName.LOCK]; _laserParameterRecvState.IsRecvCompleteLock = true; } break;
                case ParameterName.LOCKOUT: {       _laserParameterValues.Lockout = _sLaserParamValue[(int)ParameterName.LOCKOUT]; _laserParameterRecvState.IsRecvCompleteLockout = true; } break;
                case ParameterName.LRRB: {          _laserParameterValues.LRRB = _sLaserParamValue[(int)ParameterName.LRRB]; _laserParameterRecvState.IsRecvCompleteLRRB = true; } break;
                case ParameterName.MAC: {           _laserParameterValues.MAC = _sLaserParamValue[(int)ParameterName.MAC]; _laserParameterRecvState.IsRecvCompleteMAC = true; } break;
                case ParameterName.MAINT: {         if (!int.TryParse(_sLaserParamValue[(int)ParameterName.MAINT], out nTemp)) { nTemp = 0; } _laserParameterValues.Maint = nTemp; _laserParameterRecvState.IsRecvCompleteMaint = true; } break;
                case ParameterName.MAXIRPOWER: {    if (!double.TryParse(_sLaserParamValue[(int)ParameterName.MAXIRPOWER], out dTemp)) { dTemp = 0; } _laserParameterValues.MaxIRPower = dTemp; _laserParameterRecvState.IsRecvCompleteMaxIRPower = true; } break;
                case ParameterName.MOH: {           if (!double.TryParse(_sLaserParamValue[(int)ParameterName.MOH], out dTemp)) { dTemp = 0; } _laserParameterValues.MOH = dTemp; _laserParameterRecvState.IsRecvCompleteMOH = true; } break;
                case ParameterName.PATT: {          if (!double.TryParse(_sLaserParamValue[(int)ParameterName.PATT], out dTemp)) { dTemp = 0; } _laserParameterValues.PATT = dTemp; _laserParameterRecvState.IsRecvCompletePATT = true; } break;
                case ParameterName.PATTSET: {       if (!double.TryParse(_sLaserParamValue[(int)ParameterName.PATTSET], out dTemp)) { dTemp = 0; } _laserParameterValues.PATTSET = dTemp; _laserParameterRecvState.IsRecvCompletePATTSET = true; } break;
                case ParameterName.PM: {            if (!int.TryParse(_sLaserParamValue[(int)ParameterName.PM], out nTemp)) { nTemp = 0; } _laserParameterValues.PM = nTemp; _laserParameterRecvState.IsRecvCompletePM = true; } break;
                case ParameterName.POUT: {          if (!double.TryParse(_sLaserParamValue[(int)ParameterName.POUT], out dTemp)) { dTemp = 0; } _laserParameterValues.POut = dTemp; _laserParameterRecvState.IsRecvCompletePOut = true; } break;
                case ParameterName.PRENABLE: {      if (!int.TryParse(_sLaserParamValue[(int)ParameterName.PRENABLE], out nTemp)) { nTemp = 0; } _laserParameterValues.PRENABLE = nTemp; _laserParameterRecvState.IsRecvCompletePRENABLE = true; } break;
                case ParameterName.PSCODE: {        if (!int.TryParse(_sLaserParamValue[(int)ParameterName.PSCODE], out nTemp)) { nTemp = 0; } _laserParameterValues.PsCode = nTemp; _laserParameterRecvState.IsRecvCompletePsCode = true; } break;
                case ParameterName.PSID: {          if (!int.TryParse(_sLaserParamValue[(int)ParameterName.PSID], out nTemp)) { nTemp = 0; } _laserParameterValues.PsID = nTemp; _laserParameterRecvState.IsRecvCompletePsID = true; } break;
                case ParameterName.RL: {            if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RL], out dTemp)) { dTemp = 0; } _laserParameterValues.RL = dTemp; _laserParameterRecvState.IsRecvCompleteRL = true; } break;
                case ParameterName.RLMAX: {         if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RLMAX], out dTemp)) { dTemp = 0; } _laserParameterValues.RLMax = dTemp; _laserParameterRecvState.IsRecvCompleteRLMax = true; } break;
                case ParameterName.RMAINT: {        if (!int.TryParse(_sLaserParamValue[(int)ParameterName.RMAINT], out nTemp)) { nTemp = 0; } _laserParameterValues.RMaint = nTemp; _laserParameterRecvState.IsRecvCompleteRMaint = true; } break;
                case ParameterName.RR: {            if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RR], out dTemp)) { dTemp = 0; } _laserParameterValues.RR = dTemp; _laserParameterRecvState.IsRecvCompleteRR = true; } break;
                case ParameterName.RRAMP: {         if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RRAMP], out dTemp)) { dTemp = 0; } _laserParameterValues.RRAmp = dTemp; _laserParameterRecvState.IsRecvCompleteRRAMP = true; } break;
                case ParameterName.RRAMPSET: {      if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RRAMPSET], out dTemp)) { dTemp = 0; } _laserParameterValues.RRAmpSet = dTemp; _laserParameterRecvState.IsRecvCompleteRRAMPSET = true; } break;
                case ParameterName.RRD: {           if (!int.TryParse(_sLaserParamValue[(int)ParameterName.RRD], out nTemp)) { nTemp = 0; } _laserParameterValues.RRD = nTemp; _laserParameterRecvState.IsRecvCompleteRRD = true; } break;
                case ParameterName.RREFFMAX: {      if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RREFFMAX], out dTemp)) { dTemp = 0; } _laserParameterValues.RREffMax = dTemp; _laserParameterRecvState.IsRecvCompleteRREFFMAX = true; } break;
                case ParameterName.RRMAX: {         if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RRMAX], out dTemp)) { dTemp = 0; } _laserParameterValues.RRMax = dTemp; _laserParameterRecvState.IsRecvCompleteRRMAX = true; } break;
                case ParameterName.RRMIN: {         if (!double.TryParse(_sLaserParamValue[(int)ParameterName.RRMIN], out dTemp)) { dTemp = 0; } _laserParameterValues.RRMin = dTemp; _laserParameterRecvState.IsRecvCompleteRRMIN = true; } break;
                case ParameterName.S: {             if (!int.TryParse(_sLaserParamValue[(int)ParameterName.S], out nTemp)) { nTemp = 0; } _laserParameterValues.S = nTemp; _laserParameterRecvState.IsRecvCompleteS = true; } break;
                case ParameterName.SCI: {           if (!int.TryParse(_sLaserParamValue[(int)ParameterName.SCI], out nTemp)) { nTemp = 0; } _laserParameterValues.SCI = nTemp; _laserParameterRecvState.IsRecvCompleteSCI = true; } break;
                case ParameterName.SCOI: {          if (!int.TryParse(_sLaserParamValue[(int)ParameterName.SCOI], out nTemp)) { nTemp = 0; } _laserParameterValues.SCOI = nTemp; _laserParameterRecvState.IsRecvCompleteSCOI = true; } break;
                case ParameterName.SH: {            _laserParameterValues.SH = _sLaserParamValue[(int)ParameterName.SH]; _laserParameterRecvState.IsRecvCompleteSH = true; } break;
                case ParameterName.SMOD: {          if (!int.TryParse(_sLaserParamValue[(int)ParameterName.SMOD], out nTemp)) { nTemp = 0; } _laserParameterValues.SMOD = nTemp; _laserParameterRecvState.IsRecvCompleteSMOD = true; } break;
                case ParameterName.STATE: {         if (!int.TryParse(_sLaserParamValue[(int)ParameterName.STATE], out nTemp)) { nTemp = 0; } _laserParameterValues.State = nTemp; _laserParameterRecvState.IsRecvCompleteState = true; } break;
                case ParameterName.SUBNET: {        _laserParameterValues.Subnet = _sLaserParamValue[(int)ParameterName.SUBNET]; _laserParameterRecvState.IsRecvCompleteSubnet = true; } break;
                case ParameterName.SYNC1DT: {       if (!int.TryParse(_sLaserParamValue[(int)ParameterName.SYNC1DT], out nTemp)) { nTemp = 0; } _laserParameterValues.Sync1DT = nTemp; _laserParameterRecvState.IsRecvCompleteSync1DT = true; } break;
                case ParameterName.SYNC1DW: {       if (!int.TryParse(_sLaserParamValue[(int)ParameterName.SYNC1DW], out nTemp)) { nTemp = 0; } _laserParameterValues.Sync1DW = nTemp; _laserParameterRecvState.IsRecvCompleteSync1DW = true; } break;
                case ParameterName.SYNC2DT: {       if (!int.TryParse(_sLaserParamValue[(int)ParameterName.SYNC2DT], out nTemp)) { nTemp = 0; } _laserParameterValues.Sync2DT = nTemp; _laserParameterRecvState.IsRecvCompleteSync2DT = true; } break;
                case ParameterName.SYNC2DW: {       if (!int.TryParse(_sLaserParamValue[(int)ParameterName.SYNC2DW], out nTemp)) { nTemp = 0; } _laserParameterValues.Sync2DW = nTemp; _laserParameterRecvState.IsRecvCompleteSync2DW = true; } break;
                case ParameterName.TATT: {          if (!double.TryParse(_sLaserParamValue[(int)ParameterName.TATT], out dTemp)) { dTemp = 0; } _laserParameterValues.TATT = dTemp; _laserParameterRecvState.IsRecvCompleteTATT = true; } break;
                case ParameterName.TEST: {          _laserParameterValues.TEST = _sLaserParamValue[(int)ParameterName.TEST]; _laserParameterRecvState.IsRecvCompleteTEST = true; } break;
                case ParameterName.TIME: {          _laserParameterValues.Time = _sLaserParamValue[(int)ParameterName.TIME]; _laserParameterRecvState.IsRecvCompleteTime = true; } break;
                case ParameterName.TimeZone: {      _laserParameterValues.TimeZone = _sLaserParamValue[(int)ParameterName.TimeZone]; _laserParameterRecvState.IsRecvCompleteTimeZone = true; } break;
                case ParameterName.USB: {           _laserParameterValues.USB = _sLaserParamValue[(int)ParameterName.USB]; _laserParameterRecvState.IsRecvCompleteUSB = true; } break;
                case ParameterName.W: {             _laserParameterValues.W = _sLaserParamValue[(int)ParameterName.W]; _laserParameterRecvState.IsRecvCompleteW = true; } break;
                case ParameterName.WF: {            if (!double.TryParse(_sLaserParamValue[(int)ParameterName.WF], out dTemp)) { dTemp = 0; } _laserParameterValues.WF = dTemp; _laserParameterRecvState.IsRecvCompleteWF = true; } break;
                case ParameterName.WT: {            if (!double.TryParse(_sLaserParamValue[(int)ParameterName.WT], out dTemp)) { dTemp = 0; } _laserParameterValues.WT = dTemp; _laserParameterRecvState.IsRecvCompleteWT = true; } break;
                case ParameterName.EndNum: { } break;
                default:
                    {
                        string hyper = string.Format("HyperNX>");
                        string autoIP = string.Format("AUTOIP");

                        if (aryDivideValue[0].Contains(hyper) && !aryDivideValue[0].Contains(autoIP) && (aryDivideValue[0].Length < hyper.Length + 10))
                        {
                            string[] sVal = aryDivideValue[0].Split(' ');
                            double dVal = 0;
                            if (!double.TryParse(sVal[1], out dVal)) { dVal = -1; }
                            if (dVal > 1) _laserParameterValues.PD7 = dVal;
                            _laserParameterRecvState.IsRecvCompletePD7 = true;
                        }
                    }
                    break;
            }
        }

        private void TcpSession_OnError(object sender, SNetErrorEventArgs e)
        {
            Console.WriteLine("연결 종료됨");
        }

        private void TcpSession_OnClose(object sender, SNetConnectionEventArgs e)
        {
            TmrCmdSender.Stop();
            Console.WriteLine("연결 종료됨");
        }

        private void TcpSession_OnConnect(object sender, SNetConnectionEventArgs e)
        {
            TmrCmdSender.Start();
            Console.WriteLine("연결됨");
        }

        private void tmrCmdSender_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (queCmd.Count <= 0) return;
            if (!IsConnection) { queCmd.Clear(); return; }

            string sCmd = string.Empty;
            if (queCmd.Count() != 0)
            {
                sCmd = queCmd.Dequeue();
                //queSendOrder.Enqueue(sCmd);
                byte[] sendByte = Encoding.ASCII.GetBytes(sCmd);
                TcpSession.SendPacket(sendByte);
            }
            else
            {
                sCmd = string.Format("{0}", defCmdGetAll);
                //queSendOrder.Enqueue(sCmd);
                byte[] sendByte = Encoding.ASCII.GetBytes(sCmd);
                TcpSession.SendPacket(sendByte);
            }
        }

        public bool IsConnection
        {
            get
            {
                if (TcpSession == null) return false;
                return TcpSession.IsConnection;
            }
        }

        public void Start()
        {
            TcpSession.Start();
        }

        public void Stop()
        {
            TcpSession.Stop();
        }

        public bool SetEnableAutoIP(bool bIsAuto) // Sets Enable flagtoscanfor anavailable IP address (0: inactive, 1: active)
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.AUTOIP.ToString(), bIsAuto ? 1 : 0);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetBurst(int nValue) // Sets the amount of bursts (dependent onsystemandpulse rep.-rate)
        {
            if (nValue < 8 || nValue > 20)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [8 ~ 20]", nValue);
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.BURST.ToString(), nValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetBurstSecondary(int nValue) // JICHA [20181031] - 메뉴얼상 이 명령이 없음.. 확인필요..
        {
            if (nValue < 1 || nValue > 20)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [1 ~ 20]", nValue);
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.BURST_SEC.ToString(), nValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetUseEnhancedProtocol(int nValue) // 0: enhanced protocol is off (default setting), 1: index is a sequence of numbers (0..9), 2: index can be alphanumeric used by Coherent GUI, 3: custom format used by Coherent GUI
        {
            if (nValue < 0 || nValue > 3)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [0 ~ 3]", nValue);
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.EP.ToString(), nValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetQuit() // Close anEthernet connection
        {
            string cmd = string.Format("{0}\r\n", ParameterName.QUIT.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }
        
        public bool SetClearAlarm() // Send"FACK=1"toacknowledge faults andreturnthe laser toa readystate if the fault conditionis lifted
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.FACK.ToString(), 1);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetHeartBeat(int nValue) // Sets the heartbeat timeout insecs (0= disabled)
        {
            if (nValue < 0 || nValue > 300)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [1 ~ 300]", nValue); // Value is Seconds
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.HB.ToString(), nValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetHostname(string sHostName = "www.PicoLaser.com") // Sets host name for Ethernet connection
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.HOSTNAME.ToString(), sHostName);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetIPAddress(string sHostIP) // Sets the static IP address
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.IP.ToString(), sHostIP);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetIPMax(string sIpMax) // Sets end of range for AutoIP scan
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.IPMAX.ToString(), sIpMax);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetIPMin(string sIpMin) // Sets start of range for AutoIP scan
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.IPMIN.ToString(), sIpMin);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetLockOut(int nValue) // Sets the lockout status (0: inactive, 1: active) preventing other users from simultaneously controlling the laser
        {
            if (nValue < 0 || nValue > 1)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [0 ~ 1]", nValue);
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.LOCKOUT.ToString(), nValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetIRPower(double dValue) // Sets the closed-loop attenuator set power
        {
            if (dValue < 5.0 || dValue > 87.9704)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [5.0 ~ 87.9704]", dValue);
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.PATTSET.ToString(), dValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetPulseMode(int nValue) // 0: Continuous, 1: Gated, 2: Divided, 3: Divided&Gated, 4: Burst, 5: Burst &Divided 
        {
            if (nValue < 0 || nValue > 3)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [0 ~ 3]", nValue);
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.PM.ToString(), nValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetRFPercentLevel(double dValue) // Sets the output AOManaloglevel percentage ([0-100]%)
        {
            if (dValue < 0.0 || dValue > 100.0)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [0.0 ~ 100.0]", dValue);
                return false;
            }

            string cmd = string.Format("{0} {1:0.0}\r\n", ParameterName.RL.ToString(), dValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetRepetitionRate(double dValue) // Sets the amplifier pulse repetitionrate(min. / max.values dependend on system)
        {
            string cmd = string.Format("{0} {1:0.0}\r\n", ParameterName.RRAMPSET.ToString(), dValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetAOM2Divisor(int nValue) // Sets the amplifier-to-output pulse repetitionrate divider (1 everypulse, 2 everysecondp. etc.)
        {
            if (nValue < 1 || nValue > 65535)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [1 ~ 65535]", nValue);
                return false;
            }

            string cmd = string.Format("{0} {1}\r\n", ParameterName.RRD.ToString(), nValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetOpenShutter(bool bIsOpen) //  Sets the shutter state(0: close, 1: open)
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.S.ToString(), bIsOpen ? 1 : 0);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetShutterControlInput(bool bIsEnable) // 0: disables inversion(default) 1: enables inversion
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.SCI.ToString(), bIsEnable ? 1 : 0);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetShutterControlOutput(bool bIsEnable) // Set Shutter control output inversion
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.SCOI.ToString(), bIsEnable ? 1 : 0);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetShutterOperationMode(bool bIsEnable) // Set shutter operationmode - refer tosection "SafetyControl (D-Sub 15)"
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.SMOD.ToString(), bIsEnable ? 1 : 0);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetStartLaser() // Starts the laser emissionc (inside the laser head)
        {
            string cmd = string.Format("{0}\r\n", ParameterName.START.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetStopLaser() // Stops the laser emission(inside the laser head)
        {
            string cmd = string.Format("{0}\r\n", ParameterName.STOP.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetTransmission(double dValue) // Sets the transmissionof the variable attenuator in open loop mode (without active regulation)
        {
            if (dValue < 0 || dValue > 100)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Value = {0} Range over [0.0 ~ 100.0]", dValue);
                return false;
            }

            string cmd = string.Format("{0} {1:0.0}\r\n", ParameterName.TATT.ToString(), dValue);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetCloseLoop(bool bIsCloseLoop) // Sets the variable attenuator control mode (0: openloop, set transmission; 1: closedloop, set power)
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.PRENABLE.ToString(), bIsCloseLoop ? 1 : 0);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool SetExternalMode(bool bIsExternal) // Sets the external modulationstatus(0: internal setting, 1: external analogsignal)
        {
            string cmd = string.Format("{0} {1}\r\n", ParameterName.EM.ToString(), bIsExternal ? 1 : 0);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsEnableAutoIP() // Returns state of flagtoscanfor anavailable IP address (0: inactive, 1: active)
        {
            _laserParameterRecvState.IsRecvCompleteAutoIP = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.AUTOIP.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsBurst() // Returns the burst number
        {
            _laserParameterRecvState.IsRecvCompleteBurst = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.BURST.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsBurstSecondary() // JICHA [20181031] - 메뉴얼상 이 명령이 없음.. 확인필요..
        {
            _laserParameterRecvState.IsRecvCompleteBurstSec = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.BURST_SEC.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsUseEnhancedProtocol() // Returns enhancedserial protocol status
        {
            _laserParameterRecvState.IsRecvCompleteEP = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.EP.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsAlarmList() // Returns a list of numbered codes of all active faults, or returns "SYSTEMOK"if noactive faults (refer to section "List of Faults")
        {
            _laserParameterRecvState.IsRecvCompleteF = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.F.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsHeartBeat() // Returns the heartbeat timeout insecs (0= disabled)
        {
            _laserParameterRecvState.IsRecvCompleteHB = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.HB.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsHostname() // Returns host name for Ethernet connection
        {
            _laserParameterRecvState.IsRecvCompleteHostName = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.HOSTNAME.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsIPAddress() // Returns the IP address for Ethernet
        {
            _laserParameterRecvState.IsRecvCompleteIP = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.IP.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsIPMax() // Returns end of range for AutoIP scan
        {
            _laserParameterRecvState.IsRecvCompleteIPMax = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.IPMAX.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsIPMin() // Returns start of range for AutoIP scan
        {
            _laserParameterRecvState.IsRecvCompleteIPMin = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.IPMIN);
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsLockOut() // Returns the lockout status (0: inactive, 1: active) preventing other users from simultaneously controlling the laser
        {
            _laserParameterRecvState.IsRecvCompleteLockout = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.LOCKOUT.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsIRPower() // Returns the closed-loopattenuator set power
        {
            _laserParameterRecvState.IsRecvCompletePATTSET = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.PATTSET.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsPulseMode() // Returns the pulse operatingmode (trigger mode) 0: Continuous, 1: Gated, 2: Divided, 3: Divided&Gated, 4: Burst, 5: Burst &Divided 
        {
            _laserParameterRecvState.IsRecvCompletePM = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.PM.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsRFPercentLevel() // Returns the output AOManaloglevel percentage ([0-100]%)
        {
            _laserParameterRecvState.IsRecvCompleteRL = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.RL.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsRepetitionRate() // Returns the set amplifier pulse repetitionrate
        {
            _laserParameterRecvState.IsRecvCompleteRRAMPSET = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.RRAMPSET.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsAOM2Divisor() // Returns the amplifier-to-output pulse repetitionrate divider
        {
            _laserParameterRecvState.IsRecvCompleteRRD = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.RRD.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsOpenShutter() // Returns the shutter state (0: closed, 1: open)
        {
            _laserParameterRecvState.IsRecvCompleteS = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.S.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsShutterControlInput() // Returns shutter control inversionstatus
        {
            _laserParameterRecvState.IsRecvCompleteSCI = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.SCI.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsShutterControlOutput() // Get Shutter control output inversion
        {
            _laserParameterRecvState.IsRecvCompleteSCOI = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.SCOI.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsShutterOperationMode() // Return shutter operation mode
        {
            _laserParameterRecvState.IsRecvCompleteSMOD = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.SMOD.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsLaserState() // Returns the state of the laser (-1: Boot, 0: Ready, 1: On, 2: Fault)
        {
            _laserParameterRecvState.IsRecvCompleteState = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.STATE.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsTransmission() // Returns the transmissionof the variable attenuator
        {
            _laserParameterRecvState.IsRecvCompleteTATT = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.TATT.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsCloseLoop() // Returns the variable attenuator control mode (0: openloop, set transmission; 1: closedloop, set power)
        {
            _laserParameterRecvState.IsRecvCompletePRENABLE = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.PRENABLE.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }

        public bool GetIsExternalMode() // Returns the external modulationstatus (0: internal setting, 1: external analogsignal)
        {
            _laserParameterRecvState.IsRecvCompleteEM = false;
            string cmd = string.Format("?{0}\r\n", ParameterName.EM.ToString());
            queCmd.Enqueue(cmd);
            return true;
        }
    }
}

﻿using Dit.Framework.Log;
using Dit.FrameworkSingle.Net.SNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace DIT.TLC.CTRL
{
    public enum EmAlignType
    {
        PreAlign = 0,
        BreakAlign = 1,
        FineAlign = 2,
        None,
    }

    public enum EmCameraIndex
    {
        Cam1st,
        Cam2nd,
        None,
    }
    public enum EmAlignIndex
    {
        Grap1St,
        Grap2St,
        None,
    }
    public enum EmAlignResult
    {
        Fail = 0,
        Success = 1
    }
    public enum EmAlignTableIndex
    {
        A1 = 1,
        A2 = 2,
        B1 = 3,
        B2 = 4
    }

    public class AlignResult
    {
        public int MsgId { get; set; }
        public EmAlignType AlignType { get; set; }
        public EmCameraIndex CameraIndex { get; set; }
        public EmAlignResult Result { get; set; }
        public double T { get; set; }
        public double X { get; set; }
        public double Y { get; set; }

        public EmAlignIndex AlignIndex { get; set; }
        public EmAlignResult CalcResult { get; set; }
        public bool RecvComplete { get; internal set; }

        public void Copy(AlignResult result)
        {
            this.AlignType = result.AlignType;
            this.CameraIndex = result.CameraIndex;
            this.Result = result.Result;
            this.T = result.T;
            this.X = result.X;
            this.Y = result.Y;
            this.AlignIndex = result.AlignIndex;
            this.CalcResult = result.CalcResult;
        }
    }


    public class McrResult
    {
        public string PanelID { get; set; }
    }


    public class AlignPcProxy
    {


        private const int defCmdAlignStart = 100;
        private const int defCmdAlignStartAck = 200;
        private const int defCmdAlignEnd = 300;
        private const int defCmdAlignEndAck = 400;
        private const int defCmdRcpChange = 500;
        private const int defCmdRcpChangeAck = 600;
        private const int defCmdError = 700;
        private const int defCmdErrorAck = 800;
        private const int defCmdAlignBkSet = 350;
        private const int defCmdAlignBkSetAck = 450;
        private const int defCmdAlignBkSetEnd = 900;
        private const int defCmdAlignBkSetEndAck = 950;
        private const int defCmdMeasureGrabStart = 1000;
        private const int defCmdMeasureGrabStrartAck = 1050;
        private const int defCmdJigAlignStart = 1100;
        private const int defCmdJigAlignStartAck = 1150;
        private const int defCmdJigAlignEnd = 1200;
        private const int defCmdJigAlignEndAck = 1250;



        public bool ErrorCameraFail { get; set; }
        public bool ErrorLightFail { get; set; }


        public SNetClinetSession TcpSession { get; set; }

        //통신용 메모리 버퍼.        
        public string IpAddress { get; set; }
        public int Port { get; set; }
        public bool IsRecvAlignEnd { get; set; }
        public bool IsRecvVisionRecipeChange { get; set; }
        public bool IsRecvVisionError { get; set; }

        public AlignResult[] PreAlignResult { get; set; }
        public AlignResult[] BreakAlignResult { get; set; }
        public AlignResult[] FineAlignResult { get; set; }

        public AlignResult[] JigAlignResult { get; set; }

        public AlignPcProxy(string ip, int port)
        {
            PreAlignResult = new AlignResult[2] { new AlignResult(), new AlignResult() };
            BreakAlignResult = new AlignResult[2] { new AlignResult(), new AlignResult() };
            FineAlignResult = new AlignResult[2] { new AlignResult(), new AlignResult() };


            TcpSession = new SNetClinetSession();
            TcpSession.IpAddress = ip;
            TcpSession.Port = port;

            TcpSession.OnConnect += TcpSession_OnConnect;
            TcpSession.OnClose += TcpSession_OnClose;
            TcpSession.OnError += TcpSession_OnError;
            TcpSession.OnReceive += TcpSession_OnReceive;
            TcpSession.Spliter = null;
            TcpSession.SplitLength = 0;
        }

        //통신용 메모리 버퍼.
        private byte[] _allReciveDataBuffer = new byte[1024000];
        private int _allReciveDataBufferSize = 1024000;
        private int _totalBytesSize = 0;
        private int HeadSize = 6;
        public void IntializeSocket()
        {
            _allReciveDataBuffer = new byte[1024000];
            _allReciveDataBufferSize = 1024000;
            _totalBytesSize = 0;
        }
        private int _msgInx = 1;

        private void TcpSession_OnError(object sender, SNetErrorEventArgs e)
        {
            IntializeSocket();
            Console.WriteLine("연결 종료됨");
        }
        private void TcpSession_OnClose(object sender, SNetConnectionEventArgs e)
        {
            IntializeSocket();
            Console.WriteLine("연결 종료됨");
        }
        private void TcpSession_OnConnect(object sender, SNetConnectionEventArgs e)
        {
            IntializeSocket();
            Console.WriteLine("연결됨");
        }
        private void TcpSession_OnReceive(object sender, SNetReceiveEventArgs e)
        {
            if (_totalBytesSize + e.ReceiveBytes > _allReciveDataBufferSize)
            {
                _allReciveDataBufferSize = _totalBytesSize + e.ReceiveBytes + 1024;
                byte[] tBuff = new byte[_allReciveDataBufferSize];
                Array.Copy(_allReciveDataBuffer, 0, tBuff, 0, _allReciveDataBuffer.Length);

                _allReciveDataBuffer = tBuff;
            }

            Array.Copy(e.Buffer, 0, _allReciveDataBuffer, _totalBytesSize, e.ReceiveBytes);
            _totalBytesSize = _totalBytesSize + e.ReceiveBytes;

            while (_totalBytesSize > HeadSize)
            {
                int pkSize = BitConverter.ToInt32(_allReciveDataBuffer, 0);
                if (_totalBytesSize < pkSize) break;
                byte[] buff = new byte[pkSize + HeadSize];

                Array.Copy(_allReciveDataBuffer, 0, buff, 0, pkSize + HeadSize);

                _totalBytesSize = _totalBytesSize - (pkSize + HeadSize);
                Array.Copy(_allReciveDataBuffer, pkSize + HeadSize, _allReciveDataBuffer, 0, _totalBytesSize);

                OnRecivePacket(buff);
            }
        }

        //메소드 - 수신용 메소드 
        private void OnRecivePacket(byte[] onePacket)
        {
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);    /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);     /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);    /**/index += 4;


            Console.WriteLine(string.Format("RECV MSGID = [{0}] = [{1}]", cmd, string.Join(",", onePacket.ToList().Select(f => f.ToString()))));



            if (cmd == defCmdAlignStartAck)
            {
                OnAlingStartAck(onePacket);
            }
            else if (cmd == defCmdAlignEnd)
            {
                OnAlignEnd(onePacket);
            }
            else if (cmd == defCmdRcpChange)
            {
                OnRecipeChange(onePacket);
                IsRecvVisionRecipeChange = true;
            }
            else if (cmd == defCmdError)
            {
                OnError(onePacket);
                IsRecvVisionError = true;
            }
            else if (cmd == defCmdAlignBkSetAck)
            {
                OnAlgnBkSetAck(onePacket);
            }
            else if (cmd == defCmdMeasureGrabStrartAck)
            {
                OnMeasureGrabStartAck(onePacket);
            }
            else if (cmd == defCmdAlignBkSetAck)
            {
                OnAlignBkSetEnd(onePacket);
            }
            else if (cmd == defCmdJigAlignStartAck)
            {
                OnJigAlignStartAck(onePacket);
            }
            else if (cmd == defCmdJigAlignEnd)
            {
                OnJigAlignEnd(onePacket);
            }
        }
        private void OnJigAlignEnd(byte[] onePacket)
        {
            //로그 추가 필요. 
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int alignIndex = BitConverter.ToInt32(onePacket, index);        /**/index += 4;
            int alignOffsetX = BitConverter.ToInt32(onePacket, index);      /**/index += 8;
            int alignOffsetY = BitConverter.ToInt32(onePacket, index);      /**/index += 8;
            int alignAngle = BitConverter.ToInt32(onePacket, index);        /**/index += 8;
            int alignResult = BitConverter.ToInt32(onePacket, index);       /**/index += 4;

            SendJigAlignEndAck(msgInx, 1);


            if (alignIndex != 0 && alignIndex != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (alignResult != 0 && alignResult != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (alignResult == 0)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (alignIndex != 0 && alignIndex != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (alignResult != 0 && alignResult != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }

            AlignResult result = new AlignResult();

            result.CameraIndex = alignIndex == 0 ? EmCameraIndex.Cam1st : alignIndex == 1 ? EmCameraIndex.Cam1st : EmCameraIndex.None;
            result.Result = alignResult == 0 ? EmAlignResult.Fail : alignResult == 1 ? EmAlignResult.Success : EmAlignResult.Fail;
            result.T = alignAngle;
            result.X = alignOffsetX;
            result.Y = alignOffsetY;
            result.AlignIndex = alignIndex == 0 ? EmAlignIndex.Grap1St : alignIndex == 1 ? EmAlignIndex.Grap2St : EmAlignIndex.None;
            result.CalcResult = alignResult == 0 ? EmAlignResult.Fail : alignResult == 1 ? EmAlignResult.Success : EmAlignResult.Fail;

            if (result.AlignType == EmAlignType.PreAlign) // PreAlign
            {
                if (PreAlignResult[(int)result.CameraIndex].MsgId == result.MsgId)
                    PreAlignResult[(int)result.CameraIndex].Copy(result);
            }
            else if (result.AlignType == EmAlignType.FineAlign)
            {
                if (FineAlignResult[(int)result.CameraIndex].MsgId == result.MsgId)
                    FineAlignResult[(int)result.CameraIndex].Copy(result);
            }
            else if (result.AlignType == EmAlignType.BreakAlign)
            {
                if (BreakAlignResult[(int)result.CameraIndex].MsgId == result.MsgId)
                    BreakAlignResult[(int)result.CameraIndex].Copy(result);
            }

            Logger.Log.AppendLine(LogLevel.Info, "Recv AlignBk Set End= {0}", msgInx);
        }


        private void OnJigAlignStartAck(byte[] onePacket)
        {
            //로그 추가 필요. 
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int ack = BitConverter.ToInt32(onePacket, index);               /**/index += 4;
            Logger.Log.AppendLine(LogLevel.Info, "Recv Jig Align Ack = {0}", ack);
        }

        private void OnAlignBkSetEnd(byte[] onePacket)
        {
            //로그 추가 필요. 
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int alignIndex = BitConverter.ToInt32(onePacket, index);        /**/index += 4;
            int alignResult = BitConverter.ToInt32(onePacket, index);       /**/index += 4;

            int motorCenOffsetX = BitConverter.ToInt32(onePacket, index);   /**/index += 8;
            int motorCenOffsetY = BitConverter.ToInt32(onePacket, index);  /**/index += 8;

            SendAlignBkSetEndAck(msgInx, 1);


            Logger.Log.AppendLine(LogLevel.Info, "Recv AlignBk Set End= {0}", msgInx);
        }
        private void OnAlgnBkSetAck(byte[] onePacket)
        {
            //로그 추가 필요. 
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int ack = BitConverter.ToInt32(onePacket, index);               /**/index += 4;
            Logger.Log.AppendLine(LogLevel.Info, "Recv AlgnBk Set Ack = {0}", ack);
        }
        private void OnAlingStartAck(byte[] onePacket)
        {
            //로그 추가 필요. 
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int ack = BitConverter.ToInt32(onePacket, index);               /**/index += 4;
            Logger.Log.AppendLine(LogLevel.Info, "Recv Align Start Ack = {0}", ack);
        }
        private void OnAlignEnd(byte[] onePacket)
        {

            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;

            SendAlignEndAck(msgInx, 1);

            int alignType = BitConverter.ToInt32(onePacket, index);         /**/index += 4;
            int cameraIndex = BitConverter.ToInt32(onePacket, index);       /**/index += 4;
            int alignResult = BitConverter.ToInt32(onePacket, index);       /**/index += 4;
            double alignAngle = BitConverter.ToDouble(onePacket, index);    /**/index += 8;
            double alignX = BitConverter.ToDouble(onePacket, index);        /**/index += 8;
            double alignY = BitConverter.ToDouble(onePacket, index);        /**/index += 8;
            int alignIndex = BitConverter.ToInt32(onePacket, index);        /**/index += 4;
            int calcResult = BitConverter.ToInt32(onePacket, index);        /**/index += 4;

            Logger.Log.AppendLine(LogLevel.Info, "Recv Align End  alignType = {0}, cameraIndex= {1}, alignResult= {2}, alignAngle= {3}, alignX= {4}, alignY= {5}, alignIndex= {6}, calcResult= {7}"
                , alignType, cameraIndex, alignResult, alignAngle, alignX, alignY, alignIndex, calcResult);

            if (alignType != 0 && alignType != 1 && alignType != 2)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (cameraIndex != 0 && cameraIndex != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (alignResult != 0 && alignResult != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (alignResult == 0)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (alignIndex != 0 && alignIndex != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }
            if (calcResult != 0 && calcResult != 1)
            {
                //알람 추가 필요. 
                //로그 추가. 
            }


            AlignResult result = new AlignResult();
            result.AlignType = alignType == 0 ? EmAlignType.PreAlign : alignType == 1 ? EmAlignType.BreakAlign : alignType == 2 ? EmAlignType.FineAlign : EmAlignType.None;
            result.CameraIndex = cameraIndex == 0 ? EmCameraIndex.Cam1st : cameraIndex == 1 ? EmCameraIndex.Cam1st : EmCameraIndex.None;
            result.Result = alignResult == 0 ? EmAlignResult.Fail : alignResult == 1 ? EmAlignResult.Success : EmAlignResult.Fail;
            result.T = alignAngle;
            result.X = alignX;
            result.Y = alignY;
            result.AlignIndex = alignIndex == 0 ? EmAlignIndex.Grap1St : alignIndex == 1 ? EmAlignIndex.Grap2St : EmAlignIndex.None;
            result.CalcResult = alignResult == 0 ? EmAlignResult.Fail : alignResult == 1 ? EmAlignResult.Success : EmAlignResult.Fail;

            if (result.AlignType == EmAlignType.PreAlign && result.AlignIndex == EmAlignIndex.Grap2St) // PreAlign
            {
                PreAlignResult[(int)result.CameraIndex].Copy(result);
                PreAlignResult[(int)result.CameraIndex].RecvComplete = true;
            }
            else if (result.AlignType == EmAlignType.FineAlign && result.AlignIndex == EmAlignIndex.Grap2St)
            {
                FineAlignResult[(int)result.CameraIndex].Copy(result);
                PreAlignResult[(int)result.CameraIndex].RecvComplete = true;
            }
            else if (result.AlignType == EmAlignType.BreakAlign && result.AlignIndex == EmAlignIndex.Grap2St)
            {
                BreakAlignResult[(int)result.CameraIndex].Copy(result);
                PreAlignResult[(int)result.CameraIndex].RecvComplete = true;
            }
        }
        private void OnRecipeChange(byte[] onePacket)
        {
            //로그 추가 필요. 
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int code = BitConverter.ToInt32(onePacket, index);               /**/index += 4;

            SendRecipeChangeAck(msgInx, 1);

            Logger.Log.AppendLine(LogLevel.Info, "Recv Recipe Change(1: Recipe Created, 2: Recipe Modified, 3: Recipe Remove) = {0} ", code);
        }
        private void OnError(byte[] onePacket)
        {

            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int code = BitConverter.ToInt32(onePacket, index);               /**/index += 4;

            SendErrorAck(msgInx, 1);
            Logger.Log.AppendLine(LogLevel.Info, "Recv Error = {0}", code);

            ErrorCameraFail = code == 1;
            ErrorLightFail = code == 2;
        }
        private void OnMeasureGrabStartAck(byte[] onePacket)
        {
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);             /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);            /**/index += 4;
            int ack = BitConverter.ToInt32(onePacket, index);               /**/index += 4;

            Logger.Log.AppendLine(LogLevel.Info, "Recv Measure Grab Start Ack= {0}", ack);
        }
        public bool IsConnection
        {
            get
            {
                if (TcpSession == null) return false;
                return TcpSession.IsConnection;
            }
        }
        public void Start()
        {
            TcpSession.Start();
        }
        public void Stop()
        {
            TcpSession.Stop();
        }

        public bool SendPacket(byte[] buffer)
        {
            int pkSize = BitConverter.ToInt32(buffer, 0);
            short cmd = BitConverter.ToInt16(buffer, 4);

            return TcpSession.SendPacket(buffer);
        }


        public bool IsAlignComplete(string ppid, string cellID, EmAlignType alignType, EmCameraIndex cameraIndex, EmAlignIndex alignIndex, out AlignResult result)
        {
            result = new AlignResult();
            if (alignType == EmAlignType.PreAlign) // PreAlign
            {
                if (PreAlignResult[(int)cameraIndex].RecvComplete == true)
                {
                    result = PreAlignResult[(int)cameraIndex];
                    return true;
                }
            }
            else if (alignType == EmAlignType.FineAlign)
            {
                if (FineAlignResult[(int)cameraIndex].RecvComplete == true)
                {
                    result = FineAlignResult[(int)cameraIndex];
                    return true;
                }
            }
            else if (alignType == EmAlignType.BreakAlign)
            {
                if (BreakAlignResult[(int)cameraIndex].RecvComplete == true)
                {
                    result = BreakAlignResult[(int)cameraIndex];
                    return true;
                }
            }


            //if (GG.TestMode == true)
            {
                result = new AlignResult() { X = 0, Y = 1, T = 1 };
                result.Result = EmAlignResult.Success;
                result.RecvComplete = true;
                return true;

            }

            
            return false;
        }

        //메소드 - 송신용 메시지
        public bool SendAlignStart(int msgInx, string ppID, string cellID, EmAlignType alignType, EmCameraIndex cameraIndex, EmAlignIndex alignIndex, double motorOffsetX, double motorOffsetY, int autoMode)
        {
            msgInx = _msgInx++;
            byte[] buffer = MakePacket(
                 defCmdAlignStart,
                 BitConverter.GetBytes(msgInx),
                 Encoding.ASCII.GetBytes(ppID.PadRight(20)),
                 Encoding.ASCII.GetBytes(cellID.PadRight(20)),
                 BitConverter.GetBytes((int)alignType),
                 BitConverter.GetBytes((int)cameraIndex),
                 BitConverter.GetBytes((int)alignIndex),
                 BitConverter.GetBytes(motorOffsetX),
                 BitConverter.GetBytes(motorOffsetY),
                 BitConverter.GetBytes(autoMode)
            );

            if (alignType == EmAlignType.PreAlign) // PreAlign
            {
                PreAlignResult[(int)cameraIndex].MsgId = msgInx;
                PreAlignResult[(int)cameraIndex].RecvComplete = false;
            }
            else if (alignType == EmAlignType.FineAlign)
            {
                FineAlignResult[(int)cameraIndex].MsgId = msgInx;
                FineAlignResult[(int)cameraIndex].RecvComplete = false;
            }
            else if (alignType == EmAlignType.BreakAlign)
            {
                BreakAlignResult[(int)cameraIndex].MsgId = msgInx;
                BreakAlignResult[(int)cameraIndex].RecvComplete = false;
            }

            return SendPacket(buffer);
        }
        private bool SendAlignEndAck(int msgInx, int result = 1)
        {
            msgInx = _msgInx++;
            byte[] buffer = MakePacket(
                defCmdAlignEndAck,
                BitConverter.GetBytes(msgInx),
                BitConverter.GetBytes(result));
            return SendPacket(buffer);
        }
        private bool SendJigAlignEndAck(int msgInx, int result = 1)
        {
            msgInx = _msgInx++;
            byte[] buffer = MakePacket(
               defCmdJigAlignEndAck,
               BitConverter.GetBytes(msgInx),
               BitConverter.GetBytes(result));
            return SendPacket(buffer);
        }

        private bool SendRecipeChangeAck(int msgInx, int result = 1)
        {
            byte[] buffer = MakePacket(
                defCmdRcpChangeAck,
                BitConverter.GetBytes(msgInx),
                BitConverter.GetBytes(result));
            return SendPacket(buffer);
        }
        private bool SendErrorAck(int msgInx, int result = 1)
        {
            byte[] buffer = MakePacket(
                defCmdErrorAck,
                BitConverter.GetBytes(msgInx),
                BitConverter.GetBytes(result));
            return SendPacket(buffer);
        }
        public bool SendMasureGrabStart(int msgInx, string ppid, string cellID, EmAlignTableIndex tableIndex, int ImageIndex, EmCameraIndex cameraIndex)
        {
            byte[] buffer = MakePacket(
                defCmdMeasureGrabStart,
                BitConverter.GetBytes(msgInx),
                BitConverter.GetBytes((int)tableIndex),
                BitConverter.GetBytes((int)ImageIndex),
                BitConverter.GetBytes((int)cameraIndex)
           );

            return SendPacket(buffer);
        }
        private bool SendAlignBkSetEndAck(int msgInx, int result = 1)
        {
            byte[] buffer = MakePacket(
                   defCmdAlignBkSetAck,
                   BitConverter.GetBytes(msgInx),
                   BitConverter.GetBytes(result));
            return SendPacket(buffer);
        }
        public bool SendAlignBkSet(int msgInx, EmAlignIndex alignIndex, double motorOffsetX, double motorOffsetY)
        {
            byte[] buffer = MakePacket(
                 defCmdAlignBkSet,
                 BitConverter.GetBytes(msgInx),
                 BitConverter.GetBytes((int)alignIndex),
                 BitConverter.GetBytes(motorOffsetX),
                 BitConverter.GetBytes(motorOffsetY)
            );
            return SendPacket(buffer);
        }

        public bool SendJigAlignStart(int msgInx, EmCameraIndex cameraIndex, EmAlignIndex alignIndex, double motorOffsetX, double motorOffsetY)
        {
            byte[] buffer = MakePacket(
                 defCmdJigAlignStart,
                 BitConverter.GetBytes(msgInx),
                 BitConverter.GetBytes((int)alignIndex),
                 BitConverter.GetBytes((int)cameraIndex),
                 BitConverter.GetBytes(motorOffsetX),
                 BitConverter.GetBytes(motorOffsetY)
            );

            JigAlignResult[(int)cameraIndex].RecvComplete = false;

            return SendPacket(buffer);
        }

        //메소드 - 공용
        private byte[] MakePacket(short cmd, params object[] bodys)
        {
            int start = 0;
            int pkBodySize = bodys.Sum(f => (f as byte[]).Length);
            byte[] pkBytes = new byte[pkBodySize + 6];

            Array.Copy(BitConverter.GetBytes(pkBodySize),    /**/0, pkBytes, start, 4); start += 4;
            Array.Copy(BitConverter.GetBytes(cmd),           /**/0, pkBytes, start, 2); start += 2;

            foreach (byte[] item in bodys)
            {
                Array.Copy(item, /**/0, pkBytes, start, item.Length); start += item.Length;
            }
            return pkBytes;
        }
    }
}

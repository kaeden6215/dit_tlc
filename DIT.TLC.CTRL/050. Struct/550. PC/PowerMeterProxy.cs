﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DIT.TLC.CTRL
{
    public class PowerMeterProxy
    {
        private SerialComm SerialComm;

        private const string cmdRead = "pw?\r";

        public bool IsReadSuccess { get; set; }

        public bool IsReadComplete { get; set; }

        public double ReadData { get; set; }

        public PowerMeterProxy(string portName)
        {
            SerialComm = new SerialComm(portName, 9600, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);
            SerialComm.SplilterStr = "\r";
            SerialComm.OnRecvData += SerialComm_OnRecvData;
        }

        private void SerialComm_OnRecvData(byte[] recvData)
        {
            double dPower = 0;
            string s = Encoding.ASCII.GetString(recvData);
            if (!double.TryParse(s, out dPower)) dPower = -1;
            if (dPower != -1)
            {
                ReadData = dPower;
                IsReadSuccess = true;
            }
            else
            {
                ReadData = 0;
                IsReadSuccess = false;
            }
            IsReadComplete = true;
        }

        public bool OpenPort()
        {
            return SerialComm.Open();
        }

        public void ClosePort()
        {
            SerialComm.Close();
        }

        public bool SendCmd()
        {
            bool bSuccess = true;
            IsReadSuccess = false;
            IsReadComplete = false;

            try
            {
                SerialComm.SendData(cmdRead);
            }
            catch (Exception)
            {
                bSuccess = false;
            }
            return bSuccess;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DIT.TLC.CTRL
{
    //바코드
    public class HandyBarcodeProxy
    {
        private SerialComm SerialComm;

        private byte[] cmdRead = new byte[11] { 0x7E, 0x80, 0x02, 0x00, 0x00, 0x00, 0x01, 0x01, 0x82, 0x7E, 0x0D };

        public bool IsReadSuccess { get; set; }

        public bool IsReadComplete { get; set; }

        public string ReadData { get; set; }

        public HandyBarcodeProxy(string portName)
        {
            SerialComm = new SerialComm(portName, 9600, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, System.IO.Ports.Handshake.RequestToSend);
            SerialComm.SplilterStr = "\r";
            SerialComm.OnRecvData += SerialComm_OnRecvData;
        }

        private void SerialComm_OnRecvData(byte[] recvData)
        {
            ReadData = Encoding.ASCII.GetString(recvData);
            IsReadSuccess = true;
            IsReadComplete = true;
        }

        public bool OpenPort()
        {
            return SerialComm.Open();
        }

        public void ClosePort()
        {
            SerialComm.Close();
        }

        public bool SendCmd()
        {
            bool bSuccess = true;
            IsReadSuccess = false;
            IsReadComplete = false;

            try
            {
                SerialComm.SendData(cmdRead, 0, cmdRead.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }
            return bSuccess;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Dit.Framework.Ini;

namespace DIT.TLC.CTRL
{
    public class McrPcProxy
    {
        private SerialComm SerialComm;
        IniReader IniReader;

        private byte[] cmdRead = new byte[5] { 0x1B, 0x50, 0x00, 0xFF, 0x0D };

        private const string defMCR = "MCR";
        private const string defMcrExecuteDay = "ExecuteDay";
        private const string defReadSuccessCount = "SuccessCount";
        private const string defReadFailCount = "FailCount";
        private const string defTotalCount = "TotalCount";

        private string SavePath = string.Empty;
        private int McrExecuteDay = 0;

        public bool IsReadSuccess { get; set; }

        public bool IsReadComplete { get; set; }

        public string ReadData { get; set; }

        public int ReadSuccessCount { get; set; }

        public int ReadFailCount { get; set; }

        public int TotalCount { get; set; }

        public McrPcProxy(string portName, string savePath)
        {
            SerialComm = new SerialComm(portName, 9600, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.None);
            SerialComm.SplilterStr = "\r"; // 메뉴얼엔 체크섬과 구분기호가 0x0d로 같을 경우 문제 소지가 있다고 나와있음.. 고민해 볼것.
            SerialComm.OnRecvData += SerialComm_OnRecvData;

            SavePath = savePath;
            IniReader = new IniReader(string.Format(@"{0}\{1:0000}_{2:00}_{3:00}_McrCount.ini", SavePath, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
            ReadSuccessCount = IniReader.GetInteger(defMCR, defReadSuccessCount, 0);
            ReadFailCount = IniReader.GetInteger(defMCR, defReadFailCount, 0);
            TotalCount = IniReader.GetInteger(defMCR, defTotalCount, 0);
            McrExecuteDay = IniReader.GetInteger(defMCR, defMcrExecuteDay, 0);
        }

        ~McrPcProxy()
        {
            IniReader.IniFileName = string.Format(@"{0}\{1:0000}_{2:00}_{3:00}_McrCount.ini", SavePath, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            IniReader.SetInteger(defMCR, defReadSuccessCount, TotalCount);
            IniReader.SetInteger(defMCR, defReadFailCount, ReadFailCount);
            IniReader.SetInteger(defMCR, defTotalCount, ReadSuccessCount);
            IniReader.SetInteger(defMCR, defMcrExecuteDay, DateTime.Now.Day);
        }

        private void SerialComm_OnRecvData(byte[] recvData)
        {
            string sData = Encoding.ASCII.GetString(recvData);
            byte[] readingFailData = new byte[4] { 0x52, 0x11, 0x00, 0x0d };
            if (string.Compare(sData, Encoding.ASCII.GetString(readingFailData)) == 0)
            {
                IsReadSuccess = false;
                IsReadComplete = true;
                ReadFailCount++;
                return;
            }

            if (recvData.Length != 20)
            {
                // 로그 남기고
            }
            sData = sData.Substring(2, recvData.Length == 20 ? recvData.Length - 4 : recvData.Length - 3);

            ReadData = sData;
            IsReadSuccess = true;
            IsReadComplete = true;
            ReadSuccessCount++;
        }

        public bool OpenPort()
        {
            return SerialComm.Open();
        }

        public void ClosePort()
        {
            SerialComm.Close();
        }

        public bool SendCmd()
        {
            bool bSuccess = true;
            IsReadSuccess = false;
            IsReadComplete = false;
            if (McrExecuteDay != DateTime.Now.Day)
            {
                IniReader.IniFileName = string.Format(@"{0}\{1:0000}_{2:00}_{3:00}_McrCount.ini", SavePath, DateTime.Now.Year, DateTime.Now.Month, McrExecuteDay);
                IniReader.SetInteger(defMCR, defReadSuccessCount, TotalCount);
                IniReader.SetInteger(defMCR, defReadFailCount, ReadFailCount);
                IniReader.SetInteger(defMCR, defTotalCount, ReadSuccessCount);
                IniReader.SetInteger(defMCR, defMcrExecuteDay, McrExecuteDay);

                ReadSuccessCount = 0;
                ReadFailCount = 0;
                TotalCount = 0;
                McrExecuteDay = DateTime.Now.Day;
            }

            try
            {
                SerialComm.SendData(cmdRead, 0, cmdRead.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }
            
            TotalCount++;
            return bSuccess;
        }
    }
}

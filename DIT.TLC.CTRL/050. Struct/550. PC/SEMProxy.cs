﻿using Dit.FrameworkSingle.Net.SNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace DIT.TLC.CTRL
{
    public class SEMProxy
    {
        private enum EmFIXED_DATA{
            Fixed_1 = 0,
            Fixed_2,
            Fixed_3,
            Fixed_4,
            Fixed_5,
            Fixed_block,
            Fixed_unitID,
            Fixed_multi,
            Fixed_END,
        }

        private enum EmWORD_LENGTH{
            Common = 0,
            Uint16,
            Int16,
            Uint32,
            Int32,
            Float32,
            Word_END
        }

        public SNetClinetSession TcpSession { get; set; }

        private Timer TmrCmdSender = new Timer(100);
        private Queue<byte[]> QueCmd = new Queue<byte[]>(1000);

        //통신용 메모리 버퍼.
        private byte[] _allReciveDataBuffer = new byte[1024000];
        public string IpAddress { get; set; }
        public int Port { get; set; }

        public class AV_Meter_Info
        {
            public double Voltage_Vab = 0;
            public double Voltage_Vbc = 0;
            public double Voltage_Vca = 0;
            public double Voltage_Vavg = 0;

            public double Current_Ia = 0;
            public double Current_Ib = 0;
            public double Current_Ic = 0;
            public double Current_Iavg = 0;

            public double Pa_max = 0;
            public double Pb_max = 0;
            public double Pc_max = 0;
            public double Ptot_max = 0;

            public double Demand_KWA = 0;
            public double Demand_KWB = 0;
            public double Demand_KWC = 0;
            public double Demand_KWTotal = 0;

            public double Temperature_max = 0;
            public double Sum_KWh = 0;
        }

        public AV_Meter_Info AvMeterInfo = new AV_Meter_Info();
        public bool IsRecvComplete { get; set; }

        int MaxInfo = 0;
        int MaxSendDataLength = 0;
        int MaxRecvDataLength = 0;

        public SEMProxy(string ip, int port)
        {
            MaxInfo = 18;
            MaxSendDataLength = MaxInfo * 4 + 9;
            MaxRecvDataLength = MaxInfo * 4 + MaxSendDataLength;

            TcpSession = new SNetClinetSession();
            TcpSession.IpAddress = ip;
            TcpSession.Port = port;

            TcpSession.OnConnect += TcpSession_OnConnect;
            TcpSession.OnClose += TcpSession_OnClose;
            TcpSession.OnError += TcpSession_OnError;
            TcpSession.OnReceive += TcpSession_OnReceive;
            TcpSession.Spliter = null;            

            TmrCmdSender.Elapsed += new ElapsedEventHandler(tmrCmdSender_Elapsed);
        }

        public unsafe float GetIntToDouble(int n)
        {
            return *((float*)(void*)&n);
        }

        private void TcpSession_OnReceive(object sender, SNetReceiveEventArgs e)
        {
            byte[] getData = e.Buffer;
            byte[] oneData = new byte[4];
            int count = 0, nVal = 0;
            double[] dVal = new double[MaxInfo];
            for (int i = MaxSendDataLength; i < MaxRecvDataLength; i = i + 4)
            {
                if (count < MaxInfo)
                {
                    nVal = ((int)getData[i] << 24) + ((int)getData[i + 1] << 16) + ((int)getData[i + 2] << 8) + (int)getData[i + 3];
                    dVal[count] = (double)GetIntToDouble(nVal);
                    count++;
                }
            }

            if (getData[0] != 0xff)
            {
                AvMeterInfo.Voltage_Vab = dVal[0];
                AvMeterInfo.Voltage_Vbc = dVal[1];
                AvMeterInfo.Voltage_Vca = dVal[2];
                AvMeterInfo.Voltage_Vavg = dVal[3];

                AvMeterInfo.Current_Ia = dVal[4];
                AvMeterInfo.Current_Ib = dVal[5];
                AvMeterInfo.Current_Ic = dVal[6];
                AvMeterInfo.Current_Iavg = dVal[7];

                AvMeterInfo.Pa_max = dVal[8];
                AvMeterInfo.Pb_max = dVal[9];
                AvMeterInfo.Pc_max = dVal[10];
                AvMeterInfo.Ptot_max = dVal[11];

                AvMeterInfo.Demand_KWA = dVal[12];
                AvMeterInfo.Demand_KWB = dVal[13];
                AvMeterInfo.Demand_KWC = dVal[14];
                AvMeterInfo.Demand_KWTotal = dVal[15];

                AvMeterInfo.Temperature_max = dVal[16];
                AvMeterInfo.Sum_KWh = nVal;

                IsRecvComplete = true;
            }
        }

        private void TcpSession_OnError(object sender, SNetErrorEventArgs e)
        {
            Console.WriteLine("연결 종료됨");
        }

        private void TcpSession_OnClose(object sender, SNetConnectionEventArgs e)
        {
            TmrCmdSender.Stop();
            Console.WriteLine("연결 종료됨");
        }

        private void TcpSession_OnConnect(object sender, SNetConnectionEventArgs e)
        {
            TmrCmdSender.Start();
            Console.WriteLine("연결됨");
        }

        private void tmrCmdSender_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (QueCmd.Count <= 0) return;
            if (!IsConnection) { QueCmd.Clear(); return; }

            byte[] sendByte = QueCmd.Dequeue();
            TcpSession.SendPacket(sendByte);
        }

        public bool IsConnection
        {
            get
            {
                if (TcpSession == null) return false;
                return TcpSession.IsConnection;
            }
        }

        public void Start()
        {
            TcpSession.Start();
        }

        public void Stop()
        {
            TcpSession.Stop();
        }

        public bool SendRequestToAccura()
        {
            bool bIsSuccess = true;
            IsRecvComplete = false;
            byte[] Cmd = new byte[81];
            byte[] bfixed = new byte[8] { 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x01, 0x65 };
            byte[] bWordLength = new byte[6] { 0x00, 0x01, 0x01, 0x02, 0x02, 0x02 };

            Cmd[0] = bfixed[(int)EmFIXED_DATA.Fixed_1];         //  00
            Cmd[1] = bfixed[(int)EmFIXED_DATA.Fixed_2];         //  00
            Cmd[2] = bfixed[(int)EmFIXED_DATA.Fixed_3];         //  00
            Cmd[3] = bfixed[(int)EmFIXED_DATA.Fixed_4];         //  00
            Cmd[4] = bfixed[(int)EmFIXED_DATA.Fixed_5];         //  00
            Cmd[5] = bfixed[(int)EmFIXED_DATA.Fixed_block];     //  256
            Cmd[6] = bfixed[(int)EmFIXED_DATA.Fixed_unitID];    //  01
            Cmd[7] = bfixed[(int)EmFIXED_DATA.Fixed_multi];     //  
            Cmd[8] = 0x12;                                      //  18

            Cmd[9] = 0x2B;          //Voltage_Vab Starting Adderss1
            Cmd[10] = 0x64;         //Voltage_Vab Starting Adderss2
            Cmd[11] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[12] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[13] = 0x2B;         //Voltage_Vbc Starting Adderss1       
            Cmd[14] = 0x66;         //Voltage_Vbc Starting Adderss2
            Cmd[15] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[16] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[17] = 0x2B;         //Voltage_Vca Starting Adderss1
            Cmd[18] = 0x68;         //Voltage_Vca Starting Adderss2
            Cmd[19] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[20] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[21] = 0x2B;         //Voltage_Vavg Starting Adderss1
            Cmd[22] = 0x6A;         //Voltage_Vavg Starting Adderss2
            Cmd[23] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[24] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[25] = 0x2B;         //Current_Ia Starting Address1
            Cmd[26] = 0xC0;         //Current_Ia Starting Address2
            Cmd[27] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[28] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[29] = 0x2B;         //Current_Ib Starting Address1       
            Cmd[30] = 0xC2;         //Current_Ib Starting Address2
            Cmd[31] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[32] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[33] = 0x2B;         //Current_Ic Starting Address1
            Cmd[34] = 0xC4;         //Current_Ic Starting Address2
            Cmd[35] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[36] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[37] = 0x2B;         //Current_Iavg Starting Address1   
            Cmd[38] = 0xC6;         //Current_Iavg Starting Address2
            Cmd[39] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[40] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[41] = 0x43;         //Pa_max Starting Address1
            Cmd[42] = 0xEE;         //Pa_max Starting Address2
            Cmd[43] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[44] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[45] = 0x43;         //Pb_max Starting Address1       
            Cmd[46] = 0xF0;         //Pb_max Starting Address2
            Cmd[47] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[48] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[49] = 0x43;         //Pc_max Starting Address1
            Cmd[50] = 0xF2;         //Pc_max Starting Address2
            Cmd[51] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[52] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[53] = 0x43;         //Ptot_max Starting Address1
            Cmd[54] = 0xF4;         //Ptot_max Starting Address2
            Cmd[55] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[56] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[57] = 0x2C;         //Demand_KWA Starting Address1   
            Cmd[58] = 0x28;         //Demand_KWA Starting Address2
            Cmd[59] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[60] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[61] = 0x2C;         //Demand_KWB Starting Address1
            Cmd[62] = 0x2A;         //Demand_KWB Starting Address2
            Cmd[63] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[64] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[65] = 0x2C;         //Demand_KWC Starting Address1
            Cmd[66] = 0x2C;         //Demand_KWC Starting Address2
            Cmd[67] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[68] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[69] = 0x2C;         //Demand_KWTotal Starting Address1
            Cmd[70] = 0x2E;         //Demand_KWTotal Starting Address2
            Cmd[71] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[72] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[73] = 0x43;         //Temperature_MAX Starting Address1
            Cmd[74] = 0x4E;         //Temperature_MAX Starting Address2
            Cmd[75] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[76] = bWordLength[(int)EmWORD_LENGTH.Float32];

            Cmd[77] = 0x2C;         //Sum_KWh Starting Address1
            Cmd[78] = 0x1A;         //Sum_KWh Starting Address2
            Cmd[79] = bWordLength[(int)EmWORD_LENGTH.Common];
            Cmd[80] = bWordLength[(int)EmWORD_LENGTH.Float32];

            QueCmd.Enqueue(Cmd);
            return bIsSuccess;
        }
    }
}

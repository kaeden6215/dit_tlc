﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DIT.TLC.CTRL
{
    public class TiltSensorProxy
    {
        private SerialComm SerialComm;
        private double[] ReadData;
        private bool[] ReadJudge;

        public bool IsReadSuccess { get; set; }
        public bool IsReadComplete { get; set; }


        public double[] GetReadData
        {
            get { return ReadData; }
            set { ReadData = value; }
        }

        public bool[] GetReadJudge
        {
            get { return ReadJudge; }
            set { ReadJudge = value; }
        }

        public TiltSensorProxy(string portName)
        {
            SerialComm = new SerialComm(portName, 9600, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.None);
            SerialComm.SplilterStr = Environment.NewLine;
            SerialComm.OnRecvData += SerialComm_OnRecvData;
        }

        private void SerialComm_OnRecvData(byte[] recvData) // 기울기 센서 컨트롤러 옵션을 AUTO로 설정할 경우 계속해서 측정 값을 보내 줌
        {
            string Temp = Encoding.ASCII.GetString(recvData);
            string sTemp = Temp.Substring(1);
            string[] splitData = sTemp.Split(',');

            int nCount = 0, nValue = 0;
            if (!int.TryParse(splitData[1], out nCount)) nCount = -1;

            if (nCount != -1)
            {
                ReadData = new double[nCount];
                ReadJudge = new bool[nCount];

                for (int nIndex = 0; nIndex < nCount; nIndex++)
                {
                    if (!int.TryParse(splitData[2 + (nIndex*2)], out nValue)) nValue = 999999;
                    if (nValue != 999999) ReadData[nIndex] = (double)nValue / 1000;
                    ReadJudge[nIndex] = string.Compare(splitData[2+(nIndex*2)+1].Substring(0, 2), "OK") == 0 ? true : false;
                }
                IsReadSuccess = true;
            }
            else
            {
                IsReadSuccess = false;
            }
            IsReadComplete = true;
        }

        public bool OpenPort()
        {
            return SerialComm.Open();
        }

        public void ClosePort()
        {
            SerialComm.Close();
        }

        internal bool SendCmd()
        {
            return true;
        }
    }
}

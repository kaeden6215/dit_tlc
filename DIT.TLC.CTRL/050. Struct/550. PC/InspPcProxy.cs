﻿using Dit.Framework.Log;
using Dit.FrameworkSingle.Net.SNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace DIT.TLC.CTRL
{
    public class InspPcProxy
    {
        public SNetClinetSession TcpSession { get; set; }

        //통신용 메모리 버퍼.        
        private byte[] AllReciveDataBuffer = new byte[1024000];
        private int AllReciveDataBufferSize = 1024000;
        private int TotalBytesSize = 0;
        private int HeadSize = 6;

        public string IpAddress { get; set; }
        public int Port { get; set; }

        private const int defCmdInspReady = 10;       ////////////////////////////////////
        private const int defCmdGrabStart = 20;       //
        private const int defCmdGrabEnd = 30;       //
        private const int defCmdInspEndAck = 1040;     //
        private const int defCmdCorrectResultAck = 1050;     //
        private const int defCmdRecipeChangeAck = 1060;     //   [Control PC -> Inspection PC]
        private const int defCmdInspReset = 70;       //
        private const int defCmdAlarmAck = 1080;     //
        private const int defCmdMeasureReady = 90;       //
        private const int defCmdMeasureStart = 100;      ////////////////////////////////////

        private const int defCmdInspReadyAck = 1010;     ////////////////////////////////////
        private const int defCmdGrabStartAck = 1020;     //
        private const int defCmdGrabEndAck = 1030;     //
        private const int defCmdInspEnd = 40;       //
        private const int defCmdCorrectResult = 50;       //
        private const int defCmdRecipeChange = 60;       //   [Inspection PC -> Control PC]
        private const int defCmdInspResetAck = 1070;     //
        private const int defCmdAlarm = 80;       //
        private const int defCmdMeasureReadyAck = 1090;     //
        private const int defCmdMeasureStartAck = 1100;     //
        private const int defCmdMeasureEnd = 110;      //////////////////////////////////// 

        public bool IsRecvRespInspReady { get; set; }
        public bool IsRecvRespInspGrabStart { get; set; }
        public bool IsRecvRespInspGrabEnd { get; set; }
        public bool IsRecvInspEnd1 { get; set; }
        public bool IsRecvInspEnd2 { get; set; }
        public bool IsRecvCorrectResult { get; set; }
        public bool IsRecvRecipeChange { get; set; }
        public bool IsRecvRespInspReset { get; set; }
        public bool IsRecvAlarm { get; set; }
        public bool IsRecvRespMeasureReady { get; set; }
        public bool IsRecvRespMeasureStart { get; set; }
        public bool IsRecvMeasureEnd { get; set; }

        public InspPcProxy(string ip, int port)
        {
            TcpSession = new SNetClinetSession();
            TcpSession.IpAddress = ip;
            TcpSession.Port = port;

            TcpSession.OnConnect += TcpSession_OnConnect;
            TcpSession.OnClose += TcpSession_OnClose;
            TcpSession.OnError += TcpSession_OnError;
            TcpSession.OnReceive += TcpSession_OnReceive;
            TcpSession.Spliter = null;
            TcpSession.SplitLength = 0;
        }
        private void TcpSession_OnReceive(object sender, SNetReceiveEventArgs e)
        {
            if (TotalBytesSize + e.ReceiveBytes > AllReciveDataBufferSize)
            {
                AllReciveDataBufferSize = TotalBytesSize + e.ReceiveBytes + 1024;
                byte[] tBuff = new byte[AllReciveDataBufferSize];
                Array.Copy(AllReciveDataBuffer, 0, tBuff, 0, AllReciveDataBuffer.Length);

                AllReciveDataBuffer = tBuff;
            }

            Array.Copy(e.Buffer, 0, AllReciveDataBuffer, TotalBytesSize, e.ReceiveBytes);
            TotalBytesSize = TotalBytesSize + e.ReceiveBytes;

            while (TotalBytesSize > HeadSize)
            {
                int pkSize = BitConverter.ToInt32(AllReciveDataBuffer, 0);
                if (TotalBytesSize < pkSize) break;
                byte[] buff = new byte[pkSize + HeadSize];

                Array.Copy(AllReciveDataBuffer, 0, buff, 0, pkSize + HeadSize);

                TotalBytesSize = TotalBytesSize - (pkSize + HeadSize);
                Array.Copy(AllReciveDataBuffer, pkSize + HeadSize, AllReciveDataBuffer, 0, TotalBytesSize);


                OnRecivePacket(buff);
            }
        }

        //메소드 - 수신용 메소드 
        private void OnRecivePacket(byte[] onePacket)
        {
            int pkSize = BitConverter.ToInt32(onePacket, 0);
            short cmd = BitConverter.ToInt16(onePacket, 4);

            Console.WriteLine(string.Format("RECV MSGID = [{0}] = [{1}]", cmd, string.Join(",", onePacket.ToList().Select(f => f.ToString()))));


            if (cmd == defCmdInspReadyAck)
            {
                OnInspReadyAck(onePacket);
            }
            else if (cmd == defCmdGrabStartAck)
            {
                OnGrabStartAck(onePacket);
            }
            else if (cmd == defCmdGrabEndAck)
            {
                OnGrabEndAck(onePacket);
            }
            else if (cmd == defCmdInspEnd)
            {
                OnInspEnd(onePacket);
            }
            else if (cmd == defCmdCorrectResult)
            {
                OnCorrectResult(onePacket);
            }
            else if (cmd == defCmdRecipeChange)
            {
                OnRecChange(onePacket);
            }
            else if (cmd == defCmdInspResetAck) // [Align PC -> Control PC]Inspection reset ack
            {
                OnRecChange(onePacket);
            }
            else if (cmd == defCmdAlarm) // [Align PC->Control PC]Send Alarm Code - 1 : Alg Fail, 2 : Inspection Fail(Grab Fail 포함), 3 : Continuous NG, 4 : Measure Fail.
            {
                OnAlarm(onePacket);
            }
            else if (cmd == defCmdMeasureReadyAck) // [Align PC -> Control PC]Measure ready ack
            {
                OnMeasureReadyAck(onePacket);
            }
            else if (cmd == defCmdMeasureStartAck) // [Align PC -> Control PC]Measure start ack
            {
                OnMeasureStartAck(onePacket);
            }
            else if (cmd == defCmdMeasureEnd) // [Align PC -> Control PC]Measure end - Size : 4, Cmd : 110, Msg : 4Byte(int)[0 : Fail, 1 : Success]
            {
                OnMeasureEnd(onePacket);
            }
            else
            {

            }
        }
        public void IntializeSocket()
        {
            AllReciveDataBuffer = new byte[1024000];
            AllReciveDataBufferSize = 1024000;
            TotalBytesSize = 0;
        }

        private void TcpSession_OnError(object sender, SNetErrorEventArgs e)
        {
            IntializeSocket();
            Console.WriteLine("연결 종료됨");
        }

        private void TcpSession_OnClose(object sender, SNetConnectionEventArgs e)
        {
            IntializeSocket();
            Console.WriteLine("연결 종료됨");
        }

        private void TcpSession_OnConnect(object sender, SNetConnectionEventArgs e)
        {
            IntializeSocket();
            Console.WriteLine("연결됨");
        }


        public bool IsConnection
        {
            get
            {
                if (TcpSession == null) return false;
                return TcpSession.IsConnection;
            }
        }

        public void Start()
        {
            TcpSession.Start();
        }
        public void Stop()
        {
            TcpSession.Stop();
        }
        public bool SendPacket(byte[] buffer)
        {
            int pkSize = BitConverter.ToInt32(buffer, 0);
            short cmd = BitConverter.ToInt16(buffer, 4);

            Console.WriteLine(string.Format("SEND MSGID = [{0}] = [{1}]", cmd, string.Join(",", buffer.ToList().Select(f => f.ToString()))));

            
            return TcpSession.SendPacket(buffer);
        }


        //메소드 - 송신용 메시지
        public bool SendInspReady(int msgInx, string ppID, string lotID,
                              string cellID1, string cellID2,
                              double c1_BOffsetX, double c1_BOffsetY, double c1_BOffsetT,
                              double c2_BOffsetX, double c2_BOffsetY, double c2_BOffsetT,
                              int onlineStatus, string loadingTime)
        {
            byte[] buffer = MakePacket(
                 defCmdInspReady,
                 BitConverter.GetBytes(msgInx),
                 Encoding.ASCII.GetBytes(ppID.PadRight(20)),
                 Encoding.ASCII.GetBytes(lotID.PadRight(20)),
                 Encoding.ASCII.GetBytes(cellID1.PadRight(20)),
                 Encoding.ASCII.GetBytes(cellID2.PadRight(20)),
                 BitConverter.GetBytes(c1_BOffsetX),
                 BitConverter.GetBytes(c1_BOffsetY),
                 BitConverter.GetBytes(c1_BOffsetT),
                 BitConverter.GetBytes(c2_BOffsetX),
                 BitConverter.GetBytes(c2_BOffsetY),
                 BitConverter.GetBytes(c2_BOffsetT),
                 BitConverter.GetBytes(onlineStatus),
                 Encoding.ASCII.GetBytes(loadingTime.PadRight(20))
            );
            string ll = Encoding.ASCII.GetString(buffer, 10, 20);
            IsRecvRespInspReady = false;

            return SendPacket(buffer);
        }
        public bool SendGrabStart(int msgInx, int nPosIndex, int nInspIndex)
        {
            byte[] buffer = MakePacket(
                defCmdGrabStart,
                BitConverter.GetBytes(msgInx),
                BitConverter.GetBytes(nPosIndex),
                BitConverter.GetBytes(nInspIndex)
            );
            IsRecvRespInspGrabStart = false;
            IsRecvInspEnd1 = false;
            IsRecvInspEnd2 = false;
            IsRecvCorrectResult = false;

            return SendPacket(buffer);
        }
        public bool SendGrabEnd(int msgInx, int nPosIndex, int nInspIndex)
        {
            byte[] buffer = MakePacket(
                defCmdGrabEnd,
                BitConverter.GetBytes(msgInx),
                BitConverter.GetBytes(nPosIndex),
                BitConverter.GetBytes(nInspIndex)
            );
            IsRecvRespInspGrabEnd = false;

            return SendPacket(buffer);
        }
        public bool SendInspEndAck(int msgInx, int result = 1)
        {
            byte[] buffer = MakePacket(
               defCmdInspEndAck,
               BitConverter.GetBytes(msgInx),
               BitConverter.GetBytes(result)
            );
            return SendPacket(buffer);
        }
        public bool SendCorrectResultAck(int msgInx, int result = 1)
        {
            byte[] buffer = MakePacket(
                defCmdCorrectResultAck,
                BitConverter.GetBytes(msgInx),
                BitConverter.GetBytes(result)
            );

            return SendPacket(buffer);
        }
        public bool SendRecChangeAck(int msgInx, int result = 1)
        {
            byte[] buffer = MakePacket(
                 defCmdRecipeChangeAck,
                 BitConverter.GetBytes(msgInx),
                 BitConverter.GetBytes(result)
             );
            return SendPacket(buffer);
        }
        public bool SendInspReset(int msgInx, int msg = 1)
        {
            byte[] buffer = MakePacket(
                  defCmdInspReset,
                  BitConverter.GetBytes(msgInx),
                  BitConverter.GetBytes(msg)
            );
            IsRecvRespInspReset = false;

            return SendPacket(buffer);
        }
        public bool SendAlarmAck(int msgInx, int result = 1)
        {
            byte[] buffer = MakePacket(
                  defCmdAlarmAck,
                  BitConverter.GetBytes(msgInx),
                  BitConverter.GetBytes(result)
              );
            return SendPacket(buffer);
        }
        public bool SendMeasureReady(int msgInx, string ppID, string cellID, int imageCount, EmAlignTableIndex tableIndex)
        {
            byte[] buffer = MakePacket(
                defCmdMeasureReady,
                BitConverter.GetBytes(msgInx),
                Encoding.ASCII.GetBytes(ppID.PadRight(20)),
                Encoding.ASCII.GetBytes(cellID.PadRight(20)),
                BitConverter.GetBytes(imageCount),
                BitConverter.GetBytes((int)tableIndex)
            );
            IsRecvRespMeasureReady = false;

            return SendPacket(buffer);
        }
        public bool SendMeasureStart(int msgInx, int nMsg = 0)
        {
            byte[] buffer = MakePacket(
                  defCmdMeasureStart,
                  BitConverter.GetBytes(msgInx),
                  BitConverter.GetBytes(nMsg)
            );
            IsRecvRespMeasureStart = false;

            return SendPacket(buffer);
        }
        public bool SendMeasureEndAck(int msgInx, int nMsg = 0)
        {
            byte[] buffer = MakePacket(
                  defCmdMeasureStart,
                  BitConverter.GetBytes(msgInx),
                  BitConverter.GetBytes(nMsg)
             );
            return SendPacket(buffer);
        }

        //메소드 - 송신용 메시지
        private void OnInspReadyAck(byte[] onePacket)
        {
            //로그 추가 필요. 
            int ack = BitConverter.ToInt32(onePacket, 6);
            Logger.Log.AppendLine(LogLevel.Info, "Recv Insp Ready Ack = {0}", ack);
            IsRecvRespInspReady = true;
        }
        private void OnGrabStartAck(byte[] onePacket)
        {
            //로그 추가 필요. 
            int ack = BitConverter.ToInt32(onePacket, 6);
            Logger.Log.AppendLine(LogLevel.Info, "Recv Grab Start Ack = {0}", ack);
            IsRecvRespInspGrabStart = true;
        }
        private void OnGrabEndAck(byte[] onePacket)
        {
            //로그 추가 필요. 
            int ack = BitConverter.ToInt32(onePacket, 6);
            Logger.Log.AppendLine(LogLevel.Info, "Recv Grab End Ack = {0}", ack);
            IsRecvRespInspGrabEnd = true;
        }
        public InspResult inspResult1 { get; set; }
        public InspResult inspResult2 { get; set; }
        private void OnInspEnd(byte[] onePacket)
        {
            int index = 0;

            InspResult result = new InspResult();

            int pkSize = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);                 /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            result.InspIndex = BitConverter.ToInt32(onePacket, index);          /**/index += 4;
            result.PickerIndex = BitConverter.ToInt32(onePacket, index);        /**/index += 4;
            result.InspAlignAngle = BitConverter.ToDouble(onePacket, index);    /**/index += 8;
            result.InspAlignX = BitConverter.ToDouble(onePacket, index);        /**/index += 8;
            result.InspAlignY = BitConverter.ToDouble(onePacket, index);        /**/index += 8;
            result.Judgement = BitConverter.ToInt32(onePacket, index);          /**/index += 4;
            result.Result = BitConverter.ToInt32(onePacket, index);             /**/index += 4;
            result.ResultData = Encoding.ASCII.GetString(onePacket, index, 100);/**/index += 100;

            SendInspEndAck(msgInx);
            Logger.Log.AppendLine(LogLevel.Info, "Recv Insp End Judgement = {0}", result.Judgement);

            if (result.PickerIndex == 0)
            {
                inspResult1 = result;
                IsRecvInspEnd1 = true;
            }
            else if (result.PickerIndex == 1)
            {
                inspResult2 = result;
                IsRecvInspEnd2 = true;
            }
        }
        private void OnCorrectResult(byte[] onePacket)
        {
            int index = 0;
            InspResult result = new InspResult();
            int pkSize = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);                 /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            result.InspIndex = BitConverter.ToInt32(onePacket, index);          /**/index += 4;
            result.ResultData = Encoding.ASCII.GetString(onePacket, index, 100);/**/index += 100;

            SendCorrectResultAck(msgInx);
            Logger.Log.AppendLine(LogLevel.Info, "Recv Correct Result InspIndex = [{0}], ResultData= [{1}]", result.InspIndex, result.Judgement);
            IsRecvCorrectResult = true;
        }
        private void OnRecChange(byte[] onePacket)
        {
            int index = 0;


            InspResult result = new InspResult();
            int pkSize = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);                 /**/index += 2;

            int msgInx = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            int msg = BitConverter.ToInt32(onePacket, index);                   /**/index += 4;

            SendRecChangeAck(msgInx);
            Logger.Log.AppendLine(LogLevel.Info, "Recv Rec Change = [{0}]", msg);
            IsRecvRecipeChange = true;
        }
        private void OnInspResetAck(byte[] onePacket)
        {
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);                 /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            int ack = BitConverter.ToInt32(onePacket, index);                   /**/index += 4;

            Logger.Log.AppendLine(LogLevel.Info, "Recv Insp Reset Ack = [{0}]", ack);
            IsRecvRespInspReset = true;
        }
        private void OnAlarm(byte[] onePacket)
        {
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);                 /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);                /**/index += 4;
            int code = BitConverter.ToInt32(onePacket, index);                  /**/index += 4;
            SendAlarmAck(msgInx, 1);

            Logger.Log.AppendLine(LogLevel.Info, "Recv Alarm Code = [{0}]", code);
            IsRecvAlarm = true;
        }
        private void OnMeasureReadyAck(byte[] onePacket)
        {
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);    /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);     /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);    /**/index += 4;
            int ack = BitConverter.ToInt32(onePacket, index);       /**/index += 4;

            Logger.Log.AppendLine(LogLevel.Info, "Recv Measure Ready Ack= [{0}]", ack);
            IsRecvRespMeasureReady = true;
        }
        private void OnMeasureStartAck(byte[] onePacket)
        {
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);    /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);     /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);    /**/index += 4;
            int ack = BitConverter.ToInt32(onePacket, index);       /**/index += 4;

            Logger.Log.AppendLine(LogLevel.Info, "Recv Measure Start Ack= [{0}]", ack);
            IsRecvRespMeasureStart = true;
        }
        private void OnMeasureEnd(byte[] onePacket)
        {
            int index = 0;
            int pkSize = BitConverter.ToInt32(onePacket, index);    /**/index += 4;
            short cmd = BitConverter.ToInt16(onePacket, index);     /**/index += 2;
            int msgInx = BitConverter.ToInt32(onePacket, index);    /**/index += 4;
            int code = BitConverter.ToInt32(onePacket, index);      /**/index += 4;

            Logger.Log.AppendLine(LogLevel.Info, "Recv Measure End Ack= [{0}]", code);
            IsRecvMeasureEnd = true;
        }

        //메소드 - 공용
        private byte[] MakePacket(short cmd, params object[] bodys)
        {
            int start = 0;
            int pkBodySize = bodys.Sum(f => (f as byte[]).Length);
            byte[] pkBytes = new byte[pkBodySize + 6];

            Array.Copy(BitConverter.GetBytes(pkBodySize),    /**/0, pkBytes, start, 4); start += 4;
            Array.Copy(BitConverter.GetBytes(cmd),           /**/0, pkBytes, start, 2); start += 2;

            foreach (byte[] item in bodys)
            {
                Array.Copy(item, /**/0, pkBytes, start, item.Length); start += item.Length;
            }
            return pkBytes;
        }
    }

    public class InspResult
    {
        public int InspIndex { get; set; }
        public int PickerIndex { get; set; }
        public double InspAlignAngle { get; set; }
        public double InspAlignX { get; set; }
        public double InspAlignY { get; set; }
        public int Judgement { get; set; }
        public int Result { get; set; }
        public string ResultData { get; set; }
    }
}

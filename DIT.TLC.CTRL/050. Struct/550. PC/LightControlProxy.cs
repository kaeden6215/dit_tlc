﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace DIT.TLC.CTRL
{
    public class LightControlProxy
    {
        private SerialComm SerialComm;

        private const string cmdSetBrightness = "setbrightness";
        private const string cmdGetBrightness = "getbrightness";
        private const string cmdSetAllLightOnOff = "setonex";
        private const string cmdGetAllLightOnOff = "getonex";
        private const string cmdSetLightOnOff = "seton";
        private const string cmdGetLightOnOff = "geton";

        private int[] GetReadBrightness = new int[4] { 0, 0, 0, 0 };
        private bool[] GetReadLightOnStatus = new bool[4] { false, false, false, false };

        public bool IsReadSuccessBrightness { get; set; }

        public bool IsReadCompleteBrightness { get; set; }

        public bool IsReadSuccessLightOnStatus { get; set; }

        public bool IsReadCompleteLightOnStatus { get; set; }

        public int[] ReadBrightness { get; /*set;*/ }

        public bool[] ReadLightOnStatus { get; /*set;*/ }

        public LightControlProxy()
        {
            SerialComm = new SerialComm("COM2", 38400, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);
            SerialComm.SplilterStr = "\n";
            SerialComm.OnRecvData += SerialComm_OnRecvData;
        }

        private void SerialComm_OnRecvData(byte[] recvData)
        {
            int nLightOnOff = 0, nCh = 0, nBrightness = 0;
            string s = Encoding.ASCII.GetString(recvData), sTemp = string.Empty;
            if (string.Compare(s.Substring(0, cmdGetAllLightOnOff.Length), cmdGetAllLightOnOff) == 0)
            {
                string[] sAllData = s.Split(' ');
                nLightOnOff = GetHexToInt(sAllData[2]);
                if ((nCh != -1) && (nLightOnOff != -1))
                {
                    ReadLightOnStatus[0] = ((nLightOnOff & 0x01) == 0x01) ? true : false;
                    ReadLightOnStatus[1] = ((nLightOnOff & 0x02) == 0x02) ? true : false;
                    ReadLightOnStatus[2] = ((nLightOnOff & 0x04) == 0x04) ? true : false;
                    ReadLightOnStatus[3] = ((nLightOnOff & 0x08) == 0x08) ? true : false;
                    IsReadSuccessLightOnStatus = true;
                }
                else
                {
                    IsReadSuccessLightOnStatus = false;
                }
                IsReadCompleteLightOnStatus = true;
            }
            else if (string.Compare(s.Substring(0, cmdGetLightOnOff.Length), cmdGetLightOnOff) == 0)
            {
                string[] sAllData = s.Split(' ');
                if (!int.TryParse(sAllData[2], out nCh)) { nCh = -1; }
                if (!int.TryParse(sAllData[3], out nLightOnOff)) { nLightOnOff = -1; }
                if ((nCh != -1) && (nLightOnOff != -1))
                {
                    ReadLightOnStatus[nCh] = nLightOnOff == 0 ? false : true;
                    IsReadSuccessLightOnStatus = true;
                }
                else
                {
                    IsReadSuccessLightOnStatus = false;
                }
                IsReadCompleteLightOnStatus = true;
            }
            else if (string.Compare(s.Substring(0, cmdGetBrightness.Length), cmdGetBrightness) == 0)
            {
                string[] sAllData = s.Split(' ');
                if (!int.TryParse(sAllData[2], out nCh)) { nCh = -1; }
                if (!int.TryParse(sAllData[3], out nBrightness)) { nBrightness = -1; }
                if ((nCh != -1) && (nBrightness != -1))
                {
                    ReadBrightness[nCh] = nBrightness;
                    IsReadSuccessBrightness = true;
                }
                else
                {
                    IsReadSuccessBrightness = false;
                }

                IsReadCompleteBrightness = true;
            }
        }

        public bool OpenPort()
        {
            return SerialComm.Open();
        }

        public void ClosePort()
        {
            SerialComm.Close();
        }

        public bool SetBrightness(int nCh, int nValue)
        {
            bool bSuccess = true;
            IsReadCompleteBrightness = false;
            string sCmd = string.Format("{0} {1} {2}\r", cmdSetBrightness, nCh, nValue);

            try
            {
                byte[] byteCmd = Encoding.ASCII.GetBytes(sCmd);
                SerialComm.SendData(byteCmd, 0, byteCmd.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }
            return bSuccess;


        }

        public bool SetAllLightOnOff(bool bOnOff)
        {
            bool bSuccess = true;
            IsReadCompleteBrightness = false;

            string sCmd = string.Format("{0} {1}\r", cmdSetAllLightOnOff, bOnOff ? "0F" : "00");

            try
            {
                byte[] byteCmd = Encoding.ASCII.GetBytes(sCmd);
                SerialComm.SendData(byteCmd, 0, byteCmd.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }

            return bSuccess;
        }

        public bool SetLightOnOff(int nCh, bool bOnOff)
        {
            bool bSuccess = true;
            IsReadCompleteBrightness = false;

            string sCmd = string.Format("{0} {1} {2}\r", cmdSetLightOnOff, nCh, bOnOff ? 1 : 0);

            try
            {
                byte[] byteCmd = Encoding.ASCII.GetBytes(sCmd);
                SerialComm.SendData(byteCmd, 0, byteCmd.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }

            return bSuccess;
        }

        public bool GetBrightness(int nCh)
        {
            bool bSuccess = true;
            IsReadCompleteBrightness = false;

            string sCmd = string.Format("{0} {1}\r", cmdGetBrightness, nCh);

            try
            {
                byte[] byteCmd = Encoding.ASCII.GetBytes(sCmd);
                SerialComm.SendData(byteCmd, 0, byteCmd.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }
            return bSuccess;
        }

        public bool GetAllLightOnOff()
        {
            bool bSuccess = true;
            IsReadCompleteBrightness = false;

            string sCmd = string.Format("{0}\r", cmdGetAllLightOnOff);

            try
            {
                byte[] byteCmd = Encoding.ASCII.GetBytes(sCmd);
                SerialComm.SendData(byteCmd, 0, byteCmd.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }

            return bSuccess;
        }

        public bool GetLightOnOff(int nCh)
        {
            bool bSuccess = true;
            IsReadCompleteBrightness = false;

            string sCmd = string.Format("{0} {1}\r", cmdGetLightOnOff, nCh);

            try
            {
                byte[] byteCmd = Encoding.ASCII.GetBytes(sCmd);
                SerialComm.SendData(byteCmd, 0, byteCmd.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }

            return bSuccess;
        }

        private int GetHexToInt(string sHex)
        {
            int nValue = 0;
            if (string.Compare(sHex, "0x00000000") == 0) nValue = 0;
            else if (string.Compare(sHex, "0x00000001") == 0) nValue = 1;
            else if (string.Compare(sHex, "0x00000002") == 0) nValue = 2;
            else if (string.Compare(sHex, "0x00000003") == 0) nValue = 3;
            else if (string.Compare(sHex, "0x00000004") == 0) nValue = 4;
            else if (string.Compare(sHex, "0x00000005") == 0) nValue = 5;
            else if (string.Compare(sHex, "0x00000006") == 0) nValue = 6;
            else if (string.Compare(sHex, "0x00000007") == 0) nValue = 7;
            else if (string.Compare(sHex, "0x00000008") == 0) nValue = 8;
            else if (string.Compare(sHex, "0x00000009") == 0) nValue = 9;
            else if ((string.Compare(sHex, "0x0000000A") == 0) || (string.Compare(sHex, "0x0000000a") == 0)) nValue = 10;
            else if ((string.Compare(sHex, "0x0000000B") == 0) || (string.Compare(sHex, "0x0000000b") == 0)) nValue = 11;
            else if ((string.Compare(sHex, "0x0000000C") == 0) || (string.Compare(sHex, "0x0000000c") == 0)) nValue = 12;
            else if ((string.Compare(sHex, "0x0000000D") == 0) || (string.Compare(sHex, "0x0000000d") == 0)) nValue = 13;
            else if ((string.Compare(sHex, "0x0000000E") == 0) || (string.Compare(sHex, "0x0000000e") == 0)) nValue = 14;
            else if ((string.Compare(sHex, "0x0000000F") == 0) || (string.Compare(sHex, "0x0000000f") == 0)) nValue = 15;
            else nValue = -1;

            return nValue; 
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DIT.TLC.CTRL
{
    public class BeamPositionerProxy
    {
        private SerialComm SerialComm;

        private byte[] cmdReadStart = new byte[6] { (int)70, (int)97, (int)0, (int)1, (int)55, (int)153 };
        private byte[] cmdReadStop = new byte[6] { (int)70, (int)97, (int)0, (int)1, (int)70, (int)168 };

        public bool IsReadSuccess { get; set; }

        public bool IsReadComplete { get; set; }

        public int ReadCount { get; set; }

        public double ReadPower { get; set; }

        public double ReadPosX { get; set; }

        public double ReadPosY { get; set; }

        public BeamPositionerProxy(string portNmae)
        {
            SerialComm = new SerialComm(portNmae, 115200, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);
            SerialComm.SplilterStr = null;
            SerialComm.SpliltingCount = 9;
            SerialComm.OnRecvData += SerialComm_OnRecvData;
        }

        string recvDataTemp = string.Empty;
        private void SerialComm_OnRecvData(byte[] recvData)
        {
            byte[] byteRecvData = recvData;
            if ((byte)70 != byteRecvData[0]) return;

            int nChkSum = 0;
            nChkSum += byteRecvData[1];
            nChkSum += byteRecvData[2];
            nChkSum += byteRecvData[3];
            nChkSum += byteRecvData[4];
            nChkSum += byteRecvData[5];
            nChkSum += byteRecvData[6];
            nChkSum += byteRecvData[7];
            nChkSum %= 256;

            if (nChkSum == byteRecvData[8])
            {
                ReadCount = byteRecvData[1];
                ReadPower = (byteRecvData[2] * 256 + byteRecvData[3]) / 100;
                ReadPosX = (byteRecvData[4] * 256 + byteRecvData[5]);
                ReadPosY = (byteRecvData[6] * 256 + byteRecvData[7]);
                IsReadSuccess = true;
            }
            else
            {
                ReadCount = 0;
                ReadPower = 0;
                ReadPosX = 0;
                ReadPosY = 0;
                IsReadSuccess = false;
            }
            IsReadComplete = true;
        }

        public bool OpenPort()
        {
            return SerialComm.Open();
        }

        public void ClosePort()
        {
            SerialComm.Close();
        }

        public bool SendStartCmd() // JICHA [20181121] - 시작 명령을 전송하면, 종료 명령을 전송할 때까지 9바이트의 측정 결과를 계속 보내줌.
        {
            bool bSuccess = true;

            try
            {
                SerialComm.SendData(cmdReadStart, 0, cmdReadStart.Length);
            }
            catch (Exception ex)
            {
                bSuccess = false;
            }
            return bSuccess;
        }

        public bool SendEndCmd() // JICHA [20181121] - 시작 명령을 전송하면, 종료 명령을 전송할 때까지 9바이트의 측정 결과를 계속 보내줌.
        {
            bool bSuccess = true;
            IsReadSuccess = false;
            IsReadComplete = false;

            try
            {
                SerialComm.SendData(cmdReadStop, 0, cmdReadStop.Length);
            }
            catch (Exception)
            {
                bSuccess = false;
            }
            return bSuccess;
        }
    }
}

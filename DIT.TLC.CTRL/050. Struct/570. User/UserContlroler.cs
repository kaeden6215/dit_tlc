﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DIT.TLC.CTRL
{
    public class UserInfoContlroler
    {
        public static string PathOfXml = Path.Combine(Application.StartupPath, "Setting", "UserInfo.Xml");

        public List<UserInfo> LstUserInfos = new List<UserInfo>();
        public UserInfo GetUserInfo(string userID, string password)
        {
            return LstUserInfos.FirstOrDefault(f => f.UserID == userID && f.Password == password);
        }


        public static void Save(UserInfoContlroler info)
        {
            XmlFileManager<UserInfoContlroler>.TrySaveXml(PathOfXml, info);
        }
        public static UserInfoContlroler Load()
        {
            UserInfoContlroler info = new UserInfoContlroler();
            XmlFileManager<UserInfoContlroler>.TryLoadData(PathOfXml, out info);
            
            return info;
        }
    }
}

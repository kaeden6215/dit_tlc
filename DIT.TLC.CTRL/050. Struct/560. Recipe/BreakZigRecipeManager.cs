﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{

    public class BreakZigRecipe : ICloneable
    {
        public PointD BreakingZigXY { get; set; }
        public double BreakingZigThickness { get; set; }
        public double BreakingZigCamToPin { get; set; }

        public double BreakingOnTime { get; set; }
        public double BreakingCassettePitchNormal { get; set; }
        public double BreakingCassettePitchReverse { get; set; }

        public double BreakingJumpSpeed { get; set; }
        public double BreakingDefaultZ { get; set; }
        public double BreakinAlignMatch { get; set; }
        public double BreakinPreAlignErrorAng { get; set; }
        public double BreakinErrorY { get; set; }
        public double BreakinAlignDistance { get; set; }
        public double BreakinPreAlignErrorX { get; set; }
        public double BreakinPreAlignAngle { get; set; }
        public double BreakinPreAlignErrorY { get; set; }


        public BreakZigRecipe()
        {
            BreakingZigXY = new PointD(0, 0);
            BreakingZigThickness = 0;
            BreakingZigCamToPin = 0;

            BreakingOnTime = 0;
            BreakingCassettePitchNormal = 0;
            BreakingCassettePitchReverse = 0;

            BreakingJumpSpeed = 0;
            BreakingDefaultZ = 0;
            BreakinAlignMatch = 0;
            BreakinPreAlignErrorAng = 0;
            BreakinErrorY = 0;
            BreakinAlignDistance = 0;
            BreakinPreAlignErrorX = 0;
            BreakinPreAlignAngle = 0;
            BreakinPreAlignErrorY = 0;

        }


        public object Clone()
        {
            BreakZigRecipe info = new BreakZigRecipe();
            info.BreakingZigXY.X                             /**/ = this.BreakingZigXY.X;
            info.BreakingZigXY.Y                             /**/ = this.BreakingZigXY.Y;
            info.BreakingZigThickness                        /**/ = this.BreakingZigThickness;
            info.BreakingZigCamToPin                         /**/ = this.BreakingZigCamToPin;

            info.BreakingOnTime                              /**/ = this.BreakingOnTime;
            info.BreakingCassettePitchNormal                 /**/ = this.BreakingCassettePitchNormal;
            info.BreakingCassettePitchReverse                /**/ = this.BreakingCassettePitchReverse;

            info.BreakingJumpSpeed                           /**/ = this.BreakingJumpSpeed;
            info.BreakingDefaultZ                            /**/ = this.BreakingDefaultZ;
            info.BreakinAlignMatch                           /**/ = this.BreakinAlignMatch;
            info.BreakinPreAlignErrorAng                     /**/ = this.BreakinPreAlignErrorAng;
            info.BreakinErrorY                               /**/ = this.BreakinErrorY;
            info.BreakinAlignDistance                        /**/ = this.BreakinAlignDistance;
            info.BreakinPreAlignErrorX                       /**/ = this.BreakinPreAlignErrorX;
            info.BreakinPreAlignAngle                        /**/ = this.BreakinPreAlignAngle;
            info.BreakinPreAlignErrorY                        /**/ = this.BreakinPreAlignErrorY;
            return info;
        }
    }

    public class BreakZigRecipeManager
    {
        public static string PathOfXml = Path.Combine(GG.StartupPath, "Setting", "BreakZigRecipeManager.Xml");

        public List<BreakZigRecipe> LstBreakZigRcp;

        public BreakZigRecipeManager()
        {
            LstBreakZigRcp = new List<BreakZigRecipe>();
        }

        public void Save()
        {
            XmlFileManager<List<BreakZigRecipe>>.TrySaveXml(PathOfXml, LstBreakZigRcp);
        }
        public void Load()
        {
            if(XmlFileManager<List<BreakZigRecipe>>.TryLoadData(PathOfXml, out LstBreakZigRcp) ==false)
            {
                LstBreakZigRcp = new List<BreakZigRecipe>();
            }
        }
    }
}

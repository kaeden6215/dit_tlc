﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    [Serializable]
    public class BreakRecipe : ICloneable
    {
        public string Name { get; set; }
        public PointD XYPinCenter { get; set; }
        public PointD XYAlignMak1 { get; set; }
        public PointD XYAlignMak2 { get; set; }
        public PointD XYMCR { get; set; }
        public double PinDistanceA { get; set; }
        public double PinDistanceB { get; set; }


        public PointBreaking XYTBreaking1 { get; set; }
        public PointBreaking XYTBreaking2 { get; set; }
        public PointBreaking XYTBreaking3 { get; set; }
        public PointBreaking XYTBreaking4 { get; set; }

        public PointD A1XYOffset { get; set; }
        public PointD A2XYOffset { get; set; }
        public PointD B1XYOffset { get; set; }
        public PointD B2XYOffset { get; set; }

        public PointD AMCRXYOffset { get; set; }
        public PointD BMCRXYOffset { get; set; }

        public double ZAxisHighSpeedDownPitch { get; set; }
        public double ZAxisLowSpeedDownPitch { get; set; }

        public double ZAxisHighSpeedDownSpeed { get; set; }
        public double ZAxisLowSpeedDownSpeed { get; set; }

        //로드 이재기 Sequence Offset
        public double ASequnceOffsetY1 { get; set; }
        public double ASequnceOffsetY2 { get; set; }
        public double BSequnceOffsetY1 { get; set; }
        public double BSequnceOffsetY2 { get; set; }

        public BreakRecipe()
        {
            Name = string.Empty;
            XYPinCenter = new PointD(0.000, 0.000);
            XYAlignMak1 = new PointD(0.000, 0.000);
            XYAlignMak2 = new PointD(0.000, 0.000);
            XYMCR = new PointD(0.000, 0.000);
            PinDistanceA = 0.000;
            PinDistanceB = 0.000;


            XYTBreaking1 = new PointBreaking(0.000, 0.000, 0.000);
            XYTBreaking2 = new PointBreaking(0.000, 0.000, 0.000);
            XYTBreaking3 = new PointBreaking(0.000, 0.000, 0.000);
            XYTBreaking4 = new PointBreaking(0.000, 0.000, 0.000);


            A1XYOffset = new PointD(0.000, 0.000);
            A2XYOffset = new PointD(0.000, 0.000);
            B1XYOffset = new PointD(0.000, 0.000);
            B2XYOffset = new PointD(0.000, 0.000);

            AMCRXYOffset = new PointD(0.000, 0.000);
            BMCRXYOffset = new PointD(0.000, 0.000);

            ZAxisHighSpeedDownPitch = 0.000;
            ZAxisLowSpeedDownPitch = 0.000;

            ZAxisHighSpeedDownSpeed = 0.000;
            ZAxisLowSpeedDownSpeed = 0.000;

            ASequnceOffsetY1 = 0.000;
            ASequnceOffsetY2 = 0.000;
            BSequnceOffsetY1 = 0.000;
            BSequnceOffsetY2 = 0.000;
        }

    public object Clone()
        {
            BreakRecipe info = new BreakRecipe();
            //info.Name                                   /**/ = this.Name;
            info.XYPinCenter.X                          /**/ = this.XYPinCenter.X;
            info.XYPinCenter.Y                          /**/ = this.XYPinCenter.Y;
            info.XYAlignMak1.X                          /**/ = this.XYAlignMak1.X;
            info.XYAlignMak1.Y                          /**/ = this.XYAlignMak1.Y;
            info.XYAlignMak2.X                          /**/ = this.XYAlignMak2.X;
            info.XYAlignMak2.Y                          /**/ = this.XYAlignMak2.Y;
            info.XYMCR.X                                /**/ = this.XYMCR.X;
            info.XYMCR.Y                                /**/ = this.XYMCR.Y;
            info.PinDistanceA                           /**/ = this.PinDistanceA;
            info.PinDistanceB                           /**/ = this.PinDistanceB;
            info.XYTBreaking1.Left                      /**/ = this.XYTBreaking1.Left;
            info.XYTBreaking1.Right                     /**/ = this.XYTBreaking1.Right;
            info.XYTBreaking1.Half                      /**/ = this.XYTBreaking1.Half;
            info.XYTBreaking2.Left                      /**/ = this.XYTBreaking2.Left;
            info.XYTBreaking2.Right                     /**/ = this.XYTBreaking2.Right;
            info.XYTBreaking2.Half                      /**/ = this.XYTBreaking2.Half;
            info.XYTBreaking3.Left                      /**/ = this.XYTBreaking3.Left;
            info.XYTBreaking3.Right                     /**/ = this.XYTBreaking3.Right;
            info.XYTBreaking3.Half                      /**/ = this.XYTBreaking3.Half;
            info.XYTBreaking4.Left                      /**/ = this.XYTBreaking4.Left;
            info.XYTBreaking4.Right                     /**/ = this.XYTBreaking4.Right;
            info.XYTBreaking4.Half                      /**/ = this.XYTBreaking4.Half;
            info.A1XYOffset.X                           /**/ = this.A1XYOffset.X;
            info.A1XYOffset.Y                           /**/ = this.A1XYOffset.Y;
            info.A2XYOffset.X                           /**/ = this.A2XYOffset.X;
            info.A2XYOffset.Y                           /**/ = this.A2XYOffset.Y;
            info.B1XYOffset.X                           /**/ = this.B1XYOffset.X;
            info.B1XYOffset.Y                           /**/ = this.B1XYOffset.Y;
            info.B2XYOffset.X                           /**/ = this.B2XYOffset.X;
            info.B2XYOffset.Y                           /**/ = this.B2XYOffset.Y;
            info.AMCRXYOffset.X                         /**/ = this.AMCRXYOffset.X;
            info.AMCRXYOffset.Y                         /**/ = this.AMCRXYOffset.Y;
            info.BMCRXYOffset.X                         /**/ = this.BMCRXYOffset.X;
            info.BMCRXYOffset.Y                         /**/ = this.BMCRXYOffset.Y;
            info.ZAxisHighSpeedDownPitch                /**/ = this.ZAxisHighSpeedDownPitch;
            info.ZAxisLowSpeedDownPitch                 /**/ = this.ZAxisLowSpeedDownPitch;
            info.ZAxisHighSpeedDownSpeed                /**/ = this.ZAxisHighSpeedDownSpeed;
            info.ZAxisLowSpeedDownSpeed                 /**/ = this.ZAxisLowSpeedDownSpeed;
            info.ASequnceOffsetY1                       /**/ = this.ASequnceOffsetY1;
            info.ASequnceOffsetY2                       /**/ = this.ASequnceOffsetY2;
            info.BSequnceOffsetY1                       /**/ = this.BSequnceOffsetY1;
            info.BSequnceOffsetY2                       /**/ = this.BSequnceOffsetY2;
            return info;
        }
    }
}


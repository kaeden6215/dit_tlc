﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    [Serializable]
    public class ProcessRecipe : ICloneable
    {
        public string Name { get; set; }
        public double Divider { get; set; }
        public double Power { get; set; }
        public double PowerRatio { get; set; }
        public double Error { get; set; }
        public double Burst { get; set; }
        public double Frequence { get; set; }
        public double Speed { get; set; }
        public double ZPos { get; set; }
        public double Overlap { get; set; }
        public double SegmentValue { get; set; }
        public double G { get; set; }
        public double Scan { get; set; }
        public double AccDec { get; set; }

        public ProcessRecipe()
        {
             Name           = string.Empty;
             Divider        = 0.000;
             Power          = 0.000;
             PowerRatio     = 0.000;
             Error          = 0.000;
             Burst          = 0.000;
             Frequence      = 0.000;
             Speed          = 0.000;
             ZPos           = 0.000;
             Overlap        = 0.000;
             SegmentValue   = 0.000;
             G              = 0.000;
             Scan           = 0.000;
             AccDec         = 0.000;
    }

        public object Clone()
        {
            ProcessRecipe info = new ProcessRecipe();
            info.Name                          /**/ = this.Name;
            info.Divider                       /**/ = this.Divider;
            info.Power                         /**/ = this.Power;
            info.PowerRatio                    /**/ = this.PowerRatio;
            info.Error                         /**/ = this.Error;
            info.Burst                         /**/ = this.Burst;
            info.Frequence                     /**/ = this.Frequence;
            info.Speed                         /**/ = this.Speed;
            info.ZPos                          /**/ = this.ZPos;
            info.Overlap                       /**/ = this.Overlap;
            info.SegmentValue                  /**/ = this.SegmentValue;
            info.G                             /**/ = this.G;
            info.Scan                          /**/ = this.Scan;
            info.AccDec                        /**/ = this.AccDec;
            return info;
        }
    }
}

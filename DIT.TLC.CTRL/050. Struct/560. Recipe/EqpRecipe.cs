﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    [Serializable]
    public class PointD
    {
        public double X { get; set; }
        public double Y { get; set; }

        public PointD()
        {
            X = 0;
            Y = 0;
        }
        public PointD(double x, double y)
        {
            x = X;
            y = Y;
        }
    }

    [Serializable]
    public class PointBreaking
    {
        public double Left { get; set; }
        public double Right { get; set; }
        public double Half { get; set; }

        public PointBreaking()
        {
            Left = 0;
            Right = 0;
            Half = 0;
        }

        public PointBreaking(double left, double right, double half)
        {
            left = Left;
            right = Right;
            half = Half;
        }
    }

    [Serializable]
    public class EqpRecipe : ICloneable
    {

        public bool Used { get; set; }
        public bool Cim { get; set; }
        public string Name { get; set; }

        public double AlignImagePath { get; set; }

        public PointD Table1AlignMark1 { get; set; }
        public PointD Table1AlignMark2 { get; set; }

        public PointD Table2AlignMark1 { get; set; }
        public PointD Table2AlignMark2 { get; set; }

        public PointD BreakLeft1 { get; set; }
        public PointD BreakLeft2 { get; set; }
        public double BreakLeftY { get; set; }

        public PointD BreakRight1 { get; set; }
        public PointD BreakRight2 { get; set; }
        public double BreakRightY { get; set; }

        public PointD AMatrixSize { get; set; }


        //LayerInfo
        public string DxfPath { get; set; }
        public string AlignMentLayer { get; set; }
        public string ProcessLayer { get; set; }
        public string GuideLayer { get; set; }
        public string ProcessParam { get; set; }
        public string CellType { get; set; }
        public double TaThickness { get; set; }


        //Cassette Setting
        public double CassetteWidth { get; set; }
        public double CassetteHeight { get; set; }

        public double CassetteCellOutWaitHitch = 10;
        public double CassetteCellOutUpHitch = 10; //

        public int CassetteMaxHeightCount { get; set; }
        public int CassetteCellCount { get; set; }
        public bool CstNormal { get; set; }
        public emCstType CstType { get; set; }

        public emPickerType LDPickerType { get; set; }


        public emPickerType ULDPickerType { get; set; }


        //LDS Process Setting        
        public PointD LDSProcess01 { get; set; }
        public PointD LDSProcess02 { get; set; }
        public PointD LDSProcess03 { get; set; }
        public PointD LDSProcess04 { get; set; }

        //LDS Break Setting        
        public PointD LDSBreak01 { get; set; }
        public PointD LDSBreak02 { get; set; }
        public PointD LDSBreak03 { get; set; }
        public PointD LDSBreak04 { get; set; }

        //Y-Offset
        public double OffsetPre { get; set; }
        public double OffsetBreak { get; set; }

        //CutLine Pos
        public List<PointD> AColL1Pos { get; set; }
        public List<PointD> AColL2Pos { get; set; }
        public List<PointD> BColR1Pos { get; set; }
        public List<PointD> BColR2Pos { get; set; }

        //Insp Pos A열
        public PointD ALeftTop { get; set; }
        public PointD ARightTop { get; set; }
        public double ATopZ { get; set; }
        public PointD ALeftBot { get; set; }
        public PointD ARightBot { get; set; }
        public double ABotZ { get; set; }
        public double AXSpeed { get; set; }
        public double AYSpeed { get; set; }
        public double AMoveSpeed { get; set; }

        //Insp Pos B열
        public PointD BLeftTop { get; set; }
        public PointD BRightTop { get; set; }
        public double BTopZ { get; set; }
        public PointD BLeftBot { get; set; }
        public PointD BRightBot { get; set; }
        public double BBotZ { get; set; }
        public double BXSpeed { get; set; }
        public double BYSpeed { get; set; }
        public double BMoveSpeed { get; set; }
        public int GetCstMaxCol { get { return CassetteCellCount / CassetteMaxHeightCount; } }

        public EqpRecipe()
        {
            //info.Used                             /**/ = this.Used;
            //info.Cim                              /**/ = this.Cim;
            Name = string.Empty;
            Table1AlignMark1 = new PointD(0.000, 0.000);
            Table1AlignMark2 = new PointD(0.000, 0.000);
            Table2AlignMark1 = new PointD(0.000, 0.000);
            Table2AlignMark2 = new PointD(0.000, 0.000);
            BreakLeft1 = new PointD(0.000, 0.000);
            BreakLeft2 = new PointD(0.000, 0.000);
            BreakLeftY = 0.000;
            BreakRight1 = new PointD(0.000, 0.000);
            BreakRight2 = new PointD(0.000, 0.000);
            BreakRightY = 0.000;
            AMatrixSize = new PointD(0.000, 0.000);
            //info.DxfPath              
            AlignMentLayer = "align";
            ProcessLayer = "CUT";
            GuideLayer = "guide";
            ProcessParam = string.Empty;
            CellType = string.Empty;
            TaThickness = 0.000;

            //Cassette
            CassetteWidth = 15.000;
            CassetteHeight = 10.000;
            CassetteMaxHeightCount = 40;
            CassetteCellCount = 160;
            CstNormal = true;
            CstType = emCstType.Cst4;

            //Picker
            LDPickerType = emPickerType.Normal;

            ULDPickerType = emPickerType.Normal;

            //LDS Process
            LDSProcess01 = new PointD(0.000, 0.000);
            LDSProcess02 = new PointD(0.000, 0.000);
            LDSProcess03 = new PointD(0.000, 0.000);
            LDSProcess04 = new PointD(0.000, 0.000);

            //LDS Break
            LDSBreak01 = new PointD(0.000, 0.000);
            LDSBreak02 = new PointD(0.000, 0.000);
            LDSBreak03 = new PointD(0.000, 0.000);
            LDSBreak04 = new PointD(0.000, 0.000);

            //Y-Offset
            OffsetPre = 0.000;
            OffsetBreak = 0.000;

            //CutLine Pos
            AColL1Pos = new List<PointD>();
            AColL2Pos = new List<PointD>();
            BColR1Pos = new List<PointD>();
            BColR2Pos = new List<PointD>();

            //Insp Pos A열
            ALeftTop = new PointD(0.000, 0.000);
            ARightTop = new PointD(0.000, 0.000);
            ATopZ = 0.000;
            ALeftBot = new PointD(0.000, 0.000);
            ARightBot = new PointD(0.000, 0.000);
            ABotZ = 0.000;
            AXSpeed = 0.000;
            AYSpeed = 0.000;
            AMoveSpeed = 0.000;

            //Insp Pos B열
            BLeftTop = new PointD(0.000, 0.000);
            BRightTop = new PointD(0.000, 0.000);
            BTopZ = 0.000;
            BLeftBot = new PointD(0.000, 0.000);
            BRightBot = new PointD(0.000, 0.000);
            BBotZ = 0.000;
            BXSpeed = 0.000;
            BYSpeed = 0.000;
            BMoveSpeed = 0.000;
        }
        public object Clone()
        {
            EqpRecipe info = new EqpRecipe();
            //info.Used                             /**/ = this.Used;
            //info.Cim                              /**/ = this.Cim;
            info.Name                               /**/ = this.Name;
            info.Table1AlignMark1.X                 /**/ = this.Table1AlignMark1.X;
            info.Table1AlignMark1.Y                 /**/ = this.Table1AlignMark1.Y;
            info.Table1AlignMark2.X                 /**/ = this.Table1AlignMark2.X;
            info.Table1AlignMark2.Y                 /**/ = this.Table1AlignMark2.Y;
            info.Table2AlignMark1.X                 /**/ = this.Table2AlignMark1.X;
            info.Table2AlignMark1.Y                 /**/ = this.Table2AlignMark1.Y;
            info.BreakLeft1.X                       /**/ = this.BreakLeft1.X;
            info.BreakLeft1.Y                       /**/ = this.BreakLeft1.Y;
            info.BreakLeft2.X                       /**/ = this.BreakLeft2.X;
            info.BreakLeft2.Y                       /**/ = this.BreakLeft2.Y;
            info.BreakLeftY                         /**/ = this.BreakLeftY;
            info.BreakRight1.X                      /**/ = this.BreakRight1.X;
            info.BreakRight1.Y                      /**/ = this.BreakRight1.Y;
            info.BreakRight2.X                      /**/ = this.BreakRight2.X;
            info.BreakRight2.Y                      /**/ = this.BreakRight2.Y;
            info.BreakRightY                        /**/ = this.BreakRightY;
            info.AMatrixSize.X                      /**/ = this.AMatrixSize.X;
            info.AMatrixSize.Y                      /**/ = this.AMatrixSize.Y;
            //info.DxfPath                          /**/ = this.DxfPath;
            info.AlignMentLayer                     /**/ = this.AlignMentLayer;
            info.ProcessLayer                       /**/ = this.ProcessLayer;
            info.GuideLayer                         /**/ = this.GuideLayer;
            info.ProcessParam                       /**/ = this.ProcessParam;
            info.CellType                           /**/ = this.CellType;
            info.TaThickness                        /**/ = this.TaThickness;

            //Cassette
            info.CassetteWidth                      /**/ = this.CassetteWidth;
            info.CassetteHeight                     /**/ = this.CassetteHeight;
            info.CassetteMaxHeightCount             /**/ = this.CassetteMaxHeightCount;
            info.CassetteCellCount                  /**/ = this.CassetteCellCount;
            info.CstNormal                          /**/ = this.CstNormal;
            info.CstType                            /**/ = this.CstType;

            //Picker
            info.LDPickerType                      /**/ = this.LDPickerType;
            info.ULDPickerType                     /**/ = this.ULDPickerType;

            //LDS Process
            info.LDSProcess01.X                     /**/ = this.LDSProcess01.X;
            info.LDSProcess01.Y                     /**/ = this.LDSProcess01.Y;
            info.LDSProcess02.X                     /**/ = this.LDSProcess02.X;
            info.LDSProcess02.Y                     /**/ = this.LDSProcess02.Y;
            info.LDSProcess03.X                     /**/ = this.LDSProcess03.X;
            info.LDSProcess03.Y                     /**/ = this.LDSProcess03.Y;
            info.LDSProcess04.X                     /**/ = this.LDSProcess04.X;
            info.LDSProcess04.Y                     /**/ = this.LDSProcess04.Y;

            //LDS Break
            info.LDSBreak01.X                       /**/ = this.LDSBreak01.X;
            info.LDSBreak01.Y                       /**/ = this.LDSBreak01.Y;
            info.LDSBreak02.X                       /**/ = this.LDSBreak02.X;
            info.LDSBreak02.Y                       /**/ = this.LDSBreak02.Y;
            info.LDSBreak03.X                       /**/ = this.LDSBreak03.X;
            info.LDSBreak03.Y                       /**/ = this.LDSBreak03.Y;
            info.LDSBreak04.X                       /**/ = this.LDSBreak04.X;
            info.LDSBreak04.Y                       /**/ = this.LDSBreak04.Y;

            //Y-Offset
            info.OffsetPre                          /**/ = this.OffsetPre;
            info.OffsetBreak                        /**/ = this.OffsetBreak;

            //CutLine Pos
            info.AColL1Pos                          /**/ = this.AColL1Pos;
            info.AColL2Pos                          /**/ = this.AColL2Pos;
            info.BColR1Pos                          /**/ = this.BColR1Pos;
            info.BColR2Pos                          /**/ = this.BColR2Pos;

            //Insp Pos A열
            ALeftTop                                /**/ = this.ALeftTop;
            ARightTop                               /**/ = this.ARightTop;
            ATopZ                                   /**/ = this.ATopZ;
            ALeftBot                                /**/ = this.ALeftBot;
            ARightBot                               /**/ = this.ARightBot;
            ABotZ                                   /**/ = this.ABotZ;
            AXSpeed                                 /**/ = this.AXSpeed;
            AYSpeed                                 /**/ = this.AYSpeed;
            AMoveSpeed                              /**/ = this.AMoveSpeed;

            //Insp Pos B열
            BLeftTop                                /**/ = this.BLeftTop;
            BRightTop                               /**/ = this.BRightTop;
            BTopZ                                   /**/ = this.BTopZ;
            BLeftBot                                /**/ = this.BLeftBot;
            BRightBot                               /**/ = this.BRightBot;
            BBotZ                                   /**/ = this.BBotZ;
            BXSpeed                                 /**/ = this.BXSpeed;
            BYSpeed                                 /**/ = this.BYSpeed;
            BMoveSpeed                              /**/ = this.BMoveSpeed;

            return info;
        }
    }
    [Serializable]
    public enum emPickerType
    {
        Normal,
        CW90,
        CCW90,
        CW180,
    }
    [Serializable]
    public enum emCstType
    {
        Cst1,
        Cst2,
        Cst4,
        Cst8,
    }
}
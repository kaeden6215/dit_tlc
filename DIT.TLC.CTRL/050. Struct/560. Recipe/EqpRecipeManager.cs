﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class EqpRecipeManager
    {
        public static string PathOfXml = Path.Combine(GG.StartupPath, "Setting", "EqpRecipeManager.Xml");

        public List<EqpRecipe> LstEqpRecipe;
        public EqpRecipeManager()
        {
            LstEqpRecipe = new List<EqpRecipe>();
        }
        public void Save()
        {
            XmlFileManager<List<EqpRecipe>>.TrySaveXml(PathOfXml, LstEqpRecipe);
        }
        public void Load()
        {
            if (XmlFileManager<List<EqpRecipe>>.TryLoadData(PathOfXml, out LstEqpRecipe) == false)
            {
                LstEqpRecipe = new List<EqpRecipe>();
            }
        }
    }
}

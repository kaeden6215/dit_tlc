﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Timers;
using System.Diagnostics;
using Dit.Framework.PLC;


namespace DIT.TLC.CTRL
{
    public class Switch : UnitBase
    {
        public Func<bool> InterLockFunc { get; set; }

        public PlcAddr YB_OnOff { get; set; }
        public PlcAddr XB_OnOff { get; set; }
        public Stopwatch OnOffStartTime = new Stopwatch();

        public int ON_OVERTIME { get; set; }
        public int OFF_OVERTIME { get; set; }
        public EM_AL_LST ON_TIME_OUT_ERROR { get; set; }
        public EM_AL_LST OFF_TIME_OUT_ERROR { get; set; }
        public string OnOffTime { get; set; }
        public string OffOnTime { get; set; }

        public bool YBitOnOff_Simul { get; set; }
        public bool XBitOnOff_Simul { get; set; }

        public bool Simulaotr { get; set; }


        public Switch()
        {
            ON_OVERTIME = 1000;
            OFF_OVERTIME = 1000;
            ON_TIME_OUT_ERROR = EM_AL_LST.AL_NONE;
            OFF_TIME_OUT_ERROR = EM_AL_LST.AL_NONE;
        }
        public virtual void OnOff(Equipment equip, bool value)
        {
            if (InterLockFunc != null)
                if (InterLockFunc() == true)
                {
                    return;
                }

            if (Simulaotr)
            {
                YBitOnOff_Simul = XBitOnOff_Simul = value;
            }
            else
            {
                OnOffStartTime.Restart();
                YB_OnOff.vBit = value;
            }
        }
        public virtual bool IsOnOff { get { return Simulaotr ? XBitOnOff_Simul : XB_OnOff.vBit; } }
        public virtual bool IsSolOnOff { get { return Simulaotr ? YBitOnOff_Simul : YB_OnOff.vBit; } }
        public override void LogicWorking(Equipment equip)
        {
            UpdateTime();

            if (YB_OnOff.vBit == true && XB_OnOff.vBit == false)
            {
                if (OnOffStartTime.ElapsedMilliseconds > ON_OVERTIME)
                {
                    AlarmManager.Instance.Happen(equip, ON_TIME_OUT_ERROR);
                    OnOffStartTime.Stop();
                }
            }

            if (YB_OnOff.vBit == false && XB_OnOff.vBit == true)
            {
                if (OnOffStartTime.ElapsedMilliseconds > OFF_OVERTIME)
                {
                    AlarmManager.Instance.Happen(equip, OFF_TIME_OUT_ERROR);
                    OnOffStartTime.Stop();
                }
            }
        }
        protected virtual void UpdateTime()
        {
            if (XB_OnOff.vBit != YB_OnOff.vBit && (YB_OnOff.vBit == true))
                OffOnTime = (OnOffStartTime.ElapsedMilliseconds / 1000).ToString("00.#");

            if (XB_OnOff.vBit != YB_OnOff.vBit && (YB_OnOff.vBit == false))
                OnOffTime = (OnOffStartTime.ElapsedMilliseconds / 1000).ToString("00.#");
        }
    }
}

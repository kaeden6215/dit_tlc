﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL.BaseUnit
{
    public class UsingTimeChecker
    {
        public bool[] IsUsed { get; set; }
        public DateTime[] UsingTime { get; set; }
        public int[] Deadline { get; set; }
        private int _unitCount;

        public EM_AL_LST[] LAMP_SET_TIME_OVER { get; set; }

        public UsingTimeChecker(int unitCount)
        {
            _unitCount = unitCount;

            IsUsed = new bool[_unitCount];
            UsingTime = new DateTime[_unitCount];
            Deadline = new int[_unitCount];
            LAMP_SET_TIME_OVER = new EM_AL_LST[_unitCount];
        }
        public void InitilizeUsingTime(PcUsingTimeSetting UsingTimeSetting)
        {
            Func<string, DateTime> convert = delegate(string value)
            {
                return value != "" ? Convert.ToDateTime(value) : DateTime.Now;
            };

            bool[] isUsed = {
                               Convert.ToBoolean(UsingTimeSetting.Lamp1_IsUsing),
                               Convert.ToBoolean(UsingTimeSetting.Lamp2_IsUsing),
                               Convert.ToBoolean(UsingTimeSetting.Lamp3_IsUsing),
                               Convert.ToBoolean(UsingTimeSetting.Lamp4_IsUsing),
                               Convert.ToBoolean(UsingTimeSetting.Lamp5_IsUsing),
                               Convert.ToBoolean(UsingTimeSetting.Lamp6_IsUsing),
                               Convert.ToBoolean(UsingTimeSetting.Lamp7_IsUsing),
                               Convert.ToBoolean(UsingTimeSetting.Lamp8_IsUsing),                               
                            };
            DateTime[] time = {
                                  convert(UsingTimeSetting.Lamp1),
                                  convert(UsingTimeSetting.Lamp2),
                                  convert(UsingTimeSetting.Lamp3),
                                  convert(UsingTimeSetting.Lamp4),
                                  convert(UsingTimeSetting.Lamp5),
                                  convert(UsingTimeSetting.Lamp6),
                                  convert(UsingTimeSetting.Lamp7),
                                  convert(UsingTimeSetting.Lamp8)
                              };
            int[] deadline = {
                                UsingTimeSetting.Lamp1_Deadline, 
                                UsingTimeSetting.Lamp2_Deadline, 
                                UsingTimeSetting.Lamp3_Deadline, 
                                UsingTimeSetting.Lamp4_Deadline, 
                                UsingTimeSetting.Lamp5_Deadline, 
                                UsingTimeSetting.Lamp6_Deadline, 
                                UsingTimeSetting.Lamp7_Deadline, 
                                UsingTimeSetting.Lamp8_Deadline
                             };

            for (int iter = 0; iter < _unitCount; iter++)
            {
                IsUsed[iter] = isUsed[iter];
                UsingTime[iter] = time[iter];
                Deadline[iter] = deadline[iter];
            }
        }
        public void Reset(int index)
        {
            UsingTime[index] = new DateTime();
        }
        public void LogicWorking(Equipment equip)
        {            
            for (int iter = 0; iter < _unitCount; iter++)
            {
                if (IsUsed[iter] == false) continue;
                
                TimeSpan interval = DateTime.Now - UsingTime[iter];
                if (interval.TotalHours >= Deadline[iter])
                {
                    AlarmMgr.Instance.Happen(equip, LAMP_SET_TIME_OVER[iter]);
                }
            }            
        }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public class CylinderTwo : UnitBase
    {
        public Func<Equipment, EmCylinderDirect, bool> CheckStartMoveInterLockFunc;


        public int FORWARD_OVERTIME = 5000;
        public int BACKWARD_OVERTIME = 5000;
		private float _currentDirectionOverTime = 5000;

        public PlcAddr YB_ForwardCmd { get; set; }
        public PlcAddr YB_BackwardCmd { get; set; }

        public PlcAddr XB_ForwardCompleteP1 { get; set; }
        public PlcAddr XB_BackwardCompleteP1 { get; set; }

        public PlcAddr XB_ForwardCompleteP2 { get; set; }
        public PlcAddr XB_BackwardCompleteP2 { get; set; }

        public DateTime ForwardStartTime { get; set; }
        public DateTime BackwardStartTime { get; set; }

        public EM_AL_LST[] AlcdForwardTimeOut = new EM_AL_LST[2];
        public EM_AL_LST[] AlcdBackwardTimeOut = new EM_AL_LST[2];

        public string ForwardTimeP1 { get; set; }
        public string BackwardTimeP1 { get; set; }

        public string ForwardTimeP2 { get; set; }
        public string BackwardTimeP2 { get; set; }

        private DateTime _errorStartTimeP1 = DateTime.Now;
        private DateTime _errorStartTimeP2 = DateTime.Now;

        private bool _isFirstChkP1 = false;
        private bool _isFirstChkP2 = false;

        public CylinderTwo()
        {
            AlcdForwardTimeOut[0] = EM_AL_LST.AL_NONE;
            AlcdForwardTimeOut[1] = EM_AL_LST.AL_NONE;

            AlcdBackwardTimeOut[0] = EM_AL_LST.AL_NONE;
            AlcdBackwardTimeOut[1] = EM_AL_LST.AL_NONE;
        }

        public virtual bool Forward(Equipment equip)
        {
            ForwardStartTime = DateTime.Now;
            YB_ForwardCmd.vBit = true;
            YB_BackwardCmd.vBit = false;

            return true;
        }
        public virtual bool Backward(Equipment equip)
        {
            BackwardStartTime = DateTime.Now;
            YB_ForwardCmd.vBit = false;
            YB_BackwardCmd.vBit = true;

            return true;
        }


        public bool IsForward
        {
            get
            {
                return XB_ForwardCompleteP1.vBit == true && XB_ForwardCompleteP2.vBit == true &&
                       XB_BackwardCompleteP1.vBit == false && XB_BackwardCompleteP2.vBit == false;
            }
        }
        public bool IsForwarding
        {
            get
            {
                return IsForward == false && YB_ForwardCmd == true;
            }
        }
        public bool IsBackward
        {
            get
            {
                return XB_ForwardCompleteP1.vBit == false && XB_ForwardCompleteP2.vBit == false &&
                          XB_BackwardCompleteP1.vBit == true && XB_BackwardCompleteP2.vBit == true;
            }
        }
        public bool IsBackwarding
        {
            get
            {
                return IsBackward == false && YB_BackwardCmd == true;
            }
        }

        public override void LogicWorking(Equipment equip)
        {
            UpdateTime();

            if (XB_ForwardCompleteP1.vBit == false && XB_BackwardCompleteP1.vBit == false && _isFirstChkP1 == true)
            {
                _errorStartTimeP1 = DateTime.Now;
                _isFirstChkP1 = false;
            }
            else if (XB_ForwardCompleteP1.vBit != XB_BackwardCompleteP1.vBit)
            {
                _errorStartTimeP1 = DateTime.Now;
                _isFirstChkP1 = true;
            }

			_currentDirectionOverTime = YB_ForwardCmd.vBit ? FORWARD_OVERTIME : BACKWARD_OVERTIME;
            if ((DateTime.Now - _errorStartTimeP1).TotalMilliseconds > _currentDirectionOverTime)
            {
                if (YB_ForwardCmd.vBit == true)
                {
                    AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[0]);
                }
                else if (YB_BackwardCmd.vBit == true)
                {
                    AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[0]);
                }
                //else
                //{
                //    AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[0]);
                //    AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[0]);
                //}
            }

            if (XB_ForwardCompleteP2.vBit == false && XB_BackwardCompleteP2.vBit == false && _isFirstChkP2 == true)
            {
                _errorStartTimeP2 = DateTime.Now;
                _isFirstChkP2 = false;
            }
            else if (XB_ForwardCompleteP2.vBit != XB_BackwardCompleteP2.vBit)
            {
                _errorStartTimeP2 = DateTime.Now;
                _isFirstChkP2 = true;
            }

            if ((DateTime.Now - _errorStartTimeP2).TotalMilliseconds > _currentDirectionOverTime)
            {
                if (YB_ForwardCmd.vBit == true)
                {
                    AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[1]);
                }
                else if (YB_BackwardCmd.vBit == true)
                {
                    AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[1]);
                }
                //else
                //{
                //    AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[1]);
                //    AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[1]);
                //}
            }

            if (XB_ForwardCompleteP1.vBit && XB_BackwardCompleteP1.vBit)
            {
                //Logger.Log.AppendLine(LogLevel.Error, "Error occurred by cylinder concurrent completing.");
                //Console.WriteLine("실린더 완료 동시 ON Error");
            }

            if (XB_ForwardCompleteP2.vBit && XB_BackwardCompleteP2.vBit)
            {
                //Logger.Log.AppendLine(LogLevel.Error, "Error occurred by cylinder concurrent completing.");
                //Console.WriteLine("실린더 완료 동시 ON Error");
            }
            if (YB_ForwardCmd.vBit && YB_BackwardCmd.vBit)
            {
                //Logger.Log.AppendLine(LogLevel.Error, "Error occurred by cylinder concurrent order");
                //Console.WriteLine("실린더 명령 동시 ON Error");
            }

            if (YB_ForwardCmd.vBit && XB_ForwardCompleteP1.vBit && XB_ForwardCompleteP2.vBit)
            {
                YB_ForwardCmd.vBit = false;
            }
            else if (YB_BackwardCmd.vBit && XB_BackwardCompleteP1.vBit && XB_BackwardCompleteP2.vBit)
            {
                YB_BackwardCmd.vBit = false;
            }
            else
            {
                if (YB_ForwardCmd.vBit == true && (XB_ForwardCompleteP1.vBit == false || XB_ForwardCompleteP2.vBit == false))
                {
                    if ((DateTime.Now - ForwardStartTime).TotalMilliseconds > FORWARD_OVERTIME)
                    {
                        if (XB_ForwardCompleteP1.vBit == false)
                            AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[0]);
                        if (XB_ForwardCompleteP2.vBit == false)
                            AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[1]);

                        YB_ForwardCmd.vBit = false;
                    }
                }
                if (YB_BackwardCmd.vBit == true && (XB_BackwardCompleteP1.vBit == false || XB_BackwardCompleteP2.vBit == false))
                {
                    if ((DateTime.Now - BackwardStartTime).TotalMilliseconds > BACKWARD_OVERTIME)
                    {
                        if (XB_BackwardCompleteP1.vBit == false)
                            AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[0]);
                        if (XB_BackwardCompleteP2.vBit == false)
                            AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[1]);

                        YB_BackwardCmd.vBit = false;
                    }
                }
            }


        }

        private void UpdateTime()
        {
            if (YB_ForwardCmd.vBit == true && XB_ForwardCompleteP1.vBit == false)
                ForwardTimeP1 = string.Format("{0:0.#}", (DateTime.Now - ForwardStartTime).TotalMilliseconds / 1000);
            if (YB_BackwardCmd.vBit == true && XB_BackwardCompleteP1.vBit == false)
                BackwardTimeP1 = string.Format("{0:0.#}", (DateTime.Now - BackwardStartTime).TotalMilliseconds / 1000);
            if (YB_ForwardCmd.vBit == true && XB_ForwardCompleteP2.vBit == false)
                ForwardTimeP2 = string.Format("{0:0.#}", (DateTime.Now - ForwardStartTime).TotalMilliseconds / 1000);
            if (YB_BackwardCmd.vBit == true && XB_BackwardCompleteP2.vBit == false)
                BackwardTimeP2 = string.Format("{0:0.#}", (DateTime.Now - BackwardStartTime).TotalMilliseconds / 1000);
        }

        //메소드 - 공통 인터락 확인 사항 
        public virtual bool IsInterlock(Equipment equip, bool isBackward)
        {
            if (equip.IsUseInterLockOff) return false;

            if (equip.IsHeavyAlarm == true && GG.TestMode == false)
            {
                InterLockMgr.AddInterLock("Interlock <HEAVY ALARM> \n (HEAVY ALARM. Centering operation disabled)");
                return true;
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Timers;

namespace DIT.TLC.CTRL
{
    public class OffCheckSwitch : Switch
    {
        private int _offCheckInterval = 1;

        public OffCheckSwitch()
        {
            
        }
        public override void LogicWorking(Equipment equip)
        {
            if (equip.IsUseDoorInterLockOff == true || equip.IsUseInterLockOff) return;

            if (YB_OnOff.vBit == false && XB_OnOff.vBit == true)
            {
                if (OnOffStartTime.ElapsedMilliseconds > _offCheckInterval)
                {
                    AlarmManager.Instance.Happen(equip, OFF_TIME_OUT_ERROR);
                }
            }

            if (YB_OnOff.vBit == true && XB_OnOff.vBit == true)
            {
                AlarmManager.Instance.Happen(equip, OFF_TIME_OUT_ERROR);
            }
        }
    }
}

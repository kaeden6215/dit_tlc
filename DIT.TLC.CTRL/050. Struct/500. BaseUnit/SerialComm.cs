﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Data;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{


    public class SerialComm
    {
        private SerialPort _serialPort;
        public string _totalRecvData = string.Empty;
        public byte[] _totalRecvBytesData;
        public string SplilterStr { get; set; }
        public int SpliltingCount { get; set; }

        public event Action<byte[]> OnRecvData;

        public SerialComm()
        {
            _serialPort = new SerialPort();
            _totalRecvData = string.Empty;
            SplilterStr = Environment.NewLine;
        }

        public SerialComm(string comPort, int baudRate, int dataBits, Parity parity, StopBits stopBits, Handshake handShake = Handshake.None)
        {
            _serialPort = new SerialPort();

            _serialPort.PortName = comPort;
            _serialPort.BaudRate = baudRate;
            _serialPort.DataBits = dataBits;
            _serialPort.Parity = parity;
            //_serialPort.StopBits = stopBits;
            _serialPort.ReadTimeout = (int)800;
            _serialPort.WriteTimeout = (int)500;
            _serialPort.Handshake = handShake;
            _serialPort.Encoding = Encoding.Default;
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(SP_DataReveived);
        }

        public bool Open()
        {
            bool bSuccess = true;
            if (GG.TestMode == true) return bSuccess;

            try
            {
                _totalRecvData = string.Empty;
                if (!_serialPort.IsOpen) _serialPort.Open();
            }
            catch (Exception ex)
            {
                bSuccess = false;
            }

            return bSuccess;
        }

        public bool Close()
        {
            bool bSuccess = true;
            if (GG.TestMode == true) return true;

            try
            {
                if (_serialPort.IsOpen) _serialPort.Close();
            }
            catch (Exception ex)
            {
                bSuccess = false;
            }

            return bSuccess;
        }

        public bool IsOpen
        {
            get { return _serialPort.IsOpen; }
        }

        public void SP_DataReveived(object sender, SerialDataReceivedEventArgs e)
        {
            if (SplilterStr != null)
            {
                _totalRecvData = _totalRecvData + _serialPort.ReadExisting();
                int inx = _totalRecvData.IndexOf(SplilterStr);
                while (true)
                {
                    if (inx <= 0) break;

                    string recvData = _totalRecvData.Substring(0, inx);
                    _totalRecvData = _totalRecvData.Substring(inx + SplilterStr.Length);

                    if (OnRecvData != null)
                        OnRecvData(Encoding.ASCII.GetBytes(recvData));

                    inx = _totalRecvData.IndexOf(SplilterStr);
                }
            }
            else
            {
                int Count = _serialPort.BytesToRead;
                _totalRecvBytesData = new byte[Count];
                _serialPort.Read(_totalRecvBytesData, 0, Count);

                while (true)
                {
                    if (Count < SpliltingCount) break;

                    byte[] recvData = new byte[9];
                    Array.Copy(_totalRecvBytesData, 0, recvData, 0, SpliltingCount);
                    
                    if (OnRecvData != null)
                        OnRecvData(recvData);

                    Array.Copy(_totalRecvBytesData, SpliltingCount, _totalRecvBytesData, 0, Count - SpliltingCount);
                    Count -= SpliltingCount;
                }
            }
        }
        public bool SendData(string sCmd)
        {
            bool bSuccess = true;
            try
            {
                _serialPort.Write(sCmd);
            }
            catch (Exception ex)
            {
            }
            return bSuccess;
        }
        public bool SendData(byte[] byteCmd, int nOffset, int nCount)
        {
            bool bSuccess = true;
            try
            {
                _serialPort.Write(byteCmd, nOffset, nCount);
            }
            catch (Exception ex)
            {

            }
            return bSuccess;
        }
    }
}

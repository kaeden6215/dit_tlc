﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Timers;

namespace DIT.TLC.CTRL
{
    public class SwitchNormalClose :  Switch
    {
        private bool _isFirst = true;

        public SwitchNormalClose()
            :base()
        {
            
        }
        public override void OnOff(Equipment equip, bool value)
        {
            if (InterLockFunc != null)
                if (InterLockFunc() == true)
                {
                    return;
                }
            
            OnOffStartTime.Restart();
            YB_OnOff.vBit = !value;
        }
        public override bool IsSolOnOff { get { return !YB_OnOff.vBit; } }
        public override void LogicWorking(Equipment equip)
        {
            if (_isFirst == true)
            {
                YB_OnOff.vBit = !XB_OnOff.vBit;
                _isFirst = false;
            }
            UpdateTime();

            if (YB_OnOff.vBit == false && XB_OnOff.vBit == false)
            {
                if (OnOffStartTime.ElapsedMilliseconds > ON_OVERTIME)
                {                    
                    AlarmManager.Instance.Happen(equip, ON_TIME_OUT_ERROR);
                }
            }

            if (YB_OnOff.vBit == true && XB_OnOff.vBit == true)
            {
                if (OnOffStartTime.ElapsedMilliseconds > OFF_OVERTIME)
                {                    
                    AlarmManager.Instance.Happen(equip, OFF_TIME_OUT_ERROR);
                }
            }
        }
        protected override void UpdateTime()
        {
            if (XB_OnOff.vBit == YB_OnOff.vBit && (YB_OnOff.vBit == true))
                OffOnTime = (OnOffStartTime.ElapsedMilliseconds / 1000).ToString("00.#");

            if (XB_OnOff.vBit == YB_OnOff.vBit && (YB_OnOff.vBit == false))
                OnOffTime = (OnOffStartTime.ElapsedMilliseconds / 1000).ToString("00.#");
        }       
    }
}

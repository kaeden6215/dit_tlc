﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class RespSensor : UnitBase
    {
        private bool _isAContact;
        public bool IsOnOff
        {
            get
            {
                if (_isAContact)
                    return XB_OnOff.vBit == true;
                else
                    return XB_OnOff.vBit == false;
            }
        }
        public PlcAddr XB_OnOff { get; set; }
        public PlcAddr YB_OnOff { get; set; }

        public RespSensor(bool isAContact = true)
        {
            this._isAContact = isAContact;
        }

        public override void LogicWorking(Equipment equip)
        {
            YB_OnOff.vBit = XB_OnOff.vBit;
        }
    }
}

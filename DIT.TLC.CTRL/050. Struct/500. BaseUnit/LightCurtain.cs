﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class LightCurtainProxy : UnitBase
    {
        public ToggleRespSensor MutingBtnSwitch                           /**/   = new ToggleRespSensor();
        public Sensor ResetBtn                                            /**/   = new Sensor();
        public SwitchOneWay ResetBtnLamp                                   /**/   = new SwitchOneWay();


        public PlcAddr XB_MutingOnOff { get; set; }
        public PlcAddr XB_Warning_Detect_BJub { get; set; }

        public PlcAddr YB_Reset { get; set; }

        public PlcAddr YB_MutingOnOffCMD1 { get; set; }
        public PlcAddr YB_MutingOnOffCMD2 { get; set; }


        private int _mutingStep = 0;
        private int _resetStep = 0;
        private PlcTimer _plcTmrMuting = new PlcTimer();

        public LightCurtainProxy()
        {
        }

        private PlcTimer plcTmr = new PlcTimer();
        public override void LogicWorking(Equipment equip)
        {
            MutingBtnSwitch.LogicWorking(equip);

            YB_MutingOnOffCMD1.vBit = MutingBtnSwitch.IsOnOff;
            YB_MutingOnOffCMD2.vBit = MutingBtnSwitch.IsOnOff;
            ResetBtnLamp.YB_OnOff.vBit = ResetBtn.XB_OnOff.vBit || XB_Warning_Detect_BJub.vBit;

            if (_resetStep == 0)
            {
                if (ResetBtn.XB_OnOff.vBit == true && XB_Warning_Detect_BJub.vBit == false)
                {
                    _resetStep = 10;
                }
            }
            else if (_resetStep == 10)
            {
                YB_Reset.vBit = true;
                plcTmr.Start(3);
                _resetStep = 20;
            }
            else if (_resetStep == 20)
            {
                if (plcTmr)
                    if (XB_MutingOnOff.vBit == true)
                    {
                        YB_Reset.vBit = false;
                        _resetStep = 30;
                    }
            }
            else if (_resetStep == 30)
            {
                _resetStep = 0;
            }
        }

        public void Reset()
        {
            if (_resetStep == 0)
            {
                _resetStep = 10;
            }
        }
        public void Muting()
        {
            if (_mutingStep == 0)
            {
                _mutingStep = 10;
            }
        }
    }
}

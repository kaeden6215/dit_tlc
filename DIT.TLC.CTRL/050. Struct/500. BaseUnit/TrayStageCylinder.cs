﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class TrayStageCylinder : Cylinder
    {
        public override bool IsInterlock(Equipment equip, bool isUnlock)
        {
            if (base.IsInterlock(equip, isUnlock) == true) return true;
            return false;
        }
        public override bool Forward(Equipment equip)
        {
            if (IsInterlock(equip, false)) return false;

            if (equip.IsUseInterLockOff)
            {
                // 인터락 해제시 해제되는 인터락
            }
            // 인터락 해제 불가 인터락

            base.Forward(equip);
            return true;
        }
        public override bool Backward(Equipment equip)
        {
            if (IsInterlock(equip, true)) return false;

            if (equip.IsUseInterLockOff)
            {
                // 인터락 해제시 해제되는 인터락
            }
            // 인터락 해제 불가 인터락


            base.Backward(equip);
            return true;
        }
    }
}

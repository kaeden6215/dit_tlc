﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Timers;
using System.Diagnostics;
using Dit.Framework.PLC;


namespace DIT.TLC.CTRL
{
    public class Switch2Cmd1Sensor : UnitBase
    {
        public Func<bool> InterLockFunc { get; set; }

        public PlcAddr YB_On { get; set; }
        public PlcAddr YB_Off { get; set; }

        public PlcAddr XB_OnOff { get; set; }
        public Stopwatch OnOffStartTime = new Stopwatch();

        public int ON_OVERTIME { get; set; }
        public int OFF_OVERTIME { get; set; }
        public EM_AL_LST ON_TIME_OUT_ERROR { get; set; }
        public EM_AL_LST OFF_TIME_OUT_ERROR { get; set; }
        public string OnOffTime { get; set; }
        public string OffOnTime { get; set; }

        public bool YBitOn_Simul { get; set; }
        public bool YBOffBit_Simul { get; set; }

        public bool XBitOnOff_Simul { get; set; }

        public bool Simulaotr { get; set; }

        public Switch2Cmd1Sensor()
        {
            ON_OVERTIME = 1000;
            OFF_OVERTIME = 1000;
            ON_TIME_OUT_ERROR = EM_AL_LST.AL_NONE;
            OFF_TIME_OUT_ERROR = EM_AL_LST.AL_NONE;
        }
        public virtual bool OnOff(Equipment equip, bool value)
        {

            if (InterLockFunc != null)
                if (InterLockFunc() == true)
                {
                    return false;
                }

            if (Simulaotr)
            {
                XBitOnOff_Simul = YBitOn_Simul = value;
            }
            else
            {
                OnOffStartTime.Restart();
                YB_On.vBit = value;
                YB_Off.vBit = !value;
            }
            return true;
        }
        public virtual bool IsOnOff { get { return (Simulaotr) ? XBitOnOff_Simul : XB_OnOff.vBit; } }
        public virtual bool IsSolOnOff { get { return (Simulaotr) ? YBitOn_Simul : YB_On.vBit; } }
        public override void LogicWorking(Equipment equip)
        {
            UpdateTime();

            if (YB_On.vBit == true && XB_OnOff.vBit == false)
            {
                if (OnOffStartTime.ElapsedMilliseconds > ON_OVERTIME)
                {
                    AlarmManager.Instance.Happen(equip, ON_TIME_OUT_ERROR);
                    OnOffStartTime.Stop();
                }
            }

            if (YB_On.vBit == false && XB_OnOff.vBit == true)
            {
                if (OnOffStartTime.ElapsedMilliseconds > OFF_OVERTIME)
                {
                    AlarmManager.Instance.Happen(equip, OFF_TIME_OUT_ERROR);
                    OnOffStartTime.Stop();
                }
            }
        }
        protected virtual void UpdateTime()
        {
            if (XB_OnOff.vBit != YB_On.vBit && (YB_On.vBit == true))
                OffOnTime = (OnOffStartTime.ElapsedMilliseconds / 1000).ToString("00.#");

            if (XB_OnOff.vBit != YB_On.vBit && (YB_On.vBit == false))
                OnOffTime = (OnOffStartTime.ElapsedMilliseconds / 1000).ToString("00.#");
        }
    }
}

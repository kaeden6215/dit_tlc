﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public enum EmCylinderDirect
    {
        Forward,
        Backward,
    }

    public class Cylinder : UnitBase
    {
        public Func<Equipment, EmCylinderDirect, bool> CheckStartMoveInterLockFunc;

        public int FORWARD_OVERTIME = GG.TestMode ? 50000000:  5000;
        public int BACKWARD_OVERTIME = GG.TestMode ? 50000000 : 5000;
        private float _currentDirectionOverTime = GG.TestMode ? 50000000 : 5000;

        public PlcAddr YB_ForwardCmd { get; set; }
        public PlcAddr YB_BackwardCmd { get; set; }

        public PlcAddr XB_ForwardComplete { get; set; }
        public PlcAddr XB_BackwardComplete { get; set; }

        public DateTime ForwardStartTime { get; set; }
        public DateTime BackwardStartTime { get; set; }



        public bool YB_ForwardCmd_Simul { get; set; }
        public bool YB_BackwardCmd_Simul { get; set; }

        public bool XB_ForwardComplete_Simul { get; set; }
        public bool XB_BackwardComplete_Simul { get; set; }


        public EM_AL_LST[] AlcdForwardTimeOut = new EM_AL_LST[2];
        public EM_AL_LST[] AlcdBackwardTimeOut = new EM_AL_LST[2];

        public string ForwardTime { get; set; }
        public string BackwardTime { get; set; }

        private DateTime _errorStartTime = DateTime.Now;
        private bool _isFirstChk = false;


        public Cylinder()
        {
            AlcdForwardTimeOut[0] = EM_AL_LST.AL_NONE;
            AlcdForwardTimeOut[1] = EM_AL_LST.AL_NONE;

            AlcdBackwardTimeOut[0] = EM_AL_LST.AL_NONE;
            AlcdBackwardTimeOut[1] = EM_AL_LST.AL_NONE;
        }

        public virtual bool Forward(Equipment equip)
        {
            ForwardStartTime = DateTime.Now;

            if (false)
            {
                XB_ForwardComplete_Simul= YB_ForwardCmd_Simul = true;
                XB_BackwardComplete_Simul = YB_BackwardCmd_Simul = false;

            }
            else
            {
                YB_ForwardCmd.vBit = true;
                YB_BackwardCmd.vBit = false;

            }

            return true;
        }
        public virtual bool Backward(Equipment equip)
        {
            BackwardStartTime = DateTime.Now;

            if (false)
            {
                XB_ForwardComplete_Simul = YB_ForwardCmd_Simul = false;
                XB_BackwardComplete_Simul= YB_BackwardCmd_Simul = true;
            }
            else
            {
                YB_ForwardCmd.vBit = false;
                YB_BackwardCmd.vBit = true;
            }

            return true;
        }
        public bool IsForward
        {
            get
            {
                if (false)
                {
                    return XB_ForwardComplete_Simul == true && XB_BackwardComplete_Simul == false;
                }
                else
                {
                    return XB_ForwardComplete.vBit == true && XB_BackwardComplete.vBit == false;
                }
            }
        }
        public bool IsForwarding
        {
            get
            {
                return IsForward == false && YB_ForwardCmd == true;
            }
        }
        public bool IsBackward
        {
            get
            {
                if (false)
                {
                    return XB_BackwardComplete_Simul == true && XB_ForwardComplete_Simul == false;
                }
                else
                {
                    return XB_BackwardComplete.vBit == true && XB_ForwardComplete.vBit == false;
                }
            }
        }
        public bool IsBackwarding
        {
            get
            {
                return IsBackward == false && YB_BackwardCmd == true;
            }
        }

        public override void LogicWorking(Equipment equip)
        {
            UpdateTime();

            if (XB_ForwardComplete.vBit == false && XB_BackwardComplete.vBit == false && _isFirstChk == true)
            {
                _errorStartTime = DateTime.Now;
                _isFirstChk = false;
            }
            else if (XB_ForwardComplete.vBit != XB_BackwardComplete.vBit)
            {
                _errorStartTime = DateTime.Now;
                _isFirstChk = true;
            }

            _currentDirectionOverTime = YB_ForwardCmd.vBit ? FORWARD_OVERTIME : BACKWARD_OVERTIME;
            if ((DateTime.Now - _errorStartTime).TotalMilliseconds > _currentDirectionOverTime)
            {
                if (YB_ForwardCmd.vBit == true)
                {
                    AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[0]);
                }
                else if (YB_BackwardCmd.vBit == true)
                {
                    AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[0]);
                }
                else
                {
                    AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[0]);
                    AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[0]);
                }
            }

            if (XB_ForwardComplete.vBit && XB_BackwardComplete.vBit)
            {
                //Logger.Log.AppendLine(LogLevel.Error, "Error occurred by cylinder concurrent completing.");
                //Console.WriteLine("실린더 완료 동시 ON Error");
            }


            if (YB_ForwardCmd.vBit && YB_BackwardCmd.vBit)
            {
                //Logger.Log.AppendLine(LogLevel.Error, "Error occurred by cylinder concurrent order");
                //Console.WriteLine("실린더 명령 동시 ON Error");
            }


            if (YB_ForwardCmd.vBit && XB_ForwardComplete.vBit)
            {
                YB_ForwardCmd.vBit = false;
            }
            else if (YB_BackwardCmd.vBit && XB_BackwardComplete.vBit)
            {
                YB_BackwardCmd.vBit = false;
            }
            else
            {
                if (YB_ForwardCmd.vBit == true && XB_ForwardComplete.vBit == false)
                {
                    if ((DateTime.Now - ForwardStartTime).TotalMilliseconds > FORWARD_OVERTIME)
                    {
                        if (XB_ForwardComplete.vBit == false)
                            AlarmManager.Instance.Happen(equip, AlcdForwardTimeOut[0]);

                        YB_ForwardCmd.vBit = false;
                    }
                }
                if (YB_BackwardCmd.vBit == true && XB_BackwardComplete.vBit == false)
                {
                    if ((DateTime.Now - BackwardStartTime).TotalMilliseconds > BACKWARD_OVERTIME)
                    {
                        if (XB_BackwardComplete.vBit == false)
                            AlarmManager.Instance.Happen(equip, AlcdBackwardTimeOut[0]);

                        YB_BackwardCmd.vBit = false;
                    }
                }
            }
        }

        private void UpdateTime()
        {
            if (YB_ForwardCmd.vBit == true && XB_ForwardComplete.vBit == false)
                ForwardTime = string.Format("{0:0.#}", (DateTime.Now - ForwardStartTime).TotalMilliseconds / 1000);
            if (YB_BackwardCmd.vBit == true && XB_BackwardComplete.vBit == false)
                BackwardTime = string.Format("{0:0.#}", (DateTime.Now - BackwardStartTime).TotalMilliseconds / 1000);
        }

        //메소드 - 공통 인터락 확인 사항 
        public virtual bool IsInterlock(Equipment equip, bool isBackward)
        {
            if (equip.IsUseInterLockOff) return false;

            if (equip.IsHeavyAlarm == true && GG.TestMode == false)
            {
                InterLockMgr.AddInterLock("Interlock <HEAVY ALARM> \n (HEAVY ALARM. Centering operation disabled)");
                return true;
            }

            return false;
        }
    }
}

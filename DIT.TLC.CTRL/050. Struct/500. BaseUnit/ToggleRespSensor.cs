﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;


namespace DIT.TLC.CTRL
{


    //기능 확인  필요.
    public class ToggleRespSensor : UnitBase
    {
        private bool _isAContact;
        public bool IsOnOff
        {
            get
            {
                if (_isAContact)
                    return YB_OnOff.vBit == true;
                else
                    return YB_OnOff.vBit == false;
            }
        }
        public PlcAddr XB_OnOff { get; set; }
        public PlcAddr YB_OnOff { get; set; }

        public ToggleRespSensor(bool isAContact = true)
        {
            this._isAContact = isAContact;
        }

        public int Step = 0;
        public override void LogicWorking(Equipment equip)
        {
            //YB_OnOff.vBit = XB_OnOff.vBit;

            if (Step == 0)
            {
                if (XB_OnOff.vBit == true)
                {
                    Step = 10;
                }
            }
            else if (Step == 10)
            {
                YB_OnOff.vBit = !YB_OnOff.vBit;
                Step = 20;
            }
            else if (Step == 20)
            {
                if (XB_OnOff.vBit == false)
                {
                    Step = 30;
                }
            }
            else if (Step == 30)
            {
                Step = 0;
            }
        }
    }
}

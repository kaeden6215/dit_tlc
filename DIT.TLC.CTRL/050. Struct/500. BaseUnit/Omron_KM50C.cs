﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.IO.Ports;
using System.Timers;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Diagnostics;

namespace DIT.TLC.CTRL
{
    public static class Extensions
    {
        public static string Right(this string value, int startFromBack, int length)
        {
            return value.Substring(value.Length - startFromBack, length);
        }
    }

    public class Omron_KM50C : UnitBase
    {
        #region COMMAND
        public const char STX = (char)0x02; //This code (02 hex) indicates the beginning of a communications frame.
        public const char ETX = (char)0x03; //This code (03 hex) indicates the end of a communications frame.
        /* (01 : Node No / 00 : Sub-address, km50에서는 항상 0 / 0 : SID(Service ID), 항상 0
         *  01 : MRC / 01 :SRC / __ : Variable Type / ____ : Address / 00 : Bit positio, 여기서 항상 00 / ____ : No of element*/
        public const string READ_VOLTAGE_TO_CURRENT = "010000101C80000000006";
        public const string READ_POWER_VALUE = "010000101C80008000005";
        #endregion

        private SerialPort _SP;
        private System.Timers.Timer _updateTimer = new System.Timers.Timer();
        private bool _waitForResponse;
        private int _readTimeout;
        private int _readInterval;

        public float Dec_T_Current { get; set; }
        public float Dec_R_Current { get; set; }
        public float Dec_T_Voltage { get; set; }
        public float Dec_R_Voltage { get; set; }
        public float Dec_T_Power { get; set; }
        public float Dec_I_Power { get; set; }

        private int _nowCommand = 0;

        public Omron_KM50C(Equipment equip)
        {
            _SP = new SerialPort();
            _readInterval = 500;
            _waitForResponse = false;
            _updateTimer = new System.Timers.Timer();
            _SP.PortName = equip.InitSetting.KM50NPSPort;
            _SP.ReadTimeout = 800;
            _SP.BaudRate = (int)9600;
            _SP.WriteTimeout = (int)500;
            _SP.Handshake = Handshake.None;
            _SP.Encoding = Encoding.Default;
            _SP.StopBits = StopBits.One;
            _SP.DataBits = (int)8;
            _SP.Parity = Parity.None;
            _SP.DataReceived += new SerialDataReceivedEventHandler(SP_DataReveived);
            Init();
        }

        public void SP_DataReveived(object sender, SerialDataReceivedEventArgs e)
        {
            _waitForResponse = false;
            _readTimeout = _SP.ReadTimeout;

            readVariableValues();
            if ((_nowCommand >= 0) && (_nowCommand < 3))
                _nowCommand += 1;
            else
                _nowCommand = 0;
        }

        private void readVariableValues()
        {
            if (_SP.IsOpen)
            {
                try
                {
                    if (_nowCommand == 0)
                    {
                        String msg = _SP.ReadExisting();
                        string[] strgs = msg.Split(Omron_KM50C.ETX);
                        string T_Current = strgs[0].Trim().Right(8, 8);
                        string R_Current = strgs[0].Trim().Right(24, 8);
                        string T_Voltage = strgs[0].Trim().Right(32, 8);
                        string R_Voltage = strgs[0].Trim().Right(48, 8);
                        Dec_T_Current = HexToInt(T_Current) / 10.0f;
                        Dec_R_Current = HexToInt(R_Current) / 10.0f;
                        Dec_T_Voltage = HexToInt(T_Voltage) / 10.0f;
                        Dec_R_Voltage = HexToInt(R_Voltage) / 10.0f;
                    }

                    else if (_nowCommand == 1)
                    {
                        String msg = _SP.ReadExisting();
                        string[] strgs = msg.Split(Omron_KM50C.ETX);
                        string T_Power = strgs[0].Trim().Right(8, 8);
                        string I_Power = strgs[0].Trim().Right(40, 8);
                        Dec_T_Power = HexToInt(T_Power) / 10.0f;
                        Dec_I_Power = HexToInt(I_Power) / 10.0f;
                    }
                }
                catch (Exception) { }
            }
        }

        public Int32 HexToInt(string hex)
        {
            if (string.IsNullOrEmpty(hex))
                throw new FormatException("값 오류!");
            try
            {
                //hex값 int로 변경
                Int32 num = Int32.Parse(hex.ToUpper(), NumberStyles.HexNumber);
                return num;
            }

            catch (FormatException)
            {
                throw new FormatException("");
            }
        }

        public char GetBlockCheckCharacter(string frame)
        {
            int BCC = 0;
            Encoding ascii = Encoding.ASCII;
            byte[] encodeBytes = ascii.GetBytes(frame + ETX);
            foreach (byte item in encodeBytes)
            {
                BCC = BCC ^ item;
            }
            return (char)BCC;
        }

        public void Start()
        {
            if (GG.TestMode) return;

            Open();

            if (!_SP.IsOpen)
                return;

            try
            {
                _updateTimer.Start();
            }
            catch (NullReferenceException ex)
            {
                Logger.Log.AppendLine(LogLevel.Exception, "Omron : {0}", ex);
            }
        }

        public bool Open()
        {
            try
            {
                _SP.Open();
            }

            catch (System.IO.IOException SerialPortNullEx)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Omron : Serial Port Open Error ({0})", SerialPortNullEx);

                return false;
            }

            if (!_SP.IsOpen)
            {
                return false;
            }
            return true;
        }

        public bool Init()
        {
            _updateTimer.Interval = _readInterval;
            _updateTimer.Elapsed += new ElapsedEventHandler(UpdateOmron);
            return true;
        }

        private void UpdateOmron(object sender, ElapsedEventArgs e)
        {
            if (false == _waitForResponse)
            {
                if (_nowCommand == 0)
                    GetElecValue();
                else if (_nowCommand == 1)
                    GetPower();
                //UpdatePower();
            }
            else
            {
                _readTimeout -= _readInterval;

                if (0 > _readTimeout)
                {
                    _waitForResponse = false;
                    _readTimeout = 4000;
                }
            }
        }

        private void GetElecValue()
        {
            _waitForResponse = true;
            _nowCommand = 0;
            try
            {
                string frame = READ_VOLTAGE_TO_CURRENT;
                char BBC = GetBlockCheckCharacter(frame);
                string sendMsg = STX + frame + ETX + BBC;

                _SP.Write(sendMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void GetPower()
        {
            _waitForResponse = true;
            _nowCommand = 1;
            try
            {
                string frame = READ_POWER_VALUE;
                char BBC = GetBlockCheckCharacter(frame);
                string sendMsg = STX + frame + ETX + BBC;
                _SP.Write(sendMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DIT.TLC.CTRL
{
    public class CenteringCylinder : Cylinder
    {
        public override bool IsInterlock(Equipment equip, bool isBackward)
        {
            if (base.IsInterlock(equip, isBackward) == true) return true;
            return false;
        }
        public override bool Forward(Equipment equip)
        {
            if (IsInterlock(equip, false)) return false;

            if (equip.IsUseInterLockOff)
            {
                base.Forward(equip);
                return true;
            }
            
//             if (equip.ScanX.IsMoveOnPosition(ScanXServo.ScanXLoadingPosiNo) == false)
//             {
//                 if (equip.IsHomePositioning || equip.EquipRunMode == EmEquipRunMode.Auto || equip.IsCenteringStepping == true)
//                 {
//                     AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0751_SCAN_X_IS_NOT_LOADING_POSITION_STATE_ERROR);
//                     equip.IsInterLock = true;
//                 }
//                 else
//                     InterLockMgr.AddInterLock("Interlock <SCAN X> \n (Centering operation is possible only ScanX Axis on Loading Position State)");
// 
//                 return true;
//             }
// 
//             if (equip.LiftPin.IsMiddle == false)
//             {
//                 if (equip.IsHomePositioning || equip.EquipRunMode == EmEquipRunMode.Auto || equip.IsCenteringStepping == true)
//                 {
//                     AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0181_LIFT_PIN_IS_NOT_MIDDLE_POSITION_STATE_ERROR);
//                     equip.IsInterLock = true;
//                 }
//                 else
//                     InterLockMgr.AddInterLock("Interlock <LIFT PIN> \n (Centering operation is possible only LIFT PIN Middle POSITION)");
// 
//                 Logger.Log.AppendLine(LogLevel.Warning, string.Format("Centering operation is possible only LIFT PIN Middle POSITION"));
//                 return false;
//             }
            //글라스 디텍 센서 감지 인터락 추가
            //if (equip.IsNoGlassMode == false && (equip.GlassDetectPinUpSensor1.XB_OnOff == false || equip.GlassDetectPinUpSensor2.XB_OnOff == false))
            //{
            //    if (equip.IsHomePositioning || equip.EquipRunMode == EmEquipRunMode.Auto || equip.IsCenteringStepping == true)
            //    {
            //        AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0304_GLASS_DETECT_ERROR);
            //        equip.IsInterLock = true;
            //    }
            //    else
            //        InterLockMgr.AddInterLock("Interlock <LIFT PIN> \n (Centering operation is possible only GLASS DETECT(PIN UP GLASS/FLOAT GLASS1) ON STATE)");
            //
            //    Logger.Log.AppendLine(LogLevel.Warning, string.Format("Centering operation is possible only GLASS DETECT ON STATE"));
            //    return true;
            //}

            base.Forward(equip);
            return true;
        }
        public override bool Backward(Equipment equip)
        {
            if (IsInterlock(equip, true)) return false;

            if (equip.IsUseInterLockOff)
            {
                base.Backward(equip);
                return true;
            }

            base.Backward(equip);
            return true;
        }
    }
}

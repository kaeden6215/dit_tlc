﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Timers;
using DIT.TLC.CTRL;

namespace DIT.TLC.CTRL
{
    public enum FontColor
    {
        RED = 0x52,
        BLACK = 0x42,
        GREEN = 0x47,
        YELLOW = 0x59,
    }
    public class ElectronicDisplay
    {
        public const byte STX = 0x02;
        public const byte ETX = 0x03;
        public const byte COM = 0x30;

        public const byte NORMALMESSAGE = 0xA1;

        private SerialPort DisplaySP;
        private Timer DisplayUpdateTimer;
        private byte _controllerID;
        private int _readTimeout;
        private int _readInterval;
        private bool _waitForResponse;
        private List<byte> _lstRecvBytes = new List<byte>();

        public ElectronicDisplay(string portName, int readTimeout = 5000)
        {
            _controllerID = 1;
            _readInterval = 500;
            _waitForResponse = false;
            DisplaySP = new SerialPort();
            DisplayUpdateTimer = new System.Timers.Timer();

            DisplaySP.PortName = portName;
            DisplaySP.ReadTimeout = _readTimeout = readTimeout;
            DisplaySP.BaudRate = (int)19200;
            //아래 Default값.
            //_ffuSP.DataBits = (int)8;
            //_ffuSP.Parity = Parity.None;
            //_ffuSP.StopBits = StopBits.One;
            //_ffuSP.WriteTimeout = (int)100;
            //DisplaySP.DataReceived += new SerialDataReceivedEventHandler(O2DataReceived);
            Init();
        }
        private bool Init()
        {
            //_O2UpdateTimer.Interval = _readInterval;
            //_O2UpdateTimer.Elapsed += new ElapsedEventHandler(UpdateO2Status);
            return true;
        }
        public bool Open()
        {
            try
            {
                DisplaySP.Open();
                Logger.Log.AppendLine(LogLevel.Error, "Display : Serial Port Open Success");
            }
            //catch (System.IO.IOException SerialPortNullEx)
            catch (System.Exception ex)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Display : Serial Port Open Error");

                return false;
            }

            if (!DisplaySP.IsOpen)
            {
                return false;
            }
            return true;
        }
        private bool SerialPortClose()
        {
            if (!DisplaySP.IsOpen)
                return false;

            DisplaySP.Close();
            return true;
        }

        private void MessageClear()
        {
            byte[] CmdMsgClear = new byte[9];

            CmdMsgClear[0] = STX;
            CmdMsgClear[1] = 0x00;
            CmdMsgClear[2] = 0x09;
            CmdMsgClear[3] = 0x14;
            CmdMsgClear[4] = 0x80;
            CmdMsgClear[5] = 0x00;
            CmdMsgClear[6] = 0x00;
            CmdMsgClear[7] = 0x9F;
            CmdMsgClear[8] = ETX;

            DisplaySP.Write(CmdMsgClear, 0, CmdMsgClear.Length);
        }

        private void NormalMessageSendCMD(string msg, bool IsRed)
        {
            if (msg == string.Empty)
                return;

            byte[] CmdSendMsg = new byte[39];

            CmdSendMsg[0] = 0x02;
            CmdSendMsg[1] = 0x00;
            CmdSendMsg[2] = 0x27;
            CmdSendMsg[3] = 0xA1;
            CmdSendMsg[4] = 0x80;

            for (int iPos = 0; iPos < 16; iPos++)
            {
                CmdSendMsg[iPos + 5] = msg.Length > iPos ? (byte)char.Parse(msg.Substring(iPos, 1)) : (byte)0x20;
                if (IsRed)
                {
                    CmdSendMsg[iPos + 5 + 16] = msg.Length > iPos ? (byte)FontColor.RED : (byte)FontColor.BLACK;

                }
                else
                {
                    CmdSendMsg[iPos + 5 + 16] = msg.Length > iPos ? (byte)FontColor.GREEN : (byte)FontColor.BLACK;
                }
            }

            byte checkSum = 0x00;
            for (int iPos = 0; iPos < CmdSendMsg.Length - 2; iPos++)
            {
                if (iPos == 0)
                {
                    checkSum = CmdSendMsg[0];
                }
                else if (iPos > 0)
                {
                    checkSum ^= CmdSendMsg[iPos];
                }
            }

            CmdSendMsg[CmdSendMsg.Length - 2] = checkSum;
            CmdSendMsg[CmdSendMsg.Length - 1] = 0x03;

            DisplaySP.Write(CmdSendMsg, 0, CmdSendMsg.Length);
        }

        private string iTohex(int num)
        {
            string str = num.ToString("X");
            return str;
        }

        private void OneLineNormalMessageSendCMD(string msg, Equipment equip)
        {
            if (msg == string.Empty)
                return;

            byte[] CmdSendMsg = new byte[15 + msg.Length * 2];

            CmdSendMsg[0] = 0x02; //stx
            CmdSendMsg[1] = 0x00; //size high
            CmdSendMsg[2] = (byte)Convert.ToInt32(iTohex(15 + msg.Length * 2), 16); //size low
            CmdSendMsg[3] = COM; //command
            CmdSendMsg[4] = 0x80; //packet property

            //여기서부터 실제 데이터 패킷
            CmdSendMsg[5] = 0x01; //시작 방번호
            CmdSendMsg[6] = 0x00; //시작효과 종류
            CmdSendMsg[7] = 0x08; //시작효과 속도
            CmdSendMsg[8] = 0x02;  //유지시간
            CmdSendMsg[9] = 0x00;  //종료종류
            CmdSendMsg[10] = 0x08; //종료속도
            CmdSendMsg[11] = 0x01; //한줄 일반 메세지
            CmdSendMsg[12] = (byte)Convert.ToInt32(string.Format("{0:X}", iTohex(msg.Length)), 16); //표출 문자열 길이
            //문자열 중 각 글자에 대한 string
            for (int iPos = 0; iPos < msg.Length; iPos++)
            {
                CmdSendMsg[13 + iPos] = (byte)char.Parse(msg.Substring(iPos, 1));
                if(equip.ADC.O2Percent < equip.Purifier.OXYGEN_NORMAL_LIMIT)
                {
                    CmdSendMsg[13 + iPos + msg.Length] = 0x01;
                }
                else
                    CmdSendMsg[13 + iPos + msg.Length] = 0x02;
            }
            //여기까지 실제 데이터 패킷

            //LRC및 ETX 계산
            byte checkSum = 0x00;
            for (int iPos = 0; iPos < CmdSendMsg.Length - 2; iPos++)
            {
                if (iPos == 0)
                {
                    checkSum = CmdSendMsg[0];
                }
                else if (iPos > 0)
                {
                    checkSum ^= CmdSendMsg[iPos];
                }
            }

            CmdSendMsg[CmdSendMsg.Length - 2] = checkSum;
            //CmdSendMsg[CmdSendMsg.Length - 2] = ETX;            //TODO
            CmdSendMsg[CmdSendMsg.Length - 1] = ETX;

            DisplaySP.Write(CmdSendMsg, 0, CmdSendMsg.Length);
        }

        //TEST CODE
        public static string ShowAvailablePort()
        {
            string ports = "";
            foreach (string portName in SerialPort.GetPortNames())
                ports += portName + "\r\n";

            return ports;
        }
        public void Start()
        {
            Open();

            if (!DisplaySP.IsOpen)
                return;

            try
            {
                //_O2UpdateTimer.Start();

            }
            catch (NullReferenceException efuUpdateTimerNuller_E)
            {
                Logger.Log.AppendLine(LogLevel.Exception, "EFU : Update Timer Null Ex");
            }
        }
        public void Stop()
        {
            //if (_O2UpdateTimer == null) return;

            //_O2UpdateTimer.Stop();
            SerialPortClose();
        }
        public string CurrentPort
        {
            get
            {
                return DisplaySP.PortName;
            }
            set
            {
                DisplaySP.PortName = value;
            }
        }
        //public bool IsOpen()
        //{
        //return _O2ppmSP.IsOpen;
        //}
        System.Diagnostics.Stopwatch test = new System.Diagnostics.Stopwatch();
        bool initStart = false;
        bool b = false;
        private DateTime _dateTime = DateTime.Now;
        public void LogicWorking(Equipment equip)
        {
            //             if (initStart == false)
            //             {
            //                 test.Start();
            // 
            //                 initStart = true;
            //             }
            if (!DisplaySP.IsOpen)
                return;

            if ((DateTime.Now - _dateTime).Seconds > 1)
            {
                //if (equip.o2Sensor.O2PPM > 19.5)
                //{
                //    NormalMessageSendCMD(string.Format("O2 {0:0.#}%", equip.o2Sensor.O2PPM), false);
                //}
                //else if (equip.o2Sensor.O2PPM <= 19.5)
                //{
                //    NormalMessageSendCMD(string.Format("O2 {0:0.#}%", equip.o2Sensor.O2PPM), true);
                //}
                
                //Test Code : msg
                //NormalMessageSendCMD(string.Format("02 : {0:0.#}%", equip.ADC.Oxygen), true);

                Func<float, string> convert = delegate(float value)
                {
                    return value != -9999 ? string.Format("{0:N1}", value) : "Error";
                };
                Func<float, string> convert_PPM = delegate(float value)
                {
                    return value != -9999 ? string.Format("{0:N3}", value) : "Error";
                };

                //NormalMessageSendCMD(string.Format("02 : {0:0.#}%", convert(equip.ADC.Oxygen)), true);
                OneLineNormalMessageSendCMD(string.Format("02 : {0:0.#}%", convert(equip.ADC.O2Percent)), equip);
                _dateTime = DateTime.Now;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Comm;
using System.Diagnostics;
using Dit.Framework.PLC;
using System.IO;
using System.Windows.Forms;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public enum MoveCommand
    {
        NO_1_POSI = 0,
        NO_2_POSI,
        NO_3_POSI,
        NO_4_POSI,
        NO_5_POSI,
        NO_6_POSI,
        NO_7_POSI,
        NO_8_POSI,
        NO_9_POSI,
        NO_10_POSI,
        NO_11_POSI,
        NO_12_POSI,
        NO_13_POSI,
        NO_14_POSI,
        NO_15_POSI,
        NO_16_POSI,
        NO_17_POSI,
        NO_18_POSI,
        NO_19_POSI,
        NO_20_POSI,
        NO_21_POSI,
        NO_22_POSI,
        NO_23_POSI,
        NO_24_POSI,
        NO_25_POSI,
        NO_26_POSI,
        NO_27_POSI,
        NO_28_POSI,
        NO_29_POSI,
        NO_30_POSI,
        NO_31_POSI,
        NO_32_POSI,
        HOME_POSI,
        START_POSI,
        JOG,
    }
    public enum EM_SERVO_JOG
    {
        JOG_PLUS = 1,
        JOG_STOP = 0,
        JOG_MINUS = -1,
    }

    public enum emServoMoveType
    {
        PTP,
        JOG,
        HOME
    }

    public enum EM_MOTION_COMPLETE_LOG_MSG
    {
        CMD_ACK_DEFAULT,
        CMD_ACK_HOME_SWQ_OK,
        CMD_ACK_PTP_SEQ_OK,
        CMD_ACK_MOTION_SEQ_FAIL,
        CMD_ACK_MOTION_SEQ_ESTOP,
        CMD_ACK_HOME_SEQ_FAIL,
        CMD_ACK_SEQ_OEVERLAP_FAIL,
        CMD_ACK_AUTO_REVIEW_COMPLETE,
        CMD_ACK_AUTO_REVIEW_STOP_TIME_END
    }

    public enum EM_MOTOR_TYPE
    {
        Ajin,
        Umac,
        EZi,
        ALL
    }

    public enum EM_MOTOR_POSI
    {
        LD,
        PROC,
        UD,
        ALL
    }
    public enum EmAxisPosi
    {
        Aixs1,
        Aixs2,
    }
    [Serializable]
    public class ServoMotorControl : UnitBase
    {
        public Func<Equipment, float, float, emServoMoveType, bool> CheckStartMoveInterLockFunc;

        public EM_MOTOR_TYPE MotorType { get; set; }
        public EM_MOTOR_POSI MotorPosi { get; set; }
        public EmAxisPosi AxisPosi = EmAxisPosi.Aixs1;

        public static int HomePositionPosiNo = 0;
        public int SoftMinusLimit = 10;
        public int SoftPlusLimit = 10;
        public int SoftSpeedLimit = 10;
        public int SoftJogSpeedLimit = 10;
        public int EnableGripJogSpeedLimit = 30;
        public float InposOffset = 0.01f;
        public int SoftAccelPlusLimit = 1000;
        public int SoftAccelMinusLimit = 100;
        public float PinAvoidPos = 500.0f;

        /// <summary>
        /// 컨트롤러에서 가져올 때 나눠지고, 컨트롤러로 전달할 때 곱
        /// </summary>
        public float PcToCtrllerPositionScale = 1;

        public string Name { get; set; }
        public int InnerAxisNo { get; set; }
        public int OutterAxisNo { get; set; }


        public PlcAddr XI_CurrMotorStatus1 { get; set; }
        public PlcAddr XI_StatusBitSet2 { get; set; }
        public PlcAddr XI_StatusBitSet3 { get; set; }
        public PlcAddr XB_StatusHomeCompleteBit { get; set; }
        public PlcAddr XB_StatusHomeInPosition { get; set; }
        public PlcAddr XB_StatusMotorMoving { get; set; }
        public PlcAddr XB_StatusMotorInPosition { get; set; }
        public PlcAddr XB_StatusNegativeLimitSet { get; set; }
        public PlcAddr XB_StatusPositiveLimitSet { get; set; }
        public PlcAddr XB_StatusMotorServoOn { get; set; }
        public PlcAddr XB_ErrFatalFollowingError { get; set; }
        public PlcAddr XB_ErrAmpFaultError { get; set; }
        public PlcAddr XB_ErrI2TAmpFaultError { get; set; }

        public PlcAddr XF_CurrMotorPositionPlcAddr { get; set; }
        public float XF_CurrMotorPosition
        {
            get { return GG.TestMode ? XF_CurrMotorPositionPlcAddr.vFloat : (XF_CurrMotorPositionPlcAddr.vFloat / PcToCtrllerPositionScale); }
            set { XF_CurrMotorPositionPlcAddr.vFloat = GG.TestMode ? value : (value * PcToCtrllerPositionScale); }
        }
        public PlcAddr XF_CurrMotorSpeed { get; set; }
        public PlcAddr XF_CurrMotorActualLoad { get; set; }

        public PlcAddr XI_CurrMotorLow { get; set; }
        public PlcAddr XI_CurrMotorHight { get; set; }
        public PlcAddr XF_CurrMotorError { get; set; }




        //서보 OnOff Cmd
        public PlcAddr YB_ServoOnOffCmd { get; set; }
        public PlcAddr YB_ServoOnOff { get; set; }
        public PlcAddr XB_ServoOnOffCmdAck { get; set; }

        //서보 Motor Stop
        public PlcAddr YB_MotorStopCmd { get; set; }
        public PlcAddr XB_MotorStopCmdAck { get; set; }

        //Home Cmd
        public PlcAddr YB_HomeCmd { get; set; }
        public PlcAddr XB_HomeCmdAck { get; set; }

        //Jog Command
        public PlcAddr YB_MotorJogMinusMove { get; set; }
        public PlcAddr YB_MotorJogPlusMove { get; set; }
        public PlcAddr YF_MotorJogSpeedCmdPlcAddr { get; set; }
        public float YF_MotorJogSpeedCmd
        {
            get { return GG.TestMode ? YF_MotorJogSpeedCmdPlcAddr.vFloat : (float)Math.Ceiling(YF_MotorJogSpeedCmdPlcAddr.vFloat / PcToCtrllerPositionScale); }
            set { YF_MotorJogSpeedCmdPlcAddr.vFloat = GG.TestMode ? value : (value * PcToCtrllerPositionScale); }
        }
        public PlcAddr XF_MotorJogSpeedCmdAckPlcAddr { get; set; }
        public float XF_MotorJogSpeedCmdAck
        {
            get { return GG.TestMode ? XF_MotorJogSpeedCmdAckPlcAddr.vFloat : (float)Math.Ceiling(XF_MotorJogSpeedCmdAckPlcAddr.vFloat / PcToCtrllerPositionScale); }
            set { XF_MotorJogSpeedCmdAckPlcAddr.vFloat = GG.TestMode ? value : (value * PcToCtrllerPositionScale); }
        }

        //Point To Point Move Cmd        
        public PlcAddr YB_PTPMoveCmd { get; set; }
        public PlcAddr YB_PTPMoveCmdCancel { get; set; }
        public PlcAddr XB_PTPMoveCmdAck { get; set; }
        public PlcAddr YF_PTPMovePositionPlcAddr { get; set; }
        public float YF_PTPMovePosition
        {
            get { return GG.TestMode ? YF_PTPMovePositionPlcAddr.vFloat : (YF_PTPMovePositionPlcAddr.vFloat / PcToCtrllerPositionScale); }
            set { YF_PTPMovePositionPlcAddr.vFloat = GG.TestMode ? value : (value * PcToCtrllerPositionScale); }
        }
        public PlcAddr XF_PTPMovePositionAckPlcAddr { get; set; }
        public float XF_PTPMovePositionAck
        {
            get { return GG.TestMode ? XF_PTPMovePositionAckPlcAddr.vFloat : (XF_PTPMovePositionAckPlcAddr.vFloat / PcToCtrllerPositionScale); }
            set { XF_PTPMovePositionAckPlcAddr.vFloat = GG.TestMode ? value : (value * PcToCtrllerPositionScale); }
        }
        public PlcAddr YF_PTPMoveSpeed { get; set; }
        public PlcAddr XF_PTPMoveSpeedAck { get; set; }
        public PlcAddr YF_PTPMoveAccel { get; set; }
        public PlcAddr XF_PTPMoveAccelAck { get; set; }
        public PlcAddr XB_PTPMoveComplete { get; set; }

        public TimeSpan PositionMoveTime { get; set; }
        public DateTime _moveStartTime;


        public int MOVE_HOME_OVERTIME;

        public int HommingCmdStep = 0;
        public int PtpMoveCmdStep = 0;
        public int ServoOnOffCmdSetp = 0;
        public int MoveStopCmdStep = 0;
        public int JogCmdStep = 0;

        protected int DefaultServoTimeOut = 15000;
        protected int DefaultServoHomeTimeOver = 30000;

        public ServoParamSetting ParamSetting { get; set; }
        public ServoSetting Setting { get; set; }

        //알람 리스트
        public static EM_AL_LST SETTING_TIMEOUT { get; set; }
        public EM_AL_LST PLUS_LIMIT_ERROR { get; set; }
        public EM_AL_LST MINUS_LIMIT_ERROR { get; set; }
        public EM_AL_LST MOTOR_SERVO_ON_ERROR { get; set; }
        public EM_AL_LST CRITICAL_POSITION_ERROR { get; set; }
        public EM_AL_LST DRIVE_FAULT_ERROR { get; set; }
        public EM_AL_LST OVER_CURRENT_ERROR { get; set; }
        public EM_AL_LST ECALL_ERROR { get; set; }
        public EM_AL_LST MOVE_OVERTIME_ERROR { get; set; }
        public EM_AL_LST BUFFER_RESET_ERROR { get; set; }
        public EM_AL_LST MOTOR_IS_NOT_MOVING_ERROR { get; set; }
        public EM_AL_LST MESSAGE_ACK_ERROR { get; set; }
        public EM_AL_LST MESSAGE_MOTION_COMPLETE_ERROR { get; set; }

        public ServoMotorControl()
        {
        }

        public ServoMotorControl(int innerAxisNo, int outterAxisNo, string name)
        {
            InitPositionSetting();
            this.InnerAxisNo = innerAxisNo;
            this.OutterAxisNo = outterAxisNo;

            this.Name = name;

            MOVE_HOME_OVERTIME = DefaultServoHomeTimeOver;

            _moveStartTime = DateTime.Now;

            PositionMoveTime = new TimeSpan();
            SETTING_TIMEOUT = EM_AL_LST.AL_NONE;
            PLUS_LIMIT_ERROR = EM_AL_LST.AL_NONE;
            MINUS_LIMIT_ERROR = EM_AL_LST.AL_NONE;
            MOTOR_SERVO_ON_ERROR = EM_AL_LST.AL_NONE;
            CRITICAL_POSITION_ERROR = EM_AL_LST.AL_NONE;
            DRIVE_FAULT_ERROR = EM_AL_LST.AL_NONE;
            OVER_CURRENT_ERROR = EM_AL_LST.AL_NONE;
            ECALL_ERROR = EM_AL_LST.AL_NONE;
            MOVE_OVERTIME_ERROR = EM_AL_LST.AL_NONE;


            Setting = new ServoSetting();
            ParamSetting = new ServoParamSetting();
        }

        public void InitPositionSetting(
            int softMinusLimit = 10,
            int softPlusLimit = 10,
            int softSpeedLimit = 10,
            int softJogSpeedLimit = 10,
            int enableGripJogSpeedLimit = 30,
            float inposOffset = 0.1f,
            int softAccelPlusLimit = 1000,
            int softAccelMinusLimit = 100)
        {
            SoftMinusLimit = softMinusLimit;
            SoftPlusLimit = softPlusLimit;
            SoftSpeedLimit = softSpeedLimit;
            SoftJogSpeedLimit = softJogSpeedLimit;
            EnableGripJogSpeedLimit = enableGripJogSpeedLimit;
            InposOffset = inposOffset;
            SoftAccelPlusLimit = softAccelPlusLimit;
            SoftAccelMinusLimit = softAccelMinusLimit;
        }
        public virtual bool IsHomeCompleteBit
        {
            get { return XB_StatusHomeCompleteBit.vBit; }
        }
        public virtual bool IsStatusHomeInPosition
        {
            get { return XB_StatusHomeInPosition.vBit; }
        }
        public virtual bool IsStatusMotorInPosition
        {
            get { return XB_StatusMotorInPosition.vBit; }
        }
        public virtual bool IsNegativeLimit
        {
            get { return XB_StatusNegativeLimitSet.vBit; }
        }
        public virtual bool IsPositiveLimit
        {
            get { return XB_StatusPositiveLimitSet.vBit; }
        }
        public virtual bool IsServoOnBit
        {
            get { return XB_StatusMotorServoOn.vBit; }
        }
        public virtual bool IsServoError
        {
            get
            {
                if (GG.TestMode == true)
                    return IsHomeCompleteBit == false;
                else
                    return IsHomeCompleteBit == false || IsNegativeLimit || IsPositiveLimit;//|| IsServoOnBit == false || IsCriticalPositionError || IsDriveFaultError || IsOverCurrentError || IsECallError;
            }
        }
        public virtual bool IsFatalServoError
        {
            get
            {
                return false;
                //return IsCriticalPositionError || IsDriveFaultError || IsOverCurrentError || IsECallError;
            }
        }


        public bool CheckInterlock(Equipment equip)
        {
            if (equip.IsPause)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <PAUSE> \n (PAUSE state, axis = {0} Do not move.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "PAUSE state, axis = {0} Do not move.", Name);
                return false;
            }

            if (equip.IsDoorRelease && equip.IsUseInterLockOff == false && equip.IsUseDoorInterLockOff == false)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <DOOR> \n (DOOR OPEN state, axis = {0} movement prohibited.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "DOOR OPEN state, axis = {0} movement prohibited.", Name);
                return false;
            }

            if (IsServoError == true)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <SERVO ERROR> \n (Axis = {0} MOTOR ERROR, motor home position required.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Axis = {0} MOTOR ERROR, motor home position required.", Name);
                return false;
            }

            if (equip.IsImmediatStop)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <EMERGENCY> \n (EMERGENCY state, Axis = {0} can not be moved.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "EMERGENCY state, Axis = {0} can not be moved.", Name);
                return false;
            }

            if (IsHomeCompleteBit == false)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <HOME BIT ERROR> \n (Axis = {0} Motor HomeCompleteBit OFF, motor home position required.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Axis = {0} Motor HomeCompleteBit OFF, motor home position required.", Name);
                return false;
            }

            if (IsNegativeLimit || IsPositiveLimit)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <LIMIT SENSOR> \n (Axis = {0} motor limit sensor detection. \nJog and home movement only.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Axis = {0} motor limit sensor detection. \nJog and home movement only.", Name);
                return false;
            }

            return true;
        }

        public bool IsHomming { get { return HommingCmdStep != 0; } }
        // 메소드 - 커멘드 함수들
        protected virtual bool PtPMoveLogic(Equipment equip)
        {
            if (CheckInterlock(equip) == false) return false;

            //if (CheckMotorCtrlPositionSetting(equip, (MoveCommand)posiNo, MoveActionName[posiNo]) == false) return false;
            //if (IsStartedStep(equip, (MoveCommand)posiNo, MoveActionName[posiNo]) == true) return false;
            //if (CheckStartMoveInterLock(equip, )
            //{
            //    //IsInterLockError = true;
            //    return false;
            //}
            //else if (IsMoveOnPosition(posiNo) == true)
            //{
            //    return true;
            //}
            //else
            //{
            //    if (SetEquipState(equip) == false) return false;
            //    int minMoveTime = 5000; // ms
            //    _ptpMoveCmdStep = 10;
            //    Console.WriteLine("Posi {1} MoveTime{0}", _ptpMoveCmdStep, posiNo);
            //    return true;
            //}

            return false;
        }

        public virtual bool IsHomeOnPosition()
        {
            return IsStatusHomeInPosition &&
                    IsServoError == false &&
                    IsMoving == false &&
                     HommingCmdStep == 0;
        }
        protected bool JogMoveInterlock(Equipment equip, EM_SERVO_JOG jog, float speed)
        {
            if (equip.IsPause)
            {
                InterLockMgr.AddInterLock(
                    string.Format("Interlock <PAUSE> \n (PAUSE state, axis = {0}, direction = {1}, speed = {2}), prohibit operation)", Name, jog, speed));
                Logger.Log.AppendLine(LogLevel.Warning, "PAUSE state, axis = {0}, direction = {1}, speed = {2}), prohibit operation", Name, jog, speed);
                return false;
            }

            if (equip.IsDoorRelease == true && equip.IsUseInterLockOff == false && equip.IsUseDoorInterLockOff == false)
            {
                InterLockMgr.AddInterLock(
                    string.Format("Interlock <DOOR> \n (DOOR OPEN state, axis = {0}, direction = {1}, speed = {2}, prohibit operation)", Name, jog, speed));
                Logger.Log.AppendLine(
                    LogLevel.Warning, "DOOR OPEN state, axis = {0}, direction = {1}, speed = {2}, prohibit operation", Name, jog, speed);
                return false;
            }

            if (equip.IsImmediatStop)
            {
                InterLockMgr.AddInterLock(
                    string.Format("Interlock <EMERGENCY> \n (EMERGENCY state, axis = {0}, direction = {1}, speed = {2}, prohibit operation)", Name, jog, speed));
                Logger.Log.AppendLine(
                    LogLevel.Warning, "EMERGENCY state, axis = {0}, direction = {1}, speed = {2}, prohibit operation", Name, jog, speed);
                return false;
            }

            //if (IsStepEnd == false)
            //{
            //    InterLockMgr.AddInterLock(
            //        string.Format("Interlock <RUNNING> \n (axis = {0} moving, prohibit operation)", Name));
            //    Logger.Log.AppendLine(LogLevel.Warning, "axis = {0} moving, prohibit operation", Name);
            //    return false;
            //}

            if (CheckStartMoveInterLockFunc(equip, 0, speed, emServoMoveType.JOG))
            {
                //IsInterLockError = true;
                return false;
            }

            HommingCmdStep = 10;

            return true;
        }

        private EM_SERVO_JOG SERVO_JOG = EM_SERVO_JOG.JOG_STOP;
        private float JogSpeed = 0;
        public bool JogMove(Equipment equip, EM_SERVO_JOG jog, float speed)
        {
            if (equip.IsDoorRelease == true && equip.IsGrapSwithOn == false && jog != EM_SERVO_JOG.JOG_STOP && GG.NotuseDoorInterlock == false)
            {
                this.YB_MotorJogPlusMove.vBit = false;
                this.YB_MotorJogMinusMove.vBit = false;

                InterLockMgr.AddInterLock(string.Format("도어 오픈 상태에서 GRAP 스위치가 ON 상태에서만  모터 조그이 가능합니다. {0}", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "도어 오픈 상태에서 GRAP 스위치가 ON 상태에서만  모터 조그이 가능합니다. {0}", Name);
            }

            if (jog == EM_SERVO_JOG.JOG_STOP)
            {
                this.YF_MotorJogSpeedCmd = speed;
                this.YB_MotorJogMinusMove.vBit = false;
                this.YB_MotorJogPlusMove.vBit = false;

                JogCmdStep = 0;
                return true;
            }

            //if (JogMoveInterlock(equip, jog, (int)speed) == false)
            //    return false;

            Logger.Log.AppendLine(LogLevel.Info, "{0} motor {1} velocity {2} move", Name, jog, speed);
            if (jog == EM_SERVO_JOG.JOG_MINUS || jog == EM_SERVO_JOG.JOG_PLUS)
            {
                JogCmdStep = 10;
                SERVO_JOG = jog;
                JogSpeed = speed;
            }
            return true;
        }

        public virtual bool IsMoving
        {
            get { return XB_StatusMotorMoving.vBit; }
        }
        public virtual bool IsMoveOnPosition()
        {
            return Math.Abs(XF_CurrMotorPosition - XF_PTPMovePositionAck) <= InposOffset && XB_StatusMotorInPosition.vBit == true && IsMoving == false && PtpMoveCmdStep == 0 && IsHomeCompleteBit;
        }
        public virtual bool IsMovingOnPosition()
        {
            return PtpMoveCmdStep != 0 || HommingCmdStep != 0;
        }
        public virtual bool IsMovingOnHome()
        {
            return HommingCmdStep != 0;
        }

        public override void LogicWorking(Equipment equip)
        {
            CheckServoStatus(equip);

            HommingWorking(equip);
            PtpMoveWorking(equip);
            ServerOnOffWorking(equip);
            MoveStopfWorking(equip);
            JogMoveWorking(equip);
        }
        private bool _isHappenAlarm = false;
        private bool _isLimitAlarm = false;


        private DateTime _ptpStartTime = DateTime.Now;

        public float _position;
        public float _speed;
        public float _accel;
        private void PtpMoveWorking(Equipment equip)
        {

            //if (_ptpMoveCmdStep != 0 && (PcDateTime.Now - _ptpStartTime).TotalMilliseconds > 5000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "PtpMove 가 BUFFER RESET ERROR");
            //    AlarmMgr.Instance.Happen(equip, BUFFER_RESET_ERROR);
            //    _ptpStartTime = PcDateTime.Now;
            //    _ptpMoveCmdStep = 0;
            //    return;
            //}

            //if (_ptpMoveCmdStep == 20 && (PcDateTime.Now - _ptpStartTime).TotalMilliseconds > 5000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "{0}가 ACK SIGNAL OVERTIME, ", Name);
            //    AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0727_MOTOR_CMD_ACK_SIGNAL_TIMEOVER);
            //    _ptpStartTime = PcDateTime.Now;
            //    _ptpMoveCmdStep = 0;
            //    return;
            //}

            //if (stepMove == 40 && IsMoving == false && xb_Position0Complete.vBit == false && (PcDateTime.Now - startTime).TotalMilliseconds > 5000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "{0}가 IS NOT MOVING ERROR, ", Name);
            //    AlarmMgr.Instance.Happen(equip, MOTOR_IS_NOT_MOVING_ERROR);
            //    equip.IsNeedSeq = EMNeedSeq.Home;
            //    _startTime = PcDateTime.Now;
            //    stepMove = 0;
            //    return;
            //}

            //if (_ptpMoveCmdStep > 20 && (PcDateTime.Now - _ptpStartTime).TotalMilliseconds > 10000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "PtpMove가 TIME OVER,  Move Target Position  : {0} / Current Position : {1}",
            //        _position, XF_CurrMotorPosition);
            //    AlarmMgr.Instance.Happen(equip, MOVE_OVERTIME_ERROR);
            //    _ptpStartTime = PcDateTime.Now;
            //    _ptpMoveCmdStep = 0;
            //    return;
            //}


            if (PtpMoveCmdStep == 0)
            {
            }
            else if (PtpMoveCmdStep == 10)
            {
                YF_PTPMovePosition = _position;
                YF_PTPMoveSpeed.vFloat = _speed;
                YF_PTPMoveAccel.vFloat = _accel;

                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis Move Start Current Position  {1}, Target Position = {2}, Speed ={3}, Accel = {4} CMD ACK OK", Name, XF_CurrMotorPosition, _position, _speed, _accel);


                PtpMoveCmdStep = 20;
            }
            else if (PtpMoveCmdStep == 20)
            {
                YB_PTPMoveCmd.vBit = true;
                PtpMoveCmdStep = 30;
            }
            else if (PtpMoveCmdStep == 30)
            {
                if (XB_PTPMoveCmdAck.vBit == true)
                {
                    if (Math.Abs(_position - YF_PTPMovePosition) < 0.001 && Math.Abs(_position - XF_PTPMovePositionAck) < 0.001 &&
                        Math.Abs(_speed - YF_PTPMoveSpeed.vFloat) < 0.001 && Math.Abs(_speed - XF_PTPMoveSpeedAck.vFloat) < 0.001 &&
                        Math.Abs(_accel - YF_PTPMoveAccel.vFloat) < 0.001 && Math.Abs(_accel - XF_PTPMoveAccelAck.vFloat) < 0.001)
                    {
                        YB_PTPMoveCmd.vBit = false;
                        PtpMoveCmdStep = 40;
                    }
                    else
                    {
                        //이동 명령을 취소할 수 있도록 구성 요함. 
                    }
                }
            }
            else if (PtpMoveCmdStep == 40)
            {
                if (XB_PTPMoveCmdAck.vBit == false)
                {
                    PtpMoveCmdStep = 50;
                }
            }
            else if (PtpMoveCmdStep == 50)
            {
                // if (XB_StatusMotorInPosition.vBit == true && Math.Abs(_position - YF_PTPMovePosition) < InposOffset)
                {
                    PtpMoveCmdStep = 60;
                }
            }
            else if (PtpMoveCmdStep == 60)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis Move End Current Position  {1}, Target Position = {2}, Speed ={3}, Accel = {4} CMD ACK OK", Name, XF_CurrMotorPosition, _position, _speed, _accel);
                PtpMoveCmdStep = 0;
            }
        }


        private void JogMoveWorking(Equipment equip)
        {
            if (JogCmdStep == 0)
            {

            }
            else if (JogCmdStep == 10)
            {
                if (this.YF_MotorJogSpeedCmd == this.XF_MotorJogSpeedCmdAck && this.XF_MotorJogSpeedCmdAck == JogSpeed)
                {
                    JogCmdStep = 50;
                }
                else
                {
                    if (SERVO_JOG == EM_SERVO_JOG.JOG_MINUS)
                    {
                        this.YF_MotorJogSpeedCmd = JogSpeed;
                    }
                    else if (SERVO_JOG == EM_SERVO_JOG.JOG_PLUS)
                    {
                        this.YF_MotorJogSpeedCmd = JogSpeed;
                    }

                    JogCmdStep = 20;
                }
            }
            else if (JogCmdStep == 20)
            {
                if (this.YF_MotorJogSpeedCmd == this.XF_MotorJogSpeedCmdAck && this.XF_MotorJogSpeedCmdAck == JogSpeed)
                {
                    JogCmdStep = 50;
                }
            }
            else if (JogCmdStep == 50)
            {
                if (SERVO_JOG == EM_SERVO_JOG.JOG_MINUS)
                {
                    this.YB_MotorJogPlusMove.vBit = false;
                    this.YB_MotorJogMinusMove.vBit = true;
                }
                else if (SERVO_JOG == EM_SERVO_JOG.JOG_PLUS)
                {
                    this.YB_MotorJogMinusMove.vBit = false;
                    this.YB_MotorJogPlusMove.vBit = true;
                }
                JogCmdStep = 60;
            }
            else if (JogCmdStep == 60)
            {

            }


            if (equip.IsDoorRelease == true && GG.NotuseDoorInterlock == false)
            {
                if (SERVO_JOG != EM_SERVO_JOG.JOG_STOP)
                {
                    if (equip.IsGrapSwithOn == false)
                    {
                        this.YB_MotorJogPlusMove.vBit = false;
                        this.YB_MotorJogMinusMove.vBit = false;
                    }
                }
            }

        }

        private DateTime _hommingStartTime = DateTime.Now;
        private void HommingWorking(Equipment equip)
        {
            //if (HommingCmdStep != 0 && (PcDateTime.Now - _hommingStartTime).TotalMilliseconds > 5000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "{0}가 HommingBUFFER RESET ERROR", Name);
            //    AlarmMgr.Instance.Happen(equip, BUFFER_RESET_ERROR);
            //    _hommingStartTime = PcDateTime.Now;
            //    HommingCmdStep = 0;
            //    return;
            //}

            if (HommingCmdStep > 10 && (PcDateTime.Now - _hommingStartTime).TotalMilliseconds > 5000)
            {
                Logger.Log.AppendLine(LogLevel.Error, "{0}가 Homming ACK SIGNAL OVERTIME, ", Name);
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_5002_MOTOR_CMD_ACK_SIGNAL_TIMEOVER);
                _hommingStartTime = PcDateTime.Now;
                HommingCmdStep = 0;
                return;
            }

            //if (stepMove == 40 && IsMoving == false && xb_Position0Complete.vBit == false && (PcDateTime.Now - startTime).TotalMilliseconds > 5000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "{0}가 IS NOT MOVING ERROR, ", Name);
            //    AlarmMgr.Instance.Happen(equip, MOTOR_IS_NOT_MOVING_ERROR);
            //    equip.IsNeedSeq = EMNeedSeq.Home;
            //    _startTime = PcDateTime.Now;
            //    stepMove = 0;
            //    return;
            //}

            //if (HommingCmdStep > 20 && (PcDateTime.Now - _hommingStartTime).TotalMilliseconds > 10000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "{0}가 TIME OVER,  Homming Move Target Position  : {1} / Current Position : {2}",
            //        Name, _position, XF_CurrMotorPosition);
            //    AlarmMgr.Instance.Happen(equip, MOVE_OVERTIME_ERROR);
            //    _hommingStartTime = PcDateTime.Now;
            //    HommingCmdStep = 0;
            //    return;
            //}

            if (HommingCmdStep == 0)
            {
            }
            else if (HommingCmdStep == 10)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis Homming Move Start Current Position  {1}, CMD ACK OK",
                    Name, XF_CurrMotorPosition);

                _hommingStartTime = PcDateTime.Now;
                HommingCmdStep = 20;
            }
            else if (HommingCmdStep == 20)
            {
                YB_HomeCmd.vBit = true;
                HommingCmdStep = 30;
            }
            else if (HommingCmdStep == 30)
            {
                if (XB_HomeCmdAck.vBit == true)
                {
                    YB_HomeCmd.vBit = false;
                    HommingCmdStep = 40;
                }
            }
            else if (HommingCmdStep == 40)
            {
                if (XB_PTPMoveCmdAck.vBit == false)
                {
                    HommingCmdStep = 50;
                }
            }
            else if (HommingCmdStep == 50)
            {
                if (XB_StatusHomeCompleteBit.vBit == true && XB_StatusHomeInPosition.vBit == true)
                {
                    HommingCmdStep = 60;
                }
            }
            else if (HommingCmdStep == 60)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis Homming Move Command End Current Position  {1} CMD ACK OK", Name, XF_CurrMotorPosition);
                HommingCmdStep = 0;
            }
        }

        private DateTime _serverOnOffStartTime = DateTime.Now;
        private void ServerOnOffWorking(Equipment equip)
        {
            if (ServoOnOffCmdSetp == 0)
            {
            }
            else if (ServoOnOffCmdSetp == 10)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis ServerOnOff[{2}] Command Start Current Position  {1}, CMD ACK OK", Name, XF_CurrMotorPosition, YB_ServoOnOff.Bit);
                ServoOnOffCmdSetp = 20;
            }
            else if (ServoOnOffCmdSetp == 20)
            {
                YB_ServoOnOffCmd.vBit = true;
                ServoOnOffCmdSetp = 30;
            }
            else if (ServoOnOffCmdSetp == 30)
            {
                if (XB_ServoOnOffCmdAck.vBit == true)
                {
                    YB_ServoOnOffCmd.vBit = false;
                    ServoOnOffCmdSetp = 40;
                }
            }
            else if (ServoOnOffCmdSetp == 40)
            {
                if (XB_ServoOnOffCmdAck.vBit == false)
                {
                    ServoOnOffCmdSetp = 50;
                }
            }
            else if (ServoOnOffCmdSetp == 50)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis ServerOnOff[{2}] Command End Current Position  {1} CMD ACK OK", Name, XF_CurrMotorPosition, YB_ServoOnOff.Bit);
                ServoOnOffCmdSetp = 0;
            }
        }

        private DateTime _stopStartTime = DateTime.Now;
        private void MoveStopfWorking(Equipment equip)
        {
            if (MoveStopCmdStep == 0)
            {
            }
            else if (MoveStopCmdStep == 10)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis Move Stop Command Start Current Position  {1}, CMD ACK OK", Name, XF_CurrMotorPosition);
                MoveStopCmdStep = 20;
            }
            else if (MoveStopCmdStep == 20)
            {
                YB_MotorStopCmd.vBit = true;
                MoveStopCmdStep = 30;
            }
            else if (MoveStopCmdStep == 30)
            {
                if (XB_MotorStopCmdAck.vBit == true)
                {
                    YB_MotorStopCmd.vBit = false;
                    MoveStopCmdStep = 40;
                }
            }
            else if (MoveStopCmdStep == 40)
            {
                if (XB_MotorStopCmdAck.vBit == false)
                {
                    MoveStopCmdStep = 50;
                }
            }
            else if (MoveStopCmdStep == 50)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis Move Stop Command End Current Position  {1} CMD ACK OK", Name, XF_CurrMotorPosition);
                PtpMoveCmdStep = 0;
                MoveStopCmdStep = 0;
            }
        }


        //PTP 이동 명령

        public bool PtpMoveCmd(Equipment equip, int posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, equip.CurrentRecipe, opt);

            if (IsInPosition(info)) return true;

            return PtpMoveCmd(equip, info.Position, info.Speed, info.Accel);
        }
        public bool PtpMoveCmd(Equipment equip, float position, float speed, float accel)
        {
            if (GG.NotUseInterlock == false)
            {
                if (CheckStartMoveInterLockFunc(equip, position, speed, emServoMoveType.PTP) == true)
                {
                    //인터락 발생으로 이동 불가. 
                    if (GG.Equip.EqpSysMgr.Params.IsInterlockUse == false)
                        return true;
                    else
                        return false;
                }
            }

            if (IsStartedStep(equip) == true)
            {
                //인터락 발생 요망. 
                return false;
            }

            if (equip.IsDoorRelease == true && GG.NotuseDoorInterlock == false)
            {
                InterLockMgr.AddInterLock(string.Format("도어 오픈 상태에서 모터 이동이 불가 합니다. {0}", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "도어 오픈 상태에서 모터 이동이 불가 합니다. {0}", Name);
                //인터락 발생 요망. 
                return false;
            }


            _position = position;
            _speed = speed;
            _accel = accel;
            PtpMoveCmdStep = 10;

            return true;
        }


        public bool PtpMoveCmdSync(Equipment equip, int posiNo, EqpRecipe recp, object opt)
        {
            var st = new StackTrace();

            ServoPosiInfo info = GetCalPosition(posiNo, equip.CurrentRecipe, opt);
            if (PtpMoveCmdStep == 0)
            {
                if (info.Position == XF_PTPMovePositionAck && info.Accel == XF_PTPMoveAccelAck.vFloat && info.Speed == XF_PTPMoveSpeedAck.vFloat)
                {
                    return IsInPosition(info);
                }
                else
                {
                    PtpMoveCmd(equip, info.Position, info.Speed, info.Accel);
                    return false;
                }
            }
            else
            {
                if (info.Position == _position && info.Accel == _accel && info.Speed == _speed)
                    return IsInPosition(info);
                else
                    return false;
            }
        }

        //임시.         
        public bool GoHome(Equipment equip)
        {
            if (IsStartedStep(equip) == true) return false;

            if (GG.NotUseInterlock == false)
            {
                if (CheckStartMoveInterLockFunc(equip, 0, 0, emServoMoveType.HOME))
                {
                    //IsInterLockError = true;
                    return false;
                }
            }

            if (equip.IsDoorRelease == true && GG.NotuseDoorInterlock == false)
            {
                InterLockMgr.AddInterLock(string.Format("도어 오픈 상태에서 모터 이동이 불가 합니다. {0}", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "도어 오픈 상태에서 모터 이동이 불가 합니다. {0}", Name);
                //인터락 발생 요망. 
                return false;
            }


            HommingCmdStep = 10;
            _isHappenAlarm = false;
            return true;
        }
        public bool ServoOnOffCmd(Equipment equip, bool servoOnOff)
        {
            if (ServoOnOffCmdSetp != 0) return false;

            YB_ServoOnOff.vBit = servoOnOff;
            ServoOnOffCmdSetp = 10;
            return true;
        }
        public bool MoveStop(Equipment equip)
        {
            if (MoveStopCmdStep != 0) return false;
            MoveStopCmdStep = 10;
            return true;
        } 



        protected virtual bool IsStartedStep(Equipment equip)
        {
            if (HommingCmdStep != 0)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <RUNNING> \n (Homming move command entered while moving {0} home.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Homming move command entered while moving {0} home.", Name);
                return true;
            }
            if (PtpMoveCmdStep != 0)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <RUNNING> \n (Ptp Move command entered while moving {0} starting point.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Ptp move command entered while moving {0} starting point.", Name);
                return true;
            }
            return false;
        }

        //모터 알람 처리..
        private void CheckServoStatus(Equipment equip)
        {
            if (_isHappenAlarm == false)
            {
                if (this.IsNegativeLimit && IsHomming == false)
                {
                    AlarmManager.Instance.Happen(equip, MINUS_LIMIT_ERROR);
                    _isHappenAlarm = true;
                    _isLimitAlarm = true;
                }
                if (this.IsPositiveLimit && IsHomming == false)
                {
                    AlarmManager.Instance.Happen(equip, PLUS_LIMIT_ERROR);
                    _isHappenAlarm = true;
                    _isLimitAlarm = true;
                }
                //if (this.IsServoOnBit == false && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, MOTOR_SERVO_ON_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsCriticalPositionError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, CRITICAL_POSITION_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsDriveFaultError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, DRIVE_FAULT_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsOverCurrentError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, OVER_CURRENT_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsECallError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, ECALL_ERROR);
                //    _isHappenAlarm = true;
                //}
            }
            else
            {
                if (_isLimitAlarm == true && this.IsNegativeLimit == false && IsPositiveLimit == false)
                {
                    _isLimitAlarm = false;
                    _isHappenAlarm = false;
                }
            }
        }
        protected bool IsInPosition(ServoPosiInfo info)
        {
            return (Math.Abs(XF_CurrMotorPosition - info.Position) < InposOffset) && XB_StatusMotorInPosition.vBit == true && IsMoving == false &&     (GG.TestMode || IsHomeCompleteBit);
        }
        public bool IsInPosition(int posiNo, EqpRecipe recp, object opt)
        {
            ServoPosiInfo info = GetCalPosition(posiNo, recp, opt);
            return IsInPosition(info);
        }
        protected virtual ServoPosiInfo GetCalPosition(int posiNo, EqpRecipe recp, object opt)
        {
            if (Setting.LstServoPosiInfo.Length < posiNo)
                return null;

            if (Setting.LstServoPosiInfo[posiNo].No != posiNo)
                return null;

            ServoPosiInfo info = (ServoPosiInfo)Setting.LstServoPosiInfo[posiNo].Clone();
            return info;
        }
    }
}

﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class ConvertA2D
    {
        public PlcAddr XF_Value;
        public float Value
        {
            get { return 1485 /4 * (XF_Value.vFloat - 1) - 200 ; }
        }
    }
}

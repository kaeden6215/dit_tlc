﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DIT.TLC.CTRL
{
    public class DelaySensor : Sensor
    {
        public EM_AL_LST SENSOR_ONOFF_ERROR { get; set; }

        DateTime _startTime = DateTime.Now;
        bool _isAlarmOn = false;
        public int TimeOutInterval { get; set; }
        public DelaySensor(bool IsNomalOpen = true, int _timeOutInterval = 1)
        {
            _isAlarmOn = IsNomalOpen;
            TimeOutInterval = _timeOutInterval;
        }

        public void LogicWorking(Equipment equip)
        {
            if (XB_OnOff.vBit == _isAlarmOn)
            {
                if ((DateTime.Now - _startTime).TotalSeconds > TimeOutInterval)
                    AlarmManager.Instance.Happen(equip, SENSOR_ONOFF_ERROR);
            }
            else
                _startTime = DateTime.Now;
        }
    }
}

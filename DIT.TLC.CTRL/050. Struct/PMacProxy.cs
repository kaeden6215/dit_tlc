﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public class PMacmd
    {
        private int SignalTimeOut = 10000;
        public string Name { get; set; }
        public EM_AL_LST TimeOver { get; set; }

        public PlcAddr YB_CMD { get; set; }
        public PlcAddr XB_CMD_ACK { get; set; }

        public int Step = 0;
        private Stopwatch _stepTime = new Stopwatch();

        public void LogicWorking(Equipment equip)
        {
            if (Step > 10 && _stepTime.ElapsedMilliseconds > SignalTimeOut)
            {
                string msg = string.Format("CONTROL↔UMAC {0} SIGNAL TIME OVER", Name);
                Logger.Log.AppendLine(LogLevel.Error, msg);
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_5000_PMAC_EVENT_SIGNAL_TIMEOVER);
                InterLockMgr.AddInterLock(msg,
                    string.Format("Step Name : {0}\nError Step : {1}\nCmd(CONTROL→PMAC) : {2}\nAck(PMAC→CONTROL) : {3}",
                    Name, Step, YB_CMD.vBit, XB_CMD_ACK.vBit));
                Logger.Log.AppendLine(LogLevel.Error, msg);
                _stepTime.Reset();
                YB_CMD.vBit = false;
                Step = 0;
                return;
            }

            if (Step == 0)
            {
            }
            else if (Step == 10)
            {
                _stepTime.Restart();
                YB_CMD.vBit = true;
                Logger.Log.AppendLine(LogLevel.Info, "CONTROL→UMAC {0} SIGNAL START", Name);
                Step = 20;
            }
            else if (Step == 20)
            {
                if (XB_CMD_ACK.vBit)  //|| GG.TestMode
                {
                    Logger.Log.AppendLine(LogLevel.Info, "UMAC→CONTROL {0} SIGNAL END - {1}", Name, XB_CMD_ACK.vBit ? "SUCCESS" : "FAIL");
                    YB_CMD.vBit = false;

                    Step = 0;
                    _stepTime.Reset();
                }
            }
        }
    }

    public enum EmPMacmd
    {
        REVIEW_TIME_OVER,
        PMAC_RESET,
        IMMEDIATE_STOP
    }
    public class PMacProxy
    {
        public PlcAddr YB_EquipMode { get; set; }
        public PlcAddr YB_CheckAlarmStatus { get; set; }
        public PlcAddr YB_UpperInterfaceWorking { get; set; }
        public PlcAddr YB_LowerInterfaceWorking { get; set; }

        public PlcAddr YB_ImmediateStopCmd { get; set; }
        public PlcAddr XB_ImmediateStopCmdAck { get; set; }

        public PlcAddr YB_PmacValueSave { get; set; }
        public PlcAddr XB_PmacValueSaveAck { get; set; }

        public PlcAddr XB_PmacReady { get; set; }
        public PlcAddr XB_PmacAlive { get; set; }
        public PlcAddr XB_PmacHeavyAlarm { get; set; }

        public PlcAddr YB_PinUpMotorInterlockOffCmd { get; set; }
        public PlcAddr XB_PinUpMotorInterlockOffCmdAck { get; set; }
        public PlcAddr XB_PinUpInterlockOff { get; set; }

        public PlcAddr YB_ReviewTimerOverCmd { get; set; }
        public PlcAddr XB_ReviewTimerOverCmdAck { get; set; }

        public PlcAddr YB_PmacResetCmd { get; set; }
        public PlcAddr XB_PmacResetCmdAck { get; set; }

        public PlcAddr YB_Trigger1Cmd { get; set; }
        public PlcAddr YB_Trigger2Cmd { get; set; }
        public PlcAddr YB_Trigger3Cmd { get; set; }

        public PlcAddr XB_ReviewUsingPmac { get; set; }

        public PlcAddr YB_PinUpMotorInterlockDisable { get; set; }
        public PlcAddr XB_PinUpMotorInterlockDisableAck { get; set; }


        public EM_AL_LST HEAVY_ERROR { get; set; }

        public PMacmd[] LstPMacCmd = new PMacmd[10];

        public bool Initailize()
        {
            LstPMacCmd[(int)EmPMacmd.REVIEW_TIME_OVER]  /**/ = new PMacmd() { Name = "REVIEW_TIME_OVER", /**/ YB_CMD = YB_ReviewTimerOverCmd,       /**/ XB_CMD_ACK = XB_ReviewTimerOverCmdAck };
            LstPMacCmd[(int)EmPMacmd.PMAC_RESET]        /**/ = new PMacmd() { Name = "PMAC_RESET",       /**/ YB_CMD = YB_PmacResetCmd,             /**/ XB_CMD_ACK = XB_PmacResetCmdAck };
            LstPMacCmd[(int)EmPMacmd.IMMEDIATE_STOP]    /**/ = new PMacmd() { Name = "IMMADIATE_STOP",   /**/ YB_CMD = YB_ImmediateStopCmd,         /**/ XB_CMD_ACK = XB_ImmediateStopCmdAck };
            return true;
        }
        public void LogicWorking(Equipment equip)
        {
            //YB_EquipMode.vBit = (equip.EquipRunMode == EmEquipRunMode.Manual) ? false : true;
            //YB_CheckAlarmStatus.vBit = (equip.IsHeavyAlarm != true) ? false : true;

            //YB_UpperInterfaceWorking.vBit = (equip.PioI2ARecv.StepPioRecv == 0) ? false : true;
            //YB_LowerInterfaceWorking.vBit = (equip.PioA2ISend.StepPioSend == 0 && equip.PioA2IExch.StepPioExchage == 0) ? false : true;


            //YB_PinUpMotorInterlockDisable.vInt = equip.IsUseInterLockOff ? 1 :
            //                                     equip.LiftPin.IsMoveOnPosition(LiftPinServo.LiftPinDown) == true ||
            //                                     equip.LiftPin.IsMoveOnPosition(LiftPinServo.LiftPinHome) == true ? 1 : 0; // 1일때 제어 가능...

            //Inter Lock 확인후 적용 예정
            //YB_PinUpMotorInterlockDisable.vInt = equip.IsUseInterLockOff ? 1 : 0;
             

            foreach (PMacmd cmd in LstPMacCmd)
            {
                if (cmd == null) continue;
                cmd.LogicWorking(equip);
            }

            CheckServoStatus(equip);

            if (GG.TestMode == false)
                CheckAlive(equip);
        }
        private bool _isHappenAlarm = false;
        private void CheckServoStatus(Equipment equip)
        {
            if (_isHappenAlarm)
            {
                if (this.XB_PmacHeavyAlarm)
                {
                    AlarmManager.Instance.Happen(equip, HEAVY_ERROR);

                    _isHappenAlarm = true;
                }
            }
        }
        //ALIVE 처리..
        private Stopwatch _aliveDateTime = new Stopwatch();
        private bool _pmacPlcAlive = false;
        private int StepTimeOut = 10000;

        private void CheckAlive(Equipment equip)
        {
            if (_pmacPlcAlive != XB_PmacAlive.vBit)
            {
                _aliveDateTime.Restart();
                _pmacPlcAlive = XB_PmacAlive.vBit;
            }

            if (_aliveDateTime.ElapsedMilliseconds > StepTimeOut)
            {
                _aliveDateTime.Restart();
                AlarmManager.Instance.Happen(equip, EM_AL_LST.AL_5001_PMAC_ALIVE_ERROR);
                Logger.Log.AppendLine(LogLevel.Error, "PMAC ALIVE ERROR OCCURRED");
            }
        }

        public void StartReset(Equipment equip)
        {
            _isHappenAlarm = false;
        }
        public bool StartCommand(Equipment equip, EmPMacmd cmd, object tag)
        {
            if (LstPMacCmd[(int)cmd].Step != 0)
            {
                if (cmd == EmPMacmd.IMMEDIATE_STOP) return false;

                InterLockMgr.AddInterLock(string.Format("Interlock <RUNNING> \n (UMAC and {0} existing command in progress.)", cmd.ToString()));
                Logger.Log.AppendLine(LogLevel.Warning, string.Format("UMAC and {0} existing command in progress.", cmd.ToString()));
                return false;
            }

            if (GG.TestMode)
                return true;

            LstPMacCmd[(int)cmd].Step = 10;
            return true;
        }
        public bool IsCommandAck(Equipment equip, EmPMacmd cmd)
        {
            return LstPMacCmd[(int)cmd].Step == 0;
        }
        public int Triggers
        {
            get
            {
                if (YB_Trigger1Cmd.vBit == true)
                    return 1;
                else if (YB_Trigger2Cmd.vBit == true)
                    return 2;
                else if (YB_Trigger3Cmd.vBit == true)
                    return 3;
                else
                    return 0;
            }
            set
            {
                YB_Trigger1Cmd.vBit = false;
                YB_Trigger2Cmd.vBit = false;
                YB_Trigger3Cmd.vBit = false;

                if (value == 2)
                {
                    YB_Trigger2Cmd.vBit = true;
                }
                else if (value == 3)
                {
                    YB_Trigger3Cmd.vBit = true;
                }
                else
                {
                    YB_Trigger1Cmd.vBit = true;
                }
            }
        }
        public bool IsPinUpInterlock
        {
            get
            {
                return XB_PinUpInterlockOff.vBit == false ||
                   XB_PinUpMotorInterlockOffCmdAck.vBit == false;
            }
        }
    }
}

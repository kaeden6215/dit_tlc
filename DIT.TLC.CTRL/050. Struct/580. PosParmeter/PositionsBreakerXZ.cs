﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsBreakerXZ : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int VISION_Z1 { get; set; }                   //  _T("Z1: 비젼 확인 위치 (Z1)")},
        public static int BREAK_STAGE_Z1 { get; set; }                   //	 _T("Z1: 브레이킹 스테이지 위치 (Z1)")},
        public static int VISION_Z2 { get; set; }                   //  _T("Z2: 비젼 확인 위치 (Z2)")},
        public static int BREAK_STAGE_Z2 { get; set; }                   //	 _T("Z2: 브레이킹 스테이지 위치 (Z2)") }
        public static int VISION_Z3 { get; set; }                   //  _T("Z3: 비젼 확인 위치 (Z3)")},
        public static int BREAK_STAGE_Z3 { get; set; }                   //	 _T("Z3: 브레이킹 스테이지 위치 (Z3)")},
        public static int VISION_Z4 { get; set; }                   //  _T("Z4: 비젼 확인 위치 (Z4)")},
        public static int BREAK_STAGE_Z4 { get; set; }                   //	 _T("Z4: 브레이킹 스테이지 위치 (Z4)")},
        public static int VISION_ALIGN_X1 { get; set; }                   //	 _T("A Align Cam 기준 위치(X1)")},
        public static int VISION_ALIGN_X2 { get; set; }                   //	 _T("A Align Cam 기준 위치(X2)")},
        public static int VISION_ALIGN_X3 { get; set; }                   //	 _T("B Align Cam 기준 위치(X3)")},
        public static int VISION_ALIGN_X4 { get; set; }                   //	 _T("B Align Cam 기준 위치(X4)")},

        private bool[] _isPositionOn;

        public PositionsBreakerXZ(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            VISION_Z1 = 1;
            BREAK_STAGE_Z1 = 2;
            VISION_Z2 = 3;
            BREAK_STAGE_Z2 = 4;
            VISION_Z3 = 5;
            BREAK_STAGE_Z3 = 6;
            VISION_Z4 = 7;
            BREAK_STAGE_Z4 = 8;
            VISION_ALIGN_X1 = 9;
            VISION_ALIGN_X2 = 10;
            VISION_ALIGN_X3 = 11;
            VISION_ALIGN_X4 = 12;

            base.MoveActionName[0] = "Z1: 비젼 확인 위치 (Z1)";
            base.MoveActionName[1] = "Z1: 브레이킹 스테이지 위치 (Z1)";
            base.MoveActionName[2] = "Z2: 비젼 확인 위치 (Z2)";
            base.MoveActionName[3] = "Z2: 브레이킹 스테이지 위치 (Z2)";
            base.MoveActionName[5] = "Z3: 비젼 확인 위치 (Z3)";
            base.MoveActionName[6] = "Z3: 브레이킹 스테이지 위치 (Z3)";
            base.MoveActionName[7] = "Z4: 비젼 확인 위치 (Z4)";
            base.MoveActionName[8] = "Z4: 브레이킹 스테이지 위치 (Z4)";
            base.MoveActionName[9] = "A Align Cam 기준 위치(X1)";
            base.MoveActionName[10] = "A Align Cam 기준 위치(X2)";
            base.MoveActionName[11] = "B Align Cam 기준 위치(X3)";
            base.MoveActionName[12] = "B Align Cam 기준 위치(X4)";

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsCameraUnit : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int Y1_To_Inspection                  { get; set; }          //    _T("A 언로딩 이재기 (Y1) : 인스펙션") },
        public static int X1_To_Inspection_Left_1           { get; set; }          //    _T("A 카메라 이재기 (X1) : 인스펙션 1") },
        public static int X1_To_Inspection_Left_2           { get; set; }          //    _T("A 카메라 이재기 (X1) : 인스펙션 2") },
        public static int Z_To_Inspection_Left_1            { get; set; }          //    _T("A 카메라 이재기 (Z1) : 인스펙션 1") },
        public static int Z_To_Inspection_Left_2            { get; set; }          //    _T("A 카메라 이재기 (Z1) : 인스펙션 2") },
        public static int Y1_To_MCR                         { get; set; }          //    _T("A 언로딩 이재기 (Y1) : MCR") },
        public static int X1_To_MCR_Left                    { get; set; }          //    _T("A 카메라 이재기 (X1) : MCR") },
                                                                                    //  
        public static int Y2_To_Inspection                  { get; set; }          //    _T("B 언로딩 이재기 (Y2) : 인스펙션") },
        public static int X1_To_Inspection_Right_1          { get; set; }          //    _T("B 카메라 이재기 (X1) : 인스펙션 1") },
        public static int X1_To_Inspection_Right_2          { get; set; }          //    _T("B 카메라 이재기 (X1) : 인스펙션 2") },
        public static int Z_To_Inspection_Right_1           { get; set; }          //    _T("B 카메라 이재기 (Z1) : 인스펙션 1") },
        public static int Z_To_Inspection_Right_2           { get; set; }          //    _T("B 카메라 이재기 (Z1) : 인스펙션 2") },
        public static int Y2_To_MCR                         { get; set; }          //    _T("B 언로딩 이재기 (Y2) : MCR") },
        public static int X1_To_MCR_Right                   { get; set; }          //    _T("B 카메라 이재기 (X1) : MCR") },
                                                                                     // 
        public static int X1_Left_FineAlign                 { get; set; }          //    _T("A Insp Align 티칭 위치1 (X1)") },
        public static int X2_Left_FineAlign                 { get; set; }          //    _T("A Insp Align 티칭 위치2 (X1)") },
        public static int X3_Right_FineAlign                { get; set; }          //    _T("B Insp Align 티칭 위치3 (X1)") },
        public static int X4_Right_FineAlign                { get; set; }          //    _T("B Insp Align 티칭 위치4 (X1)") },

        private bool[] _isPositionOn;

        public PositionsCameraUnit(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            Y1_To_Inspection                   = 1;
            X1_To_Inspection_Left_1            = 2;
            X1_To_Inspection_Left_2            = 3;
            Z_To_Inspection_Left_1             = 4;
            Z_To_Inspection_Left_2             = 5;
            Y1_To_MCR                          = 6;
            X1_To_MCR_Left                     = 7;                                          
            Y2_To_Inspection                   = 8;
            X1_To_Inspection_Right_1           = 9;
            X1_To_Inspection_Right_2           = 10;
            Z_To_Inspection_Right_1            = 11;
            Z_To_Inspection_Right_2            = 12;
            Y2_To_MCR                          = 13;
            X1_To_MCR_Right                    = 14;                                         
            X1_Left_FineAlign                  = 15;
            X2_Left_FineAlign                  = 16;
            X3_Right_FineAlign                 = 17;
            X4_Right_FineAlign                 = 18;




            base.MoveActionName[0] = "A 언로딩 이재기 (Y1) : 인스펙션";
            base.MoveActionName[1] = "A 카메라 이재기 (X1) : 인스펙션 1";
            base.MoveActionName[2] = "A 카메라 이재기 (X1) : 인스펙션 2";
            base.MoveActionName[3] = "A 카메라 이재기 (Z1) : 인스펙션 1";
            base.MoveActionName[4] = "A 카메라 이재기 (Z1) : 인스펙션 2";
            base.MoveActionName[5] = "A 언로딩 이재기 (Y1) : MCR";
            base.MoveActionName[6] = "A 카메라 이재기 (X1) : MCR";
            base.MoveActionName[7] = "B 언로딩 이재기 (Y2) : 인스펙션";
            base.MoveActionName[8] = "B 카메라 이재기 (X1) : 인스펙션 1";
            base.MoveActionName[9] = "B 카메라 이재기 (X1) : 인스펙션 2";
            base.MoveActionName[10] = "B 카메라 이재기 (Z1) : 인스펙션 1";
            base.MoveActionName[11] = "B 카메라 이재기 (Z1) : 인스펙션 2";
            base.MoveActionName[12] = "B 언로딩 이재기 (Y2) : MCR";
            base.MoveActionName[13] = "B 카메라 이재기 (X1) : MCR";
            base.MoveActionName[14] = "A Insp Align 티칭 위치1 (X1)";
            base.MoveActionName[15] = "A Insp Align 티칭 위치2 (X1)";
            base.MoveActionName[16] = "B Insp Align 티칭 위치3 (X1)";
            base.MoveActionName[17] = "B Insp Align 티칭 위치4 (X1)";
        }                             
    }                                 
}
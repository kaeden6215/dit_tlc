﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsCassetteUnloader : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int LoadingBottomL_T             { get; set; }         //   _T("A 하부 카세트 (T2) : 카세트 투입")},                                                            
        public static int StandByBottomL_Z             { get; set; }          //  _T("A 리프트 (Z1) : 카세트 투입 대기")},
        public static int UnloadingBottomL_T           { get; set; }          //  _T("A 하부 카세트 (T2) : 리프트 이동")},
        public static int LoadingBottomL_Z             { get; set; }          //  _T("A 리프트 (Z1) : 카세트 투입")},
        public static int MeasureL_Z                   { get; set; }          //  _T("A 리프트 (Z1) : 카세트 틸트 측정")},
        public static int CellContactL_Z               { get; set; }          //  _T("A 리프트 (Z1) : 셀 투입 시작")},
        public static int UnloadingTopL_Z              { get; set; }          //  _T("A 리프트 (Z1) : 카세트 배출")},
        public static int UnloadingTopL_T              { get; set; }          //  _T("A 상부 카세트 (T1) : 카세트 배출")},
        public static int LoadingTopL_T                { get; set; }          //  _T("A 상부 카세트 (T1) : 카세트 이동")},
        public static int LoadingBottomR_T             { get; set; }          //  _T("B 하부 카세트 (T4) : 카세트 투입")},
        public static int StandByBottomR_Z             { get; set; }          //  _T("B 리프트 (Z2) : 카세트 투입 대기")},
        public static int UnloadingBottomR_T           { get; set; }          //  _T("B 하부 카세트 (T4) : 리프트 이동")},
        public static int LoadingBottomR_Z             { get; set; }          //  _T("B 리프트 (Z2) : 카세트 투입")},
        public static int MeasureR_Z                   { get; set; }          //  _T("B 리프트 (Z2) : 카세트 틸트 측정")},
        public static int CellContactR_Z               { get; set; }          //  _T("B 리프트 (Z2) : 셀 투입 시작")},
        public static int UnloadingTopR_Z              { get; set; }          //  _T("B 리프트 (Z2) : 카세트 배출")},
        public static int UnloadingTopR_T              { get; set; }          //  _T("B 상부 카세트 (T3) : 카세트 배출")},
        public static int LoadingTopR_T                { get; set; }          //  _T("B 상부 카세트 (T3) : 카세트 이동")},

        private bool[] _isPositionOn;

        public PositionsCassetteUnloader(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            LoadingBottomL_T          = 1;
            StandByBottomL_Z          = 2;
            UnloadingBottomL_T        = 3;
            LoadingBottomL_Z          = 4;
            MeasureL_Z                = 5;
            CellContactL_Z            = 6;
            UnloadingTopL_Z           = 7;
            UnloadingTopL_T           = 8;
            LoadingTopL_T             = 9;
            LoadingBottomR_T          = 10;
            StandByBottomR_Z          = 11;
            UnloadingBottomR_T        = 12;
            LoadingBottomR_Z          = 13;
            MeasureR_Z                = 14;
            CellContactR_Z            = 15;
            UnloadingTopR_Z           = 16;
            UnloadingTopR_T           = 17;
            LoadingTopR_T             = 18;

            base.MoveActionName[0]  = "A 하부 카세트 (T2) : 카세트 투입";  
            base.MoveActionName[1]  = "A 리프트 (Z1) : 카세트 투입 대기";
            base.MoveActionName[2]  = "A 하부 카세트 (T2) : 리프트 이동";
            base.MoveActionName[3]  = "A 리프트 (Z1) : 카세트 투입";
            base.MoveActionName[4]  = "A 리프트 (Z1) : 카세트 틸트 측정";
            base.MoveActionName[5]  = "A 리프트 (Z1) : 셀 투입 시작";
            base.MoveActionName[6]  = "A 리프트 (Z1) : 카세트 배출";
            base.MoveActionName[7]  = "A 상부 카세트 (T1) : 카세트 배출";
            base.MoveActionName[8]  = "A 상부 카세트 (T1) : 카세트 이동";
            base.MoveActionName[9]  = "B 하부 카세트 (T4) : 카세트 투입";
            base.MoveActionName[10] = "B 리프트 (Z2) : 카세트 투입 대기";
            base.MoveActionName[11] = "B 하부 카세트 (T4) : 리프트 이동";
            base.MoveActionName[12] = "B 리프트 (Z2) : 카세트 투입";
            base.MoveActionName[13] = "B 리프트 (Z2) : 카세트 틸트 측정";
            base.MoveActionName[14] = "B 리프트 (Z2) : 셀 투입 시작";
            base.MoveActionName[16] = "B 상부 카세트 (T3) : 카세트 배출";
            base.MoveActionName[17] = "B 상부 카세트 (T3) : 카세트 이동";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionersUnitBase
    {
        public static int HomePosition = 0;
        public int SoftMinusLimit = 10;
        public int SoftPlusLimit = 10;
        public int SoftSpeedLimit = 10;
        public int SoftJogSpeedLimit = 10;
        public int EnableGripJogSpeedLimit = 30;
        public float InposOffset = 0.1f;
        public int SoftAccelPlusLimit = 1000;
        public int SoftAccelMinusLimit = 100;
        public float PinAvoidPos = 500.0f;

        public VirtualPlc PLC;

        public string Name { get; set; }
        public int Axis { get; set; }
        public double Pos { get; set; }
        public double Speed { get; set; }

        public int PositionCount { get; set; }

        public string[] MoveActionName { get; set; }

        public PositionersUnitBase(string name, int axis, double pos, double speed, int posiCount = 7)
        {
            this.Name = name;
            this.Axis = axis;
            this.Pos = pos;
            this.Speed = speed;
            PositionCount = posiCount;

        }
    }
}

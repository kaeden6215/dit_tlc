﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsCellUnloadTransfer : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

       public static int Y1_To_Stage             { get; set; }      // _T("언로딩 이재기 (Y1) : A 스테이지") },
       public static int Y1_To_Unloader          { get; set; }      // _T("언로딩 이재기 (Y1) : 언로딩") },
       public static int T1_0                    { get; set; }      // _T("언로딩 이재기 (T1) : 0도") },
       public static int T2_0                    { get; set; }      // _T("언로딩 이재기 (T2) : 0도") },
       public static int Y2_To_Stage             { get; set; }      // _T("언로딩 이재기 (Y2) : B 스테이지") },
       public static int Y2_To_Unloader          { get; set; }      // _T("언로딩 이재기 (Y2) : 언로딩") },
       public static int T3_0                    { get; set; }      // _T("언로딩 이재기 (T3) : 0도") },
       public static int T4_0                   { get; set; }       // _T("언로딩 이재기 (T4) : 0도") },

        private bool[] _isPositionOn;

        public PositionsCellUnloadTransfer(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            Y1_To_Stage          = 1;
            Y1_To_Unloader       = 2;
            T1_0                 = 3;
            T2_0                 = 4;
            Y2_To_Stage          = 5;
            Y2_To_Unloader       = 6;
            T3_0                 = 7;
            T4_0                 = 8;


            base.MoveActionName[0] = "언로딩 이재기 (Y1) : A 스테이지";
            base.MoveActionName[1] = "언로딩 이재기 (Y1) : 언로딩";
            base.MoveActionName[2] = "언로딩 이재기 (T1) : 0도";
            base.MoveActionName[3] = "언로딩 이재기 (T2) : 0도";
            base.MoveActionName[4] = "언로딩 이재기 (Y2) : B 스테이지";
            base.MoveActionName[5] = "언로딩 이재기 (Y2) : 언로딩";
            base.MoveActionName[6] = "언로딩 이재기 (T3) : 0도";
            base.MoveActionName[7] = "언로딩 이재기 (T4) : 0도";

        }
    }
}
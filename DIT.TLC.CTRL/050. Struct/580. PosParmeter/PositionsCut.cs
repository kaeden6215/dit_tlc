﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsCut : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int Load_y1_L               { get; set; }     //     _T("A 가공 테이블 : 셀 로드위치 (Y1)")},
        public static int UnLoad_y1_L             { get; set; }     //	   _T("A 가공 테이블 : 셀 언로드위치 (Y1)")},
        public static int Load_y2_R               { get; set; }     //     _T("B 가공 테이블 : 셀 로드위치 (Y2)")},
        public static int UnLoad_y2_R             { get; set; }     //	   _T("B 가공 테이블 : 셀 언로드위치 (Y2)")},
        public static int X1_Left_Shot_L          { get; set; }     //     _T("A 가공 테이블1: 레이져 샷 (X1)")},
        public static int Y1_Left_Shot_L          { get; set; }     //     _T("A 가공 테이블1: 레이져 샷 (Y1)")},
        public static int X1_Left_vision_L        { get; set; }     //     _T("A 가공 테이블1: 카메라 확인 (X1)")},
        public static int Y1_Left_vision_L        { get; set; }     //     _T("A 가공 테이블1: 카메라 확인 (Y1)")},
        public static int X1_Left_Shot_R          { get; set; }     //     _T("A 가공 테이블2: 레이져 샷 (X1)")},
        public static int Y1_Left_Shot_R          { get; set; }     //     _T("A 가공 테이블2: 레이져 샷 (Y1)")},
        public static int X1_Left_vision_R        { get; set; }     //     _T("A 가공 테이블2: 카메라 확인 (X1)")},
        public static int Y1_Left_vision_R        { get; set; }     //     _T("A 가공 테이블2: 카메라 확인 (Y1)")},
        public static int X1_Right_Shot_L         { get; set; }     //	   _T("B 가공 테이블1: 레이져 샷 (X1)")},
        public static int Y2_Right_Shot_L         { get; set; }     //	   _T("B 가공 테이블1: 레이져 샷 (Y2)")},
        public static int X1_Right_vision_L       { get; set; }     //     _T("B 가공 테이블1: 카메라 확인 (X1)")},
        public static int Y2_Right_vision_L       { get; set; }     //     _T("B 가공 테이블1: 카메라 확인 (Y2)")},
        public static int X1_Right_Shot_R         { get; set; }     //	   _T("B 가공 테이블2: 레이져 샷 (X1)")},
        public static int Y2_Right_Shot_R         { get; set; }     //	   _T("B 가공 테이블2: 레이져 샷 (Y2)")},
        public static int X1_Right_vision_R       { get; set; }     //     _T("B 가공 테이블2: 카메라 확인 (X1)")},
        public static int Y2_Right_vision_R       { get; set; }     //     _T("B 가공 테이블2: 카메라 확인 (Y2)")},
        public static int X_PowerMeter            { get; set; }     //     _T("파워미터 측정 위치 (X1)")},
        public static int Z_PowerMeter            { get; set; }     //     _T("파워미터 측정 위치 (Z1)")},
        public static int X_Left_FineAlign        { get; set; }     //     _T("A Fine Align 티칭 위치 (X1)")},
        public static int Y_Left_FineAlign        { get; set; }     //     _T("A Fine Align 티칭 위치 (Y1)")},
        public static int X_Right_FineAlign       { get; set; }     //     _T("B Fine Align 티칭 위치 (X1)")},
        public static int Y_Right_FineAlign       { get; set; }     //     _T("B Fine Align 티칭 위치 (Y2)")},
        public static int LDS_Process_X           { get; set; }     //     _T("LDS 측정 X:  LDS 기준 측정위치(X1)")},
        public static int LDS_Process_Y           { get; set; }     //     _T("LDS 측정 Y:  LDS 기준 측정위치(Y1)")},

        private bool[] _isPositionOn;

        public PositionsCut(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            Load_y1_L                  = 1;
            UnLoad_y1_L                = 2;
            Load_y2_R                  = 3;
            UnLoad_y2_R                = 4;
            X1_Left_Shot_L             = 5;
            Y1_Left_Shot_L             = 6;
            X1_Left_vision_L           = 7;
            Y1_Left_vision_L           = 8;
            X1_Left_Shot_R             = 9;
            Y1_Left_Shot_R             = 10;
            X1_Left_vision_R           = 11;
            Y1_Left_vision_R           = 12;
            X1_Right_Shot_L            = 13;
            Y2_Right_Shot_L            = 14;
            X1_Right_vision_L          = 15;
            Y2_Right_vision_L          = 16;
            X1_Right_Shot_R            = 17;
            Y2_Right_Shot_R            = 18;
            X1_Right_vision_R          = 19;
            Y2_Right_vision_R          = 20;
            X_PowerMeter               = 21;
            Z_PowerMeter               = 22;
            X_Left_FineAlign           = 23;
            Y_Left_FineAlign           = 24;
            X_Right_FineAlign          = 25;
            Y_Right_FineAlign          = 26;
            LDS_Process_X              = 27;
            LDS_Process_Y              = 28;

            base.MoveActionName[0]  = "A 가공 테이블 : 셀 로드위치 (Y1)";
            base.MoveActionName[1]  = "A 가공 테이블 : 셀 언로드위치 (Y1)";
            base.MoveActionName[2]  = "B 가공 테이블 : 셀 로드위치 (Y2)";
            base.MoveActionName[3]  = "B 가공 테이블 : 셀 언로드위치 (Y2)";
            base.MoveActionName[4]  = "A 가공 테이블1: 레이져 샷 (X1)";
            base.MoveActionName[5]  = "A 가공 테이블1: 레이져 샷 (Y1)";
            base.MoveActionName[6]  = "A 가공 테이블1: 카메라 확인 (X1)";
            base.MoveActionName[7]  = "A 가공 테이블1: 카메라 확인 (Y1)";
            base.MoveActionName[8]  = "A 가공 테이블2: 레이져 샷 (X1)";
            base.MoveActionName[9]  = "A 가공 테이블2: 레이져 샷 (Y1)";
            base.MoveActionName[10] = "A 가공 테이블2: 카메라 확인 (X1)";
            base.MoveActionName[11] = "A 가공 테이블2: 카메라 확인 (Y1)";
            base.MoveActionName[12] = "B 가공 테이블1: 레이져 샷 (X1)";
            base.MoveActionName[13] = "B 가공 테이블1: 레이져 샷 (Y2)";
            base.MoveActionName[14] = "B 가공 테이블1: 카메라 확인 (X1)";
            base.MoveActionName[15] = "B 가공 테이블1: 카메라 확인 (Y2)";
            base.MoveActionName[16] = "B 가공 테이블2: 레이져 샷 (X1)";
            base.MoveActionName[17] = "B 가공 테이블2: 레이져 샷 (Y2)";
            base.MoveActionName[18] = "B 가공 테이블2: 카메라 확인 (X1)";
            base.MoveActionName[19] = "B 가공 테이블2: 카메라 확인 (Y2)";
            base.MoveActionName[20] = "파워미터 측정 위치 (X1)";
            base.MoveActionName[21] = "파워미터 측정 위치 (Z1)";
            base.MoveActionName[22] = "A Fine Align 티칭 위치 (X1)";
            base.MoveActionName[23] = "A Fine Align 티칭 위치 (Y1)";
            base.MoveActionName[24] = "B Fine Align 티칭 위치 (X1)";
            base.MoveActionName[25] = "B Fine Align 티칭 위치 (Y2)";
            base.MoveActionName[26] = "LDS 측정 X:  LDS 기준 측정위치(X1)";
            base.MoveActionName[27] = "LDS 측정 Y:  LDS 기준 측정위치(Y1)";
        }
    }
}
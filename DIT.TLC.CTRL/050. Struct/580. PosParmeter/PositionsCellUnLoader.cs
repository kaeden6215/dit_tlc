﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsCellUnLoader : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int X1_TR_L                      { get; set; }         // _T("언로더 (X1) : A 언로딩 이재기") },
        public static int X2_TR_L                      { get; set; }         // _T("언로더 (X2) : A 언로딩 이재기") },
        public static int Y1_TR_L                      { get; set; }         // _T("언로더 (Y1) : A 언로딩 이재기") },
        public static int Y2_TR_L                      { get; set; }         // _T("언로더 (Y2) : A 언로딩 이재기") },
        public static int X1_CST_L                     { get; set; }         // _T("언로더 (X1) : A 카세트 중간") },
        public static int X2_CST_L                     { get; set; }         // _T("언로더 (X2) : A 카세트 중간") },
        public static int Y1_CST_L                     { get; set; }         // _T("언로더 (Y1) : A 카세트 안쪽") },
        public static int Y2_CST_L                     { get; set; }         // _T("언로더 (Y2) : A 카세트 안쪽") },
        public static int X1_CST_STAND_BY_L            { get; set; }         // _T("언로더 (X1) : A 카세트 대기") },
        public static int X2_CST_STAND_BY_L            { get; set; }         // _T("언로더 (X2) : A 카세트 대기") },
        public static int X1_TR_R                      { get; set; }         // _T("언로더 (X1) : B 언로딩 이재기") },
        public static int X2_TR_R                      { get; set; }         // _T("언로더 (X2) : B 언로딩 이재기") },
        public static int Y1_TR_R                      { get; set; }         // _T("언로더 (Y1) : B 언로딩 이재기") },
        public static int Y2_TR_R                      { get; set; }         // _T("언로더 (Y2) : B 언로딩 이재기") },
        public static int X1_CST_R                     { get; set; }         // _T("언로더 (X1) : B 카세트 중간") },
        public static int X2_CST_R                     { get; set; }         // _T("언로더 (X2) : B 카세트 중간") },
        public static int Y1_CST_R                     { get; set; }         // _T("언로더 (Y1) : B 카세트 안쪽") },
        public static int Y2_CST_R                     { get; set; }         // _T("언로더 (Y2) : B 카세트 안쪽") },
        public static int X1_CST_STAND_BY_R            { get; set; }         // _T("언로더 (X1) : B 카세트 대기") },
        public static int X2_CST_STAND_BY_R            { get; set; }         // _T("언로더 (X2) : B 카세트 대기") },
        public static int Y1_CST_Mid                   { get; set; }         // _T("언로더 (Y1) : 카세트 중간") },
        public static int Y2_CST_Mid                   { get; set; }         // _T("언로더 (Y2) : 카세트 중간") },
        public static int X1_BUFFER                    { get; set; }         // _T("언로더 (X1) : 버퍼") },
        public static int X2_BUFFER                    { get; set; }         // _T("언로더 (X2) : 버퍼") },
        public static int Y1_BUFFER                    { get; set; }         // _T("언로더 (Y1) : 버퍼") },
        public static int Y2_BUFFER                    { get; set; }         // _T("언로더 (Y2) : 버퍼") },

        private bool[] _isPositionOn;

        public PositionsCellUnLoader(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            X1_TR_L                     = 1;
            X2_TR_L                     = 2;
            Y1_TR_L                     = 3;
            Y2_TR_L                     = 4;
            X1_CST_L                    = 5;
            X2_CST_L                    = 6;
            Y1_CST_L                    = 7;
            Y2_CST_L                    = 8;
            X1_CST_STAND_BY_L           = 9;
            X2_CST_STAND_BY_L           = 10;
            X1_TR_R                     = 11;
            X2_TR_R                     = 12;
            Y1_TR_R                     = 13;
            Y2_TR_R                     = 14;
            X1_CST_R                    = 15;
            X2_CST_R                    = 16;
            Y1_CST_R                    = 17;
            Y2_CST_R                    = 18;
            X1_CST_STAND_BY_R           = 19;
            X2_CST_STAND_BY_R           = 20;
            Y1_CST_Mid                  = 21;
            Y2_CST_Mid                  = 22;
            X1_BUFFER                   = 23;
            X2_BUFFER                   = 24;
            Y1_BUFFER                   = 25;
            Y2_BUFFER                   = 26;

            base.MoveActionName[0]  = "언로더 (X1) : A 언로딩 이재기";
            base.MoveActionName[1]  = "언로더 (X2) : A 언로딩 이재기";
            base.MoveActionName[2]  = "언로더 (Y1) : A 언로딩 이재기";
            base.MoveActionName[3]  = "언로더 (Y2) : A 언로딩 이재기";
            base.MoveActionName[4]  = "언로더 (X1) : A 카세트 중간";
            base.MoveActionName[5]  = "언로더 (X2) : A 카세트 중간";
            base.MoveActionName[6]  = "언로더 (Y1) : A 카세트 안쪽";
            base.MoveActionName[7]  = "언로더 (Y2) : A 카세트 안쪽";
            base.MoveActionName[8]  = "언로더 (X1) : A 카세트 대기";
            base.MoveActionName[9] = "언로더 (X2) : A 카세트 대기";
            base.MoveActionName[10] = "언로더 (X1) : B 언로딩 이재기";
            base.MoveActionName[11] = "언로더 (X2) : B 언로딩 이재기";
            base.MoveActionName[12] = "언로더 (Y1) : B 언로딩 이재기";
            base.MoveActionName[13] = "언로더 (Y2) : B 언로딩 이재기";
            base.MoveActionName[14] = "언로더 (X1) : B 카세트 중간";
            base.MoveActionName[15] = "언로더 (X2) : B 카세트 중간";
            base.MoveActionName[16] = "언로더 (Y1) : B 카세트 안쪽";
            base.MoveActionName[17] = "언로더 (Y2) : B 카세트 안쪽";
            base.MoveActionName[18] = "언로더 (X1) : B 카세트 대기";
            base.MoveActionName[19] = "언로더 (X2) : B 카세트 대기";
            base.MoveActionName[20] = "언로더 (Y1) : 카세트 중간";
            base.MoveActionName[21] = "언로더 (Y2) : 카세트 중간";
            base.MoveActionName[22] = "언로더 (X1) : 버퍼";
            base.MoveActionName[23] = "언로더 (X2) : 버퍼";
            base.MoveActionName[24] = "언로더 (Y1) : 버퍼";
            base.MoveActionName[25] = "언로더 (Y2) : 버퍼";


        }
    }
}
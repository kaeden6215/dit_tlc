﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsBreakTransfer : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int Load_Y1            { get; set; }                    // _T("After IR 이재기 겐트리 : 셀 로드 위치(Y1)")},
        public static int Load_X1_Lef        { get; set; }                    // _T("A After IR 이재기 X1 : 셀 로드 위치(X1)")},
        public static int Load_X2_Left       { get; set; }                    // _T("A After IR 이재기 X2 : 셀 로드 위치(X2)")},
        public static int Load_X1_Right      { get; set; }                    // _T("B After IR 이재기 X1 : 셀 로드 위치(X1)")},
        public static int Load_X2_Right      { get; set; }                    // _T("B After IR 이재기 X2 : 셀 로드 위치(X2)")},
        public static int UnLoad_Y1          { get; set; }                    // _T("After IR 이재기 겐트리 : 셀 언로드 위치(Y1)")},
        public static int UnLoad_X1_Lef      { get; set; }                    // _T("A After IR 이재기 X1 : 셀 언로드 위치(X1)")},
        public static int UnLoad_X2_Left     { get; set; }                    // _T("A After IR 이재기 X2 : 셀 언로드 위치(X2)")},
        public static int UnLoad_X1_Right    { get; set; }                    // _T("B After IR 이재기 X1 : 셀 언로드 위치(X1)")},
        public static int UnLoad_X2_Right    { get; set; }                    // _T("B After IR 이재기 X2 : 셀 언로드 위치(X2)")},

        private bool[] _isPositionOn;

        public PositionsBreakTransfer(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            Load_Y1                 = 1;
            Load_X1_Lef             = 2;
            Load_X2_Left            = 3;
            Load_X1_Right           = 4;
            Load_X2_Right           = 5;
            UnLoad_Y1               = 6;
            UnLoad_X1_Lef           = 7;
            UnLoad_X2_Left          = 8;
            UnLoad_X1_Right         = 9;
            UnLoad_X2_Right         = 10;


            base.MoveActionName[0]  = "After IR 이재기 겐트리 : 셀 로드 위치(Y1)";
            base.MoveActionName[1]  = "A After IR 이재기 X1 : 셀 로드 위치(X1)";
            base.MoveActionName[2]  = "A After IR 이재기 X2 : 셀 로드 위치(X2)";
            base.MoveActionName[3]  = "B After IR 이재기 X1 : 셀 로드 위치(X1)";
            base.MoveActionName[5]  = "B After IR 이재기 X2 : 셀 로드 위치(X2)";
            base.MoveActionName[6]  = "After IR 이재기 겐트리 : 셀 언로드 위치(Y1)";
            base.MoveActionName[7]  = "A After IR 이재기 X1 : 셀 언로드 위치(X1)";
            base.MoveActionName[8]  = "A After IR 이재기 X2 : 셀 언로드 위치(X2)";
            base.MoveActionName[9]  = "B After IR 이재기 X1 : 셀 언로드 위치(X1)";
            base.MoveActionName[10] = "B After IR 이재기 X2 : 셀 언로드 위치(X2)";

        }
    }
}
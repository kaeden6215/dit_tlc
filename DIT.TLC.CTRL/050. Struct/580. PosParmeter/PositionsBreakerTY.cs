﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsBreakerTY : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int Load_Y1             { get; set; }          // _T("A 테이블: 셀 로드 위치 (Y1)")},
        public static int UnLoad_Y1           { get; set; }          // _T("A 테이블: 셀 언로드 위치 (Y1)")},
                                                                     //
        public static int Load_Y2             { get; set; }          // _T("B 테이블: 셀 로드 위치 (Y2)")},
        public static int UnLoad_Y2           { get; set; }          // _T("B 테이블: 셀 언로드 위치 (Y2)")},
        public static int Load_T1             { get; set; }          // _T("T1: 대기 위치 (T1)")},
        public static int Load_T2             { get; set; }          // _T("T2: 대기 위치 (T2)")},
        public static int Load_T3             { get; set; }          // _T("T3: 대기 위치 (T3)")},
        public static int Load_T4             { get; set; }          // _T("T4: 대기 위치 (T4)")},
        public static int VISION_ALIGN_Y1     { get; set; }          // _T("A Align Cam 기준 위치(Y1)")},
        public static int VISION_ALIGN_Y2     { get; set; }          // _T("B Align Cam 기준 위치(Y2)")},
        public static int LDS_Break_X         { get; set; }          // _T("LDS 측정 X: LDS 기준 측정위치(X1)")},
        public static int LDS_Break_y         { get; set; }          // _T("LDS 측정 Y: LDS 기준 측정위치(Y1)")},

        private bool[] _isPositionOn;

        public PositionsBreakerTY(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            Load_Y1           = 1;
            UnLoad_Y1         = 2;
            Load_Y2           = 3;
            UnLoad_Y2         = 4;
            Load_T1           = 5;
            Load_T2           = 6;
            Load_T3           = 7;
            Load_T4           = 8;
            VISION_ALIGN_Y1   = 9;
            VISION_ALIGN_Y2   = 10;
            LDS_Break_X       = 11;
            LDS_Break_y       = 12;

            base.MoveActionName[0]  = "A 테이블: 셀 로드 위치 (Y1)";
            base.MoveActionName[1]  = "A 테이블: 셀 언로드 위치 (Y1)";
            base.MoveActionName[2]  = "B 테이블: 셀 로드 위치 (Y2)";
            base.MoveActionName[3]  = "B 테이블: 셀 언로드 위치 (Y2)";
            base.MoveActionName[5]  = "T1: 대기 위치 (T1)";
            base.MoveActionName[6]  = "T2: 대기 위치 (T2)";
            base.MoveActionName[7]  = "T3: 대기 위치 (T3)";
            base.MoveActionName[8]  = "T4: 대기 위치 (T4)";
            base.MoveActionName[9]  = "A Align Cam 기준 위치(Y1)";
            base.MoveActionName[10] = "B Align Cam 기준 위치(Y2)";
            base.MoveActionName[11] = "LDS 측정 X: LDS 기준 측정위치(X1)";
            base.MoveActionName[12] = "LDS 측정 Y: LDS 기준 측정위치(Y1)";

        }
    }
}

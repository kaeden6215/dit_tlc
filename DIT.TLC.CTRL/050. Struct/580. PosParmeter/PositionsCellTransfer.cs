﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._580._PosParmeter
{
    public class PositionsCellTransfer : PositionersUnitBase
    {
        //public static string PATH_SETTING_BreakerTY = Path.Combine(Application.StartupPath, "Setting", "InspY1Servo.ini");

        public static int X1_Loading               { get; set; }      // _T("로더 이재기 (X1) : 로딩") },
        public static int X2_Loading               { get; set; }      // _T("로더 이재기 (X2) : 로딩") },
        public static int Y1_Laod                  { get; set; }      // _T("로더 이재기 (Y1) : 로딩")},
        public static int Y1_Unlaod                { get; set; }      // _T("로더 이재기 (Y1) : 언로딩")},
        public static int X1_Unload_L              { get; set; }      // _T("로더 이재기 (X1) : A 스테이지 언로딩") },
        public static int X2_Unload_L              { get; set; }      // _T("로더 이재기 (X2) : A 스테이지 언로딩")},
        public static int Picker_L_Y_Load          { get; set; }      // _T("로더 이재기 (MY1) : A 피커 로딩")},
        public static int Picker_L_Y_Unload        { get; set; }      // _T("로더 이재기 (MY1) : A 피커 언로딩")},
        public static int Picker_L_0               { get; set; }      // _T("로더 이재기 (T1) : 피커 0도")},
        public static int X2_Unload_R              { get; set; }      // _T("로더 이재기 (X1) : B 스테이지 언로딩") },
        public static int X1_Unload_R              { get; set; }      // _T("로더 이재기 (X2) : B 스테이지 언로딩") },
        public static int Picker_R_Y_Load          { get; set; }      // _T("로더 이재기 (MY2) : B 피커 로딩") },
        public static int Picker_R_Y_Unload        { get; set; }      // _T("로더 이재기 (MY2) : B 피커 언로딩") },
        public static int Picker_R_0               { get; set; }      // _T("로더 이재기 (T2) : 피커 0도") },
        public static int Picker_PreAlignX1        { get; set; }      // _T("로더 이재기 (X1) : PreAlign X1") },
        public static int Picker_PreAlignX2        { get; set; }      // _T("로더 이재기 (X2) : PreAlign X2") },
        public static int Picker_PreAlignY1        { get; set; }      // _T("로더 이재기 (Y1) : PreAlign Y1") },

        private bool[] _isPositionOn;

        public PositionsCellTransfer(string name, int axis, double pos, double speed, int posiCount = 5) :
            base(name, axis, pos, speed, posiCount)
        {
            SoftMinusLimit = 0;
            SoftPlusLimit = 205;
            SoftSpeedLimit = 801;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 1000;
            SoftAccelMinusLimit = 50;
            EnableGripJogSpeedLimit = 30;

            _isPositionOn = new bool[posiCount];

            X1_Loading                     = 1;
            X2_Loading                     = 2;
            Y1_Laod                        = 3;
            Y1_Unlaod                      = 4;
            X1_Unload_L                    = 5;
            X2_Unload_L                    = 6;
            Picker_L_Y_Load                = 7;
            Picker_L_Y_Unload              = 8;
            Picker_L_0                     = 9;
            X2_Unload_R                    = 10;
            X1_Unload_R                    = 11;
            Picker_R_Y_Load                = 12;
            Picker_R_Y_Unload              = 13;
            Picker_R_0                     = 14;
            Picker_PreAlignX1              = 15;
            Picker_PreAlignX2              = 16;
            Picker_PreAlignY1              = 17;


            base.MoveActionName[0]  = "로더 이재기 (X1) : 로딩";
            base.MoveActionName[1]  = "로더 이재기 (X2) : 로딩";
            base.MoveActionName[2]  = "로더 이재기 (Y1) : 로딩";
            base.MoveActionName[3]  = "로더 이재기 (Y1) : 언로딩";
            base.MoveActionName[4]  = "로더 이재기 (X1) : A 스테이지 언로딩";
            base.MoveActionName[5]  = "로더 이재기 (X2) : A 스테이지 언로딩";
            base.MoveActionName[6]  = "로더 이재기 (MY1) : A 피커 로딩";
            base.MoveActionName[7]  = "로더 이재기 (MY1) : A 피커 언로딩";
            base.MoveActionName[8]  = "로더 이재기 (T1) : 피커 0도";
            base.MoveActionName[9]  = "로더 이재기 (X1) : B 스테이지 언로딩";
            base.MoveActionName[10] = "로더 이재기 (X2) : B 스테이지 언로딩";
            base.MoveActionName[11] = "로더 이재기 (MY2) : B 피커 로딩";
            base.MoveActionName[12] = "로더 이재기 (MY2) : B 피커 언로딩";
            base.MoveActionName[13] = "로더 이재기 (T2) : 피커 0도";
            base.MoveActionName[14] = "로더 이재기 (X1) : PreAlign X1";
            base.MoveActionName[15] = "로더 이재기 (X2) : PreAlign X2";
            base.MoveActionName[16] = "로더 이재기 (Y1) : PreAlign Y1";

        }
    }
}
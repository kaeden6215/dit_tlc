﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class SafetyManger : UnitBase
    {
        public enum EmSF_NO
        {
            S000_WAIT,
            S010_SAFETY_RESET_START,
            S020_SAFETY_RESET_END
        };

        public void StartReset()
        {
            _stepNum = EmSF_NO.S010_SAFETY_RESET_START;
        }

        public override void LogicWorking(Equipment equip)
        {

            SafetyLogicWorking(equip);
            SafetyResetWorking(equip);
            LdGpsMainMccbTripLogicWorking(equip, 0);
        }
        public void SafetyLogicWorking(Equipment equip)
        {
            bool ldBuzzser = equip.LD.IsDoorOpen == true && equip.LD.IsDoorKeyBoxOn == false;
            bool procBuzzser = equip.PROC.IsDoorOpen == true && equip.PROC.IsDoorKeyBoxOn == false;
            bool udBuzzser = equip.UD.IsDoorOpen == true && equip.UD.IsDoorKeyBoxOn == false;


            equip.LD.음성Buzzer2.YB_OnOff.vBit = ldBuzzser;
            equip.LD.음성Buzzer1.YB_OnOff.vBit = procBuzzser;
            equip.UD.음성Buzzer.YB_OnOff.vBit = udBuzzser;
        }
        public enum EM_MCCB_TRIB_CMD_ST
        {
            S000_WAIT,
            S010_TRIP_STRART,
            S020_,
        };
        private EM_MCCB_TRIB_CMD_ST[] _stepMccbTrip = new EM_MCCB_TRIB_CMD_ST[3];
        public void GpsMainMccbTripCmd(int unit)
        {
            //if (_stepMccbTrip[0] == EM_MCCB_TRIB_CMD_ST.S000_WAIT)
            //    _stepMccbTrip[0] = EM_MCCB_TRIB_CMD_ST.S010_TRIP_STRART;
        }
        public void LdGpsMainMccbTripLogicWorking(Equipment equip, int unit)
        {
            if (_stepMccbTrip[unit] == EM_MCCB_TRIB_CMD_ST.S000_WAIT)
            {
                equip.LD.GpsMainMccbTrip.OnOff(equip, false);
            }
            else if (_stepMccbTrip[unit] == EM_MCCB_TRIB_CMD_ST.S010_TRIP_STRART)
            {
                if (unit == 0)
                {
                    equip.LD.GpsMainMccbTrip.OnOff(equip, true);
                    _stepMccbTrip[unit] = EM_MCCB_TRIB_CMD_ST.S020_;
                }
                //else if (unit == 1) equip.PROC.GpsMainMccbTrip.OnOff(equip, true);
                //else if (unit == 2) equip.UD.GpsMainMccbTrip.OnOff(equip, true);
            }
            else if (_stepMccbTrip[unit] == EM_MCCB_TRIB_CMD_ST.S020_)
            {
                if (unit == 0 && equip.LD.GpsMainMccbTrip.IsOnOff == false)
                    _stepMccbTrip[unit] = EM_MCCB_TRIB_CMD_ST.S000_WAIT;
            }
        }


        private EmSF_NO _stepNum = EmSF_NO.S000_WAIT;
        private PlcTimerEx _SafetyPlcResetTmrDelay = new PlcTimerEx("Safety Plc Reset Delay");
        public void SafetyResetWorking(Equipment equip)
        {
            if (_stepNum == EmSF_NO.S000_WAIT)
            {

            }
            else if (_stepNum == EmSF_NO.S010_SAFETY_RESET_START)
            {
                equip.LD.SafteyReset.OnOff(equip, true);
                equip.PROC.Safety이중화CP1.OnOff(equip, true);
                equip.PROC.Safety이중화CP2.OnOff(equip, true);
                //equip.UD.CpBoxResetSwitch
                _SafetyPlcResetTmrDelay.Start(2);
                _stepNum = EmSF_NO.S020_SAFETY_RESET_END;
            }
            else if (_stepNum == EmSF_NO.S020_SAFETY_RESET_END)
            {
                if (_SafetyPlcResetTmrDelay)
                {
                    _SafetyPlcResetTmrDelay.Stop();


                    equip.LD.SafteyReset.OnOff(equip, false);
                    equip.PROC.Safety이중화CP1.OnOff(equip, false);
                    equip.PROC.Safety이중화CP2.OnOff(equip, false);


                    _stepNum = EmSF_NO.S000_WAIT;
                }
            }
        }


    }
}

﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.Tester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        VirtualUMacDirect vv;
        VirtualUMac vv2;
        private void button1_Click(object sender, EventArgs e)
        {
            //vv = new VirtualUMacDirect("", "", 0);
            //vv.LstReadAddr.Add(new PlcAddr(PlcMemType.M, 1000, 0, 100));
            //vv.LstWriteAddr.Add(new PlcAddr(PlcMemType.M, 5000, 0, 100));
            //vv.Open();
            vv2 = new VirtualUMac("UMAC", "192.6.94.5", 1500);
        }

        private void btnJogPlus_Click(object sender, EventArgs e)
        {
            vv.JogMove(1, 1);
        }

        private void btnJogStop_Click(object sender, EventArgs e)
        {
            vv.JogStop(1);
        }

        private void btnbtnJogMinus_Click(object sender, EventArgs e)
        {
            vv.JogMove(1, -1);
        }

        private void btnPtp_Click(object sender, EventArgs e)
        {
            vv.PTPMove(1, (double)nudPosition.Value);
        }

        private void btnPtpSpeed_Click(object sender, EventArgs e)
        {
            vv.PTPMoveSpeed(1, (double)nudPosition.Value, (double)nudSpeed.Value);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            vv.StepMove(1, (double)nudPosition.Value);
        }

        private void btnKill_Click(object sender, EventArgs e)
        {
            vv.MotorKill(1);
        }
    }
}

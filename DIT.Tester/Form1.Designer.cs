﻿namespace DIT.Tester
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnJogPlus = new System.Windows.Forms.Button();
            this.btnbtnJogMinus = new System.Windows.Forms.Button();
            this.btnJogStop = new System.Windows.Forms.Button();
            this.btnPtp = new System.Windows.Forms.Button();
            this.nudPosition = new System.Windows.Forms.NumericUpDown();
            this.btnPtpSpeed = new System.Windows.Forms.Button();
            this.nudSpeed = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.btnKill = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(147, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 88);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnJogPlus
            // 
            this.btnJogPlus.Location = new System.Drawing.Point(147, 150);
            this.btnJogPlus.Name = "btnJogPlus";
            this.btnJogPlus.Size = new System.Drawing.Size(85, 58);
            this.btnJogPlus.TabIndex = 0;
            this.btnJogPlus.Text = "Jog +";
            this.btnJogPlus.UseVisualStyleBackColor = true;
            this.btnJogPlus.Click += new System.EventHandler(this.btnJogPlus_Click);
            // 
            // btnbtnJogMinus
            // 
            this.btnbtnJogMinus.Location = new System.Drawing.Point(339, 150);
            this.btnbtnJogMinus.Name = "btnbtnJogMinus";
            this.btnbtnJogMinus.Size = new System.Drawing.Size(85, 58);
            this.btnbtnJogMinus.TabIndex = 0;
            this.btnbtnJogMinus.Text = "Jog -";
            this.btnbtnJogMinus.UseVisualStyleBackColor = true;
            this.btnbtnJogMinus.Click += new System.EventHandler(this.btnbtnJogMinus_Click);
            // 
            // btnJogStop
            // 
            this.btnJogStop.Location = new System.Drawing.Point(248, 150);
            this.btnJogStop.Name = "btnJogStop";
            this.btnJogStop.Size = new System.Drawing.Size(85, 58);
            this.btnJogStop.TabIndex = 0;
            this.btnJogStop.Text = "Jog /";
            this.btnJogStop.UseVisualStyleBackColor = true;
            this.btnJogStop.Click += new System.EventHandler(this.btnJogStop_Click);
            // 
            // btnPtp
            // 
            this.btnPtp.Location = new System.Drawing.Point(147, 236);
            this.btnPtp.Name = "btnPtp";
            this.btnPtp.Size = new System.Drawing.Size(85, 58);
            this.btnPtp.TabIndex = 0;
            this.btnPtp.Text = "PTP";
            this.btnPtp.UseVisualStyleBackColor = true;
            this.btnPtp.Click += new System.EventHandler(this.btnPtp_Click);
            // 
            // nudPosition
            // 
            this.nudPosition.Location = new System.Drawing.Point(312, 236);
            this.nudPosition.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudPosition.Name = "nudPosition";
            this.nudPosition.Size = new System.Drawing.Size(112, 21);
            this.nudPosition.TabIndex = 1;
            // 
            // btnPtpSpeed
            // 
            this.btnPtpSpeed.Location = new System.Drawing.Point(147, 315);
            this.btnPtpSpeed.Name = "btnPtpSpeed";
            this.btnPtpSpeed.Size = new System.Drawing.Size(85, 58);
            this.btnPtpSpeed.TabIndex = 0;
            this.btnPtpSpeed.Text = "PTPSpeed";
            this.btnPtpSpeed.UseVisualStyleBackColor = true;
            this.btnPtpSpeed.Click += new System.EventHandler(this.btnPtpSpeed_Click);
            // 
            // nudSpeed
            // 
            this.nudSpeed.Location = new System.Drawing.Point(312, 273);
            this.nudSpeed.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nudSpeed.Name = "nudSpeed";
            this.nudSpeed.Size = new System.Drawing.Size(112, 21);
            this.nudSpeed.TabIndex = 1;
            this.nudSpeed.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(147, 379);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 58);
            this.button2.TabIndex = 0;
            this.button2.Text = "StepMove";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnKill
            // 
            this.btnKill.Location = new System.Drawing.Point(444, 150);
            this.btnKill.Name = "btnKill";
            this.btnKill.Size = new System.Drawing.Size(85, 58);
            this.btnKill.TabIndex = 0;
            this.btnKill.Text = "Kill";
            this.btnKill.UseVisualStyleBackColor = true;
            this.btnKill.Click += new System.EventHandler(this.btnKill_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.nudSpeed);
            this.Controls.Add(this.nudPosition);
            this.Controls.Add(this.btnJogStop);
            this.Controls.Add(this.btnKill);
            this.Controls.Add(this.btnbtnJogMinus);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnPtpSpeed);
            this.Controls.Add(this.btnPtp);
            this.Controls.Add(this.btnJogPlus);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.nudPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnJogPlus;
        private System.Windows.Forms.Button btnbtnJogMinus;
        private System.Windows.Forms.Button btnJogStop;
        private System.Windows.Forms.Button btnPtp;
        private System.Windows.Forms.NumericUpDown nudPosition;
        private System.Windows.Forms.Button btnPtpSpeed;
        private System.Windows.Forms.NumericUpDown nudSpeed;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnKill;
    }
}


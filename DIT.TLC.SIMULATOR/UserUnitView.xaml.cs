﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DIT.TLC.SIMULATOR
{
    /// <summary>
    /// UserUnitView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class UserUnitView : UserControl
    {
        private DispatcherTimer _tmrUpdate = new DispatcherTimer();

        public bool IsFirstX { get; set; }


        public UserUnitView()
        {
            InitializeComponent();

            _tmrUpdate.Tick += TmrUpdate_Tick;
            _tmrUpdate.Interval = new TimeSpan(10);
            _tmrUpdate.Start();

            X = 0;
            Y = 0;

            IsFirstX = false;
        }

        private void TmrUpdate_Tick(object sender, EventArgs e)
        {
            double left = XAxis.Margin.Left;
            double top = XAxis.Margin.Top;

            if (XMin != 0 || XMax != 0)
                left = this.Width * (X - XMin) / (XMax - XMin) + Transfer.Width / 2;

            if (YMin != 0 || YMax != 0)
                top = this.Height * (Y - YMin) / (YMax - YMin) + Transfer.Height / 2;

            if (ZMin != 0 || ZMax != 0)
            {
            }

            if (IsFirstX == true)
            {
                YAxis.Margin = new Thickness(left, YAxis.Margin.Top, 0, 0);
            }
            else
            {
                XAxis.Margin = new Thickness(XAxis.Margin.Left, top, 0, 0);
            }

            Transfer.Margin = new Thickness(left, top, 0, 0);
        }

        public string UnitName { get; set; }

        public double X { get; set; }
        public double XMin { get; set; }
        public double XMax { get; set; }

        public double Y { get; set; }
        public double YMin { get; set; }
        public double YMax { get; set; }


        public double Z { get; set; }
        public double ZMin { get; set; }
        public double ZMax { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Timers;
using Dit.Framework.Log;
using Dit.Framework.PLC;

namespace DIT.TLC.SIMULATOR
{
    public class CylinderSimul
    {
        private Timer _tmrF = new Timer();
        private Timer _tmrB = new Timer();
        public string Name { get; set; }
        private DateTime _moveStartTime = DateTime.Now;


        public int TotalLength { get; set; }
        public int CurrPosition { get; set; }

        public PlcAddr YB_ForwardCmd { get; set; }
        public PlcAddr YB_BackwardCmd { get; set; }

        public PlcAddr XB_ForwardComplete { get; set; }
        public PlcAddr XB_BackwardComplete { get; set; }


        public double Position
        {
            get
            {
                return CurrPosition;
            }
        }
        public void Initialize()
        {
        }
        public CylinderSimul()
        {
        }
        public void LogicWorking()
        {
            Working();
        }
        private void Working()
        {
            if (YB_ForwardCmd.vBit)
            {
                int tmpCurrPosition = CurrPosition + 10;
                if (tmpCurrPosition >= TotalLength)
                {
                    tmpCurrPosition = TotalLength;
                    XB_ForwardComplete.vBit = true;
                }
                else
                {
                    XB_ForwardComplete.vBit = false;
                    XB_BackwardComplete.vBit = false;
                }
                CurrPosition = tmpCurrPosition;
            }

            if (YB_BackwardCmd.vBit)
            {
                int tmpCurrPosition = CurrPosition - 10;
                if (tmpCurrPosition <= 0)
                {
                    tmpCurrPosition = 0;
                    XB_BackwardComplete.vBit = true;
                }
                else
                {
                    XB_ForwardComplete.vBit = false;
                    XB_BackwardComplete.vBit = false;
                }
                CurrPosition = tmpCurrPosition;
            }

        }
    }
}

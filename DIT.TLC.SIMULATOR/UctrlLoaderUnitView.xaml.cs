﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DIT.TLC.SIMULATOR
{
    /// <summary>
    /// UserUnitView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class UctrlLoaderUnitView : UserControl
    {
        private DispatcherTimer _tmrUpdate = new DispatcherTimer();

        public bool IsFirstX { get; set; }


        public UctrlLoaderUnitView()
        {
            InitializeComponent();

            _tmrUpdate.Tick += TmrUpdate_Tick;
            _tmrUpdate.Interval = new TimeSpan(10);
            _tmrUpdate.Start();

            X1 = 0;
            X2 = 0;
            Y = 0;

            IsFirstX = false;
        }

        private void TmrUpdate_Tick(object sender, EventArgs e)
        {
            double left1 = XAxis.Margin.Left;
            double top1 = XAxis.Margin.Top;

            if (X1Min != 0 || X1Max != 0)
                left1 = this.Width * (X1 - X1Min) / (X1Max - X1Min) + Transfer1.Width / 2;

            if (YMin != 0 || YMax != 0)
                top1 = this.Height * (Y - YMin) / (YMax - YMin) + Transfer1.Height / 2;

           

            if (IsFirstX == true)
            {
                YAxis1.Margin = new Thickness(left1, YAxis1.Margin.Top, 0, 0);
            }
            else
            {
                XAxis.Margin = new Thickness(XAxis.Margin.Left, top1, 0, 0);
            }

            Transfer1.Margin = new Thickness(left1, top1, 0, 0);
            


            double left2 = XAxis.Margin.Left;
            double top2 = XAxis.Margin.Top;

            if (X2Min != 0 || X2Max != 0)
                left2 = this.Width * (X2 - X1Min) / (X2Max - X2Min) + Transfer2.Width / 2;

           

            if (IsFirstX == true)
            {
                YAxis2.Margin = new Thickness(left2, YAxis2.Margin.Top, 0, 0);
            }
            else
            {
                XAxis.Margin = new Thickness(XAxis.Margin.Left, top1, 0, 0);
            }

            Transfer2.Margin = new Thickness(left1, top1, 0, 0);
        }

        public string UnitName { get; set; }

        public double X1 { get; set; }
        public double X1Min { get; set; }
        public double X1Max { get; set; }

        public double Y { get; set; }
        public double YMin { get; set; }
        public double YMax { get; set; }
         


        public double X2 { get; set; }
        public double X2Min { get; set; }
        public double X2Max { get; set; }
         

    }
}

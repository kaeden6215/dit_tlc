﻿namespace DIT.TLC.SIMULATOR
{
    partial class FrmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpnlServo = new System.Windows.Forms.FlowLayoutPanel();
            this.tmrWorker = new System.Windows.Forms.Timer(this.components);
            this.lstLogger = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // flpnlServo
            // 
            this.flpnlServo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpnlServo.AutoScroll = true;
            this.flpnlServo.Location = new System.Drawing.Point(303, 2);
            this.flpnlServo.Name = "flpnlServo";
            this.flpnlServo.Size = new System.Drawing.Size(1319, 817);
            this.flpnlServo.TabIndex = 0;
            // 
            // tmrWorker
            // 
            this.tmrWorker.Enabled = true;
            this.tmrWorker.Tick += new System.EventHandler(this.tmrWorker_Tick);
            // 
            // lstLogger
            // 
            this.lstLogger.Location = new System.Drawing.Point(12, 530);
            this.lstLogger.Name = "lstLogger";
            this.lstLogger.Size = new System.Drawing.Size(450, 213);
            this.lstLogger.TabIndex = 97;
            this.lstLogger.UseCompatibleStateImageBehavior = false;
            this.lstLogger.View = System.Windows.Forms.View.Details;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1627, 823);
            this.Controls.Add(this.lstLogger);
            this.Controls.Add(this.flpnlServo);
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpnlServo;
        private System.Windows.Forms.Timer tmrWorker;
        private System.Windows.Forms.ListView lstLogger;
    }
}


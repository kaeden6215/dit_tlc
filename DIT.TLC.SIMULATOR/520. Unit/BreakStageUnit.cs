﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{
    public class BreakStageUnit : BaseUnit
    {
        public ServoSimulUmac YAxis { get; set; }
        
        public ServoSimulUmac XAxisHeader { get; set; }
        public ServoSimulUmac ZAxisHeader { get; set; }
        
        public SwitchSimul Vaccum { get; set; }

        public override void LogicWorking(EquipSimul equip)
        {
            YAxis.LogicWorking(equip);
            XAxisHeader.LogicWorking(equip);
            ZAxisHeader.LogicWorking(equip);
        }
    }
}

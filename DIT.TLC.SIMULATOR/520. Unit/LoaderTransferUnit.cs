﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{    
    public class LoaderTransferUnit : BaseUnit
    {
        public ServoSimulUmac YAxis { get; set; }
        public ServoSimulUmac XAxis { get; set; }

        public SwitchSimul Vaccum { get; set; }
        public SwitchSimul Blower { get; set; }
        public CylinderSimul ZAxis { get; set; } //PIKER

        public LoaderTransferUnit()
        {
            ZAxis = new CylinderSimul();
        }

        public override void LogicWorking(EquipSimul equip)
        {
            YAxis.LogicWorking(equip);            
            XAxis.LogicWorking(equip);
        }
    }
}

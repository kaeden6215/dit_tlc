﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{
    public class UnLoaderTransferUnit : BaseUnit
    {
        public ServoSimulUmac YAxis { get; set; }
        public ServoSimulUmac XAxis { get; set; }

        public CylinderSimul ZAxis { get; set; } //PIKER
        public SwitchSimul Vaccum { get; set; }
        public SwitchSimul Blower { get; set; }

        public UnLoaderTransferUnit()
        {
            ZAxis = new CylinderSimul();
        }        
        public override void LogicWorking(EquipSimul equip)
        {
            YAxis.LogicWorking(equip);
            XAxis.LogicWorking(equip);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.SIMULATOR
{
    public partial class UcrlServo : UserControl
    {
        private ServoSimulUmac _servo = null;

        private CheckBox[] _chkP = null;
        private CheckBox[] _chkEndP = null;
        private Label[] _lblPosi = null;
        public UcrlServo()
        {
            InitializeComponent();
            Initialize();
        }
        public void Initialize()
        {
            _chkP = new CheckBox[15] { chkP1, chkP2, chkP3, chkP4, chkP5, chkP6, chkP7, chkP8, chkP9, chkP10, chkP11, chkP12, chkP13, chkP14, chkP15 };
            _chkEndP = new CheckBox[15] { chkEndP1, chkEndP2, chkEndP3, chkEndP4, chkEndP5, chkEndP6, chkEndP7, chkEndP8, chkEndP9, chkEndP10, chkEndP11, chkEndP12, chkEndP13, chkEndP14, chkEndP15 };
            _lblPosi = new Label[7] { lblInspX1, lblInspX2, lblInspX3, lblInspX4, lblInspX5, lblInspX6, lblInspX7 };

            for (int iPos = 0; iPos < 15; iPos++)
            {
                _chkP[iPos].Visible = false;
                _chkEndP[iPos].Visible = false;
            }

        }
        public void Update()
        {
            if (_servo == null) return;

            lblIXPosition.Text = _servo.XF_CurrMotorPosition.vFloat.ToString();
            chkInHome.BackColor = _servo.YB_HomeCmd ? Color.Red : Color.Gray;
            chkOutHomeEnd.BackColor = _servo.XB_StatusHomeInPosition ? Color.Red : Color.Gray;
            chkOutMoveGo.BackColor = _servo.XB_StatusMotorMoving ? Color.Red : Color.Gray;

            for (int iPos = 0; iPos < _servo.PositionCount; iPos++)
            {
                _chkP[iPos].Visible = true;
                _chkEndP[iPos].Visible = true;

                _chkP[iPos].BackColor = _servo.YB_Position0MoveCmd[iPos].vInt == 1 ? Color.Red : Color.Gray;
                _chkEndP[iPos].BackColor = _servo.XB_Position0Complete[iPos].vInt == 1 ? Color.Red : Color.Gray;

                if (iPos < 7)
                    _lblPosi[iPos].Text = _servo.YF_Position0Posi[iPos].vFloat.ToString();
            }
        }

        public string Tiltel
        {
            set
            { lblTitle.Text = value; }
        }
        public ServoSimulUmac Servo
        {
            set
            {
                _servo = value;
                Tiltel = value.Name;

            }
        }
    }
}

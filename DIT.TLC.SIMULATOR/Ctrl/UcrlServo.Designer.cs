﻿namespace DIT.TLC.SIMULATOR
{
    partial class UcrlServo
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblIXPosition = new System.Windows.Forms.Label();
            this.lblIXSpeed = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.chkInHome = new System.Windows.Forms.CheckBox();
            this.btnInspXMoveJogPlus = new Dit.Framework.UI.ButtonDelay();
            this.btnInspXMoveJogMinus = new Dit.Framework.UI.ButtonDelay();
            this.chkOutHomeEnd = new System.Windows.Forms.CheckBox();
            this.chkOutMoveGo = new System.Windows.Forms.CheckBox();
            this.chkInspXReply = new System.Windows.Forms.CheckBox();
            this.chkP10 = new System.Windows.Forms.CheckBox();
            this.chkP11 = new System.Windows.Forms.CheckBox();
            this.chkEndP10 = new System.Windows.Forms.CheckBox();
            this.chkEndP11 = new System.Windows.Forms.CheckBox();
            this.lblInspX1 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.lblInspX2 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.lblInspX3 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.lblInspX4 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.lblInspX5 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.lblInspX6 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.lblInspX7 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.chkEndP14 = new System.Windows.Forms.CheckBox();
            this.chkEndP13 = new System.Windows.Forms.CheckBox();
            this.chkEndP12 = new System.Windows.Forms.CheckBox();
            this.chkEndP9 = new System.Windows.Forms.CheckBox();
            this.chkEndP8 = new System.Windows.Forms.CheckBox();
            this.chkEndP7 = new System.Windows.Forms.CheckBox();
            this.chkEndP5 = new System.Windows.Forms.CheckBox();
            this.chkEndP6 = new System.Windows.Forms.CheckBox();
            this.chkEndP4 = new System.Windows.Forms.CheckBox();
            this.chkEndP3 = new System.Windows.Forms.CheckBox();
            this.chkEndP2 = new System.Windows.Forms.CheckBox();
            this.chkEndP1 = new System.Windows.Forms.CheckBox();
            this.chkP14 = new System.Windows.Forms.CheckBox();
            this.chkP13 = new System.Windows.Forms.CheckBox();
            this.chkP12 = new System.Windows.Forms.CheckBox();
            this.chkP9 = new System.Windows.Forms.CheckBox();
            this.chkP8 = new System.Windows.Forms.CheckBox();
            this.chkP7 = new System.Windows.Forms.CheckBox();
            this.chkP5 = new System.Windows.Forms.CheckBox();
            this.chkP6 = new System.Windows.Forms.CheckBox();
            this.chkP4 = new System.Windows.Forms.CheckBox();
            this.chkP3 = new System.Windows.Forms.CheckBox();
            this.chkP2 = new System.Windows.Forms.CheckBox();
            this.chkP1 = new System.Windows.Forms.CheckBox();
            this.chkP15 = new System.Windows.Forms.CheckBox();
            this.chkEndP15 = new System.Windows.Forms.CheckBox();
            this.panel16.SuspendLayout();
            this.SuspendLayout();
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Location = new System.Drawing.Point(98, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 19);
            this.label20.TabIndex = 4;
            this.label20.Text = "Speed\'";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label21.Location = new System.Drawing.Point(98, 29);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 19);
            this.label21.TabIndex = 4;
            this.label21.Text = "Pos\'";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIXPosition
            // 
            this.lblIXPosition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIXPosition.Location = new System.Drawing.Point(147, 29);
            this.lblIXPosition.Name = "lblIXPosition";
            this.lblIXPosition.Size = new System.Drawing.Size(45, 19);
            this.lblIXPosition.TabIndex = 2;
            this.lblIXPosition.Text = "0.0";
            this.lblIXPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIXSpeed
            // 
            this.lblIXSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIXSpeed.Location = new System.Drawing.Point(147, 50);
            this.lblIXSpeed.Name = "lblIXSpeed";
            this.lblIXSpeed.Size = new System.Drawing.Size(45, 19);
            this.lblIXSpeed.TabIndex = 3;
            this.lblIXSpeed.Text = "0.0";
            this.lblIXSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoEllipsis = true;
            this.lblTitle.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(511, 24);
            this.lblTitle.TabIndex = 9;
            this.lblTitle.Text = "■ IX";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkInHome
            // 
            this.chkInHome.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkInHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkInHome.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkInHome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkInHome.Location = new System.Drawing.Point(2, 28);
            this.chkInHome.Name = "chkInHome";
            this.chkInHome.Size = new System.Drawing.Size(47, 40);
            this.chkInHome.TabIndex = 11;
            this.chkInHome.Text = "Home";
            this.chkInHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkInHome.UseVisualStyleBackColor = false;
            // 
            // btnInspXMoveJogPlus
            // 
            this.btnInspXMoveJogPlus.BackColor = System.Drawing.Color.Transparent;
            this.btnInspXMoveJogPlus.Delay = 2;
            this.btnInspXMoveJogPlus.Flicker = false;
            this.btnInspXMoveJogPlus.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspXMoveJogPlus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnInspXMoveJogPlus.Location = new System.Drawing.Point(372, 28);
            this.btnInspXMoveJogPlus.Name = "btnInspXMoveJogPlus";
            this.btnInspXMoveJogPlus.OnOff = false;
            this.btnInspXMoveJogPlus.Size = new System.Drawing.Size(52, 40);
            this.btnInspXMoveJogPlus.TabIndex = 10;
            this.btnInspXMoveJogPlus.TabStop = false;
            this.btnInspXMoveJogPlus.Text = "▶▶(+)";
            this.btnInspXMoveJogPlus.Text2 = "";
            this.btnInspXMoveJogPlus.UseVisualStyleBackColor = false;
            // 
            // btnInspXMoveJogMinus
            // 
            this.btnInspXMoveJogMinus.BackColor = System.Drawing.Color.Transparent;
            this.btnInspXMoveJogMinus.Delay = 2;
            this.btnInspXMoveJogMinus.Flicker = false;
            this.btnInspXMoveJogMinus.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspXMoveJogMinus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnInspXMoveJogMinus.Location = new System.Drawing.Point(317, 28);
            this.btnInspXMoveJogMinus.Name = "btnInspXMoveJogMinus";
            this.btnInspXMoveJogMinus.OnOff = false;
            this.btnInspXMoveJogMinus.Size = new System.Drawing.Size(52, 40);
            this.btnInspXMoveJogMinus.TabIndex = 10;
            this.btnInspXMoveJogMinus.TabStop = false;
            this.btnInspXMoveJogMinus.Text = "(-)◀◀";
            this.btnInspXMoveJogMinus.Text2 = "";
            this.btnInspXMoveJogMinus.UseVisualStyleBackColor = false;
            // 
            // chkOutHomeEnd
            // 
            this.chkOutHomeEnd.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkOutHomeEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkOutHomeEnd.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkOutHomeEnd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkOutHomeEnd.Location = new System.Drawing.Point(49, 28);
            this.chkOutHomeEnd.Name = "chkOutHomeEnd";
            this.chkOutHomeEnd.Size = new System.Drawing.Size(41, 40);
            this.chkOutHomeEnd.TabIndex = 12;
            this.chkOutHomeEnd.Text = "H Bit";
            this.chkOutHomeEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkOutHomeEnd.UseVisualStyleBackColor = false;
            // 
            // chkOutMoveGo
            // 
            this.chkOutMoveGo.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkOutMoveGo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkOutMoveGo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkOutMoveGo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkOutMoveGo.Location = new System.Drawing.Point(199, 28);
            this.chkOutMoveGo.Name = "chkOutMoveGo";
            this.chkOutMoveGo.Size = new System.Drawing.Size(57, 43);
            this.chkOutMoveGo.TabIndex = 12;
            this.chkOutMoveGo.Text = "Move \r\nGo";
            this.chkOutMoveGo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkOutMoveGo.UseVisualStyleBackColor = false;
            // 
            // chkInspXReply
            // 
            this.chkInspXReply.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkInspXReply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkInspXReply.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkInspXReply.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkInspXReply.Location = new System.Drawing.Point(258, 27);
            this.chkInspXReply.Name = "chkInspXReply";
            this.chkInspXReply.Size = new System.Drawing.Size(57, 43);
            this.chkInspXReply.TabIndex = 12;
            this.chkInspXReply.Text = "Not Replay";
            this.chkInspXReply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkInspXReply.UseVisualStyleBackColor = false;
            // 
            // chkP10
            // 
            this.chkP10.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP10.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP10.Location = new System.Drawing.Point(250, 72);
            this.chkP10.Name = "chkP10";
            this.chkP10.Size = new System.Drawing.Size(33, 40);
            this.chkP10.TabIndex = 13;
            this.chkP10.Text = "P10";
            this.chkP10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP10.UseVisualStyleBackColor = false;
            // 
            // chkP11
            // 
            this.chkP11.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP11.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP11.Location = new System.Drawing.Point(282, 72);
            this.chkP11.Name = "chkP11";
            this.chkP11.Size = new System.Drawing.Size(33, 40);
            this.chkP11.TabIndex = 13;
            this.chkP11.Text = "P11";
            this.chkP11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP11.UseVisualStyleBackColor = false;
            // 
            // chkEndP10
            // 
            this.chkEndP10.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP10.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP10.Location = new System.Drawing.Point(250, 111);
            this.chkEndP10.Name = "chkEndP10";
            this.chkEndP10.Size = new System.Drawing.Size(33, 40);
            this.chkEndP10.TabIndex = 22;
            this.chkEndP10.Text = "P10";
            this.chkEndP10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP10.UseVisualStyleBackColor = false;
            // 
            // chkEndP11
            // 
            this.chkEndP11.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP11.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP11.Location = new System.Drawing.Point(282, 111);
            this.chkEndP11.Name = "chkEndP11";
            this.chkEndP11.Size = new System.Drawing.Size(33, 40);
            this.chkEndP11.TabIndex = 22;
            this.chkEndP11.Text = "P11";
            this.chkEndP11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP11.UseVisualStyleBackColor = false;
            // 
            // lblInspX1
            // 
            this.lblInspX1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInspX1.Location = new System.Drawing.Point(462, 13);
            this.lblInspX1.Name = "lblInspX1";
            this.lblInspX1.Size = new System.Drawing.Size(45, 19);
            this.lblInspX1.TabIndex = 47;
            this.lblInspX1.Text = "0.0";
            this.lblInspX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label77.Location = new System.Drawing.Point(433, 13);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(28, 19);
            this.label77.TabIndex = 48;
            this.label77.Text = "1P";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspX2
            // 
            this.lblInspX2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInspX2.Location = new System.Drawing.Point(462, 34);
            this.lblInspX2.Name = "lblInspX2";
            this.lblInspX2.Size = new System.Drawing.Size(45, 19);
            this.lblInspX2.TabIndex = 49;
            this.lblInspX2.Text = "0.0";
            this.lblInspX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label76
            // 
            this.label76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label76.Location = new System.Drawing.Point(433, 34);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(28, 19);
            this.label76.TabIndex = 50;
            this.label76.Text = "2P";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspX3
            // 
            this.lblInspX3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInspX3.Location = new System.Drawing.Point(462, 54);
            this.lblInspX3.Name = "lblInspX3";
            this.lblInspX3.Size = new System.Drawing.Size(45, 19);
            this.lblInspX3.TabIndex = 51;
            this.lblInspX3.Text = "0.0";
            this.lblInspX3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label75
            // 
            this.label75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label75.Location = new System.Drawing.Point(433, 54);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(28, 19);
            this.label75.TabIndex = 52;
            this.label75.Text = "3P";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspX4
            // 
            this.lblInspX4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInspX4.Location = new System.Drawing.Point(462, 75);
            this.lblInspX4.Name = "lblInspX4";
            this.lblInspX4.Size = new System.Drawing.Size(45, 19);
            this.lblInspX4.TabIndex = 53;
            this.lblInspX4.Text = "0.0";
            this.lblInspX4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label74
            // 
            this.label74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label74.Location = new System.Drawing.Point(433, 75);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(28, 19);
            this.label74.TabIndex = 54;
            this.label74.Text = "4P";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspX5
            // 
            this.lblInspX5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInspX5.Location = new System.Drawing.Point(462, 95);
            this.lblInspX5.Name = "lblInspX5";
            this.lblInspX5.Size = new System.Drawing.Size(45, 19);
            this.lblInspX5.TabIndex = 55;
            this.lblInspX5.Text = "0.0";
            this.lblInspX5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label73
            // 
            this.label73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label73.Location = new System.Drawing.Point(433, 95);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(28, 19);
            this.label73.TabIndex = 56;
            this.label73.Text = "5P";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspX6
            // 
            this.lblInspX6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInspX6.Location = new System.Drawing.Point(462, 116);
            this.lblInspX6.Name = "lblInspX6";
            this.lblInspX6.Size = new System.Drawing.Size(45, 19);
            this.lblInspX6.TabIndex = 57;
            this.lblInspX6.Text = "0.0";
            this.lblInspX6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label72
            // 
            this.label72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label72.Location = new System.Drawing.Point(433, 116);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(28, 19);
            this.label72.TabIndex = 58;
            this.label72.Text = "6P";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspX7
            // 
            this.lblInspX7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInspX7.Location = new System.Drawing.Point(462, 136);
            this.lblInspX7.Name = "lblInspX7";
            this.lblInspX7.Size = new System.Drawing.Size(45, 19);
            this.lblInspX7.TabIndex = 59;
            this.lblInspX7.Text = "0.0";
            this.lblInspX7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label71.Location = new System.Drawing.Point(433, 136);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(28, 19);
            this.label71.TabIndex = 60;
            this.label71.Text = "7P";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.label71);
            this.panel16.Controls.Add(this.lblInspX7);
            this.panel16.Controls.Add(this.label72);
            this.panel16.Controls.Add(this.lblInspX6);
            this.panel16.Controls.Add(this.label73);
            this.panel16.Controls.Add(this.lblInspX5);
            this.panel16.Controls.Add(this.label74);
            this.panel16.Controls.Add(this.lblInspX4);
            this.panel16.Controls.Add(this.label75);
            this.panel16.Controls.Add(this.lblInspX3);
            this.panel16.Controls.Add(this.label76);
            this.panel16.Controls.Add(this.lblInspX2);
            this.panel16.Controls.Add(this.label77);
            this.panel16.Controls.Add(this.lblInspX1);
            this.panel16.Controls.Add(this.chkEndP15);
            this.panel16.Controls.Add(this.chkEndP14);
            this.panel16.Controls.Add(this.chkEndP13);
            this.panel16.Controls.Add(this.chkEndP12);
            this.panel16.Controls.Add(this.chkEndP11);
            this.panel16.Controls.Add(this.chkEndP10);
            this.panel16.Controls.Add(this.chkEndP9);
            this.panel16.Controls.Add(this.chkEndP8);
            this.panel16.Controls.Add(this.chkEndP7);
            this.panel16.Controls.Add(this.chkEndP5);
            this.panel16.Controls.Add(this.chkEndP6);
            this.panel16.Controls.Add(this.chkEndP4);
            this.panel16.Controls.Add(this.chkEndP3);
            this.panel16.Controls.Add(this.chkEndP2);
            this.panel16.Controls.Add(this.chkEndP1);
            this.panel16.Controls.Add(this.chkP15);
            this.panel16.Controls.Add(this.chkP14);
            this.panel16.Controls.Add(this.chkP13);
            this.panel16.Controls.Add(this.chkP12);
            this.panel16.Controls.Add(this.chkP11);
            this.panel16.Controls.Add(this.chkP10);
            this.panel16.Controls.Add(this.chkP9);
            this.panel16.Controls.Add(this.chkP8);
            this.panel16.Controls.Add(this.chkP7);
            this.panel16.Controls.Add(this.chkP5);
            this.panel16.Controls.Add(this.chkP6);
            this.panel16.Controls.Add(this.chkP4);
            this.panel16.Controls.Add(this.chkP3);
            this.panel16.Controls.Add(this.chkP2);
            this.panel16.Controls.Add(this.chkP1);
            this.panel16.Controls.Add(this.chkInspXReply);
            this.panel16.Controls.Add(this.chkOutMoveGo);
            this.panel16.Controls.Add(this.chkOutHomeEnd);
            this.panel16.Controls.Add(this.btnInspXMoveJogMinus);
            this.panel16.Controls.Add(this.btnInspXMoveJogPlus);
            this.panel16.Controls.Add(this.chkInHome);
            this.panel16.Controls.Add(this.lblTitle);
            this.panel16.Controls.Add(this.lblIXSpeed);
            this.panel16.Controls.Add(this.lblIXPosition);
            this.panel16.Controls.Add(this.label21);
            this.panel16.Controls.Add(this.label20);
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(513, 161);
            this.panel16.TabIndex = 90;
            // 
            // chkEndP14
            // 
            this.chkEndP14.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP14.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP14.Location = new System.Drawing.Point(382, 111);
            this.chkEndP14.Name = "chkEndP14";
            this.chkEndP14.Size = new System.Drawing.Size(33, 40);
            this.chkEndP14.TabIndex = 22;
            this.chkEndP14.Text = "P14";
            this.chkEndP14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP14.UseVisualStyleBackColor = false;
            // 
            // chkEndP13
            // 
            this.chkEndP13.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP13.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP13.Location = new System.Drawing.Point(348, 111);
            this.chkEndP13.Name = "chkEndP13";
            this.chkEndP13.Size = new System.Drawing.Size(33, 40);
            this.chkEndP13.TabIndex = 22;
            this.chkEndP13.Text = "P13";
            this.chkEndP13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP13.UseVisualStyleBackColor = false;
            // 
            // chkEndP12
            // 
            this.chkEndP12.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP12.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP12.Location = new System.Drawing.Point(315, 111);
            this.chkEndP12.Name = "chkEndP12";
            this.chkEndP12.Size = new System.Drawing.Size(33, 40);
            this.chkEndP12.TabIndex = 22;
            this.chkEndP12.Text = "P12";
            this.chkEndP12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP12.UseVisualStyleBackColor = false;
            // 
            // chkEndP9
            // 
            this.chkEndP9.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP9.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP9.Location = new System.Drawing.Point(223, 111);
            this.chkEndP9.Name = "chkEndP9";
            this.chkEndP9.Size = new System.Drawing.Size(28, 40);
            this.chkEndP9.TabIndex = 22;
            this.chkEndP9.Text = "P9";
            this.chkEndP9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP9.UseVisualStyleBackColor = false;
            // 
            // chkEndP8
            // 
            this.chkEndP8.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP8.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP8.Location = new System.Drawing.Point(196, 111);
            this.chkEndP8.Name = "chkEndP8";
            this.chkEndP8.Size = new System.Drawing.Size(28, 40);
            this.chkEndP8.TabIndex = 22;
            this.chkEndP8.Text = "P8";
            this.chkEndP8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP8.UseVisualStyleBackColor = false;
            // 
            // chkEndP7
            // 
            this.chkEndP7.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP7.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP7.Location = new System.Drawing.Point(169, 111);
            this.chkEndP7.Name = "chkEndP7";
            this.chkEndP7.Size = new System.Drawing.Size(28, 40);
            this.chkEndP7.TabIndex = 22;
            this.chkEndP7.Text = "P7";
            this.chkEndP7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP7.UseVisualStyleBackColor = false;
            // 
            // chkEndP5
            // 
            this.chkEndP5.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP5.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP5.Location = new System.Drawing.Point(116, 111);
            this.chkEndP5.Name = "chkEndP5";
            this.chkEndP5.Size = new System.Drawing.Size(28, 40);
            this.chkEndP5.TabIndex = 22;
            this.chkEndP5.Text = "P5";
            this.chkEndP5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP5.UseVisualStyleBackColor = false;
            // 
            // chkEndP6
            // 
            this.chkEndP6.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP6.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP6.Location = new System.Drawing.Point(142, 111);
            this.chkEndP6.Name = "chkEndP6";
            this.chkEndP6.Size = new System.Drawing.Size(28, 40);
            this.chkEndP6.TabIndex = 23;
            this.chkEndP6.Text = "P6";
            this.chkEndP6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP6.UseVisualStyleBackColor = false;
            // 
            // chkEndP4
            // 
            this.chkEndP4.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP4.Location = new System.Drawing.Point(86, 111);
            this.chkEndP4.Name = "chkEndP4";
            this.chkEndP4.Size = new System.Drawing.Size(28, 40);
            this.chkEndP4.TabIndex = 23;
            this.chkEndP4.Text = "P4";
            this.chkEndP4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP4.UseVisualStyleBackColor = false;
            // 
            // chkEndP3
            // 
            this.chkEndP3.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP3.Location = new System.Drawing.Point(58, 111);
            this.chkEndP3.Name = "chkEndP3";
            this.chkEndP3.Size = new System.Drawing.Size(28, 40);
            this.chkEndP3.TabIndex = 21;
            this.chkEndP3.Text = "P3";
            this.chkEndP3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP3.UseVisualStyleBackColor = false;
            // 
            // chkEndP2
            // 
            this.chkEndP2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP2.Location = new System.Drawing.Point(30, 111);
            this.chkEndP2.Name = "chkEndP2";
            this.chkEndP2.Size = new System.Drawing.Size(28, 40);
            this.chkEndP2.TabIndex = 19;
            this.chkEndP2.Text = "P2";
            this.chkEndP2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP2.UseVisualStyleBackColor = false;
            // 
            // chkEndP1
            // 
            this.chkEndP1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP1.Location = new System.Drawing.Point(2, 111);
            this.chkEndP1.Name = "chkEndP1";
            this.chkEndP1.Size = new System.Drawing.Size(28, 40);
            this.chkEndP1.TabIndex = 20;
            this.chkEndP1.Text = "P1";
            this.chkEndP1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP1.UseVisualStyleBackColor = false;
            // 
            // chkP14
            // 
            this.chkP14.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP14.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP14.Location = new System.Drawing.Point(382, 72);
            this.chkP14.Name = "chkP14";
            this.chkP14.Size = new System.Drawing.Size(33, 40);
            this.chkP14.TabIndex = 13;
            this.chkP14.Text = "P14";
            this.chkP14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP14.UseVisualStyleBackColor = false;
            // 
            // chkP13
            // 
            this.chkP13.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP13.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP13.Location = new System.Drawing.Point(348, 72);
            this.chkP13.Name = "chkP13";
            this.chkP13.Size = new System.Drawing.Size(33, 40);
            this.chkP13.TabIndex = 13;
            this.chkP13.Text = "P13";
            this.chkP13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP13.UseVisualStyleBackColor = false;
            // 
            // chkP12
            // 
            this.chkP12.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP12.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP12.Location = new System.Drawing.Point(315, 72);
            this.chkP12.Name = "chkP12";
            this.chkP12.Size = new System.Drawing.Size(33, 40);
            this.chkP12.TabIndex = 13;
            this.chkP12.Text = "P12";
            this.chkP12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP12.UseVisualStyleBackColor = false;
            // 
            // chkP9
            // 
            this.chkP9.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP9.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP9.Location = new System.Drawing.Point(223, 72);
            this.chkP9.Name = "chkP9";
            this.chkP9.Size = new System.Drawing.Size(28, 40);
            this.chkP9.TabIndex = 13;
            this.chkP9.Text = "P9";
            this.chkP9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP9.UseVisualStyleBackColor = false;
            // 
            // chkP8
            // 
            this.chkP8.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP8.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP8.Location = new System.Drawing.Point(196, 72);
            this.chkP8.Name = "chkP8";
            this.chkP8.Size = new System.Drawing.Size(28, 40);
            this.chkP8.TabIndex = 13;
            this.chkP8.Text = "P8";
            this.chkP8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP8.UseVisualStyleBackColor = false;
            // 
            // chkP7
            // 
            this.chkP7.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP7.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP7.Location = new System.Drawing.Point(169, 72);
            this.chkP7.Name = "chkP7";
            this.chkP7.Size = new System.Drawing.Size(28, 40);
            this.chkP7.TabIndex = 13;
            this.chkP7.Text = "P7";
            this.chkP7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP7.UseVisualStyleBackColor = false;
            // 
            // chkP5
            // 
            this.chkP5.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP5.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP5.Location = new System.Drawing.Point(115, 72);
            this.chkP5.Name = "chkP5";
            this.chkP5.Size = new System.Drawing.Size(28, 40);
            this.chkP5.TabIndex = 13;
            this.chkP5.Text = "P5";
            this.chkP5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP5.UseVisualStyleBackColor = false;
            // 
            // chkP6
            // 
            this.chkP6.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP6.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP6.Location = new System.Drawing.Point(142, 72);
            this.chkP6.Name = "chkP6";
            this.chkP6.Size = new System.Drawing.Size(28, 40);
            this.chkP6.TabIndex = 13;
            this.chkP6.Text = "P6";
            this.chkP6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP6.UseVisualStyleBackColor = false;
            // 
            // chkP4
            // 
            this.chkP4.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP4.Location = new System.Drawing.Point(86, 72);
            this.chkP4.Name = "chkP4";
            this.chkP4.Size = new System.Drawing.Size(28, 40);
            this.chkP4.TabIndex = 13;
            this.chkP4.Text = "P4";
            this.chkP4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP4.UseVisualStyleBackColor = false;
            // 
            // chkP3
            // 
            this.chkP3.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP3.Location = new System.Drawing.Point(58, 72);
            this.chkP3.Name = "chkP3";
            this.chkP3.Size = new System.Drawing.Size(28, 40);
            this.chkP3.TabIndex = 13;
            this.chkP3.Text = "P3";
            this.chkP3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP3.UseVisualStyleBackColor = false;
            // 
            // chkP2
            // 
            this.chkP2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP2.Location = new System.Drawing.Point(30, 72);
            this.chkP2.Name = "chkP2";
            this.chkP2.Size = new System.Drawing.Size(28, 40);
            this.chkP2.TabIndex = 13;
            this.chkP2.Text = "P2";
            this.chkP2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP2.UseVisualStyleBackColor = false;
            // 
            // chkP1
            // 
            this.chkP1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP1.Location = new System.Drawing.Point(2, 72);
            this.chkP1.Name = "chkP1";
            this.chkP1.Size = new System.Drawing.Size(28, 40);
            this.chkP1.TabIndex = 13;
            this.chkP1.Text = "P1";
            this.chkP1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP1.UseVisualStyleBackColor = false;
            // 
            // chkP15
            // 
            this.chkP15.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkP15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkP15.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkP15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkP15.Location = new System.Drawing.Point(416, 72);
            this.chkP15.Name = "chkP15";
            this.chkP15.Size = new System.Drawing.Size(33, 40);
            this.chkP15.TabIndex = 13;
            this.chkP15.Text = "P14";
            this.chkP15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkP15.UseVisualStyleBackColor = false;
            // 
            // chkEndP15
            // 
            this.chkEndP15.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEndP15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEndP15.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEndP15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEndP15.Location = new System.Drawing.Point(416, 111);
            this.chkEndP15.Name = "chkEndP15";
            this.chkEndP15.Size = new System.Drawing.Size(33, 40);
            this.chkEndP15.TabIndex = 22;
            this.chkEndP15.Text = "P14";
            this.chkEndP15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEndP15.UseVisualStyleBackColor = false;
            // 
            // UcrlServo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.panel16);
            this.Name = "UcrlServo";
            this.Size = new System.Drawing.Size(513, 164);
            this.panel16.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblIXPosition;
        private System.Windows.Forms.Label lblIXSpeed;
        internal System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.CheckBox chkInHome;
        internal Dit.Framework.UI.ButtonDelay btnInspXMoveJogPlus;
        internal Dit.Framework.UI.ButtonDelay btnInspXMoveJogMinus;
        private System.Windows.Forms.CheckBox chkOutHomeEnd;
        private System.Windows.Forms.CheckBox chkOutMoveGo;
        private System.Windows.Forms.CheckBox chkInspXReply;
        private System.Windows.Forms.CheckBox chkP10;
        private System.Windows.Forms.CheckBox chkP11;
        private System.Windows.Forms.CheckBox chkEndP10;
        private System.Windows.Forms.CheckBox chkEndP11;
        private System.Windows.Forms.Label lblInspX1;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label lblInspX2;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label lblInspX3;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label lblInspX4;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label lblInspX5;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label lblInspX6;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label lblInspX7;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.CheckBox chkEndP13;
        private System.Windows.Forms.CheckBox chkEndP12;
        private System.Windows.Forms.CheckBox chkEndP9;
        private System.Windows.Forms.CheckBox chkEndP8;
        private System.Windows.Forms.CheckBox chkEndP7;
        private System.Windows.Forms.CheckBox chkEndP5;
        private System.Windows.Forms.CheckBox chkEndP6;
        private System.Windows.Forms.CheckBox chkEndP4;
        private System.Windows.Forms.CheckBox chkEndP3;
        private System.Windows.Forms.CheckBox chkEndP2;
        private System.Windows.Forms.CheckBox chkEndP1;
        private System.Windows.Forms.CheckBox chkP13;
        private System.Windows.Forms.CheckBox chkP12;
        private System.Windows.Forms.CheckBox chkP9;
        private System.Windows.Forms.CheckBox chkP8;
        private System.Windows.Forms.CheckBox chkP7;
        private System.Windows.Forms.CheckBox chkP5;
        private System.Windows.Forms.CheckBox chkP6;
        private System.Windows.Forms.CheckBox chkP4;
        private System.Windows.Forms.CheckBox chkP3;
        private System.Windows.Forms.CheckBox chkP2;
        private System.Windows.Forms.CheckBox chkP1;
        private System.Windows.Forms.CheckBox chkEndP14;
        private System.Windows.Forms.CheckBox chkP14;
        private System.Windows.Forms.CheckBox chkEndP15;
        private System.Windows.Forms.CheckBox chkP15;

    }
}

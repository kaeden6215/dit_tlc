﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{
    [Serializable]
    public class PanelInfo : ICloneable
    {
        public string PanelNo { get; set; }

        //CIM 에서 사용 
        public DateTime Start = DateTime.Now;
        public DateTime End = DateTime.Now;
        
        public PanelInfo()
        {
            PanelNo = DateTime.Now.ToString("yyyyMMddHHmmssfff");
        }

        public object Clone()
        {
            PanelInfo info = new PanelInfo();

            info.Start                       /**/ = this.Start;
            info.End                         /**/ = this.End;

            return info;
        }
    }
}

﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmStepSwitchMgr : Form
    {
        public FrmStepSwitchMgr()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            btnALine_Click(null, null);
            tmrUiupdate.Start();
        }

        private void dgvSeqStepMgr_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4 && e.RowIndex >= 0)
            {
                BaseUnit unit = dgvSeqStepMgr.Rows[e.RowIndex].Tag as BaseUnit;
                if (unit == null) return;
                string stepName = dgvSeqStepMgr.Rows[e.RowIndex].Cells[1].Value.ToString();

                unit.SetStepSwith(stepName, !unit.GetStepSwith(stepName));
                dgvSeqStepMgr.Rows[e.RowIndex].Cells[2].Style.BackColor = unit.GetStepSwith(stepName) ? Color.Red : Color.Blue;
                dgvSeqStepMgr.Rows[e.RowIndex].Cells[3].Style.BackColor = unit.GetStepSwith(stepName) ? Color.Red : Color.Blue;
            }
        }

        private void tmrUiupdate_Tick(object sender, EventArgs e)
        {
            for (int iRow = 0; iRow < dgvSeqStepMgr.Rows.Count; iRow++)
            {
                BaseUnit unit = dgvSeqStepMgr.Rows[iRow].Tag as BaseUnit;
                if (unit == null) continue;
                dgvSeqStepMgr.Rows[iRow].Cells[2].Value = unit.HomeStepStr;
                dgvSeqStepMgr.Rows[iRow].Cells[3].Value = unit.SeqStepStr;
            }
        }

        private void btnALine_Click(object sender, EventArgs e)
        {
            FillBaseUnit(new List<BaseUnit>() {
                GG.Equip.LD.Loader,
                GG.Equip.LD.LoaderTransfer_A,
                GG.Equip.LD.PreAlign,
                GG.Equip.PROC.FineAlign,
                GG.Equip.PROC.IRCutStage_A,
                GG.Equip.PROC.AfterIRCutTransfer_A,                
                GG.Equip.PROC.BreakStage_A,
                GG.Equip.PROC.BreakAlign,
                GG.Equip.UD.BeforeInspUnloaderTransfer_A,
                GG.Equip.UD.InspectionStage_A,
                GG.Equip.UD.AfterInspUnloaderTransfer_A,
                GG.Equip.UD.Unloader,
                GG.Equip.UD.CstUnloader_A
            });

            btnALine.BackColor = Color.Red;
            btnBLine.BackColor = SystemColors.Control;
        }

        private void btnBLine_Click(object sender, EventArgs e)
        {
            FillBaseUnit(new List<BaseUnit>() {
                GG.Equip.LD.Loader,
                GG.Equip.LD.LoaderTransfer_B,
                GG.Equip.LD.PreAlign,
                GG.Equip.PROC.FineAlign,
                GG.Equip.PROC.IRCutStage_B,
                GG.Equip.PROC.AfterIRCutTransfer_B,
                GG.Equip.PROC.BreakStage_B,
                GG.Equip.PROC.BreakAlign,
                GG.Equip.UD.BeforeInspUnloaderTransfer_B,
                GG.Equip.UD.InspectionStage_B,
                GG.Equip.UD.AfterInspUnloaderTransfer_B,
                GG.Equip.UD.Unloader,
                GG.Equip.UD.CstUnloader_B
            });

            btnALine.BackColor = SystemColors.Control;
            btnBLine.BackColor = Color.Red;
        }

        string _filter = string.Empty;
        public void FillBaseUnit(List<BaseUnit> list)
        {
            dgvSeqStepMgr.Rows.Clear();
            foreach (BaseUnit unit in list)
            {
                foreach (string stepName in unit.DicStepSwitch.Keys)
                {
                    if (stepName.ToUpper().Contains(_filter) || string.IsNullOrEmpty(_filter))
                    {
                        int iRow = dgvSeqStepMgr.Rows.Add(unit.Name, stepName, "", "", "ON/OFF");
                        dgvSeqStepMgr.Rows[iRow].Tag = unit;
                        dgvSeqStepMgr.Rows[iRow].Cells[2].Style.BackColor = unit.GetStepSwith(stepName) ? Color.Red : Color.Blue;
                        dgvSeqStepMgr.Rows[iRow].Cells[3].Style.BackColor = unit.GetStepSwith(stepName) ? Color.Red : Color.Blue;
                    }
                }
            }
        }
        private void btnTopmost_Click(object sender, EventArgs e)
        {
            this.TopMost = !this.TopMost;
            btnTopmost.BackColor = this.TopMost ? Color.Red : Color.Blue;
        }
        private void btnAllSeq_Click(object sender, EventArgs e)
        {
            _filter = string.Empty;
        }
        private void btnHomeSeq_Click(object sender, EventArgs e)
        {
            _filter = "HOME";
        }
    }
}

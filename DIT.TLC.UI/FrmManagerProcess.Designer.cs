﻿namespace DIT.TLC.UI
{
    partial class FrmManager_Process
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmManager_Process));
            this.btnClose = new System.Windows.Forms.Button();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnProcessVision = new System.Windows.Forms.Button();
            this.btnProcessCurrent = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnProcessBProcessR2 = new System.Windows.Forms.Button();
            this.btnProcessBProcessR3 = new System.Windows.Forms.Button();
            this.btnProcessBProcessR1 = new System.Windows.Forms.Button();
            this.lblProcessB3 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnProcessAProcessL2 = new System.Windows.Forms.Button();
            this.btnProcessAProcessL3 = new System.Windows.Forms.Button();
            this.btnProcessAProcessL1 = new System.Windows.Forms.Button();
            this.lblProcessA3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnProcessBAlignR2 = new System.Windows.Forms.Button();
            this.btnProcessBAlignR3 = new System.Windows.Forms.Button();
            this.btnProcessBAlignR1 = new System.Windows.Forms.Button();
            this.lblProcessB2 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnProcessAAlignL2 = new System.Windows.Forms.Button();
            this.btnProcessAAlignL3 = new System.Windows.Forms.Button();
            this.btnProcessAAlignL1 = new System.Windows.Forms.Button();
            this.lblProcessA2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnProcessBR2BlowOff = new System.Windows.Forms.Button();
            this.btnProcessBR1BlowOff = new System.Windows.Forms.Button();
            this.btnProcessBR2VacOff = new System.Windows.Forms.Button();
            this.btnProcessBR1VacOff = new System.Windows.Forms.Button();
            this.btnProcessBR2BlowOn = new System.Windows.Forms.Button();
            this.btnProcessBR2VacOn = new System.Windows.Forms.Button();
            this.btnProcessBR1BlowOn = new System.Windows.Forms.Button();
            this.btnProcessBR1VacOn = new System.Windows.Forms.Button();
            this.lblProcessB1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnProcessAL2BlowOff = new System.Windows.Forms.Button();
            this.btnProcessAL1BlowOff = new System.Windows.Forms.Button();
            this.btnProcessAL2VacOff = new System.Windows.Forms.Button();
            this.btnProcessAL1VacOff = new System.Windows.Forms.Button();
            this.btnProcessAL2BlowOn = new System.Windows.Forms.Button();
            this.btnProcessAL2VacOn = new System.Windows.Forms.Button();
            this.btnProcessAL1BlowOn = new System.Windows.Forms.Button();
            this.btnProcessAL1VacOn = new System.Windows.Forms.Button();
            this.lblProcessA1 = new System.Windows.Forms.Label();
            this.lblProcess = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnBreakBProcessR2 = new System.Windows.Forms.Button();
            this.btnBreakBProcessR3 = new System.Windows.Forms.Button();
            this.btnBreakBProcessR1 = new System.Windows.Forms.Button();
            this.lblBreakB3 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnBreakAProcessL2 = new System.Windows.Forms.Button();
            this.btnBreakAProcessL3 = new System.Windows.Forms.Button();
            this.btnBreakAProcessL1 = new System.Windows.Forms.Button();
            this.lblBreakA3 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnBreakBAlignR2 = new System.Windows.Forms.Button();
            this.btnBreakBAlignR3 = new System.Windows.Forms.Button();
            this.btnBreakBAlignR1 = new System.Windows.Forms.Button();
            this.lblBreakB2 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnBreakAAlignL2 = new System.Windows.Forms.Button();
            this.btnBreakAAlignL3 = new System.Windows.Forms.Button();
            this.btnBreakAAlignL1 = new System.Windows.Forms.Button();
            this.lblBreakA2 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnBreakBR2BlowOff = new System.Windows.Forms.Button();
            this.btnBreakBR1BlowOff = new System.Windows.Forms.Button();
            this.btnBreakBR2VacOff = new System.Windows.Forms.Button();
            this.btnBreakBR1VacOff = new System.Windows.Forms.Button();
            this.btnBreakBR2BlowOn = new System.Windows.Forms.Button();
            this.btnBreakBR2VacOn = new System.Windows.Forms.Button();
            this.btnBreakBR1BlowOn = new System.Windows.Forms.Button();
            this.btnBreakBR1VacOn = new System.Windows.Forms.Button();
            this.lblBreakB1 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnBreakAL2BlowOff = new System.Windows.Forms.Button();
            this.btnBreakAL1BlowOff = new System.Windows.Forms.Button();
            this.btnBreakAL2VacOff = new System.Windows.Forms.Button();
            this.btnBreakAL1VacOff = new System.Windows.Forms.Button();
            this.btnBreakAL2BlowOn = new System.Windows.Forms.Button();
            this.btnBreakAL2VacOn = new System.Windows.Forms.Button();
            this.btnBreakAL1BlowOn = new System.Windows.Forms.Button();
            this.btnBreakAL1VacOn = new System.Windows.Forms.Button();
            this.lblBreakA1 = new System.Windows.Forms.Label();
            this.lblBreak = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnMoveBreakTableRightUnload = new System.Windows.Forms.Button();
            this.btnMoveBreakTableLeftUnload = new System.Windows.Forms.Button();
            this.btnMoveBreakTableRightLoad = new System.Windows.Forms.Button();
            this.btnMoveBreakTableLeftLoad = new System.Windows.Forms.Button();
            this.lblBreakTable = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnMoveBreakTransferRightUnload = new System.Windows.Forms.Button();
            this.btnMoveBreakTransferLeftUnload = new System.Windows.Forms.Button();
            this.btnMoveBreakTransferRightLoad = new System.Windows.Forms.Button();
            this.btnMoveBreakTransferLeftLoad = new System.Windows.Forms.Button();
            this.lblBreakTransfer = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.btnMoveProcessRightUnload = new System.Windows.Forms.Button();
            this.btnMoveProcessLeftUnload = new System.Windows.Forms.Button();
            this.btnMoveProcessRightLoad = new System.Windows.Forms.Button();
            this.btnMoveProcessLeftLoad = new System.Windows.Forms.Button();
            this.lblMoveProcess = new System.Windows.Forms.Label();
            this.lblMove = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtEtcTimeShot = new System.Windows.Forms.TextBox();
            this.btnEtcTimeShot = new System.Windows.Forms.Button();
            this.lblEtcTimeShot = new System.Windows.Forms.Label();
            this.btnEtcOneProcess = new System.Windows.Forms.Button();
            this.btnEtcRepeatMove = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnEtcShutterClose = new System.Windows.Forms.Button();
            this.btnEtcShutterOpen = new System.Windows.Forms.Button();
            this.lblShutter = new System.Windows.Forms.Label();
            this.lblEtc = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btnAlignIR2 = new System.Windows.Forms.Button();
            this.btnAlignIR1 = new System.Windows.Forms.Button();
            this.btnAlignIL2 = new System.Windows.Forms.Button();
            this.btnAlignIL1 = new System.Windows.Forms.Button();
            this.btnAlignTestInsp = new System.Windows.Forms.Button();
            this.txtTotalInspCount = new System.Windows.Forms.TextBox();
            this.lblTotalInspCount = new System.Windows.Forms.Label();
            this.txtInspCount = new System.Windows.Forms.TextBox();
            this.lblInspCount = new System.Windows.Forms.Label();
            this.btnAlignPreAlign = new System.Windows.Forms.Button();
            this.btnAlignIdle = new System.Windows.Forms.Button();
            this.lblAlign = new System.Windows.Forms.Label();
            this.panel30.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(1200, 673);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(140, 47);
            this.btnClose.TabIndex = 70;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.panel13);
            this.panel30.Controls.Add(this.panel12);
            this.panel30.Controls.Add(this.panel11);
            this.panel30.Controls.Add(this.panel3);
            this.panel30.Controls.Add(this.panel10);
            this.panel30.Controls.Add(this.panel2);
            this.panel30.Controls.Add(this.panel1);
            this.panel30.Controls.Add(this.lblProcess);
            this.panel30.Location = new System.Drawing.Point(12, 12);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(398, 655);
            this.panel30.TabIndex = 460;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.btnProcessVision);
            this.panel13.Controls.Add(this.btnProcessCurrent);
            this.panel13.Location = new System.Drawing.Point(5, 411);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(386, 46);
            this.panel13.TabIndex = 468;
            // 
            // btnProcessVision
            // 
            this.btnProcessVision.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessVision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessVision.ForeColor = System.Drawing.Color.Black;
            this.btnProcessVision.Location = new System.Drawing.Point(204, 6);
            this.btnProcessVision.Name = "btnProcessVision";
            this.btnProcessVision.Size = new System.Drawing.Size(173, 33);
            this.btnProcessVision.TabIndex = 49;
            this.btnProcessVision.Text = "Vision";
            this.btnProcessVision.UseVisualStyleBackColor = false;
            // 
            // btnProcessCurrent
            // 
            this.btnProcessCurrent.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessCurrent.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessCurrent.ForeColor = System.Drawing.Color.Black;
            this.btnProcessCurrent.Location = new System.Drawing.Point(6, 6);
            this.btnProcessCurrent.Name = "btnProcessCurrent";
            this.btnProcessCurrent.Size = new System.Drawing.Size(174, 33);
            this.btnProcessCurrent.TabIndex = 48;
            this.btnProcessCurrent.Text = "Current";
            this.btnProcessCurrent.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.btnProcessBProcessR2);
            this.panel12.Controls.Add(this.btnProcessBProcessR3);
            this.panel12.Controls.Add(this.btnProcessBProcessR1);
            this.panel12.Controls.Add(this.lblProcessB3);
            this.panel12.Location = new System.Drawing.Point(203, 463);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(188, 185);
            this.panel12.TabIndex = 468;
            // 
            // btnProcessBProcessR2
            // 
            this.btnProcessBProcessR2.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBProcessR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBProcessR2.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBProcessR2.Location = new System.Drawing.Point(6, 79);
            this.btnProcessBProcessR2.Name = "btnProcessBProcessR2";
            this.btnProcessBProcessR2.Size = new System.Drawing.Size(174, 43);
            this.btnProcessBProcessR2.TabIndex = 53;
            this.btnProcessBProcessR2.Text = "Process R2";
            this.btnProcessBProcessR2.UseVisualStyleBackColor = false;
            // 
            // btnProcessBProcessR3
            // 
            this.btnProcessBProcessR3.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBProcessR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBProcessR3.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBProcessR3.Location = new System.Drawing.Point(6, 128);
            this.btnProcessBProcessR3.Name = "btnProcessBProcessR3";
            this.btnProcessBProcessR3.Size = new System.Drawing.Size(174, 43);
            this.btnProcessBProcessR3.TabIndex = 52;
            this.btnProcessBProcessR3.Text = "Process R3";
            this.btnProcessBProcessR3.UseVisualStyleBackColor = false;
            // 
            // btnProcessBProcessR1
            // 
            this.btnProcessBProcessR1.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBProcessR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBProcessR1.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBProcessR1.Location = new System.Drawing.Point(6, 31);
            this.btnProcessBProcessR1.Name = "btnProcessBProcessR1";
            this.btnProcessBProcessR1.Size = new System.Drawing.Size(174, 43);
            this.btnProcessBProcessR1.TabIndex = 51;
            this.btnProcessBProcessR1.Text = "Process R1";
            this.btnProcessBProcessR1.UseVisualStyleBackColor = false;
            // 
            // lblProcessB3
            // 
            this.lblProcessB3.AutoEllipsis = true;
            this.lblProcessB3.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessB3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessB3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessB3.ForeColor = System.Drawing.Color.Black;
            this.lblProcessB3.Location = new System.Drawing.Point(0, 0);
            this.lblProcessB3.Name = "lblProcessB3";
            this.lblProcessB3.Size = new System.Drawing.Size(186, 20);
            this.lblProcessB3.TabIndex = 9;
            this.lblProcessB3.Text = "■ B";
            this.lblProcessB3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.btnProcessAProcessL2);
            this.panel11.Controls.Add(this.btnProcessAProcessL3);
            this.panel11.Controls.Add(this.btnProcessAProcessL1);
            this.panel11.Controls.Add(this.lblProcessA3);
            this.panel11.Location = new System.Drawing.Point(5, 463);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(188, 185);
            this.panel11.TabIndex = 467;
            // 
            // btnProcessAProcessL2
            // 
            this.btnProcessAProcessL2.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAProcessL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAProcessL2.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAProcessL2.Location = new System.Drawing.Point(6, 79);
            this.btnProcessAProcessL2.Name = "btnProcessAProcessL2";
            this.btnProcessAProcessL2.Size = new System.Drawing.Size(174, 43);
            this.btnProcessAProcessL2.TabIndex = 50;
            this.btnProcessAProcessL2.Text = "Process L2";
            this.btnProcessAProcessL2.UseVisualStyleBackColor = false;
            // 
            // btnProcessAProcessL3
            // 
            this.btnProcessAProcessL3.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAProcessL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAProcessL3.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAProcessL3.Location = new System.Drawing.Point(6, 128);
            this.btnProcessAProcessL3.Name = "btnProcessAProcessL3";
            this.btnProcessAProcessL3.Size = new System.Drawing.Size(174, 43);
            this.btnProcessAProcessL3.TabIndex = 49;
            this.btnProcessAProcessL3.Text = "Process L3";
            this.btnProcessAProcessL3.UseVisualStyleBackColor = false;
            // 
            // btnProcessAProcessL1
            // 
            this.btnProcessAProcessL1.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAProcessL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAProcessL1.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAProcessL1.Location = new System.Drawing.Point(6, 31);
            this.btnProcessAProcessL1.Name = "btnProcessAProcessL1";
            this.btnProcessAProcessL1.Size = new System.Drawing.Size(174, 43);
            this.btnProcessAProcessL1.TabIndex = 47;
            this.btnProcessAProcessL1.Text = "Process L1";
            this.btnProcessAProcessL1.UseVisualStyleBackColor = false;
            // 
            // lblProcessA3
            // 
            this.lblProcessA3.AutoEllipsis = true;
            this.lblProcessA3.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessA3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessA3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessA3.ForeColor = System.Drawing.Color.Black;
            this.lblProcessA3.Location = new System.Drawing.Point(0, 0);
            this.lblProcessA3.Name = "lblProcessA3";
            this.lblProcessA3.Size = new System.Drawing.Size(186, 20);
            this.lblProcessA3.TabIndex = 9;
            this.lblProcessA3.Text = "■ A";
            this.lblProcessA3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnProcessBAlignR2);
            this.panel3.Controls.Add(this.btnProcessBAlignR3);
            this.panel3.Controls.Add(this.btnProcessBAlignR1);
            this.panel3.Controls.Add(this.lblProcessB2);
            this.panel3.Location = new System.Drawing.Point(203, 220);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(188, 185);
            this.panel3.TabIndex = 467;
            // 
            // btnProcessBAlignR2
            // 
            this.btnProcessBAlignR2.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBAlignR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBAlignR2.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBAlignR2.Location = new System.Drawing.Point(6, 79);
            this.btnProcessBAlignR2.Name = "btnProcessBAlignR2";
            this.btnProcessBAlignR2.Size = new System.Drawing.Size(174, 43);
            this.btnProcessBAlignR2.TabIndex = 53;
            this.btnProcessBAlignR2.Text = "Align R2";
            this.btnProcessBAlignR2.UseVisualStyleBackColor = false;
            // 
            // btnProcessBAlignR3
            // 
            this.btnProcessBAlignR3.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBAlignR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBAlignR3.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBAlignR3.Location = new System.Drawing.Point(6, 128);
            this.btnProcessBAlignR3.Name = "btnProcessBAlignR3";
            this.btnProcessBAlignR3.Size = new System.Drawing.Size(174, 43);
            this.btnProcessBAlignR3.TabIndex = 52;
            this.btnProcessBAlignR3.Text = "Align R3";
            this.btnProcessBAlignR3.UseVisualStyleBackColor = false;
            // 
            // btnProcessBAlignR1
            // 
            this.btnProcessBAlignR1.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBAlignR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBAlignR1.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBAlignR1.Location = new System.Drawing.Point(6, 31);
            this.btnProcessBAlignR1.Name = "btnProcessBAlignR1";
            this.btnProcessBAlignR1.Size = new System.Drawing.Size(174, 43);
            this.btnProcessBAlignR1.TabIndex = 51;
            this.btnProcessBAlignR1.Text = "Align R1";
            this.btnProcessBAlignR1.UseVisualStyleBackColor = false;
            // 
            // lblProcessB2
            // 
            this.lblProcessB2.AutoEllipsis = true;
            this.lblProcessB2.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessB2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessB2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessB2.ForeColor = System.Drawing.Color.Black;
            this.lblProcessB2.Location = new System.Drawing.Point(0, 0);
            this.lblProcessB2.Name = "lblProcessB2";
            this.lblProcessB2.Size = new System.Drawing.Size(186, 20);
            this.lblProcessB2.TabIndex = 9;
            this.lblProcessB2.Text = "■ B";
            this.lblProcessB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btnProcessAAlignL2);
            this.panel10.Controls.Add(this.btnProcessAAlignL3);
            this.panel10.Controls.Add(this.btnProcessAAlignL1);
            this.panel10.Controls.Add(this.lblProcessA2);
            this.panel10.Location = new System.Drawing.Point(5, 220);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(188, 185);
            this.panel10.TabIndex = 466;
            // 
            // btnProcessAAlignL2
            // 
            this.btnProcessAAlignL2.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAAlignL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAAlignL2.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAAlignL2.Location = new System.Drawing.Point(6, 79);
            this.btnProcessAAlignL2.Name = "btnProcessAAlignL2";
            this.btnProcessAAlignL2.Size = new System.Drawing.Size(174, 43);
            this.btnProcessAAlignL2.TabIndex = 50;
            this.btnProcessAAlignL2.Text = "Align L2";
            this.btnProcessAAlignL2.UseVisualStyleBackColor = false;
            // 
            // btnProcessAAlignL3
            // 
            this.btnProcessAAlignL3.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAAlignL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAAlignL3.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAAlignL3.Location = new System.Drawing.Point(6, 128);
            this.btnProcessAAlignL3.Name = "btnProcessAAlignL3";
            this.btnProcessAAlignL3.Size = new System.Drawing.Size(174, 43);
            this.btnProcessAAlignL3.TabIndex = 49;
            this.btnProcessAAlignL3.Text = "Align L3";
            this.btnProcessAAlignL3.UseVisualStyleBackColor = false;
            // 
            // btnProcessAAlignL1
            // 
            this.btnProcessAAlignL1.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAAlignL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAAlignL1.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAAlignL1.Location = new System.Drawing.Point(6, 31);
            this.btnProcessAAlignL1.Name = "btnProcessAAlignL1";
            this.btnProcessAAlignL1.Size = new System.Drawing.Size(174, 43);
            this.btnProcessAAlignL1.TabIndex = 47;
            this.btnProcessAAlignL1.Text = "Align L1";
            this.btnProcessAAlignL1.UseVisualStyleBackColor = false;
            // 
            // lblProcessA2
            // 
            this.lblProcessA2.AutoEllipsis = true;
            this.lblProcessA2.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessA2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessA2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessA2.ForeColor = System.Drawing.Color.Black;
            this.lblProcessA2.Location = new System.Drawing.Point(0, 0);
            this.lblProcessA2.Name = "lblProcessA2";
            this.lblProcessA2.Size = new System.Drawing.Size(186, 20);
            this.lblProcessA2.TabIndex = 9;
            this.lblProcessA2.Text = "■ A";
            this.lblProcessA2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnProcessBR2BlowOff);
            this.panel2.Controls.Add(this.btnProcessBR1BlowOff);
            this.panel2.Controls.Add(this.btnProcessBR2VacOff);
            this.panel2.Controls.Add(this.btnProcessBR1VacOff);
            this.panel2.Controls.Add(this.btnProcessBR2BlowOn);
            this.panel2.Controls.Add(this.btnProcessBR2VacOn);
            this.panel2.Controls.Add(this.btnProcessBR1BlowOn);
            this.panel2.Controls.Add(this.btnProcessBR1VacOn);
            this.panel2.Controls.Add(this.lblProcessB1);
            this.panel2.Location = new System.Drawing.Point(203, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(188, 185);
            this.panel2.TabIndex = 465;
            // 
            // btnProcessBR2BlowOff
            // 
            this.btnProcessBR2BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR2BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessBR2BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR2BlowOff.Location = new System.Drawing.Point(138, 104);
            this.btnProcessBR2BlowOff.Name = "btnProcessBR2BlowOff";
            this.btnProcessBR2BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR2BlowOff.TabIndex = 50;
            this.btnProcessBR2BlowOff.Text = "R2 Blow Off";
            this.btnProcessBR2BlowOff.UseVisualStyleBackColor = false;
            this.btnProcessBR2BlowOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessBR1BlowOff
            // 
            this.btnProcessBR1BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR1BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessBR1BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR1BlowOff.Location = new System.Drawing.Point(138, 31);
            this.btnProcessBR1BlowOff.Name = "btnProcessBR1BlowOff";
            this.btnProcessBR1BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR1BlowOff.TabIndex = 49;
            this.btnProcessBR1BlowOff.Text = "R1 Blow Off";
            this.btnProcessBR1BlowOff.UseVisualStyleBackColor = false;
            this.btnProcessBR1BlowOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessBR2VacOff
            // 
            this.btnProcessBR2VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR2VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBR2VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR2VacOff.Location = new System.Drawing.Point(50, 104);
            this.btnProcessBR2VacOff.Name = "btnProcessBR2VacOff";
            this.btnProcessBR2VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR2VacOff.TabIndex = 48;
            this.btnProcessBR2VacOff.Text = "R2 Vac Off";
            this.btnProcessBR2VacOff.UseVisualStyleBackColor = false;
            this.btnProcessBR2VacOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessBR1VacOff
            // 
            this.btnProcessBR1VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR1VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBR1VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR1VacOff.Location = new System.Drawing.Point(50, 31);
            this.btnProcessBR1VacOff.Name = "btnProcessBR1VacOff";
            this.btnProcessBR1VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR1VacOff.TabIndex = 47;
            this.btnProcessBR1VacOff.Text = "R1 Vac Off";
            this.btnProcessBR1VacOff.UseVisualStyleBackColor = false;
            this.btnProcessBR1VacOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessBR2BlowOn
            // 
            this.btnProcessBR2BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR2BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessBR2BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR2BlowOn.Location = new System.Drawing.Point(94, 104);
            this.btnProcessBR2BlowOn.Name = "btnProcessBR2BlowOn";
            this.btnProcessBR2BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR2BlowOn.TabIndex = 46;
            this.btnProcessBR2BlowOn.Text = "R2 Blow On";
            this.btnProcessBR2BlowOn.UseVisualStyleBackColor = false;
            this.btnProcessBR2BlowOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessBR2VacOn
            // 
            this.btnProcessBR2VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR2VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBR2VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR2VacOn.Location = new System.Drawing.Point(6, 104);
            this.btnProcessBR2VacOn.Name = "btnProcessBR2VacOn";
            this.btnProcessBR2VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR2VacOn.TabIndex = 45;
            this.btnProcessBR2VacOn.Text = "R2 Vac On";
            this.btnProcessBR2VacOn.UseVisualStyleBackColor = false;
            this.btnProcessBR2VacOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessBR1BlowOn
            // 
            this.btnProcessBR1BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR1BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessBR1BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR1BlowOn.Location = new System.Drawing.Point(94, 31);
            this.btnProcessBR1BlowOn.Name = "btnProcessBR1BlowOn";
            this.btnProcessBR1BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR1BlowOn.TabIndex = 44;
            this.btnProcessBR1BlowOn.Text = "R1 Blow On";
            this.btnProcessBR1BlowOn.UseVisualStyleBackColor = false;
            this.btnProcessBR1BlowOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessBR1VacOn
            // 
            this.btnProcessBR1VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBR1VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBR1VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBR1VacOn.Location = new System.Drawing.Point(6, 31);
            this.btnProcessBR1VacOn.Name = "btnProcessBR1VacOn";
            this.btnProcessBR1VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessBR1VacOn.TabIndex = 43;
            this.btnProcessBR1VacOn.Text = "R1 Vac On";
            this.btnProcessBR1VacOn.UseVisualStyleBackColor = false;
            this.btnProcessBR1VacOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // lblProcessB1
            // 
            this.lblProcessB1.AutoEllipsis = true;
            this.lblProcessB1.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessB1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessB1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessB1.ForeColor = System.Drawing.Color.Black;
            this.lblProcessB1.Location = new System.Drawing.Point(0, 0);
            this.lblProcessB1.Name = "lblProcessB1";
            this.lblProcessB1.Size = new System.Drawing.Size(186, 20);
            this.lblProcessB1.TabIndex = 9;
            this.lblProcessB1.Text = "■ B";
            this.lblProcessB1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnProcessAL2BlowOff);
            this.panel1.Controls.Add(this.btnProcessAL1BlowOff);
            this.panel1.Controls.Add(this.btnProcessAL2VacOff);
            this.panel1.Controls.Add(this.btnProcessAL1VacOff);
            this.panel1.Controls.Add(this.btnProcessAL2BlowOn);
            this.panel1.Controls.Add(this.btnProcessAL2VacOn);
            this.panel1.Controls.Add(this.btnProcessAL1BlowOn);
            this.panel1.Controls.Add(this.btnProcessAL1VacOn);
            this.panel1.Controls.Add(this.lblProcessA1);
            this.panel1.Location = new System.Drawing.Point(5, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 185);
            this.panel1.TabIndex = 464;
            // 
            // btnProcessAL2BlowOff
            // 
            this.btnProcessAL2BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL2BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessAL2BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL2BlowOff.Location = new System.Drawing.Point(138, 104);
            this.btnProcessAL2BlowOff.Name = "btnProcessAL2BlowOff";
            this.btnProcessAL2BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL2BlowOff.TabIndex = 54;
            this.btnProcessAL2BlowOff.Text = "L2 Blow Off";
            this.btnProcessAL2BlowOff.UseVisualStyleBackColor = false;
            this.btnProcessAL2BlowOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessAL1BlowOff
            // 
            this.btnProcessAL1BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL1BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessAL1BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL1BlowOff.Location = new System.Drawing.Point(138, 31);
            this.btnProcessAL1BlowOff.Name = "btnProcessAL1BlowOff";
            this.btnProcessAL1BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL1BlowOff.TabIndex = 53;
            this.btnProcessAL1BlowOff.Text = "L1 Blow Off";
            this.btnProcessAL1BlowOff.UseVisualStyleBackColor = false;
            this.btnProcessAL1BlowOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessAL2VacOff
            // 
            this.btnProcessAL2VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL2VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAL2VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL2VacOff.Location = new System.Drawing.Point(50, 104);
            this.btnProcessAL2VacOff.Name = "btnProcessAL2VacOff";
            this.btnProcessAL2VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL2VacOff.TabIndex = 52;
            this.btnProcessAL2VacOff.Text = "L2 Vac Off";
            this.btnProcessAL2VacOff.UseVisualStyleBackColor = false;
            this.btnProcessAL2VacOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessAL1VacOff
            // 
            this.btnProcessAL1VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL1VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAL1VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL1VacOff.Location = new System.Drawing.Point(50, 31);
            this.btnProcessAL1VacOff.Name = "btnProcessAL1VacOff";
            this.btnProcessAL1VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL1VacOff.TabIndex = 51;
            this.btnProcessAL1VacOff.Text = "L1 Vac Off";
            this.btnProcessAL1VacOff.UseVisualStyleBackColor = false;
            this.btnProcessAL1VacOff.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessAL2BlowOn
            // 
            this.btnProcessAL2BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL2BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessAL2BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL2BlowOn.Location = new System.Drawing.Point(94, 104);
            this.btnProcessAL2BlowOn.Name = "btnProcessAL2BlowOn";
            this.btnProcessAL2BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL2BlowOn.TabIndex = 50;
            this.btnProcessAL2BlowOn.Text = "L2 Blow  On";
            this.btnProcessAL2BlowOn.UseVisualStyleBackColor = false;
            this.btnProcessAL2BlowOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessAL2VacOn
            // 
            this.btnProcessAL2VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL2VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAL2VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL2VacOn.Location = new System.Drawing.Point(6, 104);
            this.btnProcessAL2VacOn.Name = "btnProcessAL2VacOn";
            this.btnProcessAL2VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL2VacOn.TabIndex = 49;
            this.btnProcessAL2VacOn.Text = "L2 Vac On";
            this.btnProcessAL2VacOn.UseVisualStyleBackColor = false;
            this.btnProcessAL2VacOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessAL1BlowOn
            // 
            this.btnProcessAL1BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL1BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnProcessAL1BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL1BlowOn.Location = new System.Drawing.Point(94, 31);
            this.btnProcessAL1BlowOn.Name = "btnProcessAL1BlowOn";
            this.btnProcessAL1BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL1BlowOn.TabIndex = 48;
            this.btnProcessAL1BlowOn.Text = "L1 Blow On";
            this.btnProcessAL1BlowOn.UseVisualStyleBackColor = false;
            this.btnProcessAL1BlowOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnProcessAL1VacOn
            // 
            this.btnProcessAL1VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessAL1VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessAL1VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAL1VacOn.Location = new System.Drawing.Point(6, 31);
            this.btnProcessAL1VacOn.Name = "btnProcessAL1VacOn";
            this.btnProcessAL1VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnProcessAL1VacOn.TabIndex = 47;
            this.btnProcessAL1VacOn.Text = "L1 Vac On";
            this.btnProcessAL1VacOn.UseVisualStyleBackColor = false;
            this.btnProcessAL1VacOn.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // lblProcessA1
            // 
            this.lblProcessA1.AutoEllipsis = true;
            this.lblProcessA1.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessA1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessA1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessA1.ForeColor = System.Drawing.Color.Black;
            this.lblProcessA1.Location = new System.Drawing.Point(0, 0);
            this.lblProcessA1.Name = "lblProcessA1";
            this.lblProcessA1.Size = new System.Drawing.Size(186, 20);
            this.lblProcessA1.TabIndex = 9;
            this.lblProcessA1.Text = "■ A";
            this.lblProcessA1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcess
            // 
            this.lblProcess.AutoEllipsis = true;
            this.lblProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcess.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcess.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcess.ForeColor = System.Drawing.Color.Black;
            this.lblProcess.Location = new System.Drawing.Point(0, 0);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(396, 26);
            this.lblProcess.TabIndex = 9;
            this.lblProcess.Text = "■ Process";
            this.lblProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel14);
            this.panel4.Controls.Add(this.panel15);
            this.panel4.Controls.Add(this.panel16);
            this.panel4.Controls.Add(this.panel17);
            this.panel4.Controls.Add(this.panel18);
            this.panel4.Controls.Add(this.lblBreak);
            this.panel4.Location = new System.Drawing.Point(416, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(398, 655);
            this.panel4.TabIndex = 461;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnBreakBProcessR2);
            this.panel6.Controls.Add(this.btnBreakBProcessR3);
            this.panel6.Controls.Add(this.btnBreakBProcessR1);
            this.panel6.Controls.Add(this.lblBreakB3);
            this.panel6.Location = new System.Drawing.Point(203, 463);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(188, 185);
            this.panel6.TabIndex = 468;
            // 
            // btnBreakBProcessR2
            // 
            this.btnBreakBProcessR2.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBProcessR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBProcessR2.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBProcessR2.Location = new System.Drawing.Point(6, 79);
            this.btnBreakBProcessR2.Name = "btnBreakBProcessR2";
            this.btnBreakBProcessR2.Size = new System.Drawing.Size(174, 43);
            this.btnBreakBProcessR2.TabIndex = 53;
            this.btnBreakBProcessR2.Text = "Process R2";
            this.btnBreakBProcessR2.UseVisualStyleBackColor = false;
            // 
            // btnBreakBProcessR3
            // 
            this.btnBreakBProcessR3.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBProcessR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBProcessR3.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBProcessR3.Location = new System.Drawing.Point(6, 128);
            this.btnBreakBProcessR3.Name = "btnBreakBProcessR3";
            this.btnBreakBProcessR3.Size = new System.Drawing.Size(174, 43);
            this.btnBreakBProcessR3.TabIndex = 52;
            this.btnBreakBProcessR3.Text = "Process R3";
            this.btnBreakBProcessR3.UseVisualStyleBackColor = false;
            // 
            // btnBreakBProcessR1
            // 
            this.btnBreakBProcessR1.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBProcessR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBProcessR1.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBProcessR1.Location = new System.Drawing.Point(6, 31);
            this.btnBreakBProcessR1.Name = "btnBreakBProcessR1";
            this.btnBreakBProcessR1.Size = new System.Drawing.Size(174, 43);
            this.btnBreakBProcessR1.TabIndex = 51;
            this.btnBreakBProcessR1.Text = "Process R1";
            this.btnBreakBProcessR1.UseVisualStyleBackColor = false;
            // 
            // lblBreakB3
            // 
            this.lblBreakB3.AutoEllipsis = true;
            this.lblBreakB3.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakB3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakB3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakB3.ForeColor = System.Drawing.Color.Black;
            this.lblBreakB3.Location = new System.Drawing.Point(0, 0);
            this.lblBreakB3.Name = "lblBreakB3";
            this.lblBreakB3.Size = new System.Drawing.Size(186, 20);
            this.lblBreakB3.TabIndex = 9;
            this.lblBreakB3.Text = "■ B";
            this.lblBreakB3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btnBreakAProcessL2);
            this.panel14.Controls.Add(this.btnBreakAProcessL3);
            this.panel14.Controls.Add(this.btnBreakAProcessL1);
            this.panel14.Controls.Add(this.lblBreakA3);
            this.panel14.Location = new System.Drawing.Point(5, 463);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(188, 185);
            this.panel14.TabIndex = 467;
            // 
            // btnBreakAProcessL2
            // 
            this.btnBreakAProcessL2.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAProcessL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAProcessL2.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAProcessL2.Location = new System.Drawing.Point(6, 79);
            this.btnBreakAProcessL2.Name = "btnBreakAProcessL2";
            this.btnBreakAProcessL2.Size = new System.Drawing.Size(174, 43);
            this.btnBreakAProcessL2.TabIndex = 50;
            this.btnBreakAProcessL2.Text = "Process L2";
            this.btnBreakAProcessL2.UseVisualStyleBackColor = false;
            // 
            // btnBreakAProcessL3
            // 
            this.btnBreakAProcessL3.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAProcessL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAProcessL3.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAProcessL3.Location = new System.Drawing.Point(6, 128);
            this.btnBreakAProcessL3.Name = "btnBreakAProcessL3";
            this.btnBreakAProcessL3.Size = new System.Drawing.Size(174, 43);
            this.btnBreakAProcessL3.TabIndex = 49;
            this.btnBreakAProcessL3.Text = "Process L3";
            this.btnBreakAProcessL3.UseVisualStyleBackColor = false;
            // 
            // btnBreakAProcessL1
            // 
            this.btnBreakAProcessL1.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAProcessL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAProcessL1.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAProcessL1.Location = new System.Drawing.Point(6, 31);
            this.btnBreakAProcessL1.Name = "btnBreakAProcessL1";
            this.btnBreakAProcessL1.Size = new System.Drawing.Size(174, 43);
            this.btnBreakAProcessL1.TabIndex = 47;
            this.btnBreakAProcessL1.Text = "Process L1";
            this.btnBreakAProcessL1.UseVisualStyleBackColor = false;
            // 
            // lblBreakA3
            // 
            this.lblBreakA3.AutoEllipsis = true;
            this.lblBreakA3.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakA3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakA3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakA3.ForeColor = System.Drawing.Color.Black;
            this.lblBreakA3.Location = new System.Drawing.Point(0, 0);
            this.lblBreakA3.Name = "lblBreakA3";
            this.lblBreakA3.Size = new System.Drawing.Size(186, 20);
            this.lblBreakA3.TabIndex = 9;
            this.lblBreakA3.Text = "■ A";
            this.lblBreakA3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnBreakBAlignR2);
            this.panel15.Controls.Add(this.btnBreakBAlignR3);
            this.panel15.Controls.Add(this.btnBreakBAlignR1);
            this.panel15.Controls.Add(this.lblBreakB2);
            this.panel15.Location = new System.Drawing.Point(203, 220);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(188, 185);
            this.panel15.TabIndex = 467;
            // 
            // btnBreakBAlignR2
            // 
            this.btnBreakBAlignR2.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBAlignR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBAlignR2.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBAlignR2.Location = new System.Drawing.Point(6, 79);
            this.btnBreakBAlignR2.Name = "btnBreakBAlignR2";
            this.btnBreakBAlignR2.Size = new System.Drawing.Size(174, 43);
            this.btnBreakBAlignR2.TabIndex = 53;
            this.btnBreakBAlignR2.Text = "Align R2";
            this.btnBreakBAlignR2.UseVisualStyleBackColor = false;
            // 
            // btnBreakBAlignR3
            // 
            this.btnBreakBAlignR3.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBAlignR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBAlignR3.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBAlignR3.Location = new System.Drawing.Point(6, 128);
            this.btnBreakBAlignR3.Name = "btnBreakBAlignR3";
            this.btnBreakBAlignR3.Size = new System.Drawing.Size(174, 43);
            this.btnBreakBAlignR3.TabIndex = 52;
            this.btnBreakBAlignR3.Text = "Align R3";
            this.btnBreakBAlignR3.UseVisualStyleBackColor = false;
            // 
            // btnBreakBAlignR1
            // 
            this.btnBreakBAlignR1.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBAlignR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBAlignR1.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBAlignR1.Location = new System.Drawing.Point(6, 31);
            this.btnBreakBAlignR1.Name = "btnBreakBAlignR1";
            this.btnBreakBAlignR1.Size = new System.Drawing.Size(174, 43);
            this.btnBreakBAlignR1.TabIndex = 51;
            this.btnBreakBAlignR1.Text = "Align R1";
            this.btnBreakBAlignR1.UseVisualStyleBackColor = false;
            // 
            // lblBreakB2
            // 
            this.lblBreakB2.AutoEllipsis = true;
            this.lblBreakB2.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakB2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakB2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakB2.ForeColor = System.Drawing.Color.Black;
            this.lblBreakB2.Location = new System.Drawing.Point(0, 0);
            this.lblBreakB2.Name = "lblBreakB2";
            this.lblBreakB2.Size = new System.Drawing.Size(186, 20);
            this.lblBreakB2.TabIndex = 9;
            this.lblBreakB2.Text = "■ B";
            this.lblBreakB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.btnBreakAAlignL2);
            this.panel16.Controls.Add(this.btnBreakAAlignL3);
            this.panel16.Controls.Add(this.btnBreakAAlignL1);
            this.panel16.Controls.Add(this.lblBreakA2);
            this.panel16.Location = new System.Drawing.Point(5, 220);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(188, 185);
            this.panel16.TabIndex = 466;
            // 
            // btnBreakAAlignL2
            // 
            this.btnBreakAAlignL2.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAAlignL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAAlignL2.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAAlignL2.Location = new System.Drawing.Point(6, 79);
            this.btnBreakAAlignL2.Name = "btnBreakAAlignL2";
            this.btnBreakAAlignL2.Size = new System.Drawing.Size(174, 43);
            this.btnBreakAAlignL2.TabIndex = 50;
            this.btnBreakAAlignL2.Text = "Align L2";
            this.btnBreakAAlignL2.UseVisualStyleBackColor = false;
            // 
            // btnBreakAAlignL3
            // 
            this.btnBreakAAlignL3.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAAlignL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAAlignL3.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAAlignL3.Location = new System.Drawing.Point(6, 128);
            this.btnBreakAAlignL3.Name = "btnBreakAAlignL3";
            this.btnBreakAAlignL3.Size = new System.Drawing.Size(174, 43);
            this.btnBreakAAlignL3.TabIndex = 49;
            this.btnBreakAAlignL3.Text = "Align L3";
            this.btnBreakAAlignL3.UseVisualStyleBackColor = false;
            // 
            // btnBreakAAlignL1
            // 
            this.btnBreakAAlignL1.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAAlignL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAAlignL1.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAAlignL1.Location = new System.Drawing.Point(6, 31);
            this.btnBreakAAlignL1.Name = "btnBreakAAlignL1";
            this.btnBreakAAlignL1.Size = new System.Drawing.Size(174, 43);
            this.btnBreakAAlignL1.TabIndex = 47;
            this.btnBreakAAlignL1.Text = "Align L1";
            this.btnBreakAAlignL1.UseVisualStyleBackColor = false;
            // 
            // lblBreakA2
            // 
            this.lblBreakA2.AutoEllipsis = true;
            this.lblBreakA2.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakA2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakA2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakA2.ForeColor = System.Drawing.Color.Black;
            this.lblBreakA2.Location = new System.Drawing.Point(0, 0);
            this.lblBreakA2.Name = "lblBreakA2";
            this.lblBreakA2.Size = new System.Drawing.Size(186, 20);
            this.lblBreakA2.TabIndex = 9;
            this.lblBreakA2.Text = "■ A";
            this.lblBreakA2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.btnBreakBR2BlowOff);
            this.panel17.Controls.Add(this.btnBreakBR1BlowOff);
            this.panel17.Controls.Add(this.btnBreakBR2VacOff);
            this.panel17.Controls.Add(this.btnBreakBR1VacOff);
            this.panel17.Controls.Add(this.btnBreakBR2BlowOn);
            this.panel17.Controls.Add(this.btnBreakBR2VacOn);
            this.panel17.Controls.Add(this.btnBreakBR1BlowOn);
            this.panel17.Controls.Add(this.btnBreakBR1VacOn);
            this.panel17.Controls.Add(this.lblBreakB1);
            this.panel17.Location = new System.Drawing.Point(203, 29);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(188, 185);
            this.panel17.TabIndex = 465;
            // 
            // btnBreakBR2BlowOff
            // 
            this.btnBreakBR2BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR2BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakBR2BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR2BlowOff.Location = new System.Drawing.Point(140, 104);
            this.btnBreakBR2BlowOff.Name = "btnBreakBR2BlowOff";
            this.btnBreakBR2BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR2BlowOff.TabIndex = 50;
            this.btnBreakBR2BlowOff.Text = "R2 Blow Off";
            this.btnBreakBR2BlowOff.UseVisualStyleBackColor = false;
            this.btnBreakBR2BlowOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakBR1BlowOff
            // 
            this.btnBreakBR1BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR1BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakBR1BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR1BlowOff.Location = new System.Drawing.Point(140, 31);
            this.btnBreakBR1BlowOff.Name = "btnBreakBR1BlowOff";
            this.btnBreakBR1BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR1BlowOff.TabIndex = 49;
            this.btnBreakBR1BlowOff.Text = "R1 Blow Off";
            this.btnBreakBR1BlowOff.UseVisualStyleBackColor = false;
            this.btnBreakBR1BlowOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakBR2VacOff
            // 
            this.btnBreakBR2VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR2VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBR2VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR2VacOff.Location = new System.Drawing.Point(50, 104);
            this.btnBreakBR2VacOff.Name = "btnBreakBR2VacOff";
            this.btnBreakBR2VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR2VacOff.TabIndex = 48;
            this.btnBreakBR2VacOff.Text = "R2 Vac Off";
            this.btnBreakBR2VacOff.UseVisualStyleBackColor = false;
            this.btnBreakBR2VacOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakBR1VacOff
            // 
            this.btnBreakBR1VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR1VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBR1VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR1VacOff.Location = new System.Drawing.Point(50, 31);
            this.btnBreakBR1VacOff.Name = "btnBreakBR1VacOff";
            this.btnBreakBR1VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR1VacOff.TabIndex = 47;
            this.btnBreakBR1VacOff.Text = "R1 Vac Off";
            this.btnBreakBR1VacOff.UseVisualStyleBackColor = false;
            this.btnBreakBR1VacOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakBR2BlowOn
            // 
            this.btnBreakBR2BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR2BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakBR2BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR2BlowOn.Location = new System.Drawing.Point(94, 104);
            this.btnBreakBR2BlowOn.Name = "btnBreakBR2BlowOn";
            this.btnBreakBR2BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR2BlowOn.TabIndex = 46;
            this.btnBreakBR2BlowOn.Text = "R2 Blow On";
            this.btnBreakBR2BlowOn.UseVisualStyleBackColor = false;
            this.btnBreakBR2BlowOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakBR2VacOn
            // 
            this.btnBreakBR2VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR2VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBR2VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR2VacOn.Location = new System.Drawing.Point(6, 104);
            this.btnBreakBR2VacOn.Name = "btnBreakBR2VacOn";
            this.btnBreakBR2VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR2VacOn.TabIndex = 45;
            this.btnBreakBR2VacOn.Text = "R2 Vac On";
            this.btnBreakBR2VacOn.UseVisualStyleBackColor = false;
            this.btnBreakBR2VacOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakBR1BlowOn
            // 
            this.btnBreakBR1BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR1BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakBR1BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR1BlowOn.Location = new System.Drawing.Point(94, 31);
            this.btnBreakBR1BlowOn.Name = "btnBreakBR1BlowOn";
            this.btnBreakBR1BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR1BlowOn.TabIndex = 44;
            this.btnBreakBR1BlowOn.Text = "R1 Blow On";
            this.btnBreakBR1BlowOn.UseVisualStyleBackColor = false;
            this.btnBreakBR1BlowOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakBR1VacOn
            // 
            this.btnBreakBR1VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBR1VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakBR1VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBR1VacOn.Location = new System.Drawing.Point(6, 31);
            this.btnBreakBR1VacOn.Name = "btnBreakBR1VacOn";
            this.btnBreakBR1VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakBR1VacOn.TabIndex = 43;
            this.btnBreakBR1VacOn.Text = "R1 Vac On";
            this.btnBreakBR1VacOn.UseVisualStyleBackColor = false;
            this.btnBreakBR1VacOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // lblBreakB1
            // 
            this.lblBreakB1.AutoEllipsis = true;
            this.lblBreakB1.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakB1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakB1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakB1.ForeColor = System.Drawing.Color.Black;
            this.lblBreakB1.Location = new System.Drawing.Point(0, 0);
            this.lblBreakB1.Name = "lblBreakB1";
            this.lblBreakB1.Size = new System.Drawing.Size(186, 20);
            this.lblBreakB1.TabIndex = 9;
            this.lblBreakB1.Text = "■ B";
            this.lblBreakB1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.btnBreakAL2BlowOff);
            this.panel18.Controls.Add(this.btnBreakAL1BlowOff);
            this.panel18.Controls.Add(this.btnBreakAL2VacOff);
            this.panel18.Controls.Add(this.btnBreakAL1VacOff);
            this.panel18.Controls.Add(this.btnBreakAL2BlowOn);
            this.panel18.Controls.Add(this.btnBreakAL2VacOn);
            this.panel18.Controls.Add(this.btnBreakAL1BlowOn);
            this.panel18.Controls.Add(this.btnBreakAL1VacOn);
            this.panel18.Controls.Add(this.lblBreakA1);
            this.panel18.Location = new System.Drawing.Point(5, 29);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(188, 185);
            this.panel18.TabIndex = 464;
            // 
            // btnBreakAL2BlowOff
            // 
            this.btnBreakAL2BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL2BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakAL2BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL2BlowOff.Location = new System.Drawing.Point(138, 104);
            this.btnBreakAL2BlowOff.Name = "btnBreakAL2BlowOff";
            this.btnBreakAL2BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL2BlowOff.TabIndex = 54;
            this.btnBreakAL2BlowOff.Text = "L2 Blow Off";
            this.btnBreakAL2BlowOff.UseVisualStyleBackColor = false;
            this.btnBreakAL2BlowOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakAL1BlowOff
            // 
            this.btnBreakAL1BlowOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL1BlowOff.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakAL1BlowOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL1BlowOff.Location = new System.Drawing.Point(138, 31);
            this.btnBreakAL1BlowOff.Name = "btnBreakAL1BlowOff";
            this.btnBreakAL1BlowOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL1BlowOff.TabIndex = 53;
            this.btnBreakAL1BlowOff.Text = "L1 Blow Off";
            this.btnBreakAL1BlowOff.UseVisualStyleBackColor = false;
            this.btnBreakAL1BlowOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakAL2VacOff
            // 
            this.btnBreakAL2VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL2VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAL2VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL2VacOff.Location = new System.Drawing.Point(50, 104);
            this.btnBreakAL2VacOff.Name = "btnBreakAL2VacOff";
            this.btnBreakAL2VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL2VacOff.TabIndex = 52;
            this.btnBreakAL2VacOff.Text = "L2 Vac Off";
            this.btnBreakAL2VacOff.UseVisualStyleBackColor = false;
            this.btnBreakAL2VacOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakAL1VacOff
            // 
            this.btnBreakAL1VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL1VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAL1VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL1VacOff.Location = new System.Drawing.Point(50, 31);
            this.btnBreakAL1VacOff.Name = "btnBreakAL1VacOff";
            this.btnBreakAL1VacOff.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL1VacOff.TabIndex = 51;
            this.btnBreakAL1VacOff.Text = "L1 Vac Off";
            this.btnBreakAL1VacOff.UseVisualStyleBackColor = false;
            this.btnBreakAL1VacOff.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakAL2BlowOn
            // 
            this.btnBreakAL2BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL2BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakAL2BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL2BlowOn.Location = new System.Drawing.Point(94, 104);
            this.btnBreakAL2BlowOn.Name = "btnBreakAL2BlowOn";
            this.btnBreakAL2BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL2BlowOn.TabIndex = 50;
            this.btnBreakAL2BlowOn.Text = "L2 Blow On";
            this.btnBreakAL2BlowOn.UseVisualStyleBackColor = false;
            this.btnBreakAL2BlowOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakAL2VacOn
            // 
            this.btnBreakAL2VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL2VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAL2VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL2VacOn.Location = new System.Drawing.Point(6, 104);
            this.btnBreakAL2VacOn.Name = "btnBreakAL2VacOn";
            this.btnBreakAL2VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL2VacOn.TabIndex = 49;
            this.btnBreakAL2VacOn.Text = "L2 Vac On";
            this.btnBreakAL2VacOn.UseVisualStyleBackColor = false;
            this.btnBreakAL2VacOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakAL1BlowOn
            // 
            this.btnBreakAL1BlowOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL1BlowOn.Font = new System.Drawing.Font("맑은 고딕", 7.5F);
            this.btnBreakAL1BlowOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL1BlowOn.Location = new System.Drawing.Point(94, 31);
            this.btnBreakAL1BlowOn.Name = "btnBreakAL1BlowOn";
            this.btnBreakAL1BlowOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL1BlowOn.TabIndex = 48;
            this.btnBreakAL1BlowOn.Text = "L1 Blow On";
            this.btnBreakAL1BlowOn.UseVisualStyleBackColor = false;
            this.btnBreakAL1BlowOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // btnBreakAL1VacOn
            // 
            this.btnBreakAL1VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakAL1VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakAL1VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakAL1VacOn.Location = new System.Drawing.Point(6, 31);
            this.btnBreakAL1VacOn.Name = "btnBreakAL1VacOn";
            this.btnBreakAL1VacOn.Size = new System.Drawing.Size(40, 67);
            this.btnBreakAL1VacOn.TabIndex = 47;
            this.btnBreakAL1VacOn.Text = "L1 Vac On";
            this.btnBreakAL1VacOn.UseVisualStyleBackColor = false;
            this.btnBreakAL1VacOn.Click += new System.EventHandler(this.btnBreak_Click);
            // 
            // lblBreakA1
            // 
            this.lblBreakA1.AutoEllipsis = true;
            this.lblBreakA1.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakA1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakA1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakA1.ForeColor = System.Drawing.Color.Black;
            this.lblBreakA1.Location = new System.Drawing.Point(0, 0);
            this.lblBreakA1.Name = "lblBreakA1";
            this.lblBreakA1.Size = new System.Drawing.Size(186, 20);
            this.lblBreakA1.TabIndex = 9;
            this.lblBreakA1.Text = "■ A";
            this.lblBreakA1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBreak
            // 
            this.lblBreak.AutoEllipsis = true;
            this.lblBreak.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreak.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreak.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreak.ForeColor = System.Drawing.Color.Black;
            this.lblBreak.Location = new System.Drawing.Point(0, 0);
            this.lblBreak.Name = "lblBreak";
            this.lblBreak.Size = new System.Drawing.Size(396, 26);
            this.lblBreak.TabIndex = 9;
            this.lblBreak.Text = "■ Break";
            this.lblBreak.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel24);
            this.panel5.Controls.Add(this.lblMove);
            this.panel5.Location = new System.Drawing.Point(820, 12);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(520, 220);
            this.panel5.TabIndex = 462;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnMoveBreakTableRightUnload);
            this.panel8.Controls.Add(this.btnMoveBreakTableLeftUnload);
            this.panel8.Controls.Add(this.btnMoveBreakTableRightLoad);
            this.panel8.Controls.Add(this.btnMoveBreakTableLeftLoad);
            this.panel8.Controls.Add(this.lblBreakTable);
            this.panel8.Location = new System.Drawing.Point(350, 29);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(163, 185);
            this.panel8.TabIndex = 466;
            // 
            // btnMoveBreakTableRightUnload
            // 
            this.btnMoveBreakTableRightUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTableRightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTableRightUnload.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTableRightUnload.Location = new System.Drawing.Point(83, 104);
            this.btnMoveBreakTableRightUnload.Name = "btnMoveBreakTableRightUnload";
            this.btnMoveBreakTableRightUnload.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTableRightUnload.TabIndex = 50;
            this.btnMoveBreakTableRightUnload.Text = "Right Unload";
            this.btnMoveBreakTableRightUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTableLeftUnload
            // 
            this.btnMoveBreakTableLeftUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTableLeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTableLeftUnload.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTableLeftUnload.Location = new System.Drawing.Point(7, 104);
            this.btnMoveBreakTableLeftUnload.Name = "btnMoveBreakTableLeftUnload";
            this.btnMoveBreakTableLeftUnload.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTableLeftUnload.TabIndex = 49;
            this.btnMoveBreakTableLeftUnload.Text = "Left Unload";
            this.btnMoveBreakTableLeftUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTableRightLoad
            // 
            this.btnMoveBreakTableRightLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTableRightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTableRightLoad.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTableRightLoad.Location = new System.Drawing.Point(83, 31);
            this.btnMoveBreakTableRightLoad.Name = "btnMoveBreakTableRightLoad";
            this.btnMoveBreakTableRightLoad.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTableRightLoad.TabIndex = 48;
            this.btnMoveBreakTableRightLoad.Text = " Right   Load";
            this.btnMoveBreakTableRightLoad.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTableLeftLoad
            // 
            this.btnMoveBreakTableLeftLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTableLeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTableLeftLoad.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTableLeftLoad.Location = new System.Drawing.Point(7, 31);
            this.btnMoveBreakTableLeftLoad.Name = "btnMoveBreakTableLeftLoad";
            this.btnMoveBreakTableLeftLoad.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTableLeftLoad.TabIndex = 47;
            this.btnMoveBreakTableLeftLoad.Text = "  Left    Load";
            this.btnMoveBreakTableLeftLoad.UseVisualStyleBackColor = false;
            // 
            // lblBreakTable
            // 
            this.lblBreakTable.AutoEllipsis = true;
            this.lblBreakTable.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakTable.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakTable.ForeColor = System.Drawing.Color.Black;
            this.lblBreakTable.Location = new System.Drawing.Point(0, 0);
            this.lblBreakTable.Name = "lblBreakTable";
            this.lblBreakTable.Size = new System.Drawing.Size(161, 20);
            this.lblBreakTable.TabIndex = 9;
            this.lblBreakTable.Text = "■ BreakTable";
            this.lblBreakTable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnMoveBreakTransferRightUnload);
            this.panel7.Controls.Add(this.btnMoveBreakTransferLeftUnload);
            this.panel7.Controls.Add(this.btnMoveBreakTransferRightLoad);
            this.panel7.Controls.Add(this.btnMoveBreakTransferLeftLoad);
            this.panel7.Controls.Add(this.lblBreakTransfer);
            this.panel7.Location = new System.Drawing.Point(177, 29);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(163, 185);
            this.panel7.TabIndex = 465;
            // 
            // btnMoveBreakTransferRightUnload
            // 
            this.btnMoveBreakTransferRightUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTransferRightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTransferRightUnload.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTransferRightUnload.Location = new System.Drawing.Point(83, 104);
            this.btnMoveBreakTransferRightUnload.Name = "btnMoveBreakTransferRightUnload";
            this.btnMoveBreakTransferRightUnload.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTransferRightUnload.TabIndex = 50;
            this.btnMoveBreakTransferRightUnload.Text = "Right Unload";
            this.btnMoveBreakTransferRightUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTransferLeftUnload
            // 
            this.btnMoveBreakTransferLeftUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTransferLeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTransferLeftUnload.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTransferLeftUnload.Location = new System.Drawing.Point(7, 104);
            this.btnMoveBreakTransferLeftUnload.Name = "btnMoveBreakTransferLeftUnload";
            this.btnMoveBreakTransferLeftUnload.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTransferLeftUnload.TabIndex = 49;
            this.btnMoveBreakTransferLeftUnload.Text = "Left Unload";
            this.btnMoveBreakTransferLeftUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTransferRightLoad
            // 
            this.btnMoveBreakTransferRightLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTransferRightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTransferRightLoad.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTransferRightLoad.Location = new System.Drawing.Point(83, 31);
            this.btnMoveBreakTransferRightLoad.Name = "btnMoveBreakTransferRightLoad";
            this.btnMoveBreakTransferRightLoad.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTransferRightLoad.TabIndex = 48;
            this.btnMoveBreakTransferRightLoad.Text = " Right   Load";
            this.btnMoveBreakTransferRightLoad.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTransferLeftLoad
            // 
            this.btnMoveBreakTransferLeftLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveBreakTransferLeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveBreakTransferLeftLoad.ForeColor = System.Drawing.Color.Black;
            this.btnMoveBreakTransferLeftLoad.Location = new System.Drawing.Point(7, 31);
            this.btnMoveBreakTransferLeftLoad.Name = "btnMoveBreakTransferLeftLoad";
            this.btnMoveBreakTransferLeftLoad.Size = new System.Drawing.Size(70, 67);
            this.btnMoveBreakTransferLeftLoad.TabIndex = 47;
            this.btnMoveBreakTransferLeftLoad.Text = "  Left    Load";
            this.btnMoveBreakTransferLeftLoad.UseVisualStyleBackColor = false;
            // 
            // lblBreakTransfer
            // 
            this.lblBreakTransfer.AutoEllipsis = true;
            this.lblBreakTransfer.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakTransfer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakTransfer.ForeColor = System.Drawing.Color.Black;
            this.lblBreakTransfer.Location = new System.Drawing.Point(0, 0);
            this.lblBreakTransfer.Name = "lblBreakTransfer";
            this.lblBreakTransfer.Size = new System.Drawing.Size(161, 20);
            this.lblBreakTransfer.TabIndex = 9;
            this.lblBreakTransfer.Text = "■ BreakTransfer";
            this.lblBreakTransfer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.btnMoveProcessRightUnload);
            this.panel24.Controls.Add(this.btnMoveProcessLeftUnload);
            this.panel24.Controls.Add(this.btnMoveProcessRightLoad);
            this.panel24.Controls.Add(this.btnMoveProcessLeftLoad);
            this.panel24.Controls.Add(this.lblMoveProcess);
            this.panel24.Location = new System.Drawing.Point(5, 29);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(163, 185);
            this.panel24.TabIndex = 464;
            // 
            // btnMoveProcessRightUnload
            // 
            this.btnMoveProcessRightUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveProcessRightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveProcessRightUnload.ForeColor = System.Drawing.Color.Black;
            this.btnMoveProcessRightUnload.Location = new System.Drawing.Point(84, 104);
            this.btnMoveProcessRightUnload.Name = "btnMoveProcessRightUnload";
            this.btnMoveProcessRightUnload.Size = new System.Drawing.Size(70, 67);
            this.btnMoveProcessRightUnload.TabIndex = 50;
            this.btnMoveProcessRightUnload.Text = "Right Unload";
            this.btnMoveProcessRightUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveProcessLeftUnload
            // 
            this.btnMoveProcessLeftUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveProcessLeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveProcessLeftUnload.ForeColor = System.Drawing.Color.Black;
            this.btnMoveProcessLeftUnload.Location = new System.Drawing.Point(8, 104);
            this.btnMoveProcessLeftUnload.Name = "btnMoveProcessLeftUnload";
            this.btnMoveProcessLeftUnload.Size = new System.Drawing.Size(70, 67);
            this.btnMoveProcessLeftUnload.TabIndex = 49;
            this.btnMoveProcessLeftUnload.Text = "Left Unload";
            this.btnMoveProcessLeftUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveProcessRightLoad
            // 
            this.btnMoveProcessRightLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveProcessRightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveProcessRightLoad.ForeColor = System.Drawing.Color.Black;
            this.btnMoveProcessRightLoad.Location = new System.Drawing.Point(84, 31);
            this.btnMoveProcessRightLoad.Name = "btnMoveProcessRightLoad";
            this.btnMoveProcessRightLoad.Size = new System.Drawing.Size(70, 67);
            this.btnMoveProcessRightLoad.TabIndex = 48;
            this.btnMoveProcessRightLoad.Text = " Right   Load";
            this.btnMoveProcessRightLoad.UseVisualStyleBackColor = false;
            // 
            // btnMoveProcessLeftLoad
            // 
            this.btnMoveProcessLeftLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnMoveProcessLeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveProcessLeftLoad.ForeColor = System.Drawing.Color.Black;
            this.btnMoveProcessLeftLoad.Location = new System.Drawing.Point(8, 31);
            this.btnMoveProcessLeftLoad.Name = "btnMoveProcessLeftLoad";
            this.btnMoveProcessLeftLoad.Size = new System.Drawing.Size(70, 67);
            this.btnMoveProcessLeftLoad.TabIndex = 47;
            this.btnMoveProcessLeftLoad.Text = "  Left    Load";
            this.btnMoveProcessLeftLoad.UseVisualStyleBackColor = false;
            // 
            // lblMoveProcess
            // 
            this.lblMoveProcess.AutoEllipsis = true;
            this.lblMoveProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMoveProcess.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMoveProcess.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMoveProcess.ForeColor = System.Drawing.Color.Black;
            this.lblMoveProcess.Location = new System.Drawing.Point(0, 0);
            this.lblMoveProcess.Name = "lblMoveProcess";
            this.lblMoveProcess.Size = new System.Drawing.Size(161, 20);
            this.lblMoveProcess.TabIndex = 9;
            this.lblMoveProcess.Text = "■ Process";
            this.lblMoveProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMove
            // 
            this.lblMove.AutoEllipsis = true;
            this.lblMove.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMove.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMove.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMove.ForeColor = System.Drawing.Color.Black;
            this.lblMove.Location = new System.Drawing.Point(0, 0);
            this.lblMove.Name = "lblMove";
            this.lblMove.Size = new System.Drawing.Size(518, 26);
            this.lblMove.TabIndex = 9;
            this.lblMove.Text = "■ Move";
            this.lblMove.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txtEtcTimeShot);
            this.panel9.Controls.Add(this.btnEtcTimeShot);
            this.panel9.Controls.Add(this.lblEtcTimeShot);
            this.panel9.Controls.Add(this.btnEtcOneProcess);
            this.panel9.Controls.Add(this.btnEtcRepeatMove);
            this.panel9.Controls.Add(this.panel19);
            this.panel9.Controls.Add(this.lblEtc);
            this.panel9.Location = new System.Drawing.Point(820, 238);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(520, 208);
            this.panel9.TabIndex = 463;
            // 
            // txtEtcTimeShot
            // 
            this.txtEtcTimeShot.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtEtcTimeShot.Location = new System.Drawing.Point(165, 65);
            this.txtEtcTimeShot.Name = "txtEtcTimeShot";
            this.txtEtcTimeShot.Size = new System.Drawing.Size(91, 25);
            this.txtEtcTimeShot.TabIndex = 471;
            // 
            // btnEtcTimeShot
            // 
            this.btnEtcTimeShot.BackColor = System.Drawing.SystemColors.Control;
            this.btnEtcTimeShot.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEtcTimeShot.ForeColor = System.Drawing.Color.Black;
            this.btnEtcTimeShot.Location = new System.Drawing.Point(261, 65);
            this.btnEtcTimeShot.Name = "btnEtcTimeShot";
            this.btnEtcTimeShot.Size = new System.Drawing.Size(49, 25);
            this.btnEtcTimeShot.TabIndex = 470;
            this.btnEtcTimeShot.Text = "Shot";
            this.btnEtcTimeShot.UseVisualStyleBackColor = false;
            // 
            // lblEtcTimeShot
            // 
            this.lblEtcTimeShot.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEtcTimeShot.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEtcTimeShot.ForeColor = System.Drawing.Color.Black;
            this.lblEtcTimeShot.Location = new System.Drawing.Point(14, 65);
            this.lblEtcTimeShot.Name = "lblEtcTimeShot";
            this.lblEtcTimeShot.Size = new System.Drawing.Size(145, 25);
            this.lblEtcTimeShot.TabIndex = 467;
            this.lblEtcTimeShot.Text = "Time Shot[s]";
            this.lblEtcTimeShot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEtcOneProcess
            // 
            this.btnEtcOneProcess.BackColor = System.Drawing.SystemColors.Control;
            this.btnEtcOneProcess.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEtcOneProcess.ForeColor = System.Drawing.Color.Black;
            this.btnEtcOneProcess.Location = new System.Drawing.Point(165, 114);
            this.btnEtcOneProcess.Name = "btnEtcOneProcess";
            this.btnEtcOneProcess.Size = new System.Drawing.Size(145, 41);
            this.btnEtcOneProcess.TabIndex = 469;
            this.btnEtcOneProcess.Text = "One Process";
            this.btnEtcOneProcess.UseVisualStyleBackColor = false;
            // 
            // btnEtcRepeatMove
            // 
            this.btnEtcRepeatMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnEtcRepeatMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEtcRepeatMove.ForeColor = System.Drawing.Color.Black;
            this.btnEtcRepeatMove.Location = new System.Drawing.Point(14, 114);
            this.btnEtcRepeatMove.Name = "btnEtcRepeatMove";
            this.btnEtcRepeatMove.Size = new System.Drawing.Size(145, 41);
            this.btnEtcRepeatMove.TabIndex = 468;
            this.btnEtcRepeatMove.Text = "Repeat Move";
            this.btnEtcRepeatMove.UseVisualStyleBackColor = false;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.btnEtcShutterClose);
            this.panel19.Controls.Add(this.btnEtcShutterOpen);
            this.panel19.Controls.Add(this.lblShutter);
            this.panel19.Location = new System.Drawing.Point(319, 29);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(194, 174);
            this.panel19.TabIndex = 466;
            // 
            // btnEtcShutterClose
            // 
            this.btnEtcShutterClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnEtcShutterClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEtcShutterClose.ForeColor = System.Drawing.Color.Black;
            this.btnEtcShutterClose.Location = new System.Drawing.Point(99, 56);
            this.btnEtcShutterClose.Name = "btnEtcShutterClose";
            this.btnEtcShutterClose.Size = new System.Drawing.Size(90, 80);
            this.btnEtcShutterClose.TabIndex = 62;
            this.btnEtcShutterClose.Text = "Shutter Close";
            this.btnEtcShutterClose.UseVisualStyleBackColor = false;
            // 
            // btnEtcShutterOpen
            // 
            this.btnEtcShutterOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnEtcShutterOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEtcShutterOpen.ForeColor = System.Drawing.Color.Black;
            this.btnEtcShutterOpen.Location = new System.Drawing.Point(3, 55);
            this.btnEtcShutterOpen.Name = "btnEtcShutterOpen";
            this.btnEtcShutterOpen.Size = new System.Drawing.Size(90, 80);
            this.btnEtcShutterOpen.TabIndex = 61;
            this.btnEtcShutterOpen.Text = "Shutter Open";
            this.btnEtcShutterOpen.UseVisualStyleBackColor = false;
            // 
            // lblShutter
            // 
            this.lblShutter.AutoEllipsis = true;
            this.lblShutter.BackColor = System.Drawing.Color.Gainsboro;
            this.lblShutter.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblShutter.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblShutter.ForeColor = System.Drawing.Color.Black;
            this.lblShutter.Location = new System.Drawing.Point(0, 0);
            this.lblShutter.Name = "lblShutter";
            this.lblShutter.Size = new System.Drawing.Size(192, 20);
            this.lblShutter.TabIndex = 9;
            this.lblShutter.Text = "■ Shutter";
            this.lblShutter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEtc
            // 
            this.lblEtc.AutoEllipsis = true;
            this.lblEtc.BackColor = System.Drawing.Color.Gainsboro;
            this.lblEtc.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblEtc.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblEtc.ForeColor = System.Drawing.Color.Black;
            this.lblEtc.Location = new System.Drawing.Point(0, 0);
            this.lblEtc.Name = "lblEtc";
            this.lblEtc.Size = new System.Drawing.Size(518, 26);
            this.lblEtc.TabIndex = 9;
            this.lblEtc.Text = "■ Etc";
            this.lblEtc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Controls.Add(this.btnAlignTestInsp);
            this.panel20.Controls.Add(this.txtTotalInspCount);
            this.panel20.Controls.Add(this.lblTotalInspCount);
            this.panel20.Controls.Add(this.txtInspCount);
            this.panel20.Controls.Add(this.lblInspCount);
            this.panel20.Controls.Add(this.btnAlignPreAlign);
            this.panel20.Controls.Add(this.btnAlignIdle);
            this.panel20.Controls.Add(this.lblAlign);
            this.panel20.Location = new System.Drawing.Point(820, 452);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(520, 215);
            this.panel20.TabIndex = 464;
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.btnAlignIR2);
            this.panel21.Controls.Add(this.btnAlignIR1);
            this.panel21.Controls.Add(this.btnAlignIL2);
            this.panel21.Controls.Add(this.btnAlignIL1);
            this.panel21.Location = new System.Drawing.Point(155, 104);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(225, 84);
            this.panel21.TabIndex = 475;
            // 
            // btnAlignIR2
            // 
            this.btnAlignIR2.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignIR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignIR2.ForeColor = System.Drawing.Color.Black;
            this.btnAlignIR2.Location = new System.Drawing.Point(166, 20);
            this.btnAlignIR2.Name = "btnAlignIR2";
            this.btnAlignIR2.Size = new System.Drawing.Size(41, 41);
            this.btnAlignIR2.TabIndex = 75;
            this.btnAlignIR2.Text = "I   R2";
            this.btnAlignIR2.UseVisualStyleBackColor = false;
            // 
            // btnAlignIR1
            // 
            this.btnAlignIR1.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignIR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignIR1.ForeColor = System.Drawing.Color.Black;
            this.btnAlignIR1.Location = new System.Drawing.Point(116, 20);
            this.btnAlignIR1.Name = "btnAlignIR1";
            this.btnAlignIR1.Size = new System.Drawing.Size(41, 41);
            this.btnAlignIR1.TabIndex = 74;
            this.btnAlignIR1.Text = "I   R1";
            this.btnAlignIR1.UseVisualStyleBackColor = false;
            // 
            // btnAlignIL2
            // 
            this.btnAlignIL2.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignIL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignIL2.ForeColor = System.Drawing.Color.Black;
            this.btnAlignIL2.Location = new System.Drawing.Point(66, 20);
            this.btnAlignIL2.Name = "btnAlignIL2";
            this.btnAlignIL2.Size = new System.Drawing.Size(41, 41);
            this.btnAlignIL2.TabIndex = 73;
            this.btnAlignIL2.Text = "I   L2";
            this.btnAlignIL2.UseVisualStyleBackColor = false;
            // 
            // btnAlignIL1
            // 
            this.btnAlignIL1.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignIL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignIL1.ForeColor = System.Drawing.Color.Black;
            this.btnAlignIL1.Location = new System.Drawing.Point(16, 20);
            this.btnAlignIL1.Name = "btnAlignIL1";
            this.btnAlignIL1.Size = new System.Drawing.Size(41, 41);
            this.btnAlignIL1.TabIndex = 72;
            this.btnAlignIL1.Text = "I   L1";
            this.btnAlignIL1.UseVisualStyleBackColor = false;
            // 
            // btnAlignTestInsp
            // 
            this.btnAlignTestInsp.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignTestInsp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignTestInsp.ForeColor = System.Drawing.Color.Black;
            this.btnAlignTestInsp.Location = new System.Drawing.Point(385, 125);
            this.btnAlignTestInsp.Name = "btnAlignTestInsp";
            this.btnAlignTestInsp.Size = new System.Drawing.Size(119, 41);
            this.btnAlignTestInsp.TabIndex = 474;
            this.btnAlignTestInsp.Text = "TestInsp";
            this.btnAlignTestInsp.UseVisualStyleBackColor = false;
            // 
            // txtTotalInspCount
            // 
            this.txtTotalInspCount.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtTotalInspCount.Location = new System.Drawing.Point(435, 64);
            this.txtTotalInspCount.Name = "txtTotalInspCount";
            this.txtTotalInspCount.Size = new System.Drawing.Size(63, 25);
            this.txtTotalInspCount.TabIndex = 473;
            // 
            // lblTotalInspCount
            // 
            this.lblTotalInspCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTotalInspCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTotalInspCount.ForeColor = System.Drawing.Color.Black;
            this.lblTotalInspCount.Location = new System.Drawing.Point(338, 64);
            this.lblTotalInspCount.Name = "lblTotalInspCount";
            this.lblTotalInspCount.Size = new System.Drawing.Size(91, 24);
            this.lblTotalInspCount.TabIndex = 472;
            this.lblTotalInspCount.Text = "TotalInspCnt";
            this.lblTotalInspCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspCount
            // 
            this.txtInspCount.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtInspCount.Location = new System.Drawing.Point(269, 64);
            this.txtInspCount.Name = "txtInspCount";
            this.txtInspCount.Size = new System.Drawing.Size(63, 25);
            this.txtInspCount.TabIndex = 471;
            // 
            // lblInspCount
            // 
            this.lblInspCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInspCount.ForeColor = System.Drawing.Color.Black;
            this.lblInspCount.Location = new System.Drawing.Point(172, 65);
            this.lblInspCount.Name = "lblInspCount";
            this.lblInspCount.Size = new System.Drawing.Size(91, 24);
            this.lblInspCount.TabIndex = 467;
            this.lblInspCount.Text = "InspCount";
            this.lblInspCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAlignPreAlign
            // 
            this.btnAlignPreAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignPreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignPreAlign.ForeColor = System.Drawing.Color.Black;
            this.btnAlignPreAlign.Location = new System.Drawing.Point(14, 125);
            this.btnAlignPreAlign.Name = "btnAlignPreAlign";
            this.btnAlignPreAlign.Size = new System.Drawing.Size(135, 41);
            this.btnAlignPreAlign.TabIndex = 469;
            this.btnAlignPreAlign.Text = "PreAlign";
            this.btnAlignPreAlign.UseVisualStyleBackColor = false;
            // 
            // btnAlignIdle
            // 
            this.btnAlignIdle.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignIdle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignIdle.ForeColor = System.Drawing.Color.Black;
            this.btnAlignIdle.Location = new System.Drawing.Point(14, 57);
            this.btnAlignIdle.Name = "btnAlignIdle";
            this.btnAlignIdle.Size = new System.Drawing.Size(135, 41);
            this.btnAlignIdle.TabIndex = 468;
            this.btnAlignIdle.Text = "Idle";
            this.btnAlignIdle.UseVisualStyleBackColor = false;
            // 
            // lblAlign
            // 
            this.lblAlign.AutoEllipsis = true;
            this.lblAlign.BackColor = System.Drawing.Color.Gainsboro;
            this.lblAlign.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblAlign.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblAlign.ForeColor = System.Drawing.Color.Black;
            this.lblAlign.Location = new System.Drawing.Point(0, 0);
            this.lblAlign.Name = "lblAlign";
            this.lblAlign.Size = new System.Drawing.Size(518, 26);
            this.lblAlign.TabIndex = 9;
            this.lblAlign.Text = "■ Align";
            this.lblAlign.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmManager_Process
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 729);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmManager_Process";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "메뉴얼 제어";
            this.panel30.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel30;
        internal System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label lblProcessB1;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label lblProcessA1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnProcessVision;
        private System.Windows.Forms.Button btnProcessCurrent;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnProcessBProcessR2;
        private System.Windows.Forms.Button btnProcessBProcessR3;
        private System.Windows.Forms.Button btnProcessBProcessR1;
        internal System.Windows.Forms.Label lblProcessB3;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnProcessAProcessL2;
        private System.Windows.Forms.Button btnProcessAProcessL3;
        private System.Windows.Forms.Button btnProcessAProcessL1;
        internal System.Windows.Forms.Label lblProcessA3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnProcessBAlignR2;
        private System.Windows.Forms.Button btnProcessBAlignR3;
        private System.Windows.Forms.Button btnProcessBAlignR1;
        internal System.Windows.Forms.Label lblProcessB2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnProcessAAlignL2;
        private System.Windows.Forms.Button btnProcessAAlignL3;
        private System.Windows.Forms.Button btnProcessAAlignL1;
        internal System.Windows.Forms.Label lblProcessA2;
        private System.Windows.Forms.Button btnProcessBR2BlowOn;
        private System.Windows.Forms.Button btnProcessBR2VacOn;
        private System.Windows.Forms.Button btnProcessBR1BlowOn;
        private System.Windows.Forms.Button btnProcessBR1VacOn;
        private System.Windows.Forms.Button btnProcessAL2BlowOn;
        private System.Windows.Forms.Button btnProcessAL2VacOn;
        private System.Windows.Forms.Button btnProcessAL1BlowOn;
        private System.Windows.Forms.Button btnProcessAL1VacOn;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnBreakBProcessR2;
        private System.Windows.Forms.Button btnBreakBProcessR3;
        private System.Windows.Forms.Button btnBreakBProcessR1;
        internal System.Windows.Forms.Label lblBreakB3;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnBreakAProcessL2;
        private System.Windows.Forms.Button btnBreakAProcessL3;
        private System.Windows.Forms.Button btnBreakAProcessL1;
        internal System.Windows.Forms.Label lblBreakA3;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnBreakBAlignR2;
        private System.Windows.Forms.Button btnBreakBAlignR3;
        private System.Windows.Forms.Button btnBreakBAlignR1;
        internal System.Windows.Forms.Label lblBreakB2;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btnBreakAAlignL2;
        private System.Windows.Forms.Button btnBreakAAlignL3;
        private System.Windows.Forms.Button btnBreakAAlignL1;
        internal System.Windows.Forms.Label lblBreakA2;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnBreakBR2BlowOn;
        private System.Windows.Forms.Button btnBreakBR2VacOn;
        private System.Windows.Forms.Button btnBreakBR1BlowOn;
        private System.Windows.Forms.Button btnBreakBR1VacOn;
        internal System.Windows.Forms.Label lblBreakB1;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnBreakAL2BlowOn;
        private System.Windows.Forms.Button btnBreakAL2VacOn;
        private System.Windows.Forms.Button btnBreakAL1BlowOn;
        private System.Windows.Forms.Button btnBreakAL1VacOn;
        internal System.Windows.Forms.Label lblBreakA1;
        internal System.Windows.Forms.Label lblBreak;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnMoveBreakTableRightUnload;
        private System.Windows.Forms.Button btnMoveBreakTableLeftUnload;
        private System.Windows.Forms.Button btnMoveBreakTableRightLoad;
        private System.Windows.Forms.Button btnMoveBreakTableLeftLoad;
        internal System.Windows.Forms.Label lblBreakTable;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnMoveBreakTransferRightUnload;
        private System.Windows.Forms.Button btnMoveBreakTransferLeftUnload;
        private System.Windows.Forms.Button btnMoveBreakTransferRightLoad;
        private System.Windows.Forms.Button btnMoveBreakTransferLeftLoad;
        internal System.Windows.Forms.Label lblBreakTransfer;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Button btnMoveProcessRightUnload;
        private System.Windows.Forms.Button btnMoveProcessLeftUnload;
        private System.Windows.Forms.Button btnMoveProcessRightLoad;
        private System.Windows.Forms.Button btnMoveProcessLeftLoad;
        internal System.Windows.Forms.Label lblMoveProcess;
        internal System.Windows.Forms.Label lblMove;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtEtcTimeShot;
        private System.Windows.Forms.Button btnEtcTimeShot;
        private System.Windows.Forms.Label lblEtcTimeShot;
        private System.Windows.Forms.Button btnEtcOneProcess;
        private System.Windows.Forms.Button btnEtcRepeatMove;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnEtcShutterClose;
        private System.Windows.Forms.Button btnEtcShutterOpen;
        internal System.Windows.Forms.Label lblShutter;
        internal System.Windows.Forms.Label lblEtc;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnAlignIR2;
        private System.Windows.Forms.Button btnAlignIR1;
        private System.Windows.Forms.Button btnAlignIL2;
        private System.Windows.Forms.Button btnAlignIL1;
        private System.Windows.Forms.Button btnAlignTestInsp;
        private System.Windows.Forms.TextBox txtTotalInspCount;
        private System.Windows.Forms.Label lblTotalInspCount;
        private System.Windows.Forms.TextBox txtInspCount;
        private System.Windows.Forms.Label lblInspCount;
        private System.Windows.Forms.Button btnAlignPreAlign;
        private System.Windows.Forms.Button btnAlignIdle;
        internal System.Windows.Forms.Label lblAlign;
        private System.Windows.Forms.Button btnProcessBR2BlowOff;
        private System.Windows.Forms.Button btnProcessBR1BlowOff;
        private System.Windows.Forms.Button btnProcessBR2VacOff;
        private System.Windows.Forms.Button btnProcessBR1VacOff;
        private System.Windows.Forms.Button btnProcessAL2BlowOff;
        private System.Windows.Forms.Button btnProcessAL1BlowOff;
        private System.Windows.Forms.Button btnProcessAL2VacOff;
        private System.Windows.Forms.Button btnProcessAL1VacOff;
        private System.Windows.Forms.Button btnBreakBR2BlowOff;
        private System.Windows.Forms.Button btnBreakBR1BlowOff;
        private System.Windows.Forms.Button btnBreakBR2VacOff;
        private System.Windows.Forms.Button btnBreakBR1VacOff;
        private System.Windows.Forms.Button btnBreakAL2BlowOff;
        private System.Windows.Forms.Button btnBreakAL1BlowOff;
        private System.Windows.Forms.Button btnBreakAL2VacOff;
        private System.Windows.Forms.Button btnBreakAL1VacOff;
    }
}
﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmCstInfo : Form
    {
        private PortInfo port;
        public FrmCstInfo(PortInfo _port)
        {
            InitializeComponent();

            port = _port;

            FillUi();
        }

        private void FillUi()
        {
            lblCstName.Text = port.Name;

            comboPortStatus.Items.Add("EMPTY");
            comboPortStatus.Items.Add("IDLE");
            comboPortStatus.Items.Add("WAIT");
            comboPortStatus.Items.Add("BUSY");

            comboPortStatus.SelectedItem = string.Format("{0}", port.Status);

            txtCstSlotCount.Text = string.Format("{0}", port.Cst.SlotCount);
            txtCstCellCount.Text = string.Format("{0}", port.Cst.CellCount);
            txtCstInCount.Text = string.Format("{0}", port.Cst.InCount);
            txtCstOutCount.Text = string.Format("{0}", port.Cst.OutCount);
            txtCstPPID.Text = string.Format("{0}", port.Cst.PPID);
        }

        private void btnCstApply_Click(object sender, EventArgs e)
        {
            if(comboPortStatus.Items.Contains(comboPortStatus.Text))
            {
                port.Status = comboPortStatus.Text == "EMPTY" ? EmPortStatus.EMPTY :
                    comboPortStatus.Text == "IDLE" ? EmPortStatus.IDLE :
                    comboPortStatus.Text == "WAIT" ? EmPortStatus.WAIT :
                    comboPortStatus.Text == "BUSY" ? EmPortStatus.BUSY : EmPortStatus.EMPTY;
            }
            if(txtCstSlotCount.Text != string.Empty) port.Cst.SlotCount = int.Parse(txtCstSlotCount.Text);
            if(txtCstCellCount.Text != string.Empty) port.Cst.CellCount = int.Parse(txtCstCellCount.Text);
        }

        private void btnCstCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

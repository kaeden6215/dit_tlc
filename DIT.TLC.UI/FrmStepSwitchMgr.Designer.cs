﻿namespace DIT.TLC.UI
{
    partial class FrmStepSwitchMgr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dgvSeqStepMgr = new System.Windows.Forms.DataGridView();
            this.clUnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HomeStep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Accel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pos = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label241 = new System.Windows.Forms.Label();
            this.tmrUiupdate = new System.Windows.Forms.Timer(this.components);
            this.btnALine = new System.Windows.Forms.Button();
            this.btnBLine = new System.Windows.Forms.Button();
            this.btnTopmost = new System.Windows.Forms.Button();
            this.btnWorkSeq = new System.Windows.Forms.Button();
            this.btnHomeSeq = new System.Windows.Forms.Button();
            this.btnAllSeq = new System.Windows.Forms.Button();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSeqStepMgr)).BeginInit();
            this.SuspendLayout();
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.dgvSeqStepMgr);
            this.panel6.Controls.Add(this.label241);
            this.panel6.Location = new System.Drawing.Point(9, 119);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(779, 435);
            this.panel6.TabIndex = 97;
            // 
            // dgvSeqStepMgr
            // 
            this.dgvSeqStepMgr.AllowUserToResizeColumns = false;
            this.dgvSeqStepMgr.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSeqStepMgr.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSeqStepMgr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSeqStepMgr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clUnitName,
            this.posName,
            this.HomeStep,
            this.Accel,
            this.pos});
            this.dgvSeqStepMgr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSeqStepMgr.Location = new System.Drawing.Point(0, 33);
            this.dgvSeqStepMgr.MultiSelect = false;
            this.dgvSeqStepMgr.Name = "dgvSeqStepMgr";
            this.dgvSeqStepMgr.RowHeadersVisible = false;
            this.dgvSeqStepMgr.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvSeqStepMgr.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSeqStepMgr.RowTemplate.Height = 23;
            this.dgvSeqStepMgr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvSeqStepMgr.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvSeqStepMgr.Size = new System.Drawing.Size(777, 400);
            this.dgvSeqStepMgr.TabIndex = 10;
            this.dgvSeqStepMgr.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSeqStepMgr_CellClick);
            // 
            // clUnitName
            // 
            this.clUnitName.HeaderText = "UNIT 명";
            this.clUnitName.Name = "clUnitName";
            this.clUnitName.ReadOnly = true;
            this.clUnitName.Width = 200;
            // 
            // posName
            // 
            this.posName.HeaderText = "스템 명";
            this.posName.MinimumWidth = 190;
            this.posName.Name = "posName";
            this.posName.ReadOnly = true;
            this.posName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.posName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.posName.Width = 190;
            // 
            // HomeStep
            // 
            this.HomeStep.HeaderText = "HOME STEP";
            this.HomeStep.Name = "HomeStep";
            this.HomeStep.Width = 150;
            // 
            // Accel
            // 
            this.Accel.HeaderText = "WORK STEP";
            this.Accel.MinimumWidth = 65;
            this.Accel.Name = "Accel";
            this.Accel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Accel.Width = 150;
            // 
            // pos
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.pos.DefaultCellStyle = dataGridViewCellStyle2;
            this.pos.HeaderText = "ON/OFF";
            this.pos.MinimumWidth = 75;
            this.pos.Name = "pos";
            this.pos.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.pos.Width = 75;
            // 
            // label241
            // 
            this.label241.AutoEllipsis = true;
            this.label241.BackColor = System.Drawing.Color.Gainsboro;
            this.label241.Dock = System.Windows.Forms.DockStyle.Top;
            this.label241.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.label241.ForeColor = System.Drawing.Color.Black;
            this.label241.Location = new System.Drawing.Point(0, 0);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(777, 33);
            this.label241.TabIndex = 9;
            this.label241.Text = "■ 시퀀스 스텝 관리";
            this.label241.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrUiupdate
            // 
            this.tmrUiupdate.Tick += new System.EventHandler(this.tmrUiupdate_Tick);
            // 
            // btnALine
            // 
            this.btnALine.Location = new System.Drawing.Point(10, 1);
            this.btnALine.Name = "btnALine";
            this.btnALine.Size = new System.Drawing.Size(306, 62);
            this.btnALine.TabIndex = 98;
            this.btnALine.Text = "A열";
            this.btnALine.UseVisualStyleBackColor = true;
            this.btnALine.Click += new System.EventHandler(this.btnALine_Click);
            // 
            // btnBLine
            // 
            this.btnBLine.Location = new System.Drawing.Point(472, 1);
            this.btnBLine.Name = "btnBLine";
            this.btnBLine.Size = new System.Drawing.Size(316, 62);
            this.btnBLine.TabIndex = 98;
            this.btnBLine.Text = "B열";
            this.btnBLine.UseVisualStyleBackColor = true;
            this.btnBLine.Click += new System.EventHandler(this.btnBLine_Click);
            // 
            // btnTopmost
            // 
            this.btnTopmost.Location = new System.Drawing.Point(322, 1);
            this.btnTopmost.Name = "btnTopmost";
            this.btnTopmost.Size = new System.Drawing.Size(144, 62);
            this.btnTopmost.TabIndex = 98;
            this.btnTopmost.Text = "Top Most";
            this.btnTopmost.UseVisualStyleBackColor = true;
            this.btnTopmost.Click += new System.EventHandler(this.btnTopmost_Click);
            // 
            // btnWorkSeq
            // 
            this.btnWorkSeq.Location = new System.Drawing.Point(660, 69);
            this.btnWorkSeq.Name = "btnWorkSeq";
            this.btnWorkSeq.Size = new System.Drawing.Size(128, 44);
            this.btnWorkSeq.TabIndex = 98;
            this.btnWorkSeq.Text = "구동 시퀀스";
            this.btnWorkSeq.UseVisualStyleBackColor = true;            
            // 
            // btnHomeSeq
            // 
            this.btnHomeSeq.Location = new System.Drawing.Point(526, 69);
            this.btnHomeSeq.Name = "btnHomeSeq";
            this.btnHomeSeq.Size = new System.Drawing.Size(128, 44);
            this.btnHomeSeq.TabIndex = 98;
            this.btnHomeSeq.Text = "홈 시퀀스";
            this.btnHomeSeq.UseVisualStyleBackColor = true;
            this.btnHomeSeq.Click += new System.EventHandler(this.btnHomeSeq_Click);
            // 
            // btnAllSeq
            // 
            this.btnAllSeq.Location = new System.Drawing.Point(392, 69);
            this.btnAllSeq.Name = "btnAllSeq";
            this.btnAllSeq.Size = new System.Drawing.Size(128, 44);
            this.btnAllSeq.TabIndex = 98;
            this.btnAllSeq.Text = "전체";
            this.btnAllSeq.UseVisualStyleBackColor = true;
            this.btnAllSeq.Click += new System.EventHandler(this.btnAllSeq_Click);
            // 
            // FrmStepSwitchMgr
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(794, 558);
            this.Controls.Add(this.btnAllSeq);
            this.Controls.Add(this.btnHomeSeq);
            this.Controls.Add(this.btnWorkSeq);
            this.Controls.Add(this.btnBLine);
            this.Controls.Add(this.btnTopmost);
            this.Controls.Add(this.btnALine);
            this.Controls.Add(this.panel6);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "FrmStepSwitchMgr";
            this.Text = "스텝 스위치";
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSeqStepMgr)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView dgvSeqStepMgr;
        internal System.Windows.Forms.Label label241;
        private System.Windows.Forms.Timer tmrUiupdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clUnitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn posName;
        private System.Windows.Forms.DataGridViewTextBoxColumn HomeStep;
        private System.Windows.Forms.DataGridViewTextBoxColumn Accel;
        private System.Windows.Forms.DataGridViewButtonColumn pos;
        private System.Windows.Forms.Button btnALine;
        private System.Windows.Forms.Button btnBLine;
        private System.Windows.Forms.Button btnTopmost;
        private System.Windows.Forms.Button btnWorkSeq;
        private System.Windows.Forms.Button btnHomeSeq;
        private System.Windows.Forms.Button btnAllSeq;
    }
}
﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmManager_Process : Form, IUIUpdate
    {
        public FrmManager_Process()
        {
            InitializeComponent();
        }


        private void btnProcess_Click(object sender, EventArgs e)
        {
            //A VACCUM
            if (btnProcessAL1VacOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Vacuum1Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_A.Vacuum1Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessAL1VacOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Vacuum1Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_A.Vacuum1Ch2.OnOff(GG.Equip, false);
            }

            else if (btnProcessAL2VacOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Vacuum2Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_A.Vacuum2Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessAL2VacOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Vacuum2Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_A.Vacuum2Ch2.OnOff(GG.Equip, false);
            }

            //blower
            else if (btnProcessAL1BlowOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Blower1Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_A.Blower1Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessAL1BlowOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Blower1Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_A.Blower1Ch2.OnOff(GG.Equip, false);
            }

            else if (btnProcessAL2BlowOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Blower2Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_A.Blower2Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessAL2BlowOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_A.Blower2Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_A.Blower2Ch2.OnOff(GG.Equip, false);
            }

            //B VACCUM
            if (btnProcessBR1VacOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Vacuum1Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_B.Vacuum1Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessBR1VacOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Vacuum1Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_B.Vacuum1Ch2.OnOff(GG.Equip, false);
            }

            else if (btnProcessBR2VacOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Vacuum2Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_B.Vacuum2Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessBR2VacOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Vacuum2Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_B.Vacuum2Ch2.OnOff(GG.Equip, false);
            }

            //blower
            else if (btnProcessBR1BlowOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Blower1Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_B.Blower1Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessBR1BlowOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Blower1Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_B.Blower1Ch2.OnOff(GG.Equip, false);
            }
            else if (btnProcessBR2BlowOn == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Blower2Ch1.OnOff(GG.Equip, true);
                GG.Equip.PROC.IRCutStage_B.Blower2Ch2.OnOff(GG.Equip, true);
            }
            else if (btnProcessBR2BlowOff == (Button)sender)
            {
                GG.Equip.PROC.IRCutStage_B.Blower2Ch1.OnOff(GG.Equip, false);
                GG.Equip.PROC.IRCutStage_B.Blower2Ch2.OnOff(GG.Equip, false);
            }
            UIUpdate();
        }

        private void btnBreak_Click(object sender, EventArgs e)
        {
            if (btnBreakAL1VacOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Vaccum1.OnOff(GG.Equip, true);
            }
            else if (btnBreakAL1VacOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Vaccum1.OnOff(GG.Equip, false);
            }

            else if (btnBreakAL2VacOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnBreakAL2VacOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Vaccum2.OnOff(GG.Equip, false);
            }

            //blower
            else if (btnBreakAL1BlowOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Blower1.OnOff(GG.Equip, true);
            }
            else if (btnBreakAL1BlowOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Blower1.OnOff(GG.Equip, false);
            }

            else if (btnBreakAL2BlowOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnBreakAL2BlowOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.Blower2.OnOff(GG.Equip, false);
            }

            //B VACCUM
            if (btnBreakBR1VacOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Vaccum1.OnOff(GG.Equip, true);
            }
            else if (btnBreakBR1VacOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Vaccum1.OnOff(GG.Equip, false);
            }

            else if (btnBreakBR2VacOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnBreakBR2VacOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Vaccum2.OnOff(GG.Equip, false);
            }

            //blower
            else if (btnBreakBR1BlowOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Blower1.OnOff(GG.Equip, true);
            }
            else if (btnBreakBR1BlowOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Blower1.OnOff(GG.Equip, false);
            }

            else if (btnBreakBR2BlowOn == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnBreakBR2BlowOff == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.Blower2.OnOff(GG.Equip, false);
            }
            UIUpdate();
        }

        public void UIUpdate()
        {
            SetColor(btnProcessAL1VacOn,  (GG.Equip.PROC.IRCutStage_A.Vacuum1Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_A.Vacuum1Ch2.IsOnOff == true));
            SetColor(btnProcessAL1VacOff,  GG.Equip.PROC.IRCutStage_A.Vacuum1Ch1.IsOnOff == false        && GG.Equip.PROC.IRCutStage_A.Vacuum1Ch2.IsOnOff == false);
            SetColor(btnProcessAL2VacOn,  (GG.Equip.PROC.IRCutStage_A.Vacuum2Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_A.Vacuum2Ch2.IsOnOff == true));
            SetColor(btnProcessAL2VacOff, (GG.Equip.PROC.IRCutStage_A.Vacuum2Ch1.IsOnOff == false        && GG.Equip.PROC.IRCutStage_A.Vacuum2Ch2.IsOnOff == false));
            SetColor(btnProcessAL1BlowOn, (GG.Equip.PROC.IRCutStage_A.Blower1Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_A.Blower1Ch2.IsOnOff == true));
            SetColor(btnProcessAL1BlowOff,(GG.Equip.PROC.IRCutStage_A.Blower1Ch1.IsOnOff == false        && GG.Equip.PROC.IRCutStage_A.Blower1Ch2.IsOnOff == false));
            SetColor(btnProcessAL2BlowOn, (GG.Equip.PROC.IRCutStage_A.Blower2Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_A.Blower1Ch2.IsOnOff == true));
            SetColor(btnProcessAL2BlowOff,(GG.Equip.PROC.IRCutStage_A.Blower2Ch1.IsOnOff == false        && GG.Equip.PROC.IRCutStage_A.Blower1Ch2.IsOnOff == false));
            SetColor(btnProcessBR1VacOn,  (GG.Equip.PROC.IRCutStage_B.Vacuum1Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_B.Vacuum1Ch2.IsOnOff == true));
            SetColor(btnProcessBR1VacOff, (GG.Equip.PROC.IRCutStage_B.Vacuum1Ch1.IsOnOff == false        && GG.Equip.PROC.IRCutStage_B.Vacuum1Ch2.IsOnOff == false));
            SetColor(btnProcessBR2VacOn,  (GG.Equip.PROC.IRCutStage_B.Vacuum2Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_B.Vacuum2Ch2.IsOnOff == true));
            SetColor(btnProcessBR2VacOff, (GG.Equip.PROC.IRCutStage_B.Vacuum2Ch1.IsOnOff == false        && GG.Equip.PROC.IRCutStage_B.Vacuum2Ch2.IsOnOff == false));
            SetColor(btnProcessBR1BlowOn, (GG.Equip.PROC.IRCutStage_B.Blower1Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_B.Blower1Ch2.IsOnOff == true));
            SetColor(btnProcessBR1BlowOff,(GG.Equip.PROC.IRCutStage_B.Blower1Ch1.IsOnOff == false        && GG.Equip.PROC.IRCutStage_B.Blower1Ch2.IsOnOff == false));
            SetColor(btnProcessBR2BlowOn, (GG.Equip.PROC.IRCutStage_B.Blower2Ch1.IsOnOff == true         && GG.Equip.PROC.IRCutStage_B.Blower2Ch2.IsOnOff == true));
            SetColor(btnProcessBR2BlowOff, (GG.Equip.PROC.IRCutStage_B.Blower2Ch1.IsOnOff == false       && GG.Equip.PROC.IRCutStage_B.Blower2Ch2.IsOnOff == false));
            SetColor(btnBreakAL1VacOn, (GG.Equip.PROC.BreakStage_A.Vaccum1.IsOnOff == true));
            SetColor(btnBreakAL1VacOff, (GG.Equip.PROC.BreakStage_A.Vaccum1.IsOnOff == false));
            SetColor(btnBreakAL2VacOn, (GG.Equip.PROC.BreakStage_A.Vaccum2.IsOnOff == true));
            SetColor(btnBreakAL2VacOff, (GG.Equip.PROC.BreakStage_A.Vaccum2.IsOnOff == false));
            SetColor(btnBreakAL1BlowOn, (GG.Equip.PROC.BreakStage_A.Blower1.IsOnOff == true));
            SetColor(btnBreakAL1BlowOff, (GG.Equip.PROC.BreakStage_A.Blower1.IsOnOff == false));
            SetColor(btnBreakAL2BlowOn, (GG.Equip.PROC.BreakStage_A.Blower2.IsOnOff == true));
            SetColor(btnBreakAL2BlowOff, (GG.Equip.PROC.BreakStage_A.Blower2.IsOnOff == false));
            SetColor(btnBreakBR1VacOn, (GG.Equip.PROC.BreakStage_B.Vaccum1.IsOnOff == true));
            SetColor(btnBreakBR1VacOff, (GG.Equip.PROC.BreakStage_B.Vaccum1.IsOnOff == false));
            SetColor(btnBreakBR2VacOn, (GG.Equip.PROC.BreakStage_B.Vaccum2.IsOnOff == true));
            SetColor(btnBreakBR2VacOff, (GG.Equip.PROC.BreakStage_B.Vaccum2.IsOnOff == false));
            SetColor(btnBreakBR1BlowOn, (GG.Equip.PROC.BreakStage_B.Blower1.IsOnOff == true));
            SetColor(btnBreakBR1BlowOff, (GG.Equip.PROC.BreakStage_B.Blower1.IsOnOff == false));
            SetColor(btnBreakBR2BlowOn, (GG.Equip.PROC.BreakStage_B.Blower2.IsOnOff == true));
            SetColor(btnBreakBR2BlowOff, (GG.Equip.PROC.BreakStage_B.Blower2.IsOnOff == false));
            return;
        }

        public void SetColor(Button btn, bool value)
        {
            btn.BackColor = value ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

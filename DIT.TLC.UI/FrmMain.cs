﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dit.Framework.PLC;
using Dit.Framework.Comm;
using DIT.TLC.CTRL;
using System.IO;
using Dit.Framework.Log;
using DIT.TLC.UI.LOG;
using System.Windows.Forms;
using System.Diagnostics;
using Dit.Framework.PLC._05._PLC;

namespace DIT.TLC.UI
{
    public partial class FrmMain : Form
    {
        private UcrlLoginWindows _ucrlLoginForm = new UcrlLoginWindows();                          // 로그인 UI
        private UcrlMainWindow _ucrlMainForm = new UcrlMainWindow();
        private UcrlRecipeSubMenu _ucrlRecipeSubManu = new UcrlRecipeSubMenu();    // 레시피서브메뉴 UI
        private UcrlManagerSubMenu _ucrlMgrSubManu = new UcrlManagerSubMenu(); // 유지보수서브메뉴 UI
        private UcrlParameterSubMenu _ucrlParameterSubManu = new UcrlParameterSubMenu(); // 파라미터서브메뉴 UI
        private UcrlLogSubMenu _ucrlLogSubMenu = new UcrlLogSubMenu();            // 로그서브메뉴 UI
        private UcrlMeasureWindows _ucrlMeasure = new UcrlMeasureWindows();        // 측정 UI

        private FrmAlarm _frmAlarm = new FrmAlarm();
        private FrmTester _frmTester = new FrmTester();
        private FrmInnerWorkOn _frmInnerWorkOn = new FrmInnerWorkOn();
        public FrmMain()
        {
            InitializeComponent();

            InitalizeUserControlUI();

            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlLoginForm);
            _ucrlLoginForm.Location = new Point(0, 0);

            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        //VirtualUMacDirect vv;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //vv = new VirtualUMacDirect("", "", 0);
            //vv.LstReadAddr.Add(new PlcAddr(PlcMemType.M, 4000, 0, 100));
            //vv.LstReadAddr.Add(new PlcAddr(PlcMemType.M, 4100, 0, 100));
            //vv.LstReadAddr.Add(new PlcAddr(PlcMemType.M, 6200, 0, 100));
            //vv.LstReadAddr.Add(new PlcAddr(PlcMemType.M, 6300, 0, 100));

            //vv.LstWriteAddr.Add(new PlcAddr(PlcMemType.M, 5000, 0, 100));
            //vv.LstWriteAddr.Add(new PlcAddr(PlcMemType.M, 6000, 0, 100));
            //vv.LstWriteAddr.Add(new PlcAddr(PlcMemType.M, 6100, 0, 100));
            //vv.Open();

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            base.OnClosing(e);
        }

        // 메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void MenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = SystemColors.Control;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            GG.Equip.StopLogic();
            Application.Exit();
            Environment.Exit(0);
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlLoginForm);
            _ucrlLoginForm.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnMain_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlMainForm);
            _ucrlMainForm.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnRecipe_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlRecipeSubManu);
            _ucrlRecipeSubManu.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnManager_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlMgrSubManu);
            _ucrlMgrSubManu.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnParam_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlParameterSubManu);
            _ucrlParameterSubManu.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnLog_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlLogSubMenu);
            _ucrlLogSubMenu.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnMeasure_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlMeasure);
            _ucrlMeasure.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnGraph_Click(object sender, EventArgs e)
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "FrmGraph")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Maximized;
                    }
                    openForm.Activate();
                    return;
                }
            }

            FrmGraph ff = new FrmGraph();
            ff.Show();

            MenuButtonColorChange(sender, e);
        }

        FrmInterLock _frmInterLock = new FrmInterLock();
        private void tmrUiWorker_Tick(object sender, EventArgs e)
        {
            digitalTimer1.DigitText = DateTime.Now.ToString("HH:mm:ss");

            foreach (var ctrl in splitContainer3.Panel1.Controls)
            {
                IUIUpdate vv = ctrl as IUIUpdate;
                if (vv != null)
                    vv.UIUpdate();
            }

            lock (InterLockMgr.LstInterLock)
            {
                if (_frmInterLock.Visible == false)
                {
                    if (InterLockMgr.LstInterLock.Count > 0)
                    {
                        InterlockMsg interlockMsg = InterLockMgr.LstInterLock.Dequeue();

                        _frmInterLock.InterlockMsg = interlockMsg.Interlock;
                        _frmInterLock.DetailMsg = interlockMsg.Detail;
                        _frmInterLock.Show();

                        InterLockMgr.LstInterLock.Clear();
                    }
                }
            }

            lblMaxScanTime.Text = GG.Equip.MaxScanTick.ToString();
            lblScanTick.Text = GG.Equip.ScanTick.ToString();
            lblScanTime.Text = GG.Equip.ScanTime.ToString();

            FillAlarmListWorking();
            InnerWorkOn();
        }

        private void InnerWorkOn()
        {
            if (GG.Equip.IsInnerWorkOn == true && _frmInnerWorkOn.Visible == false && GG.Equip.IsInnerWorkOnClose == false)
            {
                //_frmInnerWorkOn.StartPosition = FormStartPosition.CenterParent;
                //_frmInnerWorkOn.Show();
            }
        }

        private void FillAlarmListWorking()
        {
            if (AlarmManager.Instance.QueShowAlarm.Count > 0)
            {
                while (AlarmManager.Instance.QueShowAlarm.Count > 0)
                {
                    Alarm am = AlarmManager.Instance.QueShowAlarm.Dequeue();

                    _frmAlarm.AddAlarmList(am);
                }

                _frmAlarm.StartPosition = FormStartPosition.CenterScreen;
                _frmAlarm.Show();
            }
        }

        private void InitalizeUserControlUI()
        {
            //ExtensionUI.AddClickEventLog(this);



            if (GG.TestMode)
            {
                Logger.Log = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "AlarmLog"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "HSMS", 500, 1024 * 1024 * 20, true, null);
            }
            else
            {
                System.IO.DirectoryInfo Di = new System.IO.DirectoryInfo("E:/DitCtrl");
                if (Di.Exists)
                {
                    Logger.Log = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "HSMS", 500, 1024 * 1024 * 20, true, null);
                }
                else
                {
                    Logger.Log = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "AlarmLog"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "HSMS", 500, 1024 * 1024 * 20, true, null);
                }
            }

            if (true == GG.TestMode)
            {
                //GG.CCLINK = (IVirtualMem)new VirtualCCLink(81, -1,  "CCLINK");
                //GG.CCLINK = (IVirtualMem)new VirtualCCLinkEx(81, 0xff, -1, "CCLINK");
                //GG.PMAC = (IVirtualMem)(new VirtualUMac("PMAC", Properties.Settings.Default.PmacIP, Properties.Settings.Default.PmacPort));
                //GG.HSMS = (IVirtualMem)new VirtualCCLinkEx(151, 255, 0, "CCLINK") { IsDirect = true };
                //GG.MACRO = (IVirtualMem)new VirtualShare("DIT.PLC.MACRO_MEM.S", 102400);
                //GG.MEM_DIT = (IVirtualMem)new VirtualShare("DIT.CTRL.SHARE.MEM", 102400);
                GG.EQ_DATA = (IVirtualMem)new VirtualShare("DIT.CTRL.EQ_DATA.MEM", 102400);
                GG.HSMS = (IVirtualMem)new VirtualShare("DIT.CTRL.HSMS.MEM", 102400);
                GG.UMAC = (IVirtualMem)(new VirtualMem("DIT.MOTOR.MEM"));
                GG.EZI = (IVirtualMem)(new VirtualMem("DIT.MOTOR.MEM"));
                GG.AJIN = (IVirtualMem)(new VirtualMem("DIT.MOTOR.MEM"));
            }
            else
            {
                GG.EQ_DATA = (IVirtualMem)new VirtualShare("DIT.CTRL.EQ_DATA.MEM", 102400);
                GG.HSMS = (IVirtualMem)new VirtualShare("DIT.CTRL.HSMS.MEM", 102400);
                GG.UMAC = (IVirtualMem)new VirtualUMacAsync("UMAC", "127.0.0.1", 0);
                GG.AJIN = (IVirtualMem)new VirtualAjinDirect("AJIN", "", 0, new AjinSetting() { DIOCount = 14, AIOCount = 5, MotorCount = 46 });
                GG.EZI = (new VirtualEziDirect("EZI", "192.168.0.0", 0, new EziSetting() { MotorRotaryNo = new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 } }));


                //모듈 번호가 변경될 경우. 알람. 처리. 필요 //

                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 00) { Desc = "LD-IN-1", /**/ Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 01) { Desc = "LD-IN-2", /**/ Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 02) { Desc = "LD-IN-3", /**/ Station = 0, PLC = GG.AJIN });

                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 06) { Desc = "PROC-IN-1", /**/ Station = 0, PLC = GG.AJIN });

                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 07) { Desc = "UD-IN-1", /**/ Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 08) { Desc = "UD-IN-2", /**/ Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 09) { Desc = "UD-IN-3", /**/ Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JI, 10) { Desc = "UD-IN-4", /**/ Station = 0, PLC = GG.AJIN });

                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 03) { Desc = "LD-OUT-1", Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 04) { Desc = "LD-OUT-2", Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 05) { Desc = "LD-OUT-3", Station = 0, PLC = GG.AJIN });

                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 06) { Desc = "PROC-OUT-1", Station = 0, PLC = GG.AJIN });

                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 11) { Desc = "UD-OUT-1", Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 12) { Desc = "UD-OUT-2", Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 13) { Desc = "UD-OUT-3", Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 14) { Desc = "UD-OUT-4", Station = 0, PLC = GG.AJIN });


                //(GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 3) { Station = 0, PLC = GG.AJIN });
                //(GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 4) { Station = 0, PLC = GG.AJIN });
                //(GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 5) { Station = 0, PLC = GG.AJIN });
                //(GG.AJIN as VirtualAjinDirect).LstWriteAddr.Add(new PlcAddr(PlcMemType.JO, 6) { Station = 0, PLC = GG.AJIN });

                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JR, 0) { Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JR, 1) { Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JR, 2) { Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JR, 3) { Station = 0, PLC = GG.AJIN });
                (GG.AJIN as VirtualAjinDirect).LstReadAddr.Add(new PlcAddr(PlcMemType.JR, 4) { Station = 0, PLC = GG.AJIN });

                (GG.UMAC as VirtualUMacAsync).LstWriteAddr.Add(new PlcAddr(PlcMemType.S, 4000, 0, 1000));
                (GG.UMAC as VirtualUMacAsync).LstReadAddr.Add(new PlcAddr(PlcMemType.S, 5000, 0, 1000));
            }

            AddressMgr.Load(GG.ADDRESS_EZI_MOTOR,           /**/GG.EZI);
            AddressMgr.Load(GG.ADDRESS_AJIN_IO,             /**/GG.AJIN);
            AddressMgr.Load(GG.ADDRESS_AJIN_MOTOR,          /**/GG.AJIN);
            AddressMgr.Load(GG.ADDRESS_UMAC_IO,             /**/GG.UMAC);
            AddressMgr.Load(GG.ADDRESS_UMAC_MOTOR,          /**/GG.UMAC);


            GG.Equip = new Equipment();
            GG.Equip.LoadSetting();
            GG.Equip.SetAddress();
                        
            //LOAD 모터
            GG.Equip.GetServoMotors(EM_MOTOR_POSI.ALL, EM_MOTOR_TYPE.Umac).ForEach(f => CreateUmacSvoMotorProxy(f));
            GG.Equip.GetServoMotors(EM_MOTOR_POSI.ALL, EM_MOTOR_TYPE.Ajin).ForEach(f => CreateAjinSvoMotorProxy(f));
            GG.Equip.GetServoMotors(EM_MOTOR_POSI.ALL, EM_MOTOR_TYPE.EZi).ForEach(f => CreateEziSvoMotorProxy(f));

            int eqDataResult = GG.EQ_DATA.Open();
            int hsmsResult = GG.HSMS.Open();
            int umacResult = GG.UMAC.Open();
            int eziResult = GG.EZI.Open();
            int ajinResult = GG.AJIN.Open();



            ServoPositionMgr.LoadPosition(GG.Equip);
            ServoParamMgr.LoadParam(GG.Equip);

            //UI 
            _ucrlMgrSubManu.InitalizeUserControlUI(GG.Equip);
            _ucrlMainForm.Equip = GG.Equip;



            if (GG.TestMode == false)
            {
                bool ajinError = false, eziError = false;

                GG.Equip.GetServoMotors(EM_MOTOR_POSI.ALL, EM_MOTOR_TYPE.Ajin).ForEach(
                    m =>
                    {
                        AjinMotorSetting ret = new AjinMotorSetting();
                        if (m.ParamSetting.ToAjinMotorSetting(out ret) == true)
                        {
                            if (AjinProxy.GetAppliedSetting(ref ret) == true)
                                ajinError |= true;
                        }
                        else
                            ajinError |= true;
                    }
                    );

                GG.Equip.GetServoMotors(EM_MOTOR_POSI.ALL, EM_MOTOR_TYPE.EZi).ForEach(
                    m =>
                    {
                        EziMotorSetting ret = new EziMotorSetting();
                        if (m.ParamSetting.ToEziMotorSetting(out ret) == true)
                        {
                            if (EziProxy.GetAppliedSetting(ref ret) == true)
                                eziError |= true;
                        }
                        else
                            eziError |= true;
                    }
                    );

                InterLockMgr.AddInterLock("Motor Setting Abnormal!", string.Format("{0}\n{1}", ajinError ? "AJIN Error" : "", eziError ? "EZI Error" : ""));
            }

            GG.Equip.StartLogic();

        }

        public void CreateUmacSvoMotorProxy(ServoMotorControl servo)
        {
            //UmacSvoMotorProxy umacMotor = new UmacSvoMotorProxy() { Name = servo.Name, UmacAxisNo = servo.OutterAxisNo, InteralAxisNo = servo.OutterAxisNo, Umac = GG.UMAC };

            //umacMotor.XB_StatusHomeCompleteBit = servo.XB_StatusHomeCompleteBit;
            //umacMotor.XB_StatusHomeInPosition = servo.XB_StatusHomeInPosition;
            //umacMotor.XB_StatusMotorMoving = servo.XB_StatusMotorMoving;
            //umacMotor.XB_StatusMotorInPosition = servo.XB_StatusMotorInPosition;
            //umacMotor.XB_StatusNegativeLimitSet = servo.XB_StatusNegativeLimitSet;
            //umacMotor.XB_StatusPositiveLimitSet = servo.XB_StatusPositiveLimitSet;
            //umacMotor.XB_StatusMotorServoOn = servo.XB_StatusMotorServoOn;
            //umacMotor.XB_ErrFatalFollowingError = servo.XB_ErrFatalFollowingError;
            //umacMotor.XB_ErrAmpFaultError = servo.XB_ErrAmpFaultError;
            //umacMotor.XB_ErrI2TAmpFaultError = servo.XB_ErrI2TAmpFaultError;

            //umacMotor.XF_CurrMotorPosition = servo.XF_CurrMotorPosition;
            //umacMotor.XF_CurrMotorSpeed = servo.XF_CurrMotorSpeed;
            //umacMotor.XF_CurrMotorActualLoad = servo.XF_CurrMotorActualLoad;

            //umacMotor.YB_HomeCmd = servo.YB_HomeCmd;
            //umacMotor.XB_HomeCmdAck = servo.XB_HomeCmdAck;

            //umacMotor.YB_MotorStopCmd = servo.YB_MotorStopCmd;
            //umacMotor.XB_MotorStopCmdAck = servo.XB_MotorStopCmdAck;

            //umacMotor.YB_ServoOnOffCmd = servo.YB_ServoOnOffCmd;
            //umacMotor.XB_ServoOnOffCmdAck = servo.XB_ServoOnOffCmdAck;
            //umacMotor.YB_ServoOnOff = servo.YB_ServoOnOff;

            //umacMotor.YB_MotorJogMinusMove = servo.YB_MotorJogMinusMove;
            //umacMotor.YB_MotorJogPlusMove = servo.YB_MotorJogPlusMove;
            //umacMotor.YF_MotorJogSpeedCmd = servo.YF_MotorJogSpeedCmd;

            //umacMotor.YB_PTPMoveCmd = servo.YB_PTPMoveCmd;
            //umacMotor.XB_PTPMoveCmdAck = servo.XB_PTPMoveCmdAck;
            //umacMotor.YB_PTPMoveCmdCancel = servo.YB_PTPMoveCmdCancel;

            //umacMotor.YF_PTPMoveSpeed = servo.YF_PTPMoveSpeed;
            //umacMotor.XF_PTPMoveSpeedAck = servo.XF_PTPMoveSpeedAck;
            //umacMotor.YF_PTPMovePosition = servo.YF_PTPMovePosition;
            //umacMotor.XF_PTPMovePositionAck = servo.XF_PTPMovePositionAck;
            //umacMotor.YF_PTPMoveAccel = servo.YF_PTPMoveAccel;
            //umacMotor.XF_PTPMoveAccelAck = servo.XF_PTPMoveAccelAck;

            //umacMotor.XB_PTPMoveComplete = servo.XB_PTPMoveComplete;


            //GG.UMAC.Motors.Add(umacMotor);
        }
        public void CreateAjinSvoMotorProxy(ServoMotorControl servo)
        {
            AjinSvoMotorProxy motor = new AjinSvoMotorProxy() { Name = servo.Name, AjinAxisNo = servo.OutterAxisNo, Ajin = (GG.AJIN as VirtualAjinDirect) };

            motor.XB_StatusHomeCompleteBit = servo.XB_StatusHomeCompleteBit;
            motor.XB_StatusHomeInPosition = servo.XB_StatusHomeInPosition;
            motor.XB_StatusMotorMoving = servo.XB_StatusMotorMoving;
            motor.XB_StatusMotorInPosition = servo.XB_StatusMotorInPosition;
            motor.XB_StatusNegativeLimitSet = servo.XB_StatusNegativeLimitSet;
            motor.XB_StatusPositiveLimitSet = servo.XB_StatusPositiveLimitSet;
            motor.XB_StatusMotorServoOn = servo.XB_StatusMotorServoOn;
            motor.XB_ErrFatalFollowingError = servo.XB_ErrFatalFollowingError;
            motor.XB_ErrAmpFaultError = servo.XB_ErrAmpFaultError;
            motor.XB_ErrI2TAmpFaultError = servo.XB_ErrI2TAmpFaultError;

            motor.XF_CurrMotorPosition = servo.XF_CurrMotorPositionPlcAddr;
            motor.XF_CurrMotorSpeed = servo.XF_CurrMotorSpeed;
            motor.XF_CurrMotorActualLoad = servo.XF_CurrMotorActualLoad;

            motor.XI_CurrMotorLow = servo.XI_CurrMotorLow;
            motor.XI_CurrMotorHight = servo.XI_CurrMotorHight;
            motor.XI_CurrMotorStatus1 = servo.XI_CurrMotorStatus1;

            motor.YB_HomeCmd = servo.YB_HomeCmd;
            motor.XB_HomeCmdAck = servo.XB_HomeCmdAck;

            motor.YB_MotorStopCmd = servo.YB_MotorStopCmd;
            motor.XB_MotorStopCmdAck = servo.XB_MotorStopCmdAck;

            motor.YB_ServoOnOffCmd = servo.YB_ServoOnOffCmd;
            motor.XB_ServoOnOffCmdAck = servo.XB_ServoOnOffCmdAck;
            motor.YB_ServoOnOff = servo.YB_ServoOnOff;

            motor.YB_MotorJogMinusMove = servo.YB_MotorJogMinusMove;
            motor.YB_MotorJogPlusMove = servo.YB_MotorJogPlusMove;
            motor.YF_MotorJogSpeedCmd = servo.YF_MotorJogSpeedCmdPlcAddr;
            motor.XF_MotorJogSpeedCmdAck = servo.XF_MotorJogSpeedCmdAckPlcAddr;


            motor.YB_PTPMoveCmd = servo.YB_PTPMoveCmd;
            motor.XB_PTPMoveCmdAck = servo.XB_PTPMoveCmdAck;
            motor.YB_PTPMoveCmdCancel = servo.YB_PTPMoveCmdCancel;

            motor.YF_PTPMoveSpeed = servo.YF_PTPMoveSpeed;
            motor.XF_PTPMoveSpeedAck = servo.XF_PTPMoveSpeedAck;
            motor.YF_PTPMovePosition = servo.YF_PTPMovePositionPlcAddr;
            motor.XF_PTPMovePositionAck = servo.XF_PTPMovePositionAckPlcAddr;
            motor.YF_PTPMoveAccel = servo.YF_PTPMoveAccel;
            motor.XF_PTPMoveAccelAck = servo.XF_PTPMoveAccelAck;


            if (GG.TestMode == false)
                (GG.AJIN as VirtualAjinDirect).Motors.Add(motor);
        }
        public void CreateEziSvoMotorProxy(ServoMotorControl servo)
        {
            EziSvoMotorProxy motor = new EziSvoMotorProxy() { Name = servo.Name, AxisNo = servo.InnerAxisNo, RotaryNo = servo.OutterAxisNo, Ezi = GG.EZI as VirtualEziDirect };

            motor.XB_StatusHomeCompleteBit = servo.XB_StatusHomeCompleteBit;
            motor.XB_StatusHomeInPosition = servo.XB_StatusHomeInPosition;
            motor.XB_StatusMotorMoving = servo.XB_StatusMotorMoving;
            motor.XB_StatusMotorInPosition = servo.XB_StatusMotorInPosition;
            motor.XB_StatusNegativeLimitSet = servo.XB_StatusNegativeLimitSet;
            motor.XB_StatusPositiveLimitSet = servo.XB_StatusPositiveLimitSet;
            motor.XB_StatusMotorServoOn = servo.XB_StatusMotorServoOn;
            motor.XB_ErrFatalFollowingError = servo.XB_ErrFatalFollowingError;
            motor.XB_ErrAmpFaultError = servo.XB_ErrAmpFaultError;
            motor.XB_ErrI2TAmpFaultError = servo.XB_ErrI2TAmpFaultError;

            motor.XF_CurrMotorPosition = servo.XF_CurrMotorPositionPlcAddr;
            motor.XF_CurrMotorSpeed = servo.XF_CurrMotorSpeed;
            motor.XF_CurrMotorActualLoad = servo.XF_CurrMotorActualLoad;

            motor.XI_CurrMotorLow = servo.XI_CurrMotorLow;
            motor.XI_CurrMotorHight = servo.XI_CurrMotorHight;
            motor.XI_CurrMotorStatus1 = servo.XI_CurrMotorStatus1;

            motor.YB_HomeCmd = servo.YB_HomeCmd;
            motor.XB_HomeCmdAck = servo.XB_HomeCmdAck;

            motor.YB_MotorStopCmd = servo.YB_MotorStopCmd;
            motor.XB_MotorStopCmdAck = servo.XB_MotorStopCmdAck;

            motor.YB_ServoOnOffCmd = servo.YB_ServoOnOffCmd;
            motor.XB_ServoOnOffCmdAck = servo.XB_ServoOnOffCmdAck;
            motor.YB_ServoOnOff = servo.YB_ServoOnOff;

            motor.YB_MotorJogMinusMove = servo.YB_MotorJogMinusMove;
            motor.YB_MotorJogPlusMove = servo.YB_MotorJogPlusMove;

            motor.YF_MotorJogSpeedCmd = servo.YF_MotorJogSpeedCmdPlcAddr;
            motor.YF_MotorJogSpeedCmdAck = servo.XF_MotorJogSpeedCmdAckPlcAddr;

            motor.YB_PTPMoveCmd = servo.YB_PTPMoveCmd;
            motor.XB_PTPMoveCmdAck = servo.XB_PTPMoveCmdAck;
            motor.YB_PTPMoveCmdCancel = servo.YB_PTPMoveCmdCancel;

            motor.YF_PTPMoveSpeed = servo.YF_PTPMoveSpeed;
            motor.XF_PTPMoveSpeedAck = servo.XF_PTPMoveSpeedAck;
            motor.YF_PTPMovePosition = servo.YF_PTPMovePositionPlcAddr;
            motor.XF_PTPMovePositionAck = servo.XF_PTPMovePositionAckPlcAddr;
            motor.YF_PTPMoveAccel = servo.YF_PTPMoveAccel;
            motor.XF_PTPMoveAccelAck = servo.XF_PTPMoveAccelAck;

            //motor.XB_PTPMoveComplete = servo.XB_PTPMoveComplete;

            if (GG.TestMode == false)
                (GG.EZI as VirtualEziDirect).Motors.Add(motor);
        }

        private void btnAlarmLog_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(_ucrlLogSubMenu);
            _ucrlMgrSubManu.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }
        private void btnHostMessage_Click(object sender, EventArgs e)
        {

        }
        private void lblMaxScanTime_Click(object sender, EventArgs e)
        {
            GG.Equip.MaxScanTick = 0;
            GG.Equip.ScanTick = 0;
            GG.Equip.ScanTime = 0;
        }
        private void btnOnoffLine_Click(object sender, EventArgs e)
        {

        }
        private void btnSim_Click(object sender, EventArgs e)
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "FrmSimulator")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Normal;
                        openForm.Location = new Point(this.Location.X + this.Width, this.Location.Y);
                    }
                    openForm.Activate();
                    return;
                }
            }

            FrmSimulator frmsim = new FrmSimulator();
            frmsim.StartPosition = FormStartPosition.CenterParent;
            frmsim.Show();

        }
        private void btnStepMgr_Click(object sender, EventArgs e)
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "FrmStepSwitchMgr")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Normal;
                        openForm.Location = new Point(this.Location.X + this.Width, this.Location.Y);
                    }
                    openForm.Activate();
                    return;
                }
            }

            FrmStepSwitchMgr frmsim = new FrmStepSwitchMgr();
            frmsim.StartPosition = FormStartPosition.CenterParent;
            frmsim.Show();

            AlarmManager.Instance.Happen(GG.Equip, EM_AL_LST.AL_0000_EMS_1);
        }
    }
}


﻿namespace DIT.TLC.UI
{
    partial class FrmSimulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSimulator));
            this.lstViewMotorInfo = new DitEquipMainDll.UserControls.ListViewEx();
            this.tmrWorker = new System.Windows.Forms.Timer(this.components);
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.pnlView = new System.Windows.Forms.Panel();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.shInspCam = new EquipSimulator.Ctrl.TransparentControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.shReviewCam = new EquipSimulator.Ctrl.TransparentControl();
            this.shStage = new EquipSimulator.Ctrl.TransparentControl();
            this.SpThelta = new EquipSimulator.Ctrl.TransparentControl();
            this.spLiftPin = new EquipSimulator.Ctrl.TransparentControl();
            this.pnlView.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lstViewMotorInfo
            // 
            this.lstViewMotorInfo.Location = new System.Drawing.Point(1074, 13);
            this.lstViewMotorInfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstViewMotorInfo.Name = "lstViewMotorInfo";
            this.lstViewMotorInfo.Size = new System.Drawing.Size(552, 669);
            this.lstViewMotorInfo.TabIndex = 91;
            this.lstViewMotorInfo.UseCompatibleStateImageBehavior = false;
            // 
            // tmrWorker
            // 
            this.tmrWorker.Enabled = true;
            this.tmrWorker.Tick += new System.EventHandler(this.tmrWorker_Tick);
            // 
            // bgWorker
            // 
            this.bgWorker.WorkerReportsProgress = true;
            // 
            // pnlView
            // 
            this.pnlView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlView.Controls.Add(this.elementHost1);
            this.pnlView.Controls.Add(this.label14);
            this.pnlView.Controls.Add(this.panel1);
            this.pnlView.Location = new System.Drawing.Point(12, 12);
            this.pnlView.Name = "pnlView";
            this.pnlView.Size = new System.Drawing.Size(1056, 670);
            this.pnlView.TabIndex = 92;
            // 
            // elementHost1
            // 
            this.elementHost1.Location = new System.Drawing.Point(0, 66);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(1054, 602);
            this.elementHost1.TabIndex = 10;
            this.elementHost1.Child = null;
            // 
            // label14
            // 
            this.label14.AutoEllipsis = true;
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(1054, 66);
            this.label14.TabIndex = 9;
            this.label14.Text = "■ EQUIP  STATUS";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.shInspCam);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.shReviewCam);
            this.panel1.Controls.Add(this.shStage);
            this.panel1.Controls.Add(this.SpThelta);
            this.panel1.Controls.Add(this.spLiftPin);
            this.panel1.Location = new System.Drawing.Point(43, 305);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(49, 34);
            this.panel1.TabIndex = 0;
            this.panel1.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(23, 177);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 15);
            this.label26.TabIndex = 10;
            this.label26.Text = "Litft Pin";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(189, 646);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 15);
            this.label27.TabIndex = 11;
            this.label27.Text = "Thelta";
            // 
            // shInspCam
            // 
            this.shInspCam.BackColor = System.Drawing.Color.Transparent;
            this.shInspCam.Location = new System.Drawing.Point(267, 117);
            this.shInspCam.Name = "shInspCam";
            this.shInspCam.Size = new System.Drawing.Size(91, 403);
            this.shInspCam.TabIndex = 13;
            this.shInspCam.TabStop = false;
            this.shInspCam.Text = "transparentControl1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(138, 61);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(123, 530);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // shReviewCam
            // 
            this.shReviewCam.BackColor = System.Drawing.Color.Transparent;
            this.shReviewCam.Location = new System.Drawing.Point(359, 392);
            this.shReviewCam.Name = "shReviewCam";
            this.shReviewCam.Size = new System.Drawing.Size(169, 102);
            this.shReviewCam.TabIndex = 10;
            this.shReviewCam.TabStop = false;
            this.shReviewCam.Text = "transparentControl1";
            // 
            // shStage
            // 
            this.shStage.BackColor = System.Drawing.Color.Transparent;
            this.shStage.Location = new System.Drawing.Point(177, 255);
            this.shStage.Name = "shStage";
            this.shStage.Size = new System.Drawing.Size(194, 148);
            this.shStage.TabIndex = 11;
            this.shStage.TabStop = false;
            this.shStage.Text = "transparentControl1";
            // 
            // SpThelta
            // 
            this.SpThelta.BackColor = System.Drawing.Color.DarkRed;
            this.SpThelta.Location = new System.Drawing.Point(190, 633);
            this.SpThelta.Name = "SpThelta";
            this.SpThelta.Size = new System.Drawing.Size(10, 10);
            this.SpThelta.TabIndex = 12;
            this.SpThelta.TabStop = false;
            this.SpThelta.Text = "transparentControl1";
            // 
            // spLiftPin
            // 
            this.spLiftPin.BackColor = System.Drawing.Color.Red;
            this.spLiftPin.ForeColor = System.Drawing.Color.Red;
            this.spLiftPin.Location = new System.Drawing.Point(19, 150);
            this.spLiftPin.Name = "spLiftPin";
            this.spLiftPin.Size = new System.Drawing.Size(10, 10);
            this.spLiftPin.TabIndex = 12;
            this.spLiftPin.TabStop = false;
            this.spLiftPin.Text = "transparentControl1";
            // 
            // FrmSimulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1635, 693);
            this.Controls.Add(this.pnlView);
            this.Controls.Add(this.lstViewMotorInfo);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmSimulator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "설비 뷰어";
            this.TopMost = true;
            this.pnlView.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DitEquipMainDll.UserControls.ListViewEx lstViewMotorInfo;
        private System.Windows.Forms.Timer tmrWorker;
        private System.ComponentModel.BackgroundWorker bgWorker;
        private System.Windows.Forms.Panel pnlView;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        internal System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private EquipSimulator.Ctrl.TransparentControl shInspCam;
        private System.Windows.Forms.PictureBox pictureBox1;
        private EquipSimulator.Ctrl.TransparentControl shReviewCam;
        private EquipSimulator.Ctrl.TransparentControl shStage;
        private EquipSimulator.Ctrl.TransparentControl SpThelta;
        private EquipSimulator.Ctrl.TransparentControl spLiftPin;
    }
}
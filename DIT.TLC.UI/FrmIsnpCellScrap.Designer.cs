﻿namespace DIT.TLC.UI
{
    partial class FrmIsnpCellScrap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmIsnpCellScrap));
            this.lblInspCellScrapPosition = new System.Windows.Forms.Label();
            this.btnInspCellPass = new System.Windows.Forms.Button();
            this.btnInspCellScrap = new System.Windows.Forms.Button();
            this.lblIsnpCellScrapInfo = new System.Windows.Forms.Label();
            this.btnInspCellAlarmClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblInspCellScrapPosition
            // 
            this.lblInspCellScrapPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspCellScrapPosition.ForeColor = System.Drawing.Color.Black;
            this.lblInspCellScrapPosition.Location = new System.Drawing.Point(4, 43);
            this.lblInspCellScrapPosition.Name = "lblInspCellScrapPosition";
            this.lblInspCellScrapPosition.Size = new System.Drawing.Size(252, 34);
            this.lblInspCellScrapPosition.TabIndex = 56;
            this.lblInspCellScrapPosition.Text = "실패한 인스펙션 셀 포지션";
            this.lblInspCellScrapPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInspCellPass
            // 
            this.btnInspCellPass.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspCellPass.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnInspCellPass.ForeColor = System.Drawing.Color.Black;
            this.btnInspCellPass.Location = new System.Drawing.Point(90, 93);
            this.btnInspCellPass.Name = "btnInspCellPass";
            this.btnInspCellPass.Size = new System.Drawing.Size(80, 45);
            this.btnInspCellPass.TabIndex = 55;
            this.btnInspCellPass.Text = "패스";
            this.btnInspCellPass.UseVisualStyleBackColor = false;
            // 
            // btnInspCellScrap
            // 
            this.btnInspCellScrap.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspCellScrap.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnInspCellScrap.ForeColor = System.Drawing.Color.Black;
            this.btnInspCellScrap.Location = new System.Drawing.Point(4, 93);
            this.btnInspCellScrap.Name = "btnInspCellScrap";
            this.btnInspCellScrap.Size = new System.Drawing.Size(80, 45);
            this.btnInspCellScrap.TabIndex = 54;
            this.btnInspCellScrap.Text = "스크랩";
            this.btnInspCellScrap.UseVisualStyleBackColor = false;
            // 
            // lblIsnpCellScrapInfo
            // 
            this.lblIsnpCellScrapInfo.AutoEllipsis = true;
            this.lblIsnpCellScrapInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblIsnpCellScrapInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblIsnpCellScrapInfo.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblIsnpCellScrapInfo.ForeColor = System.Drawing.Color.Black;
            this.lblIsnpCellScrapInfo.Location = new System.Drawing.Point(0, 0);
            this.lblIsnpCellScrapInfo.Name = "lblIsnpCellScrapInfo";
            this.lblIsnpCellScrapInfo.Size = new System.Drawing.Size(260, 24);
            this.lblIsnpCellScrapInfo.TabIndex = 53;
            this.lblIsnpCellScrapInfo.Text = "■ Cell Scrap";
            this.lblIsnpCellScrapInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInspCellAlarmClear
            // 
            this.btnInspCellAlarmClear.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspCellAlarmClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnInspCellAlarmClear.ForeColor = System.Drawing.Color.Black;
            this.btnInspCellAlarmClear.Location = new System.Drawing.Point(176, 93);
            this.btnInspCellAlarmClear.Name = "btnInspCellAlarmClear";
            this.btnInspCellAlarmClear.Size = new System.Drawing.Size(80, 45);
            this.btnInspCellAlarmClear.TabIndex = 57;
            this.btnInspCellAlarmClear.Text = "알람클리어";
            this.btnInspCellAlarmClear.UseVisualStyleBackColor = false;
            // 
            // FrmIsnpCellScrap
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(260, 141);
            this.Controls.Add(this.btnInspCellAlarmClear);
            this.Controls.Add(this.lblInspCellScrapPosition);
            this.Controls.Add(this.btnInspCellPass);
            this.Controls.Add(this.btnInspCellScrap);
            this.Controls.Add(this.lblIsnpCellScrapInfo);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmIsnpCellScrap";
            this.Text = "인스펙션 실패 스크랩";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblInspCellScrapPosition;
        private System.Windows.Forms.Button btnInspCellPass;
        private System.Windows.Forms.Button btnInspCellScrap;
        internal System.Windows.Forms.Label lblIsnpCellScrapInfo;
        private System.Windows.Forms.Button btnInspCellAlarmClear;
    }
}
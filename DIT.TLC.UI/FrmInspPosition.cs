﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmInspPosition : Form
    {
        private string sRecpName = string.Empty;

        // X Speed, Y Speed : Insp 촬영 시 속도
        // Move Speed : 그냥 움직일 때 속도
        public FrmInspPosition(string recpName)
        {
            InitializeComponent();

            sRecpName = recpName;
            SettingValue();
        }

        public void SettingValue()
        {
            //txtALeftTopX.Text    =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString()    ;
            //txtALeftTopY.Text    =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtALeftBotX.Text    =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString();
            //txtALeftBotY.Text    =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtARightTopX.Text   =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString();
            //txtARightTopY.Text   =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtARightBotX.Text   =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString();
            //txtARightBotY.Text   =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtATopZ.Text        =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtABotZ.Text        =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtBLeftTopX.Text    =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString();
            //txtBLeftTopY.Text    =  GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition.ToString();
            //txtBLeftBotX.Text    =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString();
            //txtBLeftBotY.Text    =  GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition.ToString();
            //txtBRightTopX.Text   =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString();
            //txtBRightTopY.Text   =  GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition.ToString();
            //txtBRightBotX.Text   =  GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition.ToString();
            //txtBRightBotY.Text   =  GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition.ToString();
            //txtBTopZ.Text        =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtBBotZ.Text        =  GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition.ToString();
            //txtAXSpeed.Text      = GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorSpeed.vFloat.ToString();
            //txtAYSpeed.Text      = GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorSpeed.vFloat.ToString();
            //txtAMoveSpeed.Text   = GG.Equip.UD.InspCamera.XAxis.XF_PTPMoveSpeedAck.vFloat.ToString();
            //txtBXSpeed.Text      = GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorSpeed.vFloat.ToString();
            //txtBYSpeed.Text      = GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorSpeed.vFloat.ToString();
            //txtBMoveSpeed.Text   = GG.Equip.UD.InspCamera.XAxis.XF_PTPMoveSpeedAck.vFloat.ToString();

            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == sRecpName);
            txtALeftTopX.Text = string.Format("{0}", recp.ALeftTop.X);
            txtALeftTopY.Text = string.Format("{0}", recp.ALeftTop.Y);
            txtALeftBotX.Text = string.Format("{0}", recp.ALeftBot.X);
            txtALeftBotY.Text = string.Format("{0}", recp.ALeftBot.Y);
            txtARightTopX.Text = string.Format("{0}", recp.ARightTop.X);
            txtARightTopY.Text = string.Format("{0}", recp.ARightTop.Y);
            txtARightBotX.Text = string.Format("{0}", recp.ARightBot.X);
            txtARightBotY.Text = string.Format("{0}", recp.ARightBot.Y);
            txtATopZ.Text = string.Format("{0}", recp.ATopZ);
            txtABotZ.Text = string.Format("{0}", recp.ABotZ);
            txtBLeftTopX.Text = string.Format("{0}", recp.BLeftTop.X);
            txtBLeftTopY.Text = string.Format("{0}", recp.BLeftTop.Y);
            txtBLeftBotX.Text = string.Format("{0}", recp.BLeftBot.X);
            txtBLeftBotY.Text = string.Format("{0}", recp.BLeftBot.Y);
            txtBRightTopX.Text = string.Format("{0}", recp.BRightTop.X);
            txtBRightTopY.Text = string.Format("{0}", recp.BRightTop.Y);
            txtBRightBotX.Text = string.Format("{0}", recp.BRightBot.X);
            txtBRightBotY.Text = string.Format("{0}", recp.BRightBot.Y);
            txtBTopZ.Text = string.Format("{0}", recp.BTopZ);
            txtBBotZ.Text = string.Format("{0}", recp.BBotZ);
            txtAXSpeed.Text = string.Format("{0}", recp.AXSpeed);
            txtAYSpeed.Text = string.Format("{0}", recp.AYSpeed);
            txtAMoveSpeed.Text = string.Format("{0}", recp.AMoveSpeed);
            txtBXSpeed.Text = string.Format("{0}", recp.BXSpeed);
            txtBYSpeed.Text = string.Format("{0}", recp.BYSpeed);
            txtBMoveSpeed.Text = string.Format("{0}", recp.BMoveSpeed);
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            if (btnALtXRead == (Button)sender)
            {
                txtALeftTopX.Text = string.Format("{0}",GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnALtYRead == (Button)sender)
            {
                txtALeftTopY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }
            else if (btnALbXRead == (Button)sender)
            {
                txtALeftBotX.Text = string.Format("{0}", GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnALbYRead == (Button)sender)
            {
                txtALeftBotY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }
            //A RIGHT
            else if (btnARtXRead == (Button)sender)
            {
                txtARightTopX.Text = string.Format("{0}", GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnARtYRead == (Button)sender)
            {
                txtARightTopY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }

            else if (btnARbXRead == (Button)sender)
            {
                txtARightBotX.Text = string.Format("{0}", GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnARbYRead == (Button)sender)
            {
                txtARightBotY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }
            else if (btnATopZRead == (Button)sender)
            {
                txtATopZ.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }
            else if (btnABotZRead == (Button)sender)
            {
                txtABotZ.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }
            //B열   //////////////////////////////////
            if (btnBLtXRead == (Button)sender)
            {
                txtBLeftTopX.Text = string.Format("{0}", GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnBLtYRead == (Button)sender)
            {
                txtBLeftTopY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition);
            }

            else if (btnBLbXRead == (Button)sender)
            {
                txtBLeftBotX.Text = string.Format("{0}", GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnBLbYRead == (Button)sender)
            {
                txtBLeftBotY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition);
            }
            //B RIGHT
            else if (btnBRtXRead == (Button)sender)
            {
                txtBRightTopX.Text = string.Format("{0}", GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnBRtYRead == (Button)sender)
            {
                txtBRightTopY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition);
            }

            else if (btnBRbXRead == (Button)sender)
            {
                txtBRightBotX.Text = string.Format("{0}", GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition);
            }
            else if (btnBRbYRead == (Button)sender)
            {
                txtBRightBotY.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition);
            }
            else if (btnBTopZRead == (Button)sender)
            {
                txtBTopZ.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }
            else if (btnBBotZRead == (Button)sender)
            {
                txtBBotZ.Text = string.Format("{0}", GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition);
            }
        }


        private void btnMove_Click(object sender, EventArgs e)
        {
            if (btnALtXMove == (Button)sender)
            {
                if (txtALeftTopX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColLTX = float.Parse(txtALeftTopX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, AColLTX, 0, 0);
            }
            else if (btnALtYMove == (Button)sender)
            {
                if (txtALeftTopY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColLTY = float.Parse(txtALeftTopY.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, AColLTY, 0, 0);
            }

            else if (btnALbXMove == (Button)sender)
            {
                if (txtALeftBotX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColLBX = float.Parse(txtALeftBotX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, AColLBX, 0, 0);
            }
            else if (btnALbYMove == (Button)sender)
            {
                if (txtALeftBotY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColLBY = float.Parse(txtALeftBotY.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, AColLBY, 0, 0);
            }
            //A RIGHT
            else if (btnARtXMove == (Button)sender)
            {
                if (txtARightTopX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColRTX = float.Parse(txtARightTopX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, AColRTX, 0, 0);
            }
            else if (btnARtYMove == (Button)sender)
            {
                if (txtARightTopY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColRTY = float.Parse(txtARightTopY.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, AColRTY, 0, 0);
            }

            else if (btnARbXMove == (Button)sender)
            {
                if (txtARightBotX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColRBX = float.Parse(txtARightBotX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, AColRBX, 0, 0);
            }
            else if (btnARbYMove == (Button)sender)
            {
                if (txtARightBotY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColRBY = float.Parse(txtARightBotY.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, AColRBY, 0, 0);
            }
            else if (btnATopZMove == (Button)sender)
            {
                if (txtATopZ.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColTopZ = float.Parse(txtATopZ.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, AColTopZ, 0, 0);
            }
            else if (btnABotZMove == (Button)sender)
            {
                if (txtABotZ.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float AColBotZ = float.Parse(txtABotZ.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, AColBotZ, 0, 0);
            }
            //B열   //////////////////////////////////
            if (btnBLtXMove == (Button)sender)
            {
                if (txtBLeftTopX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColLTX = float.Parse(txtALeftTopX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, BColLTX, 0, 0);
            }
            else if (btnBLtYMove == (Button)sender)
            {
                if (txtBLeftTopY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColLTY = float.Parse(txtALeftTopY.Text);

                GG.Equip.UD.InspectionStage_B.YAxis.PtpMoveCmd(GG.Equip, BColLTY, 0, 0);
            }

            else if (btnBLbXMove == (Button)sender)
            {
                if (txtBLeftBotX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColLBX = float.Parse(txtALeftBotX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, BColLBX, 0, 0);
            }
            else if (btnBLbYMove == (Button)sender)
            {
                if (txtBLeftBotY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColLBY = float.Parse(txtALeftBotY.Text);

                GG.Equip.UD.InspectionStage_B.YAxis.PtpMoveCmd(GG.Equip, BColLBY, 0, 0);
            }
            //A RIGHT
            else if (btnBRtXMove == (Button)sender)
            {
                if (txtBRightTopX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColRTX = float.Parse(txtBRightTopX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, BColRTX, 0, 0);
            }
            else if (btnBRtYMove == (Button)sender)
            {
                if (txtBRightTopY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColRTY = float.Parse(txtBRightTopY.Text);

                GG.Equip.UD.InspectionStage_B.YAxis.PtpMoveCmd(GG.Equip, BColRTY, 0, 0);
            }

            else if (btnBRbXMove == (Button)sender)
            {
                if (txtBRightBotX.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColRBX = float.Parse(txtARightBotX.Text);

                GG.Equip.UD.InspCamera.XAxis.PtpMoveCmd(GG.Equip, BColRBX, 0, 0);
            }
            else if (btnBRbYMove == (Button)sender)
            {
                if (txtBRightBotY.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColRBY = float.Parse(txtARightBotY.Text);

                GG.Equip.UD.InspectionStage_B.YAxis.PtpMoveCmd(GG.Equip, BColRBY, 0, 0);
            }
            else if (btnBTopZMove == (Button)sender)
            {
                if (txtBTopZ.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColTopZ = float.Parse(txtBTopZ.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, BColTopZ, 0, 0);
            }
            else if (btnBBotZMove == (Button)sender)
            {
                if (txtBBotZ.Text == null)
                {
                    MessageBox.Show("값 입력이 필요 합니다.", "검사 포지션", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                float BColBotpZ = float.Parse(txtBBotZ.Text);

                GG.Equip.UD.InspectionStage_A.YAxis.PtpMoveCmd(GG.Equip, BColBotpZ, 0, 0);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == sRecpName);
            UpdateInspPos(ref recp);
            GG.Equip.EqpRecipeMgr.Save();
        }

        public void UpdateInspPos(ref EqpRecipe recp)
        {
            #region UpdateInspPos
            //Insp Pos A열
            recp.ALeftTop.X = double.Parse(txtALeftTopX.Text);
            recp.ALeftTop.Y = double.Parse(txtALeftTopY.Text);
            recp.ARightTop.X = double.Parse(txtARightTopX.Text);
            recp.ARightTop.Y = double.Parse(txtARightTopY.Text);
            recp.ATopZ = double.Parse(txtATopZ.Text);
            recp.ALeftBot.X = double.Parse(txtALeftBotX.Text);
            recp.ALeftBot.Y = double.Parse(txtALeftBotY.Text);
            recp.ARightBot.X = double.Parse(txtARightBotX.Text);
            recp.ARightBot.Y = double.Parse(txtARightBotY.Text);
            recp.ABotZ = double.Parse(txtABotZ.Text);
            recp.AXSpeed = double.Parse(txtAXSpeed.Text);
            recp.AYSpeed = double.Parse(txtAYSpeed.Text);
            recp.AMoveSpeed = double.Parse(txtAMoveSpeed.Text);

            //Insp Pos B열
            recp.BLeftTop.X = double.Parse(txtBLeftTopX.Text);
            recp.BLeftTop.Y = double.Parse(txtBLeftTopY.Text);
            recp.BRightTop.X = double.Parse(txtBRightTopX.Text);
            recp.BRightTop.Y = double.Parse(txtBRightTopY.Text);
            recp.BTopZ = double.Parse(txtBTopZ.Text);
            recp.BLeftBot.X = double.Parse(txtBLeftBotX.Text);
            recp.BLeftBot.Y = double.Parse(txtBLeftBotY.Text);
            recp.BRightBot.X = double.Parse(txtBRightBotX.Text);
            recp.BRightBot.Y = double.Parse(txtBRightBotY.Text);
            recp.BBotZ = double.Parse(txtBBotZ.Text);
            recp.BXSpeed = double.Parse(txtBXSpeed.Text);
            recp.BYSpeed = double.Parse(txtBYSpeed.Text);
            recp.BMoveSpeed = double.Parse(txtBMoveSpeed.Text);
            #endregion
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}

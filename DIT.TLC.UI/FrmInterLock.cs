﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
namespace DIT.TLC.UI
{
    public partial class FrmInterLock : Form
    {
        public string InterlockMsg
        {
            get
            {
                return lblInterlockMsg.Text;
            }
            set
            {
                lblInterlockMsg.Text = value;
            }
        }
        public string DetailMsg
        {
            get
            {
                return lblDetailMsg.Text;
            }
            set
            {
                lblDetailMsg.Text = value;
            }
        }
        public FrmInterLock()
        {            
            InitializeComponent();            
            TopMost = true;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            base.OnClosing(e);
        }
        private void btnCheck_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}

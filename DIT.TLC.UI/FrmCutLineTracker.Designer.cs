﻿namespace DIT.TLC.UI
{
    partial class FrmCutLineTracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCutLineTracker));
            this.btn1Table = new System.Windows.Forms.Button();
            this.btnALine = new System.Windows.Forms.Button();
            this.btnBLine = new System.Windows.Forms.Button();
            this.btn2Table = new System.Windows.Forms.Button();
            this.btnAlign = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.cbbox = new System.Windows.Forms.ComboBox();
            this.lblLog = new System.Windows.Forms.TextBox();
            this.btnPlus = new System.Windows.Forms.Button();
            this.btnMinus = new System.Windows.Forms.Button();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn1Table
            // 
            this.btn1Table.BackColor = System.Drawing.SystemColors.Control;
            this.btn1Table.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn1Table.ForeColor = System.Drawing.Color.Black;
            this.btn1Table.Location = new System.Drawing.Point(12, 88);
            this.btn1Table.Name = "btn1Table";
            this.btn1Table.Size = new System.Drawing.Size(205, 70);
            this.btn1Table.TabIndex = 3;
            this.btn1Table.Text = "1Table";
            this.btn1Table.UseVisualStyleBackColor = false;
            // 
            // btnALine
            // 
            this.btnALine.BackColor = System.Drawing.SystemColors.Control;
            this.btnALine.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnALine.ForeColor = System.Drawing.Color.Black;
            this.btnALine.Location = new System.Drawing.Point(12, 12);
            this.btnALine.Name = "btnALine";
            this.btnALine.Size = new System.Drawing.Size(205, 70);
            this.btnALine.TabIndex = 1;
            this.btnALine.Text = "A-Line";
            this.btnALine.UseVisualStyleBackColor = false;
            // 
            // btnBLine
            // 
            this.btnBLine.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLine.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBLine.ForeColor = System.Drawing.Color.Black;
            this.btnBLine.Location = new System.Drawing.Point(223, 12);
            this.btnBLine.Name = "btnBLine";
            this.btnBLine.Size = new System.Drawing.Size(205, 70);
            this.btnBLine.TabIndex = 56;
            this.btnBLine.Text = "B-Line";
            this.btnBLine.UseVisualStyleBackColor = false;
            // 
            // btn2Table
            // 
            this.btn2Table.BackColor = System.Drawing.SystemColors.Control;
            this.btn2Table.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn2Table.ForeColor = System.Drawing.Color.Black;
            this.btn2Table.Location = new System.Drawing.Point(223, 88);
            this.btn2Table.Name = "btn2Table";
            this.btn2Table.Size = new System.Drawing.Size(205, 70);
            this.btn2Table.TabIndex = 4;
            this.btn2Table.Text = "2Table";
            this.btn2Table.UseVisualStyleBackColor = false;
            // 
            // btnAlign
            // 
            this.btnAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlign.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlign.ForeColor = System.Drawing.Color.Black;
            this.btnAlign.Location = new System.Drawing.Point(832, 15);
            this.btnAlign.Name = "btnAlign";
            this.btnAlign.Size = new System.Drawing.Size(180, 143);
            this.btnAlign.TabIndex = 5;
            this.btnAlign.Text = "ALIGN";
            this.btnAlign.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(862, 784);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(150, 40);
            this.btnClose.TabIndex = 464;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 171);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.427536F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.57246F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1000, 600);
            this.tableLayoutPanel2.TabIndex = 465;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.641366F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.98482F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.98482F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.38899F));
            this.tableLayoutPanel3.Controls.Add(this.cbbox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblLog, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnPlus, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnMinus, 2, 0);
            this.tableLayoutPanel3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(994, 34);
            this.tableLayoutPanel3.TabIndex = 60;
            // 
            // cbbox
            // 
            this.cbbox.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.cbbox.FormattingEnabled = true;
            this.cbbox.Location = new System.Drawing.Point(3, 3);
            this.cbbox.Name = "cbbox";
            this.cbbox.Size = new System.Drawing.Size(60, 28);
            this.cbbox.TabIndex = 0;
            // 
            // lblLog
            // 
            this.lblLog.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblLog.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLog.Location = new System.Drawing.Point(147, 7);
            this.lblLog.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.lblLog.Name = "lblLog";
            this.lblLog.ReadOnly = true;
            this.lblLog.Size = new System.Drawing.Size(844, 22);
            this.lblLog.TabIndex = 0;
            this.lblLog.Text = " TEXT";
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnPlus.Location = new System.Drawing.Point(69, 3);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(33, 28);
            this.btnPlus.TabIndex = 6;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            // 
            // btnMinus
            // 
            this.btnMinus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMinus.Location = new System.Drawing.Point(108, 3);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(33, 28);
            this.btnMinus.TabIndex = 7;
            this.btnMinus.Text = "-";
            this.btnMinus.UseVisualStyleBackColor = true;
            // 
            // FrmCutLineTracker
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1024, 836);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btn1Table);
            this.Controls.Add(this.btnALine);
            this.Controls.Add(this.btnAlign);
            this.Controls.Add(this.btnBLine);
            this.Controls.Add(this.btn2Table);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCutLineTracker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "컷 라인 트래커";
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn1Table;
        private System.Windows.Forms.Button btnALine;
        private System.Windows.Forms.Button btnBLine;
        private System.Windows.Forms.Button btn2Table;
        private System.Windows.Forms.Button btnAlign;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox cbbox;
        private System.Windows.Forms.TextBox lblLog;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Button btnMinus;
    }
}
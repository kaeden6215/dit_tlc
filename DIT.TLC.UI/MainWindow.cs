﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class MainWindow : UserControl
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // 시작 버튼 클릭
        private void btn_start_Click(object sender, EventArgs e)
        {

        }

        // 일시정지 버튼 클릭
        private void btn_pause_Click(object sender, EventArgs e)
        {

        }

        // 작업종료 버튼 클릭
        private void btn_stop_Click(object sender, EventArgs e)
        {

        }
    }
}

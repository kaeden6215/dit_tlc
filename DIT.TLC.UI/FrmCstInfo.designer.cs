﻿namespace DIT.TLC.UI
{
    partial class FrmCstInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCstInfo));
            this.lblCstName = new System.Windows.Forms.Label();
            this.btnCstApply = new System.Windows.Forms.Button();
            this.btnCstCancel = new System.Windows.Forms.Button();
            this.lblPortStatus = new System.Windows.Forms.Label();
            this.txtCstSlotCount = new System.Windows.Forms.TextBox();
            this.comboPortStatus = new System.Windows.Forms.ComboBox();
            this.lblCstSlotCount = new System.Windows.Forms.Label();
            this.lblCstCellCount = new System.Windows.Forms.Label();
            this.txtCstCellCount = new System.Windows.Forms.TextBox();
            this.lblCstInCount = new System.Windows.Forms.Label();
            this.txtCstInCount = new System.Windows.Forms.TextBox();
            this.lblCstOutCount = new System.Windows.Forms.Label();
            this.txtCstOutCount = new System.Windows.Forms.TextBox();
            this.lblCstPPID = new System.Windows.Forms.Label();
            this.txtCstPPID = new System.Windows.Forms.TextBox();
            this.lblCstInfo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCstName
            // 
            this.lblCstName.AutoEllipsis = true;
            this.lblCstName.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCstName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCstName.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstName.ForeColor = System.Drawing.Color.Black;
            this.lblCstName.Location = new System.Drawing.Point(0, 0);
            this.lblCstName.Name = "lblCstName";
            this.lblCstName.Size = new System.Drawing.Size(260, 26);
            this.lblCstName.TabIndex = 11;
            this.lblCstName.Text = "■ Port Name";
            this.lblCstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCstApply
            // 
            this.btnCstApply.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstApply.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstApply.ForeColor = System.Drawing.Color.Black;
            this.btnCstApply.Location = new System.Drawing.Point(4, 368);
            this.btnCstApply.Name = "btnCstApply";
            this.btnCstApply.Size = new System.Drawing.Size(123, 45);
            this.btnCstApply.TabIndex = 55;
            this.btnCstApply.Text = "적용";
            this.btnCstApply.UseVisualStyleBackColor = false;
            this.btnCstApply.Click += new System.EventHandler(this.btnCstApply_Click);
            // 
            // btnCstCancel
            // 
            this.btnCstCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCstCancel.Location = new System.Drawing.Point(133, 368);
            this.btnCstCancel.Name = "btnCstCancel";
            this.btnCstCancel.Size = new System.Drawing.Size(123, 45);
            this.btnCstCancel.TabIndex = 54;
            this.btnCstCancel.Text = "취소";
            this.btnCstCancel.UseVisualStyleBackColor = false;
            this.btnCstCancel.Click += new System.EventHandler(this.btnCstCancel_Click);
            // 
            // lblPortStatus
            // 
            this.lblPortStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPortStatus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPortStatus.ForeColor = System.Drawing.Color.Black;
            this.lblPortStatus.Location = new System.Drawing.Point(4, 37);
            this.lblPortStatus.Name = "lblPortStatus";
            this.lblPortStatus.Size = new System.Drawing.Size(123, 29);
            this.lblPortStatus.TabIndex = 57;
            this.lblPortStatus.Text = "포트 상태";
            this.lblPortStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCstSlotCount
            // 
            this.txtCstSlotCount.BackColor = System.Drawing.Color.White;
            this.txtCstSlotCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCstSlotCount.ForeColor = System.Drawing.Color.Black;
            this.txtCstSlotCount.Location = new System.Drawing.Point(133, 35);
            this.txtCstSlotCount.Name = "txtCstSlotCount";
            this.txtCstSlotCount.Size = new System.Drawing.Size(123, 29);
            this.txtCstSlotCount.TabIndex = 56;
            // 
            // comboPortStatus
            // 
            this.comboPortStatus.FormattingEnabled = true;
            this.comboPortStatus.Location = new System.Drawing.Point(133, 39);
            this.comboPortStatus.Name = "comboPortStatus";
            this.comboPortStatus.Size = new System.Drawing.Size(123, 23);
            this.comboPortStatus.TabIndex = 58;
            // 
            // lblCstSlotCount
            // 
            this.lblCstSlotCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstSlotCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstSlotCount.ForeColor = System.Drawing.Color.Black;
            this.lblCstSlotCount.Location = new System.Drawing.Point(4, 35);
            this.lblCstSlotCount.Name = "lblCstSlotCount";
            this.lblCstSlotCount.Size = new System.Drawing.Size(123, 29);
            this.lblCstSlotCount.TabIndex = 59;
            this.lblCstSlotCount.Text = "슬롯 갯수";
            this.lblCstSlotCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCstCellCount
            // 
            this.lblCstCellCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstCellCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstCellCount.ForeColor = System.Drawing.Color.Black;
            this.lblCstCellCount.Location = new System.Drawing.Point(4, 70);
            this.lblCstCellCount.Name = "lblCstCellCount";
            this.lblCstCellCount.Size = new System.Drawing.Size(123, 29);
            this.lblCstCellCount.TabIndex = 61;
            this.lblCstCellCount.Text = "셀 갯수";
            this.lblCstCellCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCstCellCount
            // 
            this.txtCstCellCount.BackColor = System.Drawing.Color.White;
            this.txtCstCellCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCstCellCount.ForeColor = System.Drawing.Color.Black;
            this.txtCstCellCount.Location = new System.Drawing.Point(133, 70);
            this.txtCstCellCount.Name = "txtCstCellCount";
            this.txtCstCellCount.Size = new System.Drawing.Size(123, 29);
            this.txtCstCellCount.TabIndex = 60;
            // 
            // lblCstInCount
            // 
            this.lblCstInCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstInCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstInCount.ForeColor = System.Drawing.Color.Black;
            this.lblCstInCount.Location = new System.Drawing.Point(4, 105);
            this.lblCstInCount.Name = "lblCstInCount";
            this.lblCstInCount.Size = new System.Drawing.Size(123, 29);
            this.lblCstInCount.TabIndex = 63;
            this.lblCstInCount.Text = "로딩 셀 갯수";
            this.lblCstInCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCstInCount
            // 
            this.txtCstInCount.BackColor = System.Drawing.Color.White;
            this.txtCstInCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCstInCount.ForeColor = System.Drawing.Color.Black;
            this.txtCstInCount.Location = new System.Drawing.Point(133, 105);
            this.txtCstInCount.Name = "txtCstInCount";
            this.txtCstInCount.ReadOnly = true;
            this.txtCstInCount.Size = new System.Drawing.Size(123, 29);
            this.txtCstInCount.TabIndex = 62;
            // 
            // lblCstOutCount
            // 
            this.lblCstOutCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstOutCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstOutCount.ForeColor = System.Drawing.Color.Black;
            this.lblCstOutCount.Location = new System.Drawing.Point(4, 140);
            this.lblCstOutCount.Name = "lblCstOutCount";
            this.lblCstOutCount.Size = new System.Drawing.Size(123, 29);
            this.lblCstOutCount.TabIndex = 65;
            this.lblCstOutCount.Text = "언로딩 셀 갯수";
            this.lblCstOutCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCstOutCount
            // 
            this.txtCstOutCount.BackColor = System.Drawing.Color.White;
            this.txtCstOutCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCstOutCount.ForeColor = System.Drawing.Color.Black;
            this.txtCstOutCount.Location = new System.Drawing.Point(133, 140);
            this.txtCstOutCount.Name = "txtCstOutCount";
            this.txtCstOutCount.ReadOnly = true;
            this.txtCstOutCount.Size = new System.Drawing.Size(123, 29);
            this.txtCstOutCount.TabIndex = 64;
            // 
            // lblCstPPID
            // 
            this.lblCstPPID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstPPID.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstPPID.ForeColor = System.Drawing.Color.Black;
            this.lblCstPPID.Location = new System.Drawing.Point(4, 175);
            this.lblCstPPID.Name = "lblCstPPID";
            this.lblCstPPID.Size = new System.Drawing.Size(123, 29);
            this.lblCstPPID.TabIndex = 67;
            this.lblCstPPID.Text = "PPID";
            this.lblCstPPID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCstPPID
            // 
            this.txtCstPPID.BackColor = System.Drawing.Color.White;
            this.txtCstPPID.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCstPPID.ForeColor = System.Drawing.Color.Black;
            this.txtCstPPID.Location = new System.Drawing.Point(133, 175);
            this.txtCstPPID.Name = "txtCstPPID";
            this.txtCstPPID.ReadOnly = true;
            this.txtCstPPID.Size = new System.Drawing.Size(123, 29);
            this.txtCstPPID.TabIndex = 66;
            // 
            // lblCstInfo
            // 
            this.lblCstInfo.AutoEllipsis = true;
            this.lblCstInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCstInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCstInfo.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstInfo.ForeColor = System.Drawing.Color.Black;
            this.lblCstInfo.Location = new System.Drawing.Point(0, 0);
            this.lblCstInfo.Name = "lblCstInfo";
            this.lblCstInfo.Size = new System.Drawing.Size(260, 26);
            this.lblCstInfo.TabIndex = 68;
            this.lblCstInfo.Text = "■ 카세트 정보";
            this.lblCstInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblCstInfo);
            this.panel1.Controls.Add(this.lblCstPPID);
            this.panel1.Controls.Add(this.txtCstSlotCount);
            this.panel1.Controls.Add(this.txtCstPPID);
            this.panel1.Controls.Add(this.lblCstSlotCount);
            this.panel1.Controls.Add(this.lblCstOutCount);
            this.panel1.Controls.Add(this.txtCstCellCount);
            this.panel1.Controls.Add(this.txtCstOutCount);
            this.panel1.Controls.Add(this.lblCstCellCount);
            this.panel1.Controls.Add(this.lblCstInCount);
            this.panel1.Controls.Add(this.txtCstInCount);
            this.panel1.Location = new System.Drawing.Point(0, 149);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 213);
            this.panel1.TabIndex = 69;
            // 
            // FrmCstInfo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(260, 419);
            this.ControlBox = false;
            this.Controls.Add(this.comboPortStatus);
            this.Controls.Add(this.lblPortStatus);
            this.Controls.Add(this.btnCstApply);
            this.Controls.Add(this.btnCstCancel);
            this.Controls.Add(this.lblCstName);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCstInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "카세트 정보";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblCstName;
        private System.Windows.Forms.Button btnCstApply;
        private System.Windows.Forms.Button btnCstCancel;
        private System.Windows.Forms.Label lblPortStatus;
        private System.Windows.Forms.TextBox txtCstSlotCount;
        private System.Windows.Forms.ComboBox comboPortStatus;
        private System.Windows.Forms.Label lblCstSlotCount;
        private System.Windows.Forms.Label lblCstCellCount;
        private System.Windows.Forms.TextBox txtCstCellCount;
        private System.Windows.Forms.Label lblCstInCount;
        private System.Windows.Forms.TextBox txtCstInCount;
        private System.Windows.Forms.Label lblCstOutCount;
        private System.Windows.Forms.TextBox txtCstOutCount;
        private System.Windows.Forms.Label lblCstPPID;
        private System.Windows.Forms.TextBox txtCstPPID;
        internal System.Windows.Forms.Label lblCstInfo;
        private System.Windows.Forms.Panel panel1;
    }
}
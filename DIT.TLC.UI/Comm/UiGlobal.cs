﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DIT.TLC.UI
{
    public class UiGlobal
    {
        public static Color SET_C = Color.Red;
        public static Color UNSET_C = Color.DimGray;

        public static Color SELECT_C = Color.DodgerBlue;
        public static Color UNSELECT_C = SystemColors.Control;

        public static Color EXIST_C = Color.OliveDrab;
        public static Color UNEXIST_C = Color.DimGray;

        public static Color ON_C = Color.OliveDrab;
        public static Color OFF_C = SystemColors.Control;

        public static Color LASERON_C = Color.Lime;
        public static Color LASEROFF_C = Color.Black;

        public static Color OK_C = Color.Lime;
        public static Color NG_C = Color.Red;

        public static Color READY_C = Color.Yellow;

        public static Color BLACK_C  = Color.Yellow;
        public static Color BLUE_C = Color.Yellow;

    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlMainWindow
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        } 

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnKeyswitch = new System.Windows.Forms.Button();
            this.btnPm = new System.Windows.Forms.Button();
            this.btnDoorOpen = new System.Windows.Forms.Button();
            this.btnLight = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnLoadingOutMuting = new System.Windows.Forms.Button();
            this.btnLoadingInMuting = new System.Windows.Forms.Button();
            this.lblLoading = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnDummyTankSet = new System.Windows.Forms.Button();
            this.btnDummyTankGet = new System.Windows.Forms.Button();
            this.lblDummyTank = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCstTotalCount = new System.Windows.Forms.Label();
            this.lblCstTotalCountInfo = new System.Windows.Forms.Label();
            this.lblCstReadCount = new System.Windows.Forms.Label();
            this.lblCstReadCountInfo = new System.Windows.Forms.Label();
            this.lblCst = new System.Windows.Forms.Label();
            this.lblDayTotalCount = new System.Windows.Forms.Label();
            this.lblDayTotalCountInfo = new System.Windows.Forms.Label();
            this.lblDayReadCount = new System.Windows.Forms.Label();
            this.lblDayReadCountInfo = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.lbCstTotalCount = new System.Windows.Forms.Label();
            this.lb_cst_total_count_info = new System.Windows.Forms.Label();
            this.lbCstReadCount = new System.Windows.Forms.Label();
            this.lb_cst_read_count_info = new System.Windows.Forms.Label();
            this.lb_cst = new System.Windows.Forms.Label();
            this.lblMcrCount = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUnloadingOutMuting = new System.Windows.Forms.Button();
            this.btnUnloadingInMuting = new System.Windows.Forms.Button();
            this.lblUnloading = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblBreakingCount = new System.Windows.Forms.Label();
            this.lblBreakingCountInfo = new System.Windows.Forms.Label();
            this.btnBreakingCountClear = new System.Windows.Forms.Button();
            this.lblNameBreakingCount = new System.Windows.Forms.Label();
            this.btnSafetyReset = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnIonizerUnloader = new System.Windows.Forms.Button();
            this.btnIonizerBreak = new System.Windows.Forms.Button();
            this.btnIonizerProcess = new System.Windows.Forms.Button();
            this.btnIonizerLoader = new System.Windows.Forms.Button();
            this.lblIonizer = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtAlignBreak4OffsetA = new System.Windows.Forms.TextBox();
            this.txtAlignBreak3OffsetA = new System.Windows.Forms.TextBox();
            this.txtAlignPre2OffsetA = new System.Windows.Forms.TextBox();
            this.lblAlignOffsetA2 = new System.Windows.Forms.Label();
            this.txtAlignBreak4OffsetY = new System.Windows.Forms.TextBox();
            this.txtAlignBreak3OffsetY = new System.Windows.Forms.TextBox();
            this.txtAlignPre2OffsetY = new System.Windows.Forms.TextBox();
            this.lblAlignOffsetY2 = new System.Windows.Forms.Label();
            this.txtAlignBreak4OffsetX = new System.Windows.Forms.TextBox();
            this.txtAlignBreak3OffsetX = new System.Windows.Forms.TextBox();
            this.txtAlignPre2OffsetX = new System.Windows.Forms.TextBox();
            this.lblAlignOffsetX2 = new System.Windows.Forms.Label();
            this.txtAlignBreak4Result = new System.Windows.Forms.TextBox();
            this.txtAlignBreak3Result = new System.Windows.Forms.TextBox();
            this.txtAlignPre2Result = new System.Windows.Forms.TextBox();
            this.lblAlignResult2 = new System.Windows.Forms.Label();
            this.lblAlignBreak4 = new System.Windows.Forms.Label();
            this.lblAlignBreak3 = new System.Windows.Forms.Label();
            this.lblAlignPre2 = new System.Windows.Forms.Label();
            this.lblAlignName2 = new System.Windows.Forms.Label();
            this.txtAlignBreak2OffsetA = new System.Windows.Forms.TextBox();
            this.txtAlignBreak1OffsetA = new System.Windows.Forms.TextBox();
            this.txtAlignPre1OffsetA = new System.Windows.Forms.TextBox();
            this.lblAlignOffsetA1 = new System.Windows.Forms.Label();
            this.txtAlignBreak2OffsetY = new System.Windows.Forms.TextBox();
            this.txtAlignBreak1OffsetY = new System.Windows.Forms.TextBox();
            this.txtAlignPre1OffsetY = new System.Windows.Forms.TextBox();
            this.lblAlignOffsetY1 = new System.Windows.Forms.Label();
            this.txtAlignBreak2OffsetX = new System.Windows.Forms.TextBox();
            this.txtAlignBreak1OffsetX = new System.Windows.Forms.TextBox();
            this.txtAlignPre1OffsetX = new System.Windows.Forms.TextBox();
            this.lblAlignOffsetX1 = new System.Windows.Forms.Label();
            this.txtAlignBreak2Result = new System.Windows.Forms.TextBox();
            this.txtAlignBreak1Result = new System.Windows.Forms.TextBox();
            this.txtAlignPre1Result = new System.Windows.Forms.TextBox();
            this.lblAlignResult1 = new System.Windows.Forms.Label();
            this.lblAlignBreak2 = new System.Windows.Forms.Label();
            this.lblAlignBreak1 = new System.Windows.Forms.Label();
            this.lblAlignPre1 = new System.Windows.Forms.Label();
            this.lblAlignName1 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.btnLaserCover = new System.Windows.Forms.Button();
            this.lblLoaderBox = new System.Windows.Forms.Label();
            this.lblEms3 = new System.Windows.Forms.Label();
            this.lblProcessBox = new System.Windows.Forms.Label();
            this.lblEms5 = new System.Windows.Forms.Label();
            this.lblUnloaderBox = new System.Windows.Forms.Label();
            this.btnACSTSkip = new System.Windows.Forms.Button();
            this.btnBCSTSkip = new System.Windows.Forms.Button();
            this.lblEms4 = new System.Windows.Forms.Label();
            this.btnDelayTopDoor11 = new System.Windows.Forms.Button();
            this.lblZPosGap = new System.Windows.Forms.Label();
            this.btnDelayTopDoor1 = new System.Windows.Forms.Button();
            this.lblAvgPower = new System.Windows.Forms.Label();
            this.lblEms1 = new System.Windows.Forms.Label();
            this.lblAvgPowerInfo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblLdMut = new System.Windows.Forms.Label();
            this.lblTartget = new System.Windows.Forms.Label();
            this.lblZPosGapInfo = new System.Windows.Forms.Label();
            this.lblTargetInfo = new System.Windows.Forms.Label();
            this.btnUldLight = new System.Windows.Forms.Button();
            this.btnProcessLight = new System.Windows.Forms.Button();
            this.btnDelayTopDoor10 = new System.Windows.Forms.Button();
            this.btnLdLight = new System.Windows.Forms.Button();
            this.btnDelayTopDoor9 = new System.Windows.Forms.Button();
            this.btnDelayTopDoor8 = new System.Windows.Forms.Button();
            this.btnDelayTopDoor7 = new System.Windows.Forms.Button();
            this.btnDelayTopDoor5 = new System.Windows.Forms.Button();
            this.btnDelayTopDoor6 = new System.Windows.Forms.Button();
            this.btnDelayTopDoor4 = new System.Windows.Forms.Button();
            this.lblAutoTeach = new System.Windows.Forms.Label();
            this.lblShutterOpen = new System.Windows.Forms.Label();
            this.lblLock = new System.Windows.Forms.Label();
            this.lblEms2 = new System.Windows.Forms.Label();
            this.lblGrabEms3 = new System.Windows.Forms.Label();
            this.lblGrabEms2 = new System.Windows.Forms.Label();
            this.lblGrabEms1 = new System.Windows.Forms.Label();
            this.lblEms6 = new System.Windows.Forms.Label();
            this.btnDelayTopDoor2 = new System.Windows.Forms.Button();
            this.btnDelayTopDoor3 = new System.Windows.Forms.Button();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.lbleEquipState = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblShutter = new System.Windows.Forms.Label();
            this.lblShutterInfo = new System.Windows.Forms.Label();
            this.lblPower = new System.Windows.Forms.Label();
            this.lblPowerInfo = new System.Windows.Forms.Label();
            this.lblDivider = new System.Windows.Forms.Label();
            this.lblDividerInfo = new System.Windows.Forms.Label();
            this.lblPd7Power = new System.Windows.Forms.Label();
            this.lblPd7PowerInfo = new System.Windows.Forms.Label();
            this.lblOutAmplifier = new System.Windows.Forms.Label();
            this.lblAmplifier = new System.Windows.Forms.Label();
            this.lblBurst = new System.Windows.Forms.Label();
            this.lblPulseMode = new System.Windows.Forms.Label();
            this.lblOutAmplifierInfo = new System.Windows.Forms.Label();
            this.lblAmplifierInfo = new System.Windows.Forms.Label();
            this.lblBurstInfo = new System.Windows.Forms.Label();
            this.lblPulseModeInfo = new System.Windows.Forms.Label();
            this.lblLaserInfo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtTactTime = new System.Windows.Forms.TextBox();
            this.txtRunCount = new System.Windows.Forms.TextBox();
            this.txtRunTime = new System.Windows.Forms.TextBox();
            this.txtStartTime = new System.Windows.Forms.TextBox();
            this.txtCellSize = new System.Windows.Forms.TextBox();
            this.txtProcess = new System.Windows.Forms.TextBox();
            this.txtRunName = new System.Windows.Forms.TextBox();
            this.txtRecipeName = new System.Windows.Forms.TextBox();
            this.lblTactTime = new System.Windows.Forms.Label();
            this.lblRunCount = new System.Windows.Forms.Label();
            this.lblRunTime = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.lblCellSize = new System.Windows.Forms.Label();
            this.lblProcess = new System.Windows.Forms.Label();
            this.lblRecipeName = new System.Windows.Forms.Label();
            this.lblRunName = new System.Windows.Forms.Label();
            this.lblRunInfo = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblInsp2ConnectInfo = new System.Windows.Forms.Label();
            this.lblInsp1ConnectInfo = new System.Windows.Forms.Label();
            this.lblSerialConnectInfo = new System.Windows.Forms.Label();
            this.lblSequenceConnectInfo = new System.Windows.Forms.Label();
            this.lblLaserConnectInfo = new System.Windows.Forms.Label();
            this.lblUmacConnectInfo = new System.Windows.Forms.Label();
            this.lblAjinConnectInfo = new System.Windows.Forms.Label();
            this.lblInsp2Connect = new System.Windows.Forms.Label();
            this.lblSerialConnect = new System.Windows.Forms.Label();
            this.lblInsp1Connect = new System.Windows.Forms.Label();
            this.lblSequenceConnect = new System.Windows.Forms.Label();
            this.lblLaserConnect = new System.Windows.Forms.Label();
            this.lblUmacConnect = new System.Windows.Forms.Label();
            this.lblAjinConnect = new System.Windows.Forms.Label();
            this.lblCimConnect = new System.Windows.Forms.Label();
            this.lblCimConnectInfo = new System.Windows.Forms.Label();
            this.lblProcessConnectInfo = new System.Windows.Forms.Label();
            this.ucrlCstCellCountLDA = new DIT.TLC.UI.Main.UcrlCstCellCount();
            this.ucrlCstCellCountLDB = new DIT.TLC.UI.Main.UcrlCstCellCount();
            this.ucrlCstCellCountULDA = new DIT.TLC.UI.Main.UcrlCstCellCount();
            this.ucrlCstCellCountULDB = new DIT.TLC.UI.Main.UcrlCstCellCount();
            this.UcrlDtvFineAlign = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvBreakAlign = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvInspStageA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvPreAlign = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvBreakStageA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvInspStageB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvCstLoaderA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvLoaderTransferA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvIRCutStageA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvBeforeTransferA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvAfterInspTransferA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvCstUnloaderA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvCstLoaderB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvLoader = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvLoaderTransferB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvIRCutStageB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvAfterIRCutTransferA = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvAfterIRCutTransferB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvBreakStageB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvBeforeTransferB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvAfterInspTransferB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvUnloader = new DIT.TLC.UI.Main.UcrlDetailTactView();
            this.UcrlDtvCstUnloaderB = new DIT.TLC.UI.Main.UcrlDetailTactView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnStart.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStart.ForeColor = System.Drawing.Color.Black;
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStart.Location = new System.Drawing.Point(2, 23);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(132, 50);
            this.btnStart.TabIndex = 32;
            this.btnStart.Text = "시작";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.SystemColors.Control;
            this.btnPause.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPause.ForeColor = System.Drawing.Color.Black;
            this.btnPause.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPause.Location = new System.Drawing.Point(2, 79);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(132, 50);
            this.btnPause.TabIndex = 34;
            this.btnPause.Text = "일시정지";
            this.btnPause.UseVisualStyleBackColor = false;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnStop.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStop.ForeColor = System.Drawing.Color.Black;
            this.btnStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStop.Location = new System.Drawing.Point(2, 135);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(132, 50);
            this.btnStop.TabIndex = 35;
            this.btnStop.Text = "작업종료";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnKeyswitch
            // 
            this.btnKeyswitch.BackColor = System.Drawing.SystemColors.Control;
            this.btnKeyswitch.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnKeyswitch.ForeColor = System.Drawing.Color.Black;
            this.btnKeyswitch.Location = new System.Drawing.Point(2, 230);
            this.btnKeyswitch.Name = "btnKeyswitch";
            this.btnKeyswitch.Size = new System.Drawing.Size(132, 50);
            this.btnKeyswitch.TabIndex = 36;
            this.btnKeyswitch.Text = "오토 전환 키 스위치";
            this.btnKeyswitch.UseVisualStyleBackColor = false;
            this.btnKeyswitch.Click += new System.EventHandler(this.btnKeyswitch_Click);
            // 
            // btnPm
            // 
            this.btnPm.BackColor = System.Drawing.SystemColors.Control;
            this.btnPm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPm.ForeColor = System.Drawing.Color.Black;
            this.btnPm.Location = new System.Drawing.Point(1, 286);
            this.btnPm.Name = "btnPm";
            this.btnPm.Size = new System.Drawing.Size(132, 50);
            this.btnPm.TabIndex = 37;
            this.btnPm.Text = "PM";
            this.btnPm.UseVisualStyleBackColor = false;
            // 
            // btnDoorOpen
            // 
            this.btnDoorOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnDoorOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDoorOpen.ForeColor = System.Drawing.Color.Black;
            this.btnDoorOpen.Location = new System.Drawing.Point(2, 342);
            this.btnDoorOpen.Name = "btnDoorOpen";
            this.btnDoorOpen.Size = new System.Drawing.Size(132, 50);
            this.btnDoorOpen.TabIndex = 38;
            this.btnDoorOpen.Text = "Door-Open";
            this.btnDoorOpen.UseVisualStyleBackColor = false;
            this.btnDoorOpen.Click += new System.EventHandler(this.btnDoorOpen_Click);
            // 
            // btnLight
            // 
            this.btnLight.BackColor = System.Drawing.SystemColors.Control;
            this.btnLight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLight.ForeColor = System.Drawing.Color.Black;
            this.btnLight.Location = new System.Drawing.Point(2, 398);
            this.btnLight.Name = "btnLight";
            this.btnLight.Size = new System.Drawing.Size(132, 50);
            this.btnLight.TabIndex = 39;
            this.btnLight.Text = "설비조명";
            this.btnLight.UseVisualStyleBackColor = false;
            this.btnLight.Click += new System.EventHandler(this.btnLight_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.panel5);
            this.splitContainer1.Panel1.Controls.Add(this.panel8);
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.panel7);
            this.splitContainer1.Panel1.Controls.Add(this.ucrlCstCellCountLDA);
            this.splitContainer1.Panel1.Controls.Add(this.ucrlCstCellCountLDB);
            this.splitContainer1.Panel1.Controls.Add(this.btnSafetyReset);
            this.splitContainer1.Panel1.Controls.Add(this.ucrlCstCellCountULDA);
            this.splitContainer1.Panel1.Controls.Add(this.ucrlCstCellCountULDB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvFineAlign);
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvBreakAlign);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvInspStageA);
            this.splitContainer1.Panel1.Controls.Add(this.panel9);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvPreAlign);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvBreakStageA);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvInspStageB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvCstLoaderA);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvLoaderTransferA);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvIRCutStageA);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvBeforeTransferA);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvAfterInspTransferA);
            this.splitContainer1.Panel1.Controls.Add(this.panel10);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvCstUnloaderA);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvCstLoaderB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvLoader);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvLoaderTransferB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvIRCutStageB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvAfterIRCutTransferA);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvAfterIRCutTransferB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvBreakStageB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvBeforeTransferB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvAfterInspTransferB);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvUnloader);
            this.splitContainer1.Panel1.Controls.Add(this.UcrlDtvCstUnloaderB);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Panel2.Controls.Add(this.btnLight);
            this.splitContainer1.Panel2.Controls.Add(this.btnDoorOpen);
            this.splitContainer1.Panel2.Controls.Add(this.btnPm);
            this.splitContainer1.Panel2.Controls.Add(this.btnKeyswitch);
            this.splitContainer1.Panel2.Controls.Add(this.btnStop);
            this.splitContainer1.Panel2.Controls.Add(this.btnPause);
            this.splitContainer1.Panel2.Controls.Add(this.btnStart);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnLoadingOutMuting);
            this.panel5.Controls.Add(this.btnLoadingInMuting);
            this.panel5.Controls.Add(this.lblLoading);
            this.panel5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel5.Location = new System.Drawing.Point(4, 745);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(73, 112);
            this.panel5.TabIndex = 500;
            // 
            // btnLoadingOutMuting
            // 
            this.btnLoadingOutMuting.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingOutMuting.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnLoadingOutMuting.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingOutMuting.Location = new System.Drawing.Point(5, 71);
            this.btnLoadingOutMuting.Name = "btnLoadingOutMuting";
            this.btnLoadingOutMuting.Size = new System.Drawing.Size(63, 35);
            this.btnLoadingOutMuting.TabIndex = 95;
            this.btnLoadingOutMuting.Text = "배출 뮤팅";
            this.btnLoadingOutMuting.UseVisualStyleBackColor = false;
            // 
            // btnLoadingInMuting
            // 
            this.btnLoadingInMuting.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingInMuting.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnLoadingInMuting.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingInMuting.Location = new System.Drawing.Point(5, 30);
            this.btnLoadingInMuting.Name = "btnLoadingInMuting";
            this.btnLoadingInMuting.Size = new System.Drawing.Size(63, 35);
            this.btnLoadingInMuting.TabIndex = 94;
            this.btnLoadingInMuting.Text = "투입 뮤팅";
            this.btnLoadingInMuting.UseVisualStyleBackColor = false;
            // 
            // lblLoading
            // 
            this.lblLoading.AutoEllipsis = true;
            this.lblLoading.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoading.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoading.ForeColor = System.Drawing.Color.Black;
            this.lblLoading.Location = new System.Drawing.Point(0, 0);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(71, 20);
            this.lblLoading.TabIndex = 9;
            this.lblLoading.Text = "■ 로딩";
            this.lblLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnDummyTankSet);
            this.panel8.Controls.Add(this.btnDummyTankGet);
            this.panel8.Controls.Add(this.lblDummyTank);
            this.panel8.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel8.Location = new System.Drawing.Point(162, 745);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(99, 112);
            this.panel8.TabIndex = 501;
            // 
            // btnDummyTankSet
            // 
            this.btnDummyTankSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnDummyTankSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDummyTankSet.ForeColor = System.Drawing.Color.Black;
            this.btnDummyTankSet.Location = new System.Drawing.Point(5, 71);
            this.btnDummyTankSet.Name = "btnDummyTankSet";
            this.btnDummyTankSet.Size = new System.Drawing.Size(87, 35);
            this.btnDummyTankSet.TabIndex = 95;
            this.btnDummyTankSet.Text = "배출";
            this.btnDummyTankSet.UseVisualStyleBackColor = false;
            // 
            // btnDummyTankGet
            // 
            this.btnDummyTankGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnDummyTankGet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDummyTankGet.ForeColor = System.Drawing.Color.Black;
            this.btnDummyTankGet.Location = new System.Drawing.Point(5, 30);
            this.btnDummyTankGet.Name = "btnDummyTankGet";
            this.btnDummyTankGet.Size = new System.Drawing.Size(87, 35);
            this.btnDummyTankGet.TabIndex = 94;
            this.btnDummyTankGet.Text = "투입";
            this.btnDummyTankGet.UseVisualStyleBackColor = false;
            // 
            // lblDummyTank
            // 
            this.lblDummyTank.AutoEllipsis = true;
            this.lblDummyTank.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDummyTank.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDummyTank.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblDummyTank.ForeColor = System.Drawing.Color.Black;
            this.lblDummyTank.Location = new System.Drawing.Point(0, 0);
            this.lblDummyTank.Name = "lblDummyTank";
            this.lblDummyTank.Size = new System.Drawing.Size(97, 20);
            this.lblDummyTank.TabIndex = 9;
            this.lblDummyTank.Text = "■ Dummy Tank";
            this.lblDummyTank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblCstTotalCount);
            this.panel3.Controls.Add(this.lblCstTotalCountInfo);
            this.panel3.Controls.Add(this.lblCstReadCount);
            this.panel3.Controls.Add(this.lblCstReadCountInfo);
            this.panel3.Controls.Add(this.lblCst);
            this.panel3.Controls.Add(this.lblDayTotalCount);
            this.panel3.Controls.Add(this.lblDayTotalCountInfo);
            this.panel3.Controls.Add(this.lblDayReadCount);
            this.panel3.Controls.Add(this.lblDayReadCountInfo);
            this.panel3.Controls.Add(this.lblDay);
            this.panel3.Controls.Add(this.lbCstTotalCount);
            this.panel3.Controls.Add(this.lb_cst_total_count_info);
            this.panel3.Controls.Add(this.lbCstReadCount);
            this.panel3.Controls.Add(this.lb_cst_read_count_info);
            this.panel3.Controls.Add(this.lb_cst);
            this.panel3.Controls.Add(this.lblMcrCount);
            this.panel3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel3.Location = new System.Drawing.Point(266, 745);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(206, 112);
            this.panel3.TabIndex = 498;
            // 
            // lblCstTotalCount
            // 
            this.lblCstTotalCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstTotalCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstTotalCount.Location = new System.Drawing.Point(142, 89);
            this.lblCstTotalCount.Name = "lblCstTotalCount";
            this.lblCstTotalCount.Size = new System.Drawing.Size(54, 20);
            this.lblCstTotalCount.TabIndex = 24;
            this.lblCstTotalCount.Text = "0";
            this.lblCstTotalCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCstTotalCountInfo
            // 
            this.lblCstTotalCountInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstTotalCountInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstTotalCountInfo.Location = new System.Drawing.Point(56, 89);
            this.lblCstTotalCountInfo.Name = "lblCstTotalCountInfo";
            this.lblCstTotalCountInfo.Size = new System.Drawing.Size(81, 20);
            this.lblCstTotalCountInfo.TabIndex = 23;
            this.lblCstTotalCountInfo.Text = "total count :";
            this.lblCstTotalCountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCstReadCount
            // 
            this.lblCstReadCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstReadCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstReadCount.Location = new System.Drawing.Point(142, 66);
            this.lblCstReadCount.Name = "lblCstReadCount";
            this.lblCstReadCount.Size = new System.Drawing.Size(54, 20);
            this.lblCstReadCount.TabIndex = 22;
            this.lblCstReadCount.Text = "0";
            this.lblCstReadCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCstReadCountInfo
            // 
            this.lblCstReadCountInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCstReadCountInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCstReadCountInfo.Location = new System.Drawing.Point(56, 66);
            this.lblCstReadCountInfo.Name = "lblCstReadCountInfo";
            this.lblCstReadCountInfo.Size = new System.Drawing.Size(81, 20);
            this.lblCstReadCountInfo.TabIndex = 21;
            this.lblCstReadCountInfo.Text = "read count :";
            this.lblCstReadCountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCst
            // 
            this.lblCst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCst.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCst.Location = new System.Drawing.Point(4, 67);
            this.lblCst.Name = "lblCst";
            this.lblCst.Size = new System.Drawing.Size(47, 20);
            this.lblCst.TabIndex = 20;
            this.lblCst.Text = "CST";
            this.lblCst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayTotalCount
            // 
            this.lblDayTotalCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDayTotalCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDayTotalCount.Location = new System.Drawing.Point(142, 44);
            this.lblDayTotalCount.Name = "lblDayTotalCount";
            this.lblDayTotalCount.Size = new System.Drawing.Size(54, 20);
            this.lblDayTotalCount.TabIndex = 18;
            this.lblDayTotalCount.Text = "0";
            this.lblDayTotalCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayTotalCountInfo
            // 
            this.lblDayTotalCountInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDayTotalCountInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDayTotalCountInfo.Location = new System.Drawing.Point(56, 44);
            this.lblDayTotalCountInfo.Name = "lblDayTotalCountInfo";
            this.lblDayTotalCountInfo.Size = new System.Drawing.Size(81, 20);
            this.lblDayTotalCountInfo.TabIndex = 16;
            this.lblDayTotalCountInfo.Text = "total count :";
            this.lblDayTotalCountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayReadCount
            // 
            this.lblDayReadCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDayReadCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDayReadCount.Location = new System.Drawing.Point(142, 22);
            this.lblDayReadCount.Name = "lblDayReadCount";
            this.lblDayReadCount.Size = new System.Drawing.Size(54, 20);
            this.lblDayReadCount.TabIndex = 14;
            this.lblDayReadCount.Text = "0";
            this.lblDayReadCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayReadCountInfo
            // 
            this.lblDayReadCountInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDayReadCountInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDayReadCountInfo.Location = new System.Drawing.Point(56, 22);
            this.lblDayReadCountInfo.Name = "lblDayReadCountInfo";
            this.lblDayReadCountInfo.Size = new System.Drawing.Size(81, 20);
            this.lblDayReadCountInfo.TabIndex = 12;
            this.lblDayReadCountInfo.Text = "read count :";
            this.lblDayReadCountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDay
            // 
            this.lblDay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDay.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDay.Location = new System.Drawing.Point(4, 22);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(47, 20);
            this.lblDay.TabIndex = 10;
            this.lblDay.Text = "DAY";
            this.lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCstTotalCount
            // 
            this.lbCstTotalCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCstTotalCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCstTotalCount.Location = new System.Drawing.Point(153, 44);
            this.lbCstTotalCount.Name = "lbCstTotalCount";
            this.lbCstTotalCount.Size = new System.Drawing.Size(17, 18);
            this.lbCstTotalCount.TabIndex = 19;
            this.lbCstTotalCount.Text = "0";
            this.lbCstTotalCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_cst_total_count_info
            // 
            this.lb_cst_total_count_info.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_cst_total_count_info.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_cst_total_count_info.Location = new System.Drawing.Point(56, 44);
            this.lb_cst_total_count_info.Name = "lb_cst_total_count_info";
            this.lb_cst_total_count_info.Size = new System.Drawing.Size(81, 20);
            this.lb_cst_total_count_info.TabIndex = 17;
            this.lb_cst_total_count_info.Text = "total count :";
            this.lb_cst_total_count_info.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCstReadCount
            // 
            this.lbCstReadCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCstReadCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCstReadCount.Location = new System.Drawing.Point(153, 22);
            this.lbCstReadCount.Name = "lbCstReadCount";
            this.lbCstReadCount.Size = new System.Drawing.Size(17, 18);
            this.lbCstReadCount.TabIndex = 15;
            this.lbCstReadCount.Text = "0";
            this.lbCstReadCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_cst_read_count_info
            // 
            this.lb_cst_read_count_info.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_cst_read_count_info.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_cst_read_count_info.Location = new System.Drawing.Point(56, 22);
            this.lb_cst_read_count_info.Name = "lb_cst_read_count_info";
            this.lb_cst_read_count_info.Size = new System.Drawing.Size(81, 20);
            this.lb_cst_read_count_info.TabIndex = 13;
            this.lb_cst_read_count_info.Text = "read count :";
            this.lb_cst_read_count_info.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_cst
            // 
            this.lb_cst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_cst.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_cst.Location = new System.Drawing.Point(4, 22);
            this.lb_cst.Name = "lb_cst";
            this.lb_cst.Size = new System.Drawing.Size(43, 20);
            this.lb_cst.TabIndex = 11;
            this.lb_cst.Text = "CST";
            this.lb_cst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMcrCount
            // 
            this.lblMcrCount.AutoEllipsis = true;
            this.lblMcrCount.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMcrCount.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMcrCount.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMcrCount.ForeColor = System.Drawing.Color.Black;
            this.lblMcrCount.Location = new System.Drawing.Point(0, 0);
            this.lblMcrCount.Name = "lblMcrCount";
            this.lblMcrCount.Size = new System.Drawing.Size(204, 20);
            this.lblMcrCount.TabIndex = 9;
            this.lblMcrCount.Text = "■ MCR 카운트";
            this.lblMcrCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnUnloadingOutMuting);
            this.panel4.Controls.Add(this.btnUnloadingInMuting);
            this.panel4.Controls.Add(this.lblUnloading);
            this.panel4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel4.Location = new System.Drawing.Point(83, 745);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(73, 112);
            this.panel4.TabIndex = 499;
            // 
            // btnUnloadingOutMuting
            // 
            this.btnUnloadingOutMuting.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingOutMuting.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnUnloadingOutMuting.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingOutMuting.Location = new System.Drawing.Point(5, 30);
            this.btnUnloadingOutMuting.Name = "btnUnloadingOutMuting";
            this.btnUnloadingOutMuting.Size = new System.Drawing.Size(63, 35);
            this.btnUnloadingOutMuting.TabIndex = 93;
            this.btnUnloadingOutMuting.Text = "배출 뮤팅";
            this.btnUnloadingOutMuting.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingInMuting
            // 
            this.btnUnloadingInMuting.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingInMuting.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnUnloadingInMuting.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingInMuting.Location = new System.Drawing.Point(5, 71);
            this.btnUnloadingInMuting.Name = "btnUnloadingInMuting";
            this.btnUnloadingInMuting.Size = new System.Drawing.Size(63, 35);
            this.btnUnloadingInMuting.TabIndex = 94;
            this.btnUnloadingInMuting.Text = "투입 뮤팅";
            this.btnUnloadingInMuting.UseVisualStyleBackColor = false;
            // 
            // lblUnloading
            // 
            this.lblUnloading.AutoEllipsis = true;
            this.lblUnloading.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloading.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloading.ForeColor = System.Drawing.Color.Black;
            this.lblUnloading.Location = new System.Drawing.Point(0, 0);
            this.lblUnloading.Name = "lblUnloading";
            this.lblUnloading.Size = new System.Drawing.Size(71, 20);
            this.lblUnloading.TabIndex = 9;
            this.lblUnloading.Text = "■ 언로딩";
            this.lblUnloading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.lblBreakingCount);
            this.panel7.Controls.Add(this.lblBreakingCountInfo);
            this.panel7.Controls.Add(this.btnBreakingCountClear);
            this.panel7.Controls.Add(this.lblNameBreakingCount);
            this.panel7.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel7.Location = new System.Drawing.Point(162, 632);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(152, 112);
            this.panel7.TabIndex = 497;
            // 
            // lblBreakingCount
            // 
            this.lblBreakingCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBreakingCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBreakingCount.Location = new System.Drawing.Point(110, 27);
            this.lblBreakingCount.Name = "lblBreakingCount";
            this.lblBreakingCount.Size = new System.Drawing.Size(37, 30);
            this.lblBreakingCount.TabIndex = 96;
            this.lblBreakingCount.Text = "0";
            this.lblBreakingCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBreakingCountInfo
            // 
            this.lblBreakingCountInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBreakingCountInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBreakingCountInfo.Location = new System.Drawing.Point(3, 27);
            this.lblBreakingCountInfo.Name = "lblBreakingCountInfo";
            this.lblBreakingCountInfo.Size = new System.Drawing.Size(101, 30);
            this.lblBreakingCountInfo.TabIndex = 95;
            this.lblBreakingCountInfo.Text = "Breaking Count :";
            this.lblBreakingCountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakingCountClear
            // 
            this.btnBreakingCountClear.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingCountClear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingCountClear.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingCountClear.Location = new System.Drawing.Point(3, 64);
            this.btnBreakingCountClear.Name = "btnBreakingCountClear";
            this.btnBreakingCountClear.Size = new System.Drawing.Size(144, 37);
            this.btnBreakingCountClear.TabIndex = 94;
            this.btnBreakingCountClear.Text = "Clear";
            this.btnBreakingCountClear.UseVisualStyleBackColor = false;
            // 
            // lblNameBreakingCount
            // 
            this.lblNameBreakingCount.AutoEllipsis = true;
            this.lblNameBreakingCount.BackColor = System.Drawing.Color.Gainsboro;
            this.lblNameBreakingCount.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNameBreakingCount.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblNameBreakingCount.ForeColor = System.Drawing.Color.Black;
            this.lblNameBreakingCount.Location = new System.Drawing.Point(0, 0);
            this.lblNameBreakingCount.Name = "lblNameBreakingCount";
            this.lblNameBreakingCount.Size = new System.Drawing.Size(150, 20);
            this.lblNameBreakingCount.TabIndex = 9;
            this.lblNameBreakingCount.Text = "■ 브레이킹 COUNT";
            this.lblNameBreakingCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSafetyReset
            // 
            this.btnSafetyReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnSafetyReset.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSafetyReset.ForeColor = System.Drawing.Color.Black;
            this.btnSafetyReset.Location = new System.Drawing.Point(1531, 385);
            this.btnSafetyReset.Name = "btnSafetyReset";
            this.btnSafetyReset.Size = new System.Drawing.Size(100, 127);
            this.btnSafetyReset.TabIndex = 465;
            this.btnSafetyReset.Text = "세이프티 리셋";
            this.btnSafetyReset.UseVisualStyleBackColor = false;
            this.btnSafetyReset.Click += new System.EventHandler(this.btnSafetyReset_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Control;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.btnIonizerUnloader);
            this.panel12.Controls.Add(this.btnIonizerBreak);
            this.panel12.Controls.Add(this.btnIonizerProcess);
            this.panel12.Controls.Add(this.btnIonizerLoader);
            this.panel12.Controls.Add(this.lblIonizer);
            this.panel12.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel12.Location = new System.Drawing.Point(1426, 631);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(152, 112);
            this.panel12.TabIndex = 493;
            // 
            // btnIonizerUnloader
            // 
            this.btnIonizerUnloader.BackColor = System.Drawing.SystemColors.Control;
            this.btnIonizerUnloader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIonizerUnloader.ForeColor = System.Drawing.Color.Black;
            this.btnIonizerUnloader.Location = new System.Drawing.Point(79, 65);
            this.btnIonizerUnloader.Name = "btnIonizerUnloader";
            this.btnIonizerUnloader.Size = new System.Drawing.Size(69, 40);
            this.btnIonizerUnloader.TabIndex = 97;
            this.btnIonizerUnloader.Text = "언로드";
            this.btnIonizerUnloader.UseVisualStyleBackColor = false;
            // 
            // btnIonizerBreak
            // 
            this.btnIonizerBreak.BackColor = System.Drawing.SystemColors.Control;
            this.btnIonizerBreak.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIonizerBreak.ForeColor = System.Drawing.Color.Black;
            this.btnIonizerBreak.Location = new System.Drawing.Point(4, 65);
            this.btnIonizerBreak.Name = "btnIonizerBreak";
            this.btnIonizerBreak.Size = new System.Drawing.Size(69, 40);
            this.btnIonizerBreak.TabIndex = 96;
            this.btnIonizerBreak.Text = "브레이크";
            this.btnIonizerBreak.UseVisualStyleBackColor = false;
            // 
            // btnIonizerProcess
            // 
            this.btnIonizerProcess.BackColor = System.Drawing.SystemColors.Control;
            this.btnIonizerProcess.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIonizerProcess.ForeColor = System.Drawing.Color.Black;
            this.btnIonizerProcess.Location = new System.Drawing.Point(78, 22);
            this.btnIonizerProcess.Name = "btnIonizerProcess";
            this.btnIonizerProcess.Size = new System.Drawing.Size(69, 40);
            this.btnIonizerProcess.TabIndex = 95;
            this.btnIonizerProcess.Text = "프로세스";
            this.btnIonizerProcess.UseVisualStyleBackColor = false;
            // 
            // btnIonizerLoader
            // 
            this.btnIonizerLoader.BackColor = System.Drawing.SystemColors.Control;
            this.btnIonizerLoader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIonizerLoader.ForeColor = System.Drawing.Color.Black;
            this.btnIonizerLoader.Location = new System.Drawing.Point(4, 22);
            this.btnIonizerLoader.Name = "btnIonizerLoader";
            this.btnIonizerLoader.Size = new System.Drawing.Size(69, 40);
            this.btnIonizerLoader.TabIndex = 94;
            this.btnIonizerLoader.Text = "로더";
            this.btnIonizerLoader.UseVisualStyleBackColor = false;
            // 
            // lblIonizer
            // 
            this.lblIonizer.AutoEllipsis = true;
            this.lblIonizer.BackColor = System.Drawing.Color.Gainsboro;
            this.lblIonizer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblIonizer.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblIonizer.ForeColor = System.Drawing.Color.Black;
            this.lblIonizer.Location = new System.Drawing.Point(0, 0);
            this.lblIonizer.Name = "lblIonizer";
            this.lblIonizer.Size = new System.Drawing.Size(150, 20);
            this.lblIonizer.TabIndex = 9;
            this.lblIonizer.Text = "■ 이오나이저 ON/OFF";
            this.lblIonizer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txtAlignBreak4OffsetA);
            this.panel9.Controls.Add(this.txtAlignBreak3OffsetA);
            this.panel9.Controls.Add(this.txtAlignPre2OffsetA);
            this.panel9.Controls.Add(this.lblAlignOffsetA2);
            this.panel9.Controls.Add(this.txtAlignBreak4OffsetY);
            this.panel9.Controls.Add(this.txtAlignBreak3OffsetY);
            this.panel9.Controls.Add(this.txtAlignPre2OffsetY);
            this.panel9.Controls.Add(this.lblAlignOffsetY2);
            this.panel9.Controls.Add(this.txtAlignBreak4OffsetX);
            this.panel9.Controls.Add(this.txtAlignBreak3OffsetX);
            this.panel9.Controls.Add(this.txtAlignPre2OffsetX);
            this.panel9.Controls.Add(this.lblAlignOffsetX2);
            this.panel9.Controls.Add(this.txtAlignBreak4Result);
            this.panel9.Controls.Add(this.txtAlignBreak3Result);
            this.panel9.Controls.Add(this.txtAlignPre2Result);
            this.panel9.Controls.Add(this.lblAlignResult2);
            this.panel9.Controls.Add(this.lblAlignBreak4);
            this.panel9.Controls.Add(this.lblAlignBreak3);
            this.panel9.Controls.Add(this.lblAlignPre2);
            this.panel9.Controls.Add(this.lblAlignName2);
            this.panel9.Controls.Add(this.txtAlignBreak2OffsetA);
            this.panel9.Controls.Add(this.txtAlignBreak1OffsetA);
            this.panel9.Controls.Add(this.txtAlignPre1OffsetA);
            this.panel9.Controls.Add(this.lblAlignOffsetA1);
            this.panel9.Controls.Add(this.txtAlignBreak2OffsetY);
            this.panel9.Controls.Add(this.txtAlignBreak1OffsetY);
            this.panel9.Controls.Add(this.txtAlignPre1OffsetY);
            this.panel9.Controls.Add(this.lblAlignOffsetY1);
            this.panel9.Controls.Add(this.txtAlignBreak2OffsetX);
            this.panel9.Controls.Add(this.txtAlignBreak1OffsetX);
            this.panel9.Controls.Add(this.txtAlignPre1OffsetX);
            this.panel9.Controls.Add(this.lblAlignOffsetX1);
            this.panel9.Controls.Add(this.txtAlignBreak2Result);
            this.panel9.Controls.Add(this.txtAlignBreak1Result);
            this.panel9.Controls.Add(this.txtAlignPre1Result);
            this.panel9.Controls.Add(this.lblAlignResult1);
            this.panel9.Controls.Add(this.lblAlignBreak2);
            this.panel9.Controls.Add(this.lblAlignBreak1);
            this.panel9.Controls.Add(this.lblAlignPre1);
            this.panel9.Controls.Add(this.lblAlignName1);
            this.panel9.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel9.Location = new System.Drawing.Point(952, 745);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(784, 112);
            this.panel9.TabIndex = 467;
            // 
            // txtAlignBreak4OffsetA
            // 
            this.txtAlignBreak4OffsetA.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak4OffsetA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak4OffsetA.Location = new System.Drawing.Point(706, 83);
            this.txtAlignBreak4OffsetA.Name = "txtAlignBreak4OffsetA";
            this.txtAlignBreak4OffsetA.ReadOnly = true;
            this.txtAlignBreak4OffsetA.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak4OffsetA.TabIndex = 89;
            // 
            // txtAlignBreak3OffsetA
            // 
            this.txtAlignBreak3OffsetA.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak3OffsetA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak3OffsetA.Location = new System.Drawing.Point(706, 54);
            this.txtAlignBreak3OffsetA.Name = "txtAlignBreak3OffsetA";
            this.txtAlignBreak3OffsetA.ReadOnly = true;
            this.txtAlignBreak3OffsetA.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak3OffsetA.TabIndex = 88;
            // 
            // txtAlignPre2OffsetA
            // 
            this.txtAlignPre2OffsetA.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre2OffsetA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre2OffsetA.Location = new System.Drawing.Point(706, 27);
            this.txtAlignPre2OffsetA.Name = "txtAlignPre2OffsetA";
            this.txtAlignPre2OffsetA.ReadOnly = true;
            this.txtAlignPre2OffsetA.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre2OffsetA.TabIndex = 86;
            // 
            // lblAlignOffsetA2
            // 
            this.lblAlignOffsetA2.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignOffsetA2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignOffsetA2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignOffsetA2.Location = new System.Drawing.Point(706, 4);
            this.lblAlignOffsetA2.Name = "lblAlignOffsetA2";
            this.lblAlignOffsetA2.Size = new System.Drawing.Size(73, 18);
            this.lblAlignOffsetA2.TabIndex = 87;
            this.lblAlignOffsetA2.Text = "OFFSET A";
            this.lblAlignOffsetA2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAlignBreak4OffsetY
            // 
            this.txtAlignBreak4OffsetY.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak4OffsetY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak4OffsetY.Location = new System.Drawing.Point(629, 83);
            this.txtAlignBreak4OffsetY.Name = "txtAlignBreak4OffsetY";
            this.txtAlignBreak4OffsetY.ReadOnly = true;
            this.txtAlignBreak4OffsetY.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak4OffsetY.TabIndex = 85;
            // 
            // txtAlignBreak3OffsetY
            // 
            this.txtAlignBreak3OffsetY.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak3OffsetY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak3OffsetY.Location = new System.Drawing.Point(629, 54);
            this.txtAlignBreak3OffsetY.Name = "txtAlignBreak3OffsetY";
            this.txtAlignBreak3OffsetY.ReadOnly = true;
            this.txtAlignBreak3OffsetY.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak3OffsetY.TabIndex = 84;
            // 
            // txtAlignPre2OffsetY
            // 
            this.txtAlignPre2OffsetY.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre2OffsetY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre2OffsetY.Location = new System.Drawing.Point(629, 27);
            this.txtAlignPre2OffsetY.Name = "txtAlignPre2OffsetY";
            this.txtAlignPre2OffsetY.ReadOnly = true;
            this.txtAlignPre2OffsetY.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre2OffsetY.TabIndex = 82;
            // 
            // lblAlignOffsetY2
            // 
            this.lblAlignOffsetY2.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignOffsetY2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignOffsetY2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignOffsetY2.Location = new System.Drawing.Point(629, 4);
            this.lblAlignOffsetY2.Name = "lblAlignOffsetY2";
            this.lblAlignOffsetY2.Size = new System.Drawing.Size(73, 18);
            this.lblAlignOffsetY2.TabIndex = 83;
            this.lblAlignOffsetY2.Text = "OFFSET Y";
            this.lblAlignOffsetY2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAlignBreak4OffsetX
            // 
            this.txtAlignBreak4OffsetX.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak4OffsetX.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak4OffsetX.Location = new System.Drawing.Point(550, 83);
            this.txtAlignBreak4OffsetX.Name = "txtAlignBreak4OffsetX";
            this.txtAlignBreak4OffsetX.ReadOnly = true;
            this.txtAlignBreak4OffsetX.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak4OffsetX.TabIndex = 81;
            // 
            // txtAlignBreak3OffsetX
            // 
            this.txtAlignBreak3OffsetX.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak3OffsetX.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak3OffsetX.Location = new System.Drawing.Point(550, 54);
            this.txtAlignBreak3OffsetX.Name = "txtAlignBreak3OffsetX";
            this.txtAlignBreak3OffsetX.ReadOnly = true;
            this.txtAlignBreak3OffsetX.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak3OffsetX.TabIndex = 80;
            // 
            // txtAlignPre2OffsetX
            // 
            this.txtAlignPre2OffsetX.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre2OffsetX.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre2OffsetX.Location = new System.Drawing.Point(550, 27);
            this.txtAlignPre2OffsetX.Name = "txtAlignPre2OffsetX";
            this.txtAlignPre2OffsetX.ReadOnly = true;
            this.txtAlignPre2OffsetX.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre2OffsetX.TabIndex = 78;
            // 
            // lblAlignOffsetX2
            // 
            this.lblAlignOffsetX2.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignOffsetX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignOffsetX2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignOffsetX2.Location = new System.Drawing.Point(550, 4);
            this.lblAlignOffsetX2.Name = "lblAlignOffsetX2";
            this.lblAlignOffsetX2.Size = new System.Drawing.Size(73, 18);
            this.lblAlignOffsetX2.TabIndex = 79;
            this.lblAlignOffsetX2.Text = "OFFSET X";
            this.lblAlignOffsetX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAlignBreak4Result
            // 
            this.txtAlignBreak4Result.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak4Result.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak4Result.Location = new System.Drawing.Point(471, 83);
            this.txtAlignBreak4Result.Name = "txtAlignBreak4Result";
            this.txtAlignBreak4Result.ReadOnly = true;
            this.txtAlignBreak4Result.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak4Result.TabIndex = 77;
            // 
            // txtAlignBreak3Result
            // 
            this.txtAlignBreak3Result.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak3Result.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak3Result.Location = new System.Drawing.Point(471, 54);
            this.txtAlignBreak3Result.Name = "txtAlignBreak3Result";
            this.txtAlignBreak3Result.ReadOnly = true;
            this.txtAlignBreak3Result.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak3Result.TabIndex = 76;
            // 
            // txtAlignPre2Result
            // 
            this.txtAlignPre2Result.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre2Result.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre2Result.Location = new System.Drawing.Point(471, 27);
            this.txtAlignPre2Result.Name = "txtAlignPre2Result";
            this.txtAlignPre2Result.ReadOnly = true;
            this.txtAlignPre2Result.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre2Result.TabIndex = 71;
            // 
            // lblAlignResult2
            // 
            this.lblAlignResult2.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignResult2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignResult2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignResult2.Location = new System.Drawing.Point(471, 4);
            this.lblAlignResult2.Name = "lblAlignResult2";
            this.lblAlignResult2.Size = new System.Drawing.Size(73, 18);
            this.lblAlignResult2.TabIndex = 75;
            this.lblAlignResult2.Text = "RESULT";
            this.lblAlignResult2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignBreak4
            // 
            this.lblAlignBreak4.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignBreak4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignBreak4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignBreak4.Location = new System.Drawing.Point(397, 87);
            this.lblAlignBreak4.Name = "lblAlignBreak4";
            this.lblAlignBreak4.Size = new System.Drawing.Size(70, 17);
            this.lblAlignBreak4.TabIndex = 74;
            this.lblAlignBreak4.Text = "BREAK 4";
            this.lblAlignBreak4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignBreak3
            // 
            this.lblAlignBreak3.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignBreak3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignBreak3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignBreak3.Location = new System.Drawing.Point(397, 58);
            this.lblAlignBreak3.Name = "lblAlignBreak3";
            this.lblAlignBreak3.Size = new System.Drawing.Size(70, 17);
            this.lblAlignBreak3.TabIndex = 73;
            this.lblAlignBreak3.Text = "BREAK 3";
            this.lblAlignBreak3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignPre2
            // 
            this.lblAlignPre2.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignPre2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignPre2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignPre2.Location = new System.Drawing.Point(397, 30);
            this.lblAlignPre2.Name = "lblAlignPre2";
            this.lblAlignPre2.Size = new System.Drawing.Size(70, 17);
            this.lblAlignPre2.TabIndex = 72;
            this.lblAlignPre2.Text = "PRE 2";
            this.lblAlignPre2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignName2
            // 
            this.lblAlignName2.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignName2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignName2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignName2.Location = new System.Drawing.Point(397, 4);
            this.lblAlignName2.Name = "lblAlignName2";
            this.lblAlignName2.Size = new System.Drawing.Size(70, 17);
            this.lblAlignName2.TabIndex = 70;
            this.lblAlignName2.Text = "NAME";
            this.lblAlignName2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAlignBreak2OffsetA
            // 
            this.txtAlignBreak2OffsetA.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak2OffsetA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak2OffsetA.Location = new System.Drawing.Point(316, 83);
            this.txtAlignBreak2OffsetA.Name = "txtAlignBreak2OffsetA";
            this.txtAlignBreak2OffsetA.ReadOnly = true;
            this.txtAlignBreak2OffsetA.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak2OffsetA.TabIndex = 69;
            // 
            // txtAlignBreak1OffsetA
            // 
            this.txtAlignBreak1OffsetA.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak1OffsetA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak1OffsetA.Location = new System.Drawing.Point(316, 54);
            this.txtAlignBreak1OffsetA.Name = "txtAlignBreak1OffsetA";
            this.txtAlignBreak1OffsetA.ReadOnly = true;
            this.txtAlignBreak1OffsetA.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak1OffsetA.TabIndex = 68;
            // 
            // txtAlignPre1OffsetA
            // 
            this.txtAlignPre1OffsetA.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre1OffsetA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre1OffsetA.Location = new System.Drawing.Point(316, 27);
            this.txtAlignPre1OffsetA.Name = "txtAlignPre1OffsetA";
            this.txtAlignPre1OffsetA.ReadOnly = true;
            this.txtAlignPre1OffsetA.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre1OffsetA.TabIndex = 66;
            // 
            // lblAlignOffsetA1
            // 
            this.lblAlignOffsetA1.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignOffsetA1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignOffsetA1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignOffsetA1.Location = new System.Drawing.Point(317, 4);
            this.lblAlignOffsetA1.Name = "lblAlignOffsetA1";
            this.lblAlignOffsetA1.Size = new System.Drawing.Size(73, 18);
            this.lblAlignOffsetA1.TabIndex = 67;
            this.lblAlignOffsetA1.Text = "OFFSET A";
            this.lblAlignOffsetA1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAlignBreak2OffsetY
            // 
            this.txtAlignBreak2OffsetY.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak2OffsetY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak2OffsetY.Location = new System.Drawing.Point(239, 83);
            this.txtAlignBreak2OffsetY.Name = "txtAlignBreak2OffsetY";
            this.txtAlignBreak2OffsetY.ReadOnly = true;
            this.txtAlignBreak2OffsetY.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak2OffsetY.TabIndex = 65;
            // 
            // txtAlignBreak1OffsetY
            // 
            this.txtAlignBreak1OffsetY.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak1OffsetY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak1OffsetY.Location = new System.Drawing.Point(239, 54);
            this.txtAlignBreak1OffsetY.Name = "txtAlignBreak1OffsetY";
            this.txtAlignBreak1OffsetY.ReadOnly = true;
            this.txtAlignBreak1OffsetY.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak1OffsetY.TabIndex = 64;
            // 
            // txtAlignPre1OffsetY
            // 
            this.txtAlignPre1OffsetY.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre1OffsetY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre1OffsetY.Location = new System.Drawing.Point(239, 27);
            this.txtAlignPre1OffsetY.Name = "txtAlignPre1OffsetY";
            this.txtAlignPre1OffsetY.ReadOnly = true;
            this.txtAlignPre1OffsetY.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre1OffsetY.TabIndex = 62;
            // 
            // lblAlignOffsetY1
            // 
            this.lblAlignOffsetY1.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignOffsetY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignOffsetY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignOffsetY1.Location = new System.Drawing.Point(239, 4);
            this.lblAlignOffsetY1.Name = "lblAlignOffsetY1";
            this.lblAlignOffsetY1.Size = new System.Drawing.Size(73, 18);
            this.lblAlignOffsetY1.TabIndex = 63;
            this.lblAlignOffsetY1.Text = "OFFSET Y";
            this.lblAlignOffsetY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAlignBreak2OffsetX
            // 
            this.txtAlignBreak2OffsetX.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak2OffsetX.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak2OffsetX.Location = new System.Drawing.Point(160, 83);
            this.txtAlignBreak2OffsetX.Name = "txtAlignBreak2OffsetX";
            this.txtAlignBreak2OffsetX.ReadOnly = true;
            this.txtAlignBreak2OffsetX.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak2OffsetX.TabIndex = 61;
            // 
            // txtAlignBreak1OffsetX
            // 
            this.txtAlignBreak1OffsetX.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak1OffsetX.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak1OffsetX.Location = new System.Drawing.Point(160, 54);
            this.txtAlignBreak1OffsetX.Name = "txtAlignBreak1OffsetX";
            this.txtAlignBreak1OffsetX.ReadOnly = true;
            this.txtAlignBreak1OffsetX.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak1OffsetX.TabIndex = 60;
            // 
            // txtAlignPre1OffsetX
            // 
            this.txtAlignPre1OffsetX.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre1OffsetX.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre1OffsetX.Location = new System.Drawing.Point(160, 27);
            this.txtAlignPre1OffsetX.Name = "txtAlignPre1OffsetX";
            this.txtAlignPre1OffsetX.ReadOnly = true;
            this.txtAlignPre1OffsetX.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre1OffsetX.TabIndex = 58;
            // 
            // lblAlignOffsetX1
            // 
            this.lblAlignOffsetX1.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignOffsetX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignOffsetX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignOffsetX1.Location = new System.Drawing.Point(159, 4);
            this.lblAlignOffsetX1.Name = "lblAlignOffsetX1";
            this.lblAlignOffsetX1.Size = new System.Drawing.Size(73, 18);
            this.lblAlignOffsetX1.TabIndex = 59;
            this.lblAlignOffsetX1.Text = "OFFSET X";
            this.lblAlignOffsetX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAlignBreak2Result
            // 
            this.txtAlignBreak2Result.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak2Result.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak2Result.Location = new System.Drawing.Point(81, 83);
            this.txtAlignBreak2Result.Name = "txtAlignBreak2Result";
            this.txtAlignBreak2Result.ReadOnly = true;
            this.txtAlignBreak2Result.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak2Result.TabIndex = 57;
            // 
            // txtAlignBreak1Result
            // 
            this.txtAlignBreak1Result.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignBreak1Result.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignBreak1Result.Location = new System.Drawing.Point(81, 54);
            this.txtAlignBreak1Result.Name = "txtAlignBreak1Result";
            this.txtAlignBreak1Result.ReadOnly = true;
            this.txtAlignBreak1Result.Size = new System.Drawing.Size(73, 23);
            this.txtAlignBreak1Result.TabIndex = 56;
            // 
            // txtAlignPre1Result
            // 
            this.txtAlignPre1Result.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlignPre1Result.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAlignPre1Result.Location = new System.Drawing.Point(81, 27);
            this.txtAlignPre1Result.Name = "txtAlignPre1Result";
            this.txtAlignPre1Result.ReadOnly = true;
            this.txtAlignPre1Result.Size = new System.Drawing.Size(73, 23);
            this.txtAlignPre1Result.TabIndex = 51;
            // 
            // lblAlignResult1
            // 
            this.lblAlignResult1.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignResult1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignResult1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignResult1.Location = new System.Drawing.Point(81, 4);
            this.lblAlignResult1.Name = "lblAlignResult1";
            this.lblAlignResult1.Size = new System.Drawing.Size(73, 18);
            this.lblAlignResult1.TabIndex = 55;
            this.lblAlignResult1.Text = "RESULT";
            this.lblAlignResult1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignBreak2
            // 
            this.lblAlignBreak2.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignBreak2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignBreak2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignBreak2.Location = new System.Drawing.Point(5, 85);
            this.lblAlignBreak2.Name = "lblAlignBreak2";
            this.lblAlignBreak2.Size = new System.Drawing.Size(70, 17);
            this.lblAlignBreak2.TabIndex = 54;
            this.lblAlignBreak2.Text = "BREAK 2";
            this.lblAlignBreak2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignBreak1
            // 
            this.lblAlignBreak1.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignBreak1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignBreak1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignBreak1.Location = new System.Drawing.Point(5, 56);
            this.lblAlignBreak1.Name = "lblAlignBreak1";
            this.lblAlignBreak1.Size = new System.Drawing.Size(70, 17);
            this.lblAlignBreak1.TabIndex = 53;
            this.lblAlignBreak1.Text = "BREAK 1";
            this.lblAlignBreak1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignPre1
            // 
            this.lblAlignPre1.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignPre1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignPre1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignPre1.Location = new System.Drawing.Point(5, 28);
            this.lblAlignPre1.Name = "lblAlignPre1";
            this.lblAlignPre1.Size = new System.Drawing.Size(70, 17);
            this.lblAlignPre1.TabIndex = 52;
            this.lblAlignPre1.Text = "PRE 1";
            this.lblAlignPre1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlignName1
            // 
            this.lblAlignName1.BackColor = System.Drawing.SystemColors.Control;
            this.lblAlignName1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlignName1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlignName1.Location = new System.Drawing.Point(4, 4);
            this.lblAlignName1.Name = "lblAlignName1";
            this.lblAlignName1.Size = new System.Drawing.Size(70, 17);
            this.lblAlignName1.TabIndex = 50;
            this.lblAlignName1.Text = " NAME";
            this.lblAlignName1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.Control;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.button2);
            this.panel10.Controls.Add(this.btnLaserCover);
            this.panel10.Controls.Add(this.lblLoaderBox);
            this.panel10.Controls.Add(this.lblEms3);
            this.panel10.Controls.Add(this.lblProcessBox);
            this.panel10.Controls.Add(this.lblEms5);
            this.panel10.Controls.Add(this.lblUnloaderBox);
            this.panel10.Controls.Add(this.btnACSTSkip);
            this.panel10.Controls.Add(this.btnBCSTSkip);
            this.panel10.Controls.Add(this.lblEms4);
            this.panel10.Controls.Add(this.btnDelayTopDoor11);
            this.panel10.Controls.Add(this.lblZPosGap);
            this.panel10.Controls.Add(this.btnDelayTopDoor1);
            this.panel10.Controls.Add(this.lblAvgPower);
            this.panel10.Controls.Add(this.lblEms1);
            this.panel10.Controls.Add(this.lblAvgPowerInfo);
            this.panel10.Controls.Add(this.label7);
            this.panel10.Controls.Add(this.label3);
            this.panel10.Controls.Add(this.label6);
            this.panel10.Controls.Add(this.label2);
            this.panel10.Controls.Add(this.label5);
            this.panel10.Controls.Add(this.label1);
            this.panel10.Controls.Add(this.label4);
            this.panel10.Controls.Add(this.lblLdMut);
            this.panel10.Controls.Add(this.lblTartget);
            this.panel10.Controls.Add(this.lblZPosGapInfo);
            this.panel10.Controls.Add(this.lblTargetInfo);
            this.panel10.Controls.Add(this.btnUldLight);
            this.panel10.Controls.Add(this.btnProcessLight);
            this.panel10.Controls.Add(this.btnDelayTopDoor10);
            this.panel10.Controls.Add(this.btnLdLight);
            this.panel10.Controls.Add(this.btnDelayTopDoor9);
            this.panel10.Controls.Add(this.btnDelayTopDoor8);
            this.panel10.Controls.Add(this.btnDelayTopDoor7);
            this.panel10.Controls.Add(this.btnDelayTopDoor5);
            this.panel10.Controls.Add(this.btnDelayTopDoor6);
            this.panel10.Controls.Add(this.btnDelayTopDoor4);
            this.panel10.Controls.Add(this.lblAutoTeach);
            this.panel10.Controls.Add(this.lblShutterOpen);
            this.panel10.Controls.Add(this.lblLock);
            this.panel10.Controls.Add(this.lblEms2);
            this.panel10.Controls.Add(this.lblGrabEms3);
            this.panel10.Controls.Add(this.lblGrabEms2);
            this.panel10.Controls.Add(this.lblGrabEms1);
            this.panel10.Controls.Add(this.lblEms6);
            this.panel10.Controls.Add(this.btnDelayTopDoor2);
            this.panel10.Controls.Add(this.btnDelayTopDoor3);
            this.panel10.Controls.Add(this.elementHost1);
            this.panel10.Controls.Add(this.lbleEquipState);
            this.panel10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel10.Location = new System.Drawing.Point(462, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1065, 509);
            this.panel10.TabIndex = 468;
            this.panel10.Paint += new System.Windows.Forms.PaintEventHandler(this.panel10_Paint);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(638, 88);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 25);
            this.button2.TabIndex = 494;
            this.button2.Text = "레이저 커버";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // btnLaserCover
            // 
            this.btnLaserCover.BackColor = System.Drawing.SystemColors.Control;
            this.btnLaserCover.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLaserCover.ForeColor = System.Drawing.Color.Black;
            this.btnLaserCover.Location = new System.Drawing.Point(705, 88);
            this.btnLaserCover.Name = "btnLaserCover";
            this.btnLaserCover.Size = new System.Drawing.Size(85, 25);
            this.btnLaserCover.TabIndex = 494;
            this.btnLaserCover.Text = "레이저 커버";
            this.btnLaserCover.UseVisualStyleBackColor = false;
            // 
            // lblLoaderBox
            // 
            this.lblLoaderBox.BackColor = System.Drawing.SystemColors.Control;
            this.lblLoaderBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoaderBox.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoaderBox.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderBox.Location = new System.Drawing.Point(730, 32);
            this.lblLoaderBox.Name = "lblLoaderBox";
            this.lblLoaderBox.Size = new System.Drawing.Size(88, 25);
            this.lblLoaderBox.TabIndex = 464;
            this.lblLoaderBox.Text = "로더 전장박스";
            this.lblLoaderBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEms3
            // 
            this.lblEms3.BackColor = System.Drawing.SystemColors.Control;
            this.lblEms3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEms3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEms3.ForeColor = System.Drawing.Color.Black;
            this.lblEms3.Location = new System.Drawing.Point(677, 32);
            this.lblEms3.Name = "lblEms3";
            this.lblEms3.Size = new System.Drawing.Size(47, 25);
            this.lblEms3.TabIndex = 465;
            this.lblEms3.Text = "EMS 3";
            this.lblEms3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessBox
            // 
            this.lblProcessBox.BackColor = System.Drawing.SystemColors.Control;
            this.lblProcessBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessBox.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProcessBox.ForeColor = System.Drawing.Color.Black;
            this.lblProcessBox.Location = new System.Drawing.Point(495, 32);
            this.lblProcessBox.Name = "lblProcessBox";
            this.lblProcessBox.Size = new System.Drawing.Size(114, 25);
            this.lblProcessBox.TabIndex = 466;
            this.lblProcessBox.Text = "프로세스 전장박스";
            this.lblProcessBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEms5
            // 
            this.lblEms5.BackColor = System.Drawing.SystemColors.Control;
            this.lblEms5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEms5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEms5.ForeColor = System.Drawing.Color.Black;
            this.lblEms5.Location = new System.Drawing.Point(347, 32);
            this.lblEms5.Name = "lblEms5";
            this.lblEms5.Size = new System.Drawing.Size(47, 25);
            this.lblEms5.TabIndex = 467;
            this.lblEms5.Text = "EMS 5";
            this.lblEms5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloaderBox
            // 
            this.lblUnloaderBox.BackColor = System.Drawing.SystemColors.Control;
            this.lblUnloaderBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloaderBox.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloaderBox.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderBox.Location = new System.Drawing.Point(240, 32);
            this.lblUnloaderBox.Name = "lblUnloaderBox";
            this.lblUnloaderBox.Size = new System.Drawing.Size(101, 25);
            this.lblUnloaderBox.TabIndex = 468;
            this.lblUnloaderBox.Text = "언로더 전장박스";
            this.lblUnloaderBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnACSTSkip
            // 
            this.btnACSTSkip.BackColor = System.Drawing.SystemColors.Control;
            this.btnACSTSkip.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnACSTSkip.ForeColor = System.Drawing.Color.Black;
            this.btnACSTSkip.Location = new System.Drawing.Point(862, 60);
            this.btnACSTSkip.Name = "btnACSTSkip";
            this.btnACSTSkip.Size = new System.Drawing.Size(75, 25);
            this.btnACSTSkip.TabIndex = 484;
            this.btnACSTSkip.Text = "A-CSTSkip";
            this.btnACSTSkip.UseVisualStyleBackColor = false;
            // 
            // btnBCSTSkip
            // 
            this.btnBCSTSkip.BackColor = System.Drawing.SystemColors.Control;
            this.btnBCSTSkip.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBCSTSkip.ForeColor = System.Drawing.Color.Black;
            this.btnBCSTSkip.Location = new System.Drawing.Point(943, 60);
            this.btnBCSTSkip.Name = "btnBCSTSkip";
            this.btnBCSTSkip.Size = new System.Drawing.Size(75, 25);
            this.btnBCSTSkip.TabIndex = 483;
            this.btnBCSTSkip.Text = "B-CSTSkip";
            this.btnBCSTSkip.UseVisualStyleBackColor = false;
            // 
            // lblEms4
            // 
            this.lblEms4.BackColor = System.Drawing.SystemColors.Control;
            this.lblEms4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEms4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEms4.ForeColor = System.Drawing.Color.Black;
            this.lblEms4.Location = new System.Drawing.Point(304, 481);
            this.lblEms4.Name = "lblEms4";
            this.lblEms4.Size = new System.Drawing.Size(64, 22);
            this.lblEms4.TabIndex = 471;
            this.lblEms4.Text = "EMS 4";
            this.lblEms4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDelayTopDoor11
            // 
            this.btnDelayTopDoor11.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor11.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor11.Location = new System.Drawing.Point(15, 106);
            this.btnDelayTopDoor11.Name = "btnDelayTopDoor11";
            this.btnDelayTopDoor11.Size = new System.Drawing.Size(31, 66);
            this.btnDelayTopDoor11.TabIndex = 493;
            this.btnDelayTopDoor11.Text = "도\r\n어\r\n11";
            this.btnDelayTopDoor11.UseVisualStyleBackColor = false;
            // 
            // lblZPosGap
            // 
            this.lblZPosGap.BackColor = System.Drawing.SystemColors.Control;
            this.lblZPosGap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZPosGap.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblZPosGap.ForeColor = System.Drawing.Color.Black;
            this.lblZPosGap.Location = new System.Drawing.Point(805, 436);
            this.lblZPosGap.Name = "lblZPosGap";
            this.lblZPosGap.Size = new System.Drawing.Size(50, 33);
            this.lblZPosGap.TabIndex = 476;
            this.lblZPosGap.Text = "0";
            this.lblZPosGap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDelayTopDoor1
            // 
            this.btnDelayTopDoor1.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor1.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor1.Location = new System.Drawing.Point(1029, 120);
            this.btnDelayTopDoor1.Name = "btnDelayTopDoor1";
            this.btnDelayTopDoor1.Size = new System.Drawing.Size(31, 66);
            this.btnDelayTopDoor1.TabIndex = 492;
            this.btnDelayTopDoor1.Text = "도어 1";
            this.btnDelayTopDoor1.UseVisualStyleBackColor = false;
            // 
            // lblAvgPower
            // 
            this.lblAvgPower.BackColor = System.Drawing.SystemColors.Control;
            this.lblAvgPower.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAvgPower.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAvgPower.ForeColor = System.Drawing.Color.Black;
            this.lblAvgPower.Location = new System.Drawing.Point(967, 454);
            this.lblAvgPower.Name = "lblAvgPower";
            this.lblAvgPower.Size = new System.Drawing.Size(50, 15);
            this.lblAvgPower.TabIndex = 479;
            this.lblAvgPower.Text = "0";
            this.lblAvgPower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEms1
            // 
            this.lblEms1.BackColor = System.Drawing.SystemColors.Control;
            this.lblEms1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEms1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEms1.ForeColor = System.Drawing.Color.Black;
            this.lblEms1.Location = new System.Drawing.Point(690, 482);
            this.lblEms1.Name = "lblEms1";
            this.lblEms1.Size = new System.Drawing.Size(64, 22);
            this.lblEms1.TabIndex = 475;
            this.lblEms1.Text = "EMS 1";
            this.lblEms1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAvgPowerInfo
            // 
            this.lblAvgPowerInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblAvgPowerInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAvgPowerInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAvgPowerInfo.ForeColor = System.Drawing.Color.Black;
            this.lblAvgPowerInfo.Location = new System.Drawing.Point(861, 454);
            this.lblAvgPowerInfo.Name = "lblAvgPowerInfo";
            this.lblAvgPowerInfo.Size = new System.Drawing.Size(100, 15);
            this.lblAvgPowerInfo.TabIndex = 478;
            this.lblAvgPowerInfo.Text = "AvgPower[w]:";
            this.lblAvgPowerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(52, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 22);
            this.label7.TabIndex = 477;
            this.label7.Text = "감지";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(974, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 22);
            this.label3.TabIndex = 477;
            this.label3.Text = "감지";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(52, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 22);
            this.label6.TabIndex = 477;
            this.label6.Text = "뮤팅";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(974, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 22);
            this.label2.TabIndex = 477;
            this.label2.Text = "뮤팅";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(52, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 22);
            this.label5.TabIndex = 477;
            this.label5.Text = "감지";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(974, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 22);
            this.label1.TabIndex = 477;
            this.label1.Text = "감지";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(52, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 22);
            this.label4.TabIndex = 477;
            this.label4.Text = "뮤팅";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdMut
            // 
            this.lblLdMut.BackColor = System.Drawing.SystemColors.Control;
            this.lblLdMut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdMut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdMut.ForeColor = System.Drawing.Color.Black;
            this.lblLdMut.Location = new System.Drawing.Point(974, 135);
            this.lblLdMut.Name = "lblLdMut";
            this.lblLdMut.Size = new System.Drawing.Size(49, 22);
            this.lblLdMut.TabIndex = 477;
            this.lblLdMut.Text = "뮤팅";
            this.lblLdMut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTartget
            // 
            this.lblTartget.BackColor = System.Drawing.SystemColors.Control;
            this.lblTartget.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTartget.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTartget.ForeColor = System.Drawing.Color.Black;
            this.lblTartget.Location = new System.Drawing.Point(967, 436);
            this.lblTartget.Name = "lblTartget";
            this.lblTartget.Size = new System.Drawing.Size(50, 15);
            this.lblTartget.TabIndex = 477;
            this.lblTartget.Text = "0";
            this.lblTartget.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblZPosGapInfo
            // 
            this.lblZPosGapInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblZPosGapInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZPosGapInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblZPosGapInfo.ForeColor = System.Drawing.Color.Black;
            this.lblZPosGapInfo.Location = new System.Drawing.Point(719, 436);
            this.lblZPosGapInfo.Name = "lblZPosGapInfo";
            this.lblZPosGapInfo.Size = new System.Drawing.Size(80, 33);
            this.lblZPosGapInfo.TabIndex = 470;
            this.lblZPosGapInfo.Text = "ZPos Gap :";
            this.lblZPosGapInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTargetInfo
            // 
            this.lblTargetInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblTargetInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTargetInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTargetInfo.ForeColor = System.Drawing.Color.Black;
            this.lblTargetInfo.Location = new System.Drawing.Point(861, 436);
            this.lblTargetInfo.Name = "lblTargetInfo";
            this.lblTargetInfo.Size = new System.Drawing.Size(100, 15);
            this.lblTargetInfo.TabIndex = 469;
            this.lblTargetInfo.Text = "Target[w]:";
            this.lblTargetInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUldLight
            // 
            this.btnUldLight.BackColor = System.Drawing.SystemColors.Control;
            this.btnUldLight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUldLight.ForeColor = System.Drawing.Color.Black;
            this.btnUldLight.Location = new System.Drawing.Point(164, 398);
            this.btnUldLight.Name = "btnUldLight";
            this.btnUldLight.Size = new System.Drawing.Size(125, 25);
            this.btnUldLight.TabIndex = 482;
            this.btnUldLight.Text = "언로더부 조명";
            this.btnUldLight.UseVisualStyleBackColor = false;
            // 
            // btnProcessLight
            // 
            this.btnProcessLight.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessLight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessLight.ForeColor = System.Drawing.Color.Black;
            this.btnProcessLight.Location = new System.Drawing.Point(451, 398);
            this.btnProcessLight.Name = "btnProcessLight";
            this.btnProcessLight.Size = new System.Drawing.Size(125, 25);
            this.btnProcessLight.TabIndex = 481;
            this.btnProcessLight.Text = "가공부 조명";
            this.btnProcessLight.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor10
            // 
            this.btnDelayTopDoor10.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor10.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor10.Location = new System.Drawing.Point(97, 483);
            this.btnDelayTopDoor10.Name = "btnDelayTopDoor10";
            this.btnDelayTopDoor10.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor10.TabIndex = 491;
            this.btnDelayTopDoor10.Text = "도어 10";
            this.btnDelayTopDoor10.UseVisualStyleBackColor = false;
            // 
            // btnLdLight
            // 
            this.btnLdLight.BackColor = System.Drawing.SystemColors.Control;
            this.btnLdLight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLdLight.ForeColor = System.Drawing.Color.Black;
            this.btnLdLight.Location = new System.Drawing.Point(701, 398);
            this.btnLdLight.Name = "btnLdLight";
            this.btnLdLight.Size = new System.Drawing.Size(125, 25);
            this.btnLdLight.TabIndex = 480;
            this.btnLdLight.Text = "로더부 조명";
            this.btnLdLight.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor9
            // 
            this.btnDelayTopDoor9.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor9.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor9.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor9.Location = new System.Drawing.Point(164, 483);
            this.btnDelayTopDoor9.Name = "btnDelayTopDoor9";
            this.btnDelayTopDoor9.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor9.TabIndex = 491;
            this.btnDelayTopDoor9.Text = "도어 9";
            this.btnDelayTopDoor9.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor8
            // 
            this.btnDelayTopDoor8.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor8.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor8.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor8.Location = new System.Drawing.Point(547, 116);
            this.btnDelayTopDoor8.Name = "btnDelayTopDoor8";
            this.btnDelayTopDoor8.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor8.TabIndex = 490;
            this.btnDelayTopDoor8.Text = "도어 8";
            this.btnDelayTopDoor8.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor7
            // 
            this.btnDelayTopDoor7.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor7.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor7.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor7.Location = new System.Drawing.Point(547, 88);
            this.btnDelayTopDoor7.Name = "btnDelayTopDoor7";
            this.btnDelayTopDoor7.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor7.TabIndex = 489;
            this.btnDelayTopDoor7.Text = "도어 7";
            this.btnDelayTopDoor7.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor5
            // 
            this.btnDelayTopDoor5.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor5.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor5.Location = new System.Drawing.Point(556, 481);
            this.btnDelayTopDoor5.Name = "btnDelayTopDoor5";
            this.btnDelayTopDoor5.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor5.TabIndex = 487;
            this.btnDelayTopDoor5.Text = "도어 5";
            this.btnDelayTopDoor5.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor6
            // 
            this.btnDelayTopDoor6.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor6.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor6.Location = new System.Drawing.Point(486, 481);
            this.btnDelayTopDoor6.Name = "btnDelayTopDoor6";
            this.btnDelayTopDoor6.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor6.TabIndex = 488;
            this.btnDelayTopDoor6.Text = "도어 6";
            this.btnDelayTopDoor6.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor4
            // 
            this.btnDelayTopDoor4.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor4.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor4.Location = new System.Drawing.Point(626, 481);
            this.btnDelayTopDoor4.Name = "btnDelayTopDoor4";
            this.btnDelayTopDoor4.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor4.TabIndex = 486;
            this.btnDelayTopDoor4.Text = "도어 4";
            this.btnDelayTopDoor4.UseVisualStyleBackColor = false;
            // 
            // lblAutoTeach
            // 
            this.lblAutoTeach.BackColor = System.Drawing.SystemColors.Control;
            this.lblAutoTeach.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAutoTeach.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAutoTeach.ForeColor = System.Drawing.Color.Black;
            this.lblAutoTeach.Location = new System.Drawing.Point(974, 87);
            this.lblAutoTeach.Name = "lblAutoTeach";
            this.lblAutoTeach.Size = new System.Drawing.Size(85, 25);
            this.lblAutoTeach.TabIndex = 463;
            this.lblAutoTeach.Text = "Shutter Open";
            this.lblAutoTeach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShutterOpen
            // 
            this.lblShutterOpen.BackColor = System.Drawing.SystemColors.Control;
            this.lblShutterOpen.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShutterOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblShutterOpen.ForeColor = System.Drawing.Color.Black;
            this.lblShutterOpen.Location = new System.Drawing.Point(886, 88);
            this.lblShutterOpen.Name = "lblShutterOpen";
            this.lblShutterOpen.Size = new System.Drawing.Size(85, 25);
            this.lblShutterOpen.TabIndex = 463;
            this.lblShutterOpen.Text = "Shutter Open";
            this.lblShutterOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLock
            // 
            this.lblLock.BackColor = System.Drawing.SystemColors.Control;
            this.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLock.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLock.ForeColor = System.Drawing.Color.Black;
            this.lblLock.Location = new System.Drawing.Point(796, 88);
            this.lblLock.Name = "lblLock";
            this.lblLock.Size = new System.Drawing.Size(85, 25);
            this.lblLock.TabIndex = 474;
            this.lblLock.Text = "Lock";
            this.lblLock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEms2
            // 
            this.lblEms2.BackColor = System.Drawing.SystemColors.Control;
            this.lblEms2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEms2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEms2.ForeColor = System.Drawing.Color.Black;
            this.lblEms2.Location = new System.Drawing.Point(968, 32);
            this.lblEms2.Name = "lblEms2";
            this.lblEms2.Size = new System.Drawing.Size(47, 25);
            this.lblEms2.TabIndex = 472;
            this.lblEms2.Text = "EMS 2";
            this.lblEms2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGrabEms3
            // 
            this.lblGrabEms3.BackColor = System.Drawing.SystemColors.Control;
            this.lblGrabEms3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGrabEms3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblGrabEms3.ForeColor = System.Drawing.Color.Black;
            this.lblGrabEms3.Location = new System.Drawing.Point(96, 32);
            this.lblGrabEms3.Name = "lblGrabEms3";
            this.lblGrabEms3.Size = new System.Drawing.Size(70, 25);
            this.lblGrabEms3.TabIndex = 473;
            this.lblGrabEms3.Text = "G EMS 3";
            this.lblGrabEms3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGrabEms2
            // 
            this.lblGrabEms2.BackColor = System.Drawing.SystemColors.Control;
            this.lblGrabEms2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGrabEms2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblGrabEms2.ForeColor = System.Drawing.Color.Black;
            this.lblGrabEms2.Location = new System.Drawing.Point(373, 481);
            this.lblGrabEms2.Name = "lblGrabEms2";
            this.lblGrabEms2.Size = new System.Drawing.Size(70, 22);
            this.lblGrabEms2.TabIndex = 473;
            this.lblGrabEms2.Text = "G EMS 2";
            this.lblGrabEms2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGrabEms1
            // 
            this.lblGrabEms1.BackColor = System.Drawing.SystemColors.Control;
            this.lblGrabEms1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGrabEms1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblGrabEms1.ForeColor = System.Drawing.Color.Black;
            this.lblGrabEms1.Location = new System.Drawing.Point(895, 32);
            this.lblGrabEms1.Name = "lblGrabEms1";
            this.lblGrabEms1.Size = new System.Drawing.Size(70, 25);
            this.lblGrabEms1.TabIndex = 473;
            this.lblGrabEms1.Text = "G EMS 1";
            this.lblGrabEms1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEms6
            // 
            this.lblEms6.BackColor = System.Drawing.SystemColors.Control;
            this.lblEms6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEms6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEms6.ForeColor = System.Drawing.Color.Black;
            this.lblEms6.Location = new System.Drawing.Point(44, 32);
            this.lblEms6.Name = "lblEms6";
            this.lblEms6.Size = new System.Drawing.Size(47, 25);
            this.lblEms6.TabIndex = 473;
            this.lblEms6.Text = "EMS 6";
            this.lblEms6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDelayTopDoor2
            // 
            this.btnDelayTopDoor2.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor2.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor2.Location = new System.Drawing.Point(913, 483);
            this.btnDelayTopDoor2.Name = "btnDelayTopDoor2";
            this.btnDelayTopDoor2.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor2.TabIndex = 485;
            this.btnDelayTopDoor2.Text = "도어 2";
            this.btnDelayTopDoor2.UseVisualStyleBackColor = false;
            // 
            // btnDelayTopDoor3
            // 
            this.btnDelayTopDoor3.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelayTopDoor3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelayTopDoor3.ForeColor = System.Drawing.Color.Black;
            this.btnDelayTopDoor3.Location = new System.Drawing.Point(845, 483);
            this.btnDelayTopDoor3.Name = "btnDelayTopDoor3";
            this.btnDelayTopDoor3.Size = new System.Drawing.Size(64, 22);
            this.btnDelayTopDoor3.TabIndex = 485;
            this.btnDelayTopDoor3.Text = "도어 3";
            this.btnDelayTopDoor3.UseVisualStyleBackColor = false;
            // 
            // elementHost1
            // 
            this.elementHost1.BackColor = System.Drawing.Color.Transparent;
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.ForeColor = System.Drawing.SystemColors.Control;
            this.elementHost1.Location = new System.Drawing.Point(0, 20);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(1063, 487);
            this.elementHost1.TabIndex = 1;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // lbleEquipState
            // 
            this.lbleEquipState.AutoEllipsis = true;
            this.lbleEquipState.BackColor = System.Drawing.Color.Gainsboro;
            this.lbleEquipState.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbleEquipState.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lbleEquipState.ForeColor = System.Drawing.Color.Black;
            this.lbleEquipState.Location = new System.Drawing.Point(0, 0);
            this.lbleEquipState.Name = "lbleEquipState";
            this.lbleEquipState.Size = new System.Drawing.Size(1063, 20);
            this.lbleEquipState.TabIndex = 9;
            this.lbleEquipState.Text = "■ 장비 상태";
            this.lbleEquipState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblShutter);
            this.panel1.Controls.Add(this.lblShutterInfo);
            this.panel1.Controls.Add(this.lblPower);
            this.panel1.Controls.Add(this.lblPowerInfo);
            this.panel1.Controls.Add(this.lblDivider);
            this.panel1.Controls.Add(this.lblDividerInfo);
            this.panel1.Controls.Add(this.lblPd7Power);
            this.panel1.Controls.Add(this.lblPd7PowerInfo);
            this.panel1.Controls.Add(this.lblOutAmplifier);
            this.panel1.Controls.Add(this.lblAmplifier);
            this.panel1.Controls.Add(this.lblBurst);
            this.panel1.Controls.Add(this.lblPulseMode);
            this.panel1.Controls.Add(this.lblOutAmplifierInfo);
            this.panel1.Controls.Add(this.lblAmplifierInfo);
            this.panel1.Controls.Add(this.lblBurstInfo);
            this.panel1.Controls.Add(this.lblPulseModeInfo);
            this.panel1.Controls.Add(this.lblLaserInfo);
            this.panel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel1.Location = new System.Drawing.Point(4, 252);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(253, 130);
            this.panel1.TabIndex = 460;
            // 
            // lblShutter
            // 
            this.lblShutter.BackColor = System.Drawing.SystemColors.Control;
            this.lblShutter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShutter.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblShutter.ForeColor = System.Drawing.Color.Black;
            this.lblShutter.Location = new System.Drawing.Point(216, 101);
            this.lblShutter.Name = "lblShutter";
            this.lblShutter.Size = new System.Drawing.Size(31, 20);
            this.lblShutter.TabIndex = 39;
            this.lblShutter.Text = "0";
            this.lblShutter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShutterInfo
            // 
            this.lblShutterInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblShutterInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShutterInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblShutterInfo.ForeColor = System.Drawing.Color.Black;
            this.lblShutterInfo.Location = new System.Drawing.Point(128, 101);
            this.lblShutterInfo.Name = "lblShutterInfo";
            this.lblShutterInfo.Size = new System.Drawing.Size(84, 20);
            this.lblShutterInfo.TabIndex = 38;
            this.lblShutterInfo.Text = "Shutter :";
            this.lblShutterInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPower
            // 
            this.lblPower.BackColor = System.Drawing.SystemColors.Control;
            this.lblPower.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPower.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPower.ForeColor = System.Drawing.Color.Black;
            this.lblPower.Location = new System.Drawing.Point(216, 76);
            this.lblPower.Name = "lblPower";
            this.lblPower.Size = new System.Drawing.Size(31, 20);
            this.lblPower.TabIndex = 37;
            this.lblPower.Text = "0";
            this.lblPower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPowerInfo
            // 
            this.lblPowerInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblPowerInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPowerInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPowerInfo.ForeColor = System.Drawing.Color.Black;
            this.lblPowerInfo.Location = new System.Drawing.Point(128, 76);
            this.lblPowerInfo.Name = "lblPowerInfo";
            this.lblPowerInfo.Size = new System.Drawing.Size(84, 20);
            this.lblPowerInfo.TabIndex = 36;
            this.lblPowerInfo.Text = "Power(%) :";
            this.lblPowerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDivider
            // 
            this.lblDivider.BackColor = System.Drawing.SystemColors.Control;
            this.lblDivider.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDivider.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDivider.ForeColor = System.Drawing.Color.Black;
            this.lblDivider.Location = new System.Drawing.Point(216, 51);
            this.lblDivider.Name = "lblDivider";
            this.lblDivider.Size = new System.Drawing.Size(31, 20);
            this.lblDivider.TabIndex = 35;
            this.lblDivider.Text = "0";
            this.lblDivider.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDividerInfo
            // 
            this.lblDividerInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblDividerInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDividerInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDividerInfo.ForeColor = System.Drawing.Color.Black;
            this.lblDividerInfo.Location = new System.Drawing.Point(128, 51);
            this.lblDividerInfo.Name = "lblDividerInfo";
            this.lblDividerInfo.Size = new System.Drawing.Size(84, 20);
            this.lblDividerInfo.TabIndex = 34;
            this.lblDividerInfo.Text = "Divider :";
            this.lblDividerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPd7Power
            // 
            this.lblPd7Power.BackColor = System.Drawing.SystemColors.Control;
            this.lblPd7Power.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPd7Power.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPd7Power.ForeColor = System.Drawing.Color.Black;
            this.lblPd7Power.Location = new System.Drawing.Point(216, 27);
            this.lblPd7Power.Name = "lblPd7Power";
            this.lblPd7Power.Size = new System.Drawing.Size(31, 20);
            this.lblPd7Power.TabIndex = 33;
            this.lblPd7Power.Text = "0";
            this.lblPd7Power.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPd7PowerInfo
            // 
            this.lblPd7PowerInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblPd7PowerInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPd7PowerInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPd7PowerInfo.ForeColor = System.Drawing.Color.Black;
            this.lblPd7PowerInfo.Location = new System.Drawing.Point(128, 27);
            this.lblPd7PowerInfo.Name = "lblPd7PowerInfo";
            this.lblPd7PowerInfo.Size = new System.Drawing.Size(84, 20);
            this.lblPd7PowerInfo.TabIndex = 32;
            this.lblPd7PowerInfo.Text = "PD7Power :";
            this.lblPd7PowerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOutAmplifier
            // 
            this.lblOutAmplifier.BackColor = System.Drawing.SystemColors.Control;
            this.lblOutAmplifier.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblOutAmplifier.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblOutAmplifier.ForeColor = System.Drawing.Color.Black;
            this.lblOutAmplifier.Location = new System.Drawing.Point(93, 101);
            this.lblOutAmplifier.Name = "lblOutAmplifier";
            this.lblOutAmplifier.Size = new System.Drawing.Size(31, 20);
            this.lblOutAmplifier.TabIndex = 31;
            this.lblOutAmplifier.Text = "0";
            this.lblOutAmplifier.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAmplifier
            // 
            this.lblAmplifier.BackColor = System.Drawing.SystemColors.Control;
            this.lblAmplifier.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAmplifier.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAmplifier.ForeColor = System.Drawing.Color.Black;
            this.lblAmplifier.Location = new System.Drawing.Point(93, 76);
            this.lblAmplifier.Name = "lblAmplifier";
            this.lblAmplifier.Size = new System.Drawing.Size(31, 20);
            this.lblAmplifier.TabIndex = 30;
            this.lblAmplifier.Text = "0";
            this.lblAmplifier.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBurst
            // 
            this.lblBurst.BackColor = System.Drawing.SystemColors.Control;
            this.lblBurst.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBurst.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBurst.ForeColor = System.Drawing.Color.Black;
            this.lblBurst.Location = new System.Drawing.Point(93, 51);
            this.lblBurst.Name = "lblBurst";
            this.lblBurst.Size = new System.Drawing.Size(31, 20);
            this.lblBurst.TabIndex = 29;
            this.lblBurst.Text = "0";
            this.lblBurst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPulseMode
            // 
            this.lblPulseMode.BackColor = System.Drawing.SystemColors.Control;
            this.lblPulseMode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPulseMode.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPulseMode.ForeColor = System.Drawing.Color.Black;
            this.lblPulseMode.Location = new System.Drawing.Point(93, 27);
            this.lblPulseMode.Name = "lblPulseMode";
            this.lblPulseMode.Size = new System.Drawing.Size(31, 20);
            this.lblPulseMode.TabIndex = 28;
            this.lblPulseMode.Text = "0";
            this.lblPulseMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOutAmplifierInfo
            // 
            this.lblOutAmplifierInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblOutAmplifierInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblOutAmplifierInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblOutAmplifierInfo.ForeColor = System.Drawing.Color.Black;
            this.lblOutAmplifierInfo.Location = new System.Drawing.Point(5, 101);
            this.lblOutAmplifierInfo.Name = "lblOutAmplifierInfo";
            this.lblOutAmplifierInfo.Size = new System.Drawing.Size(84, 20);
            this.lblOutAmplifierInfo.TabIndex = 27;
            this.lblOutAmplifierInfo.Text = "Out Amplifier :";
            this.lblOutAmplifierInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAmplifierInfo
            // 
            this.lblAmplifierInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblAmplifierInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAmplifierInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAmplifierInfo.ForeColor = System.Drawing.Color.Black;
            this.lblAmplifierInfo.Location = new System.Drawing.Point(5, 76);
            this.lblAmplifierInfo.Name = "lblAmplifierInfo";
            this.lblAmplifierInfo.Size = new System.Drawing.Size(84, 20);
            this.lblAmplifierInfo.TabIndex = 26;
            this.lblAmplifierInfo.Text = "Amplifier :";
            this.lblAmplifierInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBurstInfo
            // 
            this.lblBurstInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblBurstInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBurstInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBurstInfo.ForeColor = System.Drawing.Color.Black;
            this.lblBurstInfo.Location = new System.Drawing.Point(5, 51);
            this.lblBurstInfo.Name = "lblBurstInfo";
            this.lblBurstInfo.Size = new System.Drawing.Size(84, 20);
            this.lblBurstInfo.TabIndex = 25;
            this.lblBurstInfo.Text = "Burst :";
            this.lblBurstInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPulseModeInfo
            // 
            this.lblPulseModeInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblPulseModeInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPulseModeInfo.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPulseModeInfo.ForeColor = System.Drawing.Color.Black;
            this.lblPulseModeInfo.Location = new System.Drawing.Point(5, 27);
            this.lblPulseModeInfo.Name = "lblPulseModeInfo";
            this.lblPulseModeInfo.Size = new System.Drawing.Size(84, 20);
            this.lblPulseModeInfo.TabIndex = 24;
            this.lblPulseModeInfo.Text = "Pulse Mode :";
            this.lblPulseModeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLaserInfo
            // 
            this.lblLaserInfo.AutoEllipsis = true;
            this.lblLaserInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLaserInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLaserInfo.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLaserInfo.ForeColor = System.Drawing.Color.Black;
            this.lblLaserInfo.Location = new System.Drawing.Point(0, 0);
            this.lblLaserInfo.Name = "lblLaserInfo";
            this.lblLaserInfo.Size = new System.Drawing.Size(251, 20);
            this.lblLaserInfo.TabIndex = 9;
            this.lblLaserInfo.Text = "■ 레이저 정보";
            this.lblLaserInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtTactTime);
            this.panel2.Controls.Add(this.txtRunCount);
            this.panel2.Controls.Add(this.txtRunTime);
            this.panel2.Controls.Add(this.txtStartTime);
            this.panel2.Controls.Add(this.txtCellSize);
            this.panel2.Controls.Add(this.txtProcess);
            this.panel2.Controls.Add(this.txtRunName);
            this.panel2.Controls.Add(this.txtRecipeName);
            this.panel2.Controls.Add(this.lblTactTime);
            this.panel2.Controls.Add(this.lblRunCount);
            this.panel2.Controls.Add(this.lblRunTime);
            this.panel2.Controls.Add(this.lblStartTime);
            this.panel2.Controls.Add(this.lblCellSize);
            this.panel2.Controls.Add(this.lblProcess);
            this.panel2.Controls.Add(this.lblRecipeName);
            this.panel2.Controls.Add(this.lblRunName);
            this.panel2.Controls.Add(this.lblRunInfo);
            this.panel2.Location = new System.Drawing.Point(4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(253, 247);
            this.panel2.TabIndex = 459;
            // 
            // txtTactTime
            // 
            this.txtTactTime.BackColor = System.Drawing.SystemColors.Control;
            this.txtTactTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTactTime.Location = new System.Drawing.Point(83, 214);
            this.txtTactTime.Name = "txtTactTime";
            this.txtTactTime.ReadOnly = true;
            this.txtTactTime.Size = new System.Drawing.Size(164, 23);
            this.txtTactTime.TabIndex = 35;
            // 
            // txtRunCount
            // 
            this.txtRunCount.BackColor = System.Drawing.SystemColors.Control;
            this.txtRunCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRunCount.Location = new System.Drawing.Point(83, 187);
            this.txtRunCount.Name = "txtRunCount";
            this.txtRunCount.ReadOnly = true;
            this.txtRunCount.Size = new System.Drawing.Size(164, 23);
            this.txtRunCount.TabIndex = 34;
            // 
            // txtRunTime
            // 
            this.txtRunTime.BackColor = System.Drawing.SystemColors.Control;
            this.txtRunTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRunTime.Location = new System.Drawing.Point(83, 160);
            this.txtRunTime.Name = "txtRunTime";
            this.txtRunTime.ReadOnly = true;
            this.txtRunTime.Size = new System.Drawing.Size(164, 23);
            this.txtRunTime.TabIndex = 33;
            // 
            // txtStartTime
            // 
            this.txtStartTime.BackColor = System.Drawing.SystemColors.Control;
            this.txtStartTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtStartTime.Location = new System.Drawing.Point(83, 133);
            this.txtStartTime.Name = "txtStartTime";
            this.txtStartTime.ReadOnly = true;
            this.txtStartTime.Size = new System.Drawing.Size(164, 23);
            this.txtStartTime.TabIndex = 32;
            // 
            // txtCellSize
            // 
            this.txtCellSize.BackColor = System.Drawing.SystemColors.Control;
            this.txtCellSize.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCellSize.Location = new System.Drawing.Point(83, 106);
            this.txtCellSize.Name = "txtCellSize";
            this.txtCellSize.ReadOnly = true;
            this.txtCellSize.Size = new System.Drawing.Size(164, 23);
            this.txtCellSize.TabIndex = 31;
            // 
            // txtProcess
            // 
            this.txtProcess.BackColor = System.Drawing.SystemColors.Control;
            this.txtProcess.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtProcess.Location = new System.Drawing.Point(83, 80);
            this.txtProcess.Name = "txtProcess";
            this.txtProcess.ReadOnly = true;
            this.txtProcess.Size = new System.Drawing.Size(164, 23);
            this.txtProcess.TabIndex = 30;
            // 
            // txtRunName
            // 
            this.txtRunName.BackColor = System.Drawing.SystemColors.Control;
            this.txtRunName.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRunName.Location = new System.Drawing.Point(83, 26);
            this.txtRunName.Name = "txtRunName";
            this.txtRunName.ReadOnly = true;
            this.txtRunName.Size = new System.Drawing.Size(164, 23);
            this.txtRunName.TabIndex = 28;
            // 
            // txtRecipeName
            // 
            this.txtRecipeName.BackColor = System.Drawing.SystemColors.Control;
            this.txtRecipeName.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRecipeName.Location = new System.Drawing.Point(83, 53);
            this.txtRecipeName.Name = "txtRecipeName";
            this.txtRecipeName.ReadOnly = true;
            this.txtRecipeName.Size = new System.Drawing.Size(164, 23);
            this.txtRecipeName.TabIndex = 29;
            // 
            // lblTactTime
            // 
            this.lblTactTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblTactTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTactTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTactTime.ForeColor = System.Drawing.Color.Black;
            this.lblTactTime.Location = new System.Drawing.Point(3, 215);
            this.lblTactTime.Name = "lblTactTime";
            this.lblTactTime.Size = new System.Drawing.Size(72, 20);
            this.lblTactTime.TabIndex = 27;
            this.lblTactTime.Text = "Tact-Time";
            this.lblTactTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRunCount
            // 
            this.lblRunCount.BackColor = System.Drawing.SystemColors.Control;
            this.lblRunCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRunCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRunCount.ForeColor = System.Drawing.Color.Black;
            this.lblRunCount.Location = new System.Drawing.Point(3, 188);
            this.lblRunCount.Name = "lblRunCount";
            this.lblRunCount.Size = new System.Drawing.Size(72, 20);
            this.lblRunCount.TabIndex = 26;
            this.lblRunCount.Text = "작업 수량";
            this.lblRunCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRunTime
            // 
            this.lblRunTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblRunTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRunTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRunTime.ForeColor = System.Drawing.Color.Black;
            this.lblRunTime.Location = new System.Drawing.Point(3, 161);
            this.lblRunTime.Name = "lblRunTime";
            this.lblRunTime.Size = new System.Drawing.Size(72, 20);
            this.lblRunTime.TabIndex = 25;
            this.lblRunTime.Text = "작업 시간";
            this.lblRunTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStartTime
            // 
            this.lblStartTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblStartTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblStartTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStartTime.ForeColor = System.Drawing.Color.Black;
            this.lblStartTime.Location = new System.Drawing.Point(3, 135);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(72, 20);
            this.lblStartTime.TabIndex = 24;
            this.lblStartTime.Text = "시작 시간";
            this.lblStartTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCellSize
            // 
            this.lblCellSize.BackColor = System.Drawing.SystemColors.Control;
            this.lblCellSize.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellSize.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCellSize.ForeColor = System.Drawing.Color.Black;
            this.lblCellSize.Location = new System.Drawing.Point(3, 107);
            this.lblCellSize.Name = "lblCellSize";
            this.lblCellSize.Size = new System.Drawing.Size(72, 20);
            this.lblCellSize.TabIndex = 23;
            this.lblCellSize.Text = "셀 사이즈";
            this.lblCellSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcess
            // 
            this.lblProcess.BackColor = System.Drawing.SystemColors.Control;
            this.lblProcess.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcess.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProcess.ForeColor = System.Drawing.Color.Black;
            this.lblProcess.Location = new System.Drawing.Point(3, 81);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(72, 20);
            this.lblProcess.TabIndex = 22;
            this.lblProcess.Text = "프로세스";
            this.lblProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRecipeName
            // 
            this.lblRecipeName.BackColor = System.Drawing.SystemColors.Control;
            this.lblRecipeName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRecipeName.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRecipeName.ForeColor = System.Drawing.Color.Black;
            this.lblRecipeName.Location = new System.Drawing.Point(3, 54);
            this.lblRecipeName.Name = "lblRecipeName";
            this.lblRecipeName.Size = new System.Drawing.Size(72, 20);
            this.lblRecipeName.TabIndex = 21;
            this.lblRecipeName.Text = "레시피명";
            this.lblRecipeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRunName
            // 
            this.lblRunName.BackColor = System.Drawing.SystemColors.Control;
            this.lblRunName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRunName.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRunName.ForeColor = System.Drawing.Color.Black;
            this.lblRunName.Location = new System.Drawing.Point(3, 27);
            this.lblRunName.Name = "lblRunName";
            this.lblRunName.Size = new System.Drawing.Size(72, 20);
            this.lblRunName.TabIndex = 20;
            this.lblRunName.Text = "작업자명";
            this.lblRunName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRunInfo
            // 
            this.lblRunInfo.AutoEllipsis = true;
            this.lblRunInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblRunInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblRunInfo.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblRunInfo.ForeColor = System.Drawing.Color.Black;
            this.lblRunInfo.Location = new System.Drawing.Point(0, 0);
            this.lblRunInfo.Name = "lblRunInfo";
            this.lblRunInfo.Size = new System.Drawing.Size(251, 20);
            this.lblRunInfo.TabIndex = 9;
            this.lblRunInfo.Text = "■ 작업 정보";
            this.lblRunInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lblInsp2ConnectInfo);
            this.panel6.Controls.Add(this.lblInsp1ConnectInfo);
            this.panel6.Controls.Add(this.lblSerialConnectInfo);
            this.panel6.Controls.Add(this.lblSequenceConnectInfo);
            this.panel6.Controls.Add(this.lblLaserConnectInfo);
            this.panel6.Controls.Add(this.lblUmacConnectInfo);
            this.panel6.Controls.Add(this.lblAjinConnectInfo);
            this.panel6.Controls.Add(this.lblInsp2Connect);
            this.panel6.Controls.Add(this.lblSerialConnect);
            this.panel6.Controls.Add(this.lblInsp1Connect);
            this.panel6.Controls.Add(this.lblSequenceConnect);
            this.panel6.Controls.Add(this.lblLaserConnect);
            this.panel6.Controls.Add(this.lblUmacConnect);
            this.panel6.Controls.Add(this.lblAjinConnect);
            this.panel6.Controls.Add(this.lblCimConnect);
            this.panel6.Controls.Add(this.lblCimConnectInfo);
            this.panel6.Controls.Add(this.lblProcessConnectInfo);
            this.panel6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel6.Location = new System.Drawing.Point(1, 557);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(132, 300);
            this.panel6.TabIndex = 462;
            // 
            // lblInsp2ConnectInfo
            // 
            this.lblInsp2ConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblInsp2ConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInsp2ConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblInsp2ConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInsp2ConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblInsp2ConnectInfo.Location = new System.Drawing.Point(10, 263);
            this.lblInsp2ConnectInfo.Name = "lblInsp2ConnectInfo";
            this.lblInsp2ConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblInsp2ConnectInfo.TabIndex = 67;
            this.lblInsp2ConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInsp1ConnectInfo
            // 
            this.lblInsp1ConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblInsp1ConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblInsp1ConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblInsp1ConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInsp1ConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblInsp1ConnectInfo.Location = new System.Drawing.Point(10, 230);
            this.lblInsp1ConnectInfo.Name = "lblInsp1ConnectInfo";
            this.lblInsp1ConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblInsp1ConnectInfo.TabIndex = 66;
            this.lblInsp1ConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSerialConnectInfo
            // 
            this.lblSerialConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblSerialConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSerialConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSerialConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSerialConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSerialConnectInfo.Location = new System.Drawing.Point(10, 197);
            this.lblSerialConnectInfo.Name = "lblSerialConnectInfo";
            this.lblSerialConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblSerialConnectInfo.TabIndex = 65;
            this.lblSerialConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSequenceConnectInfo
            // 
            this.lblSequenceConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblSequenceConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSequenceConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSequenceConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSequenceConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSequenceConnectInfo.Location = new System.Drawing.Point(10, 164);
            this.lblSequenceConnectInfo.Name = "lblSequenceConnectInfo";
            this.lblSequenceConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblSequenceConnectInfo.TabIndex = 64;
            this.lblSequenceConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLaserConnectInfo
            // 
            this.lblLaserConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblLaserConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLaserConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLaserConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLaserConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLaserConnectInfo.Location = new System.Drawing.Point(10, 131);
            this.lblLaserConnectInfo.Name = "lblLaserConnectInfo";
            this.lblLaserConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblLaserConnectInfo.TabIndex = 63;
            this.lblLaserConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUmacConnectInfo
            // 
            this.lblUmacConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblUmacConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUmacConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUmacConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUmacConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblUmacConnectInfo.Location = new System.Drawing.Point(10, 98);
            this.lblUmacConnectInfo.Name = "lblUmacConnectInfo";
            this.lblUmacConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblUmacConnectInfo.TabIndex = 62;
            this.lblUmacConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinConnectInfo
            // 
            this.lblAjinConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblAjinConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAjinConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblAjinConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAjinConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblAjinConnectInfo.Location = new System.Drawing.Point(10, 65);
            this.lblAjinConnectInfo.Name = "lblAjinConnectInfo";
            this.lblAjinConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblAjinConnectInfo.TabIndex = 61;
            this.lblAjinConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInsp2Connect
            // 
            this.lblInsp2Connect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInsp2Connect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInsp2Connect.ForeColor = System.Drawing.Color.Black;
            this.lblInsp2Connect.Location = new System.Drawing.Point(41, 263);
            this.lblInsp2Connect.Name = "lblInsp2Connect";
            this.lblInsp2Connect.Size = new System.Drawing.Size(73, 25);
            this.lblInsp2Connect.TabIndex = 60;
            this.lblInsp2Connect.Text = "검사기 2";
            this.lblInsp2Connect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSerialConnect
            // 
            this.lblSerialConnect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerialConnect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSerialConnect.ForeColor = System.Drawing.Color.Black;
            this.lblSerialConnect.Location = new System.Drawing.Point(41, 197);
            this.lblSerialConnect.Name = "lblSerialConnect";
            this.lblSerialConnect.Size = new System.Drawing.Size(73, 25);
            this.lblSerialConnect.TabIndex = 58;
            this.lblSerialConnect.Text = "Serial 통신";
            this.lblSerialConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInsp1Connect
            // 
            this.lblInsp1Connect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInsp1Connect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInsp1Connect.ForeColor = System.Drawing.Color.Black;
            this.lblInsp1Connect.Location = new System.Drawing.Point(41, 230);
            this.lblInsp1Connect.Name = "lblInsp1Connect";
            this.lblInsp1Connect.Size = new System.Drawing.Size(73, 25);
            this.lblInsp1Connect.TabIndex = 59;
            this.lblInsp1Connect.Text = "검사기 1";
            this.lblInsp1Connect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSequenceConnect
            // 
            this.lblSequenceConnect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSequenceConnect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSequenceConnect.ForeColor = System.Drawing.Color.Black;
            this.lblSequenceConnect.Location = new System.Drawing.Point(41, 164);
            this.lblSequenceConnect.Name = "lblSequenceConnect";
            this.lblSequenceConnect.Size = new System.Drawing.Size(73, 25);
            this.lblSequenceConnect.TabIndex = 57;
            this.lblSequenceConnect.Text = "시퀀스";
            this.lblSequenceConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLaserConnect
            // 
            this.lblLaserConnect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLaserConnect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLaserConnect.ForeColor = System.Drawing.Color.Black;
            this.lblLaserConnect.Location = new System.Drawing.Point(41, 131);
            this.lblLaserConnect.Name = "lblLaserConnect";
            this.lblLaserConnect.Size = new System.Drawing.Size(73, 25);
            this.lblLaserConnect.TabIndex = 56;
            this.lblLaserConnect.Text = "레이저";
            this.lblLaserConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUmacConnect
            // 
            this.lblUmacConnect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUmacConnect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUmacConnect.ForeColor = System.Drawing.Color.Black;
            this.lblUmacConnect.Location = new System.Drawing.Point(41, 98);
            this.lblUmacConnect.Name = "lblUmacConnect";
            this.lblUmacConnect.Size = new System.Drawing.Size(73, 25);
            this.lblUmacConnect.TabIndex = 55;
            this.lblUmacConnect.Text = "UMAC-PLC";
            this.lblUmacConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinConnect
            // 
            this.lblAjinConnect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinConnect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAjinConnect.ForeColor = System.Drawing.Color.Black;
            this.lblAjinConnect.Location = new System.Drawing.Point(41, 65);
            this.lblAjinConnect.Name = "lblAjinConnect";
            this.lblAjinConnect.Size = new System.Drawing.Size(73, 25);
            this.lblAjinConnect.TabIndex = 54;
            this.lblAjinConnect.Text = "AJIN-PLC";
            this.lblAjinConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCimConnect
            // 
            this.lblCimConnect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCimConnect.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCimConnect.ForeColor = System.Drawing.Color.Black;
            this.lblCimConnect.Location = new System.Drawing.Point(41, 31);
            this.lblCimConnect.Name = "lblCimConnect";
            this.lblCimConnect.Size = new System.Drawing.Size(73, 25);
            this.lblCimConnect.TabIndex = 53;
            this.lblCimConnect.Text = "CIM-PC";
            this.lblCimConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCimConnectInfo
            // 
            this.lblCimConnectInfo.BackColor = System.Drawing.Color.Red;
            this.lblCimConnectInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCimConnectInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCimConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCimConnectInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCimConnectInfo.Location = new System.Drawing.Point(10, 32);
            this.lblCimConnectInfo.Name = "lblCimConnectInfo";
            this.lblCimConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.lblCimConnectInfo.TabIndex = 10;
            this.lblCimConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessConnectInfo
            // 
            this.lblProcessConnectInfo.AutoEllipsis = true;
            this.lblProcessConnectInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessConnectInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessConnectInfo.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessConnectInfo.ForeColor = System.Drawing.Color.Black;
            this.lblProcessConnectInfo.Location = new System.Drawing.Point(0, 0);
            this.lblProcessConnectInfo.Name = "lblProcessConnectInfo";
            this.lblProcessConnectInfo.Size = new System.Drawing.Size(130, 20);
            this.lblProcessConnectInfo.TabIndex = 9;
            this.lblProcessConnectInfo.Text = "■ 프로세스 연결상태";
            this.lblProcessConnectInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ucrlCstCellCountLDA
            // 
            this.ucrlCstCellCountLDA.BackColor = System.Drawing.SystemColors.Control;
            this.ucrlCstCellCountLDA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlCstCellCountLDA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlCstCellCountLDA.Location = new System.Drawing.Point(1635, 4);
            this.ucrlCstCellCountLDA.Name = "ucrlCstCellCountLDA";
            this.ucrlCstCellCountLDA.Port = null;
            this.ucrlCstCellCountLDA.Size = new System.Drawing.Size(99, 379);
            this.ucrlCstCellCountLDA.TabIndex = 496;
            // 
            // ucrlCstCellCountLDB
            // 
            this.ucrlCstCellCountLDB.BackColor = System.Drawing.SystemColors.Control;
            this.ucrlCstCellCountLDB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlCstCellCountLDB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlCstCellCountLDB.Location = new System.Drawing.Point(1532, 4);
            this.ucrlCstCellCountLDB.Name = "ucrlCstCellCountLDB";
            this.ucrlCstCellCountLDB.Port = null;
            this.ucrlCstCellCountLDB.Size = new System.Drawing.Size(99, 379);
            this.ucrlCstCellCountLDB.TabIndex = 496;
            // 
            // ucrlCstCellCountULDA
            // 
            this.ucrlCstCellCountULDA.BackColor = System.Drawing.SystemColors.Control;
            this.ucrlCstCellCountULDA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlCstCellCountULDA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlCstCellCountULDA.Location = new System.Drawing.Point(360, 3);
            this.ucrlCstCellCountULDA.Name = "ucrlCstCellCountULDA";
            this.ucrlCstCellCountULDA.Port = null;
            this.ucrlCstCellCountULDA.Size = new System.Drawing.Size(99, 379);
            this.ucrlCstCellCountULDA.TabIndex = 496;
            // 
            // ucrlCstCellCountULDB
            // 
            this.ucrlCstCellCountULDB.BackColor = System.Drawing.SystemColors.Control;
            this.ucrlCstCellCountULDB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlCstCellCountULDB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlCstCellCountULDB.Location = new System.Drawing.Point(258, 3);
            this.ucrlCstCellCountULDB.Name = "ucrlCstCellCountULDB";
            this.ucrlCstCellCountULDB.Port = null;
            this.ucrlCstCellCountULDB.Size = new System.Drawing.Size(99, 379);
            this.ucrlCstCellCountULDB.TabIndex = 496;
            // 
            // UcrlDtvFineAlign
            // 
            this.UcrlDtvFineAlign.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvFineAlign.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvFineAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvFineAlign.Location = new System.Drawing.Point(478, 745);
            this.UcrlDtvFineAlign.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvFineAlign.Name = "UcrlDtvFineAlign";
            this.UcrlDtvFineAlign.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvFineAlign.TabIndex = 495;
            this.UcrlDtvFineAlign.Unit = null;
            // 
            // UcrlDtvBreakAlign
            // 
            this.UcrlDtvBreakAlign.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvBreakAlign.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvBreakAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvBreakAlign.Location = new System.Drawing.Point(636, 745);
            this.UcrlDtvBreakAlign.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvBreakAlign.Name = "UcrlDtvBreakAlign";
            this.UcrlDtvBreakAlign.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvBreakAlign.TabIndex = 494;
            this.UcrlDtvBreakAlign.Unit = null;
            // 
            // UcrlDtvInspStageA
            // 
            this.UcrlDtvInspStageA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvInspStageA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvInspStageA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvInspStageA.Location = new System.Drawing.Point(478, 631);
            this.UcrlDtvInspStageA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvInspStageA.Name = "UcrlDtvInspStageA";
            this.UcrlDtvInspStageA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvInspStageA.TabIndex = 491;
            this.UcrlDtvInspStageA.Unit = null;
            // 
            // UcrlDtvPreAlign
            // 
            this.UcrlDtvPreAlign.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvPreAlign.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvPreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvPreAlign.Location = new System.Drawing.Point(794, 745);
            this.UcrlDtvPreAlign.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvPreAlign.Name = "UcrlDtvPreAlign";
            this.UcrlDtvPreAlign.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvPreAlign.TabIndex = 490;
            this.UcrlDtvPreAlign.Unit = null;
            // 
            // UcrlDtvBreakStageA
            // 
            this.UcrlDtvBreakStageA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvBreakStageA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvBreakStageA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvBreakStageA.Location = new System.Drawing.Point(794, 631);
            this.UcrlDtvBreakStageA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvBreakStageA.Name = "UcrlDtvBreakStageA";
            this.UcrlDtvBreakStageA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvBreakStageA.TabIndex = 489;
            this.UcrlDtvBreakStageA.Unit = null;
            // 
            // UcrlDtvInspStageB
            // 
            this.UcrlDtvInspStageB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvInspStageB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvInspStageB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvInspStageB.Location = new System.Drawing.Point(478, 516);
            this.UcrlDtvInspStageB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvInspStageB.Name = "UcrlDtvInspStageB";
            this.UcrlDtvInspStageB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvInspStageB.TabIndex = 488;
            this.UcrlDtvInspStageB.Unit = null;
            // 
            // UcrlDtvCstLoaderA
            // 
            this.UcrlDtvCstLoaderA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvCstLoaderA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvCstLoaderA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvCstLoaderA.Location = new System.Drawing.Point(1584, 631);
            this.UcrlDtvCstLoaderA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvCstLoaderA.Name = "UcrlDtvCstLoaderA";
            this.UcrlDtvCstLoaderA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvCstLoaderA.TabIndex = 486;
            this.UcrlDtvCstLoaderA.Unit = null;
            // 
            // UcrlDtvLoaderTransferA
            // 
            this.UcrlDtvLoaderTransferA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvLoaderTransferA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvLoaderTransferA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvLoaderTransferA.Location = new System.Drawing.Point(1268, 631);
            this.UcrlDtvLoaderTransferA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvLoaderTransferA.Name = "UcrlDtvLoaderTransferA";
            this.UcrlDtvLoaderTransferA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvLoaderTransferA.TabIndex = 485;
            this.UcrlDtvLoaderTransferA.Unit = null;
            // 
            // UcrlDtvIRCutStageA
            // 
            this.UcrlDtvIRCutStageA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvIRCutStageA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvIRCutStageA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvIRCutStageA.Location = new System.Drawing.Point(1110, 631);
            this.UcrlDtvIRCutStageA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvIRCutStageA.Name = "UcrlDtvIRCutStageA";
            this.UcrlDtvIRCutStageA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvIRCutStageA.TabIndex = 484;
            this.UcrlDtvIRCutStageA.Unit = null;
            // 
            // UcrlDtvBeforeTransferA
            // 
            this.UcrlDtvBeforeTransferA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvBeforeTransferA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvBeforeTransferA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvBeforeTransferA.Location = new System.Drawing.Point(636, 631);
            this.UcrlDtvBeforeTransferA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvBeforeTransferA.Name = "UcrlDtvBeforeTransferA";
            this.UcrlDtvBeforeTransferA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvBeforeTransferA.TabIndex = 482;
            this.UcrlDtvBeforeTransferA.Unit = null;
            // 
            // UcrlDtvAfterInspTransferA
            // 
            this.UcrlDtvAfterInspTransferA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvAfterInspTransferA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvAfterInspTransferA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvAfterInspTransferA.Location = new System.Drawing.Point(320, 631);
            this.UcrlDtvAfterInspTransferA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvAfterInspTransferA.Name = "UcrlDtvAfterInspTransferA";
            this.UcrlDtvAfterInspTransferA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvAfterInspTransferA.TabIndex = 481;
            this.UcrlDtvAfterInspTransferA.Unit = null;
            // 
            // UcrlDtvCstUnloaderA
            // 
            this.UcrlDtvCstUnloaderA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvCstUnloaderA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvCstUnloaderA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvCstUnloaderA.Location = new System.Drawing.Point(4, 631);
            this.UcrlDtvCstUnloaderA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvCstUnloaderA.Name = "UcrlDtvCstUnloaderA";
            this.UcrlDtvCstUnloaderA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvCstUnloaderA.TabIndex = 480;
            this.UcrlDtvCstUnloaderA.Unit = null;
            // 
            // UcrlDtvCstLoaderB
            // 
            this.UcrlDtvCstLoaderB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvCstLoaderB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvCstLoaderB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvCstLoaderB.Location = new System.Drawing.Point(1584, 516);
            this.UcrlDtvCstLoaderB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvCstLoaderB.Name = "UcrlDtvCstLoaderB";
            this.UcrlDtvCstLoaderB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvCstLoaderB.TabIndex = 479;
            this.UcrlDtvCstLoaderB.Unit = null;
            // 
            // UcrlDtvLoader
            // 
            this.UcrlDtvLoader.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvLoader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvLoader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvLoader.Location = new System.Drawing.Point(1426, 516);
            this.UcrlDtvLoader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvLoader.Name = "UcrlDtvLoader";
            this.UcrlDtvLoader.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvLoader.TabIndex = 478;
            this.UcrlDtvLoader.Unit = null;
            // 
            // UcrlDtvLoaderTransferB
            // 
            this.UcrlDtvLoaderTransferB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvLoaderTransferB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvLoaderTransferB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvLoaderTransferB.Location = new System.Drawing.Point(1268, 516);
            this.UcrlDtvLoaderTransferB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvLoaderTransferB.Name = "UcrlDtvLoaderTransferB";
            this.UcrlDtvLoaderTransferB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvLoaderTransferB.TabIndex = 477;
            this.UcrlDtvLoaderTransferB.Unit = null;
            // 
            // UcrlDtvIRCutStageB
            // 
            this.UcrlDtvIRCutStageB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvIRCutStageB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvIRCutStageB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvIRCutStageB.Location = new System.Drawing.Point(1110, 516);
            this.UcrlDtvIRCutStageB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvIRCutStageB.Name = "UcrlDtvIRCutStageB";
            this.UcrlDtvIRCutStageB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvIRCutStageB.TabIndex = 476;
            this.UcrlDtvIRCutStageB.Unit = null;
            // 
            // UcrlDtvAfterIRCutTransferA
            // 
            this.UcrlDtvAfterIRCutTransferA.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvAfterIRCutTransferA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvAfterIRCutTransferA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvAfterIRCutTransferA.Location = new System.Drawing.Point(952, 631);
            this.UcrlDtvAfterIRCutTransferA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvAfterIRCutTransferA.Name = "UcrlDtvAfterIRCutTransferA";
            this.UcrlDtvAfterIRCutTransferA.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvAfterIRCutTransferA.TabIndex = 475;
            this.UcrlDtvAfterIRCutTransferA.Unit = null;
            // 
            // UcrlDtvAfterIRCutTransferB
            // 
            this.UcrlDtvAfterIRCutTransferB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvAfterIRCutTransferB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvAfterIRCutTransferB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvAfterIRCutTransferB.Location = new System.Drawing.Point(952, 516);
            this.UcrlDtvAfterIRCutTransferB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvAfterIRCutTransferB.Name = "UcrlDtvAfterIRCutTransferB";
            this.UcrlDtvAfterIRCutTransferB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvAfterIRCutTransferB.TabIndex = 475;
            this.UcrlDtvAfterIRCutTransferB.Unit = null;
            // 
            // UcrlDtvBreakStageB
            // 
            this.UcrlDtvBreakStageB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvBreakStageB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvBreakStageB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvBreakStageB.Location = new System.Drawing.Point(794, 516);
            this.UcrlDtvBreakStageB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvBreakStageB.Name = "UcrlDtvBreakStageB";
            this.UcrlDtvBreakStageB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvBreakStageB.TabIndex = 474;
            this.UcrlDtvBreakStageB.Unit = null;
            // 
            // UcrlDtvBeforeTransferB
            // 
            this.UcrlDtvBeforeTransferB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvBeforeTransferB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvBeforeTransferB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvBeforeTransferB.Location = new System.Drawing.Point(636, 516);
            this.UcrlDtvBeforeTransferB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvBeforeTransferB.Name = "UcrlDtvBeforeTransferB";
            this.UcrlDtvBeforeTransferB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvBeforeTransferB.TabIndex = 473;
            this.UcrlDtvBeforeTransferB.Unit = null;
            // 
            // UcrlDtvAfterInspTransferB
            // 
            this.UcrlDtvAfterInspTransferB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvAfterInspTransferB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvAfterInspTransferB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvAfterInspTransferB.Location = new System.Drawing.Point(320, 516);
            this.UcrlDtvAfterInspTransferB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvAfterInspTransferB.Name = "UcrlDtvAfterInspTransferB";
            this.UcrlDtvAfterInspTransferB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvAfterInspTransferB.TabIndex = 472;
            this.UcrlDtvAfterInspTransferB.Unit = null;
            // 
            // UcrlDtvUnloader
            // 
            this.UcrlDtvUnloader.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvUnloader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvUnloader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvUnloader.Location = new System.Drawing.Point(162, 516);
            this.UcrlDtvUnloader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvUnloader.Name = "UcrlDtvUnloader";
            this.UcrlDtvUnloader.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvUnloader.TabIndex = 471;
            this.UcrlDtvUnloader.Unit = null;
            // 
            // UcrlDtvCstUnloaderB
            // 
            this.UcrlDtvCstUnloaderB.BackColor = System.Drawing.SystemColors.Control;
            this.UcrlDtvCstUnloaderB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UcrlDtvCstUnloaderB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UcrlDtvCstUnloaderB.Location = new System.Drawing.Point(4, 516);
            this.UcrlDtvCstUnloaderB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UcrlDtvCstUnloaderB.Name = "UcrlDtvCstUnloaderB";
            this.UcrlDtvCstUnloaderB.Size = new System.Drawing.Size(152, 112);
            this.UcrlDtvCstUnloaderB.TabIndex = 470;
            this.UcrlDtvCstUnloaderB.Unit = null;
            // 
            // UcrlMainWindow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.splitContainer1);
            this.Name = "UcrlMainWindow";
            this.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnKeyswitch;
        private System.Windows.Forms.Button btnPm;
        private System.Windows.Forms.Button btnDoorOpen;
        private System.Windows.Forms.Button btnLight;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtTactTime;
        private System.Windows.Forms.TextBox txtRunCount;
        private System.Windows.Forms.TextBox txtRunTime;
        private System.Windows.Forms.TextBox txtStartTime;
        private System.Windows.Forms.TextBox txtCellSize;
        private System.Windows.Forms.TextBox txtProcess;
        private System.Windows.Forms.TextBox txtRunName;
        private System.Windows.Forms.TextBox txtRecipeName;
        private System.Windows.Forms.Label lblTactTime;
        private System.Windows.Forms.Label lblRunCount;
        private System.Windows.Forms.Label lblRunTime;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Label lblCellSize;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Label lblRecipeName;
        private System.Windows.Forms.Label lblRunName;
        internal System.Windows.Forms.Label lblRunInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblShutter;
        private System.Windows.Forms.Label lblShutterInfo;
        private System.Windows.Forms.Label lblPower;
        private System.Windows.Forms.Label lblPowerInfo;
        private System.Windows.Forms.Label lblDivider;
        private System.Windows.Forms.Label lblDividerInfo;
        private System.Windows.Forms.Label lblPd7Power;
        private System.Windows.Forms.Label lblPd7PowerInfo;
        private System.Windows.Forms.Label lblOutAmplifier;
        private System.Windows.Forms.Label lblAmplifier;
        private System.Windows.Forms.Label lblBurst;
        private System.Windows.Forms.Label lblPulseMode;
        private System.Windows.Forms.Label lblOutAmplifierInfo;
        private System.Windows.Forms.Label lblAmplifierInfo;
        private System.Windows.Forms.Label lblBurstInfo;
        private System.Windows.Forms.Label lblPulseModeInfo;
        internal System.Windows.Forms.Label lblLaserInfo;
        private System.Windows.Forms.Button btnSafetyReset;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblCimConnectInfo;
        internal System.Windows.Forms.Label lblProcessConnectInfo;
        private System.Windows.Forms.Label lblInsp2ConnectInfo;
        private System.Windows.Forms.Label lblInsp1ConnectInfo;
        private System.Windows.Forms.Label lblSerialConnectInfo;
        private System.Windows.Forms.Label lblSequenceConnectInfo;
        private System.Windows.Forms.Label lblLaserConnectInfo;
        private System.Windows.Forms.Label lblUmacConnectInfo;
        private System.Windows.Forms.Label lblAjinConnectInfo;
        private System.Windows.Forms.Label lblInsp2Connect;
        private System.Windows.Forms.Label lblSerialConnect;
        private System.Windows.Forms.Label lblInsp1Connect;
        private System.Windows.Forms.Label lblSequenceConnect;
        private System.Windows.Forms.Label lblLaserConnect;
        private System.Windows.Forms.Label lblUmacConnect;
        private System.Windows.Forms.Label lblAjinConnect;
        private System.Windows.Forms.Label lblCimConnect;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtAlignBreak4OffsetA;
        private System.Windows.Forms.TextBox txtAlignBreak3OffsetA;
        private System.Windows.Forms.TextBox txtAlignPre2OffsetA;
        private System.Windows.Forms.Label lblAlignOffsetA2;
        private System.Windows.Forms.TextBox txtAlignBreak4OffsetY;
        private System.Windows.Forms.TextBox txtAlignBreak3OffsetY;
        private System.Windows.Forms.TextBox txtAlignPre2OffsetY;
        private System.Windows.Forms.Label lblAlignOffsetY2;
        private System.Windows.Forms.TextBox txtAlignBreak4OffsetX;
        private System.Windows.Forms.TextBox txtAlignBreak3OffsetX;
        private System.Windows.Forms.TextBox txtAlignPre2OffsetX;
        private System.Windows.Forms.Label lblAlignOffsetX2;
        private System.Windows.Forms.TextBox txtAlignBreak4Result;
        private System.Windows.Forms.TextBox txtAlignBreak3Result;
        private System.Windows.Forms.TextBox txtAlignPre2Result;
        private System.Windows.Forms.Label lblAlignResult2;
        private System.Windows.Forms.Label lblAlignBreak4;
        private System.Windows.Forms.Label lblAlignBreak3;
        private System.Windows.Forms.Label lblAlignPre2;
        private System.Windows.Forms.Label lblAlignName2;
        private System.Windows.Forms.TextBox txtAlignBreak2OffsetA;
        private System.Windows.Forms.TextBox txtAlignBreak1OffsetA;
        private System.Windows.Forms.TextBox txtAlignPre1OffsetA;
        private System.Windows.Forms.Label lblAlignOffsetA1;
        private System.Windows.Forms.TextBox txtAlignBreak2OffsetY;
        private System.Windows.Forms.TextBox txtAlignBreak1OffsetY;
        private System.Windows.Forms.TextBox txtAlignPre1OffsetY;
        private System.Windows.Forms.Label lblAlignOffsetY1;
        private System.Windows.Forms.TextBox txtAlignBreak2OffsetX;
        private System.Windows.Forms.TextBox txtAlignBreak1OffsetX;
        private System.Windows.Forms.TextBox txtAlignPre1OffsetX;
        private System.Windows.Forms.Label lblAlignOffsetX1;
        private System.Windows.Forms.TextBox txtAlignBreak2Result;
        private System.Windows.Forms.TextBox txtAlignBreak1Result;
        private System.Windows.Forms.TextBox txtAlignPre1Result;
        private System.Windows.Forms.Label lblAlignResult1;
        private System.Windows.Forms.Label lblAlignBreak2;
        private System.Windows.Forms.Label lblAlignBreak1;
        private System.Windows.Forms.Label lblAlignPre1;
        private System.Windows.Forms.Label lblAlignName1;
        private System.Windows.Forms.Panel panel10;
        internal System.Windows.Forms.Label lbleEquipState;
        private Main.UcrlDetailTactView UcrlDtvAfterIRCutTransferB;
        private Main.UcrlDetailTactView UcrlDtvBreakStageB;
        private Main.UcrlDetailTactView UcrlDtvBeforeTransferB;
        private Main.UcrlDetailTactView UcrlDtvAfterInspTransferB;
        private Main.UcrlDetailTactView UcrlDtvUnloader;
        private Main.UcrlDetailTactView UcrlDtvCstUnloaderB;
        private Main.UcrlDetailTactView UcrlDtvInspStageA;
        private Main.UcrlDetailTactView UcrlDtvPreAlign;
        private Main.UcrlDetailTactView UcrlDtvBreakStageA;
        private Main.UcrlDetailTactView UcrlDtvInspStageB;
        private Main.UcrlDetailTactView UcrlDtvCstLoaderA;
        private Main.UcrlDetailTactView UcrlDtvLoaderTransferA;
        private Main.UcrlDetailTactView UcrlDtvIRCutStageA;
        private Main.UcrlDetailTactView UcrlDtvAfterInspTransferA;
        private Main.UcrlDetailTactView UcrlDtvCstUnloaderA;
        private Main.UcrlDetailTactView UcrlDtvCstLoaderB;
        private Main.UcrlDetailTactView UcrlDtvLoader;
        private Main.UcrlDetailTactView UcrlDtvLoaderTransferB;
        private Main.UcrlDetailTactView UcrlDtvIRCutStageB;
        private Main.UcrlDetailTactView UcrlDtvAfterIRCutTransferA;
        private Main.UcrlDetailTactView UcrlDtvBeforeTransferA;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnIonizerLoader;
        internal System.Windows.Forms.Label lblIonizer;
        private System.Windows.Forms.Button btnIonizerUnloader;
        private System.Windows.Forms.Button btnIonizerBreak;
        private System.Windows.Forms.Button btnIonizerProcess;
        private Main.UcrlDetailTactView UcrlDtvFineAlign;
        private Main.UcrlDetailTactView UcrlDtvBreakAlign;
        private Main.UcrlCstCellCount ucrlCstCellCountLDA;
        private Main.UcrlCstCellCount ucrlCstCellCountLDB;
        private Main.UcrlCstCellCount ucrlCstCellCountULDA;
        private Main.UcrlCstCellCount ucrlCstCellCountULDB;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.Button btnLaserCover;
        private System.Windows.Forms.Label lblLoaderBox;
        private System.Windows.Forms.Label lblEms3;
        private System.Windows.Forms.Label lblProcessBox;
        private System.Windows.Forms.Label lblEms5;
        private System.Windows.Forms.Label lblUnloaderBox;
        private System.Windows.Forms.Button btnACSTSkip;
        private System.Windows.Forms.Button btnBCSTSkip;
        private System.Windows.Forms.Label lblEms4;
        private System.Windows.Forms.Label lblZPosGap;
        private System.Windows.Forms.Label lblAvgPower;
        private System.Windows.Forms.Label lblEms1;
        private System.Windows.Forms.Label lblAvgPowerInfo;
        private System.Windows.Forms.Label lblTartget;
        private System.Windows.Forms.Label lblZPosGapInfo;
        private System.Windows.Forms.Label lblTargetInfo;
        private System.Windows.Forms.Button btnUldLight;
        private System.Windows.Forms.Button btnProcessLight;
        private System.Windows.Forms.Button btnLdLight;
        private System.Windows.Forms.Button btnDelayTopDoor9;
        private System.Windows.Forms.Button btnDelayTopDoor8;
        private System.Windows.Forms.Button btnDelayTopDoor7;
        private System.Windows.Forms.Button btnDelayTopDoor5;
        private System.Windows.Forms.Button btnDelayTopDoor6;
        private System.Windows.Forms.Button btnDelayTopDoor4;
        private System.Windows.Forms.Label lblShutterOpen;
        private System.Windows.Forms.Label lblLock;
        private System.Windows.Forms.Label lblEms2;
        private System.Windows.Forms.Label lblEms6;
        private System.Windows.Forms.Button btnDelayTopDoor3;
        private System.Windows.Forms.Button btnDelayTopDoor1;
        private System.Windows.Forms.Label lblGrabEms3;
        private System.Windows.Forms.Label lblGrabEms2;
        private System.Windows.Forms.Label lblGrabEms1;
        private System.Windows.Forms.Button btnDelayTopDoor11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnDelayTopDoor10;
        private System.Windows.Forms.Button btnDelayTopDoor2;
        private System.Windows.Forms.Label lblAutoTeach;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblLdMut;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnLoadingOutMuting;
        private System.Windows.Forms.Button btnLoadingInMuting;
        internal System.Windows.Forms.Label lblLoading;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnDummyTankSet;
        private System.Windows.Forms.Button btnDummyTankGet;
        internal System.Windows.Forms.Label lblDummyTank;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblCstTotalCount;
        private System.Windows.Forms.Label lblCstTotalCountInfo;
        private System.Windows.Forms.Label lblCstReadCount;
        private System.Windows.Forms.Label lblCstReadCountInfo;
        private System.Windows.Forms.Label lblCst;
        private System.Windows.Forms.Label lblDayTotalCount;
        private System.Windows.Forms.Label lblDayTotalCountInfo;
        private System.Windows.Forms.Label lblDayReadCount;
        private System.Windows.Forms.Label lblDayReadCountInfo;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lbCstTotalCount;
        private System.Windows.Forms.Label lb_cst_total_count_info;
        private System.Windows.Forms.Label lbCstReadCount;
        private System.Windows.Forms.Label lb_cst_read_count_info;
        private System.Windows.Forms.Label lb_cst;
        internal System.Windows.Forms.Label lblMcrCount;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnUnloadingOutMuting;
        private System.Windows.Forms.Button btnUnloadingInMuting;
        internal System.Windows.Forms.Label lblUnloading;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblBreakingCount;
        private System.Windows.Forms.Label lblBreakingCountInfo;
        private System.Windows.Forms.Button btnBreakingCountClear;
        internal System.Windows.Forms.Label lblNameBreakingCount;
    }
}

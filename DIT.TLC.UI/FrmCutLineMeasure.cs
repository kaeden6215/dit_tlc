﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmCutLineMeasure : Form
    {
        private string sRecpName = string.Empty;
        public FrmCutLineMeasure(string recpName)
        {
            InitializeComponent();
            sRecpName = recpName;

            FillCutLinePos();
        }

        private void FillCutLinePos()
        {
            lstAColL1.Items.Clear();
            lstAColL2.Items.Clear();
            lstBColR1.Items.Clear();
            lstBColR2.Items.Clear();

            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == sRecpName);
            for (int nAColL1PosCount = 0; nAColL1PosCount < recp.AColL1Pos.Count; nAColL1PosCount++)
                lstAColL1.Items.Add(new ListViewItem(new string[] { string.Format("{0}", recp.AColL1Pos[nAColL1PosCount].X), string.Format("{0}", recp.AColL1Pos[nAColL1PosCount].Y) }));
            for (int nAColL2PosCount = 0; nAColL2PosCount < recp.AColL2Pos.Count; nAColL2PosCount++)
                lstAColL2.Items.Add(new ListViewItem(new string[] { string.Format("{0}", recp.AColL2Pos[nAColL2PosCount].X), string.Format("{0}", recp.AColL2Pos[nAColL2PosCount].Y) }));
            for (int nBColR1PosCount = 0; nBColR1PosCount < recp.BColR1Pos.Count; nBColR1PosCount++)
                lstBColR1.Items.Add(new ListViewItem(new string[] { string.Format("{0}", recp.BColR1Pos[nBColR1PosCount].X), string.Format("{0}", recp.BColR1Pos[nBColR1PosCount].Y) }));
            for (int nBColR2PosCount = 0; nBColR2PosCount < recp.BColR2Pos.Count; nBColR2PosCount++)
                lstBColR2.Items.Add(new ListViewItem(new string[] { string.Format("{0}", recp.BColR2Pos[nBColR2PosCount].X), string.Format("{0}", recp.BColR2Pos[nBColR2PosCount].Y) }));

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (btnAColL1Add == (Button)sender)
            {
                lstAColL1.Items.Add(new ListViewItem(new string[] { string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition), string.Format("{0}", GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition) }));
            }
            else if (btnAColL2Add == (Button)sender)
            {
                lstAColL2.Items.Add(new ListViewItem(new string[] { string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition), string.Format("{0}", GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition) }));
            }
            else if (btnBColR1Add == (Button)sender)
            {
                lstBColR1.Items.Add(new ListViewItem(new string[] { string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition), string.Format("{0}", GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition) }));
            }
            else
            {
                lstBColR2.Items.Add(new ListViewItem(new string[] { string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition), string.Format("{0}", GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition) }));
            }

        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (btnAColL1Change == (Button)sender)
            {
                if (lstAColL1.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstAColL1.SelectedItems[0].Index;

                lstAColL1.Items[nSelectIdx].SubItems[0].Text = string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition);
                lstAColL1.Items[nSelectIdx].SubItems[1].Text = string.Format("{0}", GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition);
            }
            else if (btnAColL2Change == (Button)sender)
            {
                if (lstAColL2.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstAColL2.SelectedItems[0].Index;

                lstAColL2.Items[nSelectIdx].SubItems[0].Text = string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition);
                lstAColL2.Items[nSelectIdx].SubItems[1].Text = string.Format("{0}", GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition);
            }
            else if (btnBColR1Change == (Button)sender)
            {
                if (lstBColR1.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstBColR1.SelectedItems[0].Index;

                lstBColR1.Items[nSelectIdx].SubItems[0].Text = string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition);
                lstBColR1.Items[nSelectIdx].SubItems[1].Text = string.Format("{0}", GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition);
            }
            else
            {
                if (lstBColR2.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstBColR2.SelectedItems[0].Index;

                lstBColR2.Items[nSelectIdx].SubItems[0].Text = string.Format("{0}", GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition);
                lstBColR2.Items[nSelectIdx].SubItems[1].Text = string.Format("{0}", GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (btnAColL1Delete == (Button)sender)
            {
                if (lstAColL1.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstAColL1.SelectedItems[0].Index;

                lstAColL1.Items.RemoveAt(nSelectIdx);
            }
            else if (btnAColL2Delete == (Button)sender)
            {
                if (lstAColL2.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstAColL2.SelectedItems[0].Index;

                lstAColL2.Items.RemoveAt(nSelectIdx);
            }
            else if (btnBColR1Delete == (Button)sender)
            {
                if (lstBColR1.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstBColR1.SelectedItems[0].Index;

                lstBColR1.Items.RemoveAt(nSelectIdx);
            }
            else
            {
                if (lstBColR2.SelectedItems.Count <= 0) return;
                int nSelectIdx = lstBColR2.SelectedItems[0].Index;

                lstBColR2.Items.RemoveAt(nSelectIdx);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == sRecpName);
            UpdateCutLinePos(ref recp);
            GG.Equip.EqpRecipeMgr.Save();
        }

        public void UpdateCutLinePos(ref EqpRecipe recp)
        {
            recp.AColL1Pos.RemoveRange(0, recp.AColL1Pos.Count);
            recp.AColL2Pos.RemoveRange(0, recp.AColL2Pos.Count);
            recp.BColR1Pos.RemoveRange(0, recp.BColR1Pos.Count);
            recp.BColR2Pos.RemoveRange(0, recp.BColR2Pos.Count);

            #region UpdateCutLinePos
            //CutLinePos
            for (int nAColL1PosCount = 0; nAColL1PosCount < lstAColL1.Items.Count; nAColL1PosCount++)
            {
                if (lstAColL1.Items.Count > 0)
                    recp.AColL1Pos.Add(new PointD(double.Parse(lstAColL1.Items[nAColL1PosCount].SubItems[0].Text), double.Parse(lstAColL1.Items[nAColL1PosCount].SubItems[1].Text)));
            }
            for (int nAColL2PosCount = 0; nAColL2PosCount < lstAColL2.Items.Count; nAColL2PosCount++)
            {
                if (lstAColL2.Items.Count > 0)
                    recp.AColL2Pos.Add(new PointD(double.Parse(lstAColL2.Items[nAColL2PosCount].SubItems[0].Text), double.Parse(lstAColL2.Items[nAColL2PosCount].SubItems[1].Text)));
            }
            for (int nBColR1PosCount = 0; nBColR1PosCount < lstBColR1.Items.Count; nBColR1PosCount++)
            {
                if(lstBColR1.Items.Count >0)
                    recp.BColR1Pos.Add(new PointD(double.Parse(lstBColR1.Items[nBColR1PosCount].SubItems[0].Text), double.Parse(lstBColR1.Items[nBColR1PosCount].SubItems[1].Text)));
            }
            for (int nBColR2PosCount = 0; nBColR2PosCount < lstBColR2.Items.Count; nBColR2PosCount++)
            {
                if (lstBColR2.Items.Count > 0)
                    recp.BColR2Pos.Add(new PointD(double.Parse(lstBColR2.Items[nBColR2PosCount].SubItems[0].Text), double.Parse(lstBColR2.Items[nBColR2PosCount].SubItems[1].Text)));
            }
            #endregion
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

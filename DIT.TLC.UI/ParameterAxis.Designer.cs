﻿namespace DIT.TLC.UI
{
    partial class ParameterAxis
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tp_iostatus_ld = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Loader_AxisParameter = new System.Windows.Forms.Label();
            this.lbl_Loader_ServoOnLevel = new System.Windows.Forms.Label();
            this.tbox_Loader_Pulse = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Home_Signal = new System.Windows.Forms.Label();
            this.tbox_Loader_Unit = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Home_Vel1 = new System.Windows.Forms.Label();
            this.tbox_Loader_MinVelocitu = new System.Windows.Forms.TextBox();
            this.lbl_Loader_AxisNo = new System.Windows.Forms.Label();
            this.tbox_Loader_MaxVelocitu = new System.Windows.Forms.TextBox();
            this.lbl_Loader_PlusEndLimit = new System.Windows.Forms.Label();
            this.cbbox_Loader_EncoderType = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Vel2 = new System.Windows.Forms.Label();
            this.cbbox_Loader_AlarmResetLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_VelProfileMode = new System.Windows.Forms.Label();
            this.cbbox_Loader_StopLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_VelLast = new System.Windows.Forms.Label();
            this.cbbox_Loader_StopMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_EncInput = new System.Windows.Forms.Label();
            this.cbbox_Loader_ZPhase = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_PulseOutput = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_Offset = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Home = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_ClearTime = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MotionSignalSetting = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_Accelation2 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_AbsRelMode = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_Accelation1 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_InPosition = new System.Windows.Forms.Label();
            this.cbbox_Loader_Home_ZPhase = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Alarm = new System.Windows.Forms.Label();
            this.cbbox_Loader_Home_Direction = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Level = new System.Windows.Forms.Label();
            this.cbbox_Loader_SW_LimitEnable = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_SW_PlusLimit = new System.Windows.Forms.Label();
            this.cbbox_Loader_SW_LimitStopMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Vel3 = new System.Windows.Forms.Label();
            this.tbox_Loader_Init_Inposition = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW = new System.Windows.Forms.Label();
            this.tbox_Loader_Init_Decelation = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW_MinusLimit = new System.Windows.Forms.Label();
            this.tbox_Loader_Init_Accelation = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init = new System.Windows.Forms.Label();
            this.tbox_Loader_Init_AccelationTime = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW_LimitMode = new System.Windows.Forms.Label();
            this.tbox_Loader_Init_PtpSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MinusEndLimit = new System.Windows.Forms.Label();
            this.tbox_Loader_Init_Velocity = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init_Position = new System.Windows.Forms.Label();
            this.tbox_Loader_Init_Position = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init_Velocity = new System.Windows.Forms.Label();
            this.cbbox_Loader_SW_LimitMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Init_PtpSpeed = new System.Windows.Forms.Label();
            this.tbox_Loader_SW_PlusLimit = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init_AccelationTime = new System.Windows.Forms.Label();
            this.tbox_Loader_SW_MinusLimit = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Pulse = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_VelLast = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MinVelocitu = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_Vel3 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MaxVelocitu = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_Vel2 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Unit = new System.Windows.Forms.Label();
            this.tbox_Loader_Home_Vel1 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_EncoderType = new System.Windows.Forms.Label();
            this.cbbox_Loader_Home_Level = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_AlarmResetLevel = new System.Windows.Forms.Label();
            this.cbbox_Loader_Home_Signal = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_ZPhase = new System.Windows.Forms.Label();
            this.cbbox_Loader_ServoOnLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_StopMode = new System.Windows.Forms.Label();
            this.cbbox_Loader_PlusEndLimit = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_StopLevel = new System.Windows.Forms.Label();
            this.cbbox_Loader_MinusEndLimit = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Direction = new System.Windows.Forms.Label();
            this.cbbox_Loader_Alarm = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Accelation1 = new System.Windows.Forms.Label();
            this.cbbox_Loader_Inposition = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Accelation2 = new System.Windows.Forms.Label();
            this.cbbox_Loader_AbsRelMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Offset = new System.Windows.Forms.Label();
            this.cbbox_Loader_EncInput = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_ZPhase = new System.Windows.Forms.Label();
            this.cbbox_Loader_PulseOutput = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_ClearTime = new System.Windows.Forms.Label();
            this.cbbox_Loader_VelProfileMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_SW_LimitEnable = new System.Windows.Forms.Label();
            this.tbox_Loader_AxisNo = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW_LimitStopMode = new System.Windows.Forms.Label();
            this.lbl_Loader_Init_InPosition = new System.Windows.Forms.Label();
            this.lbl_Loader_Init_Accelation = new System.Windows.Forms.Label();
            this.lbl_Loader_Init_Decelation = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pn_Loader_15 = new System.Windows.Forms.Panel();
            this.lbl_Loader_15 = new System.Windows.Forms.Label();
            this.pn_Loader_14 = new System.Windows.Forms.Panel();
            this.lbl_Loader_14 = new System.Windows.Forms.Label();
            this.pn_Loader_13 = new System.Windows.Forms.Panel();
            this.lbl_Loader_13 = new System.Windows.Forms.Label();
            this.pn_Loader_12 = new System.Windows.Forms.Panel();
            this.lbl_Loader_12 = new System.Windows.Forms.Label();
            this.pn_Loader_11 = new System.Windows.Forms.Panel();
            this.lbl_Loader_11 = new System.Windows.Forms.Label();
            this.pn_Loader_10 = new System.Windows.Forms.Panel();
            this.lbl_Loader_10 = new System.Windows.Forms.Label();
            this.pn_Loader_09 = new System.Windows.Forms.Panel();
            this.lbl_Loader_09 = new System.Windows.Forms.Label();
            this.pn_Loader_08 = new System.Windows.Forms.Panel();
            this.lbl_Loader_08 = new System.Windows.Forms.Label();
            this.pn_Loader_07 = new System.Windows.Forms.Panel();
            this.lbl_Loader_07 = new System.Windows.Forms.Label();
            this.pn_Loader_06 = new System.Windows.Forms.Panel();
            this.lbl_Loader_06 = new System.Windows.Forms.Label();
            this.pn_Loader_05 = new System.Windows.Forms.Panel();
            this.lbl_Loader_05 = new System.Windows.Forms.Label();
            this.pn_Loader_04 = new System.Windows.Forms.Panel();
            this.lbl_Loader_04 = new System.Windows.Forms.Label();
            this.pn_Loader_36 = new System.Windows.Forms.Panel();
            this.lbl_Loader_36 = new System.Windows.Forms.Label();
            this.pn_Loader_35 = new System.Windows.Forms.Panel();
            this.lbl_Loader_35 = new System.Windows.Forms.Label();
            this.pn_Loader_33 = new System.Windows.Forms.Panel();
            this.lbl_Loader_33 = new System.Windows.Forms.Label();
            this.pn_Loader_32 = new System.Windows.Forms.Panel();
            this.lbl_Loader_32 = new System.Windows.Forms.Label();
            this.pn_Loader_31 = new System.Windows.Forms.Panel();
            this.lbl_Loader_31 = new System.Windows.Forms.Label();
            this.tp_iostatus_process = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_Process_AxisParameter = new System.Windows.Forms.Label();
            this.tbox_Process_AccelationTime = new System.Windows.Forms.TextBox();
            this.lbl_Process_JogSpeed = new System.Windows.Forms.Label();
            this.tbox_Process_SWLimitHigh = new System.Windows.Forms.TextBox();
            this.lbl_Process_PtpSpeed = new System.Windows.Forms.Label();
            this.tbox_Process_SWLimitLow = new System.Windows.Forms.TextBox();
            this.lbl_Process_SWLimitHigh = new System.Windows.Forms.Label();
            this.tbox_Process_PtpSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Process_AxisNo = new System.Windows.Forms.Label();
            this.tbox_Process_StepSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Process_HomeAcceleration = new System.Windows.Forms.Label();
            this.tbox_Process_JogSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Process_AccelationTime = new System.Windows.Forms.Label();
            this.tbox_Process_HomeAcceleration = new System.Windows.Forms.TextBox();
            this.lbl_Process_InPosition = new System.Windows.Forms.Label();
            this.tbox_Process_Home2Velocity = new System.Windows.Forms.TextBox();
            this.lbl_Process_SpeedRate = new System.Windows.Forms.Label();
            this.tbox_Process_Home1Velocity = new System.Windows.Forms.TextBox();
            this.lbl_Process_PositionRate = new System.Windows.Forms.Label();
            this.tbox_Process_DefaultAcceleration = new System.Windows.Forms.TextBox();
            this.lbl_Process_StepSpeed = new System.Windows.Forms.Label();
            this.tbox_Process_DefauleVelocity = new System.Windows.Forms.TextBox();
            this.lbl_Process_DefauleVelocity = new System.Windows.Forms.Label();
            this.tbox_Process_InPosition = new System.Windows.Forms.TextBox();
            this.lbl_Process_MoveTimeOut = new System.Windows.Forms.Label();
            this.tbox_Process_MoveTimeOut = new System.Windows.Forms.TextBox();
            this.lbl_Process_DefaultAcceleration = new System.Windows.Forms.Label();
            this.tbox_Process_SpeedRate = new System.Windows.Forms.TextBox();
            this.lbl_Process_Home1Velocity = new System.Windows.Forms.Label();
            this.tbox_Process_PositionRate = new System.Windows.Forms.TextBox();
            this.lbl_Process_SWLimitLow = new System.Windows.Forms.Label();
            this.tbox_Process_AxisNo = new System.Windows.Forms.TextBox();
            this.lbl_Process_Home2Velocity = new System.Windows.Forms.Label();
            this.tp_iostatus_uld = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Save = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pn_Process_19 = new System.Windows.Forms.Panel();
            this.lbl_Process_19 = new System.Windows.Forms.Label();
            this.pn_Process_18 = new System.Windows.Forms.Panel();
            this.lbl_Process_18 = new System.Windows.Forms.Label();
            this.pn_Process_14 = new System.Windows.Forms.Panel();
            this.lbl_Process_14 = new System.Windows.Forms.Label();
            this.pn_Process_13 = new System.Windows.Forms.Panel();
            this.lbl_Process_13 = new System.Windows.Forms.Label();
            this.pn_Process_12 = new System.Windows.Forms.Panel();
            this.lbl_Process_12 = new System.Windows.Forms.Label();
            this.pn_Process_11 = new System.Windows.Forms.Panel();
            this.lbl_Process_11 = new System.Windows.Forms.Label();
            this.pn_Process_10 = new System.Windows.Forms.Panel();
            this.lbl_Process_10 = new System.Windows.Forms.Label();
            this.pn_Process_09 = new System.Windows.Forms.Panel();
            this.lbl_Process_09 = new System.Windows.Forms.Label();
            this.pn_Process_22 = new System.Windows.Forms.Panel();
            this.lbl_Process_22 = new System.Windows.Forms.Label();
            this.pn_Process_07 = new System.Windows.Forms.Panel();
            this.lbl_Process_07 = new System.Windows.Forms.Label();
            this.pn_Process_06 = new System.Windows.Forms.Panel();
            this.lbl_Process_06 = new System.Windows.Forms.Label();
            this.pn_Process_05 = new System.Windows.Forms.Panel();
            this.lbl_Process_05 = new System.Windows.Forms.Label();
            this.pn_Process_04 = new System.Windows.Forms.Panel();
            this.lbl_Process_04 = new System.Windows.Forms.Label();
            this.pn_Process_17 = new System.Windows.Forms.Panel();
            this.lbl_Process_17 = new System.Windows.Forms.Label();
            this.pn_Process_03 = new System.Windows.Forms.Panel();
            this.lbl_Process_03 = new System.Windows.Forms.Label();
            this.pn_Process_02 = new System.Windows.Forms.Panel();
            this.lbl_Process_02 = new System.Windows.Forms.Label();
            this.pn_Process_01 = new System.Windows.Forms.Panel();
            this.lbl_Process_01 = new System.Windows.Forms.Label();
            this.pn_Process_20 = new System.Windows.Forms.Panel();
            this.lbl_Process_20 = new System.Windows.Forms.Label();
            this.pn_Process_21 = new System.Windows.Forms.Panel();
            this.lbl_Process_21 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.pn_Unloader_37 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_37 = new System.Windows.Forms.Label();
            this.pn_Unloader_2 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_2 = new System.Windows.Forms.Label();
            this.pn_Unloader_1 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_1 = new System.Windows.Forms.Label();
            this.pn_Unloader_38 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_38 = new System.Windows.Forms.Label();
            this.pn_Unloader_39 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_39 = new System.Windows.Forms.Label();
            this.pn_Unloader_40 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_40 = new System.Windows.Forms.Label();
            this.pn_Unloader_41 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_41 = new System.Windows.Forms.Label();
            this.pn_Unloader_16 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_16 = new System.Windows.Forms.Label();
            this.pn_Unloader_17 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_17 = new System.Windows.Forms.Label();
            this.pn_Unloader_18 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_18 = new System.Windows.Forms.Label();
            this.pn_Unloader_19 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_19 = new System.Windows.Forms.Label();
            this.pn_Unloader_20 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_20 = new System.Windows.Forms.Label();
            this.pn_Unloader_21 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_21 = new System.Windows.Forms.Label();
            this.pn_Unloader_23 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_23 = new System.Windows.Forms.Label();
            this.pn_Unloader_24 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_24 = new System.Windows.Forms.Label();
            this.pn_Unloader_26 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_26 = new System.Windows.Forms.Label();
            this.pn_Unloader_27 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_27 = new System.Windows.Forms.Label();
            this.pn_Unloader_28 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_28 = new System.Windows.Forms.Label();
            this.pn_Unloader_29 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_29 = new System.Windows.Forms.Label();
            this.pn_Unloader_30 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_30 = new System.Windows.Forms.Label();
            this.pn_Unloader_0 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_0 = new System.Windows.Forms.Label();
            this.pn_Unloader_3 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_3 = new System.Windows.Forms.Label();
            this.lbl_Unloader_AxisParameter = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Pulse = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Unit = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbox_Unloader_MinVelocitu = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_AxisNo = new System.Windows.Forms.Label();
            this.tbox_Unloader_MaxVelocitu = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_EncoderType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_AlarmResetLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Unloader_VelProfileMode = new System.Windows.Forms.Label();
            this.cbbox_Unloader_StopLevel = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_StopMode = new System.Windows.Forms.ComboBox();
            this.lbl_Unloader_EncInput = new System.Windows.Forms.Label();
            this.cbbox_Unloader_ZPhase = new System.Windows.Forms.ComboBox();
            this.lbl_Unloader_PulseOutput = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Home_ClearTime = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_MotionSignalSetting = new System.Windows.Forms.Label();
            this.tbox_Unloader_Home_Accelation2 = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_AbsRelMode = new System.Windows.Forms.Label();
            this.tbox_Unloader_Home_Accelation1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_Home_ZPhase = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_Home_Direction = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_SW_LimitEnable = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_SW_LimitStopMode = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Init_InPosition = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Init_Decelation = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Init_Accelation = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Init_AccelationTime = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Init_PtpSpeed = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Init_Velocity = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Init_Position = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_SW_LimitMode = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbox_Unloader_SW_PlusLimit = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbox_Unloader_SW_MinusLimit = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.lbl_Unloader_MinVelocitu = new System.Windows.Forms.Label();
            this.tbox_Unloader_Home_Vel3 = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_MaxVelocitu = new System.Windows.Forms.Label();
            this.tbox_Unloader_Home_Vel2 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbox_Unloader_Home_Vel1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_Home_Level = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_Home_Signal = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_ServoOnLevel = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_PlusEndLimit = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_MinusEndLismit = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_Alarm = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_Inposition = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_AbsRelMode = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_EncInput = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_PulseOutput = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.cbbox_Unloader_VelProfileMode = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tbox_Unloader_AxisNo = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tbox_Unloader_Home_Offset = new System.Windows.Forms.TextBox();
            this.tbox_Unloader_Home_VelLast = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.tc_iostatus_info.SuspendLayout();
            this.tp_iostatus_ld.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pn_Loader_15.SuspendLayout();
            this.pn_Loader_14.SuspendLayout();
            this.pn_Loader_13.SuspendLayout();
            this.pn_Loader_12.SuspendLayout();
            this.pn_Loader_11.SuspendLayout();
            this.pn_Loader_10.SuspendLayout();
            this.pn_Loader_09.SuspendLayout();
            this.pn_Loader_08.SuspendLayout();
            this.pn_Loader_07.SuspendLayout();
            this.pn_Loader_06.SuspendLayout();
            this.pn_Loader_05.SuspendLayout();
            this.pn_Loader_04.SuspendLayout();
            this.pn_Loader_36.SuspendLayout();
            this.pn_Loader_35.SuspendLayout();
            this.pn_Loader_33.SuspendLayout();
            this.pn_Loader_32.SuspendLayout();
            this.pn_Loader_31.SuspendLayout();
            this.tp_iostatus_process.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tp_iostatus_uld.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.pn_Process_19.SuspendLayout();
            this.pn_Process_18.SuspendLayout();
            this.pn_Process_14.SuspendLayout();
            this.pn_Process_13.SuspendLayout();
            this.pn_Process_12.SuspendLayout();
            this.pn_Process_11.SuspendLayout();
            this.pn_Process_10.SuspendLayout();
            this.pn_Process_09.SuspendLayout();
            this.pn_Process_22.SuspendLayout();
            this.pn_Process_07.SuspendLayout();
            this.pn_Process_06.SuspendLayout();
            this.pn_Process_05.SuspendLayout();
            this.pn_Process_04.SuspendLayout();
            this.pn_Process_17.SuspendLayout();
            this.pn_Process_03.SuspendLayout();
            this.pn_Process_02.SuspendLayout();
            this.pn_Process_01.SuspendLayout();
            this.pn_Process_20.SuspendLayout();
            this.pn_Process_21.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.pn_Unloader_37.SuspendLayout();
            this.pn_Unloader_2.SuspendLayout();
            this.pn_Unloader_1.SuspendLayout();
            this.pn_Unloader_38.SuspendLayout();
            this.pn_Unloader_39.SuspendLayout();
            this.pn_Unloader_40.SuspendLayout();
            this.pn_Unloader_41.SuspendLayout();
            this.pn_Unloader_16.SuspendLayout();
            this.pn_Unloader_17.SuspendLayout();
            this.pn_Unloader_18.SuspendLayout();
            this.pn_Unloader_19.SuspendLayout();
            this.pn_Unloader_20.SuspendLayout();
            this.pn_Unloader_21.SuspendLayout();
            this.pn_Unloader_23.SuspendLayout();
            this.pn_Unloader_24.SuspendLayout();
            this.pn_Unloader_26.SuspendLayout();
            this.pn_Unloader_27.SuspendLayout();
            this.pn_Unloader_28.SuspendLayout();
            this.pn_Unloader_29.SuspendLayout();
            this.pn_Unloader_30.SuspendLayout();
            this.pn_Unloader_0.SuspendLayout();
            this.pn_Unloader_3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_ld);
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_process);
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_uld);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(577, 30);
            this.tc_iostatus_info.Location = new System.Drawing.Point(3, 10);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1734, 805);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 1;
            // 
            // tp_iostatus_ld
            // 
            this.tp_iostatus_ld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_iostatus_ld.Controls.Add(this.panel1);
            this.tp_iostatus_ld.Controls.Add(this.tableLayoutPanel1);
            this.tp_iostatus_ld.Font = new System.Drawing.Font("굴림", 20F);
            this.tp_iostatus_ld.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld.Name = "tp_iostatus_ld";
            this.tp_iostatus_ld.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld.Size = new System.Drawing.Size(1726, 767);
            this.tp_iostatus_ld.TabIndex = 0;
            this.tp_iostatus_ld.Text = "로더";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.lbl_Loader_AxisParameter);
            this.panel1.Controls.Add(this.lbl_Loader_ServoOnLevel);
            this.panel1.Controls.Add(this.tbox_Loader_Pulse);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Signal);
            this.panel1.Controls.Add(this.tbox_Loader_Unit);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Vel1);
            this.panel1.Controls.Add(this.tbox_Loader_MinVelocitu);
            this.panel1.Controls.Add(this.lbl_Loader_AxisNo);
            this.panel1.Controls.Add(this.tbox_Loader_MaxVelocitu);
            this.panel1.Controls.Add(this.lbl_Loader_PlusEndLimit);
            this.panel1.Controls.Add(this.cbbox_Loader_EncoderType);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Vel2);
            this.panel1.Controls.Add(this.cbbox_Loader_AlarmResetLevel);
            this.panel1.Controls.Add(this.lbl_Loader_VelProfileMode);
            this.panel1.Controls.Add(this.cbbox_Loader_StopLevel);
            this.panel1.Controls.Add(this.lbl_Loader_Home_VelLast);
            this.panel1.Controls.Add(this.cbbox_Loader_StopMode);
            this.panel1.Controls.Add(this.lbl_Loader_EncInput);
            this.panel1.Controls.Add(this.cbbox_Loader_ZPhase);
            this.panel1.Controls.Add(this.lbl_Loader_PulseOutput);
            this.panel1.Controls.Add(this.tbox_Loader_Home_Offset);
            this.panel1.Controls.Add(this.lbl_Loader_Home);
            this.panel1.Controls.Add(this.tbox_Loader_Home_ClearTime);
            this.panel1.Controls.Add(this.lbl_Loader_MotionSignalSetting);
            this.panel1.Controls.Add(this.tbox_Loader_Home_Accelation2);
            this.panel1.Controls.Add(this.lbl_Loader_AbsRelMode);
            this.panel1.Controls.Add(this.tbox_Loader_Home_Accelation1);
            this.panel1.Controls.Add(this.lbl_Loader_InPosition);
            this.panel1.Controls.Add(this.cbbox_Loader_Home_ZPhase);
            this.panel1.Controls.Add(this.lbl_Loader_Alarm);
            this.panel1.Controls.Add(this.cbbox_Loader_Home_Direction);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Level);
            this.panel1.Controls.Add(this.cbbox_Loader_SW_LimitEnable);
            this.panel1.Controls.Add(this.lbl_Loader_SW_PlusLimit);
            this.panel1.Controls.Add(this.cbbox_Loader_SW_LimitStopMode);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Vel3);
            this.panel1.Controls.Add(this.tbox_Loader_Init_Inposition);
            this.panel1.Controls.Add(this.lbl_Loader_SW);
            this.panel1.Controls.Add(this.tbox_Loader_Init_Decelation);
            this.panel1.Controls.Add(this.lbl_Loader_SW_MinusLimit);
            this.panel1.Controls.Add(this.tbox_Loader_Init_Accelation);
            this.panel1.Controls.Add(this.lbl_Loader_Init);
            this.panel1.Controls.Add(this.tbox_Loader_Init_AccelationTime);
            this.panel1.Controls.Add(this.lbl_Loader_SW_LimitMode);
            this.panel1.Controls.Add(this.tbox_Loader_Init_PtpSpeed);
            this.panel1.Controls.Add(this.lbl_Loader_MinusEndLimit);
            this.panel1.Controls.Add(this.tbox_Loader_Init_Velocity);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Position);
            this.panel1.Controls.Add(this.tbox_Loader_Init_Position);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Velocity);
            this.panel1.Controls.Add(this.cbbox_Loader_SW_LimitMode);
            this.panel1.Controls.Add(this.lbl_Loader_Init_PtpSpeed);
            this.panel1.Controls.Add(this.tbox_Loader_SW_PlusLimit);
            this.panel1.Controls.Add(this.lbl_Loader_Init_AccelationTime);
            this.panel1.Controls.Add(this.tbox_Loader_SW_MinusLimit);
            this.panel1.Controls.Add(this.lbl_Loader_Pulse);
            this.panel1.Controls.Add(this.tbox_Loader_Home_VelLast);
            this.panel1.Controls.Add(this.lbl_Loader_MinVelocitu);
            this.panel1.Controls.Add(this.tbox_Loader_Home_Vel3);
            this.panel1.Controls.Add(this.lbl_Loader_MaxVelocitu);
            this.panel1.Controls.Add(this.tbox_Loader_Home_Vel2);
            this.panel1.Controls.Add(this.lbl_Loader_Unit);
            this.panel1.Controls.Add(this.tbox_Loader_Home_Vel1);
            this.panel1.Controls.Add(this.lbl_Loader_EncoderType);
            this.panel1.Controls.Add(this.cbbox_Loader_Home_Level);
            this.panel1.Controls.Add(this.lbl_Loader_AlarmResetLevel);
            this.panel1.Controls.Add(this.cbbox_Loader_Home_Signal);
            this.panel1.Controls.Add(this.lbl_Loader_ZPhase);
            this.panel1.Controls.Add(this.cbbox_Loader_ServoOnLevel);
            this.panel1.Controls.Add(this.lbl_Loader_StopMode);
            this.panel1.Controls.Add(this.cbbox_Loader_PlusEndLimit);
            this.panel1.Controls.Add(this.lbl_Loader_StopLevel);
            this.panel1.Controls.Add(this.cbbox_Loader_MinusEndLimit);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Direction);
            this.panel1.Controls.Add(this.cbbox_Loader_Alarm);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Accelation1);
            this.panel1.Controls.Add(this.cbbox_Loader_Inposition);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Accelation2);
            this.panel1.Controls.Add(this.cbbox_Loader_AbsRelMode);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Offset);
            this.panel1.Controls.Add(this.cbbox_Loader_EncInput);
            this.panel1.Controls.Add(this.lbl_Loader_Home_ZPhase);
            this.panel1.Controls.Add(this.cbbox_Loader_PulseOutput);
            this.panel1.Controls.Add(this.lbl_Loader_Home_ClearTime);
            this.panel1.Controls.Add(this.cbbox_Loader_VelProfileMode);
            this.panel1.Controls.Add(this.lbl_Loader_SW_LimitEnable);
            this.panel1.Controls.Add(this.tbox_Loader_AxisNo);
            this.panel1.Controls.Add(this.lbl_Loader_SW_LimitStopMode);
            this.panel1.Controls.Add(this.lbl_Loader_Init_InPosition);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Accelation);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Decelation);
            this.panel1.Location = new System.Drawing.Point(881, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(834, 745);
            this.panel1.TabIndex = 0;
            // 
            // lbl_Loader_AxisParameter
            // 
            this.lbl_Loader_AxisParameter.AutoSize = true;
            this.lbl_Loader_AxisParameter.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AxisParameter.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AxisParameter.Location = new System.Drawing.Point(31, 10);
            this.lbl_Loader_AxisParameter.Name = "lbl_Loader_AxisParameter";
            this.lbl_Loader_AxisParameter.Size = new System.Drawing.Size(125, 23);
            this.lbl_Loader_AxisParameter.TabIndex = 0;
            this.lbl_Loader_AxisParameter.Text = "Axis Parameter";
            // 
            // lbl_Loader_ServoOnLevel
            // 
            this.lbl_Loader_ServoOnLevel.AutoSize = true;
            this.lbl_Loader_ServoOnLevel.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_ServoOnLevel.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_ServoOnLevel.Location = new System.Drawing.Point(31, 274);
            this.lbl_Loader_ServoOnLevel.Name = "lbl_Loader_ServoOnLevel";
            this.lbl_Loader_ServoOnLevel.Size = new System.Drawing.Size(135, 23);
            this.lbl_Loader_ServoOnLevel.TabIndex = 1;
            this.lbl_Loader_ServoOnLevel.Text = "Servo On Level :";
            // 
            // tbox_Loader_Pulse
            // 
            this.tbox_Loader_Pulse.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Pulse.Location = new System.Drawing.Point(672, 135);
            this.tbox_Loader_Pulse.Name = "tbox_Loader_Pulse";
            this.tbox_Loader_Pulse.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Pulse.TabIndex = 90;
            // 
            // lbl_Loader_Home_Signal
            // 
            this.lbl_Loader_Home_Signal.AutoSize = true;
            this.lbl_Loader_Home_Signal.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Signal.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Signal.Location = new System.Drawing.Point(31, 322);
            this.lbl_Loader_Home_Signal.Name = "lbl_Loader_Home_Signal";
            this.lbl_Loader_Home_Signal.Size = new System.Drawing.Size(118, 23);
            this.lbl_Loader_Home_Signal.TabIndex = 2;
            this.lbl_Loader_Home_Signal.Text = "Home Signal :";
            // 
            // tbox_Loader_Unit
            // 
            this.tbox_Loader_Unit.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Unit.Location = new System.Drawing.Point(672, 110);
            this.tbox_Loader_Unit.Name = "tbox_Loader_Unit";
            this.tbox_Loader_Unit.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Unit.TabIndex = 89;
            // 
            // lbl_Loader_Home_Vel1
            // 
            this.lbl_Loader_Home_Vel1.AutoSize = true;
            this.lbl_Loader_Home_Vel1.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Vel1.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Vel1.Location = new System.Drawing.Point(31, 370);
            this.lbl_Loader_Home_Vel1.Name = "lbl_Loader_Home_Vel1";
            this.lbl_Loader_Home_Vel1.Size = new System.Drawing.Size(124, 23);
            this.lbl_Loader_Home_Vel1.TabIndex = 3;
            this.lbl_Loader_Home_Vel1.Text = "Home Vel 1st :";
            // 
            // tbox_Loader_MinVelocitu
            // 
            this.tbox_Loader_MinVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_MinVelocitu.Location = new System.Drawing.Point(672, 85);
            this.tbox_Loader_MinVelocitu.Name = "tbox_Loader_MinVelocitu";
            this.tbox_Loader_MinVelocitu.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_MinVelocitu.TabIndex = 88;
            // 
            // lbl_Loader_AxisNo
            // 
            this.lbl_Loader_AxisNo.AutoSize = true;
            this.lbl_Loader_AxisNo.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AxisNo.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AxisNo.Location = new System.Drawing.Point(31, 34);
            this.lbl_Loader_AxisNo.Name = "lbl_Loader_AxisNo";
            this.lbl_Loader_AxisNo.Size = new System.Drawing.Size(67, 23);
            this.lbl_Loader_AxisNo.TabIndex = 4;
            this.lbl_Loader_AxisNo.Text = "AxisNo.";
            // 
            // tbox_Loader_MaxVelocitu
            // 
            this.tbox_Loader_MaxVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_MaxVelocitu.Location = new System.Drawing.Point(672, 60);
            this.tbox_Loader_MaxVelocitu.Name = "tbox_Loader_MaxVelocitu";
            this.tbox_Loader_MaxVelocitu.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_MaxVelocitu.TabIndex = 87;
            // 
            // lbl_Loader_PlusEndLimit
            // 
            this.lbl_Loader_PlusEndLimit.AutoSize = true;
            this.lbl_Loader_PlusEndLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_PlusEndLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_PlusEndLimit.Location = new System.Drawing.Point(31, 250);
            this.lbl_Loader_PlusEndLimit.Name = "lbl_Loader_PlusEndLimit";
            this.lbl_Loader_PlusEndLimit.Size = new System.Drawing.Size(110, 23);
            this.lbl_Loader_PlusEndLimit.TabIndex = 5;
            this.lbl_Loader_PlusEndLimit.Text = "+ End Limit :";
            // 
            // cbbox_Loader_EncoderType
            // 
            this.cbbox_Loader_EncoderType.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_EncoderType.FormattingEnabled = true;
            this.cbbox_Loader_EncoderType.Location = new System.Drawing.Point(672, 279);
            this.cbbox_Loader_EncoderType.Name = "cbbox_Loader_EncoderType";
            this.cbbox_Loader_EncoderType.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_EncoderType.TabIndex = 86;
            // 
            // lbl_Loader_Home_Vel2
            // 
            this.lbl_Loader_Home_Vel2.AutoSize = true;
            this.lbl_Loader_Home_Vel2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Vel2.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Vel2.Location = new System.Drawing.Point(31, 394);
            this.lbl_Loader_Home_Vel2.Name = "lbl_Loader_Home_Vel2";
            this.lbl_Loader_Home_Vel2.Size = new System.Drawing.Size(131, 23);
            this.lbl_Loader_Home_Vel2.TabIndex = 6;
            this.lbl_Loader_Home_Vel2.Text = "Home Vel 2nd :";
            // 
            // cbbox_Loader_AlarmResetLevel
            // 
            this.cbbox_Loader_AlarmResetLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_AlarmResetLevel.FormattingEnabled = true;
            this.cbbox_Loader_AlarmResetLevel.Location = new System.Drawing.Point(672, 255);
            this.cbbox_Loader_AlarmResetLevel.Name = "cbbox_Loader_AlarmResetLevel";
            this.cbbox_Loader_AlarmResetLevel.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_AlarmResetLevel.TabIndex = 85;
            // 
            // lbl_Loader_VelProfileMode
            // 
            this.lbl_Loader_VelProfileMode.AutoSize = true;
            this.lbl_Loader_VelProfileMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_VelProfileMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_VelProfileMode.Location = new System.Drawing.Point(31, 130);
            this.lbl_Loader_VelProfileMode.Name = "lbl_Loader_VelProfileMode";
            this.lbl_Loader_VelProfileMode.Size = new System.Drawing.Size(149, 23);
            this.lbl_Loader_VelProfileMode.TabIndex = 7;
            this.lbl_Loader_VelProfileMode.Text = "Vel Profile Mode :";
            // 
            // cbbox_Loader_StopLevel
            // 
            this.cbbox_Loader_StopLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_StopLevel.FormattingEnabled = true;
            this.cbbox_Loader_StopLevel.Location = new System.Drawing.Point(672, 231);
            this.cbbox_Loader_StopLevel.Name = "cbbox_Loader_StopLevel";
            this.cbbox_Loader_StopLevel.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_StopLevel.TabIndex = 84;
            // 
            // lbl_Loader_Home_VelLast
            // 
            this.lbl_Loader_Home_VelLast.AutoSize = true;
            this.lbl_Loader_Home_VelLast.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_VelLast.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_VelLast.Location = new System.Drawing.Point(31, 442);
            this.lbl_Loader_Home_VelLast.Name = "lbl_Loader_Home_VelLast";
            this.lbl_Loader_Home_VelLast.Size = new System.Drawing.Size(132, 23);
            this.lbl_Loader_Home_VelLast.TabIndex = 8;
            this.lbl_Loader_Home_VelLast.Text = "Home Vel Last :";
            // 
            // cbbox_Loader_StopMode
            // 
            this.cbbox_Loader_StopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_StopMode.FormattingEnabled = true;
            this.cbbox_Loader_StopMode.Location = new System.Drawing.Point(672, 207);
            this.cbbox_Loader_StopMode.Name = "cbbox_Loader_StopMode";
            this.cbbox_Loader_StopMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_StopMode.TabIndex = 83;
            // 
            // lbl_Loader_EncInput
            // 
            this.lbl_Loader_EncInput.AutoSize = true;
            this.lbl_Loader_EncInput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_EncInput.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_EncInput.Location = new System.Drawing.Point(31, 82);
            this.lbl_Loader_EncInput.Name = "lbl_Loader_EncInput";
            this.lbl_Loader_EncInput.Size = new System.Drawing.Size(98, 23);
            this.lbl_Loader_EncInput.TabIndex = 9;
            this.lbl_Loader_EncInput.Text = "Enc. Input :";
            // 
            // cbbox_Loader_ZPhase
            // 
            this.cbbox_Loader_ZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_ZPhase.FormattingEnabled = true;
            this.cbbox_Loader_ZPhase.Location = new System.Drawing.Point(672, 183);
            this.cbbox_Loader_ZPhase.Name = "cbbox_Loader_ZPhase";
            this.cbbox_Loader_ZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_ZPhase.TabIndex = 82;
            // 
            // lbl_Loader_PulseOutput
            // 
            this.lbl_Loader_PulseOutput.AutoSize = true;
            this.lbl_Loader_PulseOutput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_PulseOutput.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_PulseOutput.Location = new System.Drawing.Point(31, 58);
            this.lbl_Loader_PulseOutput.Name = "lbl_Loader_PulseOutput";
            this.lbl_Loader_PulseOutput.Size = new System.Drawing.Size(121, 23);
            this.lbl_Loader_PulseOutput.TabIndex = 10;
            this.lbl_Loader_PulseOutput.Text = "Pulse Output :";
            // 
            // tbox_Loader_Home_Offset
            // 
            this.tbox_Loader_Home_Offset.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_Offset.Location = new System.Drawing.Point(672, 448);
            this.tbox_Loader_Home_Offset.Name = "tbox_Loader_Home_Offset";
            this.tbox_Loader_Home_Offset.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_Offset.TabIndex = 81;
            // 
            // lbl_Loader_Home
            // 
            this.lbl_Loader_Home.AutoSize = true;
            this.lbl_Loader_Home.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home.Location = new System.Drawing.Point(31, 298);
            this.lbl_Loader_Home.Name = "lbl_Loader_Home";
            this.lbl_Loader_Home.Size = new System.Drawing.Size(179, 23);
            this.lbl_Loader_Home.TabIndex = 11;
            this.lbl_Loader_Home.Text = "Home  Search Setting";
            // 
            // tbox_Loader_Home_ClearTime
            // 
            this.tbox_Loader_Home_ClearTime.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_ClearTime.Location = new System.Drawing.Point(672, 423);
            this.tbox_Loader_Home_ClearTime.Name = "tbox_Loader_Home_ClearTime";
            this.tbox_Loader_Home_ClearTime.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_ClearTime.TabIndex = 80;
            // 
            // lbl_Loader_MotionSignalSetting
            // 
            this.lbl_Loader_MotionSignalSetting.AutoSize = true;
            this.lbl_Loader_MotionSignalSetting.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MotionSignalSetting.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MotionSignalSetting.Location = new System.Drawing.Point(31, 154);
            this.lbl_Loader_MotionSignalSetting.Name = "lbl_Loader_MotionSignalSetting";
            this.lbl_Loader_MotionSignalSetting.Size = new System.Drawing.Size(178, 23);
            this.lbl_Loader_MotionSignalSetting.TabIndex = 12;
            this.lbl_Loader_MotionSignalSetting.Text = "Motion Signal Setting";
            // 
            // tbox_Loader_Home_Accelation2
            // 
            this.tbox_Loader_Home_Accelation2.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_Accelation2.Location = new System.Drawing.Point(672, 398);
            this.tbox_Loader_Home_Accelation2.Name = "tbox_Loader_Home_Accelation2";
            this.tbox_Loader_Home_Accelation2.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_Accelation2.TabIndex = 79;
            // 
            // lbl_Loader_AbsRelMode
            // 
            this.lbl_Loader_AbsRelMode.AutoSize = true;
            this.lbl_Loader_AbsRelMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AbsRelMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AbsRelMode.Location = new System.Drawing.Point(31, 106);
            this.lbl_Loader_AbsRelMode.Name = "lbl_Loader_AbsRelMode";
            this.lbl_Loader_AbsRelMode.Size = new System.Drawing.Size(126, 23);
            this.lbl_Loader_AbsRelMode.TabIndex = 13;
            this.lbl_Loader_AbsRelMode.Text = "Abs.Rel Mode :";
            // 
            // tbox_Loader_Home_Accelation1
            // 
            this.tbox_Loader_Home_Accelation1.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_Accelation1.Location = new System.Drawing.Point(672, 373);
            this.tbox_Loader_Home_Accelation1.Name = "tbox_Loader_Home_Accelation1";
            this.tbox_Loader_Home_Accelation1.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_Accelation1.TabIndex = 78;
            // 
            // lbl_Loader_InPosition
            // 
            this.lbl_Loader_InPosition.AutoSize = true;
            this.lbl_Loader_InPosition.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_InPosition.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_InPosition.Location = new System.Drawing.Point(31, 178);
            this.lbl_Loader_InPosition.Name = "lbl_Loader_InPosition";
            this.lbl_Loader_InPosition.Size = new System.Drawing.Size(102, 23);
            this.lbl_Loader_InPosition.TabIndex = 14;
            this.lbl_Loader_InPosition.Text = "In Position :";
            // 
            // cbbox_Loader_Home_ZPhase
            // 
            this.cbbox_Loader_Home_ZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_Home_ZPhase.FormattingEnabled = true;
            this.cbbox_Loader_Home_ZPhase.Location = new System.Drawing.Point(672, 350);
            this.cbbox_Loader_Home_ZPhase.Name = "cbbox_Loader_Home_ZPhase";
            this.cbbox_Loader_Home_ZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_Home_ZPhase.TabIndex = 77;
            // 
            // lbl_Loader_Alarm
            // 
            this.lbl_Loader_Alarm.AutoSize = true;
            this.lbl_Loader_Alarm.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Alarm.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Alarm.Location = new System.Drawing.Point(31, 202);
            this.lbl_Loader_Alarm.Name = "lbl_Loader_Alarm";
            this.lbl_Loader_Alarm.Size = new System.Drawing.Size(65, 23);
            this.lbl_Loader_Alarm.TabIndex = 15;
            this.lbl_Loader_Alarm.Text = "Alarm :";
            // 
            // cbbox_Loader_Home_Direction
            // 
            this.cbbox_Loader_Home_Direction.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_Home_Direction.FormattingEnabled = true;
            this.cbbox_Loader_Home_Direction.Location = new System.Drawing.Point(672, 327);
            this.cbbox_Loader_Home_Direction.Name = "cbbox_Loader_Home_Direction";
            this.cbbox_Loader_Home_Direction.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_Home_Direction.TabIndex = 76;
            // 
            // lbl_Loader_Home_Level
            // 
            this.lbl_Loader_Home_Level.AutoSize = true;
            this.lbl_Loader_Home_Level.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Level.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Level.Location = new System.Drawing.Point(31, 346);
            this.lbl_Loader_Home_Level.Name = "lbl_Loader_Home_Level";
            this.lbl_Loader_Home_Level.Size = new System.Drawing.Size(110, 23);
            this.lbl_Loader_Home_Level.TabIndex = 16;
            this.lbl_Loader_Home_Level.Text = "Home Level :";
            // 
            // cbbox_Loader_SW_LimitEnable
            // 
            this.cbbox_Loader_SW_LimitEnable.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_SW_LimitEnable.FormattingEnabled = true;
            this.cbbox_Loader_SW_LimitEnable.Location = new System.Drawing.Point(672, 518);
            this.cbbox_Loader_SW_LimitEnable.Name = "cbbox_Loader_SW_LimitEnable";
            this.cbbox_Loader_SW_LimitEnable.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_SW_LimitEnable.TabIndex = 75;
            // 
            // lbl_Loader_SW_PlusLimit
            // 
            this.lbl_Loader_SW_PlusLimit.AutoSize = true;
            this.lbl_Loader_SW_PlusLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_PlusLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_PlusLimit.Location = new System.Drawing.Point(31, 514);
            this.lbl_Loader_SW_PlusLimit.Name = "lbl_Loader_SW_PlusLimit";
            this.lbl_Loader_SW_PlusLimit.Size = new System.Drawing.Size(153, 23);
            this.lbl_Loader_SW_PlusLimit.TabIndex = 17;
            this.lbl_Loader_SW_PlusLimit.Text = "S/W +Limit [mm] :";
            // 
            // cbbox_Loader_SW_LimitStopMode
            // 
            this.cbbox_Loader_SW_LimitStopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_SW_LimitStopMode.FormattingEnabled = true;
            this.cbbox_Loader_SW_LimitStopMode.Location = new System.Drawing.Point(672, 495);
            this.cbbox_Loader_SW_LimitStopMode.Name = "cbbox_Loader_SW_LimitStopMode";
            this.cbbox_Loader_SW_LimitStopMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_SW_LimitStopMode.TabIndex = 74;
            // 
            // lbl_Loader_Home_Vel3
            // 
            this.lbl_Loader_Home_Vel3.AutoSize = true;
            this.lbl_Loader_Home_Vel3.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Vel3.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Vel3.Location = new System.Drawing.Point(31, 418);
            this.lbl_Loader_Home_Vel3.Name = "lbl_Loader_Home_Vel3";
            this.lbl_Loader_Home_Vel3.Size = new System.Drawing.Size(127, 23);
            this.lbl_Loader_Home_Vel3.TabIndex = 18;
            this.lbl_Loader_Home_Vel3.Text = "Home Vel 3rd :";
            // 
            // tbox_Loader_Init_Inposition
            // 
            this.tbox_Loader_Init_Inposition.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Init_Inposition.Location = new System.Drawing.Point(672, 636);
            this.tbox_Loader_Init_Inposition.Name = "tbox_Loader_Init_Inposition";
            this.tbox_Loader_Init_Inposition.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Init_Inposition.TabIndex = 73;
            // 
            // lbl_Loader_SW
            // 
            this.lbl_Loader_SW.AutoSize = true;
            this.lbl_Loader_SW.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW.Location = new System.Drawing.Point(31, 466);
            this.lbl_Loader_SW.Name = "lbl_Loader_SW";
            this.lbl_Loader_SW.Size = new System.Drawing.Size(145, 23);
            this.lbl_Loader_SW.TabIndex = 19;
            this.lbl_Loader_SW.Text = "S/W Limit Setting";
            // 
            // tbox_Loader_Init_Decelation
            // 
            this.tbox_Loader_Init_Decelation.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Init_Decelation.Location = new System.Drawing.Point(672, 611);
            this.tbox_Loader_Init_Decelation.Name = "tbox_Loader_Init_Decelation";
            this.tbox_Loader_Init_Decelation.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Init_Decelation.TabIndex = 72;
            // 
            // lbl_Loader_SW_MinusLimit
            // 
            this.lbl_Loader_SW_MinusLimit.AutoSize = true;
            this.lbl_Loader_SW_MinusLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_MinusLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_MinusLimit.Location = new System.Drawing.Point(31, 490);
            this.lbl_Loader_SW_MinusLimit.Name = "lbl_Loader_SW_MinusLimit";
            this.lbl_Loader_SW_MinusLimit.Size = new System.Drawing.Size(148, 23);
            this.lbl_Loader_SW_MinusLimit.TabIndex = 20;
            this.lbl_Loader_SW_MinusLimit.Text = "S/W -Limit [mm] :";
            // 
            // tbox_Loader_Init_Accelation
            // 
            this.tbox_Loader_Init_Accelation.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Init_Accelation.Location = new System.Drawing.Point(672, 586);
            this.tbox_Loader_Init_Accelation.Name = "tbox_Loader_Init_Accelation";
            this.tbox_Loader_Init_Accelation.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Init_Accelation.TabIndex = 71;
            // 
            // lbl_Loader_Init
            // 
            this.lbl_Loader_Init.AutoSize = true;
            this.lbl_Loader_Init.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init.Location = new System.Drawing.Point(31, 562);
            this.lbl_Loader_Init.Name = "lbl_Loader_Init";
            this.lbl_Loader_Init.Size = new System.Drawing.Size(95, 23);
            this.lbl_Loader_Init.TabIndex = 21;
            this.lbl_Loader_Init.Text = "Init Setting";
            // 
            // tbox_Loader_Init_AccelationTime
            // 
            this.tbox_Loader_Init_AccelationTime.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Init_AccelationTime.Location = new System.Drawing.Point(245, 661);
            this.tbox_Loader_Init_AccelationTime.Name = "tbox_Loader_Init_AccelationTime";
            this.tbox_Loader_Init_AccelationTime.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Init_AccelationTime.TabIndex = 70;
            // 
            // lbl_Loader_SW_LimitMode
            // 
            this.lbl_Loader_SW_LimitMode.AutoSize = true;
            this.lbl_Loader_SW_LimitMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_LimitMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_LimitMode.Location = new System.Drawing.Point(31, 538);
            this.lbl_Loader_SW_LimitMode.Name = "lbl_Loader_SW_LimitMode";
            this.lbl_Loader_SW_LimitMode.Size = new System.Drawing.Size(146, 23);
            this.lbl_Loader_SW_LimitMode.TabIndex = 22;
            this.lbl_Loader_SW_LimitMode.Text = "S/W Limit Mode :";
            // 
            // tbox_Loader_Init_PtpSpeed
            // 
            this.tbox_Loader_Init_PtpSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Init_PtpSpeed.Location = new System.Drawing.Point(245, 636);
            this.tbox_Loader_Init_PtpSpeed.Name = "tbox_Loader_Init_PtpSpeed";
            this.tbox_Loader_Init_PtpSpeed.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Init_PtpSpeed.TabIndex = 69;
            // 
            // lbl_Loader_MinusEndLimit
            // 
            this.lbl_Loader_MinusEndLimit.AutoSize = true;
            this.lbl_Loader_MinusEndLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MinusEndLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MinusEndLimit.Location = new System.Drawing.Point(31, 226);
            this.lbl_Loader_MinusEndLimit.Name = "lbl_Loader_MinusEndLimit";
            this.lbl_Loader_MinusEndLimit.Size = new System.Drawing.Size(105, 23);
            this.lbl_Loader_MinusEndLimit.TabIndex = 23;
            this.lbl_Loader_MinusEndLimit.Text = "- End Limit :";
            // 
            // tbox_Loader_Init_Velocity
            // 
            this.tbox_Loader_Init_Velocity.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Init_Velocity.Location = new System.Drawing.Point(245, 611);
            this.tbox_Loader_Init_Velocity.Name = "tbox_Loader_Init_Velocity";
            this.tbox_Loader_Init_Velocity.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Init_Velocity.TabIndex = 68;
            // 
            // lbl_Loader_Init_Position
            // 
            this.lbl_Loader_Init_Position.AutoSize = true;
            this.lbl_Loader_Init_Position.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Position.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Position.Location = new System.Drawing.Point(31, 586);
            this.lbl_Loader_Init_Position.Name = "lbl_Loader_Init_Position";
            this.lbl_Loader_Init_Position.Size = new System.Drawing.Size(158, 23);
            this.lbl_Loader_Init_Position.TabIndex = 24;
            this.lbl_Loader_Init_Position.Text = "Init Position [mm] :";
            // 
            // tbox_Loader_Init_Position
            // 
            this.tbox_Loader_Init_Position.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Init_Position.Location = new System.Drawing.Point(245, 586);
            this.tbox_Loader_Init_Position.Name = "tbox_Loader_Init_Position";
            this.tbox_Loader_Init_Position.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Init_Position.TabIndex = 67;
            // 
            // lbl_Loader_Init_Velocity
            // 
            this.lbl_Loader_Init_Velocity.AutoSize = true;
            this.lbl_Loader_Init_Velocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Velocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Velocity.Location = new System.Drawing.Point(31, 610);
            this.lbl_Loader_Init_Velocity.Name = "lbl_Loader_Init_Velocity";
            this.lbl_Loader_Init_Velocity.Size = new System.Drawing.Size(188, 23);
            this.lbl_Loader_Init_Velocity.TabIndex = 25;
            this.lbl_Loader_Init_Velocity.Text = "Init Velocity [mm/sec] :";
            // 
            // cbbox_Loader_SW_LimitMode
            // 
            this.cbbox_Loader_SW_LimitMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_SW_LimitMode.FormattingEnabled = true;
            this.cbbox_Loader_SW_LimitMode.Location = new System.Drawing.Point(245, 541);
            this.cbbox_Loader_SW_LimitMode.Name = "cbbox_Loader_SW_LimitMode";
            this.cbbox_Loader_SW_LimitMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_SW_LimitMode.TabIndex = 66;
            // 
            // lbl_Loader_Init_PtpSpeed
            // 
            this.lbl_Loader_Init_PtpSpeed.AutoSize = true;
            this.lbl_Loader_Init_PtpSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_PtpSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_PtpSpeed.Location = new System.Drawing.Point(31, 634);
            this.lbl_Loader_Init_PtpSpeed.Name = "lbl_Loader_Init_PtpSpeed";
            this.lbl_Loader_Init_PtpSpeed.Size = new System.Drawing.Size(99, 23);
            this.lbl_Loader_Init_PtpSpeed.TabIndex = 26;
            this.lbl_Loader_Init_PtpSpeed.Text = "Ptp Speed :";
            // 
            // tbox_Loader_SW_PlusLimit
            // 
            this.tbox_Loader_SW_PlusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_SW_PlusLimit.Location = new System.Drawing.Point(245, 515);
            this.tbox_Loader_SW_PlusLimit.Name = "tbox_Loader_SW_PlusLimit";
            this.tbox_Loader_SW_PlusLimit.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_SW_PlusLimit.TabIndex = 65;
            // 
            // lbl_Loader_Init_AccelationTime
            // 
            this.lbl_Loader_Init_AccelationTime.AutoSize = true;
            this.lbl_Loader_Init_AccelationTime.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_AccelationTime.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_AccelationTime.Location = new System.Drawing.Point(31, 658);
            this.lbl_Loader_Init_AccelationTime.Name = "lbl_Loader_Init_AccelationTime";
            this.lbl_Loader_Init_AccelationTime.Size = new System.Drawing.Size(165, 23);
            this.lbl_Loader_Init_AccelationTime.TabIndex = 27;
            this.lbl_Loader_Init_AccelationTime.Text = "Accelation Time [s] :";
            // 
            // tbox_Loader_SW_MinusLimit
            // 
            this.tbox_Loader_SW_MinusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_SW_MinusLimit.Location = new System.Drawing.Point(245, 490);
            this.tbox_Loader_SW_MinusLimit.Name = "tbox_Loader_SW_MinusLimit";
            this.tbox_Loader_SW_MinusLimit.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_SW_MinusLimit.TabIndex = 64;
            // 
            // lbl_Loader_Pulse
            // 
            this.lbl_Loader_Pulse.AutoSize = true;
            this.lbl_Loader_Pulse.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Pulse.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Pulse.Location = new System.Drawing.Point(469, 130);
            this.lbl_Loader_Pulse.Name = "lbl_Loader_Pulse";
            this.lbl_Loader_Pulse.Size = new System.Drawing.Size(60, 23);
            this.lbl_Loader_Pulse.TabIndex = 28;
            this.lbl_Loader_Pulse.Text = "Pulse :";
            // 
            // tbox_Loader_Home_VelLast
            // 
            this.tbox_Loader_Home_VelLast.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_VelLast.Location = new System.Drawing.Point(245, 443);
            this.tbox_Loader_Home_VelLast.Name = "tbox_Loader_Home_VelLast";
            this.tbox_Loader_Home_VelLast.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_VelLast.TabIndex = 63;
            // 
            // lbl_Loader_MinVelocitu
            // 
            this.lbl_Loader_MinVelocitu.AutoSize = true;
            this.lbl_Loader_MinVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MinVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MinVelocitu.Location = new System.Drawing.Point(469, 82);
            this.lbl_Loader_MinVelocitu.Name = "lbl_Loader_MinVelocitu";
            this.lbl_Loader_MinVelocitu.Size = new System.Drawing.Size(195, 23);
            this.lbl_Loader_MinVelocitu.TabIndex = 29;
            this.lbl_Loader_MinVelocitu.Text = "Min Velocitu [mm/sec] :";
            // 
            // tbox_Loader_Home_Vel3
            // 
            this.tbox_Loader_Home_Vel3.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_Vel3.Location = new System.Drawing.Point(245, 418);
            this.tbox_Loader_Home_Vel3.Name = "tbox_Loader_Home_Vel3";
            this.tbox_Loader_Home_Vel3.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_Vel3.TabIndex = 62;
            // 
            // lbl_Loader_MaxVelocitu
            // 
            this.lbl_Loader_MaxVelocitu.AutoSize = true;
            this.lbl_Loader_MaxVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MaxVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MaxVelocitu.Location = new System.Drawing.Point(469, 58);
            this.lbl_Loader_MaxVelocitu.Name = "lbl_Loader_MaxVelocitu";
            this.lbl_Loader_MaxVelocitu.Size = new System.Drawing.Size(198, 23);
            this.lbl_Loader_MaxVelocitu.TabIndex = 30;
            this.lbl_Loader_MaxVelocitu.Text = "Max Velocitu [mm/sec] :";
            // 
            // tbox_Loader_Home_Vel2
            // 
            this.tbox_Loader_Home_Vel2.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_Vel2.Location = new System.Drawing.Point(245, 393);
            this.tbox_Loader_Home_Vel2.Name = "tbox_Loader_Home_Vel2";
            this.tbox_Loader_Home_Vel2.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_Vel2.TabIndex = 61;
            // 
            // lbl_Loader_Unit
            // 
            this.lbl_Loader_Unit.AutoSize = true;
            this.lbl_Loader_Unit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Unit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Unit.Location = new System.Drawing.Point(469, 106);
            this.lbl_Loader_Unit.Name = "lbl_Loader_Unit";
            this.lbl_Loader_Unit.Size = new System.Drawing.Size(52, 23);
            this.lbl_Loader_Unit.TabIndex = 31;
            this.lbl_Loader_Unit.Text = "Unit :";
            // 
            // tbox_Loader_Home_Vel1
            // 
            this.tbox_Loader_Home_Vel1.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_Home_Vel1.Location = new System.Drawing.Point(245, 368);
            this.tbox_Loader_Home_Vel1.Name = "tbox_Loader_Home_Vel1";
            this.tbox_Loader_Home_Vel1.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_Home_Vel1.TabIndex = 60;
            // 
            // lbl_Loader_EncoderType
            // 
            this.lbl_Loader_EncoderType.AutoSize = true;
            this.lbl_Loader_EncoderType.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_EncoderType.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_EncoderType.Location = new System.Drawing.Point(469, 274);
            this.lbl_Loader_EncoderType.Name = "lbl_Loader_EncoderType";
            this.lbl_Loader_EncoderType.Size = new System.Drawing.Size(124, 23);
            this.lbl_Loader_EncoderType.TabIndex = 32;
            this.lbl_Loader_EncoderType.Text = "Encoder Type :";
            // 
            // cbbox_Loader_Home_Level
            // 
            this.cbbox_Loader_Home_Level.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_Home_Level.FormattingEnabled = true;
            this.cbbox_Loader_Home_Level.Location = new System.Drawing.Point(245, 345);
            this.cbbox_Loader_Home_Level.Name = "cbbox_Loader_Home_Level";
            this.cbbox_Loader_Home_Level.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_Home_Level.TabIndex = 59;
            // 
            // lbl_Loader_AlarmResetLevel
            // 
            this.lbl_Loader_AlarmResetLevel.AutoSize = true;
            this.lbl_Loader_AlarmResetLevel.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AlarmResetLevel.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AlarmResetLevel.Location = new System.Drawing.Point(469, 250);
            this.lbl_Loader_AlarmResetLevel.Name = "lbl_Loader_AlarmResetLevel";
            this.lbl_Loader_AlarmResetLevel.Size = new System.Drawing.Size(156, 23);
            this.lbl_Loader_AlarmResetLevel.TabIndex = 33;
            this.lbl_Loader_AlarmResetLevel.Text = "Alarm Reset Level :";
            // 
            // cbbox_Loader_Home_Signal
            // 
            this.cbbox_Loader_Home_Signal.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_Home_Signal.FormattingEnabled = true;
            this.cbbox_Loader_Home_Signal.Location = new System.Drawing.Point(245, 322);
            this.cbbox_Loader_Home_Signal.Name = "cbbox_Loader_Home_Signal";
            this.cbbox_Loader_Home_Signal.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_Home_Signal.TabIndex = 58;
            // 
            // lbl_Loader_ZPhase
            // 
            this.lbl_Loader_ZPhase.AutoSize = true;
            this.lbl_Loader_ZPhase.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_ZPhase.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_ZPhase.Location = new System.Drawing.Point(469, 178);
            this.lbl_Loader_ZPhase.Name = "lbl_Loader_ZPhase";
            this.lbl_Loader_ZPhase.Size = new System.Drawing.Size(82, 23);
            this.lbl_Loader_ZPhase.TabIndex = 34;
            this.lbl_Loader_ZPhase.Text = "Z-Phase :";
            // 
            // cbbox_Loader_ServoOnLevel
            // 
            this.cbbox_Loader_ServoOnLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_ServoOnLevel.FormattingEnabled = true;
            this.cbbox_Loader_ServoOnLevel.Location = new System.Drawing.Point(245, 276);
            this.cbbox_Loader_ServoOnLevel.Name = "cbbox_Loader_ServoOnLevel";
            this.cbbox_Loader_ServoOnLevel.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_ServoOnLevel.TabIndex = 57;
            // 
            // lbl_Loader_StopMode
            // 
            this.lbl_Loader_StopMode.AutoSize = true;
            this.lbl_Loader_StopMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_StopMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_StopMode.Location = new System.Drawing.Point(469, 202);
            this.lbl_Loader_StopMode.Name = "lbl_Loader_StopMode";
            this.lbl_Loader_StopMode.Size = new System.Drawing.Size(106, 23);
            this.lbl_Loader_StopMode.TabIndex = 35;
            this.lbl_Loader_StopMode.Text = "Stop Mode :";
            // 
            // cbbox_Loader_PlusEndLimit
            // 
            this.cbbox_Loader_PlusEndLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_PlusEndLimit.FormattingEnabled = true;
            this.cbbox_Loader_PlusEndLimit.Location = new System.Drawing.Point(245, 252);
            this.cbbox_Loader_PlusEndLimit.Name = "cbbox_Loader_PlusEndLimit";
            this.cbbox_Loader_PlusEndLimit.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_PlusEndLimit.TabIndex = 56;
            // 
            // lbl_Loader_StopLevel
            // 
            this.lbl_Loader_StopLevel.AutoSize = true;
            this.lbl_Loader_StopLevel.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_StopLevel.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_StopLevel.Location = new System.Drawing.Point(469, 226);
            this.lbl_Loader_StopLevel.Name = "lbl_Loader_StopLevel";
            this.lbl_Loader_StopLevel.Size = new System.Drawing.Size(99, 23);
            this.lbl_Loader_StopLevel.TabIndex = 36;
            this.lbl_Loader_StopLevel.Text = "Stop Level :";
            // 
            // cbbox_Loader_MinusEndLimit
            // 
            this.cbbox_Loader_MinusEndLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_MinusEndLimit.FormattingEnabled = true;
            this.cbbox_Loader_MinusEndLimit.Location = new System.Drawing.Point(245, 228);
            this.cbbox_Loader_MinusEndLimit.Name = "cbbox_Loader_MinusEndLimit";
            this.cbbox_Loader_MinusEndLimit.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_MinusEndLimit.TabIndex = 55;
            // 
            // lbl_Loader_Home_Direction
            // 
            this.lbl_Loader_Home_Direction.AutoSize = true;
            this.lbl_Loader_Home_Direction.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Direction.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Direction.Location = new System.Drawing.Point(469, 322);
            this.lbl_Loader_Home_Direction.Name = "lbl_Loader_Home_Direction";
            this.lbl_Loader_Home_Direction.Size = new System.Drawing.Size(141, 23);
            this.lbl_Loader_Home_Direction.TabIndex = 37;
            this.lbl_Loader_Home_Direction.Text = "Home Direction :";
            // 
            // cbbox_Loader_Alarm
            // 
            this.cbbox_Loader_Alarm.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_Alarm.FormattingEnabled = true;
            this.cbbox_Loader_Alarm.Location = new System.Drawing.Point(245, 204);
            this.cbbox_Loader_Alarm.Name = "cbbox_Loader_Alarm";
            this.cbbox_Loader_Alarm.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_Alarm.TabIndex = 54;
            // 
            // lbl_Loader_Home_Accelation1
            // 
            this.lbl_Loader_Home_Accelation1.AutoSize = true;
            this.lbl_Loader_Home_Accelation1.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Accelation1.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Accelation1.Location = new System.Drawing.Point(469, 370);
            this.lbl_Loader_Home_Accelation1.Name = "lbl_Loader_Home_Accelation1";
            this.lbl_Loader_Home_Accelation1.Size = new System.Drawing.Size(179, 23);
            this.lbl_Loader_Home_Accelation1.TabIndex = 38;
            this.lbl_Loader_Home_Accelation1.Text = "Home Accelation 1st :";
            // 
            // cbbox_Loader_Inposition
            // 
            this.cbbox_Loader_Inposition.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_Inposition.FormattingEnabled = true;
            this.cbbox_Loader_Inposition.Location = new System.Drawing.Point(245, 180);
            this.cbbox_Loader_Inposition.Name = "cbbox_Loader_Inposition";
            this.cbbox_Loader_Inposition.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_Inposition.TabIndex = 53;
            // 
            // lbl_Loader_Home_Accelation2
            // 
            this.lbl_Loader_Home_Accelation2.AutoSize = true;
            this.lbl_Loader_Home_Accelation2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Accelation2.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Accelation2.Location = new System.Drawing.Point(469, 394);
            this.lbl_Loader_Home_Accelation2.Name = "lbl_Loader_Home_Accelation2";
            this.lbl_Loader_Home_Accelation2.Size = new System.Drawing.Size(186, 23);
            this.lbl_Loader_Home_Accelation2.TabIndex = 39;
            this.lbl_Loader_Home_Accelation2.Text = "Home Accelation 2nd :";
            // 
            // cbbox_Loader_AbsRelMode
            // 
            this.cbbox_Loader_AbsRelMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_AbsRelMode.FormattingEnabled = true;
            this.cbbox_Loader_AbsRelMode.Location = new System.Drawing.Point(245, 110);
            this.cbbox_Loader_AbsRelMode.Name = "cbbox_Loader_AbsRelMode";
            this.cbbox_Loader_AbsRelMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_AbsRelMode.TabIndex = 52;
            // 
            // lbl_Loader_Home_Offset
            // 
            this.lbl_Loader_Home_Offset.AutoSize = true;
            this.lbl_Loader_Home_Offset.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Offset.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Offset.Location = new System.Drawing.Point(469, 442);
            this.lbl_Loader_Home_Offset.Name = "lbl_Loader_Home_Offset";
            this.lbl_Loader_Home_Offset.Size = new System.Drawing.Size(117, 23);
            this.lbl_Loader_Home_Offset.TabIndex = 40;
            this.lbl_Loader_Home_Offset.Text = "Home Offset :";
            // 
            // cbbox_Loader_EncInput
            // 
            this.cbbox_Loader_EncInput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_EncInput.FormattingEnabled = true;
            this.cbbox_Loader_EncInput.Location = new System.Drawing.Point(245, 86);
            this.cbbox_Loader_EncInput.Name = "cbbox_Loader_EncInput";
            this.cbbox_Loader_EncInput.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_EncInput.TabIndex = 51;
            // 
            // lbl_Loader_Home_ZPhase
            // 
            this.lbl_Loader_Home_ZPhase.AutoSize = true;
            this.lbl_Loader_Home_ZPhase.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_ZPhase.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_ZPhase.Location = new System.Drawing.Point(469, 346);
            this.lbl_Loader_Home_ZPhase.Name = "lbl_Loader_Home_ZPhase";
            this.lbl_Loader_Home_ZPhase.Size = new System.Drawing.Size(134, 23);
            this.lbl_Loader_Home_ZPhase.TabIndex = 41;
            this.lbl_Loader_Home_ZPhase.Text = "Home Z_Phase :";
            // 
            // cbbox_Loader_PulseOutput
            // 
            this.cbbox_Loader_PulseOutput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_PulseOutput.FormattingEnabled = true;
            this.cbbox_Loader_PulseOutput.Location = new System.Drawing.Point(245, 63);
            this.cbbox_Loader_PulseOutput.Name = "cbbox_Loader_PulseOutput";
            this.cbbox_Loader_PulseOutput.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_PulseOutput.TabIndex = 50;
            // 
            // lbl_Loader_Home_ClearTime
            // 
            this.lbl_Loader_Home_ClearTime.AutoSize = true;
            this.lbl_Loader_Home_ClearTime.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_ClearTime.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_ClearTime.Location = new System.Drawing.Point(469, 418);
            this.lbl_Loader_Home_ClearTime.Name = "lbl_Loader_Home_ClearTime";
            this.lbl_Loader_Home_ClearTime.Size = new System.Drawing.Size(154, 23);
            this.lbl_Loader_Home_ClearTime.TabIndex = 42;
            this.lbl_Loader_Home_ClearTime.Text = "Home Clear Time :";
            // 
            // cbbox_Loader_VelProfileMode
            // 
            this.cbbox_Loader_VelProfileMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Loader_VelProfileMode.FormattingEnabled = true;
            this.cbbox_Loader_VelProfileMode.Location = new System.Drawing.Point(245, 135);
            this.cbbox_Loader_VelProfileMode.Name = "cbbox_Loader_VelProfileMode";
            this.cbbox_Loader_VelProfileMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Loader_VelProfileMode.TabIndex = 49;
            // 
            // lbl_Loader_SW_LimitEnable
            // 
            this.lbl_Loader_SW_LimitEnable.AutoSize = true;
            this.lbl_Loader_SW_LimitEnable.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_LimitEnable.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_LimitEnable.Location = new System.Drawing.Point(469, 514);
            this.lbl_Loader_SW_LimitEnable.Name = "lbl_Loader_SW_LimitEnable";
            this.lbl_Loader_SW_LimitEnable.Size = new System.Drawing.Size(152, 23);
            this.lbl_Loader_SW_LimitEnable.TabIndex = 43;
            this.lbl_Loader_SW_LimitEnable.Text = "S/W Limit Enable :";
            // 
            // tbox_Loader_AxisNo
            // 
            this.tbox_Loader_AxisNo.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Loader_AxisNo.Location = new System.Drawing.Point(245, 38);
            this.tbox_Loader_AxisNo.Name = "tbox_Loader_AxisNo";
            this.tbox_Loader_AxisNo.Size = new System.Drawing.Size(121, 23);
            this.tbox_Loader_AxisNo.TabIndex = 48;
            // 
            // lbl_Loader_SW_LimitStopMode
            // 
            this.lbl_Loader_SW_LimitStopMode.AutoSize = true;
            this.lbl_Loader_SW_LimitStopMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_LimitStopMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_LimitStopMode.Location = new System.Drawing.Point(469, 490);
            this.lbl_Loader_SW_LimitStopMode.Name = "lbl_Loader_SW_LimitStopMode";
            this.lbl_Loader_SW_LimitStopMode.Size = new System.Drawing.Size(187, 23);
            this.lbl_Loader_SW_LimitStopMode.TabIndex = 44;
            this.lbl_Loader_SW_LimitStopMode.Text = "S/W Limit Stop Mode :";
            // 
            // lbl_Loader_Init_InPosition
            // 
            this.lbl_Loader_Init_InPosition.AutoSize = true;
            this.lbl_Loader_Init_InPosition.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_InPosition.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_InPosition.Location = new System.Drawing.Point(469, 634);
            this.lbl_Loader_Init_InPosition.Name = "lbl_Loader_Init_InPosition";
            this.lbl_Loader_Init_InPosition.Size = new System.Drawing.Size(138, 23);
            this.lbl_Loader_Init_InPosition.TabIndex = 47;
            this.lbl_Loader_Init_InPosition.Text = "In Position [mm]";
            // 
            // lbl_Loader_Init_Accelation
            // 
            this.lbl_Loader_Init_Accelation.AutoSize = true;
            this.lbl_Loader_Init_Accelation.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Accelation.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Accelation.Location = new System.Drawing.Point(469, 586);
            this.lbl_Loader_Init_Accelation.Name = "lbl_Loader_Init_Accelation";
            this.lbl_Loader_Init_Accelation.Size = new System.Drawing.Size(130, 23);
            this.lbl_Loader_Init_Accelation.TabIndex = 45;
            this.lbl_Loader_Init_Accelation.Text = "Init Accelation :";
            // 
            // lbl_Loader_Init_Decelation
            // 
            this.lbl_Loader_Init_Decelation.AutoSize = true;
            this.lbl_Loader_Init_Decelation.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Decelation.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Decelation.Location = new System.Drawing.Point(469, 610);
            this.lbl_Loader_Init_Decelation.Name = "lbl_Loader_Init_Decelation";
            this.lbl_Loader_Init_Decelation.Size = new System.Drawing.Size(132, 23);
            this.lbl_Loader_Init_Decelation.TabIndex = 46;
            this.lbl_Loader_Init_Decelation.Text = "Init Decelation :";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_15, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_14, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_13, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_12, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_11, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_10, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_09, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_08, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_07, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_06, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_05, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_04, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_36, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_35, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_33, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_32, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pn_Loader_31, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 17;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(520, 529);
            this.tableLayoutPanel1.TabIndex = 32;
            // 
            // pn_Loader_15
            // 
            this.pn_Loader_15.Controls.Add(this.lbl_Loader_15);
            this.pn_Loader_15.Location = new System.Drawing.Point(3, 499);
            this.pn_Loader_15.Name = "pn_Loader_15";
            this.pn_Loader_15.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_15.TabIndex = 49;
            this.pn_Loader_15.Click += new System.EventHandler(this.pn_Loader_15_Click);
            // 
            // lbl_Loader_15
            // 
            this.lbl_Loader_15.AutoSize = true;
            this.lbl_Loader_15.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_15.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_15.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_15.Name = "lbl_Loader_15";
            this.lbl_Loader_15.Size = new System.Drawing.Size(169, 19);
            this.lbl_Loader_15.TabIndex = 0;
            this.lbl_Loader_15.Text = "15. CELL_TRANSFER_T_2";
            this.lbl_Loader_15.Click += new System.EventHandler(this.pn_Loader_15_Click);
            // 
            // pn_Loader_14
            // 
            this.pn_Loader_14.Controls.Add(this.lbl_Loader_14);
            this.pn_Loader_14.Location = new System.Drawing.Point(3, 468);
            this.pn_Loader_14.Name = "pn_Loader_14";
            this.pn_Loader_14.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_14.TabIndex = 48;
            this.pn_Loader_14.Click += new System.EventHandler(this.pn_Loader_14_Click);
            // 
            // lbl_Loader_14
            // 
            this.lbl_Loader_14.AutoSize = true;
            this.lbl_Loader_14.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_14.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_14.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_14.Name = "lbl_Loader_14";
            this.lbl_Loader_14.Size = new System.Drawing.Size(169, 19);
            this.lbl_Loader_14.TabIndex = 0;
            this.lbl_Loader_14.Text = "14. CELL_TRANSFER_Y_4";
            this.lbl_Loader_14.Click += new System.EventHandler(this.pn_Loader_14_Click);
            // 
            // pn_Loader_13
            // 
            this.pn_Loader_13.Controls.Add(this.lbl_Loader_13);
            this.pn_Loader_13.Location = new System.Drawing.Point(3, 437);
            this.pn_Loader_13.Name = "pn_Loader_13";
            this.pn_Loader_13.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_13.TabIndex = 47;
            this.pn_Loader_13.Click += new System.EventHandler(this.pn_Loader_13_Click);
            // 
            // lbl_Loader_13
            // 
            this.lbl_Loader_13.AutoSize = true;
            this.lbl_Loader_13.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_13.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_13.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_13.Name = "lbl_Loader_13";
            this.lbl_Loader_13.Size = new System.Drawing.Size(93, 19);
            this.lbl_Loader_13.TabIndex = 0;
            this.lbl_Loader_13.Text = "13. CELL_Y_2";
            this.lbl_Loader_13.Click += new System.EventHandler(this.pn_Loader_13_Click);
            // 
            // pn_Loader_12
            // 
            this.pn_Loader_12.Controls.Add(this.lbl_Loader_12);
            this.pn_Loader_12.Location = new System.Drawing.Point(3, 406);
            this.pn_Loader_12.Name = "pn_Loader_12";
            this.pn_Loader_12.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_12.TabIndex = 46;
            this.pn_Loader_12.Click += new System.EventHandler(this.pn_Loader_12_Click);
            // 
            // lbl_Loader_12
            // 
            this.lbl_Loader_12.AutoSize = true;
            this.lbl_Loader_12.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_12.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_12.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_12.Name = "lbl_Loader_12";
            this.lbl_Loader_12.Size = new System.Drawing.Size(169, 19);
            this.lbl_Loader_12.TabIndex = 0;
            this.lbl_Loader_12.Text = "12. CELL_TRANSFER_T_1";
            this.lbl_Loader_12.Click += new System.EventHandler(this.pn_Loader_12_Click);
            // 
            // pn_Loader_11
            // 
            this.pn_Loader_11.Controls.Add(this.lbl_Loader_11);
            this.pn_Loader_11.Location = new System.Drawing.Point(3, 375);
            this.pn_Loader_11.Name = "pn_Loader_11";
            this.pn_Loader_11.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_11.TabIndex = 45;
            this.pn_Loader_11.Click += new System.EventHandler(this.pn_Loader_11_Click);
            // 
            // lbl_Loader_11
            // 
            this.lbl_Loader_11.AutoSize = true;
            this.lbl_Loader_11.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_11.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_11.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_11.Name = "lbl_Loader_11";
            this.lbl_Loader_11.Size = new System.Drawing.Size(169, 19);
            this.lbl_Loader_11.TabIndex = 0;
            this.lbl_Loader_11.Text = "11. CELL_TRANSFER_Y_3";
            this.lbl_Loader_11.Click += new System.EventHandler(this.pn_Loader_11_Click);
            // 
            // pn_Loader_10
            // 
            this.pn_Loader_10.Controls.Add(this.lbl_Loader_10);
            this.pn_Loader_10.Location = new System.Drawing.Point(3, 344);
            this.pn_Loader_10.Name = "pn_Loader_10";
            this.pn_Loader_10.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_10.TabIndex = 44;
            this.pn_Loader_10.Click += new System.EventHandler(this.pn_Loader_10_Click);
            // 
            // lbl_Loader_10
            // 
            this.lbl_Loader_10.AutoSize = true;
            this.lbl_Loader_10.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_10.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_10.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_10.Name = "lbl_Loader_10";
            this.lbl_Loader_10.Size = new System.Drawing.Size(93, 19);
            this.lbl_Loader_10.TabIndex = 0;
            this.lbl_Loader_10.Text = "10. CELL_Y_1";
            this.lbl_Loader_10.Click += new System.EventHandler(this.pn_Loader_10_Click);
            // 
            // pn_Loader_09
            // 
            this.pn_Loader_09.Controls.Add(this.lbl_Loader_09);
            this.pn_Loader_09.Location = new System.Drawing.Point(3, 313);
            this.pn_Loader_09.Name = "pn_Loader_09";
            this.pn_Loader_09.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_09.TabIndex = 43;
            this.pn_Loader_09.Click += new System.EventHandler(this.pn_Loader_09_Click);
            // 
            // lbl_Loader_09
            // 
            this.lbl_Loader_09.AutoSize = true;
            this.lbl_Loader_09.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_09.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_09.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_09.Name = "lbl_Loader_09";
            this.lbl_Loader_09.Size = new System.Drawing.Size(248, 19);
            this.lbl_Loader_09.TabIndex = 0;
            this.lbl_Loader_09.Text = "09. CASSETTE_ELECTRICTY_Z_RIGHT";
            this.lbl_Loader_09.Click += new System.EventHandler(this.pn_Loader_09_Click);
            // 
            // pn_Loader_08
            // 
            this.pn_Loader_08.Controls.Add(this.lbl_Loader_08);
            this.pn_Loader_08.Location = new System.Drawing.Point(3, 282);
            this.pn_Loader_08.Name = "pn_Loader_08";
            this.pn_Loader_08.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_08.TabIndex = 42;
            this.pn_Loader_08.Click += new System.EventHandler(this.pn_Loader_08_Click);
            // 
            // lbl_Loader_08
            // 
            this.lbl_Loader_08.AutoSize = true;
            this.lbl_Loader_08.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_08.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_08.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_08.Name = "lbl_Loader_08";
            this.lbl_Loader_08.Size = new System.Drawing.Size(236, 19);
            this.lbl_Loader_08.TabIndex = 0;
            this.lbl_Loader_08.Text = "08. CASSETTE_ELECTRICTY_Z_LEFT";
            this.lbl_Loader_08.Click += new System.EventHandler(this.pn_Loader_08_Click);
            // 
            // pn_Loader_07
            // 
            this.pn_Loader_07.Controls.Add(this.lbl_Loader_07);
            this.pn_Loader_07.Location = new System.Drawing.Point(3, 251);
            this.pn_Loader_07.Name = "pn_Loader_07";
            this.pn_Loader_07.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_07.TabIndex = 41;
            this.pn_Loader_07.Click += new System.EventHandler(this.pn_Loader_07_Click);
            // 
            // lbl_Loader_07
            // 
            this.lbl_Loader_07.AutoSize = true;
            this.lbl_Loader_07.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_07.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_07.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_07.Name = "lbl_Loader_07";
            this.lbl_Loader_07.Size = new System.Drawing.Size(279, 19);
            this.lbl_Loader_07.TabIndex = 0;
            this.lbl_Loader_07.Text = "07. CASSETTE_ROTATION_DOWN_RIGHT";
            this.lbl_Loader_07.Click += new System.EventHandler(this.pn_Loader_07_Click);
            // 
            // pn_Loader_06
            // 
            this.pn_Loader_06.Controls.Add(this.lbl_Loader_06);
            this.pn_Loader_06.Location = new System.Drawing.Point(3, 220);
            this.pn_Loader_06.Name = "pn_Loader_06";
            this.pn_Loader_06.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_06.TabIndex = 40;
            this.pn_Loader_06.Click += new System.EventHandler(this.pn_Loader_06_Click);
            // 
            // lbl_Loader_06
            // 
            this.lbl_Loader_06.AutoSize = true;
            this.lbl_Loader_06.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_06.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_06.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_06.Name = "lbl_Loader_06";
            this.lbl_Loader_06.Size = new System.Drawing.Size(252, 19);
            this.lbl_Loader_06.TabIndex = 0;
            this.lbl_Loader_06.Text = "06. CASSETTE_ROTATION_UP_RIGHT";
            this.lbl_Loader_06.Click += new System.EventHandler(this.pn_Loader_06_Click);
            // 
            // pn_Loader_05
            // 
            this.pn_Loader_05.Controls.Add(this.lbl_Loader_05);
            this.pn_Loader_05.Location = new System.Drawing.Point(3, 189);
            this.pn_Loader_05.Name = "pn_Loader_05";
            this.pn_Loader_05.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_05.TabIndex = 39;
            this.pn_Loader_05.Click += new System.EventHandler(this.pn_Loader_05_Click);
            // 
            // lbl_Loader_05
            // 
            this.lbl_Loader_05.AutoSize = true;
            this.lbl_Loader_05.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_05.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_05.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_05.Name = "lbl_Loader_05";
            this.lbl_Loader_05.Size = new System.Drawing.Size(267, 19);
            this.lbl_Loader_05.TabIndex = 0;
            this.lbl_Loader_05.Text = "05. CASSETTE_ROTATION_DOWN_LEFT";
            this.lbl_Loader_05.Click += new System.EventHandler(this.pn_Loader_05_Click);
            // 
            // pn_Loader_04
            // 
            this.pn_Loader_04.Controls.Add(this.lbl_Loader_04);
            this.pn_Loader_04.Location = new System.Drawing.Point(3, 158);
            this.pn_Loader_04.Name = "pn_Loader_04";
            this.pn_Loader_04.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_04.TabIndex = 38;
            this.pn_Loader_04.Click += new System.EventHandler(this.pn_Loader_04_Click);
            // 
            // lbl_Loader_04
            // 
            this.lbl_Loader_04.AutoSize = true;
            this.lbl_Loader_04.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_04.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_04.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_04.Name = "lbl_Loader_04";
            this.lbl_Loader_04.Size = new System.Drawing.Size(240, 19);
            this.lbl_Loader_04.TabIndex = 0;
            this.lbl_Loader_04.Text = "04. CASSETTE_ROTATION_UP_LEFT";
            this.lbl_Loader_04.Click += new System.EventHandler(this.pn_Loader_04_Click);
            // 
            // pn_Loader_36
            // 
            this.pn_Loader_36.Controls.Add(this.lbl_Loader_36);
            this.pn_Loader_36.Location = new System.Drawing.Point(3, 127);
            this.pn_Loader_36.Name = "pn_Loader_36";
            this.pn_Loader_36.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_36.TabIndex = 37;
            this.pn_Loader_36.Click += new System.EventHandler(this.pn_Loader_36_Click);
            // 
            // lbl_Loader_36
            // 
            this.lbl_Loader_36.AutoSize = true;
            this.lbl_Loader_36.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_36.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_36.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_36.Name = "lbl_Loader_36";
            this.lbl_Loader_36.Size = new System.Drawing.Size(170, 19);
            this.lbl_Loader_36.TabIndex = 0;
            this.lbl_Loader_36.Text = "36. CELL_TRANSFER_X_2";
            this.lbl_Loader_36.Click += new System.EventHandler(this.pn_Loader_36_Click);
            // 
            // pn_Loader_35
            // 
            this.pn_Loader_35.Controls.Add(this.lbl_Loader_35);
            this.pn_Loader_35.Location = new System.Drawing.Point(3, 96);
            this.pn_Loader_35.Name = "pn_Loader_35";
            this.pn_Loader_35.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_35.TabIndex = 36;
            this.pn_Loader_35.Click += new System.EventHandler(this.pn_Loader_35_Click);
            // 
            // lbl_Loader_35
            // 
            this.lbl_Loader_35.AutoSize = true;
            this.lbl_Loader_35.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_35.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_35.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_35.Name = "lbl_Loader_35";
            this.lbl_Loader_35.Size = new System.Drawing.Size(170, 19);
            this.lbl_Loader_35.TabIndex = 0;
            this.lbl_Loader_35.Text = "35. CELL_TRANSFER_X_1";
            this.lbl_Loader_35.Click += new System.EventHandler(this.pn_Loader_35_Click);
            // 
            // pn_Loader_33
            // 
            this.pn_Loader_33.Controls.Add(this.lbl_Loader_33);
            this.pn_Loader_33.Location = new System.Drawing.Point(3, 65);
            this.pn_Loader_33.Name = "pn_Loader_33";
            this.pn_Loader_33.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_33.TabIndex = 35;
            this.pn_Loader_33.Click += new System.EventHandler(this.pn_Loader_33_Click);
            // 
            // lbl_Loader_33
            // 
            this.lbl_Loader_33.AutoSize = true;
            this.lbl_Loader_33.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_33.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_33.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_33.Name = "lbl_Loader_33";
            this.lbl_Loader_33.Size = new System.Drawing.Size(169, 19);
            this.lbl_Loader_33.TabIndex = 0;
            this.lbl_Loader_33.Text = "33. CELL_TRANSFER_Y_1";
            this.lbl_Loader_33.Click += new System.EventHandler(this.pn_Loader_33_Click);
            // 
            // pn_Loader_32
            // 
            this.pn_Loader_32.Controls.Add(this.lbl_Loader_32);
            this.pn_Loader_32.Location = new System.Drawing.Point(3, 34);
            this.pn_Loader_32.Name = "pn_Loader_32";
            this.pn_Loader_32.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_32.TabIndex = 34;
            this.pn_Loader_32.Click += new System.EventHandler(this.pn_Loader_32_Click);
            // 
            // lbl_Loader_32
            // 
            this.lbl_Loader_32.AutoSize = true;
            this.lbl_Loader_32.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_32.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_32.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_32.Name = "lbl_Loader_32";
            this.lbl_Loader_32.Size = new System.Drawing.Size(94, 19);
            this.lbl_Loader_32.TabIndex = 0;
            this.lbl_Loader_32.Text = "32. CELL_X_2";
            this.lbl_Loader_32.Click += new System.EventHandler(this.pn_Loader_32_Click);
            // 
            // pn_Loader_31
            // 
            this.pn_Loader_31.Controls.Add(this.lbl_Loader_31);
            this.pn_Loader_31.Location = new System.Drawing.Point(3, 3);
            this.pn_Loader_31.Name = "pn_Loader_31";
            this.pn_Loader_31.Size = new System.Drawing.Size(514, 25);
            this.pn_Loader_31.TabIndex = 33;
            this.pn_Loader_31.Click += new System.EventHandler(this.pn_Loader_31_Click);
            // 
            // lbl_Loader_31
            // 
            this.lbl_Loader_31.AutoSize = true;
            this.lbl_Loader_31.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Loader_31.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_31.Location = new System.Drawing.Point(3, 5);
            this.lbl_Loader_31.Name = "lbl_Loader_31";
            this.lbl_Loader_31.Size = new System.Drawing.Size(94, 19);
            this.lbl_Loader_31.TabIndex = 0;
            this.lbl_Loader_31.Text = "31. CELL_X_1";
            this.lbl_Loader_31.Click += new System.EventHandler(this.pn_Loader_31_Click);
            // 
            // tp_iostatus_process
            // 
            this.tp_iostatus_process.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_iostatus_process.Controls.Add(this.tableLayoutPanel2);
            this.tp_iostatus_process.Controls.Add(this.panel2);
            this.tp_iostatus_process.Font = new System.Drawing.Font("굴림", 20F);
            this.tp_iostatus_process.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process.Name = "tp_iostatus_process";
            this.tp_iostatus_process.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process.Size = new System.Drawing.Size(1726, 767);
            this.tp_iostatus_process.TabIndex = 1;
            this.tp_iostatus_process.Text = "프로세스";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Controls.Add(this.lbl_Process_AxisParameter);
            this.panel2.Controls.Add(this.tbox_Process_AccelationTime);
            this.panel2.Controls.Add(this.lbl_Process_JogSpeed);
            this.panel2.Controls.Add(this.tbox_Process_SWLimitHigh);
            this.panel2.Controls.Add(this.lbl_Process_PtpSpeed);
            this.panel2.Controls.Add(this.tbox_Process_SWLimitLow);
            this.panel2.Controls.Add(this.lbl_Process_SWLimitHigh);
            this.panel2.Controls.Add(this.tbox_Process_PtpSpeed);
            this.panel2.Controls.Add(this.lbl_Process_AxisNo);
            this.panel2.Controls.Add(this.tbox_Process_StepSpeed);
            this.panel2.Controls.Add(this.lbl_Process_HomeAcceleration);
            this.panel2.Controls.Add(this.tbox_Process_JogSpeed);
            this.panel2.Controls.Add(this.lbl_Process_AccelationTime);
            this.panel2.Controls.Add(this.tbox_Process_HomeAcceleration);
            this.panel2.Controls.Add(this.lbl_Process_InPosition);
            this.panel2.Controls.Add(this.tbox_Process_Home2Velocity);
            this.panel2.Controls.Add(this.lbl_Process_SpeedRate);
            this.panel2.Controls.Add(this.tbox_Process_Home1Velocity);
            this.panel2.Controls.Add(this.lbl_Process_PositionRate);
            this.panel2.Controls.Add(this.tbox_Process_DefaultAcceleration);
            this.panel2.Controls.Add(this.lbl_Process_StepSpeed);
            this.panel2.Controls.Add(this.tbox_Process_DefauleVelocity);
            this.panel2.Controls.Add(this.lbl_Process_DefauleVelocity);
            this.panel2.Controls.Add(this.tbox_Process_InPosition);
            this.panel2.Controls.Add(this.lbl_Process_MoveTimeOut);
            this.panel2.Controls.Add(this.tbox_Process_MoveTimeOut);
            this.panel2.Controls.Add(this.lbl_Process_DefaultAcceleration);
            this.panel2.Controls.Add(this.tbox_Process_SpeedRate);
            this.panel2.Controls.Add(this.lbl_Process_Home1Velocity);
            this.panel2.Controls.Add(this.tbox_Process_PositionRate);
            this.panel2.Controls.Add(this.lbl_Process_SWLimitLow);
            this.panel2.Controls.Add(this.tbox_Process_AxisNo);
            this.panel2.Controls.Add(this.lbl_Process_Home2Velocity);
            this.panel2.Location = new System.Drawing.Point(881, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(834, 745);
            this.panel2.TabIndex = 80;
            // 
            // lbl_Process_AxisParameter
            // 
            this.lbl_Process_AxisParameter.AutoSize = true;
            this.lbl_Process_AxisParameter.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_AxisParameter.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_AxisParameter.Location = new System.Drawing.Point(28, 24);
            this.lbl_Process_AxisParameter.Name = "lbl_Process_AxisParameter";
            this.lbl_Process_AxisParameter.Size = new System.Drawing.Size(125, 23);
            this.lbl_Process_AxisParameter.TabIndex = 24;
            this.lbl_Process_AxisParameter.Text = "Axis Parameter";
            // 
            // tbox_Process_AccelationTime
            // 
            this.tbox_Process_AccelationTime.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_AccelationTime.Location = new System.Drawing.Point(334, 496);
            this.tbox_Process_AccelationTime.Name = "tbox_Process_AccelationTime";
            this.tbox_Process_AccelationTime.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_AccelationTime.TabIndex = 79;
            // 
            // lbl_Process_JogSpeed
            // 
            this.lbl_Process_JogSpeed.AutoSize = true;
            this.lbl_Process_JogSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_JogSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_JogSpeed.Location = new System.Drawing.Point(28, 346);
            this.lbl_Process_JogSpeed.Name = "lbl_Process_JogSpeed";
            this.lbl_Process_JogSpeed.Size = new System.Drawing.Size(99, 23);
            this.lbl_Process_JogSpeed.TabIndex = 25;
            this.lbl_Process_JogSpeed.Text = "Jog Speed :";
            // 
            // tbox_Process_SWLimitHigh
            // 
            this.tbox_Process_SWLimitHigh.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_SWLimitHigh.Location = new System.Drawing.Point(334, 467);
            this.tbox_Process_SWLimitHigh.Name = "tbox_Process_SWLimitHigh";
            this.tbox_Process_SWLimitHigh.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_SWLimitHigh.TabIndex = 78;
            // 
            // lbl_Process_PtpSpeed
            // 
            this.lbl_Process_PtpSpeed.AutoSize = true;
            this.lbl_Process_PtpSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_PtpSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_PtpSpeed.Location = new System.Drawing.Point(28, 404);
            this.lbl_Process_PtpSpeed.Name = "lbl_Process_PtpSpeed";
            this.lbl_Process_PtpSpeed.Size = new System.Drawing.Size(99, 23);
            this.lbl_Process_PtpSpeed.TabIndex = 26;
            this.lbl_Process_PtpSpeed.Text = "Ptp Speed :";
            // 
            // tbox_Process_SWLimitLow
            // 
            this.tbox_Process_SWLimitLow.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_SWLimitLow.Location = new System.Drawing.Point(334, 438);
            this.tbox_Process_SWLimitLow.Name = "tbox_Process_SWLimitLow";
            this.tbox_Process_SWLimitLow.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_SWLimitLow.TabIndex = 77;
            // 
            // lbl_Process_SWLimitHigh
            // 
            this.lbl_Process_SWLimitHigh.AutoSize = true;
            this.lbl_Process_SWLimitHigh.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_SWLimitHigh.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_SWLimitHigh.Location = new System.Drawing.Point(28, 467);
            this.lbl_Process_SWLimitHigh.Name = "lbl_Process_SWLimitHigh";
            this.lbl_Process_SWLimitHigh.Size = new System.Drawing.Size(172, 23);
            this.lbl_Process_SWLimitHigh.TabIndex = 27;
            this.lbl_Process_SWLimitHigh.Text = "Software Limit High :";
            // 
            // tbox_Process_PtpSpeed
            // 
            this.tbox_Process_PtpSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_PtpSpeed.Location = new System.Drawing.Point(334, 409);
            this.tbox_Process_PtpSpeed.Name = "tbox_Process_PtpSpeed";
            this.tbox_Process_PtpSpeed.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_PtpSpeed.TabIndex = 76;
            // 
            // lbl_Process_AxisNo
            // 
            this.lbl_Process_AxisNo.AutoSize = true;
            this.lbl_Process_AxisNo.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_AxisNo.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_AxisNo.Location = new System.Drawing.Point(28, 61);
            this.lbl_Process_AxisNo.Name = "lbl_Process_AxisNo";
            this.lbl_Process_AxisNo.Size = new System.Drawing.Size(77, 23);
            this.lbl_Process_AxisNo.TabIndex = 28;
            this.lbl_Process_AxisNo.Text = "AxisNo. :";
            // 
            // tbox_Process_StepSpeed
            // 
            this.tbox_Process_StepSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_StepSpeed.Location = new System.Drawing.Point(334, 380);
            this.tbox_Process_StepSpeed.Name = "tbox_Process_StepSpeed";
            this.tbox_Process_StepSpeed.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_StepSpeed.TabIndex = 75;
            // 
            // lbl_Process_HomeAcceleration
            // 
            this.lbl_Process_HomeAcceleration.AutoSize = true;
            this.lbl_Process_HomeAcceleration.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_HomeAcceleration.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_HomeAcceleration.Location = new System.Drawing.Point(28, 322);
            this.lbl_Process_HomeAcceleration.Name = "lbl_Process_HomeAcceleration";
            this.lbl_Process_HomeAcceleration.Size = new System.Drawing.Size(166, 23);
            this.lbl_Process_HomeAcceleration.TabIndex = 29;
            this.lbl_Process_HomeAcceleration.Text = "Home Acceleration :";
            // 
            // tbox_Process_JogSpeed
            // 
            this.tbox_Process_JogSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_JogSpeed.Location = new System.Drawing.Point(334, 351);
            this.tbox_Process_JogSpeed.Name = "tbox_Process_JogSpeed";
            this.tbox_Process_JogSpeed.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_JogSpeed.TabIndex = 74;
            // 
            // lbl_Process_AccelationTime
            // 
            this.lbl_Process_AccelationTime.AutoSize = true;
            this.lbl_Process_AccelationTime.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_AccelationTime.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_AccelationTime.Location = new System.Drawing.Point(28, 496);
            this.lbl_Process_AccelationTime.Name = "lbl_Process_AccelationTime";
            this.lbl_Process_AccelationTime.Size = new System.Drawing.Size(142, 23);
            this.lbl_Process_AccelationTime.TabIndex = 30;
            this.lbl_Process_AccelationTime.Text = "Accelation Time :";
            // 
            // tbox_Process_HomeAcceleration
            // 
            this.tbox_Process_HomeAcceleration.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_HomeAcceleration.Location = new System.Drawing.Point(334, 322);
            this.tbox_Process_HomeAcceleration.Name = "tbox_Process_HomeAcceleration";
            this.tbox_Process_HomeAcceleration.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_HomeAcceleration.TabIndex = 73;
            // 
            // lbl_Process_InPosition
            // 
            this.lbl_Process_InPosition.AutoSize = true;
            this.lbl_Process_InPosition.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_InPosition.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_InPosition.Location = new System.Drawing.Point(28, 177);
            this.lbl_Process_InPosition.Name = "lbl_Process_InPosition";
            this.lbl_Process_InPosition.Size = new System.Drawing.Size(102, 23);
            this.lbl_Process_InPosition.TabIndex = 31;
            this.lbl_Process_InPosition.Text = "In Position :";
            // 
            // tbox_Process_Home2Velocity
            // 
            this.tbox_Process_Home2Velocity.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_Home2Velocity.Location = new System.Drawing.Point(334, 293);
            this.tbox_Process_Home2Velocity.Name = "tbox_Process_Home2Velocity";
            this.tbox_Process_Home2Velocity.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_Home2Velocity.TabIndex = 72;
            // 
            // lbl_Process_SpeedRate
            // 
            this.lbl_Process_SpeedRate.AutoSize = true;
            this.lbl_Process_SpeedRate.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_SpeedRate.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_SpeedRate.Location = new System.Drawing.Point(28, 119);
            this.lbl_Process_SpeedRate.Name = "lbl_Process_SpeedRate";
            this.lbl_Process_SpeedRate.Size = new System.Drawing.Size(107, 23);
            this.lbl_Process_SpeedRate.TabIndex = 32;
            this.lbl_Process_SpeedRate.Text = "Speed Rate :";
            // 
            // tbox_Process_Home1Velocity
            // 
            this.tbox_Process_Home1Velocity.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_Home1Velocity.Location = new System.Drawing.Point(334, 264);
            this.tbox_Process_Home1Velocity.Name = "tbox_Process_Home1Velocity";
            this.tbox_Process_Home1Velocity.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_Home1Velocity.TabIndex = 71;
            // 
            // lbl_Process_PositionRate
            // 
            this.lbl_Process_PositionRate.AutoSize = true;
            this.lbl_Process_PositionRate.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_PositionRate.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_PositionRate.Location = new System.Drawing.Point(28, 90);
            this.lbl_Process_PositionRate.Name = "lbl_Process_PositionRate";
            this.lbl_Process_PositionRate.Size = new System.Drawing.Size(121, 23);
            this.lbl_Process_PositionRate.TabIndex = 33;
            this.lbl_Process_PositionRate.Text = "Position Rate :";
            // 
            // tbox_Process_DefaultAcceleration
            // 
            this.tbox_Process_DefaultAcceleration.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_DefaultAcceleration.Location = new System.Drawing.Point(334, 235);
            this.tbox_Process_DefaultAcceleration.Name = "tbox_Process_DefaultAcceleration";
            this.tbox_Process_DefaultAcceleration.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_DefaultAcceleration.TabIndex = 70;
            // 
            // lbl_Process_StepSpeed
            // 
            this.lbl_Process_StepSpeed.AutoSize = true;
            this.lbl_Process_StepSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_StepSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_StepSpeed.Location = new System.Drawing.Point(28, 375);
            this.lbl_Process_StepSpeed.Name = "lbl_Process_StepSpeed";
            this.lbl_Process_StepSpeed.Size = new System.Drawing.Size(107, 23);
            this.lbl_Process_StepSpeed.TabIndex = 34;
            this.lbl_Process_StepSpeed.Text = "Step Speed :";
            // 
            // tbox_Process_DefauleVelocity
            // 
            this.tbox_Process_DefauleVelocity.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_DefauleVelocity.Location = new System.Drawing.Point(334, 206);
            this.tbox_Process_DefauleVelocity.Name = "tbox_Process_DefauleVelocity";
            this.tbox_Process_DefauleVelocity.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_DefauleVelocity.TabIndex = 69;
            // 
            // lbl_Process_DefauleVelocity
            // 
            this.lbl_Process_DefauleVelocity.AutoSize = true;
            this.lbl_Process_DefauleVelocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_DefauleVelocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_DefauleVelocity.Location = new System.Drawing.Point(28, 206);
            this.lbl_Process_DefauleVelocity.Name = "lbl_Process_DefauleVelocity";
            this.lbl_Process_DefauleVelocity.Size = new System.Drawing.Size(144, 23);
            this.lbl_Process_DefauleVelocity.TabIndex = 35;
            this.lbl_Process_DefauleVelocity.Text = "Defaule Velocity :";
            // 
            // tbox_Process_InPosition
            // 
            this.tbox_Process_InPosition.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_InPosition.Location = new System.Drawing.Point(334, 177);
            this.tbox_Process_InPosition.Name = "tbox_Process_InPosition";
            this.tbox_Process_InPosition.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_InPosition.TabIndex = 68;
            // 
            // lbl_Process_MoveTimeOut
            // 
            this.lbl_Process_MoveTimeOut.AutoSize = true;
            this.lbl_Process_MoveTimeOut.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_MoveTimeOut.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_MoveTimeOut.Location = new System.Drawing.Point(28, 148);
            this.lbl_Process_MoveTimeOut.Name = "lbl_Process_MoveTimeOut";
            this.lbl_Process_MoveTimeOut.Size = new System.Drawing.Size(141, 23);
            this.lbl_Process_MoveTimeOut.TabIndex = 36;
            this.lbl_Process_MoveTimeOut.Text = "Move Time Out :";
            // 
            // tbox_Process_MoveTimeOut
            // 
            this.tbox_Process_MoveTimeOut.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_MoveTimeOut.Location = new System.Drawing.Point(334, 148);
            this.tbox_Process_MoveTimeOut.Name = "tbox_Process_MoveTimeOut";
            this.tbox_Process_MoveTimeOut.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_MoveTimeOut.TabIndex = 67;
            // 
            // lbl_Process_DefaultAcceleration
            // 
            this.lbl_Process_DefaultAcceleration.AutoSize = true;
            this.lbl_Process_DefaultAcceleration.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_DefaultAcceleration.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_DefaultAcceleration.Location = new System.Drawing.Point(28, 235);
            this.lbl_Process_DefaultAcceleration.Name = "lbl_Process_DefaultAcceleration";
            this.lbl_Process_DefaultAcceleration.Size = new System.Drawing.Size(175, 23);
            this.lbl_Process_DefaultAcceleration.TabIndex = 37;
            this.lbl_Process_DefaultAcceleration.Text = "Default Acceleration :";
            // 
            // tbox_Process_SpeedRate
            // 
            this.tbox_Process_SpeedRate.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_SpeedRate.Location = new System.Drawing.Point(334, 119);
            this.tbox_Process_SpeedRate.Name = "tbox_Process_SpeedRate";
            this.tbox_Process_SpeedRate.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_SpeedRate.TabIndex = 66;
            // 
            // lbl_Process_Home1Velocity
            // 
            this.lbl_Process_Home1Velocity.AutoSize = true;
            this.lbl_Process_Home1Velocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_Home1Velocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_Home1Velocity.Location = new System.Drawing.Point(28, 264);
            this.lbl_Process_Home1Velocity.Name = "lbl_Process_Home1Velocity";
            this.lbl_Process_Home1Velocity.Size = new System.Drawing.Size(169, 23);
            this.lbl_Process_Home1Velocity.TabIndex = 38;
            this.lbl_Process_Home1Velocity.Text = "Home First Velocity :";
            // 
            // tbox_Process_PositionRate
            // 
            this.tbox_Process_PositionRate.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_PositionRate.Location = new System.Drawing.Point(334, 90);
            this.tbox_Process_PositionRate.Name = "tbox_Process_PositionRate";
            this.tbox_Process_PositionRate.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_PositionRate.TabIndex = 65;
            // 
            // lbl_Process_SWLimitLow
            // 
            this.lbl_Process_SWLimitLow.AutoSize = true;
            this.lbl_Process_SWLimitLow.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_SWLimitLow.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_SWLimitLow.Location = new System.Drawing.Point(28, 438);
            this.lbl_Process_SWLimitLow.Name = "lbl_Process_SWLimitLow";
            this.lbl_Process_SWLimitLow.Size = new System.Drawing.Size(167, 23);
            this.lbl_Process_SWLimitLow.TabIndex = 39;
            this.lbl_Process_SWLimitLow.Text = "Software Limit Low :";
            // 
            // tbox_Process_AxisNo
            // 
            this.tbox_Process_AxisNo.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Process_AxisNo.Location = new System.Drawing.Point(334, 61);
            this.tbox_Process_AxisNo.Name = "tbox_Process_AxisNo";
            this.tbox_Process_AxisNo.Size = new System.Drawing.Size(333, 23);
            this.tbox_Process_AxisNo.TabIndex = 64;
            // 
            // lbl_Process_Home2Velocity
            // 
            this.lbl_Process_Home2Velocity.AutoSize = true;
            this.lbl_Process_Home2Velocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_Home2Velocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_Home2Velocity.Location = new System.Drawing.Point(28, 293);
            this.lbl_Process_Home2Velocity.Name = "lbl_Process_Home2Velocity";
            this.lbl_Process_Home2Velocity.Size = new System.Drawing.Size(194, 23);
            this.lbl_Process_Home2Velocity.TabIndex = 40;
            this.lbl_Process_Home2Velocity.Text = "Home Second Velocity :";
            // 
            // tp_iostatus_uld
            // 
            this.tp_iostatus_uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_iostatus_uld.Controls.Add(this.tableLayoutPanel3);
            this.tp_iostatus_uld.Controls.Add(this.panel3);
            this.tp_iostatus_uld.Font = new System.Drawing.Font("굴림", 20F);
            this.tp_iostatus_uld.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_uld.Name = "tp_iostatus_uld";
            this.tp_iostatus_uld.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_uld.Size = new System.Drawing.Size(1726, 767);
            this.tp_iostatus_uld.TabIndex = 2;
            this.tp_iostatus_uld.Text = "언로더";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DimGray;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(881, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(834, 745);
            this.panel3.TabIndex = 81;
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.DimGray;
            this.btn_Save.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Save.Location = new System.Drawing.Point(1642, 834);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(80, 30);
            this.btn_Save.TabIndex = 31;
            this.btn_Save.Text = "저장";
            this.btn_Save.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_21, 0, 18);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_19, 0, 16);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_18, 0, 15);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_14, 0, 14);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_13, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_12, 0, 12);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_11, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_10, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_09, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_22, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_07, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_06, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_05, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_04, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_17, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_03, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_02, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_01, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pn_Process_20, 0, 17);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 26);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 19;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(520, 593);
            this.tableLayoutPanel2.TabIndex = 81;
            // 
            // pn_Process_19
            // 
            this.pn_Process_19.Controls.Add(this.lbl_Process_19);
            this.pn_Process_19.Location = new System.Drawing.Point(3, 499);
            this.pn_Process_19.Name = "pn_Process_19";
            this.pn_Process_19.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_19.TabIndex = 49;
            this.pn_Process_19.Click += new System.EventHandler(this.pn_Process_19_Click);
            // 
            // lbl_Process_19
            // 
            this.lbl_Process_19.AutoSize = true;
            this.lbl_Process_19.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_19.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_19.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_19.Name = "lbl_Process_19";
            this.lbl_Process_19.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_19.TabIndex = 0;
            this.lbl_Process_19.Text = "19. BREAK_Z_2";
            this.lbl_Process_19.Click += new System.EventHandler(this.pn_Process_19_Click);
            // 
            // pn_Process_18
            // 
            this.pn_Process_18.Controls.Add(this.lbl_Process_18);
            this.pn_Process_18.Location = new System.Drawing.Point(3, 468);
            this.pn_Process_18.Name = "pn_Process_18";
            this.pn_Process_18.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_18.TabIndex = 48;
            this.pn_Process_18.Click += new System.EventHandler(this.pn_Process_18_Click);
            // 
            // lbl_Process_18
            // 
            this.lbl_Process_18.AutoSize = true;
            this.lbl_Process_18.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_18.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_18.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_18.Name = "lbl_Process_18";
            this.lbl_Process_18.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_18.TabIndex = 0;
            this.lbl_Process_18.Text = "18. BREAK_Z_1";
            this.lbl_Process_18.Click += new System.EventHandler(this.pn_Process_18_Click);
            // 
            // pn_Process_14
            // 
            this.pn_Process_14.Controls.Add(this.lbl_Process_14);
            this.pn_Process_14.Location = new System.Drawing.Point(3, 437);
            this.pn_Process_14.Name = "pn_Process_14";
            this.pn_Process_14.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_14.TabIndex = 47;
            this.pn_Process_14.Click += new System.EventHandler(this.pn_Process_14_Click);
            // 
            // lbl_Process_14
            // 
            this.lbl_Process_14.AutoSize = true;
            this.lbl_Process_14.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_14.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_14.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_14.Name = "lbl_Process_14";
            this.lbl_Process_14.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_14.TabIndex = 0;
            this.lbl_Process_14.Text = "14. BREAK_X_4";
            this.lbl_Process_14.Click += new System.EventHandler(this.pn_Process_14_Click);
            // 
            // pn_Process_13
            // 
            this.pn_Process_13.Controls.Add(this.lbl_Process_13);
            this.pn_Process_13.Location = new System.Drawing.Point(3, 406);
            this.pn_Process_13.Name = "pn_Process_13";
            this.pn_Process_13.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_13.TabIndex = 46;
            this.pn_Process_13.Click += new System.EventHandler(this.pn_Process_13_Click);
            // 
            // lbl_Process_13
            // 
            this.lbl_Process_13.AutoSize = true;
            this.lbl_Process_13.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_13.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_13.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_13.Name = "lbl_Process_13";
            this.lbl_Process_13.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_13.TabIndex = 0;
            this.lbl_Process_13.Text = "13. BREAK_X_3";
            this.lbl_Process_13.Click += new System.EventHandler(this.pn_Process_13_Click);
            // 
            // pn_Process_12
            // 
            this.pn_Process_12.Controls.Add(this.lbl_Process_12);
            this.pn_Process_12.Location = new System.Drawing.Point(3, 375);
            this.pn_Process_12.Name = "pn_Process_12";
            this.pn_Process_12.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_12.TabIndex = 45;
            this.pn_Process_12.Click += new System.EventHandler(this.pn_Process_12_Click);
            // 
            // lbl_Process_12
            // 
            this.lbl_Process_12.AutoSize = true;
            this.lbl_Process_12.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_12.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_12.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_12.Name = "lbl_Process_12";
            this.lbl_Process_12.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_12.TabIndex = 0;
            this.lbl_Process_12.Text = "12. BREAK_X_2";
            this.lbl_Process_12.Click += new System.EventHandler(this.pn_Process_12_Click);
            // 
            // pn_Process_11
            // 
            this.pn_Process_11.Controls.Add(this.lbl_Process_11);
            this.pn_Process_11.Location = new System.Drawing.Point(3, 344);
            this.pn_Process_11.Name = "pn_Process_11";
            this.pn_Process_11.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_11.TabIndex = 44;
            this.pn_Process_11.Click += new System.EventHandler(this.pn_Process_11_Click);
            // 
            // lbl_Process_11
            // 
            this.lbl_Process_11.AutoSize = true;
            this.lbl_Process_11.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_11.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_11.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_11.Name = "lbl_Process_11";
            this.lbl_Process_11.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_11.TabIndex = 0;
            this.lbl_Process_11.Text = "11. BREAK_X_1";
            this.lbl_Process_11.Click += new System.EventHandler(this.pn_Process_11_Click);
            // 
            // pn_Process_10
            // 
            this.pn_Process_10.Controls.Add(this.lbl_Process_10);
            this.pn_Process_10.Location = new System.Drawing.Point(3, 313);
            this.pn_Process_10.Name = "pn_Process_10";
            this.pn_Process_10.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_10.TabIndex = 43;
            this.pn_Process_10.Click += new System.EventHandler(this.pn_Process_10_Click);
            // 
            // lbl_Process_10
            // 
            this.lbl_Process_10.AutoSize = true;
            this.lbl_Process_10.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_10.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_10.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_10.Name = "lbl_Process_10";
            this.lbl_Process_10.Size = new System.Drawing.Size(155, 19);
            this.lbl_Process_10.TabIndex = 0;
            this.lbl_Process_10.Text = "10. BREAK_TABLE_Y_2";
            this.lbl_Process_10.Click += new System.EventHandler(this.pn_Process_10_Click);
            // 
            // pn_Process_09
            // 
            this.pn_Process_09.Controls.Add(this.lbl_Process_09);
            this.pn_Process_09.Location = new System.Drawing.Point(3, 282);
            this.pn_Process_09.Name = "pn_Process_09";
            this.pn_Process_09.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_09.TabIndex = 42;
            this.pn_Process_09.Click += new System.EventHandler(this.pn_Process_09_Click);
            // 
            // lbl_Process_09
            // 
            this.lbl_Process_09.AutoSize = true;
            this.lbl_Process_09.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_09.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_09.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_09.Name = "lbl_Process_09";
            this.lbl_Process_09.Size = new System.Drawing.Size(155, 19);
            this.lbl_Process_09.TabIndex = 0;
            this.lbl_Process_09.Text = "09. BREAK_TABLE_Y_1";
            this.lbl_Process_09.Click += new System.EventHandler(this.pn_Process_09_Click);
            // 
            // pn_Process_22
            // 
            this.pn_Process_22.Controls.Add(this.lbl_Process_22);
            this.pn_Process_22.Location = new System.Drawing.Point(3, 251);
            this.pn_Process_22.Name = "pn_Process_22";
            this.pn_Process_22.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_22.TabIndex = 41;
            this.pn_Process_22.Click += new System.EventHandler(this.pn_Process_22_Click);
            // 
            // lbl_Process_22
            // 
            this.lbl_Process_22.AutoSize = true;
            this.lbl_Process_22.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_22.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_22.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_22.Name = "lbl_Process_22";
            this.lbl_Process_22.Size = new System.Drawing.Size(122, 19);
            this.lbl_Process_22.TabIndex = 0;
            this.lbl_Process_22.Text = "22. MASURE_X_1";
            this.lbl_Process_22.Click += new System.EventHandler(this.pn_Process_22_Click);
            // 
            // pn_Process_07
            // 
            this.pn_Process_07.Controls.Add(this.lbl_Process_07);
            this.pn_Process_07.Location = new System.Drawing.Point(3, 220);
            this.pn_Process_07.Name = "pn_Process_07";
            this.pn_Process_07.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_07.TabIndex = 40;
            this.pn_Process_07.Click += new System.EventHandler(this.pn_Process_07_Click);
            // 
            // lbl_Process_07
            // 
            this.lbl_Process_07.AutoSize = true;
            this.lbl_Process_07.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_07.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_07.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_07.Name = "lbl_Process_07";
            this.lbl_Process_07.Size = new System.Drawing.Size(169, 19);
            this.lbl_Process_07.TabIndex = 0;
            this.lbl_Process_07.Text = "07. CELL_TRANSFER_Y_1";
            this.lbl_Process_07.Click += new System.EventHandler(this.pn_Process_07_Click);
            // 
            // pn_Process_06
            // 
            this.pn_Process_06.Controls.Add(this.lbl_Process_06);
            this.pn_Process_06.Location = new System.Drawing.Point(3, 189);
            this.pn_Process_06.Name = "pn_Process_06";
            this.pn_Process_06.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_06.TabIndex = 39;
            this.pn_Process_06.Click += new System.EventHandler(this.pn_Process_06_Click);
            // 
            // lbl_Process_06
            // 
            this.lbl_Process_06.AutoSize = true;
            this.lbl_Process_06.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_06.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_06.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_06.Name = "lbl_Process_06";
            this.lbl_Process_06.Size = new System.Drawing.Size(170, 19);
            this.lbl_Process_06.TabIndex = 0;
            this.lbl_Process_06.Text = "06. CELL_TRANSFER_X_2";
            this.lbl_Process_06.Click += new System.EventHandler(this.pn_Process_06_Click);
            // 
            // pn_Process_05
            // 
            this.pn_Process_05.Controls.Add(this.lbl_Process_05);
            this.pn_Process_05.Location = new System.Drawing.Point(3, 158);
            this.pn_Process_05.Name = "pn_Process_05";
            this.pn_Process_05.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_05.TabIndex = 38;
            this.pn_Process_05.Click += new System.EventHandler(this.pn_Process_05_Click);
            // 
            // lbl_Process_05
            // 
            this.lbl_Process_05.AutoSize = true;
            this.lbl_Process_05.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_05.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_05.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_05.Name = "lbl_Process_05";
            this.lbl_Process_05.Size = new System.Drawing.Size(170, 19);
            this.lbl_Process_05.TabIndex = 0;
            this.lbl_Process_05.Text = "05. CELL_TRANSFER_X_1";
            this.lbl_Process_05.Click += new System.EventHandler(this.pn_Process_05_Click);
            // 
            // pn_Process_04
            // 
            this.pn_Process_04.Controls.Add(this.lbl_Process_04);
            this.pn_Process_04.Location = new System.Drawing.Point(3, 127);
            this.pn_Process_04.Name = "pn_Process_04";
            this.pn_Process_04.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_04.TabIndex = 37;
            this.pn_Process_04.Click += new System.EventHandler(this.pn_Process_04_Click);
            // 
            // lbl_Process_04
            // 
            this.lbl_Process_04.AutoSize = true;
            this.lbl_Process_04.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_04.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_04.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_04.Name = "lbl_Process_04";
            this.lbl_Process_04.Size = new System.Drawing.Size(105, 19);
            this.lbl_Process_04.TabIndex = 0;
            this.lbl_Process_04.Text = "04. ALIGN_X_1";
            this.lbl_Process_04.Click += new System.EventHandler(this.pn_Process_04_Click);
            // 
            // pn_Process_17
            // 
            this.pn_Process_17.Controls.Add(this.lbl_Process_17);
            this.pn_Process_17.Location = new System.Drawing.Point(3, 96);
            this.pn_Process_17.Name = "pn_Process_17";
            this.pn_Process_17.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_17.TabIndex = 36;
            this.pn_Process_17.Click += new System.EventHandler(this.pn_Process_17_Click);
            // 
            // lbl_Process_17
            // 
            this.lbl_Process_17.AutoSize = true;
            this.lbl_Process_17.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_17.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_17.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_17.Name = "lbl_Process_17";
            this.lbl_Process_17.Size = new System.Drawing.Size(102, 19);
            this.lbl_Process_17.TabIndex = 0;
            this.lbl_Process_17.Text = "17. HEAD_Z_1";
            this.lbl_Process_17.Click += new System.EventHandler(this.pn_Process_17_Click);
            // 
            // pn_Process_03
            // 
            this.pn_Process_03.Controls.Add(this.lbl_Process_03);
            this.pn_Process_03.Location = new System.Drawing.Point(3, 65);
            this.pn_Process_03.Name = "pn_Process_03";
            this.pn_Process_03.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_03.TabIndex = 35;
            this.pn_Process_03.Click += new System.EventHandler(this.pn_Process_03_Click);
            // 
            // lbl_Process_03
            // 
            this.lbl_Process_03.AutoSize = true;
            this.lbl_Process_03.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_03.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_03.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_03.Name = "lbl_Process_03";
            this.lbl_Process_03.Size = new System.Drawing.Size(102, 19);
            this.lbl_Process_03.TabIndex = 0;
            this.lbl_Process_03.Text = "03. HEAD_X_1";
            this.lbl_Process_03.Click += new System.EventHandler(this.pn_Process_03_Click);
            // 
            // pn_Process_02
            // 
            this.pn_Process_02.Controls.Add(this.lbl_Process_02);
            this.pn_Process_02.Location = new System.Drawing.Point(3, 34);
            this.pn_Process_02.Name = "pn_Process_02";
            this.pn_Process_02.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_02.TabIndex = 34;
            this.pn_Process_02.Click += new System.EventHandler(this.pn_Process_02_Click);
            // 
            // lbl_Process_02
            // 
            this.lbl_Process_02.AutoSize = true;
            this.lbl_Process_02.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_02.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_02.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_02.Name = "lbl_Process_02";
            this.lbl_Process_02.Size = new System.Drawing.Size(104, 19);
            this.lbl_Process_02.TabIndex = 0;
            this.lbl_Process_02.Text = "02. TABLE_Y_2";
            this.lbl_Process_02.Click += new System.EventHandler(this.pn_Process_02_Click);
            // 
            // pn_Process_01
            // 
            this.pn_Process_01.Controls.Add(this.lbl_Process_01);
            this.pn_Process_01.Location = new System.Drawing.Point(3, 3);
            this.pn_Process_01.Name = "pn_Process_01";
            this.pn_Process_01.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_01.TabIndex = 33;
            this.pn_Process_01.Click += new System.EventHandler(this.pn_Process_01_Click);
            // 
            // lbl_Process_01
            // 
            this.lbl_Process_01.AutoSize = true;
            this.lbl_Process_01.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_01.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_01.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_01.Name = "lbl_Process_01";
            this.lbl_Process_01.Size = new System.Drawing.Size(104, 19);
            this.lbl_Process_01.TabIndex = 0;
            this.lbl_Process_01.Text = "01. TABLE_Y_1";
            this.lbl_Process_01.Click += new System.EventHandler(this.pn_Process_01_Click);
            // 
            // pn_Process_20
            // 
            this.pn_Process_20.Controls.Add(this.lbl_Process_20);
            this.pn_Process_20.Location = new System.Drawing.Point(3, 530);
            this.pn_Process_20.Name = "pn_Process_20";
            this.pn_Process_20.Size = new System.Drawing.Size(514, 25);
            this.pn_Process_20.TabIndex = 50;
            this.pn_Process_20.Click += new System.EventHandler(this.pn_Process_20_Click);
            // 
            // lbl_Process_20
            // 
            this.lbl_Process_20.AutoSize = true;
            this.lbl_Process_20.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_20.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_20.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_20.Name = "lbl_Process_20";
            this.lbl_Process_20.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_20.TabIndex = 0;
            this.lbl_Process_20.Text = "20. BREAK_Z_3";
            this.lbl_Process_20.Click += new System.EventHandler(this.pn_Process_20_Click);
            // 
            // pn_Process_21
            // 
            this.pn_Process_21.Controls.Add(this.lbl_Process_21);
            this.pn_Process_21.Location = new System.Drawing.Point(3, 561);
            this.pn_Process_21.Name = "pn_Process_21";
            this.pn_Process_21.Size = new System.Drawing.Size(514, 29);
            this.pn_Process_21.TabIndex = 51;
            this.pn_Process_21.Click += new System.EventHandler(this.pn_Process_21_Click);
            // 
            // lbl_Process_21
            // 
            this.lbl_Process_21.AutoSize = true;
            this.lbl_Process_21.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Process_21.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_21.Location = new System.Drawing.Point(3, 5);
            this.lbl_Process_21.Name = "lbl_Process_21";
            this.lbl_Process_21.Size = new System.Drawing.Size(108, 19);
            this.lbl_Process_21.TabIndex = 0;
            this.lbl_Process_21.Text = "21. BREAK_Z_4";
            this.lbl_Process_21.Click += new System.EventHandler(this.pn_Process_21_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_3, 0, 21);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_0, 0, 18);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_30, 0, 17);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_29, 0, 16);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_28, 0, 15);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_27, 0, 14);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_26, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_24, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_23, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_21, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_20, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_19, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_18, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_17, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_16, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_41, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_40, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_39, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_38, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_1, 0, 19);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_37, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.pn_Unloader_2, 0, 20);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 26);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 22;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(520, 706);
            this.tableLayoutPanel3.TabIndex = 82;
            // 
            // pn_Unloader_37
            // 
            this.pn_Unloader_37.Controls.Add(this.lbl_Unloader_37);
            this.pn_Unloader_37.Location = new System.Drawing.Point(3, 3);
            this.pn_Unloader_37.Name = "pn_Unloader_37";
            this.pn_Unloader_37.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_37.TabIndex = 33;
            this.pn_Unloader_37.Click += new System.EventHandler(this.pn_Unloader_37_Click);
            // 
            // lbl_Unloader_37
            // 
            this.lbl_Unloader_37.AutoSize = true;
            this.lbl_Unloader_37.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_37.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_37.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_37.Name = "lbl_Unloader_37";
            this.lbl_Unloader_37.Size = new System.Drawing.Size(169, 19);
            this.lbl_Unloader_37.TabIndex = 0;
            this.lbl_Unloader_37.Text = "37. CELL_TRANSFER_Y_1";
            this.lbl_Unloader_37.Click += new System.EventHandler(this.pn_Unloader_37_Click);
            // 
            // pn_Unloader_2
            // 
            this.pn_Unloader_2.Controls.Add(this.lbl_Unloader_2);
            this.pn_Unloader_2.Location = new System.Drawing.Point(3, 643);
            this.pn_Unloader_2.Name = "pn_Unloader_2";
            this.pn_Unloader_2.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_2.TabIndex = 34;
            this.pn_Unloader_2.Click += new System.EventHandler(this.pn_Unloader_2_Click);
            // 
            // lbl_Unloader_2
            // 
            this.lbl_Unloader_2.AutoSize = true;
            this.lbl_Unloader_2.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_2.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_2.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_2.Name = "lbl_Unloader_2";
            this.lbl_Unloader_2.Size = new System.Drawing.Size(147, 19);
            this.lbl_Unloader_2.TabIndex = 0;
            this.lbl_Unloader_2.Text = "2. BREAK_TABLE_T_3";
            this.lbl_Unloader_2.Click += new System.EventHandler(this.pn_Unloader_2_Click);
            // 
            // pn_Unloader_1
            // 
            this.pn_Unloader_1.Controls.Add(this.lbl_Unloader_1);
            this.pn_Unloader_1.Location = new System.Drawing.Point(3, 611);
            this.pn_Unloader_1.Name = "pn_Unloader_1";
            this.pn_Unloader_1.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_1.TabIndex = 34;
            this.pn_Unloader_1.Click += new System.EventHandler(this.pn_Unloader_1_Click);
            // 
            // lbl_Unloader_1
            // 
            this.lbl_Unloader_1.AutoSize = true;
            this.lbl_Unloader_1.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_1.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_1.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_1.Name = "lbl_Unloader_1";
            this.lbl_Unloader_1.Size = new System.Drawing.Size(147, 19);
            this.lbl_Unloader_1.TabIndex = 0;
            this.lbl_Unloader_1.Text = "1. BREAK_TABLE_T_2";
            this.lbl_Unloader_1.Click += new System.EventHandler(this.pn_Unloader_1_Click);
            // 
            // pn_Unloader_38
            // 
            this.pn_Unloader_38.Controls.Add(this.lbl_Unloader_38);
            this.pn_Unloader_38.Location = new System.Drawing.Point(3, 35);
            this.pn_Unloader_38.Name = "pn_Unloader_38";
            this.pn_Unloader_38.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_38.TabIndex = 35;
            this.pn_Unloader_38.Click += new System.EventHandler(this.pn_Unloader_38_Click);
            // 
            // lbl_Unloader_38
            // 
            this.lbl_Unloader_38.AutoSize = true;
            this.lbl_Unloader_38.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_38.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_38.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_38.Name = "lbl_Unloader_38";
            this.lbl_Unloader_38.Size = new System.Drawing.Size(169, 19);
            this.lbl_Unloader_38.TabIndex = 0;
            this.lbl_Unloader_38.Text = "38. CELL_TRANSFER_Y_2";
            this.lbl_Unloader_38.Click += new System.EventHandler(this.pn_Unloader_38_Click);
            // 
            // pn_Unloader_39
            // 
            this.pn_Unloader_39.Controls.Add(this.lbl_Unloader_39);
            this.pn_Unloader_39.Location = new System.Drawing.Point(3, 67);
            this.pn_Unloader_39.Name = "pn_Unloader_39";
            this.pn_Unloader_39.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_39.TabIndex = 36;
            this.pn_Unloader_39.Click += new System.EventHandler(this.pn_Unloader_39_Click);
            // 
            // lbl_Unloader_39
            // 
            this.lbl_Unloader_39.AutoSize = true;
            this.lbl_Unloader_39.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_39.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_39.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_39.Name = "lbl_Unloader_39";
            this.lbl_Unloader_39.Size = new System.Drawing.Size(95, 19);
            this.lbl_Unloader_39.TabIndex = 0;
            this.lbl_Unloader_39.Text = "39. INSP_X_1";
            this.lbl_Unloader_39.Click += new System.EventHandler(this.pn_Unloader_39_Click);
            // 
            // pn_Unloader_40
            // 
            this.pn_Unloader_40.Controls.Add(this.lbl_Unloader_40);
            this.pn_Unloader_40.Location = new System.Drawing.Point(3, 99);
            this.pn_Unloader_40.Name = "pn_Unloader_40";
            this.pn_Unloader_40.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_40.TabIndex = 37;
            this.pn_Unloader_40.Click += new System.EventHandler(this.pn_Unloader_40_Click);
            // 
            // lbl_Unloader_40
            // 
            this.lbl_Unloader_40.AutoSize = true;
            this.lbl_Unloader_40.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_40.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_40.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_40.Name = "lbl_Unloader_40";
            this.lbl_Unloader_40.Size = new System.Drawing.Size(94, 19);
            this.lbl_Unloader_40.TabIndex = 0;
            this.lbl_Unloader_40.Text = "40. CELL_X_1";
            this.lbl_Unloader_40.Click += new System.EventHandler(this.pn_Unloader_40_Click);
            // 
            // pn_Unloader_41
            // 
            this.pn_Unloader_41.Controls.Add(this.lbl_Unloader_41);
            this.pn_Unloader_41.Location = new System.Drawing.Point(3, 131);
            this.pn_Unloader_41.Name = "pn_Unloader_41";
            this.pn_Unloader_41.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_41.TabIndex = 38;
            this.pn_Unloader_41.Click += new System.EventHandler(this.pn_Unloader_41_Click);
            // 
            // lbl_Unloader_41
            // 
            this.lbl_Unloader_41.AutoSize = true;
            this.lbl_Unloader_41.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_41.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_41.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_41.Name = "lbl_Unloader_41";
            this.lbl_Unloader_41.Size = new System.Drawing.Size(94, 19);
            this.lbl_Unloader_41.TabIndex = 0;
            this.lbl_Unloader_41.Text = "41. CELL_X_2";
            this.lbl_Unloader_41.Click += new System.EventHandler(this.pn_Unloader_41_Click);
            // 
            // pn_Unloader_16
            // 
            this.pn_Unloader_16.Controls.Add(this.lbl_Unloader_16);
            this.pn_Unloader_16.Location = new System.Drawing.Point(3, 163);
            this.pn_Unloader_16.Name = "pn_Unloader_16";
            this.pn_Unloader_16.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_16.TabIndex = 39;
            this.pn_Unloader_16.Click += new System.EventHandler(this.pn_Unloader_16_Click);
            // 
            // lbl_Unloader_16
            // 
            this.lbl_Unloader_16.AutoSize = true;
            this.lbl_Unloader_16.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_16.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_16.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_16.Name = "lbl_Unloader_16";
            this.lbl_Unloader_16.Size = new System.Drawing.Size(240, 19);
            this.lbl_Unloader_16.TabIndex = 0;
            this.lbl_Unloader_16.Text = "16. CASSETTE_ROTATION_UP_LEFT";
            this.lbl_Unloader_16.Click += new System.EventHandler(this.pn_Unloader_16_Click);
            // 
            // pn_Unloader_17
            // 
            this.pn_Unloader_17.Controls.Add(this.lbl_Unloader_17);
            this.pn_Unloader_17.Location = new System.Drawing.Point(3, 195);
            this.pn_Unloader_17.Name = "pn_Unloader_17";
            this.pn_Unloader_17.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_17.TabIndex = 40;
            this.pn_Unloader_17.Click += new System.EventHandler(this.pn_Unloader_17_Click);
            // 
            // lbl_Unloader_17
            // 
            this.lbl_Unloader_17.AutoSize = true;
            this.lbl_Unloader_17.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_17.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_17.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_17.Name = "lbl_Unloader_17";
            this.lbl_Unloader_17.Size = new System.Drawing.Size(267, 19);
            this.lbl_Unloader_17.TabIndex = 0;
            this.lbl_Unloader_17.Text = "17. CASSETTE_ROTATION_DOWN_LEFT";
            this.lbl_Unloader_17.Click += new System.EventHandler(this.pn_Unloader_17_Click);
            // 
            // pn_Unloader_18
            // 
            this.pn_Unloader_18.Controls.Add(this.lbl_Unloader_18);
            this.pn_Unloader_18.Location = new System.Drawing.Point(3, 227);
            this.pn_Unloader_18.Name = "pn_Unloader_18";
            this.pn_Unloader_18.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_18.TabIndex = 41;
            this.pn_Unloader_18.Click += new System.EventHandler(this.pn_Unloader_18_Click);
            // 
            // lbl_Unloader_18
            // 
            this.lbl_Unloader_18.AutoSize = true;
            this.lbl_Unloader_18.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_18.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_18.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_18.Name = "lbl_Unloader_18";
            this.lbl_Unloader_18.Size = new System.Drawing.Size(236, 19);
            this.lbl_Unloader_18.TabIndex = 0;
            this.lbl_Unloader_18.Text = "18. CASSETTE_ELECTRICTY_Z_LEFT";
            this.lbl_Unloader_18.Click += new System.EventHandler(this.pn_Unloader_18_Click);
            // 
            // pn_Unloader_19
            // 
            this.pn_Unloader_19.Controls.Add(this.lbl_Unloader_19);
            this.pn_Unloader_19.Location = new System.Drawing.Point(3, 259);
            this.pn_Unloader_19.Name = "pn_Unloader_19";
            this.pn_Unloader_19.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_19.TabIndex = 42;
            this.pn_Unloader_19.Click += new System.EventHandler(this.pn_Unloader_19_Click);
            // 
            // lbl_Unloader_19
            // 
            this.lbl_Unloader_19.AutoSize = true;
            this.lbl_Unloader_19.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_19.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_19.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_19.Name = "lbl_Unloader_19";
            this.lbl_Unloader_19.Size = new System.Drawing.Size(248, 19);
            this.lbl_Unloader_19.TabIndex = 0;
            this.lbl_Unloader_19.Text = "19. CASSETTE_ELECTRICTY_Z_RIGHT";
            this.lbl_Unloader_19.Click += new System.EventHandler(this.pn_Unloader_19_Click);
            // 
            // pn_Unloader_20
            // 
            this.pn_Unloader_20.Controls.Add(this.lbl_Unloader_20);
            this.pn_Unloader_20.Location = new System.Drawing.Point(3, 291);
            this.pn_Unloader_20.Name = "pn_Unloader_20";
            this.pn_Unloader_20.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_20.TabIndex = 43;
            this.pn_Unloader_20.Click += new System.EventHandler(this.pn_Unloader_20_Click);
            // 
            // lbl_Unloader_20
            // 
            this.lbl_Unloader_20.AutoSize = true;
            this.lbl_Unloader_20.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_20.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_20.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_20.Name = "lbl_Unloader_20";
            this.lbl_Unloader_20.Size = new System.Drawing.Size(169, 19);
            this.lbl_Unloader_20.TabIndex = 0;
            this.lbl_Unloader_20.Text = "20. CELL_TRANSFER_T_1";
            this.lbl_Unloader_20.Click += new System.EventHandler(this.pn_Unloader_20_Click);
            // 
            // pn_Unloader_21
            // 
            this.pn_Unloader_21.Controls.Add(this.lbl_Unloader_21);
            this.pn_Unloader_21.Location = new System.Drawing.Point(3, 323);
            this.pn_Unloader_21.Name = "pn_Unloader_21";
            this.pn_Unloader_21.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_21.TabIndex = 44;
            this.pn_Unloader_21.Click += new System.EventHandler(this.pn_Unloader_21_Click);
            // 
            // lbl_Unloader_21
            // 
            this.lbl_Unloader_21.AutoSize = true;
            this.lbl_Unloader_21.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_21.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_21.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_21.Name = "lbl_Unloader_21";
            this.lbl_Unloader_21.Size = new System.Drawing.Size(169, 19);
            this.lbl_Unloader_21.TabIndex = 0;
            this.lbl_Unloader_21.Text = "21. CELL_TRANSFER_T_2";
            this.lbl_Unloader_21.Click += new System.EventHandler(this.pn_Unloader_21_Click);
            // 
            // pn_Unloader_23
            // 
            this.pn_Unloader_23.Controls.Add(this.lbl_Unloader_23);
            this.pn_Unloader_23.Location = new System.Drawing.Point(3, 355);
            this.pn_Unloader_23.Name = "pn_Unloader_23";
            this.pn_Unloader_23.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_23.TabIndex = 45;
            this.pn_Unloader_23.Click += new System.EventHandler(this.pn_Unloader_23_Click);
            // 
            // lbl_Unloader_23
            // 
            this.lbl_Unloader_23.AutoSize = true;
            this.lbl_Unloader_23.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_23.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_23.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_23.Name = "lbl_Unloader_23";
            this.lbl_Unloader_23.Size = new System.Drawing.Size(169, 19);
            this.lbl_Unloader_23.TabIndex = 0;
            this.lbl_Unloader_23.Text = "23. CELL_TRANSFER_T_3";
            this.lbl_Unloader_23.Click += new System.EventHandler(this.pn_Unloader_23_Click);
            // 
            // pn_Unloader_24
            // 
            this.pn_Unloader_24.Controls.Add(this.lbl_Unloader_24);
            this.pn_Unloader_24.Location = new System.Drawing.Point(3, 387);
            this.pn_Unloader_24.Name = "pn_Unloader_24";
            this.pn_Unloader_24.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_24.TabIndex = 46;
            this.pn_Unloader_24.Click += new System.EventHandler(this.pn_Unloader_24_Click);
            // 
            // lbl_Unloader_24
            // 
            this.lbl_Unloader_24.AutoSize = true;
            this.lbl_Unloader_24.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_24.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_24.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_24.Name = "lbl_Unloader_24";
            this.lbl_Unloader_24.Size = new System.Drawing.Size(169, 19);
            this.lbl_Unloader_24.TabIndex = 0;
            this.lbl_Unloader_24.Text = "24. CELL_TRANSFER_T_4";
            this.lbl_Unloader_24.Click += new System.EventHandler(this.pn_Unloader_24_Click);
            // 
            // pn_Unloader_26
            // 
            this.pn_Unloader_26.Controls.Add(this.lbl_Unloader_26);
            this.pn_Unloader_26.Location = new System.Drawing.Point(3, 419);
            this.pn_Unloader_26.Name = "pn_Unloader_26";
            this.pn_Unloader_26.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_26.TabIndex = 47;
            this.pn_Unloader_26.Click += new System.EventHandler(this.pn_Unloader_26_Click);
            // 
            // lbl_Unloader_26
            // 
            this.lbl_Unloader_26.AutoSize = true;
            this.lbl_Unloader_26.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_26.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_26.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_26.Name = "lbl_Unloader_26";
            this.lbl_Unloader_26.Size = new System.Drawing.Size(95, 19);
            this.lbl_Unloader_26.TabIndex = 0;
            this.lbl_Unloader_26.Text = "26. INSP_Z_1";
            this.lbl_Unloader_26.Click += new System.EventHandler(this.pn_Unloader_26_Click);
            // 
            // pn_Unloader_27
            // 
            this.pn_Unloader_27.Controls.Add(this.lbl_Unloader_27);
            this.pn_Unloader_27.Location = new System.Drawing.Point(3, 451);
            this.pn_Unloader_27.Name = "pn_Unloader_27";
            this.pn_Unloader_27.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_27.TabIndex = 48;
            this.pn_Unloader_27.Click += new System.EventHandler(this.pn_Unloader_27_Click);
            // 
            // lbl_Unloader_27
            // 
            this.lbl_Unloader_27.AutoSize = true;
            this.lbl_Unloader_27.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_27.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_27.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_27.Name = "lbl_Unloader_27";
            this.lbl_Unloader_27.Size = new System.Drawing.Size(93, 19);
            this.lbl_Unloader_27.TabIndex = 0;
            this.lbl_Unloader_27.Text = "27. CELL_Y_1";
            this.lbl_Unloader_27.Click += new System.EventHandler(this.pn_Unloader_27_Click);
            // 
            // pn_Unloader_28
            // 
            this.pn_Unloader_28.Controls.Add(this.lbl_Unloader_28);
            this.pn_Unloader_28.Location = new System.Drawing.Point(3, 483);
            this.pn_Unloader_28.Name = "pn_Unloader_28";
            this.pn_Unloader_28.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_28.TabIndex = 49;
            this.pn_Unloader_28.Click += new System.EventHandler(this.pn_Unloader_28_Click);
            // 
            // lbl_Unloader_28
            // 
            this.lbl_Unloader_28.AutoSize = true;
            this.lbl_Unloader_28.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_28.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_28.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_28.Name = "lbl_Unloader_28";
            this.lbl_Unloader_28.Size = new System.Drawing.Size(93, 19);
            this.lbl_Unloader_28.TabIndex = 0;
            this.lbl_Unloader_28.Text = "28. CELL_Y_2";
            this.lbl_Unloader_28.Click += new System.EventHandler(this.pn_Unloader_28_Click);
            // 
            // pn_Unloader_29
            // 
            this.pn_Unloader_29.Controls.Add(this.lbl_Unloader_29);
            this.pn_Unloader_29.Location = new System.Drawing.Point(3, 515);
            this.pn_Unloader_29.Name = "pn_Unloader_29";
            this.pn_Unloader_29.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_29.TabIndex = 50;
            this.pn_Unloader_29.Click += new System.EventHandler(this.pn_Unloader_29_Click);
            // 
            // lbl_Unloader_29
            // 
            this.lbl_Unloader_29.AutoSize = true;
            this.lbl_Unloader_29.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_29.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_29.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_29.Name = "lbl_Unloader_29";
            this.lbl_Unloader_29.Size = new System.Drawing.Size(252, 19);
            this.lbl_Unloader_29.TabIndex = 0;
            this.lbl_Unloader_29.Text = "29. CASSETTE_ROTATION_UP_RIGHT";
            this.lbl_Unloader_29.Click += new System.EventHandler(this.pn_Unloader_29_Click);
            // 
            // pn_Unloader_30
            // 
            this.pn_Unloader_30.Controls.Add(this.lbl_Unloader_30);
            this.pn_Unloader_30.Location = new System.Drawing.Point(3, 547);
            this.pn_Unloader_30.Name = "pn_Unloader_30";
            this.pn_Unloader_30.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_30.TabIndex = 51;
            this.pn_Unloader_30.Click += new System.EventHandler(this.pn_Unloader_30_Click);
            // 
            // lbl_Unloader_30
            // 
            this.lbl_Unloader_30.AutoSize = true;
            this.lbl_Unloader_30.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_30.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_30.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_30.Name = "lbl_Unloader_30";
            this.lbl_Unloader_30.Size = new System.Drawing.Size(279, 19);
            this.lbl_Unloader_30.TabIndex = 0;
            this.lbl_Unloader_30.Text = "30. CASSETTE_ROTATION_DOWN_RIGHT";
            this.lbl_Unloader_30.Click += new System.EventHandler(this.pn_Unloader_30_Click);
            // 
            // pn_Unloader_0
            // 
            this.pn_Unloader_0.Controls.Add(this.lbl_Unloader_0);
            this.pn_Unloader_0.Location = new System.Drawing.Point(3, 579);
            this.pn_Unloader_0.Name = "pn_Unloader_0";
            this.pn_Unloader_0.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_0.TabIndex = 52;
            this.pn_Unloader_0.Click += new System.EventHandler(this.pn_Unloader_0_Click);
            // 
            // lbl_Unloader_0
            // 
            this.lbl_Unloader_0.AutoSize = true;
            this.lbl_Unloader_0.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_0.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_0.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_0.Name = "lbl_Unloader_0";
            this.lbl_Unloader_0.Size = new System.Drawing.Size(147, 19);
            this.lbl_Unloader_0.TabIndex = 0;
            this.lbl_Unloader_0.Text = "0. BREAK_TABLE_T_1";
            this.lbl_Unloader_0.Click += new System.EventHandler(this.pn_Unloader_0_Click);
            // 
            // pn_Unloader_3
            // 
            this.pn_Unloader_3.Controls.Add(this.lbl_Unloader_3);
            this.pn_Unloader_3.Location = new System.Drawing.Point(3, 675);
            this.pn_Unloader_3.Name = "pn_Unloader_3";
            this.pn_Unloader_3.Size = new System.Drawing.Size(514, 26);
            this.pn_Unloader_3.TabIndex = 53;
            this.pn_Unloader_3.Click += new System.EventHandler(this.pn_Unloader_3_Click);
            // 
            // lbl_Unloader_3
            // 
            this.lbl_Unloader_3.AutoSize = true;
            this.lbl_Unloader_3.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_Unloader_3.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_3.Location = new System.Drawing.Point(3, 5);
            this.lbl_Unloader_3.Name = "lbl_Unloader_3";
            this.lbl_Unloader_3.Size = new System.Drawing.Size(147, 19);
            this.lbl_Unloader_3.TabIndex = 0;
            this.lbl_Unloader_3.Text = "3. BREAK_TABLE_T_4";
            this.lbl_Unloader_3.Click += new System.EventHandler(this.pn_Unloader_3_Click);
            // 
            // lbl_Unloader_AxisParameter
            // 
            this.lbl_Unloader_AxisParameter.AutoSize = true;
            this.lbl_Unloader_AxisParameter.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_AxisParameter.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_AxisParameter.Location = new System.Drawing.Point(31, 15);
            this.lbl_Unloader_AxisParameter.Name = "lbl_Unloader_AxisParameter";
            this.lbl_Unloader_AxisParameter.Size = new System.Drawing.Size(125, 23);
            this.lbl_Unloader_AxisParameter.TabIndex = 0;
            this.lbl_Unloader_AxisParameter.Text = "Axis Parameter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(31, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Servo On Level :";
            // 
            // tbox_Unloader_Pulse
            // 
            this.tbox_Unloader_Pulse.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Pulse.Location = new System.Drawing.Point(672, 140);
            this.tbox_Unloader_Pulse.Name = "tbox_Unloader_Pulse";
            this.tbox_Unloader_Pulse.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Pulse.TabIndex = 90;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(31, 327);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Home Signal :";
            // 
            // tbox_Unloader_Unit
            // 
            this.tbox_Unloader_Unit.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Unit.Location = new System.Drawing.Point(672, 115);
            this.tbox_Unloader_Unit.Name = "tbox_Unloader_Unit";
            this.tbox_Unloader_Unit.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Unit.TabIndex = 89;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(31, 375);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Home Vel 1st :";
            // 
            // tbox_Unloader_MinVelocitu
            // 
            this.tbox_Unloader_MinVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_MinVelocitu.Location = new System.Drawing.Point(672, 90);
            this.tbox_Unloader_MinVelocitu.Name = "tbox_Unloader_MinVelocitu";
            this.tbox_Unloader_MinVelocitu.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_MinVelocitu.TabIndex = 88;
            // 
            // lbl_Unloader_AxisNo
            // 
            this.lbl_Unloader_AxisNo.AutoSize = true;
            this.lbl_Unloader_AxisNo.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_AxisNo.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_AxisNo.Location = new System.Drawing.Point(31, 39);
            this.lbl_Unloader_AxisNo.Name = "lbl_Unloader_AxisNo";
            this.lbl_Unloader_AxisNo.Size = new System.Drawing.Size(67, 23);
            this.lbl_Unloader_AxisNo.TabIndex = 4;
            this.lbl_Unloader_AxisNo.Text = "AxisNo.";
            // 
            // tbox_Unloader_MaxVelocitu
            // 
            this.tbox_Unloader_MaxVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_MaxVelocitu.Location = new System.Drawing.Point(672, 65);
            this.tbox_Unloader_MaxVelocitu.Name = "tbox_Unloader_MaxVelocitu";
            this.tbox_Unloader_MaxVelocitu.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_MaxVelocitu.TabIndex = 87;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(31, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "+ End Limit :";
            // 
            // cbbox_Unloader_EncoderType
            // 
            this.cbbox_Unloader_EncoderType.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_EncoderType.FormattingEnabled = true;
            this.cbbox_Unloader_EncoderType.Location = new System.Drawing.Point(672, 284);
            this.cbbox_Unloader_EncoderType.Name = "cbbox_Unloader_EncoderType";
            this.cbbox_Unloader_EncoderType.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_EncoderType.TabIndex = 86;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(31, 399);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "Home Vel 2nd :";
            // 
            // cbbox_Unloader_AlarmResetLevel
            // 
            this.cbbox_Unloader_AlarmResetLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_AlarmResetLevel.FormattingEnabled = true;
            this.cbbox_Unloader_AlarmResetLevel.Location = new System.Drawing.Point(672, 260);
            this.cbbox_Unloader_AlarmResetLevel.Name = "cbbox_Unloader_AlarmResetLevel";
            this.cbbox_Unloader_AlarmResetLevel.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_AlarmResetLevel.TabIndex = 85;
            // 
            // lbl_Unloader_VelProfileMode
            // 
            this.lbl_Unloader_VelProfileMode.AutoSize = true;
            this.lbl_Unloader_VelProfileMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_VelProfileMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_VelProfileMode.Location = new System.Drawing.Point(31, 135);
            this.lbl_Unloader_VelProfileMode.Name = "lbl_Unloader_VelProfileMode";
            this.lbl_Unloader_VelProfileMode.Size = new System.Drawing.Size(149, 23);
            this.lbl_Unloader_VelProfileMode.TabIndex = 7;
            this.lbl_Unloader_VelProfileMode.Text = "Vel Profile Mode :";
            // 
            // cbbox_Unloader_StopLevel
            // 
            this.cbbox_Unloader_StopLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_StopLevel.FormattingEnabled = true;
            this.cbbox_Unloader_StopLevel.Location = new System.Drawing.Point(672, 236);
            this.cbbox_Unloader_StopLevel.Name = "cbbox_Unloader_StopLevel";
            this.cbbox_Unloader_StopLevel.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_StopLevel.TabIndex = 84;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(31, 447);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(132, 23);
            this.label9.TabIndex = 8;
            this.label9.Text = "Home Vel Last :";
            // 
            // cbbox_Unloader_StopMode
            // 
            this.cbbox_Unloader_StopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_StopMode.FormattingEnabled = true;
            this.cbbox_Unloader_StopMode.Location = new System.Drawing.Point(672, 212);
            this.cbbox_Unloader_StopMode.Name = "cbbox_Unloader_StopMode";
            this.cbbox_Unloader_StopMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_StopMode.TabIndex = 83;
            // 
            // lbl_Unloader_EncInput
            // 
            this.lbl_Unloader_EncInput.AutoSize = true;
            this.lbl_Unloader_EncInput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_EncInput.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_EncInput.Location = new System.Drawing.Point(31, 87);
            this.lbl_Unloader_EncInput.Name = "lbl_Unloader_EncInput";
            this.lbl_Unloader_EncInput.Size = new System.Drawing.Size(98, 23);
            this.lbl_Unloader_EncInput.TabIndex = 9;
            this.lbl_Unloader_EncInput.Text = "Enc. Input :";
            // 
            // cbbox_Unloader_ZPhase
            // 
            this.cbbox_Unloader_ZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_ZPhase.FormattingEnabled = true;
            this.cbbox_Unloader_ZPhase.Location = new System.Drawing.Point(672, 188);
            this.cbbox_Unloader_ZPhase.Name = "cbbox_Unloader_ZPhase";
            this.cbbox_Unloader_ZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_ZPhase.TabIndex = 82;
            // 
            // lbl_Unloader_PulseOutput
            // 
            this.lbl_Unloader_PulseOutput.AutoSize = true;
            this.lbl_Unloader_PulseOutput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_PulseOutput.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_PulseOutput.Location = new System.Drawing.Point(31, 63);
            this.lbl_Unloader_PulseOutput.Name = "lbl_Unloader_PulseOutput";
            this.lbl_Unloader_PulseOutput.Size = new System.Drawing.Size(121, 23);
            this.lbl_Unloader_PulseOutput.TabIndex = 10;
            this.lbl_Unloader_PulseOutput.Text = "Pulse Output :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(31, 303);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(179, 23);
            this.label12.TabIndex = 11;
            this.label12.Text = "Home  Search Setting";
            // 
            // tbox_Unloader_Home_ClearTime
            // 
            this.tbox_Unloader_Home_ClearTime.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_ClearTime.Location = new System.Drawing.Point(672, 428);
            this.tbox_Unloader_Home_ClearTime.Name = "tbox_Unloader_Home_ClearTime";
            this.tbox_Unloader_Home_ClearTime.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_ClearTime.TabIndex = 80;
            // 
            // lbl_Unloader_MotionSignalSetting
            // 
            this.lbl_Unloader_MotionSignalSetting.AutoSize = true;
            this.lbl_Unloader_MotionSignalSetting.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_MotionSignalSetting.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_MotionSignalSetting.Location = new System.Drawing.Point(31, 159);
            this.lbl_Unloader_MotionSignalSetting.Name = "lbl_Unloader_MotionSignalSetting";
            this.lbl_Unloader_MotionSignalSetting.Size = new System.Drawing.Size(178, 23);
            this.lbl_Unloader_MotionSignalSetting.TabIndex = 12;
            this.lbl_Unloader_MotionSignalSetting.Text = "Motion Signal Setting";
            // 
            // tbox_Unloader_Home_Accelation2
            // 
            this.tbox_Unloader_Home_Accelation2.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_Accelation2.Location = new System.Drawing.Point(672, 403);
            this.tbox_Unloader_Home_Accelation2.Name = "tbox_Unloader_Home_Accelation2";
            this.tbox_Unloader_Home_Accelation2.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_Accelation2.TabIndex = 79;
            // 
            // lbl_Unloader_AbsRelMode
            // 
            this.lbl_Unloader_AbsRelMode.AutoSize = true;
            this.lbl_Unloader_AbsRelMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_AbsRelMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_AbsRelMode.Location = new System.Drawing.Point(31, 111);
            this.lbl_Unloader_AbsRelMode.Name = "lbl_Unloader_AbsRelMode";
            this.lbl_Unloader_AbsRelMode.Size = new System.Drawing.Size(126, 23);
            this.lbl_Unloader_AbsRelMode.TabIndex = 13;
            this.lbl_Unloader_AbsRelMode.Text = "Abs.Rel Mode :";
            // 
            // tbox_Unloader_Home_Accelation1
            // 
            this.tbox_Unloader_Home_Accelation1.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_Accelation1.Location = new System.Drawing.Point(672, 378);
            this.tbox_Unloader_Home_Accelation1.Name = "tbox_Unloader_Home_Accelation1";
            this.tbox_Unloader_Home_Accelation1.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_Accelation1.TabIndex = 78;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(31, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 23);
            this.label15.TabIndex = 14;
            this.label15.Text = "In Position :";
            // 
            // cbbox_Unloader_Home_ZPhase
            // 
            this.cbbox_Unloader_Home_ZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_Home_ZPhase.FormattingEnabled = true;
            this.cbbox_Unloader_Home_ZPhase.Location = new System.Drawing.Point(672, 355);
            this.cbbox_Unloader_Home_ZPhase.Name = "cbbox_Unloader_Home_ZPhase";
            this.cbbox_Unloader_Home_ZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_Home_ZPhase.TabIndex = 77;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(31, 207);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 23);
            this.label16.TabIndex = 15;
            this.label16.Text = "Alarm :";
            // 
            // cbbox_Unloader_Home_Direction
            // 
            this.cbbox_Unloader_Home_Direction.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_Home_Direction.FormattingEnabled = true;
            this.cbbox_Unloader_Home_Direction.Location = new System.Drawing.Point(672, 332);
            this.cbbox_Unloader_Home_Direction.Name = "cbbox_Unloader_Home_Direction";
            this.cbbox_Unloader_Home_Direction.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_Home_Direction.TabIndex = 76;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(31, 351);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 23);
            this.label17.TabIndex = 16;
            this.label17.Text = "Home Level :";
            // 
            // cbbox_Unloader_SW_LimitEnable
            // 
            this.cbbox_Unloader_SW_LimitEnable.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_SW_LimitEnable.FormattingEnabled = true;
            this.cbbox_Unloader_SW_LimitEnable.Location = new System.Drawing.Point(672, 523);
            this.cbbox_Unloader_SW_LimitEnable.Name = "cbbox_Unloader_SW_LimitEnable";
            this.cbbox_Unloader_SW_LimitEnable.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_SW_LimitEnable.TabIndex = 75;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(31, 519);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(153, 23);
            this.label18.TabIndex = 17;
            this.label18.Text = "S/W +Limit [mm] :";
            // 
            // cbbox_Unloader_SW_LimitStopMode
            // 
            this.cbbox_Unloader_SW_LimitStopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_SW_LimitStopMode.FormattingEnabled = true;
            this.cbbox_Unloader_SW_LimitStopMode.Location = new System.Drawing.Point(672, 500);
            this.cbbox_Unloader_SW_LimitStopMode.Name = "cbbox_Unloader_SW_LimitStopMode";
            this.cbbox_Unloader_SW_LimitStopMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_SW_LimitStopMode.TabIndex = 74;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(31, 423);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 23);
            this.label19.TabIndex = 18;
            this.label19.Text = "Home Vel 3rd :";
            // 
            // tbox_Unloader_Init_InPosition
            // 
            this.tbox_Unloader_Init_InPosition.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Init_InPosition.Location = new System.Drawing.Point(672, 641);
            this.tbox_Unloader_Init_InPosition.Name = "tbox_Unloader_Init_InPosition";
            this.tbox_Unloader_Init_InPosition.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Init_InPosition.TabIndex = 73;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(31, 471);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 23);
            this.label20.TabIndex = 19;
            this.label20.Text = "S/W Limit Setting";
            // 
            // tbox_Unloader_Init_Decelation
            // 
            this.tbox_Unloader_Init_Decelation.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Init_Decelation.Location = new System.Drawing.Point(672, 616);
            this.tbox_Unloader_Init_Decelation.Name = "tbox_Unloader_Init_Decelation";
            this.tbox_Unloader_Init_Decelation.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Init_Decelation.TabIndex = 72;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(31, 495);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(148, 23);
            this.label21.TabIndex = 20;
            this.label21.Text = "S/W -Limit [mm] :";
            // 
            // tbox_Unloader_Init_Accelation
            // 
            this.tbox_Unloader_Init_Accelation.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Init_Accelation.Location = new System.Drawing.Point(672, 591);
            this.tbox_Unloader_Init_Accelation.Name = "tbox_Unloader_Init_Accelation";
            this.tbox_Unloader_Init_Accelation.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Init_Accelation.TabIndex = 71;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(31, 567);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(95, 23);
            this.label22.TabIndex = 21;
            this.label22.Text = "Init Setting";
            // 
            // tbox_Unloader_Init_AccelationTime
            // 
            this.tbox_Unloader_Init_AccelationTime.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Init_AccelationTime.Location = new System.Drawing.Point(245, 666);
            this.tbox_Unloader_Init_AccelationTime.Name = "tbox_Unloader_Init_AccelationTime";
            this.tbox_Unloader_Init_AccelationTime.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Init_AccelationTime.TabIndex = 70;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(31, 543);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(146, 23);
            this.label23.TabIndex = 22;
            this.label23.Text = "S/W Limit Mode :";
            // 
            // tbox_Unloader_Init_PtpSpeed
            // 
            this.tbox_Unloader_Init_PtpSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Init_PtpSpeed.Location = new System.Drawing.Point(245, 641);
            this.tbox_Unloader_Init_PtpSpeed.Name = "tbox_Unloader_Init_PtpSpeed";
            this.tbox_Unloader_Init_PtpSpeed.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Init_PtpSpeed.TabIndex = 69;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(31, 231);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 23);
            this.label24.TabIndex = 23;
            this.label24.Text = "- End Limit :";
            // 
            // tbox_Unloader_Init_Velocity
            // 
            this.tbox_Unloader_Init_Velocity.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Init_Velocity.Location = new System.Drawing.Point(245, 616);
            this.tbox_Unloader_Init_Velocity.Name = "tbox_Unloader_Init_Velocity";
            this.tbox_Unloader_Init_Velocity.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Init_Velocity.TabIndex = 68;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(31, 591);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(158, 23);
            this.label25.TabIndex = 24;
            this.label25.Text = "Init Position [mm] :";
            // 
            // tbox_Unloader_Init_Position
            // 
            this.tbox_Unloader_Init_Position.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Init_Position.Location = new System.Drawing.Point(245, 591);
            this.tbox_Unloader_Init_Position.Name = "tbox_Unloader_Init_Position";
            this.tbox_Unloader_Init_Position.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Init_Position.TabIndex = 67;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(31, 615);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(188, 23);
            this.label26.TabIndex = 25;
            this.label26.Text = "Init Velocity [mm/sec] :";
            // 
            // cbbox_Unloader_SW_LimitMode
            // 
            this.cbbox_Unloader_SW_LimitMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_SW_LimitMode.FormattingEnabled = true;
            this.cbbox_Unloader_SW_LimitMode.Location = new System.Drawing.Point(245, 546);
            this.cbbox_Unloader_SW_LimitMode.Name = "cbbox_Unloader_SW_LimitMode";
            this.cbbox_Unloader_SW_LimitMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_SW_LimitMode.TabIndex = 66;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(31, 639);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(99, 23);
            this.label27.TabIndex = 26;
            this.label27.Text = "Ptp Speed :";
            // 
            // tbox_Unloader_SW_PlusLimit
            // 
            this.tbox_Unloader_SW_PlusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_SW_PlusLimit.Location = new System.Drawing.Point(245, 520);
            this.tbox_Unloader_SW_PlusLimit.Name = "tbox_Unloader_SW_PlusLimit";
            this.tbox_Unloader_SW_PlusLimit.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_SW_PlusLimit.TabIndex = 65;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(31, 663);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(165, 23);
            this.label28.TabIndex = 27;
            this.label28.Text = "Accelation Time [s] :";
            // 
            // tbox_Unloader_SW_MinusLimit
            // 
            this.tbox_Unloader_SW_MinusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_SW_MinusLimit.Location = new System.Drawing.Point(245, 495);
            this.tbox_Unloader_SW_MinusLimit.Name = "tbox_Unloader_SW_MinusLimit";
            this.tbox_Unloader_SW_MinusLimit.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_SW_MinusLimit.TabIndex = 64;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(469, 135);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 23);
            this.label29.TabIndex = 28;
            this.label29.Text = "Pulse :";
            // 
            // lbl_Unloader_MinVelocitu
            // 
            this.lbl_Unloader_MinVelocitu.AutoSize = true;
            this.lbl_Unloader_MinVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_MinVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_MinVelocitu.Location = new System.Drawing.Point(469, 87);
            this.lbl_Unloader_MinVelocitu.Name = "lbl_Unloader_MinVelocitu";
            this.lbl_Unloader_MinVelocitu.Size = new System.Drawing.Size(195, 23);
            this.lbl_Unloader_MinVelocitu.TabIndex = 29;
            this.lbl_Unloader_MinVelocitu.Text = "Min Velocitu [mm/sec] :";
            // 
            // tbox_Unloader_Home_Vel3
            // 
            this.tbox_Unloader_Home_Vel3.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_Vel3.Location = new System.Drawing.Point(245, 423);
            this.tbox_Unloader_Home_Vel3.Name = "tbox_Unloader_Home_Vel3";
            this.tbox_Unloader_Home_Vel3.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_Vel3.TabIndex = 62;
            // 
            // lbl_Unloader_MaxVelocitu
            // 
            this.lbl_Unloader_MaxVelocitu.AutoSize = true;
            this.lbl_Unloader_MaxVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_MaxVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_MaxVelocitu.Location = new System.Drawing.Point(469, 63);
            this.lbl_Unloader_MaxVelocitu.Name = "lbl_Unloader_MaxVelocitu";
            this.lbl_Unloader_MaxVelocitu.Size = new System.Drawing.Size(198, 23);
            this.lbl_Unloader_MaxVelocitu.TabIndex = 30;
            this.lbl_Unloader_MaxVelocitu.Text = "Max Velocitu [mm/sec] :";
            // 
            // tbox_Unloader_Home_Vel2
            // 
            this.tbox_Unloader_Home_Vel2.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_Vel2.Location = new System.Drawing.Point(245, 398);
            this.tbox_Unloader_Home_Vel2.Name = "tbox_Unloader_Home_Vel2";
            this.tbox_Unloader_Home_Vel2.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_Vel2.TabIndex = 61;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(469, 111);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(52, 23);
            this.label32.TabIndex = 31;
            this.label32.Text = "Unit :";
            // 
            // tbox_Unloader_Home_Vel1
            // 
            this.tbox_Unloader_Home_Vel1.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_Vel1.Location = new System.Drawing.Point(245, 373);
            this.tbox_Unloader_Home_Vel1.Name = "tbox_Unloader_Home_Vel1";
            this.tbox_Unloader_Home_Vel1.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_Vel1.TabIndex = 60;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(469, 279);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(124, 23);
            this.label33.TabIndex = 32;
            this.label33.Text = "Encoder Type :";
            // 
            // cbbox_Unloader_Home_Level
            // 
            this.cbbox_Unloader_Home_Level.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_Home_Level.FormattingEnabled = true;
            this.cbbox_Unloader_Home_Level.Location = new System.Drawing.Point(245, 350);
            this.cbbox_Unloader_Home_Level.Name = "cbbox_Unloader_Home_Level";
            this.cbbox_Unloader_Home_Level.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_Home_Level.TabIndex = 59;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(469, 255);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(156, 23);
            this.label34.TabIndex = 33;
            this.label34.Text = "Alarm Reset Level :";
            // 
            // cbbox_Unloader_Home_Signal
            // 
            this.cbbox_Unloader_Home_Signal.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_Home_Signal.FormattingEnabled = true;
            this.cbbox_Unloader_Home_Signal.Location = new System.Drawing.Point(245, 327);
            this.cbbox_Unloader_Home_Signal.Name = "cbbox_Unloader_Home_Signal";
            this.cbbox_Unloader_Home_Signal.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_Home_Signal.TabIndex = 58;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(469, 183);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 23);
            this.label35.TabIndex = 34;
            this.label35.Text = "Z-Phase :";
            // 
            // cbbox_Unloader_ServoOnLevel
            // 
            this.cbbox_Unloader_ServoOnLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_ServoOnLevel.FormattingEnabled = true;
            this.cbbox_Unloader_ServoOnLevel.Location = new System.Drawing.Point(245, 279);
            this.cbbox_Unloader_ServoOnLevel.Name = "cbbox_Unloader_ServoOnLevel";
            this.cbbox_Unloader_ServoOnLevel.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_ServoOnLevel.TabIndex = 57;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(469, 207);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(106, 23);
            this.label36.TabIndex = 35;
            this.label36.Text = "Stop Mode :";
            // 
            // cbbox_Unloader_PlusEndLimit
            // 
            this.cbbox_Unloader_PlusEndLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_PlusEndLimit.FormattingEnabled = true;
            this.cbbox_Unloader_PlusEndLimit.Location = new System.Drawing.Point(245, 255);
            this.cbbox_Unloader_PlusEndLimit.Name = "cbbox_Unloader_PlusEndLimit";
            this.cbbox_Unloader_PlusEndLimit.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_PlusEndLimit.TabIndex = 56;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(469, 231);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(99, 23);
            this.label37.TabIndex = 36;
            this.label37.Text = "Stop Level :";
            // 
            // cbbox_Unloader_MinusEndLismit
            // 
            this.cbbox_Unloader_MinusEndLismit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_MinusEndLismit.FormattingEnabled = true;
            this.cbbox_Unloader_MinusEndLismit.Location = new System.Drawing.Point(245, 231);
            this.cbbox_Unloader_MinusEndLismit.Name = "cbbox_Unloader_MinusEndLismit";
            this.cbbox_Unloader_MinusEndLismit.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_MinusEndLismit.TabIndex = 55;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(469, 327);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(141, 23);
            this.label38.TabIndex = 37;
            this.label38.Text = "Home Direction :";
            // 
            // cbbox_Unloader_Alarm
            // 
            this.cbbox_Unloader_Alarm.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_Alarm.FormattingEnabled = true;
            this.cbbox_Unloader_Alarm.Location = new System.Drawing.Point(245, 207);
            this.cbbox_Unloader_Alarm.Name = "cbbox_Unloader_Alarm";
            this.cbbox_Unloader_Alarm.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_Alarm.TabIndex = 54;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(469, 375);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(179, 23);
            this.label39.TabIndex = 38;
            this.label39.Text = "Home Accelation 1st :";
            // 
            // cbbox_Unloader_Inposition
            // 
            this.cbbox_Unloader_Inposition.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_Inposition.FormattingEnabled = true;
            this.cbbox_Unloader_Inposition.Location = new System.Drawing.Point(245, 183);
            this.cbbox_Unloader_Inposition.Name = "cbbox_Unloader_Inposition";
            this.cbbox_Unloader_Inposition.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_Inposition.TabIndex = 53;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(469, 399);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(186, 23);
            this.label40.TabIndex = 39;
            this.label40.Text = "Home Accelation 2nd :";
            // 
            // cbbox_Unloader_AbsRelMode
            // 
            this.cbbox_Unloader_AbsRelMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_AbsRelMode.FormattingEnabled = true;
            this.cbbox_Unloader_AbsRelMode.Location = new System.Drawing.Point(245, 115);
            this.cbbox_Unloader_AbsRelMode.Name = "cbbox_Unloader_AbsRelMode";
            this.cbbox_Unloader_AbsRelMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_AbsRelMode.TabIndex = 52;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(469, 447);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(117, 23);
            this.label41.TabIndex = 40;
            this.label41.Text = "Home Offset :";
            // 
            // cbbox_Unloader_EncInput
            // 
            this.cbbox_Unloader_EncInput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_EncInput.FormattingEnabled = true;
            this.cbbox_Unloader_EncInput.Location = new System.Drawing.Point(245, 91);
            this.cbbox_Unloader_EncInput.Name = "cbbox_Unloader_EncInput";
            this.cbbox_Unloader_EncInput.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_EncInput.TabIndex = 51;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(469, 351);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(134, 23);
            this.label42.TabIndex = 41;
            this.label42.Text = "Home Z_Phase :";
            // 
            // cbbox_Unloader_PulseOutput
            // 
            this.cbbox_Unloader_PulseOutput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_PulseOutput.FormattingEnabled = true;
            this.cbbox_Unloader_PulseOutput.Location = new System.Drawing.Point(245, 68);
            this.cbbox_Unloader_PulseOutput.Name = "cbbox_Unloader_PulseOutput";
            this.cbbox_Unloader_PulseOutput.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_PulseOutput.TabIndex = 50;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(469, 423);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(154, 23);
            this.label43.TabIndex = 42;
            this.label43.Text = "Home Clear Time :";
            // 
            // cbbox_Unloader_VelProfileMode
            // 
            this.cbbox_Unloader_VelProfileMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbbox_Unloader_VelProfileMode.FormattingEnabled = true;
            this.cbbox_Unloader_VelProfileMode.Location = new System.Drawing.Point(245, 140);
            this.cbbox_Unloader_VelProfileMode.Name = "cbbox_Unloader_VelProfileMode";
            this.cbbox_Unloader_VelProfileMode.Size = new System.Drawing.Size(121, 21);
            this.cbbox_Unloader_VelProfileMode.TabIndex = 49;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(469, 519);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(152, 23);
            this.label44.TabIndex = 43;
            this.label44.Text = "S/W Limit Enable :";
            // 
            // tbox_Unloader_AxisNo
            // 
            this.tbox_Unloader_AxisNo.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_AxisNo.Location = new System.Drawing.Point(245, 43);
            this.tbox_Unloader_AxisNo.Name = "tbox_Unloader_AxisNo";
            this.tbox_Unloader_AxisNo.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_AxisNo.TabIndex = 48;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DimGray;
            this.panel4.Controls.Add(this.lbl_Unloader_AxisParameter);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.tbox_Unloader_Pulse);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.tbox_Unloader_Unit);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.tbox_Unloader_MinVelocitu);
            this.panel4.Controls.Add(this.lbl_Unloader_AxisNo);
            this.panel4.Controls.Add(this.tbox_Unloader_MaxVelocitu);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.cbbox_Unloader_EncoderType);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.cbbox_Unloader_AlarmResetLevel);
            this.panel4.Controls.Add(this.lbl_Unloader_VelProfileMode);
            this.panel4.Controls.Add(this.cbbox_Unloader_StopLevel);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.cbbox_Unloader_StopMode);
            this.panel4.Controls.Add(this.lbl_Unloader_EncInput);
            this.panel4.Controls.Add(this.cbbox_Unloader_ZPhase);
            this.panel4.Controls.Add(this.lbl_Unloader_PulseOutput);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_Offset);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_ClearTime);
            this.panel4.Controls.Add(this.lbl_Unloader_MotionSignalSetting);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_Accelation2);
            this.panel4.Controls.Add(this.lbl_Unloader_AbsRelMode);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_Accelation1);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.cbbox_Unloader_Home_ZPhase);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.cbbox_Unloader_Home_Direction);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.cbbox_Unloader_SW_LimitEnable);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.cbbox_Unloader_SW_LimitStopMode);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.tbox_Unloader_Init_InPosition);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.tbox_Unloader_Init_Decelation);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.tbox_Unloader_Init_Accelation);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.tbox_Unloader_Init_AccelationTime);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.tbox_Unloader_Init_PtpSpeed);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.tbox_Unloader_Init_Velocity);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.tbox_Unloader_Init_Position);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.cbbox_Unloader_SW_LimitMode);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.tbox_Unloader_SW_PlusLimit);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.tbox_Unloader_SW_MinusLimit);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_VelLast);
            this.panel4.Controls.Add(this.lbl_Unloader_MinVelocitu);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_Vel3);
            this.panel4.Controls.Add(this.lbl_Unloader_MaxVelocitu);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_Vel2);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.tbox_Unloader_Home_Vel1);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.cbbox_Unloader_Home_Level);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.cbbox_Unloader_Home_Signal);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.cbbox_Unloader_ServoOnLevel);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.cbbox_Unloader_PlusEndLimit);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.cbbox_Unloader_MinusEndLismit);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.cbbox_Unloader_Alarm);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.cbbox_Unloader_Inposition);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.cbbox_Unloader_AbsRelMode);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.cbbox_Unloader_EncInput);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.cbbox_Unloader_PulseOutput);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.cbbox_Unloader_VelProfileMode);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.tbox_Unloader_AxisNo);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Controls.Add(this.label46);
            this.panel4.Controls.Add(this.label47);
            this.panel4.Controls.Add(this.label48);
            this.panel4.Location = new System.Drawing.Point(0, -1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(834, 755);
            this.panel4.TabIndex = 1;
            // 
            // tbox_Unloader_Home_Offset
            // 
            this.tbox_Unloader_Home_Offset.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_Offset.Location = new System.Drawing.Point(672, 453);
            this.tbox_Unloader_Home_Offset.Name = "tbox_Unloader_Home_Offset";
            this.tbox_Unloader_Home_Offset.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_Offset.TabIndex = 81;
            // 
            // tbox_Unloader_Home_VelLast
            // 
            this.tbox_Unloader_Home_VelLast.Font = new System.Drawing.Font("굴림", 10F);
            this.tbox_Unloader_Home_VelLast.Location = new System.Drawing.Point(245, 448);
            this.tbox_Unloader_Home_VelLast.Name = "tbox_Unloader_Home_VelLast";
            this.tbox_Unloader_Home_VelLast.Size = new System.Drawing.Size(121, 23);
            this.tbox_Unloader_Home_VelLast.TabIndex = 63;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(469, 495);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(187, 23);
            this.label45.TabIndex = 44;
            this.label45.Text = "S/W Limit Stop Mode :";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(469, 639);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(138, 23);
            this.label46.TabIndex = 47;
            this.label46.Text = "In Position [mm]";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(469, 591);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(130, 23);
            this.label47.TabIndex = 45;
            this.label47.Text = "Init Accelation :";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(469, 615);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(132, 23);
            this.label48.TabIndex = 46;
            this.label48.Text = "Init Decelation :";
            // 
            // ParameterAxis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "ParameterAxis";
            this.Size = new System.Drawing.Size(1740, 875);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tp_iostatus_ld.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pn_Loader_15.ResumeLayout(false);
            this.pn_Loader_15.PerformLayout();
            this.pn_Loader_14.ResumeLayout(false);
            this.pn_Loader_14.PerformLayout();
            this.pn_Loader_13.ResumeLayout(false);
            this.pn_Loader_13.PerformLayout();
            this.pn_Loader_12.ResumeLayout(false);
            this.pn_Loader_12.PerformLayout();
            this.pn_Loader_11.ResumeLayout(false);
            this.pn_Loader_11.PerformLayout();
            this.pn_Loader_10.ResumeLayout(false);
            this.pn_Loader_10.PerformLayout();
            this.pn_Loader_09.ResumeLayout(false);
            this.pn_Loader_09.PerformLayout();
            this.pn_Loader_08.ResumeLayout(false);
            this.pn_Loader_08.PerformLayout();
            this.pn_Loader_07.ResumeLayout(false);
            this.pn_Loader_07.PerformLayout();
            this.pn_Loader_06.ResumeLayout(false);
            this.pn_Loader_06.PerformLayout();
            this.pn_Loader_05.ResumeLayout(false);
            this.pn_Loader_05.PerformLayout();
            this.pn_Loader_04.ResumeLayout(false);
            this.pn_Loader_04.PerformLayout();
            this.pn_Loader_36.ResumeLayout(false);
            this.pn_Loader_36.PerformLayout();
            this.pn_Loader_35.ResumeLayout(false);
            this.pn_Loader_35.PerformLayout();
            this.pn_Loader_33.ResumeLayout(false);
            this.pn_Loader_33.PerformLayout();
            this.pn_Loader_32.ResumeLayout(false);
            this.pn_Loader_32.PerformLayout();
            this.pn_Loader_31.ResumeLayout(false);
            this.pn_Loader_31.PerformLayout();
            this.tp_iostatus_process.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tp_iostatus_uld.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.pn_Process_19.ResumeLayout(false);
            this.pn_Process_19.PerformLayout();
            this.pn_Process_18.ResumeLayout(false);
            this.pn_Process_18.PerformLayout();
            this.pn_Process_14.ResumeLayout(false);
            this.pn_Process_14.PerformLayout();
            this.pn_Process_13.ResumeLayout(false);
            this.pn_Process_13.PerformLayout();
            this.pn_Process_12.ResumeLayout(false);
            this.pn_Process_12.PerformLayout();
            this.pn_Process_11.ResumeLayout(false);
            this.pn_Process_11.PerformLayout();
            this.pn_Process_10.ResumeLayout(false);
            this.pn_Process_10.PerformLayout();
            this.pn_Process_09.ResumeLayout(false);
            this.pn_Process_09.PerformLayout();
            this.pn_Process_22.ResumeLayout(false);
            this.pn_Process_22.PerformLayout();
            this.pn_Process_07.ResumeLayout(false);
            this.pn_Process_07.PerformLayout();
            this.pn_Process_06.ResumeLayout(false);
            this.pn_Process_06.PerformLayout();
            this.pn_Process_05.ResumeLayout(false);
            this.pn_Process_05.PerformLayout();
            this.pn_Process_04.ResumeLayout(false);
            this.pn_Process_04.PerformLayout();
            this.pn_Process_17.ResumeLayout(false);
            this.pn_Process_17.PerformLayout();
            this.pn_Process_03.ResumeLayout(false);
            this.pn_Process_03.PerformLayout();
            this.pn_Process_02.ResumeLayout(false);
            this.pn_Process_02.PerformLayout();
            this.pn_Process_01.ResumeLayout(false);
            this.pn_Process_01.PerformLayout();
            this.pn_Process_20.ResumeLayout(false);
            this.pn_Process_20.PerformLayout();
            this.pn_Process_21.ResumeLayout(false);
            this.pn_Process_21.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.pn_Unloader_37.ResumeLayout(false);
            this.pn_Unloader_37.PerformLayout();
            this.pn_Unloader_2.ResumeLayout(false);
            this.pn_Unloader_2.PerformLayout();
            this.pn_Unloader_1.ResumeLayout(false);
            this.pn_Unloader_1.PerformLayout();
            this.pn_Unloader_38.ResumeLayout(false);
            this.pn_Unloader_38.PerformLayout();
            this.pn_Unloader_39.ResumeLayout(false);
            this.pn_Unloader_39.PerformLayout();
            this.pn_Unloader_40.ResumeLayout(false);
            this.pn_Unloader_40.PerformLayout();
            this.pn_Unloader_41.ResumeLayout(false);
            this.pn_Unloader_41.PerformLayout();
            this.pn_Unloader_16.ResumeLayout(false);
            this.pn_Unloader_16.PerformLayout();
            this.pn_Unloader_17.ResumeLayout(false);
            this.pn_Unloader_17.PerformLayout();
            this.pn_Unloader_18.ResumeLayout(false);
            this.pn_Unloader_18.PerformLayout();
            this.pn_Unloader_19.ResumeLayout(false);
            this.pn_Unloader_19.PerformLayout();
            this.pn_Unloader_20.ResumeLayout(false);
            this.pn_Unloader_20.PerformLayout();
            this.pn_Unloader_21.ResumeLayout(false);
            this.pn_Unloader_21.PerformLayout();
            this.pn_Unloader_23.ResumeLayout(false);
            this.pn_Unloader_23.PerformLayout();
            this.pn_Unloader_24.ResumeLayout(false);
            this.pn_Unloader_24.PerformLayout();
            this.pn_Unloader_26.ResumeLayout(false);
            this.pn_Unloader_26.PerformLayout();
            this.pn_Unloader_27.ResumeLayout(false);
            this.pn_Unloader_27.PerformLayout();
            this.pn_Unloader_28.ResumeLayout(false);
            this.pn_Unloader_28.PerformLayout();
            this.pn_Unloader_29.ResumeLayout(false);
            this.pn_Unloader_29.PerformLayout();
            this.pn_Unloader_30.ResumeLayout(false);
            this.pn_Unloader_30.PerformLayout();
            this.pn_Unloader_0.ResumeLayout(false);
            this.pn_Unloader_0.PerformLayout();
            this.pn_Unloader_3.ResumeLayout(false);
            this.pn_Unloader_3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tp_iostatus_ld;
        private System.Windows.Forms.TabPage tp_iostatus_process;
        private System.Windows.Forms.TabPage tp_iostatus_uld;
        private System.Windows.Forms.TextBox tbox_Loader_Pulse;
        private System.Windows.Forms.TextBox tbox_Loader_Unit;
        private System.Windows.Forms.TextBox tbox_Loader_MinVelocitu;
        private System.Windows.Forms.TextBox tbox_Loader_MaxVelocitu;
        private System.Windows.Forms.ComboBox cbbox_Loader_EncoderType;
        private System.Windows.Forms.ComboBox cbbox_Loader_AlarmResetLevel;
        private System.Windows.Forms.ComboBox cbbox_Loader_StopLevel;
        private System.Windows.Forms.ComboBox cbbox_Loader_StopMode;
        private System.Windows.Forms.ComboBox cbbox_Loader_ZPhase;
        private System.Windows.Forms.TextBox tbox_Loader_Home_Offset;
        private System.Windows.Forms.TextBox tbox_Loader_Home_ClearTime;
        private System.Windows.Forms.TextBox tbox_Loader_Home_Accelation2;
        private System.Windows.Forms.TextBox tbox_Loader_Home_Accelation1;
        private System.Windows.Forms.ComboBox cbbox_Loader_Home_ZPhase;
        private System.Windows.Forms.ComboBox cbbox_Loader_Home_Direction;
        private System.Windows.Forms.ComboBox cbbox_Loader_SW_LimitEnable;
        private System.Windows.Forms.ComboBox cbbox_Loader_SW_LimitStopMode;
        private System.Windows.Forms.TextBox tbox_Loader_Init_Inposition;
        private System.Windows.Forms.TextBox tbox_Loader_Init_Decelation;
        private System.Windows.Forms.TextBox tbox_Loader_Init_Accelation;
        private System.Windows.Forms.TextBox tbox_Loader_Init_AccelationTime;
        private System.Windows.Forms.TextBox tbox_Loader_Init_PtpSpeed;
        private System.Windows.Forms.TextBox tbox_Loader_Init_Velocity;
        private System.Windows.Forms.TextBox tbox_Loader_Init_Position;
        private System.Windows.Forms.ComboBox cbbox_Loader_SW_LimitMode;
        private System.Windows.Forms.TextBox tbox_Loader_SW_PlusLimit;
        private System.Windows.Forms.TextBox tbox_Loader_SW_MinusLimit;
        private System.Windows.Forms.TextBox tbox_Loader_Home_VelLast;
        private System.Windows.Forms.TextBox tbox_Loader_Home_Vel3;
        private System.Windows.Forms.TextBox tbox_Loader_Home_Vel2;
        private System.Windows.Forms.TextBox tbox_Loader_Home_Vel1;
        private System.Windows.Forms.ComboBox cbbox_Loader_Home_Level;
        private System.Windows.Forms.ComboBox cbbox_Loader_Home_Signal;
        private System.Windows.Forms.ComboBox cbbox_Loader_ServoOnLevel;
        private System.Windows.Forms.ComboBox cbbox_Loader_PlusEndLimit;
        private System.Windows.Forms.ComboBox cbbox_Loader_MinusEndLimit;
        private System.Windows.Forms.ComboBox cbbox_Loader_Alarm;
        private System.Windows.Forms.ComboBox cbbox_Loader_Inposition;
        private System.Windows.Forms.ComboBox cbbox_Loader_AbsRelMode;
        private System.Windows.Forms.ComboBox cbbox_Loader_EncInput;
        private System.Windows.Forms.ComboBox cbbox_Loader_PulseOutput;
        private System.Windows.Forms.ComboBox cbbox_Loader_VelProfileMode;
        private System.Windows.Forms.TextBox tbox_Loader_AxisNo;
        private System.Windows.Forms.Label lbl_Loader_Init_InPosition;
        private System.Windows.Forms.Label lbl_Loader_Init_Decelation;
        private System.Windows.Forms.Label lbl_Loader_Init_Accelation;
        private System.Windows.Forms.Label lbl_Loader_SW_LimitStopMode;
        private System.Windows.Forms.Label lbl_Loader_SW_LimitEnable;
        private System.Windows.Forms.Label lbl_Loader_Home_ClearTime;
        private System.Windows.Forms.Label lbl_Loader_Home_ZPhase;
        private System.Windows.Forms.Label lbl_Loader_Home_Offset;
        private System.Windows.Forms.Label lbl_Loader_Home_Accelation2;
        private System.Windows.Forms.Label lbl_Loader_Home_Accelation1;
        private System.Windows.Forms.Label lbl_Loader_Home_Direction;
        private System.Windows.Forms.Label lbl_Loader_StopLevel;
        private System.Windows.Forms.Label lbl_Loader_StopMode;
        private System.Windows.Forms.Label lbl_Loader_ZPhase;
        private System.Windows.Forms.Label lbl_Loader_AlarmResetLevel;
        private System.Windows.Forms.Label lbl_Loader_EncoderType;
        private System.Windows.Forms.Label lbl_Loader_Unit;
        private System.Windows.Forms.Label lbl_Loader_MaxVelocitu;
        private System.Windows.Forms.Label lbl_Loader_MinVelocitu;
        private System.Windows.Forms.Label lbl_Loader_Pulse;
        private System.Windows.Forms.Label lbl_Loader_Init_AccelationTime;
        private System.Windows.Forms.Label lbl_Loader_Init_PtpSpeed;
        private System.Windows.Forms.Label lbl_Loader_Init_Velocity;
        private System.Windows.Forms.Label lbl_Loader_Init_Position;
        private System.Windows.Forms.Label lbl_Loader_MinusEndLimit;
        private System.Windows.Forms.Label lbl_Loader_SW_LimitMode;
        private System.Windows.Forms.Label lbl_Loader_Init;
        private System.Windows.Forms.Label lbl_Loader_SW_MinusLimit;
        private System.Windows.Forms.Label lbl_Loader_SW;
        private System.Windows.Forms.Label lbl_Loader_Home_Vel3;
        private System.Windows.Forms.Label lbl_Loader_SW_PlusLimit;
        private System.Windows.Forms.Label lbl_Loader_Home_Level;
        private System.Windows.Forms.Label lbl_Loader_Alarm;
        private System.Windows.Forms.Label lbl_Loader_InPosition;
        private System.Windows.Forms.Label lbl_Loader_AbsRelMode;
        private System.Windows.Forms.Label lbl_Loader_MotionSignalSetting;
        private System.Windows.Forms.Label lbl_Loader_Home;
        private System.Windows.Forms.Label lbl_Loader_PulseOutput;
        private System.Windows.Forms.Label lbl_Loader_EncInput;
        private System.Windows.Forms.Label lbl_Loader_Home_VelLast;
        private System.Windows.Forms.Label lbl_Loader_VelProfileMode;
        private System.Windows.Forms.Label lbl_Loader_Home_Vel2;
        private System.Windows.Forms.Label lbl_Loader_PlusEndLimit;
        private System.Windows.Forms.Label lbl_Loader_AxisNo;
        private System.Windows.Forms.Label lbl_Loader_Home_Vel1;
        private System.Windows.Forms.Label lbl_Loader_Home_Signal;
        private System.Windows.Forms.Label lbl_Loader_ServoOnLevel;
        private System.Windows.Forms.Label lbl_Loader_AxisParameter;
        private System.Windows.Forms.TextBox tbox_Process_SWLimitHigh;
        private System.Windows.Forms.TextBox tbox_Process_SWLimitLow;
        private System.Windows.Forms.TextBox tbox_Process_PtpSpeed;
        private System.Windows.Forms.TextBox tbox_Process_StepSpeed;
        private System.Windows.Forms.TextBox tbox_Process_JogSpeed;
        private System.Windows.Forms.TextBox tbox_Process_HomeAcceleration;
        private System.Windows.Forms.TextBox tbox_Process_Home2Velocity;
        private System.Windows.Forms.TextBox tbox_Process_Home1Velocity;
        private System.Windows.Forms.TextBox tbox_Process_DefaultAcceleration;
        private System.Windows.Forms.TextBox tbox_Process_DefauleVelocity;
        private System.Windows.Forms.TextBox tbox_Process_InPosition;
        private System.Windows.Forms.TextBox tbox_Process_MoveTimeOut;
        private System.Windows.Forms.TextBox tbox_Process_SpeedRate;
        private System.Windows.Forms.TextBox tbox_Process_PositionRate;
        private System.Windows.Forms.TextBox tbox_Process_AxisNo;
        private System.Windows.Forms.Label lbl_Process_Home2Velocity;
        private System.Windows.Forms.Label lbl_Process_SWLimitLow;
        private System.Windows.Forms.Label lbl_Process_Home1Velocity;
        private System.Windows.Forms.Label lbl_Process_DefaultAcceleration;
        private System.Windows.Forms.Label lbl_Process_MoveTimeOut;
        private System.Windows.Forms.Label lbl_Process_DefauleVelocity;
        private System.Windows.Forms.Label lbl_Process_StepSpeed;
        private System.Windows.Forms.Label lbl_Process_PositionRate;
        private System.Windows.Forms.Label lbl_Process_SpeedRate;
        private System.Windows.Forms.Label lbl_Process_InPosition;
        private System.Windows.Forms.Label lbl_Process_AccelationTime;
        private System.Windows.Forms.Label lbl_Process_HomeAcceleration;
        private System.Windows.Forms.Label lbl_Process_AxisNo;
        private System.Windows.Forms.Label lbl_Process_SWLimitHigh;
        private System.Windows.Forms.Label lbl_Process_PtpSpeed;
        private System.Windows.Forms.Label lbl_Process_JogSpeed;
        private System.Windows.Forms.Label lbl_Process_AxisParameter;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.TextBox tbox_Process_AccelationTime;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pn_Loader_31;
        private System.Windows.Forms.Label lbl_Loader_31;
        private System.Windows.Forms.Panel pn_Loader_15;
        private System.Windows.Forms.Label lbl_Loader_15;
        private System.Windows.Forms.Panel pn_Loader_14;
        private System.Windows.Forms.Label lbl_Loader_14;
        private System.Windows.Forms.Panel pn_Loader_13;
        private System.Windows.Forms.Label lbl_Loader_13;
        private System.Windows.Forms.Panel pn_Loader_12;
        private System.Windows.Forms.Label lbl_Loader_12;
        private System.Windows.Forms.Panel pn_Loader_11;
        private System.Windows.Forms.Label lbl_Loader_11;
        private System.Windows.Forms.Panel pn_Loader_10;
        private System.Windows.Forms.Label lbl_Loader_10;
        private System.Windows.Forms.Panel pn_Loader_09;
        private System.Windows.Forms.Label lbl_Loader_09;
        private System.Windows.Forms.Panel pn_Loader_08;
        private System.Windows.Forms.Label lbl_Loader_08;
        private System.Windows.Forms.Panel pn_Loader_07;
        private System.Windows.Forms.Label lbl_Loader_07;
        private System.Windows.Forms.Panel pn_Loader_06;
        private System.Windows.Forms.Label lbl_Loader_06;
        private System.Windows.Forms.Panel pn_Loader_05;
        private System.Windows.Forms.Label lbl_Loader_05;
        private System.Windows.Forms.Panel pn_Loader_04;
        private System.Windows.Forms.Label lbl_Loader_04;
        private System.Windows.Forms.Panel pn_Loader_36;
        private System.Windows.Forms.Label lbl_Loader_36;
        private System.Windows.Forms.Panel pn_Loader_35;
        private System.Windows.Forms.Label lbl_Loader_35;
        private System.Windows.Forms.Panel pn_Loader_33;
        private System.Windows.Forms.Label lbl_Loader_33;
        private System.Windows.Forms.Panel pn_Loader_32;
        private System.Windows.Forms.Label lbl_Loader_32;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel pn_Process_21;
        private System.Windows.Forms.Label lbl_Process_21;
        private System.Windows.Forms.Panel pn_Process_19;
        private System.Windows.Forms.Label lbl_Process_19;
        private System.Windows.Forms.Panel pn_Process_18;
        private System.Windows.Forms.Label lbl_Process_18;
        private System.Windows.Forms.Panel pn_Process_14;
        private System.Windows.Forms.Label lbl_Process_14;
        private System.Windows.Forms.Panel pn_Process_13;
        private System.Windows.Forms.Label lbl_Process_13;
        private System.Windows.Forms.Panel pn_Process_12;
        private System.Windows.Forms.Label lbl_Process_12;
        private System.Windows.Forms.Panel pn_Process_11;
        private System.Windows.Forms.Label lbl_Process_11;
        private System.Windows.Forms.Panel pn_Process_10;
        private System.Windows.Forms.Label lbl_Process_10;
        private System.Windows.Forms.Panel pn_Process_09;
        private System.Windows.Forms.Label lbl_Process_09;
        private System.Windows.Forms.Panel pn_Process_22;
        private System.Windows.Forms.Label lbl_Process_22;
        private System.Windows.Forms.Panel pn_Process_07;
        private System.Windows.Forms.Label lbl_Process_07;
        private System.Windows.Forms.Panel pn_Process_06;
        private System.Windows.Forms.Label lbl_Process_06;
        private System.Windows.Forms.Panel pn_Process_05;
        private System.Windows.Forms.Label lbl_Process_05;
        private System.Windows.Forms.Panel pn_Process_04;
        private System.Windows.Forms.Label lbl_Process_04;
        private System.Windows.Forms.Panel pn_Process_17;
        private System.Windows.Forms.Label lbl_Process_17;
        private System.Windows.Forms.Panel pn_Process_03;
        private System.Windows.Forms.Label lbl_Process_03;
        private System.Windows.Forms.Panel pn_Process_02;
        private System.Windows.Forms.Label lbl_Process_02;
        private System.Windows.Forms.Panel pn_Process_01;
        private System.Windows.Forms.Label lbl_Process_01;
        private System.Windows.Forms.Panel pn_Process_20;
        private System.Windows.Forms.Label lbl_Process_20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel pn_Unloader_3;
        private System.Windows.Forms.Label lbl_Unloader_3;
        private System.Windows.Forms.Panel pn_Unloader_0;
        private System.Windows.Forms.Label lbl_Unloader_0;
        private System.Windows.Forms.Panel pn_Unloader_30;
        private System.Windows.Forms.Label lbl_Unloader_30;
        private System.Windows.Forms.Panel pn_Unloader_29;
        private System.Windows.Forms.Label lbl_Unloader_29;
        private System.Windows.Forms.Panel pn_Unloader_28;
        private System.Windows.Forms.Label lbl_Unloader_28;
        private System.Windows.Forms.Panel pn_Unloader_27;
        private System.Windows.Forms.Label lbl_Unloader_27;
        private System.Windows.Forms.Panel pn_Unloader_26;
        private System.Windows.Forms.Label lbl_Unloader_26;
        private System.Windows.Forms.Panel pn_Unloader_24;
        private System.Windows.Forms.Label lbl_Unloader_24;
        private System.Windows.Forms.Panel pn_Unloader_23;
        private System.Windows.Forms.Label lbl_Unloader_23;
        private System.Windows.Forms.Panel pn_Unloader_21;
        private System.Windows.Forms.Label lbl_Unloader_21;
        private System.Windows.Forms.Panel pn_Unloader_20;
        private System.Windows.Forms.Label lbl_Unloader_20;
        private System.Windows.Forms.Panel pn_Unloader_19;
        private System.Windows.Forms.Label lbl_Unloader_19;
        private System.Windows.Forms.Panel pn_Unloader_18;
        private System.Windows.Forms.Label lbl_Unloader_18;
        private System.Windows.Forms.Panel pn_Unloader_17;
        private System.Windows.Forms.Label lbl_Unloader_17;
        private System.Windows.Forms.Panel pn_Unloader_16;
        private System.Windows.Forms.Label lbl_Unloader_16;
        private System.Windows.Forms.Panel pn_Unloader_41;
        private System.Windows.Forms.Label lbl_Unloader_41;
        private System.Windows.Forms.Panel pn_Unloader_40;
        private System.Windows.Forms.Label lbl_Unloader_40;
        private System.Windows.Forms.Panel pn_Unloader_39;
        private System.Windows.Forms.Label lbl_Unloader_39;
        private System.Windows.Forms.Panel pn_Unloader_38;
        private System.Windows.Forms.Label lbl_Unloader_38;
        private System.Windows.Forms.Panel pn_Unloader_1;
        private System.Windows.Forms.Label lbl_Unloader_1;
        private System.Windows.Forms.Panel pn_Unloader_37;
        private System.Windows.Forms.Label lbl_Unloader_37;
        private System.Windows.Forms.Panel pn_Unloader_2;
        private System.Windows.Forms.Label lbl_Unloader_2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_Unloader_AxisParameter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbox_Unloader_Pulse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbox_Unloader_Unit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbox_Unloader_MinVelocitu;
        private System.Windows.Forms.Label lbl_Unloader_AxisNo;
        private System.Windows.Forms.TextBox tbox_Unloader_MaxVelocitu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbox_Unloader_EncoderType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbbox_Unloader_AlarmResetLevel;
        private System.Windows.Forms.Label lbl_Unloader_VelProfileMode;
        private System.Windows.Forms.ComboBox cbbox_Unloader_StopLevel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbbox_Unloader_StopMode;
        private System.Windows.Forms.Label lbl_Unloader_EncInput;
        private System.Windows.Forms.ComboBox cbbox_Unloader_ZPhase;
        private System.Windows.Forms.Label lbl_Unloader_PulseOutput;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_Offset;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_ClearTime;
        private System.Windows.Forms.Label lbl_Unloader_MotionSignalSetting;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_Accelation2;
        private System.Windows.Forms.Label lbl_Unloader_AbsRelMode;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_Accelation1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbbox_Unloader_Home_ZPhase;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbbox_Unloader_Home_Direction;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbbox_Unloader_SW_LimitEnable;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbbox_Unloader_SW_LimitStopMode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbox_Unloader_Init_InPosition;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbox_Unloader_Init_Decelation;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbox_Unloader_Init_Accelation;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbox_Unloader_Init_AccelationTime;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbox_Unloader_Init_PtpSpeed;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbox_Unloader_Init_Velocity;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbox_Unloader_Init_Position;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cbbox_Unloader_SW_LimitMode;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbox_Unloader_SW_PlusLimit;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbox_Unloader_SW_MinusLimit;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_VelLast;
        private System.Windows.Forms.Label lbl_Unloader_MinVelocitu;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_Vel3;
        private System.Windows.Forms.Label lbl_Unloader_MaxVelocitu;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_Vel2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tbox_Unloader_Home_Vel1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cbbox_Unloader_Home_Level;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbbox_Unloader_Home_Signal;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cbbox_Unloader_ServoOnLevel;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cbbox_Unloader_PlusEndLimit;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox cbbox_Unloader_MinusEndLismit;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbbox_Unloader_Alarm;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cbbox_Unloader_Inposition;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox cbbox_Unloader_AbsRelMode;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox cbbox_Unloader_EncInput;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox cbbox_Unloader_PulseOutput;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cbbox_Unloader_VelProfileMode;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbox_Unloader_AxisNo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
    }
}

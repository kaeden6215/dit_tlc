﻿namespace DIT.TLC.UI
{
    partial class LogAlarmlog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvAlarmlog = new System.Windows.Forms.ListView();
            this.colAlarmlogDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAlarmlogTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAlarmlogOnlineState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAlarmlogSafetyMode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAlarmlogContents = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAlarmlogSolution = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.calendarAlarmlog = new DIT.TLC.UI.LOG.UcrlCalendar();
            this.SuspendLayout();
            // 
            // lvAlarmlog
            // 
            this.lvAlarmlog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAlarmlogDate,
            this.colAlarmlogTime,
            this.colAlarmlogOnlineState,
            this.colAlarmlogSafetyMode,
            this.colAlarmlogContents,
            this.colAlarmlogSolution});
            this.lvAlarmlog.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvAlarmlog.GridLines = true;
            this.lvAlarmlog.Location = new System.Drawing.Point(316, 12);
            this.lvAlarmlog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lvAlarmlog.Name = "lvAlarmlog";
            this.lvAlarmlog.Size = new System.Drawing.Size(1420, 832);
            this.lvAlarmlog.TabIndex = 1;
            this.lvAlarmlog.UseCompatibleStateImageBehavior = false;
            this.lvAlarmlog.View = System.Windows.Forms.View.Details;
            // 
            // colAlarmlogDate
            // 
            this.colAlarmlogDate.Text = "날짜";
            this.colAlarmlogDate.Width = 150;
            // 
            // colAlarmlogTime
            // 
            this.colAlarmlogTime.Text = "시간";
            this.colAlarmlogTime.Width = 150;
            // 
            // colAlarmlogOnlineState
            // 
            this.colAlarmlogOnlineState.Text = "온라인 상태";
            this.colAlarmlogOnlineState.Width = 150;
            // 
            // colAlarmlogSafetyMode
            // 
            this.colAlarmlogSafetyMode.Text = "SAFETY MODE";
            this.colAlarmlogSafetyMode.Width = 150;
            // 
            // colAlarmlogContents
            // 
            this.colAlarmlogContents.Text = "알람 내용";
            this.colAlarmlogContents.Width = 239;
            // 
            // colAlarmlogSolution
            // 
            this.colAlarmlogSolution.Text = "알람 해결법";
            this.colAlarmlogSolution.Width = 550;
            // 
            // calendarAlarmlog
            // 
            this.calendarAlarmlog.BackColor = System.Drawing.SystemColors.Control;
            this.calendarAlarmlog.Location = new System.Drawing.Point(4, 2);
            this.calendarAlarmlog.Name = "calendarAlarmlog";
            this.calendarAlarmlog.Size = new System.Drawing.Size(308, 875);
            this.calendarAlarmlog.TabIndex = 2;
            // 
            // LogAlarmlog
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.calendarAlarmlog);
            this.Controls.Add(this.lvAlarmlog);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "LogAlarmlog";
            this.Size = new System.Drawing.Size(1740, 875);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView lvAlarmlog;
        private System.Windows.Forms.ColumnHeader colAlarmlogDate;
        private System.Windows.Forms.ColumnHeader colAlarmlogTime;
        private System.Windows.Forms.ColumnHeader colAlarmlogOnlineState;
        private System.Windows.Forms.ColumnHeader colAlarmlogSafetyMode;
        private System.Windows.Forms.ColumnHeader colAlarmlogContents;
        private System.Windows.Forms.ColumnHeader colAlarmlogSolution;
        private LOG.UcrlCalendar calendarAlarmlog;
    }
}

﻿namespace DIT.TLC.UI.LOG
{
    partial class UcrlLogSubMenu
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnMeasure = new System.Windows.Forms.Button();
            this.btnMovefail = new System.Windows.Forms.Button();
            this.btnOpcall = new System.Windows.Forms.Button();
            this.btnParameter = new System.Windows.Forms.Button();
            this.btnAlarm = new System.Windows.Forms.Button();
            this.btnButtonAction = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.btnMeasure);
            this.splitContainer1.Panel2.Controls.Add(this.btnMovefail);
            this.splitContainer1.Panel2.Controls.Add(this.btnOpcall);
            this.splitContainer1.Panel2.Controls.Add(this.btnParameter);
            this.splitContainer1.Panel2.Controls.Add(this.btnAlarm);
            this.splitContainer1.Panel2.Controls.Add(this.btnButtonAction);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 1;
            // 
            // btnMeasure
            // 
            this.btnMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnMeasure.Location = new System.Drawing.Point(2, 303);
            this.btnMeasure.Name = "btnMeasure";
            this.btnMeasure.Size = new System.Drawing.Size(132, 50);
            this.btnMeasure.TabIndex = 45;
            this.btnMeasure.Text = "측정 로그";
            this.btnMeasure.UseVisualStyleBackColor = false;
            this.btnMeasure.Click += new System.EventHandler(this.btnMeasure_Click);
            // 
            // btnMovefail
            // 
            this.btnMovefail.BackColor = System.Drawing.SystemColors.Control;
            this.btnMovefail.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMovefail.ForeColor = System.Drawing.Color.Black;
            this.btnMovefail.Location = new System.Drawing.Point(2, 247);
            this.btnMovefail.Name = "btnMovefail";
            this.btnMovefail.Size = new System.Drawing.Size(132, 50);
            this.btnMovefail.TabIndex = 44;
            this.btnMovefail.Text = "이동실패 로그";
            this.btnMovefail.UseVisualStyleBackColor = false;
            this.btnMovefail.Click += new System.EventHandler(this.btnMovefail_Click);
            // 
            // btnOpcall
            // 
            this.btnOpcall.BackColor = System.Drawing.SystemColors.Control;
            this.btnOpcall.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnOpcall.ForeColor = System.Drawing.Color.Black;
            this.btnOpcall.Location = new System.Drawing.Point(2, 191);
            this.btnOpcall.Name = "btnOpcall";
            this.btnOpcall.Size = new System.Drawing.Size(132, 50);
            this.btnOpcall.TabIndex = 43;
            this.btnOpcall.Text = "오피콜 로그";
            this.btnOpcall.UseVisualStyleBackColor = false;
            this.btnOpcall.Click += new System.EventHandler(this.btnOpcall_Click);
            // 
            // btnParameter
            // 
            this.btnParameter.BackColor = System.Drawing.SystemColors.Control;
            this.btnParameter.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnParameter.ForeColor = System.Drawing.Color.Black;
            this.btnParameter.Location = new System.Drawing.Point(2, 135);
            this.btnParameter.Name = "btnParameter";
            this.btnParameter.Size = new System.Drawing.Size(132, 50);
            this.btnParameter.TabIndex = 42;
            this.btnParameter.Text = "파라미터 로그";
            this.btnParameter.UseVisualStyleBackColor = false;
            this.btnParameter.Click += new System.EventHandler(this.btnParameter_Click);
            // 
            // btnAlarm
            // 
            this.btnAlarm.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlarm.ForeColor = System.Drawing.Color.Black;
            this.btnAlarm.Location = new System.Drawing.Point(2, 23);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Size = new System.Drawing.Size(132, 50);
            this.btnAlarm.TabIndex = 40;
            this.btnAlarm.Text = "알람 로그";
            this.btnAlarm.UseVisualStyleBackColor = false;
            this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
            // 
            // btnButtonAction
            // 
            this.btnButtonAction.BackColor = System.Drawing.SystemColors.Control;
            this.btnButtonAction.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnButtonAction.ForeColor = System.Drawing.Color.Black;
            this.btnButtonAction.Location = new System.Drawing.Point(2, 79);
            this.btnButtonAction.Name = "btnButtonAction";
            this.btnButtonAction.Size = new System.Drawing.Size(132, 50);
            this.btnButtonAction.TabIndex = 41;
            this.btnButtonAction.Text = "버튼액션 로그";
            this.btnButtonAction.UseVisualStyleBackColor = false;
            this.btnButtonAction.Click += new System.EventHandler(this.btnButtonAction_Click);
            // 
            // UcrlLogSubMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "UcrlLogSubMenu";
            this.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnMeasure;
        private System.Windows.Forms.Button btnMovefail;
        private System.Windows.Forms.Button btnOpcall;
        private System.Windows.Forms.Button btnParameter;
        private System.Windows.Forms.Button btnAlarm;
        private System.Windows.Forms.Button btnButtonAction;
    }
}

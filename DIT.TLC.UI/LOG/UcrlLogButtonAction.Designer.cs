﻿namespace DIT.TLC.UI.LOG
{
    partial class LogButtonAction
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvButtonAction = new System.Windows.Forms.ListView();
            this.colButtonActionDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionPushPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionPushBtn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.calendarButtonAction = new DIT.TLC.UI.LOG.UcrlCalendar();
            this.SuspendLayout();
            // 
            // lvButtonAction
            // 
            this.lvButtonAction.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colButtonActionDate,
            this.colButtonActionTime,
            this.colButtonActionPushPosition,
            this.colButtonActionPushBtn,
            this.colButtonActionUser});
            this.lvButtonAction.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvButtonAction.GridLines = true;
            this.lvButtonAction.Location = new System.Drawing.Point(316, 12);
            this.lvButtonAction.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lvButtonAction.Name = "lvButtonAction";
            this.lvButtonAction.Size = new System.Drawing.Size(1420, 832);
            this.lvButtonAction.TabIndex = 3;
            this.lvButtonAction.UseCompatibleStateImageBehavior = false;
            this.lvButtonAction.View = System.Windows.Forms.View.Details;
            // 
            // colButtonActionDate
            // 
            this.colButtonActionDate.Text = "날짜";
            this.colButtonActionDate.Width = 150;
            // 
            // colButtonActionTime
            // 
            this.colButtonActionTime.Text = "시간";
            this.colButtonActionTime.Width = 150;
            // 
            // colButtonActionPushPosition
            // 
            this.colButtonActionPushPosition.Text = "누른 위치";
            this.colButtonActionPushPosition.Width = 495;
            // 
            // colButtonActionPushBtn
            // 
            this.colButtonActionPushBtn.Text = "누른 버튼";
            this.colButtonActionPushBtn.Width = 377;
            // 
            // colButtonActionUser
            // 
            this.colButtonActionUser.Text = "누른 사용자";
            this.colButtonActionUser.Width = 210;
            // 
            // calendarButtonAction
            // 
            this.calendarButtonAction.BackColor = System.Drawing.SystemColors.Control;
            this.calendarButtonAction.Location = new System.Drawing.Point(4, 2);
            this.calendarButtonAction.Name = "calendarButtonAction";
            this.calendarButtonAction.Size = new System.Drawing.Size(308, 875);
            this.calendarButtonAction.TabIndex = 4;
            // 
            // LogButtonAction
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.calendarButtonAction);
            this.Controls.Add(this.lvButtonAction);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "LogButtonAction";
            this.Size = new System.Drawing.Size(1740, 875);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvButtonAction;
        private System.Windows.Forms.ColumnHeader colButtonActionDate;
        private System.Windows.Forms.ColumnHeader colButtonActionTime;
        private System.Windows.Forms.ColumnHeader colButtonActionPushPosition;
        private System.Windows.Forms.ColumnHeader colButtonActionPushBtn;
        private System.Windows.Forms.ColumnHeader colButtonActionUser;
        private UcrlCalendar calendarButtonAction;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.UI.LOG
{
    public partial class Log_SubMenu : UserControl
    {
        Log_Alarmlog log_alarmlog = new Log_Alarmlog();
        Log_ButtonAction log_buttonaction = new Log_ButtonAction();
        Log_Parameter log_parameter = new Log_Parameter();
        Log_Opcall log_opcall = new Log_Opcall();
        Log_Movefail log_movefail = new Log_Movefail();
        Log_Measure log_measure = new Log_Measure();

        public Log_SubMenu()
        {
            InitializeComponent();

            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(log_alarmlog);
            log_alarmlog.Location = new Point(0, 0);
        }

        // 서브메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void SubMenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = Color.DarkSlateGray;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        /// <summary>
        /// 알람 로그 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Alarm_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(log_alarmlog);
            log_alarmlog.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }
        
        /// <summary>
        /// 버튼액션 로그 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ButtonAction_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(log_buttonaction);
            log_buttonaction.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 파라미터 로그 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Parameter_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(log_parameter);
            log_parameter.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 오피콜 로그 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Opcall_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(log_opcall);
            log_opcall.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 이동실패 로그 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Movefail_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(log_movefail);
            log_movefail.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 측정 로그 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Measure_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(log_measure);
            log_measure.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }
    }
}

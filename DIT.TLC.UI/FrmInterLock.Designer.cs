﻿namespace DIT.TLC.UI
{
    partial class FrmInterLock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblDetailMsg = new System.Windows.Forms.Label();
            this.lblInterlockMsg = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.btnCheck = new System.Windows.Forms.Button();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.btnCheck);
            this.panel11.Controls.Add(this.lblDetailMsg);
            this.panel11.Controls.Add(this.lblInterlockMsg);
            this.panel11.Controls.Add(this.label62);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(584, 562);
            this.panel11.TabIndex = 13;
            // 
            // lblDetailMsg
            // 
            this.lblDetailMsg.AutoEllipsis = true;
            this.lblDetailMsg.BackColor = System.Drawing.Color.White;
            this.lblDetailMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDetailMsg.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailMsg.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.lblDetailMsg.ForeColor = System.Drawing.Color.Black;
            this.lblDetailMsg.Location = new System.Drawing.Point(0, 344);
            this.lblDetailMsg.Name = "lblDetailMsg";
            this.lblDetailMsg.Size = new System.Drawing.Size(582, 155);
            this.lblDetailMsg.TabIndex = 14;
            this.lblDetailMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInterlockMsg
            // 
            this.lblInterlockMsg.AutoEllipsis = true;
            this.lblInterlockMsg.BackColor = System.Drawing.Color.White;
            this.lblInterlockMsg.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblInterlockMsg.Font = new System.Drawing.Font("맑은 고딕", 20F);
            this.lblInterlockMsg.ForeColor = System.Drawing.Color.Red;
            this.lblInterlockMsg.Location = new System.Drawing.Point(0, 50);
            this.lblInterlockMsg.Name = "lblInterlockMsg";
            this.lblInterlockMsg.Size = new System.Drawing.Size(582, 294);
            this.lblInterlockMsg.TabIndex = 11;
            this.lblInterlockMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.AutoEllipsis = true;
            this.label62.BackColor = System.Drawing.Color.Gainsboro;
            this.label62.Dock = System.Windows.Forms.DockStyle.Top;
            this.label62.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(0, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(582, 50);
            this.label62.TabIndex = 9;
            this.label62.Text = "■ Interlock";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(0, 499);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(581, 60);
            this.btnCheck.TabIndex = 15;
            this.btnCheck.Text = "Check";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // FrmInterLock
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(584, 562);
            this.Controls.Add(this.panel11);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInterLock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmInterLock";
            this.panel11.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel11;        
        internal System.Windows.Forms.Label label62;
        internal System.Windows.Forms.Label lblInterlockMsg;
        internal System.Windows.Forms.Label lblDetailMsg;
        private System.Windows.Forms.Button btnCheck;
    }
}
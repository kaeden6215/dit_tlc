﻿namespace DIT.TLC.UI
{
    partial class FrmCellInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCellInfo));
            this.lblCellInfo = new System.Windows.Forms.Label();
            this.btnCellAScrap = new System.Windows.Forms.Button();
            this.btnCellACancel = new System.Windows.Forms.Button();
            this.lblCellAPosition = new System.Windows.Forms.Label();
            this.btnCellBScrap = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblCellAId = new System.Windows.Forms.Label();
            this.lblCellAIdInfo = new System.Windows.Forms.Label();
            this.btnCellAUnScrap = new System.Windows.Forms.Button();
            this.lblCellAInfo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCellBId = new System.Windows.Forms.Label();
            this.lblCellBIdInfo = new System.Windows.Forms.Label();
            this.lblCellBPosition = new System.Windows.Forms.Label();
            this.btnCellBUnScrap = new System.Windows.Forms.Button();
            this.btnCellBCancel = new System.Windows.Forms.Button();
            this.lblCellBInfo = new System.Windows.Forms.Label();
            this.tmrCellInfoUiWorker = new System.Windows.Forms.Timer(this.components);
            this.panel12.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCellInfo
            // 
            this.lblCellInfo.AutoEllipsis = true;
            this.lblCellInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCellInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCellInfo.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCellInfo.ForeColor = System.Drawing.Color.Black;
            this.lblCellInfo.Location = new System.Drawing.Point(0, 0);
            this.lblCellInfo.Name = "lblCellInfo";
            this.lblCellInfo.Size = new System.Drawing.Size(476, 24);
            this.lblCellInfo.TabIndex = 10;
            this.lblCellInfo.Text = "■ Cell Info";
            this.lblCellInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellAScrap
            // 
            this.btnCellAScrap.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellAScrap.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCellAScrap.Enabled = false;
            this.btnCellAScrap.ForeColor = System.Drawing.Color.Black;
            this.btnCellAScrap.Location = new System.Drawing.Point(3, 200);
            this.btnCellAScrap.Name = "btnCellAScrap";
            this.btnCellAScrap.Size = new System.Drawing.Size(70, 45);
            this.btnCellAScrap.TabIndex = 21;
            this.btnCellAScrap.Text = "셀\r\n스크랩";
            this.btnCellAScrap.UseVisualStyleBackColor = false;
            this.btnCellAScrap.Click += new System.EventHandler(this.btnCell_Click);
            // 
            // btnCellACancel
            // 
            this.btnCellACancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellACancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCellACancel.ForeColor = System.Drawing.Color.Black;
            this.btnCellACancel.Location = new System.Drawing.Point(155, 200);
            this.btnCellACancel.Name = "btnCellACancel";
            this.btnCellACancel.Size = new System.Drawing.Size(70, 45);
            this.btnCellACancel.TabIndex = 22;
            this.btnCellACancel.Text = "취소";
            this.btnCellACancel.UseVisualStyleBackColor = false;
            this.btnCellACancel.Click += new System.EventHandler(this.btnCell_Click);
            // 
            // lblCellAPosition
            // 
            this.lblCellAPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellAPosition.ForeColor = System.Drawing.Color.Black;
            this.lblCellAPosition.Location = new System.Drawing.Point(3, 51);
            this.lblCellAPosition.Name = "lblCellAPosition";
            this.lblCellAPosition.Size = new System.Drawing.Size(222, 54);
            this.lblCellAPosition.TabIndex = 52;
            this.lblCellAPosition.Text = "더블클릭한 셀 포지션";
            this.lblCellAPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellBScrap
            // 
            this.btnCellBScrap.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellBScrap.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCellBScrap.Enabled = false;
            this.btnCellBScrap.ForeColor = System.Drawing.Color.Black;
            this.btnCellBScrap.Location = new System.Drawing.Point(3, 201);
            this.btnCellBScrap.Name = "btnCellBScrap";
            this.btnCellBScrap.Size = new System.Drawing.Size(70, 45);
            this.btnCellBScrap.TabIndex = 53;
            this.btnCellBScrap.Text = "셀\r\n스크랩";
            this.btnCellBScrap.UseVisualStyleBackColor = false;
            this.btnCellBScrap.Click += new System.EventHandler(this.btnCell_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Control;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.lblCellAId);
            this.panel12.Controls.Add(this.lblCellAIdInfo);
            this.panel12.Controls.Add(this.btnCellAUnScrap);
            this.panel12.Controls.Add(this.lblCellAInfo);
            this.panel12.Controls.Add(this.lblCellAPosition);
            this.panel12.Controls.Add(this.btnCellAScrap);
            this.panel12.Controls.Add(this.btnCellACancel);
            this.panel12.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel12.Location = new System.Drawing.Point(4, 27);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(230, 250);
            this.panel12.TabIndex = 494;
            // 
            // lblCellAId
            // 
            this.lblCellAId.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellAId.ForeColor = System.Drawing.Color.Black;
            this.lblCellAId.Location = new System.Drawing.Point(141, 138);
            this.lblCellAId.Name = "lblCellAId";
            this.lblCellAId.Size = new System.Drawing.Size(84, 29);
            this.lblCellAId.TabIndex = 55;
            this.lblCellAId.Text = "0";
            this.lblCellAId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCellAIdInfo
            // 
            this.lblCellAIdInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellAIdInfo.ForeColor = System.Drawing.Color.Black;
            this.lblCellAIdInfo.Location = new System.Drawing.Point(3, 138);
            this.lblCellAIdInfo.Name = "lblCellAIdInfo";
            this.lblCellAIdInfo.Size = new System.Drawing.Size(132, 29);
            this.lblCellAIdInfo.TabIndex = 54;
            this.lblCellAIdInfo.Text = "Cell ID :";
            this.lblCellAIdInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellAUnScrap
            // 
            this.btnCellAUnScrap.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellAUnScrap.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCellAUnScrap.Enabled = false;
            this.btnCellAUnScrap.ForeColor = System.Drawing.Color.Black;
            this.btnCellAUnScrap.Location = new System.Drawing.Point(79, 200);
            this.btnCellAUnScrap.Name = "btnCellAUnScrap";
            this.btnCellAUnScrap.Size = new System.Drawing.Size(70, 45);
            this.btnCellAUnScrap.TabIndex = 53;
            this.btnCellAUnScrap.Text = "셀\r\n언스크랩";
            this.btnCellAUnScrap.UseVisualStyleBackColor = false;
            this.btnCellAUnScrap.Click += new System.EventHandler(this.btnCell_Click);
            // 
            // lblCellAInfo
            // 
            this.lblCellAInfo.AutoEllipsis = true;
            this.lblCellAInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCellAInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCellAInfo.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblCellAInfo.ForeColor = System.Drawing.Color.Black;
            this.lblCellAInfo.Location = new System.Drawing.Point(0, 0);
            this.lblCellAInfo.Name = "lblCellAInfo";
            this.lblCellAInfo.Size = new System.Drawing.Size(228, 20);
            this.lblCellAInfo.TabIndex = 9;
            this.lblCellAInfo.Text = "■ 1열";
            this.lblCellAInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblCellBId);
            this.panel1.Controls.Add(this.lblCellBIdInfo);
            this.panel1.Controls.Add(this.lblCellBPosition);
            this.panel1.Controls.Add(this.btnCellBUnScrap);
            this.panel1.Controls.Add(this.btnCellBCancel);
            this.panel1.Controls.Add(this.lblCellBInfo);
            this.panel1.Controls.Add(this.btnCellBScrap);
            this.panel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel1.Location = new System.Drawing.Point(240, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 250);
            this.panel1.TabIndex = 495;
            // 
            // lblCellBId
            // 
            this.lblCellBId.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellBId.ForeColor = System.Drawing.Color.Black;
            this.lblCellBId.Location = new System.Drawing.Point(141, 138);
            this.lblCellBId.Name = "lblCellBId";
            this.lblCellBId.Size = new System.Drawing.Size(84, 29);
            this.lblCellBId.TabIndex = 57;
            this.lblCellBId.Text = "0";
            this.lblCellBId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCellBIdInfo
            // 
            this.lblCellBIdInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellBIdInfo.ForeColor = System.Drawing.Color.Black;
            this.lblCellBIdInfo.Location = new System.Drawing.Point(3, 138);
            this.lblCellBIdInfo.Name = "lblCellBIdInfo";
            this.lblCellBIdInfo.Size = new System.Drawing.Size(132, 29);
            this.lblCellBIdInfo.TabIndex = 56;
            this.lblCellBIdInfo.Text = "Cell ID :";
            this.lblCellBIdInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCellBPosition
            // 
            this.lblCellBPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellBPosition.ForeColor = System.Drawing.Color.Black;
            this.lblCellBPosition.Location = new System.Drawing.Point(3, 51);
            this.lblCellBPosition.Name = "lblCellBPosition";
            this.lblCellBPosition.Size = new System.Drawing.Size(222, 54);
            this.lblCellBPosition.TabIndex = 56;
            this.lblCellBPosition.Text = "더블클릭한 셀 포지션";
            this.lblCellBPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellBUnScrap
            // 
            this.btnCellBUnScrap.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellBUnScrap.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCellBUnScrap.Enabled = false;
            this.btnCellBUnScrap.ForeColor = System.Drawing.Color.Black;
            this.btnCellBUnScrap.Location = new System.Drawing.Point(79, 201);
            this.btnCellBUnScrap.Name = "btnCellBUnScrap";
            this.btnCellBUnScrap.Size = new System.Drawing.Size(70, 45);
            this.btnCellBUnScrap.TabIndex = 54;
            this.btnCellBUnScrap.Text = "셀\r\n언스크랩";
            this.btnCellBUnScrap.UseVisualStyleBackColor = false;
            this.btnCellBUnScrap.Click += new System.EventHandler(this.btnCell_Click);
            // 
            // btnCellBCancel
            // 
            this.btnCellBCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellBCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCellBCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCellBCancel.Location = new System.Drawing.Point(155, 201);
            this.btnCellBCancel.Name = "btnCellBCancel";
            this.btnCellBCancel.Size = new System.Drawing.Size(70, 45);
            this.btnCellBCancel.TabIndex = 53;
            this.btnCellBCancel.Text = "취소";
            this.btnCellBCancel.UseVisualStyleBackColor = false;
            this.btnCellBCancel.Click += new System.EventHandler(this.btnCell_Click);
            // 
            // lblCellBInfo
            // 
            this.lblCellBInfo.AutoEllipsis = true;
            this.lblCellBInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCellBInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCellBInfo.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblCellBInfo.ForeColor = System.Drawing.Color.Black;
            this.lblCellBInfo.Location = new System.Drawing.Point(0, 0);
            this.lblCellBInfo.Name = "lblCellBInfo";
            this.lblCellBInfo.Size = new System.Drawing.Size(228, 20);
            this.lblCellBInfo.TabIndex = 9;
            this.lblCellBInfo.Text = "■ 2열";
            this.lblCellBInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrCellInfoUiWorker
            // 
            this.tmrCellInfoUiWorker.Enabled = true;
            this.tmrCellInfoUiWorker.Tick += new System.EventHandler(this.tmrCellInfoUiWorker_Tick);
            // 
            // FrmCellInfo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(476, 282);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.lblCellInfo);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCellInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "셀 정보";
            this.panel12.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblCellInfo;
        private System.Windows.Forms.Button btnCellAScrap;
        private System.Windows.Forms.Button btnCellACancel;
        private System.Windows.Forms.Label lblCellAPosition;
        private System.Windows.Forms.Button btnCellBScrap;
        private System.Windows.Forms.Panel panel12;
        internal System.Windows.Forms.Label lblCellAInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCellBCancel;
        internal System.Windows.Forms.Label lblCellBInfo;
        private System.Windows.Forms.Button btnCellAUnScrap;
        private System.Windows.Forms.Button btnCellBUnScrap;
        private System.Windows.Forms.Label lblCellAId;
        private System.Windows.Forms.Label lblCellAIdInfo;
        private System.Windows.Forms.Label lblCellBId;
        private System.Windows.Forms.Label lblCellBIdInfo;
        private System.Windows.Forms.Label lblCellBPosition;
        private System.Windows.Forms.Timer tmrCellInfoUiWorker;
    }
}
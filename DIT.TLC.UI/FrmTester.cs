﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmTester : Form
    {
        private UcrlParameterSubMenu _ucrlParameterSubManu = new UcrlParameterSubMenu(); // 파라미터서브메뉴 UI
        public FrmTester()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            panel1.Controls.Add(_ucrlParameterSubManu);
            _ucrlParameterSubManu.Location = new Point(0, 0);

            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (var ctrl in panel1.Controls)
            {
                IUIUpdate vv = ctrl as IUIUpdate;
                if (vv != null)
                    vv.UIUpdate();
            }
        }
    }
}

﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmCellInfo : Form, IUIUpdate
    {
        private BaseUnit unit;
        private PanelInfo backupPanel1;
        private PanelInfo backupPanel2;

        public FrmCellInfo(BaseUnit _unit)
        {
            InitializeComponent();
            unit = _unit;

            lblCellAPosition.Text = string.Format("{0}의\nCell을 스크랩 하시겠습니까?", unit.Name);
            lblCellBPosition.Text = string.Format("{0}의\nCell을 스크랩 하시겠습니까?", unit.Name);

        }

        private void btnCell_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            // 첫번째 셀 스크랩
            if(btn == btnCellAScrap)
            {
                backupPanel1 = unit.PanelSet.Panel1;
                unit.PanelSet.Panel1 = new PanelInfo();
            }
            // 두번째 셀 스크랩
            if (btn == btnCellBScrap)
            {
                backupPanel2 = unit.PanelSet.Panel2;
                unit.PanelSet.Panel2 = new PanelInfo();

            }
            // 첫번째 셀 언스크랩
            if (btn == btnCellAUnScrap)
            {
                if (backupPanel1 != null)
                    unit.PanelSet.Panel1 = backupPanel1;
            }
            // 두번째 셀 언스크랩  
            if (btn == btnCellBUnScrap)
            {
                if (backupPanel2 != null)
                    unit.PanelSet.Panel2 = backupPanel2;
            }
            // 취소
            if (btn == btnCellACancel || btn == btnCellBCancel)
            {
                this.Close();
            }
        }

        public void UIUpdate()
        {
            if (unit.PanelSet.Panel1.Exists == true) { btnCellAScrap.Enabled = true; btnCellAUnScrap.Enabled = true; }
            else { btnCellAScrap.Enabled = false; btnCellAUnScrap.Enabled = true; }
            if (unit.PanelSet.Panel2.Exists == true) { btnCellBScrap.Enabled = true; btnCellBUnScrap.Enabled = true; }
            else { btnCellBScrap.Enabled = false; btnCellBUnScrap.Enabled = true; }
            
            lblCellAId.Text = unit.PanelSet.Panel1.PanelID;
            lblCellBId.Text = unit.PanelSet.Panel2.PanelID;
        }

        private void tmrCellInfoUiWorker_Tick(object sender, EventArgs e)
        {
            UIUpdate();
        }
    }
}

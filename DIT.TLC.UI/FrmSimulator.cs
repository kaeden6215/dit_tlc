﻿using DIT.TLC.CTRL;
using EquipSimulator;
using EquipSimulator.Acturator;
using EquipView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmSimulator : Form
    {
        private EquipView.UcrlEquipView ucrlEquipView = new EquipView.UcrlEquipView();
        public ServoMotorControl[] Motors;
        private bool _isRunning = false;

        public FrmSimulator()
        {
            InitializeComponent();

            elementHost1.Child = ucrlEquipView;

            Motors = new ServoMotorControl[]
                         {
                CTRL.GG.Equip.LD.CstLoader_A.CstRotationAxis                ,
                CTRL.GG.Equip.LD.CstLoader_A.CstUpDownAxis                  ,
                CTRL.GG.Equip.LD.CstLoader_B.CstRotationAxis                ,
                CTRL.GG.Equip.LD.CstLoader_B.CstUpDownAxis                  ,
                CTRL.GG.Equip.LD.Loader.X1Axis                              ,
                CTRL.GG.Equip.LD.Loader.X2Axis                              ,
                CTRL.GG.Equip.LD.Loader.Y1Axis                              ,
                CTRL.GG.Equip.LD.Loader.Y2Axis                              ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.YAxis                     ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.X1Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.X2Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubY1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubY2Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubT1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubT2Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.YAxis                     ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.X1Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.X2Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubY1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubY2Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubT1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubT2Axis                 ,
                CTRL.GG.Equip.LD.PreAlign.XAxis                             ,
                CTRL.GG.Equip.PROC.FineAlign.XAxis                          ,
                CTRL.GG.Equip.PROC.IRCutStage_A.YAxis                       ,
                CTRL.GG.Equip.PROC.IRCutStage_B.YAxis                       ,
                CTRL.GG.Equip.PROC.LaserHead.XAxis                          ,
                CTRL.GG.Equip.PROC.LaserHead.ZAxis                          ,
                CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.YAxis               ,
                CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.YAxis               ,
                CTRL.GG.Equip.PROC.BreakStage_A.YAxis                       ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubX1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubX2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubY1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubY2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubT1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubT2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.YAxis                       ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubX1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubX2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubY1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubY2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubT1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubT2Axis                   ,
                CTRL.GG.Equip.PROC.BreakingHead.X1Axis                      ,
                CTRL.GG.Equip.PROC.BreakingHead.X2Axis                      ,
                CTRL.GG.Equip.PROC.BreakingHead.Z1Axis                      ,
                CTRL.GG.Equip.PROC.BreakingHead.Z2Axis                      ,
                CTRL.GG.Equip.PROC.BreakAlign.XAxis                         ,
                CTRL.GG.Equip.PROC.BreakAlign.ZAxis                         ,
                CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis        ,
                CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis        ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T2Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T2Axis         ,
                CTRL.GG.Equip.UD.InspCamera.XAxis                           ,
                CTRL.GG.Equip.UD.InspCamera.ZAxis                           ,
                CTRL.GG.Equip.UD.InspectionStage_A.YAxis                    ,
                CTRL.GG.Equip.UD.InspectionStage_B.YAxis                    ,
                CTRL.GG.Equip.UD.Unloader.X1Axis                            ,
                CTRL.GG.Equip.UD.Unloader.X2Axis                            ,
                CTRL.GG.Equip.UD.Unloader.Y1Axis                            ,
                CTRL.GG.Equip.UD.Unloader.Y2Axis                            ,
                CTRL.GG.Equip.UD.CstUnloader_A.CstRotationAxis              ,
                CTRL.GG.Equip.UD.CstUnloader_A.CstUpDownAxis                ,
                CTRL.GG.Equip.UD.CstUnloader_B.CstRotationAxis              ,
                CTRL.GG.Equip.UD.CstUnloader_B.CstUpDownAxis                
             };

            lstViewMotorInfo.View = View.Details;
            lstViewMotorInfo.Columns.Add("No.").Width = 30;
            lstViewMotorInfo.Columns.Add("Name").Width = 229;
            lstViewMotorInfo.Columns.Add("HomeB").Width = 54;
            lstViewMotorInfo.Columns.Add("JogP").Width = 37;
            lstViewMotorInfo.Columns.Add("JogN").Width = 39;
            lstViewMotorInfo.Columns.Add("HomeS").Width = 53;
            lstViewMotorInfo.Columns.Add("PtpS").Width = 38;
            lstViewMotorInfo.Columns.Add("Pos").Width = 60;

            foreach (ServoMotorControl s in Motors)
            {
                lstViewMotorInfo.Items.Add(s.InnerAxisNo.ToString()).SubItems.AddRange(new string[] { s.Name, "1", "2", "3", "4", "5", "6" });
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _isRunning = false;
            while (bgWorker.IsBusy)
                Application.DoEvents();

            base.OnClosing(e);
        }

        private void tmrWorker_Tick(object sender, EventArgs e)
        {
            UpdateUI();
            UpdateMotorInfo();
        }

        private void UpdateMotorInfo()
        {
            lstViewMotorInfo.BeginUpdate();

            int idx;
            foreach (ServoMotorControl s in Motors)
            {
                idx = Array.IndexOf(Motors, s);
                lstViewMotorInfo.Items[idx].SubItems[2].Text = s.XB_StatusHomeCompleteBit.vBit ? "1" : "0";
                lstViewMotorInfo.Items[idx].SubItems[3].Text = s.YB_MotorJogPlusMove.vBit ? "1" : "0";
                lstViewMotorInfo.Items[idx].SubItems[4].Text = s.YB_MotorJogMinusMove.vBit ? "1" : "0";
                lstViewMotorInfo.Items[idx].SubItems[5].Text = s.HommingCmdStep.ToString();
                lstViewMotorInfo.Items[idx].SubItems[6].Text = s.PtpMoveCmdStep.ToString();
                lstViewMotorInfo.Items[idx].SubItems[7].Text = s.XF_CurrMotorPosition.ToString();
            }
            lstViewMotorInfo.EndUpdate();
        }

        private void UpdateUI()
        {
            #region 축

            ucrlEquipView.CstLoaderARotation    = CTRL.GG.Equip.LD.CstLoader_A.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstLoaderAUpDown      = CTRL.GG.Equip.LD.CstLoader_A.CstUpDownAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstLoaderBRotation    = CTRL.GG.Equip.LD.CstLoader_B.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstLoaderBUpDown      = CTRL.GG.Equip.LD.CstLoader_B.CstUpDownAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderAX              = CTRL.GG.Equip.LD.Loader.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderBX              = CTRL.GG.Equip.LD.Loader.X2Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderAY              = CTRL.GG.Equip.LD.Loader.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderBY              = CTRL.GG.Equip.LD.Loader.Y2Axis.XF_CurrMotorPosition;

            ucrlEquipView.LoaderTRB1Y           = CTRL.GG.Equip.LD.LoaderTransfer_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRB2Y           = CTRL.GG.Equip.LD.LoaderTransfer_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRB1X           = CTRL.GG.Equip.LD.LoaderTransfer_B.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRB2X           = CTRL.GG.Equip.LD.LoaderTransfer_B.X2Axis.XF_CurrMotorPosition;

            //ucrlEquipView.Sub_LoaderTRB1Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRB2Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRB1T     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRB2T     = CTRL.GG.Equip.
            ucrlEquipView.LoaderTRA1Y           = CTRL.GG.Equip.LD.LoaderTransfer_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRA2Y           = CTRL.GG.Equip.LD.LoaderTransfer_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRA1X           = CTRL.GG.Equip.LD.LoaderTransfer_A.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRA2X           = CTRL.GG.Equip.LD.LoaderTransfer_A.X2Axis.XF_CurrMotorPosition;

            //ucrlEquipView.Sub_LoaderTRA1Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRA2Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRA1T     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRA2T     = CTRL.GG.Equip.
            ucrlEquipView.PreAlignX             = CTRL.GG.Equip.LD.PreAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.FineAlignX            = CTRL.GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageA1Y         = CTRL.GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageA2Y         = CTRL.GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageB1Y         = CTRL.GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageB2Y         = CTRL.GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LaserHeadX            = CTRL.GG.Equip.PROC.LaserHead.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.LaserHeadZ            = CTRL.GG.Equip.PROC.LaserHead.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.AfterIRCutTRAY        = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.AfterIRCutTRBY        = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakTableB1Y         = CTRL.GG.Equip.PROC.BreakStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakTableB2Y         = CTRL.GG.Equip.PROC.BreakStage_B.YAxis.XF_CurrMotorPosition;
            //ucrlEquipView.Sub_BreakTableB1T   = CTRL.GG.Equip. 
            //ucrlEquipView.Sub_BreakTableB2T   = CTRL.GG.Equip.
            ucrlEquipView.BreakTableA1Y         = CTRL.GG.Equip.PROC.BreakStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakTableA2Y         = CTRL.GG.Equip.PROC.BreakStage_A.YAxis.XF_CurrMotorPosition;
            //ucrlEquipView.Sub_BreakTableA1T   = CTRL.GG.Equip.
            //ucrlEquipView.Sub_BreakTableA2T   = CTRL.GG.Equip.
            ucrlEquipView.BreakingHead1X        = CTRL.GG.Equip.PROC.BreakingHead.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakingHead2X        = CTRL.GG.Equip.PROC.BreakingHead.X2Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakingHead1Z        = CTRL.GG.Equip.PROC.BreakingHead.Z1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakingHead2Z        = CTRL.GG.Equip.PROC.BreakingHead.Z2Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign1X          = CTRL.GG.Equip.PROC.BreakAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign2X          = CTRL.GG.Equip.PROC.BreakAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign1Z          = CTRL.GG.Equip.PROC.BreakAlign.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign2Z          = CTRL.GG.Equip.PROC.BreakAlign.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRA1Y       = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRA2Y       = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRB1Y       = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRB2Y       = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA1Y        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA2Y        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA1T        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA2T        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB1Y        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB2Y        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB1T        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB2T        = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T2Axis.XF_CurrMotorPosition;
            ucrlEquipView.InspCameraX           = CTRL.GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspCameraZ           = CTRL.GG.Equip.UD.InspCamera.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageA1Y          = CTRL.GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageA2Y          = CTRL.GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageB1Y          = CTRL.GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageB2Y          = CTRL.GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderAX            = CTRL.GG.Equip.UD.Unloader.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderBX            = CTRL.GG.Equip.UD.Unloader.X2Axis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderAY            = CTRL.GG.Equip.UD.Unloader.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderBY            = CTRL.GG.Equip.UD.Unloader.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderARotation  = CTRL.GG.Equip.UD.CstUnloader_A.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderAUpDown    = CTRL.GG.Equip.UD.CstUnloader_A.CstUpDownAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderBRotation  = CTRL.GG.Equip.UD.CstUnloader_B.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderBUpDown    = CTRL.GG.Equip.UD.CstUnloader_B.CstUpDownAxis.XF_CurrMotorPosition;
            #endregion

            #region 실린더

            ucrlEquipView.CstLoaderBL_Grip_Cylinder             = CTRL.GG.Equip.LD.CstLoader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderBR_Grip_Cylinder             = CTRL.GG.Equip.LD.CstLoader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderAL_Grip_Cylinder             = CTRL.GG.Equip.LD.CstLoader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderAR_Grip_Cylinder             = CTRL.GG.Equip.LD.CstLoader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRB1_UpDown_Cylinder            = CTRL.GG.Equip.LD.LoaderTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRB2_UpDown_Cylinder            = CTRL.GG.Equip.LD.LoaderTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRA1_UpDown_Cylinder            = CTRL.GG.Equip.LD.LoaderTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRA2_UpDown_Cylinder            = CTRL.GG.Equip.LD.LoaderTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderA_Tilt_Cylinder              = CTRL.GG.Equip.LD.CstLoader_A.TiltCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderB_Tilt_Cylinder              = CTRL.GG.Equip.LD.CstLoader_B.TiltCylinder.IsForward ? 100 : 0;

            ucrlEquipView.LaserHead1_UpDown_Cylinder            = CTRL.GG.Equip.PROC.LaserHead.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LaserHead2_UpDown_Cylinder            = CTRL.GG.Equip.PROC.LaserHead.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LaserShutter_Cylinder                 = CTRL.GG.Equip.PROC.LaserHead.LaserShutterCylinder.IsForward ? 100 : 0;
            ucrlEquipView.IRCutStageA_UpDown_Cylinder           = CTRL.GG.Equip.PROC.IRCutStage_A.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.IRCutStageB_UpDown_Cylinder           = CTRL.GG.Equip.PROC.IRCutStage_B.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTableA_UpDown_Cylinder           = CTRL.GG.Equip.PROC.BreakStage_A.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTalbeB_UpDown_Cylinder           = CTRL.GG.Equip.PROC.BreakStage_B.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRB1_UpDown_Cylinder             = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRB2_UpDown_Cylinder             = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRA1_UpDown_Cylinder             = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRA2_UpDown_Cylinder             = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LaserDummyShutter1_Cylinder           = CTRL.GG.Equip.PROC.LaserHead.DummyShutterCylinder1.IsForward ? 100 : 0;
            ucrlEquipView.LaserDummyShutter2_Cylinder           = CTRL.GG.Equip.PROC.LaserHead.DummyShutterCylinder2.IsForward ? 100 : 0;
            ucrlEquipView.DummyTank_Cylinder                    = CTRL.GG.Equip.PROC.LaserHead.DummyTankCylinder.IsForward ? 100 : 0;

            ucrlEquipView.Buffer_UpDown_Cylinder                = CTRL.GG.Equip.UD.CellBuffer.BufferCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderBL_Grip_Cylinder           = CTRL.GG.Equip.UD.CstUnloader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderBR_Grip_Cylinder           = CTRL.GG.Equip.UD.CstUnloader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderAL_Grip_Cylinder           = CTRL.GG.Equip.UD.CstUnloader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderAR_Grip_Cylinder           = CTRL.GG.Equip.UD.CstUnloader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRB1_UpDown_Cylinder        = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRB2_UpDown_Cylinder        = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRA1_UpDown_Cylinder        = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRA2_UpDown_Cylinder        = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRB1_UpDown_Cylinder         = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRB2_UpDown_Cylinder         = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRA1_UpDown_Cylinder         = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRA2_UpDown_Cylinder         = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;

            ucrlEquipView.CstUnloaderA_Tilt_Cylinder            = CTRL.GG.Equip.UD.CstUnloader_A.TiltCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderB_Tilt_Cylinder            = CTRL.GG.Equip.UD.CstUnloader_B.TiltCylinder.IsForward ? 100 : 0;

            #endregion

            #region 버큠
            ucrlEquipView.LoaderA           = CTRL.GG.Equip.LD.Loader.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderB           = CTRL.GG.Equip.LD.Loader.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderTRA1        = CTRL.GG.Equip.LD.LoaderTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRA2        = CTRL.GG.Equip.LD.LoaderTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB1        = CTRL.GG.Equip.LD.LoaderTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB2        = CTRL.GG.Equip.LD.LoaderTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;

            ucrlEquipView.ProcessTableA1_1  = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum1Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_1  = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum2Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_1  = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum1Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_1  = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum2Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA1_2  = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum1Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_2  = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum2Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_2  = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum1Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_2  = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum2Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
                                                        
            ucrlEquipView.BreakTRA          = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRA_         = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB          = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB_         = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
                                                            
            ucrlEquipView.BreakTableA1      = CTRL.GG.Equip.PROC.BreakStage_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableA2      = CTRL.GG.Equip.PROC.BreakStage_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB1      = CTRL.GG.Equip.PROC.BreakStage_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB2      = CTRL.GG.Equip.PROC.BreakStage_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
                                             
            ucrlEquipView.BeforeInspTRA1    = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRA2    = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB1    = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB2    = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.InspStageA1       = CTRL.GG.Equip.UD.InspectionStage_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageA2       = CTRL.GG.Equip.UD.InspectionStage_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB1       = CTRL.GG.Equip.UD.InspectionStage_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB2       = CTRL.GG.Equip.UD.InspectionStage_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.AfterInspTRA1     = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRA2     = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB1     = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB2     = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.UnloaderA         = CTRL.GG.Equip.UD.Unloader.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.UnloaderB         = CTRL.GG.Equip.UD.Unloader.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            #endregion

            #region 글래스
            ucrlEquipView.LoaderA_glass = CTRL.GG.Equip.LD.Loader.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderB_glass = CTRL.GG.Equip.LD.Loader.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderTRA1_glass = CTRL.GG.Equip.LD.LoaderTransfer_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRA2_glass = CTRL.GG.Equip.LD.LoaderTransfer_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB1_glass = CTRL.GG.Equip.LD.LoaderTransfer_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB2_glass = CTRL.GG.Equip.LD.LoaderTransfer_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;

            ucrlEquipView.ProcessTableA1_1_glass = CTRL.GG.Equip.PROC.IRCutStage_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_1_glass = CTRL.GG.Equip.PROC.IRCutStage_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_1_glass = CTRL.GG.Equip.PROC.IRCutStage_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_1_glass = CTRL.GG.Equip.PROC.IRCutStage_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA1_2_glass = CTRL.GG.Equip.PROC.IRCutStage_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_2_glass = CTRL.GG.Equip.PROC.IRCutStage_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_2_glass = CTRL.GG.Equip.PROC.IRCutStage_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_2_glass = CTRL.GG.Equip.PROC.IRCutStage_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;

            ucrlEquipView.BreakTRA_glass = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRA_glass_ = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB_glass = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB_glass_ = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;

            ucrlEquipView.BreakTableA1_glass = CTRL.GG.Equip.PROC.BreakStage_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableA2_glass = CTRL.GG.Equip.PROC.BreakStage_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB1_glass = CTRL.GG.Equip.PROC.BreakStage_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB2_glass = CTRL.GG.Equip.PROC.BreakStage_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;

            ucrlEquipView.BeforeInspTRA1_glass = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRA2_glass = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB1_glass = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB2_glass = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.InspStageA1_glass = CTRL.GG.Equip.UD.InspectionStage_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageA2_glass = CTRL.GG.Equip.UD.InspectionStage_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB1_glass = CTRL.GG.Equip.UD.InspectionStage_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB2_glass = CTRL.GG.Equip.UD.InspectionStage_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.AfterInspTRA1_glass = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRA2_glass = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB1_glass = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB2_glass = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.UnloaderA_glass = CTRL.GG.Equip.UD.Unloader.IsExitsPanel1 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            ucrlEquipView.UnloaderB_glass = CTRL.GG.Equip.UD.Unloader.IsExitsPanel2 ? System.Windows.Media.Brushes.SkyBlue : System.Windows.Media.Brushes.Black;
            #endregion
        }

    }
}

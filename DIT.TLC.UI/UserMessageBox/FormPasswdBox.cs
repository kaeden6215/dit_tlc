﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

public partial class FormPasswdBox : Form
{
    // 멤버 변수 
    private string _passwd;

    public string Passwd
    {
        get { return _passwd; }
        set { _passwd = value; }
    }
    public string UserID { get; set; }

    ////// 생성자
    ////public FormPasswdBox(EM_LV_LST lv = EM_LV_LST.USER, bool isLevelChange = true)
    ////{
    ////    EquipMainUi.Language.Localize.SetUICulture(new CultureInfo(EquipMainUi.Properties.Settings.Default.CultureName));
    ////    InitializeComponent();
    ////    ExtensionUI.AddClickEventLog(this);
    ////    EquipMainUi.Language.Localize.Instance.SetCulture(this, EquipMainUi.Properties.Settings.Default.CultureName);

    ////    LevelDisable(lv);
    ////    InitializeLevel(lv);

    ////    if (isLevelChange == false)
    ////        lblTitle.Text = "Please enter the password";
    ////}
    ////private void InitializeLevel(EM_LV_LST lv)
    ////{
    ////    if(lv == EM_LV_LST.USER)
    ////        radioUser.Checked = true;
    ////    else if (lv == EM_LV_LST.MASTER)
    ////        radioMaster.Checked = true;
    ////    else if (lv == EM_LV_LST.DEVELOPER)
    ////        radioDeveloper.Checked = true;
    ////    else if (lv == EM_LV_LST.SUPERVISOR)
    ////        radioSupervisor.Checked = true;
    ////}
    ////// 이벤트 오버라이드
    ////private void LevelDisable(EM_LV_LST lv)
    ////{
    ////    radioUser.Enabled = true;
    ////    radioMaster.Enabled = true;
    ////    radioDeveloper.Enabled = true;
    ////    radioSupervisor.Enabled = true;

    ////    if (lv == EM_LV_LST.MASTER)
    ////        radioUser.Enabled = false;

    ////    if (lv == EM_LV_LST.DEVELOPER)
    ////    {
    ////        radioMaster.Enabled = false;
    ////        radioUser.Enabled = false;
    ////    }

    ////    if (lv == EM_LV_LST.SUPERVISOR)
    ////    {
    ////        radioDeveloper.Enabled = false;
    ////        radioMaster.Enabled = false;
    ////        radioUser.Enabled = false;
    ////    }
    ////}
    //protected override void OnLoad(EventArgs e)
    //{
    //    base.OnLoad(e);
    //}
    //protected override void OnShown(EventArgs e)
    //{
    //    //txtPasswd.Text = _passwd;
    //    base.OnShown(e);
    //}
    private void btnOK_Click(object sender, EventArgs e)
    {
        //ID = (EM_LV_LST)combLevel.SelectedIndex;
        _passwd = txtPasswd.Text;
        this.DialogResult = DialogResult.OK;
        //this.Close();
    }
    private void btnCancel_Click(object sender, EventArgs e)
    {
        this.DialogResult = DialogResult.Cancel;
        this.Close();
    }
    private void btnlPasswd_Click(object sender, EventArgs e)
    {
        Button btn = sender as Button;

        if (btn == btnlNum1)
            _passwd += "1";
        else if (btn == btnNum2)
            _passwd += "2";
        else if (btn == btnNum3)
            _passwd += "3";
        else if (btn == btnNum4)
            _passwd += "4";
        else if (btn == btnNum5)
            _passwd += "5";
        else if (btn == btnNum6)
            _passwd += "6";
        else if (btn == btnNum7)
            _passwd += "7";
        else if (btn == btnNum8)
            _passwd += "8";
        else if (btn == btnNum9)
            _passwd += "9";
        else if (btn == btnNum0)
            _passwd += "0";
        else if (btn == btnClear)
            _passwd = "";
        else if (btn == btnBackspace)
        {
            if (_passwd == "" || _passwd == null) return;

            _passwd = _passwd.Substring(0, _passwd.Length - 1);
        }

        txtPasswd.Text = _passwd;
    }

    ////private void radioBtnLevel_CheckedChanged(object sender, EventArgs e)
    ////{
    ////    RadioButton CheckedRadioBtn = sender as RadioButton;

    ////    if (CheckedRadioBtn == radioUser)
    ////        ID = EM_LV_LST.USER;
    ////    else if (CheckedRadioBtn == radioMaster)
    ////        ID = EM_LV_LST.MASTER;
    ////    else if (CheckedRadioBtn == radioDeveloper)
    ////        ID = EM_LV_LST.DEVELOPER;
    ////    else if (CheckedRadioBtn == radioSupervisor)
    ////        ID = EM_LV_LST.SUPERVISOR;
    ////    else
    ////        throw new Exception("라디오 버튼 알수 없는 에러!!");
    ////}
    private void txtBox_KeyPress(object sender, KeyPressEventArgs e)
    {
        //숫자,백스페이스,마이너스,소숫점 만 입력받는다.
        if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8) //8:백스페이스,45:마이너스,46:소수점
        {
            e.Handled = true;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

public partial class FormAutoClose : Form
{
    // 멤버 변수
    private int _seconds = 10;
    private string _message;

    // 프로퍼티
    public int Seconds
    {
        get { return _seconds; }
        set { _seconds = value; }
    }
    public string Message
    {
        get { return _message; }
        set { _message = value; }
    }

    // 생성자
    public FormAutoClose()
    {
        InitializeComponent();        
    }

    // 이벤트 오버라이드
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
    protected override void OnShown(EventArgs e)
    {   
        SetMessage();
        if (Seconds != 0)
            timer1.Start();

        lblExit.Text = _message;
        base.OnShown(e);
    }

    // 이벤트 처리기
    private void timer1_Tick(object sender, EventArgs e)
    {
        _seconds--;
        SetMessage();

        if (_seconds <= 0)
            this.Close();
    }

    // 메서드
    private void SetMessage()
    {
        btnOK.Text = string.Format("확인 = {0}", _seconds);
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
        this.DialogResult = DialogResult.OK;
        this.Close();
    }
}

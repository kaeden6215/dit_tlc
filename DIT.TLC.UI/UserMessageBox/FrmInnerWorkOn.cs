﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EquipMainUi.Struct;
using System.Globalization;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class FrmInnerWorkOn : Form
    {
        private bool _isCloseEnable { get; set; }

        public FrmInnerWorkOn()
        {
            InitializeComponent();
            this.TopMost = true;            
        }
        private void CloseCheck_Event(object sender, FormClosingEventArgs e)
        {
            if (_isCloseEnable)
                e.Cancel = false;
            else
                e.Cancel = true;
        }

        private void btndInputPW_DelayClick(object sender, EventArgs e)
        {
            FrmUserPasswdBox ff = new FrmUserPasswdBox();
            ff.StartPosition = FormStartPosition.CenterParent;
            ff.TopMost = true;
            ff.ShowDialog();

            if (ff.DialogResult == DialogResult.OK)
            {
                UserInfo userinfo = GG.Equip.UserInfoMgr.Login(ff.UserID, ff.Passwd);

                //if (userinfo != null && userinfo.Grade == EmUserGrade.Administrator)
                {
                    //MessageBox.Show("Success Login.!!!!");
                    //passBox.Close();
                    _isCloseEnable = true;
                    GG.Equip.IsInnerWorkOnClose = true;
                    this.Close();
                    return;
                }
                MessageBox.Show("Login Fail.!!!!");
            }
            else if (ff.DialogResult == DialogResult.Cancel)
                return;
        }
    }
}

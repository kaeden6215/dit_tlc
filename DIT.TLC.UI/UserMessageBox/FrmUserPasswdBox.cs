﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

public partial class FrmUserPasswdBox : Form
{
    // 멤버 변수 
    private string _passwd;

    public string Passwd
    {
        get { return txtPasswd.Text; }
        set { txtPasswd.Text = value; }
    }
    public string UserID
    {
        get { return txtUserID.Text; }
        set { txtUserID.Text = value; }
    }
    public FrmUserPasswdBox()
    {
        InitializeComponent();
    }
    private void btnOK_Click(object sender, EventArgs e)
    {
        this.DialogResult = DialogResult.OK;
        //this.Close();
    }
    private void btnCancel_Click(object sender, EventArgs e)
    {
        this.DialogResult = DialogResult.Cancel;
        this.Close();
    }
}


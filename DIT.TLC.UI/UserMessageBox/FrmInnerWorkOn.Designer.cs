﻿namespace DIT.TLC.UI
{
    partial class FrmInnerWorkOn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btndInputPW = new Dit.Framework.UI.ButtonDelay();
            this.SuspendLayout();
            // 
            // btndInputPW
            // 
            this.btndInputPW.BackColor = System.Drawing.Color.Transparent;
            this.btndInputPW.BackgroundImage = global::DIT.TLC.UI.Properties.Resources.InnerWork;
            this.btndInputPW.Delay = 2;
            this.btndInputPW.Flicker = false;
            this.btndInputPW.IsLeftLampOn = false;
            this.btndInputPW.IsRightLampOn = false;
            this.btndInputPW.LampOnWiatTime = 0;
            this.btndInputPW.LampSize = 1;
            this.btndInputPW.LeftDelayOff = false;
            this.btndInputPW.LeftLampColor = System.Drawing.Color.Red;
            this.btndInputPW.Location = new System.Drawing.Point(0, 0);
            this.btndInputPW.Name = "btndInputPW";
            this.btndInputPW.OffColor = System.Drawing.Color.Transparent;
            this.btndInputPW.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btndInputPW.OnOff = false;
            this.btndInputPW.RightDelayOff = false;
            this.btndInputPW.RightLampColor = System.Drawing.Color.Green;
            this.btndInputPW.Size = new System.Drawing.Size(1920, 1080);
            this.btndInputPW.TabIndex = 0;
            this.btndInputPW.Text = "buttonDelay1";
            this.btndInputPW.Text2 = "";
            this.btndInputPW.UseVisualStyleBackColor = false;
            this.btndInputPW.VisibleLeftLamp = false;
            this.btndInputPW.VisibleRightLamp = false;
            this.btndInputPW.DelayClick += new System.EventHandler(this.btndInputPW_DelayClick);
            // 
            // FormPWNeedClose
            // 
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.btndInputPW);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPWNeedClose";
            this.Text = "InnerWorkMessageBox";
            this.ResumeLayout(false);

        }

        #endregion        
        private Dit.Framework.UI.ButtonDelay btndInputPW ;
    }
}
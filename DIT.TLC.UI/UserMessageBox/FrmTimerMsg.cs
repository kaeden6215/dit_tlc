﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EquipMainUi
{
    public partial class FrmTimerMsg : Form
    {
        private int _showTime;
        private DateTime _endTime;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="showTime">Second</param>
        /// <param name="msg"></param>
        public FrmTimerMsg(int showTime, string msg)
        {
            InitializeComponent();

            _showTime = showTime;
            pBarRemained.Maximum = showTime * 10;
            lblMsg.Text = msg;
            _endTime = DateTime.Now.AddSeconds(showTime);
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        public new void Show()
        {
            timer1.Start();
            base.Show();
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            double remained = (_endTime - DateTime.Now).TotalSeconds;
            lblRemainedTime.Text = remained.ToString("0.0");
            pBarRemained.Value = (_showTime - (int)remained) * 10;

            if (remained < 0)
            {
                timer1.Stop();
                this.Close();
            }
        }

        private void FrmTimerMsg_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
        }
    }
}

﻿namespace DIT.TLC.UI
{
    partial class FrmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnStepMgr = new System.Windows.Forms.Button();
            this.btnSim = new System.Windows.Forms.Button();
            this.digitalTimer1 = new DIT.TLC.UI.DigitalTimer();
            this.lblMaxScanTimeInfo = new System.Windows.Forms.Label();
            this.lblScanTickInfo = new System.Windows.Forms.Label();
            this.lblScanTimeInfo = new System.Windows.Forms.Label();
            this.lblMaxScanTime = new System.Windows.Forms.Label();
            this.lblScanTick = new System.Windows.Forms.Label();
            this.lblScanTime = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panelSafety = new System.Windows.Forms.Panel();
            this.panelWarning = new System.Windows.Forms.Panel();
            this.panelDanger = new System.Windows.Forms.Panel();
            this.btnAlarmLog = new System.Windows.Forms.Button();
            this.btnHostMessage = new System.Windows.Forms.Button();
            this.btnSem = new System.Windows.Forms.Button();
            this.btnOnoffLine = new System.Windows.Forms.Button();
            this.panel_logo = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.btnGraph = new System.Windows.Forms.Button();
            this.btnMeasure = new System.Windows.Forms.Button();
            this.btnLog = new System.Windows.Forms.Button();
            this.btnParam = new System.Windows.Forms.Button();
            this.btnManager = new System.Windows.Forms.Button();
            this.btnRecipe = new System.Windows.Forms.Button();
            this.btnMain = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.tmrUiWorker = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.btnStepMgr);
            this.splitContainer1.Panel1.Controls.Add(this.btnSim);
            this.splitContainer1.Panel1.Controls.Add(this.digitalTimer1);
            this.splitContainer1.Panel1.Controls.Add(this.lblMaxScanTimeInfo);
            this.splitContainer1.Panel1.Controls.Add(this.lblScanTickInfo);
            this.splitContainer1.Panel1.Controls.Add(this.lblScanTimeInfo);
            this.splitContainer1.Panel1.Controls.Add(this.lblMaxScanTime);
            this.splitContainer1.Panel1.Controls.Add(this.lblScanTick);
            this.splitContainer1.Panel1.Controls.Add(this.lblScanTime);
            this.splitContainer1.Panel1.Controls.Add(this.lblTitle);
            this.splitContainer1.Panel1.Controls.Add(this.panelSafety);
            this.splitContainer1.Panel1.Controls.Add(this.panelWarning);
            this.splitContainer1.Panel1.Controls.Add(this.panelDanger);
            this.splitContainer1.Panel1.Controls.Add(this.btnAlarmLog);
            this.splitContainer1.Panel1.Controls.Add(this.btnHostMessage);
            this.splitContainer1.Panel1.Controls.Add(this.btnSem);
            this.splitContainer1.Panel1.Controls.Add(this.btnOnoffLine);
            this.splitContainer1.Panel1.Controls.Add(this.panel_logo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 1026);
            this.splitContainer1.SplitterDistance = 70;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnStepMgr
            // 
            this.btnStepMgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnStepMgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepMgr.Location = new System.Drawing.Point(778, 4);
            this.btnStepMgr.Name = "btnStepMgr";
            this.btnStepMgr.Size = new System.Drawing.Size(90, 64);
            this.btnStepMgr.TabIndex = 16;
            this.btnStepMgr.Text = "스텝 관리";
            this.btnStepMgr.UseVisualStyleBackColor = false;
            this.btnStepMgr.Click += new System.EventHandler(this.btnStepMgr_Click);
            // 
            // btnSim
            // 
            this.btnSim.BackColor = System.Drawing.SystemColors.Control;
            this.btnSim.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSim.Location = new System.Drawing.Point(1254, 3);
            this.btnSim.Name = "btnSim";
            this.btnSim.Size = new System.Drawing.Size(90, 64);
            this.btnSim.TabIndex = 16;
            this.btnSim.Text = "설비 뷰어";
            this.btnSim.UseVisualStyleBackColor = false;
            this.btnSim.Click += new System.EventHandler(this.btnSim_Click);
            // 
            // digitalTimer1
            // 
            this.digitalTimer1.BackColor = System.Drawing.SystemColors.Control;
            this.digitalTimer1.DigitColor = System.Drawing.Color.Black;
            this.digitalTimer1.Location = new System.Drawing.Point(1602, 5);
            this.digitalTimer1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.digitalTimer1.Name = "digitalTimer1";
            this.digitalTimer1.Size = new System.Drawing.Size(266, 62);
            this.digitalTimer1.TabIndex = 12;
            // 
            // lblMaxScanTimeInfo
            // 
            this.lblMaxScanTimeInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblMaxScanTimeInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMaxScanTimeInfo.ForeColor = System.Drawing.Color.Black;
            this.lblMaxScanTimeInfo.Location = new System.Drawing.Point(430, 8);
            this.lblMaxScanTimeInfo.Name = "lblMaxScanTimeInfo";
            this.lblMaxScanTimeInfo.Size = new System.Drawing.Size(80, 25);
            this.lblMaxScanTimeInfo.TabIndex = 13;
            this.lblMaxScanTimeInfo.Text = "MaxScanTime";
            this.lblMaxScanTimeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblScanTickInfo
            // 
            this.lblScanTickInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblScanTickInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblScanTickInfo.ForeColor = System.Drawing.Color.Black;
            this.lblScanTickInfo.Location = new System.Drawing.Point(605, 8);
            this.lblScanTickInfo.Name = "lblScanTickInfo";
            this.lblScanTickInfo.Size = new System.Drawing.Size(80, 25);
            this.lblScanTickInfo.TabIndex = 14;
            this.lblScanTickInfo.Text = "ScanTick";
            this.lblScanTickInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblScanTimeInfo
            // 
            this.lblScanTimeInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblScanTimeInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblScanTimeInfo.ForeColor = System.Drawing.Color.Black;
            this.lblScanTimeInfo.Location = new System.Drawing.Point(519, 8);
            this.lblScanTimeInfo.Name = "lblScanTimeInfo";
            this.lblScanTimeInfo.Size = new System.Drawing.Size(80, 25);
            this.lblScanTimeInfo.TabIndex = 15;
            this.lblScanTimeInfo.Text = "ScanTime";
            this.lblScanTimeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMaxScanTime
            // 
            this.lblMaxScanTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblMaxScanTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMaxScanTime.ForeColor = System.Drawing.Color.Black;
            this.lblMaxScanTime.Location = new System.Drawing.Point(430, 35);
            this.lblMaxScanTime.Name = "lblMaxScanTime";
            this.lblMaxScanTime.Size = new System.Drawing.Size(80, 25);
            this.lblMaxScanTime.TabIndex = 10;
            this.lblMaxScanTime.Text = "-";
            this.lblMaxScanTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMaxScanTime.Click += new System.EventHandler(this.lblMaxScanTime_Click);
            // 
            // lblScanTick
            // 
            this.lblScanTick.BackColor = System.Drawing.SystemColors.Control;
            this.lblScanTick.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblScanTick.ForeColor = System.Drawing.Color.Black;
            this.lblScanTick.Location = new System.Drawing.Point(605, 35);
            this.lblScanTick.Name = "lblScanTick";
            this.lblScanTick.Size = new System.Drawing.Size(80, 25);
            this.lblScanTick.TabIndex = 10;
            this.lblScanTick.Text = "-";
            this.lblScanTick.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblScanTime
            // 
            this.lblScanTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblScanTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblScanTime.ForeColor = System.Drawing.Color.Black;
            this.lblScanTime.Location = new System.Drawing.Point(519, 35);
            this.lblScanTime.Name = "lblScanTime";
            this.lblScanTime.Size = new System.Drawing.Size(80, 25);
            this.lblScanTime.TabIndex = 10;
            this.lblScanTime.Text = "-";
            this.lblScanTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Location = new System.Drawing.Point(130, 17);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(252, 32);
            this.lblTitle.TabIndex = 9;
            this.lblTitle.Text = "Trench Laser Cutting";
            // 
            // panelSafety
            // 
            this.panelSafety.Location = new System.Drawing.Point(1528, 3);
            this.panelSafety.Name = "panelSafety";
            this.panelSafety.Size = new System.Drawing.Size(64, 64);
            this.panelSafety.TabIndex = 2;
            // 
            // panelWarning
            // 
            this.panelWarning.Location = new System.Drawing.Point(1458, 3);
            this.panelWarning.Name = "panelWarning";
            this.panelWarning.Size = new System.Drawing.Size(64, 64);
            this.panelWarning.TabIndex = 1;
            // 
            // panelDanger
            // 
            this.panelDanger.Location = new System.Drawing.Point(1388, 3);
            this.panelDanger.Name = "panelDanger";
            this.panelDanger.Size = new System.Drawing.Size(64, 64);
            this.panelDanger.TabIndex = 0;
            // 
            // btnAlarmLog
            // 
            this.btnAlarmLog.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlarmLog.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlarmLog.Location = new System.Drawing.Point(1159, 3);
            this.btnAlarmLog.Name = "btnAlarmLog";
            this.btnAlarmLog.Size = new System.Drawing.Size(90, 64);
            this.btnAlarmLog.TabIndex = 7;
            this.btnAlarmLog.Text = "알람 이력";
            this.btnAlarmLog.UseVisualStyleBackColor = false;
            this.btnAlarmLog.Click += new System.EventHandler(this.btnAlarmLog_Click);
            // 
            // btnHostMessage
            // 
            this.btnHostMessage.BackColor = System.Drawing.SystemColors.Control;
            this.btnHostMessage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHostMessage.Location = new System.Drawing.Point(1064, 3);
            this.btnHostMessage.Name = "btnHostMessage";
            this.btnHostMessage.Size = new System.Drawing.Size(90, 64);
            this.btnHostMessage.TabIndex = 6;
            this.btnHostMessage.Text = "호스트\r\n메세지";
            this.btnHostMessage.UseVisualStyleBackColor = false;
            this.btnHostMessage.Click += new System.EventHandler(this.btnHostMessage_Click);
            // 
            // btnSem
            // 
            this.btnSem.BackColor = System.Drawing.SystemColors.Control;
            this.btnSem.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSem.Location = new System.Drawing.Point(969, 3);
            this.btnSem.Name = "btnSem";
            this.btnSem.Size = new System.Drawing.Size(90, 64);
            this.btnSem.TabIndex = 5;
            this.btnSem.Text = "SEM";
            this.btnSem.UseVisualStyleBackColor = false;
            // 
            // btnOnoffLine
            // 
            this.btnOnoffLine.BackColor = System.Drawing.SystemColors.Control;
            this.btnOnoffLine.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOnoffLine.Location = new System.Drawing.Point(874, 3);
            this.btnOnoffLine.Name = "btnOnoffLine";
            this.btnOnoffLine.Size = new System.Drawing.Size(90, 64);
            this.btnOnoffLine.TabIndex = 4;
            this.btnOnoffLine.Text = "Off-Line";
            this.btnOnoffLine.UseVisualStyleBackColor = false;
            this.btnOnoffLine.Click += new System.EventHandler(this.btnOnoffLine_Click);
            // 
            // panel_logo
            // 
            this.panel_logo.BackColor = System.Drawing.Color.White;
            this.panel_logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel_logo.BackgroundImage")));
            this.panel_logo.Location = new System.Drawing.Point(4, 3);
            this.panel_logo.Name = "panel_logo";
            this.panel_logo.Size = new System.Drawing.Size(120, 65);
            this.panel_logo.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.Color.Black;
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel2.Controls.Add(this.btnGraph);
            this.splitContainer2.Panel2.Controls.Add(this.btnMeasure);
            this.splitContainer2.Panel2.Controls.Add(this.btnLog);
            this.splitContainer2.Panel2.Controls.Add(this.btnParam);
            this.splitContainer2.Panel2.Controls.Add(this.btnManager);
            this.splitContainer2.Panel2.Controls.Add(this.btnRecipe);
            this.splitContainer2.Panel2.Controls.Add(this.btnMain);
            this.splitContainer2.Panel2.Controls.Add(this.btnLogin);
            this.splitContainer2.Panel2.Controls.Add(this.btnExit);
            this.splitContainer2.Size = new System.Drawing.Size(1880, 952);
            this.splitContainer2.SplitterDistance = 868;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BackColor = System.Drawing.Color.Gray;
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer3.Panel1.Margin = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer3.Panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer3.Panel2Collapsed = true;
            this.splitContainer3.Size = new System.Drawing.Size(1880, 868);
            this.splitContainer3.SplitterDistance = 1740;
            this.splitContainer3.TabIndex = 0;
            // 
            // btnGraph
            // 
            this.btnGraph.BackColor = System.Drawing.SystemColors.Control;
            this.btnGraph.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGraph.ForeColor = System.Drawing.Color.Black;
            this.btnGraph.Location = new System.Drawing.Point(1360, 3);
            this.btnGraph.Name = "btnGraph";
            this.btnGraph.Size = new System.Drawing.Size(150, 73);
            this.btnGraph.TabIndex = 8;
            this.btnGraph.Text = "그래프";
            this.btnGraph.UseVisualStyleBackColor = false;
            this.btnGraph.Click += new System.EventHandler(this.btnGraph_Click);
            // 
            // btnMeasure
            // 
            this.btnMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnMeasure.Location = new System.Drawing.Point(1204, 3);
            this.btnMeasure.Name = "btnMeasure";
            this.btnMeasure.Size = new System.Drawing.Size(150, 73);
            this.btnMeasure.TabIndex = 7;
            this.btnMeasure.Text = "측정";
            this.btnMeasure.UseVisualStyleBackColor = false;
            this.btnMeasure.Click += new System.EventHandler(this.btnMeasure_Click);
            // 
            // btnLog
            // 
            this.btnLog.BackColor = System.Drawing.SystemColors.Control;
            this.btnLog.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLog.ForeColor = System.Drawing.Color.Black;
            this.btnLog.Location = new System.Drawing.Point(1048, 3);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(150, 73);
            this.btnLog.TabIndex = 6;
            this.btnLog.Text = "로그";
            this.btnLog.UseVisualStyleBackColor = false;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // btnParam
            // 
            this.btnParam.BackColor = System.Drawing.SystemColors.Control;
            this.btnParam.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParam.ForeColor = System.Drawing.Color.Black;
            this.btnParam.Location = new System.Drawing.Point(892, 3);
            this.btnParam.Name = "btnParam";
            this.btnParam.Size = new System.Drawing.Size(150, 73);
            this.btnParam.TabIndex = 5;
            this.btnParam.Text = "파라미터";
            this.btnParam.UseVisualStyleBackColor = false;
            this.btnParam.Click += new System.EventHandler(this.btnParam_Click);
            // 
            // btnManager
            // 
            this.btnManager.BackColor = System.Drawing.SystemColors.Control;
            this.btnManager.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManager.ForeColor = System.Drawing.Color.Black;
            this.btnManager.Location = new System.Drawing.Point(736, 3);
            this.btnManager.Name = "btnManager";
            this.btnManager.Size = new System.Drawing.Size(150, 73);
            this.btnManager.TabIndex = 4;
            this.btnManager.Text = "유지보수";
            this.btnManager.UseVisualStyleBackColor = false;
            this.btnManager.Click += new System.EventHandler(this.btnManager_Click);
            // 
            // btnRecipe
            // 
            this.btnRecipe.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipe.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecipe.ForeColor = System.Drawing.Color.Black;
            this.btnRecipe.Location = new System.Drawing.Point(580, 3);
            this.btnRecipe.Name = "btnRecipe";
            this.btnRecipe.Size = new System.Drawing.Size(150, 73);
            this.btnRecipe.TabIndex = 3;
            this.btnRecipe.Text = "레시피";
            this.btnRecipe.UseVisualStyleBackColor = false;
            this.btnRecipe.Click += new System.EventHandler(this.btnRecipe_Click);
            // 
            // btnMain
            // 
            this.btnMain.BackColor = System.Drawing.SystemColors.Control;
            this.btnMain.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMain.ForeColor = System.Drawing.Color.Black;
            this.btnMain.Location = new System.Drawing.Point(424, 3);
            this.btnMain.Name = "btnMain";
            this.btnMain.Size = new System.Drawing.Size(150, 73);
            this.btnMain.TabIndex = 2;
            this.btnMain.Text = "메인화면";
            this.btnMain.UseVisualStyleBackColor = false;
            this.btnMain.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.SystemColors.Control;
            this.btnLogin.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.Color.Black;
            this.btnLogin.Location = new System.Drawing.Point(268, 3);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(150, 73);
            this.btnLogin.TabIndex = 1;
            this.btnLogin.Text = "로그인";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.Control;
            this.btnExit.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(3, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(150, 73);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "종    료";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tmrUiWorker
            // 
            this.tmrUiWorker.Enabled = true;
            this.tmrUiWorker.Tick += new System.EventHandler(this.tmrUiWorker_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "Main";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel panel_logo;
        private System.Windows.Forms.Button btnOnoffLine;
        private System.Windows.Forms.Button btnSem;
        private System.Windows.Forms.Button btnAlarmLog;
        private System.Windows.Forms.Button btnHostMessage;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnMain;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnGraph;
        private System.Windows.Forms.Button btnMeasure;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Button btnParam;
        private System.Windows.Forms.Button btnManager;
        private System.Windows.Forms.Button btnRecipe;
        private System.Windows.Forms.Panel panelDanger;
        private System.Windows.Forms.Panel panelSafety;
        private System.Windows.Forms.Panel panelWarning;
        private System.Windows.Forms.Timer tmrUiWorker;
        private System.Windows.Forms.Label lblTitle;
        private DigitalTimer digitalTimer1;
        private System.Windows.Forms.Label lblScanTime;
        private System.Windows.Forms.Label lblMaxScanTime;
        private System.Windows.Forms.Label lblScanTick;
        private System.Windows.Forms.Label lblMaxScanTimeInfo;
        private System.Windows.Forms.Label lblScanTickInfo;
        private System.Windows.Forms.Label lblScanTimeInfo;
        private System.Windows.Forms.Button btnSim;
        private System.Windows.Forms.Button btnStepMgr;
    }
}


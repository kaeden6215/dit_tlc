﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dit.Framework.UI;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlMainWindow : UserControl, IUIUpdate
    {
        private EquipView.UcrlEquipView ucrlEquipView = new EquipView.UcrlEquipView();
        private Equipment _equip;
        public Equipment Equip
        {
            get
            {
                return _equip;
            }
            set
            {
                _equip = value;
                InitilzieEquipment();
            }
        }

        public UcrlMainWindow()
        {
            InitializeComponent();
            elementHost1.Child = ucrlEquipView;
        }

        public void InitilzieEquipment()
        {
            //// A/B CST Skip
            //btnACSTSkip.BackColor = _equip.EqpSysMgr.Params.IsLoaderInputSkipAMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnBCSTSkip.BackColor = _equip.EqpSysMgr.Params.IsLoaderInputSkipBMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            //181030 NC
            //  // 전장박스
            //  lblLoaderBox.BackColor = _equip.LD.LD전장판넬OPEN감지.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblProcessBox.BackColor = _equip.PROC.전장판넬Open감지.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblUnloaderBox.BackColor = _equip.UD.ULD전장판넬OPEN감지.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //
            //  // 조명
            //  btnLdLight.BackColor = _equip.LD.LDLightCurtain감지A.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  btnProcessLight.BackColor = _equip.LD.LDLightCurtain감지B.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  btnUldLight.BackColor = _equip.UD.ULDLightCurtain감지.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //
            //
            //  // Door
            //lblLaserCover.BackColor = _equip.LD.Door01.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;

            //btnDelayTopDoor1.BackColor = _equip.LD.Door01.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor2.BackColor = _equip.LD.Door02.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor3.BackColor = _equip.LD.Door03.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor4.BackColor = _equip.PROC.Door04.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor5.BackColor = _equip.PROC.Door05.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor6.BackColor = _equip.PROC.Door06.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor7.BackColor = _equip.PROC.Door07.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor8.BackColor = _equip.PROC.Door08.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor9.BackColor = _equip.UD.Door09.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //btnDelayTopDoor10.BackColor = _equip.UD.Door10.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;

            //btnDoorOpen.BackColor =
            //     _equip.LD.Door01.IsSolOnOff &&
            //     _equip.LD.Door02.IsSolOnOff &&
            //     _equip.LD.Door03.IsSolOnOff &&
            //     _equip.PROC.Door04.IsSolOnOff &&
            //     _equip.PROC.Door05.IsSolOnOff &&
            //     _equip.PROC.Door06.IsSolOnOff &&
            //     _equip.PROC.Door07.IsSolOnOff &&
            //     _equip.PROC.Door08.IsSolOnOff &&
            //     _equip.UD.Door09.IsSolOnOff &&
            //     _equip.UD.Door10.IsSolOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;





            //
            //  // EMS
            //  lblEms1.BackColor = _equip.LD.LDEmergencyStop1.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblEms2.BackColor = _equip.LD.LDEmergencyStop2.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblEms3.BackColor = _equip.LD.LDEmergencyStop3.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblEms4.BackColor = _equip.UD.ULDEmergencyStop4.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblEms5.BackColor = _equip.UD.ULDEmergencyStop5.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblEms6.BackColor = _equip.UD.ULDEmergencyStop6.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //
            //  // Grab EMS
            //  lblGrabEms1.BackColor = _equip.LD.EmergencyStopEnableGripSwitch1.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblGrabEms2.BackColor = _equip.PROC.EmergencyStopEnableGripSwitch.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //  lblGrabEms3.BackColor = _equip.UD.EmergencyStopEnalbeGripSwitch3.IsOnOff ? UiGlobal.ON_C : UiGlobal.OFF_C;
            //
            //  lblLoader2In.BackColor = _equip.LD.CstLoader_B.LDCstDetectExist.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;
            //  lblLoader1In.BackColor = _equip.LD.CstLoader_A.LDCstDetectExist.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;
            //
            //  // 확인필요..
            //  lblUnloader2Out.BackColor = _equip.UD.CstUnloader_B.ULDCasstteDetect_2.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;
            //  lblUnloader1Out.BackColor = _equip.UD.CstUnloader_A.ULDCasstteDetect_1.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;



            // 작업정보
            txtRunName.Text = _equip.WorkInfos.WorkName;
            txtRecipeName.Text = _equip.WorkInfos.RecipeName;
            txtProcess.Text = _equip.WorkInfos.Process;
            txtCellSize.Text = _equip.WorkInfos.CellSize;
            txtStartTime.Text = _equip.WorkInfos.StartTime;
            txtRunTime.Text = _equip.WorkInfos.WorkTime;
            txtRunCount.Text = _equip.WorkInfos.WorkCount;
            txtTactTime.Text = _equip.WorkInfos.TackTime;

            // 레이저정보 
            lblPulseMode.Text = string.Format("{0}", _equip.LaserProxy._laserParameterValues.PM);
            lblPd7Power.Text = string.Format("{0}", _equip.LaserProxy._laserParameterValues.PD7);
            lblBurst.Text = string.Format("{0}", _equip.LaserProxy._laserParameterValues.Burst);
            lblDivider.Text = string.Format("{0}", _equip.LaserProxy._laserParameterValues.RRD);
            lblAmplifier.Text = string.Format("{0}", _equip.LaserProxy._laserParameterValues.RRAmpSet); // RRAmp??
            lblPower.Text = string.Format("{0}", _equip.LaserProxy._laserParameterValues.RL);
            lblOutAmplifier.Text = string.Format("{0}", (_equip.LaserProxy._laserParameterValues.RRAmp / _equip.LaserProxy._laserParameterValues.RRD));
            lblShutter.Text = _equip.LaserProxy._laserParameterValues.S == 0 ? "Open" : "Closed";

            // 브레이킹 Count (브레이킹한 횟수 저장)
            lblBreakingCount.Text = _equip.PROC.BreakingHead.BreakingCount.ToString();

            // MCR
            lblDayReadCount.Text = "";
            lblDayTotalCount.Text = "";
            lblCstReadCount.Text = "";
            lblCstTotalCount.Text = "";

            ucrlCstCellCountLDA.Port = _equip.LD.CstLoader_A.Port;
            ucrlCstCellCountLDB.Port = _equip.LD.CstLoader_B.Port;

            //
            ucrlCstCellCountULDA.Port = _equip.UD.CstUnloader_A.Port;
            ucrlCstCellCountULDB.Port = _equip.UD.CstUnloader_B.Port;

            UcrlDtvCstLoaderB.Unit = _equip.LD.CstLoader_B;
            UcrlDtvCstLoaderA.Unit = _equip.LD.CstLoader_A;
            UcrlDtvLoader.Unit = _equip.LD.Loader;
            UcrlDtvLoaderTransferB.Unit = _equip.LD.LoaderTransfer_B;
            UcrlDtvLoaderTransferA.Unit = _equip.LD.LoaderTransfer_A;

            UcrlDtvPreAlign.Unit = _equip.LD.PreAlign;
            UcrlDtvBreakAlign.Unit = _equip.PROC.BreakAlign;
            UcrlDtvFineAlign.Unit = _equip.PROC.FineAlign;

            UcrlDtvIRCutStageB.Unit = _equip.PROC.IRCutStage_B;
            UcrlDtvIRCutStageA.Unit = _equip.PROC.IRCutStage_A;
            UcrlDtvAfterIRCutTransferB.Unit = _equip.PROC.AfterIRCutTransfer_B;
            UcrlDtvAfterIRCutTransferA.Unit = _equip.PROC.AfterIRCutTransfer_A;
            UcrlDtvBreakStageB.Unit = _equip.PROC.BreakStage_B;
            UcrlDtvBreakStageA.Unit = _equip.PROC.BreakStage_A;

            UcrlDtvBeforeTransferB.Unit = _equip.UD.BeforeInspUnloaderTransfer_B;
            UcrlDtvBeforeTransferA.Unit = _equip.UD.BeforeInspUnloaderTransfer_A;

            UcrlDtvAfterInspTransferB.Unit = _equip.UD.AfterInspUnloaderTransfer_B;
            UcrlDtvAfterInspTransferA.Unit = _equip.UD.AfterInspUnloaderTransfer_A;
            UcrlDtvInspStageB.Unit = _equip.UD.InspectionStage_B;
            UcrlDtvInspStageA.Unit = _equip.UD.InspectionStage_A;

            UcrlDtvUnloader.Unit = _equip.UD.Unloader;

            UcrlDtvCstUnloaderB.Unit = _equip.UD.CstUnloader_B;
            UcrlDtvCstUnloaderA.Unit = _equip.UD.CstUnloader_A;
        }
        // 시작 버튼 클릭
        private void btnStart_Click(object sender, EventArgs e)
        {
            GG.Equip.ChangeMode(EmEquipRunMode.Start);
        }

        // 일시정지 버튼 클릭
        private void btnPause_Click(object sender, EventArgs e)
        {
            if (GG.Equip.PROC.IRCutStage_A.ExecuteEnd)
            {
                GG.Equip.PROC.IRCutStage_A.FirstExecute = true;
            }
            if (GG.Equip.PROC.IRCutStage_B.ExecuteEnd)
            {
                GG.Equip.PROC.IRCutStage_B.FirstExecute = true;
            }
            
            GG.Equip.ChangeMode(EmEquipRunMode.Pause);
        }
        // 작업종료 버튼 클릭
        private void btnStop_Click(object sender, EventArgs e)
        {
            GG.Equip.ChangeMode(EmEquipRunMode.Stop);
        }

        private void CmdDoorOpenSol(Switch selDoorSol, ButtonDelay selDoorBtn, bool vBit)
        {
            selDoorSol.OnOff(_equip, vBit);
            DoorBtnStateChange(selDoorBtn, selDoorSol.YB_OnOff.vBit);
        }

        private void DoorBtnStateChange(ButtonDelay btnDelay, bool isOpenCmd, bool? isOpenState = null)
        {
            if (isOpenCmd == true)
            {
                btnDelay.BackColor = Color.Red;
                btnDelay.ForeColor = Color.Black;
            }
            else
            {
                btnDelay.BackColor = Color.Transparent;
                if (isOpenState == true) btnDelay.ForeColor = Color.Red;
            }

            if (isOpenState == null) return;

            if (isOpenState == true)
            {
                btnDelay.Text = "Open";
            }
            else
            {
                btnDelay.ForeColor = Color.Black;
                btnDelay.Text = "Close";
            }
        }

        public void UIUpdate()
        {
            elementHost1.BackColor = Color.FromArgb(255, 240, 240, 240);

            ucrlCstCellCountLDA.UIUpdate();
            ucrlCstCellCountLDB.UIUpdate();

            ucrlCstCellCountULDA.UIUpdate();
            ucrlCstCellCountULDB.UIUpdate();

            btnStart.BackColor = GG.Equip.RunMode == EmEquipRunMode.Start ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnPause.BackColor = GG.Equip.RunMode == EmEquipRunMode.Pause ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnStop.BackColor = GG.Equip.RunMode == EmEquipRunMode.Stop ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;


            UcrlDtvCstLoaderB.UIUpdate();
            UcrlDtvCstLoaderA.UIUpdate();
            UcrlDtvLoader.UIUpdate();
            UcrlDtvLoaderTransferB.UIUpdate();
            UcrlDtvLoaderTransferA.UIUpdate();
            UcrlDtvIRCutStageB.UIUpdate();
            UcrlDtvIRCutStageA.UIUpdate();
            UcrlDtvPreAlign.UIUpdate();
            UcrlDtvBreakAlign.UIUpdate();
            UcrlDtvFineAlign.UIUpdate();
            UcrlDtvAfterIRCutTransferB.UIUpdate();
            UcrlDtvAfterIRCutTransferA.UIUpdate();
            UcrlDtvBreakStageB.UIUpdate();
            UcrlDtvBreakStageA.UIUpdate();

            UcrlDtvBeforeTransferB.UIUpdate();
            UcrlDtvBeforeTransferA.UIUpdate();
            UcrlDtvInspStageB.UIUpdate();
            UcrlDtvInspStageA.UIUpdate();

            UcrlDtvAfterInspTransferB.UIUpdate();
            UcrlDtvAfterInspTransferA.UIUpdate();
            UcrlDtvInspStageB.UIUpdate();

            UcrlDtvUnloader.UIUpdate();
            UcrlDtvCstUnloaderB.UIUpdate();
            UcrlDtvCstUnloaderA.UIUpdate();

            #region 축

            ucrlEquipView.CstLoaderARotation = CTRL.GG.Equip.LD.CstLoader_A.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstLoaderAUpDown = CTRL.GG.Equip.LD.CstLoader_A.CstUpDownAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstLoaderBRotation = CTRL.GG.Equip.LD.CstLoader_B.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstLoaderBUpDown = CTRL.GG.Equip.LD.CstLoader_B.CstUpDownAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderAX = CTRL.GG.Equip.LD.Loader.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderBX = CTRL.GG.Equip.LD.Loader.X2Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderAY = CTRL.GG.Equip.LD.Loader.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderBY = CTRL.GG.Equip.LD.Loader.Y2Axis.XF_CurrMotorPosition;

            ucrlEquipView.LoaderTRB1Y = CTRL.GG.Equip.LD.LoaderTransfer_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRB2Y = CTRL.GG.Equip.LD.LoaderTransfer_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRB1X = CTRL.GG.Equip.LD.LoaderTransfer_B.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRB2X = CTRL.GG.Equip.LD.LoaderTransfer_B.X2Axis.XF_CurrMotorPosition;

            //ucrlEquipView.Sub_LoaderTRB1Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRB2Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRB1T     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRB2T     = CTRL.GG.Equip.
            ucrlEquipView.LoaderTRA1Y = CTRL.GG.Equip.LD.LoaderTransfer_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRA2Y = CTRL.GG.Equip.LD.LoaderTransfer_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRA1X = CTRL.GG.Equip.LD.LoaderTransfer_A.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.LoaderTRA2X = CTRL.GG.Equip.LD.LoaderTransfer_A.X2Axis.XF_CurrMotorPosition;

            //ucrlEquipView.Sub_LoaderTRA1Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRA2Y     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRA1T     = CTRL.GG.Equip.
            //ucrlEquipView.Sub_LoaderTRA2T     = CTRL.GG.Equip.
            ucrlEquipView.PreAlignX = CTRL.GG.Equip.LD.PreAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.FineAlignX = CTRL.GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageA1Y = CTRL.GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageA2Y = CTRL.GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageB1Y = CTRL.GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.IRCutStageB2Y = CTRL.GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.LaserHeadX = CTRL.GG.Equip.PROC.LaserHead.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.LaserHeadZ = CTRL.GG.Equip.PROC.LaserHead.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.AfterIRCutTRAY = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.AfterIRCutTRBY = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakTableB1Y = CTRL.GG.Equip.PROC.BreakStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakTableB2Y = CTRL.GG.Equip.PROC.BreakStage_B.YAxis.XF_CurrMotorPosition;
            //ucrlEquipView.Sub_BreakTableB1T   = CTRL.GG.Equip. 
            //ucrlEquipView.Sub_BreakTableB2T   = CTRL.GG.Equip.
            ucrlEquipView.BreakTableA1Y = CTRL.GG.Equip.PROC.BreakStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakTableA2Y = CTRL.GG.Equip.PROC.BreakStage_A.YAxis.XF_CurrMotorPosition;
            //ucrlEquipView.Sub_BreakTableA1T   = CTRL.GG.Equip.
            //ucrlEquipView.Sub_BreakTableA2T   = CTRL.GG.Equip.
            ucrlEquipView.BreakingHead1X = CTRL.GG.Equip.PROC.BreakingHead.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakingHead2X = CTRL.GG.Equip.PROC.BreakingHead.X2Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakingHead1Z = CTRL.GG.Equip.PROC.BreakingHead.Z1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakingHead2Z = CTRL.GG.Equip.PROC.BreakingHead.Z2Axis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign1X = CTRL.GG.Equip.PROC.BreakAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign2X = CTRL.GG.Equip.PROC.BreakAlign.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign1Z = CTRL.GG.Equip.PROC.BreakAlign.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.BreakAlign2Z = CTRL.GG.Equip.PROC.BreakAlign.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRA1Y = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRA2Y = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRB1Y = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.BeforeInspTRB2Y = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA1Y = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA2Y = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA1T = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRA2T = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB1Y = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB2Y = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB1T = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis.XF_CurrMotorPosition;
            ucrlEquipView.AfterInspTRB2T = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T2Axis.XF_CurrMotorPosition;
            ucrlEquipView.InspCameraX = CTRL.GG.Equip.UD.InspCamera.XAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspCameraZ = CTRL.GG.Equip.UD.InspCamera.ZAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageA1Y = CTRL.GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageA2Y = CTRL.GG.Equip.UD.InspectionStage_A.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageB1Y = CTRL.GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.InspStageB2Y = CTRL.GG.Equip.UD.InspectionStage_B.YAxis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderAX = CTRL.GG.Equip.UD.Unloader.X1Axis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderBX = CTRL.GG.Equip.UD.Unloader.X2Axis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderAY = CTRL.GG.Equip.UD.Unloader.Y1Axis.XF_CurrMotorPosition;
            ucrlEquipView.UnloaderBY = CTRL.GG.Equip.UD.Unloader.Y2Axis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderARotation = CTRL.GG.Equip.UD.CstUnloader_A.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderAUpDown = CTRL.GG.Equip.UD.CstUnloader_A.CstUpDownAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderBRotation = CTRL.GG.Equip.UD.CstUnloader_B.CstRotationAxis.XF_CurrMotorPosition;
            ucrlEquipView.CstUnloaderBUpDown = CTRL.GG.Equip.UD.CstUnloader_B.CstUpDownAxis.XF_CurrMotorPosition;
            #endregion

            #region 실린더

            ucrlEquipView.CstLoaderBL_Grip_Cylinder = CTRL.GG.Equip.LD.CstLoader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderBR_Grip_Cylinder = CTRL.GG.Equip.LD.CstLoader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderAL_Grip_Cylinder = CTRL.GG.Equip.LD.CstLoader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderAR_Grip_Cylinder = CTRL.GG.Equip.LD.CstLoader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRB1_UpDown_Cylinder = CTRL.GG.Equip.LD.LoaderTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRB2_UpDown_Cylinder = CTRL.GG.Equip.LD.LoaderTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRA1_UpDown_Cylinder = CTRL.GG.Equip.LD.LoaderTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LoaderTRA2_UpDown_Cylinder = CTRL.GG.Equip.LD.LoaderTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderA_Tilt_Cylinder = CTRL.GG.Equip.LD.CstLoader_A.TiltCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstLoaderB_Tilt_Cylinder = CTRL.GG.Equip.LD.CstLoader_B.TiltCylinder.IsForward ? 100 : 0;

            ucrlEquipView.LaserHead1_UpDown_Cylinder = CTRL.GG.Equip.PROC.LaserHead.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LaserHead2_UpDown_Cylinder = CTRL.GG.Equip.PROC.LaserHead.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LaserShutter_Cylinder = CTRL.GG.Equip.PROC.LaserHead.LaserShutterCylinder.IsForward ? 100 : 0;
            ucrlEquipView.IRCutStageA_UpDown_Cylinder = CTRL.GG.Equip.PROC.IRCutStage_A.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.IRCutStageB_UpDown_Cylinder = CTRL.GG.Equip.PROC.IRCutStage_B.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTableA_UpDown_Cylinder = CTRL.GG.Equip.PROC.BreakStage_A.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTalbeB_UpDown_Cylinder = CTRL.GG.Equip.PROC.BreakStage_B.BrushUpDownCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRB1_UpDown_Cylinder = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRB2_UpDown_Cylinder = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRA1_UpDown_Cylinder = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BreakTRA2_UpDown_Cylinder = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.LaserDummyShutter1_Cylinder = CTRL.GG.Equip.PROC.LaserHead.DummyShutterCylinder1.IsForward ? 100 : 0;
            ucrlEquipView.LaserDummyShutter2_Cylinder = CTRL.GG.Equip.PROC.LaserHead.DummyShutterCylinder2.IsForward ? 100 : 0;
            ucrlEquipView.DummyTank_Cylinder = CTRL.GG.Equip.PROC.LaserHead.DummyTankCylinder.IsForward ? 100 : 0;

            ucrlEquipView.Buffer_UpDown_Cylinder = CTRL.GG.Equip.UD.CellBuffer.BufferCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderBL_Grip_Cylinder = CTRL.GG.Equip.UD.CstUnloader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderBR_Grip_Cylinder = CTRL.GG.Equip.UD.CstUnloader_B.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderAL_Grip_Cylinder = CTRL.GG.Equip.UD.CstUnloader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderAR_Grip_Cylinder = CTRL.GG.Equip.UD.CstUnloader_A.CstGripCylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRB1_UpDown_Cylinder = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRB2_UpDown_Cylinder = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRA1_UpDown_Cylinder = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.BeforeInspTRA2_UpDown_Cylinder = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRB1_UpDown_Cylinder = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRB2_UpDown_Cylinder = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRA1_UpDown_Cylinder = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder.IsForward ? 100 : 0;
            ucrlEquipView.AfterInspTRA2_UpDown_Cylinder = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder.IsForward ? 100 : 0;

            ucrlEquipView.CstUnloaderA_Tilt_Cylinder = CTRL.GG.Equip.UD.CstUnloader_A.TiltCylinder.IsForward ? 100 : 0;
            ucrlEquipView.CstUnloaderB_Tilt_Cylinder = CTRL.GG.Equip.UD.CstUnloader_B.TiltCylinder.IsForward ? 100 : 0;

            #endregion

            #region 버큠
            ucrlEquipView.LoaderA = CTRL.GG.Equip.LD.Loader.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderB = CTRL.GG.Equip.LD.Loader.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderTRA1 = CTRL.GG.Equip.LD.LoaderTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRA2 = CTRL.GG.Equip.LD.LoaderTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB1 = CTRL.GG.Equip.LD.LoaderTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB2 = CTRL.GG.Equip.LD.LoaderTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;

            ucrlEquipView.ProcessTableA1_1 = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum1Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_1 = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum2Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_1 = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum1Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_1 = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum2Ch1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA1_2 = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum1Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_2 = CTRL.GG.Equip.PROC.IRCutStage_A.Vacuum2Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_2 = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum1Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_2 = CTRL.GG.Equip.PROC.IRCutStage_B.Vacuum2Ch2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;

            ucrlEquipView.BreakTRA = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRA_ = CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB_ = CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;

            ucrlEquipView.BreakTableA1 = CTRL.GG.Equip.PROC.BreakStage_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableA2 = CTRL.GG.Equip.PROC.BreakStage_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB1 = CTRL.GG.Equip.PROC.BreakStage_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB2 = CTRL.GG.Equip.PROC.BreakStage_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;

            ucrlEquipView.BeforeInspTRA1 = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRA2 = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB1 = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB2 = CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.InspStageA1 = CTRL.GG.Equip.UD.InspectionStage_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageA2 = CTRL.GG.Equip.UD.InspectionStage_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB1 = CTRL.GG.Equip.UD.InspectionStage_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB2 = CTRL.GG.Equip.UD.InspectionStage_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.AfterInspTRA1 = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRA2 = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB1 = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB2 = CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.UnloaderA = CTRL.GG.Equip.UD.Unloader.Vaccum1.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.UnloaderB = CTRL.GG.Equip.UD.Unloader.Vaccum2.IsOnOff ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            #endregion




            btnDelayTopDoor1.BackColor = _equip.LD.Door01.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor2.BackColor = _equip.LD.Door02.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor3.BackColor = _equip.LD.Door03.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor4.BackColor = _equip.PROC.Door04.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor5.BackColor = _equip.PROC.Door05.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor6.BackColor = _equip.PROC.Door06.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor7.BackColor = _equip.PROC.Door07.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor8.BackColor = _equip.PROC.Door08.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor9.BackColor = _equip.UD.Door09.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor10.BackColor = _equip.UD.Door10.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnDelayTopDoor11.BackColor = _equip.UD.Door11.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            btnDoorOpen.BackColor =
                 _equip.LD.Door01.IsSolOnOff &&
                 _equip.LD.Door02.IsSolOnOff &&
                 _equip.LD.Door03.IsSolOnOff &&
                 _equip.PROC.Door04.IsSolOnOff &&
                 _equip.PROC.Door05.IsSolOnOff &&
                 _equip.PROC.Door06.IsSolOnOff &&
                 _equip.PROC.Door07.IsSolOnOff &&
                 _equip.PROC.Door08.IsSolOnOff &&
                 _equip.UD.Door09.IsSolOnOff &&
                 _equip.UD.Door10.IsSolOnOff &&
                 _equip.UD.Door10.IsSolOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;


            btnUldLight.BackColor = _equip.LD.InnerLamp.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnProcessLight.BackColor = _equip.PROC.InnerLamp.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnLdLight.BackColor = _equip.UD.InnerLamp.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;




            // EMS
            lblEms1.BackColor = _equip.LD.LDEmergencyStop1.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblEms2.BackColor = _equip.LD.LDEmergencyStop2.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblEms3.BackColor = _equip.LD.LDEmergencyStop3.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblEms4.BackColor = _equip.UD.ULDEmergencyStop5.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblEms5.BackColor = _equip.UD.ULDEmergencyStop6.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblEms6.BackColor = _equip.UD.ULDEmergencyStop7.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;


            // Grab EMS
            lblGrabEms1.BackColor = _equip.LD.EmergencyStopEnableGripSwitch1.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblGrabEms2.BackColor = _equip.PROC.EmergencyStopEnableGripSwitch.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblGrabEms3.BackColor = _equip.UD.EmergencyStopEnalbeGripSwitch3.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            btnLight.BackColor = _equip.LD.InnerLamp.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            btnLdLight.BackColor = _equip.LD.InnerLamp.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnProcessLight.BackColor = _equip.PROC.InnerLamp.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnUldLight.BackColor = _equip.UD.InnerLamp.IsOnOff ? UiGlobal.SET_C : UiGlobal.UNSET_C;


            lblLoaderBox.BackColor = _equip.LD.CtrlBoxOpenDetectBJub.IsOnOff == false ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblProcessBox.BackColor = _equip.PROC.CpBoxOpenDetectBJub.IsOnOff == false ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            lblUnloaderBox.BackColor = _equip.UD.CpBoxOpenDetectBJub.IsOnOff == false ? UiGlobal.SET_C : UiGlobal.UNSET_C;


            btnKeyswitch.BackColor = _equip.LD.ModeSelectSwitchAuto.IsOnOff ? UiGlobal.UNEXIST_C : UiGlobal.EXIST_C;
            lblAutoTeach.Text = _equip.LD.ModeSelectSwitchAuto.IsOnOff ? "AUTO" : "TEACH";
            btnKeyswitch.Enabled = _equip.IsDoorRelease == true ? false : true;
            btnDoorOpen.Enabled = _equip.LD.ModeSelectSwitchAuto.IsOnOff == false;
            //lblLoader2In.BackColor = _equip.LD.CstLoader_B.LDCstDetectExist.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;
            //lblLoader1In.BackColor = _equip.LD.CstLoader_A.LDCstDetectExist.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;

            //// 확인필요..
            //lblUnloader2Out.BackColor = _equip.UD.CstUnloader_B.ULDCstDetectExist.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;
            //lblUnloader1Out.BackColor = _equip.UD.CstUnloader_A.ULDCstDetectExist.IsOnOff ? UiGlobal.EXIST_C : UiGlobal.UNEXIST_C;
        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbUnloadMutingIn_Click(object sender, EventArgs e)
        {

        }

        private void btnBreakingCountClear_Click(object sender, EventArgs e)
        {

        }

        public ServoMotorControl[] Motors;
        private void SimState()
        {
            InitializeComponent();

            elementHost1.Child = ucrlEquipView;

            Motors = new ServoMotorControl[]
                         {
                CTRL.GG.Equip.LD.CstLoader_A.CstRotationAxis                ,
                CTRL.GG.Equip.LD.CstLoader_A.CstUpDownAxis                  ,
                CTRL.GG.Equip.LD.CstLoader_B.CstRotationAxis                ,
                CTRL.GG.Equip.LD.CstLoader_B.CstUpDownAxis                  ,
                CTRL.GG.Equip.LD.Loader.X1Axis                              ,
                CTRL.GG.Equip.LD.Loader.X2Axis                              ,
                CTRL.GG.Equip.LD.Loader.Y1Axis                              ,
                CTRL.GG.Equip.LD.Loader.Y2Axis                              ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.YAxis                     ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.X1Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.X2Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubY1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubY2Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubT1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_A.SubT2Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.YAxis                     ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.X1Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.X2Axis                    ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubY1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubY2Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubT1Axis                 ,
                CTRL.GG.Equip.LD.LoaderTransfer_B.SubT2Axis                 ,
                CTRL.GG.Equip.LD.PreAlign.XAxis                             ,
                CTRL.GG.Equip.PROC.FineAlign.XAxis                          ,
                CTRL.GG.Equip.PROC.IRCutStage_A.YAxis                       ,
                CTRL.GG.Equip.PROC.IRCutStage_B.YAxis                       ,
                CTRL.GG.Equip.PROC.LaserHead.XAxis                          ,
                CTRL.GG.Equip.PROC.LaserHead.ZAxis                          ,
                CTRL.GG.Equip.PROC.AfterIRCutTransfer_A.YAxis               ,
                CTRL.GG.Equip.PROC.AfterIRCutTransfer_B.YAxis               ,
                CTRL.GG.Equip.PROC.BreakStage_A.YAxis                       ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubX1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubX2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubY1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubY2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubT1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_A.SubT2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.YAxis                       ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubX1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubX2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubY1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubY2Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubT1Axis                   ,
                CTRL.GG.Equip.PROC.BreakStage_B.SubT2Axis                   ,
                CTRL.GG.Equip.PROC.BreakingHead.X1Axis                      ,
                CTRL.GG.Equip.PROC.BreakingHead.X2Axis                      ,
                CTRL.GG.Equip.PROC.BreakingHead.Z1Axis                      ,
                CTRL.GG.Equip.PROC.BreakingHead.Z2Axis                      ,
                CTRL.GG.Equip.PROC.BreakAlign.XAxis                         ,
                CTRL.GG.Equip.PROC.BreakAlign.ZAxis                         ,
                CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis        ,
                CTRL.GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis        ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_A.T2Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis         ,
                CTRL.GG.Equip.UD.AfterInspUnloaderTransfer_B.T2Axis         ,
                CTRL.GG.Equip.UD.InspCamera.XAxis                           ,
                CTRL.GG.Equip.UD.InspCamera.ZAxis                           ,
                CTRL.GG.Equip.UD.InspectionStage_A.YAxis                    ,
                CTRL.GG.Equip.UD.InspectionStage_B.YAxis                    ,
                CTRL.GG.Equip.UD.Unloader.X1Axis                            ,
                CTRL.GG.Equip.UD.Unloader.X2Axis                            ,
                CTRL.GG.Equip.UD.Unloader.Y1Axis                            ,
                CTRL.GG.Equip.UD.Unloader.Y2Axis                            ,
                CTRL.GG.Equip.UD.CstUnloader_A.CstRotationAxis              ,
                CTRL.GG.Equip.UD.CstUnloader_A.CstUpDownAxis                ,
                CTRL.GG.Equip.UD.CstUnloader_B.CstRotationAxis              ,
                CTRL.GG.Equip.UD.CstUnloader_B.CstUpDownAxis
             };
        }

        private void btnDelayDoor_Click(object sender, EventArgs e)
        {
            ButtonDelay btnDelay = sender as ButtonDelay;

            if (_equip.LD.ModeSelectSwitchAuto.XB_OnOff.vBit == true)
            {
                InterLockMgr.AddInterLock("Interlock <AUTO MODE> \n (Door can not be opened in AUTO MODE.)");
                return;
            }

            //if (_equip.IsHomePositioning || _equip.EquipRunMode == EmEquipRunMode.Auto)
            //{
            //    InterLockMgr.AddInterLock("Interlock <AUTO RUN> \n (AUTO RUN in progress. Door can not be opened)");
            //    return;
            //}
            //if (btnDelay == btnDelayTopDoor1)
            //{
            //    CmdDoorOpenSol(_equip.LD.Door01, btnDelay, !_equip.LD.Door01.YB_OnOff.vBit);
            //}
            else if (btnDelay == btnDelayTopDoor2)
            {
                CmdDoorOpenSol(_equip.LD.Door02, btnDelay, !_equip.LD.Door02.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor3)
            {
                CmdDoorOpenSol(_equip.LD.Door03, btnDelay, !_equip.LD.Door03.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor4)
            {
                CmdDoorOpenSol(_equip.PROC.Door04, btnDelay, !_equip.PROC.Door04.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor5)
            {
                CmdDoorOpenSol(_equip.PROC.Door05, btnDelay, !_equip.PROC.Door05.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor6)
            {
                CmdDoorOpenSol(_equip.PROC.Door06, btnDelay, !_equip.PROC.Door06.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor7)
            {
                CmdDoorOpenSol(_equip.PROC.Door07, btnDelay, !_equip.PROC.Door07.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor8)
            {
                CmdDoorOpenSol(_equip.PROC.Door08, btnDelay, !_equip.PROC.Door08.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor9)
            {
                CmdDoorOpenSol(_equip.UD.Door09, btnDelay, !_equip.UD.Door09.YB_OnOff.vBit);
            }
            else if (btnDelay == btnDelayTopDoor10)
            {
                CmdDoorOpenSol(_equip.UD.Door10, btnDelay, !_equip.UD.Door10.YB_OnOff.vBit);
            }
            //else if (btnDelay == btnDelayTopDoor11)
            //{
            //    CmdDoorOpenSol(_equip.UD.Door11, btnDelay, !_equip.UD.Door11.YB_OnOff.vBit);
            //}
            //else if (btnDelay == btnDelayAllDoorOpen)

            //{
            //    CmdAllDoorOpenSol(true);
            //}
            //else if (btnDelay == btnDelayAllDoorClose)
            //{
            //    CmdAllDoorOpenSol(false);
            //}
        }

        private void lblLaserCover_Click(object sender, EventArgs e)
        {

            if (btnLaserCover == (Button)sender)
            {
                if (_equip.LD.Door01.IsOnOff != true)
                {
                    GG.Equip.LD.Door01.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.LD.Door01.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor2 == (Button)sender)
            {
                if (_equip.LD.Door02.IsOnOff != true)
                {
                    GG.Equip.LD.Door02.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.LD.Door02.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor3 == (Button)sender)
            {
                if (_equip.LD.Door03.IsOnOff != true)
                {
                    GG.Equip.LD.Door03.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.LD.Door03.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor4 == (Button)sender)
            {
                if (_equip.PROC.Door04.IsOnOff != true)
                {
                    GG.Equip.PROC.Door04.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.PROC.Door04.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor5 == (Button)sender)
            {
                if (_equip.PROC.Door05.IsOnOff != true)
                {
                    GG.Equip.PROC.Door05.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.PROC.Door05.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor6 == (Button)sender)
            {
                if (_equip.PROC.Door06.IsOnOff != true)
                {
                    GG.Equip.PROC.Door06.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.PROC.Door06.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor7 == (Button)sender)
            {
                if (_equip.PROC.Door07.IsOnOff != true)
                {
                    GG.Equip.PROC.Door07.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.PROC.Door07.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor8 == (Button)sender)
            {
                if (_equip.PROC.Door08.IsOnOff != true)
                {
                    GG.Equip.PROC.Door08.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.PROC.Door08.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor9 == (Button)sender)
            {
                if (_equip.UD.Door09.IsOnOff != true)
                {
                    GG.Equip.UD.Door09.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.UD.Door09.OnOff(GG.Equip, false);
                }
            }
            else if (btnDelayTopDoor10 == (Button)sender)
            {
                if (_equip.UD.Door10.IsOnOff != true)
                {
                    GG.Equip.UD.Door10.OnOff(GG.Equip, true);
                }
                else
                {
                    GG.Equip.UD.Door10.OnOff(GG.Equip, false);
                }
            }
        }

        private void btnDoorOpen_Click(object sender, EventArgs e)
        {
            if (GG.Equip.LD.ModeSelectSwitchAuto.IsOnOff == true)
            {
                InterLockMgr.AddInterLock("AUTO 모드에서 도어를 오픈할 수 없습니다.");
                return;
            }

            GG.Equip.DoorOpenClose(!GG.Equip.LD.Door01.IsSolOnOff);
        }
        private void btnLight_Click(object sender, EventArgs e)
        {
            bool lamp = !GG.Equip.LD.InnerLamp.IsOnOff;
            GG.Equip.LD.InnerLamp.OnOff(GG.Equip, lamp);
            GG.Equip.PROC.InnerLamp.OnOff(GG.Equip, lamp);
            GG.Equip.UD.InnerLamp.OnOff(GG.Equip, lamp);
        }

        private void btnSafetyReset_Click(object sender, EventArgs e)
        {
            GG.Equip.SafetyMgr.StartReset();
        }

        private void btnKeyswitch_Click(object sender, EventArgs e)
        {
            bool teachSol = !_equip.LD.ModelSelectSwAutoTeachLockingSol.IsOnOff;

            if (teachSol == true && _equip.IsDoorRelease == true)
            {
                InterLockMgr.AddInterLock("도어 오픈 상태에서 AUTO 모드로 변경이 불가합니다.");
                return;
            }
            else
            {
                _equip.LD.ModelSelectSwAutoTeachLockingSol.OnOff(_equip, teachSol);
            }
        }
    }
}

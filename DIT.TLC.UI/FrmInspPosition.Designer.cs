﻿namespace DIT.TLC.UI
{
    partial class FrmInspPosition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInspPosition));
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblBMoveSpeed = new System.Windows.Forms.Label();
            this.txtBMoveSpeed = new System.Windows.Forms.TextBox();
            this.lblBYSpeed = new System.Windows.Forms.Label();
            this.txtBYSpeed = new System.Windows.Forms.TextBox();
            this.lblBXSpeed = new System.Windows.Forms.Label();
            this.txtBXSpeed = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblBRightBotY = new System.Windows.Forms.Label();
            this.txtBRightBotY = new System.Windows.Forms.TextBox();
            this.btnBRbYMove = new System.Windows.Forms.Button();
            this.btnBRbYRead = new System.Windows.Forms.Button();
            this.lblBRightBotX = new System.Windows.Forms.Label();
            this.txtBRightBotX = new System.Windows.Forms.TextBox();
            this.btnBRbXMove = new System.Windows.Forms.Button();
            this.btnBRbXRead = new System.Windows.Forms.Button();
            this.lblBBotZ = new System.Windows.Forms.Label();
            this.txtBBotZ = new System.Windows.Forms.TextBox();
            this.btnBBotZMove = new System.Windows.Forms.Button();
            this.btnBBotZRead = new System.Windows.Forms.Button();
            this.lblBLeftBotY = new System.Windows.Forms.Label();
            this.txtBLeftBotY = new System.Windows.Forms.TextBox();
            this.btnBLbYMove = new System.Windows.Forms.Button();
            this.btnBLbYRead = new System.Windows.Forms.Button();
            this.lblBLeftBotX = new System.Windows.Forms.Label();
            this.txtBLeftBotX = new System.Windows.Forms.TextBox();
            this.btnBLbXMove = new System.Windows.Forms.Button();
            this.btnBLbXRead = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblBTopZ = new System.Windows.Forms.Label();
            this.txtBTopZ = new System.Windows.Forms.TextBox();
            this.btnBTopZMove = new System.Windows.Forms.Button();
            this.btnBTopZRead = new System.Windows.Forms.Button();
            this.lblBRightTopY = new System.Windows.Forms.Label();
            this.txtBRightTopY = new System.Windows.Forms.TextBox();
            this.btnBRtYMove = new System.Windows.Forms.Button();
            this.btnBRtYRead = new System.Windows.Forms.Button();
            this.lblBRrightTopX = new System.Windows.Forms.Label();
            this.txtBRightTopX = new System.Windows.Forms.TextBox();
            this.btnBRtXMove = new System.Windows.Forms.Button();
            this.btnBRtXRead = new System.Windows.Forms.Button();
            this.lblBLeftTopY = new System.Windows.Forms.Label();
            this.txtBLeftTopY = new System.Windows.Forms.TextBox();
            this.btnBLtYMove = new System.Windows.Forms.Button();
            this.btnBLtYRead = new System.Windows.Forms.Button();
            this.lblBLeftTopX = new System.Windows.Forms.Label();
            this.txtBLeftTopX = new System.Windows.Forms.TextBox();
            this.btnBLtXMove = new System.Windows.Forms.Button();
            this.btnBLtXRead = new System.Windows.Forms.Button();
            this.lblBInspPosition = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblAInspPosition = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAMoveSpeed = new System.Windows.Forms.Label();
            this.txtAMoveSpeed = new System.Windows.Forms.TextBox();
            this.lblAYSpeed = new System.Windows.Forms.Label();
            this.txtAYSpeed = new System.Windows.Forms.TextBox();
            this.lblAXSpeed = new System.Windows.Forms.Label();
            this.txtAXSpeed = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblRb = new System.Windows.Forms.Label();
            this.lblRt = new System.Windows.Forms.Label();
            this.lblARightBotY = new System.Windows.Forms.Label();
            this.txtARightBotY = new System.Windows.Forms.TextBox();
            this.btnARbYMove = new System.Windows.Forms.Button();
            this.btnARbYRead = new System.Windows.Forms.Button();
            this.lblARightBotX = new System.Windows.Forms.Label();
            this.txtARightBotX = new System.Windows.Forms.TextBox();
            this.btnARbXMove = new System.Windows.Forms.Button();
            this.btnARbXRead = new System.Windows.Forms.Button();
            this.lblABotZ = new System.Windows.Forms.Label();
            this.txtABotZ = new System.Windows.Forms.TextBox();
            this.btnABotZMove = new System.Windows.Forms.Button();
            this.btnABotZRead = new System.Windows.Forms.Button();
            this.lblALeftBotY = new System.Windows.Forms.Label();
            this.txtALeftBotY = new System.Windows.Forms.TextBox();
            this.btnALbYMove = new System.Windows.Forms.Button();
            this.btnALbYRead = new System.Windows.Forms.Button();
            this.lblALeftBotX = new System.Windows.Forms.Label();
            this.txtALeftBotX = new System.Windows.Forms.TextBox();
            this.btnALbXMove = new System.Windows.Forms.Button();
            this.btnALbXRead = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblLb = new System.Windows.Forms.Label();
            this.lblLt = new System.Windows.Forms.Label();
            this.lblATopZ = new System.Windows.Forms.Label();
            this.txtATopZ = new System.Windows.Forms.TextBox();
            this.btnATopZMove = new System.Windows.Forms.Button();
            this.btnATopZRead = new System.Windows.Forms.Button();
            this.lblARightTopY = new System.Windows.Forms.Label();
            this.txtARightTopY = new System.Windows.Forms.TextBox();
            this.btnARtYMove = new System.Windows.Forms.Button();
            this.btnARtYRead = new System.Windows.Forms.Button();
            this.lblARightTopX = new System.Windows.Forms.Label();
            this.txtARightTopX = new System.Windows.Forms.TextBox();
            this.btnARtXMove = new System.Windows.Forms.Button();
            this.btnARtXRead = new System.Windows.Forms.Button();
            this.lblALeftTopY = new System.Windows.Forms.Label();
            this.txtALeftTopY = new System.Windows.Forms.TextBox();
            this.btnALtYMove = new System.Windows.Forms.Button();
            this.btnALtYRead = new System.Windows.Forms.Button();
            this.lblALeftTopX = new System.Windows.Forms.Label();
            this.txtALeftTopX = new System.Windows.Forms.TextBox();
            this.btnALtXMove = new System.Windows.Forms.Button();
            this.btnALtXRead = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblBMoveSpeed);
            this.panel2.Controls.Add(this.txtBMoveSpeed);
            this.panel2.Controls.Add(this.lblBYSpeed);
            this.panel2.Controls.Add(this.txtBYSpeed);
            this.panel2.Controls.Add(this.lblBXSpeed);
            this.panel2.Controls.Add(this.txtBXSpeed);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.lblBRightBotY);
            this.panel2.Controls.Add(this.txtBRightBotY);
            this.panel2.Controls.Add(this.btnBRbYMove);
            this.panel2.Controls.Add(this.btnBRbYRead);
            this.panel2.Controls.Add(this.lblBRightBotX);
            this.panel2.Controls.Add(this.txtBRightBotX);
            this.panel2.Controls.Add(this.btnBRbXMove);
            this.panel2.Controls.Add(this.btnBRbXRead);
            this.panel2.Controls.Add(this.lblBBotZ);
            this.panel2.Controls.Add(this.txtBBotZ);
            this.panel2.Controls.Add(this.btnBBotZMove);
            this.panel2.Controls.Add(this.btnBBotZRead);
            this.panel2.Controls.Add(this.lblBLeftBotY);
            this.panel2.Controls.Add(this.txtBLeftBotY);
            this.panel2.Controls.Add(this.btnBLbYMove);
            this.panel2.Controls.Add(this.btnBLbYRead);
            this.panel2.Controls.Add(this.lblBLeftBotX);
            this.panel2.Controls.Add(this.txtBLeftBotX);
            this.panel2.Controls.Add(this.btnBLbXMove);
            this.panel2.Controls.Add(this.btnBLbXRead);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.lblBTopZ);
            this.panel2.Controls.Add(this.txtBTopZ);
            this.panel2.Controls.Add(this.btnBTopZMove);
            this.panel2.Controls.Add(this.btnBTopZRead);
            this.panel2.Controls.Add(this.lblBRightTopY);
            this.panel2.Controls.Add(this.txtBRightTopY);
            this.panel2.Controls.Add(this.btnBRtYMove);
            this.panel2.Controls.Add(this.btnBRtYRead);
            this.panel2.Controls.Add(this.lblBRrightTopX);
            this.panel2.Controls.Add(this.txtBRightTopX);
            this.panel2.Controls.Add(this.btnBRtXMove);
            this.panel2.Controls.Add(this.btnBRtXRead);
            this.panel2.Controls.Add(this.lblBLeftTopY);
            this.panel2.Controls.Add(this.txtBLeftTopY);
            this.panel2.Controls.Add(this.btnBLtYMove);
            this.panel2.Controls.Add(this.btnBLtYRead);
            this.panel2.Controls.Add(this.lblBLeftTopX);
            this.panel2.Controls.Add(this.txtBLeftTopX);
            this.panel2.Controls.Add(this.btnBLtXMove);
            this.panel2.Controls.Add(this.btnBLtXRead);
            this.panel2.Controls.Add(this.lblBInspPosition);
            this.panel2.Location = new System.Drawing.Point(766, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(748, 503);
            this.panel2.TabIndex = 460;
            // 
            // lblBMoveSpeed
            // 
            this.lblBMoveSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblBMoveSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBMoveSpeed.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBMoveSpeed.Location = new System.Drawing.Point(528, 463);
            this.lblBMoveSpeed.Name = "lblBMoveSpeed";
            this.lblBMoveSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblBMoveSpeed.TabIndex = 675;
            this.lblBMoveSpeed.Text = "Move Speed";
            this.lblBMoveSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBMoveSpeed
            // 
            this.txtBMoveSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBMoveSpeed.Location = new System.Drawing.Point(604, 463);
            this.txtBMoveSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBMoveSpeed.Name = "txtBMoveSpeed";
            this.txtBMoveSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtBMoveSpeed.TabIndex = 676;
            // 
            // lblBYSpeed
            // 
            this.lblBYSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblBYSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBYSpeed.Location = new System.Drawing.Point(271, 463);
            this.lblBYSpeed.Name = "lblBYSpeed";
            this.lblBYSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblBYSpeed.TabIndex = 673;
            this.lblBYSpeed.Text = "Y Speed";
            this.lblBYSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBYSpeed
            // 
            this.txtBYSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBYSpeed.Location = new System.Drawing.Point(347, 463);
            this.txtBYSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBYSpeed.Name = "txtBYSpeed";
            this.txtBYSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtBYSpeed.TabIndex = 674;
            // 
            // lblBXSpeed
            // 
            this.lblBXSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblBXSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBXSpeed.Location = new System.Drawing.Point(15, 464);
            this.lblBXSpeed.Name = "lblBXSpeed";
            this.lblBXSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblBXSpeed.TabIndex = 671;
            this.lblBXSpeed.Text = "X Speed";
            this.lblBXSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBXSpeed
            // 
            this.txtBXSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBXSpeed.Location = new System.Drawing.Point(91, 464);
            this.txtBXSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBXSpeed.Name = "txtBXSpeed";
            this.txtBXSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtBXSpeed.TabIndex = 672;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label5);
            this.panel5.ForeColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(376, 141);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(203, 219);
            this.panel5.TabIndex = 653;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.DodgerBlue;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(132, 191);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 27);
            this.label4.TabIndex = 460;
            this.label4.Text = "RB";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.GreenYellow;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(132, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 27);
            this.label5.TabIndex = 459;
            this.label5.Text = "RT";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBRightBotY
            // 
            this.lblBRightBotY.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblBRightBotY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBRightBotY.Location = new System.Drawing.Point(376, 399);
            this.lblBRightBotY.Name = "lblBRightBotY";
            this.lblBRightBotY.Size = new System.Drawing.Size(70, 27);
            this.lblBRightBotY.TabIndex = 667;
            this.lblBRightBotY.Text = "RB Y";
            this.lblBRightBotY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBRightBotY
            // 
            this.txtBRightBotY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBRightBotY.Location = new System.Drawing.Point(452, 399);
            this.txtBRightBotY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBRightBotY.Name = "txtBRightBotY";
            this.txtBRightBotY.Size = new System.Drawing.Size(127, 27);
            this.txtBRightBotY.TabIndex = 668;
            // 
            // btnBRbYMove
            // 
            this.btnBRbYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRbYMove.ForeColor = System.Drawing.Color.Black;
            this.btnBRbYMove.Location = new System.Drawing.Point(661, 399);
            this.btnBRbYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRbYMove.Name = "btnBRbYMove";
            this.btnBRbYMove.Size = new System.Drawing.Size(70, 27);
            this.btnBRbYMove.TabIndex = 670;
            this.btnBRbYMove.Text = "이동";
            this.btnBRbYMove.UseVisualStyleBackColor = false;
            this.btnBRbYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBRbYRead
            // 
            this.btnBRbYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRbYRead.ForeColor = System.Drawing.Color.Black;
            this.btnBRbYRead.Location = new System.Drawing.Point(585, 399);
            this.btnBRbYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRbYRead.Name = "btnBRbYRead";
            this.btnBRbYRead.Size = new System.Drawing.Size(70, 27);
            this.btnBRbYRead.TabIndex = 669;
            this.btnBRbYRead.Text = "읽기";
            this.btnBRbYRead.UseVisualStyleBackColor = false;
            this.btnBRbYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBRightBotX
            // 
            this.lblBRightBotX.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblBRightBotX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBRightBotX.Location = new System.Drawing.Point(376, 368);
            this.lblBRightBotX.Name = "lblBRightBotX";
            this.lblBRightBotX.Size = new System.Drawing.Size(70, 27);
            this.lblBRightBotX.TabIndex = 663;
            this.lblBRightBotX.Text = "RB X";
            this.lblBRightBotX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBRightBotX
            // 
            this.txtBRightBotX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBRightBotX.Location = new System.Drawing.Point(452, 368);
            this.txtBRightBotX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBRightBotX.Name = "txtBRightBotX";
            this.txtBRightBotX.Size = new System.Drawing.Size(127, 27);
            this.txtBRightBotX.TabIndex = 664;
            // 
            // btnBRbXMove
            // 
            this.btnBRbXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRbXMove.ForeColor = System.Drawing.Color.Black;
            this.btnBRbXMove.Location = new System.Drawing.Point(661, 368);
            this.btnBRbXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRbXMove.Name = "btnBRbXMove";
            this.btnBRbXMove.Size = new System.Drawing.Size(70, 27);
            this.btnBRbXMove.TabIndex = 666;
            this.btnBRbXMove.Text = "이동";
            this.btnBRbXMove.UseVisualStyleBackColor = false;
            this.btnBRbXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBRbXRead
            // 
            this.btnBRbXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRbXRead.ForeColor = System.Drawing.Color.Black;
            this.btnBRbXRead.Location = new System.Drawing.Point(585, 368);
            this.btnBRbXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRbXRead.Name = "btnBRbXRead";
            this.btnBRbXRead.Size = new System.Drawing.Size(70, 27);
            this.btnBRbXRead.TabIndex = 665;
            this.btnBRbXRead.Text = "읽기";
            this.btnBRbXRead.UseVisualStyleBackColor = false;
            this.btnBRbXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBBotZ
            // 
            this.lblBBotZ.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblBBotZ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBBotZ.Location = new System.Drawing.Point(15, 429);
            this.lblBBotZ.Name = "lblBBotZ";
            this.lblBBotZ.Size = new System.Drawing.Size(70, 27);
            this.lblBBotZ.TabIndex = 659;
            this.lblBBotZ.Text = "BOT Z";
            this.lblBBotZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBBotZ
            // 
            this.txtBBotZ.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBBotZ.Location = new System.Drawing.Point(91, 429);
            this.txtBBotZ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBBotZ.Name = "txtBBotZ";
            this.txtBBotZ.Size = new System.Drawing.Size(127, 27);
            this.txtBBotZ.TabIndex = 660;
            // 
            // btnBBotZMove
            // 
            this.btnBBotZMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBBotZMove.ForeColor = System.Drawing.Color.Black;
            this.btnBBotZMove.Location = new System.Drawing.Point(300, 429);
            this.btnBBotZMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBBotZMove.Name = "btnBBotZMove";
            this.btnBBotZMove.Size = new System.Drawing.Size(70, 27);
            this.btnBBotZMove.TabIndex = 662;
            this.btnBBotZMove.Text = "이동";
            this.btnBBotZMove.UseVisualStyleBackColor = false;
            this.btnBBotZMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBBotZRead
            // 
            this.btnBBotZRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBBotZRead.ForeColor = System.Drawing.Color.Black;
            this.btnBBotZRead.Location = new System.Drawing.Point(224, 429);
            this.btnBBotZRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBBotZRead.Name = "btnBBotZRead";
            this.btnBBotZRead.Size = new System.Drawing.Size(70, 27);
            this.btnBBotZRead.TabIndex = 661;
            this.btnBBotZRead.Text = "읽기";
            this.btnBBotZRead.UseVisualStyleBackColor = false;
            this.btnBBotZRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBLeftBotY
            // 
            this.lblBLeftBotY.BackColor = System.Drawing.Color.Red;
            this.lblBLeftBotY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBLeftBotY.Location = new System.Drawing.Point(15, 398);
            this.lblBLeftBotY.Name = "lblBLeftBotY";
            this.lblBLeftBotY.Size = new System.Drawing.Size(70, 27);
            this.lblBLeftBotY.TabIndex = 655;
            this.lblBLeftBotY.Text = "LB Y";
            this.lblBLeftBotY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBLeftBotY
            // 
            this.txtBLeftBotY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBLeftBotY.Location = new System.Drawing.Point(91, 398);
            this.txtBLeftBotY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBLeftBotY.Name = "txtBLeftBotY";
            this.txtBLeftBotY.Size = new System.Drawing.Size(127, 27);
            this.txtBLeftBotY.TabIndex = 656;
            // 
            // btnBLbYMove
            // 
            this.btnBLbYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLbYMove.ForeColor = System.Drawing.Color.Black;
            this.btnBLbYMove.Location = new System.Drawing.Point(300, 398);
            this.btnBLbYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLbYMove.Name = "btnBLbYMove";
            this.btnBLbYMove.Size = new System.Drawing.Size(70, 27);
            this.btnBLbYMove.TabIndex = 658;
            this.btnBLbYMove.Text = "이동";
            this.btnBLbYMove.UseVisualStyleBackColor = false;
            this.btnBLbYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBLbYRead
            // 
            this.btnBLbYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLbYRead.ForeColor = System.Drawing.Color.Black;
            this.btnBLbYRead.Location = new System.Drawing.Point(224, 398);
            this.btnBLbYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLbYRead.Name = "btnBLbYRead";
            this.btnBLbYRead.Size = new System.Drawing.Size(70, 27);
            this.btnBLbYRead.TabIndex = 657;
            this.btnBLbYRead.Text = "읽기";
            this.btnBLbYRead.UseVisualStyleBackColor = false;
            this.btnBLbYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBLeftBotX
            // 
            this.lblBLeftBotX.BackColor = System.Drawing.Color.Red;
            this.lblBLeftBotX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBLeftBotX.Location = new System.Drawing.Point(15, 367);
            this.lblBLeftBotX.Name = "lblBLeftBotX";
            this.lblBLeftBotX.Size = new System.Drawing.Size(70, 27);
            this.lblBLeftBotX.TabIndex = 650;
            this.lblBLeftBotX.Text = "LB X";
            this.lblBLeftBotX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBLeftBotX
            // 
            this.txtBLeftBotX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBLeftBotX.Location = new System.Drawing.Point(91, 367);
            this.txtBLeftBotX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBLeftBotX.Name = "txtBLeftBotX";
            this.txtBLeftBotX.Size = new System.Drawing.Size(127, 27);
            this.txtBLeftBotX.TabIndex = 651;
            // 
            // btnBLbXMove
            // 
            this.btnBLbXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLbXMove.ForeColor = System.Drawing.Color.Black;
            this.btnBLbXMove.Location = new System.Drawing.Point(300, 367);
            this.btnBLbXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLbXMove.Name = "btnBLbXMove";
            this.btnBLbXMove.Size = new System.Drawing.Size(70, 27);
            this.btnBLbXMove.TabIndex = 654;
            this.btnBLbXMove.Text = "이동";
            this.btnBLbXMove.UseVisualStyleBackColor = false;
            this.btnBLbXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBLbXRead
            // 
            this.btnBLbXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLbXRead.ForeColor = System.Drawing.Color.Black;
            this.btnBLbXRead.Location = new System.Drawing.Point(224, 367);
            this.btnBLbXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLbXRead.Name = "btnBLbXRead";
            this.btnBLbXRead.Size = new System.Drawing.Size(70, 27);
            this.btnBLbXRead.TabIndex = 652;
            this.btnBLbXRead.Text = "읽기";
            this.btnBLbXRead.UseVisualStyleBackColor = false;
            this.btnBLbXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label12);
            this.panel6.ForeColor = System.Drawing.Color.Black;
            this.panel6.Location = new System.Drawing.Point(91, 141);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(203, 219);
            this.panel6.TabIndex = 649;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Red;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Location = new System.Drawing.Point(0, 191);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 27);
            this.label11.TabIndex = 460;
            this.label11.Text = "LB";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Yellow;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 27);
            this.label12.TabIndex = 459;
            this.label12.Text = "LT";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBTopZ
            // 
            this.lblBTopZ.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblBTopZ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBTopZ.Location = new System.Drawing.Point(15, 106);
            this.lblBTopZ.Name = "lblBTopZ";
            this.lblBTopZ.Size = new System.Drawing.Size(70, 27);
            this.lblBTopZ.TabIndex = 645;
            this.lblBTopZ.Text = "TOP Z";
            this.lblBTopZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBTopZ
            // 
            this.txtBTopZ.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBTopZ.Location = new System.Drawing.Point(91, 106);
            this.txtBTopZ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBTopZ.Name = "txtBTopZ";
            this.txtBTopZ.Size = new System.Drawing.Size(127, 27);
            this.txtBTopZ.TabIndex = 646;
            // 
            // btnBTopZMove
            // 
            this.btnBTopZMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBTopZMove.ForeColor = System.Drawing.Color.Black;
            this.btnBTopZMove.Location = new System.Drawing.Point(300, 106);
            this.btnBTopZMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBTopZMove.Name = "btnBTopZMove";
            this.btnBTopZMove.Size = new System.Drawing.Size(70, 27);
            this.btnBTopZMove.TabIndex = 648;
            this.btnBTopZMove.Text = "이동";
            this.btnBTopZMove.UseVisualStyleBackColor = false;
            this.btnBTopZMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBTopZRead
            // 
            this.btnBTopZRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBTopZRead.ForeColor = System.Drawing.Color.Black;
            this.btnBTopZRead.Location = new System.Drawing.Point(224, 106);
            this.btnBTopZRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBTopZRead.Name = "btnBTopZRead";
            this.btnBTopZRead.Size = new System.Drawing.Size(70, 27);
            this.btnBTopZRead.TabIndex = 647;
            this.btnBTopZRead.Text = "읽기";
            this.btnBTopZRead.UseVisualStyleBackColor = false;
            this.btnBTopZRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBRightTopY
            // 
            this.lblBRightTopY.BackColor = System.Drawing.Color.GreenYellow;
            this.lblBRightTopY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBRightTopY.Location = new System.Drawing.Point(376, 76);
            this.lblBRightTopY.Name = "lblBRightTopY";
            this.lblBRightTopY.Size = new System.Drawing.Size(70, 27);
            this.lblBRightTopY.TabIndex = 641;
            this.lblBRightTopY.Text = "RT Y";
            this.lblBRightTopY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBRightTopY
            // 
            this.txtBRightTopY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBRightTopY.Location = new System.Drawing.Point(452, 76);
            this.txtBRightTopY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBRightTopY.Name = "txtBRightTopY";
            this.txtBRightTopY.Size = new System.Drawing.Size(127, 27);
            this.txtBRightTopY.TabIndex = 642;
            // 
            // btnBRtYMove
            // 
            this.btnBRtYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRtYMove.ForeColor = System.Drawing.Color.Black;
            this.btnBRtYMove.Location = new System.Drawing.Point(661, 76);
            this.btnBRtYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRtYMove.Name = "btnBRtYMove";
            this.btnBRtYMove.Size = new System.Drawing.Size(70, 27);
            this.btnBRtYMove.TabIndex = 644;
            this.btnBRtYMove.Text = "이동";
            this.btnBRtYMove.UseVisualStyleBackColor = false;
            this.btnBRtYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBRtYRead
            // 
            this.btnBRtYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRtYRead.ForeColor = System.Drawing.Color.Black;
            this.btnBRtYRead.Location = new System.Drawing.Point(585, 76);
            this.btnBRtYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRtYRead.Name = "btnBRtYRead";
            this.btnBRtYRead.Size = new System.Drawing.Size(70, 27);
            this.btnBRtYRead.TabIndex = 643;
            this.btnBRtYRead.Text = "읽기";
            this.btnBRtYRead.UseVisualStyleBackColor = false;
            this.btnBRtYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBRrightTopX
            // 
            this.lblBRrightTopX.BackColor = System.Drawing.Color.GreenYellow;
            this.lblBRrightTopX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBRrightTopX.Location = new System.Drawing.Point(376, 45);
            this.lblBRrightTopX.Name = "lblBRrightTopX";
            this.lblBRrightTopX.Size = new System.Drawing.Size(70, 27);
            this.lblBRrightTopX.TabIndex = 637;
            this.lblBRrightTopX.Text = "RT X";
            this.lblBRrightTopX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBRightTopX
            // 
            this.txtBRightTopX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBRightTopX.Location = new System.Drawing.Point(452, 45);
            this.txtBRightTopX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBRightTopX.Name = "txtBRightTopX";
            this.txtBRightTopX.Size = new System.Drawing.Size(127, 27);
            this.txtBRightTopX.TabIndex = 638;
            // 
            // btnBRtXMove
            // 
            this.btnBRtXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRtXMove.ForeColor = System.Drawing.Color.Black;
            this.btnBRtXMove.Location = new System.Drawing.Point(661, 45);
            this.btnBRtXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRtXMove.Name = "btnBRtXMove";
            this.btnBRtXMove.Size = new System.Drawing.Size(70, 27);
            this.btnBRtXMove.TabIndex = 640;
            this.btnBRtXMove.Text = "이동";
            this.btnBRtXMove.UseVisualStyleBackColor = false;
            this.btnBRtXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBRtXRead
            // 
            this.btnBRtXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBRtXRead.ForeColor = System.Drawing.Color.Black;
            this.btnBRtXRead.Location = new System.Drawing.Point(585, 45);
            this.btnBRtXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBRtXRead.Name = "btnBRtXRead";
            this.btnBRtXRead.Size = new System.Drawing.Size(70, 27);
            this.btnBRtXRead.TabIndex = 639;
            this.btnBRtXRead.Text = "읽기";
            this.btnBRtXRead.UseVisualStyleBackColor = false;
            this.btnBRtXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBLeftTopY
            // 
            this.lblBLeftTopY.BackColor = System.Drawing.Color.Yellow;
            this.lblBLeftTopY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBLeftTopY.Location = new System.Drawing.Point(15, 75);
            this.lblBLeftTopY.Name = "lblBLeftTopY";
            this.lblBLeftTopY.Size = new System.Drawing.Size(70, 27);
            this.lblBLeftTopY.TabIndex = 633;
            this.lblBLeftTopY.Text = "LT Y";
            this.lblBLeftTopY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBLeftTopY
            // 
            this.txtBLeftTopY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBLeftTopY.Location = new System.Drawing.Point(91, 75);
            this.txtBLeftTopY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBLeftTopY.Name = "txtBLeftTopY";
            this.txtBLeftTopY.Size = new System.Drawing.Size(127, 27);
            this.txtBLeftTopY.TabIndex = 634;
            // 
            // btnBLtYMove
            // 
            this.btnBLtYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLtYMove.ForeColor = System.Drawing.Color.Black;
            this.btnBLtYMove.Location = new System.Drawing.Point(300, 75);
            this.btnBLtYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLtYMove.Name = "btnBLtYMove";
            this.btnBLtYMove.Size = new System.Drawing.Size(70, 27);
            this.btnBLtYMove.TabIndex = 636;
            this.btnBLtYMove.Text = "이동";
            this.btnBLtYMove.UseVisualStyleBackColor = false;
            this.btnBLtYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBLtYRead
            // 
            this.btnBLtYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLtYRead.ForeColor = System.Drawing.Color.Black;
            this.btnBLtYRead.Location = new System.Drawing.Point(224, 75);
            this.btnBLtYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLtYRead.Name = "btnBLtYRead";
            this.btnBLtYRead.Size = new System.Drawing.Size(70, 27);
            this.btnBLtYRead.TabIndex = 635;
            this.btnBLtYRead.Text = "읽기";
            this.btnBLtYRead.UseVisualStyleBackColor = false;
            this.btnBLtYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBLeftTopX
            // 
            this.lblBLeftTopX.BackColor = System.Drawing.Color.Yellow;
            this.lblBLeftTopX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBLeftTopX.Location = new System.Drawing.Point(15, 44);
            this.lblBLeftTopX.Name = "lblBLeftTopX";
            this.lblBLeftTopX.Size = new System.Drawing.Size(70, 27);
            this.lblBLeftTopX.TabIndex = 629;
            this.lblBLeftTopX.Text = "LT X";
            this.lblBLeftTopX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBLeftTopX
            // 
            this.txtBLeftTopX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBLeftTopX.Location = new System.Drawing.Point(91, 44);
            this.txtBLeftTopX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBLeftTopX.Name = "txtBLeftTopX";
            this.txtBLeftTopX.Size = new System.Drawing.Size(127, 27);
            this.txtBLeftTopX.TabIndex = 630;
            // 
            // btnBLtXMove
            // 
            this.btnBLtXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLtXMove.ForeColor = System.Drawing.Color.Black;
            this.btnBLtXMove.Location = new System.Drawing.Point(300, 44);
            this.btnBLtXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLtXMove.Name = "btnBLtXMove";
            this.btnBLtXMove.Size = new System.Drawing.Size(70, 27);
            this.btnBLtXMove.TabIndex = 632;
            this.btnBLtXMove.Text = "이동";
            this.btnBLtXMove.UseVisualStyleBackColor = false;
            this.btnBLtXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnBLtXRead
            // 
            this.btnBLtXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBLtXRead.ForeColor = System.Drawing.Color.Black;
            this.btnBLtXRead.Location = new System.Drawing.Point(224, 44);
            this.btnBLtXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBLtXRead.Name = "btnBLtXRead";
            this.btnBLtXRead.Size = new System.Drawing.Size(70, 27);
            this.btnBLtXRead.TabIndex = 631;
            this.btnBLtXRead.Text = "읽기";
            this.btnBLtXRead.UseVisualStyleBackColor = false;
            this.btnBLtXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblBInspPosition
            // 
            this.lblBInspPosition.AutoEllipsis = true;
            this.lblBInspPosition.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBInspPosition.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBInspPosition.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBInspPosition.ForeColor = System.Drawing.Color.Black;
            this.lblBInspPosition.Location = new System.Drawing.Point(0, 0);
            this.lblBInspPosition.Name = "lblBInspPosition";
            this.lblBInspPosition.Size = new System.Drawing.Size(746, 28);
            this.lblBInspPosition.TabIndex = 9;
            this.lblBInspPosition.Text = "■ B열 검사 포지션";
            this.lblBInspPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(1379, 522);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(135, 45);
            this.btnClose.TabIndex = 461;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblAInspPosition
            // 
            this.lblAInspPosition.AutoEllipsis = true;
            this.lblAInspPosition.BackColor = System.Drawing.Color.Gainsboro;
            this.lblAInspPosition.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblAInspPosition.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblAInspPosition.ForeColor = System.Drawing.Color.Black;
            this.lblAInspPosition.Location = new System.Drawing.Point(0, 0);
            this.lblAInspPosition.Name = "lblAInspPosition";
            this.lblAInspPosition.Size = new System.Drawing.Size(746, 28);
            this.lblAInspPosition.TabIndex = 9;
            this.lblAInspPosition.Text = "■ A열 검사 포지션";
            this.lblAInspPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblAMoveSpeed);
            this.panel1.Controls.Add(this.txtAMoveSpeed);
            this.panel1.Controls.Add(this.lblAYSpeed);
            this.panel1.Controls.Add(this.txtAYSpeed);
            this.panel1.Controls.Add(this.lblAXSpeed);
            this.panel1.Controls.Add(this.txtAXSpeed);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.lblARightBotY);
            this.panel1.Controls.Add(this.txtARightBotY);
            this.panel1.Controls.Add(this.btnARbYMove);
            this.panel1.Controls.Add(this.btnARbYRead);
            this.panel1.Controls.Add(this.lblARightBotX);
            this.panel1.Controls.Add(this.txtARightBotX);
            this.panel1.Controls.Add(this.btnARbXMove);
            this.panel1.Controls.Add(this.btnARbXRead);
            this.panel1.Controls.Add(this.lblABotZ);
            this.panel1.Controls.Add(this.txtABotZ);
            this.panel1.Controls.Add(this.btnABotZMove);
            this.panel1.Controls.Add(this.btnABotZRead);
            this.panel1.Controls.Add(this.lblALeftBotY);
            this.panel1.Controls.Add(this.txtALeftBotY);
            this.panel1.Controls.Add(this.btnALbYMove);
            this.panel1.Controls.Add(this.btnALbYRead);
            this.panel1.Controls.Add(this.lblALeftBotX);
            this.panel1.Controls.Add(this.txtALeftBotX);
            this.panel1.Controls.Add(this.btnALbXMove);
            this.panel1.Controls.Add(this.btnALbXRead);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.lblATopZ);
            this.panel1.Controls.Add(this.txtATopZ);
            this.panel1.Controls.Add(this.btnATopZMove);
            this.panel1.Controls.Add(this.btnATopZRead);
            this.panel1.Controls.Add(this.lblARightTopY);
            this.panel1.Controls.Add(this.txtARightTopY);
            this.panel1.Controls.Add(this.btnARtYMove);
            this.panel1.Controls.Add(this.btnARtYRead);
            this.panel1.Controls.Add(this.lblARightTopX);
            this.panel1.Controls.Add(this.txtARightTopX);
            this.panel1.Controls.Add(this.btnARtXMove);
            this.panel1.Controls.Add(this.btnARtXRead);
            this.panel1.Controls.Add(this.lblALeftTopY);
            this.panel1.Controls.Add(this.txtALeftTopY);
            this.panel1.Controls.Add(this.btnALtYMove);
            this.panel1.Controls.Add(this.btnALtYRead);
            this.panel1.Controls.Add(this.lblALeftTopX);
            this.panel1.Controls.Add(this.txtALeftTopX);
            this.panel1.Controls.Add(this.btnALtXMove);
            this.panel1.Controls.Add(this.btnALtXRead);
            this.panel1.Controls.Add(this.lblAInspPosition);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(748, 503);
            this.panel1.TabIndex = 459;
            // 
            // lblAMoveSpeed
            // 
            this.lblAMoveSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblAMoveSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAMoveSpeed.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAMoveSpeed.Location = new System.Drawing.Point(530, 464);
            this.lblAMoveSpeed.Name = "lblAMoveSpeed";
            this.lblAMoveSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblAMoveSpeed.TabIndex = 627;
            this.lblAMoveSpeed.Text = "Move Speed";
            this.lblAMoveSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAMoveSpeed
            // 
            this.txtAMoveSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtAMoveSpeed.Location = new System.Drawing.Point(606, 464);
            this.txtAMoveSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAMoveSpeed.Name = "txtAMoveSpeed";
            this.txtAMoveSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtAMoveSpeed.TabIndex = 628;
            // 
            // lblAYSpeed
            // 
            this.lblAYSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblAYSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAYSpeed.Location = new System.Drawing.Point(273, 464);
            this.lblAYSpeed.Name = "lblAYSpeed";
            this.lblAYSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblAYSpeed.TabIndex = 625;
            this.lblAYSpeed.Text = "Y Speed";
            this.lblAYSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAYSpeed
            // 
            this.txtAYSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtAYSpeed.Location = new System.Drawing.Point(349, 464);
            this.txtAYSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAYSpeed.Name = "txtAYSpeed";
            this.txtAYSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtAYSpeed.TabIndex = 626;
            // 
            // lblAXSpeed
            // 
            this.lblAXSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblAXSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAXSpeed.Location = new System.Drawing.Point(17, 465);
            this.lblAXSpeed.Name = "lblAXSpeed";
            this.lblAXSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblAXSpeed.TabIndex = 623;
            this.lblAXSpeed.Text = "X Speed";
            this.lblAXSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAXSpeed
            // 
            this.txtAXSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtAXSpeed.Location = new System.Drawing.Point(93, 465);
            this.txtAXSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAXSpeed.Name = "txtAXSpeed";
            this.txtAXSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtAXSpeed.TabIndex = 624;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblRb);
            this.panel3.Controls.Add(this.lblRt);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(378, 141);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(203, 219);
            this.panel3.TabIndex = 605;
            // 
            // lblRb
            // 
            this.lblRb.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblRb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRb.Location = new System.Drawing.Point(132, 191);
            this.lblRb.Name = "lblRb";
            this.lblRb.Size = new System.Drawing.Size(70, 27);
            this.lblRb.TabIndex = 460;
            this.lblRb.Text = "RB";
            this.lblRb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRt
            // 
            this.lblRt.BackColor = System.Drawing.Color.GreenYellow;
            this.lblRt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRt.Location = new System.Drawing.Point(132, 0);
            this.lblRt.Name = "lblRt";
            this.lblRt.Size = new System.Drawing.Size(70, 27);
            this.lblRt.TabIndex = 459;
            this.lblRt.Text = "RT";
            this.lblRt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblARightBotY
            // 
            this.lblARightBotY.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblARightBotY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblARightBotY.Location = new System.Drawing.Point(378, 399);
            this.lblARightBotY.Name = "lblARightBotY";
            this.lblARightBotY.Size = new System.Drawing.Size(70, 27);
            this.lblARightBotY.TabIndex = 619;
            this.lblARightBotY.Text = "RB Y";
            this.lblARightBotY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtARightBotY
            // 
            this.txtARightBotY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtARightBotY.Location = new System.Drawing.Point(454, 399);
            this.txtARightBotY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtARightBotY.Name = "txtARightBotY";
            this.txtARightBotY.Size = new System.Drawing.Size(127, 27);
            this.txtARightBotY.TabIndex = 620;
            // 
            // btnARbYMove
            // 
            this.btnARbYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnARbYMove.ForeColor = System.Drawing.Color.Black;
            this.btnARbYMove.Location = new System.Drawing.Point(663, 399);
            this.btnARbYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARbYMove.Name = "btnARbYMove";
            this.btnARbYMove.Size = new System.Drawing.Size(70, 27);
            this.btnARbYMove.TabIndex = 622;
            this.btnARbYMove.Text = "이동";
            this.btnARbYMove.UseVisualStyleBackColor = false;
            this.btnARbYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnARbYRead
            // 
            this.btnARbYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnARbYRead.ForeColor = System.Drawing.Color.Black;
            this.btnARbYRead.Location = new System.Drawing.Point(587, 399);
            this.btnARbYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARbYRead.Name = "btnARbYRead";
            this.btnARbYRead.Size = new System.Drawing.Size(70, 27);
            this.btnARbYRead.TabIndex = 621;
            this.btnARbYRead.Text = "읽기";
            this.btnARbYRead.UseVisualStyleBackColor = false;
            this.btnARbYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblARightBotX
            // 
            this.lblARightBotX.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblARightBotX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblARightBotX.Location = new System.Drawing.Point(378, 368);
            this.lblARightBotX.Name = "lblARightBotX";
            this.lblARightBotX.Size = new System.Drawing.Size(70, 27);
            this.lblARightBotX.TabIndex = 615;
            this.lblARightBotX.Text = "RB X";
            this.lblARightBotX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtARightBotX
            // 
            this.txtARightBotX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtARightBotX.Location = new System.Drawing.Point(454, 368);
            this.txtARightBotX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtARightBotX.Name = "txtARightBotX";
            this.txtARightBotX.Size = new System.Drawing.Size(127, 27);
            this.txtARightBotX.TabIndex = 616;
            // 
            // btnARbXMove
            // 
            this.btnARbXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnARbXMove.ForeColor = System.Drawing.Color.Black;
            this.btnARbXMove.Location = new System.Drawing.Point(663, 368);
            this.btnARbXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARbXMove.Name = "btnARbXMove";
            this.btnARbXMove.Size = new System.Drawing.Size(70, 27);
            this.btnARbXMove.TabIndex = 618;
            this.btnARbXMove.Text = "이동";
            this.btnARbXMove.UseVisualStyleBackColor = false;
            this.btnARbXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnARbXRead
            // 
            this.btnARbXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnARbXRead.ForeColor = System.Drawing.Color.Black;
            this.btnARbXRead.Location = new System.Drawing.Point(587, 368);
            this.btnARbXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARbXRead.Name = "btnARbXRead";
            this.btnARbXRead.Size = new System.Drawing.Size(70, 27);
            this.btnARbXRead.TabIndex = 617;
            this.btnARbXRead.Text = "읽기";
            this.btnARbXRead.UseVisualStyleBackColor = false;
            this.btnARbXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblABotZ
            // 
            this.lblABotZ.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblABotZ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblABotZ.Location = new System.Drawing.Point(17, 429);
            this.lblABotZ.Name = "lblABotZ";
            this.lblABotZ.Size = new System.Drawing.Size(70, 27);
            this.lblABotZ.TabIndex = 611;
            this.lblABotZ.Text = "BOT Z";
            this.lblABotZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtABotZ
            // 
            this.txtABotZ.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtABotZ.Location = new System.Drawing.Point(93, 429);
            this.txtABotZ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtABotZ.Name = "txtABotZ";
            this.txtABotZ.Size = new System.Drawing.Size(127, 27);
            this.txtABotZ.TabIndex = 612;
            // 
            // btnABotZMove
            // 
            this.btnABotZMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnABotZMove.ForeColor = System.Drawing.Color.Black;
            this.btnABotZMove.Location = new System.Drawing.Point(302, 429);
            this.btnABotZMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnABotZMove.Name = "btnABotZMove";
            this.btnABotZMove.Size = new System.Drawing.Size(70, 27);
            this.btnABotZMove.TabIndex = 614;
            this.btnABotZMove.Text = "이동";
            this.btnABotZMove.UseVisualStyleBackColor = false;
            this.btnABotZMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnABotZRead
            // 
            this.btnABotZRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnABotZRead.ForeColor = System.Drawing.Color.Black;
            this.btnABotZRead.Location = new System.Drawing.Point(226, 429);
            this.btnABotZRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnABotZRead.Name = "btnABotZRead";
            this.btnABotZRead.Size = new System.Drawing.Size(70, 27);
            this.btnABotZRead.TabIndex = 613;
            this.btnABotZRead.Text = "읽기";
            this.btnABotZRead.UseVisualStyleBackColor = false;
            this.btnABotZRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblALeftBotY
            // 
            this.lblALeftBotY.BackColor = System.Drawing.Color.Red;
            this.lblALeftBotY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblALeftBotY.Location = new System.Drawing.Point(17, 398);
            this.lblALeftBotY.Name = "lblALeftBotY";
            this.lblALeftBotY.Size = new System.Drawing.Size(70, 27);
            this.lblALeftBotY.TabIndex = 607;
            this.lblALeftBotY.Text = "LB Y";
            this.lblALeftBotY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtALeftBotY
            // 
            this.txtALeftBotY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtALeftBotY.Location = new System.Drawing.Point(93, 398);
            this.txtALeftBotY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtALeftBotY.Name = "txtALeftBotY";
            this.txtALeftBotY.Size = new System.Drawing.Size(127, 27);
            this.txtALeftBotY.TabIndex = 608;
            // 
            // btnALbYMove
            // 
            this.btnALbYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnALbYMove.ForeColor = System.Drawing.Color.Black;
            this.btnALbYMove.Location = new System.Drawing.Point(302, 398);
            this.btnALbYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALbYMove.Name = "btnALbYMove";
            this.btnALbYMove.Size = new System.Drawing.Size(70, 27);
            this.btnALbYMove.TabIndex = 610;
            this.btnALbYMove.Text = "이동";
            this.btnALbYMove.UseVisualStyleBackColor = false;
            this.btnALbYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnALbYRead
            // 
            this.btnALbYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnALbYRead.ForeColor = System.Drawing.Color.Black;
            this.btnALbYRead.Location = new System.Drawing.Point(226, 398);
            this.btnALbYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALbYRead.Name = "btnALbYRead";
            this.btnALbYRead.Size = new System.Drawing.Size(70, 27);
            this.btnALbYRead.TabIndex = 609;
            this.btnALbYRead.Text = "읽기";
            this.btnALbYRead.UseVisualStyleBackColor = false;
            this.btnALbYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblALeftBotX
            // 
            this.lblALeftBotX.BackColor = System.Drawing.Color.Red;
            this.lblALeftBotX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblALeftBotX.Location = new System.Drawing.Point(17, 367);
            this.lblALeftBotX.Name = "lblALeftBotX";
            this.lblALeftBotX.Size = new System.Drawing.Size(70, 27);
            this.lblALeftBotX.TabIndex = 602;
            this.lblALeftBotX.Text = "LB X";
            this.lblALeftBotX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtALeftBotX
            // 
            this.txtALeftBotX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtALeftBotX.Location = new System.Drawing.Point(93, 367);
            this.txtALeftBotX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtALeftBotX.Name = "txtALeftBotX";
            this.txtALeftBotX.Size = new System.Drawing.Size(127, 27);
            this.txtALeftBotX.TabIndex = 603;
            // 
            // btnALbXMove
            // 
            this.btnALbXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnALbXMove.ForeColor = System.Drawing.Color.Black;
            this.btnALbXMove.Location = new System.Drawing.Point(302, 367);
            this.btnALbXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALbXMove.Name = "btnALbXMove";
            this.btnALbXMove.Size = new System.Drawing.Size(70, 27);
            this.btnALbXMove.TabIndex = 606;
            this.btnALbXMove.Text = "이동";
            this.btnALbXMove.UseVisualStyleBackColor = false;
            this.btnALbXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnALbXRead
            // 
            this.btnALbXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnALbXRead.ForeColor = System.Drawing.Color.Black;
            this.btnALbXRead.Location = new System.Drawing.Point(226, 367);
            this.btnALbXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALbXRead.Name = "btnALbXRead";
            this.btnALbXRead.Size = new System.Drawing.Size(70, 27);
            this.btnALbXRead.TabIndex = 604;
            this.btnALbXRead.Text = "읽기";
            this.btnALbXRead.UseVisualStyleBackColor = false;
            this.btnALbXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lblLb);
            this.panel4.Controls.Add(this.lblLt);
            this.panel4.ForeColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(93, 141);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(203, 219);
            this.panel4.TabIndex = 601;
            // 
            // lblLb
            // 
            this.lblLb.BackColor = System.Drawing.Color.Red;
            this.lblLb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLb.Location = new System.Drawing.Point(0, 191);
            this.lblLb.Name = "lblLb";
            this.lblLb.Size = new System.Drawing.Size(70, 27);
            this.lblLb.TabIndex = 460;
            this.lblLb.Text = "LB";
            this.lblLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLt
            // 
            this.lblLt.BackColor = System.Drawing.Color.Yellow;
            this.lblLt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLt.Location = new System.Drawing.Point(0, 0);
            this.lblLt.Name = "lblLt";
            this.lblLt.Size = new System.Drawing.Size(70, 27);
            this.lblLt.TabIndex = 459;
            this.lblLt.Text = "LT";
            this.lblLt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblATopZ
            // 
            this.lblATopZ.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblATopZ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblATopZ.Location = new System.Drawing.Point(17, 106);
            this.lblATopZ.Name = "lblATopZ";
            this.lblATopZ.Size = new System.Drawing.Size(70, 27);
            this.lblATopZ.TabIndex = 597;
            this.lblATopZ.Text = "TOP Z";
            this.lblATopZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtATopZ
            // 
            this.txtATopZ.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtATopZ.Location = new System.Drawing.Point(93, 106);
            this.txtATopZ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtATopZ.Name = "txtATopZ";
            this.txtATopZ.Size = new System.Drawing.Size(127, 27);
            this.txtATopZ.TabIndex = 598;
            // 
            // btnATopZMove
            // 
            this.btnATopZMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnATopZMove.ForeColor = System.Drawing.Color.Black;
            this.btnATopZMove.Location = new System.Drawing.Point(302, 106);
            this.btnATopZMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnATopZMove.Name = "btnATopZMove";
            this.btnATopZMove.Size = new System.Drawing.Size(70, 27);
            this.btnATopZMove.TabIndex = 600;
            this.btnATopZMove.Text = "이동";
            this.btnATopZMove.UseVisualStyleBackColor = false;
            this.btnATopZMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnATopZRead
            // 
            this.btnATopZRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnATopZRead.ForeColor = System.Drawing.Color.Black;
            this.btnATopZRead.Location = new System.Drawing.Point(226, 106);
            this.btnATopZRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnATopZRead.Name = "btnATopZRead";
            this.btnATopZRead.Size = new System.Drawing.Size(70, 27);
            this.btnATopZRead.TabIndex = 599;
            this.btnATopZRead.Text = "읽기";
            this.btnATopZRead.UseVisualStyleBackColor = false;
            this.btnATopZRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblARightTopY
            // 
            this.lblARightTopY.BackColor = System.Drawing.Color.GreenYellow;
            this.lblARightTopY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblARightTopY.Location = new System.Drawing.Point(378, 76);
            this.lblARightTopY.Name = "lblARightTopY";
            this.lblARightTopY.Size = new System.Drawing.Size(70, 27);
            this.lblARightTopY.TabIndex = 593;
            this.lblARightTopY.Text = "RT Y";
            this.lblARightTopY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtARightTopY
            // 
            this.txtARightTopY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtARightTopY.Location = new System.Drawing.Point(454, 76);
            this.txtARightTopY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtARightTopY.Name = "txtARightTopY";
            this.txtARightTopY.Size = new System.Drawing.Size(127, 27);
            this.txtARightTopY.TabIndex = 594;
            // 
            // btnARtYMove
            // 
            this.btnARtYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnARtYMove.ForeColor = System.Drawing.Color.Black;
            this.btnARtYMove.Location = new System.Drawing.Point(663, 76);
            this.btnARtYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARtYMove.Name = "btnARtYMove";
            this.btnARtYMove.Size = new System.Drawing.Size(70, 27);
            this.btnARtYMove.TabIndex = 596;
            this.btnARtYMove.Text = "이동";
            this.btnARtYMove.UseVisualStyleBackColor = false;
            this.btnARtYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnARtYRead
            // 
            this.btnARtYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnARtYRead.ForeColor = System.Drawing.Color.Black;
            this.btnARtYRead.Location = new System.Drawing.Point(587, 76);
            this.btnARtYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARtYRead.Name = "btnARtYRead";
            this.btnARtYRead.Size = new System.Drawing.Size(70, 27);
            this.btnARtYRead.TabIndex = 595;
            this.btnARtYRead.Text = "읽기";
            this.btnARtYRead.UseVisualStyleBackColor = false;
            this.btnARtYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblARightTopX
            // 
            this.lblARightTopX.BackColor = System.Drawing.Color.GreenYellow;
            this.lblARightTopX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblARightTopX.Location = new System.Drawing.Point(378, 45);
            this.lblARightTopX.Name = "lblARightTopX";
            this.lblARightTopX.Size = new System.Drawing.Size(70, 27);
            this.lblARightTopX.TabIndex = 589;
            this.lblARightTopX.Text = "RT X";
            this.lblARightTopX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtARightTopX
            // 
            this.txtARightTopX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtARightTopX.Location = new System.Drawing.Point(454, 45);
            this.txtARightTopX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtARightTopX.Name = "txtARightTopX";
            this.txtARightTopX.Size = new System.Drawing.Size(127, 27);
            this.txtARightTopX.TabIndex = 590;
            // 
            // btnARtXMove
            // 
            this.btnARtXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnARtXMove.ForeColor = System.Drawing.Color.Black;
            this.btnARtXMove.Location = new System.Drawing.Point(663, 45);
            this.btnARtXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARtXMove.Name = "btnARtXMove";
            this.btnARtXMove.Size = new System.Drawing.Size(70, 27);
            this.btnARtXMove.TabIndex = 592;
            this.btnARtXMove.Text = "이동";
            this.btnARtXMove.UseVisualStyleBackColor = false;
            this.btnARtXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnARtXRead
            // 
            this.btnARtXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnARtXRead.ForeColor = System.Drawing.Color.Black;
            this.btnARtXRead.Location = new System.Drawing.Point(587, 45);
            this.btnARtXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnARtXRead.Name = "btnARtXRead";
            this.btnARtXRead.Size = new System.Drawing.Size(70, 27);
            this.btnARtXRead.TabIndex = 591;
            this.btnARtXRead.Text = "읽기";
            this.btnARtXRead.UseVisualStyleBackColor = false;
            this.btnARtXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblALeftTopY
            // 
            this.lblALeftTopY.BackColor = System.Drawing.Color.Yellow;
            this.lblALeftTopY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblALeftTopY.Location = new System.Drawing.Point(17, 75);
            this.lblALeftTopY.Name = "lblALeftTopY";
            this.lblALeftTopY.Size = new System.Drawing.Size(70, 27);
            this.lblALeftTopY.TabIndex = 585;
            this.lblALeftTopY.Text = "LT Y";
            this.lblALeftTopY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtALeftTopY
            // 
            this.txtALeftTopY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtALeftTopY.Location = new System.Drawing.Point(93, 75);
            this.txtALeftTopY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtALeftTopY.Name = "txtALeftTopY";
            this.txtALeftTopY.Size = new System.Drawing.Size(127, 27);
            this.txtALeftTopY.TabIndex = 586;
            // 
            // btnALtYMove
            // 
            this.btnALtYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnALtYMove.ForeColor = System.Drawing.Color.Black;
            this.btnALtYMove.Location = new System.Drawing.Point(302, 75);
            this.btnALtYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALtYMove.Name = "btnALtYMove";
            this.btnALtYMove.Size = new System.Drawing.Size(70, 27);
            this.btnALtYMove.TabIndex = 588;
            this.btnALtYMove.Text = "이동";
            this.btnALtYMove.UseVisualStyleBackColor = false;
            this.btnALtYMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnALtYRead
            // 
            this.btnALtYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnALtYRead.ForeColor = System.Drawing.Color.Black;
            this.btnALtYRead.Location = new System.Drawing.Point(226, 75);
            this.btnALtYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALtYRead.Name = "btnALtYRead";
            this.btnALtYRead.Size = new System.Drawing.Size(70, 27);
            this.btnALtYRead.TabIndex = 587;
            this.btnALtYRead.Text = "읽기";
            this.btnALtYRead.UseVisualStyleBackColor = false;
            this.btnALtYRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblALeftTopX
            // 
            this.lblALeftTopX.BackColor = System.Drawing.Color.Yellow;
            this.lblALeftTopX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblALeftTopX.Location = new System.Drawing.Point(17, 44);
            this.lblALeftTopX.Name = "lblALeftTopX";
            this.lblALeftTopX.Size = new System.Drawing.Size(70, 27);
            this.lblALeftTopX.TabIndex = 581;
            this.lblALeftTopX.Text = "LT X";
            this.lblALeftTopX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtALeftTopX
            // 
            this.txtALeftTopX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtALeftTopX.Location = new System.Drawing.Point(93, 44);
            this.txtALeftTopX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtALeftTopX.Name = "txtALeftTopX";
            this.txtALeftTopX.Size = new System.Drawing.Size(127, 27);
            this.txtALeftTopX.TabIndex = 582;
            // 
            // btnALtXMove
            // 
            this.btnALtXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnALtXMove.ForeColor = System.Drawing.Color.Black;
            this.btnALtXMove.Location = new System.Drawing.Point(302, 44);
            this.btnALtXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALtXMove.Name = "btnALtXMove";
            this.btnALtXMove.Size = new System.Drawing.Size(70, 27);
            this.btnALtXMove.TabIndex = 584;
            this.btnALtXMove.Text = "이동";
            this.btnALtXMove.UseVisualStyleBackColor = false;
            this.btnALtXMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnALtXRead
            // 
            this.btnALtXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnALtXRead.ForeColor = System.Drawing.Color.Black;
            this.btnALtXRead.Location = new System.Drawing.Point(226, 44);
            this.btnALtXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnALtXRead.Name = "btnALtXRead";
            this.btnALtXRead.Size = new System.Drawing.Size(70, 27);
            this.btnALtXRead.TabIndex = 583;
            this.btnALtXRead.Text = "읽기";
            this.btnALtXRead.UseVisualStyleBackColor = false;
            this.btnALtXRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(1238, 522);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(135, 45);
            this.btnSave.TabIndex = 475;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmInspPosition
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1524, 576);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmInspPosition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "검사 포지션";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label lblBInspPosition;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblBMoveSpeed;
        private System.Windows.Forms.TextBox txtBMoveSpeed;
        private System.Windows.Forms.Label lblBYSpeed;
        private System.Windows.Forms.TextBox txtBYSpeed;
        private System.Windows.Forms.Label lblBXSpeed;
        private System.Windows.Forms.TextBox txtBXSpeed;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblBRightBotY;
        private System.Windows.Forms.TextBox txtBRightBotY;
        private System.Windows.Forms.Button btnBRbYMove;
        private System.Windows.Forms.Button btnBRbYRead;
        private System.Windows.Forms.Label lblBRightBotX;
        private System.Windows.Forms.TextBox txtBRightBotX;
        private System.Windows.Forms.Button btnBRbXMove;
        private System.Windows.Forms.Button btnBRbXRead;
        private System.Windows.Forms.Label lblBBotZ;
        private System.Windows.Forms.TextBox txtBBotZ;
        private System.Windows.Forms.Button btnBBotZMove;
        private System.Windows.Forms.Button btnBBotZRead;
        private System.Windows.Forms.Label lblBLeftBotY;
        private System.Windows.Forms.TextBox txtBLeftBotY;
        private System.Windows.Forms.Button btnBLbYMove;
        private System.Windows.Forms.Button btnBLbYRead;
        private System.Windows.Forms.Label lblBLeftBotX;
        private System.Windows.Forms.TextBox txtBLeftBotX;
        private System.Windows.Forms.Button btnBLbXMove;
        private System.Windows.Forms.Button btnBLbXRead;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblBTopZ;
        private System.Windows.Forms.TextBox txtBTopZ;
        private System.Windows.Forms.Button btnBTopZMove;
        private System.Windows.Forms.Button btnBTopZRead;
        private System.Windows.Forms.Label lblBRightTopY;
        private System.Windows.Forms.TextBox txtBRightTopY;
        private System.Windows.Forms.Button btnBRtYMove;
        private System.Windows.Forms.Button btnBRtYRead;
        private System.Windows.Forms.Label lblBRrightTopX;
        private System.Windows.Forms.TextBox txtBRightTopX;
        private System.Windows.Forms.Button btnBRtXMove;
        private System.Windows.Forms.Button btnBRtXRead;
        private System.Windows.Forms.Label lblBLeftTopY;
        private System.Windows.Forms.TextBox txtBLeftTopY;
        private System.Windows.Forms.Button btnBLtYMove;
        private System.Windows.Forms.Button btnBLtYRead;
        private System.Windows.Forms.Label lblBLeftTopX;
        private System.Windows.Forms.TextBox txtBLeftTopX;
        private System.Windows.Forms.Button btnBLtXMove;
        private System.Windows.Forms.Button btnBLtXRead;
        internal System.Windows.Forms.Label lblAInspPosition;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblAMoveSpeed;
        private System.Windows.Forms.TextBox txtAMoveSpeed;
        private System.Windows.Forms.Label lblAYSpeed;
        private System.Windows.Forms.TextBox txtAYSpeed;
        private System.Windows.Forms.Label lblAXSpeed;
        private System.Windows.Forms.TextBox txtAXSpeed;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblRb;
        private System.Windows.Forms.Label lblRt;
        private System.Windows.Forms.Label lblARightBotY;
        private System.Windows.Forms.TextBox txtARightBotY;
        private System.Windows.Forms.Button btnARbYMove;
        private System.Windows.Forms.Button btnARbYRead;
        private System.Windows.Forms.Label lblARightBotX;
        private System.Windows.Forms.TextBox txtARightBotX;
        private System.Windows.Forms.Button btnARbXMove;
        private System.Windows.Forms.Button btnARbXRead;
        private System.Windows.Forms.Label lblABotZ;
        private System.Windows.Forms.TextBox txtABotZ;
        private System.Windows.Forms.Button btnABotZMove;
        private System.Windows.Forms.Button btnABotZRead;
        private System.Windows.Forms.Label lblALeftBotY;
        private System.Windows.Forms.TextBox txtALeftBotY;
        private System.Windows.Forms.Button btnALbYMove;
        private System.Windows.Forms.Button btnALbYRead;
        private System.Windows.Forms.Label lblALeftBotX;
        private System.Windows.Forms.TextBox txtALeftBotX;
        private System.Windows.Forms.Button btnALbXMove;
        private System.Windows.Forms.Button btnALbXRead;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblLb;
        private System.Windows.Forms.Label lblLt;
        private System.Windows.Forms.Label lblATopZ;
        private System.Windows.Forms.TextBox txtATopZ;
        private System.Windows.Forms.Button btnATopZMove;
        private System.Windows.Forms.Button btnATopZRead;
        private System.Windows.Forms.Label lblARightTopY;
        private System.Windows.Forms.TextBox txtARightTopY;
        private System.Windows.Forms.Button btnARtYMove;
        private System.Windows.Forms.Button btnARtYRead;
        private System.Windows.Forms.Label lblARightTopX;
        private System.Windows.Forms.TextBox txtARightTopX;
        private System.Windows.Forms.Button btnARtXMove;
        private System.Windows.Forms.Button btnARtXRead;
        private System.Windows.Forms.Label lblALeftTopY;
        private System.Windows.Forms.TextBox txtALeftTopY;
        private System.Windows.Forms.Button btnALtYMove;
        private System.Windows.Forms.Button btnALtYRead;
        private System.Windows.Forms.Label lblALeftTopX;
        private System.Windows.Forms.TextBox txtALeftTopX;
        private System.Windows.Forms.Button btnALtXMove;
        private System.Windows.Forms.Button btnALtXRead;
        private System.Windows.Forms.Button btnSave;
    }
}
﻿namespace DIT.TLC.UI
{
    partial class UcrlInspPositionControl
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbLtX = new System.Windows.Forms.Label();
            this.txtLtX = new System.Windows.Forms.TextBox();
            this.btnLtXMove = new System.Windows.Forms.Button();
            this.btnLtXRead = new System.Windows.Forms.Button();
            this.lblLtY = new System.Windows.Forms.Label();
            this.txtLtY = new System.Windows.Forms.TextBox();
            this.btnLtYMove = new System.Windows.Forms.Button();
            this.btnLtYRead = new System.Windows.Forms.Button();
            this.lblRtY = new System.Windows.Forms.Label();
            this.txtRtY = new System.Windows.Forms.TextBox();
            this.btnRtYMove = new System.Windows.Forms.Button();
            this.btnRtYRead = new System.Windows.Forms.Button();
            this.lblRtX = new System.Windows.Forms.Label();
            this.txtRtX = new System.Windows.Forms.TextBox();
            this.btnRtXMove = new System.Windows.Forms.Button();
            this.btnRtXRead = new System.Windows.Forms.Button();
            this.lblTopZ = new System.Windows.Forms.Label();
            this.txtTopZ = new System.Windows.Forms.TextBox();
            this.btnTopZMove = new System.Windows.Forms.Button();
            this.btnTopZRead = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblLt = new System.Windows.Forms.Label();
            this.lblBotZ = new System.Windows.Forms.Label();
            this.txtBotZ = new System.Windows.Forms.TextBox();
            this.btnBotZMove = new System.Windows.Forms.Button();
            this.btnBotZRead = new System.Windows.Forms.Button();
            this.lblLbY = new System.Windows.Forms.Label();
            this.txtLbY = new System.Windows.Forms.TextBox();
            this.btnLbYMove = new System.Windows.Forms.Button();
            this.btnLbYRead = new System.Windows.Forms.Button();
            this.lblLbX = new System.Windows.Forms.Label();
            this.txtLbX = new System.Windows.Forms.TextBox();
            this.btnLbXMove = new System.Windows.Forms.Button();
            this.btnLbXRead = new System.Windows.Forms.Button();
            this.lblLb = new System.Windows.Forms.Label();
            this.lblRbY = new System.Windows.Forms.Label();
            this.txtRbY = new System.Windows.Forms.TextBox();
            this.btnRbYMove = new System.Windows.Forms.Button();
            this.btnRbYRead = new System.Windows.Forms.Button();
            this.lblRbX = new System.Windows.Forms.Label();
            this.txtRbX = new System.Windows.Forms.TextBox();
            this.btnRbXMove = new System.Windows.Forms.Button();
            this.btnRbXRead = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRb = new System.Windows.Forms.Label();
            this.lblRt = new System.Windows.Forms.Label();
            this.lblXSpeed = new System.Windows.Forms.Label();
            this.txtXSpeed = new System.Windows.Forms.TextBox();
            this.lblYSpeed = new System.Windows.Forms.Label();
            this.txtYSpeed = new System.Windows.Forms.TextBox();
            this.lblMoveSpeed = new System.Windows.Forms.Label();
            this.txtMoveSpeed = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbLtX
            // 
            this.lbLtX.BackColor = System.Drawing.Color.Yellow;
            this.lbLtX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbLtX.Location = new System.Drawing.Point(10, 10);
            this.lbLtX.Name = "lbLtX";
            this.lbLtX.Size = new System.Drawing.Size(70, 27);
            this.lbLtX.TabIndex = 152;
            this.lbLtX.Text = "LT X";
            this.lbLtX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLtX
            // 
            this.txtLtX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtLtX.Location = new System.Drawing.Point(86, 10);
            this.txtLtX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLtX.Name = "txtLtX";
            this.txtLtX.Size = new System.Drawing.Size(127, 27);
            this.txtLtX.TabIndex = 153;
            // 
            // btnLtXMove
            // 
            this.btnLtXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnLtXMove.ForeColor = System.Drawing.Color.Black;
            this.btnLtXMove.Location = new System.Drawing.Point(295, 10);
            this.btnLtXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLtXMove.Name = "btnLtXMove";
            this.btnLtXMove.Size = new System.Drawing.Size(70, 27);
            this.btnLtXMove.TabIndex = 156;
            this.btnLtXMove.Text = "이동";
            this.btnLtXMove.UseVisualStyleBackColor = false;
            // 
            // btnLtXRead
            // 
            this.btnLtXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnLtXRead.ForeColor = System.Drawing.Color.Black;
            this.btnLtXRead.Location = new System.Drawing.Point(219, 10);
            this.btnLtXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLtXRead.Name = "btnLtXRead";
            this.btnLtXRead.Size = new System.Drawing.Size(70, 27);
            this.btnLtXRead.TabIndex = 155;
            this.btnLtXRead.Text = "읽기";
            this.btnLtXRead.UseVisualStyleBackColor = false;
            // 
            // lblLtY
            // 
            this.lblLtY.BackColor = System.Drawing.Color.Yellow;
            this.lblLtY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLtY.Location = new System.Drawing.Point(10, 41);
            this.lblLtY.Name = "lblLtY";
            this.lblLtY.Size = new System.Drawing.Size(70, 27);
            this.lblLtY.TabIndex = 157;
            this.lblLtY.Text = "LT Y";
            this.lblLtY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLtY
            // 
            this.txtLtY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtLtY.Location = new System.Drawing.Point(86, 41);
            this.txtLtY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLtY.Name = "txtLtY";
            this.txtLtY.Size = new System.Drawing.Size(127, 27);
            this.txtLtY.TabIndex = 158;
            // 
            // btnLtYMove
            // 
            this.btnLtYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnLtYMove.ForeColor = System.Drawing.Color.Black;
            this.btnLtYMove.Location = new System.Drawing.Point(295, 41);
            this.btnLtYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLtYMove.Name = "btnLtYMove";
            this.btnLtYMove.Size = new System.Drawing.Size(70, 27);
            this.btnLtYMove.TabIndex = 160;
            this.btnLtYMove.Text = "이동";
            this.btnLtYMove.UseVisualStyleBackColor = false;
            // 
            // btnLtYRead
            // 
            this.btnLtYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnLtYRead.ForeColor = System.Drawing.Color.Black;
            this.btnLtYRead.Location = new System.Drawing.Point(219, 41);
            this.btnLtYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLtYRead.Name = "btnLtYRead";
            this.btnLtYRead.Size = new System.Drawing.Size(70, 27);
            this.btnLtYRead.TabIndex = 159;
            this.btnLtYRead.Text = "읽기";
            this.btnLtYRead.UseVisualStyleBackColor = false;
            // 
            // lblRtY
            // 
            this.lblRtY.BackColor = System.Drawing.Color.GreenYellow;
            this.lblRtY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRtY.Location = new System.Drawing.Point(371, 42);
            this.lblRtY.Name = "lblRtY";
            this.lblRtY.Size = new System.Drawing.Size(70, 27);
            this.lblRtY.TabIndex = 165;
            this.lblRtY.Text = "RT Y";
            this.lblRtY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRtY
            // 
            this.txtRtY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtRtY.Location = new System.Drawing.Point(447, 42);
            this.txtRtY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRtY.Name = "txtRtY";
            this.txtRtY.Size = new System.Drawing.Size(127, 27);
            this.txtRtY.TabIndex = 166;
            // 
            // btnRtYMove
            // 
            this.btnRtYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnRtYMove.ForeColor = System.Drawing.Color.Black;
            this.btnRtYMove.Location = new System.Drawing.Point(656, 42);
            this.btnRtYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRtYMove.Name = "btnRtYMove";
            this.btnRtYMove.Size = new System.Drawing.Size(70, 27);
            this.btnRtYMove.TabIndex = 168;
            this.btnRtYMove.Text = "이동";
            this.btnRtYMove.UseVisualStyleBackColor = false;
            // 
            // btnRtYRead
            // 
            this.btnRtYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnRtYRead.ForeColor = System.Drawing.Color.Black;
            this.btnRtYRead.Location = new System.Drawing.Point(580, 42);
            this.btnRtYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRtYRead.Name = "btnRtYRead";
            this.btnRtYRead.Size = new System.Drawing.Size(70, 27);
            this.btnRtYRead.TabIndex = 167;
            this.btnRtYRead.Text = "읽기";
            this.btnRtYRead.UseVisualStyleBackColor = false;
            // 
            // lblRtX
            // 
            this.lblRtX.BackColor = System.Drawing.Color.GreenYellow;
            this.lblRtX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRtX.Location = new System.Drawing.Point(371, 11);
            this.lblRtX.Name = "lblRtX";
            this.lblRtX.Size = new System.Drawing.Size(70, 27);
            this.lblRtX.TabIndex = 161;
            this.lblRtX.Text = "RT X";
            this.lblRtX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRtX
            // 
            this.txtRtX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtRtX.Location = new System.Drawing.Point(447, 11);
            this.txtRtX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRtX.Name = "txtRtX";
            this.txtRtX.Size = new System.Drawing.Size(127, 27);
            this.txtRtX.TabIndex = 162;
            // 
            // btnRtXMove
            // 
            this.btnRtXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnRtXMove.ForeColor = System.Drawing.Color.Black;
            this.btnRtXMove.Location = new System.Drawing.Point(656, 11);
            this.btnRtXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRtXMove.Name = "btnRtXMove";
            this.btnRtXMove.Size = new System.Drawing.Size(70, 27);
            this.btnRtXMove.TabIndex = 164;
            this.btnRtXMove.Text = "이동";
            this.btnRtXMove.UseVisualStyleBackColor = false;
            // 
            // btnRtXRead
            // 
            this.btnRtXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnRtXRead.ForeColor = System.Drawing.Color.Black;
            this.btnRtXRead.Location = new System.Drawing.Point(580, 11);
            this.btnRtXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRtXRead.Name = "btnRtXRead";
            this.btnRtXRead.Size = new System.Drawing.Size(70, 27);
            this.btnRtXRead.TabIndex = 163;
            this.btnRtXRead.Text = "읽기";
            this.btnRtXRead.UseVisualStyleBackColor = false;
            // 
            // lblTopZ
            // 
            this.lblTopZ.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblTopZ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTopZ.Location = new System.Drawing.Point(10, 72);
            this.lblTopZ.Name = "lblTopZ";
            this.lblTopZ.Size = new System.Drawing.Size(70, 27);
            this.lblTopZ.TabIndex = 169;
            this.lblTopZ.Text = "TOP Z";
            this.lblTopZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTopZ
            // 
            this.txtTopZ.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtTopZ.Location = new System.Drawing.Point(86, 72);
            this.txtTopZ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTopZ.Name = "txtTopZ";
            this.txtTopZ.Size = new System.Drawing.Size(127, 27);
            this.txtTopZ.TabIndex = 170;
            // 
            // btnTopZMove
            // 
            this.btnTopZMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnTopZMove.ForeColor = System.Drawing.Color.Black;
            this.btnTopZMove.Location = new System.Drawing.Point(295, 72);
            this.btnTopZMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTopZMove.Name = "btnTopZMove";
            this.btnTopZMove.Size = new System.Drawing.Size(70, 27);
            this.btnTopZMove.TabIndex = 172;
            this.btnTopZMove.Text = "이동";
            this.btnTopZMove.UseVisualStyleBackColor = false;
            // 
            // btnTopZRead
            // 
            this.btnTopZRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnTopZRead.ForeColor = System.Drawing.Color.Black;
            this.btnTopZRead.Location = new System.Drawing.Point(219, 72);
            this.btnTopZRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTopZRead.Name = "btnTopZRead";
            this.btnTopZRead.Size = new System.Drawing.Size(70, 27);
            this.btnTopZRead.TabIndex = 171;
            this.btnTopZRead.Text = "읽기";
            this.btnTopZRead.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblLb);
            this.panel2.Controls.Add(this.lblLt);
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(86, 107);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(203, 219);
            this.panel2.TabIndex = 458;
            // 
            // lblLt
            // 
            this.lblLt.BackColor = System.Drawing.Color.Yellow;
            this.lblLt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLt.Location = new System.Drawing.Point(0, 0);
            this.lblLt.Name = "lblLt";
            this.lblLt.Size = new System.Drawing.Size(70, 27);
            this.lblLt.TabIndex = 459;
            this.lblLt.Text = "LT";
            this.lblLt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBotZ
            // 
            this.lblBotZ.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblBotZ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBotZ.Location = new System.Drawing.Point(10, 395);
            this.lblBotZ.Name = "lblBotZ";
            this.lblBotZ.Size = new System.Drawing.Size(70, 27);
            this.lblBotZ.TabIndex = 467;
            this.lblBotZ.Text = "BOT Z";
            this.lblBotZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBotZ
            // 
            this.txtBotZ.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtBotZ.Location = new System.Drawing.Point(86, 395);
            this.txtBotZ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBotZ.Name = "txtBotZ";
            this.txtBotZ.Size = new System.Drawing.Size(127, 27);
            this.txtBotZ.TabIndex = 468;
            // 
            // btnBotZMove
            // 
            this.btnBotZMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBotZMove.ForeColor = System.Drawing.Color.Black;
            this.btnBotZMove.Location = new System.Drawing.Point(295, 395);
            this.btnBotZMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBotZMove.Name = "btnBotZMove";
            this.btnBotZMove.Size = new System.Drawing.Size(70, 27);
            this.btnBotZMove.TabIndex = 470;
            this.btnBotZMove.Text = "이동";
            this.btnBotZMove.UseVisualStyleBackColor = false;
            // 
            // btnBotZRead
            // 
            this.btnBotZRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBotZRead.ForeColor = System.Drawing.Color.Black;
            this.btnBotZRead.Location = new System.Drawing.Point(219, 395);
            this.btnBotZRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBotZRead.Name = "btnBotZRead";
            this.btnBotZRead.Size = new System.Drawing.Size(70, 27);
            this.btnBotZRead.TabIndex = 469;
            this.btnBotZRead.Text = "읽기";
            this.btnBotZRead.UseVisualStyleBackColor = false;
            // 
            // lblLbY
            // 
            this.lblLbY.BackColor = System.Drawing.Color.Red;
            this.lblLbY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLbY.Location = new System.Drawing.Point(10, 364);
            this.lblLbY.Name = "lblLbY";
            this.lblLbY.Size = new System.Drawing.Size(70, 27);
            this.lblLbY.TabIndex = 463;
            this.lblLbY.Text = "LB Y";
            this.lblLbY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLbY
            // 
            this.txtLbY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtLbY.Location = new System.Drawing.Point(86, 364);
            this.txtLbY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLbY.Name = "txtLbY";
            this.txtLbY.Size = new System.Drawing.Size(127, 27);
            this.txtLbY.TabIndex = 464;
            // 
            // btnLbYMove
            // 
            this.btnLbYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnLbYMove.ForeColor = System.Drawing.Color.Black;
            this.btnLbYMove.Location = new System.Drawing.Point(295, 364);
            this.btnLbYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLbYMove.Name = "btnLbYMove";
            this.btnLbYMove.Size = new System.Drawing.Size(70, 27);
            this.btnLbYMove.TabIndex = 466;
            this.btnLbYMove.Text = "이동";
            this.btnLbYMove.UseVisualStyleBackColor = false;
            // 
            // btnLbYRead
            // 
            this.btnLbYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnLbYRead.ForeColor = System.Drawing.Color.Black;
            this.btnLbYRead.Location = new System.Drawing.Point(219, 364);
            this.btnLbYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLbYRead.Name = "btnLbYRead";
            this.btnLbYRead.Size = new System.Drawing.Size(70, 27);
            this.btnLbYRead.TabIndex = 465;
            this.btnLbYRead.Text = "읽기";
            this.btnLbYRead.UseVisualStyleBackColor = false;
            // 
            // lblLbX
            // 
            this.lblLbX.BackColor = System.Drawing.Color.Red;
            this.lblLbX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLbX.Location = new System.Drawing.Point(10, 333);
            this.lblLbX.Name = "lblLbX";
            this.lblLbX.Size = new System.Drawing.Size(70, 27);
            this.lblLbX.TabIndex = 459;
            this.lblLbX.Text = "LB X";
            this.lblLbX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLbX
            // 
            this.txtLbX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtLbX.Location = new System.Drawing.Point(86, 333);
            this.txtLbX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLbX.Name = "txtLbX";
            this.txtLbX.Size = new System.Drawing.Size(127, 27);
            this.txtLbX.TabIndex = 460;
            // 
            // btnLbXMove
            // 
            this.btnLbXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnLbXMove.ForeColor = System.Drawing.Color.Black;
            this.btnLbXMove.Location = new System.Drawing.Point(295, 333);
            this.btnLbXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLbXMove.Name = "btnLbXMove";
            this.btnLbXMove.Size = new System.Drawing.Size(70, 27);
            this.btnLbXMove.TabIndex = 462;
            this.btnLbXMove.Text = "이동";
            this.btnLbXMove.UseVisualStyleBackColor = false;
            // 
            // btnLbXRead
            // 
            this.btnLbXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnLbXRead.ForeColor = System.Drawing.Color.Black;
            this.btnLbXRead.Location = new System.Drawing.Point(219, 333);
            this.btnLbXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLbXRead.Name = "btnLbXRead";
            this.btnLbXRead.Size = new System.Drawing.Size(70, 27);
            this.btnLbXRead.TabIndex = 461;
            this.btnLbXRead.Text = "읽기";
            this.btnLbXRead.UseVisualStyleBackColor = false;
            // 
            // lblLb
            // 
            this.lblLb.BackColor = System.Drawing.Color.Red;
            this.lblLb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLb.Location = new System.Drawing.Point(0, 191);
            this.lblLb.Name = "lblLb";
            this.lblLb.Size = new System.Drawing.Size(70, 27);
            this.lblLb.TabIndex = 460;
            this.lblLb.Text = "LB";
            this.lblLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRbY
            // 
            this.lblRbY.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblRbY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRbY.Location = new System.Drawing.Point(371, 365);
            this.lblRbY.Name = "lblRbY";
            this.lblRbY.Size = new System.Drawing.Size(70, 27);
            this.lblRbY.TabIndex = 475;
            this.lblRbY.Text = "RB Y";
            this.lblRbY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRbY
            // 
            this.txtRbY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtRbY.Location = new System.Drawing.Point(447, 365);
            this.txtRbY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRbY.Name = "txtRbY";
            this.txtRbY.Size = new System.Drawing.Size(127, 27);
            this.txtRbY.TabIndex = 476;
            // 
            // btnRbYMove
            // 
            this.btnRbYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnRbYMove.ForeColor = System.Drawing.Color.Black;
            this.btnRbYMove.Location = new System.Drawing.Point(656, 365);
            this.btnRbYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRbYMove.Name = "btnRbYMove";
            this.btnRbYMove.Size = new System.Drawing.Size(70, 27);
            this.btnRbYMove.TabIndex = 478;
            this.btnRbYMove.Text = "이동";
            this.btnRbYMove.UseVisualStyleBackColor = false;
            // 
            // btnRbYRead
            // 
            this.btnRbYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnRbYRead.ForeColor = System.Drawing.Color.Black;
            this.btnRbYRead.Location = new System.Drawing.Point(580, 365);
            this.btnRbYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRbYRead.Name = "btnRbYRead";
            this.btnRbYRead.Size = new System.Drawing.Size(70, 27);
            this.btnRbYRead.TabIndex = 477;
            this.btnRbYRead.Text = "읽기";
            this.btnRbYRead.UseVisualStyleBackColor = false;
            // 
            // lblRbX
            // 
            this.lblRbX.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblRbX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRbX.Location = new System.Drawing.Point(371, 334);
            this.lblRbX.Name = "lblRbX";
            this.lblRbX.Size = new System.Drawing.Size(70, 27);
            this.lblRbX.TabIndex = 471;
            this.lblRbX.Text = "RB X";
            this.lblRbX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRbX
            // 
            this.txtRbX.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtRbX.Location = new System.Drawing.Point(447, 334);
            this.txtRbX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRbX.Name = "txtRbX";
            this.txtRbX.Size = new System.Drawing.Size(127, 27);
            this.txtRbX.TabIndex = 472;
            // 
            // btnRbXMove
            // 
            this.btnRbXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnRbXMove.ForeColor = System.Drawing.Color.Black;
            this.btnRbXMove.Location = new System.Drawing.Point(656, 334);
            this.btnRbXMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRbXMove.Name = "btnRbXMove";
            this.btnRbXMove.Size = new System.Drawing.Size(70, 27);
            this.btnRbXMove.TabIndex = 474;
            this.btnRbXMove.Text = "이동";
            this.btnRbXMove.UseVisualStyleBackColor = false;
            // 
            // btnRbXRead
            // 
            this.btnRbXRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnRbXRead.ForeColor = System.Drawing.Color.Black;
            this.btnRbXRead.Location = new System.Drawing.Point(580, 334);
            this.btnRbXRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRbXRead.Name = "btnRbXRead";
            this.btnRbXRead.Size = new System.Drawing.Size(70, 27);
            this.btnRbXRead.TabIndex = 473;
            this.btnRbXRead.Text = "읽기";
            this.btnRbXRead.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblRb);
            this.panel1.Controls.Add(this.lblRt);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(371, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 219);
            this.panel1.TabIndex = 461;
            // 
            // lblRb
            // 
            this.lblRb.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblRb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRb.Location = new System.Drawing.Point(132, 191);
            this.lblRb.Name = "lblRb";
            this.lblRb.Size = new System.Drawing.Size(70, 27);
            this.lblRb.TabIndex = 460;
            this.lblRb.Text = "RB";
            this.lblRb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRt
            // 
            this.lblRt.BackColor = System.Drawing.Color.GreenYellow;
            this.lblRt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRt.Location = new System.Drawing.Point(132, 0);
            this.lblRt.Name = "lblRt";
            this.lblRt.Size = new System.Drawing.Size(70, 27);
            this.lblRt.TabIndex = 459;
            this.lblRt.Text = "RT";
            this.lblRt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblXSpeed
            // 
            this.lblXSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblXSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblXSpeed.Location = new System.Drawing.Point(10, 426);
            this.lblXSpeed.Name = "lblXSpeed";
            this.lblXSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblXSpeed.TabIndex = 479;
            this.lblXSpeed.Text = "X Speed";
            this.lblXSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtXSpeed
            // 
            this.txtXSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtXSpeed.Location = new System.Drawing.Point(86, 426);
            this.txtXSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtXSpeed.Name = "txtXSpeed";
            this.txtXSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtXSpeed.TabIndex = 480;
            // 
            // lblYSpeed
            // 
            this.lblYSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblYSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYSpeed.Location = new System.Drawing.Point(266, 425);
            this.lblYSpeed.Name = "lblYSpeed";
            this.lblYSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblYSpeed.TabIndex = 481;
            this.lblYSpeed.Text = "Y Speed";
            this.lblYSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtYSpeed
            // 
            this.txtYSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtYSpeed.Location = new System.Drawing.Point(342, 425);
            this.txtYSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtYSpeed.Name = "txtYSpeed";
            this.txtYSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtYSpeed.TabIndex = 482;
            // 
            // lblMoveSpeed
            // 
            this.lblMoveSpeed.BackColor = System.Drawing.SystemColors.Control;
            this.lblMoveSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMoveSpeed.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMoveSpeed.Location = new System.Drawing.Point(523, 425);
            this.lblMoveSpeed.Name = "lblMoveSpeed";
            this.lblMoveSpeed.Size = new System.Drawing.Size(70, 27);
            this.lblMoveSpeed.TabIndex = 483;
            this.lblMoveSpeed.Text = "Move Speed";
            this.lblMoveSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMoveSpeed
            // 
            this.txtMoveSpeed.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtMoveSpeed.Location = new System.Drawing.Point(599, 425);
            this.txtMoveSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMoveSpeed.Name = "txtMoveSpeed";
            this.txtMoveSpeed.Size = new System.Drawing.Size(127, 27);
            this.txtMoveSpeed.TabIndex = 484;
            // 
            // UcrlInspPositionControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblMoveSpeed);
            this.Controls.Add(this.txtMoveSpeed);
            this.Controls.Add(this.lblYSpeed);
            this.Controls.Add(this.txtYSpeed);
            this.Controls.Add(this.lblXSpeed);
            this.Controls.Add(this.txtXSpeed);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblRbY);
            this.Controls.Add(this.txtRbY);
            this.Controls.Add(this.btnRbYMove);
            this.Controls.Add(this.btnRbYRead);
            this.Controls.Add(this.lblRbX);
            this.Controls.Add(this.txtRbX);
            this.Controls.Add(this.btnRbXMove);
            this.Controls.Add(this.btnRbXRead);
            this.Controls.Add(this.lblBotZ);
            this.Controls.Add(this.txtBotZ);
            this.Controls.Add(this.btnBotZMove);
            this.Controls.Add(this.btnBotZRead);
            this.Controls.Add(this.lblLbY);
            this.Controls.Add(this.txtLbY);
            this.Controls.Add(this.btnLbYMove);
            this.Controls.Add(this.btnLbYRead);
            this.Controls.Add(this.lblLbX);
            this.Controls.Add(this.txtLbX);
            this.Controls.Add(this.btnLbXMove);
            this.Controls.Add(this.btnLbXRead);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblTopZ);
            this.Controls.Add(this.txtTopZ);
            this.Controls.Add(this.btnTopZMove);
            this.Controls.Add(this.btnTopZRead);
            this.Controls.Add(this.lblRtY);
            this.Controls.Add(this.txtRtY);
            this.Controls.Add(this.btnRtYMove);
            this.Controls.Add(this.btnRtYRead);
            this.Controls.Add(this.lblRtX);
            this.Controls.Add(this.txtRtX);
            this.Controls.Add(this.btnRtXMove);
            this.Controls.Add(this.btnRtXRead);
            this.Controls.Add(this.lblLtY);
            this.Controls.Add(this.txtLtY);
            this.Controls.Add(this.btnLtYMove);
            this.Controls.Add(this.btnLtYRead);
            this.Controls.Add(this.lbLtX);
            this.Controls.Add(this.txtLtX);
            this.Controls.Add(this.btnLtXMove);
            this.Controls.Add(this.btnLtXRead);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlInspPositionControl";
            this.Size = new System.Drawing.Size(737, 462);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbLtX;
        private System.Windows.Forms.TextBox txtLtX;
        private System.Windows.Forms.Button btnLtXMove;
        private System.Windows.Forms.Button btnLtXRead;
        private System.Windows.Forms.Label lblLtY;
        private System.Windows.Forms.TextBox txtLtY;
        private System.Windows.Forms.Button btnLtYMove;
        private System.Windows.Forms.Button btnLtYRead;
        private System.Windows.Forms.Label lblRtY;
        private System.Windows.Forms.TextBox txtRtY;
        private System.Windows.Forms.Button btnRtYMove;
        private System.Windows.Forms.Button btnRtYRead;
        private System.Windows.Forms.Label lblRtX;
        private System.Windows.Forms.TextBox txtRtX;
        private System.Windows.Forms.Button btnRtXMove;
        private System.Windows.Forms.Button btnRtXRead;
        private System.Windows.Forms.Label lblTopZ;
        private System.Windows.Forms.TextBox txtTopZ;
        private System.Windows.Forms.Button btnTopZMove;
        private System.Windows.Forms.Button btnTopZRead;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblLb;
        private System.Windows.Forms.Label lblLt;
        private System.Windows.Forms.Label lblBotZ;
        private System.Windows.Forms.TextBox txtBotZ;
        private System.Windows.Forms.Button btnBotZMove;
        private System.Windows.Forms.Button btnBotZRead;
        private System.Windows.Forms.Label lblLbY;
        private System.Windows.Forms.TextBox txtLbY;
        private System.Windows.Forms.Button btnLbYMove;
        private System.Windows.Forms.Button btnLbYRead;
        private System.Windows.Forms.Label lblLbX;
        private System.Windows.Forms.TextBox txtLbX;
        private System.Windows.Forms.Button btnLbXMove;
        private System.Windows.Forms.Button btnLbXRead;
        private System.Windows.Forms.Label lblRbY;
        private System.Windows.Forms.TextBox txtRbY;
        private System.Windows.Forms.Button btnRbYMove;
        private System.Windows.Forms.Button btnRbYRead;
        private System.Windows.Forms.Label lblRbX;
        private System.Windows.Forms.TextBox txtRbX;
        private System.Windows.Forms.Button btnRbXMove;
        private System.Windows.Forms.Button btnRbXRead;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblRb;
        private System.Windows.Forms.Label lblRt;
        private System.Windows.Forms.Label lblXSpeed;
        private System.Windows.Forms.TextBox txtXSpeed;
        private System.Windows.Forms.Label lblYSpeed;
        private System.Windows.Forms.TextBox txtYSpeed;
        private System.Windows.Forms.Label lblMoveSpeed;
        private System.Windows.Forms.TextBox txtMoveSpeed;
    }
}

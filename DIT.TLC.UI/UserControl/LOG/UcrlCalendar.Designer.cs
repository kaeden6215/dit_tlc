﻿namespace DIT.TLC.UI.LOG
{
    partial class UcrlCalendar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSearchStartTime = new System.Windows.Forms.Label();
            this.btnLogFileOpen = new System.Windows.Forms.Button();
            this.btnSaveCsvFile = new System.Windows.Forms.Button();
            this.btnSelectTimeCsvFileOpen = new System.Windows.Forms.Button();
            this.lblSearchEndTime = new System.Windows.Forms.Label();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAllTime = new System.Windows.Forms.Button();
            this.lblRunInfo = new System.Windows.Forms.Label();
            this.btnSelectedTime = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.btnSelectedDate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAllDate = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblSearchStartDate = new System.Windows.Forms.Label();
            this.lblSearchEndDate = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSearchStartTime
            // 
            this.lblSearchStartTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblSearchStartTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSearchStartTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSearchStartTime.ForeColor = System.Drawing.Color.Black;
            this.lblSearchStartTime.Location = new System.Drawing.Point(3, 31);
            this.lblSearchStartTime.Name = "lblSearchStartTime";
            this.lblSearchStartTime.Size = new System.Drawing.Size(88, 21);
            this.lblSearchStartTime.TabIndex = 4;
            this.lblSearchStartTime.Text = "시작 시간";
            this.lblSearchStartTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLogFileOpen
            // 
            this.btnLogFileOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnLogFileOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLogFileOpen.ForeColor = System.Drawing.Color.Black;
            this.btnLogFileOpen.Location = new System.Drawing.Point(11, 378);
            this.btnLogFileOpen.Name = "btnLogFileOpen";
            this.btnLogFileOpen.Size = new System.Drawing.Size(286, 80);
            this.btnLogFileOpen.TabIndex = 8;
            this.btnLogFileOpen.Text = "로그 파일 열기";
            this.btnLogFileOpen.UseVisualStyleBackColor = false;
            this.btnLogFileOpen.Click += new System.EventHandler(this.btnLogFileOpen_Click);
            // 
            // btnSaveCsvFile
            // 
            this.btnSaveCsvFile.BackColor = System.Drawing.SystemColors.Control;
            this.btnSaveCsvFile.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSaveCsvFile.ForeColor = System.Drawing.Color.Black;
            this.btnSaveCsvFile.Location = new System.Drawing.Point(11, 468);
            this.btnSaveCsvFile.Name = "btnSaveCsvFile";
            this.btnSaveCsvFile.Size = new System.Drawing.Size(285, 80);
            this.btnSaveCsvFile.TabIndex = 9;
            this.btnSaveCsvFile.Text = "화면 내용 CSV 파일로 저장";
            this.btnSaveCsvFile.UseVisualStyleBackColor = false;
            this.btnSaveCsvFile.Click += new System.EventHandler(this.btnSaveCsvFile_Click);
            // 
            // btnSelectTimeCsvFileOpen
            // 
            this.btnSelectTimeCsvFileOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnSelectTimeCsvFileOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSelectTimeCsvFileOpen.ForeColor = System.Drawing.Color.Black;
            this.btnSelectTimeCsvFileOpen.Location = new System.Drawing.Point(11, 558);
            this.btnSelectTimeCsvFileOpen.Name = "btnSelectTimeCsvFileOpen";
            this.btnSelectTimeCsvFileOpen.Size = new System.Drawing.Size(285, 80);
            this.btnSelectTimeCsvFileOpen.TabIndex = 10;
            this.btnSelectTimeCsvFileOpen.Text = "선택한 날에 대한 CSV 파일 저장된 폴더 열기";
            this.btnSelectTimeCsvFileOpen.UseVisualStyleBackColor = false;
            this.btnSelectTimeCsvFileOpen.Click += new System.EventHandler(this.btnSelectTimeCsvFileOpen_Click);
            // 
            // lblSearchEndTime
            // 
            this.lblSearchEndTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblSearchEndTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSearchEndTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSearchEndTime.ForeColor = System.Drawing.Color.Black;
            this.lblSearchEndTime.Location = new System.Drawing.Point(3, 61);
            this.lblSearchEndTime.Name = "lblSearchEndTime";
            this.lblSearchEndTime.Size = new System.Drawing.Size(88, 21);
            this.lblSearchEndTime.TabIndex = 11;
            this.lblSearchEndTime.Text = "종료 시간";
            this.lblSearchEndTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.CustomFormat = "HH:mm";
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartTime.Location = new System.Drawing.Point(97, 31);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.ShowUpDown = true;
            this.dtpStartTime.Size = new System.Drawing.Size(105, 21);
            this.dtpStartTime.TabIndex = 12;
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.CustomFormat = "HH:mm";
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndTime.Location = new System.Drawing.Point(97, 61);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowUpDown = true;
            this.dtpEndTime.Size = new System.Drawing.Size(105, 21);
            this.dtpEndTime.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnAllTime);
            this.panel2.Controls.Add(this.lblRunInfo);
            this.panel2.Controls.Add(this.btnSelectedTime);
            this.panel2.Controls.Add(this.dtpEndTime);
            this.panel2.Controls.Add(this.lblSearchStartTime);
            this.panel2.Controls.Add(this.lblSearchEndTime);
            this.panel2.Controls.Add(this.dtpStartTime);
            this.panel2.Location = new System.Drawing.Point(11, 277);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(286, 95);
            this.panel2.TabIndex = 460;
            // 
            // btnAllTime
            // 
            this.btnAllTime.BackColor = System.Drawing.SystemColors.Control;
            this.btnAllTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAllTime.ForeColor = System.Drawing.Color.Black;
            this.btnAllTime.Location = new System.Drawing.Point(208, 60);
            this.btnAllTime.Name = "btnAllTime";
            this.btnAllTime.Size = new System.Drawing.Size(73, 23);
            this.btnAllTime.TabIndex = 465;
            this.btnAllTime.Text = "전체 시간";
            this.btnAllTime.UseVisualStyleBackColor = false;
            this.btnAllTime.Click += new System.EventHandler(this.btnAllTime_Click);
            // 
            // lblRunInfo
            // 
            this.lblRunInfo.AutoEllipsis = true;
            this.lblRunInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblRunInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblRunInfo.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblRunInfo.ForeColor = System.Drawing.Color.Black;
            this.lblRunInfo.Location = new System.Drawing.Point(0, 0);
            this.lblRunInfo.Name = "lblRunInfo";
            this.lblRunInfo.Size = new System.Drawing.Size(284, 20);
            this.lblRunInfo.TabIndex = 9;
            this.lblRunInfo.Text = "■ 시간 선택";
            this.lblRunInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSelectedTime
            // 
            this.btnSelectedTime.BackColor = System.Drawing.SystemColors.Control;
            this.btnSelectedTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSelectedTime.ForeColor = System.Drawing.Color.Black;
            this.btnSelectedTime.Location = new System.Drawing.Point(208, 31);
            this.btnSelectedTime.Name = "btnSelectedTime";
            this.btnSelectedTime.Size = new System.Drawing.Size(73, 23);
            this.btnSelectedTime.TabIndex = 464;
            this.btnSelectedTime.Text = "선택 시간";
            this.btnSelectedTime.UseVisualStyleBackColor = false;
            this.btnSelectedTime.Click += new System.EventHandler(this.btnSelectedTime_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(31, 21);
            this.monthCalendar1.MaxSelectionCount = 9999;
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 0;
            this.monthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateSelected);
            this.monthCalendar1.MouseEnter += new System.EventHandler(this.monthCalendar1_MouseEnter);
            // 
            // btnSelectedDate
            // 
            this.btnSelectedDate.BackColor = System.Drawing.SystemColors.Control;
            this.btnSelectedDate.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSelectedDate.ForeColor = System.Drawing.Color.Black;
            this.btnSelectedDate.Location = new System.Drawing.Point(208, 193);
            this.btnSelectedDate.Name = "btnSelectedDate";
            this.btnSelectedDate.Size = new System.Drawing.Size(73, 23);
            this.btnSelectedDate.TabIndex = 466;
            this.btnSelectedDate.Text = "선택 날짜";
            this.btnSelectedDate.UseVisualStyleBackColor = false;
            this.btnSelectedDate.Click += new System.EventHandler(this.btnSelectedDate_Click);
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ 날짜 선택";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAllDate
            // 
            this.btnAllDate.BackColor = System.Drawing.SystemColors.Control;
            this.btnAllDate.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAllDate.ForeColor = System.Drawing.Color.Black;
            this.btnAllDate.Location = new System.Drawing.Point(208, 222);
            this.btnAllDate.Name = "btnAllDate";
            this.btnAllDate.Size = new System.Drawing.Size(73, 23);
            this.btnAllDate.TabIndex = 467;
            this.btnAllDate.Text = "전체 날짜";
            this.btnAllDate.UseVisualStyleBackColor = false;
            this.btnAllDate.Click += new System.EventHandler(this.btnLogFileOpen_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.dtpEndDate);
            this.panel3.Controls.Add(this.lblSearchStartDate);
            this.panel3.Controls.Add(this.lblSearchEndDate);
            this.panel3.Controls.Add(this.dtpStartDate);
            this.panel3.Controls.Add(this.btnAllDate);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btnSelectedDate);
            this.panel3.Controls.Add(this.monthCalendar1);
            this.panel3.Location = new System.Drawing.Point(11, 13);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(286, 258);
            this.panel3.TabIndex = 461;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.CustomFormat = "HH:mm";
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndDate.Location = new System.Drawing.Point(97, 223);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(105, 21);
            this.dtpEndDate.TabIndex = 471;
            this.dtpEndDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            this.dtpEndDate.MouseEnter += new System.EventHandler(this.dtpDate_MouseEnter);
            // 
            // lblSearchStartDate
            // 
            this.lblSearchStartDate.BackColor = System.Drawing.SystemColors.Control;
            this.lblSearchStartDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSearchStartDate.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSearchStartDate.ForeColor = System.Drawing.Color.Black;
            this.lblSearchStartDate.Location = new System.Drawing.Point(3, 193);
            this.lblSearchStartDate.Name = "lblSearchStartDate";
            this.lblSearchStartDate.Size = new System.Drawing.Size(88, 21);
            this.lblSearchStartDate.TabIndex = 468;
            this.lblSearchStartDate.Text = "시작 날짜";
            this.lblSearchStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSearchEndDate
            // 
            this.lblSearchEndDate.BackColor = System.Drawing.SystemColors.Control;
            this.lblSearchEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSearchEndDate.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSearchEndDate.ForeColor = System.Drawing.Color.Black;
            this.lblSearchEndDate.Location = new System.Drawing.Point(3, 223);
            this.lblSearchEndDate.Name = "lblSearchEndDate";
            this.lblSearchEndDate.Size = new System.Drawing.Size(88, 21);
            this.lblSearchEndDate.TabIndex = 469;
            this.lblSearchEndDate.Text = "종료 날짜";
            this.lblSearchEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.CustomFormat = "HH:mm";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStartDate.Location = new System.Drawing.Point(97, 193);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(105, 21);
            this.dtpStartDate.TabIndex = 470;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            this.dtpStartDate.MouseEnter += new System.EventHandler(this.dtpDate_MouseEnter);
            // 
            // UcrlCalendar
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnSelectTimeCsvFileOpen);
            this.Controls.Add(this.btnSaveCsvFile);
            this.Controls.Add(this.btnLogFileOpen);
            this.Name = "UcrlCalendar";
            this.Size = new System.Drawing.Size(308, 875);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblSearchStartTime;
        private System.Windows.Forms.Button btnLogFileOpen;
        private System.Windows.Forms.Button btnSaveCsvFile;
        private System.Windows.Forms.Button btnSelectTimeCsvFileOpen;
        private System.Windows.Forms.Label lblSearchEndTime;
        private System.Windows.Forms.DateTimePicker dtpStartTime;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label lblRunInfo;
        private System.Windows.Forms.Button btnSelectedTime;
        private System.Windows.Forms.Button btnAllTime;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button btnSelectedDate;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAllDate;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lblSearchStartDate;
        private System.Windows.Forms.Label lblSearchEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
    }
}

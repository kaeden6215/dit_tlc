﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DIT.TLC.UI;
using System.Diagnostics;

namespace DIT.TLC.UI.LOG
{
    public partial class UcrlCalendar : UserControl
    {
        private const string PATH = "C:\\Users\\dit-549\\Desktop\\TLC LOG"; // 기본경로
        private List<string> FilePathList = new List<string>();             // 경로상의 모든 파일 리스트
        private List<ListViewItem> currentList = new List<ListViewItem>();  // 선택한 날의 전체 시간 아이템
        
        public UcrlCalendar()
        {
            InitializeComponent();
        }

        #region 이벤트

        /// <summary>
        /// 달력에서 날짜 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            dtpStartDate.Value = monthCalendar1.SelectionRange.Start;
            dtpEndDate.Value = monthCalendar1.SelectionRange.End;
        }

        /// <summary>
        /// 날짜 DateTimePicker 바뀜 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            if(flag)
            {
                monthCalendar1.SelectionStart = dtpStartDate.Value;
                monthCalendar1.SelectionEnd = dtpEndDate.Value;
                monthCalendar1.SelectionRange.Start = dtpStartDate.Value;
                monthCalendar1.SelectionRange.End = dtpEndDate.Value;

                flag = false;
            }
        }

        private bool flag = false;
        /// <summary>
        /// 마우스 위치감지
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void monthCalendar1_MouseEnter(object sender, EventArgs e)
        {
            flag = false;
        }

        /// <summary>
        /// 마우스 위치감지
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpDate_MouseEnter(object sender, EventArgs e)
        {
            flag = true;
        }

        /// <summary>
        /// 선택날짜 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectedDate_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            List<string> log = new List<string>();

            switch (btn.Parent.Parent.Name)
            {
                case "calendarAlarmlog":
                    log = LogFileOpen("Alarmlog", dtpStartDate.Value, dtpEndDate.Value);
                    break;
                case "calendarButtonAction":
                    log = LogFileOpen("ButtonAction", dtpStartDate.Value, dtpEndDate.Value);
                    break;
                case "calendarParameter":
                    log = LogFileOpen("Parameter", dtpStartDate.Value, dtpEndDate.Value);
                    break;
                case "calendarOpcall":
                    log = LogFileOpen("Opcall", dtpStartDate.Value, dtpEndDate.Value);
                    break;
                case "calendarMovefail":
                    log = LogFileOpen("Movefail", dtpStartDate.Value, dtpEndDate.Value);
                    break;
                case "calendarMeasure":
                    log = LogFileOpen("Measure", dtpStartDate.Value, dtpEndDate.Value);
                    break;
            }

            ListView lv = (ListView)btn.Parent.Parent.Parent.Controls[1];
            lv.Items.Clear();
            log.ToList().ForEach(g => lv.Items.Add(new ListViewItem(g.Split('\t'))));
        }

        /// <summary>
        /// 선택시간 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectedTime_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            ListView lv = (ListView)btn.Parent.Parent.Parent.Controls[1];

            List<ListViewItem> timeFiltering = SelectTime(dtpStartTime.Value, dtpEndTime.Value);
            
            lv.Items.Clear();
            timeFiltering.ToList().ForEach(log => lv.Items.Add(log));
        }

        /// <summary>
        /// 전체시간 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAllTime_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            ListView lv = (ListView)btn.Parent.Parent.Parent.Controls[1];

            lv.Items.Clear();
            foreach (ListViewItem lvi in currentList)
                lv.Items.Add(lvi);
            lv.Update();

            dtpStartTime.Value = DateTime.Now;
            dtpEndTime.Value = DateTime.Now;
        }
        
         
        /// <summary>
        /// 로그파일 열기, 전체날짜 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogFileOpen_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            List<string> log = new List<string>();
            ListView lv = new ListView();
            string name = string.Empty;

            if(btn.Text == "로그 파일 열기")
            {
                name = btn.Parent.Name;
                lv = (ListView)btn.Parent.Parent.Controls[1];
            }
            else if(btn.Text == "전체 날짜")
            {
                name = btn.Parent.Parent.Name;
                lv = (ListView)btn.Parent.Parent.Parent.Controls[1];
            }

            name = name.Replace("calendar", "");
            log = LogFileOpen(name);
            
            lv.Items.Clear();
            log.ToList().ForEach(g => lv.Items.Add(new ListViewItem(g.Split('\t'))));

            dtpStartTime.Value = DateTime.Now;
            dtpEndTime.Value = DateTime.Now;
            dtpStartDate.Value = DateTime.Today;
            dtpEndDate.Value = DateTime.Today;
            monthCalendar1.SelectionStart = DateTime.Today;
            monthCalendar1.SelectionEnd = DateTime.Today;
        }

        /// <summary>
        /// 화면내용 CSV로 저장 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveCsvFile_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            string folderName = btn.Parent.Name.Replace("calendar", "");
            SaveCsvFile(sender, folderName);
        }

        /// <summary>
        /// 선택한 날에 대한 CSV 파일 저장된 폴더 열기 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectTimeCsvFileOpen_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string folderName = btn.Parent.Name.Replace("calendar", "");

            SelectTimeCsvFileOpen(folderName, monthCalendar1.SelectionRange.Start, monthCalendar1.SelectionRange.End);
            
        }
        
        #endregion

        /// <summary>
        /// 로그 세팅
        /// </summary>
        /// <param name="path"></param>
        private List<string> LogFileOpen(string path, DateTime startDate = new DateTime(), DateTime endDate = new DateTime())
        {
            FilePathList.Clear();
            currentList.Clear();

            DirFileSearch(PATH + "\\" + path, "*");
            
            try
            {
                List<string> log = new List<string>();
                foreach (string p in FilePathList)
                {
                    if(startDate != new DateTime() && endDate != new DateTime())
                    {
                        string[] ab = p.Split('\\');
                        DateTime date = Convert.ToDateTime(ab[ab.Length - 2]);

                        if (DateTime.Compare(startDate, date) <= 0 && DateTime.Compare(date, endDate) <= 0)
                        { }
                        else
                            continue;
                    }

                    string line;

                    
                    StreamReader file = new StreamReader(p, Encoding.Default);
                    while ((line = file.ReadLine()) != null)
                    {
                        log.Add(line);
                        currentList.Add(new ListViewItem(line.Split('\t')));
                    }

                    file.Close();
                }
                return log;
            }
            catch (Exception /*e*/)
            {
                return new List<string>();
            }
        }

        /// <summary>
        /// 시간 선택 메소드
        /// </summary>
        /// <param name="lv"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private List<ListViewItem> SelectTime(DateTime startDate, DateTime endDate)
        {
            List<ListViewItem> sl = new List<ListViewItem>();

            foreach(ListViewItem item in currentList)
            {
                DateTime dt = Convert.ToDateTime(item.SubItems[1].Text);

                if (startDate < dt && dt < endDate)
                    sl.Add(item);
            }
            return sl;
        }

        /// <summary>
        /// CSV 파일로 저장
        /// </summary>
        private void SaveCsvFile(object sender, string path)
        {
            Button btn = (Button)sender;

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "파일 저장하기";
            dialog.Filter = "CSV 파일|*.csv|Text 파일|*.txt";
            dialog.FileName = path;

            dialog.ShowDialog();

            string selectPath = dialog.FileName;

            if (selectPath != path)
            {
                using (StreamWriter outPutFile = new StreamWriter(selectPath, false, Encoding.Default))
                {
                    ListView lv = (ListView)btn.Parent.Parent.Controls[1];
                    string rows = "";

                    foreach(ColumnHeader header in lv.Columns)
                        rows += header.Text + ",";
                    rows = rows.Substring(0, rows.Length - 1);
                    outPutFile.WriteLine(rows, Encoding.Default);
                    
                    foreach (ListViewItem lvi in lv.Items)
                    {
                        rows = "";
                        foreach (ListViewItem.ListViewSubItem str in lvi.SubItems)
                            rows += str.Text + ",";
                        rows = rows.Substring(0, rows.Length - 1);
                        outPutFile.WriteLine(rows, Encoding.Default);
                    }
                }
            }
        }

        /// <summary>
        /// CSV 저장된 폴더 열기
        /// </summary>
        private void SelectTimeCsvFileOpen(string fileName, DateTime startDate, DateTime endDate)
        {
            string Path = PATH + "\\" + fileName + "\\";

            DateTime currentTime = startDate;

            try
            {
                while (DateTime.Compare(currentTime, endDate) <= 0)
                {
                    Process.Start(Path + currentTime.ToString("yy-MM-dd"));

                    currentTime = currentTime.AddDays(1);
                }
            }
            catch(Exception /*e*/)
            {
                MessageBox.Show("날짜선택이 잘못되었습니다.");
            }
           
        }
        
        /// <summary>
        /// 경로상의 폴더 얻기
        /// </summary>
        /// <param name="path"></param>
        /// <param name="file"></param>
        private void DirFileSearch(string path, string file)
        {
            try
            {
                string[] dirs = Directory.GetDirectories(path);
                string[] files = Directory.GetFiles(path, $"*.{file}");

                files.ToList().ForEach(f => FilePathList.Add(f));

                if (dirs.Length > 0)
                    dirs.ToList().ForEach(dir => DirFileSearch(dir, file));
            }
            catch (Exception /*ex*/)
            {
            }
        }

    }
}

﻿namespace DIT.TLC.UI.Moter_In_Out
{
    partial class InOutControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnXYLamp = new System.Windows.Forms.Panel();
            this.lblXY = new System.Windows.Forms.Label();
            this.lblXYName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnXYLamp
            // 
            this.btnXYLamp.BackColor = System.Drawing.Color.Gray;
            this.btnXYLamp.Location = new System.Drawing.Point(11, 5);
            this.btnXYLamp.Name = "btnXYLamp";
            this.btnXYLamp.Size = new System.Drawing.Size(40, 40);
            this.btnXYLamp.TabIndex = 130;
            // 
            // lblXY
            // 
            this.lblXY.AutoSize = true;
            this.lblXY.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblXY.ForeColor = System.Drawing.Color.White;
            this.lblXY.Location = new System.Drawing.Point(57, 20);
            this.lblXY.Name = "lblXY";
            this.lblXY.Size = new System.Drawing.Size(66, 13);
            this.lblXY.TabIndex = 132;
            this.lblXY.Text = "[ X000 ]";
            // 
            // lblXYName
            // 
            this.lblXYName.AutoSize = true;
            this.lblXYName.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblXYName.ForeColor = System.Drawing.Color.White;
            this.lblXYName.Location = new System.Drawing.Point(121, 20);
            this.lblXYName.Name = "lblXYName";
            this.lblXYName.Size = new System.Drawing.Size(15, 13);
            this.lblXYName.TabIndex = 133;
            this.lblXYName.Text = "-";
            // 
            // InOutControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblXYName);
            this.Controls.Add(this.lblXY);
            this.Controls.Add(this.btnXYLamp);
            this.Name = "InOutControl";
            this.Size = new System.Drawing.Size(416, 52);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel btnXYLamp;
        private System.Windows.Forms.Label lblXY;
        private System.Windows.Forms.Label lblXYName;
    }
}

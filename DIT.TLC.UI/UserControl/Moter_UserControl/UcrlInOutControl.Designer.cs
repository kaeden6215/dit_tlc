﻿namespace DIT.TLC.UI.Moter_In_Out
{
    partial class UcrlInOutControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnXY = new System.Windows.Forms.Button();
            this.lblXYName = new System.Windows.Forms.Label();
            this.lblXY = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnXY
            // 
            this.btnXY.BackColor = System.Drawing.SystemColors.Control;
            this.btnXY.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnXY.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXY.Location = new System.Drawing.Point(5, 2);
            this.btnXY.Margin = new System.Windows.Forms.Padding(0);
            this.btnXY.Name = "btnXY";
            this.btnXY.Size = new System.Drawing.Size(37, 37);
            this.btnXY.TabIndex = 138;
            this.btnXY.UseVisualStyleBackColor = false;
            this.btnXY.Click += new System.EventHandler(this.btnXY_Click);
            // 
            // lblXYName
            // 
            this.lblXYName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblXYName.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblXYName.ForeColor = System.Drawing.Color.Black;
            this.lblXYName.Location = new System.Drawing.Point(111, 2);
            this.lblXYName.Name = "lblXYName";
            this.lblXYName.Size = new System.Drawing.Size(290, 37);
            this.lblXYName.TabIndex = 137;
            this.lblXYName.Text = "-";
            this.lblXYName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblXY
            // 
            this.lblXY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblXY.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblXY.ForeColor = System.Drawing.Color.Black;
            this.lblXY.Location = new System.Drawing.Point(45, 2);
            this.lblXY.Name = "lblXY";
            this.lblXY.Size = new System.Drawing.Size(60, 37);
            this.lblXY.TabIndex = 136;
            this.lblXY.Text = "[ X000 ]";
            this.lblXY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlInOutControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.btnXY);
            this.Controls.Add(this.lblXYName);
            this.Controls.Add(this.lblXY);
            this.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlInOutControl";
            this.Size = new System.Drawing.Size(416, 41);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnXY;
        private System.Windows.Forms.Label lblXYName;
        private System.Windows.Forms.Label lblXY;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dit.Framework.Comm;

namespace DIT.TLC.UI.Moter_In_Out
{
    public partial class UcrlInOutTotalControl : UserControl, IUIUpdate
    {
        private List<UcrlInOutControl> _lstInOutControl = new List<UcrlInOutControl>();
        public UcrlInOutTotalControl()
        {
            InitializeComponent();

            _lstInOutControl = new List<UcrlInOutControl>(){
            inControl1  ,inControl2 , inControl3    ,inControl4 ,inControl5 ,inControl6 ,inControl7 ,inControl8 ,inControl9 ,inControl10    ,
            inControl11 ,inControl12    ,inControl13    ,inControl14    ,inControl15    ,inControl16    ,inControl17    ,inControl18    ,inControl19    ,inControl20    ,
            inControl21 ,inControl22    ,inControl23    ,inControl24    ,inControl25    ,inControl26    ,inControl27    ,inControl28    ,inControl29    ,inControl30    ,
            inControl31 ,inControl32    };

        }
        public void SetAddress(string address)
        {
            string[] items = address.Split(',');

            for (int iPos = 0; iPos < 32; iPos++)
            {
                var plcAddr = iPos >= items.Length ? null : string.IsNullOrEmpty(items[iPos]) ? null : AddressMgr.GetAddress(items[iPos], (short)81);
                if (iPos < _lstInOutControl.Count)
                    _lstInOutControl[iPos].SetPlcAddress(plcAddr);
            }
        }

        public void UIUpdate()
        {
            for (int iPos = 0; iPos < _lstInOutControl.Count; iPos++)
                _lstInOutControl[iPos].UpdateUi();
        }

    }
}

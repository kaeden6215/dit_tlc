using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dit.Framework.PLC;

namespace DIT.TLC.UI.Moter_In_Out
{
    public partial class UcrlInOutControl : UserControl
    {
        public PlcAddr _plcAddr = null;
        public UcrlInOutControl()
        {
            InitializeComponent();
        }
        public void SetPlcAddress(PlcAddr plcAddr)
        {
            if (plcAddr == null)
            {
                _plcAddr = null;
                lblXYName.Text = string.Empty;
                btnXY.BackColor = UiGlobal.UNSET_C;
            }
            else
            {
                _plcAddr = plcAddr;
                lblXY.Text = _plcAddr.WireNo;
                lblXYName.Text = string.Format("[{0}] {1}", _plcAddr.ToString(), _plcAddr.Desc).PadRight(50, ' ').Substring(0, 50);
                btnXY.BackColor = _plcAddr.vBit ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            }
        }
        public void UpdateUi()
        {
            btnXY.BackColor = _plcAddr == null ? UiGlobal.UNSET_C : _plcAddr.vBit ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            if (_plcAddr != null)
            {
                //Console.WriteLine("FSADFAS");
            }
        }

        private void btnXY_Click(object sender, EventArgs e)
        {
            if (_plcAddr == null) return;

            _plcAddr.vBit = !_plcAddr.vBit;
        }
    }
}

﻿namespace DIT.TLC.UI.Main
{
    partial class UcrlCstCellCount
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_ald_cst_name = new System.Windows.Forms.Label();
            this.lb_ald_cst_b = new System.Windows.Forms.Label();
            this.lbl_cst_count2 = new System.Windows.Forms.Label();
            this.lbl_cst_count1 = new System.Windows.Forms.Label();
            this.lblCstName = new System.Windows.Forms.Label();
            this.flpPanels = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // lb_ald_cst_name
            // 
            this.lb_ald_cst_name.BackColor = System.Drawing.SystemColors.Control;
            this.lb_ald_cst_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_ald_cst_name.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ald_cst_name.ForeColor = System.Drawing.Color.Black;
            this.lb_ald_cst_name.Location = new System.Drawing.Point(31, 344);
            this.lb_ald_cst_name.Name = "lb_ald_cst_name";
            this.lb_ald_cst_name.Size = new System.Drawing.Size(64, 17);
            this.lb_ald_cst_name.TabIndex = 441;
            this.lb_ald_cst_name.Text = "00000000";
            this.lb_ald_cst_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_ald_cst_b
            // 
            this.lb_ald_cst_b.BackColor = System.Drawing.SystemColors.Control;
            this.lb_ald_cst_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_ald_cst_b.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ald_cst_b.ForeColor = System.Drawing.Color.Black;
            this.lb_ald_cst_b.Location = new System.Drawing.Point(1, 344);
            this.lb_ald_cst_b.Name = "lb_ald_cst_b";
            this.lb_ald_cst_b.Size = new System.Drawing.Size(29, 17);
            this.lb_ald_cst_b.TabIndex = 440;
            this.lb_ald_cst_b.Text = "B :";
            this.lb_ald_cst_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_cst_count2
            // 
            this.lbl_cst_count2.BackColor = System.Drawing.SystemColors.Control;
            this.lbl_cst_count2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_cst_count2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_cst_count2.ForeColor = System.Drawing.Color.Black;
            this.lbl_cst_count2.Location = new System.Drawing.Point(40, 362);
            this.lbl_cst_count2.Name = "lbl_cst_count2";
            this.lbl_cst_count2.Size = new System.Drawing.Size(55, 16);
            this.lbl_cst_count2.TabIndex = 439;
            this.lbl_cst_count2.Text = "000";
            this.lbl_cst_count2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_cst_count1
            // 
            this.lbl_cst_count1.BackColor = System.Drawing.SystemColors.Control;
            this.lbl_cst_count1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_cst_count1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_cst_count1.ForeColor = System.Drawing.Color.Black;
            this.lbl_cst_count1.Location = new System.Drawing.Point(1, 362);
            this.lbl_cst_count1.Name = "lbl_cst_count1";
            this.lbl_cst_count1.Size = new System.Drawing.Size(38, 16);
            this.lbl_cst_count1.TabIndex = 438;
            this.lbl_cst_count1.Text = "004";
            this.lbl_cst_count1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCstName
            // 
            this.lblCstName.AutoEllipsis = true;
            this.lblCstName.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCstName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCstName.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblCstName.ForeColor = System.Drawing.Color.Black;
            this.lblCstName.Location = new System.Drawing.Point(0, 0);
            this.lblCstName.Name = "lblCstName";
            this.lblCstName.Size = new System.Drawing.Size(96, 16);
            this.lblCstName.TabIndex = 586;
            this.lblCstName.Text = "■ A/B 카세트";
            this.lblCstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flpPanels
            // 
            this.flpPanels.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpPanels.Location = new System.Drawing.Point(2, 17);
            this.flpPanels.Margin = new System.Windows.Forms.Padding(1);
            this.flpPanels.Name = "flpPanels";
            this.flpPanels.Size = new System.Drawing.Size(92, 324);
            this.flpPanels.TabIndex = 587;
            // 
            // UcrlCstCellCount
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.flpPanels);
            this.Controls.Add(this.lblCstName);
            this.Controls.Add(this.lb_ald_cst_name);
            this.Controls.Add(this.lb_ald_cst_b);
            this.Controls.Add(this.lbl_cst_count2);
            this.Controls.Add(this.lbl_cst_count1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlCstCellCount";
            this.Size = new System.Drawing.Size(96, 380);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lb_ald_cst_name;
        private System.Windows.Forms.Label lb_ald_cst_b;
        private System.Windows.Forms.Label lbl_cst_count2;
        private System.Windows.Forms.Label lbl_cst_count1;
        internal System.Windows.Forms.Label lblCstName;
        private System.Windows.Forms.FlowLayoutPanel flpPanels;
    }
}

﻿namespace DIT.TLC.UI.Main
{
    partial class DetailTactView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_tacttime = new System.Windows.Forms.TextBox();
            this.lbl_tacttime = new System.Windows.Forms.Label();
            this.tb_cell_info = new System.Windows.Forms.TextBox();
            this.lbl_Cell_info = new System.Windows.Forms.Label();
            this.tb_run_info = new System.Windows.Forms.TextBox();
            this.lbl_run_info = new System.Windows.Forms.Label();
            this.panel_cst_uld_r_info = new System.Windows.Forms.Panel();
            this.lbldtname = new System.Windows.Forms.Label();
            this.panel_cst_uld_r_info.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_tacttime
            // 
            this.tb_tacttime.BackColor = System.Drawing.Color.DimGray;
            this.tb_tacttime.Location = new System.Drawing.Point(65, 88);
            this.tb_tacttime.Name = "tb_tacttime";
            this.tb_tacttime.ReadOnly = true;
            this.tb_tacttime.Size = new System.Drawing.Size(99, 21);
            this.tb_tacttime.TabIndex = 15;
            // 
            // lbl_tacttime
            // 
            this.lbl_tacttime.AutoSize = true;
            this.lbl_tacttime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_tacttime.ForeColor = System.Drawing.Color.White;
            this.lbl_tacttime.Location = new System.Drawing.Point(5, 90);
            this.lbl_tacttime.Name = "lbl_tacttime";
            this.lbl_tacttime.Size = new System.Drawing.Size(62, 15);
            this.lbl_tacttime.TabIndex = 14;
            this.lbl_tacttime.Text = "Tact 타임";
            // 
            // tb_cell_info
            // 
            this.tb_cell_info.BackColor = System.Drawing.Color.DimGray;
            this.tb_cell_info.Location = new System.Drawing.Point(65, 61);
            this.tb_cell_info.Name = "tb_cell_info";
            this.tb_cell_info.ReadOnly = true;
            this.tb_cell_info.Size = new System.Drawing.Size(99, 21);
            this.tb_cell_info.TabIndex = 13;
            // 
            // lbl_Cell_info
            // 
            this.lbl_Cell_info.AutoSize = true;
            this.lbl_Cell_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cell_info.ForeColor = System.Drawing.Color.White;
            this.lbl_Cell_info.Location = new System.Drawing.Point(5, 63);
            this.lbl_Cell_info.Name = "lbl_Cell_info";
            this.lbl_Cell_info.Size = new System.Drawing.Size(47, 15);
            this.lbl_Cell_info.TabIndex = 12;
            this.lbl_Cell_info.Text = "셀 정보";
            // 
            // tb_run_info
            // 
            this.tb_run_info.BackColor = System.Drawing.Color.DimGray;
            this.tb_run_info.Location = new System.Drawing.Point(65, 34);
            this.tb_run_info.Name = "tb_run_info";
            this.tb_run_info.ReadOnly = true;
            this.tb_run_info.Size = new System.Drawing.Size(99, 21);
            this.tb_run_info.TabIndex = 11;
            // 
            // lbl_run_info
            // 
            this.lbl_run_info.AutoSize = true;
            this.lbl_run_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_run_info.ForeColor = System.Drawing.Color.White;
            this.lbl_run_info.Location = new System.Drawing.Point(5, 36);
            this.lbl_run_info.Name = "lbl_run_info";
            this.lbl_run_info.Size = new System.Drawing.Size(59, 15);
            this.lbl_run_info.TabIndex = 9;
            this.lbl_run_info.Text = "작업 상태";
            // 
            // panel_cst_uld_r_info
            // 
            this.panel_cst_uld_r_info.BackColor = System.Drawing.Color.DimGray;
            this.panel_cst_uld_r_info.Controls.Add(this.lbldtname);
            this.panel_cst_uld_r_info.ForeColor = System.Drawing.Color.White;
            this.panel_cst_uld_r_info.Location = new System.Drawing.Point(3, 4);
            this.panel_cst_uld_r_info.Name = "panel_cst_uld_r_info";
            this.panel_cst_uld_r_info.Size = new System.Drawing.Size(161, 25);
            this.panel_cst_uld_r_info.TabIndex = 10;
            // 
            // lbldtname
            // 
            this.lbldtname.AutoSize = true;
            this.lbldtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbldtname.Location = new System.Drawing.Point(6, 4);
            this.lbldtname.Name = "lbldtname";
            this.lbldtname.Size = new System.Drawing.Size(32, 18);
            this.lbldtname.TabIndex = 0;
            this.lbldtname.Text = "----";
            // 
            // DetailTactView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tb_tacttime);
            this.Controls.Add(this.lbl_tacttime);
            this.Controls.Add(this.tb_cell_info);
            this.Controls.Add(this.lbl_Cell_info);
            this.Controls.Add(this.tb_run_info);
            this.Controls.Add(this.lbl_run_info);
            this.Controls.Add(this.panel_cst_uld_r_info);
            this.Name = "DetailTactView";
            this.Size = new System.Drawing.Size(166, 110);
            this.panel_cst_uld_r_info.ResumeLayout(false);
            this.panel_cst_uld_r_info.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_tacttime;
        private System.Windows.Forms.Label lbl_tacttime;
        private System.Windows.Forms.TextBox tb_cell_info;
        private System.Windows.Forms.Label lbl_Cell_info;
        private System.Windows.Forms.TextBox tb_run_info;
        private System.Windows.Forms.Label lbl_run_info;
        private System.Windows.Forms.Panel panel_cst_uld_r_info;
        private System.Windows.Forms.Label lbldtname;
    }
}

﻿namespace DIT.TLC.UI.Main
{
    partial class UcrlDetailTactView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTactTime = new System.Windows.Forms.TextBox();
            this.lblTactTimeInfo = new System.Windows.Forms.Label();
            this.txtPnlID1 = new System.Windows.Forms.TextBox();
            this.lblCellInfo = new System.Windows.Forms.Label();
            this.txtRun = new System.Windows.Forms.TextBox();
            this.lblRunInfo = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txtPnlID2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtTactTime
            // 
            this.txtTactTime.BackColor = System.Drawing.SystemColors.Control;
            this.txtTactTime.Location = new System.Drawing.Point(58, 87);
            this.txtTactTime.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTactTime.Name = "txtTactTime";
            this.txtTactTime.ReadOnly = true;
            this.txtTactTime.Size = new System.Drawing.Size(90, 23);
            this.txtTactTime.TabIndex = 15;
            // 
            // lblTactTimeInfo
            // 
            this.lblTactTimeInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblTactTimeInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTactTimeInfo.ForeColor = System.Drawing.Color.Black;
            this.lblTactTimeInfo.Location = new System.Drawing.Point(5, 87);
            this.lblTactTimeInfo.Name = "lblTactTimeInfo";
            this.lblTactTimeInfo.Size = new System.Drawing.Size(50, 22);
            this.lblTactTimeInfo.TabIndex = 14;
            this.lblTactTimeInfo.Text = "Tact";
            this.lblTactTimeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPnlID1
            // 
            this.txtPnlID1.BackColor = System.Drawing.SystemColors.Control;
            this.txtPnlID1.Location = new System.Drawing.Point(58, 41);
            this.txtPnlID1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPnlID1.Name = "txtPnlID1";
            this.txtPnlID1.ReadOnly = true;
            this.txtPnlID1.Size = new System.Drawing.Size(90, 23);
            this.txtPnlID1.TabIndex = 13;
            this.txtPnlID1.DoubleClick += new System.EventHandler(this.CellID_DoubleClick);
            // 
            // lblCellInfo
            // 
            this.lblCellInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblCellInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCellInfo.ForeColor = System.Drawing.Color.Black;
            this.lblCellInfo.Location = new System.Drawing.Point(5, 41);
            this.lblCellInfo.Name = "lblCellInfo";
            this.lblCellInfo.Size = new System.Drawing.Size(50, 22);
            this.lblCellInfo.TabIndex = 12;
            this.lblCellInfo.Text = "ID 1";
            this.lblCellInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRun
            // 
            this.txtRun.BackColor = System.Drawing.SystemColors.Control;
            this.txtRun.Location = new System.Drawing.Point(58, 18);
            this.txtRun.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRun.Name = "txtRun";
            this.txtRun.ReadOnly = true;
            this.txtRun.Size = new System.Drawing.Size(90, 23);
            this.txtRun.TabIndex = 11;
            // 
            // lblRunInfo
            // 
            this.lblRunInfo.BackColor = System.Drawing.SystemColors.Control;
            this.lblRunInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRunInfo.ForeColor = System.Drawing.Color.Black;
            this.lblRunInfo.Location = new System.Drawing.Point(5, 18);
            this.lblRunInfo.Name = "lblRunInfo";
            this.lblRunInfo.Size = new System.Drawing.Size(50, 22);
            this.lblRunInfo.TabIndex = 9;
            this.lblRunInfo.Text = "작업";
            this.lblRunInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblName
            // 
            this.lblName.AutoEllipsis = true;
            this.lblName.BackColor = System.Drawing.Color.Gainsboro;
            this.lblName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblName.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblName.ForeColor = System.Drawing.Color.Black;
            this.lblName.Location = new System.Drawing.Point(0, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(152, 16);
            this.lblName.TabIndex = 16;
            this.lblName.Text = "■ ---";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPnlID2
            // 
            this.txtPnlID2.BackColor = System.Drawing.SystemColors.Control;
            this.txtPnlID2.Location = new System.Drawing.Point(58, 64);
            this.txtPnlID2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPnlID2.Name = "txtPnlID2";
            this.txtPnlID2.ReadOnly = true;
            this.txtPnlID2.Size = new System.Drawing.Size(90, 23);
            this.txtPnlID2.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(5, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 22);
            this.label1.TabIndex = 12;
            this.label1.Text = "ID 2";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlDetailTactView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtTactTime);
            this.Controls.Add(this.lblTactTimeInfo);
            this.Controls.Add(this.txtPnlID2);
            this.Controls.Add(this.txtPnlID1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCellInfo);
            this.Controls.Add(this.txtRun);
            this.Controls.Add(this.lblRunInfo);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UcrlDetailTactView";
            this.Size = new System.Drawing.Size(152, 110);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtTactTime;
        private System.Windows.Forms.Label lblTactTimeInfo;
        private System.Windows.Forms.TextBox txtPnlID1;
        private System.Windows.Forms.Label lblCellInfo;
        private System.Windows.Forms.TextBox txtRun;
        private System.Windows.Forms.Label lblRunInfo;
        internal System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtPnlID2;
        private System.Windows.Forms.Label label1;
    }
}

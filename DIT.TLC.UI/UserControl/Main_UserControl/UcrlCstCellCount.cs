﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI.Main
{
    public partial class UcrlCstCellCount : UserControl, IUIUpdate
    {
        private PortInfo _port;
        public PortInfo Port
        {
            get { return _port; }
            set
            {
                _port = value;
                Initaizle();
            }
        }

        private List<Panel> _lstPanels;
        public UcrlCstCellCount()
        {
            InitializeComponent();
            _lstPanels = new List<Panel>();

            this.DoubleClick += new System.EventHandler(Cst_DoubleClick);
        }
        public void Initaizle()
        {
            if (_port == null) return;

            lblCstName.Text = Port.Name;

            flpPanels.Controls.Clear();
            _lstPanels.ForEach(f => f.Dispose());
            Padding padding = new Padding(1);

            int pnlHeigh = flpPanels.Height / 40 - 2;
            int pnlWidth = flpPanels.Width / 4 - 2;


            for (int iPos = 0; iPos < _port.Cst.SlotCount; iPos++)
            {
                Panel pnl = new Panel() { Name = String.Format("PANEL {0}", iPos), Height = pnlHeigh , Width = pnlWidth, BackColor = Color.DarkGray, Margin = padding, Padding = padding, BorderStyle = BorderStyle.FixedSingle };                
                flpPanels.Controls.Add(pnl);
                _lstPanels.Add(pnl);
            }

            ControlsAddEvent();
        }

        private void Cst_DoubleClick(object sender, EventArgs e)
        {
            if (GG.Equip.RunMode == EmEquipRunMode.Start) return;

            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "FrmCstInfo")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Maximized;
                    }
                    openForm.Activate();
                    return;
                }
            }

            FrmCstInfo ff = new FrmCstInfo(_port);
            ff.Show();
        }

        private void ControlsAddEvent()
        {
            for (int i = 0; i < flpPanels.Controls.Count; i++)
            {
                Panel panels = (Panel)flpPanels.Controls[i];
                panels.DoubleClick += new System.EventHandler(Cst_DoubleClick);
            }

            Label[] lbls = { lb_ald_cst_b, lb_ald_cst_name, lbl_cst_count1, lbl_cst_count2, lblCstName };
            for (int i = 0; i < lbls.Count(); i++)
            {
                lbls[i].DoubleClick += new System.EventHandler(Cst_DoubleClick);
            }

            flpPanels.DoubleClick += new System.EventHandler(Cst_DoubleClick);
        }

        public void UIUpdate()
        {
            if (_port == null) return;
            if (_port.Cst == null) return;
            if (_port.Cst.LstPanel == null) return;

            for (int iPos = 0; iPos < _port.Cst.SlotCount; iPos++)
                _lstPanels[iPos].BackColor = _port.Cst.LstPanel[iPos].Exists ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
        }
    }
}

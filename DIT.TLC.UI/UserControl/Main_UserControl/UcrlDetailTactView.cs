﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI.Main
{
    public partial class UcrlDetailTactView : UserControl, IUIUpdate
    {
        private BaseUnit _unit;
        public BaseUnit Unit
        {
            get
            {
                return _unit;
            }
            set
            {
                _unit = value;
                InitilzieEquipment();
            }
        }

        public UcrlDetailTactView()
        {
            InitializeComponent();
        }
        public void InitilzieEquipment()
        {
            if (_unit == null) return;
            lblName.Text = _unit.Name;
        }
        public void UIUpdate()
        {
            if (_unit == null) return;

            lblName.BackColor = _unit.IsHomeComplete ? Color.Blue : Color.LightGray;
            txtRun.Text = _unit.ProcStatus.ToString();
            //txtPnlID1.Text = _unit.PanelSet.Panel1.PanelNo;
            txtPnlID1.Text = string.Format("{0} / {1}", _unit.PanelSet.Panel1.Index, _unit.PanelSet.Panel2.Index);
            //txtPnlID2.Text = _unit.PanelSet.Panel2.PanelNo;
            if (txtPnlID2.Text != _unit.SeqStepStr)
                txtPnlID2.Text = _unit.SeqStepStr;
        }

        public void CellID_DoubleClick(object sender, EventArgs e)
        {
            if (GG.Equip.RunMode == EmEquipRunMode.Start) return;
            if (_unit.IsExitsPanel == false) return; 

            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "FrmCellInfo")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Maximized;
                    }
                    openForm.Activate();
                    return;
                }
            }
            
            FrmCellInfo ff = new FrmCellInfo(_unit);
            ff.Show();
        }
    }
}
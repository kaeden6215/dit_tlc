﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UctrlLstTactView : UserControl
    {   
        private BaseUnit _unit = null;
        public BaseUnit Unit
        {
            get
            {
                return _unit;
            }
            set
            {
                _unit = value;
                FillUpdate();
            }
        }
        
        public UctrlLstTactView()
        {
            InitializeComponent();
        }        
        public void UpdateUI()
        {
            if (Unit == null) return;
            if (Unit.QueTactTime.Count <= 0) return;

            while (Unit.QueTactTime.Count > 0)
            {
                double tact = Unit.QueTactTime.Dequeue();

                Unit.LstTactTime.Add(tact);
                lstTactTime.Items.Add(new ListViewItem(tact.ToString("0.00#")));
            }
        }
        private void FillUpdate()
        {
            lstTactTime.Items.Clear();
            //lstTactTime.Items.AddRange(_unit.LstTactTime.Select(f => new ListViewItem(f.ToString("0.00#"))).ToArray());
        }
    }
}

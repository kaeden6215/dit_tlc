﻿namespace DIT.TLC.UI
{
    partial class UcrlAjinServoCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.btnServoOn = new System.Windows.Forms.Button();
            this.btnPtpMove = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnServoOff = new System.Windows.Forms.Button();
            this.btnServoOnOffStatus = new System.Windows.Forms.Button();
            this.btnServoHomeStatus = new System.Windows.Forms.Button();
            this.btnServoAlarmStatus = new System.Windows.Forms.Button();
            this.btnServoPlusLimitStatus = new System.Windows.Forms.Button();
            this.btnServoMinusLimitStatus = new System.Windows.Forms.Button();
            this.btnServoInpositionStatus = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRelMoveMinus = new System.Windows.Forms.Button();
            this.btnRelMovePlus = new System.Windows.Forms.Button();
            this.btnMoveJogMinus = new System.Windows.Forms.Button();
            this.btnMoveJogPlus = new System.Windows.Forms.Button();
            this.btnHomeMove = new System.Windows.Forms.Button();
            this.btnMotorStop = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMotorType = new System.Windows.Forms.Label();
            this.lblCurrPosition = new System.Windows.Forms.Label();
            this.lblCurrSpeed = new System.Windows.Forms.Label();
            this.nudSetPosition = new System.Windows.Forms.NumericUpDown();
            this.nudSetSpeed = new System.Windows.Forms.NumericUpDown();
            this.lblServoNo = new System.Windows.Forms.Label();
            this.lblServoName = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(11, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "현재 위치";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnServoOn
            // 
            this.btnServoOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnServoOn.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoOn.Location = new System.Drawing.Point(386, 40);
            this.btnServoOn.Name = "btnServoOn";
            this.btnServoOn.Size = new System.Drawing.Size(59, 45);
            this.btnServoOn.TabIndex = 2;
            this.btnServoOn.Text = "SERVO ON";
            this.btnServoOn.UseVisualStyleBackColor = false;
            this.btnServoOn.Click += new System.EventHandler(this.btnServoOn_Click);
            // 
            // btnPtpMove
            // 
            this.btnPtpMove.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPtpMove.Location = new System.Drawing.Point(388, 103);
            this.btnPtpMove.Name = "btnPtpMove";
            this.btnPtpMove.Size = new System.Drawing.Size(58, 62);
            this.btnPtpMove.TabIndex = 3;
            this.btnPtpMove.Text = "절대\r\n이동";
            this.btnPtpMove.UseVisualStyleBackColor = true;
            this.btnPtpMove.Click += new System.EventHandler(this.btnPtpMove_Click);
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(143, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "(mm)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(193, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "현재 속도";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(318, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "(mm/sec)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnServoOff
            // 
            this.btnServoOff.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoOff.Location = new System.Drawing.Point(445, 40);
            this.btnServoOff.Name = "btnServoOff";
            this.btnServoOff.Size = new System.Drawing.Size(59, 45);
            this.btnServoOff.TabIndex = 10;
            this.btnServoOff.Text = "SERVO OFF";
            this.btnServoOff.UseVisualStyleBackColor = true;
            this.btnServoOff.Click += new System.EventHandler(this.btnServoOff_Click);
            // 
            // btnServoOnOffStatus
            // 
            this.btnServoOnOffStatus.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoOnOffStatus.Location = new System.Drawing.Point(517, 40);
            this.btnServoOnOffStatus.Name = "btnServoOnOffStatus";
            this.btnServoOnOffStatus.Size = new System.Drawing.Size(59, 45);
            this.btnServoOnOffStatus.TabIndex = 11;
            this.btnServoOnOffStatus.Text = "SERVO ON";
            this.btnServoOnOffStatus.UseVisualStyleBackColor = true;
            // 
            // btnServoHomeStatus
            // 
            this.btnServoHomeStatus.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoHomeStatus.Location = new System.Drawing.Point(575, 40);
            this.btnServoHomeStatus.Name = "btnServoHomeStatus";
            this.btnServoHomeStatus.Size = new System.Drawing.Size(54, 45);
            this.btnServoHomeStatus.TabIndex = 12;
            this.btnServoHomeStatus.Text = "Home";
            this.btnServoHomeStatus.UseVisualStyleBackColor = true;
            // 
            // btnServoAlarmStatus
            // 
            this.btnServoAlarmStatus.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoAlarmStatus.Location = new System.Drawing.Point(628, 40);
            this.btnServoAlarmStatus.Name = "btnServoAlarmStatus";
            this.btnServoAlarmStatus.Size = new System.Drawing.Size(54, 45);
            this.btnServoAlarmStatus.TabIndex = 13;
            this.btnServoAlarmStatus.Text = "Alarm";
            this.btnServoAlarmStatus.UseVisualStyleBackColor = true;
            // 
            // btnServoPlusLimitStatus
            // 
            this.btnServoPlusLimitStatus.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoPlusLimitStatus.Location = new System.Drawing.Point(681, 40);
            this.btnServoPlusLimitStatus.Name = "btnServoPlusLimitStatus";
            this.btnServoPlusLimitStatus.Size = new System.Drawing.Size(54, 45);
            this.btnServoPlusLimitStatus.TabIndex = 14;
            this.btnServoPlusLimitStatus.Text = "+Limit";
            this.btnServoPlusLimitStatus.UseVisualStyleBackColor = true;
            // 
            // btnServoMinusLimitStatus
            // 
            this.btnServoMinusLimitStatus.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoMinusLimitStatus.Location = new System.Drawing.Point(734, 40);
            this.btnServoMinusLimitStatus.Name = "btnServoMinusLimitStatus";
            this.btnServoMinusLimitStatus.Size = new System.Drawing.Size(54, 45);
            this.btnServoMinusLimitStatus.TabIndex = 15;
            this.btnServoMinusLimitStatus.Text = "-Limit";
            this.btnServoMinusLimitStatus.UseVisualStyleBackColor = true;
            // 
            // btnServoInpositionStatus
            // 
            this.btnServoInpositionStatus.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.btnServoInpositionStatus.Location = new System.Drawing.Point(787, 40);
            this.btnServoInpositionStatus.Name = "btnServoInpositionStatus";
            this.btnServoInpositionStatus.Size = new System.Drawing.Size(54, 45);
            this.btnServoInpositionStatus.TabIndex = 16;
            this.btnServoInpositionStatus.Text = "InPos";
            this.btnServoInpositionStatus.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(11, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 23);
            this.label5.TabIndex = 17;
            this.label5.Text = "위치 설정";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(11, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 23);
            this.label6.TabIndex = 18;
            this.label6.Text = "속도 설정";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(193, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 23);
            this.label7.TabIndex = 21;
            this.label7.Text = "(mm)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(193, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 23);
            this.label8.TabIndex = 22;
            this.label8.Text = "(mm/sec)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRelMoveMinus
            // 
            this.btnRelMoveMinus.Enabled = false;
            this.btnRelMoveMinus.Location = new System.Drawing.Point(455, 103);
            this.btnRelMoveMinus.Name = "btnRelMoveMinus";
            this.btnRelMoveMinus.Size = new System.Drawing.Size(58, 62);
            this.btnRelMoveMinus.TabIndex = 23;
            this.btnRelMoveMinus.Text = "  -  \r\n상대\r\n이동";
            this.btnRelMoveMinus.UseVisualStyleBackColor = true;
            this.btnRelMoveMinus.Click += new System.EventHandler(this.btnRelMoveMinus_Click);
            // 
            // btnRelMovePlus
            // 
            this.btnRelMovePlus.Enabled = false;
            this.btnRelMovePlus.Location = new System.Drawing.Point(516, 103);
            this.btnRelMovePlus.Name = "btnRelMovePlus";
            this.btnRelMovePlus.Size = new System.Drawing.Size(58, 62);
            this.btnRelMovePlus.TabIndex = 24;
            this.btnRelMovePlus.Text = "  + \r\n상대\r\n이동";
            this.btnRelMovePlus.UseVisualStyleBackColor = true;
            this.btnRelMovePlus.Click += new System.EventHandler(this.btnRelMovePlus_Click);
            // 
            // btnMoveJogMinus
            // 
            this.btnMoveJogMinus.Location = new System.Drawing.Point(591, 103);
            this.btnMoveJogMinus.Name = "btnMoveJogMinus";
            this.btnMoveJogMinus.Size = new System.Drawing.Size(58, 62);
            this.btnMoveJogMinus.TabIndex = 25;
            this.btnMoveJogMinus.Text = "  -  \r\n조그\r\n이동";
            this.btnMoveJogMinus.UseVisualStyleBackColor = true;
            this.btnMoveJogMinus.Click += new System.EventHandler(this.btnMoveJogMinus_Click);
            this.btnMoveJogMinus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogMinus_MouseDown);
            this.btnMoveJogMinus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogMinus_MouseUp);
            // 
            // btnMoveJogPlus
            // 
            this.btnMoveJogPlus.Location = new System.Drawing.Point(651, 103);
            this.btnMoveJogPlus.Name = "btnMoveJogPlus";
            this.btnMoveJogPlus.Size = new System.Drawing.Size(58, 62);
            this.btnMoveJogPlus.TabIndex = 26;
            this.btnMoveJogPlus.Text = "  + \r\n조그\r\n이동";
            this.btnMoveJogPlus.UseVisualStyleBackColor = true;
            this.btnMoveJogPlus.Click += new System.EventHandler(this.btnMoveJogPlus_Click);
            this.btnMoveJogPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogPlus_MouseDown);
            this.btnMoveJogPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogPlus_MouseUp);
            // 
            // btnHomeMove
            // 
            this.btnHomeMove.Location = new System.Drawing.Point(722, 103);
            this.btnHomeMove.Name = "btnHomeMove";
            this.btnHomeMove.Size = new System.Drawing.Size(58, 62);
            this.btnHomeMove.TabIndex = 27;
            this.btnHomeMove.Text = "Home";
            this.btnHomeMove.UseVisualStyleBackColor = true;
            this.btnHomeMove.Click += new System.EventHandler(this.btnHomeMove_Click);
            // 
            // btnMotorStop
            // 
            this.btnMotorStop.Location = new System.Drawing.Point(782, 103);
            this.btnMotorStop.Name = "btnMotorStop";
            this.btnMotorStop.Size = new System.Drawing.Size(58, 62);
            this.btnMotorStop.TabIndex = 28;
            this.btnMotorStop.Text = "구동\r\n정지";
            this.btnMotorStop.UseVisualStyleBackColor = true;
            this.btnMotorStop.Click += new System.EventHandler(this.btnMotorStop_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnServoInpositionStatus);
            this.panel1.Controls.Add(this.lblMotorType);
            this.panel1.Controls.Add(this.lblCurrPosition);
            this.panel1.Controls.Add(this.lblCurrSpeed);
            this.panel1.Controls.Add(this.nudSetPosition);
            this.panel1.Controls.Add(this.nudSetSpeed);
            this.panel1.Controls.Add(this.btnMotorStop);
            this.panel1.Controls.Add(this.btnHomeMove);
            this.panel1.Controls.Add(this.btnMoveJogPlus);
            this.panel1.Controls.Add(this.btnMoveJogMinus);
            this.panel1.Controls.Add(this.btnRelMovePlus);
            this.panel1.Controls.Add(this.btnRelMoveMinus);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.btnServoMinusLimitStatus);
            this.panel1.Controls.Add(this.btnServoPlusLimitStatus);
            this.panel1.Controls.Add(this.btnServoAlarmStatus);
            this.panel1.Controls.Add(this.btnServoHomeStatus);
            this.panel1.Controls.Add(this.btnServoOnOffStatus);
            this.panel1.Controls.Add(this.btnServoOff);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnPtpMove);
            this.panel1.Controls.Add(this.btnServoOn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblServoNo);
            this.panel1.Controls.Add(this.lblServoName);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(852, 183);
            this.panel1.TabIndex = 0;
            // 
            // lblMotorType
            // 
            this.lblMotorType.AutoEllipsis = true;
            this.lblMotorType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblMotorType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMotorType.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotorType.ForeColor = System.Drawing.Color.Black;
            this.lblMotorType.Location = new System.Drawing.Point(0, 0);
            this.lblMotorType.Name = "lblMotorType";
            this.lblMotorType.Size = new System.Drawing.Size(67, 25);
            this.lblMotorType.TabIndex = 98;
            this.lblMotorType.Text = "0";
            this.lblMotorType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCurrPosition
            // 
            this.lblCurrPosition.AutoEllipsis = true;
            this.lblCurrPosition.BackColor = System.Drawing.Color.Silver;
            this.lblCurrPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCurrPosition.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrPosition.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCurrPosition.Location = new System.Drawing.Point(86, 40);
            this.lblCurrPosition.Name = "lblCurrPosition";
            this.lblCurrPosition.Size = new System.Drawing.Size(60, 20);
            this.lblCurrPosition.TabIndex = 98;
            this.lblCurrPosition.Text = "0";
            this.lblCurrPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCurrSpeed
            // 
            this.lblCurrSpeed.AutoEllipsis = true;
            this.lblCurrSpeed.BackColor = System.Drawing.Color.Silver;
            this.lblCurrSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCurrSpeed.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrSpeed.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCurrSpeed.Location = new System.Drawing.Point(250, 40);
            this.lblCurrSpeed.Name = "lblCurrSpeed";
            this.lblCurrSpeed.Size = new System.Drawing.Size(62, 21);
            this.lblCurrSpeed.TabIndex = 97;
            this.lblCurrSpeed.Text = "0";
            this.lblCurrSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nudSetPosition
            // 
            this.nudSetPosition.ForeColor = System.Drawing.SystemColors.ControlText;
            this.nudSetPosition.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudSetPosition.Location = new System.Drawing.Point(86, 80);
            this.nudSetPosition.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudSetPosition.Name = "nudSetPosition";
            this.nudSetPosition.Size = new System.Drawing.Size(101, 23);
            this.nudSetPosition.TabIndex = 96;
            this.nudSetPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudSetSpeed
            // 
            this.nudSetSpeed.ForeColor = System.Drawing.SystemColors.ControlText;
            this.nudSetSpeed.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudSetSpeed.Location = new System.Drawing.Point(87, 111);
            this.nudSetSpeed.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudSetSpeed.Name = "nudSetSpeed";
            this.nudSetSpeed.Size = new System.Drawing.Size(100, 23);
            this.nudSetSpeed.TabIndex = 95;
            this.nudSetSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblServoNo
            // 
            this.lblServoNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServoNo.AutoEllipsis = true;
            this.lblServoNo.BackColor = System.Drawing.Color.Gainsboro;
            this.lblServoNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblServoNo.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.lblServoNo.ForeColor = System.Drawing.Color.Black;
            this.lblServoNo.Location = new System.Drawing.Point(66, 0);
            this.lblServoNo.Name = "lblServoNo";
            this.lblServoNo.Size = new System.Drawing.Size(88, 25);
            this.lblServoNo.TabIndex = 99;
            this.lblServoNo.Text = "■ 축 정보";
            this.lblServoNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblServoName
            // 
            this.lblServoName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServoName.AutoEllipsis = true;
            this.lblServoName.BackColor = System.Drawing.Color.Gainsboro;
            this.lblServoName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblServoName.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.lblServoName.ForeColor = System.Drawing.Color.Black;
            this.lblServoName.Location = new System.Drawing.Point(153, 0);
            this.lblServoName.Name = "lblServoName";
            this.lblServoName.Size = new System.Drawing.Size(699, 25);
            this.lblServoName.TabIndex = 99;
            this.lblServoName.Text = "■ 축 정보";
            this.lblServoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlAjinServoCtrl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlAjinServoCtrl";
            this.Size = new System.Drawing.Size(851, 183);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetSpeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnServoOn;
        private System.Windows.Forms.Button btnPtpMove;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnServoOff;
        private System.Windows.Forms.Button btnServoOnOffStatus;
        private System.Windows.Forms.Button btnServoHomeStatus;
        private System.Windows.Forms.Button btnServoAlarmStatus;
        private System.Windows.Forms.Button btnServoPlusLimitStatus;
        private System.Windows.Forms.Button btnServoMinusLimitStatus;
        private System.Windows.Forms.Button btnServoInpositionStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnRelMoveMinus;
        private System.Windows.Forms.Button btnRelMovePlus;
        private System.Windows.Forms.Button btnMoveJogMinus;
        private System.Windows.Forms.Button btnMoveJogPlus;
        private System.Windows.Forms.Button btnHomeMove;
        private System.Windows.Forms.Button btnMotorStop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown nudSetSpeed;
        private System.Windows.Forms.NumericUpDown nudSetPosition;
        internal System.Windows.Forms.Label lblCurrPosition;
        internal System.Windows.Forms.Label lblCurrSpeed;
        private System.Windows.Forms.Timer timer1;
        internal System.Windows.Forms.Label lblMotorType;
        internal System.Windows.Forms.Label lblServoName;
        internal System.Windows.Forms.Label lblServoNo;
    }
}

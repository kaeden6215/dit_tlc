﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlAjinServoCtrl : UserControl, IUIUpdate
    {
        private ServoMotorControl _servo = null;
        public ServoMotorControl Servo
        {
            get { return _servo; }
            set
            {
                _servo = value;
                lblServoNo.Text = _servo == null ? "" : string.Format("{0}", _servo.OutterAxisNo);
                lblServoName.Text = _servo == null ? "" : string.Format("{0}", _servo.Name);
                lblMotorType.Text = _servo == null ? "" : _servo.MotorType.ToString();

                //nudSetPosition.Value = (decimal)(_servo == null ? 0f : _servo.XF_CurrMotorPosition);
                nudSetSpeed.Value = (decimal)(_servo == null ? 0f : 10f);
            }
        }

        public UcrlAjinServoCtrl()
        {
            InitializeComponent();
            nudSetPosition.Minimum = -100000;
        }
        public void UIUpdate()
        {
            if (_servo == null) return;


            lblCurrPosition.Text = _servo == null ? "" : _servo.XF_CurrMotorPosition.ToString("0.000#");
            lblCurrSpeed.Text = _servo == null ? "" : _servo.XF_CurrMotorSpeed.vFloat.ToString("0.000#");

            btnServoOnOffStatus.BackColor = _servo == null ? UiGlobal.UNSELECT_C : _servo.XB_StatusMotorServoOn.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnServoHomeStatus.BackColor = _servo == null ? UiGlobal.UNSELECT_C : _servo.XB_StatusHomeCompleteBit.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnServoAlarmStatus.BackColor = _servo == null ? UiGlobal.UNSELECT_C : _servo.XB_ErrFatalFollowingError.vBit | _servo.XB_ErrAmpFaultError.vBit | _servo.XB_ErrI2TAmpFaultError.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            btnServoPlusLimitStatus.BackColor = _servo == null ? UiGlobal.UNSELECT_C : _servo.XB_StatusPositiveLimitSet.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnServoMinusLimitStatus.BackColor = _servo == null ? UiGlobal.UNSELECT_C : _servo.XB_StatusNegativeLimitSet.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnServoInpositionStatus.BackColor = _servo == null ? UiGlobal.UNSELECT_C : _servo.XB_StatusMotorInPosition.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            btnServoOn.BackColor = _servo.ServoOnOffCmdSetp != 0 && _servo.YB_ServoOnOff.vBit == true ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnServoOff.BackColor = _servo.ServoOnOffCmdSetp != 0 && _servo.YB_ServoOnOff.vBit == false ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            btnServoOn.Enabled = _servo.YB_ServoOnOff.vBit == false;
            btnServoOff.Enabled = _servo.YB_ServoOnOff.vBit == true;


            btnPtpMove.BackColor = _servo.PtpMoveCmdStep != 0 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnHomeMove.BackColor = _servo.HommingCmdStep != 0 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;



            btnHomeMove.Enabled = GG.Equip.IsDoorOpen == false || GG.NotuseDoorInterlock == true;
            btnPtpMove.Enabled = GG.Equip.IsDoorOpen == false || GG.NotuseDoorInterlock == true;


             
            btnMoveJogMinus.Enabled = GG.Equip.IsDoorOpen == false ||
                (_servo.MotorPosi == EM_MOTOR_POSI.LD && GG.Equip.LD.SafteyEnableGripSwitchOn1.IsOnOff == true) ||
                (_servo.MotorPosi == EM_MOTOR_POSI.PROC && GG.Equip.PROC.SafetyEnableGripSwitchOn.IsOnOff == true) ||
                (_servo.MotorPosi == EM_MOTOR_POSI.UD && GG.Equip.UD.SafteyEnableGripSwitchOn3.IsOnOff == true) ||
                GG.NotuseDoorInterlock == true;

            btnMoveJogPlus.Enabled = btnMoveJogMinus.Enabled;


            //btnHomeMove.BackColor = _servo.HommingCmdStep != 0 ? UiGlobal.UNSELECT_C : _servo.XB_StatusMotorInPosition.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnHomeMove.BackColor = _servo.HommingCmdStep != 0 ? UiGlobal.UNSELECT_C : _servo.XB_StatusMotorInPosition.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnHomeMove.BackColor = _servo.HommingCmdStep != 0 ? UiGlobal.UNSELECT_C : _servo.XB_StatusMotorInPosition.vBit ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
        }
        private void btnMoveJogMinus_MouseUp(object sender, MouseEventArgs e)
        {
            if (_servo == null) return;

            btnMoveJogMinus.BackColor = UiGlobal.UNSELECT_C;
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
        private void btnMoveJogMinus_MouseDown(object sender, MouseEventArgs e)
        {
            if (_servo == null) return;

            btnMoveJogMinus.BackColor = UiGlobal.SELECT_C;
            float speed = ((float)nudSetSpeed.Value > (float)_servo.SoftJogSpeedLimit ? (float)_servo.SoftJogSpeedLimit : (float)nudSetSpeed.Value);
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_MINUS, speed);
        }
        private void btnMoveJogPlus_MouseDown(object sender, MouseEventArgs e)
        {
            if (_servo == null) return;

            btnMoveJogPlus.BackColor = UiGlobal.SELECT_C;
            float speed = ((float)nudSetSpeed.Value > (float)_servo.SoftJogSpeedLimit ? (float)_servo.SoftJogSpeedLimit : (float)nudSetSpeed.Value);
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_PLUS, speed);
        }
        private void btnMoveJogPlus_MouseUp(object sender, MouseEventArgs e)
        {
            if (_servo == null) return;

            btnMoveJogPlus.BackColor = UiGlobal.UNSELECT_C;
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
        private void btnMotorStop_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;

            _servo.ServoOnOffCmd(GG.Equip, false);
        }

        private void btnHomeMove_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;

            _servo.GoHome(GG.Equip);
        }
        private void btnRelMoveMinus_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;
        }
        private void btnPtpMove_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;
            float position = (float)nudSetPosition.Value;
            float speed = (float)nudSetSpeed.Value;
            float accel = 200;// (float)nudSetPosition.Value;

            _servo.PtpMoveCmd(GG.Equip, position, speed, accel);
        }

        private void btnServoOn_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;
            _servo.ServoOnOffCmd(GG.Equip, true);
        }

        private void btnServoOff_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;
            _servo.ServoOnOffCmd(GG.Equip, false);
        }

        private void btnMoveJogMinus_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;
        }

        private void btnRelMovePlus_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;
        }

        private void btnMoveJogPlus_Click(object sender, EventArgs e)
        {
            if (_servo == null) return;
        }
    }
}

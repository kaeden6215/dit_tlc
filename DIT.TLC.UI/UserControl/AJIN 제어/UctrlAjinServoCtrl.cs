﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlAjinServoCtrl : UserControl
    {
        private ServoMotorControl _servo = null;
        public ServoMotorControl Servo
        {
            get { return _servo; }
            set
            {
                _servo = value;
                txtServoName.Text = _servo.Name;                
            }
        }

        public UcrlAjinServoCtrl()
        {
            InitializeComponent();
        }

        public void UpdateDataUi()
        {
            lblCurrPosition.Text = _servo.XF_CurrMotorPosition.vFloat.ToString("0.000#");
            lblCurrSpeed.Text = _servo.XF_CurrMotorSpeed.vFloat.ToString("0.000#");
        }
        private void btnMoveJogMinus_MouseUp(object sender, MouseEventArgs e)
        {
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
        private void btnMoveJogMinus_MouseDown(object sender, MouseEventArgs e)
        {
            float speed = ((float)nudSetSpeed.Value > (float)_servo.SoftJogSpeedLimit * 1000.0f ? (float)_servo.SoftJogSpeedLimit : (float)nudSetSpeed.Value / 1000.0f);
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_MINUS, speed);
        }
        private void btnMoveJogPlus_MouseDown(object sender, MouseEventArgs e)
        {
            float speed = ((float)nudSetSpeed.Value > (float)_servo.SoftJogSpeedLimit * 1000.0f ? (float)_servo.SoftJogSpeedLimit : (float)nudSetSpeed.Value / 1000.0f);
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_PLUS, speed);
        }
        private void btnMoveJogPlus_MouseUp(object sender, MouseEventArgs e)
        {
            _servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
        private void btnMoveJogPlus_Click(object sender, EventArgs e)
        {

        }
    }
}

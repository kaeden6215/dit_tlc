﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool createNew;
            System.Threading.Semaphore s = new System.Threading.Semaphore(1, 2, "DIT.TLC.UI", out createNew);

            if (createNew == false)
            {
                MessageBox.Show("이미 실행중입니다.", "DIT.TLC.UI", MessageBoxButtons.OK);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMain()); 
        }
    }
}

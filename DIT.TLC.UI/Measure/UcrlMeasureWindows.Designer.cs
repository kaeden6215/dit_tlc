﻿namespace DIT.TLC.UI
{
    partial class UcrlMeasureWindows
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tciostatus_info = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnMcrBClose = new System.Windows.Forms.Button();
            this.btnMcrBOpen = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnMcrBGet = new System.Windows.Forms.Button();
            this.txtMcrB = new System.Windows.Forms.TextBox();
            this.lblMcrB = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMcrAClose = new System.Windows.Forms.Button();
            this.btnMcrAOpen = new System.Windows.Forms.Button();
            this.panel33 = new System.Windows.Forms.Panel();
            this.btnMcrAGet = new System.Windows.Forms.Button();
            this.txtMcrA = new System.Windows.Forms.TextBox();
            this.lblMcrA = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtUnloaderCheck4 = new System.Windows.Forms.TextBox();
            this.txtUnloaderCheck3 = new System.Windows.Forms.TextBox();
            this.txtUnloaderCheck2 = new System.Windows.Forms.TextBox();
            this.txtUnloaderCheck1 = new System.Windows.Forms.TextBox();
            this.lblUnloaderPosition4 = new System.Windows.Forms.Label();
            this.lblUnloaderPosition3 = new System.Windows.Forms.Label();
            this.txtUnloaderPosition4 = new System.Windows.Forms.TextBox();
            this.txtUnloaderPosition3 = new System.Windows.Forms.TextBox();
            this.lblUnloaderPosition2 = new System.Windows.Forms.Label();
            this.lblUnloaderPosition1 = new System.Windows.Forms.Label();
            this.txtUnloaderPosition2 = new System.Windows.Forms.TextBox();
            this.txtUnloaderPosition1 = new System.Windows.Forms.TextBox();
            this.lblModuleUnloader = new System.Windows.Forms.Label();
            this.btnTiltUnloaderClose = new System.Windows.Forms.Button();
            this.btnTiltUnloaderOpen = new System.Windows.Forms.Button();
            this.btnTiltLoaderClose = new System.Windows.Forms.Button();
            this.btnTiltLoaderOpen = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtLoaderCheck4 = new System.Windows.Forms.TextBox();
            this.txtLoaderCheck3 = new System.Windows.Forms.TextBox();
            this.txtLoaderCheck2 = new System.Windows.Forms.TextBox();
            this.txtLoaderCheck1 = new System.Windows.Forms.TextBox();
            this.lblLoaderPosition4 = new System.Windows.Forms.Label();
            this.lblLoaderPosition3 = new System.Windows.Forms.Label();
            this.txtLoaderPosition4 = new System.Windows.Forms.TextBox();
            this.txtLoaderPosition3 = new System.Windows.Forms.TextBox();
            this.lblLoaderPosition2 = new System.Windows.Forms.Label();
            this.lblLoaderPosition1 = new System.Windows.Forms.Label();
            this.txtLoaderPosition2 = new System.Windows.Forms.TextBox();
            this.txtLoaderPosition1 = new System.Windows.Forms.TextBox();
            this.lblModuleLoader = new System.Windows.Forms.Label();
            this.lblTiltSensor = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnControllerSave = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn2ChCurrentBrightSet = new System.Windows.Forms.Button();
            this.btn1ChCurrentBrightSet = new System.Windows.Forms.Button();
            this.lbl2ChCurrentBrightSet = new System.Windows.Forms.Label();
            this.lbl1ChCurrentBrightSet = new System.Windows.Forms.Label();
            this.txt2ChCurrentBrightSet = new System.Windows.Forms.TextBox();
            this.txt1ChCurrentBrightSet = new System.Windows.Forms.TextBox();
            this.lblBrightSetting = new System.Windows.Forms.Label();
            this.btnBrightControllerClose = new System.Windows.Forms.Button();
            this.btnBrightControllerOpen = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lbl2ChBrightValue = new System.Windows.Forms.Label();
            this.lbl1ChBrightValue = new System.Windows.Forms.Label();
            this.txt2ChBrightValue = new System.Windows.Forms.TextBox();
            this.txt1ChBrightValue = new System.Windows.Forms.TextBox();
            this.lblCurrentBright = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnUnloaderHandyBarcodeClose = new System.Windows.Forms.Button();
            this.btnUnloaderHandyBarcodeOpen = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnUnloaderHandyBarcodeGet = new System.Windows.Forms.Button();
            this.txtUnloaderHandyBarcode = new System.Windows.Forms.TextBox();
            this.lblUnloaderHandyBarcodeReader = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnUnloaderBarcodeBClose = new System.Windows.Forms.Button();
            this.btnUnloaderBarcodeBOpen = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnUnloaderBarcodeBGet = new System.Windows.Forms.Button();
            this.txtUnloaderBarcodeB = new System.Windows.Forms.TextBox();
            this.lblUnloaderFixBarcodeReaderB = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.btnLoaderBarcodeBClose = new System.Windows.Forms.Button();
            this.btnLoaderBarcodeBOpen = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btnLoaderBarcodeBGet = new System.Windows.Forms.Button();
            this.txtLoaderBarcodeB = new System.Windows.Forms.TextBox();
            this.lblLoaderFixBarcodeReaderB = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnLoaderHandyBarcodeClose = new System.Windows.Forms.Button();
            this.btnLoaderHandyBarcodeOpen = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnLoaderHandyBarcodeGet = new System.Windows.Forms.Button();
            this.txtLoaderHandyBarcode = new System.Windows.Forms.TextBox();
            this.lblLoaderHandyBarcodeReader = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnUnloaderBarcodeAClose = new System.Windows.Forms.Button();
            this.btnUnloaderBarcodeAOpen = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnUnloaderBarcodeAGet = new System.Windows.Forms.Button();
            this.txtUnloaderBarcodeA = new System.Windows.Forms.TextBox();
            this.lblUnloaderFixBarcodeReaderA = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnLoaderBarcodeAClose = new System.Windows.Forms.Button();
            this.btnLoaderBarcodeAOpen = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnLoaderBarcodeAGet = new System.Windows.Forms.Button();
            this.txtLoaderBarcodeA = new System.Windows.Forms.TextBox();
            this.lblLoaderFixBarcodeReaderA = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel22 = new System.Windows.Forms.Panel();
            this.btnLdsBreakSave = new System.Windows.Forms.Button();
            this.txtLdsBreakState = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtLdsBreakTableIdx = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtLdsBreakMeasureTime = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLdsBreakReadyTime = new System.Windows.Forms.TextBox();
            this.btnLdsBreakStop = new System.Windows.Forms.Button();
            this.btnLdsBreakStart = new System.Windows.Forms.Button();
            this.panel23 = new System.Windows.Forms.Panel();
            this.lviLdsBreak = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label39 = new System.Windows.Forms.Label();
            this.txtLdsBreakYInterval = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtLdsBreakValue9 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtLdsBreakValue6 = new System.Windows.Forms.TextBox();
            this.txtLdsBreakValue3 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtLdsBreakValue8 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txtLdsBreakValue5 = new System.Windows.Forms.TextBox();
            this.txtLdsBreakValue2 = new System.Windows.Forms.TextBox();
            this.txtLdsBreakCurrentValue = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.txtLdsBreakXInterval = new System.Windows.Forms.TextBox();
            this.txtLdsBreakValue7 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtLdsBreakValue4 = new System.Windows.Forms.TextBox();
            this.txtLdsBreakValue1 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.btnAutoCalibrationAutoCalStop = new System.Windows.Forms.Button();
            this.btnAutoCalibrationAutoCal = new System.Windows.Forms.Button();
            this.txtAutoCalibrationTargetPower = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtAutoCalibrationCurrentPower = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnPowerMeterSave = new System.Windows.Forms.Button();
            this.panel25 = new System.Windows.Forms.Panel();
            this.txtMeasureUpTime = new System.Windows.Forms.TextBox();
            this.btnMeasureSettingMeasureStop = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtRemainMeasureUpTime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWarmUpTime = new System.Windows.Forms.TextBox();
            this.btnMeasureSettingMeasure = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtRemainWarmUpTime = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.txtPowerStatusAvgPower = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPowerStatusLowPower = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPowerStatusMaxPower = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPowerStatusCureentPower = new System.Windows.Forms.TextBox();
            this.lblPowerStatus = new System.Windows.Forms.Label();
            this.btnPowerMeterClose = new System.Windows.Forms.Button();
            this.btnPowerMeterOpen = new System.Windows.Forms.Button();
            this.lblPowerMeter = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.btnLdsPorcessSave = new System.Windows.Forms.Button();
            this.txtLdsProcessState = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtLdsProcessTableIdx = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtLdsProcessMeasureTime = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtLdsProcessReadyTime = new System.Windows.Forms.TextBox();
            this.btnLdsProcessStop = new System.Windows.Forms.Button();
            this.btnLdsProcessStart = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.lviLdsProcess = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label41 = new System.Windows.Forms.Label();
            this.txtLdsProcessYInterval = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLdsProcessValue9 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtLdsProcessValue6 = new System.Windows.Forms.TextBox();
            this.txtLdsProcessValue3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLdsProcessValue8 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLdsProcessValue5 = new System.Windows.Forms.TextBox();
            this.txtLdsProcessValue2 = new System.Windows.Forms.TextBox();
            this.txtLdsProcessCurrentValue = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtLdsProcessXInterval = new System.Windows.Forms.TextBox();
            this.txtLdsProcessValue7 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLdsProcessValue4 = new System.Windows.Forms.TextBox();
            this.txtLdsProcessValue1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblLdsProcess = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel27 = new System.Windows.Forms.Panel();
            this.btnCutLineColBR2Table = new System.Windows.Forms.Button();
            this.btnCutLineColBR1Table = new System.Windows.Forms.Button();
            this.btnCutLineColAL2Table = new System.Windows.Forms.Button();
            this.btnCutLineColAL1Table = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel36 = new System.Windows.Forms.Panel();
            this.cboAlignCmd = new System.Windows.Forms.ComboBox();
            this.btnAlignConnect = new System.Windows.Forms.Button();
            this.btnAlignCmd = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.cboInspCmd = new System.Windows.Forms.ComboBox();
            this.btnInspConnect = new System.Windows.Forms.Button();
            this.btnInspCmd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBeam = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.lblBeamPowerSetting = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtBeamPosXMiniValue = new System.Windows.Forms.TextBox();
            this.txtBeamPosXCurrenValue = new System.Windows.Forms.TextBox();
            this.txtBeamPowerMiniValue = new System.Windows.Forms.TextBox();
            this.txtBeamPowerCurrenValue = new System.Windows.Forms.TextBox();
            this.txtBeamPosYMiniValue = new System.Windows.Forms.TextBox();
            this.txtBeamPosYCurrenValue = new System.Windows.Forms.TextBox();
            this.txtBeamPowerMaxValue = new System.Windows.Forms.TextBox();
            this.txtBeamPowerMedianValue = new System.Windows.Forms.TextBox();
            this.txtBeamPosXMaxValue = new System.Windows.Forms.TextBox();
            this.txtBeamPosXMedianValue = new System.Windows.Forms.TextBox();
            this.txtBeamPosYMaxValue = new System.Windows.Forms.TextBox();
            this.txtBeamPosYMedianValue = new System.Windows.Forms.TextBox();
            this.btnBeamSettingStop = new System.Windows.Forms.Button();
            this.btnBeamSettingStart = new System.Windows.Forms.Button();
            this.btnBeamOpen = new System.Windows.Forms.Button();
            this.btnBeamClose = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.lblLimitValueSetting = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtTeachingPower = new System.Windows.Forms.TextBox();
            this.txtTeachingX = new System.Windows.Forms.TextBox();
            this.txtErrorPower = new System.Windows.Forms.TextBox();
            this.txtTeachingY = new System.Windows.Forms.TextBox();
            this.txtErrorPos = new System.Windows.Forms.TextBox();
            this.btnLimitValueSave = new System.Windows.Forms.Button();
            this.panel30 = new System.Windows.Forms.Panel();
            this.tciostatus_info.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel29.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel27.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.SuspendLayout();
            // 
            // tciostatus_info
            // 
            this.tciostatus_info.Controls.Add(this.tabPage1);
            this.tciostatus_info.Controls.Add(this.tabPage2);
            this.tciostatus_info.Controls.Add(this.tabPage3);
            this.tciostatus_info.Controls.Add(this.tabPage4);
            this.tciostatus_info.Controls.Add(this.tabPage5);
            this.tciostatus_info.ItemSize = new System.Drawing.Size(300, 40);
            this.tciostatus_info.Location = new System.Drawing.Point(8, 8);
            this.tciostatus_info.Name = "tciostatus_info";
            this.tciostatus_info.SelectedIndex = 0;
            this.tciostatus_info.Size = new System.Drawing.Size(1869, 850);
            this.tciostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tciostatus_info.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Font = new System.Drawing.Font("Gulim", 20F);
            this.tabPage1.Location = new System.Drawing.Point(4, 44);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1861, 802);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "조명 컨트롤 / MCR / TiltSensor";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnMcrBClose);
            this.panel3.Controls.Add(this.btnMcrBOpen);
            this.panel3.Controls.Add(this.panel9);
            this.panel3.Controls.Add(this.lblMcrB);
            this.panel3.Location = new System.Drawing.Point(933, 405);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(923, 393);
            this.panel3.TabIndex = 459;
            // 
            // btnMcrBClose
            // 
            this.btnMcrBClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrBClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcrBClose.ForeColor = System.Drawing.Color.Black;
            this.btnMcrBClose.Location = new System.Drawing.Point(102, 33);
            this.btnMcrBClose.Name = "btnMcrBClose";
            this.btnMcrBClose.Size = new System.Drawing.Size(93, 40);
            this.btnMcrBClose.TabIndex = 459;
            this.btnMcrBClose.Text = "Close";
            this.btnMcrBClose.UseVisualStyleBackColor = false;
            this.btnMcrBClose.Click += new System.EventHandler(this.btnMcr_Click);
            // 
            // btnMcrBOpen
            // 
            this.btnMcrBOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrBOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcrBOpen.ForeColor = System.Drawing.Color.Black;
            this.btnMcrBOpen.Location = new System.Drawing.Point(3, 33);
            this.btnMcrBOpen.Name = "btnMcrBOpen";
            this.btnMcrBOpen.Size = new System.Drawing.Size(93, 40);
            this.btnMcrBOpen.TabIndex = 458;
            this.btnMcrBOpen.Text = "Open";
            this.btnMcrBOpen.UseVisualStyleBackColor = false;
            this.btnMcrBOpen.Click += new System.EventHandler(this.btnMcr_Click);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnMcrBGet);
            this.panel9.Controls.Add(this.txtMcrB);
            this.panel9.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel9.Location = new System.Drawing.Point(3, 79);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(915, 309);
            this.panel9.TabIndex = 456;
            // 
            // btnMcrBGet
            // 
            this.btnMcrBGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrBGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnMcrBGet.ForeColor = System.Drawing.Color.Black;
            this.btnMcrBGet.Location = new System.Drawing.Point(173, 110);
            this.btnMcrBGet.Name = "btnMcrBGet";
            this.btnMcrBGet.Size = new System.Drawing.Size(166, 78);
            this.btnMcrBGet.TabIndex = 31;
            this.btnMcrBGet.Text = "Get";
            this.btnMcrBGet.UseVisualStyleBackColor = false;
            this.btnMcrBGet.Click += new System.EventHandler(this.btnMcr_Click);
            // 
            // txtMcrB
            // 
            this.txtMcrB.BackColor = System.Drawing.Color.White;
            this.txtMcrB.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtMcrB.Location = new System.Drawing.Point(374, 110);
            this.txtMcrB.Name = "txtMcrB";
            this.txtMcrB.Size = new System.Drawing.Size(451, 78);
            this.txtMcrB.TabIndex = 30;
            // 
            // lblMcrB
            // 
            this.lblMcrB.AutoEllipsis = true;
            this.lblMcrB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMcrB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMcrB.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblMcrB.ForeColor = System.Drawing.Color.Black;
            this.lblMcrB.Location = new System.Drawing.Point(0, 0);
            this.lblMcrB.Name = "lblMcrB";
            this.lblMcrB.Size = new System.Drawing.Size(921, 30);
            this.lblMcrB.TabIndex = 9;
            this.lblMcrB.Text = "■ MCR B";
            this.lblMcrB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnMcrAClose);
            this.panel2.Controls.Add(this.btnMcrAOpen);
            this.panel2.Controls.Add(this.panel33);
            this.panel2.Controls.Add(this.lblMcrA);
            this.panel2.Location = new System.Drawing.Point(932, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(923, 393);
            this.panel2.TabIndex = 458;
            // 
            // btnMcrAClose
            // 
            this.btnMcrAClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrAClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcrAClose.ForeColor = System.Drawing.Color.Black;
            this.btnMcrAClose.Location = new System.Drawing.Point(102, 33);
            this.btnMcrAClose.Name = "btnMcrAClose";
            this.btnMcrAClose.Size = new System.Drawing.Size(93, 40);
            this.btnMcrAClose.TabIndex = 459;
            this.btnMcrAClose.Text = "Close";
            this.btnMcrAClose.UseVisualStyleBackColor = false;
            this.btnMcrAClose.Click += new System.EventHandler(this.btnMcr_Click);
            // 
            // btnMcrAOpen
            // 
            this.btnMcrAOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrAOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcrAOpen.ForeColor = System.Drawing.Color.Black;
            this.btnMcrAOpen.Location = new System.Drawing.Point(3, 33);
            this.btnMcrAOpen.Name = "btnMcrAOpen";
            this.btnMcrAOpen.Size = new System.Drawing.Size(93, 40);
            this.btnMcrAOpen.TabIndex = 458;
            this.btnMcrAOpen.Text = "Open";
            this.btnMcrAOpen.UseVisualStyleBackColor = false;
            this.btnMcrAOpen.Click += new System.EventHandler(this.btnMcr_Click);
            // 
            // panel33
            // 
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Controls.Add(this.btnMcrAGet);
            this.panel33.Controls.Add(this.txtMcrA);
            this.panel33.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel33.Location = new System.Drawing.Point(3, 79);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(915, 309);
            this.panel33.TabIndex = 456;
            // 
            // btnMcrAGet
            // 
            this.btnMcrAGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrAGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnMcrAGet.ForeColor = System.Drawing.Color.Black;
            this.btnMcrAGet.Location = new System.Drawing.Point(173, 110);
            this.btnMcrAGet.Name = "btnMcrAGet";
            this.btnMcrAGet.Size = new System.Drawing.Size(166, 78);
            this.btnMcrAGet.TabIndex = 31;
            this.btnMcrAGet.Text = "Get";
            this.btnMcrAGet.UseVisualStyleBackColor = false;
            this.btnMcrAGet.Click += new System.EventHandler(this.btnMcr_Click);
            // 
            // txtMcrA
            // 
            this.txtMcrA.BackColor = System.Drawing.Color.White;
            this.txtMcrA.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtMcrA.Location = new System.Drawing.Point(374, 110);
            this.txtMcrA.Name = "txtMcrA";
            this.txtMcrA.Size = new System.Drawing.Size(451, 78);
            this.txtMcrA.TabIndex = 30;
            // 
            // lblMcrA
            // 
            this.lblMcrA.AutoEllipsis = true;
            this.lblMcrA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMcrA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMcrA.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblMcrA.ForeColor = System.Drawing.Color.Black;
            this.lblMcrA.Location = new System.Drawing.Point(0, 0);
            this.lblMcrA.Name = "lblMcrA";
            this.lblMcrA.Size = new System.Drawing.Size(921, 30);
            this.lblMcrA.TabIndex = 9;
            this.lblMcrA.Text = "■ MCR A";
            this.lblMcrA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.btnTiltUnloaderClose);
            this.panel4.Controls.Add(this.btnTiltUnloaderOpen);
            this.panel4.Controls.Add(this.btnTiltLoaderClose);
            this.panel4.Controls.Add(this.btnTiltLoaderOpen);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.lblTiltSensor);
            this.panel4.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel4.Location = new System.Drawing.Point(4, 405);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(923, 393);
            this.panel4.TabIndex = 457;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.txtUnloaderCheck4);
            this.panel7.Controls.Add(this.txtUnloaderCheck3);
            this.panel7.Controls.Add(this.txtUnloaderCheck2);
            this.panel7.Controls.Add(this.txtUnloaderCheck1);
            this.panel7.Controls.Add(this.lblUnloaderPosition4);
            this.panel7.Controls.Add(this.lblUnloaderPosition3);
            this.panel7.Controls.Add(this.txtUnloaderPosition4);
            this.panel7.Controls.Add(this.txtUnloaderPosition3);
            this.panel7.Controls.Add(this.lblUnloaderPosition2);
            this.panel7.Controls.Add(this.lblUnloaderPosition1);
            this.panel7.Controls.Add(this.txtUnloaderPosition2);
            this.panel7.Controls.Add(this.txtUnloaderPosition1);
            this.panel7.Controls.Add(this.lblModuleUnloader);
            this.panel7.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel7.Location = new System.Drawing.Point(462, 79);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(455, 309);
            this.panel7.TabIndex = 462;
            // 
            // txtUnloaderCheck4
            // 
            this.txtUnloaderCheck4.BackColor = System.Drawing.Color.Red;
            this.txtUnloaderCheck4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderCheck4.ForeColor = System.Drawing.Color.White;
            this.txtUnloaderCheck4.Location = new System.Drawing.Point(350, 214);
            this.txtUnloaderCheck4.Name = "txtUnloaderCheck4";
            this.txtUnloaderCheck4.Size = new System.Drawing.Size(93, 29);
            this.txtUnloaderCheck4.TabIndex = 42;
            this.txtUnloaderCheck4.Text = "NG";
            this.txtUnloaderCheck4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtUnloaderCheck3
            // 
            this.txtUnloaderCheck3.BackColor = System.Drawing.Color.Red;
            this.txtUnloaderCheck3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderCheck3.ForeColor = System.Drawing.Color.White;
            this.txtUnloaderCheck3.Location = new System.Drawing.Point(350, 167);
            this.txtUnloaderCheck3.Name = "txtUnloaderCheck3";
            this.txtUnloaderCheck3.Size = new System.Drawing.Size(93, 29);
            this.txtUnloaderCheck3.TabIndex = 41;
            this.txtUnloaderCheck3.Text = "NG";
            this.txtUnloaderCheck3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtUnloaderCheck2
            // 
            this.txtUnloaderCheck2.BackColor = System.Drawing.Color.Red;
            this.txtUnloaderCheck2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderCheck2.ForeColor = System.Drawing.Color.White;
            this.txtUnloaderCheck2.Location = new System.Drawing.Point(350, 121);
            this.txtUnloaderCheck2.Name = "txtUnloaderCheck2";
            this.txtUnloaderCheck2.Size = new System.Drawing.Size(93, 29);
            this.txtUnloaderCheck2.TabIndex = 40;
            this.txtUnloaderCheck2.Text = "NG";
            this.txtUnloaderCheck2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtUnloaderCheck1
            // 
            this.txtUnloaderCheck1.BackColor = System.Drawing.Color.Red;
            this.txtUnloaderCheck1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderCheck1.ForeColor = System.Drawing.Color.White;
            this.txtUnloaderCheck1.Location = new System.Drawing.Point(350, 73);
            this.txtUnloaderCheck1.Name = "txtUnloaderCheck1";
            this.txtUnloaderCheck1.Size = new System.Drawing.Size(93, 29);
            this.txtUnloaderCheck1.TabIndex = 39;
            this.txtUnloaderCheck1.Text = "NG";
            this.txtUnloaderCheck1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblUnloaderPosition4
            // 
            this.lblUnloaderPosition4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloaderPosition4.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloaderPosition4.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderPosition4.Location = new System.Drawing.Point(3, 215);
            this.lblUnloaderPosition4.Name = "lblUnloaderPosition4";
            this.lblUnloaderPosition4.Size = new System.Drawing.Size(204, 29);
            this.lblUnloaderPosition4.TabIndex = 38;
            this.lblUnloaderPosition4.Text = "Position B2 [mm]";
            this.lblUnloaderPosition4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloaderPosition3
            // 
            this.lblUnloaderPosition3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloaderPosition3.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloaderPosition3.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderPosition3.Location = new System.Drawing.Point(3, 167);
            this.lblUnloaderPosition3.Name = "lblUnloaderPosition3";
            this.lblUnloaderPosition3.Size = new System.Drawing.Size(204, 29);
            this.lblUnloaderPosition3.TabIndex = 37;
            this.lblUnloaderPosition3.Text = "Position B1 [mm]";
            this.lblUnloaderPosition3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUnloaderPosition4
            // 
            this.txtUnloaderPosition4.BackColor = System.Drawing.Color.White;
            this.txtUnloaderPosition4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderPosition4.Location = new System.Drawing.Point(213, 214);
            this.txtUnloaderPosition4.Name = "txtUnloaderPosition4";
            this.txtUnloaderPosition4.Size = new System.Drawing.Size(131, 29);
            this.txtUnloaderPosition4.TabIndex = 36;
            // 
            // txtUnloaderPosition3
            // 
            this.txtUnloaderPosition3.BackColor = System.Drawing.Color.White;
            this.txtUnloaderPosition3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderPosition3.Location = new System.Drawing.Point(213, 167);
            this.txtUnloaderPosition3.Name = "txtUnloaderPosition3";
            this.txtUnloaderPosition3.Size = new System.Drawing.Size(131, 29);
            this.txtUnloaderPosition3.TabIndex = 35;
            // 
            // lblUnloaderPosition2
            // 
            this.lblUnloaderPosition2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloaderPosition2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloaderPosition2.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderPosition2.Location = new System.Drawing.Point(3, 121);
            this.lblUnloaderPosition2.Name = "lblUnloaderPosition2";
            this.lblUnloaderPosition2.Size = new System.Drawing.Size(204, 29);
            this.lblUnloaderPosition2.TabIndex = 34;
            this.lblUnloaderPosition2.Text = "Position A2 [mm]";
            this.lblUnloaderPosition2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloaderPosition1
            // 
            this.lblUnloaderPosition1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloaderPosition1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloaderPosition1.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderPosition1.Location = new System.Drawing.Point(3, 73);
            this.lblUnloaderPosition1.Name = "lblUnloaderPosition1";
            this.lblUnloaderPosition1.Size = new System.Drawing.Size(204, 29);
            this.lblUnloaderPosition1.TabIndex = 33;
            this.lblUnloaderPosition1.Text = "Position A1 [mm]";
            this.lblUnloaderPosition1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUnloaderPosition2
            // 
            this.txtUnloaderPosition2.BackColor = System.Drawing.Color.White;
            this.txtUnloaderPosition2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderPosition2.Location = new System.Drawing.Point(213, 121);
            this.txtUnloaderPosition2.Name = "txtUnloaderPosition2";
            this.txtUnloaderPosition2.Size = new System.Drawing.Size(131, 29);
            this.txtUnloaderPosition2.TabIndex = 32;
            // 
            // txtUnloaderPosition1
            // 
            this.txtUnloaderPosition1.BackColor = System.Drawing.Color.White;
            this.txtUnloaderPosition1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloaderPosition1.Location = new System.Drawing.Point(213, 73);
            this.txtUnloaderPosition1.Name = "txtUnloaderPosition1";
            this.txtUnloaderPosition1.Size = new System.Drawing.Size(131, 29);
            this.txtUnloaderPosition1.TabIndex = 31;
            // 
            // lblModuleUnloader
            // 
            this.lblModuleUnloader.AutoEllipsis = true;
            this.lblModuleUnloader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblModuleUnloader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblModuleUnloader.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblModuleUnloader.ForeColor = System.Drawing.Color.Black;
            this.lblModuleUnloader.Location = new System.Drawing.Point(0, 0);
            this.lblModuleUnloader.Name = "lblModuleUnloader";
            this.lblModuleUnloader.Size = new System.Drawing.Size(453, 28);
            this.lblModuleUnloader.TabIndex = 9;
            this.lblModuleUnloader.Text = "■ Module Unloader";
            this.lblModuleUnloader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnTiltUnloaderClose
            // 
            this.btnTiltUnloaderClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnTiltUnloaderClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTiltUnloaderClose.ForeColor = System.Drawing.Color.Black;
            this.btnTiltUnloaderClose.Location = new System.Drawing.Point(561, 33);
            this.btnTiltUnloaderClose.Name = "btnTiltUnloaderClose";
            this.btnTiltUnloaderClose.Size = new System.Drawing.Size(93, 40);
            this.btnTiltUnloaderClose.TabIndex = 461;
            this.btnTiltUnloaderClose.Text = "Close";
            this.btnTiltUnloaderClose.UseVisualStyleBackColor = false;
            this.btnTiltUnloaderClose.Click += new System.EventHandler(this.btnTiltSensor_Click);
            // 
            // btnTiltUnloaderOpen
            // 
            this.btnTiltUnloaderOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnTiltUnloaderOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTiltUnloaderOpen.ForeColor = System.Drawing.Color.Black;
            this.btnTiltUnloaderOpen.Location = new System.Drawing.Point(462, 33);
            this.btnTiltUnloaderOpen.Name = "btnTiltUnloaderOpen";
            this.btnTiltUnloaderOpen.Size = new System.Drawing.Size(93, 40);
            this.btnTiltUnloaderOpen.TabIndex = 460;
            this.btnTiltUnloaderOpen.Text = "Open";
            this.btnTiltUnloaderOpen.UseVisualStyleBackColor = false;
            this.btnTiltUnloaderOpen.Click += new System.EventHandler(this.btnTiltSensor_Click);
            // 
            // btnTiltLoaderClose
            // 
            this.btnTiltLoaderClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnTiltLoaderClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTiltLoaderClose.ForeColor = System.Drawing.Color.Black;
            this.btnTiltLoaderClose.Location = new System.Drawing.Point(102, 33);
            this.btnTiltLoaderClose.Name = "btnTiltLoaderClose";
            this.btnTiltLoaderClose.Size = new System.Drawing.Size(93, 40);
            this.btnTiltLoaderClose.TabIndex = 459;
            this.btnTiltLoaderClose.Text = "Close";
            this.btnTiltLoaderClose.UseVisualStyleBackColor = false;
            this.btnTiltLoaderClose.Click += new System.EventHandler(this.btnTiltSensor_Click);
            // 
            // btnTiltLoaderOpen
            // 
            this.btnTiltLoaderOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnTiltLoaderOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTiltLoaderOpen.ForeColor = System.Drawing.Color.Black;
            this.btnTiltLoaderOpen.Location = new System.Drawing.Point(3, 33);
            this.btnTiltLoaderOpen.Name = "btnTiltLoaderOpen";
            this.btnTiltLoaderOpen.Size = new System.Drawing.Size(93, 40);
            this.btnTiltLoaderOpen.TabIndex = 458;
            this.btnTiltLoaderOpen.Text = "Open";
            this.btnTiltLoaderOpen.UseVisualStyleBackColor = false;
            this.btnTiltLoaderOpen.Click += new System.EventHandler(this.btnTiltSensor_Click);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.txtLoaderCheck4);
            this.panel8.Controls.Add(this.txtLoaderCheck3);
            this.panel8.Controls.Add(this.txtLoaderCheck2);
            this.panel8.Controls.Add(this.txtLoaderCheck1);
            this.panel8.Controls.Add(this.lblLoaderPosition4);
            this.panel8.Controls.Add(this.lblLoaderPosition3);
            this.panel8.Controls.Add(this.txtLoaderPosition4);
            this.panel8.Controls.Add(this.txtLoaderPosition3);
            this.panel8.Controls.Add(this.lblLoaderPosition2);
            this.panel8.Controls.Add(this.lblLoaderPosition1);
            this.panel8.Controls.Add(this.txtLoaderPosition2);
            this.panel8.Controls.Add(this.txtLoaderPosition1);
            this.panel8.Controls.Add(this.lblModuleLoader);
            this.panel8.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel8.Location = new System.Drawing.Point(3, 79);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(455, 309);
            this.panel8.TabIndex = 456;
            // 
            // txtLoaderCheck4
            // 
            this.txtLoaderCheck4.BackColor = System.Drawing.Color.Red;
            this.txtLoaderCheck4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderCheck4.ForeColor = System.Drawing.Color.White;
            this.txtLoaderCheck4.Location = new System.Drawing.Point(350, 214);
            this.txtLoaderCheck4.Name = "txtLoaderCheck4";
            this.txtLoaderCheck4.Size = new System.Drawing.Size(93, 29);
            this.txtLoaderCheck4.TabIndex = 42;
            this.txtLoaderCheck4.Text = "NG";
            this.txtLoaderCheck4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLoaderCheck3
            // 
            this.txtLoaderCheck3.BackColor = System.Drawing.Color.Red;
            this.txtLoaderCheck3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderCheck3.ForeColor = System.Drawing.Color.White;
            this.txtLoaderCheck3.Location = new System.Drawing.Point(350, 167);
            this.txtLoaderCheck3.Name = "txtLoaderCheck3";
            this.txtLoaderCheck3.Size = new System.Drawing.Size(93, 29);
            this.txtLoaderCheck3.TabIndex = 41;
            this.txtLoaderCheck3.Text = "NG";
            this.txtLoaderCheck3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLoaderCheck2
            // 
            this.txtLoaderCheck2.BackColor = System.Drawing.Color.Red;
            this.txtLoaderCheck2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderCheck2.ForeColor = System.Drawing.Color.White;
            this.txtLoaderCheck2.Location = new System.Drawing.Point(350, 121);
            this.txtLoaderCheck2.Name = "txtLoaderCheck2";
            this.txtLoaderCheck2.Size = new System.Drawing.Size(93, 29);
            this.txtLoaderCheck2.TabIndex = 40;
            this.txtLoaderCheck2.Text = "NG";
            this.txtLoaderCheck2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLoaderCheck1
            // 
            this.txtLoaderCheck1.BackColor = System.Drawing.Color.Red;
            this.txtLoaderCheck1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderCheck1.ForeColor = System.Drawing.Color.White;
            this.txtLoaderCheck1.Location = new System.Drawing.Point(350, 73);
            this.txtLoaderCheck1.Name = "txtLoaderCheck1";
            this.txtLoaderCheck1.Size = new System.Drawing.Size(93, 29);
            this.txtLoaderCheck1.TabIndex = 39;
            this.txtLoaderCheck1.Text = "NG";
            this.txtLoaderCheck1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblLoaderPosition4
            // 
            this.lblLoaderPosition4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoaderPosition4.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoaderPosition4.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderPosition4.Location = new System.Drawing.Point(3, 215);
            this.lblLoaderPosition4.Name = "lblLoaderPosition4";
            this.lblLoaderPosition4.Size = new System.Drawing.Size(204, 29);
            this.lblLoaderPosition4.TabIndex = 38;
            this.lblLoaderPosition4.Text = "Position B2 [mm]";
            this.lblLoaderPosition4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoaderPosition3
            // 
            this.lblLoaderPosition3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoaderPosition3.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoaderPosition3.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderPosition3.Location = new System.Drawing.Point(3, 167);
            this.lblLoaderPosition3.Name = "lblLoaderPosition3";
            this.lblLoaderPosition3.Size = new System.Drawing.Size(204, 29);
            this.lblLoaderPosition3.TabIndex = 37;
            this.lblLoaderPosition3.Text = "Position B1 [mm]";
            this.lblLoaderPosition3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLoaderPosition4
            // 
            this.txtLoaderPosition4.BackColor = System.Drawing.Color.White;
            this.txtLoaderPosition4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderPosition4.Location = new System.Drawing.Point(213, 214);
            this.txtLoaderPosition4.Name = "txtLoaderPosition4";
            this.txtLoaderPosition4.Size = new System.Drawing.Size(131, 29);
            this.txtLoaderPosition4.TabIndex = 36;
            // 
            // txtLoaderPosition3
            // 
            this.txtLoaderPosition3.BackColor = System.Drawing.Color.White;
            this.txtLoaderPosition3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderPosition3.Location = new System.Drawing.Point(213, 167);
            this.txtLoaderPosition3.Name = "txtLoaderPosition3";
            this.txtLoaderPosition3.Size = new System.Drawing.Size(131, 29);
            this.txtLoaderPosition3.TabIndex = 35;
            // 
            // lblLoaderPosition2
            // 
            this.lblLoaderPosition2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoaderPosition2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoaderPosition2.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderPosition2.Location = new System.Drawing.Point(3, 121);
            this.lblLoaderPosition2.Name = "lblLoaderPosition2";
            this.lblLoaderPosition2.Size = new System.Drawing.Size(204, 29);
            this.lblLoaderPosition2.TabIndex = 34;
            this.lblLoaderPosition2.Text = "Position A2 [mm]";
            this.lblLoaderPosition2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoaderPosition1
            // 
            this.lblLoaderPosition1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoaderPosition1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoaderPosition1.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderPosition1.Location = new System.Drawing.Point(3, 73);
            this.lblLoaderPosition1.Name = "lblLoaderPosition1";
            this.lblLoaderPosition1.Size = new System.Drawing.Size(204, 29);
            this.lblLoaderPosition1.TabIndex = 33;
            this.lblLoaderPosition1.Text = "Position A1 [mm]";
            this.lblLoaderPosition1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLoaderPosition2
            // 
            this.txtLoaderPosition2.BackColor = System.Drawing.Color.White;
            this.txtLoaderPosition2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderPosition2.Location = new System.Drawing.Point(213, 121);
            this.txtLoaderPosition2.Name = "txtLoaderPosition2";
            this.txtLoaderPosition2.Size = new System.Drawing.Size(131, 29);
            this.txtLoaderPosition2.TabIndex = 32;
            // 
            // txtLoaderPosition1
            // 
            this.txtLoaderPosition1.BackColor = System.Drawing.Color.White;
            this.txtLoaderPosition1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoaderPosition1.Location = new System.Drawing.Point(213, 73);
            this.txtLoaderPosition1.Name = "txtLoaderPosition1";
            this.txtLoaderPosition1.Size = new System.Drawing.Size(131, 29);
            this.txtLoaderPosition1.TabIndex = 31;
            // 
            // lblModuleLoader
            // 
            this.lblModuleLoader.AutoEllipsis = true;
            this.lblModuleLoader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblModuleLoader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblModuleLoader.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblModuleLoader.ForeColor = System.Drawing.Color.Black;
            this.lblModuleLoader.Location = new System.Drawing.Point(0, 0);
            this.lblModuleLoader.Name = "lblModuleLoader";
            this.lblModuleLoader.Size = new System.Drawing.Size(453, 28);
            this.lblModuleLoader.TabIndex = 9;
            this.lblModuleLoader.Text = "■ Module Loader";
            this.lblModuleLoader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTiltSensor
            // 
            this.lblTiltSensor.AutoEllipsis = true;
            this.lblTiltSensor.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTiltSensor.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTiltSensor.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblTiltSensor.ForeColor = System.Drawing.Color.Black;
            this.lblTiltSensor.Location = new System.Drawing.Point(0, 0);
            this.lblTiltSensor.Name = "lblTiltSensor";
            this.lblTiltSensor.Size = new System.Drawing.Size(921, 30);
            this.lblTiltSensor.TabIndex = 9;
            this.lblTiltSensor.Text = "■ Tilt Sensor";
            this.lblTiltSensor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnControllerSave);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.btnBrightControllerClose);
            this.panel1.Controls.Add(this.btnBrightControllerOpen);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(4, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(923, 393);
            this.panel1.TabIndex = 456;
            // 
            // btnControllerSave
            // 
            this.btnControllerSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnControllerSave.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnControllerSave.ForeColor = System.Drawing.Color.Black;
            this.btnControllerSave.Location = new System.Drawing.Point(825, 348);
            this.btnControllerSave.Name = "btnControllerSave";
            this.btnControllerSave.Size = new System.Drawing.Size(93, 40);
            this.btnControllerSave.TabIndex = 460;
            this.btnControllerSave.Text = "저장";
            this.btnControllerSave.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btn2ChCurrentBrightSet);
            this.panel5.Controls.Add(this.btn1ChCurrentBrightSet);
            this.panel5.Controls.Add(this.lbl2ChCurrentBrightSet);
            this.panel5.Controls.Add(this.lbl1ChCurrentBrightSet);
            this.panel5.Controls.Add(this.txt2ChCurrentBrightSet);
            this.panel5.Controls.Add(this.txt1ChCurrentBrightSet);
            this.panel5.Controls.Add(this.lblBrightSetting);
            this.panel5.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel5.Location = new System.Drawing.Point(462, 79);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(455, 263);
            this.panel5.TabIndex = 457;
            // 
            // btn2ChCurrentBrightSet
            // 
            this.btn2ChCurrentBrightSet.BackColor = System.Drawing.SystemColors.Control;
            this.btn2ChCurrentBrightSet.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn2ChCurrentBrightSet.ForeColor = System.Drawing.Color.Black;
            this.btn2ChCurrentBrightSet.Location = new System.Drawing.Point(350, 156);
            this.btn2ChCurrentBrightSet.Name = "btn2ChCurrentBrightSet";
            this.btn2ChCurrentBrightSet.Size = new System.Drawing.Size(93, 29);
            this.btn2ChCurrentBrightSet.TabIndex = 35;
            this.btn2ChCurrentBrightSet.Text = "2채널 설정";
            this.btn2ChCurrentBrightSet.UseVisualStyleBackColor = false;
            this.btn2ChCurrentBrightSet.Click += new System.EventHandler(this.btnBrightController_Click);
            // 
            // btn1ChCurrentBrightSet
            // 
            this.btn1ChCurrentBrightSet.BackColor = System.Drawing.SystemColors.Control;
            this.btn1ChCurrentBrightSet.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn1ChCurrentBrightSet.ForeColor = System.Drawing.Color.Black;
            this.btn1ChCurrentBrightSet.Location = new System.Drawing.Point(350, 86);
            this.btn1ChCurrentBrightSet.Name = "btn1ChCurrentBrightSet";
            this.btn1ChCurrentBrightSet.Size = new System.Drawing.Size(93, 29);
            this.btn1ChCurrentBrightSet.TabIndex = 34;
            this.btn1ChCurrentBrightSet.Text = "1채널 설정";
            this.btn1ChCurrentBrightSet.UseVisualStyleBackColor = false;
            this.btn1ChCurrentBrightSet.Click += new System.EventHandler(this.btnBrightController_Click);
            // 
            // lbl2ChCurrentBrightSet
            // 
            this.lbl2ChCurrentBrightSet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl2ChCurrentBrightSet.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl2ChCurrentBrightSet.ForeColor = System.Drawing.Color.Black;
            this.lbl2ChCurrentBrightSet.Location = new System.Drawing.Point(3, 156);
            this.lbl2ChCurrentBrightSet.Name = "lbl2ChCurrentBrightSet";
            this.lbl2ChCurrentBrightSet.Size = new System.Drawing.Size(204, 29);
            this.lbl2ChCurrentBrightSet.TabIndex = 33;
            this.lbl2ChCurrentBrightSet.Text = "2채널 현재 밝기 설정";
            this.lbl2ChCurrentBrightSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl1ChCurrentBrightSet
            // 
            this.lbl1ChCurrentBrightSet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl1ChCurrentBrightSet.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl1ChCurrentBrightSet.ForeColor = System.Drawing.Color.Black;
            this.lbl1ChCurrentBrightSet.Location = new System.Drawing.Point(3, 86);
            this.lbl1ChCurrentBrightSet.Name = "lbl1ChCurrentBrightSet";
            this.lbl1ChCurrentBrightSet.Size = new System.Drawing.Size(204, 29);
            this.lbl1ChCurrentBrightSet.TabIndex = 32;
            this.lbl1ChCurrentBrightSet.Text = "1채널 현재 밝기 설정";
            this.lbl1ChCurrentBrightSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt2ChCurrentBrightSet
            // 
            this.txt2ChCurrentBrightSet.BackColor = System.Drawing.Color.White;
            this.txt2ChCurrentBrightSet.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt2ChCurrentBrightSet.ForeColor = System.Drawing.Color.Black;
            this.txt2ChCurrentBrightSet.Location = new System.Drawing.Point(213, 155);
            this.txt2ChCurrentBrightSet.Name = "txt2ChCurrentBrightSet";
            this.txt2ChCurrentBrightSet.Size = new System.Drawing.Size(131, 29);
            this.txt2ChCurrentBrightSet.TabIndex = 31;
            // 
            // txt1ChCurrentBrightSet
            // 
            this.txt1ChCurrentBrightSet.BackColor = System.Drawing.Color.White;
            this.txt1ChCurrentBrightSet.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt1ChCurrentBrightSet.ForeColor = System.Drawing.Color.Black;
            this.txt1ChCurrentBrightSet.Location = new System.Drawing.Point(213, 86);
            this.txt1ChCurrentBrightSet.Name = "txt1ChCurrentBrightSet";
            this.txt1ChCurrentBrightSet.Size = new System.Drawing.Size(131, 29);
            this.txt1ChCurrentBrightSet.TabIndex = 30;
            // 
            // lblBrightSetting
            // 
            this.lblBrightSetting.AutoEllipsis = true;
            this.lblBrightSetting.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBrightSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBrightSetting.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblBrightSetting.ForeColor = System.Drawing.Color.Black;
            this.lblBrightSetting.Location = new System.Drawing.Point(0, 0);
            this.lblBrightSetting.Name = "lblBrightSetting";
            this.lblBrightSetting.Size = new System.Drawing.Size(453, 28);
            this.lblBrightSetting.TabIndex = 9;
            this.lblBrightSetting.Text = "■ 밝기값 설정";
            this.lblBrightSetting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBrightControllerClose
            // 
            this.btnBrightControllerClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnBrightControllerClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBrightControllerClose.ForeColor = System.Drawing.Color.Black;
            this.btnBrightControllerClose.Location = new System.Drawing.Point(102, 33);
            this.btnBrightControllerClose.Name = "btnBrightControllerClose";
            this.btnBrightControllerClose.Size = new System.Drawing.Size(93, 40);
            this.btnBrightControllerClose.TabIndex = 459;
            this.btnBrightControllerClose.Text = "Close";
            this.btnBrightControllerClose.UseVisualStyleBackColor = false;
            this.btnBrightControllerClose.Click += new System.EventHandler(this.btnBrightController_Click);
            // 
            // btnBrightControllerOpen
            // 
            this.btnBrightControllerOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnBrightControllerOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBrightControllerOpen.ForeColor = System.Drawing.Color.Black;
            this.btnBrightControllerOpen.Location = new System.Drawing.Point(3, 33);
            this.btnBrightControllerOpen.Name = "btnBrightControllerOpen";
            this.btnBrightControllerOpen.Size = new System.Drawing.Size(93, 40);
            this.btnBrightControllerOpen.TabIndex = 458;
            this.btnBrightControllerOpen.Text = "Open";
            this.btnBrightControllerOpen.UseVisualStyleBackColor = false;
            this.btnBrightControllerOpen.Click += new System.EventHandler(this.btnBrightController_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lbl2ChBrightValue);
            this.panel6.Controls.Add(this.lbl1ChBrightValue);
            this.panel6.Controls.Add(this.txt2ChBrightValue);
            this.panel6.Controls.Add(this.txt1ChBrightValue);
            this.panel6.Controls.Add(this.lblCurrentBright);
            this.panel6.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel6.Location = new System.Drawing.Point(3, 79);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(455, 263);
            this.panel6.TabIndex = 456;
            // 
            // lbl2ChBrightValue
            // 
            this.lbl2ChBrightValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl2ChBrightValue.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl2ChBrightValue.ForeColor = System.Drawing.Color.Black;
            this.lbl2ChBrightValue.Location = new System.Drawing.Point(3, 156);
            this.lbl2ChBrightValue.Name = "lbl2ChBrightValue";
            this.lbl2ChBrightValue.Size = new System.Drawing.Size(204, 29);
            this.lbl2ChBrightValue.TabIndex = 26;
            this.lbl2ChBrightValue.Text = "2채널 현재 밝기값";
            this.lbl2ChBrightValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl1ChBrightValue
            // 
            this.lbl1ChBrightValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl1ChBrightValue.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl1ChBrightValue.ForeColor = System.Drawing.Color.Black;
            this.lbl1ChBrightValue.Location = new System.Drawing.Point(3, 85);
            this.lbl1ChBrightValue.Name = "lbl1ChBrightValue";
            this.lbl1ChBrightValue.Size = new System.Drawing.Size(204, 29);
            this.lbl1ChBrightValue.TabIndex = 25;
            this.lbl1ChBrightValue.Text = "1채널 현재 밝기값";
            this.lbl1ChBrightValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt2ChBrightValue
            // 
            this.txt2ChBrightValue.BackColor = System.Drawing.Color.White;
            this.txt2ChBrightValue.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt2ChBrightValue.ForeColor = System.Drawing.Color.Black;
            this.txt2ChBrightValue.Location = new System.Drawing.Point(213, 156);
            this.txt2ChBrightValue.Name = "txt2ChBrightValue";
            this.txt2ChBrightValue.Size = new System.Drawing.Size(227, 29);
            this.txt2ChBrightValue.TabIndex = 24;
            // 
            // txt1ChBrightValue
            // 
            this.txt1ChBrightValue.BackColor = System.Drawing.Color.White;
            this.txt1ChBrightValue.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt1ChBrightValue.ForeColor = System.Drawing.Color.Black;
            this.txt1ChBrightValue.Location = new System.Drawing.Point(213, 86);
            this.txt1ChBrightValue.Name = "txt1ChBrightValue";
            this.txt1ChBrightValue.Size = new System.Drawing.Size(227, 29);
            this.txt1ChBrightValue.TabIndex = 23;
            // 
            // lblCurrentBright
            // 
            this.lblCurrentBright.AutoEllipsis = true;
            this.lblCurrentBright.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCurrentBright.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCurrentBright.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblCurrentBright.ForeColor = System.Drawing.Color.Black;
            this.lblCurrentBright.Location = new System.Drawing.Point(0, 0);
            this.lblCurrentBright.Name = "lblCurrentBright";
            this.lblCurrentBright.Size = new System.Drawing.Size(453, 28);
            this.lblCurrentBright.TabIndex = 9;
            this.lblCurrentBright.Text = "■ 현재 밝기값";
            this.lblCurrentBright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(921, 30);
            this.label3.TabIndex = 9;
            this.label3.Text = "■ ERI 조명 컨트롤러";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel16);
            this.tabPage2.Controls.Add(this.panel18);
            this.tabPage2.Controls.Add(this.panel20);
            this.tabPage2.Controls.Add(this.panel14);
            this.tabPage2.Controls.Add(this.panel12);
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabPage2.Location = new System.Drawing.Point(4, 44);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1861, 802);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "바코드 리더";
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.btnUnloaderHandyBarcodeClose);
            this.panel16.Controls.Add(this.btnUnloaderHandyBarcodeOpen);
            this.panel16.Controls.Add(this.panel17);
            this.panel16.Controls.Add(this.lblUnloaderHandyBarcodeReader);
            this.panel16.Location = new System.Drawing.Point(932, 538);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(923, 260);
            this.panel16.TabIndex = 464;
            // 
            // btnUnloaderHandyBarcodeClose
            // 
            this.btnUnloaderHandyBarcodeClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderHandyBarcodeClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderHandyBarcodeClose.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderHandyBarcodeClose.Location = new System.Drawing.Point(102, 33);
            this.btnUnloaderHandyBarcodeClose.Name = "btnUnloaderHandyBarcodeClose";
            this.btnUnloaderHandyBarcodeClose.Size = new System.Drawing.Size(93, 40);
            this.btnUnloaderHandyBarcodeClose.TabIndex = 459;
            this.btnUnloaderHandyBarcodeClose.Text = "Close";
            this.btnUnloaderHandyBarcodeClose.UseVisualStyleBackColor = false;
            this.btnUnloaderHandyBarcodeClose.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // btnUnloaderHandyBarcodeOpen
            // 
            this.btnUnloaderHandyBarcodeOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderHandyBarcodeOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderHandyBarcodeOpen.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderHandyBarcodeOpen.Location = new System.Drawing.Point(3, 33);
            this.btnUnloaderHandyBarcodeOpen.Name = "btnUnloaderHandyBarcodeOpen";
            this.btnUnloaderHandyBarcodeOpen.Size = new System.Drawing.Size(93, 40);
            this.btnUnloaderHandyBarcodeOpen.TabIndex = 458;
            this.btnUnloaderHandyBarcodeOpen.Text = "Open";
            this.btnUnloaderHandyBarcodeOpen.UseVisualStyleBackColor = false;
            this.btnUnloaderHandyBarcodeOpen.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.btnUnloaderHandyBarcodeGet);
            this.panel17.Controls.Add(this.txtUnloaderHandyBarcode);
            this.panel17.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel17.Location = new System.Drawing.Point(3, 79);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(915, 176);
            this.panel17.TabIndex = 456;
            // 
            // btnUnloaderHandyBarcodeGet
            // 
            this.btnUnloaderHandyBarcodeGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderHandyBarcodeGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnUnloaderHandyBarcodeGet.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderHandyBarcodeGet.Location = new System.Drawing.Point(165, 50);
            this.btnUnloaderHandyBarcodeGet.Name = "btnUnloaderHandyBarcodeGet";
            this.btnUnloaderHandyBarcodeGet.Size = new System.Drawing.Size(166, 78);
            this.btnUnloaderHandyBarcodeGet.TabIndex = 31;
            this.btnUnloaderHandyBarcodeGet.Text = "Get";
            this.btnUnloaderHandyBarcodeGet.UseVisualStyleBackColor = false;
            this.btnUnloaderHandyBarcodeGet.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // txtUnloaderHandyBarcode
            // 
            this.txtUnloaderHandyBarcode.BackColor = System.Drawing.Color.White;
            this.txtUnloaderHandyBarcode.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtUnloaderHandyBarcode.Location = new System.Drawing.Point(366, 50);
            this.txtUnloaderHandyBarcode.Name = "txtUnloaderHandyBarcode";
            this.txtUnloaderHandyBarcode.Size = new System.Drawing.Size(451, 78);
            this.txtUnloaderHandyBarcode.TabIndex = 30;
            // 
            // lblUnloaderHandyBarcodeReader
            // 
            this.lblUnloaderHandyBarcodeReader.AutoEllipsis = true;
            this.lblUnloaderHandyBarcodeReader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloaderHandyBarcodeReader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloaderHandyBarcodeReader.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblUnloaderHandyBarcodeReader.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderHandyBarcodeReader.Location = new System.Drawing.Point(0, 0);
            this.lblUnloaderHandyBarcodeReader.Name = "lblUnloaderHandyBarcodeReader";
            this.lblUnloaderHandyBarcodeReader.Size = new System.Drawing.Size(921, 30);
            this.lblUnloaderHandyBarcodeReader.TabIndex = 9;
            this.lblUnloaderHandyBarcodeReader.Text = "■ 핸디 바코드 리더";
            this.lblUnloaderHandyBarcodeReader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.btnUnloaderBarcodeBClose);
            this.panel18.Controls.Add(this.btnUnloaderBarcodeBOpen);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Controls.Add(this.lblUnloaderFixBarcodeReaderB);
            this.panel18.Location = new System.Drawing.Point(932, 272);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(923, 260);
            this.panel18.TabIndex = 463;
            // 
            // btnUnloaderBarcodeBClose
            // 
            this.btnUnloaderBarcodeBClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderBarcodeBClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderBarcodeBClose.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderBarcodeBClose.Location = new System.Drawing.Point(102, 33);
            this.btnUnloaderBarcodeBClose.Name = "btnUnloaderBarcodeBClose";
            this.btnUnloaderBarcodeBClose.Size = new System.Drawing.Size(93, 40);
            this.btnUnloaderBarcodeBClose.TabIndex = 459;
            this.btnUnloaderBarcodeBClose.Text = "Close";
            this.btnUnloaderBarcodeBClose.UseVisualStyleBackColor = false;
            this.btnUnloaderBarcodeBClose.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // btnUnloaderBarcodeBOpen
            // 
            this.btnUnloaderBarcodeBOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderBarcodeBOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderBarcodeBOpen.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderBarcodeBOpen.Location = new System.Drawing.Point(3, 33);
            this.btnUnloaderBarcodeBOpen.Name = "btnUnloaderBarcodeBOpen";
            this.btnUnloaderBarcodeBOpen.Size = new System.Drawing.Size(93, 40);
            this.btnUnloaderBarcodeBOpen.TabIndex = 458;
            this.btnUnloaderBarcodeBOpen.Text = "Open";
            this.btnUnloaderBarcodeBOpen.UseVisualStyleBackColor = false;
            this.btnUnloaderBarcodeBOpen.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.btnUnloaderBarcodeBGet);
            this.panel19.Controls.Add(this.txtUnloaderBarcodeB);
            this.panel19.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel19.Location = new System.Drawing.Point(3, 79);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(915, 176);
            this.panel19.TabIndex = 456;
            // 
            // btnUnloaderBarcodeBGet
            // 
            this.btnUnloaderBarcodeBGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderBarcodeBGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnUnloaderBarcodeBGet.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderBarcodeBGet.Location = new System.Drawing.Point(165, 50);
            this.btnUnloaderBarcodeBGet.Name = "btnUnloaderBarcodeBGet";
            this.btnUnloaderBarcodeBGet.Size = new System.Drawing.Size(166, 78);
            this.btnUnloaderBarcodeBGet.TabIndex = 31;
            this.btnUnloaderBarcodeBGet.Text = "Get";
            this.btnUnloaderBarcodeBGet.UseVisualStyleBackColor = false;
            this.btnUnloaderBarcodeBGet.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // txtUnloaderBarcodeB
            // 
            this.txtUnloaderBarcodeB.BackColor = System.Drawing.Color.White;
            this.txtUnloaderBarcodeB.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtUnloaderBarcodeB.Location = new System.Drawing.Point(366, 50);
            this.txtUnloaderBarcodeB.Name = "txtUnloaderBarcodeB";
            this.txtUnloaderBarcodeB.Size = new System.Drawing.Size(451, 78);
            this.txtUnloaderBarcodeB.TabIndex = 30;
            // 
            // lblUnloaderFixBarcodeReaderB
            // 
            this.lblUnloaderFixBarcodeReaderB.AutoEllipsis = true;
            this.lblUnloaderFixBarcodeReaderB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloaderFixBarcodeReaderB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloaderFixBarcodeReaderB.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblUnloaderFixBarcodeReaderB.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderFixBarcodeReaderB.Location = new System.Drawing.Point(0, 0);
            this.lblUnloaderFixBarcodeReaderB.Name = "lblUnloaderFixBarcodeReaderB";
            this.lblUnloaderFixBarcodeReaderB.Size = new System.Drawing.Size(921, 30);
            this.lblUnloaderFixBarcodeReaderB.TabIndex = 9;
            this.lblUnloaderFixBarcodeReaderB.Text = "■ 언로더부 고정식 바코드 리더 B";
            this.lblUnloaderFixBarcodeReaderB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.btnLoaderBarcodeBClose);
            this.panel20.Controls.Add(this.btnLoaderBarcodeBOpen);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Controls.Add(this.lblLoaderFixBarcodeReaderB);
            this.panel20.Location = new System.Drawing.Point(932, 6);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(923, 260);
            this.panel20.TabIndex = 462;
            // 
            // btnLoaderBarcodeBClose
            // 
            this.btnLoaderBarcodeBClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderBarcodeBClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderBarcodeBClose.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderBarcodeBClose.Location = new System.Drawing.Point(102, 33);
            this.btnLoaderBarcodeBClose.Name = "btnLoaderBarcodeBClose";
            this.btnLoaderBarcodeBClose.Size = new System.Drawing.Size(93, 40);
            this.btnLoaderBarcodeBClose.TabIndex = 459;
            this.btnLoaderBarcodeBClose.Text = "Close";
            this.btnLoaderBarcodeBClose.UseVisualStyleBackColor = false;
            this.btnLoaderBarcodeBClose.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // btnLoaderBarcodeBOpen
            // 
            this.btnLoaderBarcodeBOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderBarcodeBOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderBarcodeBOpen.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderBarcodeBOpen.Location = new System.Drawing.Point(3, 33);
            this.btnLoaderBarcodeBOpen.Name = "btnLoaderBarcodeBOpen";
            this.btnLoaderBarcodeBOpen.Size = new System.Drawing.Size(93, 40);
            this.btnLoaderBarcodeBOpen.TabIndex = 458;
            this.btnLoaderBarcodeBOpen.Text = "Open";
            this.btnLoaderBarcodeBOpen.UseVisualStyleBackColor = false;
            this.btnLoaderBarcodeBOpen.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.btnLoaderBarcodeBGet);
            this.panel21.Controls.Add(this.txtLoaderBarcodeB);
            this.panel21.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel21.Location = new System.Drawing.Point(3, 79);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(915, 176);
            this.panel21.TabIndex = 456;
            // 
            // btnLoaderBarcodeBGet
            // 
            this.btnLoaderBarcodeBGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderBarcodeBGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnLoaderBarcodeBGet.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderBarcodeBGet.Location = new System.Drawing.Point(165, 50);
            this.btnLoaderBarcodeBGet.Name = "btnLoaderBarcodeBGet";
            this.btnLoaderBarcodeBGet.Size = new System.Drawing.Size(166, 78);
            this.btnLoaderBarcodeBGet.TabIndex = 31;
            this.btnLoaderBarcodeBGet.Text = "Get";
            this.btnLoaderBarcodeBGet.UseVisualStyleBackColor = false;
            this.btnLoaderBarcodeBGet.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // txtLoaderBarcodeB
            // 
            this.txtLoaderBarcodeB.BackColor = System.Drawing.Color.White;
            this.txtLoaderBarcodeB.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtLoaderBarcodeB.Location = new System.Drawing.Point(366, 50);
            this.txtLoaderBarcodeB.Name = "txtLoaderBarcodeB";
            this.txtLoaderBarcodeB.Size = new System.Drawing.Size(451, 78);
            this.txtLoaderBarcodeB.TabIndex = 30;
            // 
            // lblLoaderFixBarcodeReaderB
            // 
            this.lblLoaderFixBarcodeReaderB.AutoEllipsis = true;
            this.lblLoaderFixBarcodeReaderB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoaderFixBarcodeReaderB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoaderFixBarcodeReaderB.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblLoaderFixBarcodeReaderB.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderFixBarcodeReaderB.Location = new System.Drawing.Point(0, 0);
            this.lblLoaderFixBarcodeReaderB.Name = "lblLoaderFixBarcodeReaderB";
            this.lblLoaderFixBarcodeReaderB.Size = new System.Drawing.Size(921, 30);
            this.lblLoaderFixBarcodeReaderB.TabIndex = 9;
            this.lblLoaderFixBarcodeReaderB.Text = "■ 로더부 고정식 바코드 리더 B";
            this.lblLoaderFixBarcodeReaderB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btnLoaderHandyBarcodeClose);
            this.panel14.Controls.Add(this.btnLoaderHandyBarcodeOpen);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Controls.Add(this.lblLoaderHandyBarcodeReader);
            this.panel14.Location = new System.Drawing.Point(4, 538);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(923, 260);
            this.panel14.TabIndex = 461;
            // 
            // btnLoaderHandyBarcodeClose
            // 
            this.btnLoaderHandyBarcodeClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderHandyBarcodeClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderHandyBarcodeClose.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderHandyBarcodeClose.Location = new System.Drawing.Point(102, 33);
            this.btnLoaderHandyBarcodeClose.Name = "btnLoaderHandyBarcodeClose";
            this.btnLoaderHandyBarcodeClose.Size = new System.Drawing.Size(93, 40);
            this.btnLoaderHandyBarcodeClose.TabIndex = 459;
            this.btnLoaderHandyBarcodeClose.Text = "Close";
            this.btnLoaderHandyBarcodeClose.UseVisualStyleBackColor = false;
            this.btnLoaderHandyBarcodeClose.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // btnLoaderHandyBarcodeOpen
            // 
            this.btnLoaderHandyBarcodeOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderHandyBarcodeOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderHandyBarcodeOpen.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderHandyBarcodeOpen.Location = new System.Drawing.Point(3, 33);
            this.btnLoaderHandyBarcodeOpen.Name = "btnLoaderHandyBarcodeOpen";
            this.btnLoaderHandyBarcodeOpen.Size = new System.Drawing.Size(93, 40);
            this.btnLoaderHandyBarcodeOpen.TabIndex = 458;
            this.btnLoaderHandyBarcodeOpen.Text = "Open";
            this.btnLoaderHandyBarcodeOpen.UseVisualStyleBackColor = false;
            this.btnLoaderHandyBarcodeOpen.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnLoaderHandyBarcodeGet);
            this.panel15.Controls.Add(this.txtLoaderHandyBarcode);
            this.panel15.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel15.Location = new System.Drawing.Point(3, 79);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(915, 176);
            this.panel15.TabIndex = 456;
            // 
            // btnLoaderHandyBarcodeGet
            // 
            this.btnLoaderHandyBarcodeGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderHandyBarcodeGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnLoaderHandyBarcodeGet.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderHandyBarcodeGet.Location = new System.Drawing.Point(165, 50);
            this.btnLoaderHandyBarcodeGet.Name = "btnLoaderHandyBarcodeGet";
            this.btnLoaderHandyBarcodeGet.Size = new System.Drawing.Size(166, 78);
            this.btnLoaderHandyBarcodeGet.TabIndex = 31;
            this.btnLoaderHandyBarcodeGet.Text = "Get";
            this.btnLoaderHandyBarcodeGet.UseVisualStyleBackColor = false;
            this.btnLoaderHandyBarcodeGet.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // txtLoaderHandyBarcode
            // 
            this.txtLoaderHandyBarcode.BackColor = System.Drawing.Color.White;
            this.txtLoaderHandyBarcode.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtLoaderHandyBarcode.Location = new System.Drawing.Point(366, 50);
            this.txtLoaderHandyBarcode.Name = "txtLoaderHandyBarcode";
            this.txtLoaderHandyBarcode.Size = new System.Drawing.Size(451, 78);
            this.txtLoaderHandyBarcode.TabIndex = 30;
            // 
            // lblLoaderHandyBarcodeReader
            // 
            this.lblLoaderHandyBarcodeReader.AutoEllipsis = true;
            this.lblLoaderHandyBarcodeReader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoaderHandyBarcodeReader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoaderHandyBarcodeReader.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblLoaderHandyBarcodeReader.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderHandyBarcodeReader.Location = new System.Drawing.Point(0, 0);
            this.lblLoaderHandyBarcodeReader.Name = "lblLoaderHandyBarcodeReader";
            this.lblLoaderHandyBarcodeReader.Size = new System.Drawing.Size(921, 30);
            this.lblLoaderHandyBarcodeReader.TabIndex = 9;
            this.lblLoaderHandyBarcodeReader.Text = "■ 핸디 바코드 리더";
            this.lblLoaderHandyBarcodeReader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.btnUnloaderBarcodeAClose);
            this.panel12.Controls.Add(this.btnUnloaderBarcodeAOpen);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Controls.Add(this.lblUnloaderFixBarcodeReaderA);
            this.panel12.Location = new System.Drawing.Point(4, 272);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(923, 260);
            this.panel12.TabIndex = 460;
            // 
            // btnUnloaderBarcodeAClose
            // 
            this.btnUnloaderBarcodeAClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderBarcodeAClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderBarcodeAClose.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderBarcodeAClose.Location = new System.Drawing.Point(102, 33);
            this.btnUnloaderBarcodeAClose.Name = "btnUnloaderBarcodeAClose";
            this.btnUnloaderBarcodeAClose.Size = new System.Drawing.Size(93, 40);
            this.btnUnloaderBarcodeAClose.TabIndex = 459;
            this.btnUnloaderBarcodeAClose.Text = "Close";
            this.btnUnloaderBarcodeAClose.UseVisualStyleBackColor = false;
            this.btnUnloaderBarcodeAClose.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // btnUnloaderBarcodeAOpen
            // 
            this.btnUnloaderBarcodeAOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderBarcodeAOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderBarcodeAOpen.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderBarcodeAOpen.Location = new System.Drawing.Point(3, 33);
            this.btnUnloaderBarcodeAOpen.Name = "btnUnloaderBarcodeAOpen";
            this.btnUnloaderBarcodeAOpen.Size = new System.Drawing.Size(93, 40);
            this.btnUnloaderBarcodeAOpen.TabIndex = 458;
            this.btnUnloaderBarcodeAOpen.Text = "Open";
            this.btnUnloaderBarcodeAOpen.UseVisualStyleBackColor = false;
            this.btnUnloaderBarcodeAOpen.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.btnUnloaderBarcodeAGet);
            this.panel13.Controls.Add(this.txtUnloaderBarcodeA);
            this.panel13.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel13.Location = new System.Drawing.Point(3, 79);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(915, 176);
            this.panel13.TabIndex = 456;
            // 
            // btnUnloaderBarcodeAGet
            // 
            this.btnUnloaderBarcodeAGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderBarcodeAGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnUnloaderBarcodeAGet.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderBarcodeAGet.Location = new System.Drawing.Point(165, 50);
            this.btnUnloaderBarcodeAGet.Name = "btnUnloaderBarcodeAGet";
            this.btnUnloaderBarcodeAGet.Size = new System.Drawing.Size(166, 78);
            this.btnUnloaderBarcodeAGet.TabIndex = 31;
            this.btnUnloaderBarcodeAGet.Text = "Get";
            this.btnUnloaderBarcodeAGet.UseVisualStyleBackColor = false;
            this.btnUnloaderBarcodeAGet.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // txtUnloaderBarcodeA
            // 
            this.txtUnloaderBarcodeA.BackColor = System.Drawing.Color.White;
            this.txtUnloaderBarcodeA.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtUnloaderBarcodeA.Location = new System.Drawing.Point(366, 50);
            this.txtUnloaderBarcodeA.Name = "txtUnloaderBarcodeA";
            this.txtUnloaderBarcodeA.Size = new System.Drawing.Size(451, 78);
            this.txtUnloaderBarcodeA.TabIndex = 30;
            // 
            // lblUnloaderFixBarcodeReaderA
            // 
            this.lblUnloaderFixBarcodeReaderA.AutoEllipsis = true;
            this.lblUnloaderFixBarcodeReaderA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloaderFixBarcodeReaderA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloaderFixBarcodeReaderA.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblUnloaderFixBarcodeReaderA.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderFixBarcodeReaderA.Location = new System.Drawing.Point(0, 0);
            this.lblUnloaderFixBarcodeReaderA.Name = "lblUnloaderFixBarcodeReaderA";
            this.lblUnloaderFixBarcodeReaderA.Size = new System.Drawing.Size(921, 30);
            this.lblUnloaderFixBarcodeReaderA.TabIndex = 9;
            this.lblUnloaderFixBarcodeReaderA.Text = "■ 언로더부 고정식 바코드 리더 A";
            this.lblUnloaderFixBarcodeReaderA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btnLoaderBarcodeAClose);
            this.panel10.Controls.Add(this.btnLoaderBarcodeAOpen);
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Controls.Add(this.lblLoaderFixBarcodeReaderA);
            this.panel10.Location = new System.Drawing.Point(4, 6);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(923, 260);
            this.panel10.TabIndex = 459;
            // 
            // btnLoaderBarcodeAClose
            // 
            this.btnLoaderBarcodeAClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderBarcodeAClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderBarcodeAClose.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderBarcodeAClose.Location = new System.Drawing.Point(102, 33);
            this.btnLoaderBarcodeAClose.Name = "btnLoaderBarcodeAClose";
            this.btnLoaderBarcodeAClose.Size = new System.Drawing.Size(93, 40);
            this.btnLoaderBarcodeAClose.TabIndex = 459;
            this.btnLoaderBarcodeAClose.Text = "Close";
            this.btnLoaderBarcodeAClose.UseVisualStyleBackColor = false;
            this.btnLoaderBarcodeAClose.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // btnLoaderBarcodeAOpen
            // 
            this.btnLoaderBarcodeAOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderBarcodeAOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderBarcodeAOpen.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderBarcodeAOpen.Location = new System.Drawing.Point(3, 33);
            this.btnLoaderBarcodeAOpen.Name = "btnLoaderBarcodeAOpen";
            this.btnLoaderBarcodeAOpen.Size = new System.Drawing.Size(93, 40);
            this.btnLoaderBarcodeAOpen.TabIndex = 458;
            this.btnLoaderBarcodeAOpen.Text = "Open";
            this.btnLoaderBarcodeAOpen.UseVisualStyleBackColor = false;
            this.btnLoaderBarcodeAOpen.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.btnLoaderBarcodeAGet);
            this.panel11.Controls.Add(this.txtLoaderBarcodeA);
            this.panel11.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel11.Location = new System.Drawing.Point(3, 79);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(915, 176);
            this.panel11.TabIndex = 456;
            // 
            // btnLoaderBarcodeAGet
            // 
            this.btnLoaderBarcodeAGet.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderBarcodeAGet.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnLoaderBarcodeAGet.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderBarcodeAGet.Location = new System.Drawing.Point(165, 50);
            this.btnLoaderBarcodeAGet.Name = "btnLoaderBarcodeAGet";
            this.btnLoaderBarcodeAGet.Size = new System.Drawing.Size(166, 78);
            this.btnLoaderBarcodeAGet.TabIndex = 31;
            this.btnLoaderBarcodeAGet.Text = "Get";
            this.btnLoaderBarcodeAGet.UseVisualStyleBackColor = false;
            this.btnLoaderBarcodeAGet.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // txtLoaderBarcodeA
            // 
            this.txtLoaderBarcodeA.BackColor = System.Drawing.Color.White;
            this.txtLoaderBarcodeA.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.txtLoaderBarcodeA.Location = new System.Drawing.Point(366, 50);
            this.txtLoaderBarcodeA.Name = "txtLoaderBarcodeA";
            this.txtLoaderBarcodeA.Size = new System.Drawing.Size(451, 78);
            this.txtLoaderBarcodeA.TabIndex = 30;
            // 
            // lblLoaderFixBarcodeReaderA
            // 
            this.lblLoaderFixBarcodeReaderA.AutoEllipsis = true;
            this.lblLoaderFixBarcodeReaderA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoaderFixBarcodeReaderA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoaderFixBarcodeReaderA.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblLoaderFixBarcodeReaderA.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderFixBarcodeReaderA.Location = new System.Drawing.Point(0, 0);
            this.lblLoaderFixBarcodeReaderA.Name = "lblLoaderFixBarcodeReaderA";
            this.lblLoaderFixBarcodeReaderA.Size = new System.Drawing.Size(921, 30);
            this.lblLoaderFixBarcodeReaderA.TabIndex = 9;
            this.lblLoaderFixBarcodeReaderA.Text = "■ 로더부 고정식 바코드 리더 A";
            this.lblLoaderFixBarcodeReaderA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.panel22);
            this.tabPage3.Controls.Add(this.panel24);
            this.tabPage3.Controls.Add(this.panel26);
            this.tabPage3.Controls.Add(this.panel30);
            this.tabPage3.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabPage3.Location = new System.Drawing.Point(4, 44);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1861, 802);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "BeamPositionner / 파워미터 / LDS";
            // 
            // panel22
            // 
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.btnLdsBreakSave);
            this.panel22.Controls.Add(this.txtLdsBreakState);
            this.panel22.Controls.Add(this.label12);
            this.panel22.Controls.Add(this.txtLdsBreakTableIdx);
            this.panel22.Controls.Add(this.label13);
            this.panel22.Controls.Add(this.txtLdsBreakMeasureTime);
            this.panel22.Controls.Add(this.label14);
            this.panel22.Controls.Add(this.txtLdsBreakReadyTime);
            this.panel22.Controls.Add(this.btnLdsBreakStop);
            this.panel22.Controls.Add(this.btnLdsBreakStart);
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.label56);
            this.panel22.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel22.Location = new System.Drawing.Point(933, 405);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(923, 393);
            this.panel22.TabIndex = 463;
            // 
            // btnLdsBreakSave
            // 
            this.btnLdsBreakSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnLdsBreakSave.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLdsBreakSave.ForeColor = System.Drawing.Color.Black;
            this.btnLdsBreakSave.Location = new System.Drawing.Point(767, 348);
            this.btnLdsBreakSave.Name = "btnLdsBreakSave";
            this.btnLdsBreakSave.Size = new System.Drawing.Size(150, 40);
            this.btnLdsBreakSave.TabIndex = 464;
            this.btnLdsBreakSave.Text = "Save";
            this.btnLdsBreakSave.UseVisualStyleBackColor = false;
            // 
            // txtLdsBreakState
            // 
            this.txtLdsBreakState.BackColor = System.Drawing.SystemColors.Control;
            this.txtLdsBreakState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtLdsBreakState.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLdsBreakState.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakState.Location = new System.Drawing.Point(622, 33);
            this.txtLdsBreakState.Name = "txtLdsBreakState";
            this.txtLdsBreakState.Size = new System.Drawing.Size(95, 40);
            this.txtLdsBreakState.TabIndex = 63;
            this.txtLdsBreakState.Text = "대기중";
            this.txtLdsBreakState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(408, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 40);
            this.label12.TabIndex = 62;
            this.label12.Text = "테이블 인덱스 :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakTableIdx
            // 
            this.txtLdsBreakTableIdx.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakTableIdx.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLdsBreakTableIdx.ForeColor = System.Drawing.Color.White;
            this.txtLdsBreakTableIdx.Location = new System.Drawing.Point(509, 33);
            this.txtLdsBreakTableIdx.Name = "txtLdsBreakTableIdx";
            this.txtLdsBreakTableIdx.Size = new System.Drawing.Size(105, 39);
            this.txtLdsBreakTableIdx.TabIndex = 61;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.SystemColors.Control;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(216, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 40);
            this.label13.TabIndex = 60;
            this.label13.Text = "측정 시간[s]";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakMeasureTime
            // 
            this.txtLdsBreakMeasureTime.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakMeasureTime.Font = new System.Drawing.Font("Malgun Gothic", 18F);
            this.txtLdsBreakMeasureTime.ForeColor = System.Drawing.Color.White;
            this.txtLdsBreakMeasureTime.Location = new System.Drawing.Point(297, 33);
            this.txtLdsBreakMeasureTime.Name = "txtLdsBreakMeasureTime";
            this.txtLdsBreakMeasureTime.Size = new System.Drawing.Size(105, 39);
            this.txtLdsBreakMeasureTime.TabIndex = 59;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.SystemColors.Control;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(4, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 40);
            this.label14.TabIndex = 58;
            this.label14.Text = "대기 시간[s]";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakReadyTime
            // 
            this.txtLdsBreakReadyTime.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakReadyTime.Font = new System.Drawing.Font("Malgun Gothic", 18F);
            this.txtLdsBreakReadyTime.ForeColor = System.Drawing.Color.White;
            this.txtLdsBreakReadyTime.Location = new System.Drawing.Point(105, 33);
            this.txtLdsBreakReadyTime.Name = "txtLdsBreakReadyTime";
            this.txtLdsBreakReadyTime.Size = new System.Drawing.Size(105, 39);
            this.txtLdsBreakReadyTime.TabIndex = 57;
            // 
            // btnLdsBreakStop
            // 
            this.btnLdsBreakStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnLdsBreakStop.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnLdsBreakStop.ForeColor = System.Drawing.Color.Black;
            this.btnLdsBreakStop.Location = new System.Drawing.Point(823, 33);
            this.btnLdsBreakStop.Name = "btnLdsBreakStop";
            this.btnLdsBreakStop.Size = new System.Drawing.Size(93, 40);
            this.btnLdsBreakStop.TabIndex = 56;
            this.btnLdsBreakStop.Text = "측정 정지";
            this.btnLdsBreakStop.UseVisualStyleBackColor = false;
            // 
            // btnLdsBreakStart
            // 
            this.btnLdsBreakStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnLdsBreakStart.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnLdsBreakStart.ForeColor = System.Drawing.Color.Black;
            this.btnLdsBreakStart.Location = new System.Drawing.Point(724, 33);
            this.btnLdsBreakStart.Name = "btnLdsBreakStart";
            this.btnLdsBreakStart.Size = new System.Drawing.Size(93, 40);
            this.btnLdsBreakStart.TabIndex = 55;
            this.btnLdsBreakStart.Text = "측정 시작";
            this.btnLdsBreakStart.UseVisualStyleBackColor = false;
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.lviLdsBreak);
            this.panel23.Controls.Add(this.label39);
            this.panel23.Controls.Add(this.txtLdsBreakYInterval);
            this.panel23.Controls.Add(this.label45);
            this.panel23.Controls.Add(this.txtLdsBreakValue9);
            this.panel23.Controls.Add(this.label46);
            this.panel23.Controls.Add(this.label47);
            this.panel23.Controls.Add(this.txtLdsBreakValue6);
            this.panel23.Controls.Add(this.txtLdsBreakValue3);
            this.panel23.Controls.Add(this.label48);
            this.panel23.Controls.Add(this.txtLdsBreakValue8);
            this.panel23.Controls.Add(this.label49);
            this.panel23.Controls.Add(this.label50);
            this.panel23.Controls.Add(this.txtLdsBreakValue5);
            this.panel23.Controls.Add(this.txtLdsBreakValue2);
            this.panel23.Controls.Add(this.txtLdsBreakCurrentValue);
            this.panel23.Controls.Add(this.label51);
            this.panel23.Controls.Add(this.label52);
            this.panel23.Controls.Add(this.txtLdsBreakXInterval);
            this.panel23.Controls.Add(this.txtLdsBreakValue7);
            this.panel23.Controls.Add(this.label53);
            this.panel23.Controls.Add(this.label54);
            this.panel23.Controls.Add(this.txtLdsBreakValue4);
            this.panel23.Controls.Add(this.txtLdsBreakValue1);
            this.panel23.Controls.Add(this.label55);
            this.panel23.Location = new System.Drawing.Point(4, 88);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(913, 254);
            this.panel23.TabIndex = 54;
            // 
            // lviLdsBreak
            // 
            this.lviLdsBreak.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lviLdsBreak.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lviLdsBreak.GridLines = true;
            this.lviLdsBreak.Location = new System.Drawing.Point(403, 3);
            this.lviLdsBreak.Name = "lviLdsBreak";
            this.lviLdsBreak.Size = new System.Drawing.Size(505, 246);
            this.lviLdsBreak.TabIndex = 46;
            this.lviLdsBreak.UseCompatibleStateImageBehavior = false;
            this.lviLdsBreak.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "인덱스";
            this.columnHeader4.Width = 134;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "측정값";
            this.columnHeader5.Width = 121;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "차이값";
            this.columnHeader6.Width = 130;
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.SystemColors.Control;
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label39.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(203, 215);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(105, 34);
            this.label39.TabIndex = 45;
            this.label39.Text = "Y 이동간격 (mm)";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakYInterval
            // 
            this.txtLdsBreakYInterval.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakYInterval.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakYInterval.ForeColor = System.Drawing.Color.White;
            this.txtLdsBreakYInterval.Location = new System.Drawing.Point(314, 215);
            this.txtLdsBreakYInterval.Name = "txtLdsBreakYInterval";
            this.txtLdsBreakYInterval.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakYInterval.TabIndex = 44;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.SystemColors.Control;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label45.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(282, 159);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(39, 34);
            this.label45.TabIndex = 43;
            this.label45.Text = "9";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakValue9
            // 
            this.txtLdsBreakValue9.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue9.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue9.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue9.Location = new System.Drawing.Point(327, 159);
            this.txtLdsBreakValue9.Name = "txtLdsBreakValue9";
            this.txtLdsBreakValue9.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue9.TabIndex = 42;
            this.txtLdsBreakValue9.Text = "미측정";
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.SystemColors.Control;
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label46.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(282, 109);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(39, 34);
            this.label46.TabIndex = 41;
            this.label46.Text = "6";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.Control;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(282, 57);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(39, 34);
            this.label47.TabIndex = 40;
            this.label47.Text = "3";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakValue6
            // 
            this.txtLdsBreakValue6.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue6.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue6.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue6.Location = new System.Drawing.Point(327, 109);
            this.txtLdsBreakValue6.Name = "txtLdsBreakValue6";
            this.txtLdsBreakValue6.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue6.TabIndex = 39;
            this.txtLdsBreakValue6.Text = "미측정";
            // 
            // txtLdsBreakValue3
            // 
            this.txtLdsBreakValue3.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue3.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue3.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue3.Location = new System.Drawing.Point(327, 57);
            this.txtLdsBreakValue3.Name = "txtLdsBreakValue3";
            this.txtLdsBreakValue3.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue3.TabIndex = 38;
            this.txtLdsBreakValue3.Text = "미측정";
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.SystemColors.Control;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label48.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(144, 159);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(39, 34);
            this.label48.TabIndex = 37;
            this.label48.Text = "8";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakValue8
            // 
            this.txtLdsBreakValue8.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue8.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue8.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue8.Location = new System.Drawing.Point(189, 159);
            this.txtLdsBreakValue8.Name = "txtLdsBreakValue8";
            this.txtLdsBreakValue8.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue8.TabIndex = 36;
            this.txtLdsBreakValue8.Text = "미측정";
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.SystemColors.Control;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label49.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(144, 109);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(39, 34);
            this.label49.TabIndex = 35;
            this.label49.Text = "5";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.SystemColors.Control;
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label50.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(144, 57);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(39, 34);
            this.label50.TabIndex = 34;
            this.label50.Text = "2";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakValue5
            // 
            this.txtLdsBreakValue5.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue5.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue5.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue5.Location = new System.Drawing.Point(189, 109);
            this.txtLdsBreakValue5.Name = "txtLdsBreakValue5";
            this.txtLdsBreakValue5.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue5.TabIndex = 33;
            this.txtLdsBreakValue5.Text = "미측정";
            // 
            // txtLdsBreakValue2
            // 
            this.txtLdsBreakValue2.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue2.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue2.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue2.Location = new System.Drawing.Point(189, 57);
            this.txtLdsBreakValue2.Name = "txtLdsBreakValue2";
            this.txtLdsBreakValue2.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue2.TabIndex = 32;
            this.txtLdsBreakValue2.Text = "미측정";
            // 
            // txtLdsBreakCurrentValue
            // 
            this.txtLdsBreakCurrentValue.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakCurrentValue.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLdsBreakCurrentValue.Location = new System.Drawing.Point(185, 3);
            this.txtLdsBreakCurrentValue.Name = "txtLdsBreakCurrentValue";
            this.txtLdsBreakCurrentValue.Size = new System.Drawing.Size(212, 34);
            this.txtLdsBreakCurrentValue.TabIndex = 31;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.SystemColors.Control;
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label51.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(3, 215);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(115, 34);
            this.label51.TabIndex = 26;
            this.label51.Text = "X 이동간격 (mm)";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.SystemColors.Control;
            this.label52.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label52.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(3, 159);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(39, 34);
            this.label52.TabIndex = 25;
            this.label52.Text = "7";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakXInterval
            // 
            this.txtLdsBreakXInterval.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakXInterval.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakXInterval.ForeColor = System.Drawing.Color.White;
            this.txtLdsBreakXInterval.Location = new System.Drawing.Point(124, 215);
            this.txtLdsBreakXInterval.Name = "txtLdsBreakXInterval";
            this.txtLdsBreakXInterval.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakXInterval.TabIndex = 24;
            // 
            // txtLdsBreakValue7
            // 
            this.txtLdsBreakValue7.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue7.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue7.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue7.Location = new System.Drawing.Point(48, 159);
            this.txtLdsBreakValue7.Name = "txtLdsBreakValue7";
            this.txtLdsBreakValue7.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue7.TabIndex = 23;
            this.txtLdsBreakValue7.Text = "미측정";
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.SystemColors.Control;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label53.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(3, 109);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(39, 34);
            this.label53.TabIndex = 22;
            this.label53.Text = "4";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.SystemColors.Control;
            this.label54.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label54.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(3, 57);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(39, 34);
            this.label54.TabIndex = 21;
            this.label54.Text = "1";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsBreakValue4
            // 
            this.txtLdsBreakValue4.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue4.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue4.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue4.Location = new System.Drawing.Point(48, 109);
            this.txtLdsBreakValue4.Name = "txtLdsBreakValue4";
            this.txtLdsBreakValue4.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue4.TabIndex = 20;
            this.txtLdsBreakValue4.Text = "미측정";
            // 
            // txtLdsBreakValue1
            // 
            this.txtLdsBreakValue1.BackColor = System.Drawing.Color.White;
            this.txtLdsBreakValue1.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsBreakValue1.ForeColor = System.Drawing.Color.Black;
            this.txtLdsBreakValue1.Location = new System.Drawing.Point(48, 57);
            this.txtLdsBreakValue1.Name = "txtLdsBreakValue1";
            this.txtLdsBreakValue1.Size = new System.Drawing.Size(70, 34);
            this.txtLdsBreakValue1.TabIndex = 19;
            this.txtLdsBreakValue1.Text = "미측정";
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.SystemColors.Control;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label55.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(3, 3);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(176, 34);
            this.label55.TabIndex = 18;
            this.label55.Text = "LDS 현재 값(mm) :";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.AutoEllipsis = true;
            this.label56.BackColor = System.Drawing.Color.Gainsboro;
            this.label56.Dock = System.Windows.Forms.DockStyle.Top;
            this.label56.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(0, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(921, 30);
            this.label56.TabIndex = 9;
            this.label56.Text = "■ LDS BREAK";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.panel35);
            this.panel24.Controls.Add(this.btnPowerMeterSave);
            this.panel24.Controls.Add(this.panel25);
            this.panel24.Controls.Add(this.panel34);
            this.panel24.Controls.Add(this.btnPowerMeterClose);
            this.panel24.Controls.Add(this.btnPowerMeterOpen);
            this.panel24.Controls.Add(this.lblPowerMeter);
            this.panel24.Location = new System.Drawing.Point(932, 6);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(923, 393);
            this.panel24.TabIndex = 462;
            // 
            // panel35
            // 
            this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel35.Controls.Add(this.btnAutoCalibrationAutoCalStop);
            this.panel35.Controls.Add(this.btnAutoCalibrationAutoCal);
            this.panel35.Controls.Add(this.txtAutoCalibrationTargetPower);
            this.panel35.Controls.Add(this.label24);
            this.panel35.Controls.Add(this.label25);
            this.panel35.Controls.Add(this.txtAutoCalibrationCurrentPower);
            this.panel35.Controls.Add(this.label26);
            this.panel35.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel35.Location = new System.Drawing.Point(3, 210);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(915, 132);
            this.panel35.TabIndex = 464;
            // 
            // btnAutoCalibrationAutoCalStop
            // 
            this.btnAutoCalibrationAutoCalStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnAutoCalibrationAutoCalStop.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAutoCalibrationAutoCalStop.ForeColor = System.Drawing.Color.Black;
            this.btnAutoCalibrationAutoCalStop.Location = new System.Drawing.Point(286, 81);
            this.btnAutoCalibrationAutoCalStop.Name = "btnAutoCalibrationAutoCalStop";
            this.btnAutoCalibrationAutoCalStop.Size = new System.Drawing.Size(93, 35);
            this.btnAutoCalibrationAutoCalStop.TabIndex = 42;
            this.btnAutoCalibrationAutoCalStop.Text = "AutoCal Stop";
            this.btnAutoCalibrationAutoCalStop.UseVisualStyleBackColor = false;
            // 
            // btnAutoCalibrationAutoCal
            // 
            this.btnAutoCalibrationAutoCal.BackColor = System.Drawing.SystemColors.Control;
            this.btnAutoCalibrationAutoCal.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAutoCalibrationAutoCal.ForeColor = System.Drawing.Color.Black;
            this.btnAutoCalibrationAutoCal.Location = new System.Drawing.Point(187, 81);
            this.btnAutoCalibrationAutoCal.Name = "btnAutoCalibrationAutoCal";
            this.btnAutoCalibrationAutoCal.Size = new System.Drawing.Size(93, 35);
            this.btnAutoCalibrationAutoCal.TabIndex = 41;
            this.btnAutoCalibrationAutoCal.Text = "AutoCal";
            this.btnAutoCalibrationAutoCal.UseVisualStyleBackColor = false;
            // 
            // txtAutoCalibrationTargetPower
            // 
            this.txtAutoCalibrationTargetPower.BackColor = System.Drawing.Color.White;
            this.txtAutoCalibrationTargetPower.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAutoCalibrationTargetPower.ForeColor = System.Drawing.Color.Black;
            this.txtAutoCalibrationTargetPower.Location = new System.Drawing.Point(121, 81);
            this.txtAutoCalibrationTargetPower.Name = "txtAutoCalibrationTargetPower";
            this.txtAutoCalibrationTargetPower.Size = new System.Drawing.Size(60, 35);
            this.txtAutoCalibrationTargetPower.TabIndex = 27;
            // 
            // label24
            // 
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(3, 81);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 34);
            this.label24.TabIndex = 26;
            this.label24.Text = "Target Power[W] :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(3, 41);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(112, 34);
            this.label25.TabIndex = 25;
            this.label25.Text = "Current Power[%] :";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAutoCalibrationCurrentPower
            // 
            this.txtAutoCalibrationCurrentPower.BackColor = System.Drawing.Color.White;
            this.txtAutoCalibrationCurrentPower.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAutoCalibrationCurrentPower.ForeColor = System.Drawing.Color.Black;
            this.txtAutoCalibrationCurrentPower.Location = new System.Drawing.Point(121, 40);
            this.txtAutoCalibrationCurrentPower.Name = "txtAutoCalibrationCurrentPower";
            this.txtAutoCalibrationCurrentPower.Size = new System.Drawing.Size(60, 35);
            this.txtAutoCalibrationCurrentPower.TabIndex = 23;
            // 
            // label26
            // 
            this.label26.AutoEllipsis = true;
            this.label26.BackColor = System.Drawing.Color.Gainsboro;
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(0, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(913, 28);
            this.label26.TabIndex = 9;
            this.label26.Text = "■ AutoCalibration Setting";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPowerMeterSave
            // 
            this.btnPowerMeterSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnPowerMeterSave.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPowerMeterSave.ForeColor = System.Drawing.Color.Black;
            this.btnPowerMeterSave.Location = new System.Drawing.Point(769, 348);
            this.btnPowerMeterSave.Name = "btnPowerMeterSave";
            this.btnPowerMeterSave.Size = new System.Drawing.Size(150, 40);
            this.btnPowerMeterSave.TabIndex = 463;
            this.btnPowerMeterSave.Text = "Save";
            this.btnPowerMeterSave.UseVisualStyleBackColor = false;
            // 
            // panel25
            // 
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.txtMeasureUpTime);
            this.panel25.Controls.Add(this.btnMeasureSettingMeasureStop);
            this.panel25.Controls.Add(this.label21);
            this.panel25.Controls.Add(this.txtRemainMeasureUpTime);
            this.panel25.Controls.Add(this.label2);
            this.panel25.Controls.Add(this.txtWarmUpTime);
            this.panel25.Controls.Add(this.btnMeasureSettingMeasure);
            this.panel25.Controls.Add(this.label15);
            this.panel25.Controls.Add(this.txtRemainWarmUpTime);
            this.panel25.Controls.Add(this.label16);
            this.panel25.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel25.Location = new System.Drawing.Point(566, 79);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(352, 125);
            this.panel25.TabIndex = 462;
            // 
            // txtMeasureUpTime
            // 
            this.txtMeasureUpTime.BackColor = System.Drawing.Color.White;
            this.txtMeasureUpTime.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMeasureUpTime.ForeColor = System.Drawing.Color.Black;
            this.txtMeasureUpTime.Location = new System.Drawing.Point(184, 89);
            this.txtMeasureUpTime.Name = "txtMeasureUpTime";
            this.txtMeasureUpTime.Size = new System.Drawing.Size(63, 29);
            this.txtMeasureUpTime.TabIndex = 41;
            // 
            // btnMeasureSettingMeasureStop
            // 
            this.btnMeasureSettingMeasureStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnMeasureSettingMeasureStop.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMeasureSettingMeasureStop.ForeColor = System.Drawing.Color.Black;
            this.btnMeasureSettingMeasureStop.Location = new System.Drawing.Point(253, 89);
            this.btnMeasureSettingMeasureStop.Name = "btnMeasureSettingMeasureStop";
            this.btnMeasureSettingMeasureStop.Size = new System.Drawing.Size(93, 29);
            this.btnMeasureSettingMeasureStop.TabIndex = 40;
            this.btnMeasureSettingMeasureStop.Text = "Measure Stop";
            this.btnMeasureSettingMeasureStop.UseVisualStyleBackColor = false;
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(3, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 29);
            this.label21.TabIndex = 39;
            this.label21.Text = "Measure Time[s] :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemainMeasureUpTime
            // 
            this.txtRemainMeasureUpTime.BackColor = System.Drawing.Color.White;
            this.txtRemainMeasureUpTime.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRemainMeasureUpTime.ForeColor = System.Drawing.Color.Black;
            this.txtRemainMeasureUpTime.Location = new System.Drawing.Point(115, 89);
            this.txtRemainMeasureUpTime.Name = "txtRemainMeasureUpTime";
            this.txtRemainMeasureUpTime.Size = new System.Drawing.Size(63, 29);
            this.txtRemainMeasureUpTime.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(115, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 37;
            this.label2.Text = "남은 시간";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWarmUpTime
            // 
            this.txtWarmUpTime.BackColor = System.Drawing.Color.White;
            this.txtWarmUpTime.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtWarmUpTime.ForeColor = System.Drawing.Color.Black;
            this.txtWarmUpTime.Location = new System.Drawing.Point(184, 57);
            this.txtWarmUpTime.Name = "txtWarmUpTime";
            this.txtWarmUpTime.Size = new System.Drawing.Size(63, 29);
            this.txtWarmUpTime.TabIndex = 36;
            // 
            // btnMeasureSettingMeasure
            // 
            this.btnMeasureSettingMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnMeasureSettingMeasure.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMeasureSettingMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnMeasureSettingMeasure.Location = new System.Drawing.Point(253, 57);
            this.btnMeasureSettingMeasure.Name = "btnMeasureSettingMeasure";
            this.btnMeasureSettingMeasure.Size = new System.Drawing.Size(93, 29);
            this.btnMeasureSettingMeasure.TabIndex = 34;
            this.btnMeasureSettingMeasure.Text = "Measure";
            this.btnMeasureSettingMeasure.UseVisualStyleBackColor = false;
            this.btnMeasureSettingMeasure.Click += new System.EventHandler(this.btnPowerMeter_Click);
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(3, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 29);
            this.label15.TabIndex = 32;
            this.label15.Text = "WarmUp Time[s] :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemainWarmUpTime
            // 
            this.txtRemainWarmUpTime.BackColor = System.Drawing.Color.White;
            this.txtRemainWarmUpTime.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRemainWarmUpTime.ForeColor = System.Drawing.Color.Black;
            this.txtRemainWarmUpTime.Location = new System.Drawing.Point(115, 57);
            this.txtRemainWarmUpTime.Name = "txtRemainWarmUpTime";
            this.txtRemainWarmUpTime.Size = new System.Drawing.Size(63, 29);
            this.txtRemainWarmUpTime.TabIndex = 30;
            // 
            // label16
            // 
            this.label16.AutoEllipsis = true;
            this.label16.BackColor = System.Drawing.Color.Gainsboro;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(350, 28);
            this.label16.TabIndex = 9;
            this.label16.Text = "■ Measure Setting";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.txtPowerStatusAvgPower);
            this.panel34.Controls.Add(this.label20);
            this.panel34.Controls.Add(this.txtPowerStatusLowPower);
            this.panel34.Controls.Add(this.label19);
            this.panel34.Controls.Add(this.txtPowerStatusMaxPower);
            this.panel34.Controls.Add(this.label17);
            this.panel34.Controls.Add(this.label18);
            this.panel34.Controls.Add(this.txtPowerStatusCureentPower);
            this.panel34.Controls.Add(this.lblPowerStatus);
            this.panel34.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel34.Location = new System.Drawing.Point(3, 79);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(557, 125);
            this.panel34.TabIndex = 461;
            // 
            // txtPowerStatusAvgPower
            // 
            this.txtPowerStatusAvgPower.BackColor = System.Drawing.Color.White;
            this.txtPowerStatusAvgPower.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtPowerStatusAvgPower.ForeColor = System.Drawing.Color.Black;
            this.txtPowerStatusAvgPower.Location = new System.Drawing.Point(489, 74);
            this.txtPowerStatusAvgPower.Name = "txtPowerStatusAvgPower";
            this.txtPowerStatusAvgPower.Size = new System.Drawing.Size(60, 35);
            this.txtPowerStatusAvgPower.TabIndex = 31;
            // 
            // label20
            // 
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(371, 75);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(112, 34);
            this.label20.TabIndex = 30;
            this.label20.Text = "Avg Power[W] :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPowerStatusLowPower
            // 
            this.txtPowerStatusLowPower.BackColor = System.Drawing.Color.White;
            this.txtPowerStatusLowPower.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtPowerStatusLowPower.ForeColor = System.Drawing.Color.Black;
            this.txtPowerStatusLowPower.Location = new System.Drawing.Point(305, 75);
            this.txtPowerStatusLowPower.Name = "txtPowerStatusLowPower";
            this.txtPowerStatusLowPower.Size = new System.Drawing.Size(60, 35);
            this.txtPowerStatusLowPower.TabIndex = 29;
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(187, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 34);
            this.label19.TabIndex = 28;
            this.label19.Text = "Low Power[W] :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPowerStatusMaxPower
            // 
            this.txtPowerStatusMaxPower.BackColor = System.Drawing.Color.White;
            this.txtPowerStatusMaxPower.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtPowerStatusMaxPower.ForeColor = System.Drawing.Color.Black;
            this.txtPowerStatusMaxPower.Location = new System.Drawing.Point(121, 77);
            this.txtPowerStatusMaxPower.Name = "txtPowerStatusMaxPower";
            this.txtPowerStatusMaxPower.Size = new System.Drawing.Size(60, 35);
            this.txtPowerStatusMaxPower.TabIndex = 27;
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(3, 77);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 34);
            this.label17.TabIndex = 26;
            this.label17.Text = "Max Power[W] :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(3, 37);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 34);
            this.label18.TabIndex = 25;
            this.label18.Text = "Current Power[W] :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPowerStatusCureentPower
            // 
            this.txtPowerStatusCureentPower.BackColor = System.Drawing.Color.White;
            this.txtPowerStatusCureentPower.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtPowerStatusCureentPower.ForeColor = System.Drawing.Color.Black;
            this.txtPowerStatusCureentPower.Location = new System.Drawing.Point(121, 36);
            this.txtPowerStatusCureentPower.Name = "txtPowerStatusCureentPower";
            this.txtPowerStatusCureentPower.Size = new System.Drawing.Size(60, 35);
            this.txtPowerStatusCureentPower.TabIndex = 23;
            // 
            // lblPowerStatus
            // 
            this.lblPowerStatus.AutoEllipsis = true;
            this.lblPowerStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.lblPowerStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPowerStatus.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblPowerStatus.ForeColor = System.Drawing.Color.Black;
            this.lblPowerStatus.Location = new System.Drawing.Point(0, 0);
            this.lblPowerStatus.Name = "lblPowerStatus";
            this.lblPowerStatus.Size = new System.Drawing.Size(555, 28);
            this.lblPowerStatus.TabIndex = 9;
            this.lblPowerStatus.Text = "■ PowerStatus";
            this.lblPowerStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPowerMeterClose
            // 
            this.btnPowerMeterClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnPowerMeterClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPowerMeterClose.ForeColor = System.Drawing.Color.Black;
            this.btnPowerMeterClose.Location = new System.Drawing.Point(102, 33);
            this.btnPowerMeterClose.Name = "btnPowerMeterClose";
            this.btnPowerMeterClose.Size = new System.Drawing.Size(93, 40);
            this.btnPowerMeterClose.TabIndex = 459;
            this.btnPowerMeterClose.Text = "Close";
            this.btnPowerMeterClose.UseVisualStyleBackColor = false;
            this.btnPowerMeterClose.Click += new System.EventHandler(this.btnPowerMeter_Click);
            // 
            // btnPowerMeterOpen
            // 
            this.btnPowerMeterOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnPowerMeterOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPowerMeterOpen.ForeColor = System.Drawing.Color.Black;
            this.btnPowerMeterOpen.Location = new System.Drawing.Point(3, 33);
            this.btnPowerMeterOpen.Name = "btnPowerMeterOpen";
            this.btnPowerMeterOpen.Size = new System.Drawing.Size(93, 40);
            this.btnPowerMeterOpen.TabIndex = 458;
            this.btnPowerMeterOpen.Text = "Open";
            this.btnPowerMeterOpen.UseVisualStyleBackColor = false;
            this.btnPowerMeterOpen.Click += new System.EventHandler(this.btnPowerMeter_Click);
            // 
            // lblPowerMeter
            // 
            this.lblPowerMeter.AutoEllipsis = true;
            this.lblPowerMeter.BackColor = System.Drawing.Color.Gainsboro;
            this.lblPowerMeter.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPowerMeter.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblPowerMeter.ForeColor = System.Drawing.Color.Black;
            this.lblPowerMeter.Location = new System.Drawing.Point(0, 0);
            this.lblPowerMeter.Name = "lblPowerMeter";
            this.lblPowerMeter.Size = new System.Drawing.Size(921, 30);
            this.lblPowerMeter.TabIndex = 9;
            this.lblPowerMeter.Text = "■ PowerMeter";
            this.lblPowerMeter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel26
            // 
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel26.Controls.Add(this.btnLdsPorcessSave);
            this.panel26.Controls.Add(this.txtLdsProcessState);
            this.panel26.Controls.Add(this.label44);
            this.panel26.Controls.Add(this.txtLdsProcessTableIdx);
            this.panel26.Controls.Add(this.label43);
            this.panel26.Controls.Add(this.txtLdsProcessMeasureTime);
            this.panel26.Controls.Add(this.label42);
            this.panel26.Controls.Add(this.txtLdsProcessReadyTime);
            this.panel26.Controls.Add(this.btnLdsProcessStop);
            this.panel26.Controls.Add(this.btnLdsProcessStart);
            this.panel26.Controls.Add(this.panel29);
            this.panel26.Controls.Add(this.lblLdsProcess);
            this.panel26.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel26.Location = new System.Drawing.Point(4, 405);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(923, 393);
            this.panel26.TabIndex = 461;
            // 
            // btnLdsPorcessSave
            // 
            this.btnLdsPorcessSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnLdsPorcessSave.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLdsPorcessSave.ForeColor = System.Drawing.Color.Black;
            this.btnLdsPorcessSave.Location = new System.Drawing.Point(767, 348);
            this.btnLdsPorcessSave.Name = "btnLdsPorcessSave";
            this.btnLdsPorcessSave.Size = new System.Drawing.Size(150, 40);
            this.btnLdsPorcessSave.TabIndex = 464;
            this.btnLdsPorcessSave.Text = "Save";
            this.btnLdsPorcessSave.UseVisualStyleBackColor = false;
            // 
            // txtLdsProcessState
            // 
            this.txtLdsProcessState.BackColor = System.Drawing.SystemColors.Control;
            this.txtLdsProcessState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtLdsProcessState.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLdsProcessState.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessState.Location = new System.Drawing.Point(622, 33);
            this.txtLdsProcessState.Name = "txtLdsProcessState";
            this.txtLdsProcessState.Size = new System.Drawing.Size(95, 40);
            this.txtLdsProcessState.TabIndex = 63;
            this.txtLdsProcessState.Text = "대기중";
            this.txtLdsProcessState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.SystemColors.Control;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label44.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(408, 33);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(95, 40);
            this.label44.TabIndex = 62;
            this.label44.Text = "테이블 인덱스 :";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessTableIdx
            // 
            this.txtLdsProcessTableIdx.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessTableIdx.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLdsProcessTableIdx.ForeColor = System.Drawing.Color.White;
            this.txtLdsProcessTableIdx.Location = new System.Drawing.Point(509, 33);
            this.txtLdsProcessTableIdx.Name = "txtLdsProcessTableIdx";
            this.txtLdsProcessTableIdx.Size = new System.Drawing.Size(105, 39);
            this.txtLdsProcessTableIdx.TabIndex = 61;
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.SystemColors.Control;
            this.label43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label43.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(216, 32);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(75, 40);
            this.label43.TabIndex = 60;
            this.label43.Text = "측정 시간[s]";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessMeasureTime
            // 
            this.txtLdsProcessMeasureTime.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessMeasureTime.Font = new System.Drawing.Font("Malgun Gothic", 18F);
            this.txtLdsProcessMeasureTime.ForeColor = System.Drawing.Color.White;
            this.txtLdsProcessMeasureTime.Location = new System.Drawing.Point(297, 33);
            this.txtLdsProcessMeasureTime.Name = "txtLdsProcessMeasureTime";
            this.txtLdsProcessMeasureTime.Size = new System.Drawing.Size(105, 39);
            this.txtLdsProcessMeasureTime.TabIndex = 59;
            // 
            // label42
            // 
            this.label42.BackColor = System.Drawing.SystemColors.Control;
            this.label42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label42.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(4, 33);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(95, 40);
            this.label42.TabIndex = 58;
            this.label42.Text = "대기 시간[s]";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessReadyTime
            // 
            this.txtLdsProcessReadyTime.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessReadyTime.Font = new System.Drawing.Font("Malgun Gothic", 18F);
            this.txtLdsProcessReadyTime.ForeColor = System.Drawing.Color.White;
            this.txtLdsProcessReadyTime.Location = new System.Drawing.Point(105, 33);
            this.txtLdsProcessReadyTime.Name = "txtLdsProcessReadyTime";
            this.txtLdsProcessReadyTime.Size = new System.Drawing.Size(105, 39);
            this.txtLdsProcessReadyTime.TabIndex = 57;
            // 
            // btnLdsProcessStop
            // 
            this.btnLdsProcessStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnLdsProcessStop.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnLdsProcessStop.ForeColor = System.Drawing.Color.Black;
            this.btnLdsProcessStop.Location = new System.Drawing.Point(823, 33);
            this.btnLdsProcessStop.Name = "btnLdsProcessStop";
            this.btnLdsProcessStop.Size = new System.Drawing.Size(93, 40);
            this.btnLdsProcessStop.TabIndex = 56;
            this.btnLdsProcessStop.Text = "측정 정지";
            this.btnLdsProcessStop.UseVisualStyleBackColor = false;
            // 
            // btnLdsProcessStart
            // 
            this.btnLdsProcessStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnLdsProcessStart.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.btnLdsProcessStart.ForeColor = System.Drawing.Color.Black;
            this.btnLdsProcessStart.Location = new System.Drawing.Point(724, 33);
            this.btnLdsProcessStart.Name = "btnLdsProcessStart";
            this.btnLdsProcessStart.Size = new System.Drawing.Size(93, 40);
            this.btnLdsProcessStart.TabIndex = 55;
            this.btnLdsProcessStart.Text = "측정 시작";
            this.btnLdsProcessStart.UseVisualStyleBackColor = false;
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.lviLdsProcess);
            this.panel29.Controls.Add(this.label41);
            this.panel29.Controls.Add(this.txtLdsProcessYInterval);
            this.panel29.Controls.Add(this.label4);
            this.panel29.Controls.Add(this.txtLdsProcessValue9);
            this.panel29.Controls.Add(this.label5);
            this.panel29.Controls.Add(this.label40);
            this.panel29.Controls.Add(this.txtLdsProcessValue6);
            this.panel29.Controls.Add(this.txtLdsProcessValue3);
            this.panel29.Controls.Add(this.label6);
            this.panel29.Controls.Add(this.txtLdsProcessValue8);
            this.panel29.Controls.Add(this.label7);
            this.panel29.Controls.Add(this.label8);
            this.panel29.Controls.Add(this.txtLdsProcessValue5);
            this.panel29.Controls.Add(this.txtLdsProcessValue2);
            this.panel29.Controls.Add(this.txtLdsProcessCurrentValue);
            this.panel29.Controls.Add(this.label22);
            this.panel29.Controls.Add(this.label23);
            this.panel29.Controls.Add(this.txtLdsProcessXInterval);
            this.panel29.Controls.Add(this.txtLdsProcessValue7);
            this.panel29.Controls.Add(this.label9);
            this.panel29.Controls.Add(this.label10);
            this.panel29.Controls.Add(this.txtLdsProcessValue4);
            this.panel29.Controls.Add(this.txtLdsProcessValue1);
            this.panel29.Controls.Add(this.label11);
            this.panel29.Location = new System.Drawing.Point(4, 88);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(913, 254);
            this.panel29.TabIndex = 54;
            // 
            // lviLdsProcess
            // 
            this.lviLdsProcess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lviLdsProcess.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lviLdsProcess.GridLines = true;
            this.lviLdsProcess.Location = new System.Drawing.Point(403, 3);
            this.lviLdsProcess.Name = "lviLdsProcess";
            this.lviLdsProcess.Size = new System.Drawing.Size(505, 246);
            this.lviLdsProcess.TabIndex = 46;
            this.lviLdsProcess.UseCompatibleStateImageBehavior = false;
            this.lviLdsProcess.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "인덱스";
            this.columnHeader1.Width = 134;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "측정값";
            this.columnHeader2.Width = 121;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "차이값";
            this.columnHeader3.Width = 130;
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.SystemColors.Control;
            this.label41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label41.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(203, 215);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(105, 34);
            this.label41.TabIndex = 45;
            this.label41.Text = "Y 이동간격 (mm)";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessYInterval
            // 
            this.txtLdsProcessYInterval.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessYInterval.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessYInterval.ForeColor = System.Drawing.Color.White;
            this.txtLdsProcessYInterval.Location = new System.Drawing.Point(314, 215);
            this.txtLdsProcessYInterval.Name = "txtLdsProcessYInterval";
            this.txtLdsProcessYInterval.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessYInterval.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(282, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 34);
            this.label4.TabIndex = 43;
            this.label4.Text = "9";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessValue9
            // 
            this.txtLdsProcessValue9.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue9.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue9.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue9.Location = new System.Drawing.Point(327, 159);
            this.txtLdsProcessValue9.Name = "txtLdsProcessValue9";
            this.txtLdsProcessValue9.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue9.TabIndex = 42;
            this.txtLdsProcessValue9.Text = "미측정";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(282, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 34);
            this.label5.TabIndex = 41;
            this.label5.Text = "6";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.SystemColors.Control;
            this.label40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label40.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(282, 57);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(39, 34);
            this.label40.TabIndex = 40;
            this.label40.Text = "3";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessValue6
            // 
            this.txtLdsProcessValue6.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue6.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue6.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue6.Location = new System.Drawing.Point(327, 109);
            this.txtLdsProcessValue6.Name = "txtLdsProcessValue6";
            this.txtLdsProcessValue6.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue6.TabIndex = 39;
            this.txtLdsProcessValue6.Text = "미측정";
            // 
            // txtLdsProcessValue3
            // 
            this.txtLdsProcessValue3.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue3.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue3.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue3.Location = new System.Drawing.Point(327, 57);
            this.txtLdsProcessValue3.Name = "txtLdsProcessValue3";
            this.txtLdsProcessValue3.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue3.TabIndex = 38;
            this.txtLdsProcessValue3.Text = "미측정";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(144, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 34);
            this.label6.TabIndex = 37;
            this.label6.Text = "8";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessValue8
            // 
            this.txtLdsProcessValue8.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue8.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue8.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue8.Location = new System.Drawing.Point(189, 159);
            this.txtLdsProcessValue8.Name = "txtLdsProcessValue8";
            this.txtLdsProcessValue8.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue8.TabIndex = 36;
            this.txtLdsProcessValue8.Text = "미측정";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(144, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 34);
            this.label7.TabIndex = 35;
            this.label7.Text = "5";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(144, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 34);
            this.label8.TabIndex = 34;
            this.label8.Text = "2";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessValue5
            // 
            this.txtLdsProcessValue5.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue5.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue5.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue5.Location = new System.Drawing.Point(189, 109);
            this.txtLdsProcessValue5.Name = "txtLdsProcessValue5";
            this.txtLdsProcessValue5.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue5.TabIndex = 33;
            this.txtLdsProcessValue5.Text = "미측정";
            // 
            // txtLdsProcessValue2
            // 
            this.txtLdsProcessValue2.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue2.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue2.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue2.Location = new System.Drawing.Point(189, 57);
            this.txtLdsProcessValue2.Name = "txtLdsProcessValue2";
            this.txtLdsProcessValue2.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue2.TabIndex = 32;
            this.txtLdsProcessValue2.Text = "미측정";
            // 
            // txtLdsProcessCurrentValue
            // 
            this.txtLdsProcessCurrentValue.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessCurrentValue.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLdsProcessCurrentValue.Location = new System.Drawing.Point(185, 3);
            this.txtLdsProcessCurrentValue.Name = "txtLdsProcessCurrentValue";
            this.txtLdsProcessCurrentValue.Size = new System.Drawing.Size(212, 34);
            this.txtLdsProcessCurrentValue.TabIndex = 31;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.SystemColors.Control;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(3, 215);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(115, 34);
            this.label22.TabIndex = 26;
            this.label22.Text = "X 이동간격 (mm)";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.SystemColors.Control;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(3, 159);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 34);
            this.label23.TabIndex = 25;
            this.label23.Text = "7";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessXInterval
            // 
            this.txtLdsProcessXInterval.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessXInterval.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessXInterval.ForeColor = System.Drawing.Color.White;
            this.txtLdsProcessXInterval.Location = new System.Drawing.Point(124, 215);
            this.txtLdsProcessXInterval.Name = "txtLdsProcessXInterval";
            this.txtLdsProcessXInterval.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessXInterval.TabIndex = 24;
            // 
            // txtLdsProcessValue7
            // 
            this.txtLdsProcessValue7.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue7.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue7.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue7.Location = new System.Drawing.Point(48, 159);
            this.txtLdsProcessValue7.Name = "txtLdsProcessValue7";
            this.txtLdsProcessValue7.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue7.TabIndex = 23;
            this.txtLdsProcessValue7.Text = "미측정";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(3, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 34);
            this.label9.TabIndex = 22;
            this.label9.Text = "4";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(3, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 34);
            this.label10.TabIndex = 21;
            this.label10.Text = "1";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLdsProcessValue4
            // 
            this.txtLdsProcessValue4.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue4.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue4.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue4.Location = new System.Drawing.Point(48, 109);
            this.txtLdsProcessValue4.Name = "txtLdsProcessValue4";
            this.txtLdsProcessValue4.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue4.TabIndex = 20;
            this.txtLdsProcessValue4.Text = "미측정";
            // 
            // txtLdsProcessValue1
            // 
            this.txtLdsProcessValue1.BackColor = System.Drawing.Color.White;
            this.txtLdsProcessValue1.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.txtLdsProcessValue1.ForeColor = System.Drawing.Color.Black;
            this.txtLdsProcessValue1.Location = new System.Drawing.Point(48, 57);
            this.txtLdsProcessValue1.Name = "txtLdsProcessValue1";
            this.txtLdsProcessValue1.Size = new System.Drawing.Size(70, 34);
            this.txtLdsProcessValue1.TabIndex = 19;
            this.txtLdsProcessValue1.Text = "미측정";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(176, 34);
            this.label11.TabIndex = 18;
            this.label11.Text = "LDS 현재 값(mm) :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdsProcess
            // 
            this.lblLdsProcess.AutoEllipsis = true;
            this.lblLdsProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLdsProcess.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLdsProcess.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblLdsProcess.ForeColor = System.Drawing.Color.Black;
            this.lblLdsProcess.Location = new System.Drawing.Point(0, 0);
            this.lblLdsProcess.Name = "lblLdsProcess";
            this.lblLdsProcess.Size = new System.Drawing.Size(921, 30);
            this.lblLdsProcess.TabIndex = 9;
            this.lblLdsProcess.Text = "■ LDS PROCESS";
            this.lblLdsProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.panel27);
            this.tabPage4.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabPage4.Location = new System.Drawing.Point(4, 44);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1861, 802);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "컷라인 계측";
            // 
            // panel27
            // 
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.btnCutLineColBR2Table);
            this.panel27.Controls.Add(this.btnCutLineColBR1Table);
            this.panel27.Controls.Add(this.btnCutLineColAL2Table);
            this.panel27.Controls.Add(this.btnCutLineColAL1Table);
            this.panel27.Controls.Add(this.label62);
            this.panel27.Location = new System.Drawing.Point(4, 6);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(923, 255);
            this.panel27.TabIndex = 457;
            // 
            // btnCutLineColBR2Table
            // 
            this.btnCutLineColBR2Table.BackColor = System.Drawing.SystemColors.Control;
            this.btnCutLineColBR2Table.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCutLineColBR2Table.ForeColor = System.Drawing.Color.Black;
            this.btnCutLineColBR2Table.Location = new System.Drawing.Point(692, 33);
            this.btnCutLineColBR2Table.Name = "btnCutLineColBR2Table";
            this.btnCutLineColBR2Table.Size = new System.Drawing.Size(223, 120);
            this.btnCutLineColBR2Table.TabIndex = 465;
            this.btnCutLineColBR2Table.Text = "B열 R2테이블";
            this.btnCutLineColBR2Table.UseVisualStyleBackColor = false;
            // 
            // btnCutLineColBR1Table
            // 
            this.btnCutLineColBR1Table.BackColor = System.Drawing.SystemColors.Control;
            this.btnCutLineColBR1Table.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCutLineColBR1Table.ForeColor = System.Drawing.Color.Black;
            this.btnCutLineColBR1Table.Location = new System.Drawing.Point(463, 33);
            this.btnCutLineColBR1Table.Name = "btnCutLineColBR1Table";
            this.btnCutLineColBR1Table.Size = new System.Drawing.Size(223, 120);
            this.btnCutLineColBR1Table.TabIndex = 464;
            this.btnCutLineColBR1Table.Text = "B열 R1테이블";
            this.btnCutLineColBR1Table.UseVisualStyleBackColor = false;
            // 
            // btnCutLineColAL2Table
            // 
            this.btnCutLineColAL2Table.BackColor = System.Drawing.SystemColors.Control;
            this.btnCutLineColAL2Table.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCutLineColAL2Table.ForeColor = System.Drawing.Color.Black;
            this.btnCutLineColAL2Table.Location = new System.Drawing.Point(234, 33);
            this.btnCutLineColAL2Table.Name = "btnCutLineColAL2Table";
            this.btnCutLineColAL2Table.Size = new System.Drawing.Size(223, 120);
            this.btnCutLineColAL2Table.TabIndex = 459;
            this.btnCutLineColAL2Table.Text = "A열 L2테이블";
            this.btnCutLineColAL2Table.UseVisualStyleBackColor = false;
            // 
            // btnCutLineColAL1Table
            // 
            this.btnCutLineColAL1Table.BackColor = System.Drawing.SystemColors.Control;
            this.btnCutLineColAL1Table.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCutLineColAL1Table.ForeColor = System.Drawing.Color.Black;
            this.btnCutLineColAL1Table.Location = new System.Drawing.Point(5, 33);
            this.btnCutLineColAL1Table.Name = "btnCutLineColAL1Table";
            this.btnCutLineColAL1Table.Size = new System.Drawing.Size(223, 120);
            this.btnCutLineColAL1Table.TabIndex = 458;
            this.btnCutLineColAL1Table.Text = "A열 L1테이블";
            this.btnCutLineColAL1Table.UseVisualStyleBackColor = false;
            // 
            // label62
            // 
            this.label62.AutoEllipsis = true;
            this.label62.BackColor = System.Drawing.Color.Gainsboro;
            this.label62.Dock = System.Windows.Forms.DockStyle.Top;
            this.label62.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(0, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(921, 30);
            this.label62.TabIndex = 9;
            this.label62.Text = "■ 컷라인 계측";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel36);
            this.tabPage5.Controls.Add(this.panel28);
            this.tabPage5.Location = new System.Drawing.Point(4, 44);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1861, 802);
            this.tabPage5.TabIndex = 7;
            this.tabPage5.Text = "검사/얼라인";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel36
            // 
            this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel36.Controls.Add(this.cboAlignCmd);
            this.panel36.Controls.Add(this.btnAlignConnect);
            this.panel36.Controls.Add(this.btnAlignCmd);
            this.panel36.Controls.Add(this.label57);
            this.panel36.Location = new System.Drawing.Point(20, 236);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(394, 152);
            this.panel36.TabIndex = 458;
            // 
            // cboAlignCmd
            // 
            this.cboAlignCmd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAlignCmd.FormattingEnabled = true;
            this.cboAlignCmd.Items.AddRange(new object[] {
            "AlignStart",
            "AlignBkSet",
            "MeasureGrabStart",
            "JigAlignStart"});
            this.cboAlignCmd.Location = new System.Drawing.Point(20, 98);
            this.cboAlignCmd.Name = "cboAlignCmd";
            this.cboAlignCmd.Size = new System.Drawing.Size(157, 23);
            this.cboAlignCmd.TabIndex = 459;
            // 
            // btnAlignConnect
            // 
            this.btnAlignConnect.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignConnect.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignConnect.ForeColor = System.Drawing.Color.Black;
            this.btnAlignConnect.Location = new System.Drawing.Point(196, 43);
            this.btnAlignConnect.Name = "btnAlignConnect";
            this.btnAlignConnect.Size = new System.Drawing.Size(153, 49);
            this.btnAlignConnect.TabIndex = 458;
            this.btnAlignConnect.Text = "접속";
            this.btnAlignConnect.UseVisualStyleBackColor = false;
            this.btnAlignConnect.Click += new System.EventHandler(this.btnAlignConnect_Click);
            // 
            // btnAlignCmd
            // 
            this.btnAlignCmd.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlignCmd.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlignCmd.ForeColor = System.Drawing.Color.Black;
            this.btnAlignCmd.Location = new System.Drawing.Point(196, 98);
            this.btnAlignCmd.Name = "btnAlignCmd";
            this.btnAlignCmd.Size = new System.Drawing.Size(153, 49);
            this.btnAlignCmd.TabIndex = 458;
            this.btnAlignCmd.Text = "얼라인";
            this.btnAlignCmd.UseVisualStyleBackColor = false;
            this.btnAlignCmd.Click += new System.EventHandler(this.btnAlignCmd_Click);
            // 
            // label57
            // 
            this.label57.AutoEllipsis = true;
            this.label57.BackColor = System.Drawing.Color.Gainsboro;
            this.label57.Dock = System.Windows.Forms.DockStyle.Top;
            this.label57.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(0, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(392, 30);
            this.label57.TabIndex = 9;
            this.label57.Text = "■ 얼라인";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel28
            // 
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.cboInspCmd);
            this.panel28.Controls.Add(this.btnInspConnect);
            this.panel28.Controls.Add(this.btnInspCmd);
            this.panel28.Controls.Add(this.label1);
            this.panel28.Location = new System.Drawing.Point(20, 32);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(394, 169);
            this.panel28.TabIndex = 458;
            // 
            // cboInspCmd
            // 
            this.cboInspCmd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInspCmd.FormattingEnabled = true;
            this.cboInspCmd.Items.AddRange(new object[] {
            "InspReady",
            "GrabStart",
            "GrabEnd",
            "InspReset",
            "MeasureReady",
            "MeasureStart"});
            this.cboInspCmd.Location = new System.Drawing.Point(20, 105);
            this.cboInspCmd.Name = "cboInspCmd";
            this.cboInspCmd.Size = new System.Drawing.Size(157, 23);
            this.cboInspCmd.TabIndex = 459;
            // 
            // btnInspConnect
            // 
            this.btnInspConnect.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspConnect.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspConnect.ForeColor = System.Drawing.Color.Black;
            this.btnInspConnect.Location = new System.Drawing.Point(196, 33);
            this.btnInspConnect.Name = "btnInspConnect";
            this.btnInspConnect.Size = new System.Drawing.Size(153, 49);
            this.btnInspConnect.TabIndex = 458;
            this.btnInspConnect.Text = "접속";
            this.btnInspConnect.UseVisualStyleBackColor = false;
            this.btnInspConnect.Click += new System.EventHandler(this.btnInspConnect_Click);
            // 
            // btnInspCmd
            // 
            this.btnInspCmd.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspCmd.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspCmd.ForeColor = System.Drawing.Color.Black;
            this.btnInspCmd.Location = new System.Drawing.Point(196, 105);
            this.btnInspCmd.Name = "btnInspCmd";
            this.btnInspCmd.Size = new System.Drawing.Size(153, 49);
            this.btnInspCmd.TabIndex = 458;
            this.btnInspCmd.Text = "검사";
            this.btnInspCmd.UseVisualStyleBackColor = false;
            this.btnInspCmd.Click += new System.EventHandler(this.btnInspCmd_Click);
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(392, 30);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ 검사";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBeam
            // 
            this.lblBeam.AutoEllipsis = true;
            this.lblBeam.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBeam.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBeam.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblBeam.ForeColor = System.Drawing.Color.Black;
            this.lblBeam.Location = new System.Drawing.Point(0, 0);
            this.lblBeam.Name = "lblBeam";
            this.lblBeam.Size = new System.Drawing.Size(921, 30);
            this.lblBeam.TabIndex = 9;
            this.lblBeam.Text = "■ BeamPositionner";
            this.lblBeam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.btnBeamSettingStart);
            this.panel32.Controls.Add(this.btnBeamSettingStop);
            this.panel32.Controls.Add(this.txtBeamPosYMedianValue);
            this.panel32.Controls.Add(this.txtBeamPosYMaxValue);
            this.panel32.Controls.Add(this.txtBeamPosXMedianValue);
            this.panel32.Controls.Add(this.txtBeamPosXMaxValue);
            this.panel32.Controls.Add(this.txtBeamPowerMedianValue);
            this.panel32.Controls.Add(this.txtBeamPowerMaxValue);
            this.panel32.Controls.Add(this.txtBeamPosYCurrenValue);
            this.panel32.Controls.Add(this.txtBeamPosYMiniValue);
            this.panel32.Controls.Add(this.txtBeamPowerCurrenValue);
            this.panel32.Controls.Add(this.txtBeamPowerMiniValue);
            this.panel32.Controls.Add(this.txtBeamPosXCurrenValue);
            this.panel32.Controls.Add(this.txtBeamPosXMiniValue);
            this.panel32.Controls.Add(this.label36);
            this.panel32.Controls.Add(this.label35);
            this.panel32.Controls.Add(this.label34);
            this.panel32.Controls.Add(this.label30);
            this.panel32.Controls.Add(this.label31);
            this.panel32.Controls.Add(this.label32);
            this.panel32.Controls.Add(this.label33);
            this.panel32.Controls.Add(this.lblBeamPowerSetting);
            this.panel32.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel32.Location = new System.Drawing.Point(3, 79);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(455, 309);
            this.panel32.TabIndex = 456;
            // 
            // lblBeamPowerSetting
            // 
            this.lblBeamPowerSetting.AutoEllipsis = true;
            this.lblBeamPowerSetting.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBeamPowerSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBeamPowerSetting.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblBeamPowerSetting.ForeColor = System.Drawing.Color.Black;
            this.lblBeamPowerSetting.Location = new System.Drawing.Point(0, 0);
            this.lblBeamPowerSetting.Name = "lblBeamPowerSetting";
            this.lblBeamPowerSetting.Size = new System.Drawing.Size(453, 28);
            this.lblBeamPowerSetting.TabIndex = 9;
            this.lblBeamPowerSetting.Text = "■ Setting";
            this.lblBeamPowerSetting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.SystemColors.Control;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(3, 63);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(91, 61);
            this.label33.TabIndex = 48;
            this.label33.Text = "power(mw)";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.SystemColors.Control;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label32.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(3, 130);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(91, 61);
            this.label32.TabIndex = 49;
            this.label32.Text = "PosX(um)";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.SystemColors.Control;
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label31.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(3, 197);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(91, 61);
            this.label31.TabIndex = 50;
            this.label31.Text = "PosY(um)";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.SystemColors.Control;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label30.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(100, 33);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 27);
            this.label30.TabIndex = 51;
            this.label30.Text = "현재값";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.SystemColors.Control;
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label34.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(189, 33);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(83, 27);
            this.label34.TabIndex = 52;
            this.label34.Text = "최소값";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.SystemColors.Control;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(278, 33);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 27);
            this.label35.TabIndex = 53;
            this.label35.Text = "최대값";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.SystemColors.Control;
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(367, 33);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(83, 27);
            this.label36.TabIndex = 54;
            this.label36.Text = "중간값";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBeamPosXMiniValue
            // 
            this.txtBeamPosXMiniValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosXMiniValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosXMiniValue.Location = new System.Drawing.Point(189, 130);
            this.txtBeamPosXMiniValue.Name = "txtBeamPosXMiniValue";
            this.txtBeamPosXMiniValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosXMiniValue.TabIndex = 60;
            // 
            // txtBeamPosXCurrenValue
            // 
            this.txtBeamPosXCurrenValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosXCurrenValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosXCurrenValue.Location = new System.Drawing.Point(100, 130);
            this.txtBeamPosXCurrenValue.Name = "txtBeamPosXCurrenValue";
            this.txtBeamPosXCurrenValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosXCurrenValue.TabIndex = 59;
            // 
            // txtBeamPowerMiniValue
            // 
            this.txtBeamPowerMiniValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPowerMiniValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPowerMiniValue.Location = new System.Drawing.Point(189, 63);
            this.txtBeamPowerMiniValue.Name = "txtBeamPowerMiniValue";
            this.txtBeamPowerMiniValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPowerMiniValue.TabIndex = 55;
            // 
            // txtBeamPowerCurrenValue
            // 
            this.txtBeamPowerCurrenValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPowerCurrenValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPowerCurrenValue.Location = new System.Drawing.Point(100, 63);
            this.txtBeamPowerCurrenValue.Name = "txtBeamPowerCurrenValue";
            this.txtBeamPowerCurrenValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPowerCurrenValue.TabIndex = 56;
            // 
            // txtBeamPosYMiniValue
            // 
            this.txtBeamPosYMiniValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosYMiniValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosYMiniValue.Location = new System.Drawing.Point(189, 197);
            this.txtBeamPosYMiniValue.Name = "txtBeamPosYMiniValue";
            this.txtBeamPosYMiniValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosYMiniValue.TabIndex = 57;
            // 
            // txtBeamPosYCurrenValue
            // 
            this.txtBeamPosYCurrenValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosYCurrenValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosYCurrenValue.Location = new System.Drawing.Point(100, 197);
            this.txtBeamPosYCurrenValue.Name = "txtBeamPosYCurrenValue";
            this.txtBeamPosYCurrenValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosYCurrenValue.TabIndex = 58;
            // 
            // txtBeamPowerMaxValue
            // 
            this.txtBeamPowerMaxValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPowerMaxValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPowerMaxValue.Location = new System.Drawing.Point(278, 63);
            this.txtBeamPowerMaxValue.Name = "txtBeamPowerMaxValue";
            this.txtBeamPowerMaxValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPowerMaxValue.TabIndex = 61;
            // 
            // txtBeamPowerMedianValue
            // 
            this.txtBeamPowerMedianValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPowerMedianValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPowerMedianValue.Location = new System.Drawing.Point(367, 63);
            this.txtBeamPowerMedianValue.Name = "txtBeamPowerMedianValue";
            this.txtBeamPowerMedianValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPowerMedianValue.TabIndex = 62;
            // 
            // txtBeamPosXMaxValue
            // 
            this.txtBeamPosXMaxValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosXMaxValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosXMaxValue.Location = new System.Drawing.Point(278, 130);
            this.txtBeamPosXMaxValue.Name = "txtBeamPosXMaxValue";
            this.txtBeamPosXMaxValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosXMaxValue.TabIndex = 63;
            // 
            // txtBeamPosXMedianValue
            // 
            this.txtBeamPosXMedianValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosXMedianValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosXMedianValue.Location = new System.Drawing.Point(367, 130);
            this.txtBeamPosXMedianValue.Name = "txtBeamPosXMedianValue";
            this.txtBeamPosXMedianValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosXMedianValue.TabIndex = 64;
            // 
            // txtBeamPosYMaxValue
            // 
            this.txtBeamPosYMaxValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosYMaxValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosYMaxValue.Location = new System.Drawing.Point(278, 197);
            this.txtBeamPosYMaxValue.Name = "txtBeamPosYMaxValue";
            this.txtBeamPosYMaxValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosYMaxValue.TabIndex = 65;
            // 
            // txtBeamPosYMedianValue
            // 
            this.txtBeamPosYMedianValue.BackColor = System.Drawing.Color.White;
            this.txtBeamPosYMedianValue.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBeamPosYMedianValue.Location = new System.Drawing.Point(367, 197);
            this.txtBeamPosYMedianValue.Name = "txtBeamPosYMedianValue";
            this.txtBeamPosYMedianValue.Size = new System.Drawing.Size(83, 61);
            this.txtBeamPosYMedianValue.TabIndex = 66;
            // 
            // btnBeamSettingStop
            // 
            this.btnBeamSettingStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeamSettingStop.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBeamSettingStop.ForeColor = System.Drawing.Color.Black;
            this.btnBeamSettingStop.Location = new System.Drawing.Point(367, 264);
            this.btnBeamSettingStop.Name = "btnBeamSettingStop";
            this.btnBeamSettingStop.Size = new System.Drawing.Size(83, 40);
            this.btnBeamSettingStop.TabIndex = 461;
            this.btnBeamSettingStop.Text = "Stop";
            this.btnBeamSettingStop.UseVisualStyleBackColor = false;
            this.btnBeamSettingStop.Click += new System.EventHandler(this.btnBeamPositioner_Click);
            // 
            // btnBeamSettingStart
            // 
            this.btnBeamSettingStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeamSettingStart.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBeamSettingStart.ForeColor = System.Drawing.Color.Black;
            this.btnBeamSettingStart.Location = new System.Drawing.Point(278, 264);
            this.btnBeamSettingStart.Name = "btnBeamSettingStart";
            this.btnBeamSettingStart.Size = new System.Drawing.Size(83, 40);
            this.btnBeamSettingStart.TabIndex = 462;
            this.btnBeamSettingStart.Text = "Start";
            this.btnBeamSettingStart.UseVisualStyleBackColor = false;
            this.btnBeamSettingStart.Click += new System.EventHandler(this.btnBeamPositioner_Click);
            // 
            // btnBeamOpen
            // 
            this.btnBeamOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeamOpen.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBeamOpen.ForeColor = System.Drawing.Color.Black;
            this.btnBeamOpen.Location = new System.Drawing.Point(3, 33);
            this.btnBeamOpen.Name = "btnBeamOpen";
            this.btnBeamOpen.Size = new System.Drawing.Size(93, 40);
            this.btnBeamOpen.TabIndex = 458;
            this.btnBeamOpen.Text = "Open";
            this.btnBeamOpen.UseVisualStyleBackColor = false;
            this.btnBeamOpen.Click += new System.EventHandler(this.btnBeamPositioner_Click);
            // 
            // btnBeamClose
            // 
            this.btnBeamClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeamClose.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBeamClose.ForeColor = System.Drawing.Color.Black;
            this.btnBeamClose.Location = new System.Drawing.Point(102, 33);
            this.btnBeamClose.Name = "btnBeamClose";
            this.btnBeamClose.Size = new System.Drawing.Size(93, 40);
            this.btnBeamClose.TabIndex = 459;
            this.btnBeamClose.Text = "Close";
            this.btnBeamClose.UseVisualStyleBackColor = false;
            this.btnBeamClose.Click += new System.EventHandler(this.btnBeamPositioner_Click);
            // 
            // panel31
            // 
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Controls.Add(this.btnLimitValueSave);
            this.panel31.Controls.Add(this.txtErrorPos);
            this.panel31.Controls.Add(this.txtTeachingY);
            this.panel31.Controls.Add(this.txtErrorPower);
            this.panel31.Controls.Add(this.txtTeachingX);
            this.panel31.Controls.Add(this.txtTeachingPower);
            this.panel31.Controls.Add(this.label37);
            this.panel31.Controls.Add(this.label38);
            this.panel31.Controls.Add(this.label27);
            this.panel31.Controls.Add(this.label28);
            this.panel31.Controls.Add(this.label29);
            this.panel31.Controls.Add(this.lblLimitValueSetting);
            this.panel31.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel31.Location = new System.Drawing.Point(462, 79);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(455, 309);
            this.panel31.TabIndex = 457;
            // 
            // lblLimitValueSetting
            // 
            this.lblLimitValueSetting.AutoEllipsis = true;
            this.lblLimitValueSetting.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLimitValueSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLimitValueSetting.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.lblLimitValueSetting.ForeColor = System.Drawing.Color.Black;
            this.lblLimitValueSetting.Location = new System.Drawing.Point(0, 0);
            this.lblLimitValueSetting.Name = "lblLimitValueSetting";
            this.lblLimitValueSetting.Size = new System.Drawing.Size(453, 28);
            this.lblLimitValueSetting.TabIndex = 9;
            this.lblLimitValueSetting.Text = "■ Limit Value Setting";
            this.lblLimitValueSetting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.SystemColors.Control;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(3, 63);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(110, 61);
            this.label29.TabIndex = 47;
            this.label29.Text = "티칭 Power :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.SystemColors.Control;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(3, 130);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(110, 61);
            this.label28.TabIndex = 48;
            this.label28.Text = "티칭 X :";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.SystemColors.Control;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(3, 197);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 61);
            this.label27.TabIndex = 50;
            this.label27.Text = "에러 범위 POWER";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.SystemColors.Control;
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label38.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(231, 130);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(110, 61);
            this.label38.TabIndex = 52;
            this.label38.Text = "티칭 Y :";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.SystemColors.Control;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(231, 197);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(110, 61);
            this.label37.TabIndex = 54;
            this.label37.Text = "에러 범위 POS :";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTeachingPower
            // 
            this.txtTeachingPower.BackColor = System.Drawing.Color.White;
            this.txtTeachingPower.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTeachingPower.Location = new System.Drawing.Point(119, 63);
            this.txtTeachingPower.Name = "txtTeachingPower";
            this.txtTeachingPower.Size = new System.Drawing.Size(103, 61);
            this.txtTeachingPower.TabIndex = 57;
            // 
            // txtTeachingX
            // 
            this.txtTeachingX.BackColor = System.Drawing.Color.White;
            this.txtTeachingX.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTeachingX.Location = new System.Drawing.Point(119, 130);
            this.txtTeachingX.Name = "txtTeachingX";
            this.txtTeachingX.Size = new System.Drawing.Size(103, 61);
            this.txtTeachingX.TabIndex = 58;
            // 
            // txtErrorPower
            // 
            this.txtErrorPower.BackColor = System.Drawing.Color.White;
            this.txtErrorPower.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtErrorPower.Location = new System.Drawing.Point(119, 197);
            this.txtErrorPower.Name = "txtErrorPower";
            this.txtErrorPower.Size = new System.Drawing.Size(103, 61);
            this.txtErrorPower.TabIndex = 59;
            // 
            // txtTeachingY
            // 
            this.txtTeachingY.BackColor = System.Drawing.Color.White;
            this.txtTeachingY.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTeachingY.Location = new System.Drawing.Point(347, 130);
            this.txtTeachingY.Name = "txtTeachingY";
            this.txtTeachingY.Size = new System.Drawing.Size(103, 61);
            this.txtTeachingY.TabIndex = 60;
            // 
            // txtErrorPos
            // 
            this.txtErrorPos.BackColor = System.Drawing.Color.White;
            this.txtErrorPos.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtErrorPos.Location = new System.Drawing.Point(347, 197);
            this.txtErrorPos.Name = "txtErrorPos";
            this.txtErrorPos.Size = new System.Drawing.Size(103, 61);
            this.txtErrorPos.TabIndex = 61;
            // 
            // btnLimitValueSave
            // 
            this.btnLimitValueSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnLimitValueSave.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLimitValueSave.ForeColor = System.Drawing.Color.Black;
            this.btnLimitValueSave.Location = new System.Drawing.Point(347, 264);
            this.btnLimitValueSave.Name = "btnLimitValueSave";
            this.btnLimitValueSave.Size = new System.Drawing.Size(103, 40);
            this.btnLimitValueSave.TabIndex = 462;
            this.btnLimitValueSave.Text = "Save";
            this.btnLimitValueSave.UseVisualStyleBackColor = false;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.panel31);
            this.panel30.Controls.Add(this.btnBeamClose);
            this.panel30.Controls.Add(this.btnBeamOpen);
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Controls.Add(this.lblBeam);
            this.panel30.Location = new System.Drawing.Point(4, 6);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(923, 393);
            this.panel30.TabIndex = 460;
            // 
            // UcrlMeasureWindows
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.tciostatus_info);
            this.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlMeasureWindows";
            this.Size = new System.Drawing.Size(1880, 875);
            this.tciostatus_info.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tciostatus_info;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnControllerSave;
        private System.Windows.Forms.Panel panel5;
        internal System.Windows.Forms.Label lblBrightSetting;
        private System.Windows.Forms.Button btnBrightControllerClose;
        private System.Windows.Forms.Button btnBrightControllerOpen;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lbl2ChBrightValue;
        private System.Windows.Forms.Label lbl1ChBrightValue;
        private System.Windows.Forms.TextBox txt2ChBrightValue;
        private System.Windows.Forms.TextBox txt1ChBrightValue;
        internal System.Windows.Forms.Label lblCurrentBright;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn2ChCurrentBrightSet;
        private System.Windows.Forms.Button btn1ChCurrentBrightSet;
        private System.Windows.Forms.Label lbl2ChCurrentBrightSet;
        private System.Windows.Forms.Label lbl1ChCurrentBrightSet;
        private System.Windows.Forms.TextBox txt2ChCurrentBrightSet;
        private System.Windows.Forms.TextBox txt1ChCurrentBrightSet;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnTiltLoaderClose;
        private System.Windows.Forms.Button btnTiltLoaderOpen;
        private System.Windows.Forms.Panel panel8;
        internal System.Windows.Forms.Label lblModuleLoader;
        internal System.Windows.Forms.Label lblTiltSensor;
        private System.Windows.Forms.Button btnTiltUnloaderClose;
        private System.Windows.Forms.Button btnTiltUnloaderOpen;
        private System.Windows.Forms.TextBox txtLoaderCheck4;
        private System.Windows.Forms.TextBox txtLoaderCheck3;
        private System.Windows.Forms.TextBox txtLoaderCheck2;
        private System.Windows.Forms.TextBox txtLoaderCheck1;
        private System.Windows.Forms.Label lblLoaderPosition4;
        private System.Windows.Forms.Label lblLoaderPosition3;
        private System.Windows.Forms.TextBox txtLoaderPosition4;
        private System.Windows.Forms.TextBox txtLoaderPosition3;
        private System.Windows.Forms.Label lblLoaderPosition2;
        private System.Windows.Forms.Label lblLoaderPosition1;
        private System.Windows.Forms.TextBox txtLoaderPosition2;
        private System.Windows.Forms.TextBox txtLoaderPosition1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtUnloaderCheck4;
        private System.Windows.Forms.TextBox txtUnloaderCheck3;
        private System.Windows.Forms.TextBox txtUnloaderCheck2;
        private System.Windows.Forms.TextBox txtUnloaderCheck1;
        private System.Windows.Forms.Label lblUnloaderPosition4;
        private System.Windows.Forms.Label lblUnloaderPosition3;
        private System.Windows.Forms.TextBox txtUnloaderPosition4;
        private System.Windows.Forms.TextBox txtUnloaderPosition3;
        private System.Windows.Forms.Label lblUnloaderPosition2;
        private System.Windows.Forms.Label lblUnloaderPosition1;
        private System.Windows.Forms.TextBox txtUnloaderPosition2;
        private System.Windows.Forms.TextBox txtUnloaderPosition1;
        internal System.Windows.Forms.Label lblModuleUnloader;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnMcrAClose;
        private System.Windows.Forms.Button btnMcrAOpen;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Button btnMcrAGet;
        private System.Windows.Forms.TextBox txtMcrA;
        internal System.Windows.Forms.Label lblMcrA;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnMcrBClose;
        private System.Windows.Forms.Button btnMcrBOpen;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnMcrBGet;
        private System.Windows.Forms.TextBox txtMcrB;
        internal System.Windows.Forms.Label lblMcrB;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnLoaderBarcodeAClose;
        private System.Windows.Forms.Button btnLoaderBarcodeAOpen;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnLoaderBarcodeAGet;
        private System.Windows.Forms.TextBox txtLoaderBarcodeA;
        internal System.Windows.Forms.Label lblLoaderFixBarcodeReaderA;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnLoaderHandyBarcodeClose;
        private System.Windows.Forms.Button btnLoaderHandyBarcodeOpen;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnLoaderHandyBarcodeGet;
        private System.Windows.Forms.TextBox txtLoaderHandyBarcode;
        internal System.Windows.Forms.Label lblLoaderHandyBarcodeReader;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnUnloaderBarcodeAClose;
        private System.Windows.Forms.Button btnUnloaderBarcodeAOpen;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnUnloaderBarcodeAGet;
        internal System.Windows.Forms.Label lblUnloaderFixBarcodeReaderA;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btnUnloaderHandyBarcodeClose;
        private System.Windows.Forms.Button btnUnloaderHandyBarcodeOpen;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnUnloaderHandyBarcodeGet;
        private System.Windows.Forms.TextBox txtUnloaderHandyBarcode;
        internal System.Windows.Forms.Label lblUnloaderHandyBarcodeReader;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnUnloaderBarcodeBClose;
        private System.Windows.Forms.Button btnUnloaderBarcodeBOpen;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnUnloaderBarcodeBGet;
        private System.Windows.Forms.TextBox txtUnloaderBarcodeB;
        internal System.Windows.Forms.Label lblUnloaderFixBarcodeReaderB;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button btnLoaderBarcodeBClose;
        private System.Windows.Forms.Button btnLoaderBarcodeBOpen;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnLoaderBarcodeBGet;
        private System.Windows.Forms.TextBox txtLoaderBarcodeB;
        internal System.Windows.Forms.Label lblLoaderFixBarcodeReaderB;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Button btnPowerMeterClose;
        private System.Windows.Forms.Button btnPowerMeterOpen;
        internal System.Windows.Forms.Label lblPowerMeter;
        private System.Windows.Forms.Panel panel26;
        internal System.Windows.Forms.Label lblLdsProcess;
        private System.Windows.Forms.Button btnPowerMeterSave;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TextBox txtMeasureUpTime;
        private System.Windows.Forms.Button btnMeasureSettingMeasureStop;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtRemainMeasureUpTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWarmUpTime;
        private System.Windows.Forms.Button btnMeasureSettingMeasure;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtRemainWarmUpTime;
        internal System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.TextBox txtPowerStatusAvgPower;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtPowerStatusLowPower;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPowerStatusMaxPower;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPowerStatusCureentPower;
        internal System.Windows.Forms.Label lblPowerStatus;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Button btnAutoCalibrationAutoCalStop;
        private System.Windows.Forms.Button btnAutoCalibrationAutoCal;
        private System.Windows.Forms.TextBox txtAutoCalibrationTargetPower;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtAutoCalibrationCurrentPower;
        internal System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnLdsPorcessSave;
        private System.Windows.Forms.Label txtLdsProcessState;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtLdsProcessTableIdx;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtLdsProcessMeasureTime;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtLdsProcessReadyTime;
        private System.Windows.Forms.Button btnLdsProcessStop;
        private System.Windows.Forms.Button btnLdsProcessStart;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.ListView lviLdsProcess;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtLdsProcessYInterval;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLdsProcessValue9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtLdsProcessValue6;
        private System.Windows.Forms.TextBox txtLdsProcessValue3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtLdsProcessValue8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLdsProcessValue5;
        private System.Windows.Forms.TextBox txtLdsProcessValue2;
        private System.Windows.Forms.TextBox txtLdsProcessCurrentValue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtLdsProcessXInterval;
        private System.Windows.Forms.TextBox txtLdsProcessValue7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtLdsProcessValue4;
        private System.Windows.Forms.TextBox txtLdsProcessValue1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button btnLdsBreakSave;
        private System.Windows.Forms.Label txtLdsBreakState;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtLdsBreakTableIdx;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtLdsBreakMeasureTime;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtLdsBreakReadyTime;
        private System.Windows.Forms.Button btnLdsBreakStop;
        private System.Windows.Forms.Button btnLdsBreakStart;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.ListView lviLdsBreak;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtLdsBreakYInterval;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtLdsBreakValue9;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtLdsBreakValue6;
        private System.Windows.Forms.TextBox txtLdsBreakValue3;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtLdsBreakValue8;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtLdsBreakValue5;
        private System.Windows.Forms.TextBox txtLdsBreakValue2;
        private System.Windows.Forms.TextBox txtLdsBreakCurrentValue;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtLdsBreakXInterval;
        private System.Windows.Forms.TextBox txtLdsBreakValue7;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtLdsBreakValue4;
        private System.Windows.Forms.TextBox txtLdsBreakValue1;
        private System.Windows.Forms.Label label55;
        internal System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Button btnCutLineColBR2Table;
        private System.Windows.Forms.Button btnCutLineColBR1Table;
        private System.Windows.Forms.Button btnCutLineColAL2Table;
        private System.Windows.Forms.Button btnCutLineColAL1Table;
        internal System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtUnloaderBarcodeA;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Button btnInspCmd;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboInspCmd;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.ComboBox cboAlignCmd;
        private System.Windows.Forms.Button btnAlignCmd;
        internal System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button btnInspConnect;
        private System.Windows.Forms.Button btnAlignConnect;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Button btnLimitValueSave;
        private System.Windows.Forms.TextBox txtErrorPos;
        private System.Windows.Forms.TextBox txtTeachingY;
        private System.Windows.Forms.TextBox txtErrorPower;
        private System.Windows.Forms.TextBox txtTeachingX;
        private System.Windows.Forms.TextBox txtTeachingPower;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label lblLimitValueSetting;
        private System.Windows.Forms.Button btnBeamClose;
        private System.Windows.Forms.Button btnBeamOpen;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Button btnBeamSettingStart;
        private System.Windows.Forms.Button btnBeamSettingStop;
        private System.Windows.Forms.TextBox txtBeamPosYMedianValue;
        private System.Windows.Forms.TextBox txtBeamPosYMaxValue;
        private System.Windows.Forms.TextBox txtBeamPosXMedianValue;
        private System.Windows.Forms.TextBox txtBeamPosXMaxValue;
        private System.Windows.Forms.TextBox txtBeamPowerMedianValue;
        private System.Windows.Forms.TextBox txtBeamPowerMaxValue;
        private System.Windows.Forms.TextBox txtBeamPosYCurrenValue;
        private System.Windows.Forms.TextBox txtBeamPosYMiniValue;
        private System.Windows.Forms.TextBox txtBeamPowerCurrenValue;
        private System.Windows.Forms.TextBox txtBeamPowerMiniValue;
        private System.Windows.Forms.TextBox txtBeamPosXCurrenValue;
        private System.Windows.Forms.TextBox txtBeamPosXMiniValue;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        internal System.Windows.Forms.Label lblBeamPowerSetting;
        internal System.Windows.Forms.Label lblBeam;
    }
}

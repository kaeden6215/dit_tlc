﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;
using System.Timers;

namespace DIT.TLC.UI
{
    public partial class UcrlMeasureWindows : UserControl, IUIUpdate
    {
        public UcrlMeasureWindows()
        {
            InitializeComponent();
        }

        private void btnBrightController_Click(object sender, EventArgs e)
        {
            if (btnBrightControllerOpen == (Button)sender)
            {
                GG.Equip.LightProxy.OpenPort();
            }
            else if (btnBrightControllerClose == (Button)sender)
            {
                GG.Equip.LightProxy.ClosePort();
            }
            else if (btn1ChCurrentBrightSet == (Button)sender)
            {
                GG.Equip.LightProxy.SetBrightness(1, int.Parse(txt1ChCurrentBrightSet.Text));
            }
            else if (btn2ChCurrentBrightSet == (Button)sender)
            {
                GG.Equip.LightProxy.SetBrightness(2, int.Parse(txt2ChCurrentBrightSet.Text));
            }
        }

        private void btnTiltSensor_Click(object sender, EventArgs e)
        {
            // 로더
            if (btnTiltLoaderOpen == (Button)sender)
            {
                bool b = GG.Equip.LdTiltProxy.OpenPort();
            }
            else if (btnTiltLoaderClose == (Button)sender)
            {
                GG.Equip.LdTiltProxy.ClosePort();
            }
            // 언로더
            else if (btnTiltUnloaderOpen == (Button)sender)
            {
                GG.Equip.UldTiltProxy.OpenPort();
            }
            else if (btnTiltUnloaderClose == (Button)sender)
            {
                GG.Equip.UldTiltProxy.ClosePort();
            }
        }

        private void btnMcr_Click(object sender, EventArgs e)
        {
            // MCR A
            if (btnMcrAOpen == (Button)sender)
            {
                GG.Equip.Mcr1stProxy.OpenPort();
                GG.Equip.Mcr1stProxy.SendCmd();
            }
            else if (btnMcrAClose == (Button)sender)
            {
                GG.Equip.Mcr1stProxy.ClosePort();
            }
            else if (btnMcrAGet == (Button)sender)
            {
                GG.Equip.Mcr1stProxy.SendCmd();
                //if(GG.Equip.McrAProxy.IsReadComplete)
                {
                    txtMcrA.Text = GG.Equip.Mcr1stProxy.ReadData;
                }
            }
            // MCR B
            else if (btnMcrBOpen == (Button)sender)
            {
                GG.Equip.Mcr2ndProxy.OpenPort();
            }
            else if (btnMcrBClose == (Button)sender)
            {
                GG.Equip.Mcr2ndProxy.ClosePort();
            }
            else if (btnMcrBGet == (Button)sender)
            {
                GG.Equip.Mcr2ndProxy.SendCmd();
                //if (GG.Equip.McrBProxy.IsReadComplete)
                {
                    txtMcrB.Text = GG.Equip.Mcr2ndProxy.ReadData;
                }
            }
        }

        private void btnBarcode_Click(object sender, EventArgs e)
        {
            // 고정형 로더
            if (btnLoaderBarcodeAOpen == (Button)sender)
            {
                GG.Equip.LdAFixedBarcProxy.OpenPort();
            }
            else if (btnLoaderBarcodeAClose == (Button)sender)
            {
                GG.Equip.LdAFixedBarcProxy.ClosePort();
            }
            else if (btnLoaderBarcodeAGet == (Button)sender)
            {
                GG.Equip.LdAFixedBarcProxy.SendCmd();
            }
            else if (btnLoaderBarcodeBOpen == (Button)sender)
            {
                GG.Equip.LdBFixedBarcProxy.OpenPort();
            }
            else if (btnLoaderBarcodeBClose == (Button)sender)
            {
                GG.Equip.LdBFixedBarcProxy.ClosePort();
            }
            else if (btnLoaderBarcodeBGet == (Button)sender)
            {
                GG.Equip.LdBFixedBarcProxy.SendCmd();
            }
            // 고정형 언로더
            else if (btnUnloaderBarcodeAOpen == (Button)sender)
            {
                GG.Equip.UldAFixedBarcProxy.OpenPort();
            }
            else if (btnUnloaderBarcodeAClose == (Button)sender)
            {
                GG.Equip.UldAFixedBarcProxy.ClosePort();
            }
            else if (btnUnloaderBarcodeAGet == (Button)sender)
            {
                GG.Equip.UldAFixedBarcProxy.SendCmd();
            }
            else if (btnUnloaderBarcodeBOpen == (Button)sender)
            {
                GG.Equip.UldBFixedBarcProxy.OpenPort();
            }
            else if (btnUnloaderBarcodeBClose == (Button)sender)
            {
                GG.Equip.UldBFixedBarcProxy.ClosePort();
            }
            else if (btnUnloaderBarcodeBGet == (Button)sender)
            {
                GG.Equip.UldBFixedBarcProxy.SendCmd();
            }
            // 핸디형 로더
            else if (btnLoaderHandyBarcodeOpen == (Button)sender)
            {
                GG.Equip.LdHandyBarcProxy.OpenPort();
            }
            else if (btnLoaderHandyBarcodeClose == (Button)sender)
            {
                GG.Equip.LdHandyBarcProxy.ClosePort();
            }
            else if (btnLoaderHandyBarcodeGet == (Button)sender)
            {
                GG.Equip.LdHandyBarcProxy.SendCmd();
            }
            // 핸디형 언로더
            else if (btnUnloaderHandyBarcodeOpen == (Button)sender)
            {
                GG.Equip.UldHandyBarcProxy.OpenPort();
            }
            else if (btnUnloaderHandyBarcodeClose == (Button)sender)
            {
                GG.Equip.UldHandyBarcProxy.ClosePort();
            }
            else if (btnUnloaderHandyBarcodeGet == (Button)sender)
            {
                GG.Equip.UldHandyBarcProxy.SendCmd();
            }
        }

        private void btnBeamPositioner_Click(object sender, EventArgs e)
        {
            if (btnBeamOpen == (Button)sender)
            {
                GG.Equip.BeamProxy.OpenPort();
            }
            else if (btnBeamClose == (Button)sender)
            {
                GG.Equip.BeamProxy.SendEndCmd();
                GG.Equip.BeamProxy.ClosePort();
            }
            else if (btnBeamSettingStart == (Button)sender)
            {
                GG.Equip.BeamProxy.SendStartCmd();
            }
            else if (btnBeamSettingStop == (Button)sender)
            {
                GG.Equip.BeamProxy.SendEndCmd();
            }
        }

        private void btnPowerMeter_Click(object sender, EventArgs e)
        {
            if (btnPowerMeterOpen == (Button)sender)
            {
                GG.Equip.PowerProxy.OpenPort();
            }
            else if (btnPowerMeterClose == (Button)sender)
            {
                GG.Equip.PowerProxy.ClosePort();
            }
            else if (btnMeasureSettingMeasure == (Button)sender)
            {
                GG.Equip.PowerProxy.SendCmd();
            }
        }

        private void btnLds_Click(object sender, EventArgs e)
        {

        }

        enum EmBP
        {
            Min,
            Max,
            Avg
        }

        double[] BeamPosiPower = new double[3] { 9999, 0, 0 };
        double[] BeamPosiX = new double[3] { 9999, 0, 0 };
        double[] BeamPosiY = new double[3] { 9999, 0, 0 };
        public void UIUpdate()
        {
            if (GG.Equip.Mcr1stProxy.IsReadComplete && GG.Equip.Mcr1stProxy.IsReadSuccess) txtMcrA.Text = string.Format("{0}", GG.Equip.Mcr1stProxy.ReadData);
            if (GG.Equip.Mcr2ndProxy.IsReadComplete && GG.Equip.Mcr2ndProxy.IsReadSuccess) txtMcrB.Text = string.Format("{0}", GG.Equip.Mcr2ndProxy.ReadData);

            if (GG.Equip.LdTiltProxy.IsReadComplete && GG.Equip.LdTiltProxy.IsReadSuccess)
            {
                txtLoaderPosition1.Text = string.Format("{0}", GG.Equip.LdTiltProxy.GetReadData[0]);
                txtLoaderPosition2.Text = string.Format("{0}", GG.Equip.LdTiltProxy.GetReadData[1]);
                txtLoaderPosition3.Text = string.Format("{0}", GG.Equip.LdTiltProxy.GetReadData[2]);
                txtLoaderPosition4.Text = string.Format("{0}", GG.Equip.LdTiltProxy.GetReadData[3]);

                txtLoaderCheck1.BackColor = GG.Equip.LdTiltProxy.GetReadJudge[0] ? Color.Lime : Color.Red;
                txtLoaderCheck2.BackColor = GG.Equip.LdTiltProxy.GetReadJudge[1] ? Color.Lime : Color.Red;
                txtLoaderCheck3.BackColor = GG.Equip.LdTiltProxy.GetReadJudge[2] ? Color.Lime : Color.Red;
                txtLoaderCheck4.BackColor = GG.Equip.LdTiltProxy.GetReadJudge[3] ? Color.Lime : Color.Red;

                txtLoaderCheck1.Text = GG.Equip.LdTiltProxy.GetReadJudge[0] ? "OK" : "NG";
                txtLoaderCheck2.Text = GG.Equip.LdTiltProxy.GetReadJudge[1] ? "OK" : "NG";
                txtLoaderCheck3.Text = GG.Equip.LdTiltProxy.GetReadJudge[2] ? "OK" : "NG";
                txtLoaderCheck4.Text = GG.Equip.LdTiltProxy.GetReadJudge[3] ? "OK" : "NG";
            }
            if (GG.Equip.UldTiltProxy.IsReadComplete && GG.Equip.UldTiltProxy.IsReadSuccess)
            {
                txtUnloaderPosition1.Text = string.Format("{0}", GG.Equip.UldTiltProxy.GetReadData[0]);
                txtUnloaderPosition2.Text = string.Format("{0}", GG.Equip.UldTiltProxy.GetReadData[1]);
                txtUnloaderPosition3.Text = string.Format("{0}", GG.Equip.UldTiltProxy.GetReadData[2]);
                txtUnloaderPosition4.Text = string.Format("{0}", GG.Equip.UldTiltProxy.GetReadData[3]);

                txtUnloaderCheck1.BackColor = GG.Equip.UldTiltProxy.GetReadJudge[0] ? Color.Lime : Color.Red;
                txtUnloaderCheck2.BackColor = GG.Equip.UldTiltProxy.GetReadJudge[1] ? Color.Lime : Color.Red;
                txtUnloaderCheck3.BackColor = GG.Equip.UldTiltProxy.GetReadJudge[2] ? Color.Lime : Color.Red;
                txtUnloaderCheck4.BackColor = GG.Equip.UldTiltProxy.GetReadJudge[3] ? Color.Lime : Color.Red;

                txtUnloaderCheck1.Text = GG.Equip.UldTiltProxy.GetReadJudge[0] ? "OK" : "NG";
                txtUnloaderCheck2.Text = GG.Equip.UldTiltProxy.GetReadJudge[1] ? "OK" : "NG";
                txtUnloaderCheck3.Text = GG.Equip.UldTiltProxy.GetReadJudge[2] ? "OK" : "NG";
                txtUnloaderCheck4.Text = GG.Equip.UldTiltProxy.GetReadJudge[3] ? "OK" : "NG";
            }

            if (GG.Equip.LdAFixedBarcProxy.IsReadComplete && GG.Equip.LdAFixedBarcProxy.IsReadSuccess) txtLoaderBarcodeA.Text = GG.Equip.LdAFixedBarcProxy.ReadData;
            if (GG.Equip.LdBFixedBarcProxy.IsReadComplete && GG.Equip.LdBFixedBarcProxy.IsReadSuccess) txtLoaderBarcodeB.Text = GG.Equip.LdBFixedBarcProxy.ReadData;
            if (GG.Equip.UldAFixedBarcProxy.IsReadComplete && GG.Equip.UldAFixedBarcProxy.IsReadSuccess) txtUnloaderBarcodeA.Text = GG.Equip.UldAFixedBarcProxy.ReadData;
            if (GG.Equip.UldBFixedBarcProxy.IsReadComplete && GG.Equip.UldBFixedBarcProxy.IsReadSuccess) txtUnloaderBarcodeB.Text = GG.Equip.UldBFixedBarcProxy.ReadData;

            if (GG.Equip.LdHandyBarcProxy.IsReadComplete && GG.Equip.LdHandyBarcProxy.IsReadSuccess) txtLoaderHandyBarcode.Text = GG.Equip.LdHandyBarcProxy.ReadData;
            if (GG.Equip.UldHandyBarcProxy.IsReadComplete && GG.Equip.UldHandyBarcProxy.IsReadSuccess) txtUnloaderHandyBarcode.Text = GG.Equip.UldHandyBarcProxy.ReadData;

            if (GG.Equip.BeamProxy.IsReadComplete && GG.Equip.BeamProxy.IsReadSuccess)
            {
                if (BeamPosiPower[(int)EmBP.Min] > GG.Equip.BeamProxy.ReadPower)    BeamPosiPower[(int)EmBP.Min] = GG.Equip.BeamProxy.ReadPower;
                if (BeamPosiPower[(int)EmBP.Max] < GG.Equip.BeamProxy.ReadPower)    BeamPosiPower[(int)EmBP.Max] = GG.Equip.BeamProxy.ReadPower;
                BeamPosiPower[(int)EmBP.Avg] = (BeamPosiPower[(int)EmBP.Avg] + GG.Equip.BeamProxy.ReadPower) / 2;
                if (BeamPosiPower[(int)EmBP.Avg] < 0.001) BeamPosiPower[(int)EmBP.Avg] = 0;

                if (BeamPosiX[(int)EmBP.Min] > GG.Equip.BeamProxy.ReadPosX) BeamPosiX[(int)EmBP.Min] = GG.Equip.BeamProxy.ReadPosX;
                if (BeamPosiX[(int)EmBP.Max] < GG.Equip.BeamProxy.ReadPosX) BeamPosiX[(int)EmBP.Max] = GG.Equip.BeamProxy.ReadPosX;
                BeamPosiX[(int)EmBP.Avg] = (BeamPosiX[(int)EmBP.Avg] + GG.Equip.BeamProxy.ReadPosX) / 2;
                if (BeamPosiX[(int)EmBP.Avg] < 0.001) BeamPosiX[(int)EmBP.Avg] = 0;

                if (BeamPosiY[(int)EmBP.Min] > GG.Equip.BeamProxy.ReadPosY) BeamPosiY[(int)EmBP.Min] = GG.Equip.BeamProxy.ReadPosY;
                if (BeamPosiY[(int)EmBP.Max] < GG.Equip.BeamProxy.ReadPosY) BeamPosiY[(int)EmBP.Max] = GG.Equip.BeamProxy.ReadPosY;
                BeamPosiY[(int)EmBP.Avg] = (BeamPosiY[(int)EmBP.Avg] + GG.Equip.BeamProxy.ReadPosY) / 2;
                if (BeamPosiY[(int)EmBP.Avg] < 0.001) BeamPosiY[(int)EmBP.Avg] = 0;

                txtBeamPowerCurrenValue.Text = string.Format("{0}", GG.Equip.BeamProxy.ReadPower);
                txtBeamPosXCurrenValue.Text = string.Format("{0}", GG.Equip.BeamProxy.ReadPosX);
                txtBeamPosYCurrenValue.Text = string.Format("{0}", GG.Equip.BeamProxy.ReadPosY);

                txtBeamPowerMiniValue.Text = string.Format("{0}", BeamPosiPower[(int)EmBP.Min]);
                txtBeamPosXMiniValue.Text = string.Format("{0}", BeamPosiX[(int)EmBP.Min]);
                txtBeamPosYMiniValue.Text = string.Format("{0}", BeamPosiY[(int)EmBP.Min]);

                txtBeamPowerMaxValue.Text = string.Format("{0}", BeamPosiPower[(int)EmBP.Max]);
                txtBeamPosXMaxValue.Text = string.Format("{0}", BeamPosiX[(int)EmBP.Max]);
                txtBeamPosYMaxValue.Text = string.Format("{0}", BeamPosiY[(int)EmBP.Max]);

                txtBeamPowerMedianValue.Text = string.Format("{0}", BeamPosiPower[(int)EmBP.Avg]);
                txtBeamPosXMedianValue.Text = string.Format("{0}", BeamPosiX[(int)EmBP.Avg]);
                txtBeamPosYMedianValue.Text = string.Format("{0}", BeamPosiY[(int)EmBP.Avg]);
            }

            if (GG.Equip.PowerProxy.IsReadComplete && GG.Equip.PowerProxy.IsReadSuccess) txtPowerStatusCureentPower.Text = string.Format("{0}", GG.Equip.PowerProxy.ReadData);
        }

        private void btnAlignConnect_Click(object sender, EventArgs e)
        {
            GG.Equip.AlignPc.Start();
        }
        int _inxMsgID = 0;
        private void btnAlignCmd_Click(object sender, EventArgs e)
        {
            _inxMsgID++;
            if (cboAlignCmd.Text == "AlignStart")
            {
                GG.Equip.AlignPc.SendAlignStart(_inxMsgID++, "PPID", "CELLID", EmAlignType.FineAlign, EmCameraIndex.Cam1st, EmAlignIndex.Grap1St, 11, 22, 33);
            }
            else if (cboAlignCmd.Text == "AlignBkSet")
            {
                GG.Equip.AlignPc.SendAlignBkSet(_inxMsgID++, EmAlignIndex.Grap1St, 11, 22);
            }
            else if (cboAlignCmd.Text == "MeasureGrabStart")
            {
                GG.Equip.AlignPc.SendMasureGrabStart(_inxMsgID++, "PPID", "CELLID", EmAlignTableIndex.A1, 0, EmCameraIndex.Cam1st);
            }
            else if (cboAlignCmd.Text == "JigAlignStart")
            {
                GG.Equip.AlignPc.SendJigAlignStart(_inxMsgID++, EmCameraIndex.Cam1st, EmAlignIndex.Grap1St, 12, 13);
            }
        }
        private void btnInspConnect_Click(object sender, EventArgs e)
        {
            GG.Equip.InspPc.Start();

        }
        private void btnInspCmd_Click(object sender, EventArgs e)
        {
            _inxMsgID++;
            if (cboInspCmd.Text == "InspReady")
            {
                GG.Equip.InspPc.SendInspReady(_inxMsgID, "PPID", "LOTID", "CELLID1", "CELLID2", 1, 2, 3, 4, 5, 6,  1, DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            else if (cboInspCmd.Text == "GrabStart")
            {
                GG.Equip.InspPc.SendGrabStart(_inxMsgID, 1, 2);
            }
            else if (cboInspCmd.Text == "GrabEnd")
            {
                GG.Equip.InspPc.SendGrabEnd(_inxMsgID, 1, 2);
            }
            else if (cboInspCmd.Text == "InspReset")
            {
                GG.Equip.InspPc.SendInspReset(_inxMsgID);
            }
            else if (cboInspCmd.Text == "MeasureReady")
            {
                GG.Equip.InspPc.SendMeasureReady(_inxMsgID, "PPID", "cellID", 5, EmAlignTableIndex.A1);
            }
            else if (cboInspCmd.Text == "MeasureStart")
            {
                GG.Equip.InspPc.SendMeasureStart(_inxMsgID);
            }
        }

    }
}

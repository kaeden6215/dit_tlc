﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;
using Dit.Framework.Comm;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerMotorStatus : UserControl, IUIUpdate
    {

        private List<Button> _lstBtnMotorStatus1 = new List<Button>();
        private List<Button> _lstBtnMotorStatus2 = new List<Button>();
        private List<Button> _lstBtnMotorStatus3 = new List<Button>();

        private List<Label> _lstLblMotorStatus1 = new List<Label>();
        private List<Label> _lstLblMotorStatus2 = new List<Label>();
        private List<Label> _lstLblMotorStatus3 = new List<Label>();

        private ServoMotorControl _servo = null;
        public UcrlManagerMotorStatus()
        {
            InitializeComponent();
            #region  List 설정
            _lstBtnMotorStatus1.Add(btnMotorStatus101);
            _lstBtnMotorStatus1.Add(btnMotorStatus102);
            _lstBtnMotorStatus1.Add(btnMotorStatus103);
            _lstBtnMotorStatus1.Add(btnMotorStatus104);
            _lstBtnMotorStatus1.Add(btnMotorStatus105);
            _lstBtnMotorStatus1.Add(btnMotorStatus106);
            _lstBtnMotorStatus1.Add(btnMotorStatus107);
            _lstBtnMotorStatus1.Add(btnMotorStatus108);
            _lstBtnMotorStatus1.Add(btnMotorStatus109);
            _lstBtnMotorStatus1.Add(btnMotorStatus110);
            _lstBtnMotorStatus1.Add(btnMotorStatus111);
            _lstBtnMotorStatus1.Add(btnMotorStatus112);
            _lstBtnMotorStatus1.Add(btnMotorStatus113);
            _lstBtnMotorStatus1.Add(btnMotorStatus114);
            _lstBtnMotorStatus1.Add(btnMotorStatus115);
            _lstBtnMotorStatus1.Add(btnMotorStatus116);
            _lstBtnMotorStatus1.Add(btnMotorStatus117);
            _lstBtnMotorStatus1.Add(btnMotorStatus118);
            _lstBtnMotorStatus1.Add(btnMotorStatus119);
            _lstBtnMotorStatus1.Add(btnMotorStatus120);
            _lstBtnMotorStatus1.Add(btnMotorStatus121);
            _lstBtnMotorStatus1.Add(btnMotorStatus122);
            _lstBtnMotorStatus1.Add(btnMotorStatus123);
            _lstBtnMotorStatus1.Add(btnMotorStatus124);

            _lstBtnMotorStatus2.Add(btnMotorStatus201);
            _lstBtnMotorStatus2.Add(btnMotorStatus202);
            _lstBtnMotorStatus2.Add(btnMotorStatus203);
            _lstBtnMotorStatus2.Add(btnMotorStatus204);
            _lstBtnMotorStatus2.Add(btnMotorStatus205);
            _lstBtnMotorStatus2.Add(btnMotorStatus206);
            _lstBtnMotorStatus2.Add(btnMotorStatus207);
            _lstBtnMotorStatus2.Add(btnMotorStatus208);
            _lstBtnMotorStatus2.Add(btnMotorStatus209);
            _lstBtnMotorStatus2.Add(btnMotorStatus210);
            _lstBtnMotorStatus2.Add(btnMotorStatus211);
            _lstBtnMotorStatus2.Add(btnMotorStatus212);
            _lstBtnMotorStatus2.Add(btnMotorStatus213);
            _lstBtnMotorStatus2.Add(btnMotorStatus214);
            _lstBtnMotorStatus2.Add(btnMotorStatus215);
            _lstBtnMotorStatus2.Add(btnMotorStatus216);
            _lstBtnMotorStatus2.Add(btnMotorStatus217);
            _lstBtnMotorStatus2.Add(btnMotorStatus218);
            _lstBtnMotorStatus2.Add(btnMotorStatus219);
            _lstBtnMotorStatus2.Add(btnMotorStatus220);
            _lstBtnMotorStatus2.Add(btnMotorStatus221);
            _lstBtnMotorStatus2.Add(btnMotorStatus222);
            _lstBtnMotorStatus2.Add(btnMotorStatus223);
            _lstBtnMotorStatus2.Add(btnMotorStatus224);

            _lstBtnMotorStatus3.Add(btnMotorStatus301);
            _lstBtnMotorStatus3.Add(btnMotorStatus302);
            _lstBtnMotorStatus3.Add(btnMotorStatus303);
            _lstBtnMotorStatus3.Add(btnMotorStatus304);
            _lstBtnMotorStatus3.Add(btnMotorStatus305);
            _lstBtnMotorStatus3.Add(btnMotorStatus306);
            _lstBtnMotorStatus3.Add(btnMotorStatus307);
            _lstBtnMotorStatus3.Add(btnMotorStatus308);
            _lstBtnMotorStatus3.Add(btnMotorStatus309);
            _lstBtnMotorStatus3.Add(btnMotorStatus310);
            _lstBtnMotorStatus3.Add(btnMotorStatus311);
            _lstBtnMotorStatus3.Add(btnMotorStatus312);
            _lstBtnMotorStatus3.Add(btnMotorStatus313);
            _lstBtnMotorStatus3.Add(btnMotorStatus314);
            _lstBtnMotorStatus3.Add(btnMotorStatus315);
            _lstBtnMotorStatus3.Add(btnMotorStatus316);
            _lstBtnMotorStatus3.Add(btnMotorStatus317);
            _lstBtnMotorStatus3.Add(btnMotorStatus318);
            _lstBtnMotorStatus3.Add(btnMotorStatus319);
            _lstBtnMotorStatus3.Add(btnMotorStatus320);
            _lstBtnMotorStatus3.Add(btnMotorStatus321);
            _lstBtnMotorStatus3.Add(btnMotorStatus322);
            _lstBtnMotorStatus3.Add(btnMotorStatus323);
            _lstBtnMotorStatus3.Add(btnMotorStatus324);


            _lstLblMotorStatus1.Add(lblMotorStatus101);
            _lstLblMotorStatus1.Add(lblMotorStatus102);
            _lstLblMotorStatus1.Add(lblMotorStatus103);
            _lstLblMotorStatus1.Add(lblMotorStatus104);
            _lstLblMotorStatus1.Add(lblMotorStatus105);
            _lstLblMotorStatus1.Add(lblMotorStatus106);
            _lstLblMotorStatus1.Add(lblMotorStatus107);
            _lstLblMotorStatus1.Add(lblMotorStatus108);
            _lstLblMotorStatus1.Add(lblMotorStatus109);
            _lstLblMotorStatus1.Add(lblMotorStatus110);
            _lstLblMotorStatus1.Add(lblMotorStatus111);
            _lstLblMotorStatus1.Add(lblMotorStatus112);
            _lstLblMotorStatus1.Add(lblMotorStatus113);
            _lstLblMotorStatus1.Add(lblMotorStatus114);
            _lstLblMotorStatus1.Add(lblMotorStatus115);
            _lstLblMotorStatus1.Add(lblMotorStatus116);
            _lstLblMotorStatus1.Add(lblMotorStatus117);
            _lstLblMotorStatus1.Add(lblMotorStatus118);
            _lstLblMotorStatus1.Add(lblMotorStatus119);
            _lstLblMotorStatus1.Add(lblMotorStatus120);
            _lstLblMotorStatus1.Add(lblMotorStatus121);
            _lstLblMotorStatus1.Add(lblMotorStatus122);
            _lstLblMotorStatus1.Add(lblMotorStatus123);
            _lstLblMotorStatus1.Add(lblMotorStatus124);



            _lstLblMotorStatus2.Add(lblMotorStatus201);
            _lstLblMotorStatus2.Add(lblMotorStatus202);
            _lstLblMotorStatus2.Add(lblMotorStatus203);
            _lstLblMotorStatus2.Add(lblMotorStatus204);
            _lstLblMotorStatus2.Add(lblMotorStatus205);
            _lstLblMotorStatus2.Add(lblMotorStatus206);
            _lstLblMotorStatus2.Add(lblMotorStatus207);
            _lstLblMotorStatus2.Add(lblMotorStatus208);
            _lstLblMotorStatus2.Add(lblMotorStatus209);
            _lstLblMotorStatus2.Add(lblMotorStatus210);
            _lstLblMotorStatus2.Add(lblMotorStatus211);
            _lstLblMotorStatus2.Add(lblMotorStatus212);
            _lstLblMotorStatus2.Add(lblMotorStatus213);
            _lstLblMotorStatus2.Add(lblMotorStatus214);
            _lstLblMotorStatus2.Add(lblMotorStatus215);
            _lstLblMotorStatus2.Add(lblMotorStatus216);
            _lstLblMotorStatus2.Add(lblMotorStatus217);
            _lstLblMotorStatus2.Add(lblMotorStatus218);
            _lstLblMotorStatus2.Add(lblMotorStatus219);
            _lstLblMotorStatus2.Add(lblMotorStatus220);
            _lstLblMotorStatus2.Add(lblMotorStatus221);
            _lstLblMotorStatus2.Add(lblMotorStatus222);
            _lstLblMotorStatus2.Add(lblMotorStatus223);
            _lstLblMotorStatus2.Add(lblMotorStatus224);



            _lstLblMotorStatus3.Add(lblMotorStatus301);
            _lstLblMotorStatus3.Add(lblMotorStatus302);
            _lstLblMotorStatus3.Add(lblMotorStatus303);
            _lstLblMotorStatus3.Add(lblMotorStatus304);
            _lstLblMotorStatus3.Add(lblMotorStatus305);
            _lstLblMotorStatus3.Add(lblMotorStatus306);
            _lstLblMotorStatus3.Add(lblMotorStatus307);
            _lstLblMotorStatus3.Add(lblMotorStatus308);
            _lstLblMotorStatus3.Add(lblMotorStatus309);
            _lstLblMotorStatus3.Add(lblMotorStatus310);
            _lstLblMotorStatus3.Add(lblMotorStatus311);
            _lstLblMotorStatus3.Add(lblMotorStatus312);
            _lstLblMotorStatus3.Add(lblMotorStatus313);
            _lstLblMotorStatus3.Add(lblMotorStatus314);
            _lstLblMotorStatus3.Add(lblMotorStatus315);
            _lstLblMotorStatus3.Add(lblMotorStatus316);
            _lstLblMotorStatus3.Add(lblMotorStatus317);
            _lstLblMotorStatus3.Add(lblMotorStatus318);
            _lstLblMotorStatus3.Add(lblMotorStatus319);
            _lstLblMotorStatus3.Add(lblMotorStatus320);
            _lstLblMotorStatus3.Add(lblMotorStatus321);
            _lstLblMotorStatus3.Add(lblMotorStatus322);
            _lstLblMotorStatus3.Add(lblMotorStatus323);
            _lstLblMotorStatus3.Add(lblMotorStatus324);
            #endregion
        }

        public void FillServoList(FlowLayoutPanel pnl, List<ServoMotorControl> lst)
        {
            List<Button> lstBtn = new List<Button>();
            lstBtn.AddRange(pnl.Controls.OfType<Button>());
            pnl.Controls.Clear();

            lstBtn.ForEach(f => f.Dispose());

            foreach (ServoMotorControl servo in lst)
            {
                Button btn = new Button() { Width = 500, Height = 35, ForeColor = Color.Black, Left = 1, Font = new Font("맑은 고딕", 9, FontStyle.Regular), Text = string.Format("{0}.{1}", servo.OutterAxisNo, servo.Name) };
                btn.Tag = servo;
                btn.Click += SelectedServo_Click;
                pnl.Controls.Add(btn);
            }
        }

        private void SelectedServo_Click(object sender, EventArgs e)
        {
            foreach (var ctrl in flpSevoMotors.Controls)
            {
                Button motorBtn = ctrl as Button;
                motorBtn.BackColor = UiGlobal.UNSELECT_C;
            }

            Button btn = sender as Button;
            btn.BackColor = UiGlobal.SELECT_C;
            _servo = (ServoMotorControl)btn.Tag;

            UIUpdate();
        }
        public void UpdateLable()
        {
            if (btnAjinMotor.BackColor == UiGlobal.SELECT_C)
            {
                string[] lblMotorStatus01 = "Position End Limit,Negative End Limit,Positive Slow Stop Limit,Negative Slow Stop Limit,Alarm,Inposition,Emergency Stop,Home,Z Phase,Encoder Up,Encoder Down,EXPP(MPG),EXMP(MPG),SQSTR1,SQSTR2,SQSTP1,SQSTP2,Mode Terminal,,,,,,,,,,,,".Split(',');
                string[] lblMotorStatus02 = "Positive End Limit,Negative End Limit,Positive SW E-Stop,Negative SW E-Stop,Positive SW S-Stop,Negative SW S-Stop,Seovo Alarm,Emergency Stop,Emergenct Stop Command,Slow Stop Command,All-Axis Stop Command,Sync Stop Func #1 End,Sync Stop Func #2 End,Encoder Input Error,MPG Input Error,ERRSTATE,Interpolatioon Data Errorm,,,,,,,,,,".Split(',');
                string[] lblMotorStatus03 = "Busy(In Drive Move),Down(In Deceleration),Const(In Constant Speed),Up(In Acceleration),In move of continuous,In move of assigned,In move of MPG,In move of home,In move of Interpolation,In move of Slave,WAIT_INP,,,,,,,,,,,,,,,,,".Split(',');
                for (int iPos = 0; iPos < _lstLblMotorStatus1.Count; iPos++)
                {
                    _lstLblMotorStatus1[iPos].Text = lblMotorStatus01[iPos];
                    _lstLblMotorStatus2[iPos].Text = lblMotorStatus02[iPos];
                    _lstLblMotorStatus3[iPos].Text = lblMotorStatus03[iPos];
                }

            }
            else if (btnUmacMotor.BackColor == UiGlobal.SELECT_C)
            {
                string[] lblMotorStatus01 = "Rapid max velocity select,SIgn/magnitude servo enable,Software capture enable,Capture on error enable,Pos follow enable,Pos follow offset mode,Commutation enable,Y-address commute enable,User-Written servo enable,User-Written phase enable,Home search in progress,Block request,Abort deceleration in progress,Desired velocity 0,Data block error,Dwell in progress,Intergration mode,Move time active,Open loop mode,Amplifier enable,Ext servbo algo enable,Positive and limit set,Negative end limit set,Motor activated,,,,,,,,".Split(',');
                string[] lblMotorStatus02 = "In-position true,Warning floowing error exceeded,Fatal following error exceeded,Amplifier fault error,Backlash direction flag,I2T Amplifier fault error,Integrated fatal following error,Trigger move,Phasing search error,Motor Phase Request,Home complete,Stopped on position limit,Desired position limit,Foreground in-position,[Reserved for future use],Assigned to C.S.,CS Axis definition bit 0,CS Axis definition bit 1,CS Axis definition bit 2,CS Axis definition bit 3,[CS-1] # bit 0 [LSB],[CS-1] # bit 1,[CS-1] # bit 2,[CS-1] # bit 3 [MSB],,,,,,".Split(',');
                string[] lblMotorStatus03 = ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,".Split(',');
                for (int iPos = 0; iPos < _lstLblMotorStatus1.Count; iPos++)
                {
                    _lstLblMotorStatus1[iPos].Text = lblMotorStatus01[iPos];
                    _lstLblMotorStatus2[iPos].Text = lblMotorStatus02[iPos];
                    _lstLblMotorStatus3[iPos].Text = lblMotorStatus03[iPos];
                }
            }
            else if (btnEZiMotor.BackColor == UiGlobal.SELECT_C)
            {
                string[] lblMotorStatus01 = "All Error,HW Plus Limit,HW Minus Limit,SW Plus Limit,SW Minus Limit,Reserved,Position Count Over,Over Current Error,Over Speed Error,Step Out Error,Over Load Error,Over Heat Error,Back EMF Error,Motor Power Error,Motor Power Error,Inposition Error,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,".Split(',');
                string[] lblMotorStatus02 = "Emergency Stop,Slow Stop,Org Returning,Inposition,Servo On,Alarm Reset,PT Stopped,Origin Sensor,Z Pulse,Org Ret OK,Motion DIR,Motioning,Motion Pause,Motion Accel,Motion Decel,Motion Constant,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,".Split(',');
                string[] lblMotorStatus03 = ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,".Split(',');
                for (int iPos = 0; iPos < _lstLblMotorStatus1.Count; iPos++)
                {
                    _lstLblMotorStatus1[iPos].Text = lblMotorStatus01[iPos];
                    _lstLblMotorStatus2[iPos].Text = lblMotorStatus02[iPos];
                    _lstLblMotorStatus3[iPos].Text = lblMotorStatus03[iPos];
                }
            }
            else
            {
                string[] lblMotorStatus01 = ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,".Split(',');
                string[] lblMotorStatus02 = ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,".Split(',');
                string[] lblMotorStatus03 = ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,".Split(',');
                for (int iPos = 0; iPos < _lstLblMotorStatus1.Count; iPos++)
                {
                    _lstLblMotorStatus1[iPos].Text = lblMotorStatus01[iPos];
                    _lstLblMotorStatus2[iPos].Text = lblMotorStatus02[iPos];
                    _lstLblMotorStatus3[iPos].Text = lblMotorStatus03[iPos];
                }
            }
        }

        public void UIUpdate()
        {
            if (_servo == null) return;

            if (_servo.MotorType == EM_MOTOR_TYPE.Umac)
            {
                FillUmacServoSatusUpdate(_servo);
            }
            if (_servo.MotorType == EM_MOTOR_TYPE.Ajin)
            {
                FillAjinServoSatusUpdate(_servo);
            }
            if (_servo.MotorType == EM_MOTOR_TYPE.EZi)
            {
                FillEziServoSatusUpdate(_servo);
            }
        }

        private void FillEziServoSatusUpdate(ServoMotorControl servo)
        {
            _lstBtnMotorStatus1[00].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(00) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[01].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(01) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[02].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(02) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[03].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(03) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[04].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(04) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[05].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(05) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[06].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(06) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[07].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(07) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[08].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(08) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[09].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(09) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[10].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(10) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[11].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(11) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[12].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(12) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[13].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(13) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[14].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(14) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[15].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(15) ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            _lstBtnMotorStatus2[00].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(16) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[01].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(17) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[02].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(18) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[03].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(19) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[04].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(20) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[05].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(21) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[06].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(22) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[07].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(23) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[08].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(24) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[09].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(25) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[10].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(26) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[11].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(27) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[12].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(28) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[13].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(29) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[14].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(30) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[15].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(31) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }

        private void FillAjinServoSatusUpdate(ServoMotorControl servo)
        {
            _lstBtnMotorStatus1[00].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(00) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[01].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(01) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[02].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(02) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[03].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(03) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[04].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(04) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[05].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(05) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[06].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(06) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[07].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(07) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[08].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(08) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[09].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(09) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[10].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(10) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[11].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(11) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[12].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(12) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[13].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(13) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[14].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(14) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[15].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(15) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[16].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(16) ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            _lstBtnMotorStatus2[00].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(00) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[01].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(01) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[02].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(02) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[03].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(03) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[04].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(04) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[05].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(05) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[06].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(06) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[07].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(07) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[08].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(08) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[09].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(09) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[10].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(10) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[11].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(11) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[12].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(12) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[13].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(13) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[14].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(14) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[15].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(15) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[16].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(16) ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            _lstBtnMotorStatus3[00].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(00) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[01].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(01) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[02].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(02) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[03].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(03) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[04].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(04) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[05].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(05) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[06].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(06) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[07].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(07) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[08].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(08) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[09].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(09) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus3[10].BackColor = servo.XI_CurrMotorStatus1.vInt.GetBit(10) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }

        public void FillUmacServoSatusUpdate(ServoMotorControl servo)
        {
            //1B0              
            _lstBtnMotorStatus1[00].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(00) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[01].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(01) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[02].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(02) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[03].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(03) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[04].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(04) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[05].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(05) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[06].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(06) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[07].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(07) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[08].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(08) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[09].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(09) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[10].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(10) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[11].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(11) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[12].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(12) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[13].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(13) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[14].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(14) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[15].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(15) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[16].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(16) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[17].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(17) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[18].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(18) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[19].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(19) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[20].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(20) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[21].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(21) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[22].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(22) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus1[23].BackColor = servo.XI_CurrMotorLow.vInt.GetBit(23) ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            //  1C0 
            _lstBtnMotorStatus2[00].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(00) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[01].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(01) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[02].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(02) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[03].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(03) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[04].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(04) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[05].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(05) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[06].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(06) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[07].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(07) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[08].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(08) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[09].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(09) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[10].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(10) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[11].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(11) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[12].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(12) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[13].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(13) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[14].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(14) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[15].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(15) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[16].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(16) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[17].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(17) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[18].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(18) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[19].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(19) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[20].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(20) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[21].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(21) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[22].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(22) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            _lstBtnMotorStatus2[23].BackColor = servo.XI_CurrMotorHight.vInt.GetBit(23) ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }


        private void btnMonitorLoader_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btnMonitorLoader.BackColor = btnMonitorLoader == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMonitorProcess.BackColor = btnMonitorProcess == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMonitorUnloader.BackColor = btnMonitorUnloader == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            btnAjinMotor_Click(btnAjinMotor, null);
        }
        private void btnAjinMotor_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btnAjinMotor.BackColor = btnAjinMotor == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnUmacMotor.BackColor = btnUmacMotor == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnEZiMotor.BackColor = btnEZiMotor == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            
            flpSevoMotors.Controls.Clear();

            if (btnMonitorLoader.BackColor == UiGlobal.SELECT_C)
            {
                if (btnAjinMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, GG.Equip.GetServoMotors(EM_MOTOR_POSI.LD, EM_MOTOR_TYPE.Ajin));
                }
                else if (btnUmacMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }
                else if (btnEZiMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }

            }
            else if (btnMonitorProcess.BackColor == UiGlobal.SELECT_C)
            {
                if (btnAjinMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, GG.Equip.GetServoMotors(EM_MOTOR_POSI.PROC, EM_MOTOR_TYPE.Ajin));
                }
                else if (btnUmacMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, GG.Equip.GetServoMotors(EM_MOTOR_POSI.PROC, EM_MOTOR_TYPE.Umac));
                }
                else if (btnEZiMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, GG.Equip.GetServoMotors(EM_MOTOR_POSI.PROC, EM_MOTOR_TYPE.EZi));
                }
            }
            else if (btnMonitorUnloader.BackColor == UiGlobal.SELECT_C)
            {
                if (btnAjinMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, GG.Equip.GetServoMotors(EM_MOTOR_POSI.UD, EM_MOTOR_TYPE.Ajin));
                }
                else if (btnUmacMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }
                else if (btnEZiMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }
            }

            if (flpSevoMotors.Controls.Count > 0)
                SelectedServo_Click(flpSevoMotors.Controls[0], null);

            UpdateLable();

        }
    }
}

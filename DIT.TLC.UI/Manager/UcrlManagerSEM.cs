﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerSEM : UserControl, IUIUpdate
    {
        public UcrlManagerSEM()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (sender == btnGpsAvMeterOpen)
            {
                GG.Equip.SemGpsProxy.Start();
                GG.Equip.SemGpsProxy.SendRequestToAccura();
            }
            if (sender == btnGpsAvMeterClose)
            {
                GG.Equip.SemGpsProxy.Stop();
            }
            if(sender == btnUpsAvMeterOpen)
            {
                GG.Equip.SemUpsProxy.Start();
                GG.Equip.SemUpsProxy.SendRequestToAccura();
            }
            if (sender == btnUpsAvMeterClose)
            {
                GG.Equip.SemUpsProxy.Stop();
            }
        }

        public void UIUpdate()
        {            
            if(GG.Equip.SemGpsProxy.IsConnection == true)
            {
                if (GG.Equip.SemGpsProxy.IsRecvComplete)
                {
                    GG.Equip.SemGpsProxy.SendRequestToAccura();
                    lviGpsAvMeter.Items.Clear();

                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "AB 상의 선간 전압", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Voltage_Vab), "V" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "BC 상의 선간 전압", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Voltage_Vbc), "V" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "CA 상의 선간 전압", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Voltage_Vca), "V" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상의 선간 전압 평균", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Voltage_Vavg), "V" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "A상 전류", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Current_Ia), "A" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "B상 전류", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Current_Ib), "A" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "C상 전류", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Current_Ic), "A" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상 전류 평균", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Current_Iavg), "A" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "A상의 유효전력 최대값", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Pa_max), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "B상의 유효전력 최대값", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Pb_max), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "C상의 유효전력 최대값", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Pc_max), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상의 유효전력 총합 최대값", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Ptot_max), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "A상의 유효전력 demand", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Demand_KWA), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "B상의 유효전력 demand", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Demand_KWB), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "C상의 유효전력 demand", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Demand_KWC), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상의 유효전력 demand 총합", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Demand_KWTotal), "KW" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "온도", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Temperature_max), "C" }));
                    lviGpsAvMeter.Items.Add(new ListViewItem(new string[] { "좋합 전력량", string.Format("{0}", GG.Equip.SemGpsProxy.AvMeterInfo.Sum_KWh), "KW/h" }));
                }
            }

            if (GG.Equip.SemUpsProxy.IsConnection == true)
            {
                if (GG.Equip.SemUpsProxy.IsRecvComplete)
                {
                    GG.Equip.SemUpsProxy.SendRequestToAccura();
                    lviUpsAvMeter.Items.Clear();

                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "AB 상의 선간 전압", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Voltage_Vab), "V" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "BC 상의 선간 전압", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Voltage_Vbc), "V" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "CA 상의 선간 전압", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Voltage_Vca), "V" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상의 선간 전압 평균", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Voltage_Vavg), "V" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "A상 전류", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Current_Ia), "A" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "B상 전류", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Current_Ib), "A" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "C상 전류", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Current_Ic), "A" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상 전류 평균", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Current_Iavg), "A" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "A상의 유효전력 최대값", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Pa_max), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "B상의 유효전력 최대값", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Pb_max), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "C상의 유효전력 최대값", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Pc_max), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상의 유효전력 총합 최대값", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Ptot_max), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "A상의 유효전력 demand", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Demand_KWA), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "B상의 유효전력 demand", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Demand_KWB), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "C상의 유효전력 demand", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Demand_KWC), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "삼상의 유효전력 demand 총합", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Demand_KWTotal), "KW" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "온도", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Temperature_max), "C" }));
                    lviUpsAvMeter.Items.Add(new ListViewItem(new string[] { "좋합 전력량", string.Format("{0}", GG.Equip.SemUpsProxy.AvMeterInfo.Sum_KWh), "KW/h" }));
                }
            }
        }
    }
}

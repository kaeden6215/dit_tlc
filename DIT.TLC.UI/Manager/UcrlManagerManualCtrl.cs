﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerManualCtrl : UserControl, IUIUpdate
    {
        private UcrlAjinServoCtrl[] _lstUcrlAjinServoCtrl = null;
        public UcrlManagerManualCtrl()
        {
            InitializeComponent();

            _lstUcrlAjinServoCtrl = new UcrlAjinServoCtrl[]
            {
            ucrlAjinServoCtrl01, ucrlAjinServoCtrl02,
            ucrlAjinServoCtrl03, ucrlAjinServoCtrl04,
            ucrlAjinServoCtrl05, ucrlAjinServoCtrl06,
            ucrlAjinServoCtrl07, ucrlAjinServoCtrl08,
            };
        }


        public void InitalizeUserControlUI(Equipment equip)
        {
            ucrlAjinServoCtrl01.Servo = null;
            ucrlAjinServoCtrl02.Servo = null;
            ucrlAjinServoCtrl03.Servo = null;
            ucrlAjinServoCtrl04.Servo = null;            
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCstLoad_Click(object sender, EventArgs e)
        {
            btnCstLoad.BackColor = UiGlobal.UNSELECT_C;
            btnCellOutTransfer.BackColor = UiGlobal.UNSELECT_C;
            btnCellLoadTransfer01.BackColor = UiGlobal.UNSELECT_C;
            btnCellLoadTransfer02.BackColor = UiGlobal.UNSELECT_C;
            btnIRCutProcess.BackColor = UiGlobal.UNSELECT_C;
            btnAfterIRCutTransfer.BackColor = UiGlobal.UNSELECT_C;
            btnBreakUnit01.BackColor = UiGlobal.UNSELECT_C;
            btnBreakUnit02.BackColor = UiGlobal.UNSELECT_C;
            btnBreakHeadUnitXZ.BackColor = UiGlobal.UNSELECT_C;
            btnBeforeTransfer.BackColor = UiGlobal.UNSELECT_C;
            btnInspUnit.BackColor = UiGlobal.UNSELECT_C;
            btnAfterTransfer.BackColor = UiGlobal.UNSELECT_C;
            btnCameraUnit.BackColor = UiGlobal.UNSELECT_C;
            btnCellInTransfer.BackColor = UiGlobal.UNSELECT_C;
            btnCstUnloader.BackColor = UiGlobal.UNSELECT_C;

            Button btnSender = (Button)sender;
            btnSender.BackColor = UiGlobal.SELECT_C;

            if (btnSender == btnCstLoad)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.LD.CstLoader_B.CstRotationAxis, GG.Equip.LD.CstLoader_A.CstRotationAxis,
                        GG.Equip.LD.CstLoader_B.CstUpDownAxis, GG.Equip.LD.CstLoader_A.CstUpDownAxis,
                        null, null,
                        null, null
                    });
            }
            else if (btnSender == btnCellOutTransfer)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.LD.Loader.X1Axis, GG.Equip.LD.Loader.X2Axis,
                        GG.Equip.LD.Loader.Y1Axis, GG.Equip.LD.Loader.Y2Axis,
                        null, null,
                        null, null
                    });
            }
            else if (btnSender == btnCellLoadTransfer01)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.LD.LoaderTransfer_B.YAxis,  GG.Equip.LD.LoaderTransfer_A.YAxis,
                        GG.Equip.LD.LoaderTransfer_B.X1Axis,  GG.Equip.LD.LoaderTransfer_A.X1Axis,
                        GG.Equip.LD.LoaderTransfer_B.X2Axis,  GG.Equip.LD.LoaderTransfer_A.X2Axis,
                        GG.Equip.LD.LoaderTransfer_B.SubY1Axis,  GG.Equip.LD.LoaderTransfer_A.SubY1Axis,
                    });

            }
            else if (btnSender == btnCellLoadTransfer02)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.LD.LoaderTransfer_B.SubY2Axis,  GG.Equip.LD.LoaderTransfer_A.SubY2Axis,
                        GG.Equip.LD.LoaderTransfer_B.SubT1Axis,  GG.Equip.LD.LoaderTransfer_A.SubT1Axis,
                        GG.Equip.LD.LoaderTransfer_B.SubT2Axis,  GG.Equip.LD.LoaderTransfer_A.SubT2Axis,
                        GG.Equip.LD.PreAlign.XAxis, GG.Equip.PROC.FineAlign.XAxis
                    });

            }

            else if (btnSender == btnIRCutProcess)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.IRCutStage_B.YAxis,  GG.Equip.PROC.IRCutStage_A.YAxis,
                        GG.Equip.PROC.LaserHead.XAxis , null,
                        GG.Equip.PROC.LaserHead.ZAxis , null,
                        null, null,
                    });
            }
            else if (btnSender == btnAfterIRCutTransfer)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.AfterIRCutTransfer_B.YAxis,  GG.Equip.PROC.AfterIRCutTransfer_A.YAxis,
                        null, null,
                        null, null,
                        null, null,
                    });
            }
            else if (btnSender == btnBreakUnit01)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.BreakStage_B.YAxis,  GG.Equip.PROC.BreakStage_A.YAxis,
                        GG.Equip.PROC.BreakStage_B.SubY1Axis,  GG.Equip.PROC.BreakStage_A.SubY1Axis,
                        GG.Equip.PROC.BreakStage_B.SubY2Axis,  GG.Equip.PROC.BreakStage_A.SubY2Axis,
                        GG.Equip.PROC.BreakStage_B.SubX1Axis,  GG.Equip.PROC.BreakStage_A.SubX1Axis,
                    });
            }
            else if (btnSender == btnBreakUnit02)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.BreakStage_B.SubX2Axis,  GG.Equip.PROC.BreakStage_A.SubX2Axis,
                        GG.Equip.PROC.BreakStage_B.SubT1Axis,  GG.Equip.PROC.BreakStage_A.SubT1Axis,
                        GG.Equip.PROC.BreakStage_B.SubT2Axis,  GG.Equip.PROC.BreakStage_A.SubT2Axis,
                        null, null
                    });
            }
            else if (btnSender == btnBreakHeadUnitXZ)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.BreakingHead.X1Axis, GG.Equip.PROC.BreakingHead.X2Axis,
                        GG.Equip.PROC.BreakingHead.Z1Axis, GG.Equip.PROC.BreakingHead.Z2Axis,
                        GG.Equip.PROC.BreakAlign.XAxis , GG.Equip.PROC.BreakAlign.ZAxis,
                        null, null
                    });
            }
            else if (btnSender == btnBeforeTransfer)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis,  GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis,
                        null, null,
                        null, null,
                        null, null,
                    });
            }
            else if (btnSender == btnInspUnit)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.InspectionStage_A.YAxis,  GG.Equip.UD.InspectionStage_B.YAxis,
                        null, null,
                        null, null,
                        null, null,
                    });
            }
            else if (btnSender == btnAfterTransfer)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis,  GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis,
                        GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis,  GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis,
                        GG.Equip.UD.AfterInspUnloaderTransfer_B.T2Axis,  GG.Equip.UD.AfterInspUnloaderTransfer_A.T2Axis,
                        null, null,
                    });
            }
            else if (btnSender == btnCameraUnit)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.InspCamera.XAxis,  GG.Equip.UD.InspCamera.ZAxis,
                        GG.Equip.UD.InspectionStage_B.YAxis, GG.Equip.UD.InspectionStage_A.YAxis,
                        null, null,
                        null, null,
                    });
            }
            else if (btnSender == btnCellInTransfer)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.Unloader.X1Axis,  GG.Equip.UD.Unloader.X2Axis,
                        GG.Equip.UD.Unloader.Y1Axis,  GG.Equip.UD.Unloader.Y2Axis,
                        null, null,
                        null, null,
                    });
            }
            else if (btnSender == btnCstUnloader)
            {
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.CstUnloader_B.CstRotationAxis, GG.Equip.UD.CstUnloader_A.CstRotationAxis,
                        GG.Equip.UD.CstUnloader_B.CstUpDownAxis, GG.Equip.UD.CstUnloader_A.CstUpDownAxis,
                        null, null,
                        null, null
                    });
            }
        }

        private void FillServoUi(ServoMotorControl[] lstServo)
        {
            for (int iPos = 0; iPos < 8; iPos++)
            {
                if (iPos < lstServo.Length)
                {
                    _lstUcrlAjinServoCtrl[iPos].Visible = lstServo[iPos] == null ? false : true;
                    _lstUcrlAjinServoCtrl[iPos].Servo = lstServo[iPos];
                }
                else
                {
                    _lstUcrlAjinServoCtrl[iPos].Visible = false;
                    _lstUcrlAjinServoCtrl[iPos].Servo = null;
                }
            }
        }         
        public void UIUpdate()
        {
            for (int iPos = 0; iPos < 8; iPos++)
                _lstUcrlAjinServoCtrl[iPos].UIUpdate();
        }
    }
}

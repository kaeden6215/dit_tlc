﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerControl : UserControl, IUIUpdate
    {
        public UcrlManagerControl()
        {
            InitializeComponent();
        }
        public void UIUpdate()
        {
            SetColor(btnLoadingLoaderACylinderUp, (GG.Equip.LD.CstLoader_A.TiltCylinder.IsBackward));
            SetColor(btnLoadingLoaderACylinderDown, (GG.Equip.LD.CstLoader_A.TiltCylinder.IsForward));
            SetColor(btnLoadingLoaderAVaccumOn, (GG.Equip.LD.Loader.Vaccum1.IsOnOff == true));
            SetColor(btnLoadingLoaderAVaccumOff, (GG.Equip.LD.Loader.Vaccum1.IsOnOff == false));
            SetColor(btnLoadingLoaderABlowerOn, (GG.Equip.LD.Loader.Blower1.IsOnOff == true));
            SetColor(btnLoadingLoaderABlowerOff, (GG.Equip.LD.Loader.Blower1.IsOnOff == false));
            SetColor(btnLoadingLoaderBCylinderUp, (GG.Equip.LD.CstLoader_B.TiltCylinder.IsBackward));
            SetColor(btnLoadingLoaderBCylinderDown, (GG.Equip.LD.CstLoader_B.TiltCylinder.IsForward));
            SetColor(btnLoadingLoaderBVaccumOn, (GG.Equip.LD.Loader.Vaccum2.IsOnOff == true));
            SetColor(btnLoadingLoaderBVaccumOff, (GG.Equip.LD.Loader.Vaccum2.IsOnOff == false));
            SetColor(btnLoadingLoaderBBlowerOn, (GG.Equip.LD.Loader.Blower2.IsOnOff == true));
            SetColor(btnLoadingLoaderBBlowerOff, (GG.Equip.LD.Loader.Blower2.IsOnOff == false));
            SetColor(btnLoadingLoaderTransferAVaccumOn, (GG.Equip.LD.LoaderTransfer_A.Vaccum1.IsOnOff == true && GG.Equip.LD.LoaderTransfer_A.Vaccum2.IsOnOff == true));
            SetColor(btnLoadingLoaderTransferAVaccumOff, (GG.Equip.LD.LoaderTransfer_A.Vaccum1.IsOnOff == false && GG.Equip.LD.LoaderTransfer_A.Vaccum2.IsOnOff == false));
            SetColor(btnLoadingLoaderTransferABlowerOn, (GG.Equip.LD.LoaderTransfer_A.Blower1.IsOnOff == true && GG.Equip.LD.LoaderTransfer_A.Blower2.IsOnOff == true));
            SetColor(btnLoadingLoaderTransferABlowerOff, (GG.Equip.LD.LoaderTransfer_A.Blower1.IsOnOff == false && GG.Equip.LD.LoaderTransfer_A.Blower2.IsOnOff == false));
            SetColor(btnLoadingLoaderTransferBVaccumOn, (GG.Equip.LD.LoaderTransfer_B.Vaccum1.IsOnOff == true && GG.Equip.LD.LoaderTransfer_B.Vaccum2.IsOnOff == true));
            SetColor(btnLoadingLoaderTransferBVaccumOff, (GG.Equip.LD.LoaderTransfer_B.Vaccum1.IsOnOff == false && GG.Equip.LD.LoaderTransfer_B.Vaccum2.IsOnOff == false));
            SetColor(btnLoadingLoaderTransferBBlowerOn, (GG.Equip.LD.LoaderTransfer_B.Blower1.IsOnOff == true && GG.Equip.LD.LoaderTransfer_B.Blower2.IsOnOff == true));
            SetColor(btnLoadingLoaderTransferBBlowerOff, (GG.Equip.LD.LoaderTransfer_B.Blower1.IsOnOff == false && GG.Equip.LD.LoaderTransfer_B.Blower2.IsOnOff == false));
            SetColor(btnUnloadingUnloaderACylinderUp, (GG.Equip.UD.CstUnloader_A.TiltCylinder.IsBackward));
            SetColor(btnUnloadingUnloaderACylinderDown, (GG.Equip.UD.CstUnloader_A.TiltCylinder.IsForward));
            SetColor(btnUnloadingUnloaderAVaccumOn, (GG.Equip.UD.Unloader.Vaccum1.IsOnOff == true));
            SetColor(btnUnloadingUnloaderAVaccumOff, (GG.Equip.UD.Unloader.Vaccum1.IsOnOff == false));
            SetColor(btnUnloadingUnloaderABlowerOn, (GG.Equip.UD.Unloader.Blower1.IsOnOff == true));
            SetColor(btnUnloadingUnloaderABlowerOff, (GG.Equip.UD.Unloader.Blower1.IsOnOff == false));
            SetColor(btnUnloadingUnloaderBCylinderUp, (GG.Equip.UD.CstUnloader_B.TiltCylinder.IsBackward));
            SetColor(btnUnloadingUnloaderBCylinderDown, (GG.Equip.UD.CstUnloader_B.TiltCylinder.IsForward));
            SetColor(btnUnloadingUnloaderBVaccumOn, (GG.Equip.UD.Unloader.Vaccum2.IsOnOff == true));
            SetColor(btnUnloadingUnloaderBVaccumOff, (GG.Equip.UD.Unloader.Vaccum2.IsOnOff == false));
            SetColor(btnUnloadingUnloaderBBlowerOn, (GG.Equip.UD.Unloader.Blower2.IsOnOff == true));
            SetColor(btnUnloadingUnloaderBBlowerOff, (GG.Equip.UD.Unloader.Blower2.IsOnOff == false));
            SetColor(btnUnloadingBeforeTransferAVaccumOn, (GG.Equip. UD.BeforeInspUnloaderTransfer_A.Vaccum1.IsOnOff == true &&  GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum2.IsOnOff == true));
            SetColor(btnUnloadingBeforeTransferAVaccumOff, (GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum1.IsOnOff == false && GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum2.IsOnOff == false));
            SetColor(btnUnloadingBeforeTransferABlowerOn, (GG.Equip. UD.BeforeInspUnloaderTransfer_A.Blower1.IsOnOff == true &&  GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower2.IsOnOff == true));
            SetColor(btnUnloadingBeforeTransferABlowerOff, (GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower1.IsOnOff == false && GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower2.IsOnOff == false));
            SetColor(btnUnloadingBeforeTransferBVaccumOn, (GG.Equip. UD.BeforeInspUnloaderTransfer_B.Vaccum1.IsOnOff == true &&  GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum2.IsOnOff == true));
            SetColor(btnUnloadingBeforeTransferBVaccumOff, (GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum1.IsOnOff == false && GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum2.IsOnOff == false));
            SetColor(btnUnloadingBeforeTransferBBlowerOn, (GG.Equip. UD.BeforeInspUnloaderTransfer_B.Blower1.IsOnOff == true &&  GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower2.IsOnOff == true));
            SetColor(btnUnloadingBeforeTransferBBlowerOff, (GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower1.IsOnOff == false && GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower2.IsOnOff == false));
            SetColor(btnUnloadingAfterTransferAVaccumOn, (GG.Equip. UD.AfterInspUnloaderTransfer_A.Vaccum1.IsOnOff == true &&  GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum2.IsOnOff == true));
            SetColor(btnUnloadingAfterTransferAVaccumOff, (GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum1.IsOnOff == false && GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum2.IsOnOff == false));
            SetColor(btnUnloadingAfterTransferABlowerOn, (GG.Equip. UD.AfterInspUnloaderTransfer_A.Blower1.IsOnOff == true &&  GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower2.IsOnOff == true));
            SetColor(btnUnloadingAfterTransferABlowerOff, (GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower1.IsOnOff == false && GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower2.IsOnOff == false));
            SetColor(btnUnloadingAfterTransferBVaccumOn, (GG.Equip. UD.AfterInspUnloaderTransfer_B.Vaccum1.IsOnOff == true &&  GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum2.IsOnOff == true));
            SetColor(btnUnloadingAfterTransferBVaccumOff, (GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum1.IsOnOff == false && GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum2.IsOnOff == false));
            SetColor(btnUnloadingAfterTransferBBlowerOn, (GG.Equip. UD.AfterInspUnloaderTransfer_B.Blower1.IsOnOff == true &&  GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower2.IsOnOff == true));
            SetColor(btnUnloadingAfterTransferBBlowerOff, (GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower1.IsOnOff == false && GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower2.IsOnOff == false));
        }
        private void btn_Loading_Loader_A_CylinderUp_Click(object sender, EventArgs e)
        {
            GG.Equip.LD.Loader.Vaccum1.OnOff(GG.Equip, false);
        }

        private void btn_Loading_LoaderTransfer_CylinderReset_Click(object sender, EventArgs e)
        {

        }

        private void btnLoader_Click(object sender, EventArgs e)
        {
            // A
            if (btnLoadingLoaderACylinderUp == (Button)sender)
            {
                GG.Equip.LD.CstLoader_A.TiltCylinder.Forward(GG.Equip);
            }
            else if (btnLoadingLoaderACylinderDown == (Button)sender)
            {
                GG.Equip.LD.CstLoader_A.TiltCylinder.Backward(GG.Equip);
            }
            //공압
            else if (btnLoadingLoaderAVaccumOn == (Button)sender)
            {
                GG.Equip.LD.Loader.Vaccum1.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderAVaccumOff == (Button)sender)
            {
                GG.Equip.LD.Loader.Vaccum1.OnOff(GG.Equip, false);
            }
            //블로우
            else if (btnLoadingLoaderABlowerOn == (Button)sender)
            {
                GG.Equip.LD.Loader.Blower1.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderABlowerOff == (Button)sender)
            {
                GG.Equip.LD.Loader.Blower1.OnOff(GG.Equip, false);
            }

            //B열
            else if (btnLoadingLoaderBCylinderUp == (Button)sender)
            {
                GG.Equip.LD.CstLoader_B.TiltCylinder.Forward(GG.Equip);
            }
            else if (btnLoadingLoaderBCylinderDown == (Button)sender)
            {
                GG.Equip.LD.CstLoader_B.TiltCylinder.Backward(GG.Equip);
            }
            //공압
            else if (btnLoadingLoaderBVaccumOn == (Button)sender)
            {
                GG.Equip.LD.Loader.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderBVaccumOff == (Button)sender)
            {
                GG.Equip.LD.Loader.Vaccum2.OnOff(GG.Equip, false);
            }
            //블로우
            else if (btnLoadingLoaderBBlowerOn == (Button)sender)
            {
                GG.Equip.LD.Loader.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderBBlowerOff == (Button)sender)
            {
                GG.Equip.LD.Loader.Blower2.OnOff(GG.Equip, false);
            }
        }

        private void btnLoaderTransfer_Click(object sender, EventArgs e)
        {
            // A
            if (btnLoadingLoaderTransferAVaccumOn == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_A.Vaccum1.OnOff(GG.Equip, true);
                GG.Equip.LD.LoaderTransfer_A.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderTransferAVaccumOff == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_A.Vaccum1.OnOff(GG.Equip, false);
                GG.Equip.LD.LoaderTransfer_A.Vaccum2.OnOff(GG.Equip, false);
            }
            //파기
            else if (btnLoadingLoaderTransferABlowerOn == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_A.Blower1.OnOff(GG.Equip, true);
                GG.Equip.LD.LoaderTransfer_A.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderTransferABlowerOff == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_A.Blower1.OnOff(GG.Equip, false);
                GG.Equip.LD.LoaderTransfer_A.Blower2.OnOff(GG.Equip, false);
            }

 
            //B 열

            if (btnLoadingLoaderTransferBVaccumOn == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_B.Vaccum1.OnOff(GG.Equip, true);
                GG.Equip.LD.LoaderTransfer_B.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderTransferBVaccumOff == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_B.Vaccum1.OnOff(GG.Equip, false);
                GG.Equip.LD.LoaderTransfer_B.Vaccum2.OnOff(GG.Equip, false);
            }
            //파기
            else if (btnLoadingLoaderTransferBBlowerOn == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_B.Blower1.OnOff(GG.Equip, true);
                GG.Equip.LD.LoaderTransfer_B.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnLoadingLoaderTransferBBlowerOff == (Button)sender)
            {
                GG.Equip.LD.LoaderTransfer_B.Blower1.OnOff(GG.Equip, false);
                GG.Equip.LD.LoaderTransfer_B.Blower2.OnOff(GG.Equip, false);
            }

        }


        private void btnUnloader_Click(object sender, EventArgs e)
        {
            // A
            if (btnUnloadingUnloaderACylinderUp == (Button)sender)
            {
                GG.Equip.UD.CstUnloader_A.TiltCylinder.Forward(GG.Equip);
            }
            else if (btnUnloadingUnloaderACylinderDown == (Button)sender)
            {
                GG.Equip.UD.CstUnloader_A.TiltCylinder.Backward(GG.Equip);
            }
            //공압
            else if (btnUnloadingUnloaderAVaccumOn == (Button)sender)
            {
                GG.Equip.UD.Unloader.Vaccum1.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingUnloaderAVaccumOff == (Button)sender)
            {
                GG.Equip.UD.Unloader.Vaccum1.OnOff(GG.Equip, false);
            }
            //블로우
            else if (btnUnloadingUnloaderABlowerOn == (Button)sender)
            {
                GG.Equip.UD.Unloader.Blower1.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingUnloaderABlowerOff == (Button)sender)
            {
                GG.Equip.UD.Unloader.Blower1.OnOff(GG.Equip, false);
            }

            //B열
            else if (btnUnloadingUnloaderBCylinderUp == (Button)sender)
            {
                GG.Equip.UD.CstUnloader_B.TiltCylinder.Forward(GG.Equip);
            }
            else if (btnUnloadingUnloaderBCylinderDown == (Button)sender)
            {
                GG.Equip.UD.CstUnloader_B.TiltCylinder.Backward(GG.Equip);
            }
            //공압
            else if (btnUnloadingUnloaderBVaccumOn == (Button)sender)
            {
                GG.Equip.UD.Unloader.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingUnloaderBVaccumOff == (Button)sender)
            {
                GG.Equip.UD.Unloader.Vaccum2.OnOff(GG.Equip, false);
            }
            //블로우
            else if (btnUnloadingUnloaderBBlowerOn == (Button)sender)
            {
                GG.Equip.UD.Unloader.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingUnloaderBBlowerOff == (Button)sender)
            {
                GG.Equip.UD.Unloader.Blower2.OnOff(GG.Equip, false);
            }
        }


        private void btnBeforeTransfer_Click(object sender, EventArgs e)
        {
            // A
            if (btnUnloadingBeforeTransferAVaccumOn == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum1.OnOff(GG.Equip, true);
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingBeforeTransferAVaccumOff == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum1.OnOff(GG.Equip, false);
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum2.OnOff(GG.Equip, false);
            }
            //파기
            else if (btnUnloadingBeforeTransferABlowerOn == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower1.OnOff(GG.Equip, true);
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingBeforeTransferABlowerOff == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower1.OnOff(GG.Equip, false);
                GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower2.OnOff(GG.Equip, false);
            }


            //B 열

            if (btnUnloadingBeforeTransferBVaccumOn == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum1.OnOff(GG.Equip, true);
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingBeforeTransferBVaccumOff == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum1.OnOff(GG.Equip, false);
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum2.OnOff(GG.Equip, false);
            }
            //파기
            else if (btnUnloadingBeforeTransferBBlowerOn == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower1.OnOff(GG.Equip, true);
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingBeforeTransferBBlowerOff == (Button)sender)
            {
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower1.OnOff(GG.Equip, false);
                GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower2.OnOff(GG.Equip, false);
            }
        }

        private void btnAfterTransfer_Click(object sender, EventArgs e)
        {
            // A
            if (btnUnloadingAfterTransferAVaccumOn == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum1.OnOff(GG.Equip, true);
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingAfterTransferAVaccumOff == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum1.OnOff(GG.Equip, false);
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum2.OnOff(GG.Equip, false);
            }
            //파기
            else if (btnUnloadingAfterTransferABlowerOn == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower1.OnOff(GG.Equip, true);
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingAfterTransferABlowerOff == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower1.OnOff(GG.Equip, false);
                GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower2.OnOff(GG.Equip, false);
            }


            //B 열

            if (btnUnloadingAfterTransferBVaccumOn == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum1.OnOff(GG.Equip, true);
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingAfterTransferBVaccumOff == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum1.OnOff(GG.Equip, false);
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum2.OnOff(GG.Equip, false);
            }
            //파기
            else if (btnUnloadingAfterTransferBBlowerOn == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower1.OnOff(GG.Equip, true);
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower2.OnOff(GG.Equip, true);
            }
            else if (btnUnloadingAfterTransferBBlowerOff == (Button)sender)
            {
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower1.OnOff(GG.Equip, false);
                GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower2.OnOff(GG.Equip, false);
            }
        }

        public void SetColor(Button btn, bool value)
        {
            btn.BackColor = value ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }

    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerIOStatus
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMonitorLoaderIO = new System.Windows.Forms.Button();
            this.btnMonitorProcessIO = new System.Windows.Forms.Button();
            this.btnMonitorUnloaderIO = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnInIO01 = new System.Windows.Forms.Button();
            this.btnInIO02 = new System.Windows.Forms.Button();
            this.btnInIO03 = new System.Windows.Forms.Button();
            this.btnInIO04 = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnOutIO01 = new System.Windows.Forms.Button();
            this.btnOutIO02 = new System.Windows.Forms.Button();
            this.btnOutIO03 = new System.Windows.Forms.Button();
            this.btnOutIO04 = new System.Windows.Forms.Button();
            this.inOutTotalControlIn = new DIT.TLC.UI.Moter_In_Out.UcrlInOutTotalControl();
            this.inOutTotalControlOut = new DIT.TLC.UI.Moter_In_Out.UcrlInOutTotalControl();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorLoaderIO);
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorProcessIO);
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorUnloaderIO);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1740, 47);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btnMonitorLoaderIO
            // 
            this.btnMonitorLoaderIO.Location = new System.Drawing.Point(3, 3);
            this.btnMonitorLoaderIO.Name = "btnMonitorLoaderIO";
            this.btnMonitorLoaderIO.Size = new System.Drawing.Size(573, 41);
            this.btnMonitorLoaderIO.TabIndex = 0;
            this.btnMonitorLoaderIO.Text = "로더";
            this.btnMonitorLoaderIO.UseVisualStyleBackColor = true;
            this.btnMonitorLoaderIO.Click += new System.EventHandler(this.btnMonitorIO_Click);
            // 
            // btnMonitorProcessIO
            // 
            this.btnMonitorProcessIO.Location = new System.Drawing.Point(582, 3);
            this.btnMonitorProcessIO.Name = "btnMonitorProcessIO";
            this.btnMonitorProcessIO.Size = new System.Drawing.Size(573, 41);
            this.btnMonitorProcessIO.TabIndex = 1;
            this.btnMonitorProcessIO.Text = "프로세스";
            this.btnMonitorProcessIO.UseVisualStyleBackColor = true;
            this.btnMonitorProcessIO.Click += new System.EventHandler(this.btnMonitorIO_Click);
            // 
            // btnMonitorUnloaderIO
            // 
            this.btnMonitorUnloaderIO.Location = new System.Drawing.Point(1161, 3);
            this.btnMonitorUnloaderIO.Name = "btnMonitorUnloaderIO";
            this.btnMonitorUnloaderIO.Size = new System.Drawing.Size(573, 41);
            this.btnMonitorUnloaderIO.TabIndex = 2;
            this.btnMonitorUnloaderIO.Text = "언로드";
            this.btnMonitorUnloaderIO.UseVisualStyleBackColor = true;
            this.btnMonitorUnloaderIO.Click += new System.EventHandler(this.btnMonitorIO_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 47);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.inOutTotalControlIn);
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.inOutTotalControlOut);
            this.splitContainer1.Panel2.Controls.Add(this.flowLayoutPanel3);
            this.splitContainer1.Size = new System.Drawing.Size(1740, 813);
            this.splitContainer1.SplitterDistance = 900;
            this.splitContainer1.TabIndex = 3;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.btnInIO01);
            this.flowLayoutPanel2.Controls.Add(this.btnInIO02);
            this.flowLayoutPanel2.Controls.Add(this.btnInIO03);
            this.flowLayoutPanel2.Controls.Add(this.btnInIO04);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(900, 43);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // btnInIO01
            // 
            this.btnInIO01.Location = new System.Drawing.Point(3, 3);
            this.btnInIO01.Name = "btnInIO01";
            this.btnInIO01.Size = new System.Drawing.Size(218, 35);
            this.btnInIO01.TabIndex = 1;
            this.btnInIO01.Text = "IN-1";
            this.btnInIO01.UseVisualStyleBackColor = true;
            this.btnInIO01.Click += new System.EventHandler(this.btnInIO01_Click);
            // 
            // btnInIO02
            // 
            this.btnInIO02.Location = new System.Drawing.Point(227, 3);
            this.btnInIO02.Name = "btnInIO02";
            this.btnInIO02.Size = new System.Drawing.Size(218, 35);
            this.btnInIO02.TabIndex = 2;
            this.btnInIO02.Text = "IN-2";
            this.btnInIO02.UseVisualStyleBackColor = true;
            this.btnInIO02.Click += new System.EventHandler(this.btnInIO01_Click);
            // 
            // btnInIO03
            // 
            this.btnInIO03.Location = new System.Drawing.Point(451, 3);
            this.btnInIO03.Name = "btnInIO03";
            this.btnInIO03.Size = new System.Drawing.Size(218, 35);
            this.btnInIO03.TabIndex = 3;
            this.btnInIO03.Text = "IN-3";
            this.btnInIO03.UseVisualStyleBackColor = true;
            this.btnInIO03.Click += new System.EventHandler(this.btnInIO01_Click);
            // 
            // btnInIO04
            // 
            this.btnInIO04.Location = new System.Drawing.Point(675, 3);
            this.btnInIO04.Name = "btnInIO04";
            this.btnInIO04.Size = new System.Drawing.Size(218, 35);
            this.btnInIO04.TabIndex = 4;
            this.btnInIO04.Text = "IN-4";
            this.btnInIO04.UseVisualStyleBackColor = true;
            this.btnInIO04.Click += new System.EventHandler(this.btnInIO01_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.btnOutIO01);
            this.flowLayoutPanel3.Controls.Add(this.btnOutIO02);
            this.flowLayoutPanel3.Controls.Add(this.btnOutIO03);
            this.flowLayoutPanel3.Controls.Add(this.btnOutIO04);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(836, 44);
            this.flowLayoutPanel3.TabIndex = 1;
            // 
            // btnOutIO01
            // 
            this.btnOutIO01.Location = new System.Drawing.Point(3, 3);
            this.btnOutIO01.Name = "btnOutIO01";
            this.btnOutIO01.Size = new System.Drawing.Size(202, 35);
            this.btnOutIO01.TabIndex = 4;
            this.btnOutIO01.Text = "OUT-1";
            this.btnOutIO01.UseVisualStyleBackColor = true;
            this.btnOutIO01.Click += new System.EventHandler(this.btnOutIO01_Click);
            // 
            // btnOutIO02
            // 
            this.btnOutIO02.Location = new System.Drawing.Point(211, 3);
            this.btnOutIO02.Name = "btnOutIO02";
            this.btnOutIO02.Size = new System.Drawing.Size(202, 35);
            this.btnOutIO02.TabIndex = 5;
            this.btnOutIO02.Text = "OUT-2";
            this.btnOutIO02.UseVisualStyleBackColor = true;
            this.btnOutIO02.Click += new System.EventHandler(this.btnOutIO01_Click);
            // 
            // btnOutIO03
            // 
            this.btnOutIO03.Location = new System.Drawing.Point(419, 3);
            this.btnOutIO03.Name = "btnOutIO03";
            this.btnOutIO03.Size = new System.Drawing.Size(202, 35);
            this.btnOutIO03.TabIndex = 6;
            this.btnOutIO03.Text = "OUT-3";
            this.btnOutIO03.UseVisualStyleBackColor = true;
            this.btnOutIO03.Click += new System.EventHandler(this.btnOutIO01_Click);
            // 
            // btnOutIO04
            // 
            this.btnOutIO04.Location = new System.Drawing.Point(627, 3);
            this.btnOutIO04.Name = "btnOutIO04";
            this.btnOutIO04.Size = new System.Drawing.Size(202, 35);
            this.btnOutIO04.TabIndex = 7;
            this.btnOutIO04.Text = "OUT-4";
            this.btnOutIO04.UseVisualStyleBackColor = true;
            this.btnOutIO04.Click += new System.EventHandler(this.btnOutIO01_Click);
            // 
            // inOutTotalControlIn
            // 
            this.inOutTotalControlIn.BackColor = System.Drawing.SystemColors.Control;
            this.inOutTotalControlIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.inOutTotalControlIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inOutTotalControlIn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.inOutTotalControlIn.Location = new System.Drawing.Point(0, 43);
            this.inOutTotalControlIn.Name = "inOutTotalControlIn";
            this.inOutTotalControlIn.Size = new System.Drawing.Size(900, 770);
            this.inOutTotalControlIn.TabIndex = 2;
            // 
            // inOutTotalControlOut
            // 
            this.inOutTotalControlOut.BackColor = System.Drawing.SystemColors.Control;
            this.inOutTotalControlOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.inOutTotalControlOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inOutTotalControlOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.inOutTotalControlOut.Location = new System.Drawing.Point(0, 44);
            this.inOutTotalControlOut.Name = "inOutTotalControlOut";
            this.inOutTotalControlOut.Size = new System.Drawing.Size(836, 769);
            this.inOutTotalControlOut.TabIndex = 3;
            // 
            // UcrlManagerIOStatus
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.Name = "UcrlManagerIOStatus";
            this.Size = new System.Drawing.Size(1740, 860);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnMonitorLoaderIO;
        private System.Windows.Forms.Button btnMonitorProcessIO;
        private System.Windows.Forms.Button btnMonitorUnloaderIO;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Moter_In_Out.UcrlInOutTotalControl inOutTotalControlIn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private Moter_In_Out.UcrlInOutTotalControl inOutTotalControlOut;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button btnInIO01;
        private System.Windows.Forms.Button btnInIO02;
        private System.Windows.Forms.Button btnInIO03;
        private System.Windows.Forms.Button btnOutIO01;
        private System.Windows.Forms.Button btnOutIO02;
        private System.Windows.Forms.Button btnOutIO03;
        private System.Windows.Forms.Button btnInIO04;
        private System.Windows.Forms.Button btnOutIO04;
    }
}

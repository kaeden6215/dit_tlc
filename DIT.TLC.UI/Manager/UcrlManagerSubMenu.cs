﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;


namespace DIT.TLC.UI
{
    public partial class UcrlManagerSubMenu : UserControl, IUIUpdate
    {
        private UcrlManagerManualCtrl _urcrMgrSevoManualCtrl = new UcrlManagerManualCtrl();    // 메뉴얼제어 UI

        private UcrlManagerReset _ucrlManagerReset = new UcrlManagerReset();                  // 레시피-장치초기화 UI
        private UcrlManagerIOStatus _ucrlManagerIoform = new UcrlManagerIOStatus();               // 레시피-IO Status UI
        private UcrlManagerTactTime _ucrlManagerTacttime = new UcrlManagerTactTime();             // tacttime UI

        private UcrlManagerControl manager_control = new UcrlManagerControl();
        private UcrlManagerLaserWindows _ucrlManagerLaser = new UcrlManagerLaserWindows();
        private UcrlManagerSEM _ucrlManagerSem = new UcrlManagerSEM();
        private UcrlManagerAIOStatus _ucrlManagerAIO = new UcrlManagerAIOStatus();               // 레시피-AIO Status UI
        private UcrlManagerMotorStatus _ucrlManagerMoter = new UcrlManagerMotorStatus();               // 레시피-AIO Status UI
        //private Ucrl manager_ioform = new Manager_IOStatus();

        public UcrlManagerSubMenu()
        {
            InitializeComponent();

            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerReset);

            _ucrlManagerReset.Location = new Point(0, 0);
        }

        public void InitalizeUserControlUI(Equipment equip)
        {
            _urcrMgrSevoManualCtrl.InitalizeUserControlUI(equip);
        }

        // 서브메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void SubMenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = UiGlobal.UNSELECT_C;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = UiGlobal.SELECT_C;
        }

        // 장치 초기화 버튼 클릭
        private void btn_reset_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerReset);
            _ucrlManagerReset.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        // IO Status 버튼 클릭
        private void btn_io_status_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerIoform);
            _ucrlManagerIoform.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        // AIO Status 버튼 클릭
        private void btn_aio_status_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerAIO);
            _ucrlManagerAIO.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        // Motor Status 버튼 클릭
        private void btn_motor_status_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerMoter);
            _ucrlManagerMoter.Location = new Point(0, 0);            
            SubMenuButtonColorChange(sender, e);
        }

        // 메뉴얼 제어 버튼 클릭
        private void btn_manual_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_urcrMgrSevoManualCtrl);

            SubMenuButtonColorChange(sender, e);
        }

        // SEM 버튼 클릭
        private void btn_sem_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerSem);
            _ucrlManagerSem.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        // TactTime 버튼 클릭
        private void btn_tactime_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerTacttime);

            SubMenuButtonColorChange(sender, e);
        }

        // 레이저 제어 버튼 클릭
        private void btn_laser_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlManagerLaser);

            SubMenuButtonColorChange(sender, e);
        }

        // Control 버튼 클릭
        private void btn_control_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(manager_control);

            SubMenuButtonColorChange(sender, e);
        }

        // Process 버튼 클릭
        private void btn_process_Click(object sender, EventArgs e)
        {
            new FrmManager_Process().ShowDialog();

            SubMenuButtonColorChange(sender, e);
        }

        // CutLineTracker 버튼 클릭
        private void btn_cutlinetracker_Click(object sender, EventArgs e)
        {
            new FrmCutLineTracker().ShowDialog();

            SubMenuButtonColorChange(sender, e);
        }

        public void UIUpdate()
        {
            foreach (var ctrl in splitContainer1.Panel1.Controls)
            {
                IUIUpdate vv = ctrl as IUIUpdate;
                if (vv != null)
                    vv.UIUpdate();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dit.Framework.Comm;
using Dit.Framework.PLC;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerAIOStatus : UserControl, IUIUpdate
    {
        private List<Label> lstlblLDAnalog = new List<Label>();
        private List<TextBox> lstTxtLDAnalog = new List<TextBox>();

        private List<Label> lstlblPROCAnalog = new List<Label>();
        private List<TextBox> lstTxtPROCAnalog = new List<TextBox>();

        private List<Label> lstlblUDAnalog = new List<Label>();
        private List<TextBox> lstTxtUDAnalog = new List<TextBox>();

        private List<PlcAddr> lstLDAnalog = new List<PlcAddr>();
        private List<PlcAddr> lstPROCAnalog = new List<PlcAddr>();
        private List<PlcAddr> lstUDAnalog = new List<PlcAddr>();


        private string LD = "JR00.00,JR00.01,JR00.02,JR00.03,JR00.04,JR00.05,JR00.06,JR00.07,JR00.08,JR00.09,JR00.10,JR00.11,JR00.12,JR00.13,JR00.14,JR00.15";
        private string PROC = "";
        private string UD = "JO1801,JO1802,JO1803,JO1804,JO1805,JO1806,JO1807,JO1808,JO1809,JO1810,JO1811,JO1812,JO1813,JO1814,JO1815,JO1801,JO1802";

        public UcrlManagerAIOStatus()
        {
            InitializeComponent();

            #region 리스트 등록

            //Lable
            lstlblLDAnalog.Add(lblLdAnalog01);
            lstlblLDAnalog.Add(lblLdAnalog02);
            lstlblLDAnalog.Add(lblLdAnalog03);
            lstlblLDAnalog.Add(lblLdAnalog04);
            lstlblLDAnalog.Add(lblLdAnalog05);
            lstlblLDAnalog.Add(lblLdAnalog06);
            lstlblLDAnalog.Add(lblLdAnalog07);
            lstlblLDAnalog.Add(lblLdAnalog08);
            lstlblLDAnalog.Add(lblLdAnalog09);
            lstlblLDAnalog.Add(lblLdAnalog10);
            lstlblLDAnalog.Add(lblLdAnalog11);
            lstlblLDAnalog.Add(lblLdAnalog12);
            lstlblLDAnalog.Add(lblLdAnalog13);
            lstlblLDAnalog.Add(lblLdAnalog14);
            lstlblLDAnalog.Add(lblLdAnalog15);
            lstlblLDAnalog.Add(lblLdAnalog16);
            lstlblLDAnalog.Add(lblLdAnalog17);
            lstlblLDAnalog.Add(lblLdAnalog18);
            lstlblLDAnalog.Add(lblLdAnalog19);
            lstlblLDAnalog.Add(lblLdAnalog20);
            lstlblLDAnalog.Add(lblLdAnalog21);
            lstlblLDAnalog.Add(lblLdAnalog22);
            lstlblLDAnalog.Add(lblLdAnalog23);
            lstlblLDAnalog.Add(lblLdAnalog24);
            lstlblLDAnalog.Add(lblLdAnalog25);
            lstlblLDAnalog.Add(lblLdAnalog26);
            lstlblLDAnalog.Add(lblLdAnalog27);
            lstlblLDAnalog.Add(lblLdAnalog28);
            lstlblLDAnalog.Add(lblLdAnalog29);
            lstlblLDAnalog.Add(lblLdAnalog30);
            lstlblLDAnalog.Add(lblLdAnalog31);
            lstlblLDAnalog.Add(lblLdAnalog32);

            //Text
            lstTxtLDAnalog.Add(txtAnalogValue01);
            lstTxtLDAnalog.Add(txtAnalogValue02);
            lstTxtLDAnalog.Add(txtAnalogValue03);
            lstTxtLDAnalog.Add(txtAnalogValue04);
            lstTxtLDAnalog.Add(txtAnalogValue05);
            lstTxtLDAnalog.Add(txtAnalogValue06);
            lstTxtLDAnalog.Add(txtAnalogValue07);
            lstTxtLDAnalog.Add(txtAnalogValue08);
            lstTxtLDAnalog.Add(txtAnalogValue09);
            lstTxtLDAnalog.Add(txtAnalogValue10);
            lstTxtLDAnalog.Add(txtAnalogValue11);
            lstTxtLDAnalog.Add(txtAnalogValue12);
            lstTxtLDAnalog.Add(txtAnalogValue13);
            lstTxtLDAnalog.Add(txtAnalogValue14);
            lstTxtLDAnalog.Add(txtAnalogValue15);
            lstTxtLDAnalog.Add(txtAnalogValue16);
            lstTxtLDAnalog.Add(txtAnalogValue17);
            lstTxtLDAnalog.Add(txtAnalogValue18);
            lstTxtLDAnalog.Add(txtAnalogValue19);
            lstTxtLDAnalog.Add(txtAnalogValue20);
            lstTxtLDAnalog.Add(txtAnalogValue21);
            lstTxtLDAnalog.Add(txtAnalogValue22);
            lstTxtLDAnalog.Add(txtAnalogValue23);
            lstTxtLDAnalog.Add(txtAnalogValue24);
            lstTxtLDAnalog.Add(txtAnalogValue25);
            lstTxtLDAnalog.Add(txtAnalogValue26);
            lstTxtLDAnalog.Add(txtAnalogValue27);
            lstTxtLDAnalog.Add(txtAnalogValue28);
            lstTxtLDAnalog.Add(txtAnalogValue29);
            lstTxtLDAnalog.Add(txtAnalogValue30);
            lstTxtLDAnalog.Add(txtAnalogValue31);
            lstTxtLDAnalog.Add(txtAnalogValue32);

            lstlblPROCAnalog.Add(lblProcAnalog01);
            lstlblPROCAnalog.Add(lblProcAnalog02);

            lstTxtPROCAnalog.Add(txtProcAnalogValue01);
            lstTxtPROCAnalog.Add(txtProcAnalogValue02);

            lstlblUDAnalog.Add(lblUdAnalog01);
            lstlblUDAnalog.Add(lblUdAnalog02);
            lstlblUDAnalog.Add(lblUdAnalog03);
            lstlblUDAnalog.Add(lblUdAnalog04);
            lstlblUDAnalog.Add(lblUdAnalog05);
            lstlblUDAnalog.Add(lblUdAnalog06);
            lstlblUDAnalog.Add(lblUdAnalog07);
            lstlblUDAnalog.Add(lblUdAnalog08);
            lstlblUDAnalog.Add(lblUdAnalog09);
            lstlblUDAnalog.Add(lblUdAnalog10);
            lstlblUDAnalog.Add(lblUdAnalog11);
            lstlblUDAnalog.Add(lblUdAnalog12);
            lstlblUDAnalog.Add(lblUdAnalog13);
            lstlblUDAnalog.Add(lblUdAnalog14);
            lstlblUDAnalog.Add(lblUdAnalog15);
            lstlblUDAnalog.Add(lblUdAnalog16);

            lstTxtUDAnalog.Add(txtUdAnalogValue01);
            lstTxtUDAnalog.Add(txtUdAnalogValue02);
            lstTxtUDAnalog.Add(txtUdAnalogValue03);
            lstTxtUDAnalog.Add(txtUdAnalogValue04);
            lstTxtUDAnalog.Add(txtUdAnalogValue05);
            lstTxtUDAnalog.Add(txtUdAnalogValue06);
            lstTxtUDAnalog.Add(txtUdAnalogValue07);
            lstTxtUDAnalog.Add(txtUdAnalogValue08);
            lstTxtUDAnalog.Add(txtUdAnalogValue09);
            lstTxtUDAnalog.Add(txtUdAnalogValue10);
            lstTxtUDAnalog.Add(txtUdAnalogValue11);
            lstTxtUDAnalog.Add(txtUdAnalogValue12);
            lstTxtUDAnalog.Add(txtUdAnalogValue13);
            lstTxtUDAnalog.Add(txtUdAnalogValue14);
            lstTxtUDAnalog.Add(txtUdAnalogValue15);
            lstTxtUDAnalog.Add(txtUdAnalogValue16);
            #endregion

        }

        private bool _isFirst = true;
        public void UIUpdate()
        {

            if (_isFirst)
            {
                _isFirst = false;


                foreach (string str in LD.Split(','))
                {
                    if (string.IsNullOrEmpty(str)) continue;
                    PlcAddr addrAnalog = AddressMgr.GetAddress(str, (short)0);
                    if (addrAnalog == null) continue;
                    lstLDAnalog.Add(addrAnalog);
                }

                foreach (string str in PROC.Split(','))
                {
                    if (string.IsNullOrEmpty(str)) continue;
                    PlcAddr addrAnalog = AddressMgr.GetAddress(str, (short)0);
                    if (addrAnalog == null) continue;
                    lstLDAnalog.Add(addrAnalog);
                }

                foreach (string str in UD.Split(','))
                {
                    if (string.IsNullOrEmpty(str)) continue;
                    PlcAddr addrAnalog = AddressMgr.GetAddress(str, (short)0);
                    if (addrAnalog == null) continue;
                    lstLDAnalog.Add(addrAnalog);
                }

                for (int iPos = 0; iPos < lstLDAnalog.Count; iPos++)
                    lstlblLDAnalog[iPos].Text = lstLDAnalog[iPos].Desc;

                for (int iPos = 0; iPos < lstPROCAnalog.Count; iPos++)
                    lstlblPROCAnalog[iPos].Text = lstPROCAnalog[iPos].Desc;

                for (int iPos = 0; iPos < lstUDAnalog.Count; iPos++)
                    lstlblUDAnalog[iPos].Text = lstUDAnalog[iPos].Desc;

            }

            for (int iPos = 0; iPos < lstLDAnalog.Count; iPos++)
                lstTxtLDAnalog[iPos].Text = new ConvertA2D() { XF_Value = lstLDAnalog[iPos] }.Value.ToString();

            //for (int iPos = 0; iPos < lstPROCAnalog.Count; iPos++)
            //    lstTxtPROCAnalog[iPos].BackColor = lstPROCAnalog[iPos].vBit ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            //for (int iPos = 0; iPos < lstUDAnalog.Count; iPos++)
            //    lstTxtUDAnalog[iPos].BackColor = lstUDAnalog[iPos].vBit ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }
    }
}

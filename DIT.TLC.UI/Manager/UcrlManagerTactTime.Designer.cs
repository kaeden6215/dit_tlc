﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerTactTime
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMonitorLoaderIO = new System.Windows.Forms.Button();
            this.btnMonitorProcessIO = new System.Windows.Forms.Button();
            this.btnMonitorUnloaderIO = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.listTactView10 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView09 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView08 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView07 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView06 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView05 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView04 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView03 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView02 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView01 = new DIT.TLC.UI.UctrlLstTactView();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorLoaderIO);
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorProcessIO);
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorUnloaderIO);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1740, 59);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btnMonitorLoaderIO
            // 
            this.btnMonitorLoaderIO.Location = new System.Drawing.Point(3, 4);
            this.btnMonitorLoaderIO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMonitorLoaderIO.Name = "btnMonitorLoaderIO";
            this.btnMonitorLoaderIO.Size = new System.Drawing.Size(574, 51);
            this.btnMonitorLoaderIO.TabIndex = 0;
            this.btnMonitorLoaderIO.Text = "로더";
            this.btnMonitorLoaderIO.UseVisualStyleBackColor = true;
            this.btnMonitorLoaderIO.Click += new System.EventHandler(this.btnMonitorLoaderIO_Click);
            // 
            // btnMonitorProcessIO
            // 
            this.btnMonitorProcessIO.Location = new System.Drawing.Point(583, 4);
            this.btnMonitorProcessIO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMonitorProcessIO.Name = "btnMonitorProcessIO";
            this.btnMonitorProcessIO.Size = new System.Drawing.Size(574, 51);
            this.btnMonitorProcessIO.TabIndex = 1;
            this.btnMonitorProcessIO.Text = "프로세스";
            this.btnMonitorProcessIO.UseVisualStyleBackColor = true;
            this.btnMonitorProcessIO.Click += new System.EventHandler(this.btnMonitorProcessIO_Click);
            // 
            // btnMonitorUnloaderIO
            // 
            this.btnMonitorUnloaderIO.Location = new System.Drawing.Point(1163, 4);
            this.btnMonitorUnloaderIO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMonitorUnloaderIO.Name = "btnMonitorUnloaderIO";
            this.btnMonitorUnloaderIO.Size = new System.Drawing.Size(574, 51);
            this.btnMonitorUnloaderIO.TabIndex = 2;
            this.btnMonitorUnloaderIO.Text = "언로드";
            this.btnMonitorUnloaderIO.UseVisualStyleBackColor = true;
            this.btnMonitorUnloaderIO.Click += new System.EventHandler(this.btnMonitorUnloaderIO_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.listTactView01);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Location = new System.Drawing.Point(10, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(330, 395);
            this.panel1.TabIndex = 457;
            // 
            // label21
            // 
            this.label21.AutoEllipsis = true;
            this.label21.BackColor = System.Drawing.Color.Gainsboro;
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(328, 24);
            this.label21.TabIndex = 9;
            this.label21.Text = "■ RIGHT 컷팅 테이블";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.listTactView02);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(356, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(330, 395);
            this.panel2.TabIndex = 458;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(328, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ LEFT 컷팅 테이블";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.listTactView03);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(702, 64);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(330, 395);
            this.panel3.TabIndex = 459;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(328, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ 얼라인 먼트";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.listTactView04);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(1048, 64);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(330, 395);
            this.panel4.TabIndex = 460;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(328, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "■ 브레이킹 이재기";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.listTactView05);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Location = new System.Drawing.Point(1393, 64);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(330, 395);
            this.panel5.TabIndex = 461;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(328, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "■ 브레이킹 RIGHT 얼라인먼트";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.listTactView06);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Location = new System.Drawing.Point(9, 465);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(330, 395);
            this.panel6.TabIndex = 462;
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(328, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "■ 브레이킹 LEFT 얼라인먼트";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.listTactView07);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Location = new System.Drawing.Point(356, 465);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(330, 395);
            this.panel7.TabIndex = 463;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(328, 24);
            this.label6.TabIndex = 9;
            this.label6.Text = "■ 브레이킹 테이블 R2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.listTactView08);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Location = new System.Drawing.Point(702, 465);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(330, 395);
            this.panel8.TabIndex = 464;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(328, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "■ 브레이킹 테이블 R1";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.listTactView09);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Location = new System.Drawing.Point(1048, 465);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(330, 395);
            this.panel9.TabIndex = 465;
            // 
            // label8
            // 
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(328, 24);
            this.label8.TabIndex = 9;
            this.label8.Text = "■ 브레이킹 테이블 L2";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.listTactView10);
            this.panel10.Controls.Add(this.label9);
            this.panel10.Location = new System.Drawing.Point(1393, 465);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(330, 395);
            this.panel10.TabIndex = 466;
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.BackColor = System.Drawing.Color.Gainsboro;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(328, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "■ 브레이킹 테이블 L1";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listTactView10
            // 
            this.listTactView10.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView10.Location = new System.Drawing.Point(3, 28);
            this.listTactView10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView10.Name = "listTactView10";
            this.listTactView10.Size = new System.Drawing.Size(322, 361);
            this.listTactView10.TabIndex = 10;
            this.listTactView10.Unit = null;
            // 
            // listTactView09
            // 
            this.listTactView09.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView09.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView09.Location = new System.Drawing.Point(3, 28);
            this.listTactView09.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView09.Name = "listTactView09";
            this.listTactView09.Size = new System.Drawing.Size(322, 361);
            this.listTactView09.TabIndex = 10;
            this.listTactView09.Unit = null;
            // 
            // listTactView08
            // 
            this.listTactView08.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView08.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView08.Location = new System.Drawing.Point(3, 28);
            this.listTactView08.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView08.Name = "listTactView08";
            this.listTactView08.Size = new System.Drawing.Size(322, 361);
            this.listTactView08.TabIndex = 10;
            this.listTactView08.Unit = null;
            // 
            // listTactView07
            // 
            this.listTactView07.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView07.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView07.Location = new System.Drawing.Point(3, 28);
            this.listTactView07.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView07.Name = "listTactView07";
            this.listTactView07.Size = new System.Drawing.Size(322, 361);
            this.listTactView07.TabIndex = 10;
            this.listTactView07.Unit = null;
            // 
            // listTactView06
            // 
            this.listTactView06.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView06.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView06.Location = new System.Drawing.Point(3, 28);
            this.listTactView06.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView06.Name = "listTactView06";
            this.listTactView06.Size = new System.Drawing.Size(322, 361);
            this.listTactView06.TabIndex = 10;
            this.listTactView06.Unit = null;
            // 
            // listTactView05
            // 
            this.listTactView05.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView05.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView05.Location = new System.Drawing.Point(3, 28);
            this.listTactView05.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView05.Name = "listTactView05";
            this.listTactView05.Size = new System.Drawing.Size(322, 361);
            this.listTactView05.TabIndex = 10;
            this.listTactView05.Unit = null;
            // 
            // listTactView04
            // 
            this.listTactView04.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView04.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView04.Location = new System.Drawing.Point(3, 28);
            this.listTactView04.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView04.Name = "listTactView04";
            this.listTactView04.Size = new System.Drawing.Size(322, 361);
            this.listTactView04.TabIndex = 10;
            this.listTactView04.Unit = null;
            // 
            // listTactView03
            // 
            this.listTactView03.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView03.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView03.Location = new System.Drawing.Point(3, 28);
            this.listTactView03.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView03.Name = "listTactView03";
            this.listTactView03.Size = new System.Drawing.Size(322, 361);
            this.listTactView03.TabIndex = 10;
            this.listTactView03.Unit = null;
            // 
            // listTactView02
            // 
            this.listTactView02.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView02.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView02.Location = new System.Drawing.Point(3, 28);
            this.listTactView02.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView02.Name = "listTactView02";
            this.listTactView02.Size = new System.Drawing.Size(322, 361);
            this.listTactView02.TabIndex = 10;
            this.listTactView02.Unit = null;
            // 
            // listTactView01
            // 
            this.listTactView01.BackColor = System.Drawing.SystemColors.Control;
            this.listTactView01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView01.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listTactView01.Location = new System.Drawing.Point(3, 28);
            this.listTactView01.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listTactView01.Name = "listTactView01";
            this.listTactView01.Size = new System.Drawing.Size(322, 361);
            this.listTactView01.TabIndex = 10;
            this.listTactView01.Unit = null;
            // 
            // UcrlManagerTactTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UcrlManagerTactTime";
            this.Size = new System.Drawing.Size(1740, 868);
            this.Load += new System.EventHandler(this.UcrlManagerTactTime_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnMonitorLoaderIO;
        private System.Windows.Forms.Button btnMonitorProcessIO;
        private System.Windows.Forms.Button btnMonitorUnloaderIO;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label label21;
        private UctrlLstTactView listTactView01;
        private System.Windows.Forms.Panel panel2;
        private UctrlLstTactView listTactView02;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private UctrlLstTactView listTactView03;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private UctrlLstTactView listTactView04;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private UctrlLstTactView listTactView05;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private UctrlLstTactView listTactView06;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel7;
        private UctrlLstTactView listTactView07;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private UctrlLstTactView listTactView08;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel9;
        private UctrlLstTactView listTactView09;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel10;
        private UctrlLstTactView listTactView10;
        internal System.Windows.Forms.Label label9;
    }
}

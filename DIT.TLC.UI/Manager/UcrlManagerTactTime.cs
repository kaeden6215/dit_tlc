﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerTactTime : UserControl, IUIUpdate
    {
        public UcrlManagerTactTime()
        {
            InitializeComponent();
        }

        private void btnMonitorLoaderIO_Click(object sender, EventArgs e)
        {
            listTactView01.Unit = GG.Equip.LD.CstLoader_B;
            listTactView02.Unit = GG.Equip.LD.CstLoader_A;
            listTactView03.Unit = GG.Equip.LD.Loader;
            listTactView04.Unit = GG.Equip.LD.LoaderTransfer_B;
            listTactView05.Unit = GG.Equip.LD.LoaderTransfer_A;
            listTactView06.Unit = null;
            listTactView07.Unit = null;
            listTactView08.Unit = null;
            listTactView09.Unit = null;
            listTactView10.Unit = null;

        }

        private void btnMonitorUnloaderIO_Click(object sender, EventArgs e)
        {
            listTactView01.Unit = GG.Equip.LD.CstLoader_B;
            listTactView02.Unit = GG.Equip.LD.CstLoader_A;
            listTactView03.Unit = GG.Equip.LD.Loader;
            listTactView04.Unit = GG.Equip.LD.LoaderTransfer_B;
            listTactView05.Unit = GG.Equip.LD.LoaderTransfer_A;
            listTactView06.Unit = null;
            listTactView07.Unit = null;
            listTactView08.Unit = null;
            listTactView09.Unit = null;
            listTactView10.Unit = null;
        }
        private void btnMonitorProcessIO_Click(object sender, EventArgs e)
        {
            listTactView01.Unit = GG.Equip.PROC.IRCutStage_B;
            listTactView02.Unit = GG.Equip.PROC.IRCutStage_A;
            listTactView03.Unit = GG.Equip.PROC.BreakAlign;
            listTactView04.Unit = GG.Equip.PROC.AfterIRCutTransfer_B;
            listTactView05.Unit = GG.Equip.PROC.AfterIRCutTransfer_A;
            listTactView06.Unit = GG.Equip.PROC.BreakAlign_B;
            listTactView07.Unit = GG.Equip.PROC.BreakAlign_A;
            listTactView08.Unit = GG.Equip.PROC.BreakStage_B;
            listTactView09.Unit = GG.Equip.PROC.BreakStage_A;
            listTactView10.Unit = null;
        }

        public void UpdateUI()
        {
            listTactView01.UpdateUI();
            listTactView02.UpdateUI();
            listTactView03.UpdateUI();
            listTactView04.UpdateUI();
            listTactView05.UpdateUI();
            listTactView06.UpdateUI();
            listTactView07.UpdateUI();
            listTactView08.UpdateUI();
            listTactView09.UpdateUI();
            listTactView10.UpdateUI();
        }

        public void UIUpdate()
        { 
        }

        private void UcrlManagerTactTime_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerSubMenu
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_cutlinetracker = new System.Windows.Forms.Button();
            this.btn_process = new System.Windows.Forms.Button();
            this.btn_control = new System.Windows.Forms.Button();
            this.btn_laser = new System.Windows.Forms.Button();
            this.btn_tactime = new System.Windows.Forms.Button();
            this.btn_sem = new System.Windows.Forms.Button();
            this.btn_manual = new System.Windows.Forms.Button();
            this.btn_motor_status = new System.Windows.Forms.Button();
            this.btn_aio_status = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_io_status = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.btn_cutlinetracker);
            this.splitContainer1.Panel2.Controls.Add(this.btn_process);
            this.splitContainer1.Panel2.Controls.Add(this.btn_control);
            this.splitContainer1.Panel2.Controls.Add(this.btn_laser);
            this.splitContainer1.Panel2.Controls.Add(this.btn_tactime);
            this.splitContainer1.Panel2.Controls.Add(this.btn_sem);
            this.splitContainer1.Panel2.Controls.Add(this.btn_manual);
            this.splitContainer1.Panel2.Controls.Add(this.btn_motor_status);
            this.splitContainer1.Panel2.Controls.Add(this.btn_aio_status);
            this.splitContainer1.Panel2.Controls.Add(this.btn_reset);
            this.splitContainer1.Panel2.Controls.Add(this.btn_io_status);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 0;
            // 
            // btn_cutlinetracker
            // 
            this.btn_cutlinetracker.BackColor = System.Drawing.SystemColors.Control;
            this.btn_cutlinetracker.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_cutlinetracker.ForeColor = System.Drawing.Color.Black;
            this.btn_cutlinetracker.Location = new System.Drawing.Point(2, 617);
            this.btn_cutlinetracker.Name = "btn_cutlinetracker";
            this.btn_cutlinetracker.Size = new System.Drawing.Size(132, 50);
            this.btn_cutlinetracker.TabIndex = 50;
            this.btn_cutlinetracker.Text = "CutLineTracker";
            this.btn_cutlinetracker.UseVisualStyleBackColor = false;
            this.btn_cutlinetracker.Click += new System.EventHandler(this.btn_cutlinetracker_Click);
            // 
            // btn_process
            // 
            this.btn_process.BackColor = System.Drawing.SystemColors.Control;
            this.btn_process.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_process.ForeColor = System.Drawing.Color.Black;
            this.btn_process.Location = new System.Drawing.Point(2, 561);
            this.btn_process.Name = "btn_process";
            this.btn_process.Size = new System.Drawing.Size(132, 50);
            this.btn_process.TabIndex = 49;
            this.btn_process.Text = "Process";
            this.btn_process.UseVisualStyleBackColor = false;
            this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
            // 
            // btn_control
            // 
            this.btn_control.BackColor = System.Drawing.SystemColors.Control;
            this.btn_control.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_control.ForeColor = System.Drawing.Color.Black;
            this.btn_control.Location = new System.Drawing.Point(2, 505);
            this.btn_control.Name = "btn_control";
            this.btn_control.Size = new System.Drawing.Size(132, 50);
            this.btn_control.TabIndex = 48;
            this.btn_control.Text = "Control";
            this.btn_control.UseVisualStyleBackColor = false;
            this.btn_control.Click += new System.EventHandler(this.btn_control_Click);
            // 
            // btn_laser
            // 
            this.btn_laser.BackColor = System.Drawing.SystemColors.Control;
            this.btn_laser.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_laser.ForeColor = System.Drawing.Color.Black;
            this.btn_laser.Location = new System.Drawing.Point(2, 449);
            this.btn_laser.Name = "btn_laser";
            this.btn_laser.Size = new System.Drawing.Size(132, 50);
            this.btn_laser.TabIndex = 47;
            this.btn_laser.Text = "레이저 제어";
            this.btn_laser.UseVisualStyleBackColor = false;
            this.btn_laser.Click += new System.EventHandler(this.btn_laser_Click);
            // 
            // btn_tactime
            // 
            this.btn_tactime.BackColor = System.Drawing.SystemColors.Control;
            this.btn_tactime.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_tactime.ForeColor = System.Drawing.Color.Black;
            this.btn_tactime.Location = new System.Drawing.Point(2, 393);
            this.btn_tactime.Name = "btn_tactime";
            this.btn_tactime.Size = new System.Drawing.Size(132, 50);
            this.btn_tactime.TabIndex = 46;
            this.btn_tactime.Text = "TactTime";
            this.btn_tactime.UseVisualStyleBackColor = false;
            this.btn_tactime.Click += new System.EventHandler(this.btn_tactime_Click);
            // 
            // btn_sem
            // 
            this.btn_sem.BackColor = System.Drawing.SystemColors.Control;
            this.btn_sem.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_sem.ForeColor = System.Drawing.Color.Black;
            this.btn_sem.Location = new System.Drawing.Point(2, 337);
            this.btn_sem.Name = "btn_sem";
            this.btn_sem.Size = new System.Drawing.Size(132, 50);
            this.btn_sem.TabIndex = 45;
            this.btn_sem.Text = "SEM";
            this.btn_sem.UseVisualStyleBackColor = false;
            this.btn_sem.Click += new System.EventHandler(this.btn_sem_Click);
            // 
            // btn_manual
            // 
            this.btn_manual.BackColor = System.Drawing.SystemColors.Control;
            this.btn_manual.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_manual.ForeColor = System.Drawing.Color.Black;
            this.btn_manual.Location = new System.Drawing.Point(2, 281);
            this.btn_manual.Name = "btn_manual";
            this.btn_manual.Size = new System.Drawing.Size(132, 50);
            this.btn_manual.TabIndex = 44;
            this.btn_manual.Text = "메뉴얼 제어";
            this.btn_manual.UseVisualStyleBackColor = false;
            this.btn_manual.Click += new System.EventHandler(this.btn_manual_Click);
            // 
            // btn_motor_status
            // 
            this.btn_motor_status.BackColor = System.Drawing.SystemColors.Control;
            this.btn_motor_status.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_motor_status.ForeColor = System.Drawing.Color.Black;
            this.btn_motor_status.Location = new System.Drawing.Point(2, 225);
            this.btn_motor_status.Name = "btn_motor_status";
            this.btn_motor_status.Size = new System.Drawing.Size(132, 50);
            this.btn_motor_status.TabIndex = 43;
            this.btn_motor_status.Text = "Motor Status";
            this.btn_motor_status.UseVisualStyleBackColor = false;
            this.btn_motor_status.Click += new System.EventHandler(this.btn_motor_status_Click);
            // 
            // btn_aio_status
            // 
            this.btn_aio_status.BackColor = System.Drawing.SystemColors.Control;
            this.btn_aio_status.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_aio_status.ForeColor = System.Drawing.Color.Black;
            this.btn_aio_status.Location = new System.Drawing.Point(2, 169);
            this.btn_aio_status.Name = "btn_aio_status";
            this.btn_aio_status.Size = new System.Drawing.Size(132, 50);
            this.btn_aio_status.TabIndex = 42;
            this.btn_aio_status.Text = "AIO Status";
            this.btn_aio_status.UseVisualStyleBackColor = false;
            this.btn_aio_status.Click += new System.EventHandler(this.btn_aio_status_Click);
            // 
            // btn_reset
            // 
            this.btn_reset.BackColor = System.Drawing.SystemColors.Control;
            this.btn_reset.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_reset.ForeColor = System.Drawing.Color.Black;
            this.btn_reset.Location = new System.Drawing.Point(2, 23);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(132, 50);
            this.btn_reset.TabIndex = 40;
            this.btn_reset.Text = "장치 초기화";
            this.btn_reset.UseVisualStyleBackColor = false;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // btn_io_status
            // 
            this.btn_io_status.BackColor = System.Drawing.SystemColors.Control;
            this.btn_io_status.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_io_status.ForeColor = System.Drawing.Color.Black;
            this.btn_io_status.Location = new System.Drawing.Point(2, 113);
            this.btn_io_status.Name = "btn_io_status";
            this.btn_io_status.Size = new System.Drawing.Size(132, 50);
            this.btn_io_status.TabIndex = 41;
            this.btn_io_status.Text = "IO Status";
            this.btn_io_status.UseVisualStyleBackColor = false;
            this.btn_io_status.Click += new System.EventHandler(this.btn_io_status_Click);
            // 
            // UcrlManagerSubMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerSubMenu";
            this.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_io_status;
        private System.Windows.Forms.Button btn_laser;
        private System.Windows.Forms.Button btn_tactime;
        private System.Windows.Forms.Button btn_sem;
        private System.Windows.Forms.Button btn_manual;
        private System.Windows.Forms.Button btn_motor_status;
        private System.Windows.Forms.Button btn_aio_status;
        private System.Windows.Forms.Button btn_cutlinetracker;
        private System.Windows.Forms.Button btn_process;
        private System.Windows.Forms.Button btn_control;
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnLoadingLoaderTransferBBlowerOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderTransferBVaccumOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderTransferBBlowerOn = new System.Windows.Forms.Button();
            this.btnLoadingLoaderTransferBVaccumOn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnLoadingLoaderTransferABlowerOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderTransferABlowerOn = new System.Windows.Forms.Button();
            this.btnLoadingLoaderTransferAVaccumOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderTransferAVaccumOn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLoadingLoaderTransferLoadingTransfer2Align = new System.Windows.Forms.Button();
            this.btnLoadingLoaderTransferCylinderReset = new System.Windows.Forms.Button();
            this.lblLoadingLoaderTransfer = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtLoadingLoaderBGear = new System.Windows.Forms.TextBox();
            this.lblLoadingLoaderBGear = new System.Windows.Forms.Label();
            this.btnLoadingLoaderBCylinderDown = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBUldY = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBXYCst = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBVaccumOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBXYMid = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBBlowerOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBXMove = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBCol24 = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBCol13 = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBCylinderUp = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBBlowerOn = new System.Windows.Forms.Button();
            this.btnLoadingLoaderBVaccumOn = new System.Windows.Forms.Button();
            this.lblLoadingLoaderB = new System.Windows.Forms.Label();
            this.btnLoadingLoaderCylinderReset = new System.Windows.Forms.Button();
            this.panel27 = new System.Windows.Forms.Panel();
            this.txtLoadingLoaderAGear = new System.Windows.Forms.TextBox();
            this.btnLoadingLoaderACylinderDown = new System.Windows.Forms.Button();
            this.lblLoadingLoaderAGear = new System.Windows.Forms.Label();
            this.btnLoadingLoaderAVaccumOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderABlowerOff = new System.Windows.Forms.Button();
            this.btnLoadingLoaderACol24 = new System.Windows.Forms.Button();
            this.btnLoadingLoaderACylinderUp = new System.Windows.Forms.Button();
            this.btnLoadingLoaderAVaccumOn = new System.Windows.Forms.Button();
            this.btnLoadingLoaderABlowerOn = new System.Windows.Forms.Button();
            this.btnLoadingLoaderACol13 = new System.Windows.Forms.Button();
            this.btnLoadingLoaderAXMove = new System.Windows.Forms.Button();
            this.btnLoadingLoaderAXYMid = new System.Windows.Forms.Button();
            this.btnLoadingLoaderAXYCst = new System.Windows.Forms.Button();
            this.btnLoadingLoaderAUldY = new System.Windows.Forms.Button();
            this.lblLoadingLoaderA = new System.Windows.Forms.Label();
            this.lblLoadingLoader = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.btnLoadingBFreeAilgn = new System.Windows.Forms.Button();
            this.btnLoadingBCST2Loader = new System.Windows.Forms.Button();
            this.btnLoadingBCST2Stage = new System.Windows.Forms.Button();
            this.btnLoadingBCST2Lift = new System.Windows.Forms.Button();
            this.btnLoadingBCSTLift2Buffer = new System.Windows.Forms.Button();
            this.btnLoadingBLoader2LoadingTransfer = new System.Windows.Forms.Button();
            this.btnLoadingBLoadingTransfer2Stage = new System.Windows.Forms.Button();
            this.lblLoadingB = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.btnLoadingAFreeAilgn = new System.Windows.Forms.Button();
            this.btnLoadingACST2Stage = new System.Windows.Forms.Button();
            this.btnLoadingACSTLift2Buffer = new System.Windows.Forms.Button();
            this.btnLoadingALoadingTransfer2Stage = new System.Windows.Forms.Button();
            this.btnLoadingALoader2LoadingTransfer = new System.Windows.Forms.Button();
            this.btnLoadingACST2Loader = new System.Windows.Forms.Button();
            this.btnLoadingACST2Lift = new System.Windows.Forms.Button();
            this.lblLoadingA = new System.Windows.Forms.Label();
            this.lblLoading = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUnloadingAfterTransferBBlowerOff = new System.Windows.Forms.Button();
            this.btnUnloadingAfterTransferBVaccumOff = new System.Windows.Forms.Button();
            this.btnUnloadingAfterTransferBBlowerOn = new System.Windows.Forms.Button();
            this.btnUnloadingAfterTransferBVaccumOn = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnUnloadingBeforeTransferBBlowerOff = new System.Windows.Forms.Button();
            this.btnUnloadingBeforeTransferBVaccumOn = new System.Windows.Forms.Button();
            this.btnUnloadingBeforeTransferBBlowerOn = new System.Windows.Forms.Button();
            this.btnUnloadingBeforeTransferBVaccumOff = new System.Windows.Forms.Button();
            this.lblUnloadingUnloaderTransferB = new System.Windows.Forms.Label();
            this.btnUnloadingUnloaderTransferCylinderReset = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUnloadingBeforeTransferABlowerOff = new System.Windows.Forms.Button();
            this.btnUnloadingBeforeTransferAVaccumOff = new System.Windows.Forms.Button();
            this.btnUnloadingBeforeTransferABlowerOn = new System.Windows.Forms.Button();
            this.btnUnloadingBeforeTransferAVaccumOn = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUnloadingAfterTransferABlowerOff = new System.Windows.Forms.Button();
            this.btnUnloadingAfterTransferAVaccumOff = new System.Windows.Forms.Button();
            this.btnUnloadingAfterTransferABlowerOn = new System.Windows.Forms.Button();
            this.btnUnloadingAfterTransferAVaccumOn = new System.Windows.Forms.Button();
            this.lblUnloadingUnloaderTransferA = new System.Windows.Forms.Label();
            this.lblUnloadingUnloaderTransfer = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtUnloadingUnloaderBGear = new System.Windows.Forms.TextBox();
            this.lblUnloadingUnloaderBGear = new System.Windows.Forms.Label();
            this.btnUnloadingUnloaderBCylinderDown = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBUldY = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBXYCst = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBVaccumOff = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBXYMid = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBBlowerOff = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBXMove = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBCol24 = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBCol13 = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBCylinderUp = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBBlowerOn = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderBVaccumOn = new System.Windows.Forms.Button();
            this.lblUnloadingUnloaderB = new System.Windows.Forms.Label();
            this.btnUnloadingUnloaderCylinderReset = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtUnloadingUnloaderAGear = new System.Windows.Forms.TextBox();
            this.btnUnloadingUnloaderACylinderDown = new System.Windows.Forms.Button();
            this.lblUnloadingUnloaderAGear = new System.Windows.Forms.Label();
            this.btnUnloadingUnloaderAVaccumOff = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderABlowerOff = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderACol24 = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderACylinderUp = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderAVaccumOn = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderABlowerOn = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderACol13 = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderAXMove = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderAXYMid = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderAXYCst = new System.Windows.Forms.Button();
            this.btnUnloadingUnloaderAUldY = new System.Windows.Forms.Button();
            this.lblUnloadingUnloaderA = new System.Windows.Forms.Label();
            this.lblUnloadingUnloader = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnUnloadingBCIM = new System.Windows.Forms.Button();
            this.lblUnloadingB = new System.Windows.Forms.Label();
            this.btnUnloadingBCST2Lift = new System.Windows.Forms.Button();
            this.btnUnloadingBUnloader2CST = new System.Windows.Forms.Button();
            this.btnUnloadingBCSTLift2Buffer = new System.Windows.Forms.Button();
            this.btnUnloadingBStage2UnloadingTransfer = new System.Windows.Forms.Button();
            this.btnUnloadingBUnloadingTransfer2Unloader = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnUnloadingACIM = new System.Windows.Forms.Button();
            this.btnUnloadingACST2Lift = new System.Windows.Forms.Button();
            this.btnUnloadingAUnloader2CST = new System.Windows.Forms.Button();
            this.btnUnloadingAStage2UnloadingTransfer = new System.Windows.Forms.Button();
            this.btnUnloadingAUnloadingTransfer2Unloader = new System.Windows.Forms.Button();
            this.btnUnloadingACSTLift2Buffer = new System.Windows.Forms.Button();
            this.lblUnloadingA = new System.Windows.Forms.Label();
            this.lblUnloading = new System.Windows.Forms.Label();
            this.panel13.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.Control;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.panel4);
            this.panel13.Controls.Add(this.panel1);
            this.panel13.Controls.Add(this.panel29);
            this.panel13.Controls.Add(this.panel30);
            this.panel13.Controls.Add(this.lblLoading);
            this.panel13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(870, 860);
            this.panel13.TabIndex = 463;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.btnLoadingLoaderTransferLoadingTransfer2Align);
            this.panel4.Controls.Add(this.btnLoadingLoaderTransferCylinderReset);
            this.panel4.Controls.Add(this.lblLoadingLoaderTransfer);
            this.panel4.Location = new System.Drawing.Point(443, 381);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(409, 468);
            this.panel4.TabIndex = 463;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnLoadingLoaderTransferBBlowerOff);
            this.panel5.Controls.Add(this.btnLoadingLoaderTransferBVaccumOff);
            this.panel5.Controls.Add(this.btnLoadingLoaderTransferBBlowerOn);
            this.panel5.Controls.Add(this.btnLoadingLoaderTransferBVaccumOn);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Location = new System.Drawing.Point(206, 75);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(188, 180);
            this.panel5.TabIndex = 464;
            // 
            // btnLoadingLoaderTransferBBlowerOff
            // 
            this.btnLoadingLoaderTransferBBlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferBBlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferBBlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferBBlowerOff.Location = new System.Drawing.Point(94, 103);
            this.btnLoadingLoaderTransferBBlowerOff.Name = "btnLoadingLoaderTransferBBlowerOff";
            this.btnLoadingLoaderTransferBBlowerOff.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferBBlowerOff.TabIndex = 42;
            this.btnLoadingLoaderTransferBBlowerOff.Text = "파기 오프";
            this.btnLoadingLoaderTransferBBlowerOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferBBlowerOff.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // btnLoadingLoaderTransferBVaccumOff
            // 
            this.btnLoadingLoaderTransferBVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferBVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferBVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferBVaccumOff.Location = new System.Drawing.Point(95, 27);
            this.btnLoadingLoaderTransferBVaccumOff.Name = "btnLoadingLoaderTransferBVaccumOff";
            this.btnLoadingLoaderTransferBVaccumOff.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferBVaccumOff.TabIndex = 40;
            this.btnLoadingLoaderTransferBVaccumOff.Text = "공압 오프";
            this.btnLoadingLoaderTransferBVaccumOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferBVaccumOff.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // btnLoadingLoaderTransferBBlowerOn
            // 
            this.btnLoadingLoaderTransferBBlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferBBlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferBBlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferBBlowerOn.Location = new System.Drawing.Point(5, 103);
            this.btnLoadingLoaderTransferBBlowerOn.Name = "btnLoadingLoaderTransferBBlowerOn";
            this.btnLoadingLoaderTransferBBlowerOn.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferBBlowerOn.TabIndex = 41;
            this.btnLoadingLoaderTransferBBlowerOn.Text = "파기 온";
            this.btnLoadingLoaderTransferBBlowerOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferBBlowerOn.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // btnLoadingLoaderTransferBVaccumOn
            // 
            this.btnLoadingLoaderTransferBVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferBVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferBVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferBVaccumOn.Location = new System.Drawing.Point(6, 27);
            this.btnLoadingLoaderTransferBVaccumOn.Name = "btnLoadingLoaderTransferBVaccumOn";
            this.btnLoadingLoaderTransferBVaccumOn.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferBVaccumOn.TabIndex = 39;
            this.btnLoadingLoaderTransferBVaccumOn.Text = "공압 온";
            this.btnLoadingLoaderTransferBVaccumOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferBVaccumOn.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "■ B";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnLoadingLoaderTransferABlowerOff);
            this.panel6.Controls.Add(this.btnLoadingLoaderTransferABlowerOn);
            this.panel6.Controls.Add(this.btnLoadingLoaderTransferAVaccumOff);
            this.panel6.Controls.Add(this.btnLoadingLoaderTransferAVaccumOn);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(12, 75);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(188, 180);
            this.panel6.TabIndex = 463;
            // 
            // btnLoadingLoaderTransferABlowerOff
            // 
            this.btnLoadingLoaderTransferABlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferABlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferABlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferABlowerOff.Location = new System.Drawing.Point(94, 103);
            this.btnLoadingLoaderTransferABlowerOff.Name = "btnLoadingLoaderTransferABlowerOff";
            this.btnLoadingLoaderTransferABlowerOff.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferABlowerOff.TabIndex = 38;
            this.btnLoadingLoaderTransferABlowerOff.Text = "파기 오프";
            this.btnLoadingLoaderTransferABlowerOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferABlowerOff.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // btnLoadingLoaderTransferABlowerOn
            // 
            this.btnLoadingLoaderTransferABlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferABlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferABlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferABlowerOn.Location = new System.Drawing.Point(5, 103);
            this.btnLoadingLoaderTransferABlowerOn.Name = "btnLoadingLoaderTransferABlowerOn";
            this.btnLoadingLoaderTransferABlowerOn.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferABlowerOn.TabIndex = 37;
            this.btnLoadingLoaderTransferABlowerOn.Text = "파기 온";
            this.btnLoadingLoaderTransferABlowerOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferABlowerOn.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // btnLoadingLoaderTransferAVaccumOff
            // 
            this.btnLoadingLoaderTransferAVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferAVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferAVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferAVaccumOff.Location = new System.Drawing.Point(95, 27);
            this.btnLoadingLoaderTransferAVaccumOff.Name = "btnLoadingLoaderTransferAVaccumOff";
            this.btnLoadingLoaderTransferAVaccumOff.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferAVaccumOff.TabIndex = 36;
            this.btnLoadingLoaderTransferAVaccumOff.Text = "공압 오프";
            this.btnLoadingLoaderTransferAVaccumOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferAVaccumOff.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // btnLoadingLoaderTransferAVaccumOn
            // 
            this.btnLoadingLoaderTransferAVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferAVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferAVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferAVaccumOn.Location = new System.Drawing.Point(6, 27);
            this.btnLoadingLoaderTransferAVaccumOn.Name = "btnLoadingLoaderTransferAVaccumOn";
            this.btnLoadingLoaderTransferAVaccumOn.Size = new System.Drawing.Size(85, 70);
            this.btnLoadingLoaderTransferAVaccumOn.TabIndex = 35;
            this.btnLoadingLoaderTransferAVaccumOn.Text = "공압 온";
            this.btnLoadingLoaderTransferAVaccumOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderTransferAVaccumOn.Click += new System.EventHandler(this.btnLoaderTransfer_Click);
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(186, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "■ A";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoadingLoaderTransferLoadingTransfer2Align
            // 
            this.btnLoadingLoaderTransferLoadingTransfer2Align.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferLoadingTransfer2Align.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferLoadingTransfer2Align.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferLoadingTransfer2Align.Location = new System.Drawing.Point(206, 29);
            this.btnLoadingLoaderTransferLoadingTransfer2Align.Name = "btnLoadingLoaderTransferLoadingTransfer2Align";
            this.btnLoadingLoaderTransferLoadingTransfer2Align.Size = new System.Drawing.Size(188, 40);
            this.btnLoadingLoaderTransferLoadingTransfer2Align.TabIndex = 462;
            this.btnLoadingLoaderTransferLoadingTransfer2Align.Text = "로딩 트랜스퍼 → 얼라인 ";
            this.btnLoadingLoaderTransferLoadingTransfer2Align.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderTransferCylinderReset
            // 
            this.btnLoadingLoaderTransferCylinderReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderTransferCylinderReset.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderTransferCylinderReset.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderTransferCylinderReset.Location = new System.Drawing.Point(12, 29);
            this.btnLoadingLoaderTransferCylinderReset.Name = "btnLoadingLoaderTransferCylinderReset";
            this.btnLoadingLoaderTransferCylinderReset.Size = new System.Drawing.Size(188, 40);
            this.btnLoadingLoaderTransferCylinderReset.TabIndex = 460;
            this.btnLoadingLoaderTransferCylinderReset.Text = "실린더 초기화";
            this.btnLoadingLoaderTransferCylinderReset.UseVisualStyleBackColor = false;
            // 
            // lblLoadingLoaderTransfer
            // 
            this.lblLoadingLoaderTransfer.AutoEllipsis = true;
            this.lblLoadingLoaderTransfer.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoadingLoaderTransfer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoadingLoaderTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoadingLoaderTransfer.ForeColor = System.Drawing.Color.Black;
            this.lblLoadingLoaderTransfer.Location = new System.Drawing.Point(0, 0);
            this.lblLoadingLoaderTransfer.Name = "lblLoadingLoaderTransfer";
            this.lblLoadingLoaderTransfer.Size = new System.Drawing.Size(407, 22);
            this.lblLoadingLoaderTransfer.TabIndex = 9;
            this.lblLoadingLoaderTransfer.Text = "■ Loader Transfer";
            this.lblLoadingLoaderTransfer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.btnLoadingLoaderCylinderReset);
            this.panel1.Controls.Add(this.panel27);
            this.panel1.Controls.Add(this.lblLoadingLoader);
            this.panel1.Location = new System.Drawing.Point(17, 381);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(409, 468);
            this.panel1.TabIndex = 462;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtLoadingLoaderBGear);
            this.panel3.Controls.Add(this.lblLoadingLoaderBGear);
            this.panel3.Controls.Add(this.btnLoadingLoaderBCylinderDown);
            this.panel3.Controls.Add(this.btnLoadingLoaderBUldY);
            this.panel3.Controls.Add(this.btnLoadingLoaderBXYCst);
            this.panel3.Controls.Add(this.btnLoadingLoaderBVaccumOff);
            this.panel3.Controls.Add(this.btnLoadingLoaderBXYMid);
            this.panel3.Controls.Add(this.btnLoadingLoaderBBlowerOff);
            this.panel3.Controls.Add(this.btnLoadingLoaderBXMove);
            this.panel3.Controls.Add(this.btnLoadingLoaderBCol24);
            this.panel3.Controls.Add(this.btnLoadingLoaderBCol13);
            this.panel3.Controls.Add(this.btnLoadingLoaderBCylinderUp);
            this.panel3.Controls.Add(this.btnLoadingLoaderBBlowerOn);
            this.panel3.Controls.Add(this.btnLoadingLoaderBVaccumOn);
            this.panel3.Controls.Add(this.lblLoadingLoaderB);
            this.panel3.Location = new System.Drawing.Point(206, 75);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(188, 384);
            this.panel3.TabIndex = 461;
            // 
            // txtLoadingLoaderBGear
            // 
            this.txtLoadingLoaderBGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoadingLoaderBGear.Location = new System.Drawing.Point(96, 189);
            this.txtLoadingLoaderBGear.Name = "txtLoadingLoaderBGear";
            this.txtLoadingLoaderBGear.Size = new System.Drawing.Size(85, 23);
            this.txtLoadingLoaderBGear.TabIndex = 50;
            // 
            // lblLoadingLoaderBGear
            // 
            this.lblLoadingLoaderBGear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadingLoaderBGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoadingLoaderBGear.Location = new System.Drawing.Point(5, 189);
            this.lblLoadingLoaderBGear.Name = "lblLoadingLoaderBGear";
            this.lblLoadingLoaderBGear.Size = new System.Drawing.Size(85, 23);
            this.lblLoadingLoaderBGear.TabIndex = 49;
            this.lblLoadingLoaderBGear.Text = "단수 :";
            this.lblLoadingLoaderBGear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoadingLoaderBCylinderDown
            // 
            this.btnLoadingLoaderBCylinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBCylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBCylinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBCylinderDown.Location = new System.Drawing.Point(96, 25);
            this.btnLoadingLoaderBCylinderDown.Name = "btnLoadingLoaderBCylinderDown";
            this.btnLoadingLoaderBCylinderDown.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBCylinderDown.TabIndex = 48;
            this.btnLoadingLoaderBCylinderDown.Text = "실린더 다운";
            this.btnLoadingLoaderBCylinderDown.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderBCylinderDown.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderBUldY
            // 
            this.btnLoadingLoaderBUldY.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBUldY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBUldY.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBUldY.Location = new System.Drawing.Point(5, 340);
            this.btnLoadingLoaderBUldY.Name = "btnLoadingLoaderBUldY";
            this.btnLoadingLoaderBUldY.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderBUldY.TabIndex = 37;
            this.btnLoadingLoaderBUldY.Text = "Uld Y";
            this.btnLoadingLoaderBUldY.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderBXYCst
            // 
            this.btnLoadingLoaderBXYCst.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBXYCst.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBXYCst.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBXYCst.Location = new System.Drawing.Point(5, 300);
            this.btnLoadingLoaderBXYCst.Name = "btnLoadingLoaderBXYCst";
            this.btnLoadingLoaderBXYCst.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderBXYCst.TabIndex = 38;
            this.btnLoadingLoaderBXYCst.Text = "X, Y Cst";
            this.btnLoadingLoaderBXYCst.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderBVaccumOff
            // 
            this.btnLoadingLoaderBVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBVaccumOff.Location = new System.Drawing.Point(96, 65);
            this.btnLoadingLoaderBVaccumOff.Name = "btnLoadingLoaderBVaccumOff";
            this.btnLoadingLoaderBVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBVaccumOff.TabIndex = 47;
            this.btnLoadingLoaderBVaccumOff.Text = "공압 오프";
            this.btnLoadingLoaderBVaccumOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderBVaccumOff.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderBXYMid
            // 
            this.btnLoadingLoaderBXYMid.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBXYMid.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBXYMid.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBXYMid.Location = new System.Drawing.Point(5, 260);
            this.btnLoadingLoaderBXYMid.Name = "btnLoadingLoaderBXYMid";
            this.btnLoadingLoaderBXYMid.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderBXYMid.TabIndex = 39;
            this.btnLoadingLoaderBXYMid.Text = "X, Y Mid";
            this.btnLoadingLoaderBXYMid.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderBBlowerOff
            // 
            this.btnLoadingLoaderBBlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBBlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBBlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBBlowerOff.Location = new System.Drawing.Point(96, 105);
            this.btnLoadingLoaderBBlowerOff.Name = "btnLoadingLoaderBBlowerOff";
            this.btnLoadingLoaderBBlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBBlowerOff.TabIndex = 46;
            this.btnLoadingLoaderBBlowerOff.Text = "파기 오프";
            this.btnLoadingLoaderBBlowerOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderBBlowerOff.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderBXMove
            // 
            this.btnLoadingLoaderBXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBXMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBXMove.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBXMove.Location = new System.Drawing.Point(5, 220);
            this.btnLoadingLoaderBXMove.Name = "btnLoadingLoaderBXMove";
            this.btnLoadingLoaderBXMove.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderBXMove.TabIndex = 40;
            this.btnLoadingLoaderBXMove.Text = "X Move";
            this.btnLoadingLoaderBXMove.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderBCol24
            // 
            this.btnLoadingLoaderBCol24.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBCol24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBCol24.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBCol24.Location = new System.Drawing.Point(96, 145);
            this.btnLoadingLoaderBCol24.Name = "btnLoadingLoaderBCol24";
            this.btnLoadingLoaderBCol24.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBCol24.TabIndex = 45;
            this.btnLoadingLoaderBCol24.Text = "3, 4열";
            this.btnLoadingLoaderBCol24.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderBCol13
            // 
            this.btnLoadingLoaderBCol13.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBCol13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBCol13.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBCol13.Location = new System.Drawing.Point(5, 145);
            this.btnLoadingLoaderBCol13.Name = "btnLoadingLoaderBCol13";
            this.btnLoadingLoaderBCol13.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBCol13.TabIndex = 41;
            this.btnLoadingLoaderBCol13.Text = "1, 2열";
            this.btnLoadingLoaderBCol13.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderBCylinderUp
            // 
            this.btnLoadingLoaderBCylinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBCylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBCylinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBCylinderUp.Location = new System.Drawing.Point(5, 25);
            this.btnLoadingLoaderBCylinderUp.Name = "btnLoadingLoaderBCylinderUp";
            this.btnLoadingLoaderBCylinderUp.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBCylinderUp.TabIndex = 44;
            this.btnLoadingLoaderBCylinderUp.Text = "실린더 업";
            this.btnLoadingLoaderBCylinderUp.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderBCylinderUp.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderBBlowerOn
            // 
            this.btnLoadingLoaderBBlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBBlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBBlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBBlowerOn.Location = new System.Drawing.Point(5, 105);
            this.btnLoadingLoaderBBlowerOn.Name = "btnLoadingLoaderBBlowerOn";
            this.btnLoadingLoaderBBlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBBlowerOn.TabIndex = 42;
            this.btnLoadingLoaderBBlowerOn.Text = "파기 온";
            this.btnLoadingLoaderBBlowerOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderBBlowerOn.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderBVaccumOn
            // 
            this.btnLoadingLoaderBVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderBVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderBVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderBVaccumOn.Location = new System.Drawing.Point(5, 65);
            this.btnLoadingLoaderBVaccumOn.Name = "btnLoadingLoaderBVaccumOn";
            this.btnLoadingLoaderBVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderBVaccumOn.TabIndex = 43;
            this.btnLoadingLoaderBVaccumOn.Text = "공압 온";
            this.btnLoadingLoaderBVaccumOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderBVaccumOn.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // lblLoadingLoaderB
            // 
            this.lblLoadingLoaderB.AutoEllipsis = true;
            this.lblLoadingLoaderB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoadingLoaderB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoadingLoaderB.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoadingLoaderB.ForeColor = System.Drawing.Color.Black;
            this.lblLoadingLoaderB.Location = new System.Drawing.Point(0, 0);
            this.lblLoadingLoaderB.Name = "lblLoadingLoaderB";
            this.lblLoadingLoaderB.Size = new System.Drawing.Size(186, 20);
            this.lblLoadingLoaderB.TabIndex = 9;
            this.lblLoadingLoaderB.Text = "■ B";
            this.lblLoadingLoaderB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoadingLoaderCylinderReset
            // 
            this.btnLoadingLoaderCylinderReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderCylinderReset.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderCylinderReset.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderCylinderReset.Location = new System.Drawing.Point(12, 29);
            this.btnLoadingLoaderCylinderReset.Name = "btnLoadingLoaderCylinderReset";
            this.btnLoadingLoaderCylinderReset.Size = new System.Drawing.Size(188, 40);
            this.btnLoadingLoaderCylinderReset.TabIndex = 460;
            this.btnLoadingLoaderCylinderReset.Text = "실린더 초기화";
            this.btnLoadingLoaderCylinderReset.UseVisualStyleBackColor = false;
            // 
            // panel27
            // 
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.txtLoadingLoaderAGear);
            this.panel27.Controls.Add(this.btnLoadingLoaderACylinderDown);
            this.panel27.Controls.Add(this.lblLoadingLoaderAGear);
            this.panel27.Controls.Add(this.btnLoadingLoaderAVaccumOff);
            this.panel27.Controls.Add(this.btnLoadingLoaderABlowerOff);
            this.panel27.Controls.Add(this.btnLoadingLoaderACol24);
            this.panel27.Controls.Add(this.btnLoadingLoaderACylinderUp);
            this.panel27.Controls.Add(this.btnLoadingLoaderAVaccumOn);
            this.panel27.Controls.Add(this.btnLoadingLoaderABlowerOn);
            this.panel27.Controls.Add(this.btnLoadingLoaderACol13);
            this.panel27.Controls.Add(this.btnLoadingLoaderAXMove);
            this.panel27.Controls.Add(this.btnLoadingLoaderAXYMid);
            this.panel27.Controls.Add(this.btnLoadingLoaderAXYCst);
            this.panel27.Controls.Add(this.btnLoadingLoaderAUldY);
            this.panel27.Controls.Add(this.lblLoadingLoaderA);
            this.panel27.Location = new System.Drawing.Point(12, 75);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(188, 384);
            this.panel27.TabIndex = 459;
            // 
            // txtLoadingLoaderAGear
            // 
            this.txtLoadingLoaderAGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtLoadingLoaderAGear.Location = new System.Drawing.Point(96, 189);
            this.txtLoadingLoaderAGear.Name = "txtLoadingLoaderAGear";
            this.txtLoadingLoaderAGear.Size = new System.Drawing.Size(85, 23);
            this.txtLoadingLoaderAGear.TabIndex = 36;
            // 
            // btnLoadingLoaderACylinderDown
            // 
            this.btnLoadingLoaderACylinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderACylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderACylinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderACylinderDown.Location = new System.Drawing.Point(96, 25);
            this.btnLoadingLoaderACylinderDown.Name = "btnLoadingLoaderACylinderDown";
            this.btnLoadingLoaderACylinderDown.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderACylinderDown.TabIndex = 34;
            this.btnLoadingLoaderACylinderDown.Text = "실린더 다운";
            this.btnLoadingLoaderACylinderDown.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderACylinderDown.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // lblLoadingLoaderAGear
            // 
            this.lblLoadingLoaderAGear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadingLoaderAGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoadingLoaderAGear.Location = new System.Drawing.Point(5, 189);
            this.lblLoadingLoaderAGear.Name = "lblLoadingLoaderAGear";
            this.lblLoadingLoaderAGear.Size = new System.Drawing.Size(85, 23);
            this.lblLoadingLoaderAGear.TabIndex = 35;
            this.lblLoadingLoaderAGear.Text = "단수 :";
            this.lblLoadingLoaderAGear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoadingLoaderAVaccumOff
            // 
            this.btnLoadingLoaderAVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderAVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderAVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderAVaccumOff.Location = new System.Drawing.Point(96, 65);
            this.btnLoadingLoaderAVaccumOff.Name = "btnLoadingLoaderAVaccumOff";
            this.btnLoadingLoaderAVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderAVaccumOff.TabIndex = 33;
            this.btnLoadingLoaderAVaccumOff.Text = "공압 오프";
            this.btnLoadingLoaderAVaccumOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderAVaccumOff.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderABlowerOff
            // 
            this.btnLoadingLoaderABlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderABlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderABlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderABlowerOff.Location = new System.Drawing.Point(96, 105);
            this.btnLoadingLoaderABlowerOff.Name = "btnLoadingLoaderABlowerOff";
            this.btnLoadingLoaderABlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderABlowerOff.TabIndex = 32;
            this.btnLoadingLoaderABlowerOff.Text = "파기 오프";
            this.btnLoadingLoaderABlowerOff.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderABlowerOff.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderACol24
            // 
            this.btnLoadingLoaderACol24.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderACol24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderACol24.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderACol24.Location = new System.Drawing.Point(96, 145);
            this.btnLoadingLoaderACol24.Name = "btnLoadingLoaderACol24";
            this.btnLoadingLoaderACol24.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderACol24.TabIndex = 31;
            this.btnLoadingLoaderACol24.Text = "3, 4열";
            this.btnLoadingLoaderACol24.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderACylinderUp
            // 
            this.btnLoadingLoaderACylinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderACylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderACylinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderACylinderUp.Location = new System.Drawing.Point(5, 25);
            this.btnLoadingLoaderACylinderUp.Name = "btnLoadingLoaderACylinderUp";
            this.btnLoadingLoaderACylinderUp.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderACylinderUp.TabIndex = 30;
            this.btnLoadingLoaderACylinderUp.Text = "실린더 업";
            this.btnLoadingLoaderACylinderUp.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderACylinderUp.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderAVaccumOn
            // 
            this.btnLoadingLoaderAVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderAVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderAVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderAVaccumOn.Location = new System.Drawing.Point(5, 65);
            this.btnLoadingLoaderAVaccumOn.Name = "btnLoadingLoaderAVaccumOn";
            this.btnLoadingLoaderAVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderAVaccumOn.TabIndex = 29;
            this.btnLoadingLoaderAVaccumOn.Text = "공압 온";
            this.btnLoadingLoaderAVaccumOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderAVaccumOn.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderABlowerOn
            // 
            this.btnLoadingLoaderABlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderABlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderABlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderABlowerOn.Location = new System.Drawing.Point(5, 105);
            this.btnLoadingLoaderABlowerOn.Name = "btnLoadingLoaderABlowerOn";
            this.btnLoadingLoaderABlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderABlowerOn.TabIndex = 28;
            this.btnLoadingLoaderABlowerOn.Text = "파기 온";
            this.btnLoadingLoaderABlowerOn.UseVisualStyleBackColor = false;
            this.btnLoadingLoaderABlowerOn.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // btnLoadingLoaderACol13
            // 
            this.btnLoadingLoaderACol13.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderACol13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderACol13.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderACol13.Location = new System.Drawing.Point(5, 145);
            this.btnLoadingLoaderACol13.Name = "btnLoadingLoaderACol13";
            this.btnLoadingLoaderACol13.Size = new System.Drawing.Size(85, 34);
            this.btnLoadingLoaderACol13.TabIndex = 27;
            this.btnLoadingLoaderACol13.Text = "1, 2열";
            this.btnLoadingLoaderACol13.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderAXMove
            // 
            this.btnLoadingLoaderAXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderAXMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderAXMove.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderAXMove.Location = new System.Drawing.Point(5, 220);
            this.btnLoadingLoaderAXMove.Name = "btnLoadingLoaderAXMove";
            this.btnLoadingLoaderAXMove.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderAXMove.TabIndex = 26;
            this.btnLoadingLoaderAXMove.Text = "X Move";
            this.btnLoadingLoaderAXMove.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderAXYMid
            // 
            this.btnLoadingLoaderAXYMid.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderAXYMid.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderAXYMid.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderAXYMid.Location = new System.Drawing.Point(5, 260);
            this.btnLoadingLoaderAXYMid.Name = "btnLoadingLoaderAXYMid";
            this.btnLoadingLoaderAXYMid.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderAXYMid.TabIndex = 25;
            this.btnLoadingLoaderAXYMid.Text = "X, Y Mid";
            this.btnLoadingLoaderAXYMid.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderAXYCst
            // 
            this.btnLoadingLoaderAXYCst.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderAXYCst.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderAXYCst.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderAXYCst.Location = new System.Drawing.Point(5, 300);
            this.btnLoadingLoaderAXYCst.Name = "btnLoadingLoaderAXYCst";
            this.btnLoadingLoaderAXYCst.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderAXYCst.TabIndex = 24;
            this.btnLoadingLoaderAXYCst.Text = "X, Y Cst";
            this.btnLoadingLoaderAXYCst.UseVisualStyleBackColor = false;
            // 
            // btnLoadingLoaderAUldY
            // 
            this.btnLoadingLoaderAUldY.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingLoaderAUldY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingLoaderAUldY.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingLoaderAUldY.Location = new System.Drawing.Point(5, 340);
            this.btnLoadingLoaderAUldY.Name = "btnLoadingLoaderAUldY";
            this.btnLoadingLoaderAUldY.Size = new System.Drawing.Size(176, 34);
            this.btnLoadingLoaderAUldY.TabIndex = 23;
            this.btnLoadingLoaderAUldY.Text = "Uld Y";
            this.btnLoadingLoaderAUldY.UseVisualStyleBackColor = false;
            // 
            // lblLoadingLoaderA
            // 
            this.lblLoadingLoaderA.AutoEllipsis = true;
            this.lblLoadingLoaderA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoadingLoaderA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoadingLoaderA.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoadingLoaderA.ForeColor = System.Drawing.Color.Black;
            this.lblLoadingLoaderA.Location = new System.Drawing.Point(0, 0);
            this.lblLoadingLoaderA.Name = "lblLoadingLoaderA";
            this.lblLoadingLoaderA.Size = new System.Drawing.Size(186, 20);
            this.lblLoadingLoaderA.TabIndex = 9;
            this.lblLoadingLoaderA.Text = "■ A";
            this.lblLoadingLoaderA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoadingLoader
            // 
            this.lblLoadingLoader.AutoEllipsis = true;
            this.lblLoadingLoader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoadingLoader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoadingLoader.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoadingLoader.ForeColor = System.Drawing.Color.Black;
            this.lblLoadingLoader.Location = new System.Drawing.Point(0, 0);
            this.lblLoadingLoader.Name = "lblLoadingLoader";
            this.lblLoadingLoader.Size = new System.Drawing.Size(407, 22);
            this.lblLoadingLoader.TabIndex = 9;
            this.lblLoadingLoader.Text = "■ Loader";
            this.lblLoadingLoader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.btnLoadingBFreeAilgn);
            this.panel29.Controls.Add(this.btnLoadingBCST2Loader);
            this.panel29.Controls.Add(this.btnLoadingBCST2Stage);
            this.panel29.Controls.Add(this.btnLoadingBCST2Lift);
            this.panel29.Controls.Add(this.btnLoadingBCSTLift2Buffer);
            this.panel29.Controls.Add(this.btnLoadingBLoader2LoadingTransfer);
            this.panel29.Controls.Add(this.btnLoadingBLoadingTransfer2Stage);
            this.panel29.Controls.Add(this.lblLoadingB);
            this.panel29.Location = new System.Drawing.Point(443, 32);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(409, 343);
            this.panel29.TabIndex = 459;
            // 
            // btnLoadingBFreeAilgn
            // 
            this.btnLoadingBFreeAilgn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingBFreeAilgn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingBFreeAilgn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingBFreeAilgn.Location = new System.Drawing.Point(238, 99);
            this.btnLoadingBFreeAilgn.Name = "btnLoadingBFreeAilgn";
            this.btnLoadingBFreeAilgn.Size = new System.Drawing.Size(156, 30);
            this.btnLoadingBFreeAilgn.TabIndex = 20;
            this.btnLoadingBFreeAilgn.Text = "프리 얼라인";
            this.btnLoadingBFreeAilgn.UseVisualStyleBackColor = false;
            // 
            // btnLoadingBCST2Loader
            // 
            this.btnLoadingBCST2Loader.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingBCST2Loader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingBCST2Loader.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingBCST2Loader.Location = new System.Drawing.Point(12, 63);
            this.btnLoadingBCST2Loader.Name = "btnLoadingBCST2Loader";
            this.btnLoadingBCST2Loader.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingBCST2Loader.TabIndex = 15;
            this.btnLoadingBCST2Loader.Text = "카세트 → 로더";
            this.btnLoadingBCST2Loader.UseVisualStyleBackColor = false;
            // 
            // btnLoadingBCST2Stage
            // 
            this.btnLoadingBCST2Stage.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingBCST2Stage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingBCST2Stage.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingBCST2Stage.Location = new System.Drawing.Point(12, 207);
            this.btnLoadingBCST2Stage.Name = "btnLoadingBCST2Stage";
            this.btnLoadingBCST2Stage.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingBCST2Stage.TabIndex = 19;
            this.btnLoadingBCST2Stage.Text = "카세트 → 스테이지";
            this.btnLoadingBCST2Stage.UseVisualStyleBackColor = false;
            // 
            // btnLoadingBCST2Lift
            // 
            this.btnLoadingBCST2Lift.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingBCST2Lift.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingBCST2Lift.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingBCST2Lift.Location = new System.Drawing.Point(12, 27);
            this.btnLoadingBCST2Lift.Name = "btnLoadingBCST2Lift";
            this.btnLoadingBCST2Lift.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingBCST2Lift.TabIndex = 14;
            this.btnLoadingBCST2Lift.Text = "카세트 → 리프트";
            this.btnLoadingBCST2Lift.UseVisualStyleBackColor = false;
            // 
            // btnLoadingBCSTLift2Buffer
            // 
            this.btnLoadingBCSTLift2Buffer.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingBCSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingBCSTLift2Buffer.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingBCSTLift2Buffer.Location = new System.Drawing.Point(12, 171);
            this.btnLoadingBCSTLift2Buffer.Name = "btnLoadingBCSTLift2Buffer";
            this.btnLoadingBCSTLift2Buffer.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingBCSTLift2Buffer.TabIndex = 18;
            this.btnLoadingBCSTLift2Buffer.Text = "카세트 리프트 → 버퍼";
            this.btnLoadingBCSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // btnLoadingBLoader2LoadingTransfer
            // 
            this.btnLoadingBLoader2LoadingTransfer.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingBLoader2LoadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingBLoader2LoadingTransfer.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingBLoader2LoadingTransfer.Location = new System.Drawing.Point(12, 99);
            this.btnLoadingBLoader2LoadingTransfer.Name = "btnLoadingBLoader2LoadingTransfer";
            this.btnLoadingBLoader2LoadingTransfer.Size = new System.Drawing.Size(220, 30);
            this.btnLoadingBLoader2LoadingTransfer.TabIndex = 16;
            this.btnLoadingBLoader2LoadingTransfer.Text = "로더 → 로딩 트랜스퍼";
            this.btnLoadingBLoader2LoadingTransfer.UseVisualStyleBackColor = false;
            // 
            // btnLoadingBLoadingTransfer2Stage
            // 
            this.btnLoadingBLoadingTransfer2Stage.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingBLoadingTransfer2Stage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingBLoadingTransfer2Stage.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingBLoadingTransfer2Stage.Location = new System.Drawing.Point(12, 135);
            this.btnLoadingBLoadingTransfer2Stage.Name = "btnLoadingBLoadingTransfer2Stage";
            this.btnLoadingBLoadingTransfer2Stage.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingBLoadingTransfer2Stage.TabIndex = 17;
            this.btnLoadingBLoadingTransfer2Stage.Text = "로딩 트랜스퍼 → 스테이지";
            this.btnLoadingBLoadingTransfer2Stage.UseVisualStyleBackColor = false;
            // 
            // lblLoadingB
            // 
            this.lblLoadingB.AutoEllipsis = true;
            this.lblLoadingB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoadingB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoadingB.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoadingB.ForeColor = System.Drawing.Color.Black;
            this.lblLoadingB.Location = new System.Drawing.Point(0, 0);
            this.lblLoadingB.Name = "lblLoadingB";
            this.lblLoadingB.Size = new System.Drawing.Size(407, 20);
            this.lblLoadingB.TabIndex = 9;
            this.lblLoadingB.Text = "■ B";
            this.lblLoadingB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.btnLoadingAFreeAilgn);
            this.panel30.Controls.Add(this.btnLoadingACST2Stage);
            this.panel30.Controls.Add(this.btnLoadingACSTLift2Buffer);
            this.panel30.Controls.Add(this.btnLoadingALoadingTransfer2Stage);
            this.panel30.Controls.Add(this.btnLoadingALoader2LoadingTransfer);
            this.panel30.Controls.Add(this.btnLoadingACST2Loader);
            this.panel30.Controls.Add(this.btnLoadingACST2Lift);
            this.panel30.Controls.Add(this.lblLoadingA);
            this.panel30.Location = new System.Drawing.Point(17, 32);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(409, 343);
            this.panel30.TabIndex = 458;
            // 
            // btnLoadingAFreeAilgn
            // 
            this.btnLoadingAFreeAilgn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingAFreeAilgn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingAFreeAilgn.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingAFreeAilgn.Location = new System.Drawing.Point(237, 99);
            this.btnLoadingAFreeAilgn.Name = "btnLoadingAFreeAilgn";
            this.btnLoadingAFreeAilgn.Size = new System.Drawing.Size(157, 30);
            this.btnLoadingAFreeAilgn.TabIndex = 16;
            this.btnLoadingAFreeAilgn.Text = "프리 얼라인";
            this.btnLoadingAFreeAilgn.UseVisualStyleBackColor = false;
            // 
            // btnLoadingACST2Stage
            // 
            this.btnLoadingACST2Stage.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingACST2Stage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingACST2Stage.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingACST2Stage.Location = new System.Drawing.Point(12, 207);
            this.btnLoadingACST2Stage.Name = "btnLoadingACST2Stage";
            this.btnLoadingACST2Stage.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingACST2Stage.TabIndex = 15;
            this.btnLoadingACST2Stage.Text = "카세트 → 스테이지";
            this.btnLoadingACST2Stage.UseVisualStyleBackColor = false;
            // 
            // btnLoadingACSTLift2Buffer
            // 
            this.btnLoadingACSTLift2Buffer.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingACSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingACSTLift2Buffer.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingACSTLift2Buffer.Location = new System.Drawing.Point(12, 171);
            this.btnLoadingACSTLift2Buffer.Name = "btnLoadingACSTLift2Buffer";
            this.btnLoadingACSTLift2Buffer.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingACSTLift2Buffer.TabIndex = 14;
            this.btnLoadingACSTLift2Buffer.Text = "카세트 리프트 → 버퍼";
            this.btnLoadingACSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // btnLoadingALoadingTransfer2Stage
            // 
            this.btnLoadingALoadingTransfer2Stage.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingALoadingTransfer2Stage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingALoadingTransfer2Stage.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingALoadingTransfer2Stage.Location = new System.Drawing.Point(12, 135);
            this.btnLoadingALoadingTransfer2Stage.Name = "btnLoadingALoadingTransfer2Stage";
            this.btnLoadingALoadingTransfer2Stage.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingALoadingTransfer2Stage.TabIndex = 13;
            this.btnLoadingALoadingTransfer2Stage.Text = "로딩 트랜스퍼 → 스테이지";
            this.btnLoadingALoadingTransfer2Stage.UseVisualStyleBackColor = false;
            // 
            // btnLoadingALoader2LoadingTransfer
            // 
            this.btnLoadingALoader2LoadingTransfer.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingALoader2LoadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingALoader2LoadingTransfer.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingALoader2LoadingTransfer.Location = new System.Drawing.Point(12, 99);
            this.btnLoadingALoader2LoadingTransfer.Name = "btnLoadingALoader2LoadingTransfer";
            this.btnLoadingALoader2LoadingTransfer.Size = new System.Drawing.Size(219, 30);
            this.btnLoadingALoader2LoadingTransfer.TabIndex = 12;
            this.btnLoadingALoader2LoadingTransfer.Text = "로더 → 로딩 트랜스퍼";
            this.btnLoadingALoader2LoadingTransfer.UseVisualStyleBackColor = false;
            // 
            // btnLoadingACST2Loader
            // 
            this.btnLoadingACST2Loader.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingACST2Loader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingACST2Loader.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingACST2Loader.Location = new System.Drawing.Point(12, 63);
            this.btnLoadingACST2Loader.Name = "btnLoadingACST2Loader";
            this.btnLoadingACST2Loader.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingACST2Loader.TabIndex = 11;
            this.btnLoadingACST2Loader.Text = "카세트 → 로더";
            this.btnLoadingACST2Loader.UseVisualStyleBackColor = false;
            // 
            // btnLoadingACST2Lift
            // 
            this.btnLoadingACST2Lift.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoadingACST2Lift.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoadingACST2Lift.ForeColor = System.Drawing.Color.Black;
            this.btnLoadingACST2Lift.Location = new System.Drawing.Point(12, 27);
            this.btnLoadingACST2Lift.Name = "btnLoadingACST2Lift";
            this.btnLoadingACST2Lift.Size = new System.Drawing.Size(382, 30);
            this.btnLoadingACST2Lift.TabIndex = 10;
            this.btnLoadingACST2Lift.Text = "카세트 → 리프트";
            this.btnLoadingACST2Lift.UseVisualStyleBackColor = false;
            // 
            // lblLoadingA
            // 
            this.lblLoadingA.AutoEllipsis = true;
            this.lblLoadingA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoadingA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoadingA.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoadingA.ForeColor = System.Drawing.Color.Black;
            this.lblLoadingA.Location = new System.Drawing.Point(0, 0);
            this.lblLoadingA.Name = "lblLoadingA";
            this.lblLoadingA.Size = new System.Drawing.Size(407, 20);
            this.lblLoadingA.TabIndex = 9;
            this.lblLoadingA.Text = "■ A";
            this.lblLoadingA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoading
            // 
            this.lblLoading.AutoEllipsis = true;
            this.lblLoading.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoading.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoading.ForeColor = System.Drawing.Color.Black;
            this.lblLoading.Location = new System.Drawing.Point(0, 0);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(868, 26);
            this.lblLoading.TabIndex = 9;
            this.lblLoading.Text = "■ Loading";
            this.lblLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Controls.Add(this.panel14);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.lblUnloading);
            this.panel2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel2.Location = new System.Drawing.Point(869, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 860);
            this.panel2.TabIndex = 464;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.btnUnloadingUnloaderTransferCylinderReset);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.lblUnloadingUnloaderTransfer);
            this.panel7.Location = new System.Drawing.Point(443, 381);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(409, 468);
            this.panel7.TabIndex = 463;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.panel19);
            this.panel8.Controls.Add(this.panel18);
            this.panel8.Controls.Add(this.lblUnloadingUnloaderTransferB);
            this.panel8.Location = new System.Drawing.Point(206, 75);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(188, 282);
            this.panel8.TabIndex = 461;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.label5);
            this.panel19.Controls.Add(this.btnUnloadingAfterTransferBBlowerOff);
            this.panel19.Controls.Add(this.btnUnloadingAfterTransferBVaccumOff);
            this.panel19.Controls.Add(this.btnUnloadingAfterTransferBBlowerOn);
            this.panel19.Controls.Add(this.btnUnloadingAfterTransferBVaccumOn);
            this.panel19.Location = new System.Drawing.Point(3, 146);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(180, 98);
            this.panel19.TabIndex = 464;
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "■ After Transfer";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingAfterTransferBBlowerOff
            // 
            this.btnUnloadingAfterTransferBBlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferBBlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferBBlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferBBlowerOff.Location = new System.Drawing.Point(91, 59);
            this.btnUnloadingAfterTransferBBlowerOff.Name = "btnUnloadingAfterTransferBBlowerOff";
            this.btnUnloadingAfterTransferBBlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferBBlowerOff.TabIndex = 38;
            this.btnUnloadingAfterTransferBBlowerOff.Text = "파기 오프";
            this.btnUnloadingAfterTransferBBlowerOff.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferBBlowerOff.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // btnUnloadingAfterTransferBVaccumOff
            // 
            this.btnUnloadingAfterTransferBVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferBVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferBVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferBVaccumOff.Location = new System.Drawing.Point(91, 23);
            this.btnUnloadingAfterTransferBVaccumOff.Name = "btnUnloadingAfterTransferBVaccumOff";
            this.btnUnloadingAfterTransferBVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferBVaccumOff.TabIndex = 36;
            this.btnUnloadingAfterTransferBVaccumOff.Text = "공압 오프";
            this.btnUnloadingAfterTransferBVaccumOff.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferBVaccumOff.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // btnUnloadingAfterTransferBBlowerOn
            // 
            this.btnUnloadingAfterTransferBBlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferBBlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferBBlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferBBlowerOn.Location = new System.Drawing.Point(2, 59);
            this.btnUnloadingAfterTransferBBlowerOn.Name = "btnUnloadingAfterTransferBBlowerOn";
            this.btnUnloadingAfterTransferBBlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferBBlowerOn.TabIndex = 37;
            this.btnUnloadingAfterTransferBBlowerOn.Text = "파기 온";
            this.btnUnloadingAfterTransferBBlowerOn.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferBBlowerOn.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // btnUnloadingAfterTransferBVaccumOn
            // 
            this.btnUnloadingAfterTransferBVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferBVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferBVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferBVaccumOn.Location = new System.Drawing.Point(2, 23);
            this.btnUnloadingAfterTransferBVaccumOn.Name = "btnUnloadingAfterTransferBVaccumOn";
            this.btnUnloadingAfterTransferBVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferBVaccumOn.TabIndex = 35;
            this.btnUnloadingAfterTransferBVaccumOn.Text = "공압 온";
            this.btnUnloadingAfterTransferBVaccumOn.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferBVaccumOn.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.label3);
            this.panel18.Controls.Add(this.btnUnloadingBeforeTransferBBlowerOff);
            this.panel18.Controls.Add(this.btnUnloadingBeforeTransferBVaccumOn);
            this.panel18.Controls.Add(this.btnUnloadingBeforeTransferBBlowerOn);
            this.panel18.Controls.Add(this.btnUnloadingBeforeTransferBVaccumOff);
            this.panel18.Location = new System.Drawing.Point(3, 28);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(180, 98);
            this.panel18.TabIndex = 463;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "■ Before Transfer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingBeforeTransferBBlowerOff
            // 
            this.btnUnloadingBeforeTransferBBlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferBBlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferBBlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferBBlowerOff.Location = new System.Drawing.Point(91, 59);
            this.btnUnloadingBeforeTransferBBlowerOff.Name = "btnUnloadingBeforeTransferBBlowerOff";
            this.btnUnloadingBeforeTransferBBlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferBBlowerOff.TabIndex = 42;
            this.btnUnloadingBeforeTransferBBlowerOff.Text = "파기 오프";
            this.btnUnloadingBeforeTransferBBlowerOff.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferBBlowerOff.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // btnUnloadingBeforeTransferBVaccumOn
            // 
            this.btnUnloadingBeforeTransferBVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferBVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferBVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferBVaccumOn.Location = new System.Drawing.Point(2, 23);
            this.btnUnloadingBeforeTransferBVaccumOn.Name = "btnUnloadingBeforeTransferBVaccumOn";
            this.btnUnloadingBeforeTransferBVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferBVaccumOn.TabIndex = 39;
            this.btnUnloadingBeforeTransferBVaccumOn.Text = "공압 온";
            this.btnUnloadingBeforeTransferBVaccumOn.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferBVaccumOn.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // btnUnloadingBeforeTransferBBlowerOn
            // 
            this.btnUnloadingBeforeTransferBBlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferBBlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferBBlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferBBlowerOn.Location = new System.Drawing.Point(2, 59);
            this.btnUnloadingBeforeTransferBBlowerOn.Name = "btnUnloadingBeforeTransferBBlowerOn";
            this.btnUnloadingBeforeTransferBBlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferBBlowerOn.TabIndex = 41;
            this.btnUnloadingBeforeTransferBBlowerOn.Text = "파기 온";
            this.btnUnloadingBeforeTransferBBlowerOn.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferBBlowerOn.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // btnUnloadingBeforeTransferBVaccumOff
            // 
            this.btnUnloadingBeforeTransferBVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferBVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferBVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferBVaccumOff.Location = new System.Drawing.Point(91, 23);
            this.btnUnloadingBeforeTransferBVaccumOff.Name = "btnUnloadingBeforeTransferBVaccumOff";
            this.btnUnloadingBeforeTransferBVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferBVaccumOff.TabIndex = 40;
            this.btnUnloadingBeforeTransferBVaccumOff.Text = "공압 오프";
            this.btnUnloadingBeforeTransferBVaccumOff.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferBVaccumOff.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // lblUnloadingUnloaderTransferB
            // 
            this.lblUnloadingUnloaderTransferB.AutoEllipsis = true;
            this.lblUnloadingUnloaderTransferB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingUnloaderTransferB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingUnloaderTransferB.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingUnloaderTransferB.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingUnloaderTransferB.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingUnloaderTransferB.Name = "lblUnloadingUnloaderTransferB";
            this.lblUnloadingUnloaderTransferB.Size = new System.Drawing.Size(186, 20);
            this.lblUnloadingUnloaderTransferB.TabIndex = 9;
            this.lblUnloadingUnloaderTransferB.Text = "■ B";
            this.lblUnloadingUnloaderTransferB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingUnloaderTransferCylinderReset
            // 
            this.btnUnloadingUnloaderTransferCylinderReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderTransferCylinderReset.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderTransferCylinderReset.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderTransferCylinderReset.Location = new System.Drawing.Point(12, 29);
            this.btnUnloadingUnloaderTransferCylinderReset.Name = "btnUnloadingUnloaderTransferCylinderReset";
            this.btnUnloadingUnloaderTransferCylinderReset.Size = new System.Drawing.Size(188, 40);
            this.btnUnloadingUnloaderTransferCylinderReset.TabIndex = 460;
            this.btnUnloadingUnloaderTransferCylinderReset.Text = "실린더 초기화";
            this.btnUnloadingUnloaderTransferCylinderReset.UseVisualStyleBackColor = false;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.panel16);
            this.panel9.Controls.Add(this.panel17);
            this.panel9.Controls.Add(this.lblUnloadingUnloaderTransferA);
            this.panel9.Location = new System.Drawing.Point(12, 75);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(188, 282);
            this.panel9.TabIndex = 459;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.label1);
            this.panel16.Controls.Add(this.btnUnloadingBeforeTransferABlowerOff);
            this.panel16.Controls.Add(this.btnUnloadingBeforeTransferAVaccumOff);
            this.panel16.Controls.Add(this.btnUnloadingBeforeTransferABlowerOn);
            this.panel16.Controls.Add(this.btnUnloadingBeforeTransferAVaccumOn);
            this.panel16.Location = new System.Drawing.Point(3, 27);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(180, 98);
            this.panel16.TabIndex = 462;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ Before Transfer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingBeforeTransferABlowerOff
            // 
            this.btnUnloadingBeforeTransferABlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferABlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferABlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferABlowerOff.Location = new System.Drawing.Point(91, 59);
            this.btnUnloadingBeforeTransferABlowerOff.Name = "btnUnloadingBeforeTransferABlowerOff";
            this.btnUnloadingBeforeTransferABlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferABlowerOff.TabIndex = 38;
            this.btnUnloadingBeforeTransferABlowerOff.Text = "파기 오프";
            this.btnUnloadingBeforeTransferABlowerOff.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferABlowerOff.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // btnUnloadingBeforeTransferAVaccumOff
            // 
            this.btnUnloadingBeforeTransferAVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferAVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferAVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferAVaccumOff.Location = new System.Drawing.Point(91, 23);
            this.btnUnloadingBeforeTransferAVaccumOff.Name = "btnUnloadingBeforeTransferAVaccumOff";
            this.btnUnloadingBeforeTransferAVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferAVaccumOff.TabIndex = 36;
            this.btnUnloadingBeforeTransferAVaccumOff.Text = "공압 오프";
            this.btnUnloadingBeforeTransferAVaccumOff.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferAVaccumOff.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // btnUnloadingBeforeTransferABlowerOn
            // 
            this.btnUnloadingBeforeTransferABlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferABlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferABlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferABlowerOn.Location = new System.Drawing.Point(2, 59);
            this.btnUnloadingBeforeTransferABlowerOn.Name = "btnUnloadingBeforeTransferABlowerOn";
            this.btnUnloadingBeforeTransferABlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferABlowerOn.TabIndex = 37;
            this.btnUnloadingBeforeTransferABlowerOn.Text = "파기 온";
            this.btnUnloadingBeforeTransferABlowerOn.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferABlowerOn.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // btnUnloadingBeforeTransferAVaccumOn
            // 
            this.btnUnloadingBeforeTransferAVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBeforeTransferAVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBeforeTransferAVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBeforeTransferAVaccumOn.Location = new System.Drawing.Point(2, 23);
            this.btnUnloadingBeforeTransferAVaccumOn.Name = "btnUnloadingBeforeTransferAVaccumOn";
            this.btnUnloadingBeforeTransferAVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingBeforeTransferAVaccumOn.TabIndex = 35;
            this.btnUnloadingBeforeTransferAVaccumOn.Text = "공압 온";
            this.btnUnloadingBeforeTransferAVaccumOn.UseVisualStyleBackColor = false;
            this.btnUnloadingBeforeTransferAVaccumOn.Click += new System.EventHandler(this.btnBeforeTransfer_Click);
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.label2);
            this.panel17.Controls.Add(this.btnUnloadingAfterTransferABlowerOff);
            this.panel17.Controls.Add(this.btnUnloadingAfterTransferAVaccumOff);
            this.panel17.Controls.Add(this.btnUnloadingAfterTransferABlowerOn);
            this.panel17.Controls.Add(this.btnUnloadingAfterTransferAVaccumOn);
            this.panel17.Location = new System.Drawing.Point(3, 145);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(180, 98);
            this.panel17.TabIndex = 463;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ After Transfer";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingAfterTransferABlowerOff
            // 
            this.btnUnloadingAfterTransferABlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferABlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferABlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferABlowerOff.Location = new System.Drawing.Point(91, 59);
            this.btnUnloadingAfterTransferABlowerOff.Name = "btnUnloadingAfterTransferABlowerOff";
            this.btnUnloadingAfterTransferABlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferABlowerOff.TabIndex = 38;
            this.btnUnloadingAfterTransferABlowerOff.Text = "파기 오프";
            this.btnUnloadingAfterTransferABlowerOff.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferABlowerOff.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // btnUnloadingAfterTransferAVaccumOff
            // 
            this.btnUnloadingAfterTransferAVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferAVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferAVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferAVaccumOff.Location = new System.Drawing.Point(91, 23);
            this.btnUnloadingAfterTransferAVaccumOff.Name = "btnUnloadingAfterTransferAVaccumOff";
            this.btnUnloadingAfterTransferAVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferAVaccumOff.TabIndex = 36;
            this.btnUnloadingAfterTransferAVaccumOff.Text = "공압 오프";
            this.btnUnloadingAfterTransferAVaccumOff.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferAVaccumOff.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // btnUnloadingAfterTransferABlowerOn
            // 
            this.btnUnloadingAfterTransferABlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferABlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferABlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferABlowerOn.Location = new System.Drawing.Point(2, 59);
            this.btnUnloadingAfterTransferABlowerOn.Name = "btnUnloadingAfterTransferABlowerOn";
            this.btnUnloadingAfterTransferABlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferABlowerOn.TabIndex = 37;
            this.btnUnloadingAfterTransferABlowerOn.Text = "파기 온";
            this.btnUnloadingAfterTransferABlowerOn.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferABlowerOn.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // btnUnloadingAfterTransferAVaccumOn
            // 
            this.btnUnloadingAfterTransferAVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAfterTransferAVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAfterTransferAVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAfterTransferAVaccumOn.Location = new System.Drawing.Point(2, 23);
            this.btnUnloadingAfterTransferAVaccumOn.Name = "btnUnloadingAfterTransferAVaccumOn";
            this.btnUnloadingAfterTransferAVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingAfterTransferAVaccumOn.TabIndex = 35;
            this.btnUnloadingAfterTransferAVaccumOn.Text = "공압 온";
            this.btnUnloadingAfterTransferAVaccumOn.UseVisualStyleBackColor = false;
            this.btnUnloadingAfterTransferAVaccumOn.Click += new System.EventHandler(this.btnAfterTransfer_Click);
            // 
            // lblUnloadingUnloaderTransferA
            // 
            this.lblUnloadingUnloaderTransferA.AutoEllipsis = true;
            this.lblUnloadingUnloaderTransferA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingUnloaderTransferA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingUnloaderTransferA.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingUnloaderTransferA.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingUnloaderTransferA.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingUnloaderTransferA.Name = "lblUnloadingUnloaderTransferA";
            this.lblUnloadingUnloaderTransferA.Size = new System.Drawing.Size(186, 20);
            this.lblUnloadingUnloaderTransferA.TabIndex = 9;
            this.lblUnloadingUnloaderTransferA.Text = "■ A";
            this.lblUnloadingUnloaderTransferA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloadingUnloaderTransfer
            // 
            this.lblUnloadingUnloaderTransfer.AutoEllipsis = true;
            this.lblUnloadingUnloaderTransfer.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingUnloaderTransfer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingUnloaderTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingUnloaderTransfer.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingUnloaderTransfer.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingUnloaderTransfer.Name = "lblUnloadingUnloaderTransfer";
            this.lblUnloadingUnloaderTransfer.Size = new System.Drawing.Size(407, 22);
            this.lblUnloadingUnloaderTransfer.TabIndex = 9;
            this.lblUnloadingUnloaderTransfer.Text = "■ Unloader Transfer";
            this.lblUnloadingUnloaderTransfer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Controls.Add(this.btnUnloadingUnloaderCylinderReset);
            this.panel10.Controls.Add(this.panel12);
            this.panel10.Controls.Add(this.lblUnloadingUnloader);
            this.panel10.Location = new System.Drawing.Point(17, 381);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(409, 468);
            this.panel10.TabIndex = 462;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.txtUnloadingUnloaderBGear);
            this.panel11.Controls.Add(this.lblUnloadingUnloaderBGear);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBCylinderDown);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBUldY);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBXYCst);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBVaccumOff);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBXYMid);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBBlowerOff);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBXMove);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBCol24);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBCol13);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBCylinderUp);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBBlowerOn);
            this.panel11.Controls.Add(this.btnUnloadingUnloaderBVaccumOn);
            this.panel11.Controls.Add(this.lblUnloadingUnloaderB);
            this.panel11.Location = new System.Drawing.Point(206, 75);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(188, 384);
            this.panel11.TabIndex = 461;
            // 
            // txtUnloadingUnloaderBGear
            // 
            this.txtUnloadingUnloaderBGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloadingUnloaderBGear.Location = new System.Drawing.Point(96, 189);
            this.txtUnloadingUnloaderBGear.Name = "txtUnloadingUnloaderBGear";
            this.txtUnloadingUnloaderBGear.Size = new System.Drawing.Size(85, 23);
            this.txtUnloadingUnloaderBGear.TabIndex = 50;
            // 
            // lblUnloadingUnloaderBGear
            // 
            this.lblUnloadingUnloaderBGear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloadingUnloaderBGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloadingUnloaderBGear.Location = new System.Drawing.Point(5, 189);
            this.lblUnloadingUnloaderBGear.Name = "lblUnloadingUnloaderBGear";
            this.lblUnloadingUnloaderBGear.Size = new System.Drawing.Size(85, 23);
            this.lblUnloadingUnloaderBGear.TabIndex = 49;
            this.lblUnloadingUnloaderBGear.Text = "단수 :";
            this.lblUnloadingUnloaderBGear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingUnloaderBCylinderDown
            // 
            this.btnUnloadingUnloaderBCylinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBCylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBCylinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBCylinderDown.Location = new System.Drawing.Point(96, 25);
            this.btnUnloadingUnloaderBCylinderDown.Name = "btnUnloadingUnloaderBCylinderDown";
            this.btnUnloadingUnloaderBCylinderDown.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBCylinderDown.TabIndex = 48;
            this.btnUnloadingUnloaderBCylinderDown.Text = "실린더 다운";
            this.btnUnloadingUnloaderBCylinderDown.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderBCylinderDown.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderBUldY
            // 
            this.btnUnloadingUnloaderBUldY.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBUldY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBUldY.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBUldY.Location = new System.Drawing.Point(5, 340);
            this.btnUnloadingUnloaderBUldY.Name = "btnUnloadingUnloaderBUldY";
            this.btnUnloadingUnloaderBUldY.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderBUldY.TabIndex = 37;
            this.btnUnloadingUnloaderBUldY.Text = "Uld Y";
            this.btnUnloadingUnloaderBUldY.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderBXYCst
            // 
            this.btnUnloadingUnloaderBXYCst.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBXYCst.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBXYCst.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBXYCst.Location = new System.Drawing.Point(5, 300);
            this.btnUnloadingUnloaderBXYCst.Name = "btnUnloadingUnloaderBXYCst";
            this.btnUnloadingUnloaderBXYCst.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderBXYCst.TabIndex = 38;
            this.btnUnloadingUnloaderBXYCst.Text = "X, Y Cst";
            this.btnUnloadingUnloaderBXYCst.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderBVaccumOff
            // 
            this.btnUnloadingUnloaderBVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBVaccumOff.Location = new System.Drawing.Point(96, 65);
            this.btnUnloadingUnloaderBVaccumOff.Name = "btnUnloadingUnloaderBVaccumOff";
            this.btnUnloadingUnloaderBVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBVaccumOff.TabIndex = 47;
            this.btnUnloadingUnloaderBVaccumOff.Text = "공압 오프";
            this.btnUnloadingUnloaderBVaccumOff.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderBVaccumOff.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderBXYMid
            // 
            this.btnUnloadingUnloaderBXYMid.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBXYMid.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBXYMid.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBXYMid.Location = new System.Drawing.Point(5, 260);
            this.btnUnloadingUnloaderBXYMid.Name = "btnUnloadingUnloaderBXYMid";
            this.btnUnloadingUnloaderBXYMid.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderBXYMid.TabIndex = 39;
            this.btnUnloadingUnloaderBXYMid.Text = "X, Y Mid";
            this.btnUnloadingUnloaderBXYMid.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderBBlowerOff
            // 
            this.btnUnloadingUnloaderBBlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBBlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBBlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBBlowerOff.Location = new System.Drawing.Point(96, 105);
            this.btnUnloadingUnloaderBBlowerOff.Name = "btnUnloadingUnloaderBBlowerOff";
            this.btnUnloadingUnloaderBBlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBBlowerOff.TabIndex = 46;
            this.btnUnloadingUnloaderBBlowerOff.Text = "파기 오프";
            this.btnUnloadingUnloaderBBlowerOff.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderBBlowerOff.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderBXMove
            // 
            this.btnUnloadingUnloaderBXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBXMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBXMove.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBXMove.Location = new System.Drawing.Point(5, 220);
            this.btnUnloadingUnloaderBXMove.Name = "btnUnloadingUnloaderBXMove";
            this.btnUnloadingUnloaderBXMove.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderBXMove.TabIndex = 40;
            this.btnUnloadingUnloaderBXMove.Text = "X Move";
            this.btnUnloadingUnloaderBXMove.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderBCol24
            // 
            this.btnUnloadingUnloaderBCol24.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBCol24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBCol24.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBCol24.Location = new System.Drawing.Point(96, 145);
            this.btnUnloadingUnloaderBCol24.Name = "btnUnloadingUnloaderBCol24";
            this.btnUnloadingUnloaderBCol24.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBCol24.TabIndex = 45;
            this.btnUnloadingUnloaderBCol24.Text = "3, 4열";
            this.btnUnloadingUnloaderBCol24.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderBCol13
            // 
            this.btnUnloadingUnloaderBCol13.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBCol13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBCol13.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBCol13.Location = new System.Drawing.Point(5, 145);
            this.btnUnloadingUnloaderBCol13.Name = "btnUnloadingUnloaderBCol13";
            this.btnUnloadingUnloaderBCol13.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBCol13.TabIndex = 41;
            this.btnUnloadingUnloaderBCol13.Text = "1, 2열";
            this.btnUnloadingUnloaderBCol13.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderBCylinderUp
            // 
            this.btnUnloadingUnloaderBCylinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBCylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBCylinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBCylinderUp.Location = new System.Drawing.Point(5, 25);
            this.btnUnloadingUnloaderBCylinderUp.Name = "btnUnloadingUnloaderBCylinderUp";
            this.btnUnloadingUnloaderBCylinderUp.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBCylinderUp.TabIndex = 44;
            this.btnUnloadingUnloaderBCylinderUp.Text = "실린더 업";
            this.btnUnloadingUnloaderBCylinderUp.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderBCylinderUp.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderBBlowerOn
            // 
            this.btnUnloadingUnloaderBBlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBBlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBBlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBBlowerOn.Location = new System.Drawing.Point(5, 105);
            this.btnUnloadingUnloaderBBlowerOn.Name = "btnUnloadingUnloaderBBlowerOn";
            this.btnUnloadingUnloaderBBlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBBlowerOn.TabIndex = 42;
            this.btnUnloadingUnloaderBBlowerOn.Text = "파기 온";
            this.btnUnloadingUnloaderBBlowerOn.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderBBlowerOn.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderBVaccumOn
            // 
            this.btnUnloadingUnloaderBVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderBVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderBVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderBVaccumOn.Location = new System.Drawing.Point(5, 65);
            this.btnUnloadingUnloaderBVaccumOn.Name = "btnUnloadingUnloaderBVaccumOn";
            this.btnUnloadingUnloaderBVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderBVaccumOn.TabIndex = 43;
            this.btnUnloadingUnloaderBVaccumOn.Text = "공압 온";
            this.btnUnloadingUnloaderBVaccumOn.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderBVaccumOn.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // lblUnloadingUnloaderB
            // 
            this.lblUnloadingUnloaderB.AutoEllipsis = true;
            this.lblUnloadingUnloaderB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingUnloaderB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingUnloaderB.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingUnloaderB.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingUnloaderB.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingUnloaderB.Name = "lblUnloadingUnloaderB";
            this.lblUnloadingUnloaderB.Size = new System.Drawing.Size(186, 20);
            this.lblUnloadingUnloaderB.TabIndex = 9;
            this.lblUnloadingUnloaderB.Text = "■ B";
            this.lblUnloadingUnloaderB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingUnloaderCylinderReset
            // 
            this.btnUnloadingUnloaderCylinderReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderCylinderReset.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderCylinderReset.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderCylinderReset.Location = new System.Drawing.Point(12, 29);
            this.btnUnloadingUnloaderCylinderReset.Name = "btnUnloadingUnloaderCylinderReset";
            this.btnUnloadingUnloaderCylinderReset.Size = new System.Drawing.Size(188, 40);
            this.btnUnloadingUnloaderCylinderReset.TabIndex = 460;
            this.btnUnloadingUnloaderCylinderReset.Text = "실린더 초기화";
            this.btnUnloadingUnloaderCylinderReset.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.txtUnloadingUnloaderAGear);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderACylinderDown);
            this.panel12.Controls.Add(this.lblUnloadingUnloaderAGear);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderAVaccumOff);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderABlowerOff);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderACol24);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderACylinderUp);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderAVaccumOn);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderABlowerOn);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderACol13);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderAXMove);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderAXYMid);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderAXYCst);
            this.panel12.Controls.Add(this.btnUnloadingUnloaderAUldY);
            this.panel12.Controls.Add(this.lblUnloadingUnloaderA);
            this.panel12.Location = new System.Drawing.Point(12, 75);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(188, 384);
            this.panel12.TabIndex = 459;
            // 
            // txtUnloadingUnloaderAGear
            // 
            this.txtUnloadingUnloaderAGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUnloadingUnloaderAGear.Location = new System.Drawing.Point(96, 189);
            this.txtUnloadingUnloaderAGear.Name = "txtUnloadingUnloaderAGear";
            this.txtUnloadingUnloaderAGear.Size = new System.Drawing.Size(85, 23);
            this.txtUnloadingUnloaderAGear.TabIndex = 36;
            // 
            // btnUnloadingUnloaderACylinderDown
            // 
            this.btnUnloadingUnloaderACylinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderACylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderACylinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderACylinderDown.Location = new System.Drawing.Point(96, 25);
            this.btnUnloadingUnloaderACylinderDown.Name = "btnUnloadingUnloaderACylinderDown";
            this.btnUnloadingUnloaderACylinderDown.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderACylinderDown.TabIndex = 34;
            this.btnUnloadingUnloaderACylinderDown.Text = "실린더 다운";
            this.btnUnloadingUnloaderACylinderDown.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderACylinderDown.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // lblUnloadingUnloaderAGear
            // 
            this.lblUnloadingUnloaderAGear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloadingUnloaderAGear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloadingUnloaderAGear.Location = new System.Drawing.Point(5, 189);
            this.lblUnloadingUnloaderAGear.Name = "lblUnloadingUnloaderAGear";
            this.lblUnloadingUnloaderAGear.Size = new System.Drawing.Size(85, 23);
            this.lblUnloadingUnloaderAGear.TabIndex = 35;
            this.lblUnloadingUnloaderAGear.Text = "단수 :";
            this.lblUnloadingUnloaderAGear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingUnloaderAVaccumOff
            // 
            this.btnUnloadingUnloaderAVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderAVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderAVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderAVaccumOff.Location = new System.Drawing.Point(96, 65);
            this.btnUnloadingUnloaderAVaccumOff.Name = "btnUnloadingUnloaderAVaccumOff";
            this.btnUnloadingUnloaderAVaccumOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderAVaccumOff.TabIndex = 33;
            this.btnUnloadingUnloaderAVaccumOff.Text = "공압 오프";
            this.btnUnloadingUnloaderAVaccumOff.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderAVaccumOff.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderABlowerOff
            // 
            this.btnUnloadingUnloaderABlowerOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderABlowerOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderABlowerOff.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderABlowerOff.Location = new System.Drawing.Point(96, 105);
            this.btnUnloadingUnloaderABlowerOff.Name = "btnUnloadingUnloaderABlowerOff";
            this.btnUnloadingUnloaderABlowerOff.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderABlowerOff.TabIndex = 32;
            this.btnUnloadingUnloaderABlowerOff.Text = "파기 오프";
            this.btnUnloadingUnloaderABlowerOff.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderABlowerOff.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderACol24
            // 
            this.btnUnloadingUnloaderACol24.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderACol24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderACol24.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderACol24.Location = new System.Drawing.Point(96, 145);
            this.btnUnloadingUnloaderACol24.Name = "btnUnloadingUnloaderACol24";
            this.btnUnloadingUnloaderACol24.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderACol24.TabIndex = 31;
            this.btnUnloadingUnloaderACol24.Text = "3, 4열";
            this.btnUnloadingUnloaderACol24.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderACylinderUp
            // 
            this.btnUnloadingUnloaderACylinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderACylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderACylinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderACylinderUp.Location = new System.Drawing.Point(5, 25);
            this.btnUnloadingUnloaderACylinderUp.Name = "btnUnloadingUnloaderACylinderUp";
            this.btnUnloadingUnloaderACylinderUp.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderACylinderUp.TabIndex = 30;
            this.btnUnloadingUnloaderACylinderUp.Text = "실린더 업";
            this.btnUnloadingUnloaderACylinderUp.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderACylinderUp.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderAVaccumOn
            // 
            this.btnUnloadingUnloaderAVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderAVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderAVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderAVaccumOn.Location = new System.Drawing.Point(5, 65);
            this.btnUnloadingUnloaderAVaccumOn.Name = "btnUnloadingUnloaderAVaccumOn";
            this.btnUnloadingUnloaderAVaccumOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderAVaccumOn.TabIndex = 29;
            this.btnUnloadingUnloaderAVaccumOn.Text = "공압 온";
            this.btnUnloadingUnloaderAVaccumOn.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderAVaccumOn.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderABlowerOn
            // 
            this.btnUnloadingUnloaderABlowerOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderABlowerOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderABlowerOn.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderABlowerOn.Location = new System.Drawing.Point(5, 105);
            this.btnUnloadingUnloaderABlowerOn.Name = "btnUnloadingUnloaderABlowerOn";
            this.btnUnloadingUnloaderABlowerOn.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderABlowerOn.TabIndex = 28;
            this.btnUnloadingUnloaderABlowerOn.Text = "파기 온";
            this.btnUnloadingUnloaderABlowerOn.UseVisualStyleBackColor = false;
            this.btnUnloadingUnloaderABlowerOn.Click += new System.EventHandler(this.btnUnloader_Click);
            // 
            // btnUnloadingUnloaderACol13
            // 
            this.btnUnloadingUnloaderACol13.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderACol13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderACol13.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderACol13.Location = new System.Drawing.Point(5, 145);
            this.btnUnloadingUnloaderACol13.Name = "btnUnloadingUnloaderACol13";
            this.btnUnloadingUnloaderACol13.Size = new System.Drawing.Size(85, 34);
            this.btnUnloadingUnloaderACol13.TabIndex = 27;
            this.btnUnloadingUnloaderACol13.Text = "1, 2열";
            this.btnUnloadingUnloaderACol13.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderAXMove
            // 
            this.btnUnloadingUnloaderAXMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderAXMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderAXMove.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderAXMove.Location = new System.Drawing.Point(5, 220);
            this.btnUnloadingUnloaderAXMove.Name = "btnUnloadingUnloaderAXMove";
            this.btnUnloadingUnloaderAXMove.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderAXMove.TabIndex = 26;
            this.btnUnloadingUnloaderAXMove.Text = "X Move";
            this.btnUnloadingUnloaderAXMove.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderAXYMid
            // 
            this.btnUnloadingUnloaderAXYMid.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderAXYMid.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderAXYMid.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderAXYMid.Location = new System.Drawing.Point(5, 260);
            this.btnUnloadingUnloaderAXYMid.Name = "btnUnloadingUnloaderAXYMid";
            this.btnUnloadingUnloaderAXYMid.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderAXYMid.TabIndex = 25;
            this.btnUnloadingUnloaderAXYMid.Text = "X, Y Mid";
            this.btnUnloadingUnloaderAXYMid.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderAXYCst
            // 
            this.btnUnloadingUnloaderAXYCst.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderAXYCst.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderAXYCst.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderAXYCst.Location = new System.Drawing.Point(5, 300);
            this.btnUnloadingUnloaderAXYCst.Name = "btnUnloadingUnloaderAXYCst";
            this.btnUnloadingUnloaderAXYCst.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderAXYCst.TabIndex = 24;
            this.btnUnloadingUnloaderAXYCst.Text = "X, Y Cst";
            this.btnUnloadingUnloaderAXYCst.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingUnloaderAUldY
            // 
            this.btnUnloadingUnloaderAUldY.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingUnloaderAUldY.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingUnloaderAUldY.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingUnloaderAUldY.Location = new System.Drawing.Point(5, 340);
            this.btnUnloadingUnloaderAUldY.Name = "btnUnloadingUnloaderAUldY";
            this.btnUnloadingUnloaderAUldY.Size = new System.Drawing.Size(176, 34);
            this.btnUnloadingUnloaderAUldY.TabIndex = 23;
            this.btnUnloadingUnloaderAUldY.Text = "Uld Y";
            this.btnUnloadingUnloaderAUldY.UseVisualStyleBackColor = false;
            // 
            // lblUnloadingUnloaderA
            // 
            this.lblUnloadingUnloaderA.AutoEllipsis = true;
            this.lblUnloadingUnloaderA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingUnloaderA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingUnloaderA.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingUnloaderA.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingUnloaderA.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingUnloaderA.Name = "lblUnloadingUnloaderA";
            this.lblUnloadingUnloaderA.Size = new System.Drawing.Size(186, 20);
            this.lblUnloadingUnloaderA.TabIndex = 9;
            this.lblUnloadingUnloaderA.Text = "■ A";
            this.lblUnloadingUnloaderA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloadingUnloader
            // 
            this.lblUnloadingUnloader.AutoEllipsis = true;
            this.lblUnloadingUnloader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingUnloader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingUnloader.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingUnloader.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingUnloader.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingUnloader.Name = "lblUnloadingUnloader";
            this.lblUnloadingUnloader.Size = new System.Drawing.Size(407, 22);
            this.lblUnloadingUnloader.TabIndex = 9;
            this.lblUnloadingUnloader.Text = "■ Unloader";
            this.lblUnloadingUnloader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btnUnloadingBCIM);
            this.panel14.Controls.Add(this.lblUnloadingB);
            this.panel14.Controls.Add(this.btnUnloadingBCST2Lift);
            this.panel14.Controls.Add(this.btnUnloadingBUnloader2CST);
            this.panel14.Controls.Add(this.btnUnloadingBCSTLift2Buffer);
            this.panel14.Controls.Add(this.btnUnloadingBStage2UnloadingTransfer);
            this.panel14.Controls.Add(this.btnUnloadingBUnloadingTransfer2Unloader);
            this.panel14.Location = new System.Drawing.Point(443, 32);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(409, 343);
            this.panel14.TabIndex = 459;
            // 
            // btnUnloadingBCIM
            // 
            this.btnUnloadingBCIM.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBCIM.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBCIM.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBCIM.Location = new System.Drawing.Point(287, 171);
            this.btnUnloadingBCIM.Name = "btnUnloadingBCIM";
            this.btnUnloadingBCIM.Size = new System.Drawing.Size(107, 30);
            this.btnUnloadingBCIM.TabIndex = 31;
            this.btnUnloadingBCIM.Text = "CIM";
            this.btnUnloadingBCIM.UseVisualStyleBackColor = false;
            // 
            // lblUnloadingB
            // 
            this.lblUnloadingB.AutoEllipsis = true;
            this.lblUnloadingB.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingB.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingB.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingB.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingB.Name = "lblUnloadingB";
            this.lblUnloadingB.Size = new System.Drawing.Size(407, 20);
            this.lblUnloadingB.TabIndex = 9;
            this.lblUnloadingB.Text = "■ B";
            this.lblUnloadingB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnloadingBCST2Lift
            // 
            this.btnUnloadingBCST2Lift.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBCST2Lift.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBCST2Lift.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBCST2Lift.Location = new System.Drawing.Point(12, 27);
            this.btnUnloadingBCST2Lift.Name = "btnUnloadingBCST2Lift";
            this.btnUnloadingBCST2Lift.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingBCST2Lift.TabIndex = 26;
            this.btnUnloadingBCST2Lift.Text = "카세트 → 리프트";
            this.btnUnloadingBCST2Lift.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingBUnloader2CST
            // 
            this.btnUnloadingBUnloader2CST.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBUnloader2CST.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBUnloader2CST.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBUnloader2CST.Location = new System.Drawing.Point(12, 135);
            this.btnUnloadingBUnloader2CST.Name = "btnUnloadingBUnloader2CST";
            this.btnUnloadingBUnloader2CST.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingBUnloader2CST.TabIndex = 29;
            this.btnUnloadingBUnloader2CST.Text = "언로더 → 카세트";
            this.btnUnloadingBUnloader2CST.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingBCSTLift2Buffer
            // 
            this.btnUnloadingBCSTLift2Buffer.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBCSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBCSTLift2Buffer.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBCSTLift2Buffer.Location = new System.Drawing.Point(12, 171);
            this.btnUnloadingBCSTLift2Buffer.Name = "btnUnloadingBCSTLift2Buffer";
            this.btnUnloadingBCSTLift2Buffer.Size = new System.Drawing.Size(269, 30);
            this.btnUnloadingBCSTLift2Buffer.TabIndex = 30;
            this.btnUnloadingBCSTLift2Buffer.Text = "카세트 리프트  → 버퍼";
            this.btnUnloadingBCSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingBStage2UnloadingTransfer
            // 
            this.btnUnloadingBStage2UnloadingTransfer.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBStage2UnloadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBStage2UnloadingTransfer.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBStage2UnloadingTransfer.Location = new System.Drawing.Point(12, 63);
            this.btnUnloadingBStage2UnloadingTransfer.Name = "btnUnloadingBStage2UnloadingTransfer";
            this.btnUnloadingBStage2UnloadingTransfer.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingBStage2UnloadingTransfer.TabIndex = 27;
            this.btnUnloadingBStage2UnloadingTransfer.Text = "스테이지 →  언로딩 트랜스퍼";
            this.btnUnloadingBStage2UnloadingTransfer.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingBUnloadingTransfer2Unloader
            // 
            this.btnUnloadingBUnloadingTransfer2Unloader.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingBUnloadingTransfer2Unloader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingBUnloadingTransfer2Unloader.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingBUnloadingTransfer2Unloader.Location = new System.Drawing.Point(12, 99);
            this.btnUnloadingBUnloadingTransfer2Unloader.Name = "btnUnloadingBUnloadingTransfer2Unloader";
            this.btnUnloadingBUnloadingTransfer2Unloader.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingBUnloadingTransfer2Unloader.TabIndex = 28;
            this.btnUnloadingBUnloadingTransfer2Unloader.Text = "언로딩 트랜스퍼 → 언로더";
            this.btnUnloadingBUnloadingTransfer2Unloader.UseVisualStyleBackColor = false;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnUnloadingACIM);
            this.panel15.Controls.Add(this.btnUnloadingACST2Lift);
            this.panel15.Controls.Add(this.btnUnloadingAUnloader2CST);
            this.panel15.Controls.Add(this.btnUnloadingAStage2UnloadingTransfer);
            this.panel15.Controls.Add(this.btnUnloadingAUnloadingTransfer2Unloader);
            this.panel15.Controls.Add(this.btnUnloadingACSTLift2Buffer);
            this.panel15.Controls.Add(this.lblUnloadingA);
            this.panel15.Location = new System.Drawing.Point(17, 32);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(409, 343);
            this.panel15.TabIndex = 458;
            // 
            // btnUnloadingACIM
            // 
            this.btnUnloadingACIM.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingACIM.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingACIM.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingACIM.Location = new System.Drawing.Point(287, 171);
            this.btnUnloadingACIM.Name = "btnUnloadingACIM";
            this.btnUnloadingACIM.Size = new System.Drawing.Size(107, 30);
            this.btnUnloadingACIM.TabIndex = 25;
            this.btnUnloadingACIM.Text = "CIM";
            this.btnUnloadingACIM.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingACST2Lift
            // 
            this.btnUnloadingACST2Lift.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingACST2Lift.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingACST2Lift.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingACST2Lift.Location = new System.Drawing.Point(12, 27);
            this.btnUnloadingACST2Lift.Name = "btnUnloadingACST2Lift";
            this.btnUnloadingACST2Lift.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingACST2Lift.TabIndex = 20;
            this.btnUnloadingACST2Lift.Text = "카세트 → 리프트";
            this.btnUnloadingACST2Lift.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingAUnloader2CST
            // 
            this.btnUnloadingAUnloader2CST.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAUnloader2CST.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAUnloader2CST.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAUnloader2CST.Location = new System.Drawing.Point(12, 135);
            this.btnUnloadingAUnloader2CST.Name = "btnUnloadingAUnloader2CST";
            this.btnUnloadingAUnloader2CST.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingAUnloader2CST.TabIndex = 23;
            this.btnUnloadingAUnloader2CST.Text = "언로더 → 카세트";
            this.btnUnloadingAUnloader2CST.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingAStage2UnloadingTransfer
            // 
            this.btnUnloadingAStage2UnloadingTransfer.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAStage2UnloadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAStage2UnloadingTransfer.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAStage2UnloadingTransfer.Location = new System.Drawing.Point(12, 63);
            this.btnUnloadingAStage2UnloadingTransfer.Name = "btnUnloadingAStage2UnloadingTransfer";
            this.btnUnloadingAStage2UnloadingTransfer.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingAStage2UnloadingTransfer.TabIndex = 21;
            this.btnUnloadingAStage2UnloadingTransfer.Text = "스테이지 →  언로딩 트랜스퍼";
            this.btnUnloadingAStage2UnloadingTransfer.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingAUnloadingTransfer2Unloader
            // 
            this.btnUnloadingAUnloadingTransfer2Unloader.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingAUnloadingTransfer2Unloader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingAUnloadingTransfer2Unloader.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingAUnloadingTransfer2Unloader.Location = new System.Drawing.Point(12, 99);
            this.btnUnloadingAUnloadingTransfer2Unloader.Name = "btnUnloadingAUnloadingTransfer2Unloader";
            this.btnUnloadingAUnloadingTransfer2Unloader.Size = new System.Drawing.Size(382, 30);
            this.btnUnloadingAUnloadingTransfer2Unloader.TabIndex = 22;
            this.btnUnloadingAUnloadingTransfer2Unloader.Text = "언로딩 트랜스퍼 → 언로더";
            this.btnUnloadingAUnloadingTransfer2Unloader.UseVisualStyleBackColor = false;
            // 
            // btnUnloadingACSTLift2Buffer
            // 
            this.btnUnloadingACSTLift2Buffer.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloadingACSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloadingACSTLift2Buffer.ForeColor = System.Drawing.Color.Black;
            this.btnUnloadingACSTLift2Buffer.Location = new System.Drawing.Point(12, 171);
            this.btnUnloadingACSTLift2Buffer.Name = "btnUnloadingACSTLift2Buffer";
            this.btnUnloadingACSTLift2Buffer.Size = new System.Drawing.Size(269, 30);
            this.btnUnloadingACSTLift2Buffer.TabIndex = 24;
            this.btnUnloadingACSTLift2Buffer.Text = "카세트 리프트  → 버퍼";
            this.btnUnloadingACSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // lblUnloadingA
            // 
            this.lblUnloadingA.AutoEllipsis = true;
            this.lblUnloadingA.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloadingA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloadingA.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloadingA.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadingA.Location = new System.Drawing.Point(0, 0);
            this.lblUnloadingA.Name = "lblUnloadingA";
            this.lblUnloadingA.Size = new System.Drawing.Size(407, 20);
            this.lblUnloadingA.TabIndex = 9;
            this.lblUnloadingA.Text = "■ A";
            this.lblUnloadingA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloading
            // 
            this.lblUnloading.AutoEllipsis = true;
            this.lblUnloading.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloading.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloading.ForeColor = System.Drawing.Color.Black;
            this.lblUnloading.Location = new System.Drawing.Point(0, 0);
            this.lblUnloading.Name = "lblUnloading";
            this.lblUnloading.Size = new System.Drawing.Size(868, 26);
            this.lblUnloading.TabIndex = 9;
            this.lblUnloading.Text = "■ Unloading";
            this.lblUnloading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlManagerControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel13);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerControl";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel13.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Button btnLoadingBFreeAilgn;
        private System.Windows.Forms.Button btnLoadingBCST2Loader;
        private System.Windows.Forms.Button btnLoadingBCST2Stage;
        private System.Windows.Forms.Button btnLoadingBCST2Lift;
        private System.Windows.Forms.Button btnLoadingBCSTLift2Buffer;
        private System.Windows.Forms.Button btnLoadingBLoader2LoadingTransfer;
        private System.Windows.Forms.Button btnLoadingBLoadingTransfer2Stage;
        internal System.Windows.Forms.Label lblLoadingB;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Button btnLoadingAFreeAilgn;
        private System.Windows.Forms.Button btnLoadingACST2Stage;
        private System.Windows.Forms.Button btnLoadingACSTLift2Buffer;
        private System.Windows.Forms.Button btnLoadingALoadingTransfer2Stage;
        private System.Windows.Forms.Button btnLoadingALoader2LoadingTransfer;
        private System.Windows.Forms.Button btnLoadingACST2Loader;
        private System.Windows.Forms.Button btnLoadingACST2Lift;
        internal System.Windows.Forms.Label lblLoadingA;
        internal System.Windows.Forms.Label lblLoading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label lblLoadingLoaderB;
        private System.Windows.Forms.Button btnLoadingLoaderCylinderReset;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox txtLoadingLoaderAGear;
        private System.Windows.Forms.Label lblLoadingLoaderAGear;
        private System.Windows.Forms.Button btnLoadingLoaderACylinderDown;
        private System.Windows.Forms.Button btnLoadingLoaderAVaccumOff;
        private System.Windows.Forms.Button btnLoadingLoaderABlowerOff;
        private System.Windows.Forms.Button btnLoadingLoaderACol24;
        private System.Windows.Forms.Button btnLoadingLoaderACylinderUp;
        private System.Windows.Forms.Button btnLoadingLoaderAVaccumOn;
        private System.Windows.Forms.Button btnLoadingLoaderABlowerOn;
        private System.Windows.Forms.Button btnLoadingLoaderACol13;
        private System.Windows.Forms.Button btnLoadingLoaderAXMove;
        private System.Windows.Forms.Button btnLoadingLoaderAXYMid;
        private System.Windows.Forms.Button btnLoadingLoaderAXYCst;
        private System.Windows.Forms.Button btnLoadingLoaderAUldY;
        internal System.Windows.Forms.Label lblLoadingLoaderA;
        internal System.Windows.Forms.Label lblLoadingLoader;
        private System.Windows.Forms.TextBox txtLoadingLoaderBGear;
        private System.Windows.Forms.Label lblLoadingLoaderBGear;
        private System.Windows.Forms.Button btnLoadingLoaderBCylinderDown;
        private System.Windows.Forms.Button btnLoadingLoaderBUldY;
        private System.Windows.Forms.Button btnLoadingLoaderBXYCst;
        private System.Windows.Forms.Button btnLoadingLoaderBVaccumOff;
        private System.Windows.Forms.Button btnLoadingLoaderBXYMid;
        private System.Windows.Forms.Button btnLoadingLoaderBBlowerOff;
        private System.Windows.Forms.Button btnLoadingLoaderBXMove;
        private System.Windows.Forms.Button btnLoadingLoaderBCol24;
        private System.Windows.Forms.Button btnLoadingLoaderBCol13;
        private System.Windows.Forms.Button btnLoadingLoaderBCylinderUp;
        private System.Windows.Forms.Button btnLoadingLoaderBBlowerOn;
        private System.Windows.Forms.Button btnLoadingLoaderBVaccumOn;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnLoadingLoaderTransferLoadingTransfer2Align;
        private System.Windows.Forms.Button btnLoadingLoaderTransferCylinderReset;
        internal System.Windows.Forms.Label lblLoadingLoaderTransfer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnUnloadingUnloaderTransferCylinderReset;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferABlowerOff;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferABlowerOn;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferAVaccumOff;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferAVaccumOn;
        internal System.Windows.Forms.Label lblUnloadingUnloaderTransferA;
        internal System.Windows.Forms.Label lblUnloadingUnloaderTransfer;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox txtUnloadingUnloaderBGear;
        private System.Windows.Forms.Label lblUnloadingUnloaderBGear;
        private System.Windows.Forms.Button btnUnloadingUnloaderBCylinderDown;
        private System.Windows.Forms.Button btnUnloadingUnloaderBUldY;
        private System.Windows.Forms.Button btnUnloadingUnloaderBXYCst;
        private System.Windows.Forms.Button btnUnloadingUnloaderBVaccumOff;
        private System.Windows.Forms.Button btnUnloadingUnloaderBXYMid;
        private System.Windows.Forms.Button btnUnloadingUnloaderBBlowerOff;
        private System.Windows.Forms.Button btnUnloadingUnloaderBXMove;
        private System.Windows.Forms.Button btnUnloadingUnloaderBCol24;
        private System.Windows.Forms.Button btnUnloadingUnloaderBCol13;
        private System.Windows.Forms.Button btnUnloadingUnloaderBCylinderUp;
        private System.Windows.Forms.Button btnUnloadingUnloaderBBlowerOn;
        private System.Windows.Forms.Button btnUnloadingUnloaderBVaccumOn;
        internal System.Windows.Forms.Label lblUnloadingUnloaderB;
        private System.Windows.Forms.Button btnUnloadingUnloaderCylinderReset;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox txtUnloadingUnloaderAGear;
        private System.Windows.Forms.Button btnUnloadingUnloaderACylinderDown;
        private System.Windows.Forms.Label lblUnloadingUnloaderAGear;
        private System.Windows.Forms.Button btnUnloadingUnloaderAVaccumOff;
        private System.Windows.Forms.Button btnUnloadingUnloaderABlowerOff;
        private System.Windows.Forms.Button btnUnloadingUnloaderACol24;
        private System.Windows.Forms.Button btnUnloadingUnloaderACylinderUp;
        private System.Windows.Forms.Button btnUnloadingUnloaderAVaccumOn;
        private System.Windows.Forms.Button btnUnloadingUnloaderABlowerOn;
        private System.Windows.Forms.Button btnUnloadingUnloaderACol13;
        private System.Windows.Forms.Button btnUnloadingUnloaderAXMove;
        private System.Windows.Forms.Button btnUnloadingUnloaderAXYMid;
        private System.Windows.Forms.Button btnUnloadingUnloaderAXYCst;
        private System.Windows.Forms.Button btnUnloadingUnloaderAUldY;
        internal System.Windows.Forms.Label lblUnloadingUnloaderA;
        internal System.Windows.Forms.Label lblUnloadingUnloader;
        private System.Windows.Forms.Panel panel14;
        internal System.Windows.Forms.Label lblUnloadingB;
        private System.Windows.Forms.Panel panel15;
        internal System.Windows.Forms.Label lblUnloadingA;
        internal System.Windows.Forms.Label lblUnloading;
        private System.Windows.Forms.Button btnUnloadingACIM;
        private System.Windows.Forms.Button btnUnloadingACST2Lift;
        private System.Windows.Forms.Button btnUnloadingAUnloader2CST;
        private System.Windows.Forms.Button btnUnloadingAStage2UnloadingTransfer;
        private System.Windows.Forms.Button btnUnloadingAUnloadingTransfer2Unloader;
        private System.Windows.Forms.Button btnUnloadingACSTLift2Buffer;
        private System.Windows.Forms.Button btnUnloadingBCIM;
        private System.Windows.Forms.Button btnUnloadingBCST2Lift;
        private System.Windows.Forms.Button btnUnloadingBUnloader2CST;
        private System.Windows.Forms.Button btnUnloadingBCSTLift2Buffer;
        private System.Windows.Forms.Button btnUnloadingBStage2UnloadingTransfer;
        private System.Windows.Forms.Button btnUnloadingBUnloadingTransfer2Unloader;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnLoadingLoaderTransferBBlowerOff;
        private System.Windows.Forms.Button btnLoadingLoaderTransferBVaccumOff;
        private System.Windows.Forms.Button btnLoadingLoaderTransferBBlowerOn;
        private System.Windows.Forms.Button btnLoadingLoaderTransferBVaccumOn;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnLoadingLoaderTransferABlowerOff;
        private System.Windows.Forms.Button btnLoadingLoaderTransferABlowerOn;
        private System.Windows.Forms.Button btnLoadingLoaderTransferAVaccumOff;
        private System.Windows.Forms.Button btnLoadingLoaderTransferAVaccumOn;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel19;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnUnloadingAfterTransferBBlowerOff;
        private System.Windows.Forms.Button btnUnloadingAfterTransferBVaccumOff;
        private System.Windows.Forms.Button btnUnloadingAfterTransferBBlowerOn;
        private System.Windows.Forms.Button btnUnloadingAfterTransferBVaccumOn;
        private System.Windows.Forms.Panel panel18;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferBBlowerOff;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferBVaccumOn;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferBBlowerOn;
        private System.Windows.Forms.Button btnUnloadingBeforeTransferBVaccumOff;
        internal System.Windows.Forms.Label lblUnloadingUnloaderTransferB;
        private System.Windows.Forms.Panel panel16;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel17;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUnloadingAfterTransferABlowerOff;
        private System.Windows.Forms.Button btnUnloadingAfterTransferAVaccumOff;
        private System.Windows.Forms.Button btnUnloadingAfterTransferABlowerOn;
        private System.Windows.Forms.Button btnUnloadingAfterTransferAVaccumOn;
    }
}

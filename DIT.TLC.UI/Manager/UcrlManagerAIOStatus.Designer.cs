﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerAIOStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUdAnalog13 = new System.Windows.Forms.Label();
            this.lblLdAnalog16 = new System.Windows.Forms.Label();
            this.lblLdAnalog15 = new System.Windows.Forms.Label();
            this.lblLdAnalog14 = new System.Windows.Forms.Label();
            this.lblLdAnalog13 = new System.Windows.Forms.Label();
            this.lblLdAnalog12 = new System.Windows.Forms.Label();
            this.lblLdAnalog11 = new System.Windows.Forms.Label();
            this.lblLdAnalog10 = new System.Windows.Forms.Label();
            this.lblLdAnalog09 = new System.Windows.Forms.Label();
            this.lblLdAnalog08 = new System.Windows.Forms.Label();
            this.lblUdAnalog03 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue08 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog12 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue07 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog07 = new System.Windows.Forms.Label();
            this.lblLdAnalog06 = new System.Windows.Forms.Label();
            this.lblLdAnalog05 = new System.Windows.Forms.Label();
            this.lblLdAnalog04 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue09 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog04 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue05 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog03 = new System.Windows.Forms.Label();
            this.lblLdAnalog02 = new System.Windows.Forms.Label();
            this.lblLdAnalog01 = new System.Windows.Forms.Label();
            this.txtAnalogValue32 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue31 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue30 = new System.Windows.Forms.TextBox();
            this.txtUdAnalogValue11 = new System.Windows.Forms.TextBox();
            this.txtUdAnalogValue10 = new System.Windows.Forms.TextBox();
            this.txtUdAnalogValue06 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue29 = new System.Windows.Forms.TextBox();
            this.txtUdAnalogValue04 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue28 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue27 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue26 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue25 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue24 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue23 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue22 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog02 = new System.Windows.Forms.Label();
            this.lblUdAnalog11 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue03 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue21 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog01 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue12 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue20 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue19 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue18 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue17 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog32 = new System.Windows.Forms.Label();
            this.txtAnalogValue16 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog31 = new System.Windows.Forms.Label();
            this.txtAnalogValue15 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog30 = new System.Windows.Forms.Label();
            this.txtAnalogValue14 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog15 = new System.Windows.Forms.Label();
            this.lblUdAnalog14 = new System.Windows.Forms.Label();
            this.lblUdAnalog05 = new System.Windows.Forms.Label();
            this.lblLdAnalog29 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue13 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog08 = new System.Windows.Forms.Label();
            this.txtAnalogValue13 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog28 = new System.Windows.Forms.Label();
            this.txtAnalogValue12 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog27 = new System.Windows.Forms.Label();
            this.txtAnalogValue11 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog26 = new System.Windows.Forms.Label();
            this.txtAnalogValue10 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog10 = new System.Windows.Forms.Label();
            this.lblLdAnalog25 = new System.Windows.Forms.Label();
            this.lblLdAnalog24 = new System.Windows.Forms.Label();
            this.txtAnalogValue08 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog23 = new System.Windows.Forms.Label();
            this.txtAnalogValue07 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog22 = new System.Windows.Forms.Label();
            this.lblUdAnalog06 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue01 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog09 = new System.Windows.Forms.Label();
            this.txtAnalogValue09 = new System.Windows.Forms.TextBox();
            this.txtUdAnalogValue02 = new System.Windows.Forms.TextBox();
            this.txtAnalogValue06 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblServoName = new System.Windows.Forms.Label();
            this.lblLdAnalog21 = new System.Windows.Forms.Label();
            this.txtAnalogValue05 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog20 = new System.Windows.Forms.Label();
            this.txtAnalogValue04 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog19 = new System.Windows.Forms.Label();
            this.txtAnalogValue03 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog18 = new System.Windows.Forms.Label();
            this.txtAnalogValue02 = new System.Windows.Forms.TextBox();
            this.lblLdAnalog17 = new System.Windows.Forms.Label();
            this.txtAnalogValue01 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog07 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblProcAnalog01 = new System.Windows.Forms.Label();
            this.lblProcAnalog02 = new System.Windows.Forms.Label();
            this.txtProcAnalogValue02 = new System.Windows.Forms.TextBox();
            this.txtProcAnalogValue01 = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue16 = new System.Windows.Forms.TextBox();
            this.lblUdAnalog16 = new System.Windows.Forms.Label();
            this.txtUdAnalogValue15 = new System.Windows.Forms.TextBox();
            this.txtUdAnalogValue14 = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblUdAnalog13
            // 
            this.lblUdAnalog13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog13.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog13.Location = new System.Drawing.Point(15, 669);
            this.lblUdAnalog13.Name = "lblUdAnalog13";
            this.lblUdAnalog13.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog13.TabIndex = 105;
            this.lblUdAnalog13.Text = "[73] UL/D부 판넬 온도";
            this.lblUdAnalog13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog16
            // 
            this.lblLdAnalog16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog16.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog16.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog16.Location = new System.Drawing.Point(15, 821);
            this.lblLdAnalog16.Name = "lblLdAnalog16";
            this.lblLdAnalog16.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog16.TabIndex = 124;
            this.lblLdAnalog16.Text = "[16] 2 L/D 이재기 2ch 압력";
            this.lblLdAnalog16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog15
            // 
            this.lblLdAnalog15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog15.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog15.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog15.Location = new System.Drawing.Point(15, 770);
            this.lblLdAnalog15.Name = "lblLdAnalog15";
            this.lblLdAnalog15.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog15.TabIndex = 123;
            this.lblLdAnalog15.Text = "[15] 2 L/D 이재기 1ch 압력";
            this.lblLdAnalog15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog14
            // 
            this.lblLdAnalog14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog14.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog14.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog14.Location = new System.Drawing.Point(15, 719);
            this.lblLdAnalog14.Name = "lblLdAnalog14";
            this.lblLdAnalog14.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog14.TabIndex = 122;
            this.lblLdAnalog14.Text = "[14] 1 L/D 이재기 2ch 압력";
            this.lblLdAnalog14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog13
            // 
            this.lblLdAnalog13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog13.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog13.Location = new System.Drawing.Point(15, 668);
            this.lblLdAnalog13.Name = "lblLdAnalog13";
            this.lblLdAnalog13.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog13.TabIndex = 121;
            this.lblLdAnalog13.Text = "[13] 1 L/D 이재기 1ch 압력";
            this.lblLdAnalog13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog12
            // 
            this.lblLdAnalog12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog12.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog12.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog12.Location = new System.Drawing.Point(15, 617);
            this.lblLdAnalog12.Name = "lblLdAnalog12";
            this.lblLdAnalog12.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog12.TabIndex = 120;
            this.lblLdAnalog12.Text = "[12] 4 Breaking Stage 압력";
            this.lblLdAnalog12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog11
            // 
            this.lblLdAnalog11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog11.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog11.Location = new System.Drawing.Point(15, 566);
            this.lblLdAnalog11.Name = "lblLdAnalog11";
            this.lblLdAnalog11.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog11.TabIndex = 119;
            this.lblLdAnalog11.Text = "[11] 3 Breaking Stage 압력";
            this.lblLdAnalog11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog10
            // 
            this.lblLdAnalog10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog10.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog10.Location = new System.Drawing.Point(15, 515);
            this.lblLdAnalog10.Name = "lblLdAnalog10";
            this.lblLdAnalog10.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog10.TabIndex = 118;
            this.lblLdAnalog10.Text = "[10] 2 Breaking Stage 압력";
            this.lblLdAnalog10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog09
            // 
            this.lblLdAnalog09.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog09.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog09.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog09.Location = new System.Drawing.Point(15, 464);
            this.lblLdAnalog09.Name = "lblLdAnalog09";
            this.lblLdAnalog09.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog09.TabIndex = 117;
            this.lblLdAnalog09.Text = "[9] 1 Breaking Stage 압력";
            this.lblLdAnalog09.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog08
            // 
            this.lblLdAnalog08.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog08.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog08.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog08.Location = new System.Drawing.Point(15, 413);
            this.lblLdAnalog08.Name = "lblLdAnalog08";
            this.lblLdAnalog08.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog08.TabIndex = 116;
            this.lblLdAnalog08.Text = "[8] 4 Cutting Stage 2ch 압력";
            this.lblLdAnalog08.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUdAnalog03
            // 
            this.lblUdAnalog03.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog03.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog03.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog03.Location = new System.Drawing.Point(15, 159);
            this.lblUdAnalog03.Name = "lblUdAnalog03";
            this.lblUdAnalog03.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog03.TabIndex = 95;
            this.lblUdAnalog03.Text = "[63] 1 Cassette 투입 이재기 압력";
            this.lblUdAnalog03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue08
            // 
            this.txtUdAnalogValue08.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue08.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue08.Location = new System.Drawing.Point(373, 413);
            this.txtUdAnalogValue08.Name = "txtUdAnalogValue08";
            this.txtUdAnalogValue08.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue08.TabIndex = 132;
            // 
            // lblUdAnalog12
            // 
            this.lblUdAnalog12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog12.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog12.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog12.Location = new System.Drawing.Point(15, 618);
            this.lblUdAnalog12.Name = "lblUdAnalog12";
            this.lblUdAnalog12.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog12.TabIndex = 104;
            this.lblUdAnalog12.Text = "[72] 4 Align, 측정 이재기 2ch 압력";
            this.lblUdAnalog12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue07
            // 
            this.txtUdAnalogValue07.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue07.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue07.Location = new System.Drawing.Point(373, 363);
            this.txtUdAnalogValue07.Name = "txtUdAnalogValue07";
            this.txtUdAnalogValue07.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue07.TabIndex = 131;
            // 
            // lblLdAnalog07
            // 
            this.lblLdAnalog07.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog07.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog07.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog07.Location = new System.Drawing.Point(15, 362);
            this.lblLdAnalog07.Name = "lblLdAnalog07";
            this.lblLdAnalog07.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog07.TabIndex = 115;
            this.lblLdAnalog07.Text = "[7] 4 Cutting Stage 1ch 압력";
            this.lblLdAnalog07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog06
            // 
            this.lblLdAnalog06.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog06.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog06.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog06.Location = new System.Drawing.Point(15, 311);
            this.lblLdAnalog06.Name = "lblLdAnalog06";
            this.lblLdAnalog06.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog06.TabIndex = 114;
            this.lblLdAnalog06.Text = "[6] 3 Cutting Stage 2ch 압력";
            this.lblLdAnalog06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog05
            // 
            this.lblLdAnalog05.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog05.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog05.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog05.Location = new System.Drawing.Point(15, 260);
            this.lblLdAnalog05.Name = "lblLdAnalog05";
            this.lblLdAnalog05.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog05.TabIndex = 113;
            this.lblLdAnalog05.Text = "[5] 3 Cutting Stage 1ch 압력";
            this.lblLdAnalog05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog04
            // 
            this.lblLdAnalog04.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog04.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog04.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog04.Location = new System.Drawing.Point(15, 209);
            this.lblLdAnalog04.Name = "lblLdAnalog04";
            this.lblLdAnalog04.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog04.TabIndex = 112;
            this.lblLdAnalog04.Text = "[4] 2 Cutting Stage 2ch 압력";
            this.lblLdAnalog04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue09
            // 
            this.txtUdAnalogValue09.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue09.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue09.Location = new System.Drawing.Point(373, 464);
            this.txtUdAnalogValue09.Name = "txtUdAnalogValue09";
            this.txtUdAnalogValue09.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue09.TabIndex = 133;
            // 
            // lblUdAnalog04
            // 
            this.lblUdAnalog04.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog04.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog04.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog04.Location = new System.Drawing.Point(15, 210);
            this.lblUdAnalog04.Name = "lblUdAnalog04";
            this.lblUdAnalog04.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog04.TabIndex = 96;
            this.lblUdAnalog04.Text = "[64] 2 Cassette 투입 이재기 압력";
            this.lblUdAnalog04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue05
            // 
            this.txtUdAnalogValue05.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue05.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue05.Location = new System.Drawing.Point(373, 261);
            this.txtUdAnalogValue05.Name = "txtUdAnalogValue05";
            this.txtUdAnalogValue05.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue05.TabIndex = 129;
            // 
            // lblLdAnalog03
            // 
            this.lblLdAnalog03.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog03.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog03.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog03.Location = new System.Drawing.Point(15, 158);
            this.lblLdAnalog03.Name = "lblLdAnalog03";
            this.lblLdAnalog03.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog03.TabIndex = 111;
            this.lblLdAnalog03.Text = "[3] 2 Cutting Stage 1ch 압력";
            this.lblLdAnalog03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog02
            // 
            this.lblLdAnalog02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog02.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog02.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog02.Location = new System.Drawing.Point(15, 106);
            this.lblLdAnalog02.Name = "lblLdAnalog02";
            this.lblLdAnalog02.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog02.TabIndex = 110;
            this.lblLdAnalog02.Text = "[2] 1 Cutting Stage 2ch 압력";
            this.lblLdAnalog02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog01
            // 
            this.lblLdAnalog01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog01.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog01.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog01.Location = new System.Drawing.Point(15, 56);
            this.lblLdAnalog01.Name = "lblLdAnalog01";
            this.lblLdAnalog01.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog01.TabIndex = 109;
            this.lblLdAnalog01.Text = "[1] 1 Cutting Stage 1ch 압력";
            this.lblLdAnalog01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue32
            // 
            this.txtAnalogValue32.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue32.Location = new System.Drawing.Point(669, 820);
            this.txtAnalogValue32.Name = "txtAnalogValue32";
            this.txtAnalogValue32.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue32.TabIndex = 108;
            // 
            // txtAnalogValue31
            // 
            this.txtAnalogValue31.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue31.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue31.Location = new System.Drawing.Point(669, 769);
            this.txtAnalogValue31.Name = "txtAnalogValue31";
            this.txtAnalogValue31.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue31.TabIndex = 107;
            // 
            // txtAnalogValue30
            // 
            this.txtAnalogValue30.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue30.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue30.Location = new System.Drawing.Point(669, 719);
            this.txtAnalogValue30.Name = "txtAnalogValue30";
            this.txtAnalogValue30.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue30.TabIndex = 106;
            // 
            // txtUdAnalogValue11
            // 
            this.txtUdAnalogValue11.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue11.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue11.Location = new System.Drawing.Point(373, 566);
            this.txtUdAnalogValue11.Name = "txtUdAnalogValue11";
            this.txtUdAnalogValue11.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue11.TabIndex = 135;
            // 
            // txtUdAnalogValue10
            // 
            this.txtUdAnalogValue10.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue10.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue10.Location = new System.Drawing.Point(373, 516);
            this.txtUdAnalogValue10.Name = "txtUdAnalogValue10";
            this.txtUdAnalogValue10.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue10.TabIndex = 134;
            // 
            // txtUdAnalogValue06
            // 
            this.txtUdAnalogValue06.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue06.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue06.Location = new System.Drawing.Point(373, 312);
            this.txtUdAnalogValue06.Name = "txtUdAnalogValue06";
            this.txtUdAnalogValue06.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue06.TabIndex = 130;
            // 
            // txtAnalogValue29
            // 
            this.txtAnalogValue29.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue29.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue29.Location = new System.Drawing.Point(669, 667);
            this.txtAnalogValue29.Name = "txtAnalogValue29";
            this.txtAnalogValue29.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue29.TabIndex = 105;
            // 
            // txtUdAnalogValue04
            // 
            this.txtUdAnalogValue04.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue04.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue04.Location = new System.Drawing.Point(373, 210);
            this.txtUdAnalogValue04.Name = "txtUdAnalogValue04";
            this.txtUdAnalogValue04.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue04.TabIndex = 128;
            // 
            // txtAnalogValue28
            // 
            this.txtAnalogValue28.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue28.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue28.Location = new System.Drawing.Point(669, 615);
            this.txtAnalogValue28.Name = "txtAnalogValue28";
            this.txtAnalogValue28.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue28.TabIndex = 104;
            // 
            // txtAnalogValue27
            // 
            this.txtAnalogValue27.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue27.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue27.Location = new System.Drawing.Point(669, 564);
            this.txtAnalogValue27.Name = "txtAnalogValue27";
            this.txtAnalogValue27.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue27.TabIndex = 103;
            // 
            // txtAnalogValue26
            // 
            this.txtAnalogValue26.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue26.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue26.Location = new System.Drawing.Point(669, 513);
            this.txtAnalogValue26.Name = "txtAnalogValue26";
            this.txtAnalogValue26.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue26.TabIndex = 102;
            // 
            // txtAnalogValue25
            // 
            this.txtAnalogValue25.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue25.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue25.Location = new System.Drawing.Point(669, 463);
            this.txtAnalogValue25.Name = "txtAnalogValue25";
            this.txtAnalogValue25.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue25.TabIndex = 101;
            // 
            // txtAnalogValue24
            // 
            this.txtAnalogValue24.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue24.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue24.Location = new System.Drawing.Point(669, 412);
            this.txtAnalogValue24.Name = "txtAnalogValue24";
            this.txtAnalogValue24.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue24.TabIndex = 100;
            // 
            // txtAnalogValue23
            // 
            this.txtAnalogValue23.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue23.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue23.Location = new System.Drawing.Point(669, 361);
            this.txtAnalogValue23.Name = "txtAnalogValue23";
            this.txtAnalogValue23.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue23.TabIndex = 99;
            // 
            // txtAnalogValue22
            // 
            this.txtAnalogValue22.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue22.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue22.Location = new System.Drawing.Point(669, 310);
            this.txtAnalogValue22.Name = "txtAnalogValue22";
            this.txtAnalogValue22.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue22.TabIndex = 98;
            // 
            // lblUdAnalog02
            // 
            this.lblUdAnalog02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog02.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog02.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog02.Location = new System.Drawing.Point(15, 107);
            this.lblUdAnalog02.Name = "lblUdAnalog02";
            this.lblUdAnalog02.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog02.TabIndex = 94;
            this.lblUdAnalog02.Text = "[62] Spare";
            this.lblUdAnalog02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUdAnalog11
            // 
            this.lblUdAnalog11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog11.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog11.Location = new System.Drawing.Point(15, 567);
            this.lblUdAnalog11.Name = "lblUdAnalog11";
            this.lblUdAnalog11.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog11.TabIndex = 103;
            this.lblUdAnalog11.Text = "[71] 4 Align, 측정 이재기 1ch 압력";
            this.lblUdAnalog11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue03
            // 
            this.txtUdAnalogValue03.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue03.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue03.Location = new System.Drawing.Point(373, 158);
            this.txtUdAnalogValue03.Name = "txtUdAnalogValue03";
            this.txtUdAnalogValue03.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue03.TabIndex = 127;
            // 
            // txtAnalogValue21
            // 
            this.txtAnalogValue21.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue21.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue21.Location = new System.Drawing.Point(669, 258);
            this.txtAnalogValue21.Name = "txtAnalogValue21";
            this.txtAnalogValue21.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue21.TabIndex = 97;
            // 
            // lblUdAnalog01
            // 
            this.lblUdAnalog01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog01.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog01.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog01.Location = new System.Drawing.Point(15, 56);
            this.lblUdAnalog01.Name = "lblUdAnalog01";
            this.lblUdAnalog01.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog01.TabIndex = 93;
            this.lblUdAnalog01.Text = "[61] 언로딩 버퍼 1ch 파기";
            this.lblUdAnalog01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue12
            // 
            this.txtUdAnalogValue12.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue12.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue12.Location = new System.Drawing.Point(373, 617);
            this.txtUdAnalogValue12.Name = "txtUdAnalogValue12";
            this.txtUdAnalogValue12.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue12.TabIndex = 136;
            // 
            // txtAnalogValue20
            // 
            this.txtAnalogValue20.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue20.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue20.Location = new System.Drawing.Point(669, 207);
            this.txtAnalogValue20.Name = "txtAnalogValue20";
            this.txtAnalogValue20.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue20.TabIndex = 96;
            // 
            // txtAnalogValue19
            // 
            this.txtAnalogValue19.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue19.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue19.Location = new System.Drawing.Point(669, 158);
            this.txtAnalogValue19.Name = "txtAnalogValue19";
            this.txtAnalogValue19.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue19.TabIndex = 95;
            // 
            // txtAnalogValue18
            // 
            this.txtAnalogValue18.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue18.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue18.Location = new System.Drawing.Point(669, 107);
            this.txtAnalogValue18.Name = "txtAnalogValue18";
            this.txtAnalogValue18.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue18.TabIndex = 94;
            // 
            // txtAnalogValue17
            // 
            this.txtAnalogValue17.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue17.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue17.Location = new System.Drawing.Point(669, 56);
            this.txtAnalogValue17.Name = "txtAnalogValue17";
            this.txtAnalogValue17.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue17.TabIndex = 93;
            // 
            // lblLdAnalog32
            // 
            this.lblLdAnalog32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog32.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog32.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog32.Location = new System.Drawing.Point(408, 820);
            this.lblLdAnalog32.Name = "lblLdAnalog32";
            this.lblLdAnalog32.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog32.TabIndex = 91;
            this.lblLdAnalog32.Text = "[36] Spare";
            this.lblLdAnalog32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue16
            // 
            this.txtAnalogValue16.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue16.Location = new System.Drawing.Point(279, 821);
            this.txtAnalogValue16.Name = "txtAnalogValue16";
            this.txtAnalogValue16.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue16.TabIndex = 92;
            // 
            // lblLdAnalog31
            // 
            this.lblLdAnalog31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog31.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog31.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog31.Location = new System.Drawing.Point(408, 769);
            this.lblLdAnalog31.Name = "lblLdAnalog31";
            this.lblLdAnalog31.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog31.TabIndex = 89;
            this.lblLdAnalog31.Text = "[35] Spare";
            this.lblLdAnalog31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue15
            // 
            this.txtAnalogValue15.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue15.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue15.Location = new System.Drawing.Point(279, 770);
            this.txtAnalogValue15.Name = "txtAnalogValue15";
            this.txtAnalogValue15.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue15.TabIndex = 90;
            // 
            // lblLdAnalog30
            // 
            this.lblLdAnalog30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog30.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog30.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog30.Location = new System.Drawing.Point(408, 718);
            this.lblLdAnalog30.Name = "lblLdAnalog30";
            this.lblLdAnalog30.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog30.TabIndex = 87;
            this.lblLdAnalog30.Text = "[34] 2 Cassette 취출 이재기 압력";
            this.lblLdAnalog30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue14
            // 
            this.txtAnalogValue14.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue14.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue14.Location = new System.Drawing.Point(279, 719);
            this.txtAnalogValue14.Name = "txtAnalogValue14";
            this.txtAnalogValue14.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue14.TabIndex = 88;
            // 
            // lblUdAnalog15
            // 
            this.lblUdAnalog15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog15.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog15.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog15.Location = new System.Drawing.Point(15, 771);
            this.lblUdAnalog15.Name = "lblUdAnalog15";
            this.lblUdAnalog15.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog15.TabIndex = 107;
            this.lblUdAnalog15.Text = "[75] Spare";
            this.lblUdAnalog15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUdAnalog14
            // 
            this.lblUdAnalog14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog14.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog14.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog14.Location = new System.Drawing.Point(15, 720);
            this.lblUdAnalog14.Name = "lblUdAnalog14";
            this.lblUdAnalog14.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog14.TabIndex = 106;
            this.lblUdAnalog14.Text = "[74] PC Rack 온도";
            this.lblUdAnalog14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUdAnalog05
            // 
            this.lblUdAnalog05.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog05.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog05.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog05.Location = new System.Drawing.Point(15, 261);
            this.lblUdAnalog05.Name = "lblUdAnalog05";
            this.lblUdAnalog05.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog05.TabIndex = 97;
            this.lblUdAnalog05.Text = "[65] 1 Align, 측정 이재기 1ch 압력";
            this.lblUdAnalog05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog29
            // 
            this.lblLdAnalog29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog29.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog29.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog29.Location = new System.Drawing.Point(408, 667);
            this.lblLdAnalog29.Name = "lblLdAnalog29";
            this.lblLdAnalog29.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog29.TabIndex = 85;
            this.lblLdAnalog29.Text = "[33] 1 Cassette 취출 이재기 압력";
            this.lblLdAnalog29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue13
            // 
            this.txtUdAnalogValue13.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue13.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue13.Location = new System.Drawing.Point(373, 669);
            this.txtUdAnalogValue13.Name = "txtUdAnalogValue13";
            this.txtUdAnalogValue13.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue13.TabIndex = 137;
            // 
            // lblUdAnalog08
            // 
            this.lblUdAnalog08.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog08.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog08.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog08.Location = new System.Drawing.Point(15, 414);
            this.lblUdAnalog08.Name = "lblUdAnalog08";
            this.lblUdAnalog08.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog08.TabIndex = 100;
            this.lblUdAnalog08.Text = "[68] 2 Align, 측정 이재기 2ch 압력";
            this.lblUdAnalog08.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue13
            // 
            this.txtAnalogValue13.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue13.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue13.Location = new System.Drawing.Point(279, 668);
            this.txtAnalogValue13.Name = "txtAnalogValue13";
            this.txtAnalogValue13.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue13.TabIndex = 86;
            // 
            // lblLdAnalog28
            // 
            this.lblLdAnalog28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog28.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog28.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog28.Location = new System.Drawing.Point(408, 616);
            this.lblLdAnalog28.Name = "lblLdAnalog28";
            this.lblLdAnalog28.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog28.TabIndex = 83;
            this.lblLdAnalog28.Text = "[32] 가공부 판넬 온도";
            this.lblLdAnalog28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue12
            // 
            this.txtAnalogValue12.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue12.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue12.Location = new System.Drawing.Point(279, 617);
            this.txtAnalogValue12.Name = "txtAnalogValue12";
            this.txtAnalogValue12.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue12.TabIndex = 84;
            // 
            // lblLdAnalog27
            // 
            this.lblLdAnalog27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog27.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog27.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog27.Location = new System.Drawing.Point(408, 565);
            this.lblLdAnalog27.Name = "lblLdAnalog27";
            this.lblLdAnalog27.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog27.TabIndex = 81;
            this.lblLdAnalog27.Text = "[31] L/D부 판넬 온도";
            this.lblLdAnalog27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue11
            // 
            this.txtAnalogValue11.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue11.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue11.Location = new System.Drawing.Point(279, 566);
            this.txtAnalogValue11.Name = "txtAnalogValue11";
            this.txtAnalogValue11.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue11.TabIndex = 82;
            // 
            // lblLdAnalog26
            // 
            this.lblLdAnalog26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog26.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog26.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog26.Location = new System.Drawing.Point(408, 514);
            this.lblLdAnalog26.Name = "lblLdAnalog26";
            this.lblLdAnalog26.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog26.TabIndex = 79;
            this.lblLdAnalog26.Text = "[30] 3 Main AIR Flowmeter Data";
            this.lblLdAnalog26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue10
            // 
            this.txtAnalogValue10.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue10.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue10.Location = new System.Drawing.Point(279, 514);
            this.txtAnalogValue10.Name = "txtAnalogValue10";
            this.txtAnalogValue10.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue10.TabIndex = 80;
            // 
            // lblUdAnalog10
            // 
            this.lblUdAnalog10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog10.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog10.Location = new System.Drawing.Point(15, 516);
            this.lblUdAnalog10.Name = "lblUdAnalog10";
            this.lblUdAnalog10.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog10.TabIndex = 102;
            this.lblUdAnalog10.Text = "[70] 3 Align, 측정 이재기 2ch 압력";
            this.lblUdAnalog10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog25
            // 
            this.lblLdAnalog25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog25.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog25.Location = new System.Drawing.Point(408, 463);
            this.lblLdAnalog25.Name = "lblLdAnalog25";
            this.lblLdAnalog25.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog25.TabIndex = 77;
            this.lblLdAnalog25.Text = "[29] 3 Main Pressure Data_이젝터";
            this.lblLdAnalog25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog24
            // 
            this.lblLdAnalog24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog24.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog24.Location = new System.Drawing.Point(408, 412);
            this.lblLdAnalog24.Name = "lblLdAnalog24";
            this.lblLdAnalog24.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog24.TabIndex = 75;
            this.lblLdAnalog24.Text = "[28] 2 Main AIR Flowmeter Data";
            this.lblLdAnalog24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue08
            // 
            this.txtAnalogValue08.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue08.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue08.Location = new System.Drawing.Point(279, 413);
            this.txtAnalogValue08.Name = "txtAnalogValue08";
            this.txtAnalogValue08.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue08.TabIndex = 76;
            // 
            // lblLdAnalog23
            // 
            this.lblLdAnalog23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog23.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog23.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog23.Location = new System.Drawing.Point(408, 361);
            this.lblLdAnalog23.Name = "lblLdAnalog23";
            this.lblLdAnalog23.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog23.TabIndex = 73;
            this.lblLdAnalog23.Text = "[27] 2 Main 압력 Data_Ionizer, Blow";
            this.lblLdAnalog23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue07
            // 
            this.txtAnalogValue07.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue07.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue07.Location = new System.Drawing.Point(279, 362);
            this.txtAnalogValue07.Name = "txtAnalogValue07";
            this.txtAnalogValue07.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue07.TabIndex = 74;
            // 
            // lblLdAnalog22
            // 
            this.lblLdAnalog22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog22.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog22.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog22.Location = new System.Drawing.Point(408, 310);
            this.lblLdAnalog22.Name = "lblLdAnalog22";
            this.lblLdAnalog22.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog22.TabIndex = 71;
            this.lblLdAnalog22.Text = "[26] 1 Main AIR Flowmeter Data";
            this.lblLdAnalog22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUdAnalog06
            // 
            this.lblUdAnalog06.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog06.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog06.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog06.Location = new System.Drawing.Point(15, 312);
            this.lblUdAnalog06.Name = "lblUdAnalog06";
            this.lblUdAnalog06.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog06.TabIndex = 98;
            this.lblUdAnalog06.Text = "[66] 1 Align, 측정 이재기 2ch 압력";
            this.lblUdAnalog06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue01
            // 
            this.txtUdAnalogValue01.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue01.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue01.Location = new System.Drawing.Point(373, 56);
            this.txtUdAnalogValue01.Name = "txtUdAnalogValue01";
            this.txtUdAnalogValue01.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue01.TabIndex = 125;
            // 
            // lblUdAnalog09
            // 
            this.lblUdAnalog09.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog09.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog09.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog09.Location = new System.Drawing.Point(15, 465);
            this.lblUdAnalog09.Name = "lblUdAnalog09";
            this.lblUdAnalog09.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog09.TabIndex = 101;
            this.lblUdAnalog09.Text = "[69] 3 Align, 측정 이재기 1ch 압력";
            this.lblUdAnalog09.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue09
            // 
            this.txtAnalogValue09.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue09.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue09.Location = new System.Drawing.Point(279, 464);
            this.txtAnalogValue09.Name = "txtAnalogValue09";
            this.txtAnalogValue09.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue09.TabIndex = 78;
            // 
            // txtUdAnalogValue02
            // 
            this.txtUdAnalogValue02.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue02.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue02.Location = new System.Drawing.Point(373, 107);
            this.txtUdAnalogValue02.Name = "txtUdAnalogValue02";
            this.txtUdAnalogValue02.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue02.TabIndex = 126;
            // 
            // txtAnalogValue06
            // 
            this.txtAnalogValue06.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue06.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue06.Location = new System.Drawing.Point(279, 311);
            this.txtAnalogValue06.Name = "txtAnalogValue06";
            this.txtAnalogValue06.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue06.TabIndex = 72;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblServoName);
            this.panel2.Controls.Add(this.lblLdAnalog16);
            this.panel2.Controls.Add(this.lblLdAnalog15);
            this.panel2.Controls.Add(this.lblLdAnalog14);
            this.panel2.Controls.Add(this.lblLdAnalog13);
            this.panel2.Controls.Add(this.lblLdAnalog12);
            this.panel2.Controls.Add(this.lblLdAnalog11);
            this.panel2.Controls.Add(this.lblLdAnalog10);
            this.panel2.Controls.Add(this.lblLdAnalog09);
            this.panel2.Controls.Add(this.lblLdAnalog08);
            this.panel2.Controls.Add(this.lblLdAnalog07);
            this.panel2.Controls.Add(this.lblLdAnalog06);
            this.panel2.Controls.Add(this.lblLdAnalog05);
            this.panel2.Controls.Add(this.lblLdAnalog04);
            this.panel2.Controls.Add(this.lblLdAnalog03);
            this.panel2.Controls.Add(this.lblLdAnalog02);
            this.panel2.Controls.Add(this.lblLdAnalog01);
            this.panel2.Controls.Add(this.txtAnalogValue32);
            this.panel2.Controls.Add(this.txtAnalogValue31);
            this.panel2.Controls.Add(this.txtAnalogValue30);
            this.panel2.Controls.Add(this.txtAnalogValue29);
            this.panel2.Controls.Add(this.txtAnalogValue28);
            this.panel2.Controls.Add(this.txtAnalogValue27);
            this.panel2.Controls.Add(this.txtAnalogValue26);
            this.panel2.Controls.Add(this.txtAnalogValue25);
            this.panel2.Controls.Add(this.txtAnalogValue24);
            this.panel2.Controls.Add(this.txtAnalogValue23);
            this.panel2.Controls.Add(this.txtAnalogValue22);
            this.panel2.Controls.Add(this.txtAnalogValue21);
            this.panel2.Controls.Add(this.txtAnalogValue20);
            this.panel2.Controls.Add(this.txtAnalogValue19);
            this.panel2.Controls.Add(this.txtAnalogValue18);
            this.panel2.Controls.Add(this.txtAnalogValue17);
            this.panel2.Controls.Add(this.lblLdAnalog32);
            this.panel2.Controls.Add(this.txtAnalogValue16);
            this.panel2.Controls.Add(this.lblLdAnalog31);
            this.panel2.Controls.Add(this.txtAnalogValue15);
            this.panel2.Controls.Add(this.lblLdAnalog30);
            this.panel2.Controls.Add(this.txtAnalogValue14);
            this.panel2.Controls.Add(this.lblLdAnalog29);
            this.panel2.Controls.Add(this.txtAnalogValue13);
            this.panel2.Controls.Add(this.lblLdAnalog28);
            this.panel2.Controls.Add(this.txtAnalogValue12);
            this.panel2.Controls.Add(this.lblLdAnalog27);
            this.panel2.Controls.Add(this.txtAnalogValue11);
            this.panel2.Controls.Add(this.lblLdAnalog26);
            this.panel2.Controls.Add(this.txtAnalogValue10);
            this.panel2.Controls.Add(this.lblLdAnalog25);
            this.panel2.Controls.Add(this.txtAnalogValue09);
            this.panel2.Controls.Add(this.lblLdAnalog24);
            this.panel2.Controls.Add(this.txtAnalogValue08);
            this.panel2.Controls.Add(this.lblLdAnalog23);
            this.panel2.Controls.Add(this.txtAnalogValue07);
            this.panel2.Controls.Add(this.lblLdAnalog22);
            this.panel2.Controls.Add(this.txtAnalogValue06);
            this.panel2.Controls.Add(this.lblLdAnalog21);
            this.panel2.Controls.Add(this.txtAnalogValue05);
            this.panel2.Controls.Add(this.lblLdAnalog20);
            this.panel2.Controls.Add(this.txtAnalogValue04);
            this.panel2.Controls.Add(this.lblLdAnalog19);
            this.panel2.Controls.Add(this.txtAnalogValue03);
            this.panel2.Controls.Add(this.lblLdAnalog18);
            this.panel2.Controls.Add(this.txtAnalogValue02);
            this.panel2.Controls.Add(this.lblLdAnalog17);
            this.panel2.Controls.Add(this.txtAnalogValue01);
            this.panel2.Location = new System.Drawing.Point(4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(781, 869);
            this.panel2.TabIndex = 62;
            // 
            // lblServoName
            // 
            this.lblServoName.AutoEllipsis = true;
            this.lblServoName.BackColor = System.Drawing.Color.Gainsboro;
            this.lblServoName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblServoName.Font = new System.Drawing.Font("맑은 고딕", 9.75F);
            this.lblServoName.ForeColor = System.Drawing.Color.Black;
            this.lblServoName.Location = new System.Drawing.Point(0, 0);
            this.lblServoName.Name = "lblServoName";
            this.lblServoName.Size = new System.Drawing.Size(779, 47);
            this.lblServoName.TabIndex = 125;
            this.lblServoName.Text = "■ 로더";
            this.lblServoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLdAnalog21
            // 
            this.lblLdAnalog21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog21.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog21.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog21.Location = new System.Drawing.Point(408, 259);
            this.lblLdAnalog21.Name = "lblLdAnalog21";
            this.lblLdAnalog21.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog21.TabIndex = 69;
            this.lblLdAnalog21.Text = "[25] 1 Main 압력 Data_Cylinder";
            this.lblLdAnalog21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue05
            // 
            this.txtAnalogValue05.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue05.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue05.Location = new System.Drawing.Point(279, 260);
            this.txtAnalogValue05.Name = "txtAnalogValue05";
            this.txtAnalogValue05.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue05.TabIndex = 70;
            // 
            // lblLdAnalog20
            // 
            this.lblLdAnalog20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog20.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog20.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog20.Location = new System.Drawing.Point(408, 208);
            this.lblLdAnalog20.Name = "lblLdAnalog20";
            this.lblLdAnalog20.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog20.TabIndex = 67;
            this.lblLdAnalog20.Text = "[24] 2 After IR Cut 이재기 2ch 압력";
            this.lblLdAnalog20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue04
            // 
            this.txtAnalogValue04.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue04.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue04.Location = new System.Drawing.Point(279, 209);
            this.txtAnalogValue04.Name = "txtAnalogValue04";
            this.txtAnalogValue04.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue04.TabIndex = 68;
            // 
            // lblLdAnalog19
            // 
            this.lblLdAnalog19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog19.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog19.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog19.Location = new System.Drawing.Point(408, 157);
            this.lblLdAnalog19.Name = "lblLdAnalog19";
            this.lblLdAnalog19.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog19.TabIndex = 65;
            this.lblLdAnalog19.Text = "[23] 2 After IR Cut 이재기 1ch 압력";
            this.lblLdAnalog19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue03
            // 
            this.txtAnalogValue03.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue03.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue03.Location = new System.Drawing.Point(279, 157);
            this.txtAnalogValue03.Name = "txtAnalogValue03";
            this.txtAnalogValue03.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue03.TabIndex = 66;
            // 
            // lblLdAnalog18
            // 
            this.lblLdAnalog18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog18.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog18.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog18.Location = new System.Drawing.Point(408, 106);
            this.lblLdAnalog18.Name = "lblLdAnalog18";
            this.lblLdAnalog18.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog18.TabIndex = 63;
            this.lblLdAnalog18.Text = "[22] 1 After IR Cut 이재기 2ch 압력";
            this.lblLdAnalog18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue02
            // 
            this.txtAnalogValue02.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue02.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue02.Location = new System.Drawing.Point(279, 107);
            this.txtAnalogValue02.Name = "txtAnalogValue02";
            this.txtAnalogValue02.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue02.TabIndex = 64;
            // 
            // lblLdAnalog17
            // 
            this.lblLdAnalog17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLdAnalog17.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLdAnalog17.ForeColor = System.Drawing.Color.Black;
            this.lblLdAnalog17.Location = new System.Drawing.Point(408, 56);
            this.lblLdAnalog17.Name = "lblLdAnalog17";
            this.lblLdAnalog17.Size = new System.Drawing.Size(255, 37);
            this.lblLdAnalog17.TabIndex = 61;
            this.lblLdAnalog17.Text = "[21] 1 After IR Cut 이재기 1ch 압력";
            this.lblLdAnalog17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnalogValue01
            // 
            this.txtAnalogValue01.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAnalogValue01.ForeColor = System.Drawing.Color.Black;
            this.txtAnalogValue01.Location = new System.Drawing.Point(279, 56);
            this.txtAnalogValue01.Name = "txtAnalogValue01";
            this.txtAnalogValue01.Size = new System.Drawing.Size(95, 39);
            this.txtAnalogValue01.TabIndex = 62;
            // 
            // lblUdAnalog07
            // 
            this.lblUdAnalog07.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog07.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog07.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog07.Location = new System.Drawing.Point(15, 363);
            this.lblUdAnalog07.Name = "lblUdAnalog07";
            this.lblUdAnalog07.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog07.TabIndex = 99;
            this.lblUdAnalog07.Text = "[67] 2 Align, 측정 이재기 1ch 압력";
            this.lblUdAnalog07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblProcAnalog01);
            this.panel1.Controls.Add(this.lblProcAnalog02);
            this.panel1.Controls.Add(this.txtProcAnalogValue02);
            this.panel1.Controls.Add(this.txtProcAnalogValue01);
            this.panel1.Location = new System.Drawing.Point(791, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(461, 869);
            this.panel1.TabIndex = 63;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(459, 47);
            this.label1.TabIndex = 145;
            this.label1.Text = "■ 프로세스";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcAnalog01
            // 
            this.lblProcAnalog01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcAnalog01.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProcAnalog01.ForeColor = System.Drawing.Color.Black;
            this.lblProcAnalog01.Location = new System.Drawing.Point(15, 56);
            this.lblProcAnalog01.Name = "lblProcAnalog01";
            this.lblProcAnalog01.Size = new System.Drawing.Size(330, 37);
            this.lblProcAnalog01.TabIndex = 141;
            this.lblProcAnalog01.Text = "LDS 가공부";
            this.lblProcAnalog01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcAnalog02
            // 
            this.lblProcAnalog02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcAnalog02.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProcAnalog02.ForeColor = System.Drawing.Color.Black;
            this.lblProcAnalog02.Location = new System.Drawing.Point(15, 107);
            this.lblProcAnalog02.Name = "lblProcAnalog02";
            this.lblProcAnalog02.Size = new System.Drawing.Size(330, 37);
            this.lblProcAnalog02.TabIndex = 142;
            this.lblProcAnalog02.Text = "LDS 브레이킹부";
            this.lblProcAnalog02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProcAnalogValue02
            // 
            this.txtProcAnalogValue02.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtProcAnalogValue02.ForeColor = System.Drawing.Color.Black;
            this.txtProcAnalogValue02.Location = new System.Drawing.Point(351, 106);
            this.txtProcAnalogValue02.Name = "txtProcAnalogValue02";
            this.txtProcAnalogValue02.Size = new System.Drawing.Size(95, 39);
            this.txtProcAnalogValue02.TabIndex = 144;
            // 
            // txtProcAnalogValue01
            // 
            this.txtProcAnalogValue01.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtProcAnalogValue01.ForeColor = System.Drawing.Color.Black;
            this.txtProcAnalogValue01.Location = new System.Drawing.Point(351, 56);
            this.txtProcAnalogValue01.Name = "txtProcAnalogValue01";
            this.txtProcAnalogValue01.Size = new System.Drawing.Size(95, 39);
            this.txtProcAnalogValue01.TabIndex = 143;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.txtUdAnalogValue16);
            this.panel5.Controls.Add(this.lblUdAnalog16);
            this.panel5.Controls.Add(this.txtUdAnalogValue15);
            this.panel5.Controls.Add(this.txtUdAnalogValue14);
            this.panel5.Controls.Add(this.lblUdAnalog15);
            this.panel5.Controls.Add(this.txtUdAnalogValue13);
            this.panel5.Controls.Add(this.lblUdAnalog01);
            this.panel5.Controls.Add(this.txtUdAnalogValue12);
            this.panel5.Controls.Add(this.lblUdAnalog14);
            this.panel5.Controls.Add(this.txtUdAnalogValue11);
            this.panel5.Controls.Add(this.lblUdAnalog02);
            this.panel5.Controls.Add(this.txtUdAnalogValue10);
            this.panel5.Controls.Add(this.lblUdAnalog13);
            this.panel5.Controls.Add(this.txtUdAnalogValue09);
            this.panel5.Controls.Add(this.lblUdAnalog03);
            this.panel5.Controls.Add(this.txtUdAnalogValue08);
            this.panel5.Controls.Add(this.lblUdAnalog12);
            this.panel5.Controls.Add(this.txtUdAnalogValue07);
            this.panel5.Controls.Add(this.lblUdAnalog04);
            this.panel5.Controls.Add(this.txtUdAnalogValue06);
            this.panel5.Controls.Add(this.txtUdAnalogValue05);
            this.panel5.Controls.Add(this.lblUdAnalog11);
            this.panel5.Controls.Add(this.txtUdAnalogValue04);
            this.panel5.Controls.Add(this.lblUdAnalog05);
            this.panel5.Controls.Add(this.txtUdAnalogValue03);
            this.panel5.Controls.Add(this.lblUdAnalog10);
            this.panel5.Controls.Add(this.txtUdAnalogValue02);
            this.panel5.Controls.Add(this.lblUdAnalog06);
            this.panel5.Controls.Add(this.txtUdAnalogValue01);
            this.panel5.Controls.Add(this.lblUdAnalog09);
            this.panel5.Controls.Add(this.lblUdAnalog07);
            this.panel5.Controls.Add(this.lblUdAnalog08);
            this.panel5.Location = new System.Drawing.Point(1256, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(480, 869);
            this.panel5.TabIndex = 64;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(478, 47);
            this.label2.TabIndex = 141;
            this.label2.Text = "■ 언로더";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue16
            // 
            this.txtUdAnalogValue16.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue16.Location = new System.Drawing.Point(373, 821);
            this.txtUdAnalogValue16.Name = "txtUdAnalogValue16";
            this.txtUdAnalogValue16.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue16.TabIndex = 140;
            // 
            // lblUdAnalog16
            // 
            this.lblUdAnalog16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUdAnalog16.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUdAnalog16.ForeColor = System.Drawing.Color.Black;
            this.lblUdAnalog16.Location = new System.Drawing.Point(15, 822);
            this.lblUdAnalog16.Name = "lblUdAnalog16";
            this.lblUdAnalog16.Size = new System.Drawing.Size(352, 37);
            this.lblUdAnalog16.TabIndex = 108;
            this.lblUdAnalog16.Text = "[76] Spare";
            this.lblUdAnalog16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUdAnalogValue15
            // 
            this.txtUdAnalogValue15.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue15.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue15.Location = new System.Drawing.Point(373, 770);
            this.txtUdAnalogValue15.Name = "txtUdAnalogValue15";
            this.txtUdAnalogValue15.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue15.TabIndex = 139;
            // 
            // txtUdAnalogValue14
            // 
            this.txtUdAnalogValue14.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtUdAnalogValue14.ForeColor = System.Drawing.Color.Black;
            this.txtUdAnalogValue14.Location = new System.Drawing.Point(373, 719);
            this.txtUdAnalogValue14.Name = "txtUdAnalogValue14";
            this.txtUdAnalogValue14.Size = new System.Drawing.Size(95, 39);
            this.txtUdAnalogValue14.TabIndex = 138;
            // 
            // UcrlManagerAIOStatus
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerAIOStatus";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblUdAnalog13;
        private System.Windows.Forms.Label lblLdAnalog16;
        private System.Windows.Forms.Label lblLdAnalog15;
        private System.Windows.Forms.Label lblLdAnalog14;
        private System.Windows.Forms.Label lblLdAnalog13;
        private System.Windows.Forms.Label lblLdAnalog12;
        private System.Windows.Forms.Label lblLdAnalog11;
        private System.Windows.Forms.Label lblLdAnalog10;
        private System.Windows.Forms.Label lblLdAnalog09;
        private System.Windows.Forms.Label lblLdAnalog08;
        private System.Windows.Forms.Label lblUdAnalog03;
        private System.Windows.Forms.TextBox txtUdAnalogValue08;
        private System.Windows.Forms.Label lblUdAnalog12;
        private System.Windows.Forms.TextBox txtUdAnalogValue07;
        private System.Windows.Forms.Label lblLdAnalog07;
        private System.Windows.Forms.Label lblLdAnalog06;
        private System.Windows.Forms.Label lblLdAnalog05;
        private System.Windows.Forms.Label lblLdAnalog04;
        private System.Windows.Forms.TextBox txtUdAnalogValue09;
        private System.Windows.Forms.Label lblUdAnalog04;
        private System.Windows.Forms.TextBox txtUdAnalogValue05;
        private System.Windows.Forms.Label lblLdAnalog03;
        private System.Windows.Forms.Label lblLdAnalog02;
        private System.Windows.Forms.Label lblLdAnalog01;
        private System.Windows.Forms.TextBox txtAnalogValue32;
        private System.Windows.Forms.TextBox txtAnalogValue31;
        private System.Windows.Forms.TextBox txtAnalogValue30;
        private System.Windows.Forms.TextBox txtUdAnalogValue11;
        private System.Windows.Forms.TextBox txtUdAnalogValue10;
        private System.Windows.Forms.TextBox txtUdAnalogValue06;
        private System.Windows.Forms.TextBox txtAnalogValue29;
        private System.Windows.Forms.TextBox txtUdAnalogValue04;
        private System.Windows.Forms.TextBox txtAnalogValue28;
        private System.Windows.Forms.TextBox txtAnalogValue27;
        private System.Windows.Forms.TextBox txtAnalogValue26;
        private System.Windows.Forms.TextBox txtAnalogValue25;
        private System.Windows.Forms.TextBox txtAnalogValue24;
        private System.Windows.Forms.TextBox txtAnalogValue23;
        private System.Windows.Forms.TextBox txtAnalogValue22;
        private System.Windows.Forms.Label lblUdAnalog02;
        private System.Windows.Forms.Label lblUdAnalog11;
        private System.Windows.Forms.TextBox txtUdAnalogValue03;
        private System.Windows.Forms.TextBox txtAnalogValue21;
        private System.Windows.Forms.Label lblUdAnalog01;
        private System.Windows.Forms.TextBox txtUdAnalogValue12;
        private System.Windows.Forms.TextBox txtAnalogValue20;
        private System.Windows.Forms.TextBox txtAnalogValue19;
        private System.Windows.Forms.TextBox txtAnalogValue18;
        private System.Windows.Forms.TextBox txtAnalogValue17;
        private System.Windows.Forms.Label lblLdAnalog32;
        private System.Windows.Forms.TextBox txtAnalogValue16;
        private System.Windows.Forms.Label lblLdAnalog31;
        private System.Windows.Forms.TextBox txtAnalogValue15;
        private System.Windows.Forms.Label lblLdAnalog30;
        private System.Windows.Forms.TextBox txtAnalogValue14;
        private System.Windows.Forms.Label lblUdAnalog15;
        private System.Windows.Forms.Label lblUdAnalog14;
        private System.Windows.Forms.Label lblUdAnalog05;
        private System.Windows.Forms.Label lblLdAnalog29;
        private System.Windows.Forms.TextBox txtUdAnalogValue13;
        private System.Windows.Forms.Label lblUdAnalog08;
        private System.Windows.Forms.TextBox txtAnalogValue13;
        private System.Windows.Forms.Label lblLdAnalog28;
        private System.Windows.Forms.TextBox txtAnalogValue12;
        private System.Windows.Forms.Label lblLdAnalog27;
        private System.Windows.Forms.TextBox txtAnalogValue11;
        private System.Windows.Forms.Label lblLdAnalog26;
        private System.Windows.Forms.TextBox txtAnalogValue10;
        private System.Windows.Forms.Label lblUdAnalog10;
        private System.Windows.Forms.Label lblLdAnalog25;
        private System.Windows.Forms.Label lblLdAnalog24;
        private System.Windows.Forms.TextBox txtAnalogValue08;
        private System.Windows.Forms.Label lblLdAnalog23;
        private System.Windows.Forms.TextBox txtAnalogValue07;
        private System.Windows.Forms.Label lblLdAnalog22;
        private System.Windows.Forms.Label lblUdAnalog06;
        private System.Windows.Forms.TextBox txtUdAnalogValue01;
        private System.Windows.Forms.Label lblUdAnalog09;
        private System.Windows.Forms.TextBox txtAnalogValue09;
        private System.Windows.Forms.TextBox txtUdAnalogValue02;
        private System.Windows.Forms.TextBox txtAnalogValue06;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblLdAnalog21;
        private System.Windows.Forms.TextBox txtAnalogValue05;
        private System.Windows.Forms.Label lblLdAnalog20;
        private System.Windows.Forms.TextBox txtAnalogValue04;
        private System.Windows.Forms.Label lblLdAnalog19;
        private System.Windows.Forms.TextBox txtAnalogValue03;
        private System.Windows.Forms.Label lblLdAnalog18;
        private System.Windows.Forms.TextBox txtAnalogValue02;
        private System.Windows.Forms.Label lblLdAnalog17;
        private System.Windows.Forms.TextBox txtAnalogValue01;
        private System.Windows.Forms.Label lblUdAnalog07;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblProcAnalog01;
        private System.Windows.Forms.Label lblProcAnalog02;
        private System.Windows.Forms.TextBox txtProcAnalogValue02;
        private System.Windows.Forms.TextBox txtProcAnalogValue01;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtUdAnalogValue16;
        private System.Windows.Forms.Label lblUdAnalog16;
        private System.Windows.Forms.TextBox txtUdAnalogValue15;
        private System.Windows.Forms.TextBox txtUdAnalogValue14;
        internal System.Windows.Forms.Label lblServoName;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label label2;
    }
}

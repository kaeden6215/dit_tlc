﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerMotorStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpSevoMotors = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMonitorLoader = new System.Windows.Forms.Button();
            this.btnMonitorProcess = new System.Windows.Forms.Button();
            this.btnMonitorUnloader = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAjinMotor = new System.Windows.Forms.Button();
            this.btnUmacMotor = new System.Windows.Forms.Button();
            this.btnEZiMotor = new System.Windows.Forms.Button();
            this.panle11 = new System.Windows.Forms.Panel();
            this.lblMotorStatus124 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblMotorStatus324 = new System.Windows.Forms.Label();
            this.lblMotorStatus322 = new System.Windows.Forms.Label();
            this.lblMotorStatus319 = new System.Windows.Forms.Label();
            this.lblMotorStatus323 = new System.Windows.Forms.Label();
            this.lblMotorStatus321 = new System.Windows.Forms.Label();
            this.lblMotorStatus318 = new System.Windows.Forms.Label();
            this.lblMotorStatus320 = new System.Windows.Forms.Label();
            this.lblMotorStatus316 = new System.Windows.Forms.Label();
            this.lblMotorStatus315 = new System.Windows.Forms.Label();
            this.lblMotorStatus313 = new System.Windows.Forms.Label();
            this.lblMotorStatus317 = new System.Windows.Forms.Label();
            this.lblMotorStatus314 = new System.Windows.Forms.Label();
            this.lblMotorStatus312 = new System.Windows.Forms.Label();
            this.btnMotorStatus324 = new System.Windows.Forms.Button();
            this.btnMotorStatus323 = new System.Windows.Forms.Button();
            this.btnMotorStatus322 = new System.Windows.Forms.Button();
            this.btnMotorStatus321 = new System.Windows.Forms.Button();
            this.btnMotorStatus320 = new System.Windows.Forms.Button();
            this.btnMotorStatus319 = new System.Windows.Forms.Button();
            this.btnMotorStatus318 = new System.Windows.Forms.Button();
            this.btnMotorStatus317 = new System.Windows.Forms.Button();
            this.btnMotorStatus315 = new System.Windows.Forms.Button();
            this.btnMotorStatus313 = new System.Windows.Forms.Button();
            this.lblMotorStatus311 = new System.Windows.Forms.Label();
            this.btnMotorStatus312 = new System.Windows.Forms.Button();
            this.lblMotorStatus309 = new System.Windows.Forms.Label();
            this.btnMotorStatus310 = new System.Windows.Forms.Button();
            this.btnMotorStatus316 = new System.Windows.Forms.Button();
            this.btnMotorStatus314 = new System.Windows.Forms.Button();
            this.btnMotorStatus311 = new System.Windows.Forms.Button();
            this.lblMotorStatus310 = new System.Windows.Forms.Label();
            this.btnMotorStatus309 = new System.Windows.Forms.Button();
            this.lblMotorStatus308 = new System.Windows.Forms.Label();
            this.btnMotorStatus308 = new System.Windows.Forms.Button();
            this.lblMotorStatus307 = new System.Windows.Forms.Label();
            this.btnMotorStatus307 = new System.Windows.Forms.Button();
            this.lblMotorStatus306 = new System.Windows.Forms.Label();
            this.btnMotorStatus306 = new System.Windows.Forms.Button();
            this.lblMotorStatus305 = new System.Windows.Forms.Label();
            this.btnMotorStatus305 = new System.Windows.Forms.Button();
            this.lblMotorStatus304 = new System.Windows.Forms.Label();
            this.btnMotorStatus304 = new System.Windows.Forms.Button();
            this.lblMotorStatus303 = new System.Windows.Forms.Label();
            this.btnMotorStatus303 = new System.Windows.Forms.Button();
            this.lblMotorStatus302 = new System.Windows.Forms.Label();
            this.btnMotorStatus302 = new System.Windows.Forms.Button();
            this.lblMotorStatus301 = new System.Windows.Forms.Label();
            this.btnMotorStatus301 = new System.Windows.Forms.Button();
            this.lblDriveStatus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMotorStatus213 = new System.Windows.Forms.Label();
            this.lblMotorStatus211 = new System.Windows.Forms.Label();
            this.lblMotorStatus209 = new System.Windows.Forms.Label();
            this.lblMotorStatus214 = new System.Windows.Forms.Label();
            this.lblMotorStatus212 = new System.Windows.Forms.Label();
            this.lblMotorStatus210 = new System.Windows.Forms.Label();
            this.lblMotorStatus208 = new System.Windows.Forms.Label();
            this.lblMotorStatus207 = new System.Windows.Forms.Label();
            this.lblMotorStatus206 = new System.Windows.Forms.Label();
            this.lblMotorStatus205 = new System.Windows.Forms.Label();
            this.lblMotorStatus204 = new System.Windows.Forms.Label();
            this.lblMotorStatus203 = new System.Windows.Forms.Label();
            this.lblMotorStatus224 = new System.Windows.Forms.Label();
            this.lblMotionEndStatus = new System.Windows.Forms.Label();
            this.btnMotorStatus224 = new System.Windows.Forms.Button();
            this.btnMotorStatus201 = new System.Windows.Forms.Button();
            this.btnMotorStatus223 = new System.Windows.Forms.Button();
            this.lblMotorStatus201 = new System.Windows.Forms.Label();
            this.btnMotorStatus222 = new System.Windows.Forms.Button();
            this.lblMotorStatus202 = new System.Windows.Forms.Label();
            this.btnMotorStatus221 = new System.Windows.Forms.Button();
            this.btnMotorStatus202 = new System.Windows.Forms.Button();
            this.btnMotorStatus220 = new System.Windows.Forms.Button();
            this.btnMotorStatus219 = new System.Windows.Forms.Button();
            this.btnMotorStatus218 = new System.Windows.Forms.Button();
            this.btnMotorStatus217 = new System.Windows.Forms.Button();
            this.btnMotorStatus216 = new System.Windows.Forms.Button();
            this.btnMotorStatus215 = new System.Windows.Forms.Button();
            this.btnMotorStatus214 = new System.Windows.Forms.Button();
            this.btnMotorStatus213 = new System.Windows.Forms.Button();
            this.btnMotorStatus212 = new System.Windows.Forms.Button();
            this.btnMotorStatus211 = new System.Windows.Forms.Button();
            this.btnMotorStatus210 = new System.Windows.Forms.Button();
            this.btnMotorStatus209 = new System.Windows.Forms.Button();
            this.btnMotorStatus208 = new System.Windows.Forms.Button();
            this.lblMotorStatus215 = new System.Windows.Forms.Label();
            this.btnMotorStatus207 = new System.Windows.Forms.Button();
            this.lblMotorStatus216 = new System.Windows.Forms.Label();
            this.btnMotorStatus206 = new System.Windows.Forms.Button();
            this.lblMotorStatus217 = new System.Windows.Forms.Label();
            this.btnMotorStatus205 = new System.Windows.Forms.Button();
            this.lblMotorStatus218 = new System.Windows.Forms.Label();
            this.btnMotorStatus204 = new System.Windows.Forms.Button();
            this.lblMotorStatus219 = new System.Windows.Forms.Label();
            this.btnMotorStatus203 = new System.Windows.Forms.Button();
            this.lblMotorStatus220 = new System.Windows.Forms.Label();
            this.lblMotorStatus223 = new System.Windows.Forms.Label();
            this.lblMotorStatus221 = new System.Windows.Forms.Label();
            this.lblMotorStatus222 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnMotorStatus124 = new System.Windows.Forms.Button();
            this.btnMotorStatus123 = new System.Windows.Forms.Button();
            this.btnMotorStatus122 = new System.Windows.Forms.Button();
            this.btnMotorStatus121 = new System.Windows.Forms.Button();
            this.btnMotorStatus120 = new System.Windows.Forms.Button();
            this.btnMotorStatus119 = new System.Windows.Forms.Button();
            this.btnMotorStatus118 = new System.Windows.Forms.Button();
            this.btnMotorStatus117 = new System.Windows.Forms.Button();
            this.btnMotorStatus116 = new System.Windows.Forms.Button();
            this.btnMotorStatus115 = new System.Windows.Forms.Button();
            this.btnMotorStatus114 = new System.Windows.Forms.Button();
            this.btnMotorStatus113 = new System.Windows.Forms.Button();
            this.btnMotorStatus112 = new System.Windows.Forms.Button();
            this.btnMotorStatus111 = new System.Windows.Forms.Button();
            this.btnMotorStatus110 = new System.Windows.Forms.Button();
            this.btnMotorStatus109 = new System.Windows.Forms.Button();
            this.btnMotorStatus108 = new System.Windows.Forms.Button();
            this.btnMotorStatus107 = new System.Windows.Forms.Button();
            this.btnMotorStatus106 = new System.Windows.Forms.Button();
            this.btnMotorStatus105 = new System.Windows.Forms.Button();
            this.btnMotorStatus104 = new System.Windows.Forms.Button();
            this.btnMotorStatus103 = new System.Windows.Forms.Button();
            this.lblMotorStatus123 = new System.Windows.Forms.Label();
            this.lblMotorStatus122 = new System.Windows.Forms.Label();
            this.lblMotorStatus121 = new System.Windows.Forms.Label();
            this.lblMotorStatus120 = new System.Windows.Forms.Label();
            this.lblMotorStatus119 = new System.Windows.Forms.Label();
            this.lblMotorStatus118 = new System.Windows.Forms.Label();
            this.lblMotorStatus117 = new System.Windows.Forms.Label();
            this.lblMotorStatus116 = new System.Windows.Forms.Label();
            this.lblMotorStatus115 = new System.Windows.Forms.Label();
            this.lblMotorStatus114 = new System.Windows.Forms.Label();
            this.lblMotorStatus113 = new System.Windows.Forms.Label();
            this.lblMotorStatus112 = new System.Windows.Forms.Label();
            this.lblMotorStatus111 = new System.Windows.Forms.Label();
            this.lblMotorStatus110 = new System.Windows.Forms.Label();
            this.lblMotorStatus109 = new System.Windows.Forms.Label();
            this.lblMotorStatus108 = new System.Windows.Forms.Label();
            this.lblMotorStatus107 = new System.Windows.Forms.Label();
            this.lblMotorStatus106 = new System.Windows.Forms.Label();
            this.lblMotorStatus105 = new System.Windows.Forms.Label();
            this.lblMotorStatus104 = new System.Windows.Forms.Label();
            this.lblMotorStatus103 = new System.Windows.Forms.Label();
            this.btnMotorStatus102 = new System.Windows.Forms.Button();
            this.lblMotorStatus102 = new System.Windows.Forms.Label();
            this.btnMotorStatus101 = new System.Windows.Forms.Button();
            this.lblMotorStatus101 = new System.Windows.Forms.Label();
            this.lblMechanicalStatus = new System.Windows.Forms.Label();
            this.lblMotorStatus = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.panle11.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flpSevoMotors
            // 
            this.flpSevoMotors.AutoScroll = true;
            this.flpSevoMotors.Location = new System.Drawing.Point(1, 27);
            this.flpSevoMotors.Name = "flpSevoMotors";
            this.flpSevoMotors.Size = new System.Drawing.Size(707, 713);
            this.flpSevoMotors.TabIndex = 84;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnMonitorLoader);
            this.flowLayoutPanel2.Controls.Add(this.btnMonitorProcess);
            this.flowLayoutPanel2.Controls.Add(this.btnMonitorUnloader);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1740, 47);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // btnMonitorLoader
            // 
            this.btnMonitorLoader.Location = new System.Drawing.Point(3, 3);
            this.btnMonitorLoader.Name = "btnMonitorLoader";
            this.btnMonitorLoader.Size = new System.Drawing.Size(574, 41);
            this.btnMonitorLoader.TabIndex = 0;
            this.btnMonitorLoader.Text = "로더";
            this.btnMonitorLoader.UseVisualStyleBackColor = true;
            this.btnMonitorLoader.Click += new System.EventHandler(this.btnMonitorLoader_Click);
            // 
            // btnMonitorProcess
            // 
            this.btnMonitorProcess.Location = new System.Drawing.Point(583, 3);
            this.btnMonitorProcess.Name = "btnMonitorProcess";
            this.btnMonitorProcess.Size = new System.Drawing.Size(574, 41);
            this.btnMonitorProcess.TabIndex = 1;
            this.btnMonitorProcess.Text = "프로세스";
            this.btnMonitorProcess.UseVisualStyleBackColor = true;
            this.btnMonitorProcess.Click += new System.EventHandler(this.btnMonitorLoader_Click);
            // 
            // btnMonitorUnloader
            // 
            this.btnMonitorUnloader.Location = new System.Drawing.Point(1163, 3);
            this.btnMonitorUnloader.Name = "btnMonitorUnloader";
            this.btnMonitorUnloader.Size = new System.Drawing.Size(574, 41);
            this.btnMonitorUnloader.TabIndex = 2;
            this.btnMonitorUnloader.Text = "언로드";
            this.btnMonitorUnloader.UseVisualStyleBackColor = true;
            this.btnMonitorUnloader.Click += new System.EventHandler(this.btnMonitorLoader_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.btnAjinMotor);
            this.flowLayoutPanel4.Controls.Add(this.btnUmacMotor);
            this.flowLayoutPanel4.Controls.Add(this.btnEZiMotor);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(0, 47);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1740, 43);
            this.flowLayoutPanel4.TabIndex = 5;
            // 
            // btnAjinMotor
            // 
            this.btnAjinMotor.Location = new System.Drawing.Point(3, 3);
            this.btnAjinMotor.Name = "btnAjinMotor";
            this.btnAjinMotor.Size = new System.Drawing.Size(574, 35);
            this.btnAjinMotor.TabIndex = 1;
            this.btnAjinMotor.Text = "AJIN";
            this.btnAjinMotor.UseVisualStyleBackColor = true;
            this.btnAjinMotor.Click += new System.EventHandler(this.btnAjinMotor_Click);
            // 
            // btnUmacMotor
            // 
            this.btnUmacMotor.Location = new System.Drawing.Point(583, 3);
            this.btnUmacMotor.Name = "btnUmacMotor";
            this.btnUmacMotor.Size = new System.Drawing.Size(574, 35);
            this.btnUmacMotor.TabIndex = 2;
            this.btnUmacMotor.Text = "UMAC";
            this.btnUmacMotor.UseVisualStyleBackColor = true;
            this.btnUmacMotor.Click += new System.EventHandler(this.btnAjinMotor_Click);
            // 
            // btnEZiMotor
            // 
            this.btnEZiMotor.Location = new System.Drawing.Point(1163, 3);
            this.btnEZiMotor.Name = "btnEZiMotor";
            this.btnEZiMotor.Size = new System.Drawing.Size(574, 35);
            this.btnEZiMotor.TabIndex = 3;
            this.btnEZiMotor.Text = "E-ZI";
            this.btnEZiMotor.UseVisualStyleBackColor = true;
            this.btnEZiMotor.Click += new System.EventHandler(this.btnAjinMotor_Click);
            // 
            // panle11
            // 
            this.panle11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panle11.Controls.Add(this.lblMotorStatus124);
            this.panle11.Controls.Add(this.panel2);
            this.panle11.Controls.Add(this.panel1);
            this.panle11.Controls.Add(this.panel15);
            this.panle11.Controls.Add(this.lblMotorStatus);
            this.panle11.Location = new System.Drawing.Point(718, 102);
            this.panle11.Name = "panle11";
            this.panle11.Size = new System.Drawing.Size(1000, 745);
            this.panle11.TabIndex = 462;
            // 
            // lblMotorStatus124
            // 
            this.lblMotorStatus124.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus124.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus124.Location = new System.Drawing.Point(38, 707);
            this.lblMotorStatus124.Name = "lblMotorStatus124";
            this.lblMotorStatus124.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus124.TabIndex = 89;
            this.lblMotorStatus124.Text = "Mode Terminal";
            this.lblMotorStatus124.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblMotorStatus324);
            this.panel2.Controls.Add(this.lblMotorStatus322);
            this.panel2.Controls.Add(this.lblMotorStatus319);
            this.panel2.Controls.Add(this.lblMotorStatus323);
            this.panel2.Controls.Add(this.lblMotorStatus321);
            this.panel2.Controls.Add(this.lblMotorStatus318);
            this.panel2.Controls.Add(this.lblMotorStatus320);
            this.panel2.Controls.Add(this.lblMotorStatus316);
            this.panel2.Controls.Add(this.lblMotorStatus315);
            this.panel2.Controls.Add(this.lblMotorStatus313);
            this.panel2.Controls.Add(this.lblMotorStatus317);
            this.panel2.Controls.Add(this.lblMotorStatus314);
            this.panel2.Controls.Add(this.lblMotorStatus312);
            this.panel2.Controls.Add(this.btnMotorStatus324);
            this.panel2.Controls.Add(this.btnMotorStatus323);
            this.panel2.Controls.Add(this.btnMotorStatus322);
            this.panel2.Controls.Add(this.btnMotorStatus321);
            this.panel2.Controls.Add(this.btnMotorStatus320);
            this.panel2.Controls.Add(this.btnMotorStatus319);
            this.panel2.Controls.Add(this.btnMotorStatus318);
            this.panel2.Controls.Add(this.btnMotorStatus317);
            this.panel2.Controls.Add(this.btnMotorStatus315);
            this.panel2.Controls.Add(this.btnMotorStatus313);
            this.panel2.Controls.Add(this.lblMotorStatus311);
            this.panel2.Controls.Add(this.btnMotorStatus312);
            this.panel2.Controls.Add(this.lblMotorStatus309);
            this.panel2.Controls.Add(this.btnMotorStatus310);
            this.panel2.Controls.Add(this.btnMotorStatus316);
            this.panel2.Controls.Add(this.btnMotorStatus314);
            this.panel2.Controls.Add(this.btnMotorStatus311);
            this.panel2.Controls.Add(this.lblMotorStatus310);
            this.panel2.Controls.Add(this.btnMotorStatus309);
            this.panel2.Controls.Add(this.lblMotorStatus308);
            this.panel2.Controls.Add(this.btnMotorStatus308);
            this.panel2.Controls.Add(this.lblMotorStatus307);
            this.panel2.Controls.Add(this.btnMotorStatus307);
            this.panel2.Controls.Add(this.lblMotorStatus306);
            this.panel2.Controls.Add(this.btnMotorStatus306);
            this.panel2.Controls.Add(this.lblMotorStatus305);
            this.panel2.Controls.Add(this.btnMotorStatus305);
            this.panel2.Controls.Add(this.lblMotorStatus304);
            this.panel2.Controls.Add(this.btnMotorStatus304);
            this.panel2.Controls.Add(this.lblMotorStatus303);
            this.panel2.Controls.Add(this.btnMotorStatus303);
            this.panel2.Controls.Add(this.lblMotorStatus302);
            this.panel2.Controls.Add(this.btnMotorStatus302);
            this.panel2.Controls.Add(this.lblMotorStatus301);
            this.panel2.Controls.Add(this.btnMotorStatus301);
            this.panel2.Controls.Add(this.lblDriveStatus);
            this.panel2.Location = new System.Drawing.Point(669, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(326, 711);
            this.panel2.TabIndex = 460;
            // 
            // lblMotorStatus324
            // 
            this.lblMotorStatus324.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus324.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus324.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus324.Location = new System.Drawing.Point(34, 674);
            this.lblMotorStatus324.Name = "lblMotorStatus324";
            this.lblMotorStatus324.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus324.TabIndex = 270;
            this.lblMotorStatus324.Text = "Mode Terminal";
            this.lblMotorStatus324.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus322
            // 
            this.lblMotorStatus322.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus322.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus322.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus322.Location = new System.Drawing.Point(34, 616);
            this.lblMotorStatus322.Name = "lblMotorStatus322";
            this.lblMotorStatus322.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus322.TabIndex = 271;
            this.lblMotorStatus322.Text = "Mode Terminal";
            this.lblMotorStatus322.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus319
            // 
            this.lblMotorStatus319.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus319.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus319.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus319.Location = new System.Drawing.Point(34, 534);
            this.lblMotorStatus319.Name = "lblMotorStatus319";
            this.lblMotorStatus319.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus319.TabIndex = 272;
            this.lblMotorStatus319.Text = "Mode Terminal";
            this.lblMotorStatus319.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus323
            // 
            this.lblMotorStatus323.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus323.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus323.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus323.Location = new System.Drawing.Point(34, 647);
            this.lblMotorStatus323.Name = "lblMotorStatus323";
            this.lblMotorStatus323.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus323.TabIndex = 273;
            this.lblMotorStatus323.Text = "Mode Terminal";
            this.lblMotorStatus323.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus321
            // 
            this.lblMotorStatus321.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus321.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus321.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus321.Location = new System.Drawing.Point(34, 589);
            this.lblMotorStatus321.Name = "lblMotorStatus321";
            this.lblMotorStatus321.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus321.TabIndex = 274;
            this.lblMotorStatus321.Text = "Mode Terminal";
            this.lblMotorStatus321.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus318
            // 
            this.lblMotorStatus318.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus318.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus318.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus318.Location = new System.Drawing.Point(34, 507);
            this.lblMotorStatus318.Name = "lblMotorStatus318";
            this.lblMotorStatus318.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus318.TabIndex = 275;
            this.lblMotorStatus318.Text = "Mode Terminal";
            this.lblMotorStatus318.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus320
            // 
            this.lblMotorStatus320.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus320.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus320.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus320.Location = new System.Drawing.Point(34, 563);
            this.lblMotorStatus320.Name = "lblMotorStatus320";
            this.lblMotorStatus320.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus320.TabIndex = 269;
            this.lblMotorStatus320.Text = "Mode Terminal";
            this.lblMotorStatus320.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus316
            // 
            this.lblMotorStatus316.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus316.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus316.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus316.Location = new System.Drawing.Point(34, 453);
            this.lblMotorStatus316.Name = "lblMotorStatus316";
            this.lblMotorStatus316.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus316.TabIndex = 268;
            this.lblMotorStatus316.Text = "ERRSTATE";
            this.lblMotorStatus316.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus315
            // 
            this.lblMotorStatus315.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus315.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus315.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus315.Location = new System.Drawing.Point(34, 425);
            this.lblMotorStatus315.Name = "lblMotorStatus315";
            this.lblMotorStatus315.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus315.TabIndex = 267;
            this.lblMotorStatus315.Text = "MPG Input Error";
            this.lblMotorStatus315.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus313
            // 
            this.lblMotorStatus313.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus313.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus313.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus313.Location = new System.Drawing.Point(34, 369);
            this.lblMotorStatus313.Name = "lblMotorStatus313";
            this.lblMotorStatus313.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus313.TabIndex = 266;
            this.lblMotorStatus313.Text = "Sync Stop Func #2 End";
            this.lblMotorStatus313.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus317
            // 
            this.lblMotorStatus317.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus317.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus317.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus317.Location = new System.Drawing.Point(34, 481);
            this.lblMotorStatus317.Name = "lblMotorStatus317";
            this.lblMotorStatus317.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus317.TabIndex = 265;
            this.lblMotorStatus317.Text = "Interpolatioon Data Error";
            this.lblMotorStatus317.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus314
            // 
            this.lblMotorStatus314.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus314.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus314.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus314.Location = new System.Drawing.Point(34, 397);
            this.lblMotorStatus314.Name = "lblMotorStatus314";
            this.lblMotorStatus314.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus314.TabIndex = 264;
            this.lblMotorStatus314.Text = "Encoder Input Error";
            this.lblMotorStatus314.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus312
            // 
            this.lblMotorStatus312.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus312.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus312.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus312.Location = new System.Drawing.Point(34, 341);
            this.lblMotorStatus312.Name = "lblMotorStatus312";
            this.lblMotorStatus312.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus312.TabIndex = 263;
            this.lblMotorStatus312.Text = "Sync Stop Func #1 End";
            this.lblMotorStatus312.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus324
            // 
            this.btnMotorStatus324.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus324.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus324.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus324.Location = new System.Drawing.Point(3, 677);
            this.btnMotorStatus324.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus324.Name = "btnMotorStatus324";
            this.btnMotorStatus324.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus324.TabIndex = 262;
            this.btnMotorStatus324.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus323
            // 
            this.btnMotorStatus323.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus323.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus323.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus323.Location = new System.Drawing.Point(3, 649);
            this.btnMotorStatus323.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus323.Name = "btnMotorStatus323";
            this.btnMotorStatus323.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus323.TabIndex = 261;
            this.btnMotorStatus323.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus322
            // 
            this.btnMotorStatus322.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus322.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus322.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus322.Location = new System.Drawing.Point(3, 621);
            this.btnMotorStatus322.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus322.Name = "btnMotorStatus322";
            this.btnMotorStatus322.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus322.TabIndex = 260;
            this.btnMotorStatus322.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus321
            // 
            this.btnMotorStatus321.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus321.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus321.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus321.Location = new System.Drawing.Point(3, 593);
            this.btnMotorStatus321.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus321.Name = "btnMotorStatus321";
            this.btnMotorStatus321.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus321.TabIndex = 259;
            this.btnMotorStatus321.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus320
            // 
            this.btnMotorStatus320.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus320.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus320.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus320.Location = new System.Drawing.Point(3, 565);
            this.btnMotorStatus320.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus320.Name = "btnMotorStatus320";
            this.btnMotorStatus320.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus320.TabIndex = 258;
            this.btnMotorStatus320.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus319
            // 
            this.btnMotorStatus319.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus319.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus319.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus319.Location = new System.Drawing.Point(3, 537);
            this.btnMotorStatus319.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus319.Name = "btnMotorStatus319";
            this.btnMotorStatus319.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus319.TabIndex = 257;
            this.btnMotorStatus319.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus318
            // 
            this.btnMotorStatus318.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus318.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus318.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus318.Location = new System.Drawing.Point(3, 509);
            this.btnMotorStatus318.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus318.Name = "btnMotorStatus318";
            this.btnMotorStatus318.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus318.TabIndex = 256;
            this.btnMotorStatus318.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus317
            // 
            this.btnMotorStatus317.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus317.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus317.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus317.Location = new System.Drawing.Point(3, 481);
            this.btnMotorStatus317.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus317.Name = "btnMotorStatus317";
            this.btnMotorStatus317.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus317.TabIndex = 255;
            this.btnMotorStatus317.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus315
            // 
            this.btnMotorStatus315.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus315.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus315.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus315.Location = new System.Drawing.Point(3, 425);
            this.btnMotorStatus315.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus315.Name = "btnMotorStatus315";
            this.btnMotorStatus315.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus315.TabIndex = 254;
            this.btnMotorStatus315.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus313
            // 
            this.btnMotorStatus313.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus313.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus313.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus313.Location = new System.Drawing.Point(3, 369);
            this.btnMotorStatus313.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus313.Name = "btnMotorStatus313";
            this.btnMotorStatus313.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus313.TabIndex = 253;
            this.btnMotorStatus313.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus311
            // 
            this.lblMotorStatus311.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus311.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus311.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus311.Location = new System.Drawing.Point(34, 313);
            this.lblMotorStatus311.Name = "lblMotorStatus311";
            this.lblMotorStatus311.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus311.TabIndex = 251;
            this.lblMotorStatus311.Text = "WAIT_INP";
            this.lblMotorStatus311.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus312
            // 
            this.btnMotorStatus312.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus312.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus312.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus312.Location = new System.Drawing.Point(3, 341);
            this.btnMotorStatus312.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus312.Name = "btnMotorStatus312";
            this.btnMotorStatus312.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus312.TabIndex = 252;
            this.btnMotorStatus312.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus309
            // 
            this.lblMotorStatus309.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus309.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus309.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus309.Location = new System.Drawing.Point(34, 257);
            this.lblMotorStatus309.Name = "lblMotorStatus309";
            this.lblMotorStatus309.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus309.TabIndex = 249;
            this.lblMotorStatus309.Text = "In move of Interpolation";
            this.lblMotorStatus309.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus310
            // 
            this.btnMotorStatus310.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus310.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus310.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus310.Location = new System.Drawing.Point(3, 285);
            this.btnMotorStatus310.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus310.Name = "btnMotorStatus310";
            this.btnMotorStatus310.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus310.TabIndex = 250;
            this.btnMotorStatus310.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus316
            // 
            this.btnMotorStatus316.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus316.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus316.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus316.Location = new System.Drawing.Point(3, 453);
            this.btnMotorStatus316.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus316.Name = "btnMotorStatus316";
            this.btnMotorStatus316.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus316.TabIndex = 248;
            this.btnMotorStatus316.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus314
            // 
            this.btnMotorStatus314.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus314.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus314.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus314.Location = new System.Drawing.Point(3, 397);
            this.btnMotorStatus314.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus314.Name = "btnMotorStatus314";
            this.btnMotorStatus314.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus314.TabIndex = 247;
            this.btnMotorStatus314.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus311
            // 
            this.btnMotorStatus311.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus311.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus311.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus311.Location = new System.Drawing.Point(3, 313);
            this.btnMotorStatus311.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus311.Name = "btnMotorStatus311";
            this.btnMotorStatus311.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus311.TabIndex = 246;
            this.btnMotorStatus311.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus310
            // 
            this.lblMotorStatus310.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus310.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus310.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus310.Location = new System.Drawing.Point(34, 285);
            this.lblMotorStatus310.Name = "lblMotorStatus310";
            this.lblMotorStatus310.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus310.TabIndex = 244;
            this.lblMotorStatus310.Text = "In move of Slave";
            this.lblMotorStatus310.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus309
            // 
            this.btnMotorStatus309.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus309.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus309.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus309.Location = new System.Drawing.Point(3, 257);
            this.btnMotorStatus309.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus309.Name = "btnMotorStatus309";
            this.btnMotorStatus309.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus309.TabIndex = 245;
            this.btnMotorStatus309.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus308
            // 
            this.lblMotorStatus308.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus308.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus308.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus308.Location = new System.Drawing.Point(34, 229);
            this.lblMotorStatus308.Name = "lblMotorStatus308";
            this.lblMotorStatus308.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus308.TabIndex = 242;
            this.lblMotorStatus308.Text = "In move of home";
            this.lblMotorStatus308.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus308
            // 
            this.btnMotorStatus308.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus308.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus308.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus308.Location = new System.Drawing.Point(3, 229);
            this.btnMotorStatus308.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus308.Name = "btnMotorStatus308";
            this.btnMotorStatus308.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus308.TabIndex = 243;
            this.btnMotorStatus308.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus307
            // 
            this.lblMotorStatus307.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus307.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus307.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus307.Location = new System.Drawing.Point(34, 201);
            this.lblMotorStatus307.Name = "lblMotorStatus307";
            this.lblMotorStatus307.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus307.TabIndex = 240;
            this.lblMotorStatus307.Text = "In move of MPG";
            this.lblMotorStatus307.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus307
            // 
            this.btnMotorStatus307.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus307.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus307.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus307.Location = new System.Drawing.Point(3, 201);
            this.btnMotorStatus307.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus307.Name = "btnMotorStatus307";
            this.btnMotorStatus307.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus307.TabIndex = 241;
            this.btnMotorStatus307.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus306
            // 
            this.lblMotorStatus306.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus306.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus306.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus306.Location = new System.Drawing.Point(34, 173);
            this.lblMotorStatus306.Name = "lblMotorStatus306";
            this.lblMotorStatus306.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus306.TabIndex = 238;
            this.lblMotorStatus306.Text = "In move of assigned";
            this.lblMotorStatus306.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus306
            // 
            this.btnMotorStatus306.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus306.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus306.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus306.Location = new System.Drawing.Point(3, 173);
            this.btnMotorStatus306.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus306.Name = "btnMotorStatus306";
            this.btnMotorStatus306.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus306.TabIndex = 239;
            this.btnMotorStatus306.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus305
            // 
            this.lblMotorStatus305.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus305.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus305.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus305.Location = new System.Drawing.Point(34, 145);
            this.lblMotorStatus305.Name = "lblMotorStatus305";
            this.lblMotorStatus305.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus305.TabIndex = 236;
            this.lblMotorStatus305.Text = "In move of continuous";
            this.lblMotorStatus305.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus305
            // 
            this.btnMotorStatus305.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus305.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus305.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus305.Location = new System.Drawing.Point(3, 145);
            this.btnMotorStatus305.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus305.Name = "btnMotorStatus305";
            this.btnMotorStatus305.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus305.TabIndex = 237;
            this.btnMotorStatus305.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus304
            // 
            this.lblMotorStatus304.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus304.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus304.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus304.Location = new System.Drawing.Point(34, 117);
            this.lblMotorStatus304.Name = "lblMotorStatus304";
            this.lblMotorStatus304.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus304.TabIndex = 234;
            this.lblMotorStatus304.Text = "Up(In Acceleration)";
            this.lblMotorStatus304.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus304
            // 
            this.btnMotorStatus304.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus304.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus304.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus304.Location = new System.Drawing.Point(3, 117);
            this.btnMotorStatus304.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus304.Name = "btnMotorStatus304";
            this.btnMotorStatus304.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus304.TabIndex = 235;
            this.btnMotorStatus304.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus303
            // 
            this.lblMotorStatus303.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus303.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus303.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus303.Location = new System.Drawing.Point(34, 89);
            this.lblMotorStatus303.Name = "lblMotorStatus303";
            this.lblMotorStatus303.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus303.TabIndex = 232;
            this.lblMotorStatus303.Text = "Const(In Constant Speed)";
            this.lblMotorStatus303.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus303
            // 
            this.btnMotorStatus303.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus303.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus303.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus303.Location = new System.Drawing.Point(3, 89);
            this.btnMotorStatus303.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus303.Name = "btnMotorStatus303";
            this.btnMotorStatus303.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus303.TabIndex = 233;
            this.btnMotorStatus303.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus302
            // 
            this.lblMotorStatus302.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus302.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus302.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus302.Location = new System.Drawing.Point(34, 61);
            this.lblMotorStatus302.Name = "lblMotorStatus302";
            this.lblMotorStatus302.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus302.TabIndex = 230;
            this.lblMotorStatus302.Text = "Down(In Deceleration)";
            this.lblMotorStatus302.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus302
            // 
            this.btnMotorStatus302.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus302.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus302.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus302.Location = new System.Drawing.Point(3, 61);
            this.btnMotorStatus302.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus302.Name = "btnMotorStatus302";
            this.btnMotorStatus302.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus302.TabIndex = 231;
            this.btnMotorStatus302.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus301
            // 
            this.lblMotorStatus301.BackColor = System.Drawing.SystemColors.Control;
            this.lblMotorStatus301.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus301.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus301.Location = new System.Drawing.Point(34, 33);
            this.lblMotorStatus301.Name = "lblMotorStatus301";
            this.lblMotorStatus301.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus301.TabIndex = 228;
            this.lblMotorStatus301.Text = "Busy(In Drive Move)";
            this.lblMotorStatus301.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus301
            // 
            this.btnMotorStatus301.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus301.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus301.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus301.Location = new System.Drawing.Point(3, 33);
            this.btnMotorStatus301.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus301.Name = "btnMotorStatus301";
            this.btnMotorStatus301.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus301.TabIndex = 229;
            this.btnMotorStatus301.UseVisualStyleBackColor = false;
            // 
            // lblDriveStatus
            // 
            this.lblDriveStatus.AutoEllipsis = true;
            this.lblDriveStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDriveStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDriveStatus.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblDriveStatus.ForeColor = System.Drawing.Color.Black;
            this.lblDriveStatus.Location = new System.Drawing.Point(0, 0);
            this.lblDriveStatus.Name = "lblDriveStatus";
            this.lblDriveStatus.Size = new System.Drawing.Size(324, 24);
            this.lblDriveStatus.TabIndex = 9;
            this.lblDriveStatus.Text = "■ Drive Status";
            this.lblDriveStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblMotorStatus213);
            this.panel1.Controls.Add(this.lblMotorStatus211);
            this.panel1.Controls.Add(this.lblMotorStatus209);
            this.panel1.Controls.Add(this.lblMotorStatus214);
            this.panel1.Controls.Add(this.lblMotorStatus212);
            this.panel1.Controls.Add(this.lblMotorStatus210);
            this.panel1.Controls.Add(this.lblMotorStatus208);
            this.panel1.Controls.Add(this.lblMotorStatus207);
            this.panel1.Controls.Add(this.lblMotorStatus206);
            this.panel1.Controls.Add(this.lblMotorStatus205);
            this.panel1.Controls.Add(this.lblMotorStatus204);
            this.panel1.Controls.Add(this.lblMotorStatus203);
            this.panel1.Controls.Add(this.lblMotorStatus224);
            this.panel1.Controls.Add(this.lblMotionEndStatus);
            this.panel1.Controls.Add(this.btnMotorStatus224);
            this.panel1.Controls.Add(this.btnMotorStatus201);
            this.panel1.Controls.Add(this.btnMotorStatus223);
            this.panel1.Controls.Add(this.lblMotorStatus201);
            this.panel1.Controls.Add(this.btnMotorStatus222);
            this.panel1.Controls.Add(this.lblMotorStatus202);
            this.panel1.Controls.Add(this.btnMotorStatus221);
            this.panel1.Controls.Add(this.btnMotorStatus202);
            this.panel1.Controls.Add(this.btnMotorStatus220);
            this.panel1.Controls.Add(this.btnMotorStatus219);
            this.panel1.Controls.Add(this.btnMotorStatus218);
            this.panel1.Controls.Add(this.btnMotorStatus217);
            this.panel1.Controls.Add(this.btnMotorStatus216);
            this.panel1.Controls.Add(this.btnMotorStatus215);
            this.panel1.Controls.Add(this.btnMotorStatus214);
            this.panel1.Controls.Add(this.btnMotorStatus213);
            this.panel1.Controls.Add(this.btnMotorStatus212);
            this.panel1.Controls.Add(this.btnMotorStatus211);
            this.panel1.Controls.Add(this.btnMotorStatus210);
            this.panel1.Controls.Add(this.btnMotorStatus209);
            this.panel1.Controls.Add(this.btnMotorStatus208);
            this.panel1.Controls.Add(this.lblMotorStatus215);
            this.panel1.Controls.Add(this.btnMotorStatus207);
            this.panel1.Controls.Add(this.lblMotorStatus216);
            this.panel1.Controls.Add(this.btnMotorStatus206);
            this.panel1.Controls.Add(this.lblMotorStatus217);
            this.panel1.Controls.Add(this.btnMotorStatus205);
            this.panel1.Controls.Add(this.lblMotorStatus218);
            this.panel1.Controls.Add(this.btnMotorStatus204);
            this.panel1.Controls.Add(this.lblMotorStatus219);
            this.panel1.Controls.Add(this.btnMotorStatus203);
            this.panel1.Controls.Add(this.lblMotorStatus220);
            this.panel1.Controls.Add(this.lblMotorStatus223);
            this.panel1.Controls.Add(this.lblMotorStatus221);
            this.panel1.Controls.Add(this.lblMotorStatus222);
            this.panel1.Location = new System.Drawing.Point(336, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(326, 711);
            this.panel1.TabIndex = 459;
            // 
            // lblMotorStatus213
            // 
            this.lblMotorStatus213.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus213.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus213.Location = new System.Drawing.Point(34, 369);
            this.lblMotorStatus213.Name = "lblMotorStatus213";
            this.lblMotorStatus213.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus213.TabIndex = 179;
            this.lblMotorStatus213.Text = "Sync Stop Func #2 End";
            this.lblMotorStatus213.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus211
            // 
            this.lblMotorStatus211.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus211.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus211.Location = new System.Drawing.Point(34, 313);
            this.lblMotorStatus211.Name = "lblMotorStatus211";
            this.lblMotorStatus211.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus211.TabIndex = 178;
            this.lblMotorStatus211.Text = "All-Axis Stop Command";
            this.lblMotorStatus211.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus209
            // 
            this.lblMotorStatus209.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus209.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus209.Location = new System.Drawing.Point(34, 257);
            this.lblMotorStatus209.Name = "lblMotorStatus209";
            this.lblMotorStatus209.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus209.TabIndex = 177;
            this.lblMotorStatus209.Text = "Emergenct Stop Command";
            this.lblMotorStatus209.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus214
            // 
            this.lblMotorStatus214.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus214.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus214.Location = new System.Drawing.Point(34, 397);
            this.lblMotorStatus214.Name = "lblMotorStatus214";
            this.lblMotorStatus214.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus214.TabIndex = 176;
            this.lblMotorStatus214.Text = "Encoder Input Error";
            this.lblMotorStatus214.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus212
            // 
            this.lblMotorStatus212.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus212.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus212.Location = new System.Drawing.Point(34, 341);
            this.lblMotorStatus212.Name = "lblMotorStatus212";
            this.lblMotorStatus212.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus212.TabIndex = 175;
            this.lblMotorStatus212.Text = "Sync Stop Func #1 End";
            this.lblMotorStatus212.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus210
            // 
            this.lblMotorStatus210.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus210.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus210.Location = new System.Drawing.Point(34, 285);
            this.lblMotorStatus210.Name = "lblMotorStatus210";
            this.lblMotorStatus210.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus210.TabIndex = 174;
            this.lblMotorStatus210.Text = "Slow Stop Command";
            this.lblMotorStatus210.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus208
            // 
            this.lblMotorStatus208.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus208.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus208.Location = new System.Drawing.Point(34, 229);
            this.lblMotorStatus208.Name = "lblMotorStatus208";
            this.lblMotorStatus208.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus208.TabIndex = 173;
            this.lblMotorStatus208.Text = "Emergency Stop";
            this.lblMotorStatus208.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus207
            // 
            this.lblMotorStatus207.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus207.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus207.Location = new System.Drawing.Point(34, 201);
            this.lblMotorStatus207.Name = "lblMotorStatus207";
            this.lblMotorStatus207.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus207.TabIndex = 172;
            this.lblMotorStatus207.Text = "Seovo Alarm";
            this.lblMotorStatus207.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus206
            // 
            this.lblMotorStatus206.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus206.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus206.Location = new System.Drawing.Point(34, 173);
            this.lblMotorStatus206.Name = "lblMotorStatus206";
            this.lblMotorStatus206.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus206.TabIndex = 171;
            this.lblMotorStatus206.Text = "Negative SW S-Stop";
            this.lblMotorStatus206.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus205
            // 
            this.lblMotorStatus205.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus205.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus205.Location = new System.Drawing.Point(34, 145);
            this.lblMotorStatus205.Name = "lblMotorStatus205";
            this.lblMotorStatus205.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus205.TabIndex = 170;
            this.lblMotorStatus205.Text = "Positive SW S-Stop";
            this.lblMotorStatus205.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus204
            // 
            this.lblMotorStatus204.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus204.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus204.Location = new System.Drawing.Point(34, 117);
            this.lblMotorStatus204.Name = "lblMotorStatus204";
            this.lblMotorStatus204.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus204.TabIndex = 169;
            this.lblMotorStatus204.Text = "Negative SW E-Stop";
            this.lblMotorStatus204.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus203
            // 
            this.lblMotorStatus203.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus203.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus203.Location = new System.Drawing.Point(34, 89);
            this.lblMotorStatus203.Name = "lblMotorStatus203";
            this.lblMotorStatus203.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus203.TabIndex = 168;
            this.lblMotorStatus203.Text = "Positive SW E-Stop";
            this.lblMotorStatus203.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus224
            // 
            this.lblMotorStatus224.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus224.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus224.Location = new System.Drawing.Point(34, 677);
            this.lblMotorStatus224.Name = "lblMotorStatus224";
            this.lblMotorStatus224.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus224.TabIndex = 136;
            this.lblMotorStatus224.Text = "Mode Terminal";
            this.lblMotorStatus224.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotionEndStatus
            // 
            this.lblMotionEndStatus.AutoEllipsis = true;
            this.lblMotionEndStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMotionEndStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMotionEndStatus.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMotionEndStatus.ForeColor = System.Drawing.Color.Black;
            this.lblMotionEndStatus.Location = new System.Drawing.Point(0, 0);
            this.lblMotionEndStatus.Name = "lblMotionEndStatus";
            this.lblMotionEndStatus.Size = new System.Drawing.Size(324, 24);
            this.lblMotionEndStatus.TabIndex = 9;
            this.lblMotionEndStatus.Text = "■ Motion End Status";
            this.lblMotionEndStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus224
            // 
            this.btnMotorStatus224.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus224.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus224.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus224.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus224.Location = new System.Drawing.Point(3, 677);
            this.btnMotorStatus224.Name = "btnMotorStatus224";
            this.btnMotorStatus224.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus224.TabIndex = 158;
            this.btnMotorStatus224.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus201
            // 
            this.btnMotorStatus201.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus201.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus201.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus201.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus201.Location = new System.Drawing.Point(3, 33);
            this.btnMotorStatus201.Name = "btnMotorStatus201";
            this.btnMotorStatus201.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus201.TabIndex = 112;
            this.btnMotorStatus201.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus223
            // 
            this.btnMotorStatus223.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus223.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus223.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus223.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus223.Location = new System.Drawing.Point(3, 649);
            this.btnMotorStatus223.Name = "btnMotorStatus223";
            this.btnMotorStatus223.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus223.TabIndex = 157;
            this.btnMotorStatus223.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus201
            // 
            this.lblMotorStatus201.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus201.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus201.Location = new System.Drawing.Point(34, 33);
            this.lblMotorStatus201.Name = "lblMotorStatus201";
            this.lblMotorStatus201.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus201.TabIndex = 111;
            this.lblMotorStatus201.Text = "Position End Limit";
            this.lblMotorStatus201.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus222
            // 
            this.btnMotorStatus222.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus222.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus222.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus222.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus222.Location = new System.Drawing.Point(3, 621);
            this.btnMotorStatus222.Name = "btnMotorStatus222";
            this.btnMotorStatus222.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus222.TabIndex = 156;
            this.btnMotorStatus222.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus202
            // 
            this.lblMotorStatus202.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus202.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus202.Location = new System.Drawing.Point(34, 61);
            this.lblMotorStatus202.Name = "lblMotorStatus202";
            this.lblMotorStatus202.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus202.TabIndex = 113;
            this.lblMotorStatus202.Text = "Negative End Limit";
            this.lblMotorStatus202.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus221
            // 
            this.btnMotorStatus221.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus221.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus221.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus221.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus221.Location = new System.Drawing.Point(3, 593);
            this.btnMotorStatus221.Name = "btnMotorStatus221";
            this.btnMotorStatus221.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus221.TabIndex = 155;
            this.btnMotorStatus221.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus202
            // 
            this.btnMotorStatus202.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus202.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus202.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus202.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus202.Location = new System.Drawing.Point(3, 61);
            this.btnMotorStatus202.Name = "btnMotorStatus202";
            this.btnMotorStatus202.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus202.TabIndex = 114;
            this.btnMotorStatus202.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus220
            // 
            this.btnMotorStatus220.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus220.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus220.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus220.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus220.Location = new System.Drawing.Point(3, 565);
            this.btnMotorStatus220.Name = "btnMotorStatus220";
            this.btnMotorStatus220.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus220.TabIndex = 154;
            this.btnMotorStatus220.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus219
            // 
            this.btnMotorStatus219.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus219.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus219.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus219.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus219.Location = new System.Drawing.Point(3, 537);
            this.btnMotorStatus219.Name = "btnMotorStatus219";
            this.btnMotorStatus219.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus219.TabIndex = 153;
            this.btnMotorStatus219.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus218
            // 
            this.btnMotorStatus218.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus218.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus218.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus218.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus218.Location = new System.Drawing.Point(3, 509);
            this.btnMotorStatus218.Name = "btnMotorStatus218";
            this.btnMotorStatus218.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus218.TabIndex = 152;
            this.btnMotorStatus218.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus217
            // 
            this.btnMotorStatus217.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus217.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus217.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus217.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus217.Location = new System.Drawing.Point(3, 481);
            this.btnMotorStatus217.Name = "btnMotorStatus217";
            this.btnMotorStatus217.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus217.TabIndex = 151;
            this.btnMotorStatus217.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus216
            // 
            this.btnMotorStatus216.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus216.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus216.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus216.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus216.Location = new System.Drawing.Point(3, 453);
            this.btnMotorStatus216.Name = "btnMotorStatus216";
            this.btnMotorStatus216.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus216.TabIndex = 150;
            this.btnMotorStatus216.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus215
            // 
            this.btnMotorStatus215.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus215.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus215.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus215.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus215.Location = new System.Drawing.Point(3, 425);
            this.btnMotorStatus215.Name = "btnMotorStatus215";
            this.btnMotorStatus215.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus215.TabIndex = 149;
            this.btnMotorStatus215.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus214
            // 
            this.btnMotorStatus214.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus214.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus214.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus214.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus214.Location = new System.Drawing.Point(3, 397);
            this.btnMotorStatus214.Name = "btnMotorStatus214";
            this.btnMotorStatus214.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus214.TabIndex = 148;
            this.btnMotorStatus214.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus213
            // 
            this.btnMotorStatus213.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus213.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus213.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus213.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus213.Location = new System.Drawing.Point(3, 369);
            this.btnMotorStatus213.Name = "btnMotorStatus213";
            this.btnMotorStatus213.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus213.TabIndex = 147;
            this.btnMotorStatus213.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus212
            // 
            this.btnMotorStatus212.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus212.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus212.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus212.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus212.Location = new System.Drawing.Point(3, 341);
            this.btnMotorStatus212.Name = "btnMotorStatus212";
            this.btnMotorStatus212.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus212.TabIndex = 146;
            this.btnMotorStatus212.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus211
            // 
            this.btnMotorStatus211.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus211.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus211.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus211.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus211.Location = new System.Drawing.Point(3, 313);
            this.btnMotorStatus211.Name = "btnMotorStatus211";
            this.btnMotorStatus211.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus211.TabIndex = 145;
            this.btnMotorStatus211.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus210
            // 
            this.btnMotorStatus210.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus210.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus210.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus210.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus210.Location = new System.Drawing.Point(3, 285);
            this.btnMotorStatus210.Name = "btnMotorStatus210";
            this.btnMotorStatus210.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus210.TabIndex = 144;
            this.btnMotorStatus210.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus209
            // 
            this.btnMotorStatus209.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus209.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus209.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus209.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus209.Location = new System.Drawing.Point(3, 257);
            this.btnMotorStatus209.Name = "btnMotorStatus209";
            this.btnMotorStatus209.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus209.TabIndex = 143;
            this.btnMotorStatus209.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus208
            // 
            this.btnMotorStatus208.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus208.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus208.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus208.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus208.Location = new System.Drawing.Point(3, 229);
            this.btnMotorStatus208.Name = "btnMotorStatus208";
            this.btnMotorStatus208.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus208.TabIndex = 142;
            this.btnMotorStatus208.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus215
            // 
            this.lblMotorStatus215.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus215.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus215.Location = new System.Drawing.Point(34, 425);
            this.lblMotorStatus215.Name = "lblMotorStatus215";
            this.lblMotorStatus215.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus215.TabIndex = 127;
            this.lblMotorStatus215.Text = "MPG Input Error";
            this.lblMotorStatus215.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus207
            // 
            this.btnMotorStatus207.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus207.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus207.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus207.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus207.Location = new System.Drawing.Point(3, 201);
            this.btnMotorStatus207.Name = "btnMotorStatus207";
            this.btnMotorStatus207.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus207.TabIndex = 141;
            this.btnMotorStatus207.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus216
            // 
            this.lblMotorStatus216.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus216.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus216.Location = new System.Drawing.Point(34, 453);
            this.lblMotorStatus216.Name = "lblMotorStatus216";
            this.lblMotorStatus216.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus216.TabIndex = 128;
            this.lblMotorStatus216.Text = "ERRSTATE";
            this.lblMotorStatus216.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus206
            // 
            this.btnMotorStatus206.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus206.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus206.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus206.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus206.Location = new System.Drawing.Point(3, 173);
            this.btnMotorStatus206.Name = "btnMotorStatus206";
            this.btnMotorStatus206.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus206.TabIndex = 140;
            this.btnMotorStatus206.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus217
            // 
            this.lblMotorStatus217.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus217.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus217.Location = new System.Drawing.Point(34, 481);
            this.lblMotorStatus217.Name = "lblMotorStatus217";
            this.lblMotorStatus217.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus217.TabIndex = 129;
            this.lblMotorStatus217.Text = "Interpolatioon Data Error";
            this.lblMotorStatus217.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus205
            // 
            this.btnMotorStatus205.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus205.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus205.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus205.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus205.Location = new System.Drawing.Point(3, 145);
            this.btnMotorStatus205.Name = "btnMotorStatus205";
            this.btnMotorStatus205.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus205.TabIndex = 139;
            this.btnMotorStatus205.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus218
            // 
            this.lblMotorStatus218.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus218.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus218.Location = new System.Drawing.Point(34, 509);
            this.lblMotorStatus218.Name = "lblMotorStatus218";
            this.lblMotorStatus218.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus218.TabIndex = 130;
            this.lblMotorStatus218.Text = "Mode Terminal";
            this.lblMotorStatus218.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus204
            // 
            this.btnMotorStatus204.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus204.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus204.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus204.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus204.Location = new System.Drawing.Point(3, 117);
            this.btnMotorStatus204.Name = "btnMotorStatus204";
            this.btnMotorStatus204.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus204.TabIndex = 138;
            this.btnMotorStatus204.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus219
            // 
            this.lblMotorStatus219.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus219.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus219.Location = new System.Drawing.Point(34, 537);
            this.lblMotorStatus219.Name = "lblMotorStatus219";
            this.lblMotorStatus219.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus219.TabIndex = 131;
            this.lblMotorStatus219.Text = "Mode Terminal";
            this.lblMotorStatus219.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus203
            // 
            this.btnMotorStatus203.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus203.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus203.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus203.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus203.Location = new System.Drawing.Point(3, 89);
            this.btnMotorStatus203.Name = "btnMotorStatus203";
            this.btnMotorStatus203.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus203.TabIndex = 137;
            this.btnMotorStatus203.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus220
            // 
            this.lblMotorStatus220.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus220.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus220.Location = new System.Drawing.Point(34, 565);
            this.lblMotorStatus220.Name = "lblMotorStatus220";
            this.lblMotorStatus220.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus220.TabIndex = 132;
            this.lblMotorStatus220.Text = "Mode Terminal";
            this.lblMotorStatus220.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus223
            // 
            this.lblMotorStatus223.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus223.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus223.Location = new System.Drawing.Point(34, 649);
            this.lblMotorStatus223.Name = "lblMotorStatus223";
            this.lblMotorStatus223.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus223.TabIndex = 135;
            this.lblMotorStatus223.Text = "Mode Terminal";
            this.lblMotorStatus223.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus221
            // 
            this.lblMotorStatus221.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus221.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus221.Location = new System.Drawing.Point(34, 593);
            this.lblMotorStatus221.Name = "lblMotorStatus221";
            this.lblMotorStatus221.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus221.TabIndex = 133;
            this.lblMotorStatus221.Text = "Mode Terminal";
            this.lblMotorStatus221.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus222
            // 
            this.lblMotorStatus222.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus222.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus222.Location = new System.Drawing.Point(34, 621);
            this.lblMotorStatus222.Name = "lblMotorStatus222";
            this.lblMotorStatus222.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus222.TabIndex = 134;
            this.lblMotorStatus222.Text = "Mode Terminal";
            this.lblMotorStatus222.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnMotorStatus124);
            this.panel15.Controls.Add(this.btnMotorStatus123);
            this.panel15.Controls.Add(this.btnMotorStatus122);
            this.panel15.Controls.Add(this.btnMotorStatus121);
            this.panel15.Controls.Add(this.btnMotorStatus120);
            this.panel15.Controls.Add(this.btnMotorStatus119);
            this.panel15.Controls.Add(this.btnMotorStatus118);
            this.panel15.Controls.Add(this.btnMotorStatus117);
            this.panel15.Controls.Add(this.btnMotorStatus116);
            this.panel15.Controls.Add(this.btnMotorStatus115);
            this.panel15.Controls.Add(this.btnMotorStatus114);
            this.panel15.Controls.Add(this.btnMotorStatus113);
            this.panel15.Controls.Add(this.btnMotorStatus112);
            this.panel15.Controls.Add(this.btnMotorStatus111);
            this.panel15.Controls.Add(this.btnMotorStatus110);
            this.panel15.Controls.Add(this.btnMotorStatus109);
            this.panel15.Controls.Add(this.btnMotorStatus108);
            this.panel15.Controls.Add(this.btnMotorStatus107);
            this.panel15.Controls.Add(this.btnMotorStatus106);
            this.panel15.Controls.Add(this.btnMotorStatus105);
            this.panel15.Controls.Add(this.btnMotorStatus104);
            this.panel15.Controls.Add(this.btnMotorStatus103);
            this.panel15.Controls.Add(this.lblMotorStatus123);
            this.panel15.Controls.Add(this.lblMotorStatus122);
            this.panel15.Controls.Add(this.lblMotorStatus121);
            this.panel15.Controls.Add(this.lblMotorStatus120);
            this.panel15.Controls.Add(this.lblMotorStatus119);
            this.panel15.Controls.Add(this.lblMotorStatus118);
            this.panel15.Controls.Add(this.lblMotorStatus117);
            this.panel15.Controls.Add(this.lblMotorStatus116);
            this.panel15.Controls.Add(this.lblMotorStatus115);
            this.panel15.Controls.Add(this.lblMotorStatus114);
            this.panel15.Controls.Add(this.lblMotorStatus113);
            this.panel15.Controls.Add(this.lblMotorStatus112);
            this.panel15.Controls.Add(this.lblMotorStatus111);
            this.panel15.Controls.Add(this.lblMotorStatus110);
            this.panel15.Controls.Add(this.lblMotorStatus109);
            this.panel15.Controls.Add(this.lblMotorStatus108);
            this.panel15.Controls.Add(this.lblMotorStatus107);
            this.panel15.Controls.Add(this.lblMotorStatus106);
            this.panel15.Controls.Add(this.lblMotorStatus105);
            this.panel15.Controls.Add(this.lblMotorStatus104);
            this.panel15.Controls.Add(this.lblMotorStatus103);
            this.panel15.Controls.Add(this.btnMotorStatus102);
            this.panel15.Controls.Add(this.lblMotorStatus102);
            this.panel15.Controls.Add(this.btnMotorStatus101);
            this.panel15.Controls.Add(this.lblMotorStatus101);
            this.panel15.Controls.Add(this.lblMechanicalStatus);
            this.panel15.Location = new System.Drawing.Point(3, 29);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(326, 711);
            this.panel15.TabIndex = 458;
            // 
            // btnMotorStatus124
            // 
            this.btnMotorStatus124.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus124.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus124.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus124.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus124.Location = new System.Drawing.Point(3, 677);
            this.btnMotorStatus124.Name = "btnMotorStatus124";
            this.btnMotorStatus124.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus124.TabIndex = 110;
            this.btnMotorStatus124.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus123
            // 
            this.btnMotorStatus123.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus123.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus123.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus123.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus123.Location = new System.Drawing.Point(3, 649);
            this.btnMotorStatus123.Name = "btnMotorStatus123";
            this.btnMotorStatus123.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus123.TabIndex = 109;
            this.btnMotorStatus123.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus122
            // 
            this.btnMotorStatus122.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus122.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus122.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus122.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus122.Location = new System.Drawing.Point(3, 621);
            this.btnMotorStatus122.Name = "btnMotorStatus122";
            this.btnMotorStatus122.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus122.TabIndex = 108;
            this.btnMotorStatus122.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus121
            // 
            this.btnMotorStatus121.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus121.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus121.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus121.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus121.Location = new System.Drawing.Point(3, 593);
            this.btnMotorStatus121.Name = "btnMotorStatus121";
            this.btnMotorStatus121.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus121.TabIndex = 107;
            this.btnMotorStatus121.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus120
            // 
            this.btnMotorStatus120.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus120.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus120.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus120.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus120.Location = new System.Drawing.Point(3, 565);
            this.btnMotorStatus120.Name = "btnMotorStatus120";
            this.btnMotorStatus120.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus120.TabIndex = 106;
            this.btnMotorStatus120.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus119
            // 
            this.btnMotorStatus119.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus119.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus119.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus119.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus119.Location = new System.Drawing.Point(3, 537);
            this.btnMotorStatus119.Name = "btnMotorStatus119";
            this.btnMotorStatus119.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus119.TabIndex = 105;
            this.btnMotorStatus119.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus118
            // 
            this.btnMotorStatus118.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus118.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus118.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus118.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus118.Location = new System.Drawing.Point(3, 509);
            this.btnMotorStatus118.Name = "btnMotorStatus118";
            this.btnMotorStatus118.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus118.TabIndex = 104;
            this.btnMotorStatus118.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus117
            // 
            this.btnMotorStatus117.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus117.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus117.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus117.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus117.Location = new System.Drawing.Point(3, 481);
            this.btnMotorStatus117.Name = "btnMotorStatus117";
            this.btnMotorStatus117.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus117.TabIndex = 103;
            this.btnMotorStatus117.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus116
            // 
            this.btnMotorStatus116.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus116.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus116.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus116.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus116.Location = new System.Drawing.Point(3, 453);
            this.btnMotorStatus116.Name = "btnMotorStatus116";
            this.btnMotorStatus116.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus116.TabIndex = 102;
            this.btnMotorStatus116.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus115
            // 
            this.btnMotorStatus115.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus115.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus115.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus115.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus115.Location = new System.Drawing.Point(3, 425);
            this.btnMotorStatus115.Name = "btnMotorStatus115";
            this.btnMotorStatus115.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus115.TabIndex = 101;
            this.btnMotorStatus115.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus114
            // 
            this.btnMotorStatus114.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus114.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus114.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus114.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus114.Location = new System.Drawing.Point(3, 397);
            this.btnMotorStatus114.Name = "btnMotorStatus114";
            this.btnMotorStatus114.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus114.TabIndex = 100;
            this.btnMotorStatus114.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus113
            // 
            this.btnMotorStatus113.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus113.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus113.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus113.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus113.Location = new System.Drawing.Point(3, 369);
            this.btnMotorStatus113.Name = "btnMotorStatus113";
            this.btnMotorStatus113.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus113.TabIndex = 99;
            this.btnMotorStatus113.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus112
            // 
            this.btnMotorStatus112.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus112.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus112.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus112.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus112.Location = new System.Drawing.Point(3, 341);
            this.btnMotorStatus112.Name = "btnMotorStatus112";
            this.btnMotorStatus112.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus112.TabIndex = 98;
            this.btnMotorStatus112.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus111
            // 
            this.btnMotorStatus111.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus111.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus111.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus111.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus111.Location = new System.Drawing.Point(3, 313);
            this.btnMotorStatus111.Name = "btnMotorStatus111";
            this.btnMotorStatus111.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus111.TabIndex = 97;
            this.btnMotorStatus111.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus110
            // 
            this.btnMotorStatus110.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus110.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus110.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus110.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus110.Location = new System.Drawing.Point(3, 285);
            this.btnMotorStatus110.Name = "btnMotorStatus110";
            this.btnMotorStatus110.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus110.TabIndex = 96;
            this.btnMotorStatus110.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus109
            // 
            this.btnMotorStatus109.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus109.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus109.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus109.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus109.Location = new System.Drawing.Point(3, 257);
            this.btnMotorStatus109.Name = "btnMotorStatus109";
            this.btnMotorStatus109.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus109.TabIndex = 95;
            this.btnMotorStatus109.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus108
            // 
            this.btnMotorStatus108.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus108.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus108.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus108.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus108.Location = new System.Drawing.Point(3, 229);
            this.btnMotorStatus108.Name = "btnMotorStatus108";
            this.btnMotorStatus108.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus108.TabIndex = 94;
            this.btnMotorStatus108.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus107
            // 
            this.btnMotorStatus107.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus107.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus107.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus107.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus107.Location = new System.Drawing.Point(3, 201);
            this.btnMotorStatus107.Name = "btnMotorStatus107";
            this.btnMotorStatus107.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus107.TabIndex = 93;
            this.btnMotorStatus107.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus106
            // 
            this.btnMotorStatus106.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus106.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus106.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus106.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus106.Location = new System.Drawing.Point(3, 173);
            this.btnMotorStatus106.Name = "btnMotorStatus106";
            this.btnMotorStatus106.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus106.TabIndex = 92;
            this.btnMotorStatus106.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus105
            // 
            this.btnMotorStatus105.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus105.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus105.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus105.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus105.Location = new System.Drawing.Point(3, 145);
            this.btnMotorStatus105.Name = "btnMotorStatus105";
            this.btnMotorStatus105.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus105.TabIndex = 91;
            this.btnMotorStatus105.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus104
            // 
            this.btnMotorStatus104.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus104.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus104.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus104.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus104.Location = new System.Drawing.Point(3, 117);
            this.btnMotorStatus104.Name = "btnMotorStatus104";
            this.btnMotorStatus104.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus104.TabIndex = 90;
            this.btnMotorStatus104.UseVisualStyleBackColor = false;
            // 
            // btnMotorStatus103
            // 
            this.btnMotorStatus103.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus103.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus103.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus103.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus103.Location = new System.Drawing.Point(3, 89);
            this.btnMotorStatus103.Name = "btnMotorStatus103";
            this.btnMotorStatus103.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus103.TabIndex = 89;
            this.btnMotorStatus103.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus123
            // 
            this.lblMotorStatus123.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus123.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus123.Location = new System.Drawing.Point(34, 649);
            this.lblMotorStatus123.Name = "lblMotorStatus123";
            this.lblMotorStatus123.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus123.TabIndex = 88;
            this.lblMotorStatus123.Text = "Mode Terminal";
            this.lblMotorStatus123.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus122
            // 
            this.lblMotorStatus122.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus122.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus122.Location = new System.Drawing.Point(34, 621);
            this.lblMotorStatus122.Name = "lblMotorStatus122";
            this.lblMotorStatus122.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus122.TabIndex = 87;
            this.lblMotorStatus122.Text = "Mode Terminal";
            this.lblMotorStatus122.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus121
            // 
            this.lblMotorStatus121.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus121.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus121.Location = new System.Drawing.Point(34, 593);
            this.lblMotorStatus121.Name = "lblMotorStatus121";
            this.lblMotorStatus121.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus121.TabIndex = 86;
            this.lblMotorStatus121.Text = "Mode Terminal";
            this.lblMotorStatus121.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus120
            // 
            this.lblMotorStatus120.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus120.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus120.Location = new System.Drawing.Point(34, 565);
            this.lblMotorStatus120.Name = "lblMotorStatus120";
            this.lblMotorStatus120.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus120.TabIndex = 85;
            this.lblMotorStatus120.Text = "Mode Terminal";
            this.lblMotorStatus120.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus119
            // 
            this.lblMotorStatus119.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus119.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus119.Location = new System.Drawing.Point(34, 537);
            this.lblMotorStatus119.Name = "lblMotorStatus119";
            this.lblMotorStatus119.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus119.TabIndex = 84;
            this.lblMotorStatus119.Text = "Mode Terminal";
            this.lblMotorStatus119.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus118
            // 
            this.lblMotorStatus118.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus118.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus118.Location = new System.Drawing.Point(34, 509);
            this.lblMotorStatus118.Name = "lblMotorStatus118";
            this.lblMotorStatus118.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus118.TabIndex = 83;
            this.lblMotorStatus118.Text = "Mode Terminal";
            this.lblMotorStatus118.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus117
            // 
            this.lblMotorStatus117.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus117.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus117.Location = new System.Drawing.Point(34, 481);
            this.lblMotorStatus117.Name = "lblMotorStatus117";
            this.lblMotorStatus117.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus117.TabIndex = 82;
            this.lblMotorStatus117.Text = "Mode Terminal";
            this.lblMotorStatus117.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus116
            // 
            this.lblMotorStatus116.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus116.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus116.Location = new System.Drawing.Point(34, 453);
            this.lblMotorStatus116.Name = "lblMotorStatus116";
            this.lblMotorStatus116.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus116.TabIndex = 81;
            this.lblMotorStatus116.Text = "SQSTP 2";
            this.lblMotorStatus116.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus115
            // 
            this.lblMotorStatus115.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus115.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus115.Location = new System.Drawing.Point(34, 425);
            this.lblMotorStatus115.Name = "lblMotorStatus115";
            this.lblMotorStatus115.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus115.TabIndex = 80;
            this.lblMotorStatus115.Text = "SQSTP 1";
            this.lblMotorStatus115.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus114
            // 
            this.lblMotorStatus114.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus114.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus114.Location = new System.Drawing.Point(34, 397);
            this.lblMotorStatus114.Name = "lblMotorStatus114";
            this.lblMotorStatus114.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus114.TabIndex = 79;
            this.lblMotorStatus114.Text = "SQSTR 2";
            this.lblMotorStatus114.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus113
            // 
            this.lblMotorStatus113.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus113.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus113.Location = new System.Drawing.Point(34, 369);
            this.lblMotorStatus113.Name = "lblMotorStatus113";
            this.lblMotorStatus113.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus113.TabIndex = 77;
            this.lblMotorStatus113.Text = "SQSTR 1";
            this.lblMotorStatus113.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus112
            // 
            this.lblMotorStatus112.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus112.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus112.Location = new System.Drawing.Point(34, 341);
            this.lblMotorStatus112.Name = "lblMotorStatus112";
            this.lblMotorStatus112.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus112.TabIndex = 76;
            this.lblMotorStatus112.Text = "EXMP(MPG)";
            this.lblMotorStatus112.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus111
            // 
            this.lblMotorStatus111.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus111.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus111.Location = new System.Drawing.Point(34, 313);
            this.lblMotorStatus111.Name = "lblMotorStatus111";
            this.lblMotorStatus111.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus111.TabIndex = 75;
            this.lblMotorStatus111.Text = "EXPP(MPG)";
            this.lblMotorStatus111.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus110
            // 
            this.lblMotorStatus110.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus110.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus110.Location = new System.Drawing.Point(34, 285);
            this.lblMotorStatus110.Name = "lblMotorStatus110";
            this.lblMotorStatus110.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus110.TabIndex = 74;
            this.lblMotorStatus110.Text = "Encoder Down";
            this.lblMotorStatus110.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus109
            // 
            this.lblMotorStatus109.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus109.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus109.Location = new System.Drawing.Point(34, 257);
            this.lblMotorStatus109.Name = "lblMotorStatus109";
            this.lblMotorStatus109.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus109.TabIndex = 73;
            this.lblMotorStatus109.Text = "Encoder Up";
            this.lblMotorStatus109.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus108
            // 
            this.lblMotorStatus108.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus108.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus108.Location = new System.Drawing.Point(34, 229);
            this.lblMotorStatus108.Name = "lblMotorStatus108";
            this.lblMotorStatus108.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus108.TabIndex = 72;
            this.lblMotorStatus108.Text = "Home";
            this.lblMotorStatus108.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus107
            // 
            this.lblMotorStatus107.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus107.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus107.Location = new System.Drawing.Point(34, 201);
            this.lblMotorStatus107.Name = "lblMotorStatus107";
            this.lblMotorStatus107.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus107.TabIndex = 71;
            this.lblMotorStatus107.Text = "Emergency Stop";
            this.lblMotorStatus107.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus106
            // 
            this.lblMotorStatus106.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus106.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus106.Location = new System.Drawing.Point(34, 173);
            this.lblMotorStatus106.Name = "lblMotorStatus106";
            this.lblMotorStatus106.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus106.TabIndex = 70;
            this.lblMotorStatus106.Text = "Inposition";
            this.lblMotorStatus106.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus105
            // 
            this.lblMotorStatus105.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus105.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus105.Location = new System.Drawing.Point(34, 145);
            this.lblMotorStatus105.Name = "lblMotorStatus105";
            this.lblMotorStatus105.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus105.TabIndex = 69;
            this.lblMotorStatus105.Text = "Alarm";
            this.lblMotorStatus105.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus104
            // 
            this.lblMotorStatus104.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus104.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus104.Location = new System.Drawing.Point(34, 117);
            this.lblMotorStatus104.Name = "lblMotorStatus104";
            this.lblMotorStatus104.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus104.TabIndex = 68;
            this.lblMotorStatus104.Text = "Negative Slow Stop Limit";
            this.lblMotorStatus104.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus103
            // 
            this.lblMotorStatus103.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus103.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus103.Location = new System.Drawing.Point(34, 89);
            this.lblMotorStatus103.Name = "lblMotorStatus103";
            this.lblMotorStatus103.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus103.TabIndex = 67;
            this.lblMotorStatus103.Text = "Positive Slow Stop Limit";
            this.lblMotorStatus103.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus102
            // 
            this.btnMotorStatus102.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus102.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus102.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus102.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus102.Location = new System.Drawing.Point(3, 61);
            this.btnMotorStatus102.Name = "btnMotorStatus102";
            this.btnMotorStatus102.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus102.TabIndex = 66;
            this.btnMotorStatus102.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus102
            // 
            this.lblMotorStatus102.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus102.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus102.Location = new System.Drawing.Point(34, 61);
            this.lblMotorStatus102.Name = "lblMotorStatus102";
            this.lblMotorStatus102.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus102.TabIndex = 65;
            this.lblMotorStatus102.Text = "Negative End Limit";
            this.lblMotorStatus102.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMotorStatus101
            // 
            this.btnMotorStatus101.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnMotorStatus101.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMotorStatus101.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotorStatus101.ForeColor = System.Drawing.Color.Black;
            this.btnMotorStatus101.Location = new System.Drawing.Point(3, 33);
            this.btnMotorStatus101.Name = "btnMotorStatus101";
            this.btnMotorStatus101.Size = new System.Drawing.Size(25, 25);
            this.btnMotorStatus101.TabIndex = 64;
            this.btnMotorStatus101.UseVisualStyleBackColor = false;
            // 
            // lblMotorStatus101
            // 
            this.lblMotorStatus101.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMotorStatus101.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus101.Location = new System.Drawing.Point(34, 33);
            this.lblMotorStatus101.Name = "lblMotorStatus101";
            this.lblMotorStatus101.Size = new System.Drawing.Size(287, 24);
            this.lblMotorStatus101.TabIndex = 58;
            this.lblMotorStatus101.Text = "Position End Limit";
            this.lblMotorStatus101.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMechanicalStatus
            // 
            this.lblMechanicalStatus.AutoEllipsis = true;
            this.lblMechanicalStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMechanicalStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMechanicalStatus.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMechanicalStatus.ForeColor = System.Drawing.Color.Black;
            this.lblMechanicalStatus.Location = new System.Drawing.Point(0, 0);
            this.lblMechanicalStatus.Name = "lblMechanicalStatus";
            this.lblMechanicalStatus.Size = new System.Drawing.Size(324, 24);
            this.lblMechanicalStatus.TabIndex = 9;
            this.lblMechanicalStatus.Text = "■ Mechanical Status";
            this.lblMechanicalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMotorStatus
            // 
            this.lblMotorStatus.AutoEllipsis = true;
            this.lblMotorStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMotorStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMotorStatus.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMotorStatus.ForeColor = System.Drawing.Color.Black;
            this.lblMotorStatus.Location = new System.Drawing.Point(0, 0);
            this.lblMotorStatus.Name = "lblMotorStatus";
            this.lblMotorStatus.Size = new System.Drawing.Size(998, 26);
            this.lblMotorStatus.TabIndex = 9;
            this.lblMotorStatus.Text = "■ Motor Status";
            this.lblMotorStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.flpSevoMotors);
            this.panel3.Location = new System.Drawing.Point(3, 102);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(713, 745);
            this.panel3.TabIndex = 463;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(711, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ 축 리스트";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlManagerMotorStatus
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panle11);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerMotorStatus";
            this.Size = new System.Drawing.Size(1740, 875);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.panle11.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flpSevoMotors;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btnMonitorLoader;
        private System.Windows.Forms.Button btnMonitorProcess;
        private System.Windows.Forms.Button btnMonitorUnloader;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button btnAjinMotor;
        private System.Windows.Forms.Button btnUmacMotor;
        private System.Windows.Forms.Button btnEZiMotor;
        private System.Windows.Forms.Panel panle11;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label lblDriveStatus;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label lblMotionEndStatus;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label lblMotorStatus101;
        internal System.Windows.Forms.Label lblMechanicalStatus;
        internal System.Windows.Forms.Label lblMotorStatus;
        private System.Windows.Forms.Button btnMotorStatus101;
        private System.Windows.Forms.Label lblMotorStatus124;
        private System.Windows.Forms.Button btnMotorStatus124;
        private System.Windows.Forms.Button btnMotorStatus123;
        private System.Windows.Forms.Button btnMotorStatus122;
        private System.Windows.Forms.Button btnMotorStatus121;
        private System.Windows.Forms.Button btnMotorStatus120;
        private System.Windows.Forms.Button btnMotorStatus119;
        private System.Windows.Forms.Button btnMotorStatus118;
        private System.Windows.Forms.Button btnMotorStatus117;
        private System.Windows.Forms.Button btnMotorStatus116;
        private System.Windows.Forms.Button btnMotorStatus115;
        private System.Windows.Forms.Button btnMotorStatus114;
        private System.Windows.Forms.Button btnMotorStatus113;
        private System.Windows.Forms.Button btnMotorStatus112;
        private System.Windows.Forms.Button btnMotorStatus111;
        private System.Windows.Forms.Button btnMotorStatus110;
        private System.Windows.Forms.Button btnMotorStatus109;
        private System.Windows.Forms.Button btnMotorStatus108;
        private System.Windows.Forms.Button btnMotorStatus107;
        private System.Windows.Forms.Button btnMotorStatus106;
        private System.Windows.Forms.Button btnMotorStatus105;
        private System.Windows.Forms.Button btnMotorStatus104;
        private System.Windows.Forms.Button btnMotorStatus103;
        private System.Windows.Forms.Label lblMotorStatus123;
        private System.Windows.Forms.Label lblMotorStatus122;
        private System.Windows.Forms.Label lblMotorStatus121;
        private System.Windows.Forms.Label lblMotorStatus120;
        private System.Windows.Forms.Label lblMotorStatus119;
        private System.Windows.Forms.Label lblMotorStatus118;
        private System.Windows.Forms.Label lblMotorStatus117;
        private System.Windows.Forms.Label lblMotorStatus116;
        private System.Windows.Forms.Label lblMotorStatus115;
        private System.Windows.Forms.Label lblMotorStatus114;
        private System.Windows.Forms.Label lblMotorStatus113;
        private System.Windows.Forms.Label lblMotorStatus112;
        private System.Windows.Forms.Label lblMotorStatus111;
        private System.Windows.Forms.Label lblMotorStatus110;
        private System.Windows.Forms.Label lblMotorStatus109;
        private System.Windows.Forms.Label lblMotorStatus108;
        private System.Windows.Forms.Label lblMotorStatus107;
        private System.Windows.Forms.Label lblMotorStatus106;
        private System.Windows.Forms.Label lblMotorStatus105;
        private System.Windows.Forms.Label lblMotorStatus104;
        private System.Windows.Forms.Label lblMotorStatus103;
        private System.Windows.Forms.Button btnMotorStatus102;
        private System.Windows.Forms.Label lblMotorStatus102;
        private System.Windows.Forms.Label lblMotorStatus324;
        private System.Windows.Forms.Label lblMotorStatus322;
        private System.Windows.Forms.Label lblMotorStatus319;
        private System.Windows.Forms.Label lblMotorStatus323;
        private System.Windows.Forms.Label lblMotorStatus321;
        private System.Windows.Forms.Label lblMotorStatus318;
        private System.Windows.Forms.Label lblMotorStatus320;
        private System.Windows.Forms.Label lblMotorStatus316;
        private System.Windows.Forms.Label lblMotorStatus315;
        private System.Windows.Forms.Label lblMotorStatus313;
        private System.Windows.Forms.Label lblMotorStatus317;
        private System.Windows.Forms.Label lblMotorStatus314;
        private System.Windows.Forms.Label lblMotorStatus312;
        private System.Windows.Forms.Button btnMotorStatus324;
        private System.Windows.Forms.Button btnMotorStatus323;
        private System.Windows.Forms.Button btnMotorStatus322;
        private System.Windows.Forms.Button btnMotorStatus321;
        private System.Windows.Forms.Button btnMotorStatus320;
        private System.Windows.Forms.Button btnMotorStatus319;
        private System.Windows.Forms.Button btnMotorStatus318;
        private System.Windows.Forms.Button btnMotorStatus317;
        private System.Windows.Forms.Button btnMotorStatus315;
        private System.Windows.Forms.Button btnMotorStatus313;
        private System.Windows.Forms.Label lblMotorStatus311;
        private System.Windows.Forms.Button btnMotorStatus312;
        private System.Windows.Forms.Label lblMotorStatus309;
        private System.Windows.Forms.Button btnMotorStatus310;
        private System.Windows.Forms.Button btnMotorStatus316;
        private System.Windows.Forms.Button btnMotorStatus314;
        private System.Windows.Forms.Button btnMotorStatus311;
        private System.Windows.Forms.Label lblMotorStatus310;
        private System.Windows.Forms.Button btnMotorStatus309;
        private System.Windows.Forms.Label lblMotorStatus308;
        private System.Windows.Forms.Button btnMotorStatus308;
        private System.Windows.Forms.Label lblMotorStatus307;
        private System.Windows.Forms.Button btnMotorStatus307;
        private System.Windows.Forms.Label lblMotorStatus306;
        private System.Windows.Forms.Button btnMotorStatus306;
        private System.Windows.Forms.Label lblMotorStatus305;
        private System.Windows.Forms.Button btnMotorStatus305;
        private System.Windows.Forms.Label lblMotorStatus304;
        private System.Windows.Forms.Button btnMotorStatus304;
        private System.Windows.Forms.Label lblMotorStatus303;
        private System.Windows.Forms.Button btnMotorStatus303;
        private System.Windows.Forms.Label lblMotorStatus302;
        private System.Windows.Forms.Button btnMotorStatus302;
        private System.Windows.Forms.Label lblMotorStatus301;
        private System.Windows.Forms.Button btnMotorStatus301;
        private System.Windows.Forms.Label lblMotorStatus213;
        private System.Windows.Forms.Label lblMotorStatus211;
        private System.Windows.Forms.Label lblMotorStatus209;
        private System.Windows.Forms.Label lblMotorStatus214;
        private System.Windows.Forms.Label lblMotorStatus212;
        private System.Windows.Forms.Label lblMotorStatus210;
        private System.Windows.Forms.Label lblMotorStatus208;
        private System.Windows.Forms.Label lblMotorStatus207;
        private System.Windows.Forms.Label lblMotorStatus206;
        private System.Windows.Forms.Label lblMotorStatus205;
        private System.Windows.Forms.Label lblMotorStatus204;
        private System.Windows.Forms.Label lblMotorStatus203;
        private System.Windows.Forms.Label lblMotorStatus224;
        private System.Windows.Forms.Button btnMotorStatus224;
        private System.Windows.Forms.Button btnMotorStatus201;
        private System.Windows.Forms.Button btnMotorStatus223;
        private System.Windows.Forms.Label lblMotorStatus201;
        private System.Windows.Forms.Button btnMotorStatus222;
        private System.Windows.Forms.Label lblMotorStatus202;
        private System.Windows.Forms.Button btnMotorStatus221;
        private System.Windows.Forms.Button btnMotorStatus202;
        private System.Windows.Forms.Button btnMotorStatus220;
        private System.Windows.Forms.Button btnMotorStatus219;
        private System.Windows.Forms.Button btnMotorStatus218;
        private System.Windows.Forms.Button btnMotorStatus217;
        private System.Windows.Forms.Button btnMotorStatus216;
        private System.Windows.Forms.Button btnMotorStatus215;
        private System.Windows.Forms.Button btnMotorStatus214;
        private System.Windows.Forms.Button btnMotorStatus213;
        private System.Windows.Forms.Button btnMotorStatus212;
        private System.Windows.Forms.Button btnMotorStatus211;
        private System.Windows.Forms.Button btnMotorStatus210;
        private System.Windows.Forms.Button btnMotorStatus209;
        private System.Windows.Forms.Button btnMotorStatus208;
        private System.Windows.Forms.Label lblMotorStatus215;
        private System.Windows.Forms.Button btnMotorStatus207;
        private System.Windows.Forms.Label lblMotorStatus216;
        private System.Windows.Forms.Button btnMotorStatus206;
        private System.Windows.Forms.Label lblMotorStatus217;
        private System.Windows.Forms.Button btnMotorStatus205;
        private System.Windows.Forms.Label lblMotorStatus218;
        private System.Windows.Forms.Button btnMotorStatus204;
        private System.Windows.Forms.Label lblMotorStatus219;
        private System.Windows.Forms.Button btnMotorStatus203;
        private System.Windows.Forms.Label lblMotorStatus220;
        private System.Windows.Forms.Label lblMotorStatus223;
        private System.Windows.Forms.Label lblMotorStatus221;
        private System.Windows.Forms.Label lblMotorStatus222;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label label2;
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerReset
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAllReset = new System.Windows.Forms.Label();
            this.lblUnloaderReset = new System.Windows.Forms.Label();
            this.lblProcessReset = new System.Windows.Forms.Label();
            this.lblLoaderReset = new System.Windows.Forms.Label();
            this.pbarLoaderPercent = new System.Windows.Forms.ProgressBar();
            this.pbarProcessPercent = new System.Windows.Forms.ProgressBar();
            this.pbarUnloaderPercent = new System.Windows.Forms.ProgressBar();
            this.pbarAllPercent = new System.Windows.Forms.ProgressBar();
            this.btnSSCNetReset = new System.Windows.Forms.Button();
            this.btnDataInit = new System.Windows.Forms.Button();
            this.lblLoaderPercent = new System.Windows.Forms.Label();
            this.lblProcessPercent = new System.Windows.Forms.Label();
            this.lblUnloaderPercent = new System.Windows.Forms.Label();
            this.lblAllPercent = new System.Windows.Forms.Label();
            this.lblLoader = new System.Windows.Forms.Label();
            this.lbl31CellX1 = new System.Windows.Forms.Label();
            this.lbl32CellX2 = new System.Windows.Forms.Label();
            this.lbl35CellTransX1 = new System.Windows.Forms.Label();
            this.lbl33CellTransferY1 = new System.Windows.Forms.Label();
            this.lbl36CellTransferX2 = new System.Windows.Forms.Label();
            this.lbl04CstRotationUpLeft = new System.Windows.Forms.Label();
            this.lbl05CstRotationDownLeft = new System.Windows.Forms.Label();
            this.lbl06CstRotationUpRight = new System.Windows.Forms.Label();
            this.lbl07CstRotationDownRight = new System.Windows.Forms.Label();
            this.lbl08CstElectrictyZLeft = new System.Windows.Forms.Label();
            this.lbl10CellY1 = new System.Windows.Forms.Label();
            this.lbl09CstElectrictyZRight = new System.Windows.Forms.Label();
            this.lbl11CellTransferY3 = new System.Windows.Forms.Label();
            this.lbl12CellTransferT1 = new System.Windows.Forms.Label();
            this.lbl13CellY2 = new System.Windows.Forms.Label();
            this.lbl14CellTransferY4 = new System.Windows.Forms.Label();
            this.lbl15CellTransferT2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblProcess = new System.Windows.Forms.Label();
            this.lbl18BreakZ1 = new System.Windows.Forms.Label();
            this.lbl21BreakZ4 = new System.Windows.Forms.Label();
            this.lbl11BreakX1 = new System.Windows.Forms.Label();
            this.lbl20BreakZ3 = new System.Windows.Forms.Label();
            this.lbl19BreakZ2 = new System.Windows.Forms.Label();
            this.lbl14BreakX4 = new System.Windows.Forms.Label();
            this.lbl07CellTransferY1 = new System.Windows.Forms.Label();
            this.lbl13BreakX3 = new System.Windows.Forms.Label();
            this.lbl12BreakX2 = new System.Windows.Forms.Label();
            this.lbl10BreakTableY1 = new System.Windows.Forms.Label();
            this.lbl17HeadZ1 = new System.Windows.Forms.Label();
            this.lbl09BreakTableY1 = new System.Windows.Forms.Label();
            this.lbl22MasureX1 = new System.Windows.Forms.Label();
            this.lbl06CellTransferX2 = new System.Windows.Forms.Label();
            this.lbl03HeadX1 = new System.Windows.Forms.Label();
            this.lbl05CellTransferX1 = new System.Windows.Forms.Label();
            this.lbl04AlignX1 = new System.Windows.Forms.Label();
            this.lbl02TableY2 = new System.Windows.Forms.Label();
            this.lbl01TableY1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblUnloader = new System.Windows.Forms.Label();
            this.lbl4BreakTableT4 = new System.Windows.Forms.Label();
            this.lbl1BreakTableT2 = new System.Windows.Forms.Label();
            this.lbl2BreakTableT3 = new System.Windows.Forms.Label();
            this.lbl28CellY2 = new System.Windows.Forms.Label();
            this.lbl0BreakTableT1 = new System.Windows.Forms.Label();
            this.lbl23CellTransferT3 = new System.Windows.Forms.Label();
            this.lbl30CstRotationDownRight = new System.Windows.Forms.Label();
            this.lbl29CstRotationUpRight = new System.Windows.Forms.Label();
            this.lbl27CellY1 = new System.Windows.Forms.Label();
            this.lbl18CstElectrictyZLeft = new System.Windows.Forms.Label();
            this.lbl26InspZ1 = new System.Windows.Forms.Label();
            this.lbl24CellTransferT4 = new System.Windows.Forms.Label();
            this.lbl21CellTransferT2 = new System.Windows.Forms.Label();
            this.lbl40CellX1 = new System.Windows.Forms.Label();
            this.lbl20CellTransferT1 = new System.Windows.Forms.Label();
            this.lbl19CstElectrictyZRight = new System.Windows.Forms.Label();
            this.lbl17CstRotationDownLeft = new System.Windows.Forms.Label();
            this.lbl39InspX1 = new System.Windows.Forms.Label();
            this.lbl16CstRotationUpLeft = new System.Windows.Forms.Label();
            this.lbl41CellX2 = new System.Windows.Forms.Label();
            this.lbl38CellTransferY2 = new System.Windows.Forms.Label();
            this.lbl37CellTransferY1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblAllReset);
            this.panel1.Controls.Add(this.lblUnloaderReset);
            this.panel1.Controls.Add(this.lblProcessReset);
            this.panel1.Controls.Add(this.lblLoaderReset);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1734, 300);
            this.panel1.TabIndex = 0;
            // 
            // lblAllReset
            // 
            this.lblAllReset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAllReset.Font = new System.Drawing.Font("맑은 고딕", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAllReset.ForeColor = System.Drawing.Color.Black;
            this.lblAllReset.Location = new System.Drawing.Point(3, 152);
            this.lblAllReset.Name = "lblAllReset";
            this.lblAllReset.Size = new System.Drawing.Size(1726, 143);
            this.lblAllReset.TabIndex = 66;
            this.lblAllReset.Text = " 전체  초기화";
            this.lblAllReset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloaderReset
            // 
            this.lblUnloaderReset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloaderReset.Font = new System.Drawing.Font("맑은 고딕", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloaderReset.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderReset.Location = new System.Drawing.Point(1157, 3);
            this.lblUnloaderReset.Name = "lblUnloaderReset";
            this.lblUnloaderReset.Size = new System.Drawing.Size(570, 143);
            this.lblUnloaderReset.TabIndex = 65;
            this.lblUnloaderReset.Text = "언로더부초기화";
            this.lblUnloaderReset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessReset
            // 
            this.lblProcessReset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessReset.Font = new System.Drawing.Font("맑은 고딕", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProcessReset.ForeColor = System.Drawing.Color.Black;
            this.lblProcessReset.Location = new System.Drawing.Point(581, 3);
            this.lblProcessReset.Name = "lblProcessReset";
            this.lblProcessReset.Size = new System.Drawing.Size(570, 143);
            this.lblProcessReset.TabIndex = 64;
            this.lblProcessReset.Text = "프로세스부 초기화";
            this.lblProcessReset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoaderReset
            // 
            this.lblLoaderReset.BackColor = System.Drawing.SystemColors.Control;
            this.lblLoaderReset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoaderReset.Font = new System.Drawing.Font("맑은 고딕", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoaderReset.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderReset.Location = new System.Drawing.Point(3, 3);
            this.lblLoaderReset.Name = "lblLoaderReset";
            this.lblLoaderReset.Size = new System.Drawing.Size(570, 143);
            this.lblLoaderReset.TabIndex = 63;
            this.lblLoaderReset.Text = "로더부 초기화";
            this.lblLoaderReset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbarLoaderPercent
            // 
            this.pbarLoaderPercent.Location = new System.Drawing.Point(201, 309);
            this.pbarLoaderPercent.Name = "pbarLoaderPercent";
            this.pbarLoaderPercent.Size = new System.Drawing.Size(376, 44);
            this.pbarLoaderPercent.TabIndex = 5;
            // 
            // pbarProcessPercent
            // 
            this.pbarProcessPercent.Location = new System.Drawing.Point(779, 309);
            this.pbarProcessPercent.Name = "pbarProcessPercent";
            this.pbarProcessPercent.Size = new System.Drawing.Size(376, 44);
            this.pbarProcessPercent.TabIndex = 7;
            // 
            // pbarUnloaderPercent
            // 
            this.pbarUnloaderPercent.Location = new System.Drawing.Point(1357, 309);
            this.pbarUnloaderPercent.Name = "pbarUnloaderPercent";
            this.pbarUnloaderPercent.Size = new System.Drawing.Size(376, 44);
            this.pbarUnloaderPercent.TabIndex = 9;
            // 
            // pbarAllPercent
            // 
            this.pbarAllPercent.Location = new System.Drawing.Point(201, 359);
            this.pbarAllPercent.Name = "pbarAllPercent";
            this.pbarAllPercent.Size = new System.Drawing.Size(954, 44);
            this.pbarAllPercent.TabIndex = 10;
            // 
            // btnSSCNetReset
            // 
            this.btnSSCNetReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnSSCNetReset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSSCNetReset.ForeColor = System.Drawing.Color.Black;
            this.btnSSCNetReset.Location = new System.Drawing.Point(1161, 359);
            this.btnSSCNetReset.Name = "btnSSCNetReset";
            this.btnSSCNetReset.Size = new System.Drawing.Size(424, 44);
            this.btnSSCNetReset.TabIndex = 93;
            this.btnSSCNetReset.Text = "미쯔비시 SSC넷 재설정";
            this.btnSSCNetReset.UseVisualStyleBackColor = false;
            // 
            // btnDataInit
            // 
            this.btnDataInit.BackColor = System.Drawing.SystemColors.Control;
            this.btnDataInit.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDataInit.ForeColor = System.Drawing.Color.Black;
            this.btnDataInit.Location = new System.Drawing.Point(1591, 359);
            this.btnDataInit.Name = "btnDataInit";
            this.btnDataInit.Size = new System.Drawing.Size(142, 44);
            this.btnDataInit.TabIndex = 94;
            this.btnDataInit.Text = "Data Init";
            this.btnDataInit.UseVisualStyleBackColor = false;
            // 
            // lblLoaderPercent
            // 
            this.lblLoaderPercent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoaderPercent.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLoaderPercent.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderPercent.Location = new System.Drawing.Point(7, 309);
            this.lblLoaderPercent.Name = "lblLoaderPercent";
            this.lblLoaderPercent.Size = new System.Drawing.Size(188, 44);
            this.lblLoaderPercent.TabIndex = 95;
            this.lblLoaderPercent.Text = "로더부 진행률 0%";
            this.lblLoaderPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessPercent
            // 
            this.lblProcessPercent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessPercent.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProcessPercent.ForeColor = System.Drawing.Color.Black;
            this.lblProcessPercent.Location = new System.Drawing.Point(585, 309);
            this.lblProcessPercent.Name = "lblProcessPercent";
            this.lblProcessPercent.Size = new System.Drawing.Size(188, 44);
            this.lblProcessPercent.TabIndex = 96;
            this.lblProcessPercent.Text = "프로세스부 진행률 0%";
            this.lblProcessPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnloaderPercent
            // 
            this.lblUnloaderPercent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloaderPercent.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblUnloaderPercent.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderPercent.Location = new System.Drawing.Point(1163, 309);
            this.lblUnloaderPercent.Name = "lblUnloaderPercent";
            this.lblUnloaderPercent.Size = new System.Drawing.Size(188, 44);
            this.lblUnloaderPercent.TabIndex = 97;
            this.lblUnloaderPercent.Text = "언로더부 진행률 0%";
            this.lblUnloaderPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAllPercent
            // 
            this.lblAllPercent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAllPercent.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAllPercent.ForeColor = System.Drawing.Color.Black;
            this.lblAllPercent.Location = new System.Drawing.Point(7, 359);
            this.lblAllPercent.Name = "lblAllPercent";
            this.lblAllPercent.Size = new System.Drawing.Size(188, 44);
            this.lblAllPercent.TabIndex = 98;
            this.lblAllPercent.Text = "전체 진행률 0%";
            this.lblAllPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoader
            // 
            this.lblLoader.AutoEllipsis = true;
            this.lblLoader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoader.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoader.ForeColor = System.Drawing.Color.Black;
            this.lblLoader.Location = new System.Drawing.Point(0, 0);
            this.lblLoader.Name = "lblLoader";
            this.lblLoader.Size = new System.Drawing.Size(568, 24);
            this.lblLoader.TabIndex = 9;
            this.lblLoader.Text = "■ 로더";
            this.lblLoader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl31CellX1
            // 
            this.lbl31CellX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl31CellX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl31CellX1.ForeColor = System.Drawing.Color.Black;
            this.lbl31CellX1.Location = new System.Drawing.Point(6, 33);
            this.lbl31CellX1.Name = "lbl31CellX1";
            this.lbl31CellX1.Size = new System.Drawing.Size(275, 35);
            this.lbl31CellX1.TabIndex = 0;
            this.lbl31CellX1.Text = "31.CELL_X_1";
            this.lbl31CellX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl32CellX2
            // 
            this.lbl32CellX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl32CellX2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl32CellX2.ForeColor = System.Drawing.Color.Black;
            this.lbl32CellX2.Location = new System.Drawing.Point(289, 33);
            this.lbl32CellX2.Name = "lbl32CellX2";
            this.lbl32CellX2.Size = new System.Drawing.Size(275, 35);
            this.lbl32CellX2.TabIndex = 1;
            this.lbl32CellX2.Text = "32.CELL_X_2";
            this.lbl32CellX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl35CellTransX1
            // 
            this.lbl35CellTransX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl35CellTransX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl35CellTransX1.ForeColor = System.Drawing.Color.Black;
            this.lbl35CellTransX1.Location = new System.Drawing.Point(289, 74);
            this.lbl35CellTransX1.Name = "lbl35CellTransX1";
            this.lbl35CellTransX1.Size = new System.Drawing.Size(275, 35);
            this.lbl35CellTransX1.TabIndex = 2;
            this.lbl35CellTransX1.Text = "35.CELL_TRANSFER_X_1";
            this.lbl35CellTransX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl33CellTransferY1
            // 
            this.lbl33CellTransferY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl33CellTransferY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl33CellTransferY1.ForeColor = System.Drawing.Color.Black;
            this.lbl33CellTransferY1.Location = new System.Drawing.Point(6, 74);
            this.lbl33CellTransferY1.Name = "lbl33CellTransferY1";
            this.lbl33CellTransferY1.Size = new System.Drawing.Size(275, 35);
            this.lbl33CellTransferY1.TabIndex = 1;
            this.lbl33CellTransferY1.Text = "33.CELL_TRANSFER_Y_1";
            this.lbl33CellTransferY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl36CellTransferX2
            // 
            this.lbl36CellTransferX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl36CellTransferX2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl36CellTransferX2.ForeColor = System.Drawing.Color.Black;
            this.lbl36CellTransferX2.Location = new System.Drawing.Point(6, 115);
            this.lbl36CellTransferX2.Name = "lbl36CellTransferX2";
            this.lbl36CellTransferX2.Size = new System.Drawing.Size(275, 35);
            this.lbl36CellTransferX2.TabIndex = 3;
            this.lbl36CellTransferX2.Text = "36.CELL_TRANSFER_X_2";
            this.lbl36CellTransferX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl04CstRotationUpLeft
            // 
            this.lbl04CstRotationUpLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl04CstRotationUpLeft.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl04CstRotationUpLeft.ForeColor = System.Drawing.Color.Black;
            this.lbl04CstRotationUpLeft.Location = new System.Drawing.Point(289, 115);
            this.lbl04CstRotationUpLeft.Name = "lbl04CstRotationUpLeft";
            this.lbl04CstRotationUpLeft.Size = new System.Drawing.Size(275, 35);
            this.lbl04CstRotationUpLeft.TabIndex = 4;
            this.lbl04CstRotationUpLeft.Text = "04.CASSETTE_ROTATION_UP_LEFT";
            this.lbl04CstRotationUpLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl05CstRotationDownLeft
            // 
            this.lbl05CstRotationDownLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl05CstRotationDownLeft.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl05CstRotationDownLeft.ForeColor = System.Drawing.Color.Black;
            this.lbl05CstRotationDownLeft.Location = new System.Drawing.Point(6, 156);
            this.lbl05CstRotationDownLeft.Name = "lbl05CstRotationDownLeft";
            this.lbl05CstRotationDownLeft.Size = new System.Drawing.Size(275, 35);
            this.lbl05CstRotationDownLeft.TabIndex = 5;
            this.lbl05CstRotationDownLeft.Text = "05.CASSETTE_ROTATION_DOWN_LEFT";
            this.lbl05CstRotationDownLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl06CstRotationUpRight
            // 
            this.lbl06CstRotationUpRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl06CstRotationUpRight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl06CstRotationUpRight.ForeColor = System.Drawing.Color.Black;
            this.lbl06CstRotationUpRight.Location = new System.Drawing.Point(289, 156);
            this.lbl06CstRotationUpRight.Name = "lbl06CstRotationUpRight";
            this.lbl06CstRotationUpRight.Size = new System.Drawing.Size(275, 35);
            this.lbl06CstRotationUpRight.TabIndex = 6;
            this.lbl06CstRotationUpRight.Text = "06.CASSETTE_ROTATION_UP_RIGHT";
            this.lbl06CstRotationUpRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl07CstRotationDownRight
            // 
            this.lbl07CstRotationDownRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl07CstRotationDownRight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl07CstRotationDownRight.ForeColor = System.Drawing.Color.Black;
            this.lbl07CstRotationDownRight.Location = new System.Drawing.Point(6, 197);
            this.lbl07CstRotationDownRight.Name = "lbl07CstRotationDownRight";
            this.lbl07CstRotationDownRight.Size = new System.Drawing.Size(275, 35);
            this.lbl07CstRotationDownRight.TabIndex = 6;
            this.lbl07CstRotationDownRight.Text = "07.CASSETTE_ROTATION_DOWN_RIGHT";
            this.lbl07CstRotationDownRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl08CstElectrictyZLeft
            // 
            this.lbl08CstElectrictyZLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl08CstElectrictyZLeft.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl08CstElectrictyZLeft.ForeColor = System.Drawing.Color.Black;
            this.lbl08CstElectrictyZLeft.Location = new System.Drawing.Point(289, 197);
            this.lbl08CstElectrictyZLeft.Name = "lbl08CstElectrictyZLeft";
            this.lbl08CstElectrictyZLeft.Size = new System.Drawing.Size(275, 35);
            this.lbl08CstElectrictyZLeft.TabIndex = 7;
            this.lbl08CstElectrictyZLeft.Text = "08.CASSETTE_ELECTRICTY_Z_LEFT";
            this.lbl08CstElectrictyZLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl10CellY1
            // 
            this.lbl10CellY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl10CellY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl10CellY1.ForeColor = System.Drawing.Color.Black;
            this.lbl10CellY1.Location = new System.Drawing.Point(289, 238);
            this.lbl10CellY1.Name = "lbl10CellY1";
            this.lbl10CellY1.Size = new System.Drawing.Size(275, 35);
            this.lbl10CellY1.TabIndex = 1;
            this.lbl10CellY1.Text = "10.CELL_Y_1";
            this.lbl10CellY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl09CstElectrictyZRight
            // 
            this.lbl09CstElectrictyZRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl09CstElectrictyZRight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl09CstElectrictyZRight.ForeColor = System.Drawing.Color.Black;
            this.lbl09CstElectrictyZRight.Location = new System.Drawing.Point(6, 238);
            this.lbl09CstElectrictyZRight.Name = "lbl09CstElectrictyZRight";
            this.lbl09CstElectrictyZRight.Size = new System.Drawing.Size(275, 35);
            this.lbl09CstElectrictyZRight.TabIndex = 8;
            this.lbl09CstElectrictyZRight.Text = "09.CASSETTE_ELECTRICTY_Z_RIGHT";
            this.lbl09CstElectrictyZRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl11CellTransferY3
            // 
            this.lbl11CellTransferY3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl11CellTransferY3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl11CellTransferY3.ForeColor = System.Drawing.Color.Black;
            this.lbl11CellTransferY3.Location = new System.Drawing.Point(6, 279);
            this.lbl11CellTransferY3.Name = "lbl11CellTransferY3";
            this.lbl11CellTransferY3.Size = new System.Drawing.Size(275, 35);
            this.lbl11CellTransferY3.TabIndex = 1;
            this.lbl11CellTransferY3.Text = "11.CELL_TRANSFER_Y_3";
            this.lbl11CellTransferY3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl12CellTransferT1
            // 
            this.lbl12CellTransferT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl12CellTransferT1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl12CellTransferT1.ForeColor = System.Drawing.Color.Black;
            this.lbl12CellTransferT1.Location = new System.Drawing.Point(289, 279);
            this.lbl12CellTransferT1.Name = "lbl12CellTransferT1";
            this.lbl12CellTransferT1.Size = new System.Drawing.Size(275, 35);
            this.lbl12CellTransferT1.TabIndex = 2;
            this.lbl12CellTransferT1.Text = "12.CELL_TRANSFER_T_1";
            this.lbl12CellTransferT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl13CellY2
            // 
            this.lbl13CellY2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl13CellY2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl13CellY2.ForeColor = System.Drawing.Color.Black;
            this.lbl13CellY2.Location = new System.Drawing.Point(6, 320);
            this.lbl13CellY2.Name = "lbl13CellY2";
            this.lbl13CellY2.Size = new System.Drawing.Size(275, 35);
            this.lbl13CellY2.TabIndex = 2;
            this.lbl13CellY2.Text = "13.CELL_Y_2";
            this.lbl13CellY2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl14CellTransferY4
            // 
            this.lbl14CellTransferY4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl14CellTransferY4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl14CellTransferY4.ForeColor = System.Drawing.Color.Black;
            this.lbl14CellTransferY4.Location = new System.Drawing.Point(289, 320);
            this.lbl14CellTransferY4.Name = "lbl14CellTransferY4";
            this.lbl14CellTransferY4.Size = new System.Drawing.Size(275, 35);
            this.lbl14CellTransferY4.TabIndex = 2;
            this.lbl14CellTransferY4.Text = "14.CELL_TRANSFER_Y_4";
            this.lbl14CellTransferY4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl15CellTransferT2
            // 
            this.lbl15CellTransferT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl15CellTransferT2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl15CellTransferT2.ForeColor = System.Drawing.Color.Black;
            this.lbl15CellTransferT2.Location = new System.Drawing.Point(6, 361);
            this.lbl15CellTransferT2.Name = "lbl15CellTransferT2";
            this.lbl15CellTransferT2.Size = new System.Drawing.Size(275, 35);
            this.lbl15CellTransferT2.TabIndex = 2;
            this.lbl15CellTransferT2.Text = "15.CELL_TRANSFER_T_2";
            this.lbl15CellTransferT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbl15CellTransferT2);
            this.panel2.Controls.Add(this.lbl14CellTransferY4);
            this.panel2.Controls.Add(this.lbl13CellY2);
            this.panel2.Controls.Add(this.lbl12CellTransferT1);
            this.panel2.Controls.Add(this.lbl11CellTransferY3);
            this.panel2.Controls.Add(this.lbl09CstElectrictyZRight);
            this.panel2.Controls.Add(this.lbl10CellY1);
            this.panel2.Controls.Add(this.lbl08CstElectrictyZLeft);
            this.panel2.Controls.Add(this.lbl07CstRotationDownRight);
            this.panel2.Controls.Add(this.lbl06CstRotationUpRight);
            this.panel2.Controls.Add(this.lbl05CstRotationDownLeft);
            this.panel2.Controls.Add(this.lbl04CstRotationUpLeft);
            this.panel2.Controls.Add(this.lbl36CellTransferX2);
            this.panel2.Controls.Add(this.lbl33CellTransferY1);
            this.panel2.Controls.Add(this.lbl35CellTransX1);
            this.panel2.Controls.Add(this.lbl32CellX2);
            this.panel2.Controls.Add(this.lbl31CellX1);
            this.panel2.Controls.Add(this.lblLoader);
            this.panel2.Location = new System.Drawing.Point(7, 409);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(570, 448);
            this.panel2.TabIndex = 457;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lbl21BreakZ4);
            this.panel3.Controls.Add(this.lbl20BreakZ3);
            this.panel3.Controls.Add(this.lbl19BreakZ2);
            this.panel3.Controls.Add(this.lbl18BreakZ1);
            this.panel3.Controls.Add(this.lbl14BreakX4);
            this.panel3.Controls.Add(this.lbl13BreakX3);
            this.panel3.Controls.Add(this.lbl12BreakX2);
            this.panel3.Controls.Add(this.lbl11BreakX1);
            this.panel3.Controls.Add(this.lbl10BreakTableY1);
            this.panel3.Controls.Add(this.lbl09BreakTableY1);
            this.panel3.Controls.Add(this.lbl22MasureX1);
            this.panel3.Controls.Add(this.lbl07CellTransferY1);
            this.panel3.Controls.Add(this.lbl06CellTransferX2);
            this.panel3.Controls.Add(this.lbl05CellTransferX1);
            this.panel3.Controls.Add(this.lbl04AlignX1);
            this.panel3.Controls.Add(this.lbl17HeadZ1);
            this.panel3.Controls.Add(this.lbl03HeadX1);
            this.panel3.Controls.Add(this.lbl02TableY2);
            this.panel3.Controls.Add(this.lbl01TableY1);
            this.panel3.Controls.Add(this.lblProcess);
            this.panel3.Location = new System.Drawing.Point(582, 409);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(570, 448);
            this.panel3.TabIndex = 458;
            // 
            // lblProcess
            // 
            this.lblProcess.AutoEllipsis = true;
            this.lblProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcess.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcess.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcess.ForeColor = System.Drawing.Color.Black;
            this.lblProcess.Location = new System.Drawing.Point(0, 0);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(568, 24);
            this.lblProcess.TabIndex = 9;
            this.lblProcess.Text = "■ 프로세스";
            this.lblProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl18BreakZ1
            // 
            this.lbl18BreakZ1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl18BreakZ1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl18BreakZ1.ForeColor = System.Drawing.Color.Black;
            this.lbl18BreakZ1.Location = new System.Drawing.Point(289, 320);
            this.lbl18BreakZ1.Name = "lbl18BreakZ1";
            this.lbl18BreakZ1.Size = new System.Drawing.Size(275, 35);
            this.lbl18BreakZ1.TabIndex = 4;
            this.lbl18BreakZ1.Text = "18.BREAK_Z_1";
            this.lbl18BreakZ1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl21BreakZ4
            // 
            this.lbl21BreakZ4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl21BreakZ4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl21BreakZ4.ForeColor = System.Drawing.Color.Black;
            this.lbl21BreakZ4.Location = new System.Drawing.Point(6, 402);
            this.lbl21BreakZ4.Name = "lbl21BreakZ4";
            this.lbl21BreakZ4.Size = new System.Drawing.Size(275, 35);
            this.lbl21BreakZ4.TabIndex = 5;
            this.lbl21BreakZ4.Text = "21.BREAK_Z_4";
            this.lbl21BreakZ4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl11BreakX1
            // 
            this.lbl11BreakX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl11BreakX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl11BreakX1.ForeColor = System.Drawing.Color.Black;
            this.lbl11BreakX1.Location = new System.Drawing.Point(289, 238);
            this.lbl11BreakX1.Name = "lbl11BreakX1";
            this.lbl11BreakX1.Size = new System.Drawing.Size(275, 35);
            this.lbl11BreakX1.TabIndex = 2;
            this.lbl11BreakX1.Text = "11.BREAK_X_1";
            this.lbl11BreakX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl20BreakZ3
            // 
            this.lbl20BreakZ3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl20BreakZ3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl20BreakZ3.ForeColor = System.Drawing.Color.Black;
            this.lbl20BreakZ3.Location = new System.Drawing.Point(289, 361);
            this.lbl20BreakZ3.Name = "lbl20BreakZ3";
            this.lbl20BreakZ3.Size = new System.Drawing.Size(275, 35);
            this.lbl20BreakZ3.TabIndex = 5;
            this.lbl20BreakZ3.Text = "20.BREAK_Z_3";
            this.lbl20BreakZ3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl19BreakZ2
            // 
            this.lbl19BreakZ2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl19BreakZ2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl19BreakZ2.ForeColor = System.Drawing.Color.Black;
            this.lbl19BreakZ2.Location = new System.Drawing.Point(6, 361);
            this.lbl19BreakZ2.Name = "lbl19BreakZ2";
            this.lbl19BreakZ2.Size = new System.Drawing.Size(275, 35);
            this.lbl19BreakZ2.TabIndex = 5;
            this.lbl19BreakZ2.Text = "19.BREAK_Z_2";
            this.lbl19BreakZ2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl14BreakX4
            // 
            this.lbl14BreakX4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl14BreakX4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl14BreakX4.ForeColor = System.Drawing.Color.Black;
            this.lbl14BreakX4.Location = new System.Drawing.Point(6, 320);
            this.lbl14BreakX4.Name = "lbl14BreakX4";
            this.lbl14BreakX4.Size = new System.Drawing.Size(275, 35);
            this.lbl14BreakX4.TabIndex = 4;
            this.lbl14BreakX4.Text = "14.BREAK_X_4";
            this.lbl14BreakX4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl07CellTransferY1
            // 
            this.lbl07CellTransferY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl07CellTransferY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl07CellTransferY1.ForeColor = System.Drawing.Color.Black;
            this.lbl07CellTransferY1.Location = new System.Drawing.Point(289, 156);
            this.lbl07CellTransferY1.Name = "lbl07CellTransferY1";
            this.lbl07CellTransferY1.Size = new System.Drawing.Size(275, 35);
            this.lbl07CellTransferY1.TabIndex = 5;
            this.lbl07CellTransferY1.Text = "07.CELL_TRANSFER_Y_1";
            this.lbl07CellTransferY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl13BreakX3
            // 
            this.lbl13BreakX3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl13BreakX3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl13BreakX3.ForeColor = System.Drawing.Color.Black;
            this.lbl13BreakX3.Location = new System.Drawing.Point(289, 279);
            this.lbl13BreakX3.Name = "lbl13BreakX3";
            this.lbl13BreakX3.Size = new System.Drawing.Size(275, 35);
            this.lbl13BreakX3.TabIndex = 3;
            this.lbl13BreakX3.Text = "13.BREAK_X_3";
            this.lbl13BreakX3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl12BreakX2
            // 
            this.lbl12BreakX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl12BreakX2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl12BreakX2.ForeColor = System.Drawing.Color.Black;
            this.lbl12BreakX2.Location = new System.Drawing.Point(6, 279);
            this.lbl12BreakX2.Name = "lbl12BreakX2";
            this.lbl12BreakX2.Size = new System.Drawing.Size(275, 35);
            this.lbl12BreakX2.TabIndex = 3;
            this.lbl12BreakX2.Text = "12.BREAK_X_2";
            this.lbl12BreakX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl10BreakTableY1
            // 
            this.lbl10BreakTableY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl10BreakTableY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl10BreakTableY1.ForeColor = System.Drawing.Color.Black;
            this.lbl10BreakTableY1.Location = new System.Drawing.Point(6, 238);
            this.lbl10BreakTableY1.Name = "lbl10BreakTableY1";
            this.lbl10BreakTableY1.Size = new System.Drawing.Size(275, 35);
            this.lbl10BreakTableY1.TabIndex = 7;
            this.lbl10BreakTableY1.Text = "10.BREAK_TABLE_Y_1";
            this.lbl10BreakTableY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl17HeadZ1
            // 
            this.lbl17HeadZ1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl17HeadZ1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl17HeadZ1.ForeColor = System.Drawing.Color.Black;
            this.lbl17HeadZ1.Location = new System.Drawing.Point(289, 74);
            this.lbl17HeadZ1.Name = "lbl17HeadZ1";
            this.lbl17HeadZ1.Size = new System.Drawing.Size(275, 35);
            this.lbl17HeadZ1.TabIndex = 2;
            this.lbl17HeadZ1.Text = "17.HEAD_Z_1";
            this.lbl17HeadZ1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl09BreakTableY1
            // 
            this.lbl09BreakTableY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl09BreakTableY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl09BreakTableY1.ForeColor = System.Drawing.Color.Black;
            this.lbl09BreakTableY1.Location = new System.Drawing.Point(289, 197);
            this.lbl09BreakTableY1.Name = "lbl09BreakTableY1";
            this.lbl09BreakTableY1.Size = new System.Drawing.Size(275, 35);
            this.lbl09BreakTableY1.TabIndex = 6;
            this.lbl09BreakTableY1.Text = "09.BREAK_TABLE_Y_1";
            this.lbl09BreakTableY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl22MasureX1
            // 
            this.lbl22MasureX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl22MasureX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl22MasureX1.ForeColor = System.Drawing.Color.Black;
            this.lbl22MasureX1.Location = new System.Drawing.Point(6, 197);
            this.lbl22MasureX1.Name = "lbl22MasureX1";
            this.lbl22MasureX1.Size = new System.Drawing.Size(275, 35);
            this.lbl22MasureX1.TabIndex = 3;
            this.lbl22MasureX1.Text = "22.MASURE_X_1";
            this.lbl22MasureX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl06CellTransferX2
            // 
            this.lbl06CellTransferX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl06CellTransferX2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl06CellTransferX2.ForeColor = System.Drawing.Color.Black;
            this.lbl06CellTransferX2.Location = new System.Drawing.Point(6, 156);
            this.lbl06CellTransferX2.Name = "lbl06CellTransferX2";
            this.lbl06CellTransferX2.Size = new System.Drawing.Size(275, 35);
            this.lbl06CellTransferX2.TabIndex = 4;
            this.lbl06CellTransferX2.Text = "06.CELL_TRANSFER_X_2";
            this.lbl06CellTransferX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl03HeadX1
            // 
            this.lbl03HeadX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl03HeadX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl03HeadX1.ForeColor = System.Drawing.Color.Black;
            this.lbl03HeadX1.Location = new System.Drawing.Point(6, 74);
            this.lbl03HeadX1.Name = "lbl03HeadX1";
            this.lbl03HeadX1.Size = new System.Drawing.Size(275, 35);
            this.lbl03HeadX1.TabIndex = 1;
            this.lbl03HeadX1.Text = "03.HEAD_X_1";
            this.lbl03HeadX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl05CellTransferX1
            // 
            this.lbl05CellTransferX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl05CellTransferX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl05CellTransferX1.ForeColor = System.Drawing.Color.Black;
            this.lbl05CellTransferX1.Location = new System.Drawing.Point(289, 115);
            this.lbl05CellTransferX1.Name = "lbl05CellTransferX1";
            this.lbl05CellTransferX1.Size = new System.Drawing.Size(275, 35);
            this.lbl05CellTransferX1.TabIndex = 3;
            this.lbl05CellTransferX1.Text = "05.CELL_TRANSFER_X_1";
            this.lbl05CellTransferX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl04AlignX1
            // 
            this.lbl04AlignX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl04AlignX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl04AlignX1.ForeColor = System.Drawing.Color.Black;
            this.lbl04AlignX1.Location = new System.Drawing.Point(6, 115);
            this.lbl04AlignX1.Name = "lbl04AlignX1";
            this.lbl04AlignX1.Size = new System.Drawing.Size(275, 35);
            this.lbl04AlignX1.TabIndex = 2;
            this.lbl04AlignX1.Text = "04.ALIGN_X_1";
            this.lbl04AlignX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl02TableY2
            // 
            this.lbl02TableY2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl02TableY2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl02TableY2.ForeColor = System.Drawing.Color.Black;
            this.lbl02TableY2.Location = new System.Drawing.Point(289, 33);
            this.lbl02TableY2.Name = "lbl02TableY2";
            this.lbl02TableY2.Size = new System.Drawing.Size(275, 35);
            this.lbl02TableY2.TabIndex = 1;
            this.lbl02TableY2.Text = "02.TABLE_Y_2";
            this.lbl02TableY2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl01TableY1
            // 
            this.lbl01TableY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl01TableY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl01TableY1.ForeColor = System.Drawing.Color.Black;
            this.lbl01TableY1.Location = new System.Drawing.Point(6, 33);
            this.lbl01TableY1.Name = "lbl01TableY1";
            this.lbl01TableY1.Size = new System.Drawing.Size(275, 35);
            this.lbl01TableY1.TabIndex = 0;
            this.lbl01TableY1.Text = "01.TABLE_Y_1";
            this.lbl01TableY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lbl4BreakTableT4);
            this.panel4.Controls.Add(this.lbl2BreakTableT3);
            this.panel4.Controls.Add(this.lbl1BreakTableT2);
            this.panel4.Controls.Add(this.lbl0BreakTableT1);
            this.panel4.Controls.Add(this.lbl30CstRotationDownRight);
            this.panel4.Controls.Add(this.lbl29CstRotationUpRight);
            this.panel4.Controls.Add(this.lbl28CellY2);
            this.panel4.Controls.Add(this.lbl27CellY1);
            this.panel4.Controls.Add(this.lbl26InspZ1);
            this.panel4.Controls.Add(this.lbl24CellTransferT4);
            this.panel4.Controls.Add(this.lbl23CellTransferT3);
            this.panel4.Controls.Add(this.lbl21CellTransferT2);
            this.panel4.Controls.Add(this.lbl20CellTransferT1);
            this.panel4.Controls.Add(this.lbl19CstElectrictyZRight);
            this.panel4.Controls.Add(this.lbl18CstElectrictyZLeft);
            this.panel4.Controls.Add(this.lbl17CstRotationDownLeft);
            this.panel4.Controls.Add(this.lbl16CstRotationUpLeft);
            this.panel4.Controls.Add(this.lbl41CellX2);
            this.panel4.Controls.Add(this.lbl40CellX1);
            this.panel4.Controls.Add(this.lbl39InspX1);
            this.panel4.Controls.Add(this.lbl38CellTransferY2);
            this.panel4.Controls.Add(this.lbl37CellTransferY1);
            this.panel4.Controls.Add(this.lblUnloader);
            this.panel4.Location = new System.Drawing.Point(1161, 409);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(570, 448);
            this.panel4.TabIndex = 459;
            // 
            // lblUnloader
            // 
            this.lblUnloader.AutoEllipsis = true;
            this.lblUnloader.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloader.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloader.ForeColor = System.Drawing.Color.Black;
            this.lblUnloader.Location = new System.Drawing.Point(0, 0);
            this.lblUnloader.Name = "lblUnloader";
            this.lblUnloader.Size = new System.Drawing.Size(568, 24);
            this.lblUnloader.TabIndex = 9;
            this.lblUnloader.Text = "■ 언로더";
            this.lblUnloader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl4BreakTableT4
            // 
            this.lbl4BreakTableT4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl4BreakTableT4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl4BreakTableT4.ForeColor = System.Drawing.Color.Black;
            this.lbl4BreakTableT4.Location = new System.Drawing.Point(289, 403);
            this.lbl4BreakTableT4.Name = "lbl4BreakTableT4";
            this.lbl4BreakTableT4.Size = new System.Drawing.Size(275, 30);
            this.lbl4BreakTableT4.TabIndex = 8;
            this.lbl4BreakTableT4.Text = "4.BREAK_TABLE_T_4";
            this.lbl4BreakTableT4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl1BreakTableT2
            // 
            this.lbl1BreakTableT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl1BreakTableT2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl1BreakTableT2.ForeColor = System.Drawing.Color.Black;
            this.lbl1BreakTableT2.Location = new System.Drawing.Point(289, 366);
            this.lbl1BreakTableT2.Name = "lbl1BreakTableT2";
            this.lbl1BreakTableT2.Size = new System.Drawing.Size(275, 35);
            this.lbl1BreakTableT2.TabIndex = 6;
            this.lbl1BreakTableT2.Text = "1.BREAK_TABLE_T_2";
            this.lbl1BreakTableT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl2BreakTableT3
            // 
            this.lbl2BreakTableT3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl2BreakTableT3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl2BreakTableT3.ForeColor = System.Drawing.Color.Black;
            this.lbl2BreakTableT3.Location = new System.Drawing.Point(6, 403);
            this.lbl2BreakTableT3.Name = "lbl2BreakTableT3";
            this.lbl2BreakTableT3.Size = new System.Drawing.Size(275, 30);
            this.lbl2BreakTableT3.TabIndex = 7;
            this.lbl2BreakTableT3.Text = "2.BREAK_TABLE_T_3";
            this.lbl2BreakTableT3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl28CellY2
            // 
            this.lbl28CellY2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl28CellY2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl28CellY2.ForeColor = System.Drawing.Color.Black;
            this.lbl28CellY2.Location = new System.Drawing.Point(289, 292);
            this.lbl28CellY2.Name = "lbl28CellY2";
            this.lbl28CellY2.Size = new System.Drawing.Size(275, 30);
            this.lbl28CellY2.TabIndex = 5;
            this.lbl28CellY2.Text = "28.CELL_Y_2";
            this.lbl28CellY2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl0BreakTableT1
            // 
            this.lbl0BreakTableT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl0BreakTableT1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl0BreakTableT1.ForeColor = System.Drawing.Color.Black;
            this.lbl0BreakTableT1.Location = new System.Drawing.Point(6, 366);
            this.lbl0BreakTableT1.Name = "lbl0BreakTableT1";
            this.lbl0BreakTableT1.Size = new System.Drawing.Size(275, 30);
            this.lbl0BreakTableT1.TabIndex = 5;
            this.lbl0BreakTableT1.Text = "0.BREAK_TABLE_T_1";
            this.lbl0BreakTableT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl23CellTransferT3
            // 
            this.lbl23CellTransferT3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl23CellTransferT3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl23CellTransferT3.ForeColor = System.Drawing.Color.Black;
            this.lbl23CellTransferT3.Location = new System.Drawing.Point(289, 218);
            this.lbl23CellTransferT3.Name = "lbl23CellTransferT3";
            this.lbl23CellTransferT3.Size = new System.Drawing.Size(275, 30);
            this.lbl23CellTransferT3.TabIndex = 4;
            this.lbl23CellTransferT3.Text = "23.CELL_TRANSFER_T_3";
            this.lbl23CellTransferT3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl30CstRotationDownRight
            // 
            this.lbl30CstRotationDownRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl30CstRotationDownRight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl30CstRotationDownRight.ForeColor = System.Drawing.Color.Black;
            this.lbl30CstRotationDownRight.Location = new System.Drawing.Point(289, 329);
            this.lbl30CstRotationDownRight.Name = "lbl30CstRotationDownRight";
            this.lbl30CstRotationDownRight.Size = new System.Drawing.Size(275, 30);
            this.lbl30CstRotationDownRight.TabIndex = 7;
            this.lbl30CstRotationDownRight.Text = "30.CASSETTE_ROTATION_DOWN_RIGHT";
            this.lbl30CstRotationDownRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl29CstRotationUpRight
            // 
            this.lbl29CstRotationUpRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl29CstRotationUpRight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl29CstRotationUpRight.ForeColor = System.Drawing.Color.Black;
            this.lbl29CstRotationUpRight.Location = new System.Drawing.Point(6, 329);
            this.lbl29CstRotationUpRight.Name = "lbl29CstRotationUpRight";
            this.lbl29CstRotationUpRight.Size = new System.Drawing.Size(275, 30);
            this.lbl29CstRotationUpRight.TabIndex = 7;
            this.lbl29CstRotationUpRight.Text = "29.CASSETTE_ROTATION_UP_RIGHT";
            this.lbl29CstRotationUpRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl27CellY1
            // 
            this.lbl27CellY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl27CellY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl27CellY1.ForeColor = System.Drawing.Color.Black;
            this.lbl27CellY1.Location = new System.Drawing.Point(6, 292);
            this.lbl27CellY1.Name = "lbl27CellY1";
            this.lbl27CellY1.Size = new System.Drawing.Size(275, 30);
            this.lbl27CellY1.TabIndex = 4;
            this.lbl27CellY1.Text = "27.CELL_Y_1";
            this.lbl27CellY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl18CstElectrictyZLeft
            // 
            this.lbl18CstElectrictyZLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl18CstElectrictyZLeft.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl18CstElectrictyZLeft.ForeColor = System.Drawing.Color.Black;
            this.lbl18CstElectrictyZLeft.Location = new System.Drawing.Point(289, 144);
            this.lbl18CstElectrictyZLeft.Name = "lbl18CstElectrictyZLeft";
            this.lbl18CstElectrictyZLeft.Size = new System.Drawing.Size(275, 30);
            this.lbl18CstElectrictyZLeft.TabIndex = 8;
            this.lbl18CstElectrictyZLeft.Text = "18.CASSETTE_ELECTRICTY_Z_LEFT";
            this.lbl18CstElectrictyZLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl26InspZ1
            // 
            this.lbl26InspZ1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl26InspZ1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl26InspZ1.ForeColor = System.Drawing.Color.Black;
            this.lbl26InspZ1.Location = new System.Drawing.Point(289, 255);
            this.lbl26InspZ1.Name = "lbl26InspZ1";
            this.lbl26InspZ1.Size = new System.Drawing.Size(275, 30);
            this.lbl26InspZ1.TabIndex = 3;
            this.lbl26InspZ1.Text = "26.INSP_Z_1";
            this.lbl26InspZ1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl24CellTransferT4
            // 
            this.lbl24CellTransferT4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl24CellTransferT4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl24CellTransferT4.ForeColor = System.Drawing.Color.Black;
            this.lbl24CellTransferT4.Location = new System.Drawing.Point(6, 255);
            this.lbl24CellTransferT4.Name = "lbl24CellTransferT4";
            this.lbl24CellTransferT4.Size = new System.Drawing.Size(275, 30);
            this.lbl24CellTransferT4.TabIndex = 5;
            this.lbl24CellTransferT4.Text = "24.CELL_TRANSFER_T_4";
            this.lbl24CellTransferT4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl21CellTransferT2
            // 
            this.lbl21CellTransferT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl21CellTransferT2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl21CellTransferT2.ForeColor = System.Drawing.Color.Black;
            this.lbl21CellTransferT2.Location = new System.Drawing.Point(6, 218);
            this.lbl21CellTransferT2.Name = "lbl21CellTransferT2";
            this.lbl21CellTransferT2.Size = new System.Drawing.Size(275, 30);
            this.lbl21CellTransferT2.TabIndex = 4;
            this.lbl21CellTransferT2.Text = "21.CELL_TRANSFER_T_2";
            this.lbl21CellTransferT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl40CellX1
            // 
            this.lbl40CellX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl40CellX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl40CellX1.ForeColor = System.Drawing.Color.Black;
            this.lbl40CellX1.Location = new System.Drawing.Point(289, 70);
            this.lbl40CellX1.Name = "lbl40CellX1";
            this.lbl40CellX1.Size = new System.Drawing.Size(275, 30);
            this.lbl40CellX1.TabIndex = 2;
            this.lbl40CellX1.Text = "40.CELL_X_1";
            this.lbl40CellX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl20CellTransferT1
            // 
            this.lbl20CellTransferT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl20CellTransferT1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl20CellTransferT1.ForeColor = System.Drawing.Color.Black;
            this.lbl20CellTransferT1.Location = new System.Drawing.Point(289, 181);
            this.lbl20CellTransferT1.Name = "lbl20CellTransferT1";
            this.lbl20CellTransferT1.Size = new System.Drawing.Size(275, 30);
            this.lbl20CellTransferT1.TabIndex = 3;
            this.lbl20CellTransferT1.Text = "20.CELL_TRANSFER_T_1";
            this.lbl20CellTransferT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl19CstElectrictyZRight
            // 
            this.lbl19CstElectrictyZRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl19CstElectrictyZRight.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl19CstElectrictyZRight.ForeColor = System.Drawing.Color.Black;
            this.lbl19CstElectrictyZRight.Location = new System.Drawing.Point(6, 181);
            this.lbl19CstElectrictyZRight.Name = "lbl19CstElectrictyZRight";
            this.lbl19CstElectrictyZRight.Size = new System.Drawing.Size(275, 30);
            this.lbl19CstElectrictyZRight.TabIndex = 9;
            this.lbl19CstElectrictyZRight.Text = "19.CASSETTE_ELECTRICTY_Z_RIGHT";
            this.lbl19CstElectrictyZRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl17CstRotationDownLeft
            // 
            this.lbl17CstRotationDownLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl17CstRotationDownLeft.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl17CstRotationDownLeft.ForeColor = System.Drawing.Color.Black;
            this.lbl17CstRotationDownLeft.Location = new System.Drawing.Point(6, 144);
            this.lbl17CstRotationDownLeft.Name = "lbl17CstRotationDownLeft";
            this.lbl17CstRotationDownLeft.Size = new System.Drawing.Size(275, 30);
            this.lbl17CstRotationDownLeft.TabIndex = 6;
            this.lbl17CstRotationDownLeft.Text = "17.CASSETTE_ROTATION_DOWN_LEFT";
            this.lbl17CstRotationDownLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl39InspX1
            // 
            this.lbl39InspX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl39InspX1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl39InspX1.ForeColor = System.Drawing.Color.Black;
            this.lbl39InspX1.Location = new System.Drawing.Point(6, 70);
            this.lbl39InspX1.Name = "lbl39InspX1";
            this.lbl39InspX1.Size = new System.Drawing.Size(275, 30);
            this.lbl39InspX1.TabIndex = 1;
            this.lbl39InspX1.Text = "39.INSP_X_1";
            this.lbl39InspX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl16CstRotationUpLeft
            // 
            this.lbl16CstRotationUpLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl16CstRotationUpLeft.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl16CstRotationUpLeft.ForeColor = System.Drawing.Color.Black;
            this.lbl16CstRotationUpLeft.Location = new System.Drawing.Point(289, 107);
            this.lbl16CstRotationUpLeft.Name = "lbl16CstRotationUpLeft";
            this.lbl16CstRotationUpLeft.Size = new System.Drawing.Size(275, 30);
            this.lbl16CstRotationUpLeft.TabIndex = 5;
            this.lbl16CstRotationUpLeft.Text = "16.CASSETTE_ROTATION_UP_LEFT";
            this.lbl16CstRotationUpLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl41CellX2
            // 
            this.lbl41CellX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl41CellX2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl41CellX2.ForeColor = System.Drawing.Color.Black;
            this.lbl41CellX2.Location = new System.Drawing.Point(6, 107);
            this.lbl41CellX2.Name = "lbl41CellX2";
            this.lbl41CellX2.Size = new System.Drawing.Size(275, 30);
            this.lbl41CellX2.TabIndex = 3;
            this.lbl41CellX2.Text = "41.CELL_X_2";
            this.lbl41CellX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl38CellTransferY2
            // 
            this.lbl38CellTransferY2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl38CellTransferY2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl38CellTransferY2.ForeColor = System.Drawing.Color.Black;
            this.lbl38CellTransferY2.Location = new System.Drawing.Point(289, 33);
            this.lbl38CellTransferY2.Name = "lbl38CellTransferY2";
            this.lbl38CellTransferY2.Size = new System.Drawing.Size(275, 30);
            this.lbl38CellTransferY2.TabIndex = 7;
            this.lbl38CellTransferY2.Text = "38.CELL_TRANSFER_Y_2";
            this.lbl38CellTransferY2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl37CellTransferY1
            // 
            this.lbl37CellTransferY1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl37CellTransferY1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl37CellTransferY1.ForeColor = System.Drawing.Color.Black;
            this.lbl37CellTransferY1.Location = new System.Drawing.Point(6, 33);
            this.lbl37CellTransferY1.Name = "lbl37CellTransferY1";
            this.lbl37CellTransferY1.Size = new System.Drawing.Size(275, 30);
            this.lbl37CellTransferY1.TabIndex = 6;
            this.lbl37CellTransferY1.Text = "37.CELL_TRANSFER_Y_1";
            this.lbl37CellTransferY1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlManagerReset
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblAllPercent);
            this.Controls.Add(this.lblUnloaderPercent);
            this.Controls.Add(this.lblProcessPercent);
            this.Controls.Add(this.lblLoaderPercent);
            this.Controls.Add(this.btnDataInit);
            this.Controls.Add(this.btnSSCNetReset);
            this.Controls.Add(this.pbarAllPercent);
            this.Controls.Add(this.pbarUnloaderPercent);
            this.Controls.Add(this.pbarProcessPercent);
            this.Controls.Add(this.pbarLoaderPercent);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerReset";
            this.Size = new System.Drawing.Size(1740, 860);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ProgressBar pbarLoaderPercent;
        private System.Windows.Forms.ProgressBar pbarProcessPercent;
        private System.Windows.Forms.ProgressBar pbarUnloaderPercent;
        private System.Windows.Forms.ProgressBar pbarAllPercent;
        private System.Windows.Forms.Button btnSSCNetReset;
        private System.Windows.Forms.Button btnDataInit;
        private System.Windows.Forms.Label lblUnloaderReset;
        private System.Windows.Forms.Label lblProcessReset;
        private System.Windows.Forms.Label lblLoaderReset;
        private System.Windows.Forms.Label lblAllReset;
        private System.Windows.Forms.Label lblLoaderPercent;
        private System.Windows.Forms.Label lblProcessPercent;
        private System.Windows.Forms.Label lblUnloaderPercent;
        private System.Windows.Forms.Label lblAllPercent;
        internal System.Windows.Forms.Label lblLoader;
        private System.Windows.Forms.Label lbl31CellX1;
        private System.Windows.Forms.Label lbl32CellX2;
        private System.Windows.Forms.Label lbl35CellTransX1;
        private System.Windows.Forms.Label lbl33CellTransferY1;
        private System.Windows.Forms.Label lbl36CellTransferX2;
        private System.Windows.Forms.Label lbl04CstRotationUpLeft;
        private System.Windows.Forms.Label lbl05CstRotationDownLeft;
        private System.Windows.Forms.Label lbl06CstRotationUpRight;
        private System.Windows.Forms.Label lbl07CstRotationDownRight;
        private System.Windows.Forms.Label lbl08CstElectrictyZLeft;
        private System.Windows.Forms.Label lbl10CellY1;
        private System.Windows.Forms.Label lbl09CstElectrictyZRight;
        private System.Windows.Forms.Label lbl11CellTransferY3;
        private System.Windows.Forms.Label lbl12CellTransferT1;
        private System.Windows.Forms.Label lbl13CellY2;
        private System.Windows.Forms.Label lbl14CellTransferY4;
        private System.Windows.Forms.Label lbl15CellTransferT2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Label lbl21BreakZ4;
        private System.Windows.Forms.Label lbl20BreakZ3;
        private System.Windows.Forms.Label lbl19BreakZ2;
        private System.Windows.Forms.Label lbl18BreakZ1;
        private System.Windows.Forms.Label lbl14BreakX4;
        private System.Windows.Forms.Label lbl13BreakX3;
        private System.Windows.Forms.Label lbl12BreakX2;
        private System.Windows.Forms.Label lbl11BreakX1;
        private System.Windows.Forms.Label lbl10BreakTableY1;
        private System.Windows.Forms.Label lbl09BreakTableY1;
        private System.Windows.Forms.Label lbl22MasureX1;
        private System.Windows.Forms.Label lbl07CellTransferY1;
        private System.Windows.Forms.Label lbl06CellTransferX2;
        private System.Windows.Forms.Label lbl05CellTransferX1;
        private System.Windows.Forms.Label lbl04AlignX1;
        private System.Windows.Forms.Label lbl17HeadZ1;
        private System.Windows.Forms.Label lbl03HeadX1;
        private System.Windows.Forms.Label lbl02TableY2;
        private System.Windows.Forms.Label lbl01TableY1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl4BreakTableT4;
        private System.Windows.Forms.Label lbl2BreakTableT3;
        private System.Windows.Forms.Label lbl1BreakTableT2;
        private System.Windows.Forms.Label lbl0BreakTableT1;
        private System.Windows.Forms.Label lbl30CstRotationDownRight;
        private System.Windows.Forms.Label lbl29CstRotationUpRight;
        private System.Windows.Forms.Label lbl28CellY2;
        private System.Windows.Forms.Label lbl27CellY1;
        private System.Windows.Forms.Label lbl26InspZ1;
        private System.Windows.Forms.Label lbl24CellTransferT4;
        private System.Windows.Forms.Label lbl23CellTransferT3;
        private System.Windows.Forms.Label lbl21CellTransferT2;
        private System.Windows.Forms.Label lbl20CellTransferT1;
        private System.Windows.Forms.Label lbl19CstElectrictyZRight;
        private System.Windows.Forms.Label lbl18CstElectrictyZLeft;
        private System.Windows.Forms.Label lbl17CstRotationDownLeft;
        private System.Windows.Forms.Label lbl16CstRotationUpLeft;
        private System.Windows.Forms.Label lbl41CellX2;
        private System.Windows.Forms.Label lbl40CellX1;
        private System.Windows.Forms.Label lbl39InspX1;
        private System.Windows.Forms.Label lbl38CellTransferY2;
        private System.Windows.Forms.Label lbl37CellTransferY1;
        internal System.Windows.Forms.Label lblUnloader;
    }
}

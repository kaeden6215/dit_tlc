﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerLaserWindows : UserControl, IUIUpdate
    {
        public UcrlManagerLaserWindows()
        {
            InitializeComponent();
        }

        public void UIUpdate()
        {
            // Main
            // LASER
            panelConnectionToLaserLight.BackColor = GG.Equip.LaserProxy.IsConnection == true ? UiGlobal.LASERON_C : UiGlobal.LASEROFF_C;

            // KeySwitch
            // KEYON OFF 0 ON 1 command K
            panelMainKeySwitchLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.K == 1 ? UiGlobal.LASERON_C : UiGlobal.LASEROFF_C;
            lblMainKeySwitch.Text = GG.Equip.LaserProxy._laserParameterValues.K == 1 ? "Key On" : "Key Off";

            // Shutter
            GG.Equip.LaserProxy.GetIsOpenShutter();
            panelMainShutterLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.S == 1 ? UiGlobal.LASERON_C : UiGlobal.LASEROFF_C;
            lblMainShutter.Text = GG.Equip.LaserProxy._laserParameterValues.S == 1 ? "Open" : "Closed";

            // System Faults
            GG.Equip.LaserProxy.GetIsAlarmList();
            panelMainSystemFaultsLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.F == string.Empty ? UiGlobal.OK_C : UiGlobal.NG_C;
            lblMainSystemFaults.Text = GG.Equip.LaserProxy._laserParameterValues.F == string.Empty ? "SYSTEM OK" : "SYSTEM NG";

            // System Status
            // Returns the state of the laser (-1: Boot, 0: Ready, 1: On, 2: Fault)
            GG.Equip.LaserProxy.GetIsLaserState();
            panelMainSystemStatusLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.State == -1 ? UiGlobal.LASEROFF_C :
                GG.Equip.LaserProxy._laserParameterValues.State == 0 ? UiGlobal.READY_C :
                GG.Equip.LaserProxy._laserParameterValues.State == 1 ? UiGlobal.LASERON_C :
                GG.Equip.LaserProxy._laserParameterValues.State == 2 ? UiGlobal.NG_C : UiGlobal.LASEROFF_C;
            lblMainSystemStatus.Text = GG.Equip.LaserProxy._laserParameterValues.State == -1 ? "Boot" :
                GG.Equip.LaserProxy._laserParameterValues.State == 0 ? "Ready" :
                GG.Equip.LaserProxy._laserParameterValues.State == 1 ? "On" :
                GG.Equip.LaserProxy._laserParameterValues.State == 2 ? "Fault" : "Boot";

            // Timing and Frequency
            txtMainTimingAndFrequencyAmplifierReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.RRAmpSet);
            txtMainTimingAndFrequencyDividerReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.RRD);
            txtMainTimingAndFrequencyPulseModeReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.PM);
            txtMainTimingAndFrequencyBurstReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.Burst);
            txtMainTimingAndFrequencyOutputReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.POut);

            // Attenuator Power Control
            GG.Equip.LaserProxy.GetIsCloseLoop();
            panelMainAttenuatorPowerControlOpenLoopLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.PRENABLE == 0 ? UiGlobal.LASERON_C : UiGlobal.LASEROFF_C;
            panelMainAttenuatorPowerControlClosedLoopLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.PRENABLE == 1 ? UiGlobal.LASERON_C : UiGlobal.LASEROFF_C;
            txtMainAttenuatorPowerControlTransmissionReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.TATT);
            txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.PATTSET);

            // Modulator Pulse Energy Control
            GG.Equip.LaserProxy.GetIsExternalMode();
            panelMainModulatorPulseEnergyControlInternalLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.EM == 0 ? UiGlobal.LASERON_C : UiGlobal.LASEROFF_C;
            panelMainModulatorPulseEnergyControlExternalLight.BackColor = GG.Equip.LaserProxy._laserParameterValues.EM == 1 ? UiGlobal.LASERON_C : UiGlobal.LASEROFF_C;
            txtMainModulatorPulseEnergyControlReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.RL);

            // PD7 Value
            txtMainPD7ValueOutputPowerReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.POut);
            txtMainPD7ValuePD7PowerReadOnly.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.PD7);

            // Monitoring
            // Water Monitor
            //panelMonitoringWaterMonitorLight.BackColor =
            txtMonitoringWaterMonitorTemp.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.WT);
            txtMonitoringWaterMonitorFlow.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.WF);

            // Housing Temperature 1
            //panelMonitoringHousingTemperature1Light.BackColor =
            txtMonitoringHousingTemperature1Temp.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.BT1);

            // Housing Temperature 2
            //panelMonitoringHousingTemperature2Light.BackColor = 
            txtMonitoringHousingTemperature2Temp.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.BT2);

            // H1 - Amplifier
            //panelMonitoringH1AmplifierLight.BackColor = 
            txtMonitoringH1AmplifierHumidity.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H1H);
            txtMonitoringH1AmplifierTemp.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H1T);
            txtMonitoringH1AmplifierDewpoint.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H1DP);

            // H2 - Harmonic Module Air
            //panelMonitoringH2HarmonicModuleAirLight.BackColor =
            txtMonitoringH2Humidity.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H2H);
            txtMonitoringH2Temp.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H2T);
            txtMonitoringH2Dewpoint.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H2DP);

            // H3 - Ambient Air
            //panelMonitoringH3AmbientAirLight.BackColor = 
            txtMonitoringH3Humidity.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H3H);
            txtMonitoringH3Temp.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H3T);
            txtMonitoringH3Dewpoint.Text = string.Format("{0}", GG.Equip.LaserProxy._laserParameterValues.H3DP);
        }

        private void btnMainConnectionToLaserDisConnection_Click(object sender, EventArgs e)
        {
            if(GG.Equip.LaserProxy.IsConnection == false)
            {
                GG.Equip.LaserProxy.Start();
            }
            else
            {
                GG.Equip.LaserProxy.Stop();
            }
        }

        private void btnMainShutterClose_Click(object sender, EventArgs e)
        {
            // 0 : Close, 1: Open
            if(GG.Equip.LaserProxy.GetIsOpenShutter() == false)
            {
                GG.Equip.LaserProxy.SetOpenShutter(true);
            }
            else
            {
                GG.Equip.LaserProxy.SetOpenShutter(false);
            }

        }

        private void btnMainSystemFaultsClear_Click(object sender, EventArgs e)
        {
            // Alarm 조건 필요?
            GG.Equip.LaserProxy.SetClearAlarm();
        }

        private void btnMainSystemStatusStart_Click(object sender, EventArgs e)
        {

        }

        private void btnMainSystemStatusStop_Click(object sender, EventArgs e)
        {

        }

        private void btnMainTimingAndFrequencyAmplifierSet_Click(object sender, EventArgs e)
        {
            // RRAmpSet?? RRAmpSet??
            if (cbxtMainTimingAndFrequencyAmplifier.Text != string.Empty)
                GG.Equip.LaserProxy.SetRepetitionRate(double.Parse(cbxtMainTimingAndFrequencyAmplifier.Text));
        }

        private void btnMainTimingAndFrequencyDividerSet_Click(object sender, EventArgs e)
        {
            if (txtMainTimingAndFrequencyDivider.Text != string.Empty)
                GG.Equip.LaserProxy.SetAOM2Divisor(int.Parse(txtMainTimingAndFrequencyDivider.Text));
        }

        private void btnMainTimingAndFrequencyPulseModeSet_Click(object sender, EventArgs e)
        {
            // 0: Continuous, 1: Gated, 2: Divided, 3: Divided&Gated, 4: Burst, 5: Burst &Divided 
            if (cbxtMainTimingAndFrequencyPulseMode.Text != string.Empty)
                GG.Equip.LaserProxy.SetPulseMode(int.Parse(cbxtMainTimingAndFrequencyPulseMode.Text));
        }

        private void btnMainTimingAndFrequencyBurstSet_Click(object sender, EventArgs e)
        {
            if(txtMainTimingAndFrequencyBurst.Text != string.Empty)
                GG.Equip.LaserProxy.SetBurst(int.Parse(txtMainTimingAndFrequencyBurst.Text));
        }

        private void btnMainAttenuatorPowerControlTransmissionSet_Click(object sender, EventArgs e)
        {
            if (txtMainAttenuatorPowerControlTransmission.Text != string.Empty)
                GG.Equip.LaserProxy.SetTransmission(double.Parse(txtMainAttenuatorPowerControlTransmission.Text));
        }

        private void btnMainAttenuatorPowerControlAttenuatorPowerSet_Click(object sender, EventArgs e)
        {
            if (txtMainAttenuatorPowerControlAttenuatorPower.Text != string.Empty)
                GG.Equip.LaserProxy.SetIRPower(double.Parse(txtMainAttenuatorPowerControlAttenuatorPower.Text));
        }

        private void btnMainModulatorPulseEnergyControlSet_Click(object sender, EventArgs e)
        {
            if (txtMainModulatorPulseEnergyControl.Text != string.Empty)
                GG.Equip.LaserProxy.SetRFPercentLevel(double.Parse(txtMainModulatorPulseEnergyControl.Text));
        }
    }
}

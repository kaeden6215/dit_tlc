﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerManualCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCstLoad = new System.Windows.Forms.Button();
            this.btnCellOutTransfer = new System.Windows.Forms.Button();
            this.btnCellLoadTransfer01 = new System.Windows.Forms.Button();
            this.btnCellLoadTransfer02 = new System.Windows.Forms.Button();
            this.btnIRCutProcess = new System.Windows.Forms.Button();
            this.btnAfterIRCutTransfer = new System.Windows.Forms.Button();
            this.btnBreakUnit01 = new System.Windows.Forms.Button();
            this.btnBreakUnit02 = new System.Windows.Forms.Button();
            this.btnBreakHeadUnitXZ = new System.Windows.Forms.Button();
            this.btnBeforeTransfer = new System.Windows.Forms.Button();
            this.btnCameraUnit = new System.Windows.Forms.Button();
            this.btnCellInTransfer = new System.Windows.Forms.Button();
            this.btnCstUnloader = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnInspUnit = new System.Windows.Forms.Button();
            this.btnAfterTransfer = new System.Windows.Forms.Button();
            this.ucrlAjinServoCtrl07 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl08 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl05 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl06 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl03 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl04 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl01 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl02 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel1.Controls.Add(this.btnCstLoad);
            this.flowLayoutPanel1.Controls.Add(this.btnCellOutTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCellLoadTransfer01);
            this.flowLayoutPanel1.Controls.Add(this.btnCellLoadTransfer02);
            this.flowLayoutPanel1.Controls.Add(this.btnIRCutProcess);
            this.flowLayoutPanel1.Controls.Add(this.btnAfterIRCutTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakUnit01);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakUnit02);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakHeadUnitXZ);
            this.flowLayoutPanel1.Controls.Add(this.btnBeforeTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnInspUnit);
            this.flowLayoutPanel1.Controls.Add(this.btnAfterTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCameraUnit);
            this.flowLayoutPanel1.Controls.Add(this.btnCellInTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCstUnloader);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1740, 47);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btnCstLoad
            // 
            this.btnCstLoad.Location = new System.Drawing.Point(3, 3);
            this.btnCstLoad.Name = "btnCstLoad";
            this.btnCstLoad.Size = new System.Drawing.Size(110, 41);
            this.btnCstLoad.TabIndex = 0;
            this.btnCstLoad.Text = "카셋트 로드";
            this.btnCstLoad.UseVisualStyleBackColor = true;
            this.btnCstLoad.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellOutTransfer
            // 
            this.btnCellOutTransfer.Location = new System.Drawing.Point(119, 3);
            this.btnCellOutTransfer.Name = "btnCellOutTransfer";
            this.btnCellOutTransfer.Size = new System.Drawing.Size(110, 41);
            this.btnCellOutTransfer.TabIndex = 1;
            this.btnCellOutTransfer.Text = "설 취출 이재기";
            this.btnCellOutTransfer.UseVisualStyleBackColor = true;
            this.btnCellOutTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellLoadTransfer01
            // 
            this.btnCellLoadTransfer01.Location = new System.Drawing.Point(235, 3);
            this.btnCellLoadTransfer01.Name = "btnCellLoadTransfer01";
            this.btnCellLoadTransfer01.Size = new System.Drawing.Size(110, 41);
            this.btnCellLoadTransfer01.TabIndex = 11;
            this.btnCellLoadTransfer01.Text = "셀 로드 TR";
            this.btnCellLoadTransfer01.UseVisualStyleBackColor = true;
            this.btnCellLoadTransfer01.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellLoadTransfer02
            // 
            this.btnCellLoadTransfer02.Location = new System.Drawing.Point(351, 3);
            this.btnCellLoadTransfer02.Name = "btnCellLoadTransfer02";
            this.btnCellLoadTransfer02.Size = new System.Drawing.Size(110, 41);
            this.btnCellLoadTransfer02.TabIndex = 2;
            this.btnCellLoadTransfer02.Text = "셀 로드 Sub TR";
            this.btnCellLoadTransfer02.UseVisualStyleBackColor = true;
            this.btnCellLoadTransfer02.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnIRCutProcess
            // 
            this.btnIRCutProcess.Location = new System.Drawing.Point(467, 3);
            this.btnIRCutProcess.Name = "btnIRCutProcess";
            this.btnIRCutProcess.Size = new System.Drawing.Size(110, 41);
            this.btnIRCutProcess.TabIndex = 3;
            this.btnIRCutProcess.Text = "IR Cut 유닛/해더";
            this.btnIRCutProcess.UseVisualStyleBackColor = true;
            this.btnIRCutProcess.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnAfterIRCutTransfer
            // 
            this.btnAfterIRCutTransfer.Location = new System.Drawing.Point(583, 3);
            this.btnAfterIRCutTransfer.Name = "btnAfterIRCutTransfer";
            this.btnAfterIRCutTransfer.Size = new System.Drawing.Size(110, 41);
            this.btnAfterIRCutTransfer.TabIndex = 4;
            this.btnAfterIRCutTransfer.Text = "Break 트랜스퍼";
            this.btnAfterIRCutTransfer.UseVisualStyleBackColor = true;
            this.btnAfterIRCutTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakUnit01
            // 
            this.btnBreakUnit01.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnit01.Location = new System.Drawing.Point(699, 3);
            this.btnBreakUnit01.Name = "btnBreakUnit01";
            this.btnBreakUnit01.Size = new System.Drawing.Size(110, 41);
            this.btnBreakUnit01.TabIndex = 5;
            this.btnBreakUnit01.Text = "Break 유닛(X/Z축)";
            this.btnBreakUnit01.UseVisualStyleBackColor = true;
            this.btnBreakUnit01.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakUnit02
            // 
            this.btnBreakUnit02.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnBreakUnit02.Location = new System.Drawing.Point(815, 3);
            this.btnBreakUnit02.Name = "btnBreakUnit02";
            this.btnBreakUnit02.Size = new System.Drawing.Size(110, 41);
            this.btnBreakUnit02.TabIndex = 6;
            this.btnBreakUnit02.Text = "Break 유닛(T/Y축)";
            this.btnBreakUnit02.UseVisualStyleBackColor = true;
            this.btnBreakUnit02.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakHeadUnitXZ
            // 
            this.btnBreakHeadUnitXZ.Location = new System.Drawing.Point(931, 3);
            this.btnBreakHeadUnitXZ.Name = "btnBreakHeadUnitXZ";
            this.btnBreakHeadUnitXZ.Size = new System.Drawing.Size(110, 41);
            this.btnBreakHeadUnitXZ.TabIndex = 12;
            this.btnBreakHeadUnitXZ.Text = "Break Head(X,Z)";
            this.btnBreakHeadUnitXZ.UseVisualStyleBackColor = true;
            this.btnBreakHeadUnitXZ.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBeforeTransfer
            // 
            this.btnBeforeTransfer.Location = new System.Drawing.Point(1047, 3);
            this.btnBeforeTransfer.Name = "btnBeforeTransfer";
            this.btnBeforeTransfer.Size = new System.Drawing.Size(110, 41);
            this.btnBeforeTransfer.TabIndex = 7;
            this.btnBeforeTransfer.Text = "검사 전 TR";
            this.btnBeforeTransfer.UseVisualStyleBackColor = true;
            this.btnBeforeTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCameraUnit
            // 
            this.btnCameraUnit.Location = new System.Drawing.Point(1395, 3);
            this.btnCameraUnit.Name = "btnCameraUnit";
            this.btnCameraUnit.Size = new System.Drawing.Size(110, 41);
            this.btnCameraUnit.TabIndex = 8;
            this.btnCameraUnit.Text = "카메라 유닛";
            this.btnCameraUnit.UseVisualStyleBackColor = true;
            this.btnCameraUnit.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellInTransfer
            // 
            this.btnCellInTransfer.Location = new System.Drawing.Point(1511, 3);
            this.btnCellInTransfer.Name = "btnCellInTransfer";
            this.btnCellInTransfer.Size = new System.Drawing.Size(110, 41);
            this.btnCellInTransfer.TabIndex = 9;
            this.btnCellInTransfer.Text = "셀 투입 이재기";
            this.btnCellInTransfer.UseVisualStyleBackColor = true;
            this.btnCellInTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCstUnloader
            // 
            this.btnCstUnloader.Location = new System.Drawing.Point(1627, 3);
            this.btnCstUnloader.Name = "btnCstUnloader";
            this.btnCstUnloader.Size = new System.Drawing.Size(110, 41);
            this.btnCstUnloader.TabIndex = 10;
            this.btnCstUnloader.Text = "카셋트 언로드";
            this.btnCstUnloader.UseVisualStyleBackColor = true;
            this.btnCstUnloader.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl07);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl08);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl05);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl06);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl03);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl04);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl01);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl02);
            this.panel1.Location = new System.Drawing.Point(3, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1737, 807);
            this.panel1.TabIndex = 4;
            // 
            // btnInspUnit
            // 
            this.btnInspUnit.Location = new System.Drawing.Point(1163, 3);
            this.btnInspUnit.Name = "btnInspUnit";
            this.btnInspUnit.Size = new System.Drawing.Size(110, 41);
            this.btnInspUnit.TabIndex = 13;
            this.btnInspUnit.Text = "검사 유닛";
            this.btnInspUnit.UseVisualStyleBackColor = true;
            this.btnInspUnit.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnAfterTransfer
            // 
            this.btnAfterTransfer.Location = new System.Drawing.Point(1279, 3);
            this.btnAfterTransfer.Name = "btnAfterTransfer";
            this.btnAfterTransfer.Size = new System.Drawing.Size(110, 41);
            this.btnAfterTransfer.TabIndex = 14;
            this.btnAfterTransfer.Text = "검사 후 TR";
            this.btnAfterTransfer.UseVisualStyleBackColor = true;
            this.btnAfterTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // ucrlAjinServoCtrl07
            // 
            this.ucrlAjinServoCtrl07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl07.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl07.Location = new System.Drawing.Point(3, 578);
            this.ucrlAjinServoCtrl07.Name = "ucrlAjinServoCtrl07";
            this.ucrlAjinServoCtrl07.Servo = null;
            this.ucrlAjinServoCtrl07.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl07.TabIndex = 14;
            // 
            // ucrlAjinServoCtrl08
            // 
            this.ucrlAjinServoCtrl08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl08.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl08.Location = new System.Drawing.Point(862, 578);
            this.ucrlAjinServoCtrl08.Name = "ucrlAjinServoCtrl08";
            this.ucrlAjinServoCtrl08.Servo = null;
            this.ucrlAjinServoCtrl08.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl08.TabIndex = 13;
            // 
            // ucrlAjinServoCtrl05
            // 
            this.ucrlAjinServoCtrl05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl05.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl05.Location = new System.Drawing.Point(3, 389);
            this.ucrlAjinServoCtrl05.Name = "ucrlAjinServoCtrl05";
            this.ucrlAjinServoCtrl05.Servo = null;
            this.ucrlAjinServoCtrl05.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl05.TabIndex = 12;
            // 
            // ucrlAjinServoCtrl06
            // 
            this.ucrlAjinServoCtrl06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl06.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl06.Location = new System.Drawing.Point(862, 389);
            this.ucrlAjinServoCtrl06.Name = "ucrlAjinServoCtrl06";
            this.ucrlAjinServoCtrl06.Servo = null;
            this.ucrlAjinServoCtrl06.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl06.TabIndex = 11;
            // 
            // ucrlAjinServoCtrl03
            // 
            this.ucrlAjinServoCtrl03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl03.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl03.Location = new System.Drawing.Point(3, 200);
            this.ucrlAjinServoCtrl03.Name = "ucrlAjinServoCtrl03";
            this.ucrlAjinServoCtrl03.Servo = null;
            this.ucrlAjinServoCtrl03.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl03.TabIndex = 10;
            // 
            // ucrlAjinServoCtrl04
            // 
            this.ucrlAjinServoCtrl04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl04.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl04.Location = new System.Drawing.Point(862, 200);
            this.ucrlAjinServoCtrl04.Name = "ucrlAjinServoCtrl04";
            this.ucrlAjinServoCtrl04.Servo = null;
            this.ucrlAjinServoCtrl04.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl04.TabIndex = 9;
            // 
            // ucrlAjinServoCtrl01
            // 
            this.ucrlAjinServoCtrl01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl01.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl01.Location = new System.Drawing.Point(3, 11);
            this.ucrlAjinServoCtrl01.Name = "ucrlAjinServoCtrl01";
            this.ucrlAjinServoCtrl01.Servo = null;
            this.ucrlAjinServoCtrl01.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl01.TabIndex = 8;
            // 
            // ucrlAjinServoCtrl02
            // 
            this.ucrlAjinServoCtrl02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl02.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlAjinServoCtrl02.Location = new System.Drawing.Point(862, 11);
            this.ucrlAjinServoCtrl02.Name = "ucrlAjinServoCtrl02";
            this.ucrlAjinServoCtrl02.Servo = null;
            this.ucrlAjinServoCtrl02.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl02.TabIndex = 6;
            // 
            // UcrlManagerManualCtrl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerManualCtrl";
            this.Size = new System.Drawing.Size(1740, 860);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UcrlAjinServoCtrl ucrlAjinServoCtrl01;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl02;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnCstLoad;
        private System.Windows.Forms.Button btnCellOutTransfer;
        private System.Windows.Forms.Button btnCellLoadTransfer02;
        private System.Windows.Forms.Button btnIRCutProcess;
        private System.Windows.Forms.Button btnAfterIRCutTransfer;
        private System.Windows.Forms.Button btnBreakUnit01;
        private System.Windows.Forms.Button btnBreakUnit02;
        private System.Windows.Forms.Button btnBeforeTransfer;
        private System.Windows.Forms.Button btnCameraUnit;
        private System.Windows.Forms.Button btnCellInTransfer;
        private System.Windows.Forms.Button btnCstUnloader;
        private System.Windows.Forms.Panel panel1;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl07;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl08;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl05;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl06;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl03;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl04;
        private System.Windows.Forms.Button btnCellLoadTransfer01;
        private System.Windows.Forms.Button btnBreakHeadUnitXZ;
        private System.Windows.Forms.Button btnInspUnit;
        private System.Windows.Forms.Button btnAfterTransfer;
    }
}

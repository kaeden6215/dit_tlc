﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerSEM
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel29 = new System.Windows.Forms.Panel();
            this.lviGpsAvMeter = new System.Windows.Forms.ListView();
            this.ColumnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnGpsAvMeter4 = new System.Windows.Forms.Button();
            this.btnGpsAvMeterIp = new System.Windows.Forms.Button();
            this.btnGpsAvMeterClose = new System.Windows.Forms.Button();
            this.btnGpsAvMeterOpen = new System.Windows.Forms.Button();
            this.lblGpsAvMeter = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lviUpsAvMeter = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnUpsAvMeter4 = new System.Windows.Forms.Button();
            this.btnUpsAvMeterIp = new System.Windows.Forms.Button();
            this.btnUpsAvMeterClose = new System.Windows.Forms.Button();
            this.btnUpsAvMeterOpen = new System.Windows.Forms.Button();
            this.lblUpsAvMeter = new System.Windows.Forms.Label();
            this.panel29.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.lviGpsAvMeter);
            this.panel29.Controls.Add(this.btnGpsAvMeter4);
            this.panel29.Controls.Add(this.btnGpsAvMeterIp);
            this.panel29.Controls.Add(this.btnGpsAvMeterClose);
            this.panel29.Controls.Add(this.btnGpsAvMeterOpen);
            this.panel29.Controls.Add(this.lblGpsAvMeter);
            this.panel29.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel29.Location = new System.Drawing.Point(1, 1);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(865, 869);
            this.panel29.TabIndex = 460;
            // 
            // lviGpsAvMeter
            // 
            this.lviGpsAvMeter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lviGpsAvMeter.GridLines = true;
            this.lviGpsAvMeter.Location = new System.Drawing.Point(3, 75);
            this.lviGpsAvMeter.Name = "lviGpsAvMeter";
            this.lviGpsAvMeter.Size = new System.Drawing.Size(857, 777);
            this.lviGpsAvMeter.TabIndex = 68;
            this.lviGpsAvMeter.UseCompatibleStateImageBehavior = false;
            this.lviGpsAvMeter.View = System.Windows.Forms.View.Details;
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "명칭";
            this.ColumnHeader1.Width = 300;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "값";
            this.columnHeader2.Width = 120;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "단위";
            this.columnHeader3.Width = 120;
            // 
            // btnGpsAvMeter4
            // 
            this.btnGpsAvMeter4.BackColor = System.Drawing.SystemColors.Control;
            this.btnGpsAvMeter4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnGpsAvMeter4.ForeColor = System.Drawing.Color.Black;
            this.btnGpsAvMeter4.Location = new System.Drawing.Point(419, 27);
            this.btnGpsAvMeter4.Name = "btnGpsAvMeter4";
            this.btnGpsAvMeter4.Size = new System.Drawing.Size(72, 42);
            this.btnGpsAvMeter4.TabIndex = 67;
            this.btnGpsAvMeter4.Text = "502";
            this.btnGpsAvMeter4.UseVisualStyleBackColor = false;
            // 
            // btnGpsAvMeterIp
            // 
            this.btnGpsAvMeterIp.BackColor = System.Drawing.SystemColors.Control;
            this.btnGpsAvMeterIp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnGpsAvMeterIp.ForeColor = System.Drawing.Color.Black;
            this.btnGpsAvMeterIp.Location = new System.Drawing.Point(257, 27);
            this.btnGpsAvMeterIp.Name = "btnGpsAvMeterIp";
            this.btnGpsAvMeterIp.Size = new System.Drawing.Size(156, 42);
            this.btnGpsAvMeterIp.TabIndex = 66;
            this.btnGpsAvMeterIp.Text = "126.100.100.120";
            this.btnGpsAvMeterIp.UseVisualStyleBackColor = false;
            // 
            // btnGpsAvMeterClose
            // 
            this.btnGpsAvMeterClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnGpsAvMeterClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnGpsAvMeterClose.ForeColor = System.Drawing.Color.Black;
            this.btnGpsAvMeterClose.Location = new System.Drawing.Point(130, 27);
            this.btnGpsAvMeterClose.Name = "btnGpsAvMeterClose";
            this.btnGpsAvMeterClose.Size = new System.Drawing.Size(121, 42);
            this.btnGpsAvMeterClose.TabIndex = 65;
            this.btnGpsAvMeterClose.Text = "Close";
            this.btnGpsAvMeterClose.UseVisualStyleBackColor = false;
            this.btnGpsAvMeterClose.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnGpsAvMeterOpen
            // 
            this.btnGpsAvMeterOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnGpsAvMeterOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnGpsAvMeterOpen.ForeColor = System.Drawing.Color.Black;
            this.btnGpsAvMeterOpen.Location = new System.Drawing.Point(3, 27);
            this.btnGpsAvMeterOpen.Name = "btnGpsAvMeterOpen";
            this.btnGpsAvMeterOpen.Size = new System.Drawing.Size(121, 42);
            this.btnGpsAvMeterOpen.TabIndex = 64;
            this.btnGpsAvMeterOpen.Text = "Open";
            this.btnGpsAvMeterOpen.UseVisualStyleBackColor = false;
            this.btnGpsAvMeterOpen.Click += new System.EventHandler(this.Button_Click);
            // 
            // lblGpsAvMeter
            // 
            this.lblGpsAvMeter.AutoEllipsis = true;
            this.lblGpsAvMeter.BackColor = System.Drawing.Color.Gainsboro;
            this.lblGpsAvMeter.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblGpsAvMeter.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblGpsAvMeter.ForeColor = System.Drawing.Color.Black;
            this.lblGpsAvMeter.Location = new System.Drawing.Point(0, 0);
            this.lblGpsAvMeter.Name = "lblGpsAvMeter";
            this.lblGpsAvMeter.Size = new System.Drawing.Size(863, 24);
            this.lblGpsAvMeter.TabIndex = 9;
            this.lblGpsAvMeter.Text = "■ GPS AV-METER";
            this.lblGpsAvMeter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lviUpsAvMeter);
            this.panel1.Controls.Add(this.btnUpsAvMeter4);
            this.panel1.Controls.Add(this.btnUpsAvMeterIp);
            this.panel1.Controls.Add(this.btnUpsAvMeterClose);
            this.panel1.Controls.Add(this.btnUpsAvMeterOpen);
            this.panel1.Controls.Add(this.lblUpsAvMeter);
            this.panel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel1.Location = new System.Drawing.Point(874, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(865, 869);
            this.panel1.TabIndex = 461;
            // 
            // lviUpsAvMeter
            // 
            this.lviUpsAvMeter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lviUpsAvMeter.GridLines = true;
            this.lviUpsAvMeter.Location = new System.Drawing.Point(3, 75);
            this.lviUpsAvMeter.Name = "lviUpsAvMeter";
            this.lviUpsAvMeter.Size = new System.Drawing.Size(857, 777);
            this.lviUpsAvMeter.TabIndex = 68;
            this.lviUpsAvMeter.UseCompatibleStateImageBehavior = false;
            this.lviUpsAvMeter.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "명칭";
            this.columnHeader4.Width = 300;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "값";
            this.columnHeader5.Width = 120;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "단위";
            this.columnHeader6.Width = 120;
            // 
            // btnUpsAvMeter4
            // 
            this.btnUpsAvMeter4.BackColor = System.Drawing.SystemColors.Control;
            this.btnUpsAvMeter4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUpsAvMeter4.ForeColor = System.Drawing.Color.Black;
            this.btnUpsAvMeter4.Location = new System.Drawing.Point(419, 27);
            this.btnUpsAvMeter4.Name = "btnUpsAvMeter4";
            this.btnUpsAvMeter4.Size = new System.Drawing.Size(72, 42);
            this.btnUpsAvMeter4.TabIndex = 67;
            this.btnUpsAvMeter4.Text = "502";
            this.btnUpsAvMeter4.UseVisualStyleBackColor = false;
            // 
            // btnUpsAvMeterIp
            // 
            this.btnUpsAvMeterIp.BackColor = System.Drawing.SystemColors.Control;
            this.btnUpsAvMeterIp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUpsAvMeterIp.ForeColor = System.Drawing.Color.Black;
            this.btnUpsAvMeterIp.Location = new System.Drawing.Point(257, 27);
            this.btnUpsAvMeterIp.Name = "btnUpsAvMeterIp";
            this.btnUpsAvMeterIp.Size = new System.Drawing.Size(156, 42);
            this.btnUpsAvMeterIp.TabIndex = 66;
            this.btnUpsAvMeterIp.Text = "126.100.100.121";
            this.btnUpsAvMeterIp.UseVisualStyleBackColor = false;
            // 
            // btnUpsAvMeterClose
            // 
            this.btnUpsAvMeterClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnUpsAvMeterClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUpsAvMeterClose.ForeColor = System.Drawing.Color.Black;
            this.btnUpsAvMeterClose.Location = new System.Drawing.Point(130, 27);
            this.btnUpsAvMeterClose.Name = "btnUpsAvMeterClose";
            this.btnUpsAvMeterClose.Size = new System.Drawing.Size(121, 42);
            this.btnUpsAvMeterClose.TabIndex = 65;
            this.btnUpsAvMeterClose.Text = "Close";
            this.btnUpsAvMeterClose.UseVisualStyleBackColor = false;
            this.btnUpsAvMeterClose.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnUpsAvMeterOpen
            // 
            this.btnUpsAvMeterOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnUpsAvMeterOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUpsAvMeterOpen.ForeColor = System.Drawing.Color.Black;
            this.btnUpsAvMeterOpen.Location = new System.Drawing.Point(3, 27);
            this.btnUpsAvMeterOpen.Name = "btnUpsAvMeterOpen";
            this.btnUpsAvMeterOpen.Size = new System.Drawing.Size(121, 42);
            this.btnUpsAvMeterOpen.TabIndex = 64;
            this.btnUpsAvMeterOpen.Text = "Open";
            this.btnUpsAvMeterOpen.UseVisualStyleBackColor = false;
            this.btnUpsAvMeterOpen.Click += new System.EventHandler(this.Button_Click);
            // 
            // lblUpsAvMeter
            // 
            this.lblUpsAvMeter.AutoEllipsis = true;
            this.lblUpsAvMeter.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUpsAvMeter.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUpsAvMeter.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUpsAvMeter.ForeColor = System.Drawing.Color.Black;
            this.lblUpsAvMeter.Location = new System.Drawing.Point(0, 0);
            this.lblUpsAvMeter.Name = "lblUpsAvMeter";
            this.lblUpsAvMeter.Size = new System.Drawing.Size(863, 24);
            this.lblUpsAvMeter.TabIndex = 9;
            this.lblUpsAvMeter.Text = "■ UPS AV-METER";
            this.lblUpsAvMeter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlManagerSEM
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel29);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerSEM";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel29.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.ListView lviGpsAvMeter;
        private System.Windows.Forms.ColumnHeader ColumnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnGpsAvMeter4;
        private System.Windows.Forms.Button btnGpsAvMeterIp;
        private System.Windows.Forms.Button btnGpsAvMeterClose;
        private System.Windows.Forms.Button btnGpsAvMeterOpen;
        internal System.Windows.Forms.Label lblGpsAvMeter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView lviUpsAvMeter;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button btnUpsAvMeter4;
        private System.Windows.Forms.Button btnUpsAvMeterIp;
        private System.Windows.Forms.Button btnUpsAvMeterClose;
        private System.Windows.Forms.Button btnUpsAvMeterOpen;
        internal System.Windows.Forms.Label lblUpsAvMeter;
    }
}

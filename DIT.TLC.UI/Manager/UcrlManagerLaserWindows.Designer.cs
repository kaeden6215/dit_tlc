﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerLaserWindows
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtMainPD7ValuePD7PowerReadOnly = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtMainPD7ValueOutputPowerReadOnly = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panelMainModulatorPulseEnergyControlExternalLight = new System.Windows.Forms.Panel();
            this.panelMainModulatorPulseEnergyControlInternalLight = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMainModulatorPulseEnergyControl = new System.Windows.Forms.TextBox();
            this.btnMainModulatorPulseEnergyControlSet = new System.Windows.Forms.Button();
            this.txtMainModulatorPulseEnergyControlReadOnly = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panelMainAttenuatorPowerControlClosedLoopLight = new System.Windows.Forms.Panel();
            this.panelMainAttenuatorPowerControlOpenLoopLight = new System.Windows.Forms.Panel();
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly = new System.Windows.Forms.TextBox();
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.txtMainAttenuatorPowerControlAttenuatorPower = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMainAttenuatorPowerControlTransmission = new System.Windows.Forms.TextBox();
            this.txtMainAttenuatorPowerControlTransmissionReadOnly = new System.Windows.Forms.TextBox();
            this.btnMainAttenuatorPowerControlTransmissionSet = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtMainTimingAndFrequencyBurstReadOnly = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnMainTimingAndFrequencyPulseModeSet = new System.Windows.Forms.Button();
            this.txtMainTimingAndFrequencyBurst = new System.Windows.Forms.TextBox();
            this.btnMainTimingAndFrequencyAmplifierSet = new System.Windows.Forms.Button();
            this.txtMainTimingAndFrequencyPulseModeReadOnly = new System.Windows.Forms.TextBox();
            this.btnMainTimingAndFrequencyDividerSet = new System.Windows.Forms.Button();
            this.cbxtMainTimingAndFrequencyPulseMode = new System.Windows.Forms.ComboBox();
            this.btnMainTimingAndFrequencyBurstSet = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbxtMainTimingAndFrequencyAmplifier = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMainTimingAndFrequencyOutputReadOnly = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMainTimingAndFrequencyDividerReadOnly = new System.Windows.Forms.TextBox();
            this.txtMainTimingAndFrequencyAmplifierReadOnly = new System.Windows.Forms.TextBox();
            this.txtMainTimingAndFrequencyDivider = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panelMainSystemStatusLight = new System.Windows.Forms.Panel();
            this.btnMainSystemStatusStop = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.btnMainSystemStatusStart = new System.Windows.Forms.Button();
            this.lblMainSystemStatus = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panelMainSystemFaultsLight = new System.Windows.Forms.Panel();
            this.btnMainSystemFaultsClear = new System.Windows.Forms.Button();
            this.lblMainSystemFaults = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panelMainShutterLight = new System.Windows.Forms.Panel();
            this.lblMainShutter = new System.Windows.Forms.Label();
            this.btnMainShutterClose = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panelMainKeySwitchLight = new System.Windows.Forms.Panel();
            this.lblMainKeySwitch = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelConnectionToLaserLight = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMainConnectionToLaserDisConnection = new System.Windows.Forms.Button();
            this.lblLoadingLoaderTransfer = new System.Windows.Forms.Label();
            this.btnMainLock = new System.Windows.Forms.Button();
            this.txtMainLock = new System.Windows.Forms.TextBox();
            this.lblLaserMain = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panelMonitoringH3AmbientAirLight = new System.Windows.Forms.Panel();
            this.txtMonitoringH3Dewpoint = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtMonitoringH3Humidity = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtMonitoringH3Temp = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panelMonitoringH2HarmonicModuleAirLight = new System.Windows.Forms.Panel();
            this.txtMonitoringH2Dewpoint = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtMonitoringH2Humidity = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtMonitoringH2Temp = new System.Windows.Forms.TextBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panelMonitoringH1AmplifierLight = new System.Windows.Forms.Panel();
            this.txtMonitoringH1AmplifierDewpoint = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMonitoringH1AmplifierHumidity = new System.Windows.Forms.TextBox();
            this.txtMonitoringH1AmplifierTemp = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panelMonitoringHousingTemperature2Light = new System.Windows.Forms.Panel();
            this.txtMonitoringHousingTemperature2Temp = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panelMonitoringHousingTemperature1Light = new System.Windows.Forms.Panel();
            this.txtMonitoringHousingTemperature1Temp = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panelMonitoringWaterMonitorLight = new System.Windows.Forms.Panel();
            this.txtMonitoringWaterMonitorFlow = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMonitoringWaterMonitorTemp = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.btnMainLock);
            this.panel1.Controls.Add(this.txtMainLock);
            this.panel1.Controls.Add(this.lblLaserMain);
            this.panel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 860);
            this.panel1.TabIndex = 464;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.txtMainPD7ValuePD7PowerReadOnly);
            this.panel12.Controls.Add(this.label19);
            this.panel12.Controls.Add(this.label42);
            this.panel12.Controls.Add(this.txtMainPD7ValueOutputPowerReadOnly);
            this.panel12.Controls.Add(this.label18);
            this.panel12.Location = new System.Drawing.Point(3, 777);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(862, 63);
            this.panel12.TabIndex = 475;
            // 
            // txtMainPD7ValuePD7PowerReadOnly
            // 
            this.txtMainPD7ValuePD7PowerReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainPD7ValuePD7PowerReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainPD7ValuePD7PowerReadOnly.Location = new System.Drawing.Point(734, 32);
            this.txtMainPD7ValuePD7PowerReadOnly.Name = "txtMainPD7ValuePD7PowerReadOnly";
            this.txtMainPD7ValuePD7PowerReadOnly.ReadOnly = true;
            this.txtMainPD7ValuePD7PowerReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainPD7ValuePD7PowerReadOnly.TabIndex = 45;
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(598, 32);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 23);
            this.label19.TabIndex = 44;
            this.label19.Text = "PD7 Power";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.AutoEllipsis = true;
            this.label42.BackColor = System.Drawing.Color.Gainsboro;
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(0, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(860, 22);
            this.label42.TabIndex = 9;
            this.label42.Text = "■ PD7 Value";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMainPD7ValueOutputPowerReadOnly
            // 
            this.txtMainPD7ValueOutputPowerReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainPD7ValueOutputPowerReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainPD7ValueOutputPowerReadOnly.Location = new System.Drawing.Point(143, 32);
            this.txtMainPD7ValueOutputPowerReadOnly.Name = "txtMainPD7ValueOutputPowerReadOnly";
            this.txtMainPD7ValueOutputPowerReadOnly.ReadOnly = true;
            this.txtMainPD7ValueOutputPowerReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainPD7ValueOutputPowerReadOnly.TabIndex = 43;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(6, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(130, 23);
            this.label18.TabIndex = 42;
            this.label18.Text = "Output Power (W)";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.panelMainModulatorPulseEnergyControlExternalLight);
            this.panel11.Controls.Add(this.panelMainModulatorPulseEnergyControlInternalLight);
            this.panel11.Controls.Add(this.label16);
            this.panel11.Controls.Add(this.label41);
            this.panel11.Controls.Add(this.label7);
            this.panel11.Controls.Add(this.label17);
            this.panel11.Controls.Add(this.txtMainModulatorPulseEnergyControl);
            this.panel11.Controls.Add(this.btnMainModulatorPulseEnergyControlSet);
            this.panel11.Controls.Add(this.txtMainModulatorPulseEnergyControlReadOnly);
            this.panel11.Location = new System.Drawing.Point(3, 636);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(862, 135);
            this.panel11.TabIndex = 478;
            // 
            // panelMainModulatorPulseEnergyControlExternalLight
            // 
            this.panelMainModulatorPulseEnergyControlExternalLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainModulatorPulseEnergyControlExternalLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainModulatorPulseEnergyControlExternalLight.Location = new System.Drawing.Point(453, 25);
            this.panelMainModulatorPulseEnergyControlExternalLight.Name = "panelMainModulatorPulseEnergyControlExternalLight";
            this.panelMainModulatorPulseEnergyControlExternalLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainModulatorPulseEnergyControlExternalLight.TabIndex = 42;
            // 
            // panelMainModulatorPulseEnergyControlInternalLight
            // 
            this.panelMainModulatorPulseEnergyControlInternalLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainModulatorPulseEnergyControlInternalLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainModulatorPulseEnergyControlInternalLight.Location = new System.Drawing.Point(6, 25);
            this.panelMainModulatorPulseEnergyControlInternalLight.Name = "panelMainModulatorPulseEnergyControlInternalLight";
            this.panelMainModulatorPulseEnergyControlInternalLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainModulatorPulseEnergyControlInternalLight.TabIndex = 37;
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(496, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(130, 33);
            this.label16.TabIndex = 41;
            this.label16.Text = "External";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.AutoEllipsis = true;
            this.label41.BackColor = System.Drawing.Color.Gainsboro;
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(0, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(860, 22);
            this.label41.TabIndex = 9;
            this.label41.Text = "■ Modulator Pulse Energy Control";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(50, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 33);
            this.label7.TabIndex = 13;
            this.label7.Text = "Internal";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(7, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(130, 23);
            this.label17.TabIndex = 38;
            this.label17.Text = "% of max";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMainModulatorPulseEnergyControl
            // 
            this.txtMainModulatorPulseEnergyControl.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainModulatorPulseEnergyControl.Location = new System.Drawing.Point(143, 97);
            this.txtMainModulatorPulseEnergyControl.Name = "txtMainModulatorPulseEnergyControl";
            this.txtMainModulatorPulseEnergyControl.Size = new System.Drawing.Size(121, 23);
            this.txtMainModulatorPulseEnergyControl.TabIndex = 37;
            // 
            // btnMainModulatorPulseEnergyControlSet
            // 
            this.btnMainModulatorPulseEnergyControlSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainModulatorPulseEnergyControlSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainModulatorPulseEnergyControlSet.Location = new System.Drawing.Point(270, 97);
            this.btnMainModulatorPulseEnergyControlSet.Name = "btnMainModulatorPulseEnergyControlSet";
            this.btnMainModulatorPulseEnergyControlSet.Size = new System.Drawing.Size(121, 25);
            this.btnMainModulatorPulseEnergyControlSet.TabIndex = 36;
            this.btnMainModulatorPulseEnergyControlSet.Text = "Set";
            this.btnMainModulatorPulseEnergyControlSet.UseVisualStyleBackColor = false;
            this.btnMainModulatorPulseEnergyControlSet.Click += new System.EventHandler(this.btnMainModulatorPulseEnergyControlSet_Click);
            // 
            // txtMainModulatorPulseEnergyControlReadOnly
            // 
            this.txtMainModulatorPulseEnergyControlReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainModulatorPulseEnergyControlReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainModulatorPulseEnergyControlReadOnly.Location = new System.Drawing.Point(143, 68);
            this.txtMainModulatorPulseEnergyControlReadOnly.Name = "txtMainModulatorPulseEnergyControlReadOnly";
            this.txtMainModulatorPulseEnergyControlReadOnly.ReadOnly = true;
            this.txtMainModulatorPulseEnergyControlReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainModulatorPulseEnergyControlReadOnly.TabIndex = 39;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.panelMainAttenuatorPowerControlClosedLoopLight);
            this.panel10.Controls.Add(this.panelMainAttenuatorPowerControlOpenLoopLight);
            this.panel10.Controls.Add(this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly);
            this.panel10.Controls.Add(this.btnMainAttenuatorPowerControlAttenuatorPowerSet);
            this.panel10.Controls.Add(this.label40);
            this.panel10.Controls.Add(this.txtMainAttenuatorPowerControlAttenuatorPower);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Controls.Add(this.label6);
            this.panel10.Controls.Add(this.label14);
            this.panel10.Controls.Add(this.label13);
            this.panel10.Controls.Add(this.txtMainAttenuatorPowerControlTransmission);
            this.panel10.Controls.Add(this.txtMainAttenuatorPowerControlTransmissionReadOnly);
            this.panel10.Controls.Add(this.btnMainAttenuatorPowerControlTransmissionSet);
            this.panel10.Location = new System.Drawing.Point(3, 495);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(862, 135);
            this.panel10.TabIndex = 477;
            // 
            // panelMainAttenuatorPowerControlClosedLoopLight
            // 
            this.panelMainAttenuatorPowerControlClosedLoopLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainAttenuatorPowerControlClosedLoopLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainAttenuatorPowerControlClosedLoopLight.Location = new System.Drawing.Point(453, 25);
            this.panelMainAttenuatorPowerControlClosedLoopLight.Name = "panelMainAttenuatorPowerControlClosedLoopLight";
            this.panelMainAttenuatorPowerControlClosedLoopLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainAttenuatorPowerControlClosedLoopLight.TabIndex = 37;
            // 
            // panelMainAttenuatorPowerControlOpenLoopLight
            // 
            this.panelMainAttenuatorPowerControlOpenLoopLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainAttenuatorPowerControlOpenLoopLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainAttenuatorPowerControlOpenLoopLight.Location = new System.Drawing.Point(6, 25);
            this.panelMainAttenuatorPowerControlOpenLoopLight.Name = "panelMainAttenuatorPowerControlOpenLoopLight";
            this.panelMainAttenuatorPowerControlOpenLoopLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainAttenuatorPowerControlOpenLoopLight.TabIndex = 36;
            // 
            // txtMainAttenuatorPowerControlAttenuatorPowerReadOnly
            // 
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.Location = new System.Drawing.Point(734, 67);
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.Name = "txtMainAttenuatorPowerControlAttenuatorPowerReadOnly";
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.ReadOnly = true;
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainAttenuatorPowerControlAttenuatorPowerReadOnly.TabIndex = 35;
            // 
            // btnMainAttenuatorPowerControlAttenuatorPowerSet
            // 
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.ForeColor = System.Drawing.Color.Black;
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.Location = new System.Drawing.Point(734, 96);
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.Name = "btnMainAttenuatorPowerControlAttenuatorPowerSet";
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.Size = new System.Drawing.Size(121, 25);
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.TabIndex = 33;
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.Text = "Set";
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.UseVisualStyleBackColor = false;
            this.btnMainAttenuatorPowerControlAttenuatorPowerSet.Click += new System.EventHandler(this.btnMainAttenuatorPowerControlAttenuatorPowerSet_Click);
            // 
            // label40
            // 
            this.label40.AutoEllipsis = true;
            this.label40.BackColor = System.Drawing.Color.Gainsboro;
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(0, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(860, 22);
            this.label40.TabIndex = 9;
            this.label40.Text = "■ Attenuator Power Control";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMainAttenuatorPowerControlAttenuatorPower
            // 
            this.txtMainAttenuatorPowerControlAttenuatorPower.BackColor = System.Drawing.SystemColors.Window;
            this.txtMainAttenuatorPowerControlAttenuatorPower.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainAttenuatorPowerControlAttenuatorPower.Location = new System.Drawing.Point(598, 97);
            this.txtMainAttenuatorPowerControlAttenuatorPower.Name = "txtMainAttenuatorPowerControlAttenuatorPower";
            this.txtMainAttenuatorPowerControlAttenuatorPower.Size = new System.Drawing.Size(130, 23);
            this.txtMainAttenuatorPowerControlAttenuatorPower.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(453, 67);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(173, 23);
            this.label15.TabIndex = 32;
            this.label15.Text = "Attenuator Power (W)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(50, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 33);
            this.label6.TabIndex = 11;
            this.label6.Text = "Open Loop";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(496, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(130, 33);
            this.label14.TabIndex = 31;
            this.label14.Text = "Closed Loop";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(6, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(173, 24);
            this.label13.TabIndex = 28;
            this.label13.Text = "Transmission (%)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMainAttenuatorPowerControlTransmission
            // 
            this.txtMainAttenuatorPowerControlTransmission.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainAttenuatorPowerControlTransmission.Location = new System.Drawing.Point(143, 98);
            this.txtMainAttenuatorPowerControlTransmission.Name = "txtMainAttenuatorPowerControlTransmission";
            this.txtMainAttenuatorPowerControlTransmission.Size = new System.Drawing.Size(121, 23);
            this.txtMainAttenuatorPowerControlTransmission.TabIndex = 28;
            // 
            // txtMainAttenuatorPowerControlTransmissionReadOnly
            // 
            this.txtMainAttenuatorPowerControlTransmissionReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainAttenuatorPowerControlTransmissionReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainAttenuatorPowerControlTransmissionReadOnly.Location = new System.Drawing.Point(270, 68);
            this.txtMainAttenuatorPowerControlTransmissionReadOnly.Name = "txtMainAttenuatorPowerControlTransmissionReadOnly";
            this.txtMainAttenuatorPowerControlTransmissionReadOnly.ReadOnly = true;
            this.txtMainAttenuatorPowerControlTransmissionReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainAttenuatorPowerControlTransmissionReadOnly.TabIndex = 29;
            // 
            // btnMainAttenuatorPowerControlTransmissionSet
            // 
            this.btnMainAttenuatorPowerControlTransmissionSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainAttenuatorPowerControlTransmissionSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainAttenuatorPowerControlTransmissionSet.ForeColor = System.Drawing.Color.Black;
            this.btnMainAttenuatorPowerControlTransmissionSet.Location = new System.Drawing.Point(270, 97);
            this.btnMainAttenuatorPowerControlTransmissionSet.Name = "btnMainAttenuatorPowerControlTransmissionSet";
            this.btnMainAttenuatorPowerControlTransmissionSet.Size = new System.Drawing.Size(121, 25);
            this.btnMainAttenuatorPowerControlTransmissionSet.TabIndex = 28;
            this.btnMainAttenuatorPowerControlTransmissionSet.Text = "Set";
            this.btnMainAttenuatorPowerControlTransmissionSet.UseVisualStyleBackColor = false;
            this.btnMainAttenuatorPowerControlTransmissionSet.Click += new System.EventHandler(this.btnMainAttenuatorPowerControlTransmissionSet_Click);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txtMainTimingAndFrequencyBurstReadOnly);
            this.panel9.Controls.Add(this.label35);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Controls.Add(this.btnMainTimingAndFrequencyPulseModeSet);
            this.panel9.Controls.Add(this.txtMainTimingAndFrequencyBurst);
            this.panel9.Controls.Add(this.btnMainTimingAndFrequencyAmplifierSet);
            this.panel9.Controls.Add(this.txtMainTimingAndFrequencyPulseModeReadOnly);
            this.panel9.Controls.Add(this.btnMainTimingAndFrequencyDividerSet);
            this.panel9.Controls.Add(this.cbxtMainTimingAndFrequencyPulseMode);
            this.panel9.Controls.Add(this.btnMainTimingAndFrequencyBurstSet);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.cbxtMainTimingAndFrequencyAmplifier);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.txtMainTimingAndFrequencyOutputReadOnly);
            this.panel9.Controls.Add(this.label10);
            this.panel9.Controls.Add(this.txtMainTimingAndFrequencyDividerReadOnly);
            this.panel9.Controls.Add(this.txtMainTimingAndFrequencyAmplifierReadOnly);
            this.panel9.Controls.Add(this.txtMainTimingAndFrequencyDivider);
            this.panel9.Location = new System.Drawing.Point(3, 312);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(862, 177);
            this.panel9.TabIndex = 476;
            // 
            // txtMainTimingAndFrequencyBurstReadOnly
            // 
            this.txtMainTimingAndFrequencyBurstReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainTimingAndFrequencyBurstReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainTimingAndFrequencyBurstReadOnly.Location = new System.Drawing.Point(598, 146);
            this.txtMainTimingAndFrequencyBurstReadOnly.Name = "txtMainTimingAndFrequencyBurstReadOnly";
            this.txtMainTimingAndFrequencyBurstReadOnly.ReadOnly = true;
            this.txtMainTimingAndFrequencyBurstReadOnly.Size = new System.Drawing.Size(130, 23);
            this.txtMainTimingAndFrequencyBurstReadOnly.TabIndex = 27;
            // 
            // label35
            // 
            this.label35.AutoEllipsis = true;
            this.label35.BackColor = System.Drawing.Color.Gainsboro;
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(860, 22);
            this.label35.TabIndex = 9;
            this.label35.Text = "■ Timing and Frequency";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(598, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 23);
            this.label12.TabIndex = 26;
            this.label12.Text = "Burst";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMainTimingAndFrequencyPulseModeSet
            // 
            this.btnMainTimingAndFrequencyPulseModeSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainTimingAndFrequencyPulseModeSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainTimingAndFrequencyPulseModeSet.ForeColor = System.Drawing.Color.Black;
            this.btnMainTimingAndFrequencyPulseModeSet.Location = new System.Drawing.Point(734, 29);
            this.btnMainTimingAndFrequencyPulseModeSet.Name = "btnMainTimingAndFrequencyPulseModeSet";
            this.btnMainTimingAndFrequencyPulseModeSet.Size = new System.Drawing.Size(121, 25);
            this.btnMainTimingAndFrequencyPulseModeSet.TabIndex = 14;
            this.btnMainTimingAndFrequencyPulseModeSet.Text = "Set";
            this.btnMainTimingAndFrequencyPulseModeSet.UseVisualStyleBackColor = false;
            this.btnMainTimingAndFrequencyPulseModeSet.Click += new System.EventHandler(this.btnMainTimingAndFrequencyPulseModeSet_Click);
            // 
            // txtMainTimingAndFrequencyBurst
            // 
            this.txtMainTimingAndFrequencyBurst.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainTimingAndFrequencyBurst.Location = new System.Drawing.Point(734, 117);
            this.txtMainTimingAndFrequencyBurst.Name = "txtMainTimingAndFrequencyBurst";
            this.txtMainTimingAndFrequencyBurst.Size = new System.Drawing.Size(121, 21);
            this.txtMainTimingAndFrequencyBurst.TabIndex = 25;
            // 
            // btnMainTimingAndFrequencyAmplifierSet
            // 
            this.btnMainTimingAndFrequencyAmplifierSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainTimingAndFrequencyAmplifierSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainTimingAndFrequencyAmplifierSet.ForeColor = System.Drawing.Color.Black;
            this.btnMainTimingAndFrequencyAmplifierSet.Location = new System.Drawing.Point(270, 30);
            this.btnMainTimingAndFrequencyAmplifierSet.Name = "btnMainTimingAndFrequencyAmplifierSet";
            this.btnMainTimingAndFrequencyAmplifierSet.Size = new System.Drawing.Size(121, 25);
            this.btnMainTimingAndFrequencyAmplifierSet.TabIndex = 11;
            this.btnMainTimingAndFrequencyAmplifierSet.Text = "Set";
            this.btnMainTimingAndFrequencyAmplifierSet.UseVisualStyleBackColor = false;
            this.btnMainTimingAndFrequencyAmplifierSet.Click += new System.EventHandler(this.btnMainTimingAndFrequencyAmplifierSet_Click);
            // 
            // txtMainTimingAndFrequencyPulseModeReadOnly
            // 
            this.txtMainTimingAndFrequencyPulseModeReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainTimingAndFrequencyPulseModeReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainTimingAndFrequencyPulseModeReadOnly.Location = new System.Drawing.Point(598, 88);
            this.txtMainTimingAndFrequencyPulseModeReadOnly.Name = "txtMainTimingAndFrequencyPulseModeReadOnly";
            this.txtMainTimingAndFrequencyPulseModeReadOnly.ReadOnly = true;
            this.txtMainTimingAndFrequencyPulseModeReadOnly.Size = new System.Drawing.Size(257, 23);
            this.txtMainTimingAndFrequencyPulseModeReadOnly.TabIndex = 24;
            // 
            // btnMainTimingAndFrequencyDividerSet
            // 
            this.btnMainTimingAndFrequencyDividerSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainTimingAndFrequencyDividerSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainTimingAndFrequencyDividerSet.ForeColor = System.Drawing.Color.Black;
            this.btnMainTimingAndFrequencyDividerSet.Location = new System.Drawing.Point(270, 88);
            this.btnMainTimingAndFrequencyDividerSet.Name = "btnMainTimingAndFrequencyDividerSet";
            this.btnMainTimingAndFrequencyDividerSet.Size = new System.Drawing.Size(121, 25);
            this.btnMainTimingAndFrequencyDividerSet.TabIndex = 12;
            this.btnMainTimingAndFrequencyDividerSet.Text = "Set";
            this.btnMainTimingAndFrequencyDividerSet.UseVisualStyleBackColor = false;
            this.btnMainTimingAndFrequencyDividerSet.Click += new System.EventHandler(this.btnMainTimingAndFrequencyDividerSet_Click);
            // 
            // cbxtMainTimingAndFrequencyPulseMode
            // 
            this.cbxtMainTimingAndFrequencyPulseMode.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbxtMainTimingAndFrequencyPulseMode.FormattingEnabled = true;
            this.cbxtMainTimingAndFrequencyPulseMode.Location = new System.Drawing.Point(598, 59);
            this.cbxtMainTimingAndFrequencyPulseMode.Name = "cbxtMainTimingAndFrequencyPulseMode";
            this.cbxtMainTimingAndFrequencyPulseMode.Size = new System.Drawing.Size(257, 23);
            this.cbxtMainTimingAndFrequencyPulseMode.TabIndex = 23;
            // 
            // btnMainTimingAndFrequencyBurstSet
            // 
            this.btnMainTimingAndFrequencyBurstSet.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainTimingAndFrequencyBurstSet.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainTimingAndFrequencyBurstSet.ForeColor = System.Drawing.Color.Black;
            this.btnMainTimingAndFrequencyBurstSet.Location = new System.Drawing.Point(734, 144);
            this.btnMainTimingAndFrequencyBurstSet.Name = "btnMainTimingAndFrequencyBurstSet";
            this.btnMainTimingAndFrequencyBurstSet.Size = new System.Drawing.Size(121, 25);
            this.btnMainTimingAndFrequencyBurstSet.TabIndex = 13;
            this.btnMainTimingAndFrequencyBurstSet.Text = "Set";
            this.btnMainTimingAndFrequencyBurstSet.UseVisualStyleBackColor = false;
            this.btnMainTimingAndFrequencyBurstSet.Click += new System.EventHandler(this.btnMainTimingAndFrequencyBurstSet_Click);
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(598, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 23);
            this.label11.TabIndex = 22;
            this.label11.Text = "Pulse Mode";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(7, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 52);
            this.label8.TabIndex = 11;
            this.label8.Text = "Amplifier(KHz)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtMainTimingAndFrequencyAmplifier
            // 
            this.cbxtMainTimingAndFrequencyAmplifier.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbxtMainTimingAndFrequencyAmplifier.FormattingEnabled = true;
            this.cbxtMainTimingAndFrequencyAmplifier.Location = new System.Drawing.Point(143, 30);
            this.cbxtMainTimingAndFrequencyAmplifier.Name = "cbxtMainTimingAndFrequencyAmplifier";
            this.cbxtMainTimingAndFrequencyAmplifier.Size = new System.Drawing.Size(121, 23);
            this.cbxtMainTimingAndFrequencyAmplifier.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(7, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 52);
            this.label9.TabIndex = 15;
            this.label9.Text = "Divider";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMainTimingAndFrequencyOutputReadOnly
            // 
            this.txtMainTimingAndFrequencyOutputReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainTimingAndFrequencyOutputReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainTimingAndFrequencyOutputReadOnly.Location = new System.Drawing.Point(143, 146);
            this.txtMainTimingAndFrequencyOutputReadOnly.Name = "txtMainTimingAndFrequencyOutputReadOnly";
            this.txtMainTimingAndFrequencyOutputReadOnly.ReadOnly = true;
            this.txtMainTimingAndFrequencyOutputReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainTimingAndFrequencyOutputReadOnly.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(7, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 23);
            this.label10.TabIndex = 16;
            this.label10.Text = "OutPut (KHz)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMainTimingAndFrequencyDividerReadOnly
            // 
            this.txtMainTimingAndFrequencyDividerReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainTimingAndFrequencyDividerReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainTimingAndFrequencyDividerReadOnly.Location = new System.Drawing.Point(143, 117);
            this.txtMainTimingAndFrequencyDividerReadOnly.Name = "txtMainTimingAndFrequencyDividerReadOnly";
            this.txtMainTimingAndFrequencyDividerReadOnly.ReadOnly = true;
            this.txtMainTimingAndFrequencyDividerReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainTimingAndFrequencyDividerReadOnly.TabIndex = 19;
            // 
            // txtMainTimingAndFrequencyAmplifierReadOnly
            // 
            this.txtMainTimingAndFrequencyAmplifierReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMainTimingAndFrequencyAmplifierReadOnly.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainTimingAndFrequencyAmplifierReadOnly.Location = new System.Drawing.Point(143, 59);
            this.txtMainTimingAndFrequencyAmplifierReadOnly.Name = "txtMainTimingAndFrequencyAmplifierReadOnly";
            this.txtMainTimingAndFrequencyAmplifierReadOnly.ReadOnly = true;
            this.txtMainTimingAndFrequencyAmplifierReadOnly.Size = new System.Drawing.Size(121, 23);
            this.txtMainTimingAndFrequencyAmplifierReadOnly.TabIndex = 17;
            // 
            // txtMainTimingAndFrequencyDivider
            // 
            this.txtMainTimingAndFrequencyDivider.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMainTimingAndFrequencyDivider.Location = new System.Drawing.Point(143, 88);
            this.txtMainTimingAndFrequencyDivider.Name = "txtMainTimingAndFrequencyDivider";
            this.txtMainTimingAndFrequencyDivider.Size = new System.Drawing.Size(121, 23);
            this.txtMainTimingAndFrequencyDivider.TabIndex = 18;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.panelMainSystemStatusLight);
            this.panel8.Controls.Add(this.btnMainSystemStatusStop);
            this.panel8.Controls.Add(this.label39);
            this.panel8.Controls.Add(this.btnMainSystemStatusStart);
            this.panel8.Controls.Add(this.lblMainSystemStatus);
            this.panel8.Location = new System.Drawing.Point(3, 242);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(862, 64);
            this.panel8.TabIndex = 473;
            // 
            // panelMainSystemStatusLight
            // 
            this.panelMainSystemStatusLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainSystemStatusLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainSystemStatusLight.Location = new System.Drawing.Point(6, 25);
            this.panelMainSystemStatusLight.Name = "panelMainSystemStatusLight";
            this.panelMainSystemStatusLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainSystemStatusLight.TabIndex = 14;
            // 
            // btnMainSystemStatusStop
            // 
            this.btnMainSystemStatusStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainSystemStatusStop.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainSystemStatusStop.ForeColor = System.Drawing.Color.Black;
            this.btnMainSystemStatusStop.Location = new System.Drawing.Point(607, 25);
            this.btnMainSystemStatusStop.Name = "btnMainSystemStatusStop";
            this.btnMainSystemStatusStop.Size = new System.Drawing.Size(121, 33);
            this.btnMainSystemStatusStop.TabIndex = 10;
            this.btnMainSystemStatusStop.Text = "Stop";
            this.btnMainSystemStatusStop.UseVisualStyleBackColor = false;
            this.btnMainSystemStatusStop.Click += new System.EventHandler(this.btnMainSystemStatusStop_Click);
            // 
            // label39
            // 
            this.label39.AutoEllipsis = true;
            this.label39.BackColor = System.Drawing.Color.Gainsboro;
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(0, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(860, 22);
            this.label39.TabIndex = 9;
            this.label39.Text = "■ System Status";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMainSystemStatusStart
            // 
            this.btnMainSystemStatusStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainSystemStatusStart.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainSystemStatusStart.ForeColor = System.Drawing.Color.Black;
            this.btnMainSystemStatusStart.Location = new System.Drawing.Point(734, 25);
            this.btnMainSystemStatusStart.Name = "btnMainSystemStatusStart";
            this.btnMainSystemStatusStart.Size = new System.Drawing.Size(121, 33);
            this.btnMainSystemStatusStart.TabIndex = 8;
            this.btnMainSystemStatusStart.Text = "Start";
            this.btnMainSystemStatusStart.UseVisualStyleBackColor = false;
            this.btnMainSystemStatusStart.Click += new System.EventHandler(this.btnMainSystemStatusStart_Click);
            // 
            // lblMainSystemStatus
            // 
            this.lblMainSystemStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMainSystemStatus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMainSystemStatus.Location = new System.Drawing.Point(50, 25);
            this.lblMainSystemStatus.Name = "lblMainSystemStatus";
            this.lblMainSystemStatus.Size = new System.Drawing.Size(130, 33);
            this.lblMainSystemStatus.TabIndex = 9;
            this.lblMainSystemStatus.Text = "Off";
            this.lblMainSystemStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.panelMainSystemFaultsLight);
            this.panel7.Controls.Add(this.btnMainSystemFaultsClear);
            this.panel7.Controls.Add(this.lblMainSystemFaults);
            this.panel7.Controls.Add(this.label38);
            this.panel7.Location = new System.Drawing.Point(3, 172);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(862, 64);
            this.panel7.TabIndex = 471;
            // 
            // panelMainSystemFaultsLight
            // 
            this.panelMainSystemFaultsLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainSystemFaultsLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainSystemFaultsLight.Location = new System.Drawing.Point(6, 25);
            this.panelMainSystemFaultsLight.Name = "panelMainSystemFaultsLight";
            this.panelMainSystemFaultsLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainSystemFaultsLight.TabIndex = 13;
            // 
            // btnMainSystemFaultsClear
            // 
            this.btnMainSystemFaultsClear.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainSystemFaultsClear.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainSystemFaultsClear.ForeColor = System.Drawing.Color.Black;
            this.btnMainSystemFaultsClear.Location = new System.Drawing.Point(734, 25);
            this.btnMainSystemFaultsClear.Name = "btnMainSystemFaultsClear";
            this.btnMainSystemFaultsClear.Size = new System.Drawing.Size(121, 33);
            this.btnMainSystemFaultsClear.TabIndex = 6;
            this.btnMainSystemFaultsClear.Text = "Clear";
            this.btnMainSystemFaultsClear.UseVisualStyleBackColor = false;
            this.btnMainSystemFaultsClear.Click += new System.EventHandler(this.btnMainSystemFaultsClear_Click);
            // 
            // lblMainSystemFaults
            // 
            this.lblMainSystemFaults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMainSystemFaults.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMainSystemFaults.Location = new System.Drawing.Point(50, 25);
            this.lblMainSystemFaults.Name = "lblMainSystemFaults";
            this.lblMainSystemFaults.Size = new System.Drawing.Size(130, 33);
            this.lblMainSystemFaults.TabIndex = 7;
            this.lblMainSystemFaults.Text = "SYSTEM OK";
            this.lblMainSystemFaults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.AutoEllipsis = true;
            this.label38.BackColor = System.Drawing.Color.Gainsboro;
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(860, 22);
            this.label38.TabIndex = 9;
            this.label38.Text = "■ System Faults";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panelMainShutterLight);
            this.panel6.Controls.Add(this.lblMainShutter);
            this.panel6.Controls.Add(this.btnMainShutterClose);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Location = new System.Drawing.Point(280, 103);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(585, 63);
            this.panel6.TabIndex = 474;
            // 
            // panelMainShutterLight
            // 
            this.panelMainShutterLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainShutterLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainShutterLight.Location = new System.Drawing.Point(6, 25);
            this.panelMainShutterLight.Name = "panelMainShutterLight";
            this.panelMainShutterLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainShutterLight.TabIndex = 11;
            // 
            // lblMainShutter
            // 
            this.lblMainShutter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMainShutter.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMainShutter.Location = new System.Drawing.Point(50, 25);
            this.lblMainShutter.Name = "lblMainShutter";
            this.lblMainShutter.Size = new System.Drawing.Size(130, 33);
            this.lblMainShutter.TabIndex = 5;
            this.lblMainShutter.Text = "Closed";
            this.lblMainShutter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMainShutterClose
            // 
            this.btnMainShutterClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainShutterClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainShutterClose.ForeColor = System.Drawing.Color.Black;
            this.btnMainShutterClose.Location = new System.Drawing.Point(457, 25);
            this.btnMainShutterClose.Name = "btnMainShutterClose";
            this.btnMainShutterClose.Size = new System.Drawing.Size(121, 33);
            this.btnMainShutterClose.TabIndex = 3;
            this.btnMainShutterClose.Text = "Open";
            this.btnMainShutterClose.UseVisualStyleBackColor = false;
            this.btnMainShutterClose.Click += new System.EventHandler(this.btnMainShutterClose_Click);
            // 
            // label37
            // 
            this.label37.AutoEllipsis = true;
            this.label37.BackColor = System.Drawing.Color.Gainsboro;
            this.label37.Dock = System.Windows.Forms.DockStyle.Top;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(0, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(583, 22);
            this.label37.TabIndex = 9;
            this.label37.Text = "■ Shutter";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panelMainKeySwitchLight);
            this.panel5.Controls.Add(this.lblMainKeySwitch);
            this.panel5.Controls.Add(this.label36);
            this.panel5.Location = new System.Drawing.Point(3, 103);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(271, 63);
            this.panel5.TabIndex = 472;
            // 
            // panelMainKeySwitchLight
            // 
            this.panelMainKeySwitchLight.BackColor = System.Drawing.Color.Lime;
            this.panelMainKeySwitchLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainKeySwitchLight.Location = new System.Drawing.Point(6, 25);
            this.panelMainKeySwitchLight.Name = "panelMainKeySwitchLight";
            this.panelMainKeySwitchLight.Size = new System.Drawing.Size(37, 33);
            this.panelMainKeySwitchLight.TabIndex = 12;
            // 
            // lblMainKeySwitch
            // 
            this.lblMainKeySwitch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMainKeySwitch.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMainKeySwitch.Location = new System.Drawing.Point(49, 25);
            this.lblMainKeySwitch.Name = "lblMainKeySwitch";
            this.lblMainKeySwitch.Size = new System.Drawing.Size(130, 33);
            this.lblMainKeySwitch.TabIndex = 11;
            this.lblMainKeySwitch.Text = "Key on";
            this.lblMainKeySwitch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.AutoEllipsis = true;
            this.label36.BackColor = System.Drawing.Color.Gainsboro;
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(269, 22);
            this.label36.TabIndex = 9;
            this.label36.Text = "■ Key-Switch";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panelConnectionToLaserLight);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.btnMainConnectionToLaserDisConnection);
            this.panel4.Controls.Add(this.lblLoadingLoaderTransfer);
            this.panel4.Location = new System.Drawing.Point(3, 33);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(862, 64);
            this.panel4.TabIndex = 470;
            // 
            // panelConnectionToLaserLight
            // 
            this.panelConnectionToLaserLight.BackColor = System.Drawing.Color.Lime;
            this.panelConnectionToLaserLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelConnectionToLaserLight.Location = new System.Drawing.Point(6, 25);
            this.panelConnectionToLaserLight.Name = "panelConnectionToLaserLight";
            this.panelConnectionToLaserLight.Size = new System.Drawing.Size(37, 33);
            this.panelConnectionToLaserLight.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(49, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "HyperNX 1064-100";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMainConnectionToLaserDisConnection
            // 
            this.btnMainConnectionToLaserDisConnection.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainConnectionToLaserDisConnection.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainConnectionToLaserDisConnection.Location = new System.Drawing.Point(734, 25);
            this.btnMainConnectionToLaserDisConnection.Name = "btnMainConnectionToLaserDisConnection";
            this.btnMainConnectionToLaserDisConnection.Size = new System.Drawing.Size(121, 33);
            this.btnMainConnectionToLaserDisConnection.TabIndex = 2;
            this.btnMainConnectionToLaserDisConnection.Text = "Connection";
            this.btnMainConnectionToLaserDisConnection.UseVisualStyleBackColor = false;
            this.btnMainConnectionToLaserDisConnection.Click += new System.EventHandler(this.btnMainConnectionToLaserDisConnection_Click);
            // 
            // lblLoadingLoaderTransfer
            // 
            this.lblLoadingLoaderTransfer.AutoEllipsis = true;
            this.lblLoadingLoaderTransfer.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoadingLoaderTransfer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoadingLoaderTransfer.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoadingLoaderTransfer.ForeColor = System.Drawing.Color.Black;
            this.lblLoadingLoaderTransfer.Location = new System.Drawing.Point(0, 0);
            this.lblLoadingLoaderTransfer.Name = "lblLoadingLoaderTransfer";
            this.lblLoadingLoaderTransfer.Size = new System.Drawing.Size(860, 22);
            this.lblLoadingLoaderTransfer.TabIndex = 9;
            this.lblLoadingLoaderTransfer.Text = "■ Connection to Laser";
            this.lblLoadingLoaderTransfer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMainLock
            // 
            this.btnMainLock.BackColor = System.Drawing.SystemColors.Control;
            this.btnMainLock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMainLock.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMainLock.ForeColor = System.Drawing.Color.Black;
            this.btnMainLock.Location = new System.Drawing.Point(121, 5);
            this.btnMainLock.Name = "btnMainLock";
            this.btnMainLock.Size = new System.Drawing.Size(77, 20);
            this.btnMainLock.TabIndex = 464;
            this.btnMainLock.Text = "LOCK";
            this.btnMainLock.UseVisualStyleBackColor = false;
            // 
            // txtMainLock
            // 
            this.txtMainLock.Font = new System.Drawing.Font("맑은 고딕", 6F);
            this.txtMainLock.Location = new System.Drawing.Point(21, 7);
            this.txtMainLock.Name = "txtMainLock";
            this.txtMainLock.Size = new System.Drawing.Size(94, 18);
            this.txtMainLock.TabIndex = 465;
            // 
            // lblLaserMain
            // 
            this.lblLaserMain.AutoEllipsis = true;
            this.lblLaserMain.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLaserMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLaserMain.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLaserMain.ForeColor = System.Drawing.Color.Black;
            this.lblLaserMain.Location = new System.Drawing.Point(0, 0);
            this.lblLaserMain.Name = "lblLaserMain";
            this.lblLaserMain.Size = new System.Drawing.Size(868, 30);
            this.lblLaserMain.TabIndex = 9;
            this.lblLaserMain.Text = "■ Main";
            this.lblLaserMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel18);
            this.panel2.Controls.Add(this.panel17);
            this.panel2.Controls.Add(this.panel16);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel14);
            this.panel2.Controls.Add(this.panel13);
            this.panel2.Controls.Add(this.label56);
            this.panel2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel2.Location = new System.Drawing.Point(870, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 860);
            this.panel2.TabIndex = 465;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.panelMonitoringH3AmbientAirLight);
            this.panel18.Controls.Add(this.txtMonitoringH3Dewpoint);
            this.panel18.Controls.Add(this.label48);
            this.panel18.Controls.Add(this.txtMonitoringH3Humidity);
            this.panel18.Controls.Add(this.label30);
            this.panel18.Controls.Add(this.label31);
            this.panel18.Controls.Add(this.label32);
            this.panel18.Controls.Add(this.txtMonitoringH3Temp);
            this.panel18.Location = new System.Drawing.Point(5, 712);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(860, 129);
            this.panel18.TabIndex = 476;
            // 
            // panelMonitoringH3AmbientAirLight
            // 
            this.panelMonitoringH3AmbientAirLight.BackColor = System.Drawing.Color.Red;
            this.panelMonitoringH3AmbientAirLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMonitoringH3AmbientAirLight.Location = new System.Drawing.Point(7, 44);
            this.panelMonitoringH3AmbientAirLight.Name = "panelMonitoringH3AmbientAirLight";
            this.panelMonitoringH3AmbientAirLight.Size = new System.Drawing.Size(54, 54);
            this.panelMonitoringH3AmbientAirLight.TabIndex = 45;
            // 
            // txtMonitoringH3Dewpoint
            // 
            this.txtMonitoringH3Dewpoint.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH3Dewpoint.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH3Dewpoint.Location = new System.Drawing.Point(715, 59);
            this.txtMonitoringH3Dewpoint.Name = "txtMonitoringH3Dewpoint";
            this.txtMonitoringH3Dewpoint.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH3Dewpoint.TabIndex = 44;
            // 
            // label48
            // 
            this.label48.AutoEllipsis = true;
            this.label48.BackColor = System.Drawing.Color.Gainsboro;
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(0, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(858, 22);
            this.label48.TabIndex = 9;
            this.label48.Text = "■ H3 - Ambient Air";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonitoringH3Humidity
            // 
            this.txtMonitoringH3Humidity.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH3Humidity.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH3Humidity.Location = new System.Drawing.Point(233, 44);
            this.txtMonitoringH3Humidity.Name = "txtMonitoringH3Humidity";
            this.txtMonitoringH3Humidity.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH3Humidity.TabIndex = 40;
            // 
            // label30
            // 
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label30.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(549, 59);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(160, 23);
            this.label30.TabIndex = 43;
            this.label30.Text = "Dewpoint";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label31.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.Location = new System.Drawing.Point(67, 76);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(160, 23);
            this.label31.TabIndex = 41;
            this.label31.Text = "Temp";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label32.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.Location = new System.Drawing.Point(67, 44);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(160, 23);
            this.label32.TabIndex = 39;
            this.label32.Text = "Humidity";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonitoringH3Temp
            // 
            this.txtMonitoringH3Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH3Temp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH3Temp.Location = new System.Drawing.Point(233, 76);
            this.txtMonitoringH3Temp.Name = "txtMonitoringH3Temp";
            this.txtMonitoringH3Temp.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH3Temp.TabIndex = 42;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.panelMonitoringH2HarmonicModuleAirLight);
            this.panel17.Controls.Add(this.txtMonitoringH2Dewpoint);
            this.panel17.Controls.Add(this.label47);
            this.panel17.Controls.Add(this.txtMonitoringH2Humidity);
            this.panel17.Controls.Add(this.label27);
            this.panel17.Controls.Add(this.label28);
            this.panel17.Controls.Add(this.label29);
            this.panel17.Controls.Add(this.txtMonitoringH2Temp);
            this.panel17.Location = new System.Drawing.Point(5, 577);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(860, 129);
            this.panel17.TabIndex = 475;
            // 
            // panelMonitoringH2HarmonicModuleAirLight
            // 
            this.panelMonitoringH2HarmonicModuleAirLight.BackColor = System.Drawing.Color.Red;
            this.panelMonitoringH2HarmonicModuleAirLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMonitoringH2HarmonicModuleAirLight.Location = new System.Drawing.Point(7, 44);
            this.panelMonitoringH2HarmonicModuleAirLight.Name = "panelMonitoringH2HarmonicModuleAirLight";
            this.panelMonitoringH2HarmonicModuleAirLight.Size = new System.Drawing.Size(54, 54);
            this.panelMonitoringH2HarmonicModuleAirLight.TabIndex = 39;
            // 
            // txtMonitoringH2Dewpoint
            // 
            this.txtMonitoringH2Dewpoint.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH2Dewpoint.Enabled = false;
            this.txtMonitoringH2Dewpoint.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH2Dewpoint.Location = new System.Drawing.Point(715, 58);
            this.txtMonitoringH2Dewpoint.Name = "txtMonitoringH2Dewpoint";
            this.txtMonitoringH2Dewpoint.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH2Dewpoint.TabIndex = 38;
            // 
            // label47
            // 
            this.label47.AutoEllipsis = true;
            this.label47.BackColor = System.Drawing.Color.Gainsboro;
            this.label47.Dock = System.Windows.Forms.DockStyle.Top;
            this.label47.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(858, 22);
            this.label47.TabIndex = 9;
            this.label47.Text = "■ H2 - Harmonic Module Air";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonitoringH2Humidity
            // 
            this.txtMonitoringH2Humidity.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH2Humidity.Enabled = false;
            this.txtMonitoringH2Humidity.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH2Humidity.Location = new System.Drawing.Point(233, 45);
            this.txtMonitoringH2Humidity.Name = "txtMonitoringH2Humidity";
            this.txtMonitoringH2Humidity.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH2Humidity.TabIndex = 34;
            // 
            // label27
            // 
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(549, 59);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(160, 23);
            this.label27.TabIndex = 37;
            this.label27.Text = "Dewpoint";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(67, 76);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(160, 23);
            this.label28.TabIndex = 35;
            this.label28.Text = "Temp";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(67, 44);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(160, 23);
            this.label29.TabIndex = 33;
            this.label29.Text = "Humidity";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonitoringH2Temp
            // 
            this.txtMonitoringH2Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH2Temp.Enabled = false;
            this.txtMonitoringH2Temp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH2Temp.Location = new System.Drawing.Point(233, 76);
            this.txtMonitoringH2Temp.Name = "txtMonitoringH2Temp";
            this.txtMonitoringH2Temp.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH2Temp.TabIndex = 36;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.panelMonitoringH1AmplifierLight);
            this.panel16.Controls.Add(this.txtMonitoringH1AmplifierDewpoint);
            this.panel16.Controls.Add(this.label46);
            this.panel16.Controls.Add(this.label26);
            this.panel16.Controls.Add(this.txtMonitoringH1AmplifierHumidity);
            this.panel16.Controls.Add(this.txtMonitoringH1AmplifierTemp);
            this.panel16.Controls.Add(this.label24);
            this.panel16.Controls.Add(this.label25);
            this.panel16.Location = new System.Drawing.Point(5, 441);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(860, 129);
            this.panel16.TabIndex = 474;
            // 
            // panelMonitoringH1AmplifierLight
            // 
            this.panelMonitoringH1AmplifierLight.BackColor = System.Drawing.Color.Red;
            this.panelMonitoringH1AmplifierLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMonitoringH1AmplifierLight.Location = new System.Drawing.Point(7, 44);
            this.panelMonitoringH1AmplifierLight.Name = "panelMonitoringH1AmplifierLight";
            this.panelMonitoringH1AmplifierLight.Size = new System.Drawing.Size(54, 54);
            this.panelMonitoringH1AmplifierLight.TabIndex = 33;
            // 
            // txtMonitoringH1AmplifierDewpoint
            // 
            this.txtMonitoringH1AmplifierDewpoint.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH1AmplifierDewpoint.Enabled = false;
            this.txtMonitoringH1AmplifierDewpoint.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH1AmplifierDewpoint.Location = new System.Drawing.Point(715, 59);
            this.txtMonitoringH1AmplifierDewpoint.Name = "txtMonitoringH1AmplifierDewpoint";
            this.txtMonitoringH1AmplifierDewpoint.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH1AmplifierDewpoint.TabIndex = 32;
            // 
            // label46
            // 
            this.label46.AutoEllipsis = true;
            this.label46.BackColor = System.Drawing.Color.Gainsboro;
            this.label46.Dock = System.Windows.Forms.DockStyle.Top;
            this.label46.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(0, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(858, 22);
            this.label46.TabIndex = 9;
            this.label46.Text = "■ H1 - Amplifier";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(549, 59);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(160, 23);
            this.label26.TabIndex = 31;
            this.label26.Text = "Dewpoint";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonitoringH1AmplifierHumidity
            // 
            this.txtMonitoringH1AmplifierHumidity.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH1AmplifierHumidity.Enabled = false;
            this.txtMonitoringH1AmplifierHumidity.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH1AmplifierHumidity.Location = new System.Drawing.Point(233, 44);
            this.txtMonitoringH1AmplifierHumidity.Name = "txtMonitoringH1AmplifierHumidity";
            this.txtMonitoringH1AmplifierHumidity.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH1AmplifierHumidity.TabIndex = 28;
            // 
            // txtMonitoringH1AmplifierTemp
            // 
            this.txtMonitoringH1AmplifierTemp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringH1AmplifierTemp.Enabled = false;
            this.txtMonitoringH1AmplifierTemp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringH1AmplifierTemp.Location = new System.Drawing.Point(233, 77);
            this.txtMonitoringH1AmplifierTemp.Name = "txtMonitoringH1AmplifierTemp";
            this.txtMonitoringH1AmplifierTemp.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringH1AmplifierTemp.TabIndex = 30;
            // 
            // label24
            // 
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(67, 44);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(160, 23);
            this.label24.TabIndex = 27;
            this.label24.Text = "Humidity";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(67, 76);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(160, 23);
            this.label25.TabIndex = 29;
            this.label25.Text = "Temp";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.panelMonitoringHousingTemperature2Light);
            this.panel15.Controls.Add(this.txtMonitoringHousingTemperature2Temp);
            this.panel15.Controls.Add(this.label45);
            this.panel15.Controls.Add(this.label23);
            this.panel15.Location = new System.Drawing.Point(5, 311);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(860, 124);
            this.panel15.TabIndex = 473;
            // 
            // panelMonitoringHousingTemperature2Light
            // 
            this.panelMonitoringHousingTemperature2Light.BackColor = System.Drawing.Color.Red;
            this.panelMonitoringHousingTemperature2Light.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMonitoringHousingTemperature2Light.Location = new System.Drawing.Point(7, 44);
            this.panelMonitoringHousingTemperature2Light.Name = "panelMonitoringHousingTemperature2Light";
            this.panelMonitoringHousingTemperature2Light.Size = new System.Drawing.Size(54, 54);
            this.panelMonitoringHousingTemperature2Light.TabIndex = 27;
            // 
            // txtMonitoringHousingTemperature2Temp
            // 
            this.txtMonitoringHousingTemperature2Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringHousingTemperature2Temp.Enabled = false;
            this.txtMonitoringHousingTemperature2Temp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringHousingTemperature2Temp.Location = new System.Drawing.Point(233, 60);
            this.txtMonitoringHousingTemperature2Temp.Name = "txtMonitoringHousingTemperature2Temp";
            this.txtMonitoringHousingTemperature2Temp.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringHousingTemperature2Temp.TabIndex = 26;
            // 
            // label45
            // 
            this.label45.AutoEllipsis = true;
            this.label45.BackColor = System.Drawing.Color.Gainsboro;
            this.label45.Dock = System.Windows.Forms.DockStyle.Top;
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(0, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(858, 22);
            this.label45.TabIndex = 9;
            this.label45.Text = "■ Housing Temperature 2";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(67, 60);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(160, 23);
            this.label23.TabIndex = 25;
            this.label23.Text = "Temp";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.panelMonitoringHousingTemperature1Light);
            this.panel14.Controls.Add(this.txtMonitoringHousingTemperature1Temp);
            this.panel14.Controls.Add(this.label43);
            this.panel14.Controls.Add(this.label22);
            this.panel14.Location = new System.Drawing.Point(5, 172);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(860, 133);
            this.panel14.TabIndex = 472;
            // 
            // panelMonitoringHousingTemperature1Light
            // 
            this.panelMonitoringHousingTemperature1Light.BackColor = System.Drawing.Color.Red;
            this.panelMonitoringHousingTemperature1Light.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMonitoringHousingTemperature1Light.Location = new System.Drawing.Point(7, 44);
            this.panelMonitoringHousingTemperature1Light.Name = "panelMonitoringHousingTemperature1Light";
            this.panelMonitoringHousingTemperature1Light.Size = new System.Drawing.Size(54, 54);
            this.panelMonitoringHousingTemperature1Light.TabIndex = 25;
            // 
            // txtMonitoringHousingTemperature1Temp
            // 
            this.txtMonitoringHousingTemperature1Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringHousingTemperature1Temp.Enabled = false;
            this.txtMonitoringHousingTemperature1Temp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringHousingTemperature1Temp.Location = new System.Drawing.Point(233, 60);
            this.txtMonitoringHousingTemperature1Temp.Name = "txtMonitoringHousingTemperature1Temp";
            this.txtMonitoringHousingTemperature1Temp.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringHousingTemperature1Temp.TabIndex = 24;
            // 
            // label43
            // 
            this.label43.AutoEllipsis = true;
            this.label43.BackColor = System.Drawing.Color.Gainsboro;
            this.label43.Dock = System.Windows.Forms.DockStyle.Top;
            this.label43.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(0, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(858, 22);
            this.label43.TabIndex = 9;
            this.label43.Text = "■ Housing Temperature 1";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(67, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(160, 23);
            this.label22.TabIndex = 23;
            this.label22.Text = "Temp";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.panelMonitoringWaterMonitorLight);
            this.panel13.Controls.Add(this.txtMonitoringWaterMonitorFlow);
            this.panel13.Controls.Add(this.label44);
            this.panel13.Controls.Add(this.label21);
            this.panel13.Controls.Add(this.txtMonitoringWaterMonitorTemp);
            this.panel13.Controls.Add(this.label20);
            this.panel13.Location = new System.Drawing.Point(5, 33);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(860, 133);
            this.panel13.TabIndex = 471;
            // 
            // panelMonitoringWaterMonitorLight
            // 
            this.panelMonitoringWaterMonitorLight.BackColor = System.Drawing.Color.Red;
            this.panelMonitoringWaterMonitorLight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMonitoringWaterMonitorLight.Location = new System.Drawing.Point(7, 44);
            this.panelMonitoringWaterMonitorLight.Name = "panelMonitoringWaterMonitorLight";
            this.panelMonitoringWaterMonitorLight.Size = new System.Drawing.Size(54, 54);
            this.panelMonitoringWaterMonitorLight.TabIndex = 23;
            // 
            // txtMonitoringWaterMonitorFlow
            // 
            this.txtMonitoringWaterMonitorFlow.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringWaterMonitorFlow.Enabled = false;
            this.txtMonitoringWaterMonitorFlow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringWaterMonitorFlow.Location = new System.Drawing.Point(715, 58);
            this.txtMonitoringWaterMonitorFlow.Name = "txtMonitoringWaterMonitorFlow";
            this.txtMonitoringWaterMonitorFlow.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringWaterMonitorFlow.TabIndex = 22;
            // 
            // label44
            // 
            this.label44.AutoEllipsis = true;
            this.label44.BackColor = System.Drawing.Color.Gainsboro;
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(0, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(858, 22);
            this.label44.TabIndex = 9;
            this.label44.Text = "■ Water Monitor";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(549, 58);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(160, 23);
            this.label21.TabIndex = 21;
            this.label21.Text = "Flow";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonitoringWaterMonitorTemp
            // 
            this.txtMonitoringWaterMonitorTemp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoringWaterMonitorTemp.Enabled = false;
            this.txtMonitoringWaterMonitorTemp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMonitoringWaterMonitorTemp.Location = new System.Drawing.Point(233, 58);
            this.txtMonitoringWaterMonitorTemp.Name = "txtMonitoringWaterMonitorTemp";
            this.txtMonitoringWaterMonitorTemp.Size = new System.Drawing.Size(140, 23);
            this.txtMonitoringWaterMonitorTemp.TabIndex = 20;
            // 
            // label20
            // 
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(67, 58);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(160, 23);
            this.label20.TabIndex = 19;
            this.label20.Text = "Temp";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.AutoEllipsis = true;
            this.label56.BackColor = System.Drawing.Color.Gainsboro;
            this.label56.Dock = System.Windows.Forms.DockStyle.Top;
            this.label56.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(0, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(868, 30);
            this.label56.TabIndex = 9;
            this.label56.Text = "■ Monitoring";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlManagerLaserWindows
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlManagerLaserWindows";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label lblLaserMain;
        private System.Windows.Forms.Button btnMainLock;
        private System.Windows.Forms.TextBox txtMainLock;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox txtMainPD7ValuePD7PowerReadOnly;
        private System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtMainPD7ValueOutputPowerReadOnly;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox txtMainAttenuatorPowerControlAttenuatorPowerReadOnly;
        private System.Windows.Forms.Button btnMainAttenuatorPowerControlAttenuatorPowerSet;
        internal System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtMainAttenuatorPowerControlAttenuatorPower;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtMainAttenuatorPowerControlTransmission;
        private System.Windows.Forms.TextBox txtMainAttenuatorPowerControlTransmissionReadOnly;
        private System.Windows.Forms.Button btnMainAttenuatorPowerControlTransmissionSet;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtMainTimingAndFrequencyBurstReadOnly;
        internal System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnMainTimingAndFrequencyPulseModeSet;
        private System.Windows.Forms.TextBox txtMainTimingAndFrequencyBurst;
        private System.Windows.Forms.Button btnMainTimingAndFrequencyAmplifierSet;
        private System.Windows.Forms.TextBox txtMainTimingAndFrequencyPulseModeReadOnly;
        private System.Windows.Forms.Button btnMainTimingAndFrequencyDividerSet;
        private System.Windows.Forms.ComboBox cbxtMainTimingAndFrequencyPulseMode;
        private System.Windows.Forms.Button btnMainTimingAndFrequencyBurstSet;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbxtMainTimingAndFrequencyAmplifier;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMainTimingAndFrequencyOutputReadOnly;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMainTimingAndFrequencyDividerReadOnly;
        private System.Windows.Forms.TextBox txtMainTimingAndFrequencyAmplifierReadOnly;
        private System.Windows.Forms.TextBox txtMainTimingAndFrequencyDivider;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnMainSystemStatusStop;
        internal System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnMainSystemStatusStart;
        private System.Windows.Forms.Label lblMainSystemStatus;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnMainSystemFaultsClear;
        private System.Windows.Forms.Label lblMainSystemFaults;
        internal System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblMainShutter;
        private System.Windows.Forms.Button btnMainShutterClose;
        internal System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblMainKeySwitch;
        internal System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMainConnectionToLaserDisConnection;
        internal System.Windows.Forms.Label lblLoadingLoaderTransfer;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox txtMonitoringH3Dewpoint;
        internal System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtMonitoringH3Humidity;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtMonitoringH3Temp;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox txtMonitoringH2Dewpoint;
        internal System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtMonitoringH2Humidity;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtMonitoringH2Temp;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox txtMonitoringH1AmplifierDewpoint;
        internal System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMonitoringH1AmplifierHumidity;
        private System.Windows.Forms.TextBox txtMonitoringH1AmplifierTemp;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.TextBox txtMonitoringHousingTemperature2Temp;
        internal System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox txtMonitoringHousingTemperature1Temp;
        internal System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtMonitoringWaterMonitorFlow;
        internal System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMonitoringWaterMonitorTemp;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panelConnectionToLaserLight;
        private System.Windows.Forms.Panel panelMainShutterLight;
        private System.Windows.Forms.Panel panelMainSystemFaultsLight;
        private System.Windows.Forms.Panel panelMainKeySwitchLight;
        private System.Windows.Forms.Panel panelMainSystemStatusLight;
        private System.Windows.Forms.Panel panelMainAttenuatorPowerControlClosedLoopLight;
        private System.Windows.Forms.Panel panelMainAttenuatorPowerControlOpenLoopLight;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panelMainModulatorPulseEnergyControlExternalLight;
        private System.Windows.Forms.Panel panelMainModulatorPulseEnergyControlInternalLight;
        private System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMainModulatorPulseEnergyControl;
        private System.Windows.Forms.Button btnMainModulatorPulseEnergyControlSet;
        private System.Windows.Forms.TextBox txtMainModulatorPulseEnergyControlReadOnly;
        private System.Windows.Forms.Panel panelMonitoringWaterMonitorLight;
        private System.Windows.Forms.Panel panelMonitoringHousingTemperature1Light;
        private System.Windows.Forms.Panel panelMonitoringHousingTemperature2Light;
        private System.Windows.Forms.Panel panelMonitoringH3AmbientAirLight;
        private System.Windows.Forms.Panel panelMonitoringH2HarmonicModuleAirLight;
        private System.Windows.Forms.Panel panelMonitoringH1AmplifierLight;
    }
}

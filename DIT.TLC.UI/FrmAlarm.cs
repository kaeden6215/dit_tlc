﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{

    public partial class FrmAlarm : Form
    {
        public FrmAlarm()
        {
            InitializeComponent();

            //AlarmMgr.Instance.LstlogAlarms.Add(lstAlarmClone);
            //AlarmMgr.Instance.InitializeAlarmView(lstAlarmClone);
            //AlarmMgr.Instance.InitializeSolution();
        }

        public void AddAlarmList(Alarm am)
        {
            lstAlarmClone.Items.Insert(0,
                new ListViewItem(new string[] {
                             DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                              ((int)am.ID).ToString(),
                              am.ID.ToString(),
                             am.Level.ToString()
                        }));
        }
        public void AlarmClear()
        {
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            base.OnClosing(e);
        }

        private void btnAlarmClear_Click(object sender, EventArgs e)
        {

            AlarmManager.Instance.Clear(GG.Equip);
            lstAlarmClone.Items.Clear();
            this.Close();
        }

        private void btnBuzzerStop_Click(object sender, EventArgs e)
        {
            AlarmManager.Instance.BuzzserStop(GG.Equip);
        }
    }
}

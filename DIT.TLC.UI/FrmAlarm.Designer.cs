﻿namespace DIT.TLC.UI
{
    partial class FrmAlarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAlarmllMsg = new System.Windows.Forms.Label();
            this.lstAlarmClone = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.btnAlarmClear = new System.Windows.Forms.Button();
            this.btnBuzzerStop = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblAlarmllMsg
            // 
            this.lblAlarmllMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAlarmllMsg.BackColor = System.Drawing.Color.White;
            this.lblAlarmllMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAlarmllMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblAlarmllMsg.Location = new System.Drawing.Point(-1, 223);
            this.lblAlarmllMsg.Margin = new System.Windows.Forms.Padding(0);
            this.lblAlarmllMsg.Name = "lblAlarmllMsg";
            this.lblAlarmllMsg.Size = new System.Drawing.Size(700, 95);
            this.lblAlarmllMsg.TabIndex = 75;
            this.lblAlarmllMsg.Text = "-";
            this.lblAlarmllMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstAlarmClone
            // 
            this.lstAlarmClone.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lstAlarmClone.AutoArrange = false;
            this.lstAlarmClone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lstAlarmClone.BackgroundImageTiled = true;
            this.lstAlarmClone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstAlarmClone.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lstAlarmClone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lstAlarmClone.ForeColor = System.Drawing.Color.White;
            this.lstAlarmClone.FullRowSelect = true;
            this.lstAlarmClone.GridLines = true;
            this.lstAlarmClone.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstAlarmClone.Location = new System.Drawing.Point(1, 35);
            this.lstAlarmClone.MultiSelect = false;
            this.lstAlarmClone.Name = "lstAlarmClone";
            this.lstAlarmClone.Size = new System.Drawing.Size(698, 149);
            this.lstAlarmClone.TabIndex = 78;
            this.lstAlarmClone.TileSize = new System.Drawing.Size(1, 1);
            this.lstAlarmClone.UseCompatibleStateImageBehavior = false;
            this.lstAlarmClone.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TIME";
            this.columnHeader2.Width = 125;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ID";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "TEXT";
            this.columnHeader4.Width = 448;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(700, 33);
            this.label1.TabIndex = 74;
            this.label1.Text = "Alarm";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAlarmClear
            // 
            this.btnAlarmClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAlarmClear.Location = new System.Drawing.Point(491, 188);
            this.btnAlarmClear.Name = "btnAlarmClear";
            this.btnAlarmClear.Size = new System.Drawing.Size(100, 30);
            this.btnAlarmClear.TabIndex = 79;
            this.btnAlarmClear.Text = "Alarm Clear";
            this.btnAlarmClear.UseVisualStyleBackColor = true;
            this.btnAlarmClear.Click += new System.EventHandler(this.btnAlarmClear_Click);
            // 
            // btnBuzzerStop
            // 
            this.btnBuzzerStop.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBuzzerStop.Location = new System.Drawing.Point(597, 189);
            this.btnBuzzerStop.Name = "btnBuzzerStop";
            this.btnBuzzerStop.Size = new System.Drawing.Size(100, 30);
            this.btnBuzzerStop.TabIndex = 80;
            this.btnBuzzerStop.Text = "Buzzer Stop";
            this.btnBuzzerStop.UseVisualStyleBackColor = true;
            this.btnBuzzerStop.Click += new System.EventHandler(this.btnBuzzerStop_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 82;
            this.label2.Text = "Help :";
            // 
            // FrmAlarm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(699, 320);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBuzzerStop);
            this.Controls.Add(this.btnAlarmClear);
            this.Controls.Add(this.lblAlarmllMsg);
            this.Controls.Add(this.lstAlarmClone);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "FrmAlarm";
            this.Text = "FrmAlarm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label lblAlarmllMsg;
        internal System.Windows.Forms.ListView lstAlarmClone;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAlarmClear;
        private System.Windows.Forms.Button btnBuzzerStop;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
    }
}
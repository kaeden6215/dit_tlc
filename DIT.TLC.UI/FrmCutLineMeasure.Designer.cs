﻿namespace DIT.TLC.UI
{
    partial class FrmCutLineMeasure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCutLineMeasure));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel25 = new System.Windows.Forms.Panel();
            this.lstAColL1 = new System.Windows.Forms.ListView();
            this.X = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Y = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAColL1Delete = new System.Windows.Forms.Button();
            this.btnAColL1Change = new System.Windows.Forms.Button();
            this.btnAColL1Add = new System.Windows.Forms.Button();
            this.lblAColL1Table = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lstAColL2 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAColL2Delete = new System.Windows.Forms.Button();
            this.btnAColL2Change = new System.Windows.Forms.Button();
            this.btnAColL2Add = new System.Windows.Forms.Button();
            this.lblAColL2Table = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lstBColR1 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnBColR1Delete = new System.Windows.Forms.Button();
            this.btnBColR1Change = new System.Windows.Forms.Button();
            this.btnBColR1Add = new System.Windows.Forms.Button();
            this.lblBColR1Table = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lstBColR2 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnBColR2Delete = new System.Windows.Forms.Button();
            this.btnBColR2Change = new System.Windows.Forms.Button();
            this.btnBColR2Add = new System.Windows.Forms.Button();
            this.lblBColR2Table = new System.Windows.Forms.Label();
            this.panel25.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(1390, 540);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(145, 43);
            this.btnSave.TabIndex = 474;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCancel.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCancel.Location = new System.Drawing.Point(1541, 540);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(145, 41);
            this.btnCancel.TabIndex = 473;
            this.btnCancel.Text = "닫기";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.Control;
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.lstAColL1);
            this.panel25.Controls.Add(this.btnAColL1Delete);
            this.panel25.Controls.Add(this.btnAColL1Change);
            this.panel25.Controls.Add(this.btnAColL1Add);
            this.panel25.Controls.Add(this.lblAColL1Table);
            this.panel25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel25.Location = new System.Drawing.Point(12, 12);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(414, 522);
            this.panel25.TabIndex = 476;
            // 
            // lstAColL1
            // 
            this.lstAColL1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lstAColL1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.X,
            this.Y});
            this.lstAColL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lstAColL1.FullRowSelect = true;
            this.lstAColL1.GridLines = true;
            this.lstAColL1.Location = new System.Drawing.Point(29, 103);
            this.lstAColL1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstAColL1.Name = "lstAColL1";
            this.lstAColL1.Size = new System.Drawing.Size(353, 397);
            this.lstAColL1.TabIndex = 16;
            this.lstAColL1.UseCompatibleStateImageBehavior = false;
            this.lstAColL1.View = System.Windows.Forms.View.Details;
            // 
            // X
            // 
            this.X.Text = "X";
            this.X.Width = 170;
            // 
            // Y
            // 
            this.Y.Text = "Y";
            this.Y.Width = 170;
            // 
            // btnAColL1Delete
            // 
            this.btnAColL1Delete.BackColor = System.Drawing.SystemColors.Control;
            this.btnAColL1Delete.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAColL1Delete.ForeColor = System.Drawing.Color.Black;
            this.btnAColL1Delete.Location = new System.Drawing.Point(201, 46);
            this.btnAColL1Delete.Name = "btnAColL1Delete";
            this.btnAColL1Delete.Size = new System.Drawing.Size(80, 50);
            this.btnAColL1Delete.TabIndex = 15;
            this.btnAColL1Delete.Text = "DELETE";
            this.btnAColL1Delete.UseVisualStyleBackColor = false;
            this.btnAColL1Delete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAColL1Change
            // 
            this.btnAColL1Change.BackColor = System.Drawing.SystemColors.Control;
            this.btnAColL1Change.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAColL1Change.ForeColor = System.Drawing.Color.Black;
            this.btnAColL1Change.Location = new System.Drawing.Point(115, 46);
            this.btnAColL1Change.Name = "btnAColL1Change";
            this.btnAColL1Change.Size = new System.Drawing.Size(80, 50);
            this.btnAColL1Change.TabIndex = 14;
            this.btnAColL1Change.Text = "CHANGE";
            this.btnAColL1Change.UseVisualStyleBackColor = false;
            this.btnAColL1Change.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnAColL1Add
            // 
            this.btnAColL1Add.BackColor = System.Drawing.SystemColors.Control;
            this.btnAColL1Add.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAColL1Add.ForeColor = System.Drawing.Color.Black;
            this.btnAColL1Add.Location = new System.Drawing.Point(29, 46);
            this.btnAColL1Add.Name = "btnAColL1Add";
            this.btnAColL1Add.Size = new System.Drawing.Size(80, 50);
            this.btnAColL1Add.TabIndex = 13;
            this.btnAColL1Add.Text = "ADD";
            this.btnAColL1Add.UseVisualStyleBackColor = false;
            this.btnAColL1Add.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblAColL1Table
            // 
            this.lblAColL1Table.AutoEllipsis = true;
            this.lblAColL1Table.BackColor = System.Drawing.Color.Gainsboro;
            this.lblAColL1Table.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblAColL1Table.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblAColL1Table.ForeColor = System.Drawing.Color.Black;
            this.lblAColL1Table.Location = new System.Drawing.Point(0, 0);
            this.lblAColL1Table.Name = "lblAColL1Table";
            this.lblAColL1Table.Size = new System.Drawing.Size(412, 26);
            this.lblAColL1Table.TabIndex = 9;
            this.lblAColL1Table.Text = "■ A열 L1테이블";
            this.lblAColL1Table.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lstAColL2);
            this.panel1.Controls.Add(this.btnAColL2Delete);
            this.panel1.Controls.Add(this.btnAColL2Change);
            this.panel1.Controls.Add(this.btnAColL2Add);
            this.panel1.Controls.Add(this.lblAColL2Table);
            this.panel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel1.Location = new System.Drawing.Point(432, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(414, 522);
            this.panel1.TabIndex = 477;
            // 
            // lstAColL2
            // 
            this.lstAColL2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lstAColL2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstAColL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lstAColL2.FullRowSelect = true;
            this.lstAColL2.GridLines = true;
            this.lstAColL2.Location = new System.Drawing.Point(29, 103);
            this.lstAColL2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstAColL2.Name = "lstAColL2";
            this.lstAColL2.Size = new System.Drawing.Size(353, 397);
            this.lstAColL2.TabIndex = 20;
            this.lstAColL2.UseCompatibleStateImageBehavior = false;
            this.lstAColL2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "X";
            this.columnHeader1.Width = 170;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Y";
            this.columnHeader2.Width = 170;
            // 
            // btnAColL2Delete
            // 
            this.btnAColL2Delete.BackColor = System.Drawing.SystemColors.Control;
            this.btnAColL2Delete.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAColL2Delete.ForeColor = System.Drawing.Color.Black;
            this.btnAColL2Delete.Location = new System.Drawing.Point(201, 46);
            this.btnAColL2Delete.Name = "btnAColL2Delete";
            this.btnAColL2Delete.Size = new System.Drawing.Size(80, 50);
            this.btnAColL2Delete.TabIndex = 19;
            this.btnAColL2Delete.Text = "DELETE";
            this.btnAColL2Delete.UseVisualStyleBackColor = false;
            this.btnAColL2Delete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAColL2Change
            // 
            this.btnAColL2Change.BackColor = System.Drawing.SystemColors.Control;
            this.btnAColL2Change.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAColL2Change.ForeColor = System.Drawing.Color.Black;
            this.btnAColL2Change.Location = new System.Drawing.Point(115, 46);
            this.btnAColL2Change.Name = "btnAColL2Change";
            this.btnAColL2Change.Size = new System.Drawing.Size(80, 50);
            this.btnAColL2Change.TabIndex = 18;
            this.btnAColL2Change.Text = "CHANGE";
            this.btnAColL2Change.UseVisualStyleBackColor = false;
            this.btnAColL2Change.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnAColL2Add
            // 
            this.btnAColL2Add.BackColor = System.Drawing.SystemColors.Control;
            this.btnAColL2Add.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAColL2Add.ForeColor = System.Drawing.Color.Black;
            this.btnAColL2Add.Location = new System.Drawing.Point(29, 46);
            this.btnAColL2Add.Name = "btnAColL2Add";
            this.btnAColL2Add.Size = new System.Drawing.Size(80, 50);
            this.btnAColL2Add.TabIndex = 17;
            this.btnAColL2Add.Text = "ADD";
            this.btnAColL2Add.UseVisualStyleBackColor = false;
            this.btnAColL2Add.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblAColL2Table
            // 
            this.lblAColL2Table.AutoEllipsis = true;
            this.lblAColL2Table.BackColor = System.Drawing.Color.Gainsboro;
            this.lblAColL2Table.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblAColL2Table.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblAColL2Table.ForeColor = System.Drawing.Color.Black;
            this.lblAColL2Table.Location = new System.Drawing.Point(0, 0);
            this.lblAColL2Table.Name = "lblAColL2Table";
            this.lblAColL2Table.Size = new System.Drawing.Size(412, 26);
            this.lblAColL2Table.TabIndex = 9;
            this.lblAColL2Table.Text = "■ A열 L2테이블";
            this.lblAColL2Table.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lstBColR1);
            this.panel2.Controls.Add(this.btnBColR1Delete);
            this.panel2.Controls.Add(this.btnBColR1Change);
            this.panel2.Controls.Add(this.btnBColR1Add);
            this.panel2.Controls.Add(this.lblBColR1Table);
            this.panel2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel2.Location = new System.Drawing.Point(852, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(414, 522);
            this.panel2.TabIndex = 478;
            // 
            // lstBColR1
            // 
            this.lstBColR1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lstBColR1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.lstBColR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lstBColR1.FullRowSelect = true;
            this.lstBColR1.GridLines = true;
            this.lstBColR1.Location = new System.Drawing.Point(29, 103);
            this.lstBColR1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstBColR1.Name = "lstBColR1";
            this.lstBColR1.Size = new System.Drawing.Size(353, 397);
            this.lstBColR1.TabIndex = 20;
            this.lstBColR1.UseCompatibleStateImageBehavior = false;
            this.lstBColR1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "X";
            this.columnHeader3.Width = 170;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Y";
            this.columnHeader4.Width = 170;
            // 
            // btnBColR1Delete
            // 
            this.btnBColR1Delete.BackColor = System.Drawing.SystemColors.Control;
            this.btnBColR1Delete.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBColR1Delete.ForeColor = System.Drawing.Color.Black;
            this.btnBColR1Delete.Location = new System.Drawing.Point(201, 46);
            this.btnBColR1Delete.Name = "btnBColR1Delete";
            this.btnBColR1Delete.Size = new System.Drawing.Size(80, 50);
            this.btnBColR1Delete.TabIndex = 19;
            this.btnBColR1Delete.Text = "DELETE";
            this.btnBColR1Delete.UseVisualStyleBackColor = false;
            this.btnBColR1Delete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnBColR1Change
            // 
            this.btnBColR1Change.BackColor = System.Drawing.SystemColors.Control;
            this.btnBColR1Change.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBColR1Change.ForeColor = System.Drawing.Color.Black;
            this.btnBColR1Change.Location = new System.Drawing.Point(115, 46);
            this.btnBColR1Change.Name = "btnBColR1Change";
            this.btnBColR1Change.Size = new System.Drawing.Size(80, 50);
            this.btnBColR1Change.TabIndex = 18;
            this.btnBColR1Change.Text = "CHANGE";
            this.btnBColR1Change.UseVisualStyleBackColor = false;
            this.btnBColR1Change.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnBColR1Add
            // 
            this.btnBColR1Add.BackColor = System.Drawing.SystemColors.Control;
            this.btnBColR1Add.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBColR1Add.ForeColor = System.Drawing.Color.Black;
            this.btnBColR1Add.Location = new System.Drawing.Point(29, 46);
            this.btnBColR1Add.Name = "btnBColR1Add";
            this.btnBColR1Add.Size = new System.Drawing.Size(80, 50);
            this.btnBColR1Add.TabIndex = 17;
            this.btnBColR1Add.Text = "ADD";
            this.btnBColR1Add.UseVisualStyleBackColor = false;
            this.btnBColR1Add.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblBColR1Table
            // 
            this.lblBColR1Table.AutoEllipsis = true;
            this.lblBColR1Table.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBColR1Table.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBColR1Table.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBColR1Table.ForeColor = System.Drawing.Color.Black;
            this.lblBColR1Table.Location = new System.Drawing.Point(0, 0);
            this.lblBColR1Table.Name = "lblBColR1Table";
            this.lblBColR1Table.Size = new System.Drawing.Size(412, 26);
            this.lblBColR1Table.TabIndex = 9;
            this.lblBColR1Table.Text = "■ B열 R1테이블";
            this.lblBColR1Table.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lstBColR2);
            this.panel3.Controls.Add(this.btnBColR2Delete);
            this.panel3.Controls.Add(this.btnBColR2Change);
            this.panel3.Controls.Add(this.btnBColR2Add);
            this.panel3.Controls.Add(this.lblBColR2Table);
            this.panel3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel3.Location = new System.Drawing.Point(1272, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(414, 522);
            this.panel3.TabIndex = 479;
            // 
            // lstBColR2
            // 
            this.lstBColR2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lstBColR2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.lstBColR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lstBColR2.FullRowSelect = true;
            this.lstBColR2.GridLines = true;
            this.lstBColR2.Location = new System.Drawing.Point(29, 103);
            this.lstBColR2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstBColR2.Name = "lstBColR2";
            this.lstBColR2.Size = new System.Drawing.Size(353, 397);
            this.lstBColR2.TabIndex = 20;
            this.lstBColR2.UseCompatibleStateImageBehavior = false;
            this.lstBColR2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "X";
            this.columnHeader5.Width = 170;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Y";
            this.columnHeader6.Width = 170;
            // 
            // btnBColR2Delete
            // 
            this.btnBColR2Delete.BackColor = System.Drawing.SystemColors.Control;
            this.btnBColR2Delete.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBColR2Delete.ForeColor = System.Drawing.Color.Black;
            this.btnBColR2Delete.Location = new System.Drawing.Point(201, 46);
            this.btnBColR2Delete.Name = "btnBColR2Delete";
            this.btnBColR2Delete.Size = new System.Drawing.Size(80, 50);
            this.btnBColR2Delete.TabIndex = 19;
            this.btnBColR2Delete.Text = "DELETE";
            this.btnBColR2Delete.UseVisualStyleBackColor = false;
            this.btnBColR2Delete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnBColR2Change
            // 
            this.btnBColR2Change.BackColor = System.Drawing.SystemColors.Control;
            this.btnBColR2Change.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBColR2Change.ForeColor = System.Drawing.Color.Black;
            this.btnBColR2Change.Location = new System.Drawing.Point(115, 46);
            this.btnBColR2Change.Name = "btnBColR2Change";
            this.btnBColR2Change.Size = new System.Drawing.Size(80, 50);
            this.btnBColR2Change.TabIndex = 18;
            this.btnBColR2Change.Text = "CHANGE";
            this.btnBColR2Change.UseVisualStyleBackColor = false;
            this.btnBColR2Change.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnBColR2Add
            // 
            this.btnBColR2Add.BackColor = System.Drawing.SystemColors.Control;
            this.btnBColR2Add.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBColR2Add.ForeColor = System.Drawing.Color.Black;
            this.btnBColR2Add.Location = new System.Drawing.Point(29, 46);
            this.btnBColR2Add.Name = "btnBColR2Add";
            this.btnBColR2Add.Size = new System.Drawing.Size(80, 50);
            this.btnBColR2Add.TabIndex = 17;
            this.btnBColR2Add.Text = "ADD";
            this.btnBColR2Add.UseVisualStyleBackColor = false;
            this.btnBColR2Add.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblBColR2Table
            // 
            this.lblBColR2Table.AutoEllipsis = true;
            this.lblBColR2Table.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBColR2Table.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBColR2Table.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBColR2Table.ForeColor = System.Drawing.Color.Black;
            this.lblBColR2Table.Location = new System.Drawing.Point(0, 0);
            this.lblBColR2Table.Name = "lblBColR2Table";
            this.lblBColR2Table.Size = new System.Drawing.Size(412, 26);
            this.lblBColR2Table.TabIndex = 9;
            this.lblBColR2Table.Text = "■ B열 R2테이블";
            this.lblBColR2Table.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmCutLineMeasure
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1704, 639);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCutLineMeasure";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "컷 라인 계측";
            this.panel25.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel25;
        internal System.Windows.Forms.Label lblAColL1Table;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label lblAColL2Table;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label lblBColR1Table;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label lblBColR2Table;
        private System.Windows.Forms.ListView lstAColL1;
        private System.Windows.Forms.ColumnHeader X;
        private System.Windows.Forms.ColumnHeader Y;
        private System.Windows.Forms.Button btnAColL1Delete;
        private System.Windows.Forms.Button btnAColL1Change;
        private System.Windows.Forms.Button btnAColL1Add;
        private System.Windows.Forms.ListView lstAColL2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnAColL2Delete;
        private System.Windows.Forms.Button btnAColL2Change;
        private System.Windows.Forms.Button btnAColL2Add;
        private System.Windows.Forms.ListView lstBColR1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnBColR1Delete;
        private System.Windows.Forms.Button btnBColR1Change;
        private System.Windows.Forms.Button btnBColR1Add;
        private System.Windows.Forms.ListView lstBColR2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button btnBColR2Delete;
        private System.Windows.Forms.Button btnBColR2Change;
        private System.Windows.Forms.Button btnBColR2Add;
    }
}
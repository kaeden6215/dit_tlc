﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIT.TLC.UI
{
    public class BreakData
    {
        private int _cameraidx;

        public int cameraidx
        {
            get
            {
                return _cameraidx;
            }

            private set
            {
                _cameraidx = value;
            }
        }

        private List<double> _x_offset;

        public List<double> x_offset
        {
            get
            {
                return _x_offset;
            }

            private set
            {
                _x_offset = value;
            }
        }

        private List<double> _y_offset;

        public List<double> y_offset
        {
            get
            {
                return _y_offset;
            }

            private set
            {
                _y_offset = value;
            }
        }

        private List<double> _angle_offset;

        public List<double> angle_offset
        {
            get
            {
                return _angle_offset;
            }

            private set
            {
                _y_offset = value;
            }
        }
        private List<double> _time;

        public List<double> time
        {
            get
            {
                return _time;
            }

            private set
            {
                _time = value;
            }
        }

        public BreakData(List<double> x_offset, List<double> y_offset, List<double> angle_offset, List<double> time, int cameraidx = -1)
        {
            _cameraidx = cameraidx;
            _x_offset = x_offset;
            _y_offset = y_offset;
            _angle_offset = angle_offset;
            _time = time;
        }
    }
}

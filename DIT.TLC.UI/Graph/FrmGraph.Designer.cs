﻿namespace DIT.TLC.UI
{
    partial class FrmGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGraph));
            this.label6 = new System.Windows.Forms.Label();
            this.Select_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.txtDirPath = new System.Windows.Forms.TextBox();
            this.btnReadB4Day = new System.Windows.Forms.Button();
            this.btnReadTheDay = new System.Windows.Forms.Button();
            this.btn_Ini_LoadInDir = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.Btn_BeforNday = new System.Windows.Forms.Button();
            this.Comb_Befor_Days = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Break = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.Fine = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Pre = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.ucrlChartControl2 = new DIT.TLC.UI.UcrlChartControl();
            this.ucrlChartControl1 = new DIT.TLC.UI.UcrlChartControl();
            this.ucrlFineControl4 = new DIT.TLC.UI.UcrlFineControl2();
            this.ucrlFineControl3 = new DIT.TLC.UI.UcrlFineControl1();
            this.ucrlFineControl2 = new DIT.TLC.UI.UcrlFineControl2();
            this.ucrlFineControl1 = new DIT.TLC.UI.UcrlFineControl1();
            this.ucrlBreakControl2 = new DIT.TLC.UI.UcrlBreakControl();
            this.ucrlBreakControl1 = new DIT.TLC.UI.UcrlBreakControl();
            this.ucrlBreakControl6 = new DIT.TLC.UI.UcrlBreakControl1();
            this.ucrlBreakControl5 = new DIT.TLC.UI.UcrlBreakControl1();
            this.ucrlBreakControl4 = new DIT.TLC.UI.UcrlBreakControl1();
            this.ucrlBreakControl3 = new DIT.TLC.UI.UcrlBreakControl1();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.Break.SuspendLayout();
            this.panel7.SuspendLayout();
            this.Fine.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Pre.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(3, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 23);
            this.label6.TabIndex = 6;
            this.label6.Text = "기간별 :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Select_dateTimePicker
            // 
            this.Select_dateTimePicker.Location = new System.Drawing.Point(78, 32);
            this.Select_dateTimePicker.Name = "Select_dateTimePicker";
            this.Select_dateTimePicker.Size = new System.Drawing.Size(219, 23);
            this.Select_dateTimePicker.TabIndex = 5;
            this.Select_dateTimePicker.ValueChanged += new System.EventHandler(this.Select_dateTimePicker_ValueChanged);
            // 
            // txtDirPath
            // 
            this.txtDirPath.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtDirPath.Location = new System.Drawing.Point(10, 6);
            this.txtDirPath.Name = "txtDirPath";
            this.txtDirPath.Size = new System.Drawing.Size(478, 23);
            this.txtDirPath.TabIndex = 37;
            // 
            // btnReadB4Day
            // 
            this.btnReadB4Day.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnReadB4Day.Location = new System.Drawing.Point(100, 30);
            this.btnReadB4Day.Name = "btnReadB4Day";
            this.btnReadB4Day.Size = new System.Drawing.Size(89, 62);
            this.btnReadB4Day.TabIndex = 27;
            this.btnReadB4Day.Text = "전일";
            this.btnReadB4Day.UseVisualStyleBackColor = false;
            this.btnReadB4Day.Click += new System.EventHandler(this.btnReadB4Day_Click);
            // 
            // btnReadTheDay
            // 
            this.btnReadTheDay.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnReadTheDay.Location = new System.Drawing.Point(6, 30);
            this.btnReadTheDay.Name = "btnReadTheDay";
            this.btnReadTheDay.Size = new System.Drawing.Size(89, 62);
            this.btnReadTheDay.TabIndex = 26;
            this.btnReadTheDay.Text = "당일";
            this.btnReadTheDay.UseVisualStyleBackColor = false;
            this.btnReadTheDay.Click += new System.EventHandler(this.btnReadTheDay_Click);
            // 
            // btn_Ini_LoadInDir
            // 
            this.btn_Ini_LoadInDir.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Ini_LoadInDir.Location = new System.Drawing.Point(513, 6);
            this.btn_Ini_LoadInDir.Name = "btn_Ini_LoadInDir";
            this.btn_Ini_LoadInDir.Size = new System.Drawing.Size(75, 23);
            this.btn_Ini_LoadInDir.TabIndex = 38;
            this.btn_Ini_LoadInDir.Text = "기본 폴더 설정";
            this.btn_Ini_LoadInDir.UseVisualStyleBackColor = false;
            this.btn_Ini_LoadInDir.Click += new System.EventHandler(this.btn_Ini_LoadInDir_Click);
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(78, 4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 23);
            this.label15.TabIndex = 29;
            this.label15.Text = "당일 부터";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Btn_BeforNday
            // 
            this.Btn_BeforNday.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Btn_BeforNday.Location = new System.Drawing.Point(230, 4);
            this.Btn_BeforNday.Name = "Btn_BeforNday";
            this.Btn_BeforNday.Size = new System.Drawing.Size(67, 23);
            this.Btn_BeforNday.TabIndex = 31;
            this.Btn_BeforNday.Text = "전 검색";
            this.Btn_BeforNday.UseVisualStyleBackColor = false;
            this.Btn_BeforNday.Click += new System.EventHandler(this.Btn_BeforNday_Click);
            // 
            // Comb_Befor_Days
            // 
            this.Comb_Befor_Days.FormattingEnabled = true;
            this.Comb_Befor_Days.Items.AddRange(new object[] {
            "1일",
            "2일",
            "3일",
            "4일",
            "5일",
            "6일",
            "7일"});
            this.Comb_Befor_Days.Location = new System.Drawing.Point(163, 5);
            this.Comb_Befor_Days.Name = "Comb_Befor_Days";
            this.Comb_Befor_Days.Size = new System.Drawing.Size(61, 23);
            this.Comb_Befor_Days.TabIndex = 30;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.Select_dateTimePicker);
            this.panel4.Controls.Add(this.Btn_BeforNday);
            this.panel4.Controls.Add(this.Comb_Befor_Days);
            this.panel4.Location = new System.Drawing.Point(194, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(302, 62);
            this.panel4.TabIndex = 32;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 23);
            this.label5.TabIndex = 32;
            this.label5.Text = "특정일 :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.btnReadB4Day);
            this.panel5.Controls.Add(this.btnReadTheDay);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel5.Location = new System.Drawing.Point(870, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(505, 100);
            this.panel5.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(503, 24);
            this.label1.TabIndex = 33;
            this.label1.Text = "■ 조회";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Break
            // 
            this.Break.BackColor = System.Drawing.Color.Gainsboro;
            this.Break.Controls.Add(this.panel7);
            this.Break.Location = new System.Drawing.Point(4, 28);
            this.Break.Name = "Break";
            this.Break.Padding = new System.Windows.Forms.Padding(3);
            this.Break.Size = new System.Drawing.Size(1360, 900);
            this.Break.TabIndex = 2;
            this.Break.Text = "Break";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.ucrlBreakControl2);
            this.panel7.Controls.Add(this.ucrlBreakControl1);
            this.panel7.Controls.Add(this.ucrlBreakControl6);
            this.panel7.Controls.Add(this.ucrlBreakControl5);
            this.panel7.Controls.Add(this.ucrlBreakControl4);
            this.panel7.Controls.Add(this.ucrlBreakControl3);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.label28);
            this.panel7.Location = new System.Drawing.Point(1, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1353, 895);
            this.panel7.TabIndex = 24;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.Location = new System.Drawing.Point(1167, 134);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(79, 23);
            this.label29.TabIndex = 43;
            this.label29.Text = "A2-A1";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(1167, 535);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(79, 23);
            this.label30.TabIndex = 42;
            this.label30.Text = "B2-B1";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(908, 6);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 23);
            this.label22.TabIndex = 22;
            this.label22.Text = "X-Y";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(660, 6);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 23);
            this.label23.TabIndex = 21;
            this.label23.Text = "T";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(404, 6);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 23);
            this.label24.TabIndex = 20;
            this.label24.Text = "Y";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(167, 6);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 23);
            this.label25.TabIndex = 19;
            this.label25.Text = "X";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(-4, 763);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 23);
            this.label7.TabIndex = 30;
            this.label7.Text = "Break B2";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(-4, 320);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(76, 23);
            this.label26.TabIndex = 29;
            this.label26.Text = "Break A2";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(-4, 549);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(76, 23);
            this.label27.TabIndex = 28;
            this.label27.Text = "Break B1";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(-4, 116);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 23);
            this.label28.TabIndex = 27;
            this.label28.Text = "Break A1";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Fine
            // 
            this.Fine.BackColor = System.Drawing.Color.Gainsboro;
            this.Fine.Controls.Add(this.panel2);
            this.Fine.Location = new System.Drawing.Point(4, 28);
            this.Fine.Name = "Fine";
            this.Fine.Padding = new System.Windows.Forms.Padding(3);
            this.Fine.Size = new System.Drawing.Size(1360, 900);
            this.Fine.TabIndex = 1;
            this.Fine.Text = "Fine";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.ucrlFineControl4);
            this.panel2.Controls.Add(this.ucrlFineControl3);
            this.panel2.Controls.Add(this.ucrlFineControl2);
            this.panel2.Controls.Add(this.ucrlFineControl1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(1, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1353, 897);
            this.panel2.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(908, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 23);
            this.label3.TabIndex = 33;
            this.label3.Text = "X-Y";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(660, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 23);
            this.label18.TabIndex = 32;
            this.label18.Text = "T";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(398, 6);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 23);
            this.label19.TabIndex = 31;
            this.label19.Text = "Y";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(167, 6);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 23);
            this.label20.TabIndex = 30;
            this.label20.Text = "X";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(-4, 763);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 23);
            this.label14.TabIndex = 21;
            this.label14.Text = "Right B";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(-4, 320);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 23);
            this.label13.TabIndex = 20;
            this.label13.Text = "Left B";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(-4, 549);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 23);
            this.label12.TabIndex = 19;
            this.label12.Text = "Right A";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(-4, 116);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 23);
            this.label11.TabIndex = 18;
            this.label11.Text = "Left A";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Pre
            // 
            this.Pre.BackColor = System.Drawing.Color.Gainsboro;
            this.Pre.Controls.Add(this.panel3);
            this.Pre.Location = new System.Drawing.Point(4, 28);
            this.Pre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Pre.Name = "Pre";
            this.Pre.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Pre.Size = new System.Drawing.Size(1360, 900);
            this.Pre.TabIndex = 0;
            this.Pre.Text = "Pre";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.ucrlChartControl2);
            this.panel3.Controls.Add(this.ucrlChartControl1);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(4, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1350, 494);
            this.panel3.TabIndex = 26;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(11, 330);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 23);
            this.label21.TabIndex = 21;
            this.label21.Text = "Pre A2";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(908, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 23);
            this.label17.TabIndex = 18;
            this.label17.Text = "X-Y";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(660, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 23);
            this.label16.TabIndex = 17;
            this.label16.Text = "T";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(398, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 23);
            this.label8.TabIndex = 16;
            this.label8.Text = "Y";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(167, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 23);
            this.label4.TabIndex = 15;
            this.label4.Text = "X";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(10, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Pre A1";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Pre);
            this.tabControl1.Controls.Add(this.Fine);
            this.tabControl1.Controls.Add(this.Break);
            this.tabControl1.ItemSize = new System.Drawing.Size(50, 24);
            this.tabControl1.Location = new System.Drawing.Point(9, 83);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1368, 932);
            this.tabControl1.TabIndex = 40;
            // 
            // ucrlChartControl2
            // 
            this.ucrlChartControl2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlChartControl2.Location = new System.Drawing.Point(67, 237);
            this.ucrlChartControl2.Name = "ucrlChartControl2";
            this.ucrlChartControl2.Size = new System.Drawing.Size(990, 231);
            this.ucrlChartControl2.TabIndex = 23;
            // 
            // ucrlChartControl1
            // 
            this.ucrlChartControl1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlChartControl1.Location = new System.Drawing.Point(66, 22);
            this.ucrlChartControl1.Name = "ucrlChartControl1";
            this.ucrlChartControl1.Size = new System.Drawing.Size(990, 231);
            this.ucrlChartControl1.TabIndex = 22;
            // 
            // ucrlFineControl4
            // 
            this.ucrlFineControl4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlFineControl4.Location = new System.Drawing.Point(66, 671);
            this.ucrlFineControl4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ucrlFineControl4.Name = "ucrlFineControl4";
            this.ucrlFineControl4.Size = new System.Drawing.Size(995, 230);
            this.ucrlFineControl4.TabIndex = 37;
            // 
            // ucrlFineControl3
            // 
            this.ucrlFineControl3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlFineControl3.Location = new System.Drawing.Point(66, 453);
            this.ucrlFineControl3.Name = "ucrlFineControl3";
            this.ucrlFineControl3.Size = new System.Drawing.Size(995, 230);
            this.ucrlFineControl3.TabIndex = 36;
            // 
            // ucrlFineControl2
            // 
            this.ucrlFineControl2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlFineControl2.Location = new System.Drawing.Point(66, 238);
            this.ucrlFineControl2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ucrlFineControl2.Name = "ucrlFineControl2";
            this.ucrlFineControl2.Size = new System.Drawing.Size(995, 230);
            this.ucrlFineControl2.TabIndex = 35;
            // 
            // ucrlFineControl1
            // 
            this.ucrlFineControl1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlFineControl1.Location = new System.Drawing.Point(66, 22);
            this.ucrlFineControl1.Name = "ucrlFineControl1";
            this.ucrlFineControl1.Size = new System.Drawing.Size(995, 230);
            this.ucrlFineControl1.TabIndex = 34;
            // 
            // ucrlBreakControl2
            // 
            this.ucrlBreakControl2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlBreakControl2.Location = new System.Drawing.Point(1092, 561);
            this.ucrlBreakControl2.Name = "ucrlBreakControl2";
            this.ucrlBreakControl2.Size = new System.Drawing.Size(236, 225);
            this.ucrlBreakControl2.TabIndex = 49;
            // 
            // ucrlBreakControl1
            // 
            this.ucrlBreakControl1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlBreakControl1.Location = new System.Drawing.Point(1092, 160);
            this.ucrlBreakControl1.Name = "ucrlBreakControl1";
            this.ucrlBreakControl1.Size = new System.Drawing.Size(236, 225);
            this.ucrlBreakControl1.TabIndex = 48;
            // 
            // ucrlBreakControl6
            // 
            this.ucrlBreakControl6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlBreakControl6.Location = new System.Drawing.Point(66, 671);
            this.ucrlBreakControl6.Name = "ucrlBreakControl6";
            this.ucrlBreakControl6.Size = new System.Drawing.Size(995, 230);
            this.ucrlBreakControl6.TabIndex = 47;
            // 
            // ucrlBreakControl5
            // 
            this.ucrlBreakControl5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlBreakControl5.Location = new System.Drawing.Point(66, 453);
            this.ucrlBreakControl5.Name = "ucrlBreakControl5";
            this.ucrlBreakControl5.Size = new System.Drawing.Size(995, 230);
            this.ucrlBreakControl5.TabIndex = 46;
            // 
            // ucrlBreakControl4
            // 
            this.ucrlBreakControl4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlBreakControl4.Location = new System.Drawing.Point(66, 238);
            this.ucrlBreakControl4.Name = "ucrlBreakControl4";
            this.ucrlBreakControl4.Size = new System.Drawing.Size(995, 230);
            this.ucrlBreakControl4.TabIndex = 45;
            // 
            // ucrlBreakControl3
            // 
            this.ucrlBreakControl3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlBreakControl3.Location = new System.Drawing.Point(66, 22);
            this.ucrlBreakControl3.Name = "ucrlBreakControl3";
            this.ucrlBreakControl3.Size = new System.Drawing.Size(995, 230);
            this.ucrlBreakControl3.TabIndex = 44;
            // 
            // FrmGraph
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1386, 1021);
            this.Controls.Add(this.txtDirPath);
            this.Controls.Add(this.btn_Ini_LoadInDir);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmGraph";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "그래프";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.Break.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.Fine.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.Pre.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker Select_dateTimePicker;
        private System.Windows.Forms.TextBox txtDirPath;
        private System.Windows.Forms.Button btnReadB4Day;
        private System.Windows.Forms.Button btnReadTheDay;
        private System.Windows.Forms.Button btn_Ini_LoadInDir;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button Btn_BeforNday;
        private System.Windows.Forms.ComboBox Comb_Befor_Days;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Break;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TabPage Fine;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage Pre;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private UcrlChartControl ucrlChartControl2;
        private UcrlChartControl ucrlChartControl1;
        private UcrlBreakControl ucrlBreakControl2;
        private UcrlBreakControl ucrlBreakControl1;
        private UcrlBreakControl1 ucrlBreakControl6;
        private UcrlBreakControl1 ucrlBreakControl5;
        private UcrlBreakControl1 ucrlBreakControl4;
        private UcrlBreakControl1 ucrlBreakControl3;
        private UcrlFineControl2 ucrlFineControl4;
        private UcrlFineControl1 ucrlFineControl3;
        private UcrlFineControl2 ucrlFineControl2;
        private UcrlFineControl1 ucrlFineControl1;
    }
}
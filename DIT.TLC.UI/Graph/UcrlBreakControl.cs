﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DIT.TLC.UI
{
    public partial class UcrlBreakControl : UserControl
    {
        BreakData _btData;

        public UcrlBreakControl()
        {
            InitializeComponent();

            chart1.ChartAreas[0].AxisY.Maximum = 2;
            chart1.ChartAreas[0].AxisY.Minimum = -2;
            chart1.ChartAreas[0].AxisY.Interval = 1;
            chart1.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
            chartClearALL();
        }


        public void SetChart(BreakData btdata)
        {
            _btData = btdata;

            SetChart(1, _btData.time, _btData.y_offset);
            //SetChart(2, _btData.time, _btData.y_offset);
            //SetChart(3, _btData.time, _btData.angle_offset);
            //SetChart(4, _btData.x_offset, _btData.y_offset);
        }

        private void SetChart(int idx, List<double> xAxis, List<double> yAxis)
        {
            Chart chart;

            if (idx == 1)
            {
                chart = chart1;
            }
            //else if (idx == 2)
            //{
            //    chart = chart2;
            //}

            else
                return;

            chart.Series.Clear();
            chart.Series.Add("series");
            chart.Series[0].Points.DataBindXY(xAxis, yAxis);
            chart.Series[0].ChartType = SeriesChartType.Point;
        }

        public void chartClearALL()
        {
            chart1.Series.Clear();
            //chart2.Series.Clear();
        }

        private void chart1_DoubleClick_1(object sender, EventArgs e)
        {
            FrmBreakView frm = new FrmBreakView(chart1, _btData);
            frm.Text = "TIME - X_OFFSET";
            frm.Show();
        }
    }
}

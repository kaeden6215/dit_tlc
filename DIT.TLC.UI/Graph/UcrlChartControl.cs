﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DIT.TLC.UI
{
    public partial class UcrlChartControl : UserControl
    {
        ChartData _ctdata;

        public UcrlChartControl()
        {
            InitializeComponent();

            chart1.ChartAreas[0].AxisY.Maximum = 6;
            chart1.ChartAreas[0].AxisY.Minimum = -6;
            chart1.ChartAreas[0].AxisY.Interval = 3;
            chart1.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

            chart2.ChartAreas[0].AxisY.Maximum = 10;
            chart2.ChartAreas[0].AxisY.Minimum = -10;
            chart2.ChartAreas[0].AxisY.Interval = 5;
            chart2.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

            chart3.ChartAreas[0].AxisY.Maximum = 4;
            chart3.ChartAreas[0].AxisY.Minimum = -4;
            chart3.ChartAreas[0].AxisY.Interval = 2;
            chart3.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

            chart4.ChartAreas[0].AxisY.Maximum = 10;
            chart4.ChartAreas[0].AxisY.Minimum = -10;
            chart4.ChartAreas[0].AxisY.Interval = 5;
            chart4.ChartAreas[0].AxisX.Maximum = 5;
            chart4.ChartAreas[0].AxisX.Minimum = -5;
            chart4.ChartAreas[0].AxisX.Interval = 2.5;
            chart4.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
            chartClearALL();

        }

        public void SetChart(ChartData ctdata)
        {
            _ctdata = ctdata;

            SetChart(1, _ctdata.time, _ctdata.x_offset);
            SetChart(2, _ctdata.time, _ctdata.y_offset);
            SetChart(3, _ctdata.time, _ctdata.angle_offset);
            SetChart(4, _ctdata.x_offset, _ctdata.y_offset);
        }

        private void SetChart(int idx, List<double> xAxis, List<double> yAxis)
        {
            Chart chart;

            if (idx == 1)
            {
                chart = chart1;
            }
            else if (idx == 2)
            {
                chart = chart2;
            }
            else if (idx == 3)
            {
                chart = chart3;
            }
            else if (idx == 4)
            {
                chart = chart4;
            }
            else
                return;

            chart.Series.Clear();
            chart.Series.Add("series");
            chart.Series[0].Points.DataBindXY(xAxis, yAxis);
            chart.Series[0].ChartType = SeriesChartType.Point;
        }

        private void chart1_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart1, _ctdata);
            frm.Text = "TIME - X_OFFSET";
            frm.Show();
        }

        private void chart2_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart2, _ctdata);
            frm.Text = " TIME - Y_OFFSET";
            frm.Show();
        }

        private void chart3_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart3, _ctdata);
            frm.Text = " TIME - THETA";
            frm.Show();
        }

        private void chart4_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart4, _ctdata);
            frm.Text = " X_OFFSET - Y_OFFSET";
            frm.Show();
        }


        public void chartClearALL()
        {
            chart1.Series.Clear();
            chart2.Series.Clear();
            chart3.Series.Clear();
            chart4.Series.Clear();

        }

    }
}

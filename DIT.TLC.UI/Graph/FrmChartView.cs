﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DIT.TLC.UI
{
    public partial class FrmChartView : Form
    {
        ChartData _ctdata;

        public FrmChartView(Chart chart, ChartData ctdata)
        {
            InitializeComponent();

            _ctdata = ctdata;

            chart1.Series.Clear();

            for (int seriesIdx = 0; seriesIdx < chart.Series.Count; ++seriesIdx)
            {
                chart1.Series.Add(chart.Series[seriesIdx].Name);
                chart1.Series[seriesIdx].ChartType = SeriesChartType.Point;

                for (int i = 0; i < chart.Series[seriesIdx].Points.Count; ++i)
                {
                    chart1.Series[seriesIdx].Points.AddXY(chart.Series[seriesIdx].Points[i].XValue, chart.Series[seriesIdx].Points[i].YValues[0]);
                }
            }
            //오토 스케일
            //chart1.ChartAreas[0].AxisX.Minimum = chart.ChartAreas[0].AxisX.Minimum;
            //chart1.ChartAreas[0].AxisX.Maximum = chart.ChartAreas[0].AxisX.Maximum;
            //chart1.ChartAreas[0].AxisY.Minimum = chart.ChartAreas[0].AxisY.Minimum;
            //chart1.ChartAreas[0].AxisY.Maximum = chart.ChartAreas[0].AxisY.Maximum;

            chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;

            chart1.ChartAreas[0].AxisX.ScrollBar.Enabled = true;
            chart1.ChartAreas[0].AxisY.ScrollBar.Enabled = true;

            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            chart1.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;

            chart1.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
        }

        private void FrmChartView_ResizeBegin(object sender, EventArgs e)
        {
            chart1.Hide();
        }

        private void FrmChartView_ResizeEnd(object sender, EventArgs e)
        {
            chart1.Show();
        }

        private void chart1_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            // Check selected chart element and set tooltip text for it
            switch (e.HitTestResult.ChartElementType)
            {
                case ChartElementType.DataPoint:
                    int selectedSeriesIdx = 0;//chart1.Series.IndexOf(e.HitTestResult.Series);

                    if (selectedSeriesIdx == -1)
                        break;

                    var dataPoint = e.HitTestResult.Series.Points[e.HitTestResult.PointIndex];

                    int idx = e.HitTestResult.PointIndex;

                    if (chart1.Series[selectedSeriesIdx].Name == "Error")
                    {
                        e.Text = string.Format("Error Item\nX : {0}\nY : {1}",
                         dataPoint.XValue,
                         dataPoint.YValues[0]);
                        break;
                    }

                    bool isErrorData = (_ctdata.x_offset[idx] != dataPoint.XValue) || (_ctdata.y_offset[idx] != dataPoint.YValues[0]);

                    e.Text = string.Format("{0}{1}\nTime : {2}\nX : {3}\nY : {4}\nT : {5}\nCam : {6}",
                                           isErrorData ? "Error Item\n" : "",
                                           e.HitTestResult.Series.Name,
                                           TimeToString(_ctdata.time[idx]),
                                           _ctdata.x_offset[idx],
                                           _ctdata.y_offset[idx],
                                           _ctdata.angle_offset[idx],
                                           _ctdata.cameraidx);

                    break;
            }
        }

        private string TimeToString(double time)
        {
            string result;

            int sec = (int)(time % 60) % 60;
            int min = (int)(time / 60) % 60;
            int hour = (int)(time / 60) / 60;

            result = string.Format("{0:D2}:{1:D2}:{2:D2}:", hour, min, sec);

            return result;
        }
    }
}
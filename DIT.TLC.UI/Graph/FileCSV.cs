﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIT.TLC.UI
{
    public class FileCSV
    {
        private readonly string _basePath;
        public string BasePath
        {
            get
            {
                return _basePath;
            }
        }
        private List<string> _rowDatas;
        private List<string> _headerNames;
        public string Seperator = "\t";
        public int DataCount;

        /// <summary>
        /// 첫 행은 Header로, 나머지는 Data로.
        /// </summary>
        /// <param name="path"></param>
        public FileCSV(string path)
        {
            _basePath = path;
        }

        public void LoadFile()//데이터 추가부분
        {
            //ReadOnly 
            var fs = new FileStream(_basePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using (StreamReader sr = new StreamReader(fs))
            //using (StreamReader sr = new StreamReader(_basePath, Encoding.Default, true))
            {
                string fileContent = sr.ReadToEnd();
                string[] rows = fileContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                if (rows.Length < 2)
                    return;

                string[] headers = rows[0].Split(new string[] { Seperator }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < headers.Length; ++i)
                    headers[i] = headers[i].ToUpper();
                _headerNames = new List<string>(headers);

                string[] rowDatas = new string[rows.Length - 1];
                Array.Copy(rows, 1, rowDatas, 0, rows.Length - 1);
                _rowDatas = new List<string>(rowDatas);

                DataCount = _rowDatas.Count;
                sr.Close();
            }
        }

        public string GetData(int rowIdx, string headerName)
        {
            int headerIndex = _headerNames.IndexOf(headerName.ToUpper());
            if (headerIndex == -1)
                return "Header 없음";
            if (DataCount - 1 < rowIdx || rowIdx < 0)
                return "row Idx 이상";

            string[] datas = _rowDatas[rowIdx].Split(new string[] { Seperator }, StringSplitOptions.RemoveEmptyEntries);
            if (datas.Length - 1 < headerIndex || headerIndex < 0)
                return "";

            return datas[headerIndex];
        }
    }
}

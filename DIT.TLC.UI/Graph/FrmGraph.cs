﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DIT.TLC.UI
{
    public partial class FrmGraph : Form
    {
        List<FileCSV> _csvs = new List<FileCSV>();

        string[] filename = { "Break_FindAlign", "eLog_FindAlign_Left_A", "eLog_FindAlign_Left_B", "eLog_FindAlign_Right_A", "eLog_FindAlign_Right_B", "PreAlign" };

        //두 변수의 key는 무조건 동일, key 값으로 컨트롤
        public static Dictionary<String, ChartData> chartData = new Dictionary<String, ChartData>();
        public static Dictionary<String, UcrlChartControl> chartView = new Dictionary<String, UcrlChartControl>();

        public static Dictionary<String, UcrlFineControl1> chartView1 = new Dictionary<String, UcrlFineControl1>(); //Fine Plus
        public static Dictionary<String, UcrlFineControl2> chartView2 = new Dictionary<String, UcrlFineControl2>(); //Fine Minus

        public static Dictionary<String, UcrlBreakControl1> chartView3 = new Dictionary<String, UcrlBreakControl1>(); //Break

        public static Dictionary<String, BreakData> breakData = new Dictionary<String, BreakData>();
        public static Dictionary<String, UcrlBreakControl> breakView = new Dictionary<String, UcrlBreakControl>();

        //public static int idx = -1;

        public FrmGraph()
        {
            InitializeComponent();

            FileStream fs = new FileStream(Path.Combine(Application.StartupPath + @"\Setting\", "CameraOffsetInfo" + ".ini"), FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
            string Path_Temp;
            Path_Temp = sr.ReadToEnd();
            txtDirPath.Text = Path_Temp;
            sr.Dispose();
            fs.Dispose();

            Comb_Befor_Days.SelectedIndex = 0;

            chartData.Clear();
            chartView.Clear();
            chartView1.Clear();
            chartView2.Clear();
            chartView3.Clear();
            breakData.Clear();
            breakView.Clear();

            chartView3.Add(filename[0] + string.Format("_0"), ucrlBreakControl5);
            chartView3.Add(filename[0] + string.Format("_1"), ucrlBreakControl6);
            chartView3.Add(filename[0] + string.Format("_2"), ucrlBreakControl3);
            chartView3.Add(filename[0] + string.Format("_3"), ucrlBreakControl4);


            breakView.Add(filename[0] + string.Format("_1"), ucrlBreakControl1);
            breakView.Add(filename[0] + string.Format("_3"), ucrlBreakControl2);

            chartView1.Add(filename[1], ucrlFineControl1);
            chartView2.Add(filename[2], ucrlFineControl2);
            chartView1.Add(filename[3], ucrlFineControl3);
            chartView2.Add(filename[4], ucrlFineControl4);

            chartView.Add(filename[5] + string.Format("_0"), ucrlChartControl1);
            chartView.Add(filename[5] + string.Format("_1"), ucrlChartControl2);
            fileOpen();
            TodayView();

        }


        private void fileOpen()
        {
            for (int idx = 0; idx < _csvs.Count; idx++)
            {
                string str;

                List<double> x_offset = new List<double>();

                List<double> x_offsetresult1 = new List<double>();
                List<double> x_offsetresult3 = new List<double>();

                List<double> x_offsetcamera0 = new List<double>();
                List<double> x_offsetcamera1 = new List<double>();
                List<double> x_offsetcamera2 = new List<double>();
                List<double> x_offsetcamera3 = new List<double>();

                List<double> y_offset = new List<double>();
                List<double> angle_offset = new List<double>();

                List<double> time = new List<double>();

                List<double> timecamera0 = new List<double>();
                List<double> timecamera1 = new List<double>();
                List<double> timecamera2 = new List<double>();
                List<double> timecamera3 = new List<double>();

                List<double> timeresult0 = new List<double>();
                List<double> timeresult1 = new List<double>();
                List<double> timeresult2 = new List<double>();
                List<double> timeresult3 = new List<double>();

                List<double> result_y_offset0 = new List<double>();
                List<double> result_y_offset1 = new List<double>();
                List<double> result_y_offset2 = new List<double>();
                List<double> result_y_offset3 = new List<double>();

                List<double> result_y_offset0_1 = new List<double>();
                List<double> result_y_offset2_3 = new List<double>();

                List<double> result_time1 = new List<double>();
                List<double> result_time3 = new List<double>();

                List<double> result1 = new List<double>();
                List<double> result2 = new List<double>();

                if (_csvs[idx].BasePath.Contains(filename[0]))
                {
                    for (int cameraid = 0; cameraid < 4; cameraid++)
                    {
                        x_offset.Clear();
                        y_offset.Clear();
                        angle_offset.Clear();
                        time.Clear();

                        for (int i = 0; i < _csvs[idx].DataCount; i++)
                        {
                            if (cameraid != int.Parse(_csvs[idx].GetData(i, "CAMERA_IDX")))
                            {
                                continue;
                            }

                            x_offset.Add(double.Parse(_csvs[idx].GetData(i, "x_offset")));
                            y_offset.Add(double.Parse(_csvs[idx].GetData(i, "y_offset")));
                            angle_offset.Add(double.Parse(_csvs[idx].GetData(i, "angle_offset")));

                            str = _csvs[idx].GetData(i, "time");
                            time.Add(TimeSpan.Parse(str).TotalSeconds);
                        }
                        if (cameraid == 0)
                        {
                            string create_key = filename[0] + string.Format("_{0}", cameraid);


                            if (chartData.ContainsKey(create_key))
                            {
                                chartData.Remove(create_key);
                            }

                            chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                            new List<double>(y_offset),
                                            new List<double>(angle_offset),
                                            new List<double>(time), cameraid));

                            chartView3[create_key].chartClearALL();
                            chartView3[create_key].SetChart(chartData[create_key]);
                        }
                        else if (cameraid == 1)
                        {
                            string create_key = filename[0] + string.Format("_{0}", cameraid);


                            if (chartData.ContainsKey(create_key))
                            {
                                chartData.Remove(create_key);
                            }

                            chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                            new List<double>(y_offset),
                                            new List<double>(angle_offset),
                                            new List<double>(time), cameraid));


                            chartView3[create_key].chartClearALL();
                            chartView3[create_key].SetChart(chartData[create_key]);
                        }


                        else if (cameraid == 2 || cameraid == 3)
                        {
                            string create_key = filename[0] + string.Format("_{0}", cameraid);

                            if (chartData.ContainsKey(create_key))
                            {
                                chartData.Remove(create_key);
                            }

                            chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                            new List<double>(y_offset),
                                            new List<double>(angle_offset),
                                            new List<double>(time), cameraid));

                            chartView3[create_key].chartClearALL();
                            chartView3[create_key].SetChart(chartData[create_key]);

                        }

                        for (int i = 0; i < y_offset.Count; i++)
                        {
                            if (cameraid == 0)
                            {
                                result_y_offset0.Add(y_offset[i]);
                            }

                            else if (cameraid == 1)
                            {
                                result_y_offset1.Add(y_offset[i]);
                            }

                            else if (cameraid == 2)
                            {
                                result_y_offset2.Add(y_offset[i]);
                            }

                            else if (cameraid == 3)
                            {
                                result_y_offset3.Add(y_offset[i]);
                            }
                        }

                        for (int i = 0; i < time.Count; i++)
                        {
                            if (cameraid == 0)
                            {
                                timecamera0.Add(time[i]);
                            }
                            else if (cameraid == 1)
                            {
                                timecamera1.Add(time[i]);
                            }
                            else if (cameraid == 2)
                            {
                                timecamera2.Add(time[i]);
                            }
                            else if (cameraid == 3)
                            {
                                timecamera3.Add(time[i]);
                            }
                        }

                        for (int i = 0; i < x_offset.Count; i++)
                        {
                            if (cameraid == 0)
                            {
                                x_offsetcamera0.Add(x_offset[i]);
                            }
                            else if (cameraid == 1)
                            {
                                x_offsetcamera1.Add(x_offset[i]);
                            }
                            else if (cameraid == 2)
                            {
                                x_offsetcamera2.Add(x_offset[i]);
                            }
                            else if (cameraid == 3)
                            {
                                x_offsetcamera3.Add(x_offset[i]);
                            }
                        }

                        //if (result_y_offset0.Count >= result_y_offset1.Count)
                        //    findbigone(result_y_offset0_1, result_y_offset0, result_y_offset1, timecamera0, timecamera1, true);
                        //else if (result_y_offset0.Count <= result_y_offset1.Count)
                        //    findbigone(result_y_offset0_1, result_y_offset1, result_y_offset0, timecamera1, timecamera0, false);
                        //
                        //if (result_y_offset2.Count >= result_y_offset3.Count)
                        //    findbigone(result_y_offset2_3, result_y_offset2, result_y_offset3, timecamera2, timecamera3, true);
                        //else if (result_y_offset2.Count <= result_y_offset3.Count)
                        //    findbigone(result_y_offset2_3, result_y_offset3, result_y_offset2, timecamera3, timecamera2, false);

                        if (result_y_offset0.Count >= result_y_offset1.Count)
                            findbigone(result_y_offset0_1, result_time1, x_offsetresult1, result_y_offset0, result_y_offset1, timecamera0, timecamera1, x_offsetcamera0, x_offsetcamera1, true);
                        else if (result_y_offset0.Count <= result_y_offset1.Count)
                            findbigone(result_y_offset0_1, result_time1, x_offsetresult1, result_y_offset1, result_y_offset0, timecamera1, timecamera0, x_offsetcamera1, x_offsetcamera0, false);

                        if (result_y_offset2.Count >= result_y_offset3.Count)
                            findbigone(result_y_offset2_3, result_time3, x_offsetresult3, result_y_offset2, result_y_offset3, timecamera2, timecamera3, x_offsetcamera2, x_offsetcamera3, true);
                        else if (result_y_offset2.Count <= result_y_offset3.Count)
                            findbigone(result_y_offset2_3, result_time3, x_offsetresult3, result_y_offset3, result_y_offset2, timecamera3, timecamera2, x_offsetcamera3, x_offsetcamera2, false);
                    }


                    int[] data = { result_y_offset0_1.Count, result_time1.Count, timecamera1.Count, x_offsetcamera1.Count, angle_offset.Count /* x_offsetcamera3.Count, timecamera3.Count*/ };
                    int min = Int32.MaxValue;
                    for (int i = 0; i < data.Length; i++)
                    {
                        if (min > data[i])
                        {
                            min = data[i];
                        }
                    }
                    for (int check = 0; check < min; check++)
                    {
                        result1.Add(result_y_offset0_1[check]);
                        timeresult1.Add(result_time1[check]);
                        x_offsetresult1.Add(x_offsetcamera1[check]);
                    }

                    int[] data2 = { result_y_offset2_3.Count, result_time3.Count, x_offsetcamera3.Count, angle_offset.Count };
                    int min2 = Int32.MaxValue;
                    for (int i = 0; i < data2.Length; i++)
                    {
                        if (min2 > data2[i])
                        {
                            min2 = data2[i];
                        }
                    }

                    for (int check = 0; check < min2; check++)
                    {
                        result2.Add(result_y_offset2_3[check]);
                        timeresult3.Add(result_time3[check]);
                        x_offsetresult3.Add(x_offsetcamera3[check]);
                    }

                    for (int cameraid = 0; cameraid < 4; cameraid++)
                    {

                        if (cameraid == 1)
                        {
                            string break_key = filename[0] + string.Format("_{0}", cameraid);

                            if (breakData.ContainsKey(break_key))
                            {
                                breakData.Remove(break_key);
                            }
                            //왜 인덱스 3번 값이 들어갈까?
                            breakData.Add(break_key, new BreakData(new List<double>(x_offsetresult1),
                                        new List<double>(result1),
                                        new List<double>(angle_offset),
                                        new List<double>(timeresult1), cameraid));

                            //breakData.Add(break_key, new BreakData(x_offset, result1, angle_offset, time, cameraid));
                            breakView[break_key].chartClearALL();
                            breakView[break_key].SetChart(breakData[break_key]);
                        }

                        else if (cameraid == 3)
                        {
                            string break_key = filename[0] + string.Format("_{0}", cameraid);

                            if (breakData.ContainsKey(break_key))
                            {
                                breakData.Remove(break_key);
                            }

                            breakData.Add(break_key, new BreakData(new List<double>(x_offsetresult3),
                                        new List<double>(result2),
                                        new List<double>(angle_offset),
                                        new List<double>(timeresult3), cameraid));

                            //breakData.Add(break_key, new BreakData(x_offset, result2, angle_offset, time, cameraid));
                            breakView[break_key].chartClearALL();
                            breakView[break_key].SetChart(breakData[break_key]);
                        }
                    }


                }
                else if (_csvs[idx].BasePath.Contains(filename[1]))
                {
                    x_offset.Clear();
                    y_offset.Clear();
                    angle_offset.Clear();
                    time.Clear();

                    for (int i = 0; i < _csvs[idx].DataCount; i++)
                    {
                        x_offset.Add(double.Parse(_csvs[idx].GetData(i, "x_offset")));
                        y_offset.Add(double.Parse(_csvs[idx].GetData(i, "y_offset")));
                        angle_offset.Add(double.Parse(_csvs[idx].GetData(i, "angle_offset")));

                        str = _csvs[idx].GetData(i, "time");
                        time.Add(TimeSpan.Parse(str).TotalSeconds);
                    }

                    string create_key = filename[1];

                    if (chartData.ContainsKey(create_key))
                    {
                        chartData.Remove(create_key);
                    }

                    chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                                            new List<double>(y_offset),
                                                            new List<double>(angle_offset),
                                                            new List<double>(time)));

                    chartView1[create_key].chartClearALL();
                    //chartView[create_key].chartClearALL();

                    chartView1[create_key].SetChart(chartData[create_key]);
                }
                else if (_csvs[idx].BasePath.Contains(filename[2]))
                {
                    x_offset.Clear();
                    y_offset.Clear();
                    angle_offset.Clear();
                    time.Clear();

                    for (int i = 0; i < _csvs[idx].DataCount; i++)
                    {
                        x_offset.Add(double.Parse(_csvs[idx].GetData(i, "x_offset")));
                        y_offset.Add(double.Parse(_csvs[idx].GetData(i, "y_offset")));
                        angle_offset.Add(double.Parse(_csvs[idx].GetData(i, "angle_offset")));

                        str = _csvs[idx].GetData(i, "time");
                        time.Add(TimeSpan.Parse(str).TotalSeconds);
                    }

                    string create_key = filename[2];

                    if (chartData.ContainsKey(create_key))
                    {
                        chartData.Remove(create_key);
                    }

                    chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                                            new List<double>(y_offset),
                                                            new List<double>(angle_offset),
                                                            new List<double>(time)));

                    chartView2[create_key].chartClearALL();

                    chartView2[create_key].SetChart(chartData[create_key]);
                }
                else if (_csvs[idx].BasePath.Contains(filename[3]))
                {
                    x_offset.Clear();
                    y_offset.Clear();
                    angle_offset.Clear();
                    time.Clear();

                    for (int i = 0; i < _csvs[idx].DataCount; i++)
                    {
                        x_offset.Add(double.Parse(_csvs[idx].GetData(i, "x_offset")));
                        y_offset.Add(double.Parse(_csvs[idx].GetData(i, "y_offset")));
                        angle_offset.Add(double.Parse(_csvs[idx].GetData(i, "angle_offset")));

                        str = _csvs[idx].GetData(i, "time");
                        time.Add(TimeSpan.Parse(str).TotalSeconds);
                    }

                    string create_key = filename[3];

                    if (chartData.ContainsKey(create_key))
                    {
                        chartData.Remove(create_key);
                    }

                    chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                                            new List<double>(y_offset),
                                                            new List<double>(angle_offset),
                                                            new List<double>(time)));

                    string key = filename[3];

                    chartView1[key].chartClearALL();

                    chartView1[key].SetChart(chartData[key]);
                }
                else if (_csvs[idx].BasePath.Contains(filename[4]))
                {
                    x_offset.Clear();
                    y_offset.Clear();
                    angle_offset.Clear();
                    time.Clear();

                    for (int i = 0; i < _csvs[idx].DataCount; i++)
                    {
                        x_offset.Add(double.Parse(_csvs[idx].GetData(i, "x_offset")));
                        y_offset.Add(double.Parse(_csvs[idx].GetData(i, "y_offset")));
                        angle_offset.Add(double.Parse(_csvs[idx].GetData(i, "angle_offset")));

                        str = _csvs[idx].GetData(i, "time");
                        time.Add(TimeSpan.Parse(str).TotalSeconds);
                    }

                    string create_key = filename[4];

                    if (chartData.ContainsKey(create_key))
                    {
                        chartData.Remove(create_key);
                    }

                    chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                                            new List<double>(y_offset),
                                                            new List<double>(angle_offset),
                                                            new List<double>(time)));

                    chartView2[create_key].chartClearALL();

                    chartView2[create_key].SetChart(chartData[create_key]);

                }
                else if (_csvs[idx].BasePath.Contains(filename[5]))
                {
                    for (int cameraid = 0; cameraid < 2; cameraid++)
                    {
                        x_offset.Clear();
                        y_offset.Clear();
                        angle_offset.Clear();
                        time.Clear();

                        for (int i = 0; i < _csvs[idx].DataCount; i++)
                        {
                            if (cameraid != int.Parse(_csvs[idx].GetData(i, "CAMERA_IDX")))
                            {
                                continue;
                            }

                            x_offset.Add(double.Parse(_csvs[idx].GetData(i, "x_offset")));
                            y_offset.Add(double.Parse(_csvs[idx].GetData(i, "y_offset")));
                            angle_offset.Add(double.Parse(_csvs[idx].GetData(i, "angle_offset")));

                            str = _csvs[idx].GetData(i, "time");
                            time.Add(TimeSpan.Parse(str).TotalSeconds);
                        }

                        string create_key = filename[5] + string.Format("_{0}", cameraid);

                        if (chartData.ContainsKey(create_key))
                        {
                            chartData.Remove(create_key);
                        }

                        chartData.Add(create_key, new ChartData(new List<double>(x_offset),
                                                                new List<double>(y_offset),
                                                                new List<double>(angle_offset),
                                                                new List<double>(time), cameraid));

                        chartView[create_key].chartClearALL();

                        chartView[create_key].SetChart(chartData[create_key]);
                    }
                }
            }
        }

        /// <summary>
        /// findbigone
        /// </summary>
        private void findbigone(List<double> saveindex, List<double> timeindex, List<double> xoffsetindex, List<double> index1, List<double> index2, List<double> time1, List<double> time2, List<double> xoffset1, List<double> xoffset2, bool whosfirst)
        {
            double check_time = 0;

            for (int i = 0; i < index1.Count; i++)
            {
                check_time = time1[i];

                for (int x = 0; x < index2.Count; x++)
                {
                    if (i + 10 >= x)
                    {
                        if (check_time == time2[x])
                            if (whosfirst == true)
                            {
                                saveindex.Add(index2[x] - index1[i]);
                                timeindex.Add(time1[i]);
                                xoffsetindex.Add(xoffset2[x]);
                            }
                            else if (whosfirst == false)
                            {
                                saveindex.Add(index1[i] - index2[x]);
                                timeindex.Add(time2[x]);
                                xoffsetindex.Add(xoffset1[i]);
                            }
                    }
                }
            }
        }

        /// ////////////////////////////////////////////////

        private void btnLoad_Click(object sender, EventArgs e)//해당 파일 읽기
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "CSV파일|*.csv";
            ofd.DefaultExt = ".csv";
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == DialogResult.OK)
            {

                _csvs.Clear();
                foreach (string file in ofd.FileNames)
                {
                    _csvs.Add(new FileCSV(file));
                    _csvs[_csvs.Count - 1].LoadFile();
                }
            }
            else
                return;

            //SortChart();
            fileOpen();
        }



        private enum FilePreName
        {
            Break,
            eLog_Find,
            Pre
        }

        private void btnLoadInDir_Click(object sender, EventArgs e)//해당 폴더 읽기
        {
            string dirpath = string.Empty;
            string[] targetNames = Enum.GetNames(typeof(FilePreName));

            Stopwatch sw = new Stopwatch();


            using (FolderBrowserDialog folderDlg = new FolderBrowserDialog())
            {

                folderDlg.RootFolder = Environment.SpecialFolder.MyComputer;
                if (folderDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    dirpath = folderDlg.SelectedPath;

                }
                else
                {
                    return;
                }
            }


            sw.Start();

            _csvs.Clear();
            foreach (string filePath in Directory.GetFiles(dirpath))
            {
                bool isTarget = false;
                foreach (string targetName in targetNames)
                {
                    if (Path.GetFileName(filePath).IndexOf(targetName) == 0)
                        isTarget |= true;
                }
                if (isTarget)
                {
                    _csvs.Add(new FileCSV(filePath));
                    _csvs[_csvs.Count - 1].LoadFile();
                };
            }
            fileOpen();
            sw.Stop();
            //lblLoadTime.Text = sw.ElapsedMilliseconds.ToString();
        }



        #region 주석처리
        //차트에다가 포인트 표시부분(브레이크 프리)
        //private void SortChartByCamIdx(Chart[] chartSet, FileCSV csv)
        //{
        //    double x = 0, y = 0;
        //    int camIdx = 0;

        //    for (int dataIdx = 0; dataIdx < csv.DataCount; ++dataIdx)//dataidx는 해당라인 읽으려고
        //    {
        //        if (int.TryParse(csv.GetData(dataIdx, "camera_idx"), out camIdx) == false)
        //        {
        //            Console.WriteLine("sort chart cam idx error");
        //        }

        //        if (0 <= camIdx && camIdx <= 3)
        //        {
        //            string fileName = Path.GetFileName(csv.BasePath);
        //            if (chartSet[camIdx].Series.IndexOf(fileName) == -1)
        //                chartSet[camIdx].Series.Add(fileName).ChartType = SeriesChartType.Point;

        //            if (rb_scale_x.Checked == true)
        //            {
        //                if (double.TryParse(csv.GetData(dataIdx, "x_offset"), out x) == false)
        //                {
        //                    Console.WriteLine("pre/break chart x data error");
        //                }
        //            }
        //            if (rb_vertical_y.Checked == true)
        //            {
        //                if (double.TryParse(csv.GetData(dataIdx, "y_offset"), out y) == false)
        //                {
        //                    Console.WriteLine("pre/break chart y data error");
        //                }
        //            }
        //            if (rb_vertical_x.Checked == true)
        //            {
        //                if (double.TryParse(csv.GetData(dataIdx, "x_offset"), out y) == false)
        //                {
        //                    Console.WriteLine("pre/break chart x data error");
        //                }
        //            }
        //            if (rb_vertical_t.Checked == true)
        //            {
        //                if (double.TryParse(csv.GetData(dataIdx, "angle_offset"), out y) == false)
        //                {
        //                    Console.WriteLine("pre/break chart t data error");
        //                }
        //            }
        //            if (rb_scale_time.Checked == true)
        //            {
        //                if (double.TryParse(csv.GetData(dataIdx, "time"), out x) == false)
        //                {
        //                    Console.WriteLine("pre/break chart time data error");
        //                }
        //            }

        //            chartSet[camIdx].Series[fileName].Points.AddXY(x, y);
        //        }
        //        else
        //            Console.WriteLine("pre/break chart cam idx error");
        //    }
        //}
        //private void SortFindChart(FileCSV csv)
        //{
        //    string fileName = Path.GetFileName(csv.BasePath);

        //    Chart targetChart = null;
        //    if (fileName.Contains("Left_A"))
        //        targetChart = chartFindLeftA;
        //    else if (fileName.Contains("Left_B"))
        //        targetChart = chartFindLeftB;
        //    else if (fileName.Contains("Right_A"))
        //        targetChart = chartFindRightA;
        //    else if (fileName.Contains("Right_B"))
        //        targetChart = chartFindRightB;
        //    else
        //        Console.WriteLine("find chart name error");

        //    if (targetChart == null)
        //        return;

        //    if (targetChart.Series.IndexOf(fileName) == -1)
        //        targetChart.Series.Add(fileName).ChartType = SeriesChartType.Point;

        //    double x = 0, y = 0;
        //    DateTime time = DateTime.Now;
        //    for (int dataIdx = 0; dataIdx < csv.DataCount; ++dataIdx)
        //    {
        //        if (rb_scale_x.Checked == true)
        //        {
        //            if (double.TryParse(csv.GetData(dataIdx, "x_offset"), out x) == false)
        //            {
        //                Console.WriteLine("pre/break chart x data error");
        //            }
        //        }

        //        if (rb_vertical_y.Checked == true)
        //        {
        //            if (double.TryParse(csv.GetData(dataIdx, "y_offset"), out y) == false)
        //            {
        //                Console.WriteLine("pre/break chart y data error");
        //            }
        //        }
        //        if (rb_vertical_x.Checked == true)
        //        {
        //            if (double.TryParse(csv.GetData(dataIdx, "x_offset"), out y) == false)
        //            {
        //                Console.WriteLine("pre/break chart x data error");
        //            }
        //        }
        //        if (rb_vertical_t.Checked == true)
        //        {
        //            if (double.TryParse(csv.GetData(dataIdx, "angle_offset"), out y) == false)
        //            {
        //                Console.WriteLine("pre/break chart t data error");
        //            }
        //        }
        //            targetChart.Series[fileName].Points.AddXY(x, y);
        //    }
        //}

        //이거 좀 애매
        private void ResizeChart(Chart chart)
        {
            chart.ChartAreas[0].RecalculateAxesScale();

            double[] size = new double[]
           {
           Math.Abs(chart.ChartAreas[0].AxisX.Minimum),
           Math.Abs(chart.ChartAreas[0].AxisX.Maximum),
           Math.Abs(chart.ChartAreas[0].AxisY.Minimum),
           Math.Abs(chart.ChartAreas[0].AxisY.Maximum),
           };

            double max = size.Max();//여기 민맥스 값 x별로 y별로 해야하는게 맞는듯
            max *= 1;
            //chart.ChartAreas[0].AxisX.Minimum = -max;
            //chart.ChartAreas[0].AxisX.Maximum = max;
            chart.ChartAreas[0].AxisY.Minimum = -max;
            chart.ChartAreas[0].AxisY.Maximum = max;
        }
        #endregion

        private void btnReadTheDay_Click(object sender, EventArgs e)
        {
            string dirpath = string.Empty;
            string[] targetNames = Enum.GetNames(typeof(FilePreName));
            //   string TODAY = DateTime.Today.ToString("yyyyMMdd");

            if (txtDirPath.Text == string.Empty || Directory.Exists(txtDirPath.Text) == false)
            {
                //MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
            }
            else
            {

                dirpath = txtDirPath.Text + "\\" + DateTime.Today.ToString("yyyyMMdd");
                dirpath = dirpath.Replace("\r\n", "");
            }

            if (Directory.Exists(dirpath) == false)
            {
                //MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
                return;
            }
            Stopwatch sw = new Stopwatch();

            _csvs.Clear();
            //chartClearALL();
            sw.Start();

            foreach (string filePath in Directory.GetFiles(dirpath))
            {
                bool isTarget = false;
                foreach (string targetName in targetNames)
                {
                    if (Path.GetFileName(filePath).IndexOf(targetName) == 0)
                        isTarget |= true;
                }
                if (isTarget)
                {
                    _csvs.Add(new FileCSV(filePath));
                    _csvs[_csvs.Count - 1].LoadFile();
                };
            }

            //SortChart();
            fileOpen();

            //Resizechart();
            sw.Stop();
            //lblLoadTime.Text = sw.ElapsedMilliseconds.ToString();
        }

        private void TodayView()
        {
            string dirpath = string.Empty;
            string[] targetNames = Enum.GetNames(typeof(FilePreName));
            //   string TODAY = DateTime.Today.ToString("yyyyMMdd");

            if (txtDirPath.Text == string.Empty || Directory.Exists(txtDirPath.Text) == false)
            {
                //MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
            }
            else
            {

                dirpath = txtDirPath.Text + "\\" + DateTime.Today.ToString("yyyyMMdd");
                dirpath = dirpath.Replace("\r\n", "");
            }

            if (Directory.Exists(dirpath) == false)
            {
                //MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
                return;
            }
            Stopwatch sw = new Stopwatch();

            _csvs.Clear();
            //chartClearALL();
            sw.Start();

            foreach (string filePath in Directory.GetFiles(dirpath))
            {
                bool isTarget = false;
                foreach (string targetName in targetNames)
                {
                    if (Path.GetFileName(filePath).IndexOf(targetName) == 0)
                        isTarget |= true;
                }
                if (isTarget)
                {
                    _csvs.Add(new FileCSV(filePath));
                    _csvs[_csvs.Count - 1].LoadFile();
                };
            }

            //SortChart();
            fileOpen();

            //Resizechart_All();
            sw.Stop();
            //lblLoadTime.Text = sw.ElapsedMilliseconds.ToString();
        }

        private void btn_Ini_LoadInDir_Click(object sender, EventArgs e)//기본폴더 설정
        {
            string dirpath = string.Empty;
            string[] targetNames = Enum.GetNames(typeof(FilePreName));


            using (FolderBrowserDialog folderDlg = new FolderBrowserDialog())
            {
                if (folderDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    dirpath = folderDlg.SelectedPath;

                }
                else
                {
                    return;
                }
            }

            txtDirPath.Text = dirpath;


            using (StreamWriter swPath = new StreamWriter(Path.Combine(Application.StartupPath, "CameraOffsetInfo" + ".ini")))
            {
                dirpath = dirpath.Replace("\r\n", "");
                swPath.WriteLine(dirpath);
                txtDirPath.Text = dirpath;
                swPath.Close();
                swPath.Dispose();
            }
        }

        private void btnReadB4Day_Click(object sender, EventArgs e)//전일버튼
        {

            string dirpath = string.Empty;
            string[] targetNames = Enum.GetNames(typeof(FilePreName));
            //   string TODAY = DateTime.Today.ToString("yyyyMMdd");

            if (txtDirPath.Text == string.Empty || Directory.Exists(txtDirPath.Text) == false)
            {
                //MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
            }
            else
            {

                dirpath = txtDirPath.Text + "\\" + DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
                dirpath = dirpath.Replace("\r\n", "");
            }

            if (Directory.Exists(dirpath) == false)
            {
                // MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
                return;
            }
            Stopwatch sw = new Stopwatch();


            _csvs.Clear();
            //chartClearALL();
            sw.Start();
            foreach (string filePath in Directory.GetFiles(dirpath))
            {
                bool isTarget = false;
                foreach (string targetName in targetNames)
                {
                    if (Path.GetFileName(filePath).IndexOf(targetName) == 0)
                        isTarget |= true;
                }
                if (isTarget)
                {
                    _csvs.Add(new FileCSV(filePath));
                    _csvs[_csvs.Count - 1].LoadFile();
                };
            }

            //SortChart();//여기서 차트에다가 표시
            fileOpen();
            //Resizechart_All();

            sw.Stop();
            //lblLoadTime.Text = sw.ElapsedMilliseconds.ToString();


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Btn_BeforNday_Click(object sender, EventArgs e)//몇일만큼 더 볼건지 당일기준
        {
            int days = Comb_Befor_Days.SelectedIndex;
            string dirpath = string.Empty;
            string[] targetNames = Enum.GetNames(typeof(FilePreName));
            //   string TODAY = DateTime.Today.ToString("yyyyMMdd");


            if (txtDirPath.Text == string.Empty || Directory.Exists(txtDirPath.Text) == false)
            {
                //MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
                return;
            }
            //    else
            //    {

            Stopwatch sw = new Stopwatch();

            _csvs.Clear();

            //chartClearALL();

            //    }
            sw.Start();
            for (int k = 0; k <= days; ++k)
            {
                dirpath = txtDirPath.Text + "\\" + DateTime.Today.AddDays(-k).ToString("yyyyMMdd");
                dirpath = dirpath.Replace("\r\n", "");
                if (Directory.Exists(dirpath) == false)
                {
                    MessageBox.Show(DateTime.Today.AddDays(-k).ToString("yyyy-MM-dd") + " 자료가 없습니다. \r\n 기본 폴더 설정 혹은 작업 일자 확인하시기 바랍니다.");
                    //Resizechart_All();
                    return;

                }

                foreach (string filePath in Directory.GetFiles(dirpath))
                {
                    bool isTarget = false;
                    foreach (string targetName in targetNames)
                    {
                        if (Path.GetFileName(filePath).IndexOf(targetName) == 0)
                            isTarget |= true;
                    }
                    if (isTarget)
                    {
                        _csvs.Add(new FileCSV(filePath));
                        _csvs[_csvs.Count - 1].LoadFile();
                    };
                }

                fileOpen();
            }

            //Resizechart_All();
            sw.Stop();
            //lblLoadTime.Text = sw.ElapsedMilliseconds.ToString();

        }

        private void Select_dateTimePicker_ValueChanged(object sender, EventArgs e)//해당날짜 보려면
        {
            ReadTheDay();

        }


        //해당날짜 보려면
        void ReadTheDay()
        {
            string strDate1 = Select_dateTimePicker.Value.ToString("yyyyMMdd");
            //string DateTime1 = DateTime.Now.ToString("yyyyMMdd");

            string dirpath = string.Empty;
            string[] targetNames = Enum.GetNames(typeof(FilePreName));
            //   string TODAY = DateTime.Today.ToString("yyyyMMdd");


            if (txtDirPath.Text == string.Empty || Directory.Exists(txtDirPath.Text) == false)
            {
                MessageBox.Show("기본 리딩 폴더 위치를 먼저 설정 하셔요");
                return;
            }

            //if (strDate1 == DateTime1)
            //{
            //    MessageBox.Show("오늘 날짜 파일은 열수 없습니다.");
            //    return;
            //}
            Stopwatch sw = new Stopwatch();

            _csvs.Clear();
            //chartClearALL();

            sw.Start();
            //    }

            dirpath = txtDirPath.Text + "\\" + strDate1;
            dirpath = dirpath.Replace("\r\n", "");
            if (Directory.Exists(dirpath) == false)
            {
                MessageBox.Show(strDate1 + " 자료가 없습니다. \r\n 기본 폴더 설정 혹은 작업 일자 확인하시기 바랍니다");
                return;

            }

            foreach (string filePath in Directory.GetFiles(dirpath))
            {
                bool isTarget = false;
                foreach (string targetName in targetNames)
                {
                    if (Path.GetFileName(filePath).IndexOf(targetName) == 0)
                        isTarget |= true;
                }
                if (isTarget)
                {
                    _csvs.Add(new FileCSV(filePath));
                    _csvs[_csvs.Count - 1].LoadFile();
                };

            }

            fileOpen();
            sw.Stop();
            //lblLoadTime.Text = sw.ElapsedMilliseconds.ToString();
        }
    }
}
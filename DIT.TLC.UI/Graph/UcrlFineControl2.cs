﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DIT.TLC.UI
{
    public partial class UcrlFineControl2 : UserControl
    {
        ChartData _ctData;

        public UcrlFineControl2()
        {
            InitializeComponent();

            chart1.ChartAreas[0].AxisY.Maximum = 3;
            chart1.ChartAreas[0].AxisY.Minimum = -3;
            chart1.ChartAreas[0].AxisY.Interval = 1.5;
            chart1.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

            chart2.ChartAreas[0].AxisY.Maximum = 2;
            chart2.ChartAreas[0].AxisY.Minimum = -2;
            chart2.ChartAreas[0].AxisY.Interval = 1;
            chart2.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

            chart3.ChartAreas[0].AxisY.Maximum = 0.4;
            chart3.ChartAreas[0].AxisY.Minimum = -0.4;
            chart3.ChartAreas[0].AxisY.Interval = 0.2;
            chart3.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

            chart4.ChartAreas[0].AxisY.Maximum = 2;
            chart4.ChartAreas[0].AxisY.Minimum = -2;
            chart4.ChartAreas[0].AxisY.Interval = 1;
            chart4.ChartAreas[0].AxisX.Maximum = 3;
            chart4.ChartAreas[0].AxisX.Minimum = -3;
            chart4.ChartAreas[0].AxisX.Interval = 1.5;
            chart4.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

            chartClearALL();
        }

        public void SetChart(ChartData ctdata)
        {
            _ctData = ctdata;

            SetChart(1, _ctData.time, _ctData.x_offset);
            SetChart(2, _ctData.time, _ctData.y_offset);
            SetChart(3, _ctData.time, _ctData.angle_offset);
            SetChart(4, _ctData.x_offset, _ctData.y_offset);
        }

        private void SetChart(int idx, List<double> xAxis, List<double> yAxis)
        {
            Chart chart;

            if (idx == 1)
            {
                chart = chart1;
            }
            else if (idx == 2)
            {
                chart = chart2;
            }
            else if (idx == 3)
            {
                chart = chart3;
            }
            else if (idx == 4)
            {
                chart = chart4;
            }
            else
                return;

            chart.Series.Clear();
            chart.Series.Add("series");
            chart.Series[0].Points.DataBindXY(xAxis, yAxis);
            chart.Series[0].ChartType = SeriesChartType.Point;
        }

        private void chart1_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart1, _ctData);
            frm.Text = "TIME - X_OFFSET";
            frm.Show();
        }

        private void chart2_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart2, _ctData);
            frm.Text = " TIME - Y_OFFSET";
            frm.Show();
        }

        private void chart3_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart3, _ctData);
            frm.Text = " TIME - THETA";
            frm.Show();
        }

        private void chart4_DoubleClick(object sender, EventArgs e)
        {
            FrmChartView frm = new FrmChartView(chart4, _ctData);
            frm.Text = " X_OFFSET - Y_OFFSET";
            frm.Show();
        }


        public void chartClearALL()
        {
            chart1.Series.Clear();
            chart2.Series.Clear();
            chart3.Series.Clear();
            chart4.Series.Clear();

        }
    }
}
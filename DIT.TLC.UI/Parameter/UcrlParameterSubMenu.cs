﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlParameterSubMenu : UserControl, IUIUpdate
    {
        private UcrlParameterServoPositionSetting _ucrlParameterLocation = new UcrlParameterServoPositionSetting();                       // 위치 파라미터 UI
        private UcrlParameterSequenceOffset _ucrlParameterSequenceOffset = new UcrlParameterSequenceOffset();     // 시퀀스 offset UI
        private UcrlParameterAxis _ucrlParameterAxis = new UcrlParameterAxis();                                     // 축 파라미터 UI
        private UcrlParameterSystem _ucrlParameterSystem = new UcrlParameterSystem();                             // 시스템 파라미터 UI
        private UcrlParameterAlarm _ucrlParameterAlarm = new UcrlParameterAlarm();                                 // 알람 셋팅 UI
        private UcrlParameterUserManager _ucrlParameterUserManager = new UcrlParameterUserManager();              // 사용자 매니저 UI

        public UcrlParameterSubMenu()
        {
            InitializeComponent();

            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlParameterLocation);
            _ucrlParameterLocation.Location = new Point(0, 0);
        }

        // 서브메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void SubMenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = SystemColors.Control;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        /// <summary>
        /// 위치 파라미터 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLocation_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlParameterLocation);
            _ucrlParameterLocation.Location = new Point(0, 0);
            //_ucrlParameterLocation.Initailzie();
            SubMenuButtonColorChange(sender, e);
            
        }

        /// <summary>
        /// 시퀀스 offset 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSequenceOffset_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlParameterSequenceOffset);
            _ucrlParameterSequenceOffset.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 축 파라미터 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAxis_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlParameterAxis);
            _ucrlParameterAxis.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 시스템 파라미터 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSystem_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlParameterSystem);
            _ucrlParameterSystem.Location = new Point(0, 0);
            SystemParamSetting sysparam = GG.Equip.EqpSysParamMgr.LstEqpSysParam;
            SystemParamSkip sysparamskip = GG.Equip.EqpSysMgr.Params;
            _ucrlParameterSystem.FillSettingData(sysparam, sysparamskip);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 알람 셋팅 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlarmSetting_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlParameterAlarm);
            _ucrlParameterAlarm.Location = new Point(0, 0);
            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 사용자 매니저 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUserManager_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlParameterUserManager);
            _ucrlParameterUserManager.Location = new Point(0, 0);
            _ucrlParameterUserManager.Initizlie();
            SubMenuButtonColorChange(sender, e);
        }

        public void UIUpdate()
        {
            foreach (var ctrl in splitContainer1.Panel1.Controls)
            {
                IUIUpdate vv = ctrl as IUIUpdate;
                if (vv != null)
                    vv.UIUpdate();
            }
        }
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlParameterServoPositionSetting
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCstLoad = new System.Windows.Forms.Button();
            this.btnCellOutTransfer = new System.Windows.Forms.Button();
            this.btnCellLoadTransfer01 = new System.Windows.Forms.Button();
            this.btnIRCutProcess = new System.Windows.Forms.Button();
            this.btnAfterIRCutTransfer = new System.Windows.Forms.Button();
            this.btnBreakUnit01 = new System.Windows.Forms.Button();
            this.btnBreakUnit02 = new System.Windows.Forms.Button();
            this.btnBreakHeadUnitXZ = new System.Windows.Forms.Button();
            this.btnBeforeTransfer = new System.Windows.Forms.Button();
            this.btnInspStage = new System.Windows.Forms.Button();
            this.btnAfterTransfer = new System.Windows.Forms.Button();
            this.btnCameraUnit = new System.Windows.Forms.Button();
            this.btnCellInTransfer = new System.Windows.Forms.Button();
            this.btnCstUnloader = new System.Windows.Forms.Button();
            this.btnCellInputSpeedSetting = new System.Windows.Forms.Button();
            this.btnCellInputAllSetting = new System.Windows.Forms.Button();
            this.btnCellInputLocationSetting = new System.Windows.Forms.Button();
            this.btnCellInputMoveLocation = new System.Windows.Forms.Button();
            this.txtSelectedServoNoSetPosi = new System.Windows.Forms.TextBox();
            this.txtSelectedServoNoSetSpeed = new System.Windows.Forms.TextBox();
            this.txtSelectedServoNoCurrPosi = new System.Windows.Forms.TextBox();
            this.txtSelectedServoNo = new System.Windows.Forms.TextBox();
            this.lbl_CellInput_Location = new System.Windows.Forms.Label();
            this.lbl_CellInput_Speed = new System.Windows.Forms.Label();
            this.lbl_CellInput_SelectedShaft = new System.Windows.Forms.Label();
            this.lbl_CellInput_CurrentLocation = new System.Windows.Forms.Label();
            this.dgvSevoPosi = new System.Windows.Forms.DataGridView();
            this.posName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aixs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCasseteLoadSave = new System.Windows.Forms.Button();
            this.btnLDCstBGripUn = new System.Windows.Forms.Button();
            this.btnLDCstBGrip = new System.Windows.Forms.Button();
            this.btnLDCstAGripUn = new System.Windows.Forms.Button();
            this.btnLDCstAGrip = new System.Windows.Forms.Button();
            this.btnCstloadBMeasure = new System.Windows.Forms.Button();
            this.btnCstloadBLoader = new System.Windows.Forms.Button();
            this.btnCstloadB90dgr = new System.Windows.Forms.Button();
            this.btnCstloadB0dgr = new System.Windows.Forms.Button();
            this.btnLoaderAxisX2BlowerOnOff = new System.Windows.Forms.Button();
            this.btnLoaderAxisX2VaccumOff = new System.Windows.Forms.Button();
            this.btnLoaderAxisX2VaccumOn = new System.Windows.Forms.Button();
            this.btnLoaderAxisX1BlowerOnOff = new System.Windows.Forms.Button();
            this.btnLoaderAxisX1VaccumOff = new System.Windows.Forms.Button();
            this.btnLoaderAxisX1VaccumOn = new System.Windows.Forms.Button();
            this.btnCellLoadX3LD = new System.Windows.Forms.Button();
            this.btnCellLoadX2LD = new System.Windows.Forms.Button();
            this.btnCellLoadX1LD = new System.Windows.Forms.Button();
            this.btnCellLoadBMcrReading = new System.Windows.Forms.Button();
            this.btnCellLoadBVisionAlign = new System.Windows.Forms.Button();
            this.btnCellLoadAMcrReading = new System.Windows.Forms.Button();
            this.btnCellLoadAVisionAlign = new System.Windows.Forms.Button();
            this.btnCellLoadB1PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnCellLoadB1PickerUp = new System.Windows.Forms.Button();
            this.btnCellLoadB1PickerDown = new System.Windows.Forms.Button();
            this.btnCellLoadB1PickerVaccumOn = new System.Windows.Forms.Button();
            this.btnCellLoadB1PickerVaccumOff = new System.Windows.Forms.Button();
            this.btnCellLoadA1PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnCellLoadA1PickerUp = new System.Windows.Forms.Button();
            this.btnCellLoadA1PickerDown = new System.Windows.Forms.Button();
            this.btnCellLoadA1PickerVaccumOn = new System.Windows.Forms.Button();
            this.btnBreakTRBULD = new System.Windows.Forms.Button();
            this.btnBreakTRBLD = new System.Windows.Forms.Button();
            this.btnBreakTRAULD = new System.Windows.Forms.Button();
            this.btnBreakTRALD = new System.Windows.Forms.Button();
            this.btnBreakTransferB1PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakTransferB1PickerUp = new System.Windows.Forms.Button();
            this.btnBreakTransferB1PickerDown = new System.Windows.Forms.Button();
            this.btnBreakTransferB1PickerVaccumOn = new System.Windows.Forms.Button();
            this.btnBreakTransferB1PickerVaccumOff = new System.Windows.Forms.Button();
            this.btnBreakTransferA1PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakTransferA1PickerUp = new System.Windows.Forms.Button();
            this.btnBreakTransferA1PickerDown = new System.Windows.Forms.Button();
            this.btnBreakTransferA1PickerVaccumOn = new System.Windows.Forms.Button();
            this.btnBreakTransferA1PickerVaccumOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessB1LaserShot = new System.Windows.Forms.Button();
            this.btnIRCutProcessB2LaserShot = new System.Windows.Forms.Button();
            this.btnIRCutProcessA1LaserShot = new System.Windows.Forms.Button();
            this.btnIRCutProcessA2LaserShot = new System.Windows.Forms.Button();
            this.btnIRCutProcessAFineAlign = new System.Windows.Forms.Button();
            this.btnIRCutProcessBFineAlign = new System.Windows.Forms.Button();
            this.btnIRCutProcessBCamera = new System.Windows.Forms.Button();
            this.btnIRCutProcessBCellLoad = new System.Windows.Forms.Button();
            this.btnIRCutProcessBCellUnload = new System.Windows.Forms.Button();
            this.btnIRCutProcessBLaser = new System.Windows.Forms.Button();
            this.btnIRCutProcessACamera = new System.Windows.Forms.Button();
            this.btnIRCutProcessACellLoad = new System.Windows.Forms.Button();
            this.btnIRCutProcessACellUnload = new System.Windows.Forms.Button();
            this.btnIRCutProcessALaser = new System.Windows.Forms.Button();
            this.btnIRCutProcessB1VaccumCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessB2VaccumCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessB2VaccumCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessB1VaccumCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessB2VaccumCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessB2VaccumCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessB1VaccumCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessB1VaccumCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessB1BlowerCh1OnOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessB2BlowerCh1OnOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessB1BlowerCh2OnOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessB2BlowerCh2OnOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessA1BlowerCh2OnOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessA2BlowerCh2OnOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessA1VaccumCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessA2VaccumCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessA2VaccumCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessA1VaccumCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessA2VaccumCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessA2VaccumCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessA1VaccumCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessA1VaccumCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessA1BlowerCh1OnOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessA2BlowerCh1OnOff = new System.Windows.Forms.Button();
            this.btnBreakZ1Hold = new System.Windows.Forms.Button();
            this.btnBreakZ1BreakingPos = new System.Windows.Forms.Button();
            this.btnBreakXZAAlignMeasure = new System.Windows.Forms.Button();
            this.btnBreakXZBAlignMeasure = new System.Windows.Forms.Button();
            this.btnBreakUnitTYShutterClose = new System.Windows.Forms.Button();
            this.btnBreakUnitTYShutterOpen = new System.Windows.Forms.Button();
            this.btnBreakUnitTYDummyBoxOutput = new System.Windows.Forms.Button();
            this.btnBreakUnitTYDummyBoxInput = new System.Windows.Forms.Button();
            this.btnBreakUnitTYAULD = new System.Windows.Forms.Button();
            this.btnBreakUnitTYALD = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB1BlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB2BlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB1VaccumOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB1VaccumOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB2VaccumOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB2VaccumOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA1BlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA2BlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA1VaccumOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA1VaccumOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA2VaccumOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA2VaccumOff = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_Muting4 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_Muting2 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_Muting3 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_Muting1 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MutingOff = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MutingIn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel66 = new System.Windows.Forms.Panel();
            this.btnCstloadBCellOut = new System.Windows.Forms.Button();
            this.label65 = new System.Windows.Forms.Label();
            this.panel67 = new System.Windows.Forms.Panel();
            this.btnCstloadAMeasure = new System.Windows.Forms.Button();
            this.btnCstloadALoader = new System.Windows.Forms.Button();
            this.btnCstloadA0dgr = new System.Windows.Forms.Button();
            this.btnCstloadA90dgr = new System.Windows.Forms.Button();
            this.btnCstloadACellOut = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.panel62 = new System.Windows.Forms.Panel();
            this.btnLDTiltACylinderDown = new System.Windows.Forms.Button();
            this.btnLDTiltACylinderUp = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.btnLDTiltBCylinderDown = new System.Windows.Forms.Button();
            this.btnLDTiltBCylinderUp = new System.Windows.Forms.Button();
            this.label63 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.btnLoaderY2AxisCstMiddle = new System.Windows.Forms.Button();
            this.btnLoaderY2AxisCstAUnloading = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.btnLoaderY2AxisCstBInner = new System.Windows.Forms.Button();
            this.btnLoaderY2AxisCstBUnloading = new System.Windows.Forms.Button();
            this.btnLoaderY2AxisCstAInner = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnLoaderY1AxisCstMiddle = new System.Windows.Forms.Button();
            this.btnLoaderY1AxisCstAUnloading = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnLoaderY1AxisCstBInner = new System.Windows.Forms.Button();
            this.btnLoaderY1AxisCstBUnloading = new System.Windows.Forms.Button();
            this.btnLoaderY1AxisCstAInner = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnLoaderAxisX2Cst_B_Wait = new System.Windows.Forms.Button();
            this.btnLoaderAxisX2Cst_B_Middle = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnLoaderAxisX2Cst_B_Unloading = new System.Windows.Forms.Button();
            this.btnLoaderAxisX2Cst_A_Unloading = new System.Windows.Forms.Button();
            this.btnLoaderAxisX2Cst_A_Wait = new System.Windows.Forms.Button();
            this.btnLoaderAxisX2Cst_A_Middle = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnLoaderAxisX1Cst_B_Wait = new System.Windows.Forms.Button();
            this.btnLoaderAxisX1Cst_B_Middle = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLoaderAxisX1Cst_B_Unloading = new System.Windows.Forms.Button();
            this.btnLoaderAxisX1Cst_A_Unloading = new System.Windows.Forms.Button();
            this.btnLoaderAxisX1Cst_A_Wait = new System.Windows.Forms.Button();
            this.btnLoaderAxisX1Cst_A_Middle = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel26 = new System.Windows.Forms.Panel();
            this.btnCellLoadB2PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnCellLoadB2PickerDown = new System.Windows.Forms.Button();
            this.btnCellLoadB2PickerUp = new System.Windows.Forms.Button();
            this.btnCellLoadB2PickerVaccumOff = new System.Windows.Forms.Button();
            this.btnCellLoadB2PickerVaccumOn = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.btnCellLoadA2PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnCellLoadA2PickerDown = new System.Windows.Forms.Button();
            this.btnCellLoadA2PickerUp = new System.Windows.Forms.Button();
            this.btnCellLoadA2PickerVaccumOff = new System.Windows.Forms.Button();
            this.btnCellLoadA2PickerVaccumOn = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.btnCellLoadA1PickerVaccumOff = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel84 = new System.Windows.Forms.Panel();
            this.btnPreAlignB = new System.Windows.Forms.Button();
            this.btnPreAlignA = new System.Windows.Forms.Button();
            this.label84 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.btnCellLoadBPickerMinus90 = new System.Windows.Forms.Button();
            this.btnCellLoadBPickerPlus90 = new System.Windows.Forms.Button();
            this.btnCellLoadBPicker0 = new System.Windows.Forms.Button();
            this.btnCellLoadBCellULD = new System.Windows.Forms.Button();
            this.btnCellLoadBCellLD = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.btnCellLoadX4ULD = new System.Windows.Forms.Button();
            this.btnCellLoadX4PreAlign = new System.Windows.Forms.Button();
            this.btnCellLoadX4LD = new System.Windows.Forms.Button();
            this.btnCellLoadX3ULD = new System.Windows.Forms.Button();
            this.btnCellLoadX1ULD = new System.Windows.Forms.Button();
            this.btnCellLoadX2ULD = new System.Windows.Forms.Button();
            this.btnCellLoadX3PreAlign = new System.Windows.Forms.Button();
            this.btnCellLoadX1PreAlign = new System.Windows.Forms.Button();
            this.btnCellLoadX2PreAlign = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.btnCellLoadAPickerMinus90 = new System.Windows.Forms.Button();
            this.btnCellLoadAPickerPlus90 = new System.Windows.Forms.Button();
            this.btnCellLoadAPicker0 = new System.Windows.Forms.Button();
            this.btnCellLoadACellULD = new System.Windows.Forms.Button();
            this.btnCellLoadACellLD = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel48 = new System.Windows.Forms.Panel();
            this.btnIRCut2BrushDown = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.btnIRCut1BrushDown = new System.Windows.Forms.Button();
            this.btnIRCut2BrushUp = new System.Windows.Forms.Button();
            this.btnIRCut1BrushUp = new System.Windows.Forms.Button();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel83 = new System.Windows.Forms.Panel();
            this.btnIRCutProcessZPowerMeter = new System.Windows.Forms.Button();
            this.label83 = new System.Windows.Forms.Label();
            this.btnIRCutProcessZLaserShot = new System.Windows.Forms.Button();
            this.panel36 = new System.Windows.Forms.Panel();
            this.btnIRCutProcessPowerMeterAutoMeasure = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.btnBreakTransferA2PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakTransferA2PickerUp = new System.Windows.Forms.Button();
            this.btnBreakTransferA2PickerVaccumOff = new System.Windows.Forms.Button();
            this.btnBreakTransferA2PickerDown = new System.Windows.Forms.Button();
            this.btnBreakTransferA2PickerVaccumOn = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.btnBreakTransferB2PickerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBreakTransferB2PickerUp = new System.Windows.Forms.Button();
            this.btnBreakTransferB2PickerVaccumOff = new System.Windows.Forms.Button();
            this.btnBreakTransferB2PickerDown = new System.Windows.Forms.Button();
            this.btnBreakTransferB2PickerVaccumOn = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel71 = new System.Windows.Forms.Panel();
            this.btnBreakBBrushDown = new System.Windows.Forms.Button();
            this.label71 = new System.Windows.Forms.Label();
            this.btnBreakABrushDown = new System.Windows.Forms.Button();
            this.btnBreakBBrushUp = new System.Windows.Forms.Button();
            this.btnBreakABrushUp = new System.Windows.Forms.Button();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.btnBreakX1BreakAlign = new System.Windows.Forms.Button();
            this.btnBreakX1JigAlign = new System.Windows.Forms.Button();
            this.btnBreakXZB2LDS = new System.Windows.Forms.Button();
            this.btnBreakXZB1LDS = new System.Windows.Forms.Button();
            this.btnBreakXZA2LDS = new System.Windows.Forms.Button();
            this.btnBreakXZA1LDS = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.btnBreakUnitTYMeasure4 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYHold4 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYMeasure3 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYMeasure2 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYHold3 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYHold2 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYMeasure1 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYHold1 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.btnBreakUnitTYBBreaking = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.btnBreakUnitTYBAlignCam = new System.Windows.Forms.Button();
            this.btnBreakUnitTYBULD = new System.Windows.Forms.Button();
            this.btnBreakUnitTYBLD = new System.Windows.Forms.Button();
            this.panel56 = new System.Windows.Forms.Panel();
            this.btnBreakUnitTYABreaking = new System.Windows.Forms.Button();
            this.btnBreakUnitTYAAlignCam = new System.Windows.Forms.Button();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.panel50 = new System.Windows.Forms.Panel();
            this.label50 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.panel74 = new System.Windows.Forms.Panel();
            this.panel76 = new System.Windows.Forms.Panel();
            this.btnBZDown3 = new System.Windows.Forms.Button();
            this.btnBZDown1 = new System.Windows.Forms.Button();
            this.btnAZDown3 = new System.Windows.Forms.Button();
            this.btnBZJigAlign = new System.Windows.Forms.Button();
            this.btnAZDown1 = new System.Windows.Forms.Button();
            this.label75 = new System.Windows.Forms.Label();
            this.btnAZJigAlign = new System.Windows.Forms.Button();
            this.panel77 = new System.Windows.Forms.Panel();
            this.btnB2Breaking = new System.Windows.Forms.Button();
            this.btnB1Breaking = new System.Windows.Forms.Button();
            this.btnA2Breaking = new System.Windows.Forms.Button();
            this.btnA1Breaking = new System.Windows.Forms.Button();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel73 = new System.Windows.Forms.Panel();
            this.btnBeforeTr2Insp = new System.Windows.Forms.Button();
            this.label72 = new System.Windows.Forms.Label();
            this.btnBeforeTr2A0dgr = new System.Windows.Forms.Button();
            this.btnBeforeTr2Loading = new System.Windows.Forms.Button();
            this.btnBeforeTr2A90dgr = new System.Windows.Forms.Button();
            this.btnBeforeTr2A90rdgr = new System.Windows.Forms.Button();
            this.panel75 = new System.Windows.Forms.Panel();
            this.btnBeforeTr1Insp = new System.Windows.Forms.Button();
            this.label73 = new System.Windows.Forms.Label();
            this.btnBeforeTr1A0dgr = new System.Windows.Forms.Button();
            this.btnBeforeTr1Loading = new System.Windows.Forms.Button();
            this.btnBeforeTr1A90dgr = new System.Windows.Forms.Button();
            this.btnBeforeTr1A90rdgr = new System.Windows.Forms.Button();
            this.label74 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.btnBeforeB2PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBeforeB2PikerDown = new System.Windows.Forms.Button();
            this.btnBeforeB1PikerUp = new System.Windows.Forms.Button();
            this.btnBeforeB1PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBeforeB1PikerDown = new System.Windows.Forms.Button();
            this.btnBeforeB2PikerVaccumOff = new System.Windows.Forms.Button();
            this.btnBeforeB2PikerUp = new System.Windows.Forms.Button();
            this.btnBeforeB2PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnBeforeB1PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnBeforeB1PikerVaccumOff = new System.Windows.Forms.Button();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.btnBeforeA2PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBeforeA2PikerDown = new System.Windows.Forms.Button();
            this.btnBeforeA1PikerUp = new System.Windows.Forms.Button();
            this.btnBeforeA1PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnBeforeA1PikerDown = new System.Windows.Forms.Button();
            this.btnBeforeA2PikerVaccumOff = new System.Windows.Forms.Button();
            this.btnBeforeA2PikerUp = new System.Windows.Forms.Button();
            this.btnBeforeA2PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnBeforeA1PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnBeforeA1PikerVaccumOff = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.panel79 = new System.Windows.Forms.Panel();
            this.panel80 = new System.Windows.Forms.Panel();
            this.btnInspStBULD = new System.Windows.Forms.Button();
            this.label79 = new System.Windows.Forms.Label();
            this.btnInspStBInsp = new System.Windows.Forms.Button();
            this.btnInspStBLD = new System.Windows.Forms.Button();
            this.panel81 = new System.Windows.Forms.Panel();
            this.btnInspStAULD = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this.btnInspStAInsp = new System.Windows.Forms.Button();
            this.btnInspStALD = new System.Windows.Forms.Button();
            this.label81 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.btnInspStB1BlowerOnOff = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.btnInspStB2VacOff = new System.Windows.Forms.Button();
            this.btnInspStB2VacOn = new System.Windows.Forms.Button();
            this.btnInspStB2BlowerOnOff = new System.Windows.Forms.Button();
            this.btnInspStB1VacOn = new System.Windows.Forms.Button();
            this.btnInspStB1VacOff = new System.Windows.Forms.Button();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.btnInspStA1VacOn = new System.Windows.Forms.Button();
            this.btnInspStA1BlowerOnOff = new System.Windows.Forms.Button();
            this.btnInspStA2VacOn = new System.Windows.Forms.Button();
            this.btnInspStA2VacOff = new System.Windows.Forms.Button();
            this.btnInspStA1VacOff = new System.Windows.Forms.Button();
            this.btnInspStA2BlowerOnOff = new System.Windows.Forms.Button();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.btnAfterTr2Insp = new System.Windows.Forms.Button();
            this.btnAfterTr2Unloading = new System.Windows.Forms.Button();
            this.btnAfterTr2B0dgr = new System.Windows.Forms.Button();
            this.btnAfterTr2B90dgr = new System.Windows.Forms.Button();
            this.label58 = new System.Windows.Forms.Label();
            this.btnAfterTr2B90rdgr = new System.Windows.Forms.Button();
            this.panel65 = new System.Windows.Forms.Panel();
            this.btnAfterTr1Insp = new System.Windows.Forms.Button();
            this.btnAfterTr1Unloading = new System.Windows.Forms.Button();
            this.btnAfterTr1A0dgr = new System.Windows.Forms.Button();
            this.btnAfterTr1A90dgr = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.btnAfterTr1A90rdgr = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.btnAfterB2PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnAfterB2PikerDown = new System.Windows.Forms.Button();
            this.btnAfterB1PikerUp = new System.Windows.Forms.Button();
            this.btnAfterB1PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnAfterB1PikerDown = new System.Windows.Forms.Button();
            this.btnAfterB2PikerVaccumOff = new System.Windows.Forms.Button();
            this.btnAfterB2PikerUp = new System.Windows.Forms.Button();
            this.btnAfterB2PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnAfterB1PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnAfterB1PikerVaccumOff = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.btnAfterA2PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnAfterA2PikerDown = new System.Windows.Forms.Button();
            this.btnAfterA1PikerUp = new System.Windows.Forms.Button();
            this.btnAfterA1PikerBlowerOnOff = new System.Windows.Forms.Button();
            this.btnAfterA1PikerDown = new System.Windows.Forms.Button();
            this.btnAfterA2PikerVaccumOff = new System.Windows.Forms.Button();
            this.btnAfterA2PikerUp = new System.Windows.Forms.Button();
            this.btnAfterA2PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnAfterA1PikerVaccumOn = new System.Windows.Forms.Button();
            this.btnAfterA1PikerVaccumOff = new System.Windows.Forms.Button();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel82 = new System.Windows.Forms.Panel();
            this.btnCamUnitB2 = new System.Windows.Forms.Button();
            this.btnCamUnitB1 = new System.Windows.Forms.Button();
            this.btnCamUnitA2 = new System.Windows.Forms.Button();
            this.label82 = new System.Windows.Forms.Label();
            this.btnCamUnitA1 = new System.Windows.Forms.Button();
            this.panel78 = new System.Windows.Forms.Panel();
            this.btnCamUnitBInspUpEnd = new System.Windows.Forms.Button();
            this.btnCamUnitBInspLowEnd = new System.Windows.Forms.Button();
            this.btnCamUnitBInspUpStart = new System.Windows.Forms.Button();
            this.label78 = new System.Windows.Forms.Label();
            this.btnCamUnitBInspLowStart = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btnCamUnitAInspUpEnd = new System.Windows.Forms.Button();
            this.btnCamUnitAInspLowEnd = new System.Windows.Forms.Button();
            this.btnCamUnitAInspUpStart = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.btnCamUnitAInspLowStart = new System.Windows.Forms.Button();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCellInputY1BCstInner = new System.Windows.Forms.Button();
            this.btnCellInputY1ACstInner = new System.Windows.Forms.Button();
            this.btnCellInputY1CstMid = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCellInputY1CellULD = new System.Windows.Forms.Button();
            this.btnCellInputY1CellLD = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnCellInputB1LoadingA = new System.Windows.Forms.Button();
            this.btnCellInputB1HoldB = new System.Windows.Forms.Button();
            this.btnCellInputB1LoadingB = new System.Windows.Forms.Button();
            this.btnCellInputB1MiddleB = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.btnCellInputB1HoldA = new System.Windows.Forms.Button();
            this.btnCellInputB1MiddleA = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnCellInputA1LoadingA = new System.Windows.Forms.Button();
            this.btnCellInputA1LoadingB = new System.Windows.Forms.Button();
            this.btnCellInputA1HoldB = new System.Windows.Forms.Button();
            this.btnCellInputA1MiddleB = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.btnCellInputA1HoldA = new System.Windows.Forms.Button();
            this.btnCellInputA1MiddleA = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnCellInputY2BCstInner = new System.Windows.Forms.Button();
            this.btnCellInputY2ACstInner = new System.Windows.Forms.Button();
            this.btnCellInputY2CstMid = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.btnCellInputY2CellULD = new System.Windows.Forms.Button();
            this.btnCellInputY2CellLD = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCellInputBlowerOnOff = new System.Windows.Forms.Button();
            this.btnCellInputIonizerOnOff = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCellInputABlowerOnOff = new System.Windows.Forms.Button();
            this.btnCellInputAVaccumOff = new System.Windows.Forms.Button();
            this.btnCellInputAVaccumOn = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnCellInputBBlowerOnOff = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCellInputBVaccumOn = new System.Windows.Forms.Button();
            this.btnCellInputBVaccumOff = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnCellInputBufferBlowerOnOff = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.btnCellInputBufferPickerDown = new System.Windows.Forms.Button();
            this.btnCellInputBufferPickerUp = new System.Windows.Forms.Button();
            this.btnCellInputBufferVaccumOff = new System.Windows.Forms.Button();
            this.btnCellInputBufferVaccumOn = new System.Windows.Forms.Button();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnCstunloadBCellOut = new System.Windows.Forms.Button();
            this.btnCstunloadBMeasure = new System.Windows.Forms.Button();
            this.btnCstunloadBLoader = new System.Windows.Forms.Button();
            this.btnCstunloadB90dgr = new System.Windows.Forms.Button();
            this.btnCstunloadB0dgr = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.btnCstunloadAMeasure = new System.Windows.Forms.Button();
            this.btnCstunloadALoader = new System.Windows.Forms.Button();
            this.btnCstunloadA0dgr = new System.Windows.Forms.Button();
            this.btnCstunloadA90dgr = new System.Windows.Forms.Button();
            this.btnCstunloadACellOut = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.panel68 = new System.Windows.Forms.Panel();
            this.label68 = new System.Windows.Forms.Label();
            this.btnCasseteUnloadATopCstGrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadATopCstUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadALiftTiltSlinderDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadALiftTiltSlinderUp = new System.Windows.Forms.Button();
            this.panel69 = new System.Windows.Forms.Panel();
            this.label69 = new System.Windows.Forms.Label();
            this.btnCasseteUnloadBTopCstGrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBTopCstUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBLiftTiltSlinderDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBLiftTiltSlinderUp = new System.Windows.Forms.Button();
            this.panel70 = new System.Windows.Forms.Panel();
            this.btn_CasseteUnload_Muting4 = new System.Windows.Forms.Button();
            this.label70 = new System.Windows.Forms.Label();
            this.btn_CasseteUnload_Muting2 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting3 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MutingIn = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting1 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MutingOut = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.ucrlServoCtrl = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSevoPosi)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel30.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel83.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel47.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel51.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.panel74.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel77.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.panel72.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel22.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.panel79.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel81.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel49.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel21.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel68.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            this.panel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnCstLoad);
            this.flowLayoutPanel1.Controls.Add(this.btnCellOutTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCellLoadTransfer01);
            this.flowLayoutPanel1.Controls.Add(this.btnIRCutProcess);
            this.flowLayoutPanel1.Controls.Add(this.btnAfterIRCutTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakUnit01);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakUnit02);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakHeadUnitXZ);
            this.flowLayoutPanel1.Controls.Add(this.btnBeforeTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnInspStage);
            this.flowLayoutPanel1.Controls.Add(this.btnAfterTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCameraUnit);
            this.flowLayoutPanel1.Controls.Add(this.btnCellInTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCstUnloader);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1740, 47);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // btnCstLoad
            // 
            this.btnCstLoad.Location = new System.Drawing.Point(3, 3);
            this.btnCstLoad.Name = "btnCstLoad";
            this.btnCstLoad.Size = new System.Drawing.Size(115, 41);
            this.btnCstLoad.TabIndex = 0;
            this.btnCstLoad.Text = "카셋트 로드";
            this.btnCstLoad.UseVisualStyleBackColor = true;
            this.btnCstLoad.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellOutTransfer
            // 
            this.btnCellOutTransfer.Location = new System.Drawing.Point(124, 3);
            this.btnCellOutTransfer.Name = "btnCellOutTransfer";
            this.btnCellOutTransfer.Size = new System.Drawing.Size(115, 41);
            this.btnCellOutTransfer.TabIndex = 1;
            this.btnCellOutTransfer.Text = "설 취출 이재기";
            this.btnCellOutTransfer.UseVisualStyleBackColor = true;
            this.btnCellOutTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellLoadTransfer01
            // 
            this.btnCellLoadTransfer01.Location = new System.Drawing.Point(245, 3);
            this.btnCellLoadTransfer01.Name = "btnCellLoadTransfer01";
            this.btnCellLoadTransfer01.Size = new System.Drawing.Size(115, 41);
            this.btnCellLoadTransfer01.TabIndex = 11;
            this.btnCellLoadTransfer01.Text = "셀 로드 이재기";
            this.btnCellLoadTransfer01.UseVisualStyleBackColor = true;
            this.btnCellLoadTransfer01.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnIRCutProcess
            // 
            this.btnIRCutProcess.Location = new System.Drawing.Point(366, 3);
            this.btnIRCutProcess.Name = "btnIRCutProcess";
            this.btnIRCutProcess.Size = new System.Drawing.Size(115, 41);
            this.btnIRCutProcess.TabIndex = 3;
            this.btnIRCutProcess.Text = "IR Cut 프로세스";
            this.btnIRCutProcess.UseVisualStyleBackColor = true;
            this.btnIRCutProcess.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnAfterIRCutTransfer
            // 
            this.btnAfterIRCutTransfer.Location = new System.Drawing.Point(487, 3);
            this.btnAfterIRCutTransfer.Name = "btnAfterIRCutTransfer";
            this.btnAfterIRCutTransfer.Size = new System.Drawing.Size(115, 41);
            this.btnAfterIRCutTransfer.TabIndex = 4;
            this.btnAfterIRCutTransfer.Text = "BREAK 트랜스퍼";
            this.btnAfterIRCutTransfer.UseVisualStyleBackColor = true;
            this.btnAfterIRCutTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakUnit01
            // 
            this.btnBreakUnit01.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnBreakUnit01.Location = new System.Drawing.Point(608, 3);
            this.btnBreakUnit01.Name = "btnBreakUnit01";
            this.btnBreakUnit01.Size = new System.Drawing.Size(115, 41);
            this.btnBreakUnit01.TabIndex = 5;
            this.btnBreakUnit01.Text = "Break 유닛(X축 Z축)";
            this.btnBreakUnit01.UseVisualStyleBackColor = true;
            this.btnBreakUnit01.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakUnit02
            // 
            this.btnBreakUnit02.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnBreakUnit02.Location = new System.Drawing.Point(729, 3);
            this.btnBreakUnit02.Name = "btnBreakUnit02";
            this.btnBreakUnit02.Size = new System.Drawing.Size(115, 41);
            this.btnBreakUnit02.TabIndex = 6;
            this.btnBreakUnit02.Text = "Break 유닛(T축 Y축)";
            this.btnBreakUnit02.UseVisualStyleBackColor = true;
            this.btnBreakUnit02.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakHeadUnitXZ
            // 
            this.btnBreakHeadUnitXZ.Location = new System.Drawing.Point(850, 3);
            this.btnBreakHeadUnitXZ.Name = "btnBreakHeadUnitXZ";
            this.btnBreakHeadUnitXZ.Size = new System.Drawing.Size(115, 41);
            this.btnBreakHeadUnitXZ.TabIndex = 12;
            this.btnBreakHeadUnitXZ.Text = "Break Head(X,Z)";
            this.btnBreakHeadUnitXZ.UseVisualStyleBackColor = true;
            this.btnBreakHeadUnitXZ.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBeforeTransfer
            // 
            this.btnBeforeTransfer.Location = new System.Drawing.Point(971, 3);
            this.btnBeforeTransfer.Name = "btnBeforeTransfer";
            this.btnBeforeTransfer.Size = new System.Drawing.Size(115, 41);
            this.btnBeforeTransfer.TabIndex = 7;
            this.btnBeforeTransfer.Text = "Before 트랜스퍼";
            this.btnBeforeTransfer.UseVisualStyleBackColor = true;
            this.btnBeforeTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnInspStage
            // 
            this.btnInspStage.Location = new System.Drawing.Point(1092, 3);
            this.btnInspStage.Name = "btnInspStage";
            this.btnInspStage.Size = new System.Drawing.Size(115, 41);
            this.btnInspStage.TabIndex = 13;
            this.btnInspStage.Text = "검사 스테이지";
            this.btnInspStage.UseVisualStyleBackColor = true;
            this.btnInspStage.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnAfterTransfer
            // 
            this.btnAfterTransfer.Location = new System.Drawing.Point(1213, 3);
            this.btnAfterTransfer.Name = "btnAfterTransfer";
            this.btnAfterTransfer.Size = new System.Drawing.Size(115, 41);
            this.btnAfterTransfer.TabIndex = 14;
            this.btnAfterTransfer.Text = "After 트랜스퍼";
            this.btnAfterTransfer.UseVisualStyleBackColor = true;
            this.btnAfterTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCameraUnit
            // 
            this.btnCameraUnit.Location = new System.Drawing.Point(1334, 3);
            this.btnCameraUnit.Name = "btnCameraUnit";
            this.btnCameraUnit.Size = new System.Drawing.Size(115, 41);
            this.btnCameraUnit.TabIndex = 8;
            this.btnCameraUnit.Text = "카메라 유닛";
            this.btnCameraUnit.UseVisualStyleBackColor = true;
            this.btnCameraUnit.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellInTransfer
            // 
            this.btnCellInTransfer.Location = new System.Drawing.Point(1455, 3);
            this.btnCellInTransfer.Name = "btnCellInTransfer";
            this.btnCellInTransfer.Size = new System.Drawing.Size(115, 41);
            this.btnCellInTransfer.TabIndex = 9;
            this.btnCellInTransfer.Text = "셀 투입 이재기";
            this.btnCellInTransfer.UseVisualStyleBackColor = true;
            this.btnCellInTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCstUnloader
            // 
            this.btnCstUnloader.Location = new System.Drawing.Point(1576, 3);
            this.btnCstUnloader.Name = "btnCstUnloader";
            this.btnCstUnloader.Size = new System.Drawing.Size(115, 41);
            this.btnCstUnloader.TabIndex = 10;
            this.btnCstUnloader.Text = "카셋트 언로드";
            this.btnCstUnloader.UseVisualStyleBackColor = true;
            this.btnCstUnloader.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellInputSpeedSetting
            // 
            this.btnCellInputSpeedSetting.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputSpeedSetting.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputSpeedSetting.Location = new System.Drawing.Point(509, 77);
            this.btnCellInputSpeedSetting.Name = "btnCellInputSpeedSetting";
            this.btnCellInputSpeedSetting.Size = new System.Drawing.Size(90, 31);
            this.btnCellInputSpeedSetting.TabIndex = 61;
            this.btnCellInputSpeedSetting.Text = "속도 설정";
            this.btnCellInputSpeedSetting.UseVisualStyleBackColor = false;
            this.btnCellInputSpeedSetting.Click += new System.EventHandler(this.btnCellInputSpeedSetting_Click);
            // 
            // btnCellInputAllSetting
            // 
            this.btnCellInputAllSetting.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputAllSetting.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputAllSetting.Location = new System.Drawing.Point(603, 36);
            this.btnCellInputAllSetting.Name = "btnCellInputAllSetting";
            this.btnCellInputAllSetting.Size = new System.Drawing.Size(90, 31);
            this.btnCellInputAllSetting.TabIndex = 60;
            this.btnCellInputAllSetting.Text = "전체 설정";
            this.btnCellInputAllSetting.UseVisualStyleBackColor = false;
            this.btnCellInputAllSetting.Click += new System.EventHandler(this.btnCellInputAllSetting_Click);
            // 
            // btnCellInputLocationSetting
            // 
            this.btnCellInputLocationSetting.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputLocationSetting.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputLocationSetting.Location = new System.Drawing.Point(309, 77);
            this.btnCellInputLocationSetting.Name = "btnCellInputLocationSetting";
            this.btnCellInputLocationSetting.Size = new System.Drawing.Size(90, 31);
            this.btnCellInputLocationSetting.TabIndex = 59;
            this.btnCellInputLocationSetting.Text = "위치 설정";
            this.btnCellInputLocationSetting.UseVisualStyleBackColor = false;
            this.btnCellInputLocationSetting.Click += new System.EventHandler(this.btnCellInputLocationSetting_Click);
            // 
            // btnCellInputMoveLocation
            // 
            this.btnCellInputMoveLocation.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputMoveLocation.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputMoveLocation.Location = new System.Drawing.Point(206, 77);
            this.btnCellInputMoveLocation.Name = "btnCellInputMoveLocation";
            this.btnCellInputMoveLocation.Size = new System.Drawing.Size(90, 31);
            this.btnCellInputMoveLocation.TabIndex = 58;
            this.btnCellInputMoveLocation.Text = "위치 이동";
            this.btnCellInputMoveLocation.UseVisualStyleBackColor = false;
            this.btnCellInputMoveLocation.Click += new System.EventHandler(this.btnCellInputMoveLocation_Click);
            // 
            // txtSelectedServoNoSetPosi
            // 
            this.txtSelectedServoNoSetPosi.Font = new System.Drawing.Font("굴림", 13F);
            this.txtSelectedServoNoSetPosi.Location = new System.Drawing.Point(107, 79);
            this.txtSelectedServoNoSetPosi.Name = "txtSelectedServoNoSetPosi";
            this.txtSelectedServoNoSetPosi.Size = new System.Drawing.Size(90, 27);
            this.txtSelectedServoNoSetPosi.TabIndex = 57;
            // 
            // txtSelectedServoNoSetSpeed
            // 
            this.txtSelectedServoNoSetSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtSelectedServoNoSetSpeed.Location = new System.Drawing.Point(509, 38);
            this.txtSelectedServoNoSetSpeed.Name = "txtSelectedServoNoSetSpeed";
            this.txtSelectedServoNoSetSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtSelectedServoNoSetSpeed.TabIndex = 56;
            // 
            // txtSelectedServoNoCurrPosi
            // 
            this.txtSelectedServoNoCurrPosi.Font = new System.Drawing.Font("굴림", 13F);
            this.txtSelectedServoNoCurrPosi.Location = new System.Drawing.Point(309, 39);
            this.txtSelectedServoNoCurrPosi.Name = "txtSelectedServoNoCurrPosi";
            this.txtSelectedServoNoCurrPosi.Size = new System.Drawing.Size(90, 27);
            this.txtSelectedServoNoCurrPosi.TabIndex = 55;
            // 
            // txtSelectedServoNo
            // 
            this.txtSelectedServoNo.Font = new System.Drawing.Font("굴림", 13F);
            this.txtSelectedServoNo.Location = new System.Drawing.Point(107, 40);
            this.txtSelectedServoNo.Name = "txtSelectedServoNo";
            this.txtSelectedServoNo.Size = new System.Drawing.Size(90, 27);
            this.txtSelectedServoNo.TabIndex = 54;
            // 
            // lbl_CellInput_Location
            // 
            this.lbl_CellInput_Location.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CellInput_Location.ForeColor = System.Drawing.Color.Black;
            this.lbl_CellInput_Location.Location = new System.Drawing.Point(3, 80);
            this.lbl_CellInput_Location.Name = "lbl_CellInput_Location";
            this.lbl_CellInput_Location.Size = new System.Drawing.Size(100, 26);
            this.lbl_CellInput_Location.TabIndex = 53;
            this.lbl_CellInput_Location.Text = "위치[mm]";
            this.lbl_CellInput_Location.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CellInput_Speed
            // 
            this.lbl_CellInput_Speed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CellInput_Speed.ForeColor = System.Drawing.Color.Black;
            this.lbl_CellInput_Speed.Location = new System.Drawing.Point(405, 39);
            this.lbl_CellInput_Speed.Name = "lbl_CellInput_Speed";
            this.lbl_CellInput_Speed.Size = new System.Drawing.Size(100, 26);
            this.lbl_CellInput_Speed.TabIndex = 52;
            this.lbl_CellInput_Speed.Text = "속도[mm/sec]";
            this.lbl_CellInput_Speed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CellInput_SelectedShaft
            // 
            this.lbl_CellInput_SelectedShaft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CellInput_SelectedShaft.ForeColor = System.Drawing.Color.Black;
            this.lbl_CellInput_SelectedShaft.Location = new System.Drawing.Point(3, 40);
            this.lbl_CellInput_SelectedShaft.Name = "lbl_CellInput_SelectedShaft";
            this.lbl_CellInput_SelectedShaft.Size = new System.Drawing.Size(100, 26);
            this.lbl_CellInput_SelectedShaft.TabIndex = 51;
            this.lbl_CellInput_SelectedShaft.Text = "선택된 축";
            this.lbl_CellInput_SelectedShaft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CellInput_CurrentLocation
            // 
            this.lbl_CellInput_CurrentLocation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CellInput_CurrentLocation.ForeColor = System.Drawing.Color.Black;
            this.lbl_CellInput_CurrentLocation.Location = new System.Drawing.Point(203, 40);
            this.lbl_CellInput_CurrentLocation.Name = "lbl_CellInput_CurrentLocation";
            this.lbl_CellInput_CurrentLocation.Size = new System.Drawing.Size(100, 26);
            this.lbl_CellInput_CurrentLocation.TabIndex = 49;
            this.lbl_CellInput_CurrentLocation.Text = "현재위치[mm]";
            this.lbl_CellInput_CurrentLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvSevoPosi
            // 
            this.dgvSevoPosi.AllowUserToResizeColumns = false;
            this.dgvSevoPosi.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSevoPosi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSevoPosi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSevoPosi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.posName,
            this.aixs,
            this.Position,
            this.speed});
            this.dgvSevoPosi.Location = new System.Drawing.Point(14, 67);
            this.dgvSevoPosi.MultiSelect = false;
            this.dgvSevoPosi.Name = "dgvSevoPosi";
            this.dgvSevoPosi.RowHeadersVisible = false;
            this.dgvSevoPosi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvSevoPosi.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvSevoPosi.RowTemplate.Height = 23;
            this.dgvSevoPosi.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvSevoPosi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSevoPosi.Size = new System.Drawing.Size(721, 784);
            this.dgvSevoPosi.TabIndex = 66;
            this.dgvSevoPosi.SelectionChanged += new System.EventHandler(this.dgvSevoPosi_SelectionChanged);
            // 
            // posName
            // 
            this.posName.HeaderText = "위치별 명칭";
            this.posName.MinimumWidth = 330;
            this.posName.Name = "posName";
            this.posName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.posName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.posName.Width = 330;
            // 
            // aixs
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.aixs.DefaultCellStyle = dataGridViewCellStyle2;
            this.aixs.HeaderText = "축";
            this.aixs.MinimumWidth = 80;
            this.aixs.Name = "aixs";
            this.aixs.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.aixs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Position
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Position.DefaultCellStyle = dataGridViewCellStyle3;
            this.Position.HeaderText = "위치[mm]";
            this.Position.MinimumWidth = 130;
            this.Position.Name = "Position";
            this.Position.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Position.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Position.Width = 130;
            // 
            // speed
            // 
            this.speed.HeaderText = "속도[mm/s]";
            this.speed.MinimumWidth = 130;
            this.speed.Name = "speed";
            this.speed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.speed.Width = 130;
            // 
            // btnCasseteLoadSave
            // 
            this.btnCasseteLoadSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteLoadSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadSave.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteLoadSave.Location = new System.Drawing.Point(1568, 824);
            this.btnCasseteLoadSave.Name = "btnCasseteLoadSave";
            this.btnCasseteLoadSave.Size = new System.Drawing.Size(155, 35);
            this.btnCasseteLoadSave.TabIndex = 67;
            this.btnCasseteLoadSave.Text = "저장";
            this.btnCasseteLoadSave.UseVisualStyleBackColor = false;
            this.btnCasseteLoadSave.Click += new System.EventHandler(this.btnCasseteLoadSave_Click);
            // 
            // btnLDCstBGripUn
            // 
            this.btnLDCstBGripUn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDCstBGripUn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDCstBGripUn.ForeColor = System.Drawing.Color.Black;
            this.btnLDCstBGripUn.Location = new System.Drawing.Point(107, 29);
            this.btnLDCstBGripUn.Name = "btnLDCstBGripUn";
            this.btnLDCstBGripUn.Size = new System.Drawing.Size(93, 66);
            this.btnLDCstBGripUn.TabIndex = 25;
            this.btnLDCstBGripUn.Text = "카세트 언그립";
            this.btnLDCstBGripUn.UseVisualStyleBackColor = false;
            // 
            // btnLDCstBGrip
            // 
            this.btnLDCstBGrip.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDCstBGrip.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDCstBGrip.ForeColor = System.Drawing.Color.Black;
            this.btnLDCstBGrip.Location = new System.Drawing.Point(6, 29);
            this.btnLDCstBGrip.Name = "btnLDCstBGrip";
            this.btnLDCstBGrip.Size = new System.Drawing.Size(93, 66);
            this.btnLDCstBGrip.TabIndex = 24;
            this.btnLDCstBGrip.Text = "카세트 그립";
            this.btnLDCstBGrip.UseVisualStyleBackColor = false;
            // 
            // btnLDCstAGripUn
            // 
            this.btnLDCstAGripUn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDCstAGripUn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDCstAGripUn.ForeColor = System.Drawing.Color.Black;
            this.btnLDCstAGripUn.Location = new System.Drawing.Point(107, 29);
            this.btnLDCstAGripUn.Name = "btnLDCstAGripUn";
            this.btnLDCstAGripUn.Size = new System.Drawing.Size(93, 66);
            this.btnLDCstAGripUn.TabIndex = 21;
            this.btnLDCstAGripUn.Text = "카세트 언그립";
            this.btnLDCstAGripUn.UseVisualStyleBackColor = false;
            // 
            // btnLDCstAGrip
            // 
            this.btnLDCstAGrip.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDCstAGrip.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDCstAGrip.ForeColor = System.Drawing.Color.Black;
            this.btnLDCstAGrip.Location = new System.Drawing.Point(6, 29);
            this.btnLDCstAGrip.Name = "btnLDCstAGrip";
            this.btnLDCstAGrip.Size = new System.Drawing.Size(93, 66);
            this.btnLDCstAGrip.TabIndex = 20;
            this.btnLDCstAGrip.Text = "카세트 그립";
            this.btnLDCstAGrip.UseVisualStyleBackColor = false;
            // 
            // btnCstloadBMeasure
            // 
            this.btnCstloadBMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadBMeasure.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadBMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadBMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadBMeasure.Location = new System.Drawing.Point(118, 95);
            this.btnCstloadBMeasure.Name = "btnCstloadBMeasure";
            this.btnCstloadBMeasure.Size = new System.Drawing.Size(103, 58);
            this.btnCstloadBMeasure.TabIndex = 62;
            this.btnCstloadBMeasure.Text = "측정";
            this.btnCstloadBMeasure.UseVisualStyleBackColor = false;
            // 
            // btnCstloadBLoader
            // 
            this.btnCstloadBLoader.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadBLoader.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadBLoader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadBLoader.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadBLoader.Location = new System.Drawing.Point(118, 33);
            this.btnCstloadBLoader.Name = "btnCstloadBLoader";
            this.btnCstloadBLoader.Size = new System.Drawing.Size(103, 55);
            this.btnCstloadBLoader.TabIndex = 57;
            this.btnCstloadBLoader.Text = "카세트 로더";
            this.btnCstloadBLoader.UseVisualStyleBackColor = false;
            // 
            // btnCstloadB90dgr
            // 
            this.btnCstloadB90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadB90dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadB90dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadB90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadB90dgr.Location = new System.Drawing.Point(9, 95);
            this.btnCstloadB90dgr.Name = "btnCstloadB90dgr";
            this.btnCstloadB90dgr.Size = new System.Drawing.Size(103, 58);
            this.btnCstloadB90dgr.TabIndex = 55;
            this.btnCstloadB90dgr.Text = "카세트 90도";
            this.btnCstloadB90dgr.UseVisualStyleBackColor = false;
            // 
            // btnCstloadB0dgr
            // 
            this.btnCstloadB0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadB0dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadB0dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadB0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadB0dgr.Location = new System.Drawing.Point(9, 33);
            this.btnCstloadB0dgr.Name = "btnCstloadB0dgr";
            this.btnCstloadB0dgr.Size = new System.Drawing.Size(103, 55);
            this.btnCstloadB0dgr.TabIndex = 54;
            this.btnCstloadB0dgr.Text = "카세트 0도";
            this.btnCstloadB0dgr.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX2BlowerOnOff
            // 
            this.btnLoaderAxisX2BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2BlowerOnOff.Location = new System.Drawing.Point(193, 29);
            this.btnLoaderAxisX2BlowerOnOff.Name = "btnLoaderAxisX2BlowerOnOff";
            this.btnLoaderAxisX2BlowerOnOff.Size = new System.Drawing.Size(85, 81);
            this.btnLoaderAxisX2BlowerOnOff.TabIndex = 23;
            this.btnLoaderAxisX2BlowerOnOff.Text = "파기 온/오프";
            this.btnLoaderAxisX2BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX2VaccumOff
            // 
            this.btnLoaderAxisX2VaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2VaccumOff.Location = new System.Drawing.Point(102, 29);
            this.btnLoaderAxisX2VaccumOff.Name = "btnLoaderAxisX2VaccumOff";
            this.btnLoaderAxisX2VaccumOff.Size = new System.Drawing.Size(85, 81);
            this.btnLoaderAxisX2VaccumOff.TabIndex = 22;
            this.btnLoaderAxisX2VaccumOff.Text = "공압 오프";
            this.btnLoaderAxisX2VaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX2VaccumOn
            // 
            this.btnLoaderAxisX2VaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2VaccumOn.Location = new System.Drawing.Point(11, 29);
            this.btnLoaderAxisX2VaccumOn.Name = "btnLoaderAxisX2VaccumOn";
            this.btnLoaderAxisX2VaccumOn.Size = new System.Drawing.Size(85, 81);
            this.btnLoaderAxisX2VaccumOn.TabIndex = 21;
            this.btnLoaderAxisX2VaccumOn.Text = "공압 온";
            this.btnLoaderAxisX2VaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX1BlowerOnOff
            // 
            this.btnLoaderAxisX1BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1BlowerOnOff.Location = new System.Drawing.Point(193, 29);
            this.btnLoaderAxisX1BlowerOnOff.Name = "btnLoaderAxisX1BlowerOnOff";
            this.btnLoaderAxisX1BlowerOnOff.Size = new System.Drawing.Size(85, 81);
            this.btnLoaderAxisX1BlowerOnOff.TabIndex = 19;
            this.btnLoaderAxisX1BlowerOnOff.Text = "파기 온/오프";
            this.btnLoaderAxisX1BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX1VaccumOff
            // 
            this.btnLoaderAxisX1VaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1VaccumOff.Location = new System.Drawing.Point(102, 29);
            this.btnLoaderAxisX1VaccumOff.Name = "btnLoaderAxisX1VaccumOff";
            this.btnLoaderAxisX1VaccumOff.Size = new System.Drawing.Size(85, 81);
            this.btnLoaderAxisX1VaccumOff.TabIndex = 18;
            this.btnLoaderAxisX1VaccumOff.Text = "공압 오프";
            this.btnLoaderAxisX1VaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX1VaccumOn
            // 
            this.btnLoaderAxisX1VaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1VaccumOn.Location = new System.Drawing.Point(11, 29);
            this.btnLoaderAxisX1VaccumOn.Name = "btnLoaderAxisX1VaccumOn";
            this.btnLoaderAxisX1VaccumOn.Size = new System.Drawing.Size(85, 81);
            this.btnLoaderAxisX1VaccumOn.TabIndex = 17;
            this.btnLoaderAxisX1VaccumOn.Text = "공압 온";
            this.btnLoaderAxisX1VaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX3LD
            // 
            this.btnCellLoadX3LD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX3LD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX3LD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX3LD.Location = new System.Drawing.Point(3, 91);
            this.btnCellLoadX3LD.Name = "btnCellLoadX3LD";
            this.btnCellLoadX3LD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX3LD.TabIndex = 60;
            this.btnCellLoadX3LD.Text = "X3 LD";
            this.btnCellLoadX3LD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX2LD
            // 
            this.btnCellLoadX2LD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX2LD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX2LD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX2LD.Location = new System.Drawing.Point(3, 59);
            this.btnCellLoadX2LD.Name = "btnCellLoadX2LD";
            this.btnCellLoadX2LD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX2LD.TabIndex = 56;
            this.btnCellLoadX2LD.Text = "X2 LD";
            this.btnCellLoadX2LD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX1LD
            // 
            this.btnCellLoadX1LD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX1LD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX1LD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX1LD.Location = new System.Drawing.Point(3, 27);
            this.btnCellLoadX1LD.Name = "btnCellLoadX1LD";
            this.btnCellLoadX1LD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX1LD.TabIndex = 57;
            this.btnCellLoadX1LD.Text = "X1 LD";
            this.btnCellLoadX1LD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBMcrReading
            // 
            this.btnCellLoadBMcrReading.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadBMcrReading.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadBMcrReading.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadBMcrReading.Location = new System.Drawing.Point(122, 27);
            this.btnCellLoadBMcrReading.Name = "btnCellLoadBMcrReading";
            this.btnCellLoadBMcrReading.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadBMcrReading.TabIndex = 59;
            this.btnCellLoadBMcrReading.Text = "B Mcr Reading";
            this.btnCellLoadBMcrReading.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBVisionAlign
            // 
            this.btnCellLoadBVisionAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadBVisionAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadBVisionAlign.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadBVisionAlign.Location = new System.Drawing.Point(3, 27);
            this.btnCellLoadBVisionAlign.Name = "btnCellLoadBVisionAlign";
            this.btnCellLoadBVisionAlign.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadBVisionAlign.TabIndex = 58;
            this.btnCellLoadBVisionAlign.Text = "B Vision Align";
            this.btnCellLoadBVisionAlign.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAMcrReading
            // 
            this.btnCellLoadAMcrReading.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadAMcrReading.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadAMcrReading.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadAMcrReading.Location = new System.Drawing.Point(121, 27);
            this.btnCellLoadAMcrReading.Name = "btnCellLoadAMcrReading";
            this.btnCellLoadAMcrReading.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadAMcrReading.TabIndex = 59;
            this.btnCellLoadAMcrReading.Text = "A Mcr Reading";
            this.btnCellLoadAMcrReading.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAVisionAlign
            // 
            this.btnCellLoadAVisionAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadAVisionAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadAVisionAlign.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadAVisionAlign.Location = new System.Drawing.Point(3, 27);
            this.btnCellLoadAVisionAlign.Name = "btnCellLoadAVisionAlign";
            this.btnCellLoadAVisionAlign.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadAVisionAlign.TabIndex = 58;
            this.btnCellLoadAVisionAlign.Text = "A Vision Align";
            this.btnCellLoadAVisionAlign.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB1PickerBlowerOnOff
            // 
            this.btnCellLoadB1PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB1PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB1PickerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnCellLoadB1PickerBlowerOnOff.Name = "btnCellLoadB1PickerBlowerOnOff";
            this.btnCellLoadB1PickerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnCellLoadB1PickerBlowerOnOff.TabIndex = 63;
            this.btnCellLoadB1PickerBlowerOnOff.Text = "B1\r\n파커 파기 온/오프";
            this.btnCellLoadB1PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB1PickerUp
            // 
            this.btnCellLoadB1PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB1PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB1PickerUp.Location = new System.Drawing.Point(6, 29);
            this.btnCellLoadB1PickerUp.Name = "btnCellLoadB1PickerUp";
            this.btnCellLoadB1PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB1PickerUp.TabIndex = 58;
            this.btnCellLoadB1PickerUp.Text = "B1\r\n피커 업";
            this.btnCellLoadB1PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB1PickerDown
            // 
            this.btnCellLoadB1PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB1PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB1PickerDown.Location = new System.Drawing.Point(120, 29);
            this.btnCellLoadB1PickerDown.Name = "btnCellLoadB1PickerDown";
            this.btnCellLoadB1PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB1PickerDown.TabIndex = 59;
            this.btnCellLoadB1PickerDown.Text = "B1\r\n피커 다운";
            this.btnCellLoadB1PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB1PickerVaccumOn
            // 
            this.btnCellLoadB1PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB1PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB1PickerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnCellLoadB1PickerVaccumOn.Name = "btnCellLoadB1PickerVaccumOn";
            this.btnCellLoadB1PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB1PickerVaccumOn.TabIndex = 60;
            this.btnCellLoadB1PickerVaccumOn.Text = "B1\r\n피커 공압 온";
            this.btnCellLoadB1PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB1PickerVaccumOff
            // 
            this.btnCellLoadB1PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB1PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB1PickerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnCellLoadB1PickerVaccumOff.Name = "btnCellLoadB1PickerVaccumOff";
            this.btnCellLoadB1PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB1PickerVaccumOff.TabIndex = 61;
            this.btnCellLoadB1PickerVaccumOff.Text = "B1\r\n피커 공압 오프";
            this.btnCellLoadB1PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA1PickerBlowerOnOff
            // 
            this.btnCellLoadA1PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA1PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA1PickerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnCellLoadA1PickerBlowerOnOff.Name = "btnCellLoadA1PickerBlowerOnOff";
            this.btnCellLoadA1PickerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnCellLoadA1PickerBlowerOnOff.TabIndex = 63;
            this.btnCellLoadA1PickerBlowerOnOff.Text = "A1\r\n피커 파기 온/오프";
            this.btnCellLoadA1PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA1PickerUp
            // 
            this.btnCellLoadA1PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA1PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA1PickerUp.Location = new System.Drawing.Point(6, 29);
            this.btnCellLoadA1PickerUp.Name = "btnCellLoadA1PickerUp";
            this.btnCellLoadA1PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA1PickerUp.TabIndex = 58;
            this.btnCellLoadA1PickerUp.Text = "A1\r\n피커 업";
            this.btnCellLoadA1PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA1PickerDown
            // 
            this.btnCellLoadA1PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA1PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA1PickerDown.Location = new System.Drawing.Point(120, 29);
            this.btnCellLoadA1PickerDown.Name = "btnCellLoadA1PickerDown";
            this.btnCellLoadA1PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA1PickerDown.TabIndex = 59;
            this.btnCellLoadA1PickerDown.Text = "A1\r\n피커 다운";
            this.btnCellLoadA1PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA1PickerVaccumOn
            // 
            this.btnCellLoadA1PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA1PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA1PickerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnCellLoadA1PickerVaccumOn.Name = "btnCellLoadA1PickerVaccumOn";
            this.btnCellLoadA1PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA1PickerVaccumOn.TabIndex = 60;
            this.btnCellLoadA1PickerVaccumOn.Text = "A1\r\n피커 공압 온";
            this.btnCellLoadA1PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakTRBULD
            // 
            this.btnBreakTRBULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTRBULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTRBULD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTRBULD.Location = new System.Drawing.Point(132, 31);
            this.btnBreakTRBULD.Name = "btnBreakTRBULD";
            this.btnBreakTRBULD.Size = new System.Drawing.Size(120, 120);
            this.btnBreakTRBULD.TabIndex = 67;
            this.btnBreakTRBULD.Text = "언로드";
            this.btnBreakTRBULD.UseVisualStyleBackColor = false;
            // 
            // btnBreakTRBLD
            // 
            this.btnBreakTRBLD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTRBLD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTRBLD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTRBLD.Location = new System.Drawing.Point(7, 31);
            this.btnBreakTRBLD.Name = "btnBreakTRBLD";
            this.btnBreakTRBLD.Size = new System.Drawing.Size(120, 120);
            this.btnBreakTRBLD.TabIndex = 66;
            this.btnBreakTRBLD.Text = "로드";
            this.btnBreakTRBLD.UseVisualStyleBackColor = false;
            // 
            // btnBreakTRAULD
            // 
            this.btnBreakTRAULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTRAULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTRAULD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTRAULD.Location = new System.Drawing.Point(132, 31);
            this.btnBreakTRAULD.Name = "btnBreakTRAULD";
            this.btnBreakTRAULD.Size = new System.Drawing.Size(120, 120);
            this.btnBreakTRAULD.TabIndex = 67;
            this.btnBreakTRAULD.Text = "언로드";
            this.btnBreakTRAULD.UseVisualStyleBackColor = false;
            // 
            // btnBreakTRALD
            // 
            this.btnBreakTRALD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTRALD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTRALD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTRALD.Location = new System.Drawing.Point(7, 31);
            this.btnBreakTRALD.Name = "btnBreakTRALD";
            this.btnBreakTRALD.Size = new System.Drawing.Size(120, 120);
            this.btnBreakTRALD.TabIndex = 66;
            this.btnBreakTRALD.Text = "로드";
            this.btnBreakTRALD.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB1PickerBlowerOnOff
            // 
            this.btnBreakTransferB1PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB1PickerBlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB1PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB1PickerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnBreakTransferB1PickerBlowerOnOff.Name = "btnBreakTransferB1PickerBlowerOnOff";
            this.btnBreakTransferB1PickerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnBreakTransferB1PickerBlowerOnOff.TabIndex = 65;
            this.btnBreakTransferB1PickerBlowerOnOff.Text = "B1\r\n피커 파기 온/오프";
            this.btnBreakTransferB1PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB1PickerUp
            // 
            this.btnBreakTransferB1PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB1PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB1PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB1PickerUp.Location = new System.Drawing.Point(6, 29);
            this.btnBreakTransferB1PickerUp.Name = "btnBreakTransferB1PickerUp";
            this.btnBreakTransferB1PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB1PickerUp.TabIndex = 60;
            this.btnBreakTransferB1PickerUp.Text = "B1\r\n피커 업";
            this.btnBreakTransferB1PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB1PickerDown
            // 
            this.btnBreakTransferB1PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB1PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB1PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB1PickerDown.Location = new System.Drawing.Point(120, 29);
            this.btnBreakTransferB1PickerDown.Name = "btnBreakTransferB1PickerDown";
            this.btnBreakTransferB1PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB1PickerDown.TabIndex = 61;
            this.btnBreakTransferB1PickerDown.Text = "B1\r\n피커 다운";
            this.btnBreakTransferB1PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB1PickerVaccumOn
            // 
            this.btnBreakTransferB1PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB1PickerVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB1PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB1PickerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnBreakTransferB1PickerVaccumOn.Name = "btnBreakTransferB1PickerVaccumOn";
            this.btnBreakTransferB1PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB1PickerVaccumOn.TabIndex = 62;
            this.btnBreakTransferB1PickerVaccumOn.Text = "B1\r\n피커 공압 온";
            this.btnBreakTransferB1PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB1PickerVaccumOff
            // 
            this.btnBreakTransferB1PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB1PickerVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB1PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB1PickerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnBreakTransferB1PickerVaccumOff.Name = "btnBreakTransferB1PickerVaccumOff";
            this.btnBreakTransferB1PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB1PickerVaccumOff.TabIndex = 63;
            this.btnBreakTransferB1PickerVaccumOff.Text = "B1\r\n피커 공압 오프";
            this.btnBreakTransferB1PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA1PickerBlowerOnOff
            // 
            this.btnBreakTransferA1PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA1PickerBlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA1PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA1PickerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnBreakTransferA1PickerBlowerOnOff.Name = "btnBreakTransferA1PickerBlowerOnOff";
            this.btnBreakTransferA1PickerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnBreakTransferA1PickerBlowerOnOff.TabIndex = 65;
            this.btnBreakTransferA1PickerBlowerOnOff.Text = "A1\r\n피커 파기 온/오프";
            this.btnBreakTransferA1PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA1PickerUp
            // 
            this.btnBreakTransferA1PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA1PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA1PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA1PickerUp.Location = new System.Drawing.Point(6, 29);
            this.btnBreakTransferA1PickerUp.Name = "btnBreakTransferA1PickerUp";
            this.btnBreakTransferA1PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA1PickerUp.TabIndex = 60;
            this.btnBreakTransferA1PickerUp.Text = "A1\r\n피커 업";
            this.btnBreakTransferA1PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA1PickerDown
            // 
            this.btnBreakTransferA1PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA1PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA1PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA1PickerDown.Location = new System.Drawing.Point(120, 29);
            this.btnBreakTransferA1PickerDown.Name = "btnBreakTransferA1PickerDown";
            this.btnBreakTransferA1PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA1PickerDown.TabIndex = 61;
            this.btnBreakTransferA1PickerDown.Text = "A1\r\n피커 다운";
            this.btnBreakTransferA1PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA1PickerVaccumOn
            // 
            this.btnBreakTransferA1PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA1PickerVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA1PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA1PickerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnBreakTransferA1PickerVaccumOn.Name = "btnBreakTransferA1PickerVaccumOn";
            this.btnBreakTransferA1PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA1PickerVaccumOn.TabIndex = 62;
            this.btnBreakTransferA1PickerVaccumOn.Text = "A1\r\n피커 공압 온";
            this.btnBreakTransferA1PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA1PickerVaccumOff
            // 
            this.btnBreakTransferA1PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA1PickerVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA1PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA1PickerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnBreakTransferA1PickerVaccumOff.Name = "btnBreakTransferA1PickerVaccumOff";
            this.btnBreakTransferA1PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA1PickerVaccumOff.TabIndex = 63;
            this.btnBreakTransferA1PickerVaccumOff.Text = "A1\r\n피커 공압 오프";
            this.btnBreakTransferA1PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB1LaserShot
            // 
            this.btnIRCutProcessB1LaserShot.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB1LaserShot.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessB1LaserShot.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB1LaserShot.Location = new System.Drawing.Point(12, 76);
            this.btnIRCutProcessB1LaserShot.Name = "btnIRCutProcessB1LaserShot";
            this.btnIRCutProcessB1LaserShot.Size = new System.Drawing.Size(95, 40);
            this.btnIRCutProcessB1LaserShot.TabIndex = 66;
            this.btnIRCutProcessB1LaserShot.Text = "B1 레이저 샷";
            this.btnIRCutProcessB1LaserShot.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB2LaserShot
            // 
            this.btnIRCutProcessB2LaserShot.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB2LaserShot.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessB2LaserShot.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB2LaserShot.Location = new System.Drawing.Point(113, 76);
            this.btnIRCutProcessB2LaserShot.Name = "btnIRCutProcessB2LaserShot";
            this.btnIRCutProcessB2LaserShot.Size = new System.Drawing.Size(95, 40);
            this.btnIRCutProcessB2LaserShot.TabIndex = 65;
            this.btnIRCutProcessB2LaserShot.Text = "B2 레이저 샷";
            this.btnIRCutProcessB2LaserShot.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA1LaserShot
            // 
            this.btnIRCutProcessA1LaserShot.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA1LaserShot.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessA1LaserShot.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA1LaserShot.Location = new System.Drawing.Point(12, 31);
            this.btnIRCutProcessA1LaserShot.Name = "btnIRCutProcessA1LaserShot";
            this.btnIRCutProcessA1LaserShot.Size = new System.Drawing.Size(95, 40);
            this.btnIRCutProcessA1LaserShot.TabIndex = 63;
            this.btnIRCutProcessA1LaserShot.Text = "A1 레이저 샷";
            this.btnIRCutProcessA1LaserShot.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA2LaserShot
            // 
            this.btnIRCutProcessA2LaserShot.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA2LaserShot.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessA2LaserShot.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA2LaserShot.Location = new System.Drawing.Point(113, 31);
            this.btnIRCutProcessA2LaserShot.Name = "btnIRCutProcessA2LaserShot";
            this.btnIRCutProcessA2LaserShot.Size = new System.Drawing.Size(95, 40);
            this.btnIRCutProcessA2LaserShot.TabIndex = 64;
            this.btnIRCutProcessA2LaserShot.Text = "A2 레이저 샷";
            this.btnIRCutProcessA2LaserShot.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAFineAlign
            // 
            this.btnIRCutProcessAFineAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessAFineAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessAFineAlign.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessAFineAlign.Location = new System.Drawing.Point(3, 32);
            this.btnIRCutProcessAFineAlign.Name = "btnIRCutProcessAFineAlign";
            this.btnIRCutProcessAFineAlign.Size = new System.Drawing.Size(87, 123);
            this.btnIRCutProcessAFineAlign.TabIndex = 63;
            this.btnIRCutProcessAFineAlign.Text = "A Fine Align 위치";
            this.btnIRCutProcessAFineAlign.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBFineAlign
            // 
            this.btnIRCutProcessBFineAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessBFineAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessBFineAlign.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessBFineAlign.Location = new System.Drawing.Point(96, 32);
            this.btnIRCutProcessBFineAlign.Name = "btnIRCutProcessBFineAlign";
            this.btnIRCutProcessBFineAlign.Size = new System.Drawing.Size(87, 123);
            this.btnIRCutProcessBFineAlign.TabIndex = 64;
            this.btnIRCutProcessBFineAlign.Text = "B Fine Align 위치";
            this.btnIRCutProcessBFineAlign.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBCamera
            // 
            this.btnIRCutProcessBCamera.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessBCamera.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessBCamera.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessBCamera.Location = new System.Drawing.Point(86, 93);
            this.btnIRCutProcessBCamera.Name = "btnIRCutProcessBCamera";
            this.btnIRCutProcessBCamera.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessBCamera.TabIndex = 65;
            this.btnIRCutProcessBCamera.Text = "카메라 확인";
            this.btnIRCutProcessBCamera.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBCellLoad
            // 
            this.btnIRCutProcessBCellLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessBCellLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessBCellLoad.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessBCellLoad.Location = new System.Drawing.Point(9, 29);
            this.btnIRCutProcessBCellLoad.Name = "btnIRCutProcessBCellLoad";
            this.btnIRCutProcessBCellLoad.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessBCellLoad.TabIndex = 60;
            this.btnIRCutProcessBCellLoad.Text = "셀 로드";
            this.btnIRCutProcessBCellLoad.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBCellUnload
            // 
            this.btnIRCutProcessBCellUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessBCellUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessBCellUnload.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessBCellUnload.Location = new System.Drawing.Point(86, 30);
            this.btnIRCutProcessBCellUnload.Name = "btnIRCutProcessBCellUnload";
            this.btnIRCutProcessBCellUnload.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessBCellUnload.TabIndex = 62;
            this.btnIRCutProcessBCellUnload.Text = "셀 언로드";
            this.btnIRCutProcessBCellUnload.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBLaser
            // 
            this.btnIRCutProcessBLaser.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessBLaser.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessBLaser.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessBLaser.Location = new System.Drawing.Point(9, 93);
            this.btnIRCutProcessBLaser.Name = "btnIRCutProcessBLaser";
            this.btnIRCutProcessBLaser.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessBLaser.TabIndex = 61;
            this.btnIRCutProcessBLaser.Text = "레이저샷";
            this.btnIRCutProcessBLaser.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessACamera
            // 
            this.btnIRCutProcessACamera.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessACamera.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessACamera.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessACamera.Location = new System.Drawing.Point(90, 92);
            this.btnIRCutProcessACamera.Name = "btnIRCutProcessACamera";
            this.btnIRCutProcessACamera.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessACamera.TabIndex = 65;
            this.btnIRCutProcessACamera.Text = "카메라 확인";
            this.btnIRCutProcessACamera.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessACellLoad
            // 
            this.btnIRCutProcessACellLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessACellLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessACellLoad.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessACellLoad.Location = new System.Drawing.Point(10, 29);
            this.btnIRCutProcessACellLoad.Name = "btnIRCutProcessACellLoad";
            this.btnIRCutProcessACellLoad.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessACellLoad.TabIndex = 60;
            this.btnIRCutProcessACellLoad.Text = "셀 로드";
            this.btnIRCutProcessACellLoad.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessACellUnload
            // 
            this.btnIRCutProcessACellUnload.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessACellUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessACellUnload.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessACellUnload.Location = new System.Drawing.Point(90, 29);
            this.btnIRCutProcessACellUnload.Name = "btnIRCutProcessACellUnload";
            this.btnIRCutProcessACellUnload.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessACellUnload.TabIndex = 62;
            this.btnIRCutProcessACellUnload.Text = "셀 언로드";
            this.btnIRCutProcessACellUnload.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessALaser
            // 
            this.btnIRCutProcessALaser.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessALaser.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessALaser.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessALaser.Location = new System.Drawing.Point(10, 93);
            this.btnIRCutProcessALaser.Name = "btnIRCutProcessALaser";
            this.btnIRCutProcessALaser.Size = new System.Drawing.Size(71, 60);
            this.btnIRCutProcessALaser.TabIndex = 61;
            this.btnIRCutProcessALaser.Text = "레이저샷";
            this.btnIRCutProcessALaser.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB1VaccumCh1Off
            // 
            this.btnIRCutProcessB1VaccumCh1Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB1VaccumCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB1VaccumCh1Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB1VaccumCh1Off.Location = new System.Drawing.Point(96, 25);
            this.btnIRCutProcessB1VaccumCh1Off.Name = "btnIRCutProcessB1VaccumCh1Off";
            this.btnIRCutProcessB1VaccumCh1Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB1VaccumCh1Off.TabIndex = 60;
            this.btnIRCutProcessB1VaccumCh1Off.Text = "B1 공압\r\n채널1 오프";
            this.btnIRCutProcessB1VaccumCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB2VaccumCh1On
            // 
            this.btnIRCutProcessB2VaccumCh1On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB2VaccumCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB2VaccumCh1On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB2VaccumCh1On.Location = new System.Drawing.Point(186, 25);
            this.btnIRCutProcessB2VaccumCh1On.Name = "btnIRCutProcessB2VaccumCh1On";
            this.btnIRCutProcessB2VaccumCh1On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB2VaccumCh1On.TabIndex = 63;
            this.btnIRCutProcessB2VaccumCh1On.Text = "B2 공압\r\n채널1 온";
            this.btnIRCutProcessB2VaccumCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB2VaccumCh1Off
            // 
            this.btnIRCutProcessB2VaccumCh1Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB2VaccumCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB2VaccumCh1Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB2VaccumCh1Off.Location = new System.Drawing.Point(277, 25);
            this.btnIRCutProcessB2VaccumCh1Off.Name = "btnIRCutProcessB2VaccumCh1Off";
            this.btnIRCutProcessB2VaccumCh1Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB2VaccumCh1Off.TabIndex = 62;
            this.btnIRCutProcessB2VaccumCh1Off.Text = "B2 공압\r\n채널1 오프";
            this.btnIRCutProcessB2VaccumCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB1VaccumCh2Off
            // 
            this.btnIRCutProcessB1VaccumCh2Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB1VaccumCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB1VaccumCh2Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB1VaccumCh2Off.Location = new System.Drawing.Point(96, 63);
            this.btnIRCutProcessB1VaccumCh2Off.Name = "btnIRCutProcessB1VaccumCh2Off";
            this.btnIRCutProcessB1VaccumCh2Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB1VaccumCh2Off.TabIndex = 65;
            this.btnIRCutProcessB1VaccumCh2Off.Text = "B1 공압\r\n채널2 오프";
            this.btnIRCutProcessB1VaccumCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB2VaccumCh2On
            // 
            this.btnIRCutProcessB2VaccumCh2On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB2VaccumCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB2VaccumCh2On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB2VaccumCh2On.Location = new System.Drawing.Point(186, 63);
            this.btnIRCutProcessB2VaccumCh2On.Name = "btnIRCutProcessB2VaccumCh2On";
            this.btnIRCutProcessB2VaccumCh2On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB2VaccumCh2On.TabIndex = 64;
            this.btnIRCutProcessB2VaccumCh2On.Text = "B2 공압\r\n채널2 온";
            this.btnIRCutProcessB2VaccumCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB2VaccumCh2Off
            // 
            this.btnIRCutProcessB2VaccumCh2Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB2VaccumCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB2VaccumCh2Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB2VaccumCh2Off.Location = new System.Drawing.Point(277, 63);
            this.btnIRCutProcessB2VaccumCh2Off.Name = "btnIRCutProcessB2VaccumCh2Off";
            this.btnIRCutProcessB2VaccumCh2Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB2VaccumCh2Off.TabIndex = 68;
            this.btnIRCutProcessB2VaccumCh2Off.Text = "B2 공압\r\n채널2 오프";
            this.btnIRCutProcessB2VaccumCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB1VaccumCh2On
            // 
            this.btnIRCutProcessB1VaccumCh2On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB1VaccumCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB1VaccumCh2On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB1VaccumCh2On.Location = new System.Drawing.Point(5, 63);
            this.btnIRCutProcessB1VaccumCh2On.Name = "btnIRCutProcessB1VaccumCh2On";
            this.btnIRCutProcessB1VaccumCh2On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB1VaccumCh2On.TabIndex = 61;
            this.btnIRCutProcessB1VaccumCh2On.Text = "B1 공압\r\n채널2 온";
            this.btnIRCutProcessB1VaccumCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB1VaccumCh1On
            // 
            this.btnIRCutProcessB1VaccumCh1On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB1VaccumCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB1VaccumCh1On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB1VaccumCh1On.Location = new System.Drawing.Point(5, 25);
            this.btnIRCutProcessB1VaccumCh1On.Name = "btnIRCutProcessB1VaccumCh1On";
            this.btnIRCutProcessB1VaccumCh1On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessB1VaccumCh1On.TabIndex = 59;
            this.btnIRCutProcessB1VaccumCh1On.Text = "B1 공압\r\n채널1 온";
            this.btnIRCutProcessB1VaccumCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB1BlowerCh1OnOff
            // 
            this.btnIRCutProcessB1BlowerCh1OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB1BlowerCh1OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB1BlowerCh1OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB1BlowerCh1OnOff.Location = new System.Drawing.Point(5, 101);
            this.btnIRCutProcessB1BlowerCh1OnOff.Name = "btnIRCutProcessB1BlowerCh1OnOff";
            this.btnIRCutProcessB1BlowerCh1OnOff.Size = new System.Drawing.Size(176, 35);
            this.btnIRCutProcessB1BlowerCh1OnOff.TabIndex = 69;
            this.btnIRCutProcessB1BlowerCh1OnOff.Text = "B1 파기\r\n채널1 온/오프";
            this.btnIRCutProcessB1BlowerCh1OnOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB2BlowerCh1OnOff
            // 
            this.btnIRCutProcessB2BlowerCh1OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB2BlowerCh1OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB2BlowerCh1OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB2BlowerCh1OnOff.Location = new System.Drawing.Point(186, 101);
            this.btnIRCutProcessB2BlowerCh1OnOff.Name = "btnIRCutProcessB2BlowerCh1OnOff";
            this.btnIRCutProcessB2BlowerCh1OnOff.Size = new System.Drawing.Size(177, 35);
            this.btnIRCutProcessB2BlowerCh1OnOff.TabIndex = 71;
            this.btnIRCutProcessB2BlowerCh1OnOff.Text = "B2 파기\r\n채널1 온/오프";
            this.btnIRCutProcessB2BlowerCh1OnOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB1BlowerCh2OnOff
            // 
            this.btnIRCutProcessB1BlowerCh2OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB1BlowerCh2OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB1BlowerCh2OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB1BlowerCh2OnOff.Location = new System.Drawing.Point(5, 138);
            this.btnIRCutProcessB1BlowerCh2OnOff.Name = "btnIRCutProcessB1BlowerCh2OnOff";
            this.btnIRCutProcessB1BlowerCh2OnOff.Size = new System.Drawing.Size(176, 35);
            this.btnIRCutProcessB1BlowerCh2OnOff.TabIndex = 73;
            this.btnIRCutProcessB1BlowerCh2OnOff.Text = "B1 파기\r\n채널2 온/오프";
            this.btnIRCutProcessB1BlowerCh2OnOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessB2BlowerCh2OnOff
            // 
            this.btnIRCutProcessB2BlowerCh2OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessB2BlowerCh2OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessB2BlowerCh2OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessB2BlowerCh2OnOff.Location = new System.Drawing.Point(186, 138);
            this.btnIRCutProcessB2BlowerCh2OnOff.Name = "btnIRCutProcessB2BlowerCh2OnOff";
            this.btnIRCutProcessB2BlowerCh2OnOff.Size = new System.Drawing.Size(177, 35);
            this.btnIRCutProcessB2BlowerCh2OnOff.TabIndex = 75;
            this.btnIRCutProcessB2BlowerCh2OnOff.Text = "B2 파기\r\n채널2 온/오프";
            this.btnIRCutProcessB2BlowerCh2OnOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA1BlowerCh2OnOff
            // 
            this.btnIRCutProcessA1BlowerCh2OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA1BlowerCh2OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA1BlowerCh2OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA1BlowerCh2OnOff.Location = new System.Drawing.Point(5, 138);
            this.btnIRCutProcessA1BlowerCh2OnOff.Name = "btnIRCutProcessA1BlowerCh2OnOff";
            this.btnIRCutProcessA1BlowerCh2OnOff.Size = new System.Drawing.Size(175, 35);
            this.btnIRCutProcessA1BlowerCh2OnOff.TabIndex = 75;
            this.btnIRCutProcessA1BlowerCh2OnOff.Text = "A1 파기\r\n채널2 온/오프";
            this.btnIRCutProcessA1BlowerCh2OnOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA2BlowerCh2OnOff
            // 
            this.btnIRCutProcessA2BlowerCh2OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA2BlowerCh2OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA2BlowerCh2OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA2BlowerCh2OnOff.Location = new System.Drawing.Point(186, 138);
            this.btnIRCutProcessA2BlowerCh2OnOff.Name = "btnIRCutProcessA2BlowerCh2OnOff";
            this.btnIRCutProcessA2BlowerCh2OnOff.Size = new System.Drawing.Size(176, 35);
            this.btnIRCutProcessA2BlowerCh2OnOff.TabIndex = 73;
            this.btnIRCutProcessA2BlowerCh2OnOff.Text = "A2 파기\r\n채널2 온/오프";
            this.btnIRCutProcessA2BlowerCh2OnOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA1VaccumCh1Off
            // 
            this.btnIRCutProcessA1VaccumCh1Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA1VaccumCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA1VaccumCh1Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA1VaccumCh1Off.Location = new System.Drawing.Point(96, 25);
            this.btnIRCutProcessA1VaccumCh1Off.Name = "btnIRCutProcessA1VaccumCh1Off";
            this.btnIRCutProcessA1VaccumCh1Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA1VaccumCh1Off.TabIndex = 60;
            this.btnIRCutProcessA1VaccumCh1Off.Text = "A1 공압\r\n채널1 오프";
            this.btnIRCutProcessA1VaccumCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA2VaccumCh1On
            // 
            this.btnIRCutProcessA2VaccumCh1On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA2VaccumCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA2VaccumCh1On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA2VaccumCh1On.Location = new System.Drawing.Point(186, 25);
            this.btnIRCutProcessA2VaccumCh1On.Name = "btnIRCutProcessA2VaccumCh1On";
            this.btnIRCutProcessA2VaccumCh1On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA2VaccumCh1On.TabIndex = 63;
            this.btnIRCutProcessA2VaccumCh1On.Text = "A2 공압\r\n채널1 온";
            this.btnIRCutProcessA2VaccumCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA2VaccumCh1Off
            // 
            this.btnIRCutProcessA2VaccumCh1Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA2VaccumCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA2VaccumCh1Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA2VaccumCh1Off.Location = new System.Drawing.Point(277, 25);
            this.btnIRCutProcessA2VaccumCh1Off.Name = "btnIRCutProcessA2VaccumCh1Off";
            this.btnIRCutProcessA2VaccumCh1Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA2VaccumCh1Off.TabIndex = 62;
            this.btnIRCutProcessA2VaccumCh1Off.Text = "A2 공압\r\n채널1 오프";
            this.btnIRCutProcessA2VaccumCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA1VaccumCh2Off
            // 
            this.btnIRCutProcessA1VaccumCh2Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA1VaccumCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA1VaccumCh2Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA1VaccumCh2Off.Location = new System.Drawing.Point(96, 62);
            this.btnIRCutProcessA1VaccumCh2Off.Name = "btnIRCutProcessA1VaccumCh2Off";
            this.btnIRCutProcessA1VaccumCh2Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA1VaccumCh2Off.TabIndex = 65;
            this.btnIRCutProcessA1VaccumCh2Off.Text = "A1 공압\r\n채널2 오프";
            this.btnIRCutProcessA1VaccumCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA2VaccumCh2On
            // 
            this.btnIRCutProcessA2VaccumCh2On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA2VaccumCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA2VaccumCh2On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA2VaccumCh2On.Location = new System.Drawing.Point(186, 62);
            this.btnIRCutProcessA2VaccumCh2On.Name = "btnIRCutProcessA2VaccumCh2On";
            this.btnIRCutProcessA2VaccumCh2On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA2VaccumCh2On.TabIndex = 64;
            this.btnIRCutProcessA2VaccumCh2On.Text = "A2 공압\r\n채널2 온";
            this.btnIRCutProcessA2VaccumCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA2VaccumCh2Off
            // 
            this.btnIRCutProcessA2VaccumCh2Off.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA2VaccumCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA2VaccumCh2Off.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA2VaccumCh2Off.Location = new System.Drawing.Point(277, 62);
            this.btnIRCutProcessA2VaccumCh2Off.Name = "btnIRCutProcessA2VaccumCh2Off";
            this.btnIRCutProcessA2VaccumCh2Off.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA2VaccumCh2Off.TabIndex = 68;
            this.btnIRCutProcessA2VaccumCh2Off.Text = "A2 공압\r\n채널2 오프";
            this.btnIRCutProcessA2VaccumCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA1VaccumCh2On
            // 
            this.btnIRCutProcessA1VaccumCh2On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA1VaccumCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA1VaccumCh2On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA1VaccumCh2On.Location = new System.Drawing.Point(5, 62);
            this.btnIRCutProcessA1VaccumCh2On.Name = "btnIRCutProcessA1VaccumCh2On";
            this.btnIRCutProcessA1VaccumCh2On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA1VaccumCh2On.TabIndex = 61;
            this.btnIRCutProcessA1VaccumCh2On.Text = "A1 공압\r\n채널2 온";
            this.btnIRCutProcessA1VaccumCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA1VaccumCh1On
            // 
            this.btnIRCutProcessA1VaccumCh1On.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA1VaccumCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA1VaccumCh1On.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA1VaccumCh1On.Location = new System.Drawing.Point(5, 25);
            this.btnIRCutProcessA1VaccumCh1On.Name = "btnIRCutProcessA1VaccumCh1On";
            this.btnIRCutProcessA1VaccumCh1On.Size = new System.Drawing.Size(85, 35);
            this.btnIRCutProcessA1VaccumCh1On.TabIndex = 59;
            this.btnIRCutProcessA1VaccumCh1On.Text = "A1 공압\r\n채널1 온";
            this.btnIRCutProcessA1VaccumCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA1BlowerCh1OnOff
            // 
            this.btnIRCutProcessA1BlowerCh1OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA1BlowerCh1OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA1BlowerCh1OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA1BlowerCh1OnOff.Location = new System.Drawing.Point(5, 100);
            this.btnIRCutProcessA1BlowerCh1OnOff.Name = "btnIRCutProcessA1BlowerCh1OnOff";
            this.btnIRCutProcessA1BlowerCh1OnOff.Size = new System.Drawing.Size(175, 35);
            this.btnIRCutProcessA1BlowerCh1OnOff.TabIndex = 69;
            this.btnIRCutProcessA1BlowerCh1OnOff.Text = "A1 파기\r\n채널1 온/오프";
            this.btnIRCutProcessA1BlowerCh1OnOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessA2BlowerCh1OnOff
            // 
            this.btnIRCutProcessA2BlowerCh1OnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessA2BlowerCh1OnOff.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnIRCutProcessA2BlowerCh1OnOff.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessA2BlowerCh1OnOff.Location = new System.Drawing.Point(186, 100);
            this.btnIRCutProcessA2BlowerCh1OnOff.Name = "btnIRCutProcessA2BlowerCh1OnOff";
            this.btnIRCutProcessA2BlowerCh1OnOff.Size = new System.Drawing.Size(176, 35);
            this.btnIRCutProcessA2BlowerCh1OnOff.TabIndex = 71;
            this.btnIRCutProcessA2BlowerCh1OnOff.Text = "A2 파기\r\n채널1 온/오프";
            this.btnIRCutProcessA2BlowerCh1OnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakZ1Hold
            // 
            this.btnBreakZ1Hold.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakZ1Hold.ForeColor = System.Drawing.Color.Black;
            this.btnBreakZ1Hold.Location = new System.Drawing.Point(10, 33);
            this.btnBreakZ1Hold.Name = "btnBreakZ1Hold";
            this.btnBreakZ1Hold.Size = new System.Drawing.Size(130, 55);
            this.btnBreakZ1Hold.TabIndex = 61;
            this.btnBreakZ1Hold.Text = "Z1 대기";
            this.btnBreakZ1Hold.UseVisualStyleBackColor = false;
            // 
            // btnBreakZ1BreakingPos
            // 
            this.btnBreakZ1BreakingPos.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakZ1BreakingPos.ForeColor = System.Drawing.Color.Black;
            this.btnBreakZ1BreakingPos.Location = new System.Drawing.Point(10, 94);
            this.btnBreakZ1BreakingPos.Name = "btnBreakZ1BreakingPos";
            this.btnBreakZ1BreakingPos.Size = new System.Drawing.Size(130, 55);
            this.btnBreakZ1BreakingPos.TabIndex = 62;
            this.btnBreakZ1BreakingPos.Text = "Z1 Breaking 위치";
            this.btnBreakZ1BreakingPos.UseVisualStyleBackColor = false;
            // 
            // btnBreakXZAAlignMeasure
            // 
            this.btnBreakXZAAlignMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakXZAAlignMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnBreakXZAAlignMeasure.Location = new System.Drawing.Point(11, 33);
            this.btnBreakXZAAlignMeasure.Name = "btnBreakXZAAlignMeasure";
            this.btnBreakXZAAlignMeasure.Size = new System.Drawing.Size(125, 55);
            this.btnBreakXZAAlignMeasure.TabIndex = 61;
            this.btnBreakXZAAlignMeasure.Text = "A Align 측정 위치";
            this.btnBreakXZAAlignMeasure.UseVisualStyleBackColor = false;
            // 
            // btnBreakXZBAlignMeasure
            // 
            this.btnBreakXZBAlignMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakXZBAlignMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnBreakXZBAlignMeasure.Location = new System.Drawing.Point(12, 95);
            this.btnBreakXZBAlignMeasure.Name = "btnBreakXZBAlignMeasure";
            this.btnBreakXZBAlignMeasure.Size = new System.Drawing.Size(125, 55);
            this.btnBreakXZBAlignMeasure.TabIndex = 62;
            this.btnBreakXZBAlignMeasure.Text = "B Align 측정 위치";
            this.btnBreakXZBAlignMeasure.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYShutterClose
            // 
            this.btnBreakUnitTYShutterClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYShutterClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYShutterClose.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYShutterClose.Location = new System.Drawing.Point(90, 27);
            this.btnBreakUnitTYShutterClose.Name = "btnBreakUnitTYShutterClose";
            this.btnBreakUnitTYShutterClose.Size = new System.Drawing.Size(80, 55);
            this.btnBreakUnitTYShutterClose.TabIndex = 58;
            this.btnBreakUnitTYShutterClose.Text = "더미 셔터\r\nClose";
            this.btnBreakUnitTYShutterClose.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYShutterOpen
            // 
            this.btnBreakUnitTYShutterOpen.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYShutterOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYShutterOpen.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYShutterOpen.Location = new System.Drawing.Point(5, 27);
            this.btnBreakUnitTYShutterOpen.Name = "btnBreakUnitTYShutterOpen";
            this.btnBreakUnitTYShutterOpen.Size = new System.Drawing.Size(80, 55);
            this.btnBreakUnitTYShutterOpen.TabIndex = 57;
            this.btnBreakUnitTYShutterOpen.Text = "더미 셔터\r\nOpen";
            this.btnBreakUnitTYShutterOpen.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYDummyBoxOutput
            // 
            this.btnBreakUnitTYDummyBoxOutput.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYDummyBoxOutput.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYDummyBoxOutput.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYDummyBoxOutput.Location = new System.Drawing.Point(97, 33);
            this.btnBreakUnitTYDummyBoxOutput.Name = "btnBreakUnitTYDummyBoxOutput";
            this.btnBreakUnitTYDummyBoxOutput.Size = new System.Drawing.Size(85, 40);
            this.btnBreakUnitTYDummyBoxOutput.TabIndex = 58;
            this.btnBreakUnitTYDummyBoxOutput.Text = "더미 배출";
            this.btnBreakUnitTYDummyBoxOutput.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYDummyBoxInput
            // 
            this.btnBreakUnitTYDummyBoxInput.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYDummyBoxInput.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYDummyBoxInput.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYDummyBoxInput.Location = new System.Drawing.Point(6, 33);
            this.btnBreakUnitTYDummyBoxInput.Name = "btnBreakUnitTYDummyBoxInput";
            this.btnBreakUnitTYDummyBoxInput.Size = new System.Drawing.Size(85, 40);
            this.btnBreakUnitTYDummyBoxInput.TabIndex = 57;
            this.btnBreakUnitTYDummyBoxInput.Text = "더미 투입";
            this.btnBreakUnitTYDummyBoxInput.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYAULD
            // 
            this.btnBreakUnitTYAULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYAULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYAULD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYAULD.Location = new System.Drawing.Point(150, 34);
            this.btnBreakUnitTYAULD.Name = "btnBreakUnitTYAULD";
            this.btnBreakUnitTYAULD.Size = new System.Drawing.Size(121, 50);
            this.btnBreakUnitTYAULD.TabIndex = 63;
            this.btnBreakUnitTYAULD.Text = "언로드";
            this.btnBreakUnitTYAULD.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYALD
            // 
            this.btnBreakUnitTYALD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYALD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYALD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYALD.Location = new System.Drawing.Point(8, 34);
            this.btnBreakUnitTYALD.Name = "btnBreakUnitTYALD";
            this.btnBreakUnitTYALD.Size = new System.Drawing.Size(121, 50);
            this.btnBreakUnitTYALD.TabIndex = 62;
            this.btnBreakUnitTYALD.Text = "로드";
            this.btnBreakUnitTYALD.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB1BlowerOnOff
            // 
            this.btnBreakUnitTYB1BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYB1BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYB1BlowerOnOff.Location = new System.Drawing.Point(15, 101);
            this.btnBreakUnitTYB1BlowerOnOff.Name = "btnBreakUnitTYB1BlowerOnOff";
            this.btnBreakUnitTYB1BlowerOnOff.Size = new System.Drawing.Size(166, 65);
            this.btnBreakUnitTYB1BlowerOnOff.TabIndex = 62;
            this.btnBreakUnitTYB1BlowerOnOff.Text = "B1\r\n파기 온/오프";
            this.btnBreakUnitTYB1BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB2BlowerOnOff
            // 
            this.btnBreakUnitTYB2BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYB2BlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB2BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYB2BlowerOnOff.Location = new System.Drawing.Point(187, 101);
            this.btnBreakUnitTYB2BlowerOnOff.Name = "btnBreakUnitTYB2BlowerOnOff";
            this.btnBreakUnitTYB2BlowerOnOff.Size = new System.Drawing.Size(166, 65);
            this.btnBreakUnitTYB2BlowerOnOff.TabIndex = 60;
            this.btnBreakUnitTYB2BlowerOnOff.Text = "B2\r\n파기 온/오프";
            this.btnBreakUnitTYB2BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB1VaccumOff
            // 
            this.btnBreakUnitTYB1VaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYB1VaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB1VaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYB1VaccumOff.Location = new System.Drawing.Point(101, 30);
            this.btnBreakUnitTYB1VaccumOff.Name = "btnBreakUnitTYB1VaccumOff";
            this.btnBreakUnitTYB1VaccumOff.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYB1VaccumOff.TabIndex = 55;
            this.btnBreakUnitTYB1VaccumOff.Text = "B1\r\n공압 오프";
            this.btnBreakUnitTYB1VaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB1VaccumOn
            // 
            this.btnBreakUnitTYB1VaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYB1VaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB1VaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYB1VaccumOn.Location = new System.Drawing.Point(15, 30);
            this.btnBreakUnitTYB1VaccumOn.Name = "btnBreakUnitTYB1VaccumOn";
            this.btnBreakUnitTYB1VaccumOn.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYB1VaccumOn.TabIndex = 57;
            this.btnBreakUnitTYB1VaccumOn.Text = "B1\r\n공압 온";
            this.btnBreakUnitTYB1VaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB2VaccumOn
            // 
            this.btnBreakUnitTYB2VaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYB2VaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB2VaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYB2VaccumOn.Location = new System.Drawing.Point(187, 30);
            this.btnBreakUnitTYB2VaccumOn.Name = "btnBreakUnitTYB2VaccumOn";
            this.btnBreakUnitTYB2VaccumOn.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYB2VaccumOn.TabIndex = 59;
            this.btnBreakUnitTYB2VaccumOn.Text = "B2\r\n공압 온";
            this.btnBreakUnitTYB2VaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB2VaccumOff
            // 
            this.btnBreakUnitTYB2VaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYB2VaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB2VaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYB2VaccumOff.Location = new System.Drawing.Point(273, 30);
            this.btnBreakUnitTYB2VaccumOff.Name = "btnBreakUnitTYB2VaccumOff";
            this.btnBreakUnitTYB2VaccumOff.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYB2VaccumOff.TabIndex = 56;
            this.btnBreakUnitTYB2VaccumOff.Text = "B2\r\n공압 오프";
            this.btnBreakUnitTYB2VaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA1BlowerOnOff
            // 
            this.btnBreakUnitTYA1BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYA1BlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA1BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYA1BlowerOnOff.Location = new System.Drawing.Point(15, 101);
            this.btnBreakUnitTYA1BlowerOnOff.Name = "btnBreakUnitTYA1BlowerOnOff";
            this.btnBreakUnitTYA1BlowerOnOff.Size = new System.Drawing.Size(166, 65);
            this.btnBreakUnitTYA1BlowerOnOff.TabIndex = 62;
            this.btnBreakUnitTYA1BlowerOnOff.Text = "A1\r\n파기 온/오프";
            this.btnBreakUnitTYA1BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA2BlowerOnOff
            // 
            this.btnBreakUnitTYA2BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYA2BlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA2BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYA2BlowerOnOff.Location = new System.Drawing.Point(187, 101);
            this.btnBreakUnitTYA2BlowerOnOff.Name = "btnBreakUnitTYA2BlowerOnOff";
            this.btnBreakUnitTYA2BlowerOnOff.Size = new System.Drawing.Size(166, 65);
            this.btnBreakUnitTYA2BlowerOnOff.TabIndex = 60;
            this.btnBreakUnitTYA2BlowerOnOff.Text = "A2\r\n파기 온/오프";
            this.btnBreakUnitTYA2BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA1VaccumOff
            // 
            this.btnBreakUnitTYA1VaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYA1VaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA1VaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYA1VaccumOff.Location = new System.Drawing.Point(101, 30);
            this.btnBreakUnitTYA1VaccumOff.Name = "btnBreakUnitTYA1VaccumOff";
            this.btnBreakUnitTYA1VaccumOff.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYA1VaccumOff.TabIndex = 55;
            this.btnBreakUnitTYA1VaccumOff.Text = "A1\r\n공압 오프";
            this.btnBreakUnitTYA1VaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA1VaccumOn
            // 
            this.btnBreakUnitTYA1VaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYA1VaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA1VaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYA1VaccumOn.Location = new System.Drawing.Point(15, 30);
            this.btnBreakUnitTYA1VaccumOn.Name = "btnBreakUnitTYA1VaccumOn";
            this.btnBreakUnitTYA1VaccumOn.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYA1VaccumOn.TabIndex = 57;
            this.btnBreakUnitTYA1VaccumOn.Text = "A1\r\n공압 온";
            this.btnBreakUnitTYA1VaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA2VaccumOn
            // 
            this.btnBreakUnitTYA2VaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYA2VaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA2VaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYA2VaccumOn.Location = new System.Drawing.Point(187, 30);
            this.btnBreakUnitTYA2VaccumOn.Name = "btnBreakUnitTYA2VaccumOn";
            this.btnBreakUnitTYA2VaccumOn.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYA2VaccumOn.TabIndex = 59;
            this.btnBreakUnitTYA2VaccumOn.Text = "A2\r\n공압 온";
            this.btnBreakUnitTYA2VaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA2VaccumOff
            // 
            this.btnBreakUnitTYA2VaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYA2VaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA2VaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYA2VaccumOff.Location = new System.Drawing.Point(273, 30);
            this.btnBreakUnitTYA2VaccumOff.Name = "btnBreakUnitTYA2VaccumOff";
            this.btnBreakUnitTYA2VaccumOff.Size = new System.Drawing.Size(80, 65);
            this.btnBreakUnitTYA2VaccumOff.TabIndex = 56;
            this.btnBreakUnitTYA2VaccumOff.Text = "A2\r\n공압 오프";
            this.btnBreakUnitTYA2VaccumOff.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_Muting4
            // 
            this.btn_CasseteLoad_Muting4.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteLoad_Muting4.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteLoad_Muting4.Location = new System.Drawing.Point(97, 127);
            this.btn_CasseteLoad_Muting4.Name = "btn_CasseteLoad_Muting4";
            this.btn_CasseteLoad_Muting4.Size = new System.Drawing.Size(85, 44);
            this.btn_CasseteLoad_Muting4.TabIndex = 5;
            this.btn_CasseteLoad_Muting4.Text = "뮤팅 B 배출";
            this.btn_CasseteLoad_Muting4.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_Muting2
            // 
            this.btn_CasseteLoad_Muting2.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteLoad_Muting2.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteLoad_Muting2.Location = new System.Drawing.Point(6, 127);
            this.btn_CasseteLoad_Muting2.Name = "btn_CasseteLoad_Muting2";
            this.btn_CasseteLoad_Muting2.Size = new System.Drawing.Size(85, 44);
            this.btn_CasseteLoad_Muting2.TabIndex = 4;
            this.btn_CasseteLoad_Muting2.Text = "뮤팅 B 투입";
            this.btn_CasseteLoad_Muting2.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_Muting3
            // 
            this.btn_CasseteLoad_Muting3.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteLoad_Muting3.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteLoad_Muting3.Location = new System.Drawing.Point(97, 77);
            this.btn_CasseteLoad_Muting3.Name = "btn_CasseteLoad_Muting3";
            this.btn_CasseteLoad_Muting3.Size = new System.Drawing.Size(85, 44);
            this.btn_CasseteLoad_Muting3.TabIndex = 3;
            this.btn_CasseteLoad_Muting3.Text = "뮤팅 A 배출";
            this.btn_CasseteLoad_Muting3.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_Muting1
            // 
            this.btn_CasseteLoad_Muting1.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteLoad_Muting1.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteLoad_Muting1.Location = new System.Drawing.Point(6, 77);
            this.btn_CasseteLoad_Muting1.Name = "btn_CasseteLoad_Muting1";
            this.btn_CasseteLoad_Muting1.Size = new System.Drawing.Size(85, 44);
            this.btn_CasseteLoad_Muting1.TabIndex = 2;
            this.btn_CasseteLoad_Muting1.Text = "뮤팅 A 투입";
            this.btn_CasseteLoad_Muting1.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MutingOff
            // 
            this.btn_CasseteLoad_MutingOff.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteLoad_MutingOff.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteLoad_MutingOff.Location = new System.Drawing.Point(97, 27);
            this.btn_CasseteLoad_MutingOff.Name = "btn_CasseteLoad_MutingOff";
            this.btn_CasseteLoad_MutingOff.Size = new System.Drawing.Size(85, 44);
            this.btn_CasseteLoad_MutingOff.TabIndex = 1;
            this.btn_CasseteLoad_MutingOff.Text = "뮤팅 아웃";
            this.btn_CasseteLoad_MutingOff.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MutingIn
            // 
            this.btn_CasseteLoad_MutingIn.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteLoad_MutingIn.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteLoad_MutingIn.Location = new System.Drawing.Point(6, 27);
            this.btn_CasseteLoad_MutingIn.Name = "btn_CasseteLoad_MutingIn";
            this.btn_CasseteLoad_MutingIn.Size = new System.Drawing.Size(85, 44);
            this.btn_CasseteLoad_MutingIn.TabIndex = 0;
            this.btn_CasseteLoad_MutingIn.Text = "뮤팅 인";
            this.btn_CasseteLoad_MutingIn.UseVisualStyleBackColor = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.ItemSize = new System.Drawing.Size(5, 30);
            this.tabControl1.Location = new System.Drawing.Point(748, 382);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(977, 435);
            this.tabControl1.TabIndex = 96;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.panel64);
            this.tabPage1.Controls.Add(this.panel62);
            this.tabPage1.Controls.Add(this.panel63);
            this.tabPage1.Controls.Add(this.panel9);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(969, 397);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "카셋트 로드";
            // 
            // panel64
            // 
            this.panel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel64.Controls.Add(this.panel66);
            this.panel64.Controls.Add(this.panel67);
            this.panel64.Controls.Add(this.label67);
            this.panel64.Location = new System.Drawing.Point(7, 188);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(956, 201);
            this.panel64.TabIndex = 475;
            // 
            // panel66
            // 
            this.panel66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel66.Controls.Add(this.btnCstloadBCellOut);
            this.panel66.Controls.Add(this.btnCstloadBMeasure);
            this.panel66.Controls.Add(this.btnCstloadBLoader);
            this.panel66.Controls.Add(this.btnCstloadB90dgr);
            this.panel66.Controls.Add(this.btnCstloadB0dgr);
            this.panel66.Controls.Add(this.label65);
            this.panel66.Location = new System.Drawing.Point(354, 31);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(342, 160);
            this.panel66.TabIndex = 456;
            // 
            // btnCstloadBCellOut
            // 
            this.btnCstloadBCellOut.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadBCellOut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadBCellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadBCellOut.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadBCellOut.Location = new System.Drawing.Point(228, 95);
            this.btnCstloadBCellOut.Name = "btnCstloadBCellOut";
            this.btnCstloadBCellOut.Size = new System.Drawing.Size(103, 58);
            this.btnCstloadBCellOut.TabIndex = 64;
            this.btnCstloadBCellOut.Text = "셀 배출";
            this.btnCstloadBCellOut.UseVisualStyleBackColor = false;
            // 
            // label65
            // 
            this.label65.AutoEllipsis = true;
            this.label65.BackColor = System.Drawing.Color.Gainsboro;
            this.label65.Dock = System.Windows.Forms.DockStyle.Top;
            this.label65.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(0, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(340, 24);
            this.label65.TabIndex = 9;
            this.label65.Text = "■ B열";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel67
            // 
            this.panel67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel67.Controls.Add(this.btnCstloadAMeasure);
            this.panel67.Controls.Add(this.btnCstloadALoader);
            this.panel67.Controls.Add(this.btnCstloadA0dgr);
            this.panel67.Controls.Add(this.btnCstloadA90dgr);
            this.panel67.Controls.Add(this.btnCstloadACellOut);
            this.panel67.Controls.Add(this.label66);
            this.panel67.Location = new System.Drawing.Point(6, 31);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(342, 160);
            this.panel67.TabIndex = 455;
            // 
            // btnCstloadAMeasure
            // 
            this.btnCstloadAMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadAMeasure.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadAMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadAMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadAMeasure.Location = new System.Drawing.Point(118, 95);
            this.btnCstloadAMeasure.Name = "btnCstloadAMeasure";
            this.btnCstloadAMeasure.Size = new System.Drawing.Size(103, 58);
            this.btnCstloadAMeasure.TabIndex = 54;
            this.btnCstloadAMeasure.Text = "측정";
            this.btnCstloadAMeasure.UseVisualStyleBackColor = false;
            // 
            // btnCstloadALoader
            // 
            this.btnCstloadALoader.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadALoader.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadALoader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadALoader.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadALoader.Location = new System.Drawing.Point(118, 33);
            this.btnCstloadALoader.Name = "btnCstloadALoader";
            this.btnCstloadALoader.Size = new System.Drawing.Size(103, 55);
            this.btnCstloadALoader.TabIndex = 53;
            this.btnCstloadALoader.Text = "카세트 로더";
            this.btnCstloadALoader.UseVisualStyleBackColor = false;
            // 
            // btnCstloadA0dgr
            // 
            this.btnCstloadA0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadA0dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadA0dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadA0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadA0dgr.Location = new System.Drawing.Point(9, 33);
            this.btnCstloadA0dgr.Name = "btnCstloadA0dgr";
            this.btnCstloadA0dgr.Size = new System.Drawing.Size(103, 55);
            this.btnCstloadA0dgr.TabIndex = 44;
            this.btnCstloadA0dgr.Text = "카세트 0 도";
            this.btnCstloadA0dgr.UseVisualStyleBackColor = false;
            // 
            // btnCstloadA90dgr
            // 
            this.btnCstloadA90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadA90dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadA90dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadA90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadA90dgr.Location = new System.Drawing.Point(9, 95);
            this.btnCstloadA90dgr.Name = "btnCstloadA90dgr";
            this.btnCstloadA90dgr.Size = new System.Drawing.Size(103, 58);
            this.btnCstloadA90dgr.TabIndex = 45;
            this.btnCstloadA90dgr.Text = "카세트 90도";
            this.btnCstloadA90dgr.UseVisualStyleBackColor = false;
            // 
            // btnCstloadACellOut
            // 
            this.btnCstloadACellOut.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstloadACellOut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstloadACellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstloadACellOut.ForeColor = System.Drawing.Color.Black;
            this.btnCstloadACellOut.Location = new System.Drawing.Point(227, 95);
            this.btnCstloadACellOut.Name = "btnCstloadACellOut";
            this.btnCstloadACellOut.Size = new System.Drawing.Size(103, 58);
            this.btnCstloadACellOut.TabIndex = 52;
            this.btnCstloadACellOut.Text = "셀 배출";
            this.btnCstloadACellOut.UseVisualStyleBackColor = false;
            // 
            // label66
            // 
            this.label66.AutoEllipsis = true;
            this.label66.BackColor = System.Drawing.Color.Gainsboro;
            this.label66.Dock = System.Windows.Forms.DockStyle.Top;
            this.label66.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(0, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(340, 24);
            this.label66.TabIndex = 9;
            this.label66.Text = "■ A열";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.AutoEllipsis = true;
            this.label67.BackColor = System.Drawing.Color.Gainsboro;
            this.label67.Dock = System.Windows.Forms.DockStyle.Top;
            this.label67.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(0, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(954, 24);
            this.label67.TabIndex = 9;
            this.label67.Text = "■ 이동";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel62
            // 
            this.panel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel62.Controls.Add(this.btnLDTiltACylinderDown);
            this.panel62.Controls.Add(this.btnLDTiltACylinderUp);
            this.panel62.Controls.Add(this.label62);
            this.panel62.Controls.Add(this.btnLDCstAGrip);
            this.panel62.Controls.Add(this.btnLDCstAGripUn);
            this.panel62.Location = new System.Drawing.Point(7, 7);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(210, 175);
            this.panel62.TabIndex = 474;
            // 
            // btnLDTiltACylinderDown
            // 
            this.btnLDTiltACylinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDTiltACylinderDown.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDTiltACylinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnLDTiltACylinderDown.Location = new System.Drawing.Point(107, 101);
            this.btnLDTiltACylinderDown.Name = "btnLDTiltACylinderDown";
            this.btnLDTiltACylinderDown.Size = new System.Drawing.Size(93, 66);
            this.btnLDTiltACylinderDown.TabIndex = 25;
            this.btnLDTiltACylinderDown.Text = "카세트 틸트 실린더 다운";
            this.btnLDTiltACylinderDown.UseVisualStyleBackColor = false;
            // 
            // btnLDTiltACylinderUp
            // 
            this.btnLDTiltACylinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDTiltACylinderUp.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDTiltACylinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnLDTiltACylinderUp.Location = new System.Drawing.Point(6, 101);
            this.btnLDTiltACylinderUp.Name = "btnLDTiltACylinderUp";
            this.btnLDTiltACylinderUp.Size = new System.Drawing.Size(93, 66);
            this.btnLDTiltACylinderUp.TabIndex = 24;
            this.btnLDTiltACylinderUp.Text = "카세트 틸트 실린더 업";
            this.btnLDTiltACylinderUp.UseVisualStyleBackColor = false;
            // 
            // label62
            // 
            this.label62.AutoEllipsis = true;
            this.label62.BackColor = System.Drawing.Color.Gainsboro;
            this.label62.Dock = System.Windows.Forms.DockStyle.Top;
            this.label62.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(0, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(208, 24);
            this.label62.TabIndex = 9;
            this.label62.Text = "■ A열";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel63
            // 
            this.panel63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel63.Controls.Add(this.btnLDTiltBCylinderDown);
            this.panel63.Controls.Add(this.btnLDTiltBCylinderUp);
            this.panel63.Controls.Add(this.label63);
            this.panel63.Controls.Add(this.btnLDCstBGripUn);
            this.panel63.Controls.Add(this.btnLDCstBGrip);
            this.panel63.Location = new System.Drawing.Point(223, 7);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(210, 175);
            this.panel63.TabIndex = 473;
            // 
            // btnLDTiltBCylinderDown
            // 
            this.btnLDTiltBCylinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDTiltBCylinderDown.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDTiltBCylinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnLDTiltBCylinderDown.Location = new System.Drawing.Point(107, 101);
            this.btnLDTiltBCylinderDown.Name = "btnLDTiltBCylinderDown";
            this.btnLDTiltBCylinderDown.Size = new System.Drawing.Size(93, 66);
            this.btnLDTiltBCylinderDown.TabIndex = 29;
            this.btnLDTiltBCylinderDown.Text = "카세트 틸트 실린더 다운";
            this.btnLDTiltBCylinderDown.UseVisualStyleBackColor = false;
            // 
            // btnLDTiltBCylinderUp
            // 
            this.btnLDTiltBCylinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnLDTiltBCylinderUp.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLDTiltBCylinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnLDTiltBCylinderUp.Location = new System.Drawing.Point(6, 101);
            this.btnLDTiltBCylinderUp.Name = "btnLDTiltBCylinderUp";
            this.btnLDTiltBCylinderUp.Size = new System.Drawing.Size(93, 66);
            this.btnLDTiltBCylinderUp.TabIndex = 28;
            this.btnLDTiltBCylinderUp.Text = "카세트 틸트 실린더 업";
            this.btnLDTiltBCylinderUp.UseVisualStyleBackColor = false;
            // 
            // label63
            // 
            this.label63.AutoEllipsis = true;
            this.label63.BackColor = System.Drawing.Color.Gainsboro;
            this.label63.Dock = System.Windows.Forms.DockStyle.Top;
            this.label63.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(0, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(208, 24);
            this.label63.TabIndex = 9;
            this.label63.Text = "■ B열";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btn_CasseteLoad_Muting4);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.btn_CasseteLoad_Muting2);
            this.panel9.Controls.Add(this.btn_CasseteLoad_Muting1);
            this.panel9.Controls.Add(this.btn_CasseteLoad_Muting3);
            this.panel9.Controls.Add(this.btn_CasseteLoad_MutingIn);
            this.panel9.Controls.Add(this.btn_CasseteLoad_MutingOff);
            this.panel9.Location = new System.Drawing.Point(771, 7);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(190, 175);
            this.panel9.TabIndex = 457;
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.BackColor = System.Drawing.Color.Gainsboro;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(188, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "■ 뮤팅";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(969, 397);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "셀 취출 이재기";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panel25);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(6, 128);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(956, 263);
            this.panel5.TabIndex = 474;
            // 
            // panel25
            // 
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.btnLoaderY2AxisCstMiddle);
            this.panel25.Controls.Add(this.btnLoaderY2AxisCstAUnloading);
            this.panel25.Controls.Add(this.label25);
            this.panel25.Controls.Add(this.btnLoaderY2AxisCstBInner);
            this.panel25.Controls.Add(this.btnLoaderY2AxisCstBUnloading);
            this.panel25.Controls.Add(this.btnLoaderY2AxisCstAInner);
            this.panel25.Location = new System.Drawing.Point(714, 33);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(228, 220);
            this.panel25.TabIndex = 458;
            // 
            // btnLoaderY2AxisCstMiddle
            // 
            this.btnLoaderY2AxisCstMiddle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY2AxisCstMiddle.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY2AxisCstMiddle.Location = new System.Drawing.Point(11, 95);
            this.btnLoaderY2AxisCstMiddle.Name = "btnLoaderY2AxisCstMiddle";
            this.btnLoaderY2AxisCstMiddle.Size = new System.Drawing.Size(206, 55);
            this.btnLoaderY2AxisCstMiddle.TabIndex = 39;
            this.btnLoaderY2AxisCstMiddle.Text = "카셋트 중간";
            this.btnLoaderY2AxisCstMiddle.UseVisualStyleBackColor = false;
            // 
            // btnLoaderY2AxisCstAUnloading
            // 
            this.btnLoaderY2AxisCstAUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY2AxisCstAUnloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY2AxisCstAUnloading.Location = new System.Drawing.Point(11, 156);
            this.btnLoaderY2AxisCstAUnloading.Name = "btnLoaderY2AxisCstAUnloading";
            this.btnLoaderY2AxisCstAUnloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY2AxisCstAUnloading.TabIndex = 36;
            this.btnLoaderY2AxisCstAUnloading.Text = "A 언로딩";
            this.btnLoaderY2AxisCstAUnloading.UseVisualStyleBackColor = false;
            // 
            // label25
            // 
            this.label25.AutoEllipsis = true;
            this.label25.BackColor = System.Drawing.Color.Gainsboro;
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(0, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(226, 24);
            this.label25.TabIndex = 9;
            this.label25.Text = "■ Y 2번축";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoaderY2AxisCstBInner
            // 
            this.btnLoaderY2AxisCstBInner.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY2AxisCstBInner.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY2AxisCstBInner.Location = new System.Drawing.Point(117, 34);
            this.btnLoaderY2AxisCstBInner.Name = "btnLoaderY2AxisCstBInner";
            this.btnLoaderY2AxisCstBInner.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY2AxisCstBInner.TabIndex = 35;
            this.btnLoaderY2AxisCstBInner.Text = "B 안쪽";
            this.btnLoaderY2AxisCstBInner.UseVisualStyleBackColor = false;
            // 
            // btnLoaderY2AxisCstBUnloading
            // 
            this.btnLoaderY2AxisCstBUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY2AxisCstBUnloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY2AxisCstBUnloading.Location = new System.Drawing.Point(117, 156);
            this.btnLoaderY2AxisCstBUnloading.Name = "btnLoaderY2AxisCstBUnloading";
            this.btnLoaderY2AxisCstBUnloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY2AxisCstBUnloading.TabIndex = 38;
            this.btnLoaderY2AxisCstBUnloading.Text = "B 언로딩";
            this.btnLoaderY2AxisCstBUnloading.UseVisualStyleBackColor = false;
            // 
            // btnLoaderY2AxisCstAInner
            // 
            this.btnLoaderY2AxisCstAInner.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY2AxisCstAInner.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY2AxisCstAInner.Location = new System.Drawing.Point(11, 34);
            this.btnLoaderY2AxisCstAInner.Name = "btnLoaderY2AxisCstAInner";
            this.btnLoaderY2AxisCstAInner.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY2AxisCstAInner.TabIndex = 33;
            this.btnLoaderY2AxisCstAInner.Text = "A 안쪽";
            this.btnLoaderY2AxisCstAInner.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnLoaderY1AxisCstMiddle);
            this.panel8.Controls.Add(this.btnLoaderY1AxisCstAUnloading);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.btnLoaderY1AxisCstBInner);
            this.panel8.Controls.Add(this.btnLoaderY1AxisCstBUnloading);
            this.panel8.Controls.Add(this.btnLoaderY1AxisCstAInner);
            this.panel8.Location = new System.Drawing.Point(478, 33);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(228, 220);
            this.panel8.TabIndex = 456;
            // 
            // btnLoaderY1AxisCstMiddle
            // 
            this.btnLoaderY1AxisCstMiddle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY1AxisCstMiddle.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY1AxisCstMiddle.Location = new System.Drawing.Point(11, 95);
            this.btnLoaderY1AxisCstMiddle.Name = "btnLoaderY1AxisCstMiddle";
            this.btnLoaderY1AxisCstMiddle.Size = new System.Drawing.Size(206, 55);
            this.btnLoaderY1AxisCstMiddle.TabIndex = 39;
            this.btnLoaderY1AxisCstMiddle.Text = "카셋트 중간";
            this.btnLoaderY1AxisCstMiddle.UseVisualStyleBackColor = false;
            // 
            // btnLoaderY1AxisCstAUnloading
            // 
            this.btnLoaderY1AxisCstAUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY1AxisCstAUnloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY1AxisCstAUnloading.Location = new System.Drawing.Point(12, 156);
            this.btnLoaderY1AxisCstAUnloading.Name = "btnLoaderY1AxisCstAUnloading";
            this.btnLoaderY1AxisCstAUnloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY1AxisCstAUnloading.TabIndex = 36;
            this.btnLoaderY1AxisCstAUnloading.Text = "A 언로딩";
            this.btnLoaderY1AxisCstAUnloading.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(226, 24);
            this.label8.TabIndex = 9;
            this.label8.Text = "■ Y 1번축";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoaderY1AxisCstBInner
            // 
            this.btnLoaderY1AxisCstBInner.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY1AxisCstBInner.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY1AxisCstBInner.Location = new System.Drawing.Point(117, 34);
            this.btnLoaderY1AxisCstBInner.Name = "btnLoaderY1AxisCstBInner";
            this.btnLoaderY1AxisCstBInner.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY1AxisCstBInner.TabIndex = 35;
            this.btnLoaderY1AxisCstBInner.Text = "B 안쪽";
            this.btnLoaderY1AxisCstBInner.UseVisualStyleBackColor = false;
            // 
            // btnLoaderY1AxisCstBUnloading
            // 
            this.btnLoaderY1AxisCstBUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY1AxisCstBUnloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY1AxisCstBUnloading.Location = new System.Drawing.Point(118, 156);
            this.btnLoaderY1AxisCstBUnloading.Name = "btnLoaderY1AxisCstBUnloading";
            this.btnLoaderY1AxisCstBUnloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY1AxisCstBUnloading.TabIndex = 38;
            this.btnLoaderY1AxisCstBUnloading.Text = "B 언로딩";
            this.btnLoaderY1AxisCstBUnloading.UseVisualStyleBackColor = false;
            // 
            // btnLoaderY1AxisCstAInner
            // 
            this.btnLoaderY1AxisCstAInner.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderY1AxisCstAInner.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderY1AxisCstAInner.Location = new System.Drawing.Point(11, 34);
            this.btnLoaderY1AxisCstAInner.Name = "btnLoaderY1AxisCstAInner";
            this.btnLoaderY1AxisCstAInner.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderY1AxisCstAInner.TabIndex = 33;
            this.btnLoaderY1AxisCstAInner.Text = "A 안쪽";
            this.btnLoaderY1AxisCstAInner.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnLoaderAxisX2Cst_B_Wait);
            this.panel7.Controls.Add(this.btnLoaderAxisX2Cst_B_Middle);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.btnLoaderAxisX2Cst_B_Unloading);
            this.panel7.Controls.Add(this.btnLoaderAxisX2Cst_A_Unloading);
            this.panel7.Controls.Add(this.btnLoaderAxisX2Cst_A_Wait);
            this.panel7.Controls.Add(this.btnLoaderAxisX2Cst_A_Middle);
            this.panel7.Location = new System.Drawing.Point(244, 33);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(228, 220);
            this.panel7.TabIndex = 456;
            // 
            // btnLoaderAxisX2Cst_B_Wait
            // 
            this.btnLoaderAxisX2Cst_B_Wait.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX2Cst_B_Wait.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2Cst_B_Wait.Location = new System.Drawing.Point(117, 93);
            this.btnLoaderAxisX2Cst_B_Wait.Name = "btnLoaderAxisX2Cst_B_Wait";
            this.btnLoaderAxisX2Cst_B_Wait.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX2Cst_B_Wait.TabIndex = 32;
            this.btnLoaderAxisX2Cst_B_Wait.Text = "B 대기";
            this.btnLoaderAxisX2Cst_B_Wait.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX2Cst_B_Middle
            // 
            this.btnLoaderAxisX2Cst_B_Middle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX2Cst_B_Middle.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2Cst_B_Middle.Location = new System.Drawing.Point(117, 32);
            this.btnLoaderAxisX2Cst_B_Middle.Name = "btnLoaderAxisX2Cst_B_Middle";
            this.btnLoaderAxisX2Cst_B_Middle.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX2Cst_B_Middle.TabIndex = 31;
            this.btnLoaderAxisX2Cst_B_Middle.Text = "B 중간";
            this.btnLoaderAxisX2Cst_B_Middle.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(226, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "■ X 2번축";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoaderAxisX2Cst_B_Unloading
            // 
            this.btnLoaderAxisX2Cst_B_Unloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX2Cst_B_Unloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2Cst_B_Unloading.Location = new System.Drawing.Point(117, 154);
            this.btnLoaderAxisX2Cst_B_Unloading.Name = "btnLoaderAxisX2Cst_B_Unloading";
            this.btnLoaderAxisX2Cst_B_Unloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX2Cst_B_Unloading.TabIndex = 26;
            this.btnLoaderAxisX2Cst_B_Unloading.Text = "A 언로딩";
            this.btnLoaderAxisX2Cst_B_Unloading.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX2Cst_A_Unloading
            // 
            this.btnLoaderAxisX2Cst_A_Unloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX2Cst_A_Unloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2Cst_A_Unloading.Location = new System.Drawing.Point(11, 154);
            this.btnLoaderAxisX2Cst_A_Unloading.Name = "btnLoaderAxisX2Cst_A_Unloading";
            this.btnLoaderAxisX2Cst_A_Unloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX2Cst_A_Unloading.TabIndex = 26;
            this.btnLoaderAxisX2Cst_A_Unloading.Text = "A 언로딩";
            this.btnLoaderAxisX2Cst_A_Unloading.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX2Cst_A_Wait
            // 
            this.btnLoaderAxisX2Cst_A_Wait.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX2Cst_A_Wait.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2Cst_A_Wait.Location = new System.Drawing.Point(11, 93);
            this.btnLoaderAxisX2Cst_A_Wait.Name = "btnLoaderAxisX2Cst_A_Wait";
            this.btnLoaderAxisX2Cst_A_Wait.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX2Cst_A_Wait.TabIndex = 30;
            this.btnLoaderAxisX2Cst_A_Wait.Text = "A 대기";
            this.btnLoaderAxisX2Cst_A_Wait.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX2Cst_A_Middle
            // 
            this.btnLoaderAxisX2Cst_A_Middle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX2Cst_A_Middle.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX2Cst_A_Middle.Location = new System.Drawing.Point(11, 32);
            this.btnLoaderAxisX2Cst_A_Middle.Name = "btnLoaderAxisX2Cst_A_Middle";
            this.btnLoaderAxisX2Cst_A_Middle.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX2Cst_A_Middle.TabIndex = 29;
            this.btnLoaderAxisX2Cst_A_Middle.Text = "A 중간";
            this.btnLoaderAxisX2Cst_A_Middle.UseVisualStyleBackColor = false;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnLoaderAxisX1Cst_B_Wait);
            this.panel6.Controls.Add(this.btnLoaderAxisX1Cst_B_Middle);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.btnLoaderAxisX1Cst_B_Unloading);
            this.panel6.Controls.Add(this.btnLoaderAxisX1Cst_A_Unloading);
            this.panel6.Controls.Add(this.btnLoaderAxisX1Cst_A_Wait);
            this.panel6.Controls.Add(this.btnLoaderAxisX1Cst_A_Middle);
            this.panel6.Location = new System.Drawing.Point(10, 33);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(228, 220);
            this.panel6.TabIndex = 455;
            // 
            // btnLoaderAxisX1Cst_B_Wait
            // 
            this.btnLoaderAxisX1Cst_B_Wait.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX1Cst_B_Wait.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1Cst_B_Wait.Location = new System.Drawing.Point(117, 94);
            this.btnLoaderAxisX1Cst_B_Wait.Name = "btnLoaderAxisX1Cst_B_Wait";
            this.btnLoaderAxisX1Cst_B_Wait.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX1Cst_B_Wait.TabIndex = 32;
            this.btnLoaderAxisX1Cst_B_Wait.Text = "B 대기";
            this.btnLoaderAxisX1Cst_B_Wait.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX1Cst_B_Middle
            // 
            this.btnLoaderAxisX1Cst_B_Middle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX1Cst_B_Middle.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1Cst_B_Middle.Location = new System.Drawing.Point(117, 32);
            this.btnLoaderAxisX1Cst_B_Middle.Name = "btnLoaderAxisX1Cst_B_Middle";
            this.btnLoaderAxisX1Cst_B_Middle.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX1Cst_B_Middle.TabIndex = 31;
            this.btnLoaderAxisX1Cst_B_Middle.Text = "B 중간";
            this.btnLoaderAxisX1Cst_B_Middle.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(226, 24);
            this.label6.TabIndex = 9;
            this.label6.Text = "■ X 1번축";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoaderAxisX1Cst_B_Unloading
            // 
            this.btnLoaderAxisX1Cst_B_Unloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX1Cst_B_Unloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1Cst_B_Unloading.Location = new System.Drawing.Point(117, 156);
            this.btnLoaderAxisX1Cst_B_Unloading.Name = "btnLoaderAxisX1Cst_B_Unloading";
            this.btnLoaderAxisX1Cst_B_Unloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX1Cst_B_Unloading.TabIndex = 26;
            this.btnLoaderAxisX1Cst_B_Unloading.Text = "A 언로딩";
            this.btnLoaderAxisX1Cst_B_Unloading.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX1Cst_A_Unloading
            // 
            this.btnLoaderAxisX1Cst_A_Unloading.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX1Cst_A_Unloading.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1Cst_A_Unloading.Location = new System.Drawing.Point(11, 156);
            this.btnLoaderAxisX1Cst_A_Unloading.Name = "btnLoaderAxisX1Cst_A_Unloading";
            this.btnLoaderAxisX1Cst_A_Unloading.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX1Cst_A_Unloading.TabIndex = 26;
            this.btnLoaderAxisX1Cst_A_Unloading.Text = "A 언로딩";
            this.btnLoaderAxisX1Cst_A_Unloading.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX1Cst_A_Wait
            // 
            this.btnLoaderAxisX1Cst_A_Wait.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX1Cst_A_Wait.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1Cst_A_Wait.Location = new System.Drawing.Point(11, 95);
            this.btnLoaderAxisX1Cst_A_Wait.Name = "btnLoaderAxisX1Cst_A_Wait";
            this.btnLoaderAxisX1Cst_A_Wait.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX1Cst_A_Wait.TabIndex = 26;
            this.btnLoaderAxisX1Cst_A_Wait.Text = "A 대기";
            this.btnLoaderAxisX1Cst_A_Wait.UseVisualStyleBackColor = false;
            // 
            // btnLoaderAxisX1Cst_A_Middle
            // 
            this.btnLoaderAxisX1Cst_A_Middle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnLoaderAxisX1Cst_A_Middle.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderAxisX1Cst_A_Middle.Location = new System.Drawing.Point(11, 32);
            this.btnLoaderAxisX1Cst_A_Middle.Name = "btnLoaderAxisX1Cst_A_Middle";
            this.btnLoaderAxisX1Cst_A_Middle.Size = new System.Drawing.Size(100, 55);
            this.btnLoaderAxisX1Cst_A_Middle.TabIndex = 25;
            this.btnLoaderAxisX1Cst_A_Middle.Text = "A 중간";
            this.btnLoaderAxisX1Cst_A_Middle.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(954, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "■ 이동";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnLoaderAxisX1BlowerOnOff);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnLoaderAxisX1VaccumOn);
            this.panel1.Controls.Add(this.btnLoaderAxisX1VaccumOff);
            this.panel1.Location = new System.Drawing.Point(7, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(289, 115);
            this.panel1.TabIndex = 454;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ X 1번축";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnLoaderAxisX2BlowerOnOff);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.btnLoaderAxisX2VaccumOn);
            this.panel3.Controls.Add(this.btnLoaderAxisX2VaccumOff);
            this.panel3.Location = new System.Drawing.Point(302, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(289, 115);
            this.panel3.TabIndex = 455;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(287, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "■ X 2번축";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.panel26);
            this.tabPage3.Controls.Add(this.panel27);
            this.tabPage3.Controls.Add(this.panel18);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(969, 397);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "셀 로드 이재기";
            // 
            // panel26
            // 
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel26.Controls.Add(this.btnCellLoadB2PickerBlowerOnOff);
            this.panel26.Controls.Add(this.btnCellLoadB2PickerDown);
            this.panel26.Controls.Add(this.btnCellLoadB2PickerUp);
            this.panel26.Controls.Add(this.btnCellLoadB2PickerVaccumOff);
            this.panel26.Controls.Add(this.btnCellLoadB2PickerVaccumOn);
            this.panel26.Controls.Add(this.btnCellLoadB1PickerBlowerOnOff);
            this.panel26.Controls.Add(this.label26);
            this.panel26.Controls.Add(this.btnCellLoadB1PickerDown);
            this.panel26.Controls.Add(this.btnCellLoadB1PickerUp);
            this.panel26.Controls.Add(this.btnCellLoadB1PickerVaccumOff);
            this.panel26.Controls.Add(this.btnCellLoadB1PickerVaccumOn);
            this.panel26.Location = new System.Drawing.Point(481, 7);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(465, 175);
            this.panel26.TabIndex = 463;
            // 
            // btnCellLoadB2PickerBlowerOnOff
            // 
            this.btnCellLoadB2PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB2PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB2PickerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnCellLoadB2PickerBlowerOnOff.Name = "btnCellLoadB2PickerBlowerOnOff";
            this.btnCellLoadB2PickerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnCellLoadB2PickerBlowerOnOff.TabIndex = 69;
            this.btnCellLoadB2PickerBlowerOnOff.Text = "B2\r\n피커 파기 온/오프";
            this.btnCellLoadB2PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB2PickerDown
            // 
            this.btnCellLoadB2PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB2PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB2PickerDown.Location = new System.Drawing.Point(348, 29);
            this.btnCellLoadB2PickerDown.Name = "btnCellLoadB2PickerDown";
            this.btnCellLoadB2PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB2PickerDown.TabIndex = 65;
            this.btnCellLoadB2PickerDown.Text = "B2\r\n피커 다운";
            this.btnCellLoadB2PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB2PickerUp
            // 
            this.btnCellLoadB2PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB2PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB2PickerUp.Location = new System.Drawing.Point(234, 29);
            this.btnCellLoadB2PickerUp.Name = "btnCellLoadB2PickerUp";
            this.btnCellLoadB2PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB2PickerUp.TabIndex = 64;
            this.btnCellLoadB2PickerUp.Text = "B2\r\n피커 업";
            this.btnCellLoadB2PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB2PickerVaccumOff
            // 
            this.btnCellLoadB2PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB2PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB2PickerVaccumOff.Location = new System.Drawing.Point(348, 77);
            this.btnCellLoadB2PickerVaccumOff.Name = "btnCellLoadB2PickerVaccumOff";
            this.btnCellLoadB2PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB2PickerVaccumOff.TabIndex = 67;
            this.btnCellLoadB2PickerVaccumOff.Text = "B2\r\n피커 공압 오프";
            this.btnCellLoadB2PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadB2PickerVaccumOn
            // 
            this.btnCellLoadB2PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadB2PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadB2PickerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnCellLoadB2PickerVaccumOn.Name = "btnCellLoadB2PickerVaccumOn";
            this.btnCellLoadB2PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadB2PickerVaccumOn.TabIndex = 66;
            this.btnCellLoadB2PickerVaccumOn.Text = "B2\r\n피커 공압 온";
            this.btnCellLoadB2PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // label26
            // 
            this.label26.AutoEllipsis = true;
            this.label26.BackColor = System.Drawing.Color.Gainsboro;
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(0, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(463, 24);
            this.label26.TabIndex = 9;
            this.label26.Text = "■ B열";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel27
            // 
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.btnCellLoadA2PickerBlowerOnOff);
            this.panel27.Controls.Add(this.btnCellLoadA2PickerDown);
            this.panel27.Controls.Add(this.btnCellLoadA2PickerUp);
            this.panel27.Controls.Add(this.btnCellLoadA2PickerVaccumOff);
            this.panel27.Controls.Add(this.btnCellLoadA2PickerVaccumOn);
            this.panel27.Controls.Add(this.btnCellLoadA1PickerBlowerOnOff);
            this.panel27.Controls.Add(this.label27);
            this.panel27.Controls.Add(this.btnCellLoadA1PickerDown);
            this.panel27.Controls.Add(this.btnCellLoadA1PickerUp);
            this.panel27.Controls.Add(this.btnCellLoadA1PickerVaccumOff);
            this.panel27.Controls.Add(this.btnCellLoadA1PickerVaccumOn);
            this.panel27.Location = new System.Drawing.Point(7, 7);
            this.panel27.Name = "panel27";
            this.panel27.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel27.Size = new System.Drawing.Size(465, 175);
            this.panel27.TabIndex = 462;
            // 
            // btnCellLoadA2PickerBlowerOnOff
            // 
            this.btnCellLoadA2PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA2PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA2PickerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnCellLoadA2PickerBlowerOnOff.Name = "btnCellLoadA2PickerBlowerOnOff";
            this.btnCellLoadA2PickerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnCellLoadA2PickerBlowerOnOff.TabIndex = 69;
            this.btnCellLoadA2PickerBlowerOnOff.Text = "A2\r\n피커 파기 온/오프";
            this.btnCellLoadA2PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA2PickerDown
            // 
            this.btnCellLoadA2PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA2PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA2PickerDown.Location = new System.Drawing.Point(348, 29);
            this.btnCellLoadA2PickerDown.Name = "btnCellLoadA2PickerDown";
            this.btnCellLoadA2PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA2PickerDown.TabIndex = 65;
            this.btnCellLoadA2PickerDown.Text = "A2\r\n피커 다운";
            this.btnCellLoadA2PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA2PickerUp
            // 
            this.btnCellLoadA2PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA2PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA2PickerUp.Location = new System.Drawing.Point(234, 29);
            this.btnCellLoadA2PickerUp.Name = "btnCellLoadA2PickerUp";
            this.btnCellLoadA2PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA2PickerUp.TabIndex = 64;
            this.btnCellLoadA2PickerUp.Text = "A2\r\n피커 업";
            this.btnCellLoadA2PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA2PickerVaccumOff
            // 
            this.btnCellLoadA2PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA2PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA2PickerVaccumOff.Location = new System.Drawing.Point(348, 75);
            this.btnCellLoadA2PickerVaccumOff.Name = "btnCellLoadA2PickerVaccumOff";
            this.btnCellLoadA2PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA2PickerVaccumOff.TabIndex = 67;
            this.btnCellLoadA2PickerVaccumOff.Text = "A2\r\n피커 공압 오프";
            this.btnCellLoadA2PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadA2PickerVaccumOn
            // 
            this.btnCellLoadA2PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA2PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA2PickerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnCellLoadA2PickerVaccumOn.Name = "btnCellLoadA2PickerVaccumOn";
            this.btnCellLoadA2PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA2PickerVaccumOn.TabIndex = 66;
            this.btnCellLoadA2PickerVaccumOn.Text = "A2\r\n피커 공압 온";
            this.btnCellLoadA2PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // label27
            // 
            this.label27.AutoEllipsis = true;
            this.label27.BackColor = System.Drawing.Color.Gainsboro;
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(463, 24);
            this.label27.TabIndex = 9;
            this.label27.Text = "■ A열";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellLoadA1PickerVaccumOff
            // 
            this.btnCellLoadA1PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadA1PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadA1PickerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnCellLoadA1PickerVaccumOff.Name = "btnCellLoadA1PickerVaccumOff";
            this.btnCellLoadA1PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnCellLoadA1PickerVaccumOff.TabIndex = 61;
            this.btnCellLoadA1PickerVaccumOff.Text = "A1\r\n피커 공압 오프";
            this.btnCellLoadA1PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.panel84);
            this.panel18.Controls.Add(this.panel28);
            this.panel18.Controls.Add(this.panel29);
            this.panel18.Controls.Add(this.panel30);
            this.panel18.Controls.Add(this.label21);
            this.panel18.Location = new System.Drawing.Point(7, 188);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(956, 201);
            this.panel18.TabIndex = 460;
            // 
            // panel84
            // 
            this.panel84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel84.Controls.Add(this.btnPreAlignB);
            this.panel84.Controls.Add(this.btnPreAlignA);
            this.panel84.Controls.Add(this.label84);
            this.panel84.Location = new System.Drawing.Point(801, 33);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(150, 160);
            this.panel84.TabIndex = 460;
            // 
            // btnPreAlignB
            // 
            this.btnPreAlignB.BackColor = System.Drawing.SystemColors.Control;
            this.btnPreAlignB.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPreAlignB.ForeColor = System.Drawing.Color.Black;
            this.btnPreAlignB.Location = new System.Drawing.Point(10, 92);
            this.btnPreAlignB.Name = "btnPreAlignB";
            this.btnPreAlignB.Size = new System.Drawing.Size(130, 61);
            this.btnPreAlignB.TabIndex = 60;
            this.btnPreAlignB.Text = "B열 Align";
            this.btnPreAlignB.UseVisualStyleBackColor = false;
            // 
            // btnPreAlignA
            // 
            this.btnPreAlignA.BackColor = System.Drawing.SystemColors.Control;
            this.btnPreAlignA.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPreAlignA.ForeColor = System.Drawing.Color.Black;
            this.btnPreAlignA.Location = new System.Drawing.Point(10, 27);
            this.btnPreAlignA.Name = "btnPreAlignA";
            this.btnPreAlignA.Size = new System.Drawing.Size(130, 61);
            this.btnPreAlignA.TabIndex = 59;
            this.btnPreAlignA.Text = "A열 Align";
            this.btnPreAlignA.UseVisualStyleBackColor = false;
            // 
            // label84
            // 
            this.label84.AutoEllipsis = true;
            this.label84.BackColor = System.Drawing.Color.Gainsboro;
            this.label84.Dock = System.Windows.Forms.DockStyle.Top;
            this.label84.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(0, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(148, 24);
            this.label84.TabIndex = 9;
            this.label84.Text = "■ Pre Align";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel28
            // 
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.btnCellLoadBPickerMinus90);
            this.panel28.Controls.Add(this.btnCellLoadBPickerPlus90);
            this.panel28.Controls.Add(this.btnCellLoadBPicker0);
            this.panel28.Controls.Add(this.btnCellLoadBCellULD);
            this.panel28.Controls.Add(this.btnCellLoadBCellLD);
            this.panel28.Controls.Add(this.btnCellLoadBMcrReading);
            this.panel28.Controls.Add(this.btnCellLoadBVisionAlign);
            this.panel28.Controls.Add(this.label28);
            this.panel28.Location = new System.Drawing.Point(558, 33);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(237, 160);
            this.panel28.TabIndex = 459;
            // 
            // btnCellLoadBPickerMinus90
            // 
            this.btnCellLoadBPickerMinus90.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadBPickerMinus90.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadBPickerMinus90.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadBPickerMinus90.Location = new System.Drawing.Point(159, 115);
            this.btnCellLoadBPickerMinus90.Name = "btnCellLoadBPickerMinus90";
            this.btnCellLoadBPickerMinus90.Size = new System.Drawing.Size(72, 38);
            this.btnCellLoadBPickerMinus90.TabIndex = 67;
            this.btnCellLoadBPickerMinus90.Text = "B 피커\r\n-90도";
            this.btnCellLoadBPickerMinus90.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBPickerPlus90
            // 
            this.btnCellLoadBPickerPlus90.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadBPickerPlus90.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadBPickerPlus90.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadBPickerPlus90.Location = new System.Drawing.Point(81, 115);
            this.btnCellLoadBPickerPlus90.Name = "btnCellLoadBPickerPlus90";
            this.btnCellLoadBPickerPlus90.Size = new System.Drawing.Size(72, 38);
            this.btnCellLoadBPickerPlus90.TabIndex = 66;
            this.btnCellLoadBPickerPlus90.Text = "B 피커\r\n+90도";
            this.btnCellLoadBPickerPlus90.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBPicker0
            // 
            this.btnCellLoadBPicker0.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadBPicker0.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadBPicker0.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadBPicker0.Location = new System.Drawing.Point(3, 115);
            this.btnCellLoadBPicker0.Name = "btnCellLoadBPicker0";
            this.btnCellLoadBPicker0.Size = new System.Drawing.Size(72, 38);
            this.btnCellLoadBPicker0.TabIndex = 65;
            this.btnCellLoadBPicker0.Text = "B 피커\r\n0도";
            this.btnCellLoadBPicker0.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBCellULD
            // 
            this.btnCellLoadBCellULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadBCellULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadBCellULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadBCellULD.Location = new System.Drawing.Point(122, 71);
            this.btnCellLoadBCellULD.Name = "btnCellLoadBCellULD";
            this.btnCellLoadBCellULD.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadBCellULD.TabIndex = 61;
            this.btnCellLoadBCellULD.Text = "B ULD";
            this.btnCellLoadBCellULD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBCellLD
            // 
            this.btnCellLoadBCellLD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadBCellLD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadBCellLD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadBCellLD.Location = new System.Drawing.Point(3, 71);
            this.btnCellLoadBCellLD.Name = "btnCellLoadBCellLD";
            this.btnCellLoadBCellLD.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadBCellLD.TabIndex = 60;
            this.btnCellLoadBCellLD.Text = "B LD";
            this.btnCellLoadBCellLD.UseVisualStyleBackColor = false;
            // 
            // label28
            // 
            this.label28.AutoEllipsis = true;
            this.label28.BackColor = System.Drawing.Color.Gainsboro;
            this.label28.Dock = System.Windows.Forms.DockStyle.Top;
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(235, 24);
            this.label28.TabIndex = 9;
            this.label28.Text = "■ B열";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.btnCellLoadX4ULD);
            this.panel29.Controls.Add(this.btnCellLoadX4PreAlign);
            this.panel29.Controls.Add(this.btnCellLoadX4LD);
            this.panel29.Controls.Add(this.btnCellLoadX3ULD);
            this.panel29.Controls.Add(this.btnCellLoadX1ULD);
            this.panel29.Controls.Add(this.btnCellLoadX2ULD);
            this.panel29.Controls.Add(this.btnCellLoadX3PreAlign);
            this.panel29.Controls.Add(this.btnCellLoadX1PreAlign);
            this.panel29.Controls.Add(this.btnCellLoadX2PreAlign);
            this.panel29.Controls.Add(this.btnCellLoadX3LD);
            this.panel29.Controls.Add(this.label29);
            this.panel29.Controls.Add(this.btnCellLoadX1LD);
            this.panel29.Controls.Add(this.btnCellLoadX2LD);
            this.panel29.Location = new System.Drawing.Point(3, 32);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(302, 160);
            this.panel29.TabIndex = 458;
            // 
            // btnCellLoadX4ULD
            // 
            this.btnCellLoadX4ULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX4ULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX4ULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX4ULD.Location = new System.Drawing.Point(202, 124);
            this.btnCellLoadX4ULD.Name = "btnCellLoadX4ULD";
            this.btnCellLoadX4ULD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX4ULD.TabIndex = 69;
            this.btnCellLoadX4ULD.Text = "X4 ULD";
            this.btnCellLoadX4ULD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX4PreAlign
            // 
            this.btnCellLoadX4PreAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX4PreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX4PreAlign.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX4PreAlign.Location = new System.Drawing.Point(103, 124);
            this.btnCellLoadX4PreAlign.Name = "btnCellLoadX4PreAlign";
            this.btnCellLoadX4PreAlign.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX4PreAlign.TabIndex = 68;
            this.btnCellLoadX4PreAlign.Text = "X4 PreAlign";
            this.btnCellLoadX4PreAlign.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX4LD
            // 
            this.btnCellLoadX4LD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX4LD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX4LD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX4LD.Location = new System.Drawing.Point(3, 124);
            this.btnCellLoadX4LD.Name = "btnCellLoadX4LD";
            this.btnCellLoadX4LD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX4LD.TabIndex = 67;
            this.btnCellLoadX4LD.Text = "X4 LD";
            this.btnCellLoadX4LD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX3ULD
            // 
            this.btnCellLoadX3ULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX3ULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX3ULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX3ULD.Location = new System.Drawing.Point(202, 91);
            this.btnCellLoadX3ULD.Name = "btnCellLoadX3ULD";
            this.btnCellLoadX3ULD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX3ULD.TabIndex = 66;
            this.btnCellLoadX3ULD.Text = "X3 ULD";
            this.btnCellLoadX3ULD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX1ULD
            // 
            this.btnCellLoadX1ULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX1ULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX1ULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX1ULD.Location = new System.Drawing.Point(202, 27);
            this.btnCellLoadX1ULD.Name = "btnCellLoadX1ULD";
            this.btnCellLoadX1ULD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX1ULD.TabIndex = 65;
            this.btnCellLoadX1ULD.Text = "X1 ULD";
            this.btnCellLoadX1ULD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX2ULD
            // 
            this.btnCellLoadX2ULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX2ULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX2ULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX2ULD.Location = new System.Drawing.Point(202, 59);
            this.btnCellLoadX2ULD.Name = "btnCellLoadX2ULD";
            this.btnCellLoadX2ULD.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX2ULD.TabIndex = 64;
            this.btnCellLoadX2ULD.Text = "X2 ULD";
            this.btnCellLoadX2ULD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX3PreAlign
            // 
            this.btnCellLoadX3PreAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX3PreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX3PreAlign.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX3PreAlign.Location = new System.Drawing.Point(103, 91);
            this.btnCellLoadX3PreAlign.Name = "btnCellLoadX3PreAlign";
            this.btnCellLoadX3PreAlign.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX3PreAlign.TabIndex = 63;
            this.btnCellLoadX3PreAlign.Text = "X3 PreAlign";
            this.btnCellLoadX3PreAlign.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX1PreAlign
            // 
            this.btnCellLoadX1PreAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX1PreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX1PreAlign.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX1PreAlign.Location = new System.Drawing.Point(103, 27);
            this.btnCellLoadX1PreAlign.Name = "btnCellLoadX1PreAlign";
            this.btnCellLoadX1PreAlign.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX1PreAlign.TabIndex = 62;
            this.btnCellLoadX1PreAlign.Text = "X1 PreAlign";
            this.btnCellLoadX1PreAlign.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadX2PreAlign
            // 
            this.btnCellLoadX2PreAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadX2PreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadX2PreAlign.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadX2PreAlign.Location = new System.Drawing.Point(103, 59);
            this.btnCellLoadX2PreAlign.Name = "btnCellLoadX2PreAlign";
            this.btnCellLoadX2PreAlign.Size = new System.Drawing.Size(95, 30);
            this.btnCellLoadX2PreAlign.TabIndex = 61;
            this.btnCellLoadX2PreAlign.Text = "X2 PreAlign";
            this.btnCellLoadX2PreAlign.UseVisualStyleBackColor = false;
            // 
            // label29
            // 
            this.label29.AutoEllipsis = true;
            this.label29.BackColor = System.Drawing.Color.Gainsboro;
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(0, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(300, 24);
            this.label29.TabIndex = 9;
            this.label29.Text = "■ 로딩 / 언로딩";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.btnCellLoadAPickerMinus90);
            this.panel30.Controls.Add(this.btnCellLoadAPickerPlus90);
            this.panel30.Controls.Add(this.btnCellLoadAPicker0);
            this.panel30.Controls.Add(this.btnCellLoadACellULD);
            this.panel30.Controls.Add(this.btnCellLoadACellLD);
            this.panel30.Controls.Add(this.btnCellLoadAMcrReading);
            this.panel30.Controls.Add(this.btnCellLoadAVisionAlign);
            this.panel30.Controls.Add(this.label30);
            this.panel30.Location = new System.Drawing.Point(316, 32);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(236, 160);
            this.panel30.TabIndex = 457;
            // 
            // btnCellLoadAPickerMinus90
            // 
            this.btnCellLoadAPickerMinus90.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadAPickerMinus90.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadAPickerMinus90.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadAPickerMinus90.Location = new System.Drawing.Point(158, 115);
            this.btnCellLoadAPickerMinus90.Name = "btnCellLoadAPickerMinus90";
            this.btnCellLoadAPickerMinus90.Size = new System.Drawing.Size(72, 38);
            this.btnCellLoadAPickerMinus90.TabIndex = 64;
            this.btnCellLoadAPickerMinus90.Text = "A 피커\r\n-90도";
            this.btnCellLoadAPickerMinus90.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAPickerPlus90
            // 
            this.btnCellLoadAPickerPlus90.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadAPickerPlus90.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadAPickerPlus90.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadAPickerPlus90.Location = new System.Drawing.Point(80, 115);
            this.btnCellLoadAPickerPlus90.Name = "btnCellLoadAPickerPlus90";
            this.btnCellLoadAPickerPlus90.Size = new System.Drawing.Size(72, 38);
            this.btnCellLoadAPickerPlus90.TabIndex = 63;
            this.btnCellLoadAPickerPlus90.Text = "A 피커\r\n+90도";
            this.btnCellLoadAPickerPlus90.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAPicker0
            // 
            this.btnCellLoadAPicker0.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadAPicker0.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadAPicker0.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadAPicker0.Location = new System.Drawing.Point(2, 115);
            this.btnCellLoadAPicker0.Name = "btnCellLoadAPicker0";
            this.btnCellLoadAPicker0.Size = new System.Drawing.Size(72, 38);
            this.btnCellLoadAPicker0.TabIndex = 62;
            this.btnCellLoadAPicker0.Text = "A 피커\r\n0도";
            this.btnCellLoadAPicker0.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadACellULD
            // 
            this.btnCellLoadACellULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadACellULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadACellULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadACellULD.Location = new System.Drawing.Point(121, 71);
            this.btnCellLoadACellULD.Name = "btnCellLoadACellULD";
            this.btnCellLoadACellULD.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadACellULD.TabIndex = 61;
            this.btnCellLoadACellULD.Text = "A ULD";
            this.btnCellLoadACellULD.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadACellLD
            // 
            this.btnCellLoadACellLD.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellLoadACellLD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadACellLD.ForeColor = System.Drawing.Color.Black;
            this.btnCellLoadACellLD.Location = new System.Drawing.Point(3, 71);
            this.btnCellLoadACellLD.Name = "btnCellLoadACellLD";
            this.btnCellLoadACellLD.Size = new System.Drawing.Size(109, 38);
            this.btnCellLoadACellLD.TabIndex = 60;
            this.btnCellLoadACellLD.Text = "A LD";
            this.btnCellLoadACellLD.UseVisualStyleBackColor = false;
            // 
            // label30
            // 
            this.label30.AutoEllipsis = true;
            this.label30.BackColor = System.Drawing.Color.Gainsboro;
            this.label30.Dock = System.Windows.Forms.DockStyle.Top;
            this.label30.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(234, 24);
            this.label30.TabIndex = 9;
            this.label30.Text = "■ A열";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoEllipsis = true;
            this.label21.BackColor = System.Drawing.Color.Gainsboro;
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(954, 24);
            this.label21.TabIndex = 9;
            this.label21.Text = "■ 이동";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.panel48);
            this.tabPage4.Controls.Add(this.panel33);
            this.tabPage4.Controls.Add(this.panel31);
            this.tabPage4.Controls.Add(this.panel32);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(969, 397);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "IR Cut 프로세스";
            // 
            // panel48
            // 
            this.panel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel48.Controls.Add(this.btnIRCut2BrushDown);
            this.panel48.Controls.Add(this.label48);
            this.panel48.Controls.Add(this.btnIRCut1BrushDown);
            this.panel48.Controls.Add(this.btnIRCut2BrushUp);
            this.panel48.Controls.Add(this.btnIRCut1BrushUp);
            this.panel48.Location = new System.Drawing.Point(769, 7);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(192, 178);
            this.panel48.TabIndex = 475;
            // 
            // btnIRCut2BrushDown
            // 
            this.btnIRCut2BrushDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCut2BrushDown.ForeColor = System.Drawing.Color.Black;
            this.btnIRCut2BrushDown.Location = new System.Drawing.Point(97, 99);
            this.btnIRCut2BrushDown.Name = "btnIRCut2BrushDown";
            this.btnIRCut2BrushDown.Size = new System.Drawing.Size(85, 66);
            this.btnIRCut2BrushDown.TabIndex = 459;
            this.btnIRCut2BrushDown.Text = "2 가공 Brush 실린더 다운";
            this.btnIRCut2BrushDown.UseVisualStyleBackColor = false;
            // 
            // label48
            // 
            this.label48.AutoEllipsis = true;
            this.label48.BackColor = System.Drawing.Color.Gainsboro;
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(0, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(190, 24);
            this.label48.TabIndex = 9;
            this.label48.Text = "■ IR CUT 브러쉬";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnIRCut1BrushDown
            // 
            this.btnIRCut1BrushDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCut1BrushDown.ForeColor = System.Drawing.Color.Black;
            this.btnIRCut1BrushDown.Location = new System.Drawing.Point(96, 30);
            this.btnIRCut1BrushDown.Name = "btnIRCut1BrushDown";
            this.btnIRCut1BrushDown.Size = new System.Drawing.Size(85, 66);
            this.btnIRCut1BrushDown.TabIndex = 458;
            this.btnIRCut1BrushDown.Text = "1 가공 Brush 실린더 다운";
            this.btnIRCut1BrushDown.UseVisualStyleBackColor = false;
            // 
            // btnIRCut2BrushUp
            // 
            this.btnIRCut2BrushUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCut2BrushUp.ForeColor = System.Drawing.Color.Black;
            this.btnIRCut2BrushUp.Location = new System.Drawing.Point(7, 99);
            this.btnIRCut2BrushUp.Name = "btnIRCut2BrushUp";
            this.btnIRCut2BrushUp.Size = new System.Drawing.Size(85, 66);
            this.btnIRCut2BrushUp.TabIndex = 457;
            this.btnIRCut2BrushUp.Text = "2 가공 Brush 실린더 업";
            this.btnIRCut2BrushUp.UseVisualStyleBackColor = false;
            // 
            // btnIRCut1BrushUp
            // 
            this.btnIRCut1BrushUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCut1BrushUp.ForeColor = System.Drawing.Color.Black;
            this.btnIRCut1BrushUp.Location = new System.Drawing.Point(7, 30);
            this.btnIRCut1BrushUp.Name = "btnIRCut1BrushUp";
            this.btnIRCut1BrushUp.Size = new System.Drawing.Size(85, 66);
            this.btnIRCut1BrushUp.TabIndex = 456;
            this.btnIRCut1BrushUp.Text = "1 가공 Brush 실린더 업";
            this.btnIRCut1BrushUp.UseVisualStyleBackColor = false;
            // 
            // panel33
            // 
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Controls.Add(this.panel83);
            this.panel33.Controls.Add(this.panel36);
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Controls.Add(this.panel37);
            this.panel33.Controls.Add(this.panel35);
            this.panel33.Controls.Add(this.label36);
            this.panel33.Location = new System.Drawing.Point(7, 190);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(956, 201);
            this.panel33.TabIndex = 469;
            // 
            // panel83
            // 
            this.panel83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel83.Controls.Add(this.btnIRCutProcessZPowerMeter);
            this.panel83.Controls.Add(this.label83);
            this.panel83.Controls.Add(this.btnIRCutProcessZLaserShot);
            this.panel83.Location = new System.Drawing.Point(595, 32);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(151, 160);
            this.panel83.TabIndex = 462;
            // 
            // btnIRCutProcessZPowerMeter
            // 
            this.btnIRCutProcessZPowerMeter.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessZPowerMeter.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessZPowerMeter.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessZPowerMeter.Location = new System.Drawing.Point(12, 94);
            this.btnIRCutProcessZPowerMeter.Name = "btnIRCutProcessZPowerMeter";
            this.btnIRCutProcessZPowerMeter.Size = new System.Drawing.Size(124, 58);
            this.btnIRCutProcessZPowerMeter.TabIndex = 67;
            this.btnIRCutProcessZPowerMeter.Text = "파워미터 측정 위치";
            this.btnIRCutProcessZPowerMeter.UseVisualStyleBackColor = false;
            // 
            // label83
            // 
            this.label83.AutoEllipsis = true;
            this.label83.BackColor = System.Drawing.Color.Gainsboro;
            this.label83.Dock = System.Windows.Forms.DockStyle.Top;
            this.label83.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(0, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(149, 24);
            this.label83.TabIndex = 9;
            this.label83.Text = "■ Laser Head Z 축";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnIRCutProcessZLaserShot
            // 
            this.btnIRCutProcessZLaserShot.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessZLaserShot.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessZLaserShot.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessZLaserShot.Location = new System.Drawing.Point(12, 31);
            this.btnIRCutProcessZLaserShot.Name = "btnIRCutProcessZLaserShot";
            this.btnIRCutProcessZLaserShot.Size = new System.Drawing.Size(124, 57);
            this.btnIRCutProcessZLaserShot.TabIndex = 63;
            this.btnIRCutProcessZLaserShot.Text = "레이저 샷 위치";
            this.btnIRCutProcessZLaserShot.UseVisualStyleBackColor = false;
            // 
            // panel36
            // 
            this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel36.Controls.Add(this.btnIRCutProcessPowerMeterAutoMeasure);
            this.panel36.Controls.Add(this.btnIRCutProcessB1LaserShot);
            this.panel36.Controls.Add(this.label35);
            this.panel36.Controls.Add(this.btnIRCutProcessB2LaserShot);
            this.panel36.Controls.Add(this.btnIRCutProcessA2LaserShot);
            this.panel36.Controls.Add(this.btnIRCutProcessA1LaserShot);
            this.panel36.Location = new System.Drawing.Point(365, 32);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(223, 160);
            this.panel36.TabIndex = 461;
            // 
            // btnIRCutProcessPowerMeterAutoMeasure
            // 
            this.btnIRCutProcessPowerMeterAutoMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnIRCutProcessPowerMeterAutoMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessPowerMeterAutoMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnIRCutProcessPowerMeterAutoMeasure.Location = new System.Drawing.Point(12, 121);
            this.btnIRCutProcessPowerMeterAutoMeasure.Name = "btnIRCutProcessPowerMeterAutoMeasure";
            this.btnIRCutProcessPowerMeterAutoMeasure.Size = new System.Drawing.Size(196, 31);
            this.btnIRCutProcessPowerMeterAutoMeasure.TabIndex = 67;
            this.btnIRCutProcessPowerMeterAutoMeasure.Text = "파워미터 측정 위치";
            this.btnIRCutProcessPowerMeterAutoMeasure.UseVisualStyleBackColor = false;
            // 
            // label35
            // 
            this.label35.AutoEllipsis = true;
            this.label35.BackColor = System.Drawing.Color.Gainsboro;
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(221, 24);
            this.label35.TabIndex = 9;
            this.label35.Text = "■ Laser Head X 축";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.btnIRCutProcessBCamera);
            this.panel34.Controls.Add(this.label33);
            this.panel34.Controls.Add(this.btnIRCutProcessBCellUnload);
            this.panel34.Controls.Add(this.btnIRCutProcessBCellLoad);
            this.panel34.Controls.Add(this.btnIRCutProcessBLaser);
            this.panel34.Location = new System.Drawing.Point(187, 33);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(173, 160);
            this.panel34.TabIndex = 459;
            // 
            // label33
            // 
            this.label33.AutoEllipsis = true;
            this.label33.BackColor = System.Drawing.Color.Gainsboro;
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(171, 24);
            this.label33.TabIndex = 9;
            this.label33.Text = "■ B열";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel37
            // 
            this.panel37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel37.Controls.Add(this.label37);
            this.panel37.Controls.Add(this.btnIRCutProcessBFineAlign);
            this.panel37.Controls.Add(this.btnIRCutProcessAFineAlign);
            this.panel37.Location = new System.Drawing.Point(754, 32);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(190, 160);
            this.panel37.TabIndex = 460;
            // 
            // label37
            // 
            this.label37.AutoEllipsis = true;
            this.label37.BackColor = System.Drawing.Color.Gainsboro;
            this.label37.Dock = System.Windows.Forms.DockStyle.Top;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(0, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(188, 24);
            this.label37.TabIndex = 9;
            this.label37.Text = "■ Fine Align";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel35
            // 
            this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel35.Controls.Add(this.btnIRCutProcessACamera);
            this.panel35.Controls.Add(this.label34);
            this.panel35.Controls.Add(this.btnIRCutProcessACellLoad);
            this.panel35.Controls.Add(this.btnIRCutProcessACellUnload);
            this.panel35.Controls.Add(this.btnIRCutProcessALaser);
            this.panel35.Location = new System.Drawing.Point(8, 32);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(173, 160);
            this.panel35.TabIndex = 458;
            // 
            // label34
            // 
            this.label34.AutoEllipsis = true;
            this.label34.BackColor = System.Drawing.Color.Gainsboro;
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(171, 24);
            this.label34.TabIndex = 9;
            this.label34.Text = "■ A열";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.AutoEllipsis = true;
            this.label36.BackColor = System.Drawing.Color.Gainsboro;
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(954, 24);
            this.label36.TabIndex = 9;
            this.label36.Text = "■ 이동";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel31
            // 
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Controls.Add(this.label31);
            this.panel31.Controls.Add(this.btnIRCutProcessA1VaccumCh2Off);
            this.panel31.Controls.Add(this.btnIRCutProcessA2VaccumCh1On);
            this.panel31.Controls.Add(this.btnIRCutProcessA1BlowerCh2OnOff);
            this.panel31.Controls.Add(this.btnIRCutProcessA2BlowerCh1OnOff);
            this.panel31.Controls.Add(this.btnIRCutProcessA2VaccumCh2Off);
            this.panel31.Controls.Add(this.btnIRCutProcessA1BlowerCh1OnOff);
            this.panel31.Controls.Add(this.btnIRCutProcessA2VaccumCh2On);
            this.panel31.Controls.Add(this.btnIRCutProcessA1VaccumCh1Off);
            this.panel31.Controls.Add(this.btnIRCutProcessA2VaccumCh1Off);
            this.panel31.Controls.Add(this.btnIRCutProcessA1VaccumCh2On);
            this.panel31.Controls.Add(this.btnIRCutProcessA1VaccumCh1On);
            this.panel31.Controls.Add(this.btnIRCutProcessA2BlowerCh2OnOff);
            this.panel31.Location = new System.Drawing.Point(7, 7);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(368, 178);
            this.panel31.TabIndex = 468;
            // 
            // label31
            // 
            this.label31.AutoEllipsis = true;
            this.label31.BackColor = System.Drawing.Color.Gainsboro;
            this.label31.Dock = System.Windows.Forms.DockStyle.Top;
            this.label31.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(0, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(366, 24);
            this.label31.TabIndex = 9;
            this.label31.Text = "■ A열";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.btnIRCutProcessB1VaccumCh1Off);
            this.panel32.Controls.Add(this.label32);
            this.panel32.Controls.Add(this.btnIRCutProcessB2VaccumCh1On);
            this.panel32.Controls.Add(this.btnIRCutProcessB2VaccumCh2Off);
            this.panel32.Controls.Add(this.btnIRCutProcessB2VaccumCh1Off);
            this.panel32.Controls.Add(this.btnIRCutProcessB2BlowerCh2OnOff);
            this.panel32.Controls.Add(this.btnIRCutProcessB1VaccumCh2Off);
            this.panel32.Controls.Add(this.btnIRCutProcessB2VaccumCh2On);
            this.panel32.Controls.Add(this.btnIRCutProcessB1BlowerCh2OnOff);
            this.panel32.Controls.Add(this.btnIRCutProcessB1VaccumCh2On);
            this.panel32.Controls.Add(this.btnIRCutProcessB1VaccumCh1On);
            this.panel32.Controls.Add(this.btnIRCutProcessB2BlowerCh1OnOff);
            this.panel32.Controls.Add(this.btnIRCutProcessB1BlowerCh1OnOff);
            this.panel32.Location = new System.Drawing.Point(391, 7);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(368, 178);
            this.panel32.TabIndex = 467;
            // 
            // label32
            // 
            this.label32.AutoEllipsis = true;
            this.label32.BackColor = System.Drawing.Color.Gainsboro;
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(366, 24);
            this.label32.TabIndex = 9;
            this.label32.Text = "■ B열";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage5.Controls.Add(this.panel40);
            this.tabPage5.Controls.Add(this.panel38);
            this.tabPage5.Controls.Add(this.panel39);
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(969, 397);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Break 트랜스퍼";
            // 
            // panel40
            // 
            this.panel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel40.Controls.Add(this.panel41);
            this.panel40.Controls.Add(this.panel43);
            this.panel40.Controls.Add(this.label43);
            this.panel40.Location = new System.Drawing.Point(7, 188);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(956, 201);
            this.panel40.TabIndex = 471;
            // 
            // panel41
            // 
            this.panel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel41.Controls.Add(this.btnBreakTRBULD);
            this.panel41.Controls.Add(this.btnBreakTRBLD);
            this.panel41.Controls.Add(this.label40);
            this.panel41.Location = new System.Drawing.Point(301, 33);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(260, 160);
            this.panel41.TabIndex = 459;
            // 
            // label40
            // 
            this.label40.AutoEllipsis = true;
            this.label40.BackColor = System.Drawing.Color.Gainsboro;
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(0, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(258, 24);
            this.label40.TabIndex = 9;
            this.label40.Text = "■ B열";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel43
            // 
            this.panel43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel43.Controls.Add(this.btnBreakTRAULD);
            this.panel43.Controls.Add(this.label42);
            this.panel43.Controls.Add(this.btnBreakTRALD);
            this.panel43.Location = new System.Drawing.Point(16, 33);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(260, 160);
            this.panel43.TabIndex = 457;
            // 
            // label42
            // 
            this.label42.AutoEllipsis = true;
            this.label42.BackColor = System.Drawing.Color.Gainsboro;
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(0, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(258, 24);
            this.label42.TabIndex = 9;
            this.label42.Text = "■ A열";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.AutoEllipsis = true;
            this.label43.BackColor = System.Drawing.Color.Gainsboro;
            this.label43.Dock = System.Windows.Forms.DockStyle.Top;
            this.label43.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(0, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(954, 24);
            this.label43.TabIndex = 9;
            this.label43.Text = "■ 이동";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel38
            // 
            this.panel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel38.Controls.Add(this.btnBreakTransferA2PickerBlowerOnOff);
            this.panel38.Controls.Add(this.btnBreakTransferA2PickerUp);
            this.panel38.Controls.Add(this.btnBreakTransferA2PickerVaccumOff);
            this.panel38.Controls.Add(this.btnBreakTransferA2PickerDown);
            this.panel38.Controls.Add(this.btnBreakTransferA2PickerVaccumOn);
            this.panel38.Controls.Add(this.btnBreakTransferA1PickerBlowerOnOff);
            this.panel38.Controls.Add(this.label38);
            this.panel38.Controls.Add(this.btnBreakTransferA1PickerUp);
            this.panel38.Controls.Add(this.btnBreakTransferA1PickerVaccumOff);
            this.panel38.Controls.Add(this.btnBreakTransferA1PickerDown);
            this.panel38.Controls.Add(this.btnBreakTransferA1PickerVaccumOn);
            this.panel38.Location = new System.Drawing.Point(7, 7);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(465, 175);
            this.panel38.TabIndex = 470;
            // 
            // btnBreakTransferA2PickerBlowerOnOff
            // 
            this.btnBreakTransferA2PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA2PickerBlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA2PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA2PickerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnBreakTransferA2PickerBlowerOnOff.Name = "btnBreakTransferA2PickerBlowerOnOff";
            this.btnBreakTransferA2PickerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnBreakTransferA2PickerBlowerOnOff.TabIndex = 71;
            this.btnBreakTransferA2PickerBlowerOnOff.Text = "A2\r\n피커 파기 온/오프";
            this.btnBreakTransferA2PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA2PickerUp
            // 
            this.btnBreakTransferA2PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA2PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA2PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA2PickerUp.Location = new System.Drawing.Point(234, 29);
            this.btnBreakTransferA2PickerUp.Name = "btnBreakTransferA2PickerUp";
            this.btnBreakTransferA2PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA2PickerUp.TabIndex = 66;
            this.btnBreakTransferA2PickerUp.Text = "A2\r\n피커 업";
            this.btnBreakTransferA2PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA2PickerVaccumOff
            // 
            this.btnBreakTransferA2PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA2PickerVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA2PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA2PickerVaccumOff.Location = new System.Drawing.Point(348, 77);
            this.btnBreakTransferA2PickerVaccumOff.Name = "btnBreakTransferA2PickerVaccumOff";
            this.btnBreakTransferA2PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA2PickerVaccumOff.TabIndex = 69;
            this.btnBreakTransferA2PickerVaccumOff.Text = "A2\r\n피커 공압 오프";
            this.btnBreakTransferA2PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA2PickerDown
            // 
            this.btnBreakTransferA2PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA2PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA2PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA2PickerDown.Location = new System.Drawing.Point(348, 29);
            this.btnBreakTransferA2PickerDown.Name = "btnBreakTransferA2PickerDown";
            this.btnBreakTransferA2PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA2PickerDown.TabIndex = 67;
            this.btnBreakTransferA2PickerDown.Text = "A2\r\n피커 다운";
            this.btnBreakTransferA2PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferA2PickerVaccumOn
            // 
            this.btnBreakTransferA2PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferA2PickerVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferA2PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferA2PickerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnBreakTransferA2PickerVaccumOn.Name = "btnBreakTransferA2PickerVaccumOn";
            this.btnBreakTransferA2PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferA2PickerVaccumOn.TabIndex = 68;
            this.btnBreakTransferA2PickerVaccumOn.Text = "A2\r\n피커 공압 온";
            this.btnBreakTransferA2PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // label38
            // 
            this.label38.AutoEllipsis = true;
            this.label38.BackColor = System.Drawing.Color.Gainsboro;
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(463, 24);
            this.label38.TabIndex = 9;
            this.label38.Text = "■ A열";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel39
            // 
            this.panel39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel39.Controls.Add(this.btnBreakTransferB2PickerBlowerOnOff);
            this.panel39.Controls.Add(this.btnBreakTransferB2PickerUp);
            this.panel39.Controls.Add(this.btnBreakTransferB2PickerVaccumOff);
            this.panel39.Controls.Add(this.btnBreakTransferB2PickerDown);
            this.panel39.Controls.Add(this.btnBreakTransferB2PickerVaccumOn);
            this.panel39.Controls.Add(this.btnBreakTransferB1PickerBlowerOnOff);
            this.panel39.Controls.Add(this.label39);
            this.panel39.Controls.Add(this.btnBreakTransferB1PickerUp);
            this.panel39.Controls.Add(this.btnBreakTransferB1PickerVaccumOff);
            this.panel39.Controls.Add(this.btnBreakTransferB1PickerDown);
            this.panel39.Controls.Add(this.btnBreakTransferB1PickerVaccumOn);
            this.panel39.Location = new System.Drawing.Point(481, 7);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(465, 175);
            this.panel39.TabIndex = 469;
            // 
            // btnBreakTransferB2PickerBlowerOnOff
            // 
            this.btnBreakTransferB2PickerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB2PickerBlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB2PickerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB2PickerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnBreakTransferB2PickerBlowerOnOff.Name = "btnBreakTransferB2PickerBlowerOnOff";
            this.btnBreakTransferB2PickerBlowerOnOff.Size = new System.Drawing.Size(223, 42);
            this.btnBreakTransferB2PickerBlowerOnOff.TabIndex = 71;
            this.btnBreakTransferB2PickerBlowerOnOff.Text = "B2\r\n피커 파기 온/오프";
            this.btnBreakTransferB2PickerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB2PickerUp
            // 
            this.btnBreakTransferB2PickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB2PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB2PickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB2PickerUp.Location = new System.Drawing.Point(234, 29);
            this.btnBreakTransferB2PickerUp.Name = "btnBreakTransferB2PickerUp";
            this.btnBreakTransferB2PickerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB2PickerUp.TabIndex = 66;
            this.btnBreakTransferB2PickerUp.Text = "B2\r\n피커 업";
            this.btnBreakTransferB2PickerUp.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB2PickerVaccumOff
            // 
            this.btnBreakTransferB2PickerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB2PickerVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB2PickerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB2PickerVaccumOff.Location = new System.Drawing.Point(348, 77);
            this.btnBreakTransferB2PickerVaccumOff.Name = "btnBreakTransferB2PickerVaccumOff";
            this.btnBreakTransferB2PickerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB2PickerVaccumOff.TabIndex = 69;
            this.btnBreakTransferB2PickerVaccumOff.Text = "B2\r\n피커 공압 오프";
            this.btnBreakTransferB2PickerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB2PickerDown
            // 
            this.btnBreakTransferB2PickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB2PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB2PickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB2PickerDown.Location = new System.Drawing.Point(348, 29);
            this.btnBreakTransferB2PickerDown.Name = "btnBreakTransferB2PickerDown";
            this.btnBreakTransferB2PickerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB2PickerDown.TabIndex = 67;
            this.btnBreakTransferB2PickerDown.Text = "B2\r\n피커 다운";
            this.btnBreakTransferB2PickerDown.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferB2PickerVaccumOn
            // 
            this.btnBreakTransferB2PickerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakTransferB2PickerVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferB2PickerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBreakTransferB2PickerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnBreakTransferB2PickerVaccumOn.Name = "btnBreakTransferB2PickerVaccumOn";
            this.btnBreakTransferB2PickerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBreakTransferB2PickerVaccumOn.TabIndex = 68;
            this.btnBreakTransferB2PickerVaccumOn.Text = "B2\r\n피커 공압 온";
            this.btnBreakTransferB2PickerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // label39
            // 
            this.label39.AutoEllipsis = true;
            this.label39.BackColor = System.Drawing.Color.Gainsboro;
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(0, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(463, 24);
            this.label39.TabIndex = 9;
            this.label39.Text = "■ B열";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage6.Controls.Add(this.panel71);
            this.tabPage6.Controls.Add(this.panel42);
            this.tabPage6.Location = new System.Drawing.Point(4, 34);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(969, 397);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Break 유닛(X축 Z축)";
            // 
            // panel71
            // 
            this.panel71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel71.Controls.Add(this.btnBreakBBrushDown);
            this.panel71.Controls.Add(this.label71);
            this.panel71.Controls.Add(this.btnBreakABrushDown);
            this.panel71.Controls.Add(this.btnBreakBBrushUp);
            this.panel71.Controls.Add(this.btnBreakABrushUp);
            this.panel71.Location = new System.Drawing.Point(7, 7);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(205, 174);
            this.panel71.TabIndex = 476;
            // 
            // btnBreakBBrushDown
            // 
            this.btnBreakBBrushDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBBrushDown.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBBrushDown.Location = new System.Drawing.Point(104, 99);
            this.btnBreakBBrushDown.Name = "btnBreakBBrushDown";
            this.btnBreakBBrushDown.Size = new System.Drawing.Size(90, 66);
            this.btnBreakBBrushDown.TabIndex = 459;
            this.btnBreakBBrushDown.Text = "2 Break Brush 실린더 다운";
            this.btnBreakBBrushDown.UseVisualStyleBackColor = false;
            // 
            // label71
            // 
            this.label71.AutoEllipsis = true;
            this.label71.BackColor = System.Drawing.Color.Gainsboro;
            this.label71.Dock = System.Windows.Forms.DockStyle.Top;
            this.label71.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(0, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(203, 24);
            this.label71.TabIndex = 9;
            this.label71.Text = "■ Break 브러쉬";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakABrushDown
            // 
            this.btnBreakABrushDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakABrushDown.ForeColor = System.Drawing.Color.Black;
            this.btnBreakABrushDown.Location = new System.Drawing.Point(104, 30);
            this.btnBreakABrushDown.Name = "btnBreakABrushDown";
            this.btnBreakABrushDown.Size = new System.Drawing.Size(90, 66);
            this.btnBreakABrushDown.TabIndex = 458;
            this.btnBreakABrushDown.Text = "1 Break Brush 실린더 다운";
            this.btnBreakABrushDown.UseVisualStyleBackColor = false;
            // 
            // btnBreakBBrushUp
            // 
            this.btnBreakBBrushUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakBBrushUp.ForeColor = System.Drawing.Color.Black;
            this.btnBreakBBrushUp.Location = new System.Drawing.Point(7, 99);
            this.btnBreakBBrushUp.Name = "btnBreakBBrushUp";
            this.btnBreakBBrushUp.Size = new System.Drawing.Size(90, 66);
            this.btnBreakBBrushUp.TabIndex = 457;
            this.btnBreakBBrushUp.Text = "2 Break Brush 실린더 업";
            this.btnBreakBBrushUp.UseVisualStyleBackColor = false;
            // 
            // btnBreakABrushUp
            // 
            this.btnBreakABrushUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakABrushUp.ForeColor = System.Drawing.Color.Black;
            this.btnBreakABrushUp.Location = new System.Drawing.Point(7, 30);
            this.btnBreakABrushUp.Name = "btnBreakABrushUp";
            this.btnBreakABrushUp.Size = new System.Drawing.Size(90, 66);
            this.btnBreakABrushUp.TabIndex = 456;
            this.btnBreakABrushUp.Text = "1 Break Brush 실린더 업";
            this.btnBreakABrushUp.UseVisualStyleBackColor = false;
            // 
            // panel42
            // 
            this.panel42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel42.Controls.Add(this.panel44);
            this.panel42.Controls.Add(this.panel47);
            this.panel42.Controls.Add(this.label45);
            this.panel42.Location = new System.Drawing.Point(7, 188);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(956, 201);
            this.panel42.TabIndex = 473;
            // 
            // panel44
            // 
            this.panel44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel44.Controls.Add(this.label41);
            this.panel44.Controls.Add(this.btnBreakZ1Hold);
            this.panel44.Controls.Add(this.btnBreakZ1BreakingPos);
            this.panel44.Location = new System.Drawing.Point(561, 32);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(153, 160);
            this.panel44.TabIndex = 465;
            // 
            // label41
            // 
            this.label41.AutoEllipsis = true;
            this.label41.BackColor = System.Drawing.Color.Gainsboro;
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(0, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(151, 24);
            this.label41.TabIndex = 9;
            this.label41.Text = "■ 중간, 언로딩";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel47
            // 
            this.panel47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel47.Controls.Add(this.btnBreakX1BreakAlign);
            this.panel47.Controls.Add(this.btnBreakX1JigAlign);
            this.panel47.Controls.Add(this.btnBreakXZB2LDS);
            this.panel47.Controls.Add(this.btnBreakXZB1LDS);
            this.panel47.Controls.Add(this.btnBreakXZA2LDS);
            this.panel47.Controls.Add(this.btnBreakXZA1LDS);
            this.panel47.Controls.Add(this.label47);
            this.panel47.Controls.Add(this.btnBreakXZAAlignMeasure);
            this.panel47.Controls.Add(this.btnBreakXZBAlignMeasure);
            this.panel47.Location = new System.Drawing.Point(7, 32);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(548, 160);
            this.panel47.TabIndex = 462;
            // 
            // btnBreakX1BreakAlign
            // 
            this.btnBreakX1BreakAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakX1BreakAlign.ForeColor = System.Drawing.Color.Black;
            this.btnBreakX1BreakAlign.Location = new System.Drawing.Point(404, 94);
            this.btnBreakX1BreakAlign.Name = "btnBreakX1BreakAlign";
            this.btnBreakX1BreakAlign.Size = new System.Drawing.Size(125, 55);
            this.btnBreakX1BreakAlign.TabIndex = 72;
            this.btnBreakX1BreakAlign.Text = "평탄도 Z축";
            this.btnBreakX1BreakAlign.UseVisualStyleBackColor = false;
            // 
            // btnBreakX1JigAlign
            // 
            this.btnBreakX1JigAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakX1JigAlign.ForeColor = System.Drawing.Color.Black;
            this.btnBreakX1JigAlign.Location = new System.Drawing.Point(404, 35);
            this.btnBreakX1JigAlign.Name = "btnBreakX1JigAlign";
            this.btnBreakX1JigAlign.Size = new System.Drawing.Size(125, 55);
            this.btnBreakX1JigAlign.TabIndex = 71;
            this.btnBreakX1JigAlign.Text = "Jig 측정 위치";
            this.btnBreakX1JigAlign.UseVisualStyleBackColor = false;
            // 
            // btnBreakXZB2LDS
            // 
            this.btnBreakXZB2LDS.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakXZB2LDS.ForeColor = System.Drawing.Color.Black;
            this.btnBreakXZB2LDS.Location = new System.Drawing.Point(273, 94);
            this.btnBreakXZB2LDS.Name = "btnBreakXZB2LDS";
            this.btnBreakXZB2LDS.Size = new System.Drawing.Size(125, 55);
            this.btnBreakXZB2LDS.TabIndex = 70;
            this.btnBreakXZB2LDS.Text = "B-2 Breaking Table 평탄도 위치";
            this.btnBreakXZB2LDS.UseVisualStyleBackColor = false;
            // 
            // btnBreakXZB1LDS
            // 
            this.btnBreakXZB1LDS.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakXZB1LDS.ForeColor = System.Drawing.Color.Black;
            this.btnBreakXZB1LDS.Location = new System.Drawing.Point(142, 94);
            this.btnBreakXZB1LDS.Name = "btnBreakXZB1LDS";
            this.btnBreakXZB1LDS.Size = new System.Drawing.Size(125, 55);
            this.btnBreakXZB1LDS.TabIndex = 69;
            this.btnBreakXZB1LDS.Text = "B-1 Breaking Table 평탄도 위치";
            this.btnBreakXZB1LDS.UseVisualStyleBackColor = false;
            // 
            // btnBreakXZA2LDS
            // 
            this.btnBreakXZA2LDS.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakXZA2LDS.ForeColor = System.Drawing.Color.Black;
            this.btnBreakXZA2LDS.Location = new System.Drawing.Point(273, 34);
            this.btnBreakXZA2LDS.Name = "btnBreakXZA2LDS";
            this.btnBreakXZA2LDS.Size = new System.Drawing.Size(125, 55);
            this.btnBreakXZA2LDS.TabIndex = 68;
            this.btnBreakXZA2LDS.Text = "A-2 Breaking Table 평탄도 위치";
            this.btnBreakXZA2LDS.UseVisualStyleBackColor = false;
            // 
            // btnBreakXZA1LDS
            // 
            this.btnBreakXZA1LDS.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakXZA1LDS.ForeColor = System.Drawing.Color.Black;
            this.btnBreakXZA1LDS.Location = new System.Drawing.Point(142, 34);
            this.btnBreakXZA1LDS.Name = "btnBreakXZA1LDS";
            this.btnBreakXZA1LDS.Size = new System.Drawing.Size(125, 55);
            this.btnBreakXZA1LDS.TabIndex = 67;
            this.btnBreakXZA1LDS.Text = "A-1 Breaking Table 평탄도 위치";
            this.btnBreakXZA1LDS.UseVisualStyleBackColor = false;
            // 
            // label47
            // 
            this.label47.AutoEllipsis = true;
            this.label47.BackColor = System.Drawing.Color.Gainsboro;
            this.label47.Dock = System.Windows.Forms.DockStyle.Top;
            this.label47.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(546, 24);
            this.label47.TabIndex = 9;
            this.label47.Text = "■ 중간, 언로딩";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            this.label45.AutoEllipsis = true;
            this.label45.BackColor = System.Drawing.Color.Gainsboro;
            this.label45.Dock = System.Windows.Forms.DockStyle.Top;
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(0, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(954, 24);
            this.label45.TabIndex = 9;
            this.label45.Text = "■ 이동";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage7.Controls.Add(this.panel54);
            this.tabPage7.Controls.Add(this.panel50);
            this.tabPage7.Controls.Add(this.panel52);
            this.tabPage7.Controls.Add(this.panel51);
            this.tabPage7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage7.Location = new System.Drawing.Point(4, 34);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(969, 397);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Break 유닛(T축 Y축)";
            // 
            // panel54
            // 
            this.panel54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel54.Controls.Add(this.panel57);
            this.panel54.Controls.Add(this.panel55);
            this.panel54.Controls.Add(this.panel56);
            this.panel54.Controls.Add(this.label56);
            this.panel54.Location = new System.Drawing.Point(7, 188);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(956, 201);
            this.panel54.TabIndex = 472;
            // 
            // panel57
            // 
            this.panel57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel57.Controls.Add(this.btnBreakUnitTYMeasure4);
            this.panel57.Controls.Add(this.btnBreakUnitTYHold4);
            this.panel57.Controls.Add(this.btnBreakUnitTYMeasure3);
            this.panel57.Controls.Add(this.btnBreakUnitTYMeasure2);
            this.panel57.Controls.Add(this.btnBreakUnitTYHold3);
            this.panel57.Controls.Add(this.btnBreakUnitTYHold2);
            this.panel57.Controls.Add(this.btnBreakUnitTYMeasure1);
            this.panel57.Controls.Add(this.btnBreakUnitTYHold1);
            this.panel57.Controls.Add(this.label57);
            this.panel57.Location = new System.Drawing.Point(595, 32);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(352, 160);
            this.panel57.TabIndex = 460;
            // 
            // btnBreakUnitTYMeasure4
            // 
            this.btnBreakUnitTYMeasure4.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYMeasure4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMeasure4.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYMeasure4.Location = new System.Drawing.Point(264, 98);
            this.btnBreakUnitTYMeasure4.Name = "btnBreakUnitTYMeasure4";
            this.btnBreakUnitTYMeasure4.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYMeasure4.TabIndex = 66;
            this.btnBreakUnitTYMeasure4.Text = "T4 정밀";
            this.btnBreakUnitTYMeasure4.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYHold4
            // 
            this.btnBreakUnitTYHold4.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYHold4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYHold4.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYHold4.Location = new System.Drawing.Point(179, 98);
            this.btnBreakUnitTYHold4.Name = "btnBreakUnitTYHold4";
            this.btnBreakUnitTYHold4.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYHold4.TabIndex = 65;
            this.btnBreakUnitTYHold4.Text = "T4 대기";
            this.btnBreakUnitTYHold4.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYMeasure3
            // 
            this.btnBreakUnitTYMeasure3.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYMeasure3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMeasure3.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYMeasure3.Location = new System.Drawing.Point(94, 98);
            this.btnBreakUnitTYMeasure3.Name = "btnBreakUnitTYMeasure3";
            this.btnBreakUnitTYMeasure3.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYMeasure3.TabIndex = 64;
            this.btnBreakUnitTYMeasure3.Text = "T3 정밀";
            this.btnBreakUnitTYMeasure3.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYMeasure2
            // 
            this.btnBreakUnitTYMeasure2.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYMeasure2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMeasure2.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYMeasure2.Location = new System.Drawing.Point(264, 35);
            this.btnBreakUnitTYMeasure2.Name = "btnBreakUnitTYMeasure2";
            this.btnBreakUnitTYMeasure2.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYMeasure2.TabIndex = 62;
            this.btnBreakUnitTYMeasure2.Text = "T2 정밀";
            this.btnBreakUnitTYMeasure2.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYHold3
            // 
            this.btnBreakUnitTYHold3.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYHold3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYHold3.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYHold3.Location = new System.Drawing.Point(9, 98);
            this.btnBreakUnitTYHold3.Name = "btnBreakUnitTYHold3";
            this.btnBreakUnitTYHold3.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYHold3.TabIndex = 63;
            this.btnBreakUnitTYHold3.Text = "T3 대기";
            this.btnBreakUnitTYHold3.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYHold2
            // 
            this.btnBreakUnitTYHold2.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYHold2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYHold2.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYHold2.Location = new System.Drawing.Point(179, 35);
            this.btnBreakUnitTYHold2.Name = "btnBreakUnitTYHold2";
            this.btnBreakUnitTYHold2.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYHold2.TabIndex = 61;
            this.btnBreakUnitTYHold2.Text = "T2 대기";
            this.btnBreakUnitTYHold2.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYMeasure1
            // 
            this.btnBreakUnitTYMeasure1.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYMeasure1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMeasure1.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYMeasure1.Location = new System.Drawing.Point(94, 35);
            this.btnBreakUnitTYMeasure1.Name = "btnBreakUnitTYMeasure1";
            this.btnBreakUnitTYMeasure1.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYMeasure1.TabIndex = 60;
            this.btnBreakUnitTYMeasure1.Text = "T1 정밀";
            this.btnBreakUnitTYMeasure1.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYHold1
            // 
            this.btnBreakUnitTYHold1.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYHold1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYHold1.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYHold1.Location = new System.Drawing.Point(9, 35);
            this.btnBreakUnitTYHold1.Name = "btnBreakUnitTYHold1";
            this.btnBreakUnitTYHold1.Size = new System.Drawing.Size(75, 50);
            this.btnBreakUnitTYHold1.TabIndex = 59;
            this.btnBreakUnitTYHold1.Text = "T1 대기";
            this.btnBreakUnitTYHold1.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.AutoEllipsis = true;
            this.label57.BackColor = System.Drawing.Color.Gainsboro;
            this.label57.Dock = System.Windows.Forms.DockStyle.Top;
            this.label57.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(0, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(350, 24);
            this.label57.TabIndex = 9;
            this.label57.Text = "■ 세타";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel55
            // 
            this.panel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel55.Controls.Add(this.btnBreakUnitTYBBreaking);
            this.panel55.Controls.Add(this.label54);
            this.panel55.Controls.Add(this.btnBreakUnitTYBAlignCam);
            this.panel55.Controls.Add(this.btnBreakUnitTYBULD);
            this.panel55.Controls.Add(this.btnBreakUnitTYBLD);
            this.panel55.Location = new System.Drawing.Point(304, 32);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(283, 160);
            this.panel55.TabIndex = 459;
            // 
            // btnBreakUnitTYBBreaking
            // 
            this.btnBreakUnitTYBBreaking.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYBBreaking.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYBBreaking.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYBBreaking.Location = new System.Drawing.Point(12, 93);
            this.btnBreakUnitTYBBreaking.Name = "btnBreakUnitTYBBreaking";
            this.btnBreakUnitTYBBreaking.Size = new System.Drawing.Size(121, 55);
            this.btnBreakUnitTYBBreaking.TabIndex = 72;
            this.btnBreakUnitTYBBreaking.Text = "Breaking";
            this.btnBreakUnitTYBBreaking.UseVisualStyleBackColor = false;
            // 
            // label54
            // 
            this.label54.AutoEllipsis = true;
            this.label54.BackColor = System.Drawing.Color.Gainsboro;
            this.label54.Dock = System.Windows.Forms.DockStyle.Top;
            this.label54.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(0, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(281, 24);
            this.label54.TabIndex = 9;
            this.label54.Text = "■ B열";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakUnitTYBAlignCam
            // 
            this.btnBreakUnitTYBAlignCam.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYBAlignCam.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYBAlignCam.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYBAlignCam.Location = new System.Drawing.Point(148, 95);
            this.btnBreakUnitTYBAlignCam.Name = "btnBreakUnitTYBAlignCam";
            this.btnBreakUnitTYBAlignCam.Size = new System.Drawing.Size(122, 55);
            this.btnBreakUnitTYBAlignCam.TabIndex = 71;
            this.btnBreakUnitTYBAlignCam.Text = "Align Cam";
            this.btnBreakUnitTYBAlignCam.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYBULD
            // 
            this.btnBreakUnitTYBULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYBULD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYBULD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYBULD.Location = new System.Drawing.Point(148, 34);
            this.btnBreakUnitTYBULD.Name = "btnBreakUnitTYBULD";
            this.btnBreakUnitTYBULD.Size = new System.Drawing.Size(121, 50);
            this.btnBreakUnitTYBULD.TabIndex = 69;
            this.btnBreakUnitTYBULD.Text = "언로드";
            this.btnBreakUnitTYBULD.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYBLD
            // 
            this.btnBreakUnitTYBLD.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYBLD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYBLD.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYBLD.Location = new System.Drawing.Point(12, 34);
            this.btnBreakUnitTYBLD.Name = "btnBreakUnitTYBLD";
            this.btnBreakUnitTYBLD.Size = new System.Drawing.Size(121, 50);
            this.btnBreakUnitTYBLD.TabIndex = 68;
            this.btnBreakUnitTYBLD.Text = "로드";
            this.btnBreakUnitTYBLD.UseVisualStyleBackColor = false;
            // 
            // panel56
            // 
            this.panel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel56.Controls.Add(this.btnBreakUnitTYABreaking);
            this.panel56.Controls.Add(this.btnBreakUnitTYAAlignCam);
            this.panel56.Controls.Add(this.btnBreakUnitTYAULD);
            this.panel56.Controls.Add(this.btnBreakUnitTYALD);
            this.panel56.Controls.Add(this.label55);
            this.panel56.Location = new System.Drawing.Point(12, 32);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(283, 160);
            this.panel56.TabIndex = 457;
            // 
            // btnBreakUnitTYABreaking
            // 
            this.btnBreakUnitTYABreaking.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYABreaking.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYABreaking.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYABreaking.Location = new System.Drawing.Point(8, 93);
            this.btnBreakUnitTYABreaking.Name = "btnBreakUnitTYABreaking";
            this.btnBreakUnitTYABreaking.Size = new System.Drawing.Size(121, 55);
            this.btnBreakUnitTYABreaking.TabIndex = 67;
            this.btnBreakUnitTYABreaking.Text = "Breaking";
            this.btnBreakUnitTYABreaking.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYAAlignCam
            // 
            this.btnBreakUnitTYAAlignCam.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakUnitTYAAlignCam.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYAAlignCam.ForeColor = System.Drawing.Color.Black;
            this.btnBreakUnitTYAAlignCam.Location = new System.Drawing.Point(150, 94);
            this.btnBreakUnitTYAAlignCam.Name = "btnBreakUnitTYAAlignCam";
            this.btnBreakUnitTYAAlignCam.Size = new System.Drawing.Size(120, 55);
            this.btnBreakUnitTYAAlignCam.TabIndex = 66;
            this.btnBreakUnitTYAAlignCam.Text = "Align Cam";
            this.btnBreakUnitTYAAlignCam.UseVisualStyleBackColor = false;
            // 
            // label55
            // 
            this.label55.AutoEllipsis = true;
            this.label55.BackColor = System.Drawing.Color.Gainsboro;
            this.label55.Dock = System.Windows.Forms.DockStyle.Top;
            this.label55.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(0, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(281, 24);
            this.label55.TabIndex = 9;
            this.label55.Text = "■ A열";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.AutoEllipsis = true;
            this.label56.BackColor = System.Drawing.Color.Gainsboro;
            this.label56.Dock = System.Windows.Forms.DockStyle.Top;
            this.label56.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(0, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(954, 24);
            this.label56.TabIndex = 9;
            this.label56.Text = "■ 이동";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel50
            // 
            this.panel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel50.Controls.Add(this.btnBreakUnitTYB1BlowerOnOff);
            this.panel50.Controls.Add(this.label50);
            this.panel50.Controls.Add(this.btnBreakUnitTYB2VaccumOff);
            this.panel50.Controls.Add(this.btnBreakUnitTYB2VaccumOn);
            this.panel50.Controls.Add(this.btnBreakUnitTYB2BlowerOnOff);
            this.panel50.Controls.Add(this.btnBreakUnitTYB1VaccumOn);
            this.panel50.Controls.Add(this.btnBreakUnitTYB1VaccumOff);
            this.panel50.Location = new System.Drawing.Point(381, 7);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(368, 175);
            this.panel50.TabIndex = 466;
            // 
            // label50
            // 
            this.label50.AutoEllipsis = true;
            this.label50.BackColor = System.Drawing.Color.Gainsboro;
            this.label50.Dock = System.Windows.Forms.DockStyle.Top;
            this.label50.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(0, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(366, 24);
            this.label50.TabIndex = 9;
            this.label50.Text = "■ B열";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel52
            // 
            this.panel52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel52.Controls.Add(this.btnBreakUnitTYDummyBoxOutput);
            this.panel52.Controls.Add(this.btnBreakUnitTYDummyBoxInput);
            this.panel52.Controls.Add(this.panel53);
            this.panel52.Controls.Add(this.label52);
            this.panel52.Location = new System.Drawing.Point(756, 7);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(187, 175);
            this.panel52.TabIndex = 468;
            // 
            // panel53
            // 
            this.panel53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel53.Controls.Add(this.btnBreakUnitTYShutterClose);
            this.panel53.Controls.Add(this.btnBreakUnitTYShutterOpen);
            this.panel53.Controls.Add(this.label53);
            this.panel53.Location = new System.Drawing.Point(5, 78);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(175, 89);
            this.panel53.TabIndex = 469;
            // 
            // label53
            // 
            this.label53.AutoEllipsis = true;
            this.label53.BackColor = System.Drawing.Color.Gainsboro;
            this.label53.Dock = System.Windows.Forms.DockStyle.Top;
            this.label53.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(0, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(173, 24);
            this.label53.TabIndex = 9;
            this.label53.Text = "■ 셔터";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label52
            // 
            this.label52.AutoEllipsis = true;
            this.label52.BackColor = System.Drawing.Color.Gainsboro;
            this.label52.Dock = System.Windows.Forms.DockStyle.Top;
            this.label52.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(0, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(185, 24);
            this.label52.TabIndex = 9;
            this.label52.Text = "■ 더미박스";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel51
            // 
            this.panel51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel51.Controls.Add(this.label51);
            this.panel51.Controls.Add(this.btnBreakUnitTYA1VaccumOn);
            this.panel51.Controls.Add(this.btnBreakUnitTYA1BlowerOnOff);
            this.panel51.Controls.Add(this.btnBreakUnitTYA2VaccumOn);
            this.panel51.Controls.Add(this.btnBreakUnitTYA2VaccumOff);
            this.panel51.Controls.Add(this.btnBreakUnitTYA1VaccumOff);
            this.panel51.Controls.Add(this.btnBreakUnitTYA2BlowerOnOff);
            this.panel51.Location = new System.Drawing.Point(7, 7);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(368, 175);
            this.panel51.TabIndex = 465;
            // 
            // label51
            // 
            this.label51.AutoEllipsis = true;
            this.label51.BackColor = System.Drawing.Color.Gainsboro;
            this.label51.Dock = System.Windows.Forms.DockStyle.Top;
            this.label51.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(0, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(366, 24);
            this.label51.TabIndex = 9;
            this.label51.Text = "■ A열";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage8.Controls.Add(this.panel74);
            this.tabPage8.Location = new System.Drawing.Point(4, 34);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(969, 397);
            this.tabPage8.TabIndex = 11;
            this.tabPage8.Text = "Break Head(X,Z)";
            // 
            // panel74
            // 
            this.panel74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel74.Controls.Add(this.panel76);
            this.panel74.Controls.Add(this.panel77);
            this.panel74.Controls.Add(this.label77);
            this.panel74.Location = new System.Drawing.Point(7, 188);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(956, 201);
            this.panel74.TabIndex = 473;
            // 
            // panel76
            // 
            this.panel76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel76.Controls.Add(this.btnBZDown3);
            this.panel76.Controls.Add(this.btnBZDown1);
            this.panel76.Controls.Add(this.btnAZDown3);
            this.panel76.Controls.Add(this.btnBZJigAlign);
            this.panel76.Controls.Add(this.btnAZDown1);
            this.panel76.Controls.Add(this.label75);
            this.panel76.Controls.Add(this.btnAZJigAlign);
            this.panel76.Location = new System.Drawing.Point(283, 32);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(321, 160);
            this.panel76.TabIndex = 459;
            // 
            // btnBZDown3
            // 
            this.btnBZDown3.BackColor = System.Drawing.SystemColors.Control;
            this.btnBZDown3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBZDown3.ForeColor = System.Drawing.Color.Black;
            this.btnBZDown3.Location = new System.Drawing.Point(237, 90);
            this.btnBZDown3.Name = "btnBZDown3";
            this.btnBZDown3.Size = new System.Drawing.Size(69, 50);
            this.btnBZDown3.TabIndex = 78;
            this.btnBZDown3.Text = "B\r\nZ Down3";
            this.btnBZDown3.UseVisualStyleBackColor = false;
            // 
            // btnBZDown1
            // 
            this.btnBZDown1.BackColor = System.Drawing.SystemColors.Control;
            this.btnBZDown1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBZDown1.ForeColor = System.Drawing.Color.Black;
            this.btnBZDown1.Location = new System.Drawing.Point(162, 90);
            this.btnBZDown1.Name = "btnBZDown1";
            this.btnBZDown1.Size = new System.Drawing.Size(69, 50);
            this.btnBZDown1.TabIndex = 77;
            this.btnBZDown1.Text = "B\r\nZ Down1";
            this.btnBZDown1.UseVisualStyleBackColor = false;
            // 
            // btnAZDown3
            // 
            this.btnAZDown3.BackColor = System.Drawing.SystemColors.Control;
            this.btnAZDown3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAZDown3.ForeColor = System.Drawing.Color.Black;
            this.btnAZDown3.Location = new System.Drawing.Point(87, 90);
            this.btnAZDown3.Name = "btnAZDown3";
            this.btnAZDown3.Size = new System.Drawing.Size(69, 50);
            this.btnAZDown3.TabIndex = 72;
            this.btnAZDown3.Text = "A\r\nZ Down3";
            this.btnAZDown3.UseVisualStyleBackColor = false;
            // 
            // btnBZJigAlign
            // 
            this.btnBZJigAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnBZJigAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBZJigAlign.ForeColor = System.Drawing.Color.Black;
            this.btnBZJigAlign.Location = new System.Drawing.Point(162, 34);
            this.btnBZJigAlign.Name = "btnBZJigAlign";
            this.btnBZJigAlign.Size = new System.Drawing.Size(143, 50);
            this.btnBZJigAlign.TabIndex = 71;
            this.btnBZJigAlign.Text = "B\r\nZ jig";
            this.btnBZJigAlign.UseVisualStyleBackColor = false;
            // 
            // btnAZDown1
            // 
            this.btnAZDown1.BackColor = System.Drawing.SystemColors.Control;
            this.btnAZDown1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAZDown1.ForeColor = System.Drawing.Color.Black;
            this.btnAZDown1.Location = new System.Drawing.Point(12, 90);
            this.btnAZDown1.Name = "btnAZDown1";
            this.btnAZDown1.Size = new System.Drawing.Size(69, 50);
            this.btnAZDown1.TabIndex = 70;
            this.btnAZDown1.Text = "A\r\n Z Down1";
            this.btnAZDown1.UseVisualStyleBackColor = false;
            // 
            // label75
            // 
            this.label75.AutoEllipsis = true;
            this.label75.BackColor = System.Drawing.Color.Gainsboro;
            this.label75.Dock = System.Windows.Forms.DockStyle.Top;
            this.label75.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(0, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(319, 24);
            this.label75.TabIndex = 9;
            this.label75.Text = "■ Z축";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAZJigAlign
            // 
            this.btnAZJigAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnAZJigAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAZJigAlign.ForeColor = System.Drawing.Color.Black;
            this.btnAZJigAlign.Location = new System.Drawing.Point(12, 34);
            this.btnAZJigAlign.Name = "btnAZJigAlign";
            this.btnAZJigAlign.Size = new System.Drawing.Size(144, 50);
            this.btnAZJigAlign.TabIndex = 68;
            this.btnAZJigAlign.Text = "A\r\nZ jig";
            this.btnAZJigAlign.UseVisualStyleBackColor = false;
            // 
            // panel77
            // 
            this.panel77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel77.Controls.Add(this.btnB2Breaking);
            this.panel77.Controls.Add(this.btnB1Breaking);
            this.panel77.Controls.Add(this.btnA2Breaking);
            this.panel77.Controls.Add(this.btnA1Breaking);
            this.panel77.Controls.Add(this.label76);
            this.panel77.Location = new System.Drawing.Point(12, 32);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(265, 160);
            this.panel77.TabIndex = 457;
            // 
            // btnB2Breaking
            // 
            this.btnB2Breaking.BackColor = System.Drawing.SystemColors.Control;
            this.btnB2Breaking.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnB2Breaking.ForeColor = System.Drawing.Color.Black;
            this.btnB2Breaking.Location = new System.Drawing.Point(135, 90);
            this.btnB2Breaking.Name = "btnB2Breaking";
            this.btnB2Breaking.Size = new System.Drawing.Size(121, 50);
            this.btnB2Breaking.TabIndex = 66;
            this.btnB2Breaking.Text = "B2 브레이킹 위치";
            this.btnB2Breaking.UseVisualStyleBackColor = false;
            // 
            // btnB1Breaking
            // 
            this.btnB1Breaking.BackColor = System.Drawing.SystemColors.Control;
            this.btnB1Breaking.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnB1Breaking.ForeColor = System.Drawing.Color.Black;
            this.btnB1Breaking.Location = new System.Drawing.Point(8, 90);
            this.btnB1Breaking.Name = "btnB1Breaking";
            this.btnB1Breaking.Size = new System.Drawing.Size(121, 50);
            this.btnB1Breaking.TabIndex = 65;
            this.btnB1Breaking.Text = "B1 브레이킹 위치";
            this.btnB1Breaking.UseVisualStyleBackColor = false;
            // 
            // btnA2Breaking
            // 
            this.btnA2Breaking.BackColor = System.Drawing.SystemColors.Control;
            this.btnA2Breaking.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnA2Breaking.ForeColor = System.Drawing.Color.Black;
            this.btnA2Breaking.Location = new System.Drawing.Point(135, 34);
            this.btnA2Breaking.Name = "btnA2Breaking";
            this.btnA2Breaking.Size = new System.Drawing.Size(121, 50);
            this.btnA2Breaking.TabIndex = 64;
            this.btnA2Breaking.Text = "A2 브레이킹 위치";
            this.btnA2Breaking.UseVisualStyleBackColor = false;
            // 
            // btnA1Breaking
            // 
            this.btnA1Breaking.BackColor = System.Drawing.SystemColors.Control;
            this.btnA1Breaking.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnA1Breaking.ForeColor = System.Drawing.Color.Black;
            this.btnA1Breaking.Location = new System.Drawing.Point(8, 34);
            this.btnA1Breaking.Name = "btnA1Breaking";
            this.btnA1Breaking.Size = new System.Drawing.Size(121, 50);
            this.btnA1Breaking.TabIndex = 62;
            this.btnA1Breaking.Text = "A1 브레이킹 위치";
            this.btnA1Breaking.UseVisualStyleBackColor = false;
            // 
            // label76
            // 
            this.label76.AutoEllipsis = true;
            this.label76.BackColor = System.Drawing.Color.Gainsboro;
            this.label76.Dock = System.Windows.Forms.DockStyle.Top;
            this.label76.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(0, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(263, 24);
            this.label76.TabIndex = 9;
            this.label76.Text = "■ X축";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label77
            // 
            this.label77.AutoEllipsis = true;
            this.label77.BackColor = System.Drawing.Color.Gainsboro;
            this.label77.Dock = System.Windows.Forms.DockStyle.Top;
            this.label77.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(0, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(954, 24);
            this.label77.TabIndex = 9;
            this.label77.Text = "■ 이동";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage9.Controls.Add(this.panel72);
            this.tabPage9.Controls.Add(this.panel20);
            this.tabPage9.Controls.Add(this.panel22);
            this.tabPage9.Location = new System.Drawing.Point(4, 34);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(969, 397);
            this.tabPage9.TabIndex = 7;
            this.tabPage9.Text = "Before 트랜스퍼";
            // 
            // panel72
            // 
            this.panel72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel72.Controls.Add(this.panel73);
            this.panel72.Controls.Add(this.panel75);
            this.panel72.Controls.Add(this.label74);
            this.panel72.Location = new System.Drawing.Point(7, 188);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(523, 201);
            this.panel72.TabIndex = 474;
            // 
            // panel73
            // 
            this.panel73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel73.Controls.Add(this.btnBeforeTr2Insp);
            this.panel73.Controls.Add(this.label72);
            this.panel73.Controls.Add(this.btnBeforeTr2A0dgr);
            this.panel73.Controls.Add(this.btnBeforeTr2Loading);
            this.panel73.Controls.Add(this.btnBeforeTr2A90dgr);
            this.panel73.Controls.Add(this.btnBeforeTr2A90rdgr);
            this.panel73.Location = new System.Drawing.Point(261, 32);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(248, 160);
            this.panel73.TabIndex = 456;
            // 
            // btnBeforeTr2Insp
            // 
            this.btnBeforeTr2Insp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr2Insp.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr2Insp.Location = new System.Drawing.Point(130, 34);
            this.btnBeforeTr2Insp.Name = "btnBeforeTr2Insp";
            this.btnBeforeTr2Insp.Size = new System.Drawing.Size(100, 55);
            this.btnBeforeTr2Insp.TabIndex = 55;
            this.btnBeforeTr2Insp.Text = "TR2 검사";
            this.btnBeforeTr2Insp.UseVisualStyleBackColor = false;
            // 
            // label72
            // 
            this.label72.AutoEllipsis = true;
            this.label72.BackColor = System.Drawing.Color.Gainsboro;
            this.label72.Dock = System.Windows.Forms.DockStyle.Top;
            this.label72.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(0, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(246, 24);
            this.label72.TabIndex = 9;
            this.label72.Text = "■ B열";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBeforeTr2A0dgr
            // 
            this.btnBeforeTr2A0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr2A0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr2A0dgr.Location = new System.Drawing.Point(12, 99);
            this.btnBeforeTr2A0dgr.Name = "btnBeforeTr2A0dgr";
            this.btnBeforeTr2A0dgr.Size = new System.Drawing.Size(70, 50);
            this.btnBeforeTr2A0dgr.TabIndex = 58;
            this.btnBeforeTr2A0dgr.Text = "T2\r\n0도";
            this.btnBeforeTr2A0dgr.UseVisualStyleBackColor = false;
            // 
            // btnBeforeTr2Loading
            // 
            this.btnBeforeTr2Loading.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr2Loading.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr2Loading.Location = new System.Drawing.Point(12, 34);
            this.btnBeforeTr2Loading.Name = "btnBeforeTr2Loading";
            this.btnBeforeTr2Loading.Size = new System.Drawing.Size(100, 55);
            this.btnBeforeTr2Loading.TabIndex = 56;
            this.btnBeforeTr2Loading.Text = "TR2 로딩";
            this.btnBeforeTr2Loading.UseVisualStyleBackColor = false;
            // 
            // btnBeforeTr2A90dgr
            // 
            this.btnBeforeTr2A90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr2A90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr2A90dgr.Location = new System.Drawing.Point(86, 99);
            this.btnBeforeTr2A90dgr.Name = "btnBeforeTr2A90dgr";
            this.btnBeforeTr2A90dgr.Size = new System.Drawing.Size(70, 50);
            this.btnBeforeTr2A90dgr.TabIndex = 57;
            this.btnBeforeTr2A90dgr.Text = "T2\r\n+90도";
            this.btnBeforeTr2A90dgr.UseVisualStyleBackColor = false;
            // 
            // btnBeforeTr2A90rdgr
            // 
            this.btnBeforeTr2A90rdgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr2A90rdgr.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr2A90rdgr.Location = new System.Drawing.Point(160, 99);
            this.btnBeforeTr2A90rdgr.Name = "btnBeforeTr2A90rdgr";
            this.btnBeforeTr2A90rdgr.Size = new System.Drawing.Size(70, 50);
            this.btnBeforeTr2A90rdgr.TabIndex = 56;
            this.btnBeforeTr2A90rdgr.Text = "T2\r\n-90도";
            this.btnBeforeTr2A90rdgr.UseVisualStyleBackColor = false;
            // 
            // panel75
            // 
            this.panel75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel75.Controls.Add(this.btnBeforeTr1Insp);
            this.panel75.Controls.Add(this.label73);
            this.panel75.Controls.Add(this.btnBeforeTr1A0dgr);
            this.panel75.Controls.Add(this.btnBeforeTr1Loading);
            this.panel75.Controls.Add(this.btnBeforeTr1A90dgr);
            this.panel75.Controls.Add(this.btnBeforeTr1A90rdgr);
            this.panel75.Location = new System.Drawing.Point(7, 32);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(248, 160);
            this.panel75.TabIndex = 455;
            // 
            // btnBeforeTr1Insp
            // 
            this.btnBeforeTr1Insp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr1Insp.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr1Insp.Location = new System.Drawing.Point(130, 34);
            this.btnBeforeTr1Insp.Name = "btnBeforeTr1Insp";
            this.btnBeforeTr1Insp.Size = new System.Drawing.Size(100, 55);
            this.btnBeforeTr1Insp.TabIndex = 55;
            this.btnBeforeTr1Insp.Text = "TR1 검사";
            this.btnBeforeTr1Insp.UseVisualStyleBackColor = false;
            // 
            // label73
            // 
            this.label73.AutoEllipsis = true;
            this.label73.BackColor = System.Drawing.Color.Gainsboro;
            this.label73.Dock = System.Windows.Forms.DockStyle.Top;
            this.label73.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(0, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(246, 24);
            this.label73.TabIndex = 9;
            this.label73.Text = "■ A열";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBeforeTr1A0dgr
            // 
            this.btnBeforeTr1A0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr1A0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr1A0dgr.Location = new System.Drawing.Point(12, 98);
            this.btnBeforeTr1A0dgr.Name = "btnBeforeTr1A0dgr";
            this.btnBeforeTr1A0dgr.Size = new System.Drawing.Size(70, 50);
            this.btnBeforeTr1A0dgr.TabIndex = 58;
            this.btnBeforeTr1A0dgr.Text = "T1\r\n0도";
            this.btnBeforeTr1A0dgr.UseVisualStyleBackColor = false;
            // 
            // btnBeforeTr1Loading
            // 
            this.btnBeforeTr1Loading.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr1Loading.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr1Loading.Location = new System.Drawing.Point(12, 34);
            this.btnBeforeTr1Loading.Name = "btnBeforeTr1Loading";
            this.btnBeforeTr1Loading.Size = new System.Drawing.Size(100, 55);
            this.btnBeforeTr1Loading.TabIndex = 56;
            this.btnBeforeTr1Loading.Text = "TR1 로딩";
            this.btnBeforeTr1Loading.UseVisualStyleBackColor = false;
            // 
            // btnBeforeTr1A90dgr
            // 
            this.btnBeforeTr1A90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr1A90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr1A90dgr.Location = new System.Drawing.Point(86, 98);
            this.btnBeforeTr1A90dgr.Name = "btnBeforeTr1A90dgr";
            this.btnBeforeTr1A90dgr.Size = new System.Drawing.Size(70, 50);
            this.btnBeforeTr1A90dgr.TabIndex = 57;
            this.btnBeforeTr1A90dgr.Text = "T1\r\n+90도";
            this.btnBeforeTr1A90dgr.UseVisualStyleBackColor = false;
            // 
            // btnBeforeTr1A90rdgr
            // 
            this.btnBeforeTr1A90rdgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeTr1A90rdgr.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeTr1A90rdgr.Location = new System.Drawing.Point(160, 98);
            this.btnBeforeTr1A90rdgr.Name = "btnBeforeTr1A90rdgr";
            this.btnBeforeTr1A90rdgr.Size = new System.Drawing.Size(70, 50);
            this.btnBeforeTr1A90rdgr.TabIndex = 56;
            this.btnBeforeTr1A90rdgr.Text = "T1\r\n-90도";
            this.btnBeforeTr1A90rdgr.UseVisualStyleBackColor = false;
            // 
            // label74
            // 
            this.label74.AutoEllipsis = true;
            this.label74.BackColor = System.Drawing.Color.Gainsboro;
            this.label74.Dock = System.Windows.Forms.DockStyle.Top;
            this.label74.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(0, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(521, 24);
            this.label74.TabIndex = 9;
            this.label74.Text = "■ 이동";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.label19);
            this.panel20.Controls.Add(this.btnBeforeB2PikerBlowerOnOff);
            this.panel20.Controls.Add(this.btnBeforeB2PikerDown);
            this.panel20.Controls.Add(this.btnBeforeB1PikerUp);
            this.panel20.Controls.Add(this.btnBeforeB1PikerBlowerOnOff);
            this.panel20.Controls.Add(this.btnBeforeB1PikerDown);
            this.panel20.Controls.Add(this.btnBeforeB2PikerVaccumOff);
            this.panel20.Controls.Add(this.btnBeforeB2PikerUp);
            this.panel20.Controls.Add(this.btnBeforeB2PikerVaccumOn);
            this.panel20.Controls.Add(this.btnBeforeB1PikerVaccumOn);
            this.panel20.Controls.Add(this.btnBeforeB1PikerVaccumOff);
            this.panel20.Location = new System.Drawing.Point(478, 7);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(465, 175);
            this.panel20.TabIndex = 467;
            // 
            // label19
            // 
            this.label19.AutoEllipsis = true;
            this.label19.BackColor = System.Drawing.Color.Gainsboro;
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(463, 24);
            this.label19.TabIndex = 9;
            this.label19.Text = "■ B열";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBeforeB2PikerBlowerOnOff
            // 
            this.btnBeforeB2PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB2PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB2PikerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnBeforeB2PikerBlowerOnOff.Name = "btnBeforeB2PikerBlowerOnOff";
            this.btnBeforeB2PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnBeforeB2PikerBlowerOnOff.TabIndex = 64;
            this.btnBeforeB2PikerBlowerOnOff.Text = "B2\r\n피커 파기 온/오프";
            this.btnBeforeB2PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB2PikerDown
            // 
            this.btnBeforeB2PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB2PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB2PikerDown.Location = new System.Drawing.Point(348, 29);
            this.btnBeforeB2PikerDown.Name = "btnBeforeB2PikerDown";
            this.btnBeforeB2PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB2PikerDown.TabIndex = 57;
            this.btnBeforeB2PikerDown.Text = "B2\r\n피커 다운";
            this.btnBeforeB2PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB1PikerUp
            // 
            this.btnBeforeB1PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB1PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB1PikerUp.Location = new System.Drawing.Point(6, 29);
            this.btnBeforeB1PikerUp.Name = "btnBeforeB1PikerUp";
            this.btnBeforeB1PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB1PikerUp.TabIndex = 54;
            this.btnBeforeB1PikerUp.Text = "B1\r\n피커 업";
            this.btnBeforeB1PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB1PikerBlowerOnOff
            // 
            this.btnBeforeB1PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB1PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB1PikerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnBeforeB1PikerBlowerOnOff.Name = "btnBeforeB1PikerBlowerOnOff";
            this.btnBeforeB1PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnBeforeB1PikerBlowerOnOff.TabIndex = 62;
            this.btnBeforeB1PikerBlowerOnOff.Text = "B1\r\n피커 파기 온/오프";
            this.btnBeforeB1PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB1PikerDown
            // 
            this.btnBeforeB1PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB1PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB1PikerDown.Location = new System.Drawing.Point(120, 29);
            this.btnBeforeB1PikerDown.Name = "btnBeforeB1PikerDown";
            this.btnBeforeB1PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB1PikerDown.TabIndex = 55;
            this.btnBeforeB1PikerDown.Text = "B1\r\n피커 다운";
            this.btnBeforeB1PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB2PikerVaccumOff
            // 
            this.btnBeforeB2PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB2PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB2PikerVaccumOff.Location = new System.Drawing.Point(348, 77);
            this.btnBeforeB2PikerVaccumOff.Name = "btnBeforeB2PikerVaccumOff";
            this.btnBeforeB2PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB2PikerVaccumOff.TabIndex = 61;
            this.btnBeforeB2PikerVaccumOff.Text = "B2\r\n피커 공압 오프";
            this.btnBeforeB2PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB2PikerUp
            // 
            this.btnBeforeB2PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB2PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB2PikerUp.Location = new System.Drawing.Point(234, 29);
            this.btnBeforeB2PikerUp.Name = "btnBeforeB2PikerUp";
            this.btnBeforeB2PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB2PikerUp.TabIndex = 56;
            this.btnBeforeB2PikerUp.Text = "B2\r\n피커 업";
            this.btnBeforeB2PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB2PikerVaccumOn
            // 
            this.btnBeforeB2PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB2PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB2PikerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnBeforeB2PikerVaccumOn.Name = "btnBeforeB2PikerVaccumOn";
            this.btnBeforeB2PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB2PikerVaccumOn.TabIndex = 60;
            this.btnBeforeB2PikerVaccumOn.Text = "B2\r\n피커 공압 온";
            this.btnBeforeB2PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB1PikerVaccumOn
            // 
            this.btnBeforeB1PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB1PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB1PikerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnBeforeB1PikerVaccumOn.Name = "btnBeforeB1PikerVaccumOn";
            this.btnBeforeB1PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB1PikerVaccumOn.TabIndex = 58;
            this.btnBeforeB1PikerVaccumOn.Text = "B1\r\n피커 공압 온";
            this.btnBeforeB1PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBeforeB1PikerVaccumOff
            // 
            this.btnBeforeB1PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeB1PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeB1PikerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnBeforeB1PikerVaccumOff.Name = "btnBeforeB1PikerVaccumOff";
            this.btnBeforeB1PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeB1PikerVaccumOff.TabIndex = 59;
            this.btnBeforeB1PikerVaccumOff.Text = "B1\r\n파기 공압 오프";
            this.btnBeforeB1PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // panel22
            // 
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.label22);
            this.panel22.Controls.Add(this.btnBeforeA2PikerBlowerOnOff);
            this.panel22.Controls.Add(this.btnBeforeA2PikerDown);
            this.panel22.Controls.Add(this.btnBeforeA1PikerUp);
            this.panel22.Controls.Add(this.btnBeforeA1PikerBlowerOnOff);
            this.panel22.Controls.Add(this.btnBeforeA1PikerDown);
            this.panel22.Controls.Add(this.btnBeforeA2PikerVaccumOff);
            this.panel22.Controls.Add(this.btnBeforeA2PikerUp);
            this.panel22.Controls.Add(this.btnBeforeA2PikerVaccumOn);
            this.panel22.Controls.Add(this.btnBeforeA1PikerVaccumOn);
            this.panel22.Controls.Add(this.btnBeforeA1PikerVaccumOff);
            this.panel22.Location = new System.Drawing.Point(7, 7);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(465, 175);
            this.panel22.TabIndex = 466;
            // 
            // label22
            // 
            this.label22.AutoEllipsis = true;
            this.label22.BackColor = System.Drawing.Color.Gainsboro;
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(463, 24);
            this.label22.TabIndex = 9;
            this.label22.Text = "■ A열";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBeforeA2PikerBlowerOnOff
            // 
            this.btnBeforeA2PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA2PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA2PikerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnBeforeA2PikerBlowerOnOff.Name = "btnBeforeA2PikerBlowerOnOff";
            this.btnBeforeA2PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnBeforeA2PikerBlowerOnOff.TabIndex = 64;
            this.btnBeforeA2PikerBlowerOnOff.Text = "A2\r\n피커 파기 온/오프";
            this.btnBeforeA2PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA2PikerDown
            // 
            this.btnBeforeA2PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA2PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA2PikerDown.Location = new System.Drawing.Point(348, 29);
            this.btnBeforeA2PikerDown.Name = "btnBeforeA2PikerDown";
            this.btnBeforeA2PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA2PikerDown.TabIndex = 57;
            this.btnBeforeA2PikerDown.Text = "A2\r\n피커 다운";
            this.btnBeforeA2PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA1PikerUp
            // 
            this.btnBeforeA1PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA1PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA1PikerUp.Location = new System.Drawing.Point(6, 29);
            this.btnBeforeA1PikerUp.Name = "btnBeforeA1PikerUp";
            this.btnBeforeA1PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA1PikerUp.TabIndex = 54;
            this.btnBeforeA1PikerUp.Text = "A1\r\n피커 업";
            this.btnBeforeA1PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA1PikerBlowerOnOff
            // 
            this.btnBeforeA1PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA1PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA1PikerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnBeforeA1PikerBlowerOnOff.Name = "btnBeforeA1PikerBlowerOnOff";
            this.btnBeforeA1PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnBeforeA1PikerBlowerOnOff.TabIndex = 62;
            this.btnBeforeA1PikerBlowerOnOff.Text = "A1\r\n피커 파기 온/오프";
            this.btnBeforeA1PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA1PikerDown
            // 
            this.btnBeforeA1PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA1PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA1PikerDown.Location = new System.Drawing.Point(120, 29);
            this.btnBeforeA1PikerDown.Name = "btnBeforeA1PikerDown";
            this.btnBeforeA1PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA1PikerDown.TabIndex = 55;
            this.btnBeforeA1PikerDown.Text = "A1\r\n피커 다운";
            this.btnBeforeA1PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA2PikerVaccumOff
            // 
            this.btnBeforeA2PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA2PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA2PikerVaccumOff.Location = new System.Drawing.Point(348, 77);
            this.btnBeforeA2PikerVaccumOff.Name = "btnBeforeA2PikerVaccumOff";
            this.btnBeforeA2PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA2PikerVaccumOff.TabIndex = 61;
            this.btnBeforeA2PikerVaccumOff.Text = "A2\r\n피커 공압 오프";
            this.btnBeforeA2PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA2PikerUp
            // 
            this.btnBeforeA2PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA2PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA2PikerUp.Location = new System.Drawing.Point(234, 29);
            this.btnBeforeA2PikerUp.Name = "btnBeforeA2PikerUp";
            this.btnBeforeA2PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA2PikerUp.TabIndex = 56;
            this.btnBeforeA2PikerUp.Text = "A2\r\n피커 업";
            this.btnBeforeA2PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA2PikerVaccumOn
            // 
            this.btnBeforeA2PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA2PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA2PikerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnBeforeA2PikerVaccumOn.Name = "btnBeforeA2PikerVaccumOn";
            this.btnBeforeA2PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA2PikerVaccumOn.TabIndex = 60;
            this.btnBeforeA2PikerVaccumOn.Text = "A2\r\n피커 공압 온";
            this.btnBeforeA2PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA1PikerVaccumOn
            // 
            this.btnBeforeA1PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA1PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA1PikerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnBeforeA1PikerVaccumOn.Name = "btnBeforeA1PikerVaccumOn";
            this.btnBeforeA1PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA1PikerVaccumOn.TabIndex = 58;
            this.btnBeforeA1PikerVaccumOn.Text = "A1\r\n피커 공압 온";
            this.btnBeforeA1PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnBeforeA1PikerVaccumOff
            // 
            this.btnBeforeA1PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnBeforeA1PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnBeforeA1PikerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnBeforeA1PikerVaccumOff.Name = "btnBeforeA1PikerVaccumOff";
            this.btnBeforeA1PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnBeforeA1PikerVaccumOff.TabIndex = 59;
            this.btnBeforeA1PikerVaccumOff.Text = "A1\r\n피커 공압 오프";
            this.btnBeforeA1PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // tabPage10
            // 
            this.tabPage10.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage10.Controls.Add(this.panel79);
            this.tabPage10.Controls.Add(this.panel23);
            this.tabPage10.Controls.Add(this.panel24);
            this.tabPage10.Location = new System.Drawing.Point(4, 34);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(969, 397);
            this.tabPage10.TabIndex = 12;
            this.tabPage10.Text = "검사 유닛";
            // 
            // panel79
            // 
            this.panel79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel79.Controls.Add(this.panel80);
            this.panel79.Controls.Add(this.panel81);
            this.panel79.Controls.Add(this.label81);
            this.panel79.Location = new System.Drawing.Point(7, 188);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(487, 201);
            this.panel79.TabIndex = 475;
            // 
            // panel80
            // 
            this.panel80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel80.Controls.Add(this.btnInspStBULD);
            this.panel80.Controls.Add(this.label79);
            this.panel80.Controls.Add(this.btnInspStBInsp);
            this.panel80.Controls.Add(this.btnInspStBLD);
            this.panel80.Location = new System.Drawing.Point(246, 32);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(233, 160);
            this.panel80.TabIndex = 456;
            // 
            // btnInspStBULD
            // 
            this.btnInspStBULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStBULD.ForeColor = System.Drawing.Color.Black;
            this.btnInspStBULD.Location = new System.Drawing.Point(118, 34);
            this.btnInspStBULD.Name = "btnInspStBULD";
            this.btnInspStBULD.Size = new System.Drawing.Size(100, 55);
            this.btnInspStBULD.TabIndex = 55;
            this.btnInspStBULD.Text = "B 언로딩";
            this.btnInspStBULD.UseVisualStyleBackColor = false;
            // 
            // label79
            // 
            this.label79.AutoEllipsis = true;
            this.label79.BackColor = System.Drawing.Color.Gainsboro;
            this.label79.Dock = System.Windows.Forms.DockStyle.Top;
            this.label79.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(0, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(231, 24);
            this.label79.TabIndex = 9;
            this.label79.Text = "■ B열";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInspStBInsp
            // 
            this.btnInspStBInsp.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStBInsp.ForeColor = System.Drawing.Color.Black;
            this.btnInspStBInsp.Location = new System.Drawing.Point(12, 94);
            this.btnInspStBInsp.Name = "btnInspStBInsp";
            this.btnInspStBInsp.Size = new System.Drawing.Size(206, 55);
            this.btnInspStBInsp.TabIndex = 58;
            this.btnInspStBInsp.Text = "A 검사";
            this.btnInspStBInsp.UseVisualStyleBackColor = false;
            // 
            // btnInspStBLD
            // 
            this.btnInspStBLD.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStBLD.ForeColor = System.Drawing.Color.Black;
            this.btnInspStBLD.Location = new System.Drawing.Point(12, 34);
            this.btnInspStBLD.Name = "btnInspStBLD";
            this.btnInspStBLD.Size = new System.Drawing.Size(100, 55);
            this.btnInspStBLD.TabIndex = 56;
            this.btnInspStBLD.Text = "B 로딩";
            this.btnInspStBLD.UseVisualStyleBackColor = false;
            // 
            // panel81
            // 
            this.panel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel81.Controls.Add(this.btnInspStAULD);
            this.panel81.Controls.Add(this.label80);
            this.panel81.Controls.Add(this.btnInspStAInsp);
            this.panel81.Controls.Add(this.btnInspStALD);
            this.panel81.Location = new System.Drawing.Point(7, 32);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(233, 160);
            this.panel81.TabIndex = 455;
            // 
            // btnInspStAULD
            // 
            this.btnInspStAULD.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStAULD.ForeColor = System.Drawing.Color.Black;
            this.btnInspStAULD.Location = new System.Drawing.Point(118, 34);
            this.btnInspStAULD.Name = "btnInspStAULD";
            this.btnInspStAULD.Size = new System.Drawing.Size(100, 55);
            this.btnInspStAULD.TabIndex = 55;
            this.btnInspStAULD.Text = "A 언로딩";
            this.btnInspStAULD.UseVisualStyleBackColor = false;
            // 
            // label80
            // 
            this.label80.AutoEllipsis = true;
            this.label80.BackColor = System.Drawing.Color.Gainsboro;
            this.label80.Dock = System.Windows.Forms.DockStyle.Top;
            this.label80.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(0, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(231, 24);
            this.label80.TabIndex = 9;
            this.label80.Text = "■ A열";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInspStAInsp
            // 
            this.btnInspStAInsp.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStAInsp.ForeColor = System.Drawing.Color.Black;
            this.btnInspStAInsp.Location = new System.Drawing.Point(12, 94);
            this.btnInspStAInsp.Name = "btnInspStAInsp";
            this.btnInspStAInsp.Size = new System.Drawing.Size(206, 55);
            this.btnInspStAInsp.TabIndex = 58;
            this.btnInspStAInsp.Text = "A 검사";
            this.btnInspStAInsp.UseVisualStyleBackColor = false;
            // 
            // btnInspStALD
            // 
            this.btnInspStALD.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStALD.ForeColor = System.Drawing.Color.Black;
            this.btnInspStALD.Location = new System.Drawing.Point(12, 34);
            this.btnInspStALD.Name = "btnInspStALD";
            this.btnInspStALD.Size = new System.Drawing.Size(100, 55);
            this.btnInspStALD.TabIndex = 56;
            this.btnInspStALD.Text = "A 로딩";
            this.btnInspStALD.UseVisualStyleBackColor = false;
            // 
            // label81
            // 
            this.label81.AutoEllipsis = true;
            this.label81.BackColor = System.Drawing.Color.Gainsboro;
            this.label81.Dock = System.Windows.Forms.DockStyle.Top;
            this.label81.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(0, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(485, 24);
            this.label81.TabIndex = 9;
            this.label81.Text = "■ 이동";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.btnInspStB1BlowerOnOff);
            this.panel23.Controls.Add(this.label23);
            this.panel23.Controls.Add(this.btnInspStB2VacOff);
            this.panel23.Controls.Add(this.btnInspStB2VacOn);
            this.panel23.Controls.Add(this.btnInspStB2BlowerOnOff);
            this.panel23.Controls.Add(this.btnInspStB1VacOn);
            this.panel23.Controls.Add(this.btnInspStB1VacOff);
            this.panel23.Location = new System.Drawing.Point(478, 7);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(465, 175);
            this.panel23.TabIndex = 470;
            // 
            // btnInspStB1BlowerOnOff
            // 
            this.btnInspStB1BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStB1BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStB1BlowerOnOff.Location = new System.Drawing.Point(22, 102);
            this.btnInspStB1BlowerOnOff.Name = "btnInspStB1BlowerOnOff";
            this.btnInspStB1BlowerOnOff.Size = new System.Drawing.Size(206, 60);
            this.btnInspStB1BlowerOnOff.TabIndex = 62;
            this.btnInspStB1BlowerOnOff.Text = "B1\r\n파기 온/오프";
            this.btnInspStB1BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // label23
            // 
            this.label23.AutoEllipsis = true;
            this.label23.BackColor = System.Drawing.Color.Gainsboro;
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(0, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(463, 24);
            this.label23.TabIndex = 9;
            this.label23.Text = "■ B열";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInspStB2VacOff
            // 
            this.btnInspStB2VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStB2VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStB2VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStB2VacOff.Location = new System.Drawing.Point(340, 36);
            this.btnInspStB2VacOff.Name = "btnInspStB2VacOff";
            this.btnInspStB2VacOff.Size = new System.Drawing.Size(100, 60);
            this.btnInspStB2VacOff.TabIndex = 56;
            this.btnInspStB2VacOff.Text = "B2\r\n공압 오프";
            this.btnInspStB2VacOff.UseVisualStyleBackColor = false;
            // 
            // btnInspStB2VacOn
            // 
            this.btnInspStB2VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStB2VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStB2VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnInspStB2VacOn.Location = new System.Drawing.Point(234, 36);
            this.btnInspStB2VacOn.Name = "btnInspStB2VacOn";
            this.btnInspStB2VacOn.Size = new System.Drawing.Size(100, 60);
            this.btnInspStB2VacOn.TabIndex = 59;
            this.btnInspStB2VacOn.Text = "B2\r\n공압 온";
            this.btnInspStB2VacOn.UseVisualStyleBackColor = false;
            // 
            // btnInspStB2BlowerOnOff
            // 
            this.btnInspStB2BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStB2BlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStB2BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStB2BlowerOnOff.Location = new System.Drawing.Point(234, 102);
            this.btnInspStB2BlowerOnOff.Name = "btnInspStB2BlowerOnOff";
            this.btnInspStB2BlowerOnOff.Size = new System.Drawing.Size(206, 60);
            this.btnInspStB2BlowerOnOff.TabIndex = 60;
            this.btnInspStB2BlowerOnOff.Text = "B2\r\n파기 온/오프";
            this.btnInspStB2BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnInspStB1VacOn
            // 
            this.btnInspStB1VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStB1VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStB1VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnInspStB1VacOn.Location = new System.Drawing.Point(22, 36);
            this.btnInspStB1VacOn.Name = "btnInspStB1VacOn";
            this.btnInspStB1VacOn.Size = new System.Drawing.Size(100, 60);
            this.btnInspStB1VacOn.TabIndex = 57;
            this.btnInspStB1VacOn.Text = "B1\r\n공압 온";
            this.btnInspStB1VacOn.UseVisualStyleBackColor = false;
            // 
            // btnInspStB1VacOff
            // 
            this.btnInspStB1VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStB1VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStB1VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStB1VacOff.Location = new System.Drawing.Point(128, 36);
            this.btnInspStB1VacOff.Name = "btnInspStB1VacOff";
            this.btnInspStB1VacOff.Size = new System.Drawing.Size(100, 60);
            this.btnInspStB1VacOff.TabIndex = 55;
            this.btnInspStB1VacOff.Text = "B1\r\n공압 오프";
            this.btnInspStB1VacOff.UseVisualStyleBackColor = false;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.label24);
            this.panel24.Controls.Add(this.btnInspStA1VacOn);
            this.panel24.Controls.Add(this.btnInspStA1BlowerOnOff);
            this.panel24.Controls.Add(this.btnInspStA2VacOn);
            this.panel24.Controls.Add(this.btnInspStA2VacOff);
            this.panel24.Controls.Add(this.btnInspStA1VacOff);
            this.panel24.Controls.Add(this.btnInspStA2BlowerOnOff);
            this.panel24.Location = new System.Drawing.Point(7, 7);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(465, 175);
            this.panel24.TabIndex = 469;
            // 
            // label24
            // 
            this.label24.AutoEllipsis = true;
            this.label24.BackColor = System.Drawing.Color.Gainsboro;
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(463, 24);
            this.label24.TabIndex = 9;
            this.label24.Text = "■ A열";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInspStA1VacOn
            // 
            this.btnInspStA1VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStA1VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStA1VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnInspStA1VacOn.Location = new System.Drawing.Point(22, 36);
            this.btnInspStA1VacOn.Name = "btnInspStA1VacOn";
            this.btnInspStA1VacOn.Size = new System.Drawing.Size(100, 60);
            this.btnInspStA1VacOn.TabIndex = 57;
            this.btnInspStA1VacOn.Text = "A1\r\n공압 온";
            this.btnInspStA1VacOn.UseVisualStyleBackColor = false;
            // 
            // btnInspStA1BlowerOnOff
            // 
            this.btnInspStA1BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStA1BlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStA1BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStA1BlowerOnOff.Location = new System.Drawing.Point(22, 102);
            this.btnInspStA1BlowerOnOff.Name = "btnInspStA1BlowerOnOff";
            this.btnInspStA1BlowerOnOff.Size = new System.Drawing.Size(206, 60);
            this.btnInspStA1BlowerOnOff.TabIndex = 62;
            this.btnInspStA1BlowerOnOff.Text = "A1\r\n파기 온/오프";
            this.btnInspStA1BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnInspStA2VacOn
            // 
            this.btnInspStA2VacOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStA2VacOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStA2VacOn.ForeColor = System.Drawing.Color.Black;
            this.btnInspStA2VacOn.Location = new System.Drawing.Point(234, 36);
            this.btnInspStA2VacOn.Name = "btnInspStA2VacOn";
            this.btnInspStA2VacOn.Size = new System.Drawing.Size(100, 60);
            this.btnInspStA2VacOn.TabIndex = 59;
            this.btnInspStA2VacOn.Text = "A2\r\n공압 온";
            this.btnInspStA2VacOn.UseVisualStyleBackColor = false;
            // 
            // btnInspStA2VacOff
            // 
            this.btnInspStA2VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStA2VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStA2VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStA2VacOff.Location = new System.Drawing.Point(340, 36);
            this.btnInspStA2VacOff.Name = "btnInspStA2VacOff";
            this.btnInspStA2VacOff.Size = new System.Drawing.Size(100, 60);
            this.btnInspStA2VacOff.TabIndex = 56;
            this.btnInspStA2VacOff.Text = "A2\r\n공압 오프";
            this.btnInspStA2VacOff.UseVisualStyleBackColor = false;
            // 
            // btnInspStA1VacOff
            // 
            this.btnInspStA1VacOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStA1VacOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStA1VacOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStA1VacOff.Location = new System.Drawing.Point(128, 36);
            this.btnInspStA1VacOff.Name = "btnInspStA1VacOff";
            this.btnInspStA1VacOff.Size = new System.Drawing.Size(100, 60);
            this.btnInspStA1VacOff.TabIndex = 55;
            this.btnInspStA1VacOff.Text = "A1\r\n공압 오프";
            this.btnInspStA1VacOff.UseVisualStyleBackColor = false;
            // 
            // btnInspStA2BlowerOnOff
            // 
            this.btnInspStA2BlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspStA2BlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspStA2BlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnInspStA2BlowerOnOff.Location = new System.Drawing.Point(234, 102);
            this.btnInspStA2BlowerOnOff.Name = "btnInspStA2BlowerOnOff";
            this.btnInspStA2BlowerOnOff.Size = new System.Drawing.Size(206, 60);
            this.btnInspStA2BlowerOnOff.TabIndex = 60;
            this.btnInspStA2BlowerOnOff.Text = "A2\r\n파기 온/오프";
            this.btnInspStA2BlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // tabPage11
            // 
            this.tabPage11.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage11.Controls.Add(this.panel59);
            this.tabPage11.Controls.Add(this.panel46);
            this.tabPage11.Controls.Add(this.panel49);
            this.tabPage11.Location = new System.Drawing.Point(4, 34);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(969, 397);
            this.tabPage11.TabIndex = 13;
            this.tabPage11.Text = "After 트랜스퍼";
            // 
            // panel59
            // 
            this.panel59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel59.Controls.Add(this.panel61);
            this.panel59.Controls.Add(this.panel65);
            this.panel59.Controls.Add(this.label61);
            this.panel59.Location = new System.Drawing.Point(7, 188);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(956, 201);
            this.panel59.TabIndex = 473;
            // 
            // panel61
            // 
            this.panel61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel61.Controls.Add(this.btnAfterTr2Insp);
            this.panel61.Controls.Add(this.btnAfterTr2Unloading);
            this.panel61.Controls.Add(this.btnAfterTr2B0dgr);
            this.panel61.Controls.Add(this.btnAfterTr2B90dgr);
            this.panel61.Controls.Add(this.label58);
            this.panel61.Controls.Add(this.btnAfterTr2B90rdgr);
            this.panel61.Location = new System.Drawing.Point(261, 32);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(248, 160);
            this.panel61.TabIndex = 456;
            // 
            // btnAfterTr2Insp
            // 
            this.btnAfterTr2Insp.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr2Insp.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr2Insp.Location = new System.Drawing.Point(12, 34);
            this.btnAfterTr2Insp.Name = "btnAfterTr2Insp";
            this.btnAfterTr2Insp.Size = new System.Drawing.Size(100, 55);
            this.btnAfterTr2Insp.TabIndex = 62;
            this.btnAfterTr2Insp.Text = "TR2 검사";
            this.btnAfterTr2Insp.UseVisualStyleBackColor = false;
            // 
            // btnAfterTr2Unloading
            // 
            this.btnAfterTr2Unloading.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr2Unloading.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr2Unloading.Location = new System.Drawing.Point(130, 34);
            this.btnAfterTr2Unloading.Name = "btnAfterTr2Unloading";
            this.btnAfterTr2Unloading.Size = new System.Drawing.Size(100, 55);
            this.btnAfterTr2Unloading.TabIndex = 63;
            this.btnAfterTr2Unloading.Text = "TR2 언로딩";
            this.btnAfterTr2Unloading.UseVisualStyleBackColor = false;
            // 
            // btnAfterTr2B0dgr
            // 
            this.btnAfterTr2B0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr2B0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr2B0dgr.Location = new System.Drawing.Point(12, 99);
            this.btnAfterTr2B0dgr.Name = "btnAfterTr2B0dgr";
            this.btnAfterTr2B0dgr.Size = new System.Drawing.Size(70, 50);
            this.btnAfterTr2B0dgr.TabIndex = 61;
            this.btnAfterTr2B0dgr.Text = "T2\n0도";
            this.btnAfterTr2B0dgr.UseVisualStyleBackColor = false;
            // 
            // btnAfterTr2B90dgr
            // 
            this.btnAfterTr2B90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr2B90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr2B90dgr.Location = new System.Drawing.Point(86, 99);
            this.btnAfterTr2B90dgr.Name = "btnAfterTr2B90dgr";
            this.btnAfterTr2B90dgr.Size = new System.Drawing.Size(70, 50);
            this.btnAfterTr2B90dgr.TabIndex = 60;
            this.btnAfterTr2B90dgr.Text = "T2\r\n+90도";
            this.btnAfterTr2B90dgr.UseVisualStyleBackColor = false;
            // 
            // label58
            // 
            this.label58.AutoEllipsis = true;
            this.label58.BackColor = System.Drawing.Color.Gainsboro;
            this.label58.Dock = System.Windows.Forms.DockStyle.Top;
            this.label58.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(0, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(246, 24);
            this.label58.TabIndex = 9;
            this.label58.Text = "■ B열";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAfterTr2B90rdgr
            // 
            this.btnAfterTr2B90rdgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr2B90rdgr.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr2B90rdgr.Location = new System.Drawing.Point(160, 99);
            this.btnAfterTr2B90rdgr.Name = "btnAfterTr2B90rdgr";
            this.btnAfterTr2B90rdgr.Size = new System.Drawing.Size(70, 50);
            this.btnAfterTr2B90rdgr.TabIndex = 59;
            this.btnAfterTr2B90rdgr.Text = "T2\r\n-90도";
            this.btnAfterTr2B90rdgr.UseVisualStyleBackColor = false;
            // 
            // panel65
            // 
            this.panel65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel65.Controls.Add(this.btnAfterTr1Insp);
            this.panel65.Controls.Add(this.btnAfterTr1Unloading);
            this.panel65.Controls.Add(this.btnAfterTr1A0dgr);
            this.panel65.Controls.Add(this.btnAfterTr1A90dgr);
            this.panel65.Controls.Add(this.label59);
            this.panel65.Controls.Add(this.btnAfterTr1A90rdgr);
            this.panel65.Location = new System.Drawing.Point(7, 32);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(248, 160);
            this.panel65.TabIndex = 455;
            // 
            // btnAfterTr1Insp
            // 
            this.btnAfterTr1Insp.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr1Insp.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr1Insp.Location = new System.Drawing.Point(12, 34);
            this.btnAfterTr1Insp.Name = "btnAfterTr1Insp";
            this.btnAfterTr1Insp.Size = new System.Drawing.Size(100, 55);
            this.btnAfterTr1Insp.TabIndex = 62;
            this.btnAfterTr1Insp.Text = "TR1 검사";
            this.btnAfterTr1Insp.UseVisualStyleBackColor = false;
            // 
            // btnAfterTr1Unloading
            // 
            this.btnAfterTr1Unloading.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr1Unloading.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr1Unloading.Location = new System.Drawing.Point(130, 34);
            this.btnAfterTr1Unloading.Name = "btnAfterTr1Unloading";
            this.btnAfterTr1Unloading.Size = new System.Drawing.Size(100, 55);
            this.btnAfterTr1Unloading.TabIndex = 63;
            this.btnAfterTr1Unloading.Text = "TR1 언로딩";
            this.btnAfterTr1Unloading.UseVisualStyleBackColor = false;
            // 
            // btnAfterTr1A0dgr
            // 
            this.btnAfterTr1A0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr1A0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr1A0dgr.Location = new System.Drawing.Point(12, 98);
            this.btnAfterTr1A0dgr.Name = "btnAfterTr1A0dgr";
            this.btnAfterTr1A0dgr.Size = new System.Drawing.Size(70, 50);
            this.btnAfterTr1A0dgr.TabIndex = 61;
            this.btnAfterTr1A0dgr.Text = "T1\r\n0도";
            this.btnAfterTr1A0dgr.UseVisualStyleBackColor = false;
            // 
            // btnAfterTr1A90dgr
            // 
            this.btnAfterTr1A90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr1A90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr1A90dgr.Location = new System.Drawing.Point(86, 98);
            this.btnAfterTr1A90dgr.Name = "btnAfterTr1A90dgr";
            this.btnAfterTr1A90dgr.Size = new System.Drawing.Size(70, 50);
            this.btnAfterTr1A90dgr.TabIndex = 60;
            this.btnAfterTr1A90dgr.Text = "T1\r\n+90도";
            this.btnAfterTr1A90dgr.UseVisualStyleBackColor = false;
            // 
            // label59
            // 
            this.label59.AutoEllipsis = true;
            this.label59.BackColor = System.Drawing.Color.Gainsboro;
            this.label59.Dock = System.Windows.Forms.DockStyle.Top;
            this.label59.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(0, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(246, 24);
            this.label59.TabIndex = 9;
            this.label59.Text = "■ A열";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAfterTr1A90rdgr
            // 
            this.btnAfterTr1A90rdgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterTr1A90rdgr.ForeColor = System.Drawing.Color.Black;
            this.btnAfterTr1A90rdgr.Location = new System.Drawing.Point(160, 98);
            this.btnAfterTr1A90rdgr.Name = "btnAfterTr1A90rdgr";
            this.btnAfterTr1A90rdgr.Size = new System.Drawing.Size(70, 50);
            this.btnAfterTr1A90rdgr.TabIndex = 59;
            this.btnAfterTr1A90rdgr.Text = "T1\r\n-90도";
            this.btnAfterTr1A90rdgr.UseVisualStyleBackColor = false;
            // 
            // label61
            // 
            this.label61.AutoEllipsis = true;
            this.label61.BackColor = System.Drawing.Color.Gainsboro;
            this.label61.Dock = System.Windows.Forms.DockStyle.Top;
            this.label61.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(0, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(954, 24);
            this.label61.TabIndex = 9;
            this.label61.Text = "■ 이동";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel46
            // 
            this.panel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel46.Controls.Add(this.label46);
            this.panel46.Controls.Add(this.btnAfterB2PikerBlowerOnOff);
            this.panel46.Controls.Add(this.btnAfterB2PikerDown);
            this.panel46.Controls.Add(this.btnAfterB1PikerUp);
            this.panel46.Controls.Add(this.btnAfterB1PikerBlowerOnOff);
            this.panel46.Controls.Add(this.btnAfterB1PikerDown);
            this.panel46.Controls.Add(this.btnAfterB2PikerVaccumOff);
            this.panel46.Controls.Add(this.btnAfterB2PikerUp);
            this.panel46.Controls.Add(this.btnAfterB2PikerVaccumOn);
            this.panel46.Controls.Add(this.btnAfterB1PikerVaccumOn);
            this.panel46.Controls.Add(this.btnAfterB1PikerVaccumOff);
            this.panel46.Location = new System.Drawing.Point(478, 7);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(465, 175);
            this.panel46.TabIndex = 472;
            // 
            // label46
            // 
            this.label46.AutoEllipsis = true;
            this.label46.BackColor = System.Drawing.Color.Gainsboro;
            this.label46.Dock = System.Windows.Forms.DockStyle.Top;
            this.label46.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(0, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(463, 24);
            this.label46.TabIndex = 9;
            this.label46.Text = "■ B열";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAfterB2PikerBlowerOnOff
            // 
            this.btnAfterB2PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB2PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB2PikerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnAfterB2PikerBlowerOnOff.Name = "btnAfterB2PikerBlowerOnOff";
            this.btnAfterB2PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnAfterB2PikerBlowerOnOff.TabIndex = 64;
            this.btnAfterB2PikerBlowerOnOff.Text = "B2\r\n피커 파기 온/오프";
            this.btnAfterB2PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnAfterB2PikerDown
            // 
            this.btnAfterB2PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB2PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB2PikerDown.Location = new System.Drawing.Point(348, 29);
            this.btnAfterB2PikerDown.Name = "btnAfterB2PikerDown";
            this.btnAfterB2PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB2PikerDown.TabIndex = 57;
            this.btnAfterB2PikerDown.Text = "B2\r\n피커 다운";
            this.btnAfterB2PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnAfterB1PikerUp
            // 
            this.btnAfterB1PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB1PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB1PikerUp.Location = new System.Drawing.Point(6, 29);
            this.btnAfterB1PikerUp.Name = "btnAfterB1PikerUp";
            this.btnAfterB1PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB1PikerUp.TabIndex = 54;
            this.btnAfterB1PikerUp.Text = "B1\r\n피커 업";
            this.btnAfterB1PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnAfterB1PikerBlowerOnOff
            // 
            this.btnAfterB1PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB1PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB1PikerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnAfterB1PikerBlowerOnOff.Name = "btnAfterB1PikerBlowerOnOff";
            this.btnAfterB1PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnAfterB1PikerBlowerOnOff.TabIndex = 62;
            this.btnAfterB1PikerBlowerOnOff.Text = "B1\r\n피커 파기 온/오프";
            this.btnAfterB1PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnAfterB1PikerDown
            // 
            this.btnAfterB1PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB1PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB1PikerDown.Location = new System.Drawing.Point(120, 29);
            this.btnAfterB1PikerDown.Name = "btnAfterB1PikerDown";
            this.btnAfterB1PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB1PikerDown.TabIndex = 55;
            this.btnAfterB1PikerDown.Text = "B1\r\n피커 다운";
            this.btnAfterB1PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnAfterB2PikerVaccumOff
            // 
            this.btnAfterB2PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB2PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB2PikerVaccumOff.Location = new System.Drawing.Point(348, 77);
            this.btnAfterB2PikerVaccumOff.Name = "btnAfterB2PikerVaccumOff";
            this.btnAfterB2PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB2PikerVaccumOff.TabIndex = 61;
            this.btnAfterB2PikerVaccumOff.Text = "B2\r\n피커 공압 오프";
            this.btnAfterB2PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnAfterB2PikerUp
            // 
            this.btnAfterB2PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB2PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB2PikerUp.Location = new System.Drawing.Point(234, 29);
            this.btnAfterB2PikerUp.Name = "btnAfterB2PikerUp";
            this.btnAfterB2PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB2PikerUp.TabIndex = 56;
            this.btnAfterB2PikerUp.Text = "B2\r\n피커 업";
            this.btnAfterB2PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnAfterB2PikerVaccumOn
            // 
            this.btnAfterB2PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB2PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB2PikerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnAfterB2PikerVaccumOn.Name = "btnAfterB2PikerVaccumOn";
            this.btnAfterB2PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB2PikerVaccumOn.TabIndex = 60;
            this.btnAfterB2PikerVaccumOn.Text = "B2\r\n피커 공압 온";
            this.btnAfterB2PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnAfterB1PikerVaccumOn
            // 
            this.btnAfterB1PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB1PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB1PikerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnAfterB1PikerVaccumOn.Name = "btnAfterB1PikerVaccumOn";
            this.btnAfterB1PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB1PikerVaccumOn.TabIndex = 58;
            this.btnAfterB1PikerVaccumOn.Text = "B1\r\n피커 공압 온";
            this.btnAfterB1PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnAfterB1PikerVaccumOff
            // 
            this.btnAfterB1PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterB1PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterB1PikerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnAfterB1PikerVaccumOff.Name = "btnAfterB1PikerVaccumOff";
            this.btnAfterB1PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnAfterB1PikerVaccumOff.TabIndex = 59;
            this.btnAfterB1PikerVaccumOff.Text = "B1\r\n피커 공압 오프";
            this.btnAfterB1PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // panel49
            // 
            this.panel49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel49.Controls.Add(this.label49);
            this.panel49.Controls.Add(this.btnAfterA2PikerBlowerOnOff);
            this.panel49.Controls.Add(this.btnAfterA2PikerDown);
            this.panel49.Controls.Add(this.btnAfterA1PikerUp);
            this.panel49.Controls.Add(this.btnAfterA1PikerBlowerOnOff);
            this.panel49.Controls.Add(this.btnAfterA1PikerDown);
            this.panel49.Controls.Add(this.btnAfterA2PikerVaccumOff);
            this.panel49.Controls.Add(this.btnAfterA2PikerUp);
            this.panel49.Controls.Add(this.btnAfterA2PikerVaccumOn);
            this.panel49.Controls.Add(this.btnAfterA1PikerVaccumOn);
            this.panel49.Controls.Add(this.btnAfterA1PikerVaccumOff);
            this.panel49.Location = new System.Drawing.Point(7, 7);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(465, 175);
            this.panel49.TabIndex = 471;
            // 
            // label49
            // 
            this.label49.AutoEllipsis = true;
            this.label49.BackColor = System.Drawing.Color.Gainsboro;
            this.label49.Dock = System.Windows.Forms.DockStyle.Top;
            this.label49.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(0, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(463, 24);
            this.label49.TabIndex = 9;
            this.label49.Text = "■ A열";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAfterA2PikerBlowerOnOff
            // 
            this.btnAfterA2PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA2PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA2PikerBlowerOnOff.Location = new System.Drawing.Point(234, 125);
            this.btnAfterA2PikerBlowerOnOff.Name = "btnAfterA2PikerBlowerOnOff";
            this.btnAfterA2PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnAfterA2PikerBlowerOnOff.TabIndex = 64;
            this.btnAfterA2PikerBlowerOnOff.Text = "A2\r\n피커 파기 온/오프";
            this.btnAfterA2PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnAfterA2PikerDown
            // 
            this.btnAfterA2PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA2PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA2PikerDown.Location = new System.Drawing.Point(348, 29);
            this.btnAfterA2PikerDown.Name = "btnAfterA2PikerDown";
            this.btnAfterA2PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA2PikerDown.TabIndex = 57;
            this.btnAfterA2PikerDown.Text = "A2\r\n피커 다운";
            this.btnAfterA2PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnAfterA1PikerUp
            // 
            this.btnAfterA1PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA1PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA1PikerUp.Location = new System.Drawing.Point(6, 29);
            this.btnAfterA1PikerUp.Name = "btnAfterA1PikerUp";
            this.btnAfterA1PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA1PikerUp.TabIndex = 54;
            this.btnAfterA1PikerUp.Text = "A1\r\n피커 업";
            this.btnAfterA1PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnAfterA1PikerBlowerOnOff
            // 
            this.btnAfterA1PikerBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA1PikerBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA1PikerBlowerOnOff.Location = new System.Drawing.Point(6, 125);
            this.btnAfterA1PikerBlowerOnOff.Name = "btnAfterA1PikerBlowerOnOff";
            this.btnAfterA1PikerBlowerOnOff.Size = new System.Drawing.Size(222, 42);
            this.btnAfterA1PikerBlowerOnOff.TabIndex = 62;
            this.btnAfterA1PikerBlowerOnOff.Text = "A1\r\n피커 파기 온/오프";
            this.btnAfterA1PikerBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnAfterA1PikerDown
            // 
            this.btnAfterA1PikerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA1PikerDown.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA1PikerDown.Location = new System.Drawing.Point(120, 29);
            this.btnAfterA1PikerDown.Name = "btnAfterA1PikerDown";
            this.btnAfterA1PikerDown.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA1PikerDown.TabIndex = 55;
            this.btnAfterA1PikerDown.Text = "A1\r\n피커 다운";
            this.btnAfterA1PikerDown.UseVisualStyleBackColor = false;
            // 
            // btnAfterA2PikerVaccumOff
            // 
            this.btnAfterA2PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA2PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA2PikerVaccumOff.Location = new System.Drawing.Point(348, 77);
            this.btnAfterA2PikerVaccumOff.Name = "btnAfterA2PikerVaccumOff";
            this.btnAfterA2PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA2PikerVaccumOff.TabIndex = 61;
            this.btnAfterA2PikerVaccumOff.Text = "A2\r\n피커 공압 오프";
            this.btnAfterA2PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnAfterA2PikerUp
            // 
            this.btnAfterA2PikerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA2PikerUp.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA2PikerUp.Location = new System.Drawing.Point(234, 29);
            this.btnAfterA2PikerUp.Name = "btnAfterA2PikerUp";
            this.btnAfterA2PikerUp.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA2PikerUp.TabIndex = 56;
            this.btnAfterA2PikerUp.Text = "A2\r\n피커 업";
            this.btnAfterA2PikerUp.UseVisualStyleBackColor = false;
            // 
            // btnAfterA2PikerVaccumOn
            // 
            this.btnAfterA2PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA2PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA2PikerVaccumOn.Location = new System.Drawing.Point(234, 77);
            this.btnAfterA2PikerVaccumOn.Name = "btnAfterA2PikerVaccumOn";
            this.btnAfterA2PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA2PikerVaccumOn.TabIndex = 60;
            this.btnAfterA2PikerVaccumOn.Text = "A2\r\n피커 공압 온";
            this.btnAfterA2PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnAfterA1PikerVaccumOn
            // 
            this.btnAfterA1PikerVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA1PikerVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA1PikerVaccumOn.Location = new System.Drawing.Point(6, 77);
            this.btnAfterA1PikerVaccumOn.Name = "btnAfterA1PikerVaccumOn";
            this.btnAfterA1PikerVaccumOn.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA1PikerVaccumOn.TabIndex = 58;
            this.btnAfterA1PikerVaccumOn.Text = "A1\r\n피커 공압 온";
            this.btnAfterA1PikerVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnAfterA1PikerVaccumOff
            // 
            this.btnAfterA1PikerVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnAfterA1PikerVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnAfterA1PikerVaccumOff.Location = new System.Drawing.Point(120, 77);
            this.btnAfterA1PikerVaccumOff.Name = "btnAfterA1PikerVaccumOff";
            this.btnAfterA1PikerVaccumOff.Size = new System.Drawing.Size(108, 42);
            this.btnAfterA1PikerVaccumOff.TabIndex = 59;
            this.btnAfterA1PikerVaccumOff.Text = "A1\r\n피커 공압 오프";
            this.btnAfterA1PikerVaccumOff.UseVisualStyleBackColor = false;
            // 
            // tabPage12
            // 
            this.tabPage12.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage12.Controls.Add(this.panel58);
            this.tabPage12.Location = new System.Drawing.Point(4, 34);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(969, 397);
            this.tabPage12.TabIndex = 8;
            this.tabPage12.Text = "카메라 유닛";
            // 
            // panel58
            // 
            this.panel58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel58.Controls.Add(this.panel82);
            this.panel58.Controls.Add(this.panel78);
            this.panel58.Controls.Add(this.label60);
            this.panel58.Controls.Add(this.panel21);
            this.panel58.Location = new System.Drawing.Point(7, 188);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(956, 201);
            this.panel58.TabIndex = 468;
            // 
            // panel82
            // 
            this.panel82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel82.Controls.Add(this.btnCamUnitB2);
            this.panel82.Controls.Add(this.btnCamUnitB1);
            this.panel82.Controls.Add(this.btnCamUnitA2);
            this.panel82.Controls.Add(this.label82);
            this.panel82.Controls.Add(this.btnCamUnitA1);
            this.panel82.Location = new System.Drawing.Point(621, 32);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(300, 160);
            this.panel82.TabIndex = 460;
            // 
            // btnCamUnitB2
            // 
            this.btnCamUnitB2.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitB2.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitB2.Location = new System.Drawing.Point(155, 92);
            this.btnCamUnitB2.Name = "btnCamUnitB2";
            this.btnCamUnitB2.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitB2.TabIndex = 67;
            this.btnCamUnitB2.Text = "B2";
            this.btnCamUnitB2.UseVisualStyleBackColor = false;
            // 
            // btnCamUnitB1
            // 
            this.btnCamUnitB1.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitB1.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitB1.Location = new System.Drawing.Point(155, 31);
            this.btnCamUnitB1.Name = "btnCamUnitB1";
            this.btnCamUnitB1.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitB1.TabIndex = 66;
            this.btnCamUnitB1.Text = "B1";
            this.btnCamUnitB1.UseVisualStyleBackColor = false;
            // 
            // btnCamUnitA2
            // 
            this.btnCamUnitA2.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitA2.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitA2.Location = new System.Drawing.Point(10, 92);
            this.btnCamUnitA2.Name = "btnCamUnitA2";
            this.btnCamUnitA2.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitA2.TabIndex = 65;
            this.btnCamUnitA2.Text = "A2";
            this.btnCamUnitA2.UseVisualStyleBackColor = false;
            // 
            // label82
            // 
            this.label82.AutoEllipsis = true;
            this.label82.BackColor = System.Drawing.Color.Gainsboro;
            this.label82.Dock = System.Windows.Forms.DockStyle.Top;
            this.label82.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(0, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(298, 24);
            this.label82.TabIndex = 9;
            this.label82.Text = "■ Z축";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCamUnitA1
            // 
            this.btnCamUnitA1.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitA1.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitA1.Location = new System.Drawing.Point(10, 31);
            this.btnCamUnitA1.Name = "btnCamUnitA1";
            this.btnCamUnitA1.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitA1.TabIndex = 64;
            this.btnCamUnitA1.Text = "A1";
            this.btnCamUnitA1.UseVisualStyleBackColor = false;
            // 
            // panel78
            // 
            this.panel78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel78.Controls.Add(this.btnCamUnitBInspUpEnd);
            this.panel78.Controls.Add(this.btnCamUnitBInspLowEnd);
            this.panel78.Controls.Add(this.btnCamUnitBInspUpStart);
            this.panel78.Controls.Add(this.label78);
            this.panel78.Controls.Add(this.btnCamUnitBInspLowStart);
            this.panel78.Location = new System.Drawing.Point(315, 32);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(300, 160);
            this.panel78.TabIndex = 459;
            // 
            // btnCamUnitBInspUpEnd
            // 
            this.btnCamUnitBInspUpEnd.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitBInspUpEnd.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitBInspUpEnd.Location = new System.Drawing.Point(155, 92);
            this.btnCamUnitBInspUpEnd.Name = "btnCamUnitBInspUpEnd";
            this.btnCamUnitBInspUpEnd.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitBInspUpEnd.TabIndex = 67;
            this.btnCamUnitBInspUpEnd.Text = "Inspection B\r\n상부 End";
            this.btnCamUnitBInspUpEnd.UseVisualStyleBackColor = false;
            // 
            // btnCamUnitBInspLowEnd
            // 
            this.btnCamUnitBInspLowEnd.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitBInspLowEnd.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitBInspLowEnd.Location = new System.Drawing.Point(155, 31);
            this.btnCamUnitBInspLowEnd.Name = "btnCamUnitBInspLowEnd";
            this.btnCamUnitBInspLowEnd.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitBInspLowEnd.TabIndex = 66;
            this.btnCamUnitBInspLowEnd.Text = "Inspection B\r\n하부 End";
            this.btnCamUnitBInspLowEnd.UseVisualStyleBackColor = false;
            // 
            // btnCamUnitBInspUpStart
            // 
            this.btnCamUnitBInspUpStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitBInspUpStart.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitBInspUpStart.Location = new System.Drawing.Point(10, 92);
            this.btnCamUnitBInspUpStart.Name = "btnCamUnitBInspUpStart";
            this.btnCamUnitBInspUpStart.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitBInspUpStart.TabIndex = 65;
            this.btnCamUnitBInspUpStart.Text = "Inspection B\r\n상부 Start";
            this.btnCamUnitBInspUpStart.UseVisualStyleBackColor = false;
            // 
            // label78
            // 
            this.label78.AutoEllipsis = true;
            this.label78.BackColor = System.Drawing.Color.Gainsboro;
            this.label78.Dock = System.Windows.Forms.DockStyle.Top;
            this.label78.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(0, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(298, 24);
            this.label78.TabIndex = 9;
            this.label78.Text = "■ A열";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCamUnitBInspLowStart
            // 
            this.btnCamUnitBInspLowStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitBInspLowStart.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitBInspLowStart.Location = new System.Drawing.Point(10, 31);
            this.btnCamUnitBInspLowStart.Name = "btnCamUnitBInspLowStart";
            this.btnCamUnitBInspLowStart.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitBInspLowStart.TabIndex = 64;
            this.btnCamUnitBInspLowStart.Text = "Inspection B\r\n하부 Start";
            this.btnCamUnitBInspLowStart.UseVisualStyleBackColor = false;
            // 
            // label60
            // 
            this.label60.AutoEllipsis = true;
            this.label60.BackColor = System.Drawing.Color.Gainsboro;
            this.label60.Dock = System.Windows.Forms.DockStyle.Top;
            this.label60.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(0, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(954, 24);
            this.label60.TabIndex = 9;
            this.label60.Text = "■ 이동";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.btnCamUnitAInspUpEnd);
            this.panel21.Controls.Add(this.btnCamUnitAInspLowEnd);
            this.panel21.Controls.Add(this.btnCamUnitAInspUpStart);
            this.panel21.Controls.Add(this.label20);
            this.panel21.Controls.Add(this.btnCamUnitAInspLowStart);
            this.panel21.Location = new System.Drawing.Point(9, 32);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(300, 160);
            this.panel21.TabIndex = 457;
            // 
            // btnCamUnitAInspUpEnd
            // 
            this.btnCamUnitAInspUpEnd.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitAInspUpEnd.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitAInspUpEnd.Location = new System.Drawing.Point(155, 92);
            this.btnCamUnitAInspUpEnd.Name = "btnCamUnitAInspUpEnd";
            this.btnCamUnitAInspUpEnd.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitAInspUpEnd.TabIndex = 67;
            this.btnCamUnitAInspUpEnd.Text = "Inspection A\r\n상부 End";
            this.btnCamUnitAInspUpEnd.UseVisualStyleBackColor = false;
            // 
            // btnCamUnitAInspLowEnd
            // 
            this.btnCamUnitAInspLowEnd.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitAInspLowEnd.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitAInspLowEnd.Location = new System.Drawing.Point(155, 31);
            this.btnCamUnitAInspLowEnd.Name = "btnCamUnitAInspLowEnd";
            this.btnCamUnitAInspLowEnd.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitAInspLowEnd.TabIndex = 66;
            this.btnCamUnitAInspLowEnd.Text = "Inspection A\r\n하부 End";
            this.btnCamUnitAInspLowEnd.UseVisualStyleBackColor = false;
            // 
            // btnCamUnitAInspUpStart
            // 
            this.btnCamUnitAInspUpStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitAInspUpStart.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitAInspUpStart.Location = new System.Drawing.Point(10, 92);
            this.btnCamUnitAInspUpStart.Name = "btnCamUnitAInspUpStart";
            this.btnCamUnitAInspUpStart.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitAInspUpStart.TabIndex = 65;
            this.btnCamUnitAInspUpStart.Text = "Inspection A\r\n상부 Start";
            this.btnCamUnitAInspUpStart.UseVisualStyleBackColor = false;
            // 
            // label20
            // 
            this.label20.AutoEllipsis = true;
            this.label20.BackColor = System.Drawing.Color.Gainsboro;
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(298, 24);
            this.label20.TabIndex = 9;
            this.label20.Text = "■ A열";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCamUnitAInspLowStart
            // 
            this.btnCamUnitAInspLowStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnCamUnitAInspLowStart.ForeColor = System.Drawing.Color.Black;
            this.btnCamUnitAInspLowStart.Location = new System.Drawing.Point(10, 31);
            this.btnCamUnitAInspLowStart.Name = "btnCamUnitAInspLowStart";
            this.btnCamUnitAInspLowStart.Size = new System.Drawing.Size(130, 55);
            this.btnCamUnitAInspLowStart.TabIndex = 64;
            this.btnCamUnitAInspLowStart.Text = "Inspection A\r\n하부 Start";
            this.btnCamUnitAInspLowStart.UseVisualStyleBackColor = false;
            // 
            // tabPage13
            // 
            this.tabPage13.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage13.Controls.Add(this.panel13);
            this.tabPage13.Controls.Add(this.panel4);
            this.tabPage13.Controls.Add(this.panel11);
            this.tabPage13.Controls.Add(this.panel10);
            this.tabPage13.Controls.Add(this.panel12);
            this.tabPage13.Location = new System.Drawing.Point(4, 34);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(969, 397);
            this.tabPage13.TabIndex = 9;
            this.tabPage13.Text = "셀 투입 이재기";
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.panel2);
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Controls.Add(this.panel16);
            this.panel13.Controls.Add(this.label17);
            this.panel13.Controls.Add(this.panel19);
            this.panel13.Location = new System.Drawing.Point(7, 185);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(956, 206);
            this.panel13.TabIndex = 474;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnCellInputY1BCstInner);
            this.panel2.Controls.Add(this.btnCellInputY1ACstInner);
            this.panel2.Controls.Add(this.btnCellInputY1CstMid);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnCellInputY1CellULD);
            this.panel2.Controls.Add(this.btnCellInputY1CellLD);
            this.panel2.Location = new System.Drawing.Point(478, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(228, 168);
            this.panel2.TabIndex = 459;
            // 
            // btnCellInputY1BCstInner
            // 
            this.btnCellInputY1BCstInner.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY1BCstInner.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY1BCstInner.Location = new System.Drawing.Point(117, 29);
            this.btnCellInputY1BCstInner.Name = "btnCellInputY1BCstInner";
            this.btnCellInputY1BCstInner.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY1BCstInner.TabIndex = 41;
            this.btnCellInputY1BCstInner.Text = "Y1\r\nB카세트 안쪽";
            this.btnCellInputY1BCstInner.UseVisualStyleBackColor = false;
            // 
            // btnCellInputY1ACstInner
            // 
            this.btnCellInputY1ACstInner.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY1ACstInner.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY1ACstInner.Location = new System.Drawing.Point(11, 29);
            this.btnCellInputY1ACstInner.Name = "btnCellInputY1ACstInner";
            this.btnCellInputY1ACstInner.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY1ACstInner.TabIndex = 40;
            this.btnCellInputY1ACstInner.Text = "Y1\r\nA카세트 안쪽";
            this.btnCellInputY1ACstInner.UseVisualStyleBackColor = false;
            // 
            // btnCellInputY1CstMid
            // 
            this.btnCellInputY1CstMid.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY1CstMid.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY1CstMid.Location = new System.Drawing.Point(11, 75);
            this.btnCellInputY1CstMid.Name = "btnCellInputY1CstMid";
            this.btnCellInputY1CstMid.Size = new System.Drawing.Size(206, 40);
            this.btnCellInputY1CstMid.TabIndex = 39;
            this.btnCellInputY1CstMid.Text = "Y1 \r\n카세트 중간";
            this.btnCellInputY1CstMid.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(226, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ Y 1번축";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputY1CellULD
            // 
            this.btnCellInputY1CellULD.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY1CellULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY1CellULD.Location = new System.Drawing.Point(117, 121);
            this.btnCellInputY1CellULD.Name = "btnCellInputY1CellULD";
            this.btnCellInputY1CellULD.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY1CellULD.TabIndex = 35;
            this.btnCellInputY1CellULD.Text = "Y1\r\nCell UL/D";
            this.btnCellInputY1CellULD.UseVisualStyleBackColor = false;
            // 
            // btnCellInputY1CellLD
            // 
            this.btnCellInputY1CellLD.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY1CellLD.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY1CellLD.Location = new System.Drawing.Point(11, 121);
            this.btnCellInputY1CellLD.Name = "btnCellInputY1CellLD";
            this.btnCellInputY1CellLD.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY1CellLD.TabIndex = 33;
            this.btnCellInputY1CellLD.Text = "Y1\r\n Cell L/D";
            this.btnCellInputY1CellLD.UseVisualStyleBackColor = false;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnCellInputB1LoadingA);
            this.panel15.Controls.Add(this.btnCellInputB1HoldB);
            this.panel15.Controls.Add(this.btnCellInputB1LoadingB);
            this.panel15.Controls.Add(this.btnCellInputB1MiddleB);
            this.panel15.Controls.Add(this.label14);
            this.panel15.Controls.Add(this.btnCellInputB1HoldA);
            this.panel15.Controls.Add(this.btnCellInputB1MiddleA);
            this.panel15.Location = new System.Drawing.Point(244, 33);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(228, 168);
            this.panel15.TabIndex = 458;
            // 
            // btnCellInputB1LoadingA
            // 
            this.btnCellInputB1LoadingA.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputB1LoadingA.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputB1LoadingA.Location = new System.Drawing.Point(11, 121);
            this.btnCellInputB1LoadingA.Name = "btnCellInputB1LoadingA";
            this.btnCellInputB1LoadingA.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputB1LoadingA.TabIndex = 32;
            this.btnCellInputB1LoadingA.Text = "A 로딩";
            this.btnCellInputB1LoadingA.UseVisualStyleBackColor = false;
            // 
            // btnCellInputB1HoldB
            // 
            this.btnCellInputB1HoldB.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputB1HoldB.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputB1HoldB.Location = new System.Drawing.Point(117, 75);
            this.btnCellInputB1HoldB.Name = "btnCellInputB1HoldB";
            this.btnCellInputB1HoldB.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputB1HoldB.TabIndex = 32;
            this.btnCellInputB1HoldB.Text = "B 대기";
            this.btnCellInputB1HoldB.UseVisualStyleBackColor = false;
            // 
            // btnCellInputB1LoadingB
            // 
            this.btnCellInputB1LoadingB.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputB1LoadingB.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputB1LoadingB.Location = new System.Drawing.Point(117, 121);
            this.btnCellInputB1LoadingB.Name = "btnCellInputB1LoadingB";
            this.btnCellInputB1LoadingB.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputB1LoadingB.TabIndex = 31;
            this.btnCellInputB1LoadingB.Text = "B 로딩";
            this.btnCellInputB1LoadingB.UseVisualStyleBackColor = false;
            // 
            // btnCellInputB1MiddleB
            // 
            this.btnCellInputB1MiddleB.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputB1MiddleB.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputB1MiddleB.Location = new System.Drawing.Point(117, 29);
            this.btnCellInputB1MiddleB.Name = "btnCellInputB1MiddleB";
            this.btnCellInputB1MiddleB.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputB1MiddleB.TabIndex = 31;
            this.btnCellInputB1MiddleB.Text = "B 중간";
            this.btnCellInputB1MiddleB.UseVisualStyleBackColor = false;
            // 
            // label14
            // 
            this.label14.AutoEllipsis = true;
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(226, 24);
            this.label14.TabIndex = 9;
            this.label14.Text = "■ B열";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputB1HoldA
            // 
            this.btnCellInputB1HoldA.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputB1HoldA.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputB1HoldA.Location = new System.Drawing.Point(11, 75);
            this.btnCellInputB1HoldA.Name = "btnCellInputB1HoldA";
            this.btnCellInputB1HoldA.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputB1HoldA.TabIndex = 30;
            this.btnCellInputB1HoldA.Text = "A 대기";
            this.btnCellInputB1HoldA.UseVisualStyleBackColor = false;
            // 
            // btnCellInputB1MiddleA
            // 
            this.btnCellInputB1MiddleA.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputB1MiddleA.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputB1MiddleA.Location = new System.Drawing.Point(11, 29);
            this.btnCellInputB1MiddleA.Name = "btnCellInputB1MiddleA";
            this.btnCellInputB1MiddleA.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputB1MiddleA.TabIndex = 29;
            this.btnCellInputB1MiddleA.Text = "A 중간";
            this.btnCellInputB1MiddleA.UseVisualStyleBackColor = false;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.btnCellInputA1LoadingA);
            this.panel16.Controls.Add(this.btnCellInputA1LoadingB);
            this.panel16.Controls.Add(this.btnCellInputA1HoldB);
            this.panel16.Controls.Add(this.btnCellInputA1MiddleB);
            this.panel16.Controls.Add(this.label15);
            this.panel16.Controls.Add(this.btnCellInputA1HoldA);
            this.panel16.Controls.Add(this.btnCellInputA1MiddleA);
            this.panel16.Location = new System.Drawing.Point(10, 33);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(228, 168);
            this.panel16.TabIndex = 457;
            // 
            // btnCellInputA1LoadingA
            // 
            this.btnCellInputA1LoadingA.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputA1LoadingA.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputA1LoadingA.Location = new System.Drawing.Point(11, 121);
            this.btnCellInputA1LoadingA.Name = "btnCellInputA1LoadingA";
            this.btnCellInputA1LoadingA.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputA1LoadingA.TabIndex = 30;
            this.btnCellInputA1LoadingA.Text = "A 로딩";
            this.btnCellInputA1LoadingA.UseVisualStyleBackColor = false;
            // 
            // btnCellInputA1LoadingB
            // 
            this.btnCellInputA1LoadingB.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputA1LoadingB.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputA1LoadingB.Location = new System.Drawing.Point(118, 121);
            this.btnCellInputA1LoadingB.Name = "btnCellInputA1LoadingB";
            this.btnCellInputA1LoadingB.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputA1LoadingB.TabIndex = 29;
            this.btnCellInputA1LoadingB.Text = "B 로딩";
            this.btnCellInputA1LoadingB.UseVisualStyleBackColor = false;
            // 
            // btnCellInputA1HoldB
            // 
            this.btnCellInputA1HoldB.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputA1HoldB.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputA1HoldB.Location = new System.Drawing.Point(118, 75);
            this.btnCellInputA1HoldB.Name = "btnCellInputA1HoldB";
            this.btnCellInputA1HoldB.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputA1HoldB.TabIndex = 28;
            this.btnCellInputA1HoldB.Text = "B 대기";
            this.btnCellInputA1HoldB.UseVisualStyleBackColor = false;
            // 
            // btnCellInputA1MiddleB
            // 
            this.btnCellInputA1MiddleB.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputA1MiddleB.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputA1MiddleB.Location = new System.Drawing.Point(117, 29);
            this.btnCellInputA1MiddleB.Name = "btnCellInputA1MiddleB";
            this.btnCellInputA1MiddleB.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputA1MiddleB.TabIndex = 27;
            this.btnCellInputA1MiddleB.Text = "B 중간";
            this.btnCellInputA1MiddleB.UseVisualStyleBackColor = false;
            // 
            // label15
            // 
            this.label15.AutoEllipsis = true;
            this.label15.BackColor = System.Drawing.Color.Gainsboro;
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(226, 24);
            this.label15.TabIndex = 9;
            this.label15.Text = "■ A열";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputA1HoldA
            // 
            this.btnCellInputA1HoldA.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputA1HoldA.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputA1HoldA.Location = new System.Drawing.Point(11, 75);
            this.btnCellInputA1HoldA.Name = "btnCellInputA1HoldA";
            this.btnCellInputA1HoldA.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputA1HoldA.TabIndex = 26;
            this.btnCellInputA1HoldA.Text = "A 대기";
            this.btnCellInputA1HoldA.UseVisualStyleBackColor = false;
            // 
            // btnCellInputA1MiddleA
            // 
            this.btnCellInputA1MiddleA.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputA1MiddleA.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputA1MiddleA.Location = new System.Drawing.Point(11, 29);
            this.btnCellInputA1MiddleA.Name = "btnCellInputA1MiddleA";
            this.btnCellInputA1MiddleA.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputA1MiddleA.TabIndex = 25;
            this.btnCellInputA1MiddleA.Text = "A 중간";
            this.btnCellInputA1MiddleA.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoEllipsis = true;
            this.label17.BackColor = System.Drawing.Color.Gainsboro;
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(954, 24);
            this.label17.TabIndex = 9;
            this.label17.Text = "■ 이동";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.btnCellInputY2BCstInner);
            this.panel19.Controls.Add(this.btnCellInputY2ACstInner);
            this.panel19.Controls.Add(this.btnCellInputY2CstMid);
            this.panel19.Controls.Add(this.label18);
            this.panel19.Controls.Add(this.btnCellInputY2CellULD);
            this.panel19.Controls.Add(this.btnCellInputY2CellLD);
            this.panel19.Location = new System.Drawing.Point(714, 33);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(228, 168);
            this.panel19.TabIndex = 460;
            // 
            // btnCellInputY2BCstInner
            // 
            this.btnCellInputY2BCstInner.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY2BCstInner.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY2BCstInner.Location = new System.Drawing.Point(116, 29);
            this.btnCellInputY2BCstInner.Name = "btnCellInputY2BCstInner";
            this.btnCellInputY2BCstInner.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY2BCstInner.TabIndex = 41;
            this.btnCellInputY2BCstInner.Text = "Y2\r\nB카세트 안쪽";
            this.btnCellInputY2BCstInner.UseVisualStyleBackColor = false;
            // 
            // btnCellInputY2ACstInner
            // 
            this.btnCellInputY2ACstInner.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY2ACstInner.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY2ACstInner.Location = new System.Drawing.Point(10, 29);
            this.btnCellInputY2ACstInner.Name = "btnCellInputY2ACstInner";
            this.btnCellInputY2ACstInner.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY2ACstInner.TabIndex = 40;
            this.btnCellInputY2ACstInner.Text = "Y2\r\nA카세트 안쪽";
            this.btnCellInputY2ACstInner.UseVisualStyleBackColor = false;
            // 
            // btnCellInputY2CstMid
            // 
            this.btnCellInputY2CstMid.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY2CstMid.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY2CstMid.Location = new System.Drawing.Point(12, 75);
            this.btnCellInputY2CstMid.Name = "btnCellInputY2CstMid";
            this.btnCellInputY2CstMid.Size = new System.Drawing.Size(206, 40);
            this.btnCellInputY2CstMid.TabIndex = 39;
            this.btnCellInputY2CstMid.Text = "Y2 \r\n카세트 중간";
            this.btnCellInputY2CstMid.UseVisualStyleBackColor = false;
            // 
            // label18
            // 
            this.label18.AutoEllipsis = true;
            this.label18.BackColor = System.Drawing.Color.Gainsboro;
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(226, 24);
            this.label18.TabIndex = 9;
            this.label18.Text = "■ Y 1번축";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputY2CellULD
            // 
            this.btnCellInputY2CellULD.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY2CellULD.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY2CellULD.Location = new System.Drawing.Point(116, 121);
            this.btnCellInputY2CellULD.Name = "btnCellInputY2CellULD";
            this.btnCellInputY2CellULD.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY2CellULD.TabIndex = 35;
            this.btnCellInputY2CellULD.Text = "Y2\r\nCell UL/D";
            this.btnCellInputY2CellULD.UseVisualStyleBackColor = false;
            // 
            // btnCellInputY2CellLD
            // 
            this.btnCellInputY2CellLD.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.btnCellInputY2CellLD.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputY2CellLD.Location = new System.Drawing.Point(10, 121);
            this.btnCellInputY2CellLD.Name = "btnCellInputY2CellLD";
            this.btnCellInputY2CellLD.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputY2CellLD.TabIndex = 33;
            this.btnCellInputY2CellLD.Text = "Y2\r\n Cell L/D";
            this.btnCellInputY2CellLD.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.btnCellInputBlowerOnOff);
            this.panel4.Controls.Add(this.btnCellInputIonizerOnOff);
            this.panel4.Location = new System.Drawing.Point(729, 8);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(230, 171);
            this.panel4.TabIndex = 469;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(228, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "■ 이오나이저";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputBlowerOnOff
            // 
            this.btnCellInputBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBlowerOnOff.Location = new System.Drawing.Point(11, 97);
            this.btnCellInputBlowerOnOff.Name = "btnCellInputBlowerOnOff";
            this.btnCellInputBlowerOnOff.Size = new System.Drawing.Size(206, 63);
            this.btnCellInputBlowerOnOff.TabIndex = 457;
            this.btnCellInputBlowerOnOff.Text = "파기 온/오프";
            this.btnCellInputBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputIonizerOnOff
            // 
            this.btnCellInputIonizerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputIonizerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputIonizerOnOff.Location = new System.Drawing.Point(11, 30);
            this.btnCellInputIonizerOnOff.Name = "btnCellInputIonizerOnOff";
            this.btnCellInputIonizerOnOff.Size = new System.Drawing.Size(206, 63);
            this.btnCellInputIonizerOnOff.TabIndex = 456;
            this.btnCellInputIonizerOnOff.Text = "이오나이저\r\n온/오프";
            this.btnCellInputIonizerOnOff.UseVisualStyleBackColor = false;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.btnCellInputABlowerOnOff);
            this.panel11.Controls.Add(this.btnCellInputAVaccumOff);
            this.panel11.Controls.Add(this.btnCellInputAVaccumOn);
            this.panel11.Location = new System.Drawing.Point(7, 6);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(230, 173);
            this.panel11.TabIndex = 465;
            // 
            // label12
            // 
            this.label12.AutoEllipsis = true;
            this.label12.BackColor = System.Drawing.Color.Gainsboro;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(228, 24);
            this.label12.TabIndex = 9;
            this.label12.Text = "■ A열";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputABlowerOnOff
            // 
            this.btnCellInputABlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputABlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputABlowerOnOff.Location = new System.Drawing.Point(11, 99);
            this.btnCellInputABlowerOnOff.Name = "btnCellInputABlowerOnOff";
            this.btnCellInputABlowerOnOff.Size = new System.Drawing.Size(206, 63);
            this.btnCellInputABlowerOnOff.TabIndex = 58;
            this.btnCellInputABlowerOnOff.Text = "파기 온/오프";
            this.btnCellInputABlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputAVaccumOff
            // 
            this.btnCellInputAVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputAVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputAVaccumOff.Location = new System.Drawing.Point(117, 30);
            this.btnCellInputAVaccumOff.Name = "btnCellInputAVaccumOff";
            this.btnCellInputAVaccumOff.Size = new System.Drawing.Size(100, 63);
            this.btnCellInputAVaccumOff.TabIndex = 55;
            this.btnCellInputAVaccumOff.Text = "공압 오프";
            this.btnCellInputAVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputAVaccumOn
            // 
            this.btnCellInputAVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputAVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputAVaccumOn.Location = new System.Drawing.Point(11, 30);
            this.btnCellInputAVaccumOn.Name = "btnCellInputAVaccumOn";
            this.btnCellInputAVaccumOn.Size = new System.Drawing.Size(100, 63);
            this.btnCellInputAVaccumOn.TabIndex = 57;
            this.btnCellInputAVaccumOn.Text = "공압 온";
            this.btnCellInputAVaccumOn.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btnCellInputBBlowerOnOff);
            this.panel10.Controls.Add(this.label11);
            this.panel10.Controls.Add(this.btnCellInputBVaccumOn);
            this.panel10.Controls.Add(this.btnCellInputBVaccumOff);
            this.panel10.Location = new System.Drawing.Point(243, 6);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(230, 173);
            this.panel10.TabIndex = 466;
            // 
            // btnCellInputBBlowerOnOff
            // 
            this.btnCellInputBBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBBlowerOnOff.Location = new System.Drawing.Point(11, 99);
            this.btnCellInputBBlowerOnOff.Name = "btnCellInputBBlowerOnOff";
            this.btnCellInputBBlowerOnOff.Size = new System.Drawing.Size(206, 63);
            this.btnCellInputBBlowerOnOff.TabIndex = 58;
            this.btnCellInputBBlowerOnOff.Text = "파기 온/오프";
            this.btnCellInputBBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoEllipsis = true;
            this.label11.BackColor = System.Drawing.Color.Gainsboro;
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(228, 24);
            this.label11.TabIndex = 9;
            this.label11.Text = "■ B열";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputBVaccumOn
            // 
            this.btnCellInputBVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBVaccumOn.Location = new System.Drawing.Point(11, 30);
            this.btnCellInputBVaccumOn.Name = "btnCellInputBVaccumOn";
            this.btnCellInputBVaccumOn.Size = new System.Drawing.Size(100, 63);
            this.btnCellInputBVaccumOn.TabIndex = 57;
            this.btnCellInputBVaccumOn.Text = "공압 온";
            this.btnCellInputBVaccumOn.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBVaccumOff
            // 
            this.btnCellInputBVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBVaccumOff.Location = new System.Drawing.Point(117, 30);
            this.btnCellInputBVaccumOff.Name = "btnCellInputBVaccumOff";
            this.btnCellInputBVaccumOff.Size = new System.Drawing.Size(100, 63);
            this.btnCellInputBVaccumOff.TabIndex = 55;
            this.btnCellInputBVaccumOff.Text = "공압 오프";
            this.btnCellInputBVaccumOff.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.btnCellInputBufferBlowerOnOff);
            this.panel12.Controls.Add(this.label13);
            this.panel12.Controls.Add(this.btnCellInputBufferPickerDown);
            this.panel12.Controls.Add(this.btnCellInputBufferPickerUp);
            this.panel12.Controls.Add(this.btnCellInputBufferVaccumOff);
            this.panel12.Controls.Add(this.btnCellInputBufferVaccumOn);
            this.panel12.Location = new System.Drawing.Point(490, 7);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(230, 172);
            this.panel12.TabIndex = 467;
            // 
            // btnCellInputBufferBlowerOnOff
            // 
            this.btnCellInputBufferBlowerOnOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBufferBlowerOnOff.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputBufferBlowerOnOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBufferBlowerOnOff.Location = new System.Drawing.Point(9, 121);
            this.btnCellInputBufferBlowerOnOff.Name = "btnCellInputBufferBlowerOnOff";
            this.btnCellInputBufferBlowerOnOff.Size = new System.Drawing.Size(207, 40);
            this.btnCellInputBufferBlowerOnOff.TabIndex = 63;
            this.btnCellInputBufferBlowerOnOff.Text = "파기 온/오프";
            this.btnCellInputBufferBlowerOnOff.UseVisualStyleBackColor = false;
            // 
            // label13
            // 
            this.label13.AutoEllipsis = true;
            this.label13.BackColor = System.Drawing.Color.Gainsboro;
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(228, 24);
            this.label13.TabIndex = 9;
            this.label13.Text = "■ 버퍼";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCellInputBufferPickerDown
            // 
            this.btnCellInputBufferPickerDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBufferPickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputBufferPickerDown.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBufferPickerDown.Location = new System.Drawing.Point(116, 31);
            this.btnCellInputBufferPickerDown.Name = "btnCellInputBufferPickerDown";
            this.btnCellInputBufferPickerDown.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputBufferPickerDown.TabIndex = 60;
            this.btnCellInputBufferPickerDown.Text = "버퍼 피커\r\n다운";
            this.btnCellInputBufferPickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferPickerUp
            // 
            this.btnCellInputBufferPickerUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBufferPickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputBufferPickerUp.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBufferPickerUp.Location = new System.Drawing.Point(8, 31);
            this.btnCellInputBufferPickerUp.Name = "btnCellInputBufferPickerUp";
            this.btnCellInputBufferPickerUp.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputBufferPickerUp.TabIndex = 58;
            this.btnCellInputBufferPickerUp.Text = "버퍼 피커\r\n업";
            this.btnCellInputBufferPickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferVaccumOff
            // 
            this.btnCellInputBufferVaccumOff.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBufferVaccumOff.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputBufferVaccumOff.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBufferVaccumOff.Location = new System.Drawing.Point(116, 77);
            this.btnCellInputBufferVaccumOff.Name = "btnCellInputBufferVaccumOff";
            this.btnCellInputBufferVaccumOff.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputBufferVaccumOff.TabIndex = 61;
            this.btnCellInputBufferVaccumOff.Text = "공압 오프";
            this.btnCellInputBufferVaccumOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferVaccumOn
            // 
            this.btnCellInputBufferVaccumOn.BackColor = System.Drawing.SystemColors.Control;
            this.btnCellInputBufferVaccumOn.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputBufferVaccumOn.ForeColor = System.Drawing.Color.Black;
            this.btnCellInputBufferVaccumOn.Location = new System.Drawing.Point(8, 75);
            this.btnCellInputBufferVaccumOn.Name = "btnCellInputBufferVaccumOn";
            this.btnCellInputBufferVaccumOn.Size = new System.Drawing.Size(100, 40);
            this.btnCellInputBufferVaccumOn.TabIndex = 59;
            this.btnCellInputBufferVaccumOn.Text = "공압 온";
            this.btnCellInputBufferVaccumOn.UseVisualStyleBackColor = false;
            // 
            // tabPage14
            // 
            this.tabPage14.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage14.Controls.Add(this.panel60);
            this.tabPage14.Controls.Add(this.panel68);
            this.tabPage14.Controls.Add(this.panel69);
            this.tabPage14.Controls.Add(this.panel70);
            this.tabPage14.Location = new System.Drawing.Point(4, 34);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(969, 397);
            this.tabPage14.TabIndex = 10;
            this.tabPage14.Text = "카셋트 언로드";
            // 
            // panel60
            // 
            this.panel60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel60.Controls.Add(this.panel17);
            this.panel60.Controls.Add(this.panel45);
            this.panel60.Controls.Add(this.label64);
            this.panel60.Location = new System.Drawing.Point(7, 188);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(956, 201);
            this.panel60.TabIndex = 479;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.btnCstunloadBCellOut);
            this.panel17.Controls.Add(this.btnCstunloadBMeasure);
            this.panel17.Controls.Add(this.btnCstunloadBLoader);
            this.panel17.Controls.Add(this.btnCstunloadB90dgr);
            this.panel17.Controls.Add(this.btnCstunloadB0dgr);
            this.panel17.Controls.Add(this.label16);
            this.panel17.Location = new System.Drawing.Point(356, 31);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(342, 160);
            this.panel17.TabIndex = 458;
            // 
            // btnCstunloadBCellOut
            // 
            this.btnCstunloadBCellOut.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadBCellOut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadBCellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadBCellOut.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadBCellOut.Location = new System.Drawing.Point(228, 96);
            this.btnCstunloadBCellOut.Name = "btnCstunloadBCellOut";
            this.btnCstunloadBCellOut.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadBCellOut.TabIndex = 64;
            this.btnCstunloadBCellOut.Text = "셀 배출";
            this.btnCstunloadBCellOut.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadBMeasure
            // 
            this.btnCstunloadBMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadBMeasure.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadBMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadBMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadBMeasure.Location = new System.Drawing.Point(118, 96);
            this.btnCstunloadBMeasure.Name = "btnCstunloadBMeasure";
            this.btnCstunloadBMeasure.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadBMeasure.TabIndex = 62;
            this.btnCstunloadBMeasure.Text = "측정";
            this.btnCstunloadBMeasure.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadBLoader
            // 
            this.btnCstunloadBLoader.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadBLoader.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadBLoader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadBLoader.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadBLoader.Location = new System.Drawing.Point(118, 33);
            this.btnCstunloadBLoader.Name = "btnCstunloadBLoader";
            this.btnCstunloadBLoader.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadBLoader.TabIndex = 57;
            this.btnCstunloadBLoader.Text = "카세트 로더";
            this.btnCstunloadBLoader.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadB90dgr
            // 
            this.btnCstunloadB90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadB90dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadB90dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadB90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadB90dgr.Location = new System.Drawing.Point(9, 96);
            this.btnCstunloadB90dgr.Name = "btnCstunloadB90dgr";
            this.btnCstunloadB90dgr.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadB90dgr.TabIndex = 55;
            this.btnCstunloadB90dgr.Text = "카세트 90도";
            this.btnCstunloadB90dgr.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadB0dgr
            // 
            this.btnCstunloadB0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadB0dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadB0dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadB0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadB0dgr.Location = new System.Drawing.Point(9, 33);
            this.btnCstunloadB0dgr.Name = "btnCstunloadB0dgr";
            this.btnCstunloadB0dgr.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadB0dgr.TabIndex = 54;
            this.btnCstunloadB0dgr.Text = "카세트 0도";
            this.btnCstunloadB0dgr.UseVisualStyleBackColor = false;
            // 
            // label16
            // 
            this.label16.AutoEllipsis = true;
            this.label16.BackColor = System.Drawing.Color.Gainsboro;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(340, 24);
            this.label16.TabIndex = 9;
            this.label16.Text = "■ B열";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel45
            // 
            this.panel45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel45.Controls.Add(this.btnCstunloadAMeasure);
            this.panel45.Controls.Add(this.btnCstunloadALoader);
            this.panel45.Controls.Add(this.btnCstunloadA0dgr);
            this.panel45.Controls.Add(this.btnCstunloadA90dgr);
            this.panel45.Controls.Add(this.btnCstunloadACellOut);
            this.panel45.Controls.Add(this.label44);
            this.panel45.Location = new System.Drawing.Point(8, 31);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(342, 160);
            this.panel45.TabIndex = 457;
            // 
            // btnCstunloadAMeasure
            // 
            this.btnCstunloadAMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadAMeasure.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadAMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadAMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadAMeasure.Location = new System.Drawing.Point(118, 96);
            this.btnCstunloadAMeasure.Name = "btnCstunloadAMeasure";
            this.btnCstunloadAMeasure.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadAMeasure.TabIndex = 54;
            this.btnCstunloadAMeasure.Text = "측정";
            this.btnCstunloadAMeasure.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadALoader
            // 
            this.btnCstunloadALoader.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadALoader.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadALoader.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadALoader.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadALoader.Location = new System.Drawing.Point(118, 33);
            this.btnCstunloadALoader.Name = "btnCstunloadALoader";
            this.btnCstunloadALoader.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadALoader.TabIndex = 53;
            this.btnCstunloadALoader.Text = "카세트 로더";
            this.btnCstunloadALoader.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadA0dgr
            // 
            this.btnCstunloadA0dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadA0dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadA0dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadA0dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadA0dgr.Location = new System.Drawing.Point(9, 33);
            this.btnCstunloadA0dgr.Name = "btnCstunloadA0dgr";
            this.btnCstunloadA0dgr.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadA0dgr.TabIndex = 44;
            this.btnCstunloadA0dgr.Text = "카세트 0 도";
            this.btnCstunloadA0dgr.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadA90dgr
            // 
            this.btnCstunloadA90dgr.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadA90dgr.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadA90dgr.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadA90dgr.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadA90dgr.Location = new System.Drawing.Point(9, 96);
            this.btnCstunloadA90dgr.Name = "btnCstunloadA90dgr";
            this.btnCstunloadA90dgr.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadA90dgr.TabIndex = 45;
            this.btnCstunloadA90dgr.Text = "카세트 90도";
            this.btnCstunloadA90dgr.UseVisualStyleBackColor = false;
            // 
            // btnCstunloadACellOut
            // 
            this.btnCstunloadACellOut.BackColor = System.Drawing.SystemColors.Control;
            this.btnCstunloadACellOut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCstunloadACellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCstunloadACellOut.ForeColor = System.Drawing.Color.Black;
            this.btnCstunloadACellOut.Location = new System.Drawing.Point(227, 96);
            this.btnCstunloadACellOut.Name = "btnCstunloadACellOut";
            this.btnCstunloadACellOut.Size = new System.Drawing.Size(103, 55);
            this.btnCstunloadACellOut.TabIndex = 52;
            this.btnCstunloadACellOut.Text = "셀 배출";
            this.btnCstunloadACellOut.UseVisualStyleBackColor = false;
            // 
            // label44
            // 
            this.label44.AutoEllipsis = true;
            this.label44.BackColor = System.Drawing.Color.Gainsboro;
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(0, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(340, 24);
            this.label44.TabIndex = 9;
            this.label44.Text = "■ A열";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label64
            // 
            this.label64.AutoEllipsis = true;
            this.label64.BackColor = System.Drawing.Color.Gainsboro;
            this.label64.Dock = System.Windows.Forms.DockStyle.Top;
            this.label64.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(0, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(954, 24);
            this.label64.TabIndex = 9;
            this.label64.Text = "■ 이동";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel68
            // 
            this.panel68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel68.Controls.Add(this.label68);
            this.panel68.Controls.Add(this.btnCasseteUnloadATopCstGrib);
            this.panel68.Controls.Add(this.btnCasseteUnloadATopCstUngrib);
            this.panel68.Controls.Add(this.btnCasseteUnloadALiftTiltSlinderDown);
            this.panel68.Controls.Add(this.btnCasseteUnloadALiftTiltSlinderUp);
            this.panel68.Location = new System.Drawing.Point(7, 7);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(210, 175);
            this.panel68.TabIndex = 478;
            // 
            // label68
            // 
            this.label68.AutoEllipsis = true;
            this.label68.BackColor = System.Drawing.Color.Gainsboro;
            this.label68.Dock = System.Windows.Forms.DockStyle.Top;
            this.label68.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(0, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(208, 24);
            this.label68.TabIndex = 9;
            this.label68.Text = "■ A열";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCasseteUnloadATopCstGrib
            // 
            this.btnCasseteUnloadATopCstGrib.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadATopCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadATopCstGrib.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadATopCstGrib.Location = new System.Drawing.Point(9, 34);
            this.btnCasseteUnloadATopCstGrib.Name = "btnCasseteUnloadATopCstGrib";
            this.btnCasseteUnloadATopCstGrib.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadATopCstGrib.TabIndex = 54;
            this.btnCasseteUnloadATopCstGrib.Text = "상단 카세트\r\n그립";
            this.btnCasseteUnloadATopCstGrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadATopCstUngrib
            // 
            this.btnCasseteUnloadATopCstUngrib.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadATopCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadATopCstUngrib.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadATopCstUngrib.Location = new System.Drawing.Point(108, 34);
            this.btnCasseteUnloadATopCstUngrib.Name = "btnCasseteUnloadATopCstUngrib";
            this.btnCasseteUnloadATopCstUngrib.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadATopCstUngrib.TabIndex = 55;
            this.btnCasseteUnloadATopCstUngrib.Text = "상단 카세트\r\n언그립";
            this.btnCasseteUnloadATopCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadALiftTiltSlinderDown
            // 
            this.btnCasseteUnloadALiftTiltSlinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadALiftTiltSlinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadALiftTiltSlinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadALiftTiltSlinderDown.Location = new System.Drawing.Point(108, 100);
            this.btnCasseteUnloadALiftTiltSlinderDown.Name = "btnCasseteUnloadALiftTiltSlinderDown";
            this.btnCasseteUnloadALiftTiltSlinderDown.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadALiftTiltSlinderDown.TabIndex = 61;
            this.btnCasseteUnloadALiftTiltSlinderDown.Text = "리프트 틸트\r\n실린더 다운";
            this.btnCasseteUnloadALiftTiltSlinderDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadALiftTiltSlinderUp
            // 
            this.btnCasseteUnloadALiftTiltSlinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadALiftTiltSlinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadALiftTiltSlinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadALiftTiltSlinderUp.Location = new System.Drawing.Point(9, 100);
            this.btnCasseteUnloadALiftTiltSlinderUp.Name = "btnCasseteUnloadALiftTiltSlinderUp";
            this.btnCasseteUnloadALiftTiltSlinderUp.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadALiftTiltSlinderUp.TabIndex = 60;
            this.btnCasseteUnloadALiftTiltSlinderUp.Text = "리프트 틸트\r\n실린더 업";
            this.btnCasseteUnloadALiftTiltSlinderUp.UseVisualStyleBackColor = false;
            // 
            // panel69
            // 
            this.panel69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel69.Controls.Add(this.label69);
            this.panel69.Controls.Add(this.btnCasseteUnloadBTopCstGrib);
            this.panel69.Controls.Add(this.btnCasseteUnloadBTopCstUngrib);
            this.panel69.Controls.Add(this.btnCasseteUnloadBLiftTiltSlinderDown);
            this.panel69.Controls.Add(this.btnCasseteUnloadBLiftTiltSlinderUp);
            this.panel69.Location = new System.Drawing.Point(223, 7);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(210, 175);
            this.panel69.TabIndex = 477;
            // 
            // label69
            // 
            this.label69.AutoEllipsis = true;
            this.label69.BackColor = System.Drawing.Color.Gainsboro;
            this.label69.Dock = System.Windows.Forms.DockStyle.Top;
            this.label69.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(0, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(208, 24);
            this.label69.TabIndex = 9;
            this.label69.Text = "■ B열";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCasseteUnloadBTopCstGrib
            // 
            this.btnCasseteUnloadBTopCstGrib.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadBTopCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadBTopCstGrib.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadBTopCstGrib.Location = new System.Drawing.Point(9, 34);
            this.btnCasseteUnloadBTopCstGrib.Name = "btnCasseteUnloadBTopCstGrib";
            this.btnCasseteUnloadBTopCstGrib.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadBTopCstGrib.TabIndex = 54;
            this.btnCasseteUnloadBTopCstGrib.Text = "상단 카세트\r\n그립";
            this.btnCasseteUnloadBTopCstGrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBTopCstUngrib
            // 
            this.btnCasseteUnloadBTopCstUngrib.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadBTopCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadBTopCstUngrib.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadBTopCstUngrib.Location = new System.Drawing.Point(108, 34);
            this.btnCasseteUnloadBTopCstUngrib.Name = "btnCasseteUnloadBTopCstUngrib";
            this.btnCasseteUnloadBTopCstUngrib.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadBTopCstUngrib.TabIndex = 55;
            this.btnCasseteUnloadBTopCstUngrib.Text = "상단 카세트\r\n언그립";
            this.btnCasseteUnloadBTopCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBLiftTiltSlinderDown
            // 
            this.btnCasseteUnloadBLiftTiltSlinderDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadBLiftTiltSlinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadBLiftTiltSlinderDown.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadBLiftTiltSlinderDown.Location = new System.Drawing.Point(108, 100);
            this.btnCasseteUnloadBLiftTiltSlinderDown.Name = "btnCasseteUnloadBLiftTiltSlinderDown";
            this.btnCasseteUnloadBLiftTiltSlinderDown.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadBLiftTiltSlinderDown.TabIndex = 61;
            this.btnCasseteUnloadBLiftTiltSlinderDown.Text = "리프트 틸트\r\n실린더 다운";
            this.btnCasseteUnloadBLiftTiltSlinderDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBLiftTiltSlinderUp
            // 
            this.btnCasseteUnloadBLiftTiltSlinderUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnCasseteUnloadBLiftTiltSlinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadBLiftTiltSlinderUp.ForeColor = System.Drawing.Color.Black;
            this.btnCasseteUnloadBLiftTiltSlinderUp.Location = new System.Drawing.Point(9, 100);
            this.btnCasseteUnloadBLiftTiltSlinderUp.Name = "btnCasseteUnloadBLiftTiltSlinderUp";
            this.btnCasseteUnloadBLiftTiltSlinderUp.Size = new System.Drawing.Size(93, 60);
            this.btnCasseteUnloadBLiftTiltSlinderUp.TabIndex = 60;
            this.btnCasseteUnloadBLiftTiltSlinderUp.Text = "리프트 틸트\r\n실린더 업";
            this.btnCasseteUnloadBLiftTiltSlinderUp.UseVisualStyleBackColor = false;
            // 
            // panel70
            // 
            this.panel70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel70.Controls.Add(this.btn_CasseteUnload_Muting4);
            this.panel70.Controls.Add(this.label70);
            this.panel70.Controls.Add(this.btn_CasseteUnload_Muting2);
            this.panel70.Controls.Add(this.btn_CasseteUnload_Muting3);
            this.panel70.Controls.Add(this.btn_CasseteUnload_MutingIn);
            this.panel70.Controls.Add(this.btn_CasseteUnload_Muting1);
            this.panel70.Controls.Add(this.btn_CasseteUnload_MutingOut);
            this.panel70.Location = new System.Drawing.Point(754, 6);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(190, 175);
            this.panel70.TabIndex = 476;
            // 
            // btn_CasseteUnload_Muting4
            // 
            this.btn_CasseteUnload_Muting4.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteUnload_Muting4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_Muting4.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteUnload_Muting4.Location = new System.Drawing.Point(97, 127);
            this.btn_CasseteUnload_Muting4.Name = "btn_CasseteUnload_Muting4";
            this.btn_CasseteUnload_Muting4.Size = new System.Drawing.Size(83, 44);
            this.btn_CasseteUnload_Muting4.TabIndex = 461;
            this.btn_CasseteUnload_Muting4.Text = "뮤팅 B 배출";
            this.btn_CasseteUnload_Muting4.UseVisualStyleBackColor = false;
            // 
            // label70
            // 
            this.label70.AutoEllipsis = true;
            this.label70.BackColor = System.Drawing.Color.Gainsboro;
            this.label70.Dock = System.Windows.Forms.DockStyle.Top;
            this.label70.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(0, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(188, 24);
            this.label70.TabIndex = 9;
            this.label70.Text = "■ 뮤팅";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_CasseteUnload_Muting2
            // 
            this.btn_CasseteUnload_Muting2.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteUnload_Muting2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_Muting2.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteUnload_Muting2.Location = new System.Drawing.Point(8, 127);
            this.btn_CasseteUnload_Muting2.Name = "btn_CasseteUnload_Muting2";
            this.btn_CasseteUnload_Muting2.Size = new System.Drawing.Size(83, 44);
            this.btn_CasseteUnload_Muting2.TabIndex = 460;
            this.btn_CasseteUnload_Muting2.Text = "뮤팅 B 투입";
            this.btn_CasseteUnload_Muting2.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting3
            // 
            this.btn_CasseteUnload_Muting3.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteUnload_Muting3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_Muting3.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteUnload_Muting3.Location = new System.Drawing.Point(97, 77);
            this.btn_CasseteUnload_Muting3.Name = "btn_CasseteUnload_Muting3";
            this.btn_CasseteUnload_Muting3.Size = new System.Drawing.Size(83, 44);
            this.btn_CasseteUnload_Muting3.TabIndex = 459;
            this.btn_CasseteUnload_Muting3.Text = "뮤팅 A 배출";
            this.btn_CasseteUnload_Muting3.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MutingIn
            // 
            this.btn_CasseteUnload_MutingIn.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteUnload_MutingIn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MutingIn.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteUnload_MutingIn.Location = new System.Drawing.Point(8, 27);
            this.btn_CasseteUnload_MutingIn.Name = "btn_CasseteUnload_MutingIn";
            this.btn_CasseteUnload_MutingIn.Size = new System.Drawing.Size(83, 44);
            this.btn_CasseteUnload_MutingIn.TabIndex = 456;
            this.btn_CasseteUnload_MutingIn.Text = "뮤팅 인";
            this.btn_CasseteUnload_MutingIn.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting1
            // 
            this.btn_CasseteUnload_Muting1.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteUnload_Muting1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_Muting1.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteUnload_Muting1.Location = new System.Drawing.Point(8, 77);
            this.btn_CasseteUnload_Muting1.Name = "btn_CasseteUnload_Muting1";
            this.btn_CasseteUnload_Muting1.Size = new System.Drawing.Size(83, 44);
            this.btn_CasseteUnload_Muting1.TabIndex = 458;
            this.btn_CasseteUnload_Muting1.Text = "뮤팅 A 투입";
            this.btn_CasseteUnload_Muting1.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MutingOut
            // 
            this.btn_CasseteUnload_MutingOut.BackColor = System.Drawing.SystemColors.Control;
            this.btn_CasseteUnload_MutingOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MutingOut.ForeColor = System.Drawing.Color.Black;
            this.btn_CasseteUnload_MutingOut.Location = new System.Drawing.Point(97, 27);
            this.btn_CasseteUnload_MutingOut.Name = "btn_CasseteUnload_MutingOut";
            this.btn_CasseteUnload_MutingOut.Size = new System.Drawing.Size(83, 44);
            this.btn_CasseteUnload_MutingOut.TabIndex = 457;
            this.btn_CasseteUnload_MutingOut.Text = "뮤팅 아웃";
            this.btn_CasseteUnload_MutingOut.UseVisualStyleBackColor = false;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.label10);
            this.panel14.Controls.Add(this.txtSelectedServoNo);
            this.panel14.Controls.Add(this.lbl_CellInput_CurrentLocation);
            this.panel14.Controls.Add(this.lbl_CellInput_SelectedShaft);
            this.panel14.Controls.Add(this.lbl_CellInput_Speed);
            this.panel14.Controls.Add(this.lbl_CellInput_Location);
            this.panel14.Controls.Add(this.txtSelectedServoNoCurrPosi);
            this.panel14.Controls.Add(this.txtSelectedServoNoSetSpeed);
            this.panel14.Controls.Add(this.txtSelectedServoNoSetPosi);
            this.panel14.Controls.Add(this.btnCellInputMoveLocation);
            this.panel14.Controls.Add(this.btnCellInputSpeedSetting);
            this.panel14.Controls.Add(this.btnCellInputLocationSetting);
            this.panel14.Controls.Add(this.btnCellInputAllSetting);
            this.panel14.Location = new System.Drawing.Point(751, 249);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(975, 127);
            this.panel14.TabIndex = 454;
            // 
            // label10
            // 
            this.label10.AutoEllipsis = true;
            this.label10.BackColor = System.Drawing.Color.Gainsboro;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(973, 24);
            this.label10.TabIndex = 9;
            this.label10.Text = "   ■ 위치값 설정";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ucrlServoCtrl
            // 
            this.ucrlServoCtrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlServoCtrl.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ucrlServoCtrl.Location = new System.Drawing.Point(751, 67);
            this.ucrlServoCtrl.Name = "ucrlServoCtrl";
            this.ucrlServoCtrl.Servo = null;
            this.ucrlServoCtrl.Size = new System.Drawing.Size(853, 169);
            this.ucrlServoCtrl.TabIndex = 31;
            // 
            // UcrlParameterServoPositionSetting
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnCasseteLoadSave);
            this.Controls.Add(this.dgvSevoPosi);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.ucrlServoCtrl);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlParameterServoPositionSetting";
            this.Size = new System.Drawing.Size(1740, 875);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSevoPosi)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel64.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel63.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel84.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel83.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.panel71.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel56.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            this.panel76.ResumeLayout(false);
            this.panel77.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.panel72.ResumeLayout(false);
            this.panel73.ResumeLayout(false);
            this.panel75.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.panel79.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel81.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel82.ResumeLayout(false);
            this.panel78.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.tabPage13.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.panel60.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel68.ResumeLayout(false);
            this.panel69.ResumeLayout(false);
            this.panel70.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private UcrlAjinServoCtrl ucrlServoCtrl;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnCstLoad;
        private System.Windows.Forms.Button btnCellOutTransfer;
        private System.Windows.Forms.Button btnCellLoadTransfer01;
        private System.Windows.Forms.Button btnIRCutProcess;
        private System.Windows.Forms.Button btnAfterIRCutTransfer;
        private System.Windows.Forms.Button btnBreakUnit01;
        private System.Windows.Forms.Button btnBreakUnit02;
        private System.Windows.Forms.Button btnBeforeTransfer;
        private System.Windows.Forms.Button btnCameraUnit;
        private System.Windows.Forms.Button btnCellInTransfer;
        private System.Windows.Forms.Button btnCstUnloader;
        private System.Windows.Forms.Button btnBreakHeadUnitXZ;
        private System.Windows.Forms.Button btnCellInputSpeedSetting;
        private System.Windows.Forms.Button btnCellInputAllSetting;
        private System.Windows.Forms.Button btnCellInputLocationSetting;
        private System.Windows.Forms.Button btnCellInputMoveLocation;
        private System.Windows.Forms.TextBox txtSelectedServoNoSetPosi;
        private System.Windows.Forms.TextBox txtSelectedServoNoSetSpeed;
        private System.Windows.Forms.TextBox txtSelectedServoNoCurrPosi;
        private System.Windows.Forms.TextBox txtSelectedServoNo;
        private System.Windows.Forms.Label lbl_CellInput_Location;
        private System.Windows.Forms.Label lbl_CellInput_Speed;
        private System.Windows.Forms.Label lbl_CellInput_SelectedShaft;
        private System.Windows.Forms.Label lbl_CellInput_CurrentLocation;
        private System.Windows.Forms.DataGridView dgvSevoPosi;
        private System.Windows.Forms.Button btnCasseteLoadSave;
        private System.Windows.Forms.Button btnLDCstBGripUn;
        private System.Windows.Forms.Button btnLDCstBGrip;
        private System.Windows.Forms.Button btnLDCstAGripUn;
        private System.Windows.Forms.Button btnLDCstAGrip;
        private System.Windows.Forms.Button btnCstloadBMeasure;
        private System.Windows.Forms.Button btnCstloadBLoader;
        private System.Windows.Forms.Button btnCstloadB90dgr;
        private System.Windows.Forms.Button btnCstloadB0dgr;
        private System.Windows.Forms.Button btnLoaderAxisX2BlowerOnOff;
        private System.Windows.Forms.Button btnLoaderAxisX2VaccumOff;
        private System.Windows.Forms.Button btnLoaderAxisX2VaccumOn;
        private System.Windows.Forms.Button btnLoaderAxisX1BlowerOnOff;
        private System.Windows.Forms.Button btnLoaderAxisX1VaccumOff;
        private System.Windows.Forms.Button btnLoaderAxisX1VaccumOn;
        private System.Windows.Forms.Button btnCellLoadX3LD;
        private System.Windows.Forms.Button btnCellLoadX2LD;
        private System.Windows.Forms.Button btnCellLoadX1LD;
        private System.Windows.Forms.Button btnCellLoadBMcrReading;
        private System.Windows.Forms.Button btnCellLoadBVisionAlign;
        private System.Windows.Forms.Button btnCellLoadAMcrReading;
        private System.Windows.Forms.Button btnCellLoadAVisionAlign;
        private System.Windows.Forms.Button btnCellLoadB1PickerBlowerOnOff;
        private System.Windows.Forms.Button btnCellLoadB1PickerUp;
        private System.Windows.Forms.Button btnCellLoadB1PickerDown;
        private System.Windows.Forms.Button btnCellLoadB1PickerVaccumOn;
        private System.Windows.Forms.Button btnCellLoadB1PickerVaccumOff;
        private System.Windows.Forms.Button btnCellLoadA1PickerBlowerOnOff;
        private System.Windows.Forms.Button btnCellLoadA1PickerUp;
        private System.Windows.Forms.Button btnCellLoadA1PickerDown;
        private System.Windows.Forms.Button btnCellLoadA1PickerVaccumOn;
        private System.Windows.Forms.Button btnBreakTRBULD;
        private System.Windows.Forms.Button btnBreakTRBLD;
        private System.Windows.Forms.Button btnBreakTRAULD;
        private System.Windows.Forms.Button btnBreakTRALD;
        private System.Windows.Forms.Button btnBreakTransferB1PickerBlowerOnOff;
        private System.Windows.Forms.Button btnBreakTransferB1PickerUp;
        private System.Windows.Forms.Button btnBreakTransferB1PickerDown;
        private System.Windows.Forms.Button btnBreakTransferB1PickerVaccumOn;
        private System.Windows.Forms.Button btnBreakTransferB1PickerVaccumOff;
        private System.Windows.Forms.Button btnBreakTransferA1PickerBlowerOnOff;
        private System.Windows.Forms.Button btnBreakTransferA1PickerUp;
        private System.Windows.Forms.Button btnBreakTransferA1PickerDown;
        private System.Windows.Forms.Button btnBreakTransferA1PickerVaccumOn;
        private System.Windows.Forms.Button btnBreakTransferA1PickerVaccumOff;
        private System.Windows.Forms.Button btnIRCutProcessB1LaserShot;
        private System.Windows.Forms.Button btnIRCutProcessB2LaserShot;
        private System.Windows.Forms.Button btnIRCutProcessA1LaserShot;
        private System.Windows.Forms.Button btnIRCutProcessA2LaserShot;
        private System.Windows.Forms.Button btnIRCutProcessAFineAlign;
        private System.Windows.Forms.Button btnIRCutProcessBFineAlign;
        private System.Windows.Forms.Button btnIRCutProcessBCamera;
        private System.Windows.Forms.Button btnIRCutProcessBCellLoad;
        private System.Windows.Forms.Button btnIRCutProcessBCellUnload;
        private System.Windows.Forms.Button btnIRCutProcessBLaser;
        private System.Windows.Forms.Button btnIRCutProcessACamera;
        private System.Windows.Forms.Button btnIRCutProcessACellLoad;
        private System.Windows.Forms.Button btnIRCutProcessACellUnload;
        private System.Windows.Forms.Button btnIRCutProcessALaser;
        private System.Windows.Forms.Button btnIRCutProcessB1VaccumCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessB2VaccumCh1On;
        private System.Windows.Forms.Button btnIRCutProcessB2VaccumCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessB1VaccumCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessB2VaccumCh2On;
        private System.Windows.Forms.Button btnIRCutProcessB2VaccumCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessB1VaccumCh2On;
        private System.Windows.Forms.Button btnIRCutProcessB1VaccumCh1On;
        private System.Windows.Forms.Button btnIRCutProcessB1BlowerCh1OnOff;
        private System.Windows.Forms.Button btnIRCutProcessB2BlowerCh1OnOff;
        private System.Windows.Forms.Button btnIRCutProcessB1BlowerCh2OnOff;
        private System.Windows.Forms.Button btnIRCutProcessB2BlowerCh2OnOff;
        private System.Windows.Forms.Button btnIRCutProcessA1BlowerCh2OnOff;
        private System.Windows.Forms.Button btnIRCutProcessA2BlowerCh2OnOff;
        private System.Windows.Forms.Button btnIRCutProcessA1VaccumCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessA2VaccumCh1On;
        private System.Windows.Forms.Button btnIRCutProcessA2VaccumCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessA1VaccumCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessA2VaccumCh2On;
        private System.Windows.Forms.Button btnIRCutProcessA2VaccumCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessA1VaccumCh2On;
        private System.Windows.Forms.Button btnIRCutProcessA1VaccumCh1On;
        private System.Windows.Forms.Button btnIRCutProcessA1BlowerCh1OnOff;
        private System.Windows.Forms.Button btnIRCutProcessA2BlowerCh1OnOff;
        private System.Windows.Forms.Button btnBreakZ1Hold;
        private System.Windows.Forms.Button btnBreakZ1BreakingPos;
        private System.Windows.Forms.Button btnBreakXZAAlignMeasure;
        private System.Windows.Forms.Button btnBreakXZBAlignMeasure;
        private System.Windows.Forms.Button btnBreakUnitTYShutterClose;
        private System.Windows.Forms.Button btnBreakUnitTYShutterOpen;
        private System.Windows.Forms.Button btnBreakUnitTYDummyBoxOutput;
        private System.Windows.Forms.Button btnBreakUnitTYDummyBoxInput;
        private System.Windows.Forms.Button btnBreakUnitTYAULD;
        private System.Windows.Forms.Button btnBreakUnitTYALD;
        private System.Windows.Forms.Button btnBreakUnitTYB1BlowerOnOff;
        private System.Windows.Forms.Button btnBreakUnitTYB2BlowerOnOff;
        private System.Windows.Forms.Button btnBreakUnitTYB1VaccumOff;
        private System.Windows.Forms.Button btnBreakUnitTYB1VaccumOn;
        private System.Windows.Forms.Button btnBreakUnitTYB2VaccumOn;
        private System.Windows.Forms.Button btnBreakUnitTYB2VaccumOff;
        private System.Windows.Forms.Button btnBreakUnitTYA1BlowerOnOff;
        private System.Windows.Forms.Button btnBreakUnitTYA2BlowerOnOff;
        private System.Windows.Forms.Button btnBreakUnitTYA1VaccumOff;
        private System.Windows.Forms.Button btnBreakUnitTYA1VaccumOn;
        private System.Windows.Forms.Button btnBreakUnitTYA2VaccumOn;
        private System.Windows.Forms.Button btnBreakUnitTYA2VaccumOff;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting4;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting2;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting3;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting1;
        private System.Windows.Forms.Button btn_CasseteLoad_MutingOff;
        private System.Windows.Forms.Button btn_CasseteLoad_MutingIn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.Panel panel14;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel9;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel18;
        internal System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel20;
        internal System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnBeforeB2PikerBlowerOnOff;
        private System.Windows.Forms.Button btnBeforeB2PikerDown;
        private System.Windows.Forms.Button btnBeforeB1PikerUp;
        private System.Windows.Forms.Button btnBeforeB1PikerBlowerOnOff;
        private System.Windows.Forms.Button btnBeforeB1PikerDown;
        private System.Windows.Forms.Button btnBeforeB2PikerVaccumOff;
        private System.Windows.Forms.Button btnBeforeB2PikerUp;
        private System.Windows.Forms.Button btnBeforeB2PikerVaccumOn;
        private System.Windows.Forms.Button btnBeforeB1PikerVaccumOn;
        private System.Windows.Forms.Button btnBeforeB1PikerVaccumOff;
        private System.Windows.Forms.Panel panel22;
        internal System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnBeforeA2PikerBlowerOnOff;
        private System.Windows.Forms.Button btnBeforeA2PikerDown;
        private System.Windows.Forms.Button btnBeforeA1PikerUp;
        private System.Windows.Forms.Button btnBeforeA1PikerBlowerOnOff;
        private System.Windows.Forms.Button btnBeforeA1PikerDown;
        private System.Windows.Forms.Button btnBeforeA2PikerVaccumOff;
        private System.Windows.Forms.Button btnBeforeA2PikerUp;
        private System.Windows.Forms.Button btnBeforeA2PikerVaccumOn;
        private System.Windows.Forms.Button btnBeforeA1PikerVaccumOn;
        private System.Windows.Forms.Button btnBeforeA1PikerVaccumOff;
        private System.Windows.Forms.Panel panel26;
        internal System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel27;
        internal System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel28;
        internal System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel29;
        internal System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel30;
        internal System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel36;
        internal System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel34;
        internal System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel37;
        internal System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel35;
        internal System.Windows.Forms.Label label34;
        internal System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel31;
        internal System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel32;
        internal System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel41;
        internal System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel43;
        internal System.Windows.Forms.Label label42;
        internal System.Windows.Forms.Label label43;
        private System.Windows.Forms.Panel panel38;
        internal System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel39;
        internal System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnCamUnitAInspUpStart;
        internal System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnCamUnitAInspLowStart;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel44;
        internal System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel panel47;
        internal System.Windows.Forms.Label label47;
        internal System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel52;
        internal System.Windows.Forms.Label label52;
        private System.Windows.Forms.Panel panel50;
        internal System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panel51;
        internal System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel66;
        internal System.Windows.Forms.Label label65;
        internal System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel panel62;
        internal System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel panel63;
        internal System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnLoaderY1AxisCstAUnloading;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnLoaderY1AxisCstBInner;
        private System.Windows.Forms.Button btnLoaderY1AxisCstBUnloading;
        private System.Windows.Forms.Button btnLoaderY1AxisCstAInner;
        private System.Windows.Forms.Panel panel7;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnLoaderAxisX2Cst_A_Wait;
        private System.Windows.Forms.Button btnLoaderAxisX2Cst_A_Middle;
        private System.Windows.Forms.Panel panel6;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnLoaderAxisX1Cst_A_Wait;
        private System.Windows.Forms.Button btnLoaderAxisX1Cst_A_Middle;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Button btnBreakUnitTYMeasure2;
        private System.Windows.Forms.Button btnBreakUnitTYHold2;
        private System.Windows.Forms.Button btnBreakUnitTYMeasure1;
        private System.Windows.Forms.Button btnBreakUnitTYHold1;
        internal System.Windows.Forms.Label label57;
        private System.Windows.Forms.Panel panel55;
        internal System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel56;
        internal System.Windows.Forms.Label label55;
        internal System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panel53;
        internal System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel58;
        internal System.Windows.Forms.Label label60;
        private System.Windows.Forms.Panel panel4;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnCellInputBufferBlowerOnOff;
        internal System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnCellInputBufferPickerDown;
        private System.Windows.Forms.Button btnCellInputBufferPickerUp;
        private System.Windows.Forms.Button btnCellInputBufferVaccumOff;
        private System.Windows.Forms.Button btnCellInputBufferVaccumOn;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnCellInputBBlowerOnOff;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCellInputBVaccumOn;
        private System.Windows.Forms.Button btnCellInputBVaccumOff;
        private System.Windows.Forms.Panel panel11;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnCellInputABlowerOnOff;
        private System.Windows.Forms.Button btnCellInputAVaccumOff;
        private System.Windows.Forms.Button btnCellInputAVaccumOn;
        private System.Windows.Forms.Panel panel13;
        internal System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Button btnCstloadA0dgr;
        private System.Windows.Forms.Button btnCstloadA90dgr;
        private System.Windows.Forms.Button btnCstloadACellOut;
        internal System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel60;
        internal System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel68;
        internal System.Windows.Forms.Label label68;
        private System.Windows.Forms.Panel panel69;
        internal System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel panel70;
        internal System.Windows.Forms.Label label70;
        private System.Windows.Forms.Button btnCasseteUnloadBLiftTiltSlinderDown;
        private System.Windows.Forms.Button btnCasseteUnloadBLiftTiltSlinderUp;
        private System.Windows.Forms.Button btnCasseteUnloadBTopCstUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadBTopCstGrib;
        private System.Windows.Forms.Button btnCasseteUnloadALiftTiltSlinderDown;
        private System.Windows.Forms.Button btnCasseteUnloadALiftTiltSlinderUp;
        private System.Windows.Forms.Button btnCasseteUnloadATopCstUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadATopCstGrib;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting4;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting2;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting3;
        private System.Windows.Forms.Button btn_CasseteUnload_MutingIn;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting1;
        private System.Windows.Forms.Button btn_CasseteUnload_MutingOut;
        private System.Windows.Forms.Button btnCellInputBlowerOnOff;
        private System.Windows.Forms.Button btnCellInputIonizerOnOff;
        private System.Windows.Forms.Button btnLDTiltACylinderDown;
        private System.Windows.Forms.Button btnLDTiltACylinderUp;
        private System.Windows.Forms.Button btnLDTiltBCylinderDown;
        private System.Windows.Forms.Button btnLDTiltBCylinderUp;
        private System.Windows.Forms.Button btnCellLoadB2PickerBlowerOnOff;
        private System.Windows.Forms.Button btnCellLoadB2PickerDown;
        private System.Windows.Forms.Button btnCellLoadB2PickerUp;
        private System.Windows.Forms.Button btnCellLoadB2PickerVaccumOff;
        private System.Windows.Forms.Button btnCellLoadB2PickerVaccumOn;
        private System.Windows.Forms.Button btnCellLoadA2PickerBlowerOnOff;
        private System.Windows.Forms.Button btnCellLoadA2PickerDown;
        private System.Windows.Forms.Button btnCellLoadA2PickerUp;
        private System.Windows.Forms.Button btnCellLoadA2PickerVaccumOff;
        private System.Windows.Forms.Button btnCellLoadA2PickerVaccumOn;
        private System.Windows.Forms.Button btnBreakTransferA2PickerBlowerOnOff;
        private System.Windows.Forms.Button btnBreakTransferA2PickerUp;
        private System.Windows.Forms.Button btnBreakTransferA2PickerVaccumOff;
        private System.Windows.Forms.Button btnBreakTransferA2PickerDown;
        private System.Windows.Forms.Button btnBreakTransferA2PickerVaccumOn;
        private System.Windows.Forms.Button btnBreakTransferB2PickerBlowerOnOff;
        private System.Windows.Forms.Button btnBreakTransferB2PickerUp;
        private System.Windows.Forms.Button btnBreakTransferB2PickerVaccumOff;
        private System.Windows.Forms.Button btnBreakTransferB2PickerDown;
        private System.Windows.Forms.Button btnBreakTransferB2PickerVaccumOn;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Button btnIRCut2BrushDown;
        internal System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button btnIRCut1BrushDown;
        private System.Windows.Forms.Button btnIRCut2BrushUp;
        private System.Windows.Forms.Button btnIRCut1BrushUp;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Button btnBreakBBrushDown;
        internal System.Windows.Forms.Label label71;
        private System.Windows.Forms.Button btnBreakABrushDown;
        private System.Windows.Forms.Button btnBreakBBrushUp;
        private System.Windows.Forms.Button btnBreakABrushUp;
        private System.Windows.Forms.DataGridViewTextBoxColumn posName;
        private System.Windows.Forms.DataGridViewTextBoxColumn aixs;
        private System.Windows.Forms.DataGridViewTextBoxColumn Position;
        private System.Windows.Forms.DataGridViewTextBoxColumn speed;
        private System.Windows.Forms.Button btnCstloadBCellOut;
        private System.Windows.Forms.Button btnCstloadAMeasure;
        private System.Windows.Forms.Button btnCstloadALoader;
        private System.Windows.Forms.Button btnCellLoadX4ULD;
        private System.Windows.Forms.Button btnCellLoadX4PreAlign;
        private System.Windows.Forms.Button btnCellLoadX4LD;
        private System.Windows.Forms.Button btnCellLoadX3ULD;
        private System.Windows.Forms.Button btnCellLoadX1ULD;
        private System.Windows.Forms.Button btnCellLoadX2ULD;
        private System.Windows.Forms.Button btnCellLoadX3PreAlign;
        private System.Windows.Forms.Button btnCellLoadX1PreAlign;
        private System.Windows.Forms.Button btnCellLoadX2PreAlign;
        private System.Windows.Forms.Button btnIRCutProcessPowerMeterAutoMeasure;
        private System.Windows.Forms.Button btnBreakXZB2LDS;
        private System.Windows.Forms.Button btnBreakXZB1LDS;
        private System.Windows.Forms.Button btnBreakXZA2LDS;
        private System.Windows.Forms.Button btnBreakXZA1LDS;
        private System.Windows.Forms.Button btnBreakUnitTYBBreaking;
        private System.Windows.Forms.Button btnBreakUnitTYBAlignCam;
        private System.Windows.Forms.Button btnBreakUnitTYBULD;
        private System.Windows.Forms.Button btnBreakUnitTYBLD;
        private System.Windows.Forms.Button btnBreakUnitTYABreaking;
        private System.Windows.Forms.Button btnBreakUnitTYAAlignCam;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCellInputY1CellULD;
        private System.Windows.Forms.Button btnCellInputY1CellLD;
        private System.Windows.Forms.Panel panel15;
        internal System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnCellInputB1HoldA;
        private System.Windows.Forms.Button btnCellInputB1MiddleA;
        private System.Windows.Forms.Panel panel16;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnCellInputA1HoldA;
        private System.Windows.Forms.Button btnCellInputA1MiddleA;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnCstunloadBCellOut;
        private System.Windows.Forms.Button btnCstunloadBMeasure;
        private System.Windows.Forms.Button btnCstunloadB90dgr;
        private System.Windows.Forms.Button btnCstunloadB0dgr;
        internal System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Button btnCstunloadAMeasure;
        private System.Windows.Forms.Button btnCstunloadA0dgr;
        private System.Windows.Forms.Button btnCstunloadA90dgr;
        private System.Windows.Forms.Button btnCstunloadACellOut;
        internal System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnLoaderY1AxisCstMiddle;
        private System.Windows.Forms.Button btnCellInputY1CstMid;
        private System.Windows.Forms.Button btnBreakUnitTYMeasure4;
        private System.Windows.Forms.Button btnBreakUnitTYHold4;
        private System.Windows.Forms.Button btnBreakUnitTYMeasure3;
        private System.Windows.Forms.Button btnBreakUnitTYHold3;
        private System.Windows.Forms.Button btnLoaderAxisX2Cst_B_Wait;
        private System.Windows.Forms.Button btnLoaderAxisX2Cst_B_Middle;
        private System.Windows.Forms.Button btnLoaderAxisX1Cst_B_Wait;
        private System.Windows.Forms.Button btnLoaderAxisX1Cst_B_Middle;
        private System.Windows.Forms.Button btnCellInputB1HoldB;
        private System.Windows.Forms.Button btnCellInputB1MiddleB;
        private System.Windows.Forms.Button btnCellInputA1HoldB;
        private System.Windows.Forms.Button btnCellInputA1MiddleB;
        private System.Windows.Forms.Button btnInspStage;
        private System.Windows.Forms.Button btnAfterTransfer;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Panel panel46;
        internal System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button btnAfterB2PikerBlowerOnOff;
        private System.Windows.Forms.Button btnAfterB2PikerDown;
        private System.Windows.Forms.Button btnAfterB1PikerUp;
        private System.Windows.Forms.Button btnAfterB1PikerBlowerOnOff;
        private System.Windows.Forms.Button btnAfterB1PikerDown;
        private System.Windows.Forms.Button btnAfterB2PikerVaccumOff;
        private System.Windows.Forms.Button btnAfterB2PikerUp;
        private System.Windows.Forms.Button btnAfterB2PikerVaccumOn;
        private System.Windows.Forms.Button btnAfterB1PikerVaccumOn;
        private System.Windows.Forms.Button btnAfterB1PikerVaccumOff;
        private System.Windows.Forms.Panel panel49;
        internal System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button btnAfterA2PikerBlowerOnOff;
        private System.Windows.Forms.Button btnAfterA2PikerDown;
        private System.Windows.Forms.Button btnAfterA1PikerUp;
        private System.Windows.Forms.Button btnAfterA1PikerBlowerOnOff;
        private System.Windows.Forms.Button btnAfterA1PikerDown;
        private System.Windows.Forms.Button btnAfterA2PikerVaccumOff;
        private System.Windows.Forms.Button btnAfterA2PikerUp;
        private System.Windows.Forms.Button btnAfterA2PikerVaccumOn;
        private System.Windows.Forms.Button btnAfterA1PikerVaccumOn;
        private System.Windows.Forms.Button btnAfterA1PikerVaccumOff;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Button btnAfterTr2Insp;
        private System.Windows.Forms.Button btnAfterTr2Unloading;
        private System.Windows.Forms.Button btnAfterTr2B0dgr;
        private System.Windows.Forms.Button btnAfterTr2B90dgr;
        internal System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button btnAfterTr2B90rdgr;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Button btnAfterTr1Insp;
        private System.Windows.Forms.Button btnAfterTr1Unloading;
        private System.Windows.Forms.Button btnAfterTr1A0dgr;
        private System.Windows.Forms.Button btnAfterTr1A90dgr;
        internal System.Windows.Forms.Label label59;
        private System.Windows.Forms.Button btnAfterTr1A90rdgr;
        internal System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Button btnInspStB1BlowerOnOff;
        internal System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnInspStB2VacOff;
        private System.Windows.Forms.Button btnInspStB2VacOn;
        private System.Windows.Forms.Button btnInspStB2BlowerOnOff;
        private System.Windows.Forms.Button btnInspStB1VacOn;
        private System.Windows.Forms.Button btnInspStB1VacOff;
        private System.Windows.Forms.Panel panel24;
        internal System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnInspStA1VacOn;
        private System.Windows.Forms.Button btnInspStA1BlowerOnOff;
        private System.Windows.Forms.Button btnInspStA2VacOn;
        private System.Windows.Forms.Button btnInspStA2VacOff;
        private System.Windows.Forms.Button btnInspStA1VacOff;
        private System.Windows.Forms.Button btnInspStA2BlowerOnOff;
        private System.Windows.Forms.Button btnLoaderAxisX1Cst_A_Unloading;
        private System.Windows.Forms.Button btnLoaderAxisX1Cst_B_Unloading;
        private System.Windows.Forms.Button btnLoaderAxisX2Cst_B_Unloading;
        private System.Windows.Forms.Button btnLoaderAxisX2Cst_A_Unloading;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Button btnLoaderY2AxisCstMiddle;
        private System.Windows.Forms.Button btnLoaderY2AxisCstAUnloading;
        internal System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnLoaderY2AxisCstBInner;
        private System.Windows.Forms.Button btnLoaderY2AxisCstBUnloading;
        private System.Windows.Forms.Button btnLoaderY2AxisCstAInner;
        private System.Windows.Forms.Button btnCellLoadA1PickerVaccumOff;
        private System.Windows.Forms.Button btnCellLoadBCellULD;
        private System.Windows.Forms.Button btnCellLoadBCellLD;
        private System.Windows.Forms.Button btnCellLoadACellULD;
        private System.Windows.Forms.Button btnCellLoadACellLD;
        private System.Windows.Forms.Button btnCellLoadAPicker0;
        private System.Windows.Forms.Button btnCellLoadAPickerMinus90;
        private System.Windows.Forms.Button btnCellLoadAPickerPlus90;
        private System.Windows.Forms.Button btnCellLoadBPickerMinus90;
        private System.Windows.Forms.Button btnCellLoadBPickerPlus90;
        private System.Windows.Forms.Button btnCellLoadBPicker0;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Panel panel76;
        internal System.Windows.Forms.Label label75;
        private System.Windows.Forms.Button btnAZJigAlign;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Button btnA1Breaking;
        internal System.Windows.Forms.Label label76;
        internal System.Windows.Forms.Label label77;
        private System.Windows.Forms.Button btnA2Breaking;
        private System.Windows.Forms.Button btnAZDown1;
        private System.Windows.Forms.Button btnBZJigAlign;
        private System.Windows.Forms.Button btnAZDown3;
        private System.Windows.Forms.Button btnBZDown3;
        private System.Windows.Forms.Button btnBZDown1;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Button btnBeforeTr2Insp;
        internal System.Windows.Forms.Label label72;
        private System.Windows.Forms.Button btnBeforeTr2A0dgr;
        private System.Windows.Forms.Button btnBeforeTr2Loading;
        private System.Windows.Forms.Button btnBeforeTr2A90dgr;
        private System.Windows.Forms.Button btnBeforeTr2A90rdgr;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Button btnBeforeTr1Insp;
        internal System.Windows.Forms.Label label73;
        private System.Windows.Forms.Button btnBeforeTr1A0dgr;
        private System.Windows.Forms.Button btnBeforeTr1Loading;
        private System.Windows.Forms.Button btnBeforeTr1A90dgr;
        private System.Windows.Forms.Button btnBeforeTr1A90rdgr;
        internal System.Windows.Forms.Label label74;
        private System.Windows.Forms.Button btnCamUnitAInspLowEnd;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Button btnCamUnitBInspUpEnd;
        private System.Windows.Forms.Button btnCamUnitBInspLowEnd;
        private System.Windows.Forms.Button btnCamUnitBInspUpStart;
        internal System.Windows.Forms.Label label78;
        private System.Windows.Forms.Button btnCamUnitBInspLowStart;
        private System.Windows.Forms.Button btnCamUnitAInspUpEnd;
        private System.Windows.Forms.Button btnCellInputY1BCstInner;
        private System.Windows.Forms.Button btnCellInputY1ACstInner;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnCellInputY2BCstInner;
        private System.Windows.Forms.Button btnCellInputY2ACstInner;
        private System.Windows.Forms.Button btnCellInputY2CstMid;
        internal System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnCellInputY2CellULD;
        private System.Windows.Forms.Button btnCellInputY2CellLD;
        private System.Windows.Forms.Button btnCstunloadBLoader;
        private System.Windows.Forms.Button btnCstunloadALoader;
        private System.Windows.Forms.Button btnB2Breaking;
        private System.Windows.Forms.Button btnB1Breaking;
        private System.Windows.Forms.Button btnCellInputA1LoadingA;
        private System.Windows.Forms.Button btnCellInputA1LoadingB;
        private System.Windows.Forms.Button btnCellInputB1LoadingA;
        private System.Windows.Forms.Button btnCellInputB1LoadingB;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Button btnInspStBULD;
        internal System.Windows.Forms.Label label79;
        private System.Windows.Forms.Button btnInspStBInsp;
        private System.Windows.Forms.Button btnInspStBLD;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Button btnInspStAULD;
        internal System.Windows.Forms.Label label80;
        private System.Windows.Forms.Button btnInspStAInsp;
        private System.Windows.Forms.Button btnInspStALD;
        internal System.Windows.Forms.Label label81;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Button btnCamUnitB2;
        private System.Windows.Forms.Button btnCamUnitB1;
        private System.Windows.Forms.Button btnCamUnitA2;
        internal System.Windows.Forms.Label label82;
        private System.Windows.Forms.Button btnCamUnitA1;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Button btnIRCutProcessZPowerMeter;
        internal System.Windows.Forms.Label label83;
        private System.Windows.Forms.Button btnIRCutProcessZLaserShot;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Button btnPreAlignB;
        private System.Windows.Forms.Button btnPreAlignA;
        internal System.Windows.Forms.Label label84;
        private System.Windows.Forms.Button btnBreakX1BreakAlign;
        private System.Windows.Forms.Button btnBreakX1JigAlign;
    }
}

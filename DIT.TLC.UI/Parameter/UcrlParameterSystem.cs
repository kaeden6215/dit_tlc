﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlParameterSystem : UserControl, IUIUpdate
    {
        public UcrlParameterSystem()
        {
            InitializeComponent();
        }

        private void btnMode_Click(object sender, EventArgs e)
        {
            if (btnSimulationModeUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsSimulationMode = true;
            }
            else if (btnSimulationModeDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsSimulationMode = false;
            }
            else if (btnDistributionModeUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsDistributionMode = true;
            }
            else if (btnDistributionModeDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsDistributionMode = false;
            }
            //나중에 삭제 요망
            UIUpdate();
        }

        private void btnSkip_Click(object sender, EventArgs e)
        {
            //프로세스 A 물류 스킵
            if (btnProcessASkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsProcessASkipMode = true;
            }
            else if (btnProcessASkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsProcessASkipMode = false;
            }
            //프로세스 B 물류 스킵
            else if (btnProcessBSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsProcessBSkipMode = true;
            }
            else if (btnProcessBSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsProcessBSkipMode = false;
            }
            //브레이킹 A 물류 스킵
            else if (btnBreakingASkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingASkipMode = true;
            }
            else if (btnBreakingASkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingASkipMode = false;
            }
            //브레이킹 B 물류 스킵
            else if (btnBreakingBSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingBSkipMode = true;
            }
            else if (btnBreakingBSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingBSkipMode = false;
            }
            //레이저 스킵
            else if (btnLaserSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLaserSkipMode = true;
            }
            else if (btnLaserSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLaserSkipMode = false;
            }
            // 브레이킹 모션 스킵
            else if (btnBreakingMotionSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingMotionSkipMode = true;
            }
            else if (btnBreakingMotionSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingMotionSkipMode = false;
            }
            //인스펙션 스킵
            else if (btnInspectionSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsInspectionSkipUseMode = true;
            }
            else if (btnInspectionSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsInspectionSkipUseMode = false;
            }
            //브레이킹 얼라인 스킵
            else if (btnBreakingAlignSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingAlignSkipMode = true;
            }
            else if (btnBreakingAlignSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBreakingAlignSkipMode = false;
            }
            // MCR 스킵
            else if (btnMcrSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsMcrSkipMode = true;
            }
            else if (btnMcrSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsMcrSkipMode = false;
            }
            //틸트 측정 스킵
            else if (btnTiltMeasureSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsTiltMeasureSkipMode = true;
            }
            else if (btnTiltMeasureSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsTiltMeasureSkipMode = false;
            }
            //BCR 스킵
            else if (btnBcrSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBcrSkipMode = true;
            }
            else if (btnBcrSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBcrSkipMode = false;
            }
            //버퍼 피커 스킵
            else if (btnBufferPickerSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBufferPickerSkipMode = true;
            }
            else if (btnBufferPickerSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsBufferPickerSkipMode = false;
            }
            // 레이저 측정 스킵
            else if (btnLaserMeasureSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLaserMeasureSkipMode = true;
            }
            else if (btnLaserMeasureSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLaserMeasureSkipMode = false;
            }
            // 뮤팅 스킵
            else if (btnMutingSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsMutingSkipMode = true;
            }
            else if (btnMutingSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsMutingSkipMode = false;
            }
            // 그랩 스위치 스킵
            else if (btnGrabSwitchSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsGrabSwitchSkipMode = true;
            }
            else if (btnGrabSwitchSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsGrabSwitchSkipMode = false;
            }
            // 티치 스킵
            else if (btnTeachSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsTeachSkipMode = true;
            }
            else if (btnTeachSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsTeachSkipMode = false;
            }
            // 도어 스킵
            else if (btnDoorSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsDoorSkipMode = true;
            }
            else if (btnDoorSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsDoorSkipMode = false;
            }
            // MC 다운 스킵
            else if (btnMcDownSkipUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsMcDownSkipMode = true;
            }
            else if (btnMcDownSkipDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsMcDownSkipMode = false;
            }
            
            // 인터락 사용 유무. 
            else if (btnInterlockUse== (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsInterlockUse = true;
            }
            else if (btnInterlockDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsInterlockUse = false;
            }

            //나중에 삭제 요망
            UIUpdate();
        }

        private void btnCstSkip_Click(object sender, EventArgs e)
        {
            if (btnLoaderInputSkipAUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipAMode = true;
            }
            else if (btnLoaderInputSkipADontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipAMode = false;
            }
            else if (btnLoaderInputSkipBUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipBMode = true;
            }
            else if (btnLoaderInputSkipBDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipBMode = false;
            }
            else if (btnUnloaderInputSkipAUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipAMode = true;
            }
            else if (btnUnloaderInputSkipADontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipAMode = false;
            }
            else if (btnUnloaderInputSkipBUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipBMode = true;
            }
            else if (btnUnloaderInputSkipBDontUse == (Button)sender)
            {
                GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipBMode = false;
            }
            //나중에 삭제 요망
            UIUpdate();
        }

        public void FillSettingData(SystemParamSetting psetting, SystemParamSkip pskip)
        {
            #region FillSettingData
            txtPreAlignSpeed.Text = psetting.PreAlignSpeed.ToString();
            txtFineAlignSpeed.Text = psetting.FineAlignSpeed.ToString();
            txtBreakingAlignSpeed.Text = psetting.BreakingAlignSpeed.ToString();
            txtVacuumTimeout.Text = psetting.VacuumTimeout.ToString();
            txtCylinderTimeout.Text = psetting.CylinderTimeout.ToString();
            txtPreAlignTimeout.Text = psetting.PreAlignTimeout.ToString();
            txtBreakingAlignTimeout.Text = psetting.BreakingAlignTimeout.ToString();
            txtInspectiongTimeout.Text = psetting.InspectionTimeout.ToString();
            txtSequenceMoveTimeout.Text = psetting.SeqMoveTimeout.ToString();
            txtMutingTimeout.Text = psetting.MutingTimeout.ToString();
            txtMCR.Text = psetting.MCRErrorCount.ToString();
            txtINSP.Text = psetting.INSPErrorCount.ToString();
            txtDoorOpenSpeed.Text = psetting.DoorOpenSpeed.ToString();
            txtLightAlarm.Text = psetting.LightBreakingCount.ToString();
            txtCriticalAlarm.Text = psetting.HeavyBreakingCount.ToString();
            txtAutoDeleteCycle.Text = psetting.AutoDeleteCycleTime.ToString();
            txtTemperatureLightAlarm.Text = psetting.LightTemperature.ToString();
            txtTemperatureCriticalAlarm.Text = psetting.HeavyTemperature.ToString();
            txtCutLineMeasure.Text = psetting.CUTLine.ToString();
            txtBlowingCheck.Text = psetting.BlowingCheck.ToString();
            txtRetryCount.Text = psetting.RetryCount.ToString();
            txtLaserPD7Power.Text = psetting.LaserPD7Power.ToString();
            txtLaserPD7ErrorRange.Text = psetting.PD7ErrorRange.ToString();
            txtMeasureCycle.Text = psetting.PowerMeasureCycleTime.ToString();
            txtTargetPEC.Text = psetting.LaserSettingPower.ToString();
            txtTargetPower.Text = psetting.LaserTargetPower.ToString();
            txtWarmUpTime.Text = psetting.LaserWarmupTime.ToString();
            txtMeasureTime.Text = psetting.PowerMeasureTime.ToString();
            txtCorrectionErrorRange.Text = psetting.RateOfLOSSErrorRange.ToString();
            txtTargetErrorRange.Text = psetting.TargetErrorRate.ToString();
            txtLossRateErrorRange.Text = psetting.LossRateErrorRange.ToString();
            txtLossErrorRateRange.Text = psetting.LossErrorRateRange.ToString();
            txtLDSMeasureCycle.Text = psetting.LDSMeasureCycleTime.ToString();
            txtLDSWaitingTime.Text = psetting.LDSWaitingTime.ToString();
            txtLDSMeasureTime.Text = psetting.LDSMeasureTime.ToString();
            txtLDSErrorRange.Text = psetting.LDSErrorRange.ToString();
            #endregion

            #region FillSkipData
            btnSimulationModeUse.BackColor  = pskip.IsSimulationMode ? Color.DodgerBlue : SystemColors.Control;
            btnSimulationModeDontUse.BackColor = !pskip.IsSimulationMode ? Color.DodgerBlue : SystemColors.Control;
            btnDistributionModeUse.BackColor = pskip.IsDistributionMode ? Color.DodgerBlue : SystemColors.Control;
            btnDistributionModeDontUse.BackColor = !pskip.IsDistributionMode ? Color.DodgerBlue : SystemColors.Control;
            btnProcessASkipUse.BackColor = pskip.IsProcessASkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnProcessASkipDontUse.BackColor = !pskip.IsProcessASkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnProcessBSkipUse.BackColor = pskip.IsProcessBSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnProcessBSkipDontUse.BackColor = !pskip.IsProcessBSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingASkipUse.BackColor = pskip.IsBreakingASkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingASkipDontUse.BackColor = !pskip.IsBreakingASkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingBSkipUse.BackColor = pskip.IsBreakingBSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingBSkipDontUse.BackColor = !pskip.IsBreakingBSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnLaserSkipUse.BackColor = pskip.IsLaserSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnLaserSkipDontUse.BackColor = !pskip.IsLaserSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingMotionSkipUse.BackColor = pskip.IsBreakingMotionSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingMotionSkipDontUse.BackColor = !pskip.IsBreakingMotionSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnInspectionSkipUse.BackColor = pskip.IsInspectionSkipUseMode ? Color.DodgerBlue : SystemColors.Control;
            btnInspectionSkipDontUse.BackColor = !pskip.IsInspectionSkipUseMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingAlignSkipUse.BackColor = pskip.IsBreakingAlignSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBreakingAlignSkipDontUse.BackColor = !pskip.IsBreakingAlignSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnMcrSkipUse.BackColor = pskip.IsMcrSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnMcrSkipDontUse.BackColor = !pskip.IsMcrSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnTiltMeasureSkipUse.BackColor = pskip.IsTiltMeasureSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnTiltMeasureSkipDontUse.BackColor = !pskip.IsTiltMeasureSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBcrSkipUse.BackColor = pskip.IsBcrSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBcrSkipDontUse.BackColor = !pskip.IsBcrSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBufferPickerSkipUse.BackColor = pskip.IsBufferPickerSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnBufferPickerSkipDontUse.BackColor = !pskip.IsBufferPickerSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnLoaderInputSkipAUse.BackColor = pskip.IsLoaderInputSkipAMode ? Color.DodgerBlue : SystemColors.Control;
            btnLoaderInputSkipADontUse.BackColor = !pskip.IsLoaderInputSkipAMode ? Color.DodgerBlue : SystemColors.Control;
            btnLoaderInputSkipBUse.BackColor = pskip.IsLoaderInputSkipBMode ? Color.DodgerBlue : SystemColors.Control;
            btnLoaderInputSkipBDontUse.BackColor = !pskip.IsLoaderInputSkipBMode ? Color.DodgerBlue : SystemColors.Control;
            btnUnloaderInputSkipAUse.BackColor = pskip.IsUnloaderInputSkipAMode ? Color.DodgerBlue : SystemColors.Control;
            btnUnloaderInputSkipADontUse.BackColor = !pskip.IsUnloaderInputSkipAMode ? Color.DodgerBlue : SystemColors.Control;
            btnUnloaderInputSkipBUse.BackColor = pskip.IsUnloaderInputSkipBMode ? Color.DodgerBlue : SystemColors.Control;
            btnUnloaderInputSkipBDontUse.BackColor = !pskip.IsUnloaderInputSkipBMode ? Color.DodgerBlue : SystemColors.Control;
            btnLaserMeasureSkipUse.BackColor = pskip.IsLaserMeasureSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnLaserMeasureSkipDontUse.BackColor = !pskip.IsLaserMeasureSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnMutingSkipUse.BackColor = pskip.IsMutingSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnMutingSkipDontUse.BackColor = !pskip.IsMutingSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnGrabSwitchSkipUse.BackColor = pskip.IsGrabSwitchSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnGrabSwitchSkipDontUse.BackColor = !pskip.IsGrabSwitchSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnTeachSkipUse.BackColor = pskip.IsTeachSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnTeachSkipDontUse.BackColor = !pskip.IsTeachSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnDoorSkipUse.BackColor = pskip.IsDoorSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnDoorSkipDontUse.BackColor = !pskip.IsDoorSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnMcDownSkipUse.BackColor = pskip.IsMcDownSkipMode ? Color.DodgerBlue : SystemColors.Control;
            btnMcDownSkipDontUse.BackColor = !pskip.IsMcDownSkipMode ? Color.DodgerBlue : SystemColors.Control;
            #endregion
        }

        public void UpdateSetting(ref SystemParamSetting psetting, ref SystemParamSkip pskip)
        {
            #region UpdateSetting
            psetting.PreAlignSpeed = double.Parse(txtPreAlignSpeed.Text);
            psetting.FineAlignSpeed = double.Parse(txtFineAlignSpeed.Text);
            psetting.BreakingAlignSpeed = double.Parse(txtBreakingAlignSpeed.Text);
            psetting.VacuumTimeout = int.Parse(txtVacuumTimeout.Text);
            psetting.CylinderTimeout = int.Parse(txtCylinderTimeout.Text);
            psetting.PreAlignTimeout = int.Parse(txtPreAlignTimeout.Text);
            psetting.BreakingAlignTimeout = int.Parse(txtBreakingAlignTimeout.Text);
            psetting.InspectionTimeout = int.Parse(txtInspectiongTimeout.Text);
            psetting.SeqMoveTimeout = int.Parse(txtSequenceMoveTimeout.Text);
            psetting.MutingTimeout = int.Parse(txtMutingTimeout.Text);
            psetting.MCRErrorCount = int.Parse(txtMCR.Text);
            psetting.INSPErrorCount = int.Parse(txtINSP.Text);
            psetting.DoorOpenSpeed = double.Parse(txtDoorOpenSpeed.Text);
            psetting.LightBreakingCount = int.Parse(txtLightAlarm.Text);
            psetting.HeavyBreakingCount = int.Parse(txtCriticalAlarm.Text);
            psetting.AutoDeleteCycleTime = int.Parse(txtAutoDeleteCycle.Text);
            psetting.LightTemperature = double.Parse(txtTemperatureLightAlarm.Text);
            psetting.HeavyTemperature = double.Parse(txtTemperatureCriticalAlarm.Text);
            psetting.CUTLine = int.Parse(txtCutLineMeasure.Text);
            psetting.BlowingCheck = double.Parse(txtBlowingCheck.Text);
            psetting.RetryCount = int.Parse(txtRetryCount.Text);
            psetting.LaserPD7Power = double.Parse(txtLaserPD7Power.Text);
            psetting.PD7ErrorRange = int.Parse(txtLaserPD7ErrorRange.Text);
            psetting.PowerMeasureCycleTime = int.Parse(txtMeasureCycle.Text);
            psetting.LaserSettingPower = double.Parse(txtTargetPEC.Text);
            psetting.LaserTargetPower = double.Parse(txtTargetPower.Text);
            psetting.LaserWarmupTime = int.Parse(txtWarmUpTime.Text);
            psetting.PowerMeasureTime = int.Parse(txtMeasureTime.Text);
            psetting.RateOfLOSSErrorRange = double.Parse(txtCorrectionErrorRange.Text);
            psetting.TargetErrorRate = int.Parse(txtTargetErrorRange.Text);
            psetting.LossRateErrorRange = double.Parse(txtLossRateErrorRange.Text);
            psetting.LossErrorRateRange = double.Parse(txtLossErrorRateRange.Text);
            psetting.LDSMeasureCycleTime = int.Parse(txtLDSMeasureCycle.Text);
            psetting.LDSWaitingTime = int.Parse(txtLDSWaitingTime.Text);
            psetting.LDSMeasureTime = int.Parse(txtLDSMeasureTime.Text);
            psetting.LDSErrorRange = double.Parse(txtLDSErrorRange.Text);
            #endregion

            #region UpdateSkipSetting
            pskip.IsSimulationMode = (btnSimulationModeUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsDistributionMode = (btnDistributionModeUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsProcessASkipMode = (btnProcessASkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsProcessBSkipMode = (btnProcessBSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsBreakingASkipMode = (btnBreakingASkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsBreakingBSkipMode = (btnBreakingBSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsLaserSkipMode = (btnLaserSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsBreakingMotionSkipMode = (btnBreakingMotionSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsInspectionSkipUseMode = (btnInspectionSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsBreakingAlignSkipMode = (btnBreakingAlignSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsMcrSkipMode = (btnMcrSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsTiltMeasureSkipMode = (btnTiltMeasureSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsBcrSkipMode = (btnBcrSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsBufferPickerSkipMode = (btnBufferPickerSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsLoaderInputSkipAMode = (btnLoaderInputSkipAUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsLoaderInputSkipBMode = (btnLoaderInputSkipBUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsUnloaderInputSkipAMode = (btnUnloaderInputSkipAUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsUnloaderInputSkipBMode = (btnUnloaderInputSkipBUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsLaserMeasureSkipMode = (btnLaserMeasureSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsMutingSkipMode = (btnMutingSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsGrabSwitchSkipMode = (btnGrabSwitchSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsTeachSkipMode = (btnTeachSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsDoorSkipMode = (btnDoorSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            pskip.IsMcDownSkipMode = (btnMcDownSkipUse.BackColor == UiGlobal.SELECT_C) ? true : false;
            #endregion
        }

        public void UIUpdate()
        {
            btnSimulationModeUse.BackColor = GG.Equip.EqpSysMgr.Params.IsSimulationMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnSimulationModeDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsSimulationMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnDistributionModeUse.BackColor = GG.Equip.EqpSysMgr.Params.IsDistributionMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnDistributionModeDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsDistributionMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnProcessASkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsProcessASkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnProcessASkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsProcessASkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnProcessBSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsProcessBSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnProcessBSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsProcessBSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingASkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsBreakingASkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingASkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsBreakingASkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingBSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsBreakingBSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingBSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsBreakingBSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLaserSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsLaserSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLaserSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsLaserSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingMotionSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsBreakingMotionSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingMotionSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsBreakingMotionSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnInspectionSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsInspectionSkipUseMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnInspectionSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsInspectionSkipUseMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingAlignSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsBreakingAlignSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakingAlignSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsBreakingAlignSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMcrSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsMcrSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMcrSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsMcrSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnTiltMeasureSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsTiltMeasureSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnTiltMeasureSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsTiltMeasureSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBcrSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsBcrSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBcrSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsBcrSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBufferPickerSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsBufferPickerSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBufferPickerSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsBufferPickerSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLoaderInputSkipAUse.BackColor = GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipAMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLoaderInputSkipADontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipAMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLoaderInputSkipBUse.BackColor = GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipBMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLoaderInputSkipBDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsLoaderInputSkipBMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnUnloaderInputSkipAUse.BackColor = GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipAMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnUnloaderInputSkipADontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipAMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnUnloaderInputSkipBUse.BackColor = GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipBMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnUnloaderInputSkipBDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsUnloaderInputSkipBMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLaserMeasureSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsLaserMeasureSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnLaserMeasureSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsLaserMeasureSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMutingSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsMutingSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMutingSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsMutingSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnGrabSwitchSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsGrabSwitchSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnGrabSwitchSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsGrabSwitchSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnTeachSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsTeachSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnTeachSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsTeachSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnDoorSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsDoorSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnDoorSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsDoorSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMcDownSkipUse.BackColor = GG.Equip.EqpSysMgr.Params.IsMcDownSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMcDownSkipDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsMcDownSkipMode ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            
            btnInterlockUse.BackColor = GG.Equip.EqpSysMgr.Params.IsInterlockUse? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnInterlockDontUse.BackColor = !GG.Equip.EqpSysMgr.Params.IsInterlockUse ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SystemParamSetting sysparam = GG.Equip.EqpSysParamMgr.LstEqpSysParam;
            SystemParamSkip sysparamskip = GG.Equip.EqpSysMgr.Params;
            UpdateSetting(ref sysparam, ref sysparamskip);
            GG.Equip.EqpSysMgr.Save();
            GG.Equip.EqpSysParamMgr.Save();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlParameterServoPositionSetting : UserControl, IUIUpdate
    {
        public class CylinderActionChecker
        {
            private Button _btnForward;
            private Button _btnBackward;
            private Cylinder _cylinder;
            private CylinderTwo _cylinderTwo;
            
            public CylinderActionChecker(Button btnForward, Button btnBackward, object cylinder)
            {
                _btnForward = btnForward;
                _btnBackward = btnBackward;

                _cylinder = cylinder as Cylinder;
                _cylinderTwo = cylinder as CylinderTwo;

                _btnForward.MouseClick += BtnForward_MouseClick;
                _btnBackward.MouseClick += _btnBackward_MouseClick;
            }
            private void _btnBackward_MouseClick(object sender, MouseEventArgs e)
            {
                if (_cylinder != null) _cylinder.Backward(GG.Equip);
                if (_cylinderTwo != null) _cylinderTwo.Backward(GG.Equip);
            }
            private void BtnForward_MouseClick(object sender, MouseEventArgs e)
            {
                if (_cylinder != null) _cylinder.Forward(GG.Equip);
                if (_cylinderTwo != null) _cylinderTwo.Forward(GG.Equip);
            }
            public void UIUpdate()
            {
                if (_cylinder != null) _btnForward.BackColor = _cylinder.IsForward ? UiGlobal.SET_C : UiGlobal.UNSET_C;
                if (_cylinderTwo != null) _btnForward.BackColor = _cylinderTwo.IsForward ? UiGlobal.SET_C : UiGlobal.UNSET_C;


                if (_cylinder != null) _btnBackward.BackColor = _cylinder.IsBackward ? UiGlobal.SET_C : UiGlobal.UNSET_C;
                if (_cylinderTwo != null) _btnBackward.BackColor = _cylinderTwo.IsBackward ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            }
        }
        public class ServoActionChecker
        {
            private Button _btnMove;
            private ServoMotorControl _servo;
            private int _posiNo;

            public ServoActionChecker(Button btn, ServoMotorControl servo, int posiNo)
            {
                _btnMove = btn;
                _servo = servo;
                _posiNo = posiNo;
                _btnMove.MouseClick += BtnMove_MouseClick;
            }
            private void BtnMove_MouseClick(object sender, MouseEventArgs e)
            {
                _servo.PtpMoveCmd(GG.Equip, _posiNo, GG.Equip.CurrentRecipe, null);
            }

            public void UIUpdate()
            {
                SetColor(_servo.IsInPosition(_posiNo, GG.Equip.CurrentRecipe, null));
            }
            public void SetColor(bool value)
            {
                _btnMove.BackColor = value ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            }
        }
        public class Switch2Cmd1SensorActionChecker
        {
            private Button _btnOn;
            private Button _btnOff;
            private Switch2Cmd1Sensor _switch;

            public Switch2Cmd1SensorActionChecker(Button btnOn, Button btnOff, Switch2Cmd1Sensor switchSensor)
            {
                _btnOn = btnOn;
                _btnOff = btnOff;

                _switch = switchSensor;

                _btnOn.MouseClick += BtnOn_MouseClick;
                _btnOff.MouseClick += _btnOff_MouseClick;
            }
            private void BtnOn_MouseClick(object sender, MouseEventArgs e)
            {
                _switch.OnOff(GG.Equip, true);
            }
            private void _btnOff_MouseClick(object sender, MouseEventArgs e)
            {
                _switch.OnOff(GG.Equip, false);
            }
            public void UIUpdate()
            {
                _btnOn.BackColor = _switch.IsOnOff == true ? UiGlobal.SET_C : UiGlobal.UNSET_C;
                _btnOff.BackColor = _switch.IsOnOff == false ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            }
        }
        public class SwitchOneWayActionChecker
        {
            private Button _btnOnOff;            
            private SwitchOneWay _switch;
            
            public SwitchOneWayActionChecker(Button btnOnOff, SwitchOneWay switchSensor)
            {
                _btnOnOff = btnOnOff;
              
                _switch = switchSensor;

                _btnOnOff.MouseDown += _btnOnOff_MouseDown;
                _btnOnOff.MouseUp += _btnOnOff_MouseUp;
                _btnOnOff.MouseLeave += _btnOnOff_MouseLeave;

            }
            private void _btnOnOff_MouseLeave(object sender, EventArgs e)
            {
                _switch.OnOff(GG.Equip, false);
            }
            private void _btnOnOff_MouseUp(object sender, MouseEventArgs e)
            {
                _switch.OnOff(GG.Equip, false);
            }
            private void _btnOnOff_MouseDown(object sender, MouseEventArgs e)
            {
                _switch.OnOff(GG.Equip, true);
            }
             
            public void UIUpdate()
            {
                _btnOnOff.BackColor = _switch.IsOnOff == true ? UiGlobal.SET_C : UiGlobal.UNSET_C;                
            }
        }
        public class SwitchSensorActionChecker
        {
            private Button _btnOn;
            private Button _btnOff;
            private Switch _switch;

            public SwitchSensorActionChecker(Button btnOn, Button btnOff, Switch switchSensor)
            {
                _btnOn = btnOn;
                _btnOff = btnOff;

                _switch = switchSensor;

                _btnOn.MouseClick += BtnOn_MouseClick;
                _btnOff.MouseClick += _btnOff_MouseClick;
            }
            private void BtnOn_MouseClick(object sender, MouseEventArgs e)
            {
                _switch.OnOff(GG.Equip, true);
            }
            private void _btnOff_MouseClick(object sender, MouseEventArgs e)
            {
                _switch.OnOff(GG.Equip, false);
            }
            public void UIUpdate()
            {
                _btnOn.BackColor = _switch.IsOnOff == true ? UiGlobal.SET_C : UiGlobal.UNSET_C;
                _btnOff.BackColor = _switch.IsOnOff == false ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            }
        }

        private ServoPosiInfo _selectedServoPosi;
        public UcrlParameterServoPositionSetting()
        {
            InitializeComponent();
        }

        private List<ServoActionChecker> lstServoActChecker = null;
        private List<CylinderActionChecker> lstCylinderActChecker = null;
        private List<SwitchOneWayActionChecker> lstSwitchOneActChecker = null;
        private List<Switch2Cmd1SensorActionChecker> lstSwitch2Cmd1SensorActChecker = null;
        private List<SwitchSensorActionChecker> lstSwitchSensorActChecker = null;

        private int cnt = 0;
        private int colorccnt = 0;
        public void Fill(DataGridView dgv, ServoMotorControl servo)
        {
            if (lstServoActChecker == null)
            {
                lstServoActChecker = new List<ServoActionChecker>();
                lstCylinderActChecker = new List<CylinderActionChecker>();
                lstSwitchOneActChecker = new List<SwitchOneWayActionChecker>();
                lstSwitch2Cmd1SensorActChecker = new List<Switch2Cmd1SensorActionChecker>();
                lstSwitchSensorActChecker = new List<SwitchSensorActionChecker>();

                //카셋트 로더. 
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadA0dgr, GG.Equip.LD.CstLoader_A.CstRotationAxis, (int)EmCstLoaderRotationUpAxisServo.R_0dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadA90dgr, GG.Equip.LD.CstLoader_A.CstRotationAxis, (int)EmCstLoaderRotationUpAxisServo.R_90dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadALoader, GG.Equip.LD.CstLoader_A.CstUpDownAxis, (int)EmCstLoaderUpDownAxisServo.Cst_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadAMeasure, GG.Equip.LD.CstLoader_A.CstUpDownAxis, (int)EmCstLoaderUpDownAxisServo.Cst_Measure));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadACellOut, GG.Equip.LD.CstLoader_A.CstUpDownAxis, (int)EmCstLoaderUpDownAxisServo.Cst_CellOut));

                lstServoActChecker.Add(new ServoActionChecker(btnCstloadB0dgr, GG.Equip.LD.CstLoader_B.CstRotationAxis, (int)EmCstLoaderRotationUpAxisServo.R_0dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadB90dgr, GG.Equip.LD.CstLoader_B.CstRotationAxis, (int)EmCstLoaderRotationUpAxisServo.R_90dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadBLoader, GG.Equip.LD.CstLoader_B.CstUpDownAxis, (int)EmCstLoaderUpDownAxisServo.Cst_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadBMeasure, GG.Equip.LD.CstLoader_B.CstUpDownAxis, (int)EmCstLoaderUpDownAxisServo.Cst_Measure));
                lstServoActChecker.Add(new ServoActionChecker(btnCstloadBCellOut, GG.Equip.LD.CstLoader_B.CstUpDownAxis, (int)EmCstLoaderUpDownAxisServo.Cst_CellOut));


                lstCylinderActChecker.Add(new CylinderActionChecker(btnLDCstAGrip, btnLDCstAGripUn, GG.Equip.LD.CstLoader_A.CstGripCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnLDTiltACylinderDown, btnLDTiltACylinderUp, GG.Equip.LD.CstLoader_A.TiltCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnLDCstBGrip, btnLDCstBGripUn, GG.Equip.LD.CstLoader_B.CstGripCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnLDTiltBCylinderDown, btnLDTiltBCylinderUp, GG.Equip.LD.CstLoader_B.TiltCylinder));

                //셀 추출 이재기 
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX1Cst_A_Middle, GG.Equip.LD.Loader.X1Axis, (int)EmLoaderXAxisServo.Cst_A_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX1Cst_A_Wait, GG.Equip.LD.Loader.X1Axis, (int)EmLoaderXAxisServo.Cst_A_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX1Cst_A_Unloading, GG.Equip.LD.Loader.X1Axis, (int)EmLoaderXAxisServo.Cell_A_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX1Cst_B_Middle, GG.Equip.LD.Loader.X1Axis, (int)EmLoaderXAxisServo.Cst_B_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX1Cst_B_Wait, GG.Equip.LD.Loader.X1Axis, (int)EmLoaderXAxisServo.Cst_B_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX1Cst_B_Unloading, GG.Equip.LD.Loader.X1Axis, (int)EmLoaderXAxisServo.Cell_B_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX2Cst_A_Middle, GG.Equip.LD.Loader.X2Axis, (int)EmLoaderXAxisServo.Cst_A_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX2Cst_A_Wait, GG.Equip.LD.Loader.X2Axis, (int)EmLoaderXAxisServo.Cst_A_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX2Cst_A_Unloading, GG.Equip.LD.Loader.X2Axis, (int)EmLoaderXAxisServo.Cell_A_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX2Cst_B_Middle, GG.Equip.LD.Loader.X2Axis, (int)EmLoaderXAxisServo.Cst_B_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX2Cst_B_Wait, GG.Equip.LD.Loader.X2Axis, (int)EmLoaderXAxisServo.Cst_B_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderAxisX2Cst_B_Unloading, GG.Equip.LD.Loader.X2Axis, (int)EmLoaderXAxisServo.Cell_B_Unloading));


                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY1AxisCstAInner, GG.Equip.LD.Loader.Y1Axis, (int)EmLoaderYAxisServo.Cst_A_Inner));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY1AxisCstAUnloading, GG.Equip.LD.Loader.Y1Axis, (int)EmLoaderYAxisServo.Cell_A_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY1AxisCstBInner, GG.Equip.LD.Loader.Y1Axis, (int)EmLoaderYAxisServo.Cst_B_Inner));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY1AxisCstBUnloading, GG.Equip.LD.Loader.Y1Axis, (int)EmLoaderYAxisServo.Cell_B_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY1AxisCstMiddle, GG.Equip.LD.Loader.Y1Axis, (int)EmLoaderYAxisServo.Cst_Middle));

                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY2AxisCstAInner, GG.Equip.LD.Loader.Y2Axis, (int)EmLoaderYAxisServo.Cst_A_Inner));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY2AxisCstAUnloading, GG.Equip.LD.Loader.Y2Axis, (int)EmLoaderYAxisServo.Cell_A_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY2AxisCstBInner, GG.Equip.LD.Loader.Y2Axis, (int)EmLoaderYAxisServo.Cst_B_Inner));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY2AxisCstBUnloading, GG.Equip.LD.Loader.Y2Axis, (int)EmLoaderYAxisServo.Cell_B_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnLoaderY2AxisCstMiddle, GG.Equip.LD.Loader.Y2Axis, (int)EmLoaderYAxisServo.Cst_Middle));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnLoaderAxisX1VaccumOn, btnLoaderAxisX1VaccumOff, GG.Equip.LD.Loader.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnLoaderAxisX2VaccumOn, btnLoaderAxisX2VaccumOff, GG.Equip.LD.Loader.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnLoaderAxisX1BlowerOnOff, GG.Equip.LD.Loader.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnLoaderAxisX2BlowerOnOff, GG.Equip.LD.Loader.Blower2));


                // 셀 로드 이재기
                lstCylinderActChecker.Add(new CylinderActionChecker(btnCellLoadA1PickerDown, btnCellLoadA1PickerUp, GG.Equip.LD.LoaderTransfer_A.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnCellLoadA2PickerDown, btnCellLoadA2PickerUp, GG.Equip.LD.LoaderTransfer_A.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnCellLoadA1PickerVaccumOn, btnCellLoadA1PickerVaccumOff, GG.Equip.LD.LoaderTransfer_A.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnCellLoadA2PickerVaccumOn, btnCellLoadA2PickerVaccumOff, GG.Equip.LD.LoaderTransfer_A.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnCellLoadA1PickerBlowerOnOff, GG.Equip.LD.LoaderTransfer_A.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnCellLoadA2PickerBlowerOnOff, GG.Equip.LD.LoaderTransfer_A.Blower2));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnCellLoadB1PickerDown, btnCellLoadB1PickerUp, GG.Equip.LD.LoaderTransfer_B.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnCellLoadB2PickerDown, btnCellLoadB2PickerUp, GG.Equip.LD.LoaderTransfer_B.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnCellLoadB1PickerVaccumOn, btnCellLoadB1PickerVaccumOff, GG.Equip.LD.LoaderTransfer_B.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnCellLoadB2PickerVaccumOn, btnCellLoadB2PickerVaccumOff, GG.Equip.LD.LoaderTransfer_B.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnCellLoadB1PickerBlowerOnOff, GG.Equip.LD.LoaderTransfer_B.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnCellLoadB2PickerBlowerOnOff, GG.Equip.LD.LoaderTransfer_B.Blower2));

                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX1LD, GG.Equip.LD.LoaderTransfer_A.X1Axis, (int)EmLoaderTransferXAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX1PreAlign, GG.Equip.LD.LoaderTransfer_A.X1Axis, (int)EmLoaderTransferXAxisServo.Pre_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX1ULD, GG.Equip.LD.LoaderTransfer_A.X1Axis, (int)EmLoaderTransferXAxisServo.Cell_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX2LD, GG.Equip.LD.LoaderTransfer_A.X2Axis, (int)EmLoaderTransferXAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX2PreAlign, GG.Equip.LD.LoaderTransfer_A.X2Axis, (int)EmLoaderTransferXAxisServo.Pre_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX2ULD, GG.Equip.LD.LoaderTransfer_A.X2Axis, (int)EmLoaderTransferXAxisServo.Cell_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX3LD, GG.Equip.LD.LoaderTransfer_B.X1Axis, (int)EmLoaderTransferXAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX3PreAlign, GG.Equip.LD.LoaderTransfer_B.X1Axis, (int)EmLoaderTransferXAxisServo.Pre_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX3ULD, GG.Equip.LD.LoaderTransfer_B.X1Axis, (int)EmLoaderTransferXAxisServo.Cell_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX4LD, GG.Equip.LD.LoaderTransfer_B.X2Axis, (int)EmLoaderTransferXAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX4PreAlign, GG.Equip.LD.LoaderTransfer_B.X2Axis, (int)EmLoaderTransferXAxisServo.Pre_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadX4ULD, GG.Equip.LD.LoaderTransfer_B.X2Axis, (int)EmLoaderTransferXAxisServo.Cell_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadAVisionAlign, GG.Equip.LD.LoaderTransfer_A.YAxis, (int)EmLoaderTransferYAxisServo.Pre_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadAMcrReading, GG.Equip.LD.LoaderTransfer_A.YAxis, (int)EmLoaderTransferYAxisServo.Cal_Mcr_Read));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadACellLD, GG.Equip.LD.LoaderTransfer_A.YAxis, (int)EmLoaderTransferYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadACellULD, GG.Equip.LD.LoaderTransfer_A.YAxis, (int)EmLoaderTransferYAxisServo.Cell_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadBVisionAlign, GG.Equip.LD.LoaderTransfer_B.YAxis, (int)EmLoaderTransferYAxisServo.Pre_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadBMcrReading, GG.Equip.LD.LoaderTransfer_B.YAxis, (int)EmLoaderTransferYAxisServo.Cal_Mcr_Read));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadBCellLD, GG.Equip.LD.LoaderTransfer_B.YAxis, (int)EmLoaderTransferYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellLoadBCellULD, GG.Equip.LD.LoaderTransfer_B.YAxis, (int)EmLoaderTransferYAxisServo.Cell_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnPreAlignA, GG.Equip.LD.PreAlign.XAxis, (int)EmPreAlignXAxisServo.PreAlign_A_1st));
                lstServoActChecker.Add(new ServoActionChecker(btnPreAlignB, GG.Equip.LD.PreAlign.XAxis, (int)EmPreAlignXAxisServo.PreAlign_B_1st));


                // IR Cut 프로세스 (스테이지는 바큠이 Switch)
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessA1VaccumCh1On, btnIRCutProcessA1VaccumCh1Off, GG.Equip.PROC.IRCutStage_A.Vacuum1Ch1));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessA1VaccumCh2On, btnIRCutProcessA1VaccumCh2Off, GG.Equip.PROC.IRCutStage_A.Vacuum1Ch2));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessA2VaccumCh1On, btnIRCutProcessA2VaccumCh1Off, GG.Equip.PROC.IRCutStage_A.Vacuum2Ch1));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessA2VaccumCh2On, btnIRCutProcessA2VaccumCh2Off, GG.Equip.PROC.IRCutStage_A.Vacuum2Ch2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessA1BlowerCh1OnOff, GG.Equip.PROC.IRCutStage_A.Blower1Ch1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessA1BlowerCh2OnOff, GG.Equip.PROC.IRCutStage_A.Blower1Ch2));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessA2BlowerCh1OnOff, GG.Equip.PROC.IRCutStage_A.Blower2Ch1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessA2BlowerCh2OnOff, GG.Equip.PROC.IRCutStage_A.Blower2Ch2));

                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessB1VaccumCh1On, btnIRCutProcessB1VaccumCh1Off, GG.Equip.PROC.IRCutStage_B.Vacuum1Ch1));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessB1VaccumCh2On, btnIRCutProcessB1VaccumCh2Off, GG.Equip.PROC.IRCutStage_B.Vacuum1Ch2));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessB2VaccumCh1On, btnIRCutProcessB2VaccumCh1Off, GG.Equip.PROC.IRCutStage_B.Vacuum2Ch1));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnIRCutProcessB2VaccumCh2On, btnIRCutProcessB2VaccumCh2Off, GG.Equip.PROC.IRCutStage_B.Vacuum2Ch2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessB1BlowerCh1OnOff, GG.Equip.PROC.IRCutStage_B.Blower1Ch1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessB1BlowerCh2OnOff, GG.Equip.PROC.IRCutStage_B.Blower1Ch2));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessB2BlowerCh1OnOff, GG.Equip.PROC.IRCutStage_B.Blower2Ch1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnIRCutProcessB2BlowerCh2OnOff, GG.Equip.PROC.IRCutStage_B.Blower2Ch2));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnIRCut1BrushDown, btnIRCut1BrushUp, GG.Equip.PROC.IRCutStage_A.BrushUpDownCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnIRCut2BrushDown, btnIRCut2BrushUp, GG.Equip.PROC.IRCutStage_B.BrushUpDownCylinder));

                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessACellLoad, GG.Equip.PROC.IRCutStage_A.YAxis, (int)EmLaserCuttingStageYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessACellUnload, GG.Equip.PROC.IRCutStage_A.YAxis, (int)EmLaserCuttingStageYAxisServo.Cell_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessALaser, GG.Equip.PROC.IRCutStage_A.YAxis, (int)EmLaserCuttingStageYAxisServo.Laser_Cutting));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessACamera, GG.Equip.PROC.IRCutStage_A.YAxis, (int)EmLaserCuttingStageYAxisServo.Fine_Align));

                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessBCellLoad, GG.Equip.PROC.IRCutStage_B.YAxis, (int)EmLaserCuttingStageYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessBCellUnload, GG.Equip.PROC.IRCutStage_B.YAxis, (int)EmLaserCuttingStageYAxisServo.Cell_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessBLaser, GG.Equip.PROC.IRCutStage_B.YAxis, (int)EmLaserCuttingStageYAxisServo.Laser_Cutting));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessBCamera, GG.Equip.PROC.IRCutStage_B.YAxis, (int)EmLaserCuttingStageYAxisServo.Fine_Align));

                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessAFineAlign, GG.Equip.PROC.FineAlign.XAxis, (int)EmFineAlignXAxisServo.FineAlign_A_1st));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessBFineAlign, GG.Equip.PROC.FineAlign.XAxis, (int)EmFineAlignXAxisServo.FineAlign_B_1st));

                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessA1LaserShot, GG.Equip.PROC.LaserHead.XAxis, (int)EmLaserCuttingHeadXAxisServo.Laser_A_1Shot));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessA2LaserShot, GG.Equip.PROC.LaserHead.XAxis, (int)EmLaserCuttingHeadXAxisServo.Laser_A_2Shot));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessB1LaserShot, GG.Equip.PROC.LaserHead.XAxis, (int)EmLaserCuttingHeadXAxisServo.Laser_B_1Shot));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessB2LaserShot, GG.Equip.PROC.LaserHead.XAxis, (int)EmLaserCuttingHeadXAxisServo.Laser_B_2Shot));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessPowerMeterAutoMeasure, GG.Equip.PROC.LaserHead.XAxis, (int)EmLaserCuttingHeadXAxisServo.PowerMeterMeasure));

                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessZLaserShot, GG.Equip.PROC.LaserHead.ZAxis, (int)EmLaserCuttingHeadZAxisServo.LaserCutting));
                lstServoActChecker.Add(new ServoActionChecker(btnIRCutProcessZPowerMeter, GG.Equip.PROC.LaserHead.ZAxis, (int)EmLaserCuttingHeadZAxisServo.PowermeterMeasure));


                // Break트랜스퍼 (AfterIRCutTransfer)
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakTransferA1PickerDown, btnBreakTransferA1PickerUp, GG.Equip.PROC.AfterIRCutTransfer_A.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakTransferA2PickerDown, btnBreakTransferA2PickerUp, GG.Equip.PROC.AfterIRCutTransfer_A.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBreakTransferA1PickerVaccumOn, btnBreakTransferA1PickerVaccumOff, GG.Equip.PROC.AfterIRCutTransfer_A.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBreakTransferA2PickerVaccumOn, btnBreakTransferA2PickerVaccumOff, GG.Equip.PROC.AfterIRCutTransfer_A.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakTransferA1PickerBlowerOnOff, GG.Equip.PROC.AfterIRCutTransfer_A.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakTransferA2PickerBlowerOnOff, GG.Equip.PROC.AfterIRCutTransfer_A.Blower2));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakTransferB1PickerDown, btnBreakTransferB1PickerUp, GG.Equip.PROC.AfterIRCutTransfer_B.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakTransferB2PickerDown, btnBreakTransferB2PickerUp, GG.Equip.PROC.AfterIRCutTransfer_B.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBreakTransferB1PickerVaccumOn, btnBreakTransferB1PickerVaccumOff, GG.Equip.PROC.AfterIRCutTransfer_B.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBreakTransferB2PickerVaccumOn, btnBreakTransferB2PickerVaccumOff, GG.Equip.PROC.AfterIRCutTransfer_B.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakTransferB1PickerBlowerOnOff, GG.Equip.PROC.AfterIRCutTransfer_B.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakTransferB2PickerBlowerOnOff, GG.Equip.PROC.AfterIRCutTransfer_B.Blower2));

                lstServoActChecker.Add(new ServoActionChecker(btnBreakTRALD, GG.Equip.PROC.AfterIRCutTransfer_A.YAxis, (int)EmAfterIRCutYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakTRAULD, GG.Equip.PROC.AfterIRCutTransfer_A.YAxis, (int)EmAfterIRCutYAxisServo.Cell_Unloading));

                lstServoActChecker.Add(new ServoActionChecker(btnBreakTRBLD, GG.Equip.PROC.AfterIRCutTransfer_B.YAxis, (int)EmAfterInspUnloaderTransferYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakTRBULD, GG.Equip.PROC.AfterIRCutTransfer_B.YAxis, (int)EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading));

                // Break유닛(X Z축)
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakABrushDown, btnBreakABrushUp, GG.Equip.PROC.BreakStage_A.BrushUpDownCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakBBrushDown, btnBreakBBrushUp, GG.Equip.PROC.BreakStage_B.BrushUpDownCylinder));

                lstServoActChecker.Add(new ServoActionChecker(btnBreakXZAAlignMeasure, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.BreakAling_A_1st));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakXZBAlignMeasure, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.BreakAling_B_1st));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakXZA1LDS, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.Flatness_A_1_tb));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakXZA2LDS, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.Flatness_A_2_tb));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakXZB1LDS, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.Flatness_B_1_tb));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakXZB2LDS, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.Flatness_B_2_tb));

                lstServoActChecker.Add(new ServoActionChecker(btnBreakX1JigAlign, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.Jig_Aling));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakX1BreakAlign, GG.Equip.PROC.BreakAlign.XAxis, (int)EmBreakAlignXAxisServo.FlatnessZ));

                lstServoActChecker.Add(new ServoActionChecker(btnBreakZ1Hold, GG.Equip.PROC.BreakAlign.ZAxis, (int)EmBreakAlignZAxisServo.StandbyPos));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakZ1BreakingPos, GG.Equip.PROC.BreakAlign.ZAxis, (int)EmBreakAlignZAxisServo.Breaking));
                

                // Break유닛(T Y축)
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnBreakUnitTYA1VaccumOn, btnBreakUnitTYA1VaccumOff, GG.Equip.PROC.BreakStage_A.Vaccum1));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnBreakUnitTYA2VaccumOn, btnBreakUnitTYA2VaccumOff, GG.Equip.PROC.BreakStage_A.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakUnitTYA1BlowerOnOff, GG.Equip.PROC.BreakStage_A.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakUnitTYA2BlowerOnOff, GG.Equip.PROC.BreakStage_A.Blower2));

                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnBreakUnitTYB1VaccumOn, btnBreakUnitTYB1VaccumOff, GG.Equip.PROC.BreakStage_B.Vaccum1));
                lstSwitchSensorActChecker.Add(new SwitchSensorActionChecker(btnBreakUnitTYB2VaccumOn, btnBreakUnitTYB2VaccumOff, GG.Equip.PROC.BreakStage_B.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakUnitTYB1BlowerOnOff, GG.Equip.PROC.BreakStage_B.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBreakUnitTYB2BlowerOnOff, GG.Equip.PROC.BreakStage_B.Blower2));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakUnitTYDummyBoxOutput, btnBreakUnitTYDummyBoxInput, GG.Equip.PROC.LaserHead.DummyTankCylinder));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakUnitTYShutterClose, btnBreakUnitTYShutterOpen, GG.Equip.PROC.LaserHead.DummyShutterCylinder1));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBreakUnitTYShutterClose, btnBreakUnitTYShutterOpen, GG.Equip.PROC.LaserHead.DummyShutterCylinder2));

                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYALD, GG.Equip.PROC.BreakStage_A.YAxis, (int)EmBreakUnitYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYAULD, GG.Equip.PROC.BreakStage_A.YAxis, (int)EmBreakUnitYAxisServo.Cell_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYABreaking, GG.Equip.PROC.BreakStage_A.YAxis, (int)EmBreakUnitYAxisServo.Breaking));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYAAlignCam, GG.Equip.PROC.BreakStage_A.YAxis, (int)EmBreakUnitYAxisServo.BreakAlignCamPos));

                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYBLD, GG.Equip.PROC.BreakStage_B.YAxis, (int)EmBreakUnitYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYBULD, GG.Equip.PROC.BreakStage_B.YAxis, (int)EmBreakUnitYAxisServo.Cell_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYBBreaking, GG.Equip.PROC.BreakStage_B.YAxis, (int)EmBreakUnitYAxisServo.Breaking));
                lstServoActChecker.Add(new ServoActionChecker(btnBreakUnitTYBAlignCam, GG.Equip.PROC.BreakStage_B.YAxis, (int)EmBreakUnitYAxisServo.BreakAlignCamPos));

                // Break유닛(X Z축)
                lstServoActChecker.Add(new ServoActionChecker(btnA1Breaking, GG.Equip.PROC.BreakingHead.X1Axis, (int)EmBreakingHeadXAxisServo.Breaking_1st));
                lstServoActChecker.Add(new ServoActionChecker(btnA2Breaking, GG.Equip.PROC.BreakingHead.X1Axis, (int)EmBreakingHeadXAxisServo.Breaking_2nd));
                lstServoActChecker.Add(new ServoActionChecker(btnB1Breaking, GG.Equip.PROC.BreakingHead.X2Axis, (int)EmBreakingHeadXAxisServo.Breaking_1st));
                lstServoActChecker.Add(new ServoActionChecker(btnB2Breaking, GG.Equip.PROC.BreakingHead.X2Axis, (int)EmBreakingHeadXAxisServo.Breaking_2nd));

                lstServoActChecker.Add(new ServoActionChecker(btnAZJigAlign, GG.Equip.PROC.BreakingHead.Z1Axis, (int)EmBreakingHeadZAxisServo.Jig_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnAZDown1, GG.Equip.PROC.BreakingHead.Z1Axis, (int)EmBreakingHeadZAxisServo.BreakDown_Stage1_Stage2));
                lstServoActChecker.Add(new ServoActionChecker(btnAZDown3, GG.Equip.PROC.BreakingHead.Z1Axis, (int)EmBreakingHeadZAxisServo.BreakDown_Stage3_Stage4));

                lstServoActChecker.Add(new ServoActionChecker(btnBZJigAlign, GG.Equip.PROC.BreakingHead.Z2Axis, (int)EmBreakingHeadZAxisServo.Jig_Align));
                lstServoActChecker.Add(new ServoActionChecker(btnBZDown1, GG.Equip.PROC.BreakingHead.Z2Axis, (int)EmBreakingHeadZAxisServo.BreakDown_Stage1_Stage2));
                lstServoActChecker.Add(new ServoActionChecker(btnBZDown3, GG.Equip.PROC.BreakingHead.Z2Axis, (int)EmBreakingHeadZAxisServo.BreakDown_Stage3_Stage4));

                // Before 트랜스퍼
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBeforeA1PikerDown, btnBeforeA1PikerUp, GG.Equip.UD.BeforeInspUnloaderTransfer_A.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBeforeA2PikerDown, btnBeforeA2PikerUp, GG.Equip.UD.BeforeInspUnloaderTransfer_A.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBeforeA1PikerVaccumOn, btnBeforeA1PikerVaccumOff, GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBeforeA2PikerVaccumOn, btnBeforeA2PikerVaccumOff, GG.Equip.UD.BeforeInspUnloaderTransfer_A.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBeforeA1PikerBlowerOnOff, GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBeforeA2PikerBlowerOnOff, GG.Equip.UD.BeforeInspUnloaderTransfer_A.Blower2));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnBeforeB1PikerDown, btnBeforeB1PikerUp, GG.Equip.UD.BeforeInspUnloaderTransfer_B.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnBeforeB2PikerDown, btnBeforeB2PikerUp, GG.Equip.UD.BeforeInspUnloaderTransfer_B.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBeforeB1PikerVaccumOn, btnBeforeB1PikerVaccumOff, GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnBeforeB2PikerVaccumOn, btnBeforeB2PikerVaccumOff, GG.Equip.UD.BeforeInspUnloaderTransfer_B.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBeforeB1PikerBlowerOnOff, GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnBeforeB2PikerBlowerOnOff, GG.Equip.UD.BeforeInspUnloaderTransfer_B.Blower2));

                lstServoActChecker.Add(new ServoActionChecker(btnBeforeTr1Loading, GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis, (int)EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnBeforeTr1Insp, GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis, (int)EmBeforeInspUnloaderTransferYAxisServo.Cell_Unloading));


                lstServoActChecker.Add(new ServoActionChecker(btnBeforeTr2Loading, GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis, (int)EmBeforeInspUnloaderTransferYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnBeforeTr2Insp, GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis, (int)EmBeforeInspUnloaderTransferYAxisServo.Cell_Unloading));


                // 검사 유닛
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnInspStA1VacOn, btnInspStA1VacOff, GG.Equip.UD.InspectionStage_A.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnInspStA2VacOn, btnInspStA2VacOff, GG.Equip.UD.InspectionStage_A.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnInspStA1BlowerOnOff, GG.Equip.UD.InspectionStage_A.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnInspStA2BlowerOnOff, GG.Equip.UD.InspectionStage_A.Blower2));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnInspStB1VacOn, btnInspStB1VacOff, GG.Equip.UD.InspectionStage_B.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnInspStB2VacOn, btnInspStB2VacOff, GG.Equip.UD.InspectionStage_B.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnInspStB1BlowerOnOff, GG.Equip.UD.InspectionStage_B.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnInspStB2BlowerOnOff, GG.Equip.UD.InspectionStage_B.Blower2));

                lstServoActChecker.Add(new ServoActionChecker(btnInspStALD, GG.Equip.UD.InspectionStage_A.YAxis, (int)EmInspStageYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnInspStAULD, GG.Equip.UD.InspectionStage_A.YAxis, (int)EmInspStageYAxisServo.Cell_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnInspStAInsp, GG.Equip.UD.InspectionStage_A.YAxis, (int)EmInspStageYAxisServo.Cell_Insp));

                lstServoActChecker.Add(new ServoActionChecker(btnInspStBLD, GG.Equip.UD.InspectionStage_B.YAxis, (int)EmInspStageYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnInspStBULD, GG.Equip.UD.InspectionStage_B.YAxis, (int)EmInspStageYAxisServo.Cell_Unloading));
                lstServoActChecker.Add(new ServoActionChecker(btnInspStBInsp, GG.Equip.UD.InspectionStage_B.YAxis, (int)EmInspStageYAxisServo.Cell_Insp));


                // After 트랜스퍼
                lstCylinderActChecker.Add(new CylinderActionChecker(btnAfterA1PikerDown, btnAfterA1PikerUp, GG.Equip.UD.AfterInspUnloaderTransfer_A.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnAfterA2PikerDown, btnAfterA2PikerUp, GG.Equip.UD.AfterInspUnloaderTransfer_A.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnAfterA1PikerVaccumOn, btnAfterA1PikerVaccumOff, GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnAfterA2PikerVaccumOn, btnAfterA2PikerVaccumOff, GG.Equip.UD.AfterInspUnloaderTransfer_A.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnAfterA1PikerBlowerOnOff, GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnAfterA2PikerBlowerOnOff, GG.Equip.UD.AfterInspUnloaderTransfer_A.Blower2));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnAfterB1PikerDown, btnAfterB1PikerUp, GG.Equip.UD.AfterInspUnloaderTransfer_B.UpDown1Cylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnAfterB2PikerDown, btnAfterB2PikerUp, GG.Equip.UD.AfterInspUnloaderTransfer_B.UpDown2Cylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnAfterB1PikerVaccumOn, btnAfterB1PikerVaccumOff, GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum1));
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnAfterB2PikerVaccumOn, btnAfterB2PikerVaccumOff, GG.Equip.UD.AfterInspUnloaderTransfer_B.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnAfterB1PikerBlowerOnOff, GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower1));
                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnAfterB2PikerBlowerOnOff, GG.Equip.UD.AfterInspUnloaderTransfer_B.Blower2));

                lstServoActChecker.Add(new ServoActionChecker(btnAfterTr1Insp, GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis, (int)EmAfterInspUnloaderTransferYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnAfterTr1Unloading, GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis, (int)EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading));

                //lstServoActChecker.Add(new ServoActionChecker(btnAfterTr1A0dgr, GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis, (int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr));
                //lstServoActChecker.Add(new ServoActionChecker(btnAfterTr1A0dgr, GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis, (int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr));
                //lstServoActChecker.Add(new ServoActionChecker(btnAfterTr1A0dgr, GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis, (int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr));

                lstServoActChecker.Add(new ServoActionChecker(btnAfterTr2Insp, GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis, (int)EmAfterInspUnloaderTransferYAxisServo.Cell_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnAfterTr2Unloading, GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis, (int)EmAfterInspUnloaderTransferYAxisServo.Cell_Unloading));

                //lstServoActChecker.Add(new ServoActionChecker(btnAfterTr2B0dgr, GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis, (int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr));
                //lstServoActChecker.Add(new ServoActionChecker(btnAfterTr2B0dgr, GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis, (int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr));
                //lstServoActChecker.Add(new ServoActionChecker(btnAfterTr2B0dgr, GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis, (int)EmAfterInspUnloaderTransferTAxisServo.Piker_0dgr));


                // 카메라 유닛
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitAInspLowStart, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.A_Lower_Start));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitAInspLowEnd, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.A_Lower_End));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitAInspUpStart, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.A_Upper_Start));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitAInspUpEnd, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.A_Upper_End));

                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitBInspLowStart, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.B_Lower_Start));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitBInspLowEnd, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.B_Lower_End));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitBInspUpStart, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.B_Upper_Start));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitBInspUpEnd, GG.Equip.UD.InspCamera.XAxis, (int)EmInspCameraXAxisServo.B_Upper_End));

                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitA1, GG.Equip.UD.InspCamera.ZAxis, (int)EmInspCameraZAxisServo.A_1_InspZ1)); //오정석   추가확인 필요
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitA2, GG.Equip.UD.InspCamera.ZAxis, (int)EmInspCameraZAxisServo.A_2_InspZ1));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitB1, GG.Equip.UD.InspCamera.ZAxis, (int)EmInspCameraZAxisServo.B_1_InspZ1));
                lstServoActChecker.Add(new ServoActionChecker(btnCamUnitB2, GG.Equip.UD.InspCamera.ZAxis, (int)EmInspCameraZAxisServo.B_2_InspZ1));

                // 셀 투입 이재기 (이오나이저는??)
                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnCellInputAVaccumOn, btnCellInputAVaccumOff, GG.Equip.UD.Unloader.Vaccum1));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnCellInputABlowerOnOff, GG.Equip.UD.Unloader.Blower1));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnCellInputBVaccumOn, btnCellInputBVaccumOff, GG.Equip.UD.Unloader.Vaccum2));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnCellInputBBlowerOnOff, GG.Equip.UD.Unloader.Blower2));

                lstCylinderActChecker.Add(new CylinderActionChecker(btnCellInputBufferPickerDown, btnCellInputBufferPickerUp, GG.Equip.UD.CellBuffer.BufferCylinder));

                lstSwitch2Cmd1SensorActChecker.Add(new Switch2Cmd1SensorActionChecker(btnCellInputBufferVaccumOn, btnCellInputBufferVaccumOff, GG.Equip.UD.CellBuffer.Vaccum1));

                lstSwitchOneActChecker.Add(new SwitchOneWayActionChecker(btnCellInputBufferBlowerOnOff, GG.Equip.UD.CellBuffer.Blower1));


                lstServoActChecker.Add(new ServoActionChecker(btnCellInputA1MiddleA, GG.Equip.UD.Unloader.X1Axis, (int)EmUnLoaderXAxisServo.Cst_A_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputA1HoldA, GG.Equip.UD.Unloader.X1Axis, (int)EmUnLoaderXAxisServo.Cst_A_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputA1LoadingA, GG.Equip.UD.Unloader.X1Axis, (int)EmUnLoaderXAxisServo.Cell_A_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputA1MiddleB, GG.Equip.UD.Unloader.X1Axis, (int)EmUnLoaderXAxisServo.Cst_B_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputA1HoldB, GG.Equip.UD.Unloader.X1Axis, (int)EmUnLoaderXAxisServo.Cst_B_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputA1LoadingB, GG.Equip.UD.Unloader.X1Axis, (int)EmUnLoaderXAxisServo.Cell_B_Loading));

                lstServoActChecker.Add(new ServoActionChecker(btnCellInputB1MiddleA, GG.Equip.UD.Unloader.X2Axis, (int)EmUnLoaderXAxisServo.Cst_A_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputB1HoldA, GG.Equip.UD.Unloader.X2Axis, (int)EmUnLoaderXAxisServo.Cst_A_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputB1LoadingA, GG.Equip.UD.Unloader.X2Axis, (int)EmUnLoaderXAxisServo.Cell_A_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputB1MiddleB, GG.Equip.UD.Unloader.X2Axis, (int)EmUnLoaderXAxisServo.Cst_B_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputB1HoldB, GG.Equip.UD.Unloader.X2Axis, (int)EmUnLoaderXAxisServo.Cst_B_Wait));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputB1LoadingB, GG.Equip.UD.Unloader.X2Axis, (int)EmUnLoaderXAxisServo.Cell_B_Loading));


                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY1CellLD, GG.Equip.UD.Unloader.Y1Axis, (int)EmUnLoaderYAxisServo.Cell_A_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY1CellULD, GG.Equip.UD.Unloader.Y1Axis, (int)EmUnLoaderYAxisServo.Cell_B_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY1CstMid, GG.Equip.UD.Unloader.Y1Axis, (int)EmUnLoaderYAxisServo.Cst_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY1ACstInner, GG.Equip.UD.Unloader.Y1Axis, (int)EmUnLoaderYAxisServo.Cst_A_Inner));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY1BCstInner, GG.Equip.UD.Unloader.Y1Axis, (int)EmUnLoaderYAxisServo.Cst_B_Inner));

                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY2CellLD, GG.Equip.UD.Unloader.Y2Axis, (int)EmUnLoaderYAxisServo.Cell_A_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY2CellULD, GG.Equip.UD.Unloader.Y2Axis, (int)EmUnLoaderYAxisServo.Cell_B_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY2CstMid, GG.Equip.UD.Unloader.Y2Axis, (int)EmUnLoaderYAxisServo.Cst_Middle));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY2ACstInner, GG.Equip.UD.Unloader.Y2Axis, (int)EmUnLoaderYAxisServo.Cst_A_Inner));
                lstServoActChecker.Add(new ServoActionChecker(btnCellInputY2BCstInner, GG.Equip.UD.Unloader.Y2Axis, (int)EmUnLoaderYAxisServo.Cst_B_Inner));



                lstCylinderActChecker.Add(new CylinderActionChecker(btnCasseteUnloadATopCstGrib, btnCasseteUnloadATopCstUngrib, GG.Equip.UD.CstUnloader_A.CstGripCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnCasseteUnloadALiftTiltSlinderDown, btnCasseteUnloadALiftTiltSlinderUp, GG.Equip.UD.CstUnloader_A.TiltCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnCasseteUnloadBTopCstGrib, btnCasseteUnloadBTopCstUngrib, GG.Equip.UD.CstUnloader_B.CstGripCylinder));
                lstCylinderActChecker.Add(new CylinderActionChecker(btnCasseteUnloadBLiftTiltSlinderDown, btnCasseteUnloadBLiftTiltSlinderUp, GG.Equip.UD.CstUnloader_B.TiltCylinder));

                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadA0dgr, GG.Equip.UD.CstUnloader_A.CstRotationAxis, (int)EmCstUnloaderRotationUpAxisServo.R_0dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadA90dgr, GG.Equip.UD.CstUnloader_A.CstRotationAxis, (int)EmCstUnloaderRotationUpAxisServo.R_90dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadALoader, GG.Equip.UD.CstUnloader_A.CstUpDownAxis, (int)EmCstUnloaderUpDownAxisServo.Cst_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadAMeasure, GG.Equip.UD.CstUnloader_A.CstUpDownAxis, (int)EmCstUnloaderUpDownAxisServo.Cst_Measure));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadACellOut, GG.Equip.UD.CstUnloader_A.CstUpDownAxis, (int)EmCstUnloaderUpDownAxisServo.Cst_CellOut));

                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadB0dgr, GG.Equip.UD.CstUnloader_B.CstRotationAxis, (int)EmCstUnloaderRotationUpAxisServo.R_0dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadB90dgr, GG.Equip.UD.CstUnloader_B.CstRotationAxis, (int)EmCstUnloaderRotationUpAxisServo.R_90dgr));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadBLoader, GG.Equip.UD.CstUnloader_B.CstUpDownAxis, (int)EmCstUnloaderUpDownAxisServo.Cst_Loading));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadBMeasure, GG.Equip.UD.CstUnloader_B.CstUpDownAxis, (int)EmCstUnloaderUpDownAxisServo.Cst_Measure));
                lstServoActChecker.Add(new ServoActionChecker(btnCstunloadBCellOut, GG.Equip.UD.CstUnloader_B.CstUpDownAxis, (int)EmCstUnloaderUpDownAxisServo.Cst_CellOut));
            }


            List<Color> colors = new List<Color>();
            colors.Add(Color.LightPink);
            colors.Add(Color.Aquamarine);
            colors.Add(Color.LightCyan);
            colors.Add(Color.LightSkyBlue);


            foreach (var posi in servo.Setting.LstServoPosiInfo)
            {
                int iRow = dgv.Rows.Add(new string[] { posi.Name, posi.Servo.OutterAxisNo.ToString(), posi.Position.ToString(), posi.Speed.ToString() });
                dgv.Rows[iRow].Tag = posi;

                if (cnt == 0 || cnt == posi.Servo.OutterAxisNo)
                {
                    dgv.Rows[iRow].DefaultCellStyle.BackColor = colors[colorccnt];
                    cnt = posi.Servo.OutterAxisNo;
                }
                else
                {
                    colorccnt++;
                    if (colorccnt == colors.Count) colorccnt = 0;
                    dgv.Rows[iRow].DefaultCellStyle.BackColor = colors[colorccnt];
                    cnt = posi.Servo.OutterAxisNo;
                }
            }
        }

        private void btnCstLoad_Click(object sender, EventArgs e)
        {
            dgvSevoPosi.Rows.Clear();
            DataGridViewRow style = new DataGridViewRow();
            if (btnCstLoad == (Button)sender)
            {
                // 축 셀에 대한 색깔을 바까야됨
                Fill(dgvSevoPosi, GG.Equip.LD.CstLoader_A.CstRotationAxis);
                Fill(dgvSevoPosi, GG.Equip.LD.CstLoader_B.CstRotationAxis);
                Fill(dgvSevoPosi, GG.Equip.LD.CstLoader_A.CstUpDownAxis);
                Fill(dgvSevoPosi, GG.Equip.LD.CstLoader_B.CstUpDownAxis);
                this.tabControl1.SelectedTab = this.tabPage1;
            }
            else if (btnCellOutTransfer == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.LD.Loader.X1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.Loader.X2Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.Loader.Y1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.Loader.Y2Axis);
                this.tabControl1.SelectedTab = this.tabPage2;
            }
            else if (btnCellLoadTransfer01 == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_A.X1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_A.X2Axis);
           
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_B.X1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_B.X2Axis);
           
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_A.YAxis);
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_B.YAxis);
           
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_A.SubY1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_A.SubY2Axis);
           
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_B.SubY1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_B.SubY2Axis);
           
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_A.SubT1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_A.SubT2Axis);
           
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_B.SubT1Axis);
                Fill(dgvSevoPosi, GG.Equip.LD.LoaderTransfer_B.SubT2Axis);
           
                Fill(dgvSevoPosi, GG.Equip.LD.PreAlign.XAxis);
                this.tabControl1.SelectedTab = this.tabPage3;
            }
            else if (btnIRCutProcess == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.PROC.IRCutStage_A.YAxis);
           
                Fill(dgvSevoPosi, GG.Equip.PROC.IRCutStage_B.YAxis);
           
                Fill(dgvSevoPosi, GG.Equip.PROC.FineAlign.XAxis);
           
                Fill(dgvSevoPosi, GG.Equip.PROC.LaserHead.XAxis);
                Fill(dgvSevoPosi, GG.Equip.PROC.LaserHead.ZAxis);
                this.tabControl1.SelectedTab = this.tabPage4;
            }
            else if (btnAfterIRCutTransfer == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.PROC.AfterIRCutTransfer_A.YAxis);
                Fill(dgvSevoPosi, GG.Equip.PROC.AfterIRCutTransfer_B.YAxis);
                this.tabControl1.SelectedTab = this.tabPage5;
            }
            else if (btnBreakUnit01 == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakAlign.XAxis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakAlign.ZAxis);
                this.tabControl1.SelectedTab = this.tabPage6;
            }
            else if (btnBreakUnit02 == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_A.YAxis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_B.YAxis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_A.SubX1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_A.SubX2Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_B.SubX1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_B.SubX2Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_A.SubY1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_A.SubY2Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_B.SubY1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_B.SubY2Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_A.SubT1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_A.SubT2Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_B.SubT1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakStage_B.SubT2Axis);
                this.tabControl1.SelectedTab = this.tabPage7;
            }
            else if (btnBreakHeadUnitXZ == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakingHead.X1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakingHead.X2Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakingHead.Z1Axis);
                Fill(dgvSevoPosi, GG.Equip.PROC.BreakingHead.Z2Axis);
                this.tabControl1.SelectedTab = this.tabPage8;
            }
            else if (btnBeforeTransfer == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis);
                this.tabControl1.SelectedTab = this.tabPage9;
            }
            else if (btnInspStage == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.UD.InspectionStage_A.YAxis);
                Fill(dgvSevoPosi, GG.Equip.UD.InspectionStage_B.YAxis);
                this.tabControl1.SelectedTab = this.tabPage10;
            }
            else if (btnAfterTransfer == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis);
           
                Fill(dgvSevoPosi, GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.AfterInspUnloaderTransfer_A.T2Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.AfterInspUnloaderTransfer_B.T2Axis);
                this.tabControl1.SelectedTab = this.tabPage11;
            }
            else if (btnCameraUnit == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.UD.InspCamera.XAxis);
                Fill(dgvSevoPosi, GG.Equip.UD.InspCamera.ZAxis);
                //Fill(dgvSevoPosi, GG.Equip.UD.BotUnloaderTransfer.Y1Axis);
                //Fill(dgvSevoPosi, GG.Equip.UD.TopUnloaderTransfer.Y2Axis);
                this.tabControl1.SelectedTab = this.tabPage12;
            }
            else if (btnCellInTransfer == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.UD.Unloader.X1Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.Unloader.X2Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.Unloader.Y1Axis);
                Fill(dgvSevoPosi, GG.Equip.UD.Unloader.Y2Axis);
                this.tabControl1.SelectedTab = this.tabPage13;
            }
            else if (btnCstUnloader == (Button)sender)
            {
                Fill(dgvSevoPosi, GG.Equip.UD.CstUnloader_A.CstRotationAxis);
                Fill(dgvSevoPosi, GG.Equip.UD.CstUnloader_B.CstRotationAxis);
           
                Fill(dgvSevoPosi, GG.Equip.UD.CstUnloader_B.CstUpDownAxis);
                Fill(dgvSevoPosi, GG.Equip.UD.CstUnloader_A.CstUpDownAxis);
                this.tabControl1.SelectedTab = this.tabPage14;
            }


            btnCstLoad.BackColor = btnCstLoad == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCellOutTransfer.BackColor = btnCellOutTransfer == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCellLoadTransfer01.BackColor = btnCellLoadTransfer01 == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnIRCutProcess.BackColor = btnIRCutProcess == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnAfterIRCutTransfer.BackColor = btnAfterIRCutTransfer == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakUnit01.BackColor = btnBreakUnit01 == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakUnit02.BackColor = btnBreakUnit02 == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBeforeTransfer.BackColor = btnBeforeTransfer == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnInspStage.BackColor = btnInspStage == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnAfterTransfer.BackColor = btnAfterTransfer == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCameraUnit.BackColor = btnCameraUnit == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCellInTransfer.BackColor = btnCellInTransfer == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCstUnloader.BackColor = btnCstUnloader == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnBreakHeadUnitXZ.BackColor = btnBreakHeadUnitXZ == sender ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
        }
        private void dgvSevoPosi_SelectionChanged(object sender, EventArgs e)
        {

            if (dgvSevoPosi.SelectedRows.Count <= 0) return;

            ServoPosiInfo servoPosi = dgvSevoPosi.SelectedRows[0].Tag as ServoPosiInfo;
            if (servoPosi == null) return;

            _selectedServoPosi = servoPosi;
            ucrlServoCtrl.Servo = servoPosi.Servo;
            txtSelectedServoNo.Text = servoPosi.Servo.OutterAxisNo.ToString();
            txtSelectedServoNoCurrPosi.Text = servoPosi.Servo.XF_CurrMotorPosition.ToString();
            txtSelectedServoNoSetSpeed.Text = servoPosi.Speed.ToString();
            txtSelectedServoNoSetPosi.Text = servoPosi.Position.ToString();
        }
        private void btnCellInputMoveLocation_Click(object sender, EventArgs e)
        {
            float posi = 0;
            float speed = 0;
            if (float.TryParse(txtSelectedServoNoSetPosi.Text, out posi) && float.TryParse(txtSelectedServoNoSetSpeed.Text, out speed))
            {
                _selectedServoPosi.Servo.PtpMoveCmd(GG.Equip, posi, speed, 0);
            }
        }

        private void btnCellInputLocationSetting_Click(object sender, EventArgs e)
        {
            SetServoPosion();
        }
        private void btnCellInputSpeedSetting_Click(object sender, EventArgs e)
        {
            SetServoSpeed();
        }
        private void btnCellInputAllSetting_Click(object sender, EventArgs e)
        {
            SetServoPosion();
            SetServoSpeed();
        }
        private void SetServoPosion()
        {
            float posi = 0;
            if (float.TryParse(txtSelectedServoNoSetPosi.Text, out posi))
            {
                foreach (DataGridViewRow row in dgvSevoPosi.Rows)
                {
                    if ((ServoPosiInfo)row.Tag == _selectedServoPosi)
                    {
                        row.Cells["Position"].Value = posi.ToString();
                        break;
                    }
                }
            }
        }
        private void SetServoSpeed()
        {
            float speed = 0;
            if (float.TryParse(txtSelectedServoNoSetSpeed.Text, out speed))
            {
                foreach (DataGridViewRow row in dgvSevoPosi.Rows)
                {
                    if ((ServoPosiInfo)row.Tag == _selectedServoPosi)
                    {
                        row.Cells["speed"].Value = speed.ToString();
                        break;
                    }
                }
            }
        }

        private void btnCasseteLoadSave_Click(object sender, EventArgs e)
        {
            List<ServoMotorControl> lstServo = new List<ServoMotorControl>();
            float posi = 0;
            if (float.TryParse(txtSelectedServoNoSetPosi.Text, out posi))
            {
                foreach (DataGridViewRow row in dgvSevoPosi.Rows)
                {
                    ServoPosiInfo servoPosi = (ServoPosiInfo)row.Tag;
                    if (servoPosi == null) continue;

                    servoPosi.Position = float.Parse(row.Cells["Position"].Value.ToString());
                    servoPosi.Speed = float.Parse(row.Cells["Speed"].Value.ToString());

                    if (lstServo.Contains(servoPosi.Servo) == false)
                        lstServo.Add(servoPosi.Servo);
                }
            }

            lstServo.ForEach(f => f.Setting.Save());
        }
        public void UIUpdate()
        {
            ucrlServoCtrl.UIUpdate();

            if (lstServoActChecker != null) lstServoActChecker.ForEach(f => f.UIUpdate());
            if (lstCylinderActChecker != null) lstCylinderActChecker.ForEach(f => f.UIUpdate());
            if (lstSwitch2Cmd1SensorActChecker != null) lstSwitch2Cmd1SensorActChecker.ForEach(f => f.UIUpdate());
            if (lstSwitchOneActChecker != null) lstSwitchOneActChecker.ForEach(f => f.UIUpdate());
            if (lstSwitchSensorActChecker != null) lstSwitchSensorActChecker.ForEach(f => f.UIUpdate());
        }
    }
}

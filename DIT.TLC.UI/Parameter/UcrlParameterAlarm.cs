﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlParameterAlarm : UserControl
    {
        public UcrlParameterAlarm()
        {
            InitializeComponent();

            LoadAlarmList();
        }

        private void LoadAlarmList()
        {
            AlarmManager.Instance.LoadAlarmSettingList(dgvAlarmSetting);
        }
        private void listAlarm_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 3) return;

            if (e.RowIndex < 0) return;

            if (dgvAlarmSetting.Rows[e.RowIndex].Cells[1].Value == null) return;

            if (dgvAlarmSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor != Color.LightGray)
            {
                dgvAlarmSetting.Rows[e.RowIndex].Cells[3].Value = false;
                dgvAlarmSetting.Rows[e.RowIndex].Cells[4].Value = false;
                dgvAlarmSetting.Rows[e.RowIndex].Cells[5].Value = false;

                dgvAlarmSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
            }
        }
        private void btnlAlarmSettingSave_Click(object sender, EventArgs e)
        {
            //if (PrivilegeMgr.Instance.LevelCheck(EM_LV_LST.MASTER) == false) return;

            AlarmManager.Instance.AlarmStateChange(dgvAlarmSetting);


            if (AlarmManager.Instance.SaveAlarmINIFile(dgvAlarmSetting) == true)
            {
                //CheckMgr.AddCheckMsg(true, "Alarm setting saved.");
                MessageBox.Show("SAVE!!");
            }
            else
            {
                //CheckMgr.AddCheckMsg(false, "Failed to save alarm setting.");
                MessageBox.Show("Fail!!");
            }
        }
    }
}

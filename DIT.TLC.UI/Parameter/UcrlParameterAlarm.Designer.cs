﻿namespace DIT.TLC.UI
{
    partial class UcrlParameterAlarm
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblAlarmSettingName = new System.Windows.Forms.Label();
            this.dgvAlarmSetting = new System.Windows.Forms.DataGridView();
            this.code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state_heavy = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.state_warn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.state_unused = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnAlarmSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAlarmSettingName
            // 
            this.lblAlarmSettingName.AutoEllipsis = true;
            this.lblAlarmSettingName.BackColor = System.Drawing.Color.Gainsboro;
            this.lblAlarmSettingName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblAlarmSettingName.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAlarmSettingName.ForeColor = System.Drawing.Color.Black;
            this.lblAlarmSettingName.Location = new System.Drawing.Point(0, 0);
            this.lblAlarmSettingName.Name = "lblAlarmSettingName";
            this.lblAlarmSettingName.Size = new System.Drawing.Size(1735, 48);
            this.lblAlarmSettingName.TabIndex = 10;
            this.lblAlarmSettingName.Text = "■ Alarm Setting";
            this.lblAlarmSettingName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvAlarmSetting
            // 
            this.dgvAlarmSetting.AllowUserToAddRows = false;
            this.dgvAlarmSetting.AllowUserToResizeColumns = false;
            this.dgvAlarmSetting.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAlarmSetting.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAlarmSetting.ColumnHeadersHeight = 40;
            this.dgvAlarmSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAlarmSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.code,
            this.name,
            this.desc,
            this.state_heavy,
            this.state_warn,
            this.state_unused});
            this.dgvAlarmSetting.Location = new System.Drawing.Point(5, 51);
            this.dgvAlarmSetting.Name = "dgvAlarmSetting";
            this.dgvAlarmSetting.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAlarmSetting.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAlarmSetting.RowHeadersVisible = false;
            this.dgvAlarmSetting.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            this.dgvAlarmSetting.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAlarmSetting.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dgvAlarmSetting.RowTemplate.Height = 27;
            this.dgvAlarmSetting.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvAlarmSetting.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAlarmSetting.Size = new System.Drawing.Size(1727, 746);
            this.dgvAlarmSetting.TabIndex = 11;
            this.dgvAlarmSetting.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listAlarm_CellContentClick);
            // 
            // code
            // 
            this.code.HeaderText = "CODE";
            this.code.Name = "code";
            this.code.ReadOnly = true;
            this.code.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.code.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.code.Width = 150;
            // 
            // name
            // 
            this.name.HeaderText = "NAME";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.name.Width = 565;
            // 
            // desc
            // 
            this.desc.HeaderText = "DESCRIPTION";
            this.desc.Name = "desc";
            this.desc.ReadOnly = true;
            this.desc.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.desc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.desc.Width = 615;
            // 
            // state_heavy
            // 
            this.state_heavy.HeaderText = "HEAVY";
            this.state_heavy.Name = "state_heavy";
            this.state_heavy.ReadOnly = true;
            this.state_heavy.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.state_heavy.Width = 135;
            // 
            // state_warn
            // 
            this.state_warn.HeaderText = "WARN";
            this.state_warn.Name = "state_warn";
            this.state_warn.ReadOnly = true;
            this.state_warn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.state_warn.Width = 135;
            // 
            // state_unused
            // 
            this.state_unused.HeaderText = "UNUSED";
            this.state_unused.Name = "state_unused";
            this.state_unused.ReadOnly = true;
            this.state_unused.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.state_unused.Width = 135;
            // 
            // btnAlarmSave
            // 
            this.btnAlarmSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlarmSave.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlarmSave.ForeColor = System.Drawing.Color.Black;
            this.btnAlarmSave.Location = new System.Drawing.Point(1499, 803);
            this.btnAlarmSave.Name = "btnAlarmSave";
            this.btnAlarmSave.Size = new System.Drawing.Size(233, 55);
            this.btnAlarmSave.TabIndex = 68;
            this.btnAlarmSave.Text = "SAVE";
            this.btnAlarmSave.UseVisualStyleBackColor = false;
            // 
            // UcrlParameterAlarm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.btnAlarmSave);
            this.Controls.Add(this.dgvAlarmSetting);
            this.Controls.Add(this.lblAlarmSettingName);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlParameterAlarm";
            this.Size = new System.Drawing.Size(1735, 870);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmSetting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblAlarmSettingName;
        private System.Windows.Forms.DataGridView dgvAlarmSetting;
        private System.Windows.Forms.DataGridViewTextBoxColumn code;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn desc;
        private System.Windows.Forms.DataGridViewCheckBoxColumn state_heavy;
        private System.Windows.Forms.DataGridViewCheckBoxColumn state_warn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn state_unused;
        private System.Windows.Forms.Button btnAlarmSave;
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlParameterSubMenu
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnUserManager = new System.Windows.Forms.Button();
            this.btnSystem = new System.Windows.Forms.Button();
            this.btnAxis = new System.Windows.Forms.Button();
            this.btnSequenceOffset = new System.Windows.Forms.Button();
            this.btnLocation = new System.Windows.Forms.Button();
            this.btnAlarmSetting = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.btnAlarmSetting);
            this.splitContainer1.Panel2.Controls.Add(this.btnUserManager);
            this.splitContainer1.Panel2.Controls.Add(this.btnSystem);
            this.splitContainer1.Panel2.Controls.Add(this.btnAxis);
            this.splitContainer1.Panel2.Controls.Add(this.btnSequenceOffset);
            this.splitContainer1.Panel2.Controls.Add(this.btnLocation);
            this.splitContainer1.Panel2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 1;
            // 
            // btnUserManager
            // 
            this.btnUserManager.BackColor = System.Drawing.SystemColors.Control;
            this.btnUserManager.ForeColor = System.Drawing.Color.Black;
            this.btnUserManager.Location = new System.Drawing.Point(2, 303);
            this.btnUserManager.Name = "btnUserManager";
            this.btnUserManager.Size = new System.Drawing.Size(132, 50);
            this.btnUserManager.TabIndex = 51;
            this.btnUserManager.Text = "사용자 매니저";
            this.btnUserManager.UseVisualStyleBackColor = false;
            this.btnUserManager.Click += new System.EventHandler(this.btnUserManager_Click);
            // 
            // btnSystem
            // 
            this.btnSystem.BackColor = System.Drawing.SystemColors.Control;
            this.btnSystem.ForeColor = System.Drawing.Color.Black;
            this.btnSystem.Location = new System.Drawing.Point(2, 191);
            this.btnSystem.Name = "btnSystem";
            this.btnSystem.Size = new System.Drawing.Size(132, 50);
            this.btnSystem.TabIndex = 50;
            this.btnSystem.Text = "시스템 파라미터";
            this.btnSystem.UseVisualStyleBackColor = false;
            this.btnSystem.Click += new System.EventHandler(this.btnSystem_Click);
            // 
            // btnAxis
            // 
            this.btnAxis.BackColor = System.Drawing.SystemColors.Control;
            this.btnAxis.ForeColor = System.Drawing.Color.Black;
            this.btnAxis.Location = new System.Drawing.Point(2, 135);
            this.btnAxis.Name = "btnAxis";
            this.btnAxis.Size = new System.Drawing.Size(132, 50);
            this.btnAxis.TabIndex = 49;
            this.btnAxis.Text = "축 파라미터";
            this.btnAxis.UseVisualStyleBackColor = false;
            this.btnAxis.Click += new System.EventHandler(this.btnAxis_Click);
            // 
            // btnSequenceOffset
            // 
            this.btnSequenceOffset.BackColor = System.Drawing.SystemColors.Control;
            this.btnSequenceOffset.ForeColor = System.Drawing.Color.Black;
            this.btnSequenceOffset.Location = new System.Drawing.Point(2, 79);
            this.btnSequenceOffset.Name = "btnSequenceOffset";
            this.btnSequenceOffset.Size = new System.Drawing.Size(132, 50);
            this.btnSequenceOffset.TabIndex = 48;
            this.btnSequenceOffset.Text = "시퀀스 Offset";
            this.btnSequenceOffset.UseVisualStyleBackColor = false;
            this.btnSequenceOffset.Click += new System.EventHandler(this.btnSequenceOffset_Click);
            // 
            // btnLocation
            // 
            this.btnLocation.BackColor = System.Drawing.SystemColors.Control;
            this.btnLocation.ForeColor = System.Drawing.Color.Black;
            this.btnLocation.Location = new System.Drawing.Point(2, 23);
            this.btnLocation.Name = "btnLocation";
            this.btnLocation.Size = new System.Drawing.Size(132, 50);
            this.btnLocation.TabIndex = 47;
            this.btnLocation.Text = "위치 파라미터";
            this.btnLocation.UseVisualStyleBackColor = false;
            this.btnLocation.Click += new System.EventHandler(this.btnLocation_Click);
            // 
            // btnAlarmSetting
            // 
            this.btnAlarmSetting.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlarmSetting.ForeColor = System.Drawing.Color.Black;
            this.btnAlarmSetting.Location = new System.Drawing.Point(2, 247);
            this.btnAlarmSetting.Name = "btnAlarmSetting";
            this.btnAlarmSetting.Size = new System.Drawing.Size(132, 50);
            this.btnAlarmSetting.TabIndex = 52;
            this.btnAlarmSetting.Text = "알람 셋팅";
            this.btnAlarmSetting.UseVisualStyleBackColor = false;
            this.btnAlarmSetting.Click += new System.EventHandler(this.btnAlarmSetting_Click);
            // 
            // UcrlParameterSubMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "UcrlParameterSubMenu";
            this.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnUserManager;
        private System.Windows.Forms.Button btnSystem;
        private System.Windows.Forms.Button btnAxis;
        private System.Windows.Forms.Button btnSequenceOffset;
        private System.Windows.Forms.Button btnLocation;
        private System.Windows.Forms.Button btnAlarmSetting;
    }
}

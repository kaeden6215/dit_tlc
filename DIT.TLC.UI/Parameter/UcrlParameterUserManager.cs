﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlParameterUserManager : UserControl
    {
        public UcrlParameterUserManager()
        {
            InitializeComponent();
        }
        public void Initizlie()
        {
            FillUserList();
        }

        public void FillUserList()
        {
            lvInformation.Items.Clear();
            int iPos = 1;
            foreach (UserInfo info in GG.Equip.UserInfoMgr.LstUserInfos)
            {
                lvInformation.Items.Add(new ListViewItem(new string[] { iPos.ToString(), info.UserID, info.Grade.ToString() }));
                iPos++;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormSetUserAdd ff = new FormSetUserAdd();
            ff.StartPosition = FormStartPosition.CenterParent;
            if (ff.ShowDialog() == DialogResult.OK)
            {
                GG.Equip.UserInfoMgr.LstUserInfos.Add(new UserInfo() { UserID = ff.UserID, Password = ff.Password, Grade = ff.Grade });
                GG.Equip.UserInfoMgr.Save();
                FillUserList();
            }
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            if (lvInformation.SelectedItems.Count <= 0) return;
            string userid = lvInformation.SelectedItems[0].SubItems[1].Text;
            UserInfo info = GG.Equip.UserInfoMgr.LstUserInfos.FirstOrDefault(f => f.UserID == userid);

            FormSetUserEdit ff = new FormSetUserEdit();
            ff.StartPosition = FormStartPosition.CenterParent;
            ff.UserID = info.UserID;
            ff.Password = info.Password;
            ff.Grade = info.Grade;
            if (ff.ShowDialog() == DialogResult.OK)
            {
                info.UserID = ff.UserID;
                info.Password = ff.Password;
                info.Grade = ff.Grade;

                GG.Equip.UserInfoMgr.Save();
            }
            FillUserList();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvInformation.SelectedItems.Count <= 0) return;
            string userid = lvInformation.SelectedItems[0].SubItems[1].Text;

            if (MessageBox.Show(string.Format("{0} 사용자를 삭제합니다.", userid), "TLC 제어 프로그램", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                UserInfo info = GG.Equip.UserInfoMgr.LstUserInfos.FirstOrDefault(f => f.UserID == userid);
                GG.Equip.UserInfoMgr.LstUserInfos.Remove(info);
                GG.Equip.UserInfoMgr.Save();
                FillUserList();
            }
        }
    }
}

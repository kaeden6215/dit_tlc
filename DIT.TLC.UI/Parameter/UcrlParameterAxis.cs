﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DIT.TLC.CTRL;
using Dit.Framework.PLC;

namespace DIT.TLC.UI
{
    public partial class UcrlParameterAxis : UserControl
    {
        private ServoMotorControl _servo = null;

        public UcrlParameterAxis()
        {
            InitializeComponent();
        }

        // 클릭시 색 변경 함수
        Panel SelectedMenu = null;
        private void MenuColorChange(object sender, EventArgs e)
        {
            if (SelectedMenu != null)
                SelectedMenu.BackColor = Color.FromArgb(64, 64, 64);

            SelectedMenu = sender as Panel;
            SelectedMenu.BackColor = Color.Blue;
        }


        public void FillServoList(FlowLayoutPanel pnl, List<ServoMotorControl> lst)
        {
            List<Button> lstBtn = new List<Button>();
            lstBtn.AddRange(pnl.Controls.OfType<Button>());
            pnl.Controls.Clear();

            lstBtn.ForEach(f => f.Dispose());

            foreach (ServoMotorControl servo in lst)
            {
                Button btn = new Button() { Width = 500, Height = 35, ForeColor = Color.Black, Left = 1, Font = new Font("맑은 고딕", 9, FontStyle.Regular), Text = string.Format("{0}.{1}", servo.OutterAxisNo, servo.Name) };
                btn.Tag = servo;
                btn.Click += SelectedServo_Click;
                pnl.Controls.Add(btn);
            }
        }
        private void SelectedServo_Click(object sender, EventArgs e)
        {
            foreach (var ctrl in flpSevoMotors.Controls)
            {
                Button motorBtn = ctrl as Button;
                motorBtn.BackColor = UiGlobal.UNSELECT_C;
            }

            Button btn = sender as Button;
            btn.BackColor = UiGlobal.SELECT_C;
            _servo = (ServoMotorControl)btn.Tag;
            UIUpdate();
        }

        private void btnMonitorLoader_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btnMonitorLoader.BackColor = btnMonitorLoader == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMonitorProcess.BackColor = btnMonitorProcess == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnMonitorUnloader.BackColor = btnMonitorUnloader == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            btnAjinMotor_Click(btnAjinMotor, null);
        }
        private void btnAjinMotor_Click(object sender, EventArgs e)
        {

            Button btn = sender as Button;
            btnAjinMotor.BackColor = btnAjinMotor == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnUmacMotor.BackColor = btnUmacMotor == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnEZiMotor.BackColor = btnEZiMotor == btn ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            flpSevoMotors.Controls.Clear();

            if (btnMonitorLoader.BackColor == UiGlobal.SELECT_C)
            {
                if (btnAjinMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> {
                        GG.Equip.LD.CstLoader_B.CstRotationAxis, GG.Equip.LD.CstLoader_B.CstUpDownAxis,
                        GG.Equip.LD.CstLoader_A.CstRotationAxis, GG.Equip.LD.CstLoader_A.CstUpDownAxis,
                        GG.Equip.LD.Loader.X1Axis, GG.Equip.LD.Loader.X2Axis, GG.Equip.LD.Loader.Y1Axis,GG.Equip.LD.Loader.Y2Axis,
                        GG.Equip.LD.LoaderTransfer_B.X1Axis,GG.Equip.LD.LoaderTransfer_B.X2Axis,GG.Equip.LD.LoaderTransfer_B.YAxis,
                        GG.Equip.LD.LoaderTransfer_B.SubY1Axis, GG.Equip.LD.LoaderTransfer_B.SubY2Axis, GG.Equip.LD.LoaderTransfer_B.SubT1Axis, GG.Equip.LD.LoaderTransfer_B.SubT2Axis,
                        GG.Equip.LD.LoaderTransfer_A.X1Axis,GG.Equip.LD.LoaderTransfer_A.X2Axis,GG.Equip.LD.LoaderTransfer_A.YAxis,
                        GG.Equip.LD.LoaderTransfer_A.SubY1Axis, GG.Equip.LD.LoaderTransfer_A.SubY2Axis, GG.Equip.LD.LoaderTransfer_A.SubT1Axis,GG.Equip.LD.LoaderTransfer_A.SubT2Axis,
                        GG.Equip.LD.PreAlign.XAxis
                        });
                }
                else if (btnUmacMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }
                else if (btnEZiMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }

            }
            else if (btnMonitorProcess.BackColor == UiGlobal.SELECT_C)
            {
                if (btnAjinMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> {
                            GG.Equip.PROC.AfterIRCutTransfer_B.YAxis,
                            GG.Equip.PROC.AfterIRCutTransfer_A.YAxis});
                }
                else if (btnUmacMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> {
                            GG.Equip.PROC.FineAlign.XAxis,
                            GG.Equip.PROC.LaserHead.XAxis,GG.Equip.PROC.LaserHead.ZAxis,
                            GG.Equip.PROC.AfterIRCutTransfer_B.YAxis,
                            GG.Equip.PROC.AfterIRCutTransfer_A.YAxis,
                            GG.Equip.PROC.BreakStage_B.YAxis,
                            GG.Equip.PROC.BreakStage_A.YAxis,
                            GG.Equip.PROC.BreakingHead.X1Axis, GG.Equip.PROC.BreakingHead.X2Axis,GG.Equip.PROC.BreakingHead.Z1Axis, GG.Equip.PROC.BreakingHead.Z2Axis,
                            GG.Equip.PROC.BreakAlign.XAxis,GG.Equip.PROC.BreakAlign.ZAxis});
                }
                else if (btnEZiMotor.BackColor == UiGlobal.SELECT_C)
                {

                    FillServoList(flpSevoMotors, new List<ServoMotorControl> {
                        GG.Equip.PROC.BreakStage_B.SubY1Axis, GG.Equip.PROC.BreakStage_B.SubY2Axis, GG.Equip.PROC.BreakStage_B.SubX1Axis,
                        GG.Equip.PROC.BreakStage_B.SubX2Axis, GG.Equip.PROC.BreakStage_B.SubT1Axis, GG.Equip.PROC.BreakStage_B.SubT2Axis,
                        GG.Equip.PROC.BreakStage_A.SubY1Axis, GG.Equip.PROC.BreakStage_A.SubY2Axis, GG.Equip.PROC.BreakStage_A.SubX1Axis,
                        GG.Equip.PROC.BreakStage_A.SubX2Axis, GG.Equip.PROC.BreakStage_A.SubT1Axis, GG.Equip.PROC.BreakStage_A.SubT2Axis });

                }
            }
            else if (btnMonitorUnloader.BackColor == UiGlobal.SELECT_C)
            {
                if (btnAjinMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> {
                    GG.Equip.UD.BeforeInspUnloaderTransfer_B.Y1Axis, GG.Equip.UD.AfterInspUnloaderTransfer_B.Y2Axis, GG.Equip.UD.AfterInspUnloaderTransfer_B.T1Axis, GG.Equip.UD.AfterInspUnloaderTransfer_B.T2Axis,
                    GG.Equip.UD.BeforeInspUnloaderTransfer_A.Y1Axis, GG.Equip.UD.AfterInspUnloaderTransfer_A.Y2Axis, GG.Equip.UD.AfterInspUnloaderTransfer_A.T1Axis, GG.Equip.UD.AfterInspUnloaderTransfer_A.T2Axis,

                    GG.Equip.UD.InspCamera.XAxis, GG.Equip.UD.InspCamera.ZAxis,
                    GG.Equip.UD.InspectionStage_B.YAxis,
                    GG.Equip.UD.InspectionStage_A.YAxis,

                    GG.Equip.UD.Unloader.X1Axis, GG.Equip.UD.Unloader.X2Axis, GG.Equip.UD.Unloader.Y1Axis, GG.Equip.UD.Unloader.Y2Axis,
                    GG.Equip.UD.CstUnloader_B.CstRotationAxis, GG.Equip.UD.CstUnloader_B.CstUpDownAxis,
                    GG.Equip.UD.CstUnloader_A.CstRotationAxis, GG.Equip.UD.CstUnloader_A.CstUpDownAxis,
                    });
                }
                else if (btnUmacMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }
                else if (btnEZiMotor.BackColor == UiGlobal.SELECT_C)
                {
                    FillServoList(flpSevoMotors, new List<ServoMotorControl> { });
                }
            }

            if (flpSevoMotors.Controls.Count > 0)
                SelectedServo_Click(flpSevoMotors.Controls[0], null);

            UIUpdate();
        }
        public void UIUpdate()
        {
            if (_servo == null) return;

            pnlUmacMotorParamInfo.Visible = (_servo.MotorType == EM_MOTOR_TYPE.Umac);
            pnlAjinMotorParamInfo.Visible = (_servo.MotorType == EM_MOTOR_TYPE.Ajin);
            pnlEZiMotorParamInfo.Visible = (_servo.MotorType == EM_MOTOR_TYPE.EZi);

            if (_servo.MotorType == EM_MOTOR_TYPE.Ajin)
            {
                txtAjinAxisNo.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.AxisNo);
                cbxtAjinPulseOutput.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.PulseOutput);
                cbxtAjinEncInput.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.EncInput);
                cbxtAjinAbsRelMode.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.AbsRelMode);
                cbxtAjinVelProfileMode.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.VelProfileMode);
                txtAjinMaxVelocitu.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.MaxVelocitu);
                txtAjinMinVelocitu.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.MinVelocitu);
                txtAjinUnit.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.Unit);
                txtAjinPulse.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.Pulse);
                cbxtAjinInposition.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.Inposition);
                cbxtAjinAlarm.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.Alarm);
                cbxtAjinMinusEndLimit.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.MinusEndLimit);
                cbxtAjinPlusEndLimit.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.PlusEndLimit);
                cbxtAjinServoOnLevel.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.ServoOnLevel);
                cbxtAjinZPhase.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.ZPhaseLevel);
                cbxtAjinStopMode.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.StopMode);
                cbxtAjinStopLevel.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.StopLevel);
                cbxtAjinAlarmResetLevel.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.AlarmResetLevel);
                cbxtAjinEncoderType.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.EncoderType);
                cbxtAjinHomeSignal.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeSignal);
                cbxtAjinHomeLevel.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeLevel);
                txtAjinHomeVel1.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeVel1);
                txtAjinHomeVel2.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeVel2);
                txtAjinHomeVel3.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeVel3);
                txtAjinHomeVelLast.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeVelLast);
                cbxtAjinHomeDirection.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeDirection);
                cbxtAjinHomeZPhase.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeZPhaseType);
                txtAjinHomeAccelation1.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeAccelation1);
                txtAjinHomeAccelation2.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeAccelation2);
                txtAjinHomeClearTime.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeClearTime);
                txtAjinHomeOffset.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.HomeOffset);
                txtAjinSWMinusLimit.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.SWMinusLimit);
                txtAjinSWPlusLimit.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.SWPlusLimit);
                cbxtAjinSWLimitMode.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.SWLimitMode);
                cbxtAjinSWLimitStopMode.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.SWLimitStopMode);
                cbxtAjinSWLimitEnable.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.SWLimitEnable);
                txtAjinInitPosition.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.InitPosition);
                txtAjinInitVelocity.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.InitVelocity);
                txtAjinInitPtpSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.InitPtpSpeed);
                txtAjinInitAccelationTime.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.InitAccelationTime);
                txtAjinInitAccelation.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.InitAccelation);
                txtAjinInitDecelation.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.InitDecelation);
                txtAjinInitInposition.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmAjinParam.InitInposition);
            }

            if (_servo.MotorType == EM_MOTOR_TYPE.Umac)
            {
                txtProcessAxisNo.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.AxisNo);
                txtProcessPositionRate.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.PositionRate);
                txtProcessSpeedRate.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.SpeedRate);
                txtProcessMoveTimeOut.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.MoveTimeOut);
                txtProcessInPosition.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.InPosition);
                txtProcessDefauleVelocity.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.DefauleVelocity);
                txtProcessDefaultAcceleration.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.DefaultAcceleration);
                txtProcessHome1Velocity.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.Home1Velocity);
                txtProcessHome2Velocity.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.Home2Velocity);
                txtProcessHomeAcceleration.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.HomeAcceleration);
                txtProcessJogSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.JogSpeed);
                txtProcessStepSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.StepSpeed);
                txtProcessPtpSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.PtpSpeed);
                txtProcessSWLimitLow.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.SWLimitLow);
                txtProcessSWLimitHigh.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.SWLimitHigh);
                txtProcessAccelationTime.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmUmacParam.AccelationTime);
            }

            if (_servo.MotorType == EM_MOTOR_TYPE.EZi)
            {
                txtEziAxisNo.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.AxisNo);
                txtEziPulsePerRevolution.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.PulsePerRevolution);
                txtEziAxisMaxSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.AxisMaxSpeed);
                txtEziAxisStartSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.AxisStartSpeed);
                txtEziAxisAccSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.AxisAccTime);
                txtEziAxisDecTime.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.AxisDecTime);
                txtEziSpeedOverride.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.SpeedOverride);
                txtEziJogSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.JogSpeed);
                txtEziJogStartSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.JogStartSpeed);
                txtEziJogAccDecTime.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.JogAccDecTime);
                txtEziSWLimitPlusValue.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.SWLimitPlusValue);
                txtSWLimitMinusValue.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.SWLimitMinusValue);
                txtEziSWLimitStopMethod.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.SWLimitStopMethod);
                txtHWLimitStopMethod.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.HWLimitStopMethod);
                txtEziLimitSensorLogic.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.LimitSensorLogic);
                txtEziOrgSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgSpeed);
                txtEziOrgSearchSpeed.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgSearchSpeed);
                txtEziAccDecTime.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgAccDecTime);
                txtEziOrgMethod.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgMethod);
                txEziOrgDir.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgDir);
                txtEziOrgOffset.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgOffset);
                txtEziOrgPositionSet.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgPositionSet);
                txtEziOrgSensorLogic.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgSensorLogic);
                txtEziPositionLoopGain.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.PositionLoopGain);
                txtEziInposValue.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.InposValue);
                txtEziPosTrackingLimit.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.PosTrackingLimit);
                txtEziMotionDir.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.MotionDir);
                txtEziLimitSensorDir.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.LimitSensorDir);
                txtEziOrgTorqueRatio.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.OrgTorqueRatio);
                txtEziPosErrorOverflowLimit.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.PosErrorOverflowLimit);
                txtEziBrakeDelayTime.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.BrakeDelayTime);
                txtEziRunCurrent.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.RunCurrent);
                txtEziBoostCurrent.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.BoostCurrent);
                txtEziStopCurrent.Text = _servo.ParamSetting.GetParamStringVlaue((int)EmEziParam.StopCurrent);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // 선택안하고 Save 눌렀을 시 처리 해줘야됨
            if (_servo.MotorType == EM_MOTOR_TYPE.Ajin)
            {
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.AxisNo, txtAjinAxisNo.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.PulseOutput, cbxtAjinPulseOutput.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.EncInput, cbxtAjinEncInput.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.AbsRelMode, cbxtAjinAbsRelMode.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.VelProfileMode, cbxtAjinVelProfileMode.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.MaxVelocitu, txtAjinMaxVelocitu.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.MinVelocitu, txtAjinMinVelocitu.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.Unit, txtAjinUnit.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.Pulse, txtAjinPulse.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.Inposition, cbxtAjinInposition.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.Alarm, cbxtAjinAlarm.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.MinusEndLimit, cbxtAjinMinusEndLimit.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.PlusEndLimit, cbxtAjinPlusEndLimit.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.ServoOnLevel, cbxtAjinServoOnLevel.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.ZPhaseLevel, cbxtAjinZPhase.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.StopMode, cbxtAjinStopMode.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.StopLevel, cbxtAjinStopLevel.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.AlarmResetLevel, cbxtAjinAlarmResetLevel.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.EncoderType, cbxtAjinEncoderType.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeSignal, cbxtAjinHomeSignal.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeLevel, cbxtAjinHomeLevel.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeVel1, txtAjinHomeVel1.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeVel2, txtAjinHomeVel2.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeVel3, txtAjinHomeVel3.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeVelLast, txtAjinHomeVelLast.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeDirection, cbxtAjinHomeDirection.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeZPhaseType, cbxtAjinHomeZPhase.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeAccelation1, txtAjinHomeAccelation1.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeAccelation2, txtAjinHomeAccelation2.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeClearTime, txtAjinHomeClearTime.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.HomeOffset, txtAjinHomeOffset.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.SWMinusLimit, txtAjinSWMinusLimit.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.SWPlusLimit, txtAjinSWPlusLimit.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.SWLimitMode, cbxtAjinSWLimitMode.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.SWLimitStopMode, cbxtAjinSWLimitStopMode.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.SWLimitEnable, cbxtAjinSWLimitEnable.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.InitPosition, txtAjinInitPosition.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.InitVelocity, txtAjinInitVelocity.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.InitAccelationTime, txtAjinInitPtpSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.InitAccelationTime, txtAjinInitAccelationTime.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.InitAccelation, txtAjinInitAccelation.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.InitDecelation, txtAjinInitDecelation.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmAjinParam.InitInposition, txtAjinInitInposition.Text);

                _servo.ParamSetting.Save();
            }

            if (_servo.MotorType == EM_MOTOR_TYPE.Umac)
            {
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.AxisNo, txtProcessAxisNo.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.PositionRate, txtProcessPositionRate.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.SpeedRate, txtProcessSpeedRate.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.MoveTimeOut, txtProcessMoveTimeOut.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.InPosition, txtProcessInPosition.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.DefauleVelocity, txtProcessDefauleVelocity.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.DefaultAcceleration, txtProcessDefaultAcceleration.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.Home1Velocity, txtProcessHome1Velocity.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.Home2Velocity, txtProcessHome2Velocity.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.HomeAcceleration, txtProcessHomeAcceleration.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.JogSpeed, txtProcessJogSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.StepSpeed, txtProcessStepSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.PtpSpeed, txtProcessPtpSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.SWLimitLow, txtProcessSWLimitLow.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.SWLimitHigh, txtProcessSWLimitHigh.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmUmacParam.AccelationTime, txtProcessAccelationTime.Text);

                _servo.ParamSetting.Save();
            }

            if (_servo.MotorType == EM_MOTOR_TYPE.EZi)
            {
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.AxisNo, txtEziAxisNo.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.PulsePerRevolution, txtEziPulsePerRevolution.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.AxisMaxSpeed, txtEziAxisMaxSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.AxisStartSpeed, txtEziAxisStartSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.AxisAccTime, txtEziAxisAccSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.AxisDecTime, txtEziAxisDecTime.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.SpeedOverride, txtEziSpeedOverride.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.JogSpeed, txtEziJogSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.JogStartSpeed, txtEziJogStartSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.JogAccDecTime, txtEziJogAccDecTime.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.SWLimitPlusValue, txtEziSWLimitPlusValue.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.SWLimitMinusValue, txtSWLimitMinusValue.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.SWLimitStopMethod, txtEziSWLimitStopMethod.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.HWLimitStopMethod, txtHWLimitStopMethod.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.LimitSensorLogic, txtEziLimitSensorLogic.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgSpeed, txtEziOrgSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgSearchSpeed, txtEziOrgSearchSpeed.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgAccDecTime, txtEziAccDecTime.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgMethod, txtEziOrgMethod.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgDir, txEziOrgDir.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgOffset, txtEziOrgOffset.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgPositionSet, txtEziOrgPositionSet.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgSensorLogic, txtEziOrgSensorLogic.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.PositionLoopGain, txtEziPositionLoopGain.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.InposValue, txtEziInposValue.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.PosTrackingLimit, txtEziPosTrackingLimit.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.MotionDir, txtEziMotionDir.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.LimitSensorDir, txtEziLimitSensorDir.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.OrgTorqueRatio, txtEziOrgTorqueRatio.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.PosErrorOverflowLimit, txtEziPosErrorOverflowLimit.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.BrakeDelayTime, txtEziBrakeDelayTime.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.RunCurrent, txtEziRunCurrent.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.BoostCurrent, txtEziBoostCurrent.Text);
                _servo.ParamSetting.SetParamVlaue((int)EmEziParam.StopCurrent, txtEziStopCurrent.Text);

                _servo.ParamSetting.Save();
            }
        }
    }
}

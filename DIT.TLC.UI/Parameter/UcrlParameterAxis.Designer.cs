﻿namespace DIT.TLC.UI
{
    partial class UcrlParameterAxis
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAjinInitInPosition = new System.Windows.Forms.Label();
            this.txtAjinInitInposition = new System.Windows.Forms.TextBox();
            this.lblAjinInitDecelation = new System.Windows.Forms.Label();
            this.txtAjinInitDecelation = new System.Windows.Forms.TextBox();
            this.lblAjinInitAccelation = new System.Windows.Forms.Label();
            this.txtAjinInitAccelation = new System.Windows.Forms.TextBox();
            this.cbxtAjinSWLimitEnable = new System.Windows.Forms.ComboBox();
            this.lblAjinSWLimitEnable = new System.Windows.Forms.Label();
            this.cbxtAjinSWLimitStopMode = new System.Windows.Forms.ComboBox();
            this.lblAjinSWLimitStopMode = new System.Windows.Forms.Label();
            this.lblAjinHomeOffset = new System.Windows.Forms.Label();
            this.txtAjinHomeOffset = new System.Windows.Forms.TextBox();
            this.lblAjinHomeClearTime = new System.Windows.Forms.Label();
            this.txtAjinHomeClearTime = new System.Windows.Forms.TextBox();
            this.lblAjinHomeAccelation2 = new System.Windows.Forms.Label();
            this.txtAjinHomeAccelation2 = new System.Windows.Forms.TextBox();
            this.lblAjinHomeAccelation1 = new System.Windows.Forms.Label();
            this.txtAjinHomeAccelation1 = new System.Windows.Forms.TextBox();
            this.cbxtAjinHomeZPhase = new System.Windows.Forms.ComboBox();
            this.lblAjinHomeZPhase = new System.Windows.Forms.Label();
            this.cbxtAjinHomeDirection = new System.Windows.Forms.ComboBox();
            this.lblAjinHomeDirection = new System.Windows.Forms.Label();
            this.cbxtAjinEncoderType = new System.Windows.Forms.ComboBox();
            this.lblAjinEncoderType = new System.Windows.Forms.Label();
            this.cbxtAjinAlarmResetLevel = new System.Windows.Forms.ComboBox();
            this.lblAjinAlarmResetLevel = new System.Windows.Forms.Label();
            this.cbxtAjinStopLevel = new System.Windows.Forms.ComboBox();
            this.lblAjinStopLevel = new System.Windows.Forms.Label();
            this.cbxtAjinStopMode = new System.Windows.Forms.ComboBox();
            this.lblAjinStopMode = new System.Windows.Forms.Label();
            this.cbxtAjinZPhase = new System.Windows.Forms.ComboBox();
            this.lblAjinZPhase = new System.Windows.Forms.Label();
            this.lblAjinPulse = new System.Windows.Forms.Label();
            this.txtAjinPulse = new System.Windows.Forms.TextBox();
            this.lblAjinUnit = new System.Windows.Forms.Label();
            this.txtAjinUnit = new System.Windows.Forms.TextBox();
            this.lblAjinMinVelocitu = new System.Windows.Forms.Label();
            this.txtAjinMinVelocitu = new System.Windows.Forms.TextBox();
            this.lblAjinMaxVelocitu = new System.Windows.Forms.Label();
            this.txtAjinMaxVelocitu = new System.Windows.Forms.TextBox();
            this.lblAjinInitAccelationTime = new System.Windows.Forms.Label();
            this.txtAjinInitAccelationTime = new System.Windows.Forms.TextBox();
            this.lblAjinInitPtpSpeed = new System.Windows.Forms.Label();
            this.txtAjinInitPtpSpeed = new System.Windows.Forms.TextBox();
            this.lblAjinInitVelocity = new System.Windows.Forms.Label();
            this.txtAjinInitVelocity = new System.Windows.Forms.TextBox();
            this.lblAjinInitPosition = new System.Windows.Forms.Label();
            this.txtAjinInitPosition = new System.Windows.Forms.TextBox();
            this.lblAjinInit = new System.Windows.Forms.Label();
            this.cbxtAjinSWLimitMode = new System.Windows.Forms.ComboBox();
            this.lblAjinSWLimitMode = new System.Windows.Forms.Label();
            this.lblAjinSWPlusLimit = new System.Windows.Forms.Label();
            this.txtAjinSWPlusLimit = new System.Windows.Forms.TextBox();
            this.lblAjinSWMinusLimit = new System.Windows.Forms.Label();
            this.txtAjinSWMinusLimit = new System.Windows.Forms.TextBox();
            this.lblAjinSW = new System.Windows.Forms.Label();
            this.lblAjinHomeVelLast = new System.Windows.Forms.Label();
            this.txtAjinHomeVelLast = new System.Windows.Forms.TextBox();
            this.lblAjinHomeVel3 = new System.Windows.Forms.Label();
            this.txtAjinHomeVel3 = new System.Windows.Forms.TextBox();
            this.lblAjinHomeVel2 = new System.Windows.Forms.Label();
            this.txtAjinHomeVel2 = new System.Windows.Forms.TextBox();
            this.lblAjinHomeVel1 = new System.Windows.Forms.Label();
            this.txtAjinHomeVel1 = new System.Windows.Forms.TextBox();
            this.cbxtAjinHomeLevel = new System.Windows.Forms.ComboBox();
            this.lblAjinHomeLevel = new System.Windows.Forms.Label();
            this.cbxtAjinHomeSignal = new System.Windows.Forms.ComboBox();
            this.lblAjinHomeSignal = new System.Windows.Forms.Label();
            this.lblAjinHome = new System.Windows.Forms.Label();
            this.cbxtAjinServoOnLevel = new System.Windows.Forms.ComboBox();
            this.lblAjinServoOnLevel = new System.Windows.Forms.Label();
            this.cbxtAjinPlusEndLimit = new System.Windows.Forms.ComboBox();
            this.lblAjinPlusEndLimit = new System.Windows.Forms.Label();
            this.cbxtAjinMinusEndLimit = new System.Windows.Forms.ComboBox();
            this.lblAjinMinusEndLimit = new System.Windows.Forms.Label();
            this.cbxtAjinAlarm = new System.Windows.Forms.ComboBox();
            this.lblAjinAlarm = new System.Windows.Forms.Label();
            this.cbxtAjinInposition = new System.Windows.Forms.ComboBox();
            this.lblAjinInPosition = new System.Windows.Forms.Label();
            this.lblAjinMotionSignalSetting = new System.Windows.Forms.Label();
            this.cbxtAjinVelProfileMode = new System.Windows.Forms.ComboBox();
            this.lblAjinVelProfileMode = new System.Windows.Forms.Label();
            this.cbxtAjinAbsRelMode = new System.Windows.Forms.ComboBox();
            this.lblAjinAbsRelMode = new System.Windows.Forms.Label();
            this.cbxtAjinEncInput = new System.Windows.Forms.ComboBox();
            this.lblAjinEncInput = new System.Windows.Forms.Label();
            this.cbxtAjinPulseOutput = new System.Windows.Forms.ComboBox();
            this.lblAjinPulseOutput = new System.Windows.Forms.Label();
            this.lblAjinAxisNo = new System.Windows.Forms.Label();
            this.txtAjinAxisNo = new System.Windows.Forms.TextBox();
            this.lblAjinAxisParameter = new System.Windows.Forms.Label();
            this.txtProcessSWLimitLow = new System.Windows.Forms.TextBox();
            this.txtProcessPtpSpeed = new System.Windows.Forms.TextBox();
            this.txtProcessStepSpeed = new System.Windows.Forms.TextBox();
            this.txtProcessJogSpeed = new System.Windows.Forms.TextBox();
            this.txtProcessHomeAcceleration = new System.Windows.Forms.TextBox();
            this.txtProcessHome2Velocity = new System.Windows.Forms.TextBox();
            this.txtProcessHome1Velocity = new System.Windows.Forms.TextBox();
            this.txtProcessDefaultAcceleration = new System.Windows.Forms.TextBox();
            this.txtProcessDefauleVelocity = new System.Windows.Forms.TextBox();
            this.txtProcessInPosition = new System.Windows.Forms.TextBox();
            this.txtProcessMoveTimeOut = new System.Windows.Forms.TextBox();
            this.txtProcessSpeedRate = new System.Windows.Forms.TextBox();
            this.txtProcessPositionRate = new System.Windows.Forms.TextBox();
            this.lblProcessAccelationTime = new System.Windows.Forms.Label();
            this.txtProcessAccelationTime = new System.Windows.Forms.TextBox();
            this.lblProcessSWLimitHigh = new System.Windows.Forms.Label();
            this.txtProcessSWLimitHigh = new System.Windows.Forms.TextBox();
            this.lblProcessSWLimitLow = new System.Windows.Forms.Label();
            this.lblProcessPtpSpeed = new System.Windows.Forms.Label();
            this.lblProcessStepSpeed = new System.Windows.Forms.Label();
            this.lblProcessJogSpeed = new System.Windows.Forms.Label();
            this.lblProcessHomeAcceleration = new System.Windows.Forms.Label();
            this.lbl_Process_Home2Velocity = new System.Windows.Forms.Label();
            this.lblProcessHome1Velocity = new System.Windows.Forms.Label();
            this.lblProcessDefaultAcceleration = new System.Windows.Forms.Label();
            this.lblProcessDefauleVelocity = new System.Windows.Forms.Label();
            this.lblProcessInPosition = new System.Windows.Forms.Label();
            this.lblProcessMoveTimeOut = new System.Windows.Forms.Label();
            this.lblProcessSpeedRate = new System.Windows.Forms.Label();
            this.lblProcessPositionRate = new System.Windows.Forms.Label();
            this.lblProcessAxisNo = new System.Windows.Forms.Label();
            this.txtProcessAxisNo = new System.Windows.Forms.TextBox();
            this.lblProcessAxisParameter = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMonitorLoader = new System.Windows.Forms.Button();
            this.btnMonitorProcess = new System.Windows.Forms.Button();
            this.btnMonitorUnloader = new System.Windows.Forms.Button();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAjinMotor = new System.Windows.Forms.Button();
            this.btnUmacMotor = new System.Windows.Forms.Button();
            this.btnEZiMotor = new System.Windows.Forms.Button();
            this.pnlAjinMotorParamInfo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.flpSevoMotors = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlUmacMotorParamInfo = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlEZiMotorParamInfo = new System.Windows.Forms.Panel();
            this.txtEziStopCurrent = new System.Windows.Forms.TextBox();
            this.txtEziBoostCurrent = new System.Windows.Forms.TextBox();
            this.lblEziRunCurrent = new System.Windows.Forms.Label();
            this.txtEziRunCurrent = new System.Windows.Forms.TextBox();
            this.txtEziAxisStartSpeed = new System.Windows.Forms.TextBox();
            this.lbEziAxisStartSpeed = new System.Windows.Forms.Label();
            this.txtEziPositionLoopGain = new System.Windows.Forms.TextBox();
            this.txtEziOrgSensorLogic = new System.Windows.Forms.TextBox();
            this.txEziOrgDir = new System.Windows.Forms.TextBox();
            this.txtEziLimitSensorLogic = new System.Windows.Forms.TextBox();
            this.txtHWLimitStopMethod = new System.Windows.Forms.TextBox();
            this.txtEziSWLimitStopMethod = new System.Windows.Forms.TextBox();
            this.txtSWLimitMinusValue = new System.Windows.Forms.TextBox();
            this.txtEziSWLimitPlusValue = new System.Windows.Forms.TextBox();
            this.txtEziJogAccDecTime = new System.Windows.Forms.TextBox();
            this.txtEziJogStartSpeed = new System.Windows.Forms.TextBox();
            this.txtEziJogSpeed = new System.Windows.Forms.TextBox();
            this.txtEziSpeedOverride = new System.Windows.Forms.TextBox();
            this.txtEziAxisDecTime = new System.Windows.Forms.TextBox();
            this.txtEziAxisAccSpeed = new System.Windows.Forms.TextBox();
            this.txtEziAxisMaxSpeed = new System.Windows.Forms.TextBox();
            this.txtEziPulsePerRevolution = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.lblEziAxisParameter = new System.Windows.Forms.Label();
            this.txtEziAxisNo = new System.Windows.Forms.TextBox();
            this.lblEziAxisNo = new System.Windows.Forms.Label();
            this.lblEziPulsePerRevolution = new System.Windows.Forms.Label();
            this.lblEziAxisMaxSpeed = new System.Windows.Forms.Label();
            this.lblEziAxisAccSpeed = new System.Windows.Forms.Label();
            this.lblEziAxisDecTime = new System.Windows.Forms.Label();
            this.lblEziSpeedOverride = new System.Windows.Forms.Label();
            this.lblEziJogSpeed = new System.Windows.Forms.Label();
            this.lblEziJogStartSpeed = new System.Windows.Forms.Label();
            this.lblEziJogAccDecTime = new System.Windows.Forms.Label();
            this.lblEziSWLimitPlusValue = new System.Windows.Forms.Label();
            this.lblEziSWLimitMinusValue = new System.Windows.Forms.Label();
            this.lblEziSWLimitStopMethod = new System.Windows.Forms.Label();
            this.lblEziHWLimitStopMethod = new System.Windows.Forms.Label();
            this.lblEziLimitSensorLogic = new System.Windows.Forms.Label();
            this.txtEziOrgSpeed = new System.Windows.Forms.TextBox();
            this.lblEziStopCurrent = new System.Windows.Forms.Label();
            this.lblEziOrgSpeed = new System.Windows.Forms.Label();
            this.txtEziOrgSearchSpeed = new System.Windows.Forms.TextBox();
            this.lblEziBoostCurrent = new System.Windows.Forms.Label();
            this.lblEziOrgSearchSpeed = new System.Windows.Forms.Label();
            this.lblEziBrakeDelayTime = new System.Windows.Forms.Label();
            this.txtEziAccDecTime = new System.Windows.Forms.TextBox();
            this.txtEziBrakeDelayTime = new System.Windows.Forms.TextBox();
            this.lblEziOrgAccDecTime = new System.Windows.Forms.Label();
            this.lblEziPosErrorOverflowLimit = new System.Windows.Forms.Label();
            this.txtEziOrgMethod = new System.Windows.Forms.TextBox();
            this.txtEziPosErrorOverflowLimit = new System.Windows.Forms.TextBox();
            this.lblEziOrgMethod = new System.Windows.Forms.Label();
            this.lblEziOrgTorqueRatio = new System.Windows.Forms.Label();
            this.lblEziOrgDir = new System.Windows.Forms.Label();
            this.txtEziOrgTorqueRatio = new System.Windows.Forms.TextBox();
            this.txtEziOrgOffset = new System.Windows.Forms.TextBox();
            this.lblEziLimitSensorDir = new System.Windows.Forms.Label();
            this.lblEziOrgOffset = new System.Windows.Forms.Label();
            this.txtEziLimitSensorDir = new System.Windows.Forms.TextBox();
            this.txtEziOrgPositionSet = new System.Windows.Forms.TextBox();
            this.lblEziMotionDir = new System.Windows.Forms.Label();
            this.lblEziOrgPositionSet = new System.Windows.Forms.Label();
            this.txtEziMotionDir = new System.Windows.Forms.TextBox();
            this.lblEziOrgSensorLogic = new System.Windows.Forms.Label();
            this.lblEziPosTrackingLimit = new System.Windows.Forms.Label();
            this.txtEziPosTrackingLimit = new System.Windows.Forms.TextBox();
            this.lblEziPositionLoopGain = new System.Windows.Forms.Label();
            this.txtEziInposValue = new System.Windows.Forms.TextBox();
            this.lblEziInposValue = new System.Windows.Forms.Label();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.pnlAjinMotorParamInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlUmacMotorParamInfo.SuspendLayout();
            this.pnlEZiMotorParamInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAjinInitInPosition
            // 
            this.lblAjinInitInPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInitInPosition.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInitInPosition.Location = new System.Drawing.Point(458, 703);
            this.lblAjinInitInPosition.Name = "lblAjinInitInPosition";
            this.lblAjinInitInPosition.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInitInPosition.TabIndex = 167;
            this.lblAjinInitInPosition.Text = "In Position [mm]";
            this.lblAjinInitInPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinInitInposition
            // 
            this.txtAjinInitInposition.Location = new System.Drawing.Point(694, 702);
            this.txtAjinInitInposition.Name = "txtAjinInitInposition";
            this.txtAjinInitInposition.Size = new System.Drawing.Size(170, 23);
            this.txtAjinInitInposition.TabIndex = 166;
            // 
            // lblAjinInitDecelation
            // 
            this.lblAjinInitDecelation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInitDecelation.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInitDecelation.Location = new System.Drawing.Point(458, 677);
            this.lblAjinInitDecelation.Name = "lblAjinInitDecelation";
            this.lblAjinInitDecelation.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInitDecelation.TabIndex = 165;
            this.lblAjinInitDecelation.Text = "Init Decelation :";
            this.lblAjinInitDecelation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinInitDecelation
            // 
            this.txtAjinInitDecelation.Location = new System.Drawing.Point(694, 676);
            this.txtAjinInitDecelation.Name = "txtAjinInitDecelation";
            this.txtAjinInitDecelation.Size = new System.Drawing.Size(170, 23);
            this.txtAjinInitDecelation.TabIndex = 164;
            // 
            // lblAjinInitAccelation
            // 
            this.lblAjinInitAccelation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInitAccelation.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInitAccelation.Location = new System.Drawing.Point(458, 650);
            this.lblAjinInitAccelation.Name = "lblAjinInitAccelation";
            this.lblAjinInitAccelation.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInitAccelation.TabIndex = 163;
            this.lblAjinInitAccelation.Text = "Init Accelation :";
            this.lblAjinInitAccelation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinInitAccelation
            // 
            this.txtAjinInitAccelation.Location = new System.Drawing.Point(694, 649);
            this.txtAjinInitAccelation.Name = "txtAjinInitAccelation";
            this.txtAjinInitAccelation.Size = new System.Drawing.Size(170, 23);
            this.txtAjinInitAccelation.TabIndex = 162;
            // 
            // cbxtAjinSWLimitEnable
            // 
            this.cbxtAjinSWLimitEnable.FormattingEnabled = true;
            this.cbxtAjinSWLimitEnable.Location = new System.Drawing.Point(694, 572);
            this.cbxtAjinSWLimitEnable.Name = "cbxtAjinSWLimitEnable";
            this.cbxtAjinSWLimitEnable.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinSWLimitEnable.TabIndex = 157;
            // 
            // lblAjinSWLimitEnable
            // 
            this.lblAjinSWLimitEnable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinSWLimitEnable.ForeColor = System.Drawing.Color.Black;
            this.lblAjinSWLimitEnable.Location = new System.Drawing.Point(458, 573);
            this.lblAjinSWLimitEnable.Name = "lblAjinSWLimitEnable";
            this.lblAjinSWLimitEnable.Size = new System.Drawing.Size(230, 21);
            this.lblAjinSWLimitEnable.TabIndex = 156;
            this.lblAjinSWLimitEnable.Text = "S/W Limit Enable :";
            this.lblAjinSWLimitEnable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinSWLimitStopMode
            // 
            this.cbxtAjinSWLimitStopMode.FormattingEnabled = true;
            this.cbxtAjinSWLimitStopMode.Location = new System.Drawing.Point(694, 545);
            this.cbxtAjinSWLimitStopMode.Name = "cbxtAjinSWLimitStopMode";
            this.cbxtAjinSWLimitStopMode.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinSWLimitStopMode.TabIndex = 155;
            // 
            // lblAjinSWLimitStopMode
            // 
            this.lblAjinSWLimitStopMode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinSWLimitStopMode.ForeColor = System.Drawing.Color.Black;
            this.lblAjinSWLimitStopMode.Location = new System.Drawing.Point(458, 546);
            this.lblAjinSWLimitStopMode.Name = "lblAjinSWLimitStopMode";
            this.lblAjinSWLimitStopMode.Size = new System.Drawing.Size(230, 21);
            this.lblAjinSWLimitStopMode.TabIndex = 154;
            this.lblAjinSWLimitStopMode.Text = "S/W Limit Stop Mode :";
            this.lblAjinSWLimitStopMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinHomeOffset
            // 
            this.lblAjinHomeOffset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeOffset.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeOffset.Location = new System.Drawing.Point(458, 495);
            this.lblAjinHomeOffset.Name = "lblAjinHomeOffset";
            this.lblAjinHomeOffset.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeOffset.TabIndex = 151;
            this.lblAjinHomeOffset.Text = "Home Offset :";
            this.lblAjinHomeOffset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeOffset
            // 
            this.txtAjinHomeOffset.Location = new System.Drawing.Point(694, 494);
            this.txtAjinHomeOffset.Name = "txtAjinHomeOffset";
            this.txtAjinHomeOffset.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeOffset.TabIndex = 150;
            // 
            // lblAjinHomeClearTime
            // 
            this.lblAjinHomeClearTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeClearTime.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeClearTime.Location = new System.Drawing.Point(458, 469);
            this.lblAjinHomeClearTime.Name = "lblAjinHomeClearTime";
            this.lblAjinHomeClearTime.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeClearTime.TabIndex = 149;
            this.lblAjinHomeClearTime.Text = "Home Clear Time :";
            this.lblAjinHomeClearTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeClearTime
            // 
            this.txtAjinHomeClearTime.Location = new System.Drawing.Point(694, 468);
            this.txtAjinHomeClearTime.Name = "txtAjinHomeClearTime";
            this.txtAjinHomeClearTime.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeClearTime.TabIndex = 148;
            // 
            // lblAjinHomeAccelation2
            // 
            this.lblAjinHomeAccelation2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeAccelation2.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeAccelation2.Location = new System.Drawing.Point(458, 442);
            this.lblAjinHomeAccelation2.Name = "lblAjinHomeAccelation2";
            this.lblAjinHomeAccelation2.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeAccelation2.TabIndex = 147;
            this.lblAjinHomeAccelation2.Text = "Home Accelation 2nd :";
            this.lblAjinHomeAccelation2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeAccelation2
            // 
            this.txtAjinHomeAccelation2.Location = new System.Drawing.Point(694, 442);
            this.txtAjinHomeAccelation2.Name = "txtAjinHomeAccelation2";
            this.txtAjinHomeAccelation2.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeAccelation2.TabIndex = 146;
            // 
            // lblAjinHomeAccelation1
            // 
            this.lblAjinHomeAccelation1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeAccelation1.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeAccelation1.Location = new System.Drawing.Point(458, 417);
            this.lblAjinHomeAccelation1.Name = "lblAjinHomeAccelation1";
            this.lblAjinHomeAccelation1.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeAccelation1.TabIndex = 145;
            this.lblAjinHomeAccelation1.Text = "Home Accelation 1st :";
            this.lblAjinHomeAccelation1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeAccelation1
            // 
            this.txtAjinHomeAccelation1.Location = new System.Drawing.Point(694, 416);
            this.txtAjinHomeAccelation1.Name = "txtAjinHomeAccelation1";
            this.txtAjinHomeAccelation1.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeAccelation1.TabIndex = 144;
            // 
            // cbxtAjinHomeZPhase
            // 
            this.cbxtAjinHomeZPhase.FormattingEnabled = true;
            this.cbxtAjinHomeZPhase.Location = new System.Drawing.Point(694, 390);
            this.cbxtAjinHomeZPhase.Name = "cbxtAjinHomeZPhase";
            this.cbxtAjinHomeZPhase.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinHomeZPhase.TabIndex = 143;
            // 
            // lblAjinHomeZPhase
            // 
            this.lblAjinHomeZPhase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeZPhase.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeZPhase.Location = new System.Drawing.Point(458, 391);
            this.lblAjinHomeZPhase.Name = "lblAjinHomeZPhase";
            this.lblAjinHomeZPhase.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeZPhase.TabIndex = 142;
            this.lblAjinHomeZPhase.Text = "Home Z_Phase :";
            this.lblAjinHomeZPhase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinHomeDirection
            // 
            this.cbxtAjinHomeDirection.FormattingEnabled = true;
            this.cbxtAjinHomeDirection.Location = new System.Drawing.Point(694, 363);
            this.cbxtAjinHomeDirection.Name = "cbxtAjinHomeDirection";
            this.cbxtAjinHomeDirection.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinHomeDirection.TabIndex = 141;
            // 
            // lblAjinHomeDirection
            // 
            this.lblAjinHomeDirection.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeDirection.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeDirection.Location = new System.Drawing.Point(458, 364);
            this.lblAjinHomeDirection.Name = "lblAjinHomeDirection";
            this.lblAjinHomeDirection.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeDirection.TabIndex = 140;
            this.lblAjinHomeDirection.Text = "Home Direction :";
            this.lblAjinHomeDirection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinEncoderType
            // 
            this.cbxtAjinEncoderType.FormattingEnabled = true;
            this.cbxtAjinEncoderType.Location = new System.Drawing.Point(694, 312);
            this.cbxtAjinEncoderType.Name = "cbxtAjinEncoderType";
            this.cbxtAjinEncoderType.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinEncoderType.TabIndex = 137;
            // 
            // lblAjinEncoderType
            // 
            this.lblAjinEncoderType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinEncoderType.ForeColor = System.Drawing.Color.Black;
            this.lblAjinEncoderType.Location = new System.Drawing.Point(458, 313);
            this.lblAjinEncoderType.Name = "lblAjinEncoderType";
            this.lblAjinEncoderType.Size = new System.Drawing.Size(230, 21);
            this.lblAjinEncoderType.TabIndex = 136;
            this.lblAjinEncoderType.Text = "Encoder Type :";
            this.lblAjinEncoderType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinAlarmResetLevel
            // 
            this.cbxtAjinAlarmResetLevel.FormattingEnabled = true;
            this.cbxtAjinAlarmResetLevel.Location = new System.Drawing.Point(694, 286);
            this.cbxtAjinAlarmResetLevel.Name = "cbxtAjinAlarmResetLevel";
            this.cbxtAjinAlarmResetLevel.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinAlarmResetLevel.TabIndex = 135;
            // 
            // lblAjinAlarmResetLevel
            // 
            this.lblAjinAlarmResetLevel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinAlarmResetLevel.ForeColor = System.Drawing.Color.Black;
            this.lblAjinAlarmResetLevel.Location = new System.Drawing.Point(458, 287);
            this.lblAjinAlarmResetLevel.Name = "lblAjinAlarmResetLevel";
            this.lblAjinAlarmResetLevel.Size = new System.Drawing.Size(230, 21);
            this.lblAjinAlarmResetLevel.TabIndex = 134;
            this.lblAjinAlarmResetLevel.Text = "Alarm Reset Level :";
            this.lblAjinAlarmResetLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinStopLevel
            // 
            this.cbxtAjinStopLevel.FormattingEnabled = true;
            this.cbxtAjinStopLevel.Location = new System.Drawing.Point(694, 260);
            this.cbxtAjinStopLevel.Name = "cbxtAjinStopLevel";
            this.cbxtAjinStopLevel.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinStopLevel.TabIndex = 133;
            // 
            // lblAjinStopLevel
            // 
            this.lblAjinStopLevel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinStopLevel.ForeColor = System.Drawing.Color.Black;
            this.lblAjinStopLevel.Location = new System.Drawing.Point(458, 261);
            this.lblAjinStopLevel.Name = "lblAjinStopLevel";
            this.lblAjinStopLevel.Size = new System.Drawing.Size(230, 21);
            this.lblAjinStopLevel.TabIndex = 132;
            this.lblAjinStopLevel.Text = "Stop Level :";
            this.lblAjinStopLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinStopMode
            // 
            this.cbxtAjinStopMode.FormattingEnabled = true;
            this.cbxtAjinStopMode.Location = new System.Drawing.Point(694, 234);
            this.cbxtAjinStopMode.Name = "cbxtAjinStopMode";
            this.cbxtAjinStopMode.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinStopMode.TabIndex = 131;
            // 
            // lblAjinStopMode
            // 
            this.lblAjinStopMode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinStopMode.ForeColor = System.Drawing.Color.Black;
            this.lblAjinStopMode.Location = new System.Drawing.Point(458, 235);
            this.lblAjinStopMode.Name = "lblAjinStopMode";
            this.lblAjinStopMode.Size = new System.Drawing.Size(230, 21);
            this.lblAjinStopMode.TabIndex = 130;
            this.lblAjinStopMode.Text = "Stop Mode :";
            this.lblAjinStopMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinZPhase
            // 
            this.cbxtAjinZPhase.FormattingEnabled = true;
            this.cbxtAjinZPhase.Location = new System.Drawing.Point(694, 207);
            this.cbxtAjinZPhase.Name = "cbxtAjinZPhase";
            this.cbxtAjinZPhase.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinZPhase.TabIndex = 129;
            // 
            // lblAjinZPhase
            // 
            this.lblAjinZPhase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinZPhase.ForeColor = System.Drawing.Color.Black;
            this.lblAjinZPhase.Location = new System.Drawing.Point(458, 208);
            this.lblAjinZPhase.Name = "lblAjinZPhase";
            this.lblAjinZPhase.Size = new System.Drawing.Size(230, 21);
            this.lblAjinZPhase.TabIndex = 128;
            this.lblAjinZPhase.Text = "Z-Phase :";
            this.lblAjinZPhase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinPulse
            // 
            this.lblAjinPulse.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinPulse.ForeColor = System.Drawing.Color.Black;
            this.lblAjinPulse.Location = new System.Drawing.Point(458, 157);
            this.lblAjinPulse.Name = "lblAjinPulse";
            this.lblAjinPulse.Size = new System.Drawing.Size(230, 21);
            this.lblAjinPulse.TabIndex = 125;
            this.lblAjinPulse.Text = "Pulse :";
            this.lblAjinPulse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinPulse
            // 
            this.txtAjinPulse.Location = new System.Drawing.Point(694, 156);
            this.txtAjinPulse.Name = "txtAjinPulse";
            this.txtAjinPulse.Size = new System.Drawing.Size(170, 23);
            this.txtAjinPulse.TabIndex = 124;
            // 
            // lblAjinUnit
            // 
            this.lblAjinUnit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinUnit.ForeColor = System.Drawing.Color.Black;
            this.lblAjinUnit.Location = new System.Drawing.Point(458, 131);
            this.lblAjinUnit.Name = "lblAjinUnit";
            this.lblAjinUnit.Size = new System.Drawing.Size(230, 21);
            this.lblAjinUnit.TabIndex = 123;
            this.lblAjinUnit.Text = "Unit :";
            this.lblAjinUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinUnit
            // 
            this.txtAjinUnit.Location = new System.Drawing.Point(694, 130);
            this.txtAjinUnit.Name = "txtAjinUnit";
            this.txtAjinUnit.Size = new System.Drawing.Size(170, 23);
            this.txtAjinUnit.TabIndex = 122;
            // 
            // lblAjinMinVelocitu
            // 
            this.lblAjinMinVelocitu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinMinVelocitu.ForeColor = System.Drawing.Color.Black;
            this.lblAjinMinVelocitu.Location = new System.Drawing.Point(458, 105);
            this.lblAjinMinVelocitu.Name = "lblAjinMinVelocitu";
            this.lblAjinMinVelocitu.Size = new System.Drawing.Size(230, 21);
            this.lblAjinMinVelocitu.TabIndex = 121;
            this.lblAjinMinVelocitu.Text = "Min Velocitu [mm/sec] :";
            this.lblAjinMinVelocitu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinMinVelocitu
            // 
            this.txtAjinMinVelocitu.Location = new System.Drawing.Point(694, 104);
            this.txtAjinMinVelocitu.Name = "txtAjinMinVelocitu";
            this.txtAjinMinVelocitu.Size = new System.Drawing.Size(170, 23);
            this.txtAjinMinVelocitu.TabIndex = 120;
            // 
            // lblAjinMaxVelocitu
            // 
            this.lblAjinMaxVelocitu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinMaxVelocitu.ForeColor = System.Drawing.Color.Black;
            this.lblAjinMaxVelocitu.Location = new System.Drawing.Point(458, 78);
            this.lblAjinMaxVelocitu.Name = "lblAjinMaxVelocitu";
            this.lblAjinMaxVelocitu.Size = new System.Drawing.Size(230, 21);
            this.lblAjinMaxVelocitu.TabIndex = 119;
            this.lblAjinMaxVelocitu.Text = "Max Velocitu [mm/sec] :";
            this.lblAjinMaxVelocitu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinMaxVelocitu
            // 
            this.txtAjinMaxVelocitu.Location = new System.Drawing.Point(694, 77);
            this.txtAjinMaxVelocitu.Name = "txtAjinMaxVelocitu";
            this.txtAjinMaxVelocitu.Size = new System.Drawing.Size(170, 23);
            this.txtAjinMaxVelocitu.TabIndex = 118;
            // 
            // lblAjinInitAccelationTime
            // 
            this.lblAjinInitAccelationTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInitAccelationTime.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInitAccelationTime.Location = new System.Drawing.Point(3, 729);
            this.lblAjinInitAccelationTime.Name = "lblAjinInitAccelationTime";
            this.lblAjinInitAccelationTime.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInitAccelationTime.TabIndex = 117;
            this.lblAjinInitAccelationTime.Text = "Accelation Time [s] :";
            this.lblAjinInitAccelationTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinInitAccelationTime
            // 
            this.txtAjinInitAccelationTime.Location = new System.Drawing.Point(239, 728);
            this.txtAjinInitAccelationTime.Name = "txtAjinInitAccelationTime";
            this.txtAjinInitAccelationTime.Size = new System.Drawing.Size(170, 23);
            this.txtAjinInitAccelationTime.TabIndex = 116;
            // 
            // lblAjinInitPtpSpeed
            // 
            this.lblAjinInitPtpSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInitPtpSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInitPtpSpeed.Location = new System.Drawing.Point(3, 703);
            this.lblAjinInitPtpSpeed.Name = "lblAjinInitPtpSpeed";
            this.lblAjinInitPtpSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInitPtpSpeed.TabIndex = 115;
            this.lblAjinInitPtpSpeed.Text = "Ptp Speed :";
            this.lblAjinInitPtpSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinInitPtpSpeed
            // 
            this.txtAjinInitPtpSpeed.Location = new System.Drawing.Point(239, 702);
            this.txtAjinInitPtpSpeed.Name = "txtAjinInitPtpSpeed";
            this.txtAjinInitPtpSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtAjinInitPtpSpeed.TabIndex = 114;
            // 
            // lblAjinInitVelocity
            // 
            this.lblAjinInitVelocity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInitVelocity.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInitVelocity.Location = new System.Drawing.Point(3, 677);
            this.lblAjinInitVelocity.Name = "lblAjinInitVelocity";
            this.lblAjinInitVelocity.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInitVelocity.TabIndex = 113;
            this.lblAjinInitVelocity.Text = "Init Velocity [mm/sec] :";
            this.lblAjinInitVelocity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinInitVelocity
            // 
            this.txtAjinInitVelocity.Location = new System.Drawing.Point(239, 676);
            this.txtAjinInitVelocity.Name = "txtAjinInitVelocity";
            this.txtAjinInitVelocity.Size = new System.Drawing.Size(170, 23);
            this.txtAjinInitVelocity.TabIndex = 112;
            // 
            // lblAjinInitPosition
            // 
            this.lblAjinInitPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInitPosition.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInitPosition.Location = new System.Drawing.Point(3, 650);
            this.lblAjinInitPosition.Name = "lblAjinInitPosition";
            this.lblAjinInitPosition.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInitPosition.TabIndex = 111;
            this.lblAjinInitPosition.Text = "Init Position [mm] :";
            this.lblAjinInitPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinInitPosition
            // 
            this.txtAjinInitPosition.Location = new System.Drawing.Point(239, 649);
            this.txtAjinInitPosition.Name = "txtAjinInitPosition";
            this.txtAjinInitPosition.Size = new System.Drawing.Size(170, 23);
            this.txtAjinInitPosition.TabIndex = 110;
            // 
            // lblAjinInit
            // 
            this.lblAjinInit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInit.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInit.Location = new System.Drawing.Point(3, 624);
            this.lblAjinInit.Name = "lblAjinInit";
            this.lblAjinInit.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInit.TabIndex = 108;
            this.lblAjinInit.Text = "Init Setting";
            this.lblAjinInit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinSWLimitMode
            // 
            this.cbxtAjinSWLimitMode.FormattingEnabled = true;
            this.cbxtAjinSWLimitMode.Location = new System.Drawing.Point(239, 598);
            this.cbxtAjinSWLimitMode.Name = "cbxtAjinSWLimitMode";
            this.cbxtAjinSWLimitMode.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinSWLimitMode.TabIndex = 107;
            // 
            // lblAjinSWLimitMode
            // 
            this.lblAjinSWLimitMode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinSWLimitMode.ForeColor = System.Drawing.Color.Black;
            this.lblAjinSWLimitMode.Location = new System.Drawing.Point(3, 599);
            this.lblAjinSWLimitMode.Name = "lblAjinSWLimitMode";
            this.lblAjinSWLimitMode.Size = new System.Drawing.Size(230, 21);
            this.lblAjinSWLimitMode.TabIndex = 106;
            this.lblAjinSWLimitMode.Text = "S/W Limit Mode :";
            this.lblAjinSWLimitMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinSWPlusLimit
            // 
            this.lblAjinSWPlusLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinSWPlusLimit.ForeColor = System.Drawing.Color.Black;
            this.lblAjinSWPlusLimit.Location = new System.Drawing.Point(3, 573);
            this.lblAjinSWPlusLimit.Name = "lblAjinSWPlusLimit";
            this.lblAjinSWPlusLimit.Size = new System.Drawing.Size(230, 21);
            this.lblAjinSWPlusLimit.TabIndex = 104;
            this.lblAjinSWPlusLimit.Text = "S/W +Limit [mm] :";
            this.lblAjinSWPlusLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinSWPlusLimit
            // 
            this.txtAjinSWPlusLimit.Location = new System.Drawing.Point(239, 572);
            this.txtAjinSWPlusLimit.Name = "txtAjinSWPlusLimit";
            this.txtAjinSWPlusLimit.Size = new System.Drawing.Size(170, 23);
            this.txtAjinSWPlusLimit.TabIndex = 103;
            // 
            // lblAjinSWMinusLimit
            // 
            this.lblAjinSWMinusLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinSWMinusLimit.ForeColor = System.Drawing.Color.Black;
            this.lblAjinSWMinusLimit.Location = new System.Drawing.Point(3, 546);
            this.lblAjinSWMinusLimit.Name = "lblAjinSWMinusLimit";
            this.lblAjinSWMinusLimit.Size = new System.Drawing.Size(230, 21);
            this.lblAjinSWMinusLimit.TabIndex = 102;
            this.lblAjinSWMinusLimit.Text = "S/W -Limit [mm] :";
            this.lblAjinSWMinusLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinSWMinusLimit
            // 
            this.txtAjinSWMinusLimit.Location = new System.Drawing.Point(239, 545);
            this.txtAjinSWMinusLimit.Name = "txtAjinSWMinusLimit";
            this.txtAjinSWMinusLimit.Size = new System.Drawing.Size(170, 23);
            this.txtAjinSWMinusLimit.TabIndex = 101;
            // 
            // lblAjinSW
            // 
            this.lblAjinSW.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinSW.ForeColor = System.Drawing.Color.Black;
            this.lblAjinSW.Location = new System.Drawing.Point(3, 520);
            this.lblAjinSW.Name = "lblAjinSW";
            this.lblAjinSW.Size = new System.Drawing.Size(230, 21);
            this.lblAjinSW.TabIndex = 100;
            this.lblAjinSW.Text = "S/W Limit Setting";
            this.lblAjinSW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinHomeVelLast
            // 
            this.lblAjinHomeVelLast.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeVelLast.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeVelLast.Location = new System.Drawing.Point(3, 495);
            this.lblAjinHomeVelLast.Name = "lblAjinHomeVelLast";
            this.lblAjinHomeVelLast.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeVelLast.TabIndex = 98;
            this.lblAjinHomeVelLast.Text = "Home Vel Last :";
            this.lblAjinHomeVelLast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeVelLast
            // 
            this.txtAjinHomeVelLast.Location = new System.Drawing.Point(239, 494);
            this.txtAjinHomeVelLast.Name = "txtAjinHomeVelLast";
            this.txtAjinHomeVelLast.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeVelLast.TabIndex = 97;
            // 
            // lblAjinHomeVel3
            // 
            this.lblAjinHomeVel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeVel3.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeVel3.Location = new System.Drawing.Point(3, 469);
            this.lblAjinHomeVel3.Name = "lblAjinHomeVel3";
            this.lblAjinHomeVel3.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeVel3.TabIndex = 96;
            this.lblAjinHomeVel3.Text = "Home Vel 3rd :";
            this.lblAjinHomeVel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeVel3
            // 
            this.txtAjinHomeVel3.Location = new System.Drawing.Point(239, 468);
            this.txtAjinHomeVel3.Name = "txtAjinHomeVel3";
            this.txtAjinHomeVel3.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeVel3.TabIndex = 95;
            // 
            // lblAjinHomeVel2
            // 
            this.lblAjinHomeVel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeVel2.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeVel2.Location = new System.Drawing.Point(3, 443);
            this.lblAjinHomeVel2.Name = "lblAjinHomeVel2";
            this.lblAjinHomeVel2.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeVel2.TabIndex = 94;
            this.lblAjinHomeVel2.Text = "Home Vel 2nd :";
            this.lblAjinHomeVel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeVel2
            // 
            this.txtAjinHomeVel2.Location = new System.Drawing.Point(239, 442);
            this.txtAjinHomeVel2.Name = "txtAjinHomeVel2";
            this.txtAjinHomeVel2.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeVel2.TabIndex = 93;
            // 
            // lblAjinHomeVel1
            // 
            this.lblAjinHomeVel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeVel1.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeVel1.Location = new System.Drawing.Point(3, 417);
            this.lblAjinHomeVel1.Name = "lblAjinHomeVel1";
            this.lblAjinHomeVel1.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeVel1.TabIndex = 92;
            this.lblAjinHomeVel1.Text = "Home Vel 1st :";
            this.lblAjinHomeVel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinHomeVel1
            // 
            this.txtAjinHomeVel1.Location = new System.Drawing.Point(239, 416);
            this.txtAjinHomeVel1.Name = "txtAjinHomeVel1";
            this.txtAjinHomeVel1.Size = new System.Drawing.Size(170, 23);
            this.txtAjinHomeVel1.TabIndex = 91;
            // 
            // cbxtAjinHomeLevel
            // 
            this.cbxtAjinHomeLevel.FormattingEnabled = true;
            this.cbxtAjinHomeLevel.Location = new System.Drawing.Point(239, 390);
            this.cbxtAjinHomeLevel.Name = "cbxtAjinHomeLevel";
            this.cbxtAjinHomeLevel.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinHomeLevel.TabIndex = 90;
            // 
            // lblAjinHomeLevel
            // 
            this.lblAjinHomeLevel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeLevel.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeLevel.Location = new System.Drawing.Point(3, 391);
            this.lblAjinHomeLevel.Name = "lblAjinHomeLevel";
            this.lblAjinHomeLevel.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeLevel.TabIndex = 89;
            this.lblAjinHomeLevel.Text = "Home Level :";
            this.lblAjinHomeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinHomeSignal
            // 
            this.cbxtAjinHomeSignal.FormattingEnabled = true;
            this.cbxtAjinHomeSignal.Location = new System.Drawing.Point(239, 363);
            this.cbxtAjinHomeSignal.Name = "cbxtAjinHomeSignal";
            this.cbxtAjinHomeSignal.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinHomeSignal.TabIndex = 88;
            // 
            // lblAjinHomeSignal
            // 
            this.lblAjinHomeSignal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHomeSignal.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHomeSignal.Location = new System.Drawing.Point(3, 363);
            this.lblAjinHomeSignal.Name = "lblAjinHomeSignal";
            this.lblAjinHomeSignal.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHomeSignal.TabIndex = 87;
            this.lblAjinHomeSignal.Text = "Home Signal :";
            this.lblAjinHomeSignal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinHome
            // 
            this.lblAjinHome.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinHome.ForeColor = System.Drawing.Color.Black;
            this.lblAjinHome.Location = new System.Drawing.Point(3, 338);
            this.lblAjinHome.Name = "lblAjinHome";
            this.lblAjinHome.Size = new System.Drawing.Size(230, 21);
            this.lblAjinHome.TabIndex = 85;
            this.lblAjinHome.Text = "Home Search Setting";
            this.lblAjinHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinServoOnLevel
            // 
            this.cbxtAjinServoOnLevel.FormattingEnabled = true;
            this.cbxtAjinServoOnLevel.Location = new System.Drawing.Point(239, 312);
            this.cbxtAjinServoOnLevel.Name = "cbxtAjinServoOnLevel";
            this.cbxtAjinServoOnLevel.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinServoOnLevel.TabIndex = 84;
            // 
            // lblAjinServoOnLevel
            // 
            this.lblAjinServoOnLevel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinServoOnLevel.ForeColor = System.Drawing.Color.Black;
            this.lblAjinServoOnLevel.Location = new System.Drawing.Point(3, 313);
            this.lblAjinServoOnLevel.Name = "lblAjinServoOnLevel";
            this.lblAjinServoOnLevel.Size = new System.Drawing.Size(230, 21);
            this.lblAjinServoOnLevel.TabIndex = 83;
            this.lblAjinServoOnLevel.Text = "Servo On Level :";
            this.lblAjinServoOnLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinPlusEndLimit
            // 
            this.cbxtAjinPlusEndLimit.FormattingEnabled = true;
            this.cbxtAjinPlusEndLimit.Location = new System.Drawing.Point(239, 286);
            this.cbxtAjinPlusEndLimit.Name = "cbxtAjinPlusEndLimit";
            this.cbxtAjinPlusEndLimit.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinPlusEndLimit.TabIndex = 82;
            // 
            // lblAjinPlusEndLimit
            // 
            this.lblAjinPlusEndLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinPlusEndLimit.ForeColor = System.Drawing.Color.Black;
            this.lblAjinPlusEndLimit.Location = new System.Drawing.Point(3, 287);
            this.lblAjinPlusEndLimit.Name = "lblAjinPlusEndLimit";
            this.lblAjinPlusEndLimit.Size = new System.Drawing.Size(230, 21);
            this.lblAjinPlusEndLimit.TabIndex = 81;
            this.lblAjinPlusEndLimit.Text = "+ End Limit :";
            this.lblAjinPlusEndLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinMinusEndLimit
            // 
            this.cbxtAjinMinusEndLimit.FormattingEnabled = true;
            this.cbxtAjinMinusEndLimit.Location = new System.Drawing.Point(239, 260);
            this.cbxtAjinMinusEndLimit.Name = "cbxtAjinMinusEndLimit";
            this.cbxtAjinMinusEndLimit.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinMinusEndLimit.TabIndex = 80;
            // 
            // lblAjinMinusEndLimit
            // 
            this.lblAjinMinusEndLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinMinusEndLimit.ForeColor = System.Drawing.Color.Black;
            this.lblAjinMinusEndLimit.Location = new System.Drawing.Point(3, 261);
            this.lblAjinMinusEndLimit.Name = "lblAjinMinusEndLimit";
            this.lblAjinMinusEndLimit.Size = new System.Drawing.Size(230, 21);
            this.lblAjinMinusEndLimit.TabIndex = 79;
            this.lblAjinMinusEndLimit.Text = "- End Limit :";
            this.lblAjinMinusEndLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinAlarm
            // 
            this.cbxtAjinAlarm.FormattingEnabled = true;
            this.cbxtAjinAlarm.Location = new System.Drawing.Point(239, 234);
            this.cbxtAjinAlarm.Name = "cbxtAjinAlarm";
            this.cbxtAjinAlarm.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinAlarm.TabIndex = 78;
            // 
            // lblAjinAlarm
            // 
            this.lblAjinAlarm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinAlarm.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblAjinAlarm.ForeColor = System.Drawing.Color.Black;
            this.lblAjinAlarm.Location = new System.Drawing.Point(3, 235);
            this.lblAjinAlarm.Name = "lblAjinAlarm";
            this.lblAjinAlarm.Size = new System.Drawing.Size(230, 21);
            this.lblAjinAlarm.TabIndex = 77;
            this.lblAjinAlarm.Text = "Alarm :";
            this.lblAjinAlarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinInposition
            // 
            this.cbxtAjinInposition.FormattingEnabled = true;
            this.cbxtAjinInposition.Location = new System.Drawing.Point(239, 207);
            this.cbxtAjinInposition.Name = "cbxtAjinInposition";
            this.cbxtAjinInposition.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinInposition.TabIndex = 76;
            // 
            // lblAjinInPosition
            // 
            this.lblAjinInPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinInPosition.ForeColor = System.Drawing.Color.Black;
            this.lblAjinInPosition.Location = new System.Drawing.Point(3, 208);
            this.lblAjinInPosition.Name = "lblAjinInPosition";
            this.lblAjinInPosition.Size = new System.Drawing.Size(230, 21);
            this.lblAjinInPosition.TabIndex = 75;
            this.lblAjinInPosition.Text = "In Position :";
            this.lblAjinInPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinMotionSignalSetting
            // 
            this.lblAjinMotionSignalSetting.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinMotionSignalSetting.ForeColor = System.Drawing.Color.Black;
            this.lblAjinMotionSignalSetting.Location = new System.Drawing.Point(3, 182);
            this.lblAjinMotionSignalSetting.Name = "lblAjinMotionSignalSetting";
            this.lblAjinMotionSignalSetting.Size = new System.Drawing.Size(230, 21);
            this.lblAjinMotionSignalSetting.TabIndex = 73;
            this.lblAjinMotionSignalSetting.Text = "Motion Signal Setting";
            this.lblAjinMotionSignalSetting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinVelProfileMode
            // 
            this.cbxtAjinVelProfileMode.FormattingEnabled = true;
            this.cbxtAjinVelProfileMode.Location = new System.Drawing.Point(239, 156);
            this.cbxtAjinVelProfileMode.Name = "cbxtAjinVelProfileMode";
            this.cbxtAjinVelProfileMode.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinVelProfileMode.TabIndex = 72;
            // 
            // lblAjinVelProfileMode
            // 
            this.lblAjinVelProfileMode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinVelProfileMode.ForeColor = System.Drawing.Color.Black;
            this.lblAjinVelProfileMode.Location = new System.Drawing.Point(3, 157);
            this.lblAjinVelProfileMode.Name = "lblAjinVelProfileMode";
            this.lblAjinVelProfileMode.Size = new System.Drawing.Size(230, 21);
            this.lblAjinVelProfileMode.TabIndex = 71;
            this.lblAjinVelProfileMode.Text = "Vel Profile Mode :";
            this.lblAjinVelProfileMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinAbsRelMode
            // 
            this.cbxtAjinAbsRelMode.FormattingEnabled = true;
            this.cbxtAjinAbsRelMode.Location = new System.Drawing.Point(239, 130);
            this.cbxtAjinAbsRelMode.Name = "cbxtAjinAbsRelMode";
            this.cbxtAjinAbsRelMode.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinAbsRelMode.TabIndex = 70;
            // 
            // lblAjinAbsRelMode
            // 
            this.lblAjinAbsRelMode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinAbsRelMode.ForeColor = System.Drawing.Color.Black;
            this.lblAjinAbsRelMode.Location = new System.Drawing.Point(3, 131);
            this.lblAjinAbsRelMode.Name = "lblAjinAbsRelMode";
            this.lblAjinAbsRelMode.Size = new System.Drawing.Size(230, 21);
            this.lblAjinAbsRelMode.TabIndex = 69;
            this.lblAjinAbsRelMode.Text = "Abs.Rel Mode :";
            this.lblAjinAbsRelMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinEncInput
            // 
            this.cbxtAjinEncInput.FormattingEnabled = true;
            this.cbxtAjinEncInput.Location = new System.Drawing.Point(239, 104);
            this.cbxtAjinEncInput.Name = "cbxtAjinEncInput";
            this.cbxtAjinEncInput.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinEncInput.TabIndex = 68;
            // 
            // lblAjinEncInput
            // 
            this.lblAjinEncInput.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinEncInput.ForeColor = System.Drawing.Color.Black;
            this.lblAjinEncInput.Location = new System.Drawing.Point(3, 105);
            this.lblAjinEncInput.Name = "lblAjinEncInput";
            this.lblAjinEncInput.Size = new System.Drawing.Size(230, 21);
            this.lblAjinEncInput.TabIndex = 67;
            this.lblAjinEncInput.Text = "Enc. Input :";
            this.lblAjinEncInput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxtAjinPulseOutput
            // 
            this.cbxtAjinPulseOutput.FormattingEnabled = true;
            this.cbxtAjinPulseOutput.Location = new System.Drawing.Point(239, 78);
            this.cbxtAjinPulseOutput.Name = "cbxtAjinPulseOutput";
            this.cbxtAjinPulseOutput.Size = new System.Drawing.Size(170, 23);
            this.cbxtAjinPulseOutput.TabIndex = 66;
            // 
            // lblAjinPulseOutput
            // 
            this.lblAjinPulseOutput.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinPulseOutput.ForeColor = System.Drawing.Color.Black;
            this.lblAjinPulseOutput.Location = new System.Drawing.Point(3, 79);
            this.lblAjinPulseOutput.Name = "lblAjinPulseOutput";
            this.lblAjinPulseOutput.Size = new System.Drawing.Size(230, 21);
            this.lblAjinPulseOutput.TabIndex = 65;
            this.lblAjinPulseOutput.Text = "Pulse Output :";
            this.lblAjinPulseOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAjinAxisNo
            // 
            this.lblAjinAxisNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinAxisNo.ForeColor = System.Drawing.Color.Black;
            this.lblAjinAxisNo.Location = new System.Drawing.Point(3, 52);
            this.lblAjinAxisNo.Name = "lblAjinAxisNo";
            this.lblAjinAxisNo.Size = new System.Drawing.Size(230, 21);
            this.lblAjinAxisNo.TabIndex = 63;
            this.lblAjinAxisNo.Text = "AxisNo.";
            this.lblAjinAxisNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAjinAxisNo
            // 
            this.txtAjinAxisNo.Location = new System.Drawing.Point(239, 51);
            this.txtAjinAxisNo.Name = "txtAjinAxisNo";
            this.txtAjinAxisNo.Size = new System.Drawing.Size(170, 23);
            this.txtAjinAxisNo.TabIndex = 62;
            // 
            // lblAjinAxisParameter
            // 
            this.lblAjinAxisParameter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAjinAxisParameter.ForeColor = System.Drawing.Color.Black;
            this.lblAjinAxisParameter.Location = new System.Drawing.Point(3, 26);
            this.lblAjinAxisParameter.Name = "lblAjinAxisParameter";
            this.lblAjinAxisParameter.Size = new System.Drawing.Size(230, 21);
            this.lblAjinAxisParameter.TabIndex = 61;
            this.lblAjinAxisParameter.Text = "Axis Parameter";
            this.lblAjinAxisParameter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProcessSWLimitLow
            // 
            this.txtProcessSWLimitLow.Location = new System.Drawing.Point(239, 401);
            this.txtProcessSWLimitLow.Name = "txtProcessSWLimitLow";
            this.txtProcessSWLimitLow.Size = new System.Drawing.Size(170, 23);
            this.txtProcessSWLimitLow.TabIndex = 180;
            // 
            // txtProcessPtpSpeed
            // 
            this.txtProcessPtpSpeed.Location = new System.Drawing.Point(239, 374);
            this.txtProcessPtpSpeed.Name = "txtProcessPtpSpeed";
            this.txtProcessPtpSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtProcessPtpSpeed.TabIndex = 179;
            // 
            // txtProcessStepSpeed
            // 
            this.txtProcessStepSpeed.Location = new System.Drawing.Point(239, 347);
            this.txtProcessStepSpeed.Name = "txtProcessStepSpeed";
            this.txtProcessStepSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtProcessStepSpeed.TabIndex = 178;
            // 
            // txtProcessJogSpeed
            // 
            this.txtProcessJogSpeed.Location = new System.Drawing.Point(239, 320);
            this.txtProcessJogSpeed.Name = "txtProcessJogSpeed";
            this.txtProcessJogSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtProcessJogSpeed.TabIndex = 177;
            // 
            // txtProcessHomeAcceleration
            // 
            this.txtProcessHomeAcceleration.Location = new System.Drawing.Point(239, 293);
            this.txtProcessHomeAcceleration.Name = "txtProcessHomeAcceleration";
            this.txtProcessHomeAcceleration.Size = new System.Drawing.Size(170, 23);
            this.txtProcessHomeAcceleration.TabIndex = 176;
            // 
            // txtProcessHome2Velocity
            // 
            this.txtProcessHome2Velocity.Location = new System.Drawing.Point(239, 266);
            this.txtProcessHome2Velocity.Name = "txtProcessHome2Velocity";
            this.txtProcessHome2Velocity.Size = new System.Drawing.Size(170, 23);
            this.txtProcessHome2Velocity.TabIndex = 175;
            // 
            // txtProcessHome1Velocity
            // 
            this.txtProcessHome1Velocity.Location = new System.Drawing.Point(239, 239);
            this.txtProcessHome1Velocity.Name = "txtProcessHome1Velocity";
            this.txtProcessHome1Velocity.Size = new System.Drawing.Size(170, 23);
            this.txtProcessHome1Velocity.TabIndex = 174;
            // 
            // txtProcessDefaultAcceleration
            // 
            this.txtProcessDefaultAcceleration.Location = new System.Drawing.Point(239, 212);
            this.txtProcessDefaultAcceleration.Name = "txtProcessDefaultAcceleration";
            this.txtProcessDefaultAcceleration.Size = new System.Drawing.Size(170, 23);
            this.txtProcessDefaultAcceleration.TabIndex = 173;
            // 
            // txtProcessDefauleVelocity
            // 
            this.txtProcessDefauleVelocity.Location = new System.Drawing.Point(239, 185);
            this.txtProcessDefauleVelocity.Name = "txtProcessDefauleVelocity";
            this.txtProcessDefauleVelocity.Size = new System.Drawing.Size(170, 23);
            this.txtProcessDefauleVelocity.TabIndex = 172;
            // 
            // txtProcessInPosition
            // 
            this.txtProcessInPosition.Location = new System.Drawing.Point(239, 158);
            this.txtProcessInPosition.Name = "txtProcessInPosition";
            this.txtProcessInPosition.Size = new System.Drawing.Size(170, 23);
            this.txtProcessInPosition.TabIndex = 171;
            // 
            // txtProcessMoveTimeOut
            // 
            this.txtProcessMoveTimeOut.Location = new System.Drawing.Point(239, 131);
            this.txtProcessMoveTimeOut.Name = "txtProcessMoveTimeOut";
            this.txtProcessMoveTimeOut.Size = new System.Drawing.Size(170, 23);
            this.txtProcessMoveTimeOut.TabIndex = 170;
            // 
            // txtProcessSpeedRate
            // 
            this.txtProcessSpeedRate.Location = new System.Drawing.Point(239, 104);
            this.txtProcessSpeedRate.Name = "txtProcessSpeedRate";
            this.txtProcessSpeedRate.Size = new System.Drawing.Size(170, 23);
            this.txtProcessSpeedRate.TabIndex = 169;
            // 
            // txtProcessPositionRate
            // 
            this.txtProcessPositionRate.Location = new System.Drawing.Point(239, 78);
            this.txtProcessPositionRate.Name = "txtProcessPositionRate";
            this.txtProcessPositionRate.Size = new System.Drawing.Size(170, 23);
            this.txtProcessPositionRate.TabIndex = 168;
            // 
            // lblProcessAccelationTime
            // 
            this.lblProcessAccelationTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessAccelationTime.ForeColor = System.Drawing.Color.Black;
            this.lblProcessAccelationTime.Location = new System.Drawing.Point(3, 456);
            this.lblProcessAccelationTime.Name = "lblProcessAccelationTime";
            this.lblProcessAccelationTime.Size = new System.Drawing.Size(230, 21);
            this.lblProcessAccelationTime.TabIndex = 94;
            this.lblProcessAccelationTime.Text = "Accelation Time :";
            this.lblProcessAccelationTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProcessAccelationTime
            // 
            this.txtProcessAccelationTime.Location = new System.Drawing.Point(239, 455);
            this.txtProcessAccelationTime.Name = "txtProcessAccelationTime";
            this.txtProcessAccelationTime.Size = new System.Drawing.Size(170, 23);
            this.txtProcessAccelationTime.TabIndex = 93;
            // 
            // lblProcessSWLimitHigh
            // 
            this.lblProcessSWLimitHigh.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessSWLimitHigh.ForeColor = System.Drawing.Color.Black;
            this.lblProcessSWLimitHigh.Location = new System.Drawing.Point(3, 429);
            this.lblProcessSWLimitHigh.Name = "lblProcessSWLimitHigh";
            this.lblProcessSWLimitHigh.Size = new System.Drawing.Size(230, 21);
            this.lblProcessSWLimitHigh.TabIndex = 92;
            this.lblProcessSWLimitHigh.Text = "Software Limit High :";
            this.lblProcessSWLimitHigh.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProcessSWLimitHigh
            // 
            this.txtProcessSWLimitHigh.Location = new System.Drawing.Point(239, 428);
            this.txtProcessSWLimitHigh.Name = "txtProcessSWLimitHigh";
            this.txtProcessSWLimitHigh.Size = new System.Drawing.Size(170, 23);
            this.txtProcessSWLimitHigh.TabIndex = 91;
            // 
            // lblProcessSWLimitLow
            // 
            this.lblProcessSWLimitLow.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessSWLimitLow.ForeColor = System.Drawing.Color.Black;
            this.lblProcessSWLimitLow.Location = new System.Drawing.Point(3, 402);
            this.lblProcessSWLimitLow.Name = "lblProcessSWLimitLow";
            this.lblProcessSWLimitLow.Size = new System.Drawing.Size(230, 21);
            this.lblProcessSWLimitLow.TabIndex = 89;
            this.lblProcessSWLimitLow.Text = "Software Limit Low :";
            this.lblProcessSWLimitLow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessPtpSpeed
            // 
            this.lblProcessPtpSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessPtpSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblProcessPtpSpeed.Location = new System.Drawing.Point(3, 375);
            this.lblProcessPtpSpeed.Name = "lblProcessPtpSpeed";
            this.lblProcessPtpSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblProcessPtpSpeed.TabIndex = 87;
            this.lblProcessPtpSpeed.Text = "Ptp Speed :";
            this.lblProcessPtpSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessStepSpeed
            // 
            this.lblProcessStepSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessStepSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblProcessStepSpeed.Location = new System.Drawing.Point(3, 348);
            this.lblProcessStepSpeed.Name = "lblProcessStepSpeed";
            this.lblProcessStepSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblProcessStepSpeed.TabIndex = 85;
            this.lblProcessStepSpeed.Text = "Step Speed :";
            this.lblProcessStepSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessJogSpeed
            // 
            this.lblProcessJogSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessJogSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblProcessJogSpeed.Location = new System.Drawing.Point(3, 321);
            this.lblProcessJogSpeed.Name = "lblProcessJogSpeed";
            this.lblProcessJogSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblProcessJogSpeed.TabIndex = 83;
            this.lblProcessJogSpeed.Text = "Jog Speed :";
            this.lblProcessJogSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessHomeAcceleration
            // 
            this.lblProcessHomeAcceleration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessHomeAcceleration.ForeColor = System.Drawing.Color.Black;
            this.lblProcessHomeAcceleration.Location = new System.Drawing.Point(3, 294);
            this.lblProcessHomeAcceleration.Name = "lblProcessHomeAcceleration";
            this.lblProcessHomeAcceleration.Size = new System.Drawing.Size(230, 21);
            this.lblProcessHomeAcceleration.TabIndex = 81;
            this.lblProcessHomeAcceleration.Text = "Home Acceleration :";
            this.lblProcessHomeAcceleration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Process_Home2Velocity
            // 
            this.lbl_Process_Home2Velocity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_Process_Home2Velocity.ForeColor = System.Drawing.Color.Black;
            this.lbl_Process_Home2Velocity.Location = new System.Drawing.Point(3, 267);
            this.lbl_Process_Home2Velocity.Name = "lbl_Process_Home2Velocity";
            this.lbl_Process_Home2Velocity.Size = new System.Drawing.Size(230, 21);
            this.lbl_Process_Home2Velocity.TabIndex = 79;
            this.lbl_Process_Home2Velocity.Text = "Home Second Velocity :";
            this.lbl_Process_Home2Velocity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessHome1Velocity
            // 
            this.lblProcessHome1Velocity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessHome1Velocity.ForeColor = System.Drawing.Color.Black;
            this.lblProcessHome1Velocity.Location = new System.Drawing.Point(3, 240);
            this.lblProcessHome1Velocity.Name = "lblProcessHome1Velocity";
            this.lblProcessHome1Velocity.Size = new System.Drawing.Size(230, 21);
            this.lblProcessHome1Velocity.TabIndex = 77;
            this.lblProcessHome1Velocity.Text = "Home First Velocity :";
            this.lblProcessHome1Velocity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessDefaultAcceleration
            // 
            this.lblProcessDefaultAcceleration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessDefaultAcceleration.ForeColor = System.Drawing.Color.Black;
            this.lblProcessDefaultAcceleration.Location = new System.Drawing.Point(3, 213);
            this.lblProcessDefaultAcceleration.Name = "lblProcessDefaultAcceleration";
            this.lblProcessDefaultAcceleration.Size = new System.Drawing.Size(230, 21);
            this.lblProcessDefaultAcceleration.TabIndex = 75;
            this.lblProcessDefaultAcceleration.Text = "Default Acceleration :";
            this.lblProcessDefaultAcceleration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessDefauleVelocity
            // 
            this.lblProcessDefauleVelocity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessDefauleVelocity.ForeColor = System.Drawing.Color.Black;
            this.lblProcessDefauleVelocity.Location = new System.Drawing.Point(3, 186);
            this.lblProcessDefauleVelocity.Name = "lblProcessDefauleVelocity";
            this.lblProcessDefauleVelocity.Size = new System.Drawing.Size(230, 21);
            this.lblProcessDefauleVelocity.TabIndex = 73;
            this.lblProcessDefauleVelocity.Text = "Defaule Velocity :";
            this.lblProcessDefauleVelocity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessInPosition
            // 
            this.lblProcessInPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessInPosition.ForeColor = System.Drawing.Color.Black;
            this.lblProcessInPosition.Location = new System.Drawing.Point(3, 159);
            this.lblProcessInPosition.Name = "lblProcessInPosition";
            this.lblProcessInPosition.Size = new System.Drawing.Size(230, 21);
            this.lblProcessInPosition.TabIndex = 71;
            this.lblProcessInPosition.Text = "In Position :";
            this.lblProcessInPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessMoveTimeOut
            // 
            this.lblProcessMoveTimeOut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessMoveTimeOut.ForeColor = System.Drawing.Color.Black;
            this.lblProcessMoveTimeOut.Location = new System.Drawing.Point(3, 132);
            this.lblProcessMoveTimeOut.Name = "lblProcessMoveTimeOut";
            this.lblProcessMoveTimeOut.Size = new System.Drawing.Size(230, 21);
            this.lblProcessMoveTimeOut.TabIndex = 69;
            this.lblProcessMoveTimeOut.Text = "Move Time Out :";
            this.lblProcessMoveTimeOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessSpeedRate
            // 
            this.lblProcessSpeedRate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessSpeedRate.ForeColor = System.Drawing.Color.Black;
            this.lblProcessSpeedRate.Location = new System.Drawing.Point(3, 105);
            this.lblProcessSpeedRate.Name = "lblProcessSpeedRate";
            this.lblProcessSpeedRate.Size = new System.Drawing.Size(230, 21);
            this.lblProcessSpeedRate.TabIndex = 67;
            this.lblProcessSpeedRate.Text = "Speed Rate :";
            this.lblProcessSpeedRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessPositionRate
            // 
            this.lblProcessPositionRate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessPositionRate.ForeColor = System.Drawing.Color.Black;
            this.lblProcessPositionRate.Location = new System.Drawing.Point(3, 79);
            this.lblProcessPositionRate.Name = "lblProcessPositionRate";
            this.lblProcessPositionRate.Size = new System.Drawing.Size(230, 21);
            this.lblProcessPositionRate.TabIndex = 65;
            this.lblProcessPositionRate.Text = "Position Rate :";
            this.lblProcessPositionRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProcessAxisNo
            // 
            this.lblProcessAxisNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessAxisNo.ForeColor = System.Drawing.Color.Black;
            this.lblProcessAxisNo.Location = new System.Drawing.Point(3, 52);
            this.lblProcessAxisNo.Name = "lblProcessAxisNo";
            this.lblProcessAxisNo.Size = new System.Drawing.Size(230, 21);
            this.lblProcessAxisNo.TabIndex = 63;
            this.lblProcessAxisNo.Text = "AxisNo.";
            this.lblProcessAxisNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProcessAxisNo
            // 
            this.txtProcessAxisNo.Location = new System.Drawing.Point(239, 51);
            this.txtProcessAxisNo.Name = "txtProcessAxisNo";
            this.txtProcessAxisNo.Size = new System.Drawing.Size(170, 23);
            this.txtProcessAxisNo.TabIndex = 62;
            // 
            // lblProcessAxisParameter
            // 
            this.lblProcessAxisParameter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblProcessAxisParameter.ForeColor = System.Drawing.Color.Black;
            this.lblProcessAxisParameter.Location = new System.Drawing.Point(3, 26);
            this.lblProcessAxisParameter.Name = "lblProcessAxisParameter";
            this.lblProcessAxisParameter.Size = new System.Drawing.Size(230, 21);
            this.lblProcessAxisParameter.TabIndex = 61;
            this.lblProcessAxisParameter.Text = "Axis Parameter";
            this.lblProcessAxisParameter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(1601, 824);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(136, 30);
            this.btnSave.TabIndex = 47;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnMonitorLoader);
            this.flowLayoutPanel2.Controls.Add(this.btnMonitorProcess);
            this.flowLayoutPanel2.Controls.Add(this.btnMonitorUnloader);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1740, 47);
            this.flowLayoutPanel2.TabIndex = 48;
            // 
            // btnMonitorLoader
            // 
            this.btnMonitorLoader.Location = new System.Drawing.Point(3, 3);
            this.btnMonitorLoader.Name = "btnMonitorLoader";
            this.btnMonitorLoader.Size = new System.Drawing.Size(574, 41);
            this.btnMonitorLoader.TabIndex = 0;
            this.btnMonitorLoader.Text = "로더";
            this.btnMonitorLoader.UseVisualStyleBackColor = true;
            this.btnMonitorLoader.Click += new System.EventHandler(this.btnMonitorLoader_Click);
            // 
            // btnMonitorProcess
            // 
            this.btnMonitorProcess.Location = new System.Drawing.Point(583, 3);
            this.btnMonitorProcess.Name = "btnMonitorProcess";
            this.btnMonitorProcess.Size = new System.Drawing.Size(574, 41);
            this.btnMonitorProcess.TabIndex = 1;
            this.btnMonitorProcess.Text = "프로세스";
            this.btnMonitorProcess.UseVisualStyleBackColor = true;
            this.btnMonitorProcess.Click += new System.EventHandler(this.btnMonitorLoader_Click);
            // 
            // btnMonitorUnloader
            // 
            this.btnMonitorUnloader.Location = new System.Drawing.Point(1163, 3);
            this.btnMonitorUnloader.Name = "btnMonitorUnloader";
            this.btnMonitorUnloader.Size = new System.Drawing.Size(574, 41);
            this.btnMonitorUnloader.TabIndex = 2;
            this.btnMonitorUnloader.Text = "언로드";
            this.btnMonitorUnloader.UseVisualStyleBackColor = true;
            this.btnMonitorUnloader.Click += new System.EventHandler(this.btnMonitorLoader_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.btnAjinMotor);
            this.flowLayoutPanel4.Controls.Add(this.btnUmacMotor);
            this.flowLayoutPanel4.Controls.Add(this.btnEZiMotor);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(0, 47);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1740, 43);
            this.flowLayoutPanel4.TabIndex = 49;
            // 
            // btnAjinMotor
            // 
            this.btnAjinMotor.Location = new System.Drawing.Point(3, 3);
            this.btnAjinMotor.Name = "btnAjinMotor";
            this.btnAjinMotor.Size = new System.Drawing.Size(574, 35);
            this.btnAjinMotor.TabIndex = 1;
            this.btnAjinMotor.Text = "AJIN";
            this.btnAjinMotor.UseVisualStyleBackColor = true;
            this.btnAjinMotor.Click += new System.EventHandler(this.btnAjinMotor_Click);
            // 
            // btnUmacMotor
            // 
            this.btnUmacMotor.Location = new System.Drawing.Point(583, 3);
            this.btnUmacMotor.Name = "btnUmacMotor";
            this.btnUmacMotor.Size = new System.Drawing.Size(574, 35);
            this.btnUmacMotor.TabIndex = 2;
            this.btnUmacMotor.Text = "UMAC";
            this.btnUmacMotor.UseVisualStyleBackColor = true;
            this.btnUmacMotor.Click += new System.EventHandler(this.btnAjinMotor_Click);
            // 
            // btnEZiMotor
            // 
            this.btnEZiMotor.Location = new System.Drawing.Point(1163, 3);
            this.btnEZiMotor.Name = "btnEZiMotor";
            this.btnEZiMotor.Size = new System.Drawing.Size(574, 35);
            this.btnEZiMotor.TabIndex = 3;
            this.btnEZiMotor.Text = "E-ZI";
            this.btnEZiMotor.UseVisualStyleBackColor = true;
            this.btnEZiMotor.Click += new System.EventHandler(this.btnAjinMotor_Click);
            // 
            // pnlAjinMotorParamInfo
            // 
            this.pnlAjinMotorParamInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInitInPosition);
            this.pnlAjinMotorParamInfo.Controls.Add(this.label1);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinInitInposition);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinAxisParameter);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInitDecelation);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinAxisNo);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinInitDecelation);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinAxisNo);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInitAccelation);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinPulseOutput);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinInitAccelation);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinPulseOutput);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinSWLimitEnable);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinEncInput);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinSWLimitEnable);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinEncInput);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinSWLimitStopMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinAbsRelMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinSWLimitStopMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinAbsRelMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeOffset);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinVelProfileMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeOffset);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinVelProfileMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeClearTime);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinMotionSignalSetting);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeClearTime);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInPosition);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeAccelation2);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinInposition);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeAccelation2);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinAlarm);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeAccelation1);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinAlarm);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeAccelation1);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinMinusEndLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinHomeZPhase);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinMinusEndLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeZPhase);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinPlusEndLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinHomeDirection);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinPlusEndLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeDirection);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinServoOnLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinEncoderType);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinServoOnLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinEncoderType);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHome);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinAlarmResetLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeSignal);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinAlarmResetLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinHomeSignal);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinStopLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinStopLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinHomeLevel);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinStopMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeVel1);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinStopMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeVel1);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinZPhase);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeVel2);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinZPhase);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeVel2);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinPulse);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeVel3);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinPulse);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeVel3);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinUnit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinHomeVelLast);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinUnit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinHomeVelLast);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinMinVelocitu);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinSW);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinMinVelocitu);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinSWMinusLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinMaxVelocitu);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinSWMinusLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinMaxVelocitu);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinSWPlusLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInitAccelationTime);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinSWPlusLimit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinInitAccelationTime);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinSWLimitMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInitPtpSpeed);
            this.pnlAjinMotorParamInfo.Controls.Add(this.cbxtAjinSWLimitMode);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinInitPtpSpeed);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInit);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInitVelocity);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinInitPosition);
            this.pnlAjinMotorParamInfo.Controls.Add(this.txtAjinInitVelocity);
            this.pnlAjinMotorParamInfo.Controls.Add(this.lblAjinInitPosition);
            this.pnlAjinMotorParamInfo.Location = new System.Drawing.Point(723, 96);
            this.pnlAjinMotorParamInfo.Name = "pnlAjinMotorParamInfo";
            this.pnlAjinMotorParamInfo.Size = new System.Drawing.Size(872, 758);
            this.pnlAjinMotorParamInfo.TabIndex = 464;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(870, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ 축 설정 정보";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.flpSevoMotors);
            this.panel3.Location = new System.Drawing.Point(5, 95);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(713, 759);
            this.panel3.TabIndex = 465;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(711, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ 축 리스트";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flpSevoMotors
            // 
            this.flpSevoMotors.AutoScroll = true;
            this.flpSevoMotors.Location = new System.Drawing.Point(1, 27);
            this.flpSevoMotors.Name = "flpSevoMotors";
            this.flpSevoMotors.Size = new System.Drawing.Size(707, 727);
            this.flpSevoMotors.TabIndex = 84;
            // 
            // pnlUmacMotorParamInfo
            // 
            this.pnlUmacMotorParamInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessSWLimitLow);
            this.pnlUmacMotorParamInfo.Controls.Add(this.label3);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessPtpSpeed);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessStepSpeed);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessAxisParameter);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessJogSpeed);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessAxisNo);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessHomeAcceleration);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessAxisNo);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessHome2Velocity);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessPositionRate);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessHome1Velocity);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessSpeedRate);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessDefaultAcceleration);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessMoveTimeOut);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessDefauleVelocity);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessInPosition);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessInPosition);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessDefauleVelocity);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessMoveTimeOut);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessDefaultAcceleration);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessSpeedRate);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessHome1Velocity);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessPositionRate);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lbl_Process_Home2Velocity);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessAccelationTime);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessHomeAcceleration);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessAccelationTime);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessJogSpeed);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessSWLimitHigh);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessStepSpeed);
            this.pnlUmacMotorParamInfo.Controls.Add(this.txtProcessSWLimitHigh);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessPtpSpeed);
            this.pnlUmacMotorParamInfo.Controls.Add(this.lblProcessSWLimitLow);
            this.pnlUmacMotorParamInfo.Location = new System.Drawing.Point(723, 96);
            this.pnlUmacMotorParamInfo.Name = "pnlUmacMotorParamInfo";
            this.pnlUmacMotorParamInfo.Size = new System.Drawing.Size(872, 758);
            this.pnlUmacMotorParamInfo.TabIndex = 465;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(870, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "■ 축 설정 정보";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlEZiMotorParamInfo
            // 
            this.pnlEZiMotorParamInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziStopCurrent);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziBoostCurrent);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziRunCurrent);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziRunCurrent);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziAxisStartSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lbEziAxisStartSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziPositionLoopGain);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziOrgSensorLogic);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txEziOrgDir);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziLimitSensorLogic);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtHWLimitStopMethod);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziSWLimitStopMethod);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtSWLimitMinusValue);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziSWLimitPlusValue);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziJogAccDecTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziJogStartSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziJogSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziSpeedOverride);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziAxisDecTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziAxisAccSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziAxisMaxSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziPulsePerRevolution);
            this.pnlEZiMotorParamInfo.Controls.Add(this.label52);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziAxisParameter);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziAxisNo);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziAxisNo);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziPulsePerRevolution);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziAxisMaxSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziAxisAccSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziAxisDecTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziSpeedOverride);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziJogSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziJogStartSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziJogAccDecTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziSWLimitPlusValue);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziSWLimitMinusValue);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziSWLimitStopMethod);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziHWLimitStopMethod);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziLimitSensorLogic);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziOrgSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziStopCurrent);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziOrgSearchSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziBoostCurrent);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgSearchSpeed);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziBrakeDelayTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziAccDecTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziBrakeDelayTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgAccDecTime);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziPosErrorOverflowLimit);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziOrgMethod);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziPosErrorOverflowLimit);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgMethod);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgTorqueRatio);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgDir);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziOrgTorqueRatio);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziOrgOffset);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziLimitSensorDir);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgOffset);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziLimitSensorDir);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziOrgPositionSet);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziMotionDir);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgPositionSet);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziMotionDir);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziOrgSensorLogic);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziPosTrackingLimit);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziPosTrackingLimit);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziPositionLoopGain);
            this.pnlEZiMotorParamInfo.Controls.Add(this.txtEziInposValue);
            this.pnlEZiMotorParamInfo.Controls.Add(this.lblEziInposValue);
            this.pnlEZiMotorParamInfo.Location = new System.Drawing.Point(723, 96);
            this.pnlEZiMotorParamInfo.Name = "pnlEZiMotorParamInfo";
            this.pnlEZiMotorParamInfo.Size = new System.Drawing.Size(872, 758);
            this.pnlEZiMotorParamInfo.TabIndex = 464;
            // 
            // txtEziStopCurrent
            // 
            this.txtEziStopCurrent.Location = new System.Drawing.Point(694, 234);
            this.txtEziStopCurrent.Name = "txtEziStopCurrent";
            this.txtEziStopCurrent.Size = new System.Drawing.Size(170, 23);
            this.txtEziStopCurrent.TabIndex = 189;
            // 
            // txtEziBoostCurrent
            // 
            this.txtEziBoostCurrent.Location = new System.Drawing.Point(694, 208);
            this.txtEziBoostCurrent.Name = "txtEziBoostCurrent";
            this.txtEziBoostCurrent.Size = new System.Drawing.Size(170, 23);
            this.txtEziBoostCurrent.TabIndex = 188;
            // 
            // lblEziRunCurrent
            // 
            this.lblEziRunCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziRunCurrent.ForeColor = System.Drawing.Color.Black;
            this.lblEziRunCurrent.Location = new System.Drawing.Point(458, 183);
            this.lblEziRunCurrent.Name = "lblEziRunCurrent";
            this.lblEziRunCurrent.Size = new System.Drawing.Size(230, 21);
            this.lblEziRunCurrent.TabIndex = 187;
            this.lblEziRunCurrent.Text = "Run Current [*10%] :";
            this.lblEziRunCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziRunCurrent
            // 
            this.txtEziRunCurrent.Location = new System.Drawing.Point(694, 182);
            this.txtEziRunCurrent.Name = "txtEziRunCurrent";
            this.txtEziRunCurrent.Size = new System.Drawing.Size(170, 23);
            this.txtEziRunCurrent.TabIndex = 186;
            // 
            // txtEziAxisStartSpeed
            // 
            this.txtEziAxisStartSpeed.Location = new System.Drawing.Point(239, 130);
            this.txtEziAxisStartSpeed.Name = "txtEziAxisStartSpeed";
            this.txtEziAxisStartSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtEziAxisStartSpeed.TabIndex = 185;
            // 
            // lbEziAxisStartSpeed
            // 
            this.lbEziAxisStartSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbEziAxisStartSpeed.ForeColor = System.Drawing.Color.Black;
            this.lbEziAxisStartSpeed.Location = new System.Drawing.Point(3, 130);
            this.lbEziAxisStartSpeed.Name = "lbEziAxisStartSpeed";
            this.lbEziAxisStartSpeed.Size = new System.Drawing.Size(230, 21);
            this.lbEziAxisStartSpeed.TabIndex = 184;
            this.lbEziAxisStartSpeed.Text = "Axis Start Speed [pps] :";
            this.lbEziAxisStartSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziPositionLoopGain
            // 
            this.txtEziPositionLoopGain.Location = new System.Drawing.Point(239, 650);
            this.txtEziPositionLoopGain.Name = "txtEziPositionLoopGain";
            this.txtEziPositionLoopGain.Size = new System.Drawing.Size(170, 23);
            this.txtEziPositionLoopGain.TabIndex = 183;
            // 
            // txtEziOrgSensorLogic
            // 
            this.txtEziOrgSensorLogic.Location = new System.Drawing.Point(239, 624);
            this.txtEziOrgSensorLogic.Name = "txtEziOrgSensorLogic";
            this.txtEziOrgSensorLogic.Size = new System.Drawing.Size(170, 23);
            this.txtEziOrgSensorLogic.TabIndex = 182;
            // 
            // txEziOrgDir
            // 
            this.txEziOrgDir.Location = new System.Drawing.Point(239, 546);
            this.txEziOrgDir.Name = "txEziOrgDir";
            this.txEziOrgDir.Size = new System.Drawing.Size(170, 23);
            this.txEziOrgDir.TabIndex = 181;
            // 
            // txtEziLimitSensorLogic
            // 
            this.txtEziLimitSensorLogic.Location = new System.Drawing.Point(239, 416);
            this.txtEziLimitSensorLogic.Name = "txtEziLimitSensorLogic";
            this.txtEziLimitSensorLogic.Size = new System.Drawing.Size(170, 23);
            this.txtEziLimitSensorLogic.TabIndex = 180;
            // 
            // txtHWLimitStopMethod
            // 
            this.txtHWLimitStopMethod.Location = new System.Drawing.Point(239, 390);
            this.txtHWLimitStopMethod.Name = "txtHWLimitStopMethod";
            this.txtHWLimitStopMethod.Size = new System.Drawing.Size(170, 23);
            this.txtHWLimitStopMethod.TabIndex = 179;
            // 
            // txtEziSWLimitStopMethod
            // 
            this.txtEziSWLimitStopMethod.Location = new System.Drawing.Point(239, 364);
            this.txtEziSWLimitStopMethod.Name = "txtEziSWLimitStopMethod";
            this.txtEziSWLimitStopMethod.Size = new System.Drawing.Size(170, 23);
            this.txtEziSWLimitStopMethod.TabIndex = 178;
            // 
            // txtSWLimitMinusValue
            // 
            this.txtSWLimitMinusValue.Location = new System.Drawing.Point(239, 338);
            this.txtSWLimitMinusValue.Name = "txtSWLimitMinusValue";
            this.txtSWLimitMinusValue.Size = new System.Drawing.Size(170, 23);
            this.txtSWLimitMinusValue.TabIndex = 177;
            // 
            // txtEziSWLimitPlusValue
            // 
            this.txtEziSWLimitPlusValue.Location = new System.Drawing.Point(239, 312);
            this.txtEziSWLimitPlusValue.Name = "txtEziSWLimitPlusValue";
            this.txtEziSWLimitPlusValue.Size = new System.Drawing.Size(170, 23);
            this.txtEziSWLimitPlusValue.TabIndex = 176;
            // 
            // txtEziJogAccDecTime
            // 
            this.txtEziJogAccDecTime.Location = new System.Drawing.Point(239, 286);
            this.txtEziJogAccDecTime.Name = "txtEziJogAccDecTime";
            this.txtEziJogAccDecTime.Size = new System.Drawing.Size(170, 23);
            this.txtEziJogAccDecTime.TabIndex = 175;
            // 
            // txtEziJogStartSpeed
            // 
            this.txtEziJogStartSpeed.Location = new System.Drawing.Point(239, 260);
            this.txtEziJogStartSpeed.Name = "txtEziJogStartSpeed";
            this.txtEziJogStartSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtEziJogStartSpeed.TabIndex = 174;
            // 
            // txtEziJogSpeed
            // 
            this.txtEziJogSpeed.Location = new System.Drawing.Point(239, 234);
            this.txtEziJogSpeed.Name = "txtEziJogSpeed";
            this.txtEziJogSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtEziJogSpeed.TabIndex = 173;
            // 
            // txtEziSpeedOverride
            // 
            this.txtEziSpeedOverride.Location = new System.Drawing.Point(239, 208);
            this.txtEziSpeedOverride.Name = "txtEziSpeedOverride";
            this.txtEziSpeedOverride.Size = new System.Drawing.Size(170, 23);
            this.txtEziSpeedOverride.TabIndex = 172;
            // 
            // txtEziAxisDecTime
            // 
            this.txtEziAxisDecTime.Location = new System.Drawing.Point(239, 182);
            this.txtEziAxisDecTime.Name = "txtEziAxisDecTime";
            this.txtEziAxisDecTime.Size = new System.Drawing.Size(170, 23);
            this.txtEziAxisDecTime.TabIndex = 171;
            // 
            // txtEziAxisAccSpeed
            // 
            this.txtEziAxisAccSpeed.Location = new System.Drawing.Point(239, 156);
            this.txtEziAxisAccSpeed.Name = "txtEziAxisAccSpeed";
            this.txtEziAxisAccSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtEziAxisAccSpeed.TabIndex = 170;
            // 
            // txtEziAxisMaxSpeed
            // 
            this.txtEziAxisMaxSpeed.Location = new System.Drawing.Point(239, 104);
            this.txtEziAxisMaxSpeed.Name = "txtEziAxisMaxSpeed";
            this.txtEziAxisMaxSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtEziAxisMaxSpeed.TabIndex = 169;
            // 
            // txtEziPulsePerRevolution
            // 
            this.txtEziPulsePerRevolution.Location = new System.Drawing.Point(239, 78);
            this.txtEziPulsePerRevolution.Name = "txtEziPulsePerRevolution";
            this.txtEziPulsePerRevolution.Size = new System.Drawing.Size(170, 23);
            this.txtEziPulsePerRevolution.TabIndex = 168;
            // 
            // label52
            // 
            this.label52.AutoEllipsis = true;
            this.label52.BackColor = System.Drawing.Color.Gainsboro;
            this.label52.Dock = System.Windows.Forms.DockStyle.Top;
            this.label52.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(0, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(870, 24);
            this.label52.TabIndex = 9;
            this.label52.Text = "■ 축 설정 정보";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziAxisParameter
            // 
            this.lblEziAxisParameter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziAxisParameter.ForeColor = System.Drawing.Color.Black;
            this.lblEziAxisParameter.Location = new System.Drawing.Point(3, 26);
            this.lblEziAxisParameter.Name = "lblEziAxisParameter";
            this.lblEziAxisParameter.Size = new System.Drawing.Size(230, 21);
            this.lblEziAxisParameter.TabIndex = 61;
            this.lblEziAxisParameter.Text = "Axis Parameter";
            this.lblEziAxisParameter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziAxisNo
            // 
            this.txtEziAxisNo.Location = new System.Drawing.Point(239, 51);
            this.txtEziAxisNo.Name = "txtEziAxisNo";
            this.txtEziAxisNo.Size = new System.Drawing.Size(170, 23);
            this.txtEziAxisNo.TabIndex = 62;
            // 
            // lblEziAxisNo
            // 
            this.lblEziAxisNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziAxisNo.ForeColor = System.Drawing.Color.Black;
            this.lblEziAxisNo.Location = new System.Drawing.Point(3, 52);
            this.lblEziAxisNo.Name = "lblEziAxisNo";
            this.lblEziAxisNo.Size = new System.Drawing.Size(230, 21);
            this.lblEziAxisNo.TabIndex = 63;
            this.lblEziAxisNo.Text = "AxisNo.";
            this.lblEziAxisNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziPulsePerRevolution
            // 
            this.lblEziPulsePerRevolution.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziPulsePerRevolution.ForeColor = System.Drawing.Color.Black;
            this.lblEziPulsePerRevolution.Location = new System.Drawing.Point(3, 79);
            this.lblEziPulsePerRevolution.Name = "lblEziPulsePerRevolution";
            this.lblEziPulsePerRevolution.Size = new System.Drawing.Size(230, 21);
            this.lblEziPulsePerRevolution.TabIndex = 65;
            this.lblEziPulsePerRevolution.Text = "Pulse Per Revolution :";
            this.lblEziPulsePerRevolution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziAxisMaxSpeed
            // 
            this.lblEziAxisMaxSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziAxisMaxSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblEziAxisMaxSpeed.Location = new System.Drawing.Point(3, 105);
            this.lblEziAxisMaxSpeed.Name = "lblEziAxisMaxSpeed";
            this.lblEziAxisMaxSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblEziAxisMaxSpeed.TabIndex = 67;
            this.lblEziAxisMaxSpeed.Text = "Axis Max Speed [pps] :";
            this.lblEziAxisMaxSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziAxisAccSpeed
            // 
            this.lblEziAxisAccSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziAxisAccSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblEziAxisAccSpeed.Location = new System.Drawing.Point(3, 157);
            this.lblEziAxisAccSpeed.Name = "lblEziAxisAccSpeed";
            this.lblEziAxisAccSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblEziAxisAccSpeed.TabIndex = 69;
            this.lblEziAxisAccSpeed.Text = "Axis Acc Speed [msec] :";
            this.lblEziAxisAccSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziAxisDecTime
            // 
            this.lblEziAxisDecTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziAxisDecTime.ForeColor = System.Drawing.Color.Black;
            this.lblEziAxisDecTime.Location = new System.Drawing.Point(3, 183);
            this.lblEziAxisDecTime.Name = "lblEziAxisDecTime";
            this.lblEziAxisDecTime.Size = new System.Drawing.Size(230, 21);
            this.lblEziAxisDecTime.TabIndex = 71;
            this.lblEziAxisDecTime.Text = "Axis Dec Time [msec] :";
            this.lblEziAxisDecTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziSpeedOverride
            // 
            this.lblEziSpeedOverride.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziSpeedOverride.ForeColor = System.Drawing.Color.Black;
            this.lblEziSpeedOverride.Location = new System.Drawing.Point(3, 208);
            this.lblEziSpeedOverride.Name = "lblEziSpeedOverride";
            this.lblEziSpeedOverride.Size = new System.Drawing.Size(230, 21);
            this.lblEziSpeedOverride.TabIndex = 73;
            this.lblEziSpeedOverride.Text = "Speed Override [%] :";
            this.lblEziSpeedOverride.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziJogSpeed
            // 
            this.lblEziJogSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziJogSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblEziJogSpeed.Location = new System.Drawing.Point(3, 234);
            this.lblEziJogSpeed.Name = "lblEziJogSpeed";
            this.lblEziJogSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblEziJogSpeed.TabIndex = 75;
            this.lblEziJogSpeed.Text = "Jog Speed [pps] :";
            this.lblEziJogSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziJogStartSpeed
            // 
            this.lblEziJogStartSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziJogStartSpeed.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblEziJogStartSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblEziJogStartSpeed.Location = new System.Drawing.Point(3, 261);
            this.lblEziJogStartSpeed.Name = "lblEziJogStartSpeed";
            this.lblEziJogStartSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblEziJogStartSpeed.TabIndex = 77;
            this.lblEziJogStartSpeed.Text = "Jog Start Speed [pps] :";
            this.lblEziJogStartSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziJogAccDecTime
            // 
            this.lblEziJogAccDecTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziJogAccDecTime.ForeColor = System.Drawing.Color.Black;
            this.lblEziJogAccDecTime.Location = new System.Drawing.Point(3, 287);
            this.lblEziJogAccDecTime.Name = "lblEziJogAccDecTime";
            this.lblEziJogAccDecTime.Size = new System.Drawing.Size(230, 21);
            this.lblEziJogAccDecTime.TabIndex = 79;
            this.lblEziJogAccDecTime.Text = "Jog Acc Dec Time [msec] :";
            this.lblEziJogAccDecTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziSWLimitPlusValue
            // 
            this.lblEziSWLimitPlusValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziSWLimitPlusValue.ForeColor = System.Drawing.Color.Black;
            this.lblEziSWLimitPlusValue.Location = new System.Drawing.Point(3, 313);
            this.lblEziSWLimitPlusValue.Name = "lblEziSWLimitPlusValue";
            this.lblEziSWLimitPlusValue.Size = new System.Drawing.Size(230, 21);
            this.lblEziSWLimitPlusValue.TabIndex = 81;
            this.lblEziSWLimitPlusValue.Text = "S/W Limit Plus Value [pulse] :";
            this.lblEziSWLimitPlusValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziSWLimitMinusValue
            // 
            this.lblEziSWLimitMinusValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziSWLimitMinusValue.ForeColor = System.Drawing.Color.Black;
            this.lblEziSWLimitMinusValue.Location = new System.Drawing.Point(3, 339);
            this.lblEziSWLimitMinusValue.Name = "lblEziSWLimitMinusValue";
            this.lblEziSWLimitMinusValue.Size = new System.Drawing.Size(230, 21);
            this.lblEziSWLimitMinusValue.TabIndex = 83;
            this.lblEziSWLimitMinusValue.Text = "S/W Limit Minus Value [pulse] :";
            this.lblEziSWLimitMinusValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziSWLimitStopMethod
            // 
            this.lblEziSWLimitStopMethod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziSWLimitStopMethod.ForeColor = System.Drawing.Color.Black;
            this.lblEziSWLimitStopMethod.Location = new System.Drawing.Point(3, 364);
            this.lblEziSWLimitStopMethod.Name = "lblEziSWLimitStopMethod";
            this.lblEziSWLimitStopMethod.Size = new System.Drawing.Size(230, 21);
            this.lblEziSWLimitStopMethod.TabIndex = 85;
            this.lblEziSWLimitStopMethod.Text = "S/W Limit Stop Method :";
            this.lblEziSWLimitStopMethod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziHWLimitStopMethod
            // 
            this.lblEziHWLimitStopMethod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziHWLimitStopMethod.ForeColor = System.Drawing.Color.Black;
            this.lblEziHWLimitStopMethod.Location = new System.Drawing.Point(3, 389);
            this.lblEziHWLimitStopMethod.Name = "lblEziHWLimitStopMethod";
            this.lblEziHWLimitStopMethod.Size = new System.Drawing.Size(230, 21);
            this.lblEziHWLimitStopMethod.TabIndex = 87;
            this.lblEziHWLimitStopMethod.Text = "H/W Limit Stop Method :";
            this.lblEziHWLimitStopMethod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziLimitSensorLogic
            // 
            this.lblEziLimitSensorLogic.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziLimitSensorLogic.ForeColor = System.Drawing.Color.Black;
            this.lblEziLimitSensorLogic.Location = new System.Drawing.Point(3, 417);
            this.lblEziLimitSensorLogic.Name = "lblEziLimitSensorLogic";
            this.lblEziLimitSensorLogic.Size = new System.Drawing.Size(230, 21);
            this.lblEziLimitSensorLogic.TabIndex = 89;
            this.lblEziLimitSensorLogic.Text = "Limit Sensor Logic :";
            this.lblEziLimitSensorLogic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziOrgSpeed
            // 
            this.txtEziOrgSpeed.Location = new System.Drawing.Point(239, 442);
            this.txtEziOrgSpeed.Name = "txtEziOrgSpeed";
            this.txtEziOrgSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtEziOrgSpeed.TabIndex = 91;
            // 
            // lblEziStopCurrent
            // 
            this.lblEziStopCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziStopCurrent.ForeColor = System.Drawing.Color.Black;
            this.lblEziStopCurrent.Location = new System.Drawing.Point(458, 235);
            this.lblEziStopCurrent.Name = "lblEziStopCurrent";
            this.lblEziStopCurrent.Size = new System.Drawing.Size(230, 21);
            this.lblEziStopCurrent.TabIndex = 130;
            this.lblEziStopCurrent.Text = "Stop Current [*10%] :";
            this.lblEziStopCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziOrgSpeed
            // 
            this.lblEziOrgSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgSpeed.Location = new System.Drawing.Point(3, 443);
            this.lblEziOrgSpeed.Name = "lblEziOrgSpeed";
            this.lblEziOrgSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgSpeed.TabIndex = 92;
            this.lblEziOrgSpeed.Text = "Org Speed [pps] :";
            this.lblEziOrgSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziOrgSearchSpeed
            // 
            this.txtEziOrgSearchSpeed.Location = new System.Drawing.Point(239, 468);
            this.txtEziOrgSearchSpeed.Name = "txtEziOrgSearchSpeed";
            this.txtEziOrgSearchSpeed.Size = new System.Drawing.Size(170, 23);
            this.txtEziOrgSearchSpeed.TabIndex = 93;
            // 
            // lblEziBoostCurrent
            // 
            this.lblEziBoostCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziBoostCurrent.ForeColor = System.Drawing.Color.Black;
            this.lblEziBoostCurrent.Location = new System.Drawing.Point(458, 208);
            this.lblEziBoostCurrent.Name = "lblEziBoostCurrent";
            this.lblEziBoostCurrent.Size = new System.Drawing.Size(230, 21);
            this.lblEziBoostCurrent.TabIndex = 128;
            this.lblEziBoostCurrent.Text = "Boost Current [*50%] :";
            this.lblEziBoostCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziOrgSearchSpeed
            // 
            this.lblEziOrgSearchSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgSearchSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgSearchSpeed.Location = new System.Drawing.Point(3, 469);
            this.lblEziOrgSearchSpeed.Name = "lblEziOrgSearchSpeed";
            this.lblEziOrgSearchSpeed.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgSearchSpeed.TabIndex = 94;
            this.lblEziOrgSearchSpeed.Text = "Org Search Speed [pps] :";
            this.lblEziOrgSearchSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziBrakeDelayTime
            // 
            this.lblEziBrakeDelayTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziBrakeDelayTime.ForeColor = System.Drawing.Color.Black;
            this.lblEziBrakeDelayTime.Location = new System.Drawing.Point(458, 157);
            this.lblEziBrakeDelayTime.Name = "lblEziBrakeDelayTime";
            this.lblEziBrakeDelayTime.Size = new System.Drawing.Size(230, 21);
            this.lblEziBrakeDelayTime.TabIndex = 125;
            this.lblEziBrakeDelayTime.Text = "Brake Delay Time [msec] :";
            this.lblEziBrakeDelayTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziAccDecTime
            // 
            this.txtEziAccDecTime.Location = new System.Drawing.Point(239, 494);
            this.txtEziAccDecTime.Name = "txtEziAccDecTime";
            this.txtEziAccDecTime.Size = new System.Drawing.Size(170, 23);
            this.txtEziAccDecTime.TabIndex = 95;
            // 
            // txtEziBrakeDelayTime
            // 
            this.txtEziBrakeDelayTime.Location = new System.Drawing.Point(694, 156);
            this.txtEziBrakeDelayTime.Name = "txtEziBrakeDelayTime";
            this.txtEziBrakeDelayTime.Size = new System.Drawing.Size(170, 23);
            this.txtEziBrakeDelayTime.TabIndex = 124;
            // 
            // lblEziOrgAccDecTime
            // 
            this.lblEziOrgAccDecTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgAccDecTime.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgAccDecTime.Location = new System.Drawing.Point(3, 495);
            this.lblEziOrgAccDecTime.Name = "lblEziOrgAccDecTime";
            this.lblEziOrgAccDecTime.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgAccDecTime.TabIndex = 96;
            this.lblEziOrgAccDecTime.Text = "Org Acc Dec Time [msec] :";
            this.lblEziOrgAccDecTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziPosErrorOverflowLimit
            // 
            this.lblEziPosErrorOverflowLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziPosErrorOverflowLimit.ForeColor = System.Drawing.Color.Black;
            this.lblEziPosErrorOverflowLimit.Location = new System.Drawing.Point(458, 131);
            this.lblEziPosErrorOverflowLimit.Name = "lblEziPosErrorOverflowLimit";
            this.lblEziPosErrorOverflowLimit.Size = new System.Drawing.Size(230, 21);
            this.lblEziPosErrorOverflowLimit.TabIndex = 123;
            this.lblEziPosErrorOverflowLimit.Text = "Pos. Error Overflow Limit [pulse] :";
            this.lblEziPosErrorOverflowLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziOrgMethod
            // 
            this.txtEziOrgMethod.Location = new System.Drawing.Point(239, 520);
            this.txtEziOrgMethod.Name = "txtEziOrgMethod";
            this.txtEziOrgMethod.Size = new System.Drawing.Size(170, 23);
            this.txtEziOrgMethod.TabIndex = 97;
            // 
            // txtEziPosErrorOverflowLimit
            // 
            this.txtEziPosErrorOverflowLimit.Location = new System.Drawing.Point(694, 130);
            this.txtEziPosErrorOverflowLimit.Name = "txtEziPosErrorOverflowLimit";
            this.txtEziPosErrorOverflowLimit.Size = new System.Drawing.Size(170, 23);
            this.txtEziPosErrorOverflowLimit.TabIndex = 122;
            // 
            // lblEziOrgMethod
            // 
            this.lblEziOrgMethod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgMethod.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgMethod.Location = new System.Drawing.Point(3, 521);
            this.lblEziOrgMethod.Name = "lblEziOrgMethod";
            this.lblEziOrgMethod.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgMethod.TabIndex = 98;
            this.lblEziOrgMethod.Text = "Org Method :";
            this.lblEziOrgMethod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziOrgTorqueRatio
            // 
            this.lblEziOrgTorqueRatio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgTorqueRatio.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgTorqueRatio.Location = new System.Drawing.Point(458, 105);
            this.lblEziOrgTorqueRatio.Name = "lblEziOrgTorqueRatio";
            this.lblEziOrgTorqueRatio.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgTorqueRatio.TabIndex = 121;
            this.lblEziOrgTorqueRatio.Text = "Org Torque Ratio [%] :";
            this.lblEziOrgTorqueRatio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziOrgDir
            // 
            this.lblEziOrgDir.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgDir.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgDir.Location = new System.Drawing.Point(3, 546);
            this.lblEziOrgDir.Name = "lblEziOrgDir";
            this.lblEziOrgDir.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgDir.TabIndex = 100;
            this.lblEziOrgDir.Text = "Org Dir :";
            this.lblEziOrgDir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziOrgTorqueRatio
            // 
            this.txtEziOrgTorqueRatio.Location = new System.Drawing.Point(694, 104);
            this.txtEziOrgTorqueRatio.Name = "txtEziOrgTorqueRatio";
            this.txtEziOrgTorqueRatio.Size = new System.Drawing.Size(170, 23);
            this.txtEziOrgTorqueRatio.TabIndex = 120;
            // 
            // txtEziOrgOffset
            // 
            this.txtEziOrgOffset.Location = new System.Drawing.Point(239, 571);
            this.txtEziOrgOffset.Name = "txtEziOrgOffset";
            this.txtEziOrgOffset.Size = new System.Drawing.Size(170, 23);
            this.txtEziOrgOffset.TabIndex = 101;
            // 
            // lblEziLimitSensorDir
            // 
            this.lblEziLimitSensorDir.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziLimitSensorDir.ForeColor = System.Drawing.Color.Black;
            this.lblEziLimitSensorDir.Location = new System.Drawing.Point(458, 78);
            this.lblEziLimitSensorDir.Name = "lblEziLimitSensorDir";
            this.lblEziLimitSensorDir.Size = new System.Drawing.Size(230, 21);
            this.lblEziLimitSensorDir.TabIndex = 119;
            this.lblEziLimitSensorDir.Text = "Limit Sensor Dir";
            this.lblEziLimitSensorDir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziOrgOffset
            // 
            this.lblEziOrgOffset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgOffset.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgOffset.Location = new System.Drawing.Point(3, 572);
            this.lblEziOrgOffset.Name = "lblEziOrgOffset";
            this.lblEziOrgOffset.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgOffset.TabIndex = 102;
            this.lblEziOrgOffset.Text = "Org Offset [pulse] :";
            this.lblEziOrgOffset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziLimitSensorDir
            // 
            this.txtEziLimitSensorDir.Location = new System.Drawing.Point(694, 77);
            this.txtEziLimitSensorDir.Name = "txtEziLimitSensorDir";
            this.txtEziLimitSensorDir.Size = new System.Drawing.Size(170, 23);
            this.txtEziLimitSensorDir.TabIndex = 118;
            // 
            // txtEziOrgPositionSet
            // 
            this.txtEziOrgPositionSet.Location = new System.Drawing.Point(239, 598);
            this.txtEziOrgPositionSet.Name = "txtEziOrgPositionSet";
            this.txtEziOrgPositionSet.Size = new System.Drawing.Size(170, 23);
            this.txtEziOrgPositionSet.TabIndex = 103;
            // 
            // lblEziMotionDir
            // 
            this.lblEziMotionDir.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziMotionDir.ForeColor = System.Drawing.Color.Black;
            this.lblEziMotionDir.Location = new System.Drawing.Point(3, 729);
            this.lblEziMotionDir.Name = "lblEziMotionDir";
            this.lblEziMotionDir.Size = new System.Drawing.Size(230, 21);
            this.lblEziMotionDir.TabIndex = 117;
            this.lblEziMotionDir.Text = "Motion Dir :";
            this.lblEziMotionDir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziOrgPositionSet
            // 
            this.lblEziOrgPositionSet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgPositionSet.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgPositionSet.Location = new System.Drawing.Point(3, 599);
            this.lblEziOrgPositionSet.Name = "lblEziOrgPositionSet";
            this.lblEziOrgPositionSet.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgPositionSet.TabIndex = 104;
            this.lblEziOrgPositionSet.Text = "Org Position Set [pulse] :";
            this.lblEziOrgPositionSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziMotionDir
            // 
            this.txtEziMotionDir.Location = new System.Drawing.Point(239, 728);
            this.txtEziMotionDir.Name = "txtEziMotionDir";
            this.txtEziMotionDir.Size = new System.Drawing.Size(170, 23);
            this.txtEziMotionDir.TabIndex = 116;
            // 
            // lblEziOrgSensorLogic
            // 
            this.lblEziOrgSensorLogic.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziOrgSensorLogic.ForeColor = System.Drawing.Color.Black;
            this.lblEziOrgSensorLogic.Location = new System.Drawing.Point(3, 625);
            this.lblEziOrgSensorLogic.Name = "lblEziOrgSensorLogic";
            this.lblEziOrgSensorLogic.Size = new System.Drawing.Size(230, 21);
            this.lblEziOrgSensorLogic.TabIndex = 106;
            this.lblEziOrgSensorLogic.Text = "Org Sensor Logic :";
            this.lblEziOrgSensorLogic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEziPosTrackingLimit
            // 
            this.lblEziPosTrackingLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziPosTrackingLimit.ForeColor = System.Drawing.Color.Black;
            this.lblEziPosTrackingLimit.Location = new System.Drawing.Point(3, 703);
            this.lblEziPosTrackingLimit.Name = "lblEziPosTrackingLimit";
            this.lblEziPosTrackingLimit.Size = new System.Drawing.Size(230, 21);
            this.lblEziPosTrackingLimit.TabIndex = 115;
            this.lblEziPosTrackingLimit.Text = "Pos Tracking Limit [pulse] :";
            this.lblEziPosTrackingLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziPosTrackingLimit
            // 
            this.txtEziPosTrackingLimit.Location = new System.Drawing.Point(239, 702);
            this.txtEziPosTrackingLimit.Name = "txtEziPosTrackingLimit";
            this.txtEziPosTrackingLimit.Size = new System.Drawing.Size(170, 23);
            this.txtEziPosTrackingLimit.TabIndex = 114;
            // 
            // lblEziPositionLoopGain
            // 
            this.lblEziPositionLoopGain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziPositionLoopGain.ForeColor = System.Drawing.Color.Black;
            this.lblEziPositionLoopGain.Location = new System.Drawing.Point(3, 650);
            this.lblEziPositionLoopGain.Name = "lblEziPositionLoopGain";
            this.lblEziPositionLoopGain.Size = new System.Drawing.Size(230, 21);
            this.lblEziPositionLoopGain.TabIndex = 108;
            this.lblEziPositionLoopGain.Text = "Position Loop Gain";
            this.lblEziPositionLoopGain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEziInposValue
            // 
            this.txtEziInposValue.Location = new System.Drawing.Point(239, 676);
            this.txtEziInposValue.Name = "txtEziInposValue";
            this.txtEziInposValue.Size = new System.Drawing.Size(170, 23);
            this.txtEziInposValue.TabIndex = 110;
            // 
            // lblEziInposValue
            // 
            this.lblEziInposValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEziInposValue.ForeColor = System.Drawing.Color.Black;
            this.lblEziInposValue.Location = new System.Drawing.Point(3, 676);
            this.lblEziInposValue.Name = "lblEziInposValue";
            this.lblEziInposValue.Size = new System.Drawing.Size(230, 21);
            this.lblEziInposValue.TabIndex = 111;
            this.lblEziInposValue.Text = "Inpos Value :";
            this.lblEziInposValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlParameterAxis
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.pnlAjinMotorParamInfo);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.pnlEZiMotorParamInfo);
            this.Controls.Add(this.pnlUmacMotorParamInfo);
            this.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Location = new System.Drawing.Point(723, 96);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UcrlParameterAxis";
            this.Size = new System.Drawing.Size(1740, 875);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.pnlAjinMotorParamInfo.ResumeLayout(false);
            this.pnlAjinMotorParamInfo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.pnlUmacMotorParamInfo.ResumeLayout(false);
            this.pnlUmacMotorParamInfo.PerformLayout();
            this.pnlEZiMotorParamInfo.ResumeLayout(false);
            this.pnlEZiMotorParamInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblAjinInitAccelationTime;
        private System.Windows.Forms.TextBox txtAjinInitAccelationTime;
        private System.Windows.Forms.Label lblAjinInitPtpSpeed;
        private System.Windows.Forms.TextBox txtAjinInitPtpSpeed;
        private System.Windows.Forms.Label lblAjinInitVelocity;
        private System.Windows.Forms.TextBox txtAjinInitVelocity;
        private System.Windows.Forms.Label lblAjinInitPosition;
        private System.Windows.Forms.TextBox txtAjinInitPosition;
        private System.Windows.Forms.Label lblAjinInit;
        private System.Windows.Forms.ComboBox cbxtAjinSWLimitMode;
        private System.Windows.Forms.Label lblAjinSWLimitMode;
        private System.Windows.Forms.Label lblAjinSWPlusLimit;
        private System.Windows.Forms.TextBox txtAjinSWPlusLimit;
        private System.Windows.Forms.Label lblAjinSWMinusLimit;
        private System.Windows.Forms.TextBox txtAjinSWMinusLimit;
        private System.Windows.Forms.Label lblAjinSW;
        private System.Windows.Forms.Label lblAjinHomeVelLast;
        private System.Windows.Forms.TextBox txtAjinHomeVelLast;
        private System.Windows.Forms.Label lblAjinHomeVel3;
        private System.Windows.Forms.TextBox txtAjinHomeVel3;
        private System.Windows.Forms.Label lblAjinHomeVel2;
        private System.Windows.Forms.TextBox txtAjinHomeVel2;
        private System.Windows.Forms.Label lblAjinHomeVel1;
        private System.Windows.Forms.TextBox txtAjinHomeVel1;
        private System.Windows.Forms.ComboBox cbxtAjinHomeLevel;
        private System.Windows.Forms.Label lblAjinHomeLevel;
        private System.Windows.Forms.ComboBox cbxtAjinHomeSignal;
        private System.Windows.Forms.Label lblAjinHomeSignal;
        private System.Windows.Forms.Label lblAjinHome;
        private System.Windows.Forms.ComboBox cbxtAjinServoOnLevel;
        private System.Windows.Forms.Label lblAjinServoOnLevel;
        private System.Windows.Forms.ComboBox cbxtAjinPlusEndLimit;
        private System.Windows.Forms.Label lblAjinPlusEndLimit;
        private System.Windows.Forms.ComboBox cbxtAjinMinusEndLimit;
        private System.Windows.Forms.Label lblAjinMinusEndLimit;
        private System.Windows.Forms.ComboBox cbxtAjinAlarm;
        private System.Windows.Forms.Label lblAjinAlarm;
        private System.Windows.Forms.ComboBox cbxtAjinInposition;
        private System.Windows.Forms.Label lblAjinInPosition;
        private System.Windows.Forms.Label lblAjinMotionSignalSetting;
        private System.Windows.Forms.ComboBox cbxtAjinVelProfileMode;
        private System.Windows.Forms.Label lblAjinVelProfileMode;
        private System.Windows.Forms.ComboBox cbxtAjinAbsRelMode;
        private System.Windows.Forms.Label lblAjinAbsRelMode;
        private System.Windows.Forms.ComboBox cbxtAjinEncInput;
        private System.Windows.Forms.Label lblAjinEncInput;
        private System.Windows.Forms.ComboBox cbxtAjinPulseOutput;
        private System.Windows.Forms.Label lblAjinPulseOutput;
        private System.Windows.Forms.Label lblAjinAxisNo;
        private System.Windows.Forms.TextBox txtAjinAxisNo;
        private System.Windows.Forms.Label lblAjinAxisParameter;
        private System.Windows.Forms.ComboBox cbxtAjinZPhase;
        private System.Windows.Forms.Label lblAjinZPhase;
        private System.Windows.Forms.Label lblAjinPulse;
        private System.Windows.Forms.TextBox txtAjinPulse;
        private System.Windows.Forms.Label lblAjinUnit;
        private System.Windows.Forms.TextBox txtAjinUnit;
        private System.Windows.Forms.Label lblAjinMinVelocitu;
        private System.Windows.Forms.TextBox txtAjinMinVelocitu;
        private System.Windows.Forms.Label lblAjinMaxVelocitu;
        private System.Windows.Forms.TextBox txtAjinMaxVelocitu;
        private System.Windows.Forms.ComboBox cbxtAjinHomeZPhase;
        private System.Windows.Forms.Label lblAjinHomeZPhase;
        private System.Windows.Forms.ComboBox cbxtAjinHomeDirection;
        private System.Windows.Forms.Label lblAjinHomeDirection;
        private System.Windows.Forms.ComboBox cbxtAjinEncoderType;
        private System.Windows.Forms.Label lblAjinEncoderType;
        private System.Windows.Forms.ComboBox cbxtAjinAlarmResetLevel;
        private System.Windows.Forms.Label lblAjinAlarmResetLevel;
        private System.Windows.Forms.ComboBox cbxtAjinStopLevel;
        private System.Windows.Forms.Label lblAjinStopLevel;
        private System.Windows.Forms.ComboBox cbxtAjinStopMode;
        private System.Windows.Forms.Label lblAjinStopMode;
        private System.Windows.Forms.Label lblAjinHomeAccelation2;
        private System.Windows.Forms.TextBox txtAjinHomeAccelation2;
        private System.Windows.Forms.Label lblAjinHomeAccelation1;
        private System.Windows.Forms.TextBox txtAjinHomeAccelation1;
        private System.Windows.Forms.Label lblAjinInitInPosition;
        private System.Windows.Forms.TextBox txtAjinInitInposition;
        private System.Windows.Forms.Label lblAjinInitDecelation;
        private System.Windows.Forms.TextBox txtAjinInitDecelation;
        private System.Windows.Forms.Label lblAjinInitAccelation;
        private System.Windows.Forms.TextBox txtAjinInitAccelation;
        private System.Windows.Forms.ComboBox cbxtAjinSWLimitEnable;
        private System.Windows.Forms.Label lblAjinSWLimitEnable;
        private System.Windows.Forms.ComboBox cbxtAjinSWLimitStopMode;
        private System.Windows.Forms.Label lblAjinSWLimitStopMode;
        private System.Windows.Forms.Label lblAjinHomeOffset;
        private System.Windows.Forms.TextBox txtAjinHomeOffset;
        private System.Windows.Forms.Label lblAjinHomeClearTime;
        private System.Windows.Forms.TextBox txtAjinHomeClearTime;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtProcessSWLimitLow;
        private System.Windows.Forms.TextBox txtProcessPtpSpeed;
        private System.Windows.Forms.TextBox txtProcessStepSpeed;
        private System.Windows.Forms.TextBox txtProcessJogSpeed;
        private System.Windows.Forms.TextBox txtProcessHomeAcceleration;
        private System.Windows.Forms.TextBox txtProcessHome2Velocity;
        private System.Windows.Forms.TextBox txtProcessHome1Velocity;
        private System.Windows.Forms.TextBox txtProcessDefaultAcceleration;
        private System.Windows.Forms.TextBox txtProcessDefauleVelocity;
        private System.Windows.Forms.TextBox txtProcessInPosition;
        private System.Windows.Forms.TextBox txtProcessMoveTimeOut;
        private System.Windows.Forms.TextBox txtProcessSpeedRate;
        private System.Windows.Forms.TextBox txtProcessPositionRate;
        private System.Windows.Forms.Label lblProcessAccelationTime;
        private System.Windows.Forms.TextBox txtProcessAccelationTime;
        private System.Windows.Forms.Label lblProcessSWLimitHigh;
        private System.Windows.Forms.TextBox txtProcessSWLimitHigh;
        private System.Windows.Forms.Label lblProcessSWLimitLow;
        private System.Windows.Forms.Label lblProcessPtpSpeed;
        private System.Windows.Forms.Label lblProcessStepSpeed;
        private System.Windows.Forms.Label lblProcessJogSpeed;
        private System.Windows.Forms.Label lblProcessHomeAcceleration;
        private System.Windows.Forms.Label lbl_Process_Home2Velocity;
        private System.Windows.Forms.Label lblProcessHome1Velocity;
        private System.Windows.Forms.Label lblProcessDefaultAcceleration;
        private System.Windows.Forms.Label lblProcessDefauleVelocity;
        private System.Windows.Forms.Label lblProcessInPosition;
        private System.Windows.Forms.Label lblProcessMoveTimeOut;
        private System.Windows.Forms.Label lblProcessSpeedRate;
        private System.Windows.Forms.Label lblProcessPositionRate;
        private System.Windows.Forms.Label lblProcessAxisNo;
        private System.Windows.Forms.TextBox txtProcessAxisNo;
        private System.Windows.Forms.Label lblProcessAxisParameter;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btnMonitorLoader;
        private System.Windows.Forms.Button btnMonitorProcess;
        private System.Windows.Forms.Button btnMonitorUnloader;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button btnAjinMotor;
        private System.Windows.Forms.Button btnUmacMotor;
        private System.Windows.Forms.Button btnEZiMotor;
        private System.Windows.Forms.Panel pnlAjinMotorParamInfo;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flpSevoMotors;
        private System.Windows.Forms.Panel pnlUmacMotorParamInfo;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlEZiMotorParamInfo;
        private System.Windows.Forms.Label lblEziStopCurrent;
        private System.Windows.Forms.Label lblEziBoostCurrent;
        private System.Windows.Forms.Label lblEziBrakeDelayTime;
        private System.Windows.Forms.TextBox txtEziBrakeDelayTime;
        private System.Windows.Forms.Label lblEziPosErrorOverflowLimit;
        private System.Windows.Forms.TextBox txtEziPosErrorOverflowLimit;
        private System.Windows.Forms.Label lblEziOrgTorqueRatio;
        private System.Windows.Forms.TextBox txtEziOrgTorqueRatio;
        private System.Windows.Forms.Label lblEziLimitSensorDir;
        private System.Windows.Forms.TextBox txtEziLimitSensorDir;
        private System.Windows.Forms.Label lblEziMotionDir;
        private System.Windows.Forms.TextBox txtEziMotionDir;
        private System.Windows.Forms.Label lblEziPosTrackingLimit;
        private System.Windows.Forms.TextBox txtEziPosTrackingLimit;
        private System.Windows.Forms.Label lblEziInposValue;
        private System.Windows.Forms.TextBox txtEziInposValue;
        private System.Windows.Forms.Label lblEziPositionLoopGain;
        private System.Windows.Forms.Label lblEziOrgSensorLogic;
        private System.Windows.Forms.Label lblEziOrgPositionSet;
        private System.Windows.Forms.TextBox txtEziOrgPositionSet;
        private System.Windows.Forms.Label lblEziOrgOffset;
        private System.Windows.Forms.TextBox txtEziOrgOffset;
        private System.Windows.Forms.Label lblEziOrgDir;
        private System.Windows.Forms.Label lblEziOrgMethod;
        private System.Windows.Forms.TextBox txtEziOrgMethod;
        private System.Windows.Forms.Label lblEziOrgAccDecTime;
        private System.Windows.Forms.TextBox txtEziAccDecTime;
        private System.Windows.Forms.Label lblEziOrgSearchSpeed;
        private System.Windows.Forms.TextBox txtEziOrgSearchSpeed;
        private System.Windows.Forms.Label lblEziOrgSpeed;
        private System.Windows.Forms.TextBox txtEziOrgSpeed;
        private System.Windows.Forms.Label lblEziLimitSensorLogic;
        private System.Windows.Forms.Label lblEziHWLimitStopMethod;
        private System.Windows.Forms.Label lblEziSWLimitStopMethod;
        private System.Windows.Forms.Label lblEziSWLimitMinusValue;
        private System.Windows.Forms.Label lblEziSWLimitPlusValue;
        private System.Windows.Forms.Label lblEziJogAccDecTime;
        private System.Windows.Forms.Label lblEziJogStartSpeed;
        private System.Windows.Forms.Label lblEziJogSpeed;
        private System.Windows.Forms.Label lblEziAxisDecTime;
        private System.Windows.Forms.Label lblEziAxisAccSpeed;
        private System.Windows.Forms.Label lblEziAxisMaxSpeed;
        private System.Windows.Forms.Label lblEziPulsePerRevolution;
        private System.Windows.Forms.Label lblEziAxisNo;
        private System.Windows.Forms.TextBox txtEziAxisNo;
        private System.Windows.Forms.Label lblEziAxisParameter;
        internal System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtEziPulsePerRevolution;
        private System.Windows.Forms.TextBox txtEziAxisAccSpeed;
        private System.Windows.Forms.TextBox txtEziAxisMaxSpeed;
        private System.Windows.Forms.TextBox txtEziSpeedOverride;
        private System.Windows.Forms.TextBox txtEziAxisDecTime;
        private System.Windows.Forms.Label lblEziSpeedOverride;
        private System.Windows.Forms.TextBox txtEziJogSpeed;
        private System.Windows.Forms.TextBox txtEziJogStartSpeed;
        private System.Windows.Forms.TextBox txtEziJogAccDecTime;
        private System.Windows.Forms.TextBox txtEziSWLimitPlusValue;
        private System.Windows.Forms.TextBox txtEziSWLimitStopMethod;
        private System.Windows.Forms.TextBox txtSWLimitMinusValue;
        private System.Windows.Forms.TextBox txtEziLimitSensorLogic;
        private System.Windows.Forms.TextBox txtHWLimitStopMethod;
        private System.Windows.Forms.TextBox txEziOrgDir;
        private System.Windows.Forms.TextBox txtEziOrgSensorLogic;
        private System.Windows.Forms.TextBox txtEziPositionLoopGain;
        private System.Windows.Forms.TextBox txtEziAxisStartSpeed;
        private System.Windows.Forms.Label lbEziAxisStartSpeed;
        private System.Windows.Forms.TextBox txtEziStopCurrent;
        private System.Windows.Forms.TextBox txtEziBoostCurrent;
        private System.Windows.Forms.Label lblEziRunCurrent;
        private System.Windows.Forms.TextBox txtEziRunCurrent;
    }
}

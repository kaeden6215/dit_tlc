﻿namespace DIT.TLC.UI
{
    partial class UcrlParameterSequenceOffset
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLoadAX1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblangle4 = new System.Windows.Forms.Label();
            this.txtloadBT2 = new System.Windows.Forms.TextBox();
            this.lblLoadBT2 = new System.Windows.Forms.Label();
            this.lblangle3 = new System.Windows.Forms.Label();
            this.txtloadBT1 = new System.Windows.Forms.TextBox();
            this.lblLoadBT1 = new System.Windows.Forms.Label();
            this.lblmm4 = new System.Windows.Forms.Label();
            this.txtloadBX2 = new System.Windows.Forms.TextBox();
            this.lblLoadBX2 = new System.Windows.Forms.Label();
            this.lblmm3 = new System.Windows.Forms.Label();
            this.txtloadBX1 = new System.Windows.Forms.TextBox();
            this.lblLoadBX1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblangle2 = new System.Windows.Forms.Label();
            this.txtloadAT2 = new System.Windows.Forms.TextBox();
            this.lblLoadAT2 = new System.Windows.Forms.Label();
            this.lblangle1 = new System.Windows.Forms.Label();
            this.txtloadAT1 = new System.Windows.Forms.TextBox();
            this.lblLoadAT1 = new System.Windows.Forms.Label();
            this.lblmm2 = new System.Windows.Forms.Label();
            this.txtloadAX2 = new System.Windows.Forms.TextBox();
            this.lblLoadAX2 = new System.Windows.Forms.Label();
            this.lblmm1 = new System.Windows.Forms.Label();
            this.txtloadAX1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblmm8 = new System.Windows.Forms.Label();
            this.txtAfterIRBX2 = new System.Windows.Forms.TextBox();
            this.lblAfterIRBX2 = new System.Windows.Forms.Label();
            this.lblmm7 = new System.Windows.Forms.Label();
            this.txtAfterIRBX1 = new System.Windows.Forms.TextBox();
            this.lblAfterIRBX1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblmm6 = new System.Windows.Forms.Label();
            this.txtAfterIRAX2 = new System.Windows.Forms.TextBox();
            this.lblAfterIRAX2 = new System.Windows.Forms.Label();
            this.lblmm5 = new System.Windows.Forms.Label();
            this.txtAfterIRAX1 = new System.Windows.Forms.TextBox();
            this.lblAfterIRAX1 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblmm12 = new System.Windows.Forms.Label();
            this.txtUldRotate180BX2 = new System.Windows.Forms.TextBox();
            this.lblUldRotate180BX2 = new System.Windows.Forms.Label();
            this.lblmm11 = new System.Windows.Forms.Label();
            this.txtUldRotate180BX1 = new System.Windows.Forms.TextBox();
            this.lblUldRotate180BX1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblmm10 = new System.Windows.Forms.Label();
            this.txtUldRotate180AX2 = new System.Windows.Forms.TextBox();
            this.lblUldRotate180AX2 = new System.Windows.Forms.Label();
            this.lblmm9 = new System.Windows.Forms.Label();
            this.txtUldRotate180AX1 = new System.Windows.Forms.TextBox();
            this.lblUldRotate180AX1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblangle8 = new System.Windows.Forms.Label();
            this.txtUnloadBT2 = new System.Windows.Forms.TextBox();
            this.lblUnloadBT2 = new System.Windows.Forms.Label();
            this.lblangle7 = new System.Windows.Forms.Label();
            this.txtUnloadBT1 = new System.Windows.Forms.TextBox();
            this.lblUnloadBT1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblangle6 = new System.Windows.Forms.Label();
            this.txtUnloadAT2 = new System.Windows.Forms.TextBox();
            this.lblUnloadAT2 = new System.Windows.Forms.Label();
            this.lblangle5 = new System.Windows.Forms.Label();
            this.txtUnloadAT1 = new System.Windows.Forms.TextBox();
            this.lblUnloadAT1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lblmm20 = new System.Windows.Forms.Label();
            this.txtInspection4Y = new System.Windows.Forms.TextBox();
            this.lblInspection4Y = new System.Windows.Forms.Label();
            this.lblmm19 = new System.Windows.Forms.Label();
            this.txtInspection4X = new System.Windows.Forms.TextBox();
            this.lblInspection4X = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.lblmm18 = new System.Windows.Forms.Label();
            this.txtInspection3Y = new System.Windows.Forms.TextBox();
            this.lblInspection3Y = new System.Windows.Forms.Label();
            this.lblmm17 = new System.Windows.Forms.Label();
            this.txtInspection3X = new System.Windows.Forms.TextBox();
            this.lblInspection3X = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.lblmm16 = new System.Windows.Forms.Label();
            this.txtInspection2Y = new System.Windows.Forms.TextBox();
            this.lblInspection2Y = new System.Windows.Forms.Label();
            this.lblmm15 = new System.Windows.Forms.Label();
            this.txtInspection2X = new System.Windows.Forms.TextBox();
            this.lblInspection2X = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.lblmm14 = new System.Windows.Forms.Label();
            this.txtInspection1Y = new System.Windows.Forms.TextBox();
            this.lblInspection1Y = new System.Windows.Forms.Label();
            this.lblmm13 = new System.Windows.Forms.Label();
            this.txtInspection1X = new System.Windows.Forms.TextBox();
            this.lblInspection1X = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLoadAX1
            // 
            this.lblLoadAX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadAX1.ForeColor = System.Drawing.Color.Black;
            this.lblLoadAX1.Location = new System.Drawing.Point(11, 37);
            this.lblLoadAX1.Name = "lblLoadAX1";
            this.lblLoadAX1.Size = new System.Drawing.Size(50, 21);
            this.lblLoadAX1.TabIndex = 58;
            this.lblLoadAX1.Text = "X1 : ";
            this.lblLoadAX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(853, 155);
            this.panel1.TabIndex = 455;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblangle4);
            this.panel3.Controls.Add(this.txtloadBT2);
            this.panel3.Controls.Add(this.lblLoadBT2);
            this.panel3.Controls.Add(this.lblangle3);
            this.panel3.Controls.Add(this.txtloadBT1);
            this.panel3.Controls.Add(this.lblLoadBT1);
            this.panel3.Controls.Add(this.lblmm4);
            this.panel3.Controls.Add(this.txtloadBX2);
            this.panel3.Controls.Add(this.lblLoadBX2);
            this.panel3.Controls.Add(this.lblmm3);
            this.panel3.Controls.Add(this.txtloadBX1);
            this.panel3.Controls.Add(this.lblLoadBX1);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(432, 31);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(414, 115);
            this.panel3.TabIndex = 457;
            // 
            // lblangle4
            // 
            this.lblangle4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle4.ForeColor = System.Drawing.Color.Black;
            this.lblangle4.Location = new System.Drawing.Point(353, 79);
            this.lblangle4.Name = "lblangle4";
            this.lblangle4.Size = new System.Drawing.Size(47, 20);
            this.lblangle4.TabIndex = 71;
            this.lblangle4.Text = "º";
            this.lblangle4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadBT2
            // 
            this.txtloadBT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBT2.Location = new System.Drawing.Point(276, 75);
            this.txtloadBT2.Name = "txtloadBT2";
            this.txtloadBT2.Size = new System.Drawing.Size(70, 27);
            this.txtloadBT2.TabIndex = 70;
            // 
            // lblLoadBT2
            // 
            this.lblLoadBT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadBT2.ForeColor = System.Drawing.Color.Black;
            this.lblLoadBT2.Location = new System.Drawing.Point(216, 78);
            this.lblLoadBT2.Name = "lblLoadBT2";
            this.lblLoadBT2.Size = new System.Drawing.Size(50, 21);
            this.lblLoadBT2.TabIndex = 69;
            this.lblLoadBT2.Text = "T2 : ";
            this.lblLoadBT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblangle3
            // 
            this.lblangle3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle3.ForeColor = System.Drawing.Color.Black;
            this.lblangle3.Location = new System.Drawing.Point(353, 38);
            this.lblangle3.Name = "lblangle3";
            this.lblangle3.Size = new System.Drawing.Size(47, 20);
            this.lblangle3.TabIndex = 68;
            this.lblangle3.Text = "º";
            this.lblangle3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadBT1
            // 
            this.txtloadBT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBT1.Location = new System.Drawing.Point(276, 34);
            this.txtloadBT1.Name = "txtloadBT1";
            this.txtloadBT1.Size = new System.Drawing.Size(70, 27);
            this.txtloadBT1.TabIndex = 67;
            // 
            // lblLoadBT1
            // 
            this.lblLoadBT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadBT1.ForeColor = System.Drawing.Color.Black;
            this.lblLoadBT1.Location = new System.Drawing.Point(216, 37);
            this.lblLoadBT1.Name = "lblLoadBT1";
            this.lblLoadBT1.Size = new System.Drawing.Size(50, 21);
            this.lblLoadBT1.TabIndex = 66;
            this.lblLoadBT1.Text = "T1 : ";
            this.lblLoadBT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm4
            // 
            this.lblmm4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm4.ForeColor = System.Drawing.Color.Black;
            this.lblmm4.Location = new System.Drawing.Point(148, 79);
            this.lblmm4.Name = "lblmm4";
            this.lblmm4.Size = new System.Drawing.Size(47, 20);
            this.lblmm4.TabIndex = 65;
            this.lblmm4.Text = "(mm)";
            this.lblmm4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadBX2
            // 
            this.txtloadBX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBX2.Location = new System.Drawing.Point(71, 75);
            this.txtloadBX2.Name = "txtloadBX2";
            this.txtloadBX2.Size = new System.Drawing.Size(70, 27);
            this.txtloadBX2.TabIndex = 64;
            // 
            // lblLoadBX2
            // 
            this.lblLoadBX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadBX2.ForeColor = System.Drawing.Color.Black;
            this.lblLoadBX2.Location = new System.Drawing.Point(11, 78);
            this.lblLoadBX2.Name = "lblLoadBX2";
            this.lblLoadBX2.Size = new System.Drawing.Size(50, 21);
            this.lblLoadBX2.TabIndex = 63;
            this.lblLoadBX2.Text = "X2 : ";
            this.lblLoadBX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm3
            // 
            this.lblmm3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm3.ForeColor = System.Drawing.Color.Black;
            this.lblmm3.Location = new System.Drawing.Point(148, 38);
            this.lblmm3.Name = "lblmm3";
            this.lblmm3.Size = new System.Drawing.Size(47, 20);
            this.lblmm3.TabIndex = 62;
            this.lblmm3.Text = "(mm)";
            this.lblmm3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadBX1
            // 
            this.txtloadBX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBX1.Location = new System.Drawing.Point(71, 34);
            this.txtloadBX1.Name = "txtloadBX1";
            this.txtloadBX1.Size = new System.Drawing.Size(70, 27);
            this.txtloadBX1.TabIndex = 61;
            // 
            // lblLoadBX1
            // 
            this.lblLoadBX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadBX1.ForeColor = System.Drawing.Color.Black;
            this.lblLoadBX1.Location = new System.Drawing.Point(11, 37);
            this.lblLoadBX1.Name = "lblLoadBX1";
            this.lblLoadBX1.Size = new System.Drawing.Size(50, 21);
            this.lblLoadBX1.TabIndex = 58;
            this.lblLoadBX1.Text = "X1 : ";
            this.lblLoadBX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoEllipsis = true;
            this.label11.BackColor = System.Drawing.Color.Gainsboro;
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(412, 24);
            this.label11.TabIndex = 9;
            this.label11.Text = "■ B";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblangle2);
            this.panel2.Controls.Add(this.txtloadAT2);
            this.panel2.Controls.Add(this.lblLoadAT2);
            this.panel2.Controls.Add(this.lblangle1);
            this.panel2.Controls.Add(this.txtloadAT1);
            this.panel2.Controls.Add(this.lblLoadAT1);
            this.panel2.Controls.Add(this.lblmm2);
            this.panel2.Controls.Add(this.txtloadAX2);
            this.panel2.Controls.Add(this.lblLoadAX2);
            this.panel2.Controls.Add(this.lblmm1);
            this.panel2.Controls.Add(this.txtloadAX1);
            this.panel2.Controls.Add(this.lblLoadAX1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(6, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(414, 115);
            this.panel2.TabIndex = 456;
            // 
            // lblangle2
            // 
            this.lblangle2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle2.ForeColor = System.Drawing.Color.Black;
            this.lblangle2.Location = new System.Drawing.Point(353, 79);
            this.lblangle2.Name = "lblangle2";
            this.lblangle2.Size = new System.Drawing.Size(47, 20);
            this.lblangle2.TabIndex = 71;
            this.lblangle2.Text = "º";
            this.lblangle2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadAT2
            // 
            this.txtloadAT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAT2.Location = new System.Drawing.Point(276, 75);
            this.txtloadAT2.Name = "txtloadAT2";
            this.txtloadAT2.Size = new System.Drawing.Size(70, 27);
            this.txtloadAT2.TabIndex = 70;
            // 
            // lblLoadAT2
            // 
            this.lblLoadAT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadAT2.ForeColor = System.Drawing.Color.Black;
            this.lblLoadAT2.Location = new System.Drawing.Point(216, 78);
            this.lblLoadAT2.Name = "lblLoadAT2";
            this.lblLoadAT2.Size = new System.Drawing.Size(50, 21);
            this.lblLoadAT2.TabIndex = 69;
            this.lblLoadAT2.Text = "T2 : ";
            this.lblLoadAT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblangle1
            // 
            this.lblangle1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle1.ForeColor = System.Drawing.Color.Black;
            this.lblangle1.Location = new System.Drawing.Point(353, 38);
            this.lblangle1.Name = "lblangle1";
            this.lblangle1.Size = new System.Drawing.Size(47, 20);
            this.lblangle1.TabIndex = 68;
            this.lblangle1.Text = "º";
            this.lblangle1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadAT1
            // 
            this.txtloadAT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAT1.Location = new System.Drawing.Point(276, 34);
            this.txtloadAT1.Name = "txtloadAT1";
            this.txtloadAT1.Size = new System.Drawing.Size(70, 27);
            this.txtloadAT1.TabIndex = 67;
            // 
            // lblLoadAT1
            // 
            this.lblLoadAT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadAT1.ForeColor = System.Drawing.Color.Black;
            this.lblLoadAT1.Location = new System.Drawing.Point(216, 37);
            this.lblLoadAT1.Name = "lblLoadAT1";
            this.lblLoadAT1.Size = new System.Drawing.Size(50, 21);
            this.lblLoadAT1.TabIndex = 66;
            this.lblLoadAT1.Text = "T1 : ";
            this.lblLoadAT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm2
            // 
            this.lblmm2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm2.ForeColor = System.Drawing.Color.Black;
            this.lblmm2.Location = new System.Drawing.Point(148, 79);
            this.lblmm2.Name = "lblmm2";
            this.lblmm2.Size = new System.Drawing.Size(47, 20);
            this.lblmm2.TabIndex = 65;
            this.lblmm2.Text = "(mm)";
            this.lblmm2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadAX2
            // 
            this.txtloadAX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAX2.Location = new System.Drawing.Point(71, 75);
            this.txtloadAX2.Name = "txtloadAX2";
            this.txtloadAX2.Size = new System.Drawing.Size(70, 27);
            this.txtloadAX2.TabIndex = 64;
            // 
            // lblLoadAX2
            // 
            this.lblLoadAX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoadAX2.ForeColor = System.Drawing.Color.Black;
            this.lblLoadAX2.Location = new System.Drawing.Point(11, 78);
            this.lblLoadAX2.Name = "lblLoadAX2";
            this.lblLoadAX2.Size = new System.Drawing.Size(50, 21);
            this.lblLoadAX2.TabIndex = 63;
            this.lblLoadAX2.Text = "X2 : ";
            this.lblLoadAX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm1
            // 
            this.lblmm1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm1.ForeColor = System.Drawing.Color.Black;
            this.lblmm1.Location = new System.Drawing.Point(148, 38);
            this.lblmm1.Name = "lblmm1";
            this.lblmm1.Size = new System.Drawing.Size(47, 20);
            this.lblmm1.TabIndex = 62;
            this.lblmm1.Text = "(mm)";
            this.lblmm1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtloadAX1
            // 
            this.txtloadAX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAX1.Location = new System.Drawing.Point(71, 34);
            this.txtloadAX1.Name = "txtloadAX1";
            this.txtloadAX1.Size = new System.Drawing.Size(70, 27);
            this.txtloadAX1.TabIndex = 61;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(412, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ A";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(851, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ 로드 이재기";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Location = new System.Drawing.Point(862, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(434, 155);
            this.panel4.TabIndex = 458;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.lblmm8);
            this.panel5.Controls.Add(this.txtAfterIRBX2);
            this.panel5.Controls.Add(this.lblAfterIRBX2);
            this.panel5.Controls.Add(this.lblmm7);
            this.panel5.Controls.Add(this.txtAfterIRBX1);
            this.panel5.Controls.Add(this.lblAfterIRBX1);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(222, 31);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(205, 115);
            this.panel5.TabIndex = 457;
            // 
            // lblmm8
            // 
            this.lblmm8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm8.ForeColor = System.Drawing.Color.Black;
            this.lblmm8.Location = new System.Drawing.Point(144, 79);
            this.lblmm8.Name = "lblmm8";
            this.lblmm8.Size = new System.Drawing.Size(47, 20);
            this.lblmm8.TabIndex = 65;
            this.lblmm8.Text = "(mm)";
            this.lblmm8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAfterIRBX2
            // 
            this.txtAfterIRBX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRBX2.Location = new System.Drawing.Point(67, 75);
            this.txtAfterIRBX2.Name = "txtAfterIRBX2";
            this.txtAfterIRBX2.Size = new System.Drawing.Size(70, 27);
            this.txtAfterIRBX2.TabIndex = 64;
            // 
            // lblAfterIRBX2
            // 
            this.lblAfterIRBX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAfterIRBX2.ForeColor = System.Drawing.Color.Black;
            this.lblAfterIRBX2.Location = new System.Drawing.Point(7, 78);
            this.lblAfterIRBX2.Name = "lblAfterIRBX2";
            this.lblAfterIRBX2.Size = new System.Drawing.Size(50, 21);
            this.lblAfterIRBX2.TabIndex = 63;
            this.lblAfterIRBX2.Text = "X2 : ";
            this.lblAfterIRBX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm7
            // 
            this.lblmm7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm7.ForeColor = System.Drawing.Color.Black;
            this.lblmm7.Location = new System.Drawing.Point(144, 38);
            this.lblmm7.Name = "lblmm7";
            this.lblmm7.Size = new System.Drawing.Size(47, 20);
            this.lblmm7.TabIndex = 62;
            this.lblmm7.Text = "(mm)";
            this.lblmm7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAfterIRBX1
            // 
            this.txtAfterIRBX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRBX1.Location = new System.Drawing.Point(67, 34);
            this.txtAfterIRBX1.Name = "txtAfterIRBX1";
            this.txtAfterIRBX1.Size = new System.Drawing.Size(70, 27);
            this.txtAfterIRBX1.TabIndex = 61;
            // 
            // lblAfterIRBX1
            // 
            this.lblAfterIRBX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAfterIRBX1.ForeColor = System.Drawing.Color.Black;
            this.lblAfterIRBX1.Location = new System.Drawing.Point(7, 37);
            this.lblAfterIRBX1.Name = "lblAfterIRBX1";
            this.lblAfterIRBX1.Size = new System.Drawing.Size(50, 21);
            this.lblAfterIRBX1.TabIndex = 58;
            this.lblAfterIRBX1.Text = "X1 : ";
            this.lblAfterIRBX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoEllipsis = true;
            this.label12.BackColor = System.Drawing.Color.Gainsboro;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(203, 24);
            this.label12.TabIndex = 9;
            this.label12.Text = "■ B";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lblmm6);
            this.panel6.Controls.Add(this.txtAfterIRAX2);
            this.panel6.Controls.Add(this.lblAfterIRAX2);
            this.panel6.Controls.Add(this.lblmm5);
            this.panel6.Controls.Add(this.txtAfterIRAX1);
            this.panel6.Controls.Add(this.lblAfterIRAX1);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Location = new System.Drawing.Point(6, 31);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(205, 115);
            this.panel6.TabIndex = 456;
            // 
            // lblmm6
            // 
            this.lblmm6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm6.ForeColor = System.Drawing.Color.Black;
            this.lblmm6.Location = new System.Drawing.Point(144, 79);
            this.lblmm6.Name = "lblmm6";
            this.lblmm6.Size = new System.Drawing.Size(47, 20);
            this.lblmm6.TabIndex = 65;
            this.lblmm6.Text = "(mm)";
            this.lblmm6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAfterIRAX2
            // 
            this.txtAfterIRAX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRAX2.Location = new System.Drawing.Point(67, 75);
            this.txtAfterIRAX2.Name = "txtAfterIRAX2";
            this.txtAfterIRAX2.Size = new System.Drawing.Size(70, 27);
            this.txtAfterIRAX2.TabIndex = 64;
            // 
            // lblAfterIRAX2
            // 
            this.lblAfterIRAX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAfterIRAX2.ForeColor = System.Drawing.Color.Black;
            this.lblAfterIRAX2.Location = new System.Drawing.Point(7, 78);
            this.lblAfterIRAX2.Name = "lblAfterIRAX2";
            this.lblAfterIRAX2.Size = new System.Drawing.Size(50, 21);
            this.lblAfterIRAX2.TabIndex = 63;
            this.lblAfterIRAX2.Text = "X2 : ";
            this.lblAfterIRAX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm5
            // 
            this.lblmm5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm5.ForeColor = System.Drawing.Color.Black;
            this.lblmm5.Location = new System.Drawing.Point(144, 38);
            this.lblmm5.Name = "lblmm5";
            this.lblmm5.Size = new System.Drawing.Size(47, 20);
            this.lblmm5.TabIndex = 62;
            this.lblmm5.Text = "(mm)";
            this.lblmm5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAfterIRAX1
            // 
            this.txtAfterIRAX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRAX1.Location = new System.Drawing.Point(67, 34);
            this.txtAfterIRAX1.Name = "txtAfterIRAX1";
            this.txtAfterIRAX1.Size = new System.Drawing.Size(70, 27);
            this.txtAfterIRAX1.TabIndex = 61;
            // 
            // lblAfterIRAX1
            // 
            this.lblAfterIRAX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAfterIRAX1.ForeColor = System.Drawing.Color.Black;
            this.lblAfterIRAX1.Location = new System.Drawing.Point(7, 37);
            this.lblAfterIRAX1.Name = "lblAfterIRAX1";
            this.lblAfterIRAX1.Size = new System.Drawing.Size(50, 21);
            this.lblAfterIRAX1.TabIndex = 58;
            this.lblAfterIRAX1.Text = "X1 : ";
            this.lblAfterIRAX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoEllipsis = true;
            this.label21.BackColor = System.Drawing.Color.Gainsboro;
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(203, 24);
            this.label21.TabIndex = 9;
            this.label21.Text = "■ A";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoEllipsis = true;
            this.label22.BackColor = System.Drawing.Color.Gainsboro;
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(432, 24);
            this.label22.TabIndex = 9;
            this.label22.Text = "■ After IR 이재기";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Location = new System.Drawing.Point(1301, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(434, 155);
            this.panel7.TabIndex = 459;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.lblmm12);
            this.panel8.Controls.Add(this.txtUldRotate180BX2);
            this.panel8.Controls.Add(this.lblUldRotate180BX2);
            this.panel8.Controls.Add(this.lblmm11);
            this.panel8.Controls.Add(this.txtUldRotate180BX1);
            this.panel8.Controls.Add(this.lblUldRotate180BX1);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Location = new System.Drawing.Point(222, 31);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(205, 115);
            this.panel8.TabIndex = 457;
            // 
            // lblmm12
            // 
            this.lblmm12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm12.ForeColor = System.Drawing.Color.Black;
            this.lblmm12.Location = new System.Drawing.Point(144, 79);
            this.lblmm12.Name = "lblmm12";
            this.lblmm12.Size = new System.Drawing.Size(47, 20);
            this.lblmm12.TabIndex = 65;
            this.lblmm12.Text = "(mm)";
            this.lblmm12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUldRotate180BX2
            // 
            this.txtUldRotate180BX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUldRotate180BX2.Location = new System.Drawing.Point(67, 75);
            this.txtUldRotate180BX2.Name = "txtUldRotate180BX2";
            this.txtUldRotate180BX2.Size = new System.Drawing.Size(70, 27);
            this.txtUldRotate180BX2.TabIndex = 64;
            // 
            // lblUldRotate180BX2
            // 
            this.lblUldRotate180BX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUldRotate180BX2.ForeColor = System.Drawing.Color.Black;
            this.lblUldRotate180BX2.Location = new System.Drawing.Point(7, 78);
            this.lblUldRotate180BX2.Name = "lblUldRotate180BX2";
            this.lblUldRotate180BX2.Size = new System.Drawing.Size(50, 21);
            this.lblUldRotate180BX2.TabIndex = 63;
            this.lblUldRotate180BX2.Text = "X2 : ";
            this.lblUldRotate180BX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm11
            // 
            this.lblmm11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm11.ForeColor = System.Drawing.Color.Black;
            this.lblmm11.Location = new System.Drawing.Point(144, 38);
            this.lblmm11.Name = "lblmm11";
            this.lblmm11.Size = new System.Drawing.Size(47, 20);
            this.lblmm11.TabIndex = 62;
            this.lblmm11.Text = "(mm)";
            this.lblmm11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUldRotate180BX1
            // 
            this.txtUldRotate180BX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUldRotate180BX1.Location = new System.Drawing.Point(67, 34);
            this.txtUldRotate180BX1.Name = "txtUldRotate180BX1";
            this.txtUldRotate180BX1.Size = new System.Drawing.Size(70, 27);
            this.txtUldRotate180BX1.TabIndex = 61;
            // 
            // lblUldRotate180BX1
            // 
            this.lblUldRotate180BX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUldRotate180BX1.ForeColor = System.Drawing.Color.Black;
            this.lblUldRotate180BX1.Location = new System.Drawing.Point(7, 37);
            this.lblUldRotate180BX1.Name = "lblUldRotate180BX1";
            this.lblUldRotate180BX1.Size = new System.Drawing.Size(50, 21);
            this.lblUldRotate180BX1.TabIndex = 58;
            this.lblUldRotate180BX1.Text = "X1 : ";
            this.lblUldRotate180BX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(203, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "■ B";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.lblmm10);
            this.panel9.Controls.Add(this.txtUldRotate180AX2);
            this.panel9.Controls.Add(this.lblUldRotate180AX2);
            this.panel9.Controls.Add(this.lblmm9);
            this.panel9.Controls.Add(this.txtUldRotate180AX1);
            this.panel9.Controls.Add(this.lblUldRotate180AX1);
            this.panel9.Controls.Add(this.label14);
            this.panel9.Location = new System.Drawing.Point(6, 31);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(205, 115);
            this.panel9.TabIndex = 456;
            // 
            // lblmm10
            // 
            this.lblmm10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm10.ForeColor = System.Drawing.Color.Black;
            this.lblmm10.Location = new System.Drawing.Point(144, 79);
            this.lblmm10.Name = "lblmm10";
            this.lblmm10.Size = new System.Drawing.Size(47, 20);
            this.lblmm10.TabIndex = 65;
            this.lblmm10.Text = "(mm)";
            this.lblmm10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUldRotate180AX2
            // 
            this.txtUldRotate180AX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUldRotate180AX2.Location = new System.Drawing.Point(67, 75);
            this.txtUldRotate180AX2.Name = "txtUldRotate180AX2";
            this.txtUldRotate180AX2.Size = new System.Drawing.Size(70, 27);
            this.txtUldRotate180AX2.TabIndex = 64;
            // 
            // lblUldRotate180AX2
            // 
            this.lblUldRotate180AX2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUldRotate180AX2.ForeColor = System.Drawing.Color.Black;
            this.lblUldRotate180AX2.Location = new System.Drawing.Point(7, 78);
            this.lblUldRotate180AX2.Name = "lblUldRotate180AX2";
            this.lblUldRotate180AX2.Size = new System.Drawing.Size(50, 21);
            this.lblUldRotate180AX2.TabIndex = 63;
            this.lblUldRotate180AX2.Text = "X2 : ";
            this.lblUldRotate180AX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm9
            // 
            this.lblmm9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm9.ForeColor = System.Drawing.Color.Black;
            this.lblmm9.Location = new System.Drawing.Point(144, 38);
            this.lblmm9.Name = "lblmm9";
            this.lblmm9.Size = new System.Drawing.Size(47, 20);
            this.lblmm9.TabIndex = 62;
            this.lblmm9.Text = "(mm)";
            this.lblmm9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUldRotate180AX1
            // 
            this.txtUldRotate180AX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUldRotate180AX1.Location = new System.Drawing.Point(67, 34);
            this.txtUldRotate180AX1.Name = "txtUldRotate180AX1";
            this.txtUldRotate180AX1.Size = new System.Drawing.Size(70, 27);
            this.txtUldRotate180AX1.TabIndex = 61;
            // 
            // lblUldRotate180AX1
            // 
            this.lblUldRotate180AX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUldRotate180AX1.ForeColor = System.Drawing.Color.Black;
            this.lblUldRotate180AX1.Location = new System.Drawing.Point(7, 37);
            this.lblUldRotate180AX1.Name = "lblUldRotate180AX1";
            this.lblUldRotate180AX1.Size = new System.Drawing.Size(50, 21);
            this.lblUldRotate180AX1.TabIndex = 58;
            this.lblUldRotate180AX1.Text = "X1 : ";
            this.lblUldRotate180AX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoEllipsis = true;
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(203, 24);
            this.label14.TabIndex = 9;
            this.label14.Text = "■ A";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoEllipsis = true;
            this.label15.BackColor = System.Drawing.Color.Gainsboro;
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(432, 24);
            this.label15.TabIndex = 9;
            this.label15.Text = "■ ULD Rotate 180";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Controls.Add(this.panel12);
            this.panel10.Controls.Add(this.label18);
            this.panel10.Location = new System.Drawing.Point(3, 164);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(434, 155);
            this.panel10.TabIndex = 460;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.lblangle8);
            this.panel11.Controls.Add(this.txtUnloadBT2);
            this.panel11.Controls.Add(this.lblUnloadBT2);
            this.panel11.Controls.Add(this.lblangle7);
            this.panel11.Controls.Add(this.txtUnloadBT1);
            this.panel11.Controls.Add(this.lblUnloadBT1);
            this.panel11.Controls.Add(this.label8);
            this.panel11.Location = new System.Drawing.Point(222, 31);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(205, 115);
            this.panel11.TabIndex = 457;
            // 
            // lblangle8
            // 
            this.lblangle8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle8.ForeColor = System.Drawing.Color.Black;
            this.lblangle8.Location = new System.Drawing.Point(144, 79);
            this.lblangle8.Name = "lblangle8";
            this.lblangle8.Size = new System.Drawing.Size(47, 20);
            this.lblangle8.TabIndex = 65;
            this.lblangle8.Text = "º";
            this.lblangle8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUnloadBT2
            // 
            this.txtUnloadBT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadBT2.Location = new System.Drawing.Point(67, 75);
            this.txtUnloadBT2.Name = "txtUnloadBT2";
            this.txtUnloadBT2.Size = new System.Drawing.Size(70, 27);
            this.txtUnloadBT2.TabIndex = 64;
            // 
            // lblUnloadBT2
            // 
            this.lblUnloadBT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloadBT2.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadBT2.Location = new System.Drawing.Point(7, 78);
            this.lblUnloadBT2.Name = "lblUnloadBT2";
            this.lblUnloadBT2.Size = new System.Drawing.Size(50, 21);
            this.lblUnloadBT2.TabIndex = 63;
            this.lblUnloadBT2.Text = "T2 : ";
            this.lblUnloadBT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblangle7
            // 
            this.lblangle7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle7.ForeColor = System.Drawing.Color.Black;
            this.lblangle7.Location = new System.Drawing.Point(144, 38);
            this.lblangle7.Name = "lblangle7";
            this.lblangle7.Size = new System.Drawing.Size(47, 20);
            this.lblangle7.TabIndex = 62;
            this.lblangle7.Text = "º";
            this.lblangle7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUnloadBT1
            // 
            this.txtUnloadBT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadBT1.Location = new System.Drawing.Point(67, 34);
            this.txtUnloadBT1.Name = "txtUnloadBT1";
            this.txtUnloadBT1.Size = new System.Drawing.Size(70, 27);
            this.txtUnloadBT1.TabIndex = 61;
            // 
            // lblUnloadBT1
            // 
            this.lblUnloadBT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloadBT1.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadBT1.Location = new System.Drawing.Point(7, 37);
            this.lblUnloadBT1.Name = "lblUnloadBT1";
            this.lblUnloadBT1.Size = new System.Drawing.Size(50, 21);
            this.lblUnloadBT1.TabIndex = 58;
            this.lblUnloadBT1.Text = "T1 : ";
            this.lblUnloadBT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(203, 24);
            this.label8.TabIndex = 9;
            this.label8.Text = "■ B";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.lblangle6);
            this.panel12.Controls.Add(this.txtUnloadAT2);
            this.panel12.Controls.Add(this.lblUnloadAT2);
            this.panel12.Controls.Add(this.lblangle5);
            this.panel12.Controls.Add(this.txtUnloadAT1);
            this.panel12.Controls.Add(this.lblUnloadAT1);
            this.panel12.Controls.Add(this.label17);
            this.panel12.Location = new System.Drawing.Point(6, 31);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(205, 115);
            this.panel12.TabIndex = 456;
            // 
            // lblangle6
            // 
            this.lblangle6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle6.ForeColor = System.Drawing.Color.Black;
            this.lblangle6.Location = new System.Drawing.Point(144, 79);
            this.lblangle6.Name = "lblangle6";
            this.lblangle6.Size = new System.Drawing.Size(47, 20);
            this.lblangle6.TabIndex = 65;
            this.lblangle6.Text = "º";
            this.lblangle6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUnloadAT2
            // 
            this.txtUnloadAT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadAT2.Location = new System.Drawing.Point(67, 75);
            this.txtUnloadAT2.Name = "txtUnloadAT2";
            this.txtUnloadAT2.Size = new System.Drawing.Size(70, 27);
            this.txtUnloadAT2.TabIndex = 64;
            // 
            // lblUnloadAT2
            // 
            this.lblUnloadAT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloadAT2.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadAT2.Location = new System.Drawing.Point(7, 78);
            this.lblUnloadAT2.Name = "lblUnloadAT2";
            this.lblUnloadAT2.Size = new System.Drawing.Size(50, 21);
            this.lblUnloadAT2.TabIndex = 63;
            this.lblUnloadAT2.Text = "T2 : ";
            this.lblUnloadAT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblangle5
            // 
            this.lblangle5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblangle5.ForeColor = System.Drawing.Color.Black;
            this.lblangle5.Location = new System.Drawing.Point(144, 38);
            this.lblangle5.Name = "lblangle5";
            this.lblangle5.Size = new System.Drawing.Size(47, 20);
            this.lblangle5.TabIndex = 62;
            this.lblangle5.Text = "º";
            this.lblangle5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUnloadAT1
            // 
            this.txtUnloadAT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadAT1.Location = new System.Drawing.Point(67, 34);
            this.txtUnloadAT1.Name = "txtUnloadAT1";
            this.txtUnloadAT1.Size = new System.Drawing.Size(70, 27);
            this.txtUnloadAT1.TabIndex = 61;
            // 
            // lblUnloadAT1
            // 
            this.lblUnloadAT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUnloadAT1.ForeColor = System.Drawing.Color.Black;
            this.lblUnloadAT1.Location = new System.Drawing.Point(7, 37);
            this.lblUnloadAT1.Name = "lblUnloadAT1";
            this.lblUnloadAT1.Size = new System.Drawing.Size(50, 21);
            this.lblUnloadAT1.TabIndex = 58;
            this.lblUnloadAT1.Text = "T1 : ";
            this.lblUnloadAT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoEllipsis = true;
            this.label17.BackColor = System.Drawing.Color.Gainsboro;
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(203, 24);
            this.label17.TabIndex = 9;
            this.label17.Text = "■ A";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoEllipsis = true;
            this.label18.BackColor = System.Drawing.Color.Gainsboro;
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(432, 24);
            this.label18.TabIndex = 9;
            this.label18.Text = "■ 언로드 이재기";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoEllipsis = true;
            this.label31.BackColor = System.Drawing.Color.Gainsboro;
            this.label31.Dock = System.Windows.Forms.DockStyle.Top;
            this.label31.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(0, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(851, 24);
            this.label31.TabIndex = 9;
            this.label31.Text = "■ 인스펙션 Offset";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.panel16);
            this.panel13.Controls.Add(this.panel17);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Controls.Add(this.label31);
            this.panel13.Location = new System.Drawing.Point(443, 164);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(853, 155);
            this.panel13.TabIndex = 461;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.lblmm20);
            this.panel16.Controls.Add(this.txtInspection4Y);
            this.panel16.Controls.Add(this.lblInspection4Y);
            this.panel16.Controls.Add(this.lblmm19);
            this.panel16.Controls.Add(this.txtInspection4X);
            this.panel16.Controls.Add(this.lblInspection4X);
            this.panel16.Controls.Add(this.label27);
            this.panel16.Location = new System.Drawing.Point(638, 31);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(204, 115);
            this.panel16.TabIndex = 461;
            // 
            // lblmm20
            // 
            this.lblmm20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm20.ForeColor = System.Drawing.Color.Black;
            this.lblmm20.Location = new System.Drawing.Point(144, 79);
            this.lblmm20.Name = "lblmm20";
            this.lblmm20.Size = new System.Drawing.Size(47, 20);
            this.lblmm20.TabIndex = 65;
            this.lblmm20.Text = "(mm)";
            this.lblmm20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection4Y
            // 
            this.txtInspection4Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection4Y.Location = new System.Drawing.Point(67, 75);
            this.txtInspection4Y.Name = "txtInspection4Y";
            this.txtInspection4Y.Size = new System.Drawing.Size(68, 27);
            this.txtInspection4Y.TabIndex = 64;
            // 
            // lblInspection4Y
            // 
            this.lblInspection4Y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection4Y.ForeColor = System.Drawing.Color.Black;
            this.lblInspection4Y.Location = new System.Drawing.Point(7, 78);
            this.lblInspection4Y.Name = "lblInspection4Y";
            this.lblInspection4Y.Size = new System.Drawing.Size(50, 21);
            this.lblInspection4Y.TabIndex = 63;
            this.lblInspection4Y.Text = "Y";
            this.lblInspection4Y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm19
            // 
            this.lblmm19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm19.ForeColor = System.Drawing.Color.Black;
            this.lblmm19.Location = new System.Drawing.Point(144, 38);
            this.lblmm19.Name = "lblmm19";
            this.lblmm19.Size = new System.Drawing.Size(47, 20);
            this.lblmm19.TabIndex = 62;
            this.lblmm19.Text = "(mm)";
            this.lblmm19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection4X
            // 
            this.txtInspection4X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection4X.Location = new System.Drawing.Point(67, 34);
            this.txtInspection4X.Name = "txtInspection4X";
            this.txtInspection4X.Size = new System.Drawing.Size(68, 27);
            this.txtInspection4X.TabIndex = 61;
            // 
            // lblInspection4X
            // 
            this.lblInspection4X.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection4X.ForeColor = System.Drawing.Color.Black;
            this.lblInspection4X.Location = new System.Drawing.Point(7, 37);
            this.lblInspection4X.Name = "lblInspection4X";
            this.lblInspection4X.Size = new System.Drawing.Size(50, 21);
            this.lblInspection4X.TabIndex = 58;
            this.lblInspection4X.Text = "X";
            this.lblInspection4X.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoEllipsis = true;
            this.label27.BackColor = System.Drawing.Color.Gainsboro;
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(202, 24);
            this.label27.TabIndex = 9;
            this.label27.Text = "■ 4";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.lblmm18);
            this.panel17.Controls.Add(this.txtInspection3Y);
            this.panel17.Controls.Add(this.lblInspection3Y);
            this.panel17.Controls.Add(this.lblmm17);
            this.panel17.Controls.Add(this.txtInspection3X);
            this.panel17.Controls.Add(this.lblInspection3X);
            this.panel17.Controls.Add(this.label33);
            this.panel17.Location = new System.Drawing.Point(428, 31);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(204, 115);
            this.panel17.TabIndex = 460;
            // 
            // lblmm18
            // 
            this.lblmm18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm18.ForeColor = System.Drawing.Color.Black;
            this.lblmm18.Location = new System.Drawing.Point(144, 79);
            this.lblmm18.Name = "lblmm18";
            this.lblmm18.Size = new System.Drawing.Size(47, 20);
            this.lblmm18.TabIndex = 65;
            this.lblmm18.Text = "(mm)";
            this.lblmm18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection3Y
            // 
            this.txtInspection3Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection3Y.Location = new System.Drawing.Point(67, 75);
            this.txtInspection3Y.Name = "txtInspection3Y";
            this.txtInspection3Y.Size = new System.Drawing.Size(68, 27);
            this.txtInspection3Y.TabIndex = 64;
            // 
            // lblInspection3Y
            // 
            this.lblInspection3Y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection3Y.ForeColor = System.Drawing.Color.Black;
            this.lblInspection3Y.Location = new System.Drawing.Point(7, 78);
            this.lblInspection3Y.Name = "lblInspection3Y";
            this.lblInspection3Y.Size = new System.Drawing.Size(50, 21);
            this.lblInspection3Y.TabIndex = 63;
            this.lblInspection3Y.Text = "Y";
            this.lblInspection3Y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm17
            // 
            this.lblmm17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm17.ForeColor = System.Drawing.Color.Black;
            this.lblmm17.Location = new System.Drawing.Point(144, 38);
            this.lblmm17.Name = "lblmm17";
            this.lblmm17.Size = new System.Drawing.Size(47, 20);
            this.lblmm17.TabIndex = 62;
            this.lblmm17.Text = "(mm)";
            this.lblmm17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection3X
            // 
            this.txtInspection3X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection3X.Location = new System.Drawing.Point(67, 34);
            this.txtInspection3X.Name = "txtInspection3X";
            this.txtInspection3X.Size = new System.Drawing.Size(68, 27);
            this.txtInspection3X.TabIndex = 61;
            // 
            // lblInspection3X
            // 
            this.lblInspection3X.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection3X.ForeColor = System.Drawing.Color.Black;
            this.lblInspection3X.Location = new System.Drawing.Point(7, 37);
            this.lblInspection3X.Name = "lblInspection3X";
            this.lblInspection3X.Size = new System.Drawing.Size(50, 21);
            this.lblInspection3X.TabIndex = 58;
            this.lblInspection3X.Text = "X";
            this.lblInspection3X.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.AutoEllipsis = true;
            this.label33.BackColor = System.Drawing.Color.Gainsboro;
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(202, 24);
            this.label33.TabIndex = 9;
            this.label33.Text = "■ 3";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.lblmm16);
            this.panel14.Controls.Add(this.txtInspection2Y);
            this.panel14.Controls.Add(this.lblInspection2Y);
            this.panel14.Controls.Add(this.lblmm15);
            this.panel14.Controls.Add(this.txtInspection2X);
            this.panel14.Controls.Add(this.lblInspection2X);
            this.panel14.Controls.Add(this.label9);
            this.panel14.Location = new System.Drawing.Point(218, 31);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(204, 115);
            this.panel14.TabIndex = 459;
            // 
            // lblmm16
            // 
            this.lblmm16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm16.ForeColor = System.Drawing.Color.Black;
            this.lblmm16.Location = new System.Drawing.Point(144, 79);
            this.lblmm16.Name = "lblmm16";
            this.lblmm16.Size = new System.Drawing.Size(47, 20);
            this.lblmm16.TabIndex = 65;
            this.lblmm16.Text = "(mm)";
            this.lblmm16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection2Y
            // 
            this.txtInspection2Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection2Y.Location = new System.Drawing.Point(67, 75);
            this.txtInspection2Y.Name = "txtInspection2Y";
            this.txtInspection2Y.Size = new System.Drawing.Size(68, 27);
            this.txtInspection2Y.TabIndex = 64;
            // 
            // lblInspection2Y
            // 
            this.lblInspection2Y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection2Y.ForeColor = System.Drawing.Color.Black;
            this.lblInspection2Y.Location = new System.Drawing.Point(7, 78);
            this.lblInspection2Y.Name = "lblInspection2Y";
            this.lblInspection2Y.Size = new System.Drawing.Size(50, 21);
            this.lblInspection2Y.TabIndex = 63;
            this.lblInspection2Y.Text = "Y";
            this.lblInspection2Y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm15
            // 
            this.lblmm15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm15.ForeColor = System.Drawing.Color.Black;
            this.lblmm15.Location = new System.Drawing.Point(144, 38);
            this.lblmm15.Name = "lblmm15";
            this.lblmm15.Size = new System.Drawing.Size(47, 20);
            this.lblmm15.TabIndex = 62;
            this.lblmm15.Text = "(mm)";
            this.lblmm15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection2X
            // 
            this.txtInspection2X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection2X.Location = new System.Drawing.Point(67, 34);
            this.txtInspection2X.Name = "txtInspection2X";
            this.txtInspection2X.Size = new System.Drawing.Size(68, 27);
            this.txtInspection2X.TabIndex = 61;
            // 
            // lblInspection2X
            // 
            this.lblInspection2X.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection2X.ForeColor = System.Drawing.Color.Black;
            this.lblInspection2X.Location = new System.Drawing.Point(7, 37);
            this.lblInspection2X.Name = "lblInspection2X";
            this.lblInspection2X.Size = new System.Drawing.Size(50, 21);
            this.lblInspection2X.TabIndex = 58;
            this.lblInspection2X.Text = "X";
            this.lblInspection2X.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.BackColor = System.Drawing.Color.Gainsboro;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(202, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "■ 2";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.lblmm14);
            this.panel15.Controls.Add(this.txtInspection1Y);
            this.panel15.Controls.Add(this.lblInspection1Y);
            this.panel15.Controls.Add(this.lblmm13);
            this.panel15.Controls.Add(this.txtInspection1X);
            this.panel15.Controls.Add(this.lblInspection1X);
            this.panel15.Controls.Add(this.label20);
            this.panel15.Location = new System.Drawing.Point(8, 31);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(204, 115);
            this.panel15.TabIndex = 458;
            // 
            // lblmm14
            // 
            this.lblmm14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm14.ForeColor = System.Drawing.Color.Black;
            this.lblmm14.Location = new System.Drawing.Point(144, 79);
            this.lblmm14.Name = "lblmm14";
            this.lblmm14.Size = new System.Drawing.Size(47, 20);
            this.lblmm14.TabIndex = 65;
            this.lblmm14.Text = "(mm)";
            this.lblmm14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection1Y
            // 
            this.txtInspection1Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection1Y.Location = new System.Drawing.Point(67, 75);
            this.txtInspection1Y.Name = "txtInspection1Y";
            this.txtInspection1Y.Size = new System.Drawing.Size(68, 27);
            this.txtInspection1Y.TabIndex = 64;
            // 
            // lblInspection1Y
            // 
            this.lblInspection1Y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection1Y.ForeColor = System.Drawing.Color.Black;
            this.lblInspection1Y.Location = new System.Drawing.Point(7, 78);
            this.lblInspection1Y.Name = "lblInspection1Y";
            this.lblInspection1Y.Size = new System.Drawing.Size(50, 21);
            this.lblInspection1Y.TabIndex = 63;
            this.lblInspection1Y.Text = "Y";
            this.lblInspection1Y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmm13
            // 
            this.lblmm13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblmm13.ForeColor = System.Drawing.Color.Black;
            this.lblmm13.Location = new System.Drawing.Point(144, 38);
            this.lblmm13.Name = "lblmm13";
            this.lblmm13.Size = new System.Drawing.Size(47, 20);
            this.lblmm13.TabIndex = 62;
            this.lblmm13.Text = "(mm)";
            this.lblmm13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspection1X
            // 
            this.txtInspection1X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection1X.Location = new System.Drawing.Point(67, 34);
            this.txtInspection1X.Name = "txtInspection1X";
            this.txtInspection1X.Size = new System.Drawing.Size(68, 27);
            this.txtInspection1X.TabIndex = 61;
            // 
            // lblInspection1X
            // 
            this.lblInspection1X.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspection1X.ForeColor = System.Drawing.Color.Black;
            this.lblInspection1X.Location = new System.Drawing.Point(7, 37);
            this.lblInspection1X.Name = "lblInspection1X";
            this.lblInspection1X.Size = new System.Drawing.Size(50, 21);
            this.lblInspection1X.TabIndex = 58;
            this.lblInspection1X.Text = "X";
            this.lblInspection1X.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoEllipsis = true;
            this.label20.BackColor = System.Drawing.Color.Gainsboro;
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(202, 24);
            this.label20.TabIndex = 9;
            this.label20.Text = "■ 1";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlParameterSequenceOffset
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlParameterSequenceOffset";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblLoadAX1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblmm1;
        private System.Windows.Forms.TextBox txtloadAX1;
        private System.Windows.Forms.Label lblmm2;
        private System.Windows.Forms.TextBox txtloadAX2;
        private System.Windows.Forms.Label lblLoadAX2;
        private System.Windows.Forms.Label lblangle2;
        private System.Windows.Forms.TextBox txtloadAT2;
        private System.Windows.Forms.Label lblLoadAT2;
        private System.Windows.Forms.Label lblangle1;
        private System.Windows.Forms.TextBox txtloadAT1;
        private System.Windows.Forms.Label lblLoadAT1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblangle4;
        private System.Windows.Forms.TextBox txtloadBT2;
        private System.Windows.Forms.Label lblLoadBT2;
        private System.Windows.Forms.Label lblangle3;
        private System.Windows.Forms.TextBox txtloadBT1;
        private System.Windows.Forms.Label lblLoadBT1;
        private System.Windows.Forms.Label lblmm4;
        private System.Windows.Forms.TextBox txtloadBX2;
        private System.Windows.Forms.Label lblLoadBX2;
        private System.Windows.Forms.Label lblmm3;
        private System.Windows.Forms.TextBox txtloadBX1;
        private System.Windows.Forms.Label lblLoadBX1;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblmm8;
        private System.Windows.Forms.TextBox txtAfterIRBX2;
        private System.Windows.Forms.Label lblAfterIRBX2;
        private System.Windows.Forms.Label lblmm7;
        private System.Windows.Forms.TextBox txtAfterIRBX1;
        private System.Windows.Forms.Label lblAfterIRBX1;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblmm6;
        private System.Windows.Forms.TextBox txtAfterIRAX2;
        private System.Windows.Forms.Label lblAfterIRAX2;
        private System.Windows.Forms.Label lblmm5;
        private System.Windows.Forms.TextBox txtAfterIRAX1;
        private System.Windows.Forms.Label lblAfterIRAX1;
        internal System.Windows.Forms.Label label21;
        internal System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblmm12;
        private System.Windows.Forms.TextBox txtUldRotate180BX2;
        private System.Windows.Forms.Label lblUldRotate180BX2;
        private System.Windows.Forms.Label lblmm11;
        private System.Windows.Forms.TextBox txtUldRotate180BX1;
        private System.Windows.Forms.Label lblUldRotate180BX1;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lblmm10;
        private System.Windows.Forms.TextBox txtUldRotate180AX2;
        private System.Windows.Forms.Label lblUldRotate180AX2;
        private System.Windows.Forms.Label lblmm9;
        private System.Windows.Forms.TextBox txtUldRotate180AX1;
        private System.Windows.Forms.Label lblUldRotate180AX1;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lblangle8;
        private System.Windows.Forms.TextBox txtUnloadBT2;
        private System.Windows.Forms.Label lblUnloadBT2;
        private System.Windows.Forms.Label lblangle7;
        private System.Windows.Forms.TextBox txtUnloadBT1;
        private System.Windows.Forms.Label lblUnloadBT1;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblangle6;
        private System.Windows.Forms.TextBox txtUnloadAT2;
        private System.Windows.Forms.Label lblUnloadAT2;
        private System.Windows.Forms.Label lblangle5;
        private System.Windows.Forms.TextBox txtUnloadAT1;
        private System.Windows.Forms.Label lblUnloadAT1;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label lblmm20;
        private System.Windows.Forms.TextBox txtInspection4Y;
        private System.Windows.Forms.Label lblInspection4Y;
        private System.Windows.Forms.Label lblmm19;
        private System.Windows.Forms.TextBox txtInspection4X;
        private System.Windows.Forms.Label lblInspection4X;
        internal System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label lblmm18;
        private System.Windows.Forms.TextBox txtInspection3Y;
        private System.Windows.Forms.Label lblInspection3Y;
        private System.Windows.Forms.Label lblmm17;
        private System.Windows.Forms.TextBox txtInspection3X;
        private System.Windows.Forms.Label lblInspection3X;
        internal System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label lblmm16;
        private System.Windows.Forms.TextBox txtInspection2Y;
        private System.Windows.Forms.Label lblInspection2Y;
        private System.Windows.Forms.Label lblmm15;
        private System.Windows.Forms.TextBox txtInspection2X;
        private System.Windows.Forms.Label lblInspection2X;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label lblmm14;
        private System.Windows.Forms.TextBox txtInspection1Y;
        private System.Windows.Forms.Label lblInspection1Y;
        private System.Windows.Forms.Label lblmm13;
        private System.Windows.Forms.TextBox txtInspection1X;
        private System.Windows.Forms.Label lblInspection1X;
        internal System.Windows.Forms.Label label20;
    }
}

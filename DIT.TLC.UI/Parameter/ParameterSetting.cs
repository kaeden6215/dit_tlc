﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIT.TLC.UI
{
    public class ParameterSetting
    {
        //물류모드
        public double PreAlignSpeed { get; set; }
        public double FineAlignSpeed { get; set; }
        public double BreakingAlignSpeed { get; set; }

        //타임아웃
        public int VacuumTimeout { get; set; }
        public int CylinderTimeout { get; set; }
        public int PreAlignTimeout { get; set; }
        public int BreakingAlignTimeout { get; set; }
        public int InspectionTimeout { get; set; }
        public int MutingTimeout { get; set; }
        public int SeqMoveTimeout { get; set; }

        //count error
        public int INSPErrorCount { get; set; }
        public int MCRErrorCount { get; set; }

        // 도어 오픈 스피드
        public double DoorOpenSpeed { get; set; }

        //브레이킹 횟수
        public int LightBreakingCount { get; set; }
        public int HeavyBreakingCount { get; set; }

        //AUTO Delete
        public int AutoDeleteCycleTime { get; set; }

        //온도 알림
        public double LightTemperature { get; set; }
        public double HeavyTemperature { get; set; }

        //컷라인 측정 주기
        public int CUTLine { get; set; }

        //Blowing 체크
        public double BlowingCheck { get; set; }

        // retry Count
        public int RetryCount { get; set; }

        // 레이저 PD7 에러
        public double LaserPD7Power { get; set; }
        public int PD7ErrorRange { get; set; }

        //레이저 파워 측정
        public int PowerMeasureCycleTime { get; set; }
        public double LaserSettingPower { get; set; }
        public double LaserTargetPower { get; set; }
        public int LaserWarmupTime { get; set; }
        public int PowerMeasureTime { get; set; }
        public double RateOfLOSSErrorRange { get; set; }
        public int TargetErrorRate { get; set; }

        // LDS
        public int LDSMeasureCycleTime { get; set; }
        public int LDSWaitingTime { get; set; }
        public int LDSMeasureTime { get; set; }
        public double LDSErrorRange { get; set; }

        public ParameterSetting()
        {
            PreAlignSpeed = 0;
            FineAlignSpeed = 0;
            BreakingAlignSpeed = 0;
            VacuumTimeout = 0;
            CylinderTimeout = 0;
            PreAlignTimeout = 0;
            BreakingAlignTimeout = 0;
            InspectionTimeout = 0;
            MutingTimeout = 0;
            SeqMoveTimeout = 0;
            INSPErrorCount = 0;
            MCRErrorCount = 0;
            DoorOpenSpeed = 0;
            LightBreakingCount = 0;
            HeavyBreakingCount = 0;
            AutoDeleteCycleTime = 0;
            LightTemperature = 0;
            HeavyTemperature = 0;
            CUTLine = 0;
            BlowingCheck = 0;
            RetryCount = 0;
            LaserPD7Power = 0;
            PD7ErrorRange = 0;
            PowerMeasureCycleTime = 0;
            LaserSettingPower = 0;
            LaserTargetPower = 0;
            LaserWarmupTime = 0;
            PowerMeasureTime = 0;
            RateOfLOSSErrorRange = 0;
            TargetErrorRate = 0;
            LDSMeasureCycleTime = 0;
            LDSWaitingTime = 0;
            LDSMeasureTime = 0;
            LDSErrorRange = 0;
        }
    }
}

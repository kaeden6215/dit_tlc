﻿using DIT.TLC.CTRL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FormSetUserEdit : Form
    {
        //
        public static readonly string MESSAGE_ALREADY_EXIST = "이미 사용하고 있는 아이디 입니다.";
        public static readonly string MESSAGE_NOT_MATCH = "비밀번호와 재입력 비밀번호가 서로 맞지 않습니다.";


        public static readonly string TITLE_ALREADY_EXIST = "생성 실패";
        public static readonly string TITLE_NOT_MATCH = "생성 실패";

        // 생성자
        public FormSetUserEdit()
        {
            InitializeComponent();
        }

        public string UserID { get; internal set; }
        public string Password { get; internal set; }
        public EmUserGrade Grade { get; internal set; }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            tbUserID.Text = UserID;
            tbUserID.ReadOnly = true;

            cboGrade.SelectedIndex = Grade == EmUserGrade.Administrator ? 0 : 1;

        }


        // 이벤트 처리기
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            string passwordNew = tbPasswordNew.Text.Trim();
            string userID = tbUserID.Text.Trim();

            if (string.IsNullOrEmpty(userID) == true)
            {
                tbUserID.Focus();
                return;
            }
 
            if (string.IsNullOrEmpty(passwordNew) == true)
            {
                tbPasswordNew.Focus();
                return;
            }

            if (string.IsNullOrEmpty(tbAgain.Text.Trim()) == true)
            {
                tbAgain.Focus();
                return;
            }

            if (tbPasswordNew.Text.Trim() != tbAgain.Text.Trim())
            {
                MessageBox.Show(MESSAGE_NOT_MATCH, TITLE_NOT_MATCH, MessageBoxButtons.OK);
                return;
            }

            UserID = tbUserID.Text;
            Password = tbPasswordNew.Text;
            Grade = cboGrade.SelectedIndex == 0 ? EmUserGrade.Administrator : EmUserGrade.Operator;

            this.Close();
        }

        private void tbAgain_TextChanged(object sender, EventArgs e)
        {

        }
        private void tbPasswordNew_TextChanged(object sender, EventArgs e)
        {

        }
        private void tbUserID_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlParameterUserManager
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvInformation = new System.Windows.Forms.ListView();
            this.col_No = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Grade = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblUserInformation = new System.Windows.Forms.Label();
            this.panel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvInformation
            // 
            this.lvInformation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvInformation.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_No,
            this.col_ID,
            this.col_Grade});
            this.lvInformation.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvInformation.FullRowSelect = true;
            this.lvInformation.GridLines = true;
            this.lvInformation.Location = new System.Drawing.Point(5, 27);
            this.lvInformation.MultiSelect = false;
            this.lvInformation.Name = "lvInformation";
            this.lvInformation.Size = new System.Drawing.Size(424, 760);
            this.lvInformation.TabIndex = 2;
            this.lvInformation.UseCompatibleStateImageBehavior = false;
            this.lvInformation.View = System.Windows.Forms.View.Details;
            // 
            // col_No
            // 
            this.col_No.Text = "No.";
            // 
            // col_ID
            // 
            this.col_ID.Text = "ID";
            this.col_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_ID.Width = 213;
            // 
            // col_Grade
            // 
            this.col_Grade.Text = "Grade";
            this.col_Grade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_Grade.Width = 140;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnDelete);
            this.panel15.Controls.Add(this.btnModify);
            this.panel15.Controls.Add(this.lvInformation);
            this.panel15.Controls.Add(this.btnAdd);
            this.panel15.Controls.Add(this.lblUserInformation);
            this.panel15.Location = new System.Drawing.Point(3, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(436, 848);
            this.panel15.TabIndex = 459;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.Control;
            this.btnDelete.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(289, 793);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(136, 50);
            this.btnDelete.TabIndex = 48;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.BackColor = System.Drawing.SystemColors.Control;
            this.btnModify.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnModify.ForeColor = System.Drawing.Color.Black;
            this.btnModify.Location = new System.Drawing.Point(147, 793);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(136, 50);
            this.btnModify.TabIndex = 47;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = false;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.Location = new System.Drawing.Point(5, 793);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(136, 50);
            this.btnAdd.TabIndex = 46;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblUserInformation
            // 
            this.lblUserInformation.AutoEllipsis = true;
            this.lblUserInformation.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUserInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUserInformation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUserInformation.ForeColor = System.Drawing.Color.Black;
            this.lblUserInformation.Location = new System.Drawing.Point(0, 0);
            this.lblUserInformation.Name = "lblUserInformation";
            this.lblUserInformation.Size = new System.Drawing.Size(434, 24);
            this.lblUserInformation.TabIndex = 9;
            this.lblUserInformation.Text = "■ User Information";
            this.lblUserInformation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlParameterUserManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel15);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlParameterUserManager";
            this.Size = new System.Drawing.Size(1738, 873);
            this.panel15.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView lvInformation;
        private System.Windows.Forms.ColumnHeader col_No;
        private System.Windows.Forms.ColumnHeader col_ID;
        private System.Windows.Forms.ColumnHeader col_Grade;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Label lblUserInformation;
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlParameterSystem
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.btnDoorSkipDontUse = new System.Windows.Forms.Button();
            this.txtDoorSkip = new System.Windows.Forms.TextBox();
            this.btnDoorSkipUse = new System.Windows.Forms.Button();
            this.lblDoorSkip = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.btnGrabSwitchSkipDontUse = new System.Windows.Forms.Button();
            this.txtGrabSwitchSkip = new System.Windows.Forms.TextBox();
            this.btnGrabSwitchSkipUse = new System.Windows.Forms.Button();
            this.lblGrabSwitchSkip = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.btnMcDownSkipDontUse = new System.Windows.Forms.Button();
            this.txtMcDownSkip = new System.Windows.Forms.TextBox();
            this.btnMcDownSkipUse = new System.Windows.Forms.Button();
            this.lblMcDownSkip = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btnTeachSkipDontUse = new System.Windows.Forms.Button();
            this.txtTeachSkip = new System.Windows.Forms.TextBox();
            this.btnTeachSkipUse = new System.Windows.Forms.Button();
            this.lblTeachSkip = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.btnMutingSkipDontUse = new System.Windows.Forms.Button();
            this.txtMutingSkip = new System.Windows.Forms.TextBox();
            this.btnMutingSkipUse = new System.Windows.Forms.Button();
            this.lblMutingSkip = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnLaserMeasureSkipDontUse = new System.Windows.Forms.Button();
            this.txtLaserMeasureSkip = new System.Windows.Forms.TextBox();
            this.btnLaserMeasureSkipUse = new System.Windows.Forms.Button();
            this.lblLaserMeasureSkip = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnUnloaderInputSkipBDontUse = new System.Windows.Forms.Button();
            this.txtUnloaderInputSkipB = new System.Windows.Forms.TextBox();
            this.btnUnloaderInputSkipBUse = new System.Windows.Forms.Button();
            this.btnUnloaderInputSkipADontUse = new System.Windows.Forms.Button();
            this.txtUnloaderInputSkipA = new System.Windows.Forms.TextBox();
            this.btnUnloaderInputSkipAUse = new System.Windows.Forms.Button();
            this.lblUnloaderInputSkip = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnLoaderInputSkipBDontUse = new System.Windows.Forms.Button();
            this.txtLoaderInputSkipB = new System.Windows.Forms.TextBox();
            this.btnLoaderInputSkipBUse = new System.Windows.Forms.Button();
            this.btnLoaderInputSkipADontUse = new System.Windows.Forms.Button();
            this.txtLoaderInputSkipA = new System.Windows.Forms.TextBox();
            this.btnLoaderInputSkipAUse = new System.Windows.Forms.Button();
            this.lblLoaderInputSkip = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnBufferPickerSkipDontUse = new System.Windows.Forms.Button();
            this.txtBufferPickerSkip = new System.Windows.Forms.TextBox();
            this.btnBufferPickerSkipUse = new System.Windows.Forms.Button();
            this.lblBufferPickerSkip = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnTiltMeasureSkipDontUse = new System.Windows.Forms.Button();
            this.txtTiltMeasureSkip = new System.Windows.Forms.TextBox();
            this.btnTiltMeasureSkipUse = new System.Windows.Forms.Button();
            this.lblTiltMeasureSkip = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnBcrSkipDontUse = new System.Windows.Forms.Button();
            this.txtBcrSkip = new System.Windows.Forms.TextBox();
            this.btnBcrSkipUse = new System.Windows.Forms.Button();
            this.lblBcrSkip = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnMcrSkipDontUse = new System.Windows.Forms.Button();
            this.txtMcrSkip = new System.Windows.Forms.TextBox();
            this.btnMcrSkipUse = new System.Windows.Forms.Button();
            this.lblMcrSkip = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnBreakingAlignSkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingAlignSkip = new System.Windows.Forms.TextBox();
            this.btnBreakingAlignSkipUse = new System.Windows.Forms.Button();
            this.lblBreakingAlignSkip = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnInspectionSkipDontUse = new System.Windows.Forms.Button();
            this.txtInspectionSkip = new System.Windows.Forms.TextBox();
            this.btnInspectionSkipUse = new System.Windows.Forms.Button();
            this.lblInspectionSkip = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnBreakingMotionSkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingMotionSkip = new System.Windows.Forms.TextBox();
            this.btnBreakingMotionSkipUse = new System.Windows.Forms.Button();
            this.lblBreakingMotionSkip = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnLaserSkipDontUse = new System.Windows.Forms.Button();
            this.txtLaserSkip = new System.Windows.Forms.TextBox();
            this.btnLaserSkipUse = new System.Windows.Forms.Button();
            this.lblLaserSkip = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnBreakingBSkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingBSkip = new System.Windows.Forms.TextBox();
            this.btnBreakingBSkipUse = new System.Windows.Forms.Button();
            this.lblBreakingBSkip = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnBreakingASkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingASkip = new System.Windows.Forms.TextBox();
            this.btnBreakingASkipUse = new System.Windows.Forms.Button();
            this.lblBreakingASkip = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnProcessBSkipDontUse = new System.Windows.Forms.Button();
            this.txtProcessBSkip = new System.Windows.Forms.TextBox();
            this.btnProcessBSkipUse = new System.Windows.Forms.Button();
            this.lblProcessBSkip = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnProcessASkipDontUse = new System.Windows.Forms.Button();
            this.txtProcessASkip = new System.Windows.Forms.TextBox();
            this.btnProcessASkipUse = new System.Windows.Forms.Button();
            this.lblProcessASkip = new System.Windows.Forms.Label();
            this.lblSkipMode = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDistributionModeDontUse = new System.Windows.Forms.Button();
            this.txtDistributionMode = new System.Windows.Forms.TextBox();
            this.btnDistributionModeUse = new System.Windows.Forms.Button();
            this.lblDistributionMode = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnSimulationModeDontUse = new System.Windows.Forms.Button();
            this.txtSimulationMode = new System.Windows.Forms.TextBox();
            this.btnSimulationModeUse = new System.Windows.Forms.Button();
            this.lblSimulationMode = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.lblErrorRange = new System.Windows.Forms.Label();
            this.txtLDSErrorRange = new System.Windows.Forms.TextBox();
            this.lblLDSMeasureTime = new System.Windows.Forms.Label();
            this.txtLDSMeasureTime = new System.Windows.Forms.TextBox();
            this.lblWaitingTime = new System.Windows.Forms.Label();
            this.txtLDSWaitingTime = new System.Windows.Forms.TextBox();
            this.lblLDSMeasureCycle = new System.Windows.Forms.Label();
            this.txtLDSMeasureCycle = new System.Windows.Forms.TextBox();
            this.lblLDS = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.lblLossErrorRateRange = new System.Windows.Forms.Label();
            this.txtLossErrorRateRange = new System.Windows.Forms.TextBox();
            this.lbLossRateErrorRange = new System.Windows.Forms.Label();
            this.txtLossRateErrorRange = new System.Windows.Forms.TextBox();
            this.lblTargetErrorRange = new System.Windows.Forms.Label();
            this.txtTargetErrorRange = new System.Windows.Forms.TextBox();
            this.lblCorrectionErrorRange = new System.Windows.Forms.Label();
            this.txtCorrectionErrorRange = new System.Windows.Forms.TextBox();
            this.lblMeasureTime = new System.Windows.Forms.Label();
            this.txtMeasureTime = new System.Windows.Forms.TextBox();
            this.lblWarmUpTime = new System.Windows.Forms.Label();
            this.txtWarmUpTime = new System.Windows.Forms.TextBox();
            this.lblTargetPower = new System.Windows.Forms.Label();
            this.txtTargetPower = new System.Windows.Forms.TextBox();
            this.lblTargetPEC = new System.Windows.Forms.Label();
            this.txtTargetPEC = new System.Windows.Forms.TextBox();
            this.lblLaserPowerMeasureMeasureCycle = new System.Windows.Forms.Label();
            this.txtMeasureCycle = new System.Windows.Forms.TextBox();
            this.lblLaserPowerMeasure = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.txtAutoDeleteCycle = new System.Windows.Forms.TextBox();
            this.lblAutoDeleteCycle = new System.Windows.Forms.Label();
            this.lblNameAutoDeleteCycle = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.txtDoorOpenSpeed = new System.Windows.Forms.TextBox();
            this.lblDoorOpenSpeed = new System.Windows.Forms.Label();
            this.lblNameDoorOpenSpeed = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.lblCriticalAlarm = new System.Windows.Forms.Label();
            this.txtCriticalAlarm = new System.Windows.Forms.TextBox();
            this.lblBreakingCount = new System.Windows.Forms.Label();
            this.lblLightAlarm = new System.Windows.Forms.Label();
            this.txtLightAlarm = new System.Windows.Forms.TextBox();
            this.panel31 = new System.Windows.Forms.Panel();
            this.lblINSP = new System.Windows.Forms.Label();
            this.txtINSP = new System.Windows.Forms.TextBox();
            this.lblCountError = new System.Windows.Forms.Label();
            this.lblMCR = new System.Windows.Forms.Label();
            this.txtMCR = new System.Windows.Forms.TextBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.lblMutingTimeout = new System.Windows.Forms.Label();
            this.txtMutingTimeout = new System.Windows.Forms.TextBox();
            this.lblSequenceMoveTimeout = new System.Windows.Forms.Label();
            this.txtSequenceMoveTimeout = new System.Windows.Forms.TextBox();
            this.lblInspectionTimeout = new System.Windows.Forms.Label();
            this.txtInspectiongTimeout = new System.Windows.Forms.TextBox();
            this.lblBreakingAlignTimeout = new System.Windows.Forms.Label();
            this.txtBreakingAlignTimeout = new System.Windows.Forms.TextBox();
            this.lblPreAlignTimeout = new System.Windows.Forms.Label();
            this.txtPreAlignTimeout = new System.Windows.Forms.TextBox();
            this.lblCylinderTimeout = new System.Windows.Forms.Label();
            this.txtCylinderTimeout = new System.Windows.Forms.TextBox();
            this.lblVacuumTimeout = new System.Windows.Forms.Label();
            this.txtVacuumTimeout = new System.Windows.Forms.TextBox();
            this.lblTimeout = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.lblLaserPD7ErrorRange = new System.Windows.Forms.Label();
            this.txtLaserPD7ErrorRange = new System.Windows.Forms.TextBox();
            this.lblLaserPD7Error = new System.Windows.Forms.Label();
            this.lblLaserPD7Power = new System.Windows.Forms.Label();
            this.txtLaserPD7Power = new System.Windows.Forms.TextBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.lblNameRetryCount = new System.Windows.Forms.Label();
            this.lblRetryCount = new System.Windows.Forms.Label();
            this.txtRetryCount = new System.Windows.Forms.TextBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.lblMeasureCycle = new System.Windows.Forms.Label();
            this.lblCutLineMeasure = new System.Windows.Forms.Label();
            this.txtCutLineMeasure = new System.Windows.Forms.TextBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.txtBlowingCheck = new System.Windows.Forms.TextBox();
            this.lblBlowingCheck = new System.Windows.Forms.Label();
            this.lblNameBlowingCheck = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.txtTemperatureCriticalAlarm = new System.Windows.Forms.TextBox();
            this.lblTemperatureAlarm = new System.Windows.Forms.Label();
            this.txtTemperatureLightAlarm = new System.Windows.Forms.TextBox();
            this.lblNameTemperatureAlarm = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.lblBreakingAlignSpeed = new System.Windows.Forms.Label();
            this.txtBreakingAlignSpeed = new System.Windows.Forms.TextBox();
            this.lblFineAlignSpeed = new System.Windows.Forms.Label();
            this.txtFineAlignSpeed = new System.Windows.Forms.TextBox();
            this.lblPreAlignSpeed = new System.Windows.Forms.Label();
            this.txtPreAlignSpeed = new System.Windows.Forms.TextBox();
            this.lblSettingDistributionMode = new System.Windows.Forms.Label();
            this.lblSetting = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.btnInterlockDontUse = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnInterlockUse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel13.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel37.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(1580, 820);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(155, 35);
            this.btnSave.TabIndex = 48;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.Control;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.panel2);
            this.panel13.Controls.Add(this.panel1);
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Controls.Add(this.lblMode);
            this.panel13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(869, 815);
            this.panel13.TabIndex = 462;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel24);
            this.panel2.Controls.Add(this.panel23);
            this.panel2.Controls.Add(this.panel37);
            this.panel2.Controls.Add(this.panel22);
            this.panel2.Controls.Add(this.panel21);
            this.panel2.Controls.Add(this.panel20);
            this.panel2.Controls.Add(this.panel19);
            this.panel2.Controls.Add(this.panel17);
            this.panel2.Controls.Add(this.panel18);
            this.panel2.Controls.Add(this.panel14);
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.panel16);
            this.panel2.Controls.Add(this.panel12);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.lblSkipMode);
            this.panel2.Location = new System.Drawing.Point(11, 92);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(846, 713);
            this.panel2.TabIndex = 460;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.btnDoorSkipDontUse);
            this.panel24.Controls.Add(this.txtDoorSkip);
            this.panel24.Controls.Add(this.btnDoorSkipUse);
            this.panel24.Controls.Add(this.lblDoorSkip);
            this.panel24.Location = new System.Drawing.Point(425, 588);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(415, 56);
            this.panel24.TabIndex = 475;
            // 
            // btnDoorSkipDontUse
            // 
            this.btnDoorSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnDoorSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDoorSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnDoorSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnDoorSkipDontUse.Name = "btnDoorSkipDontUse";
            this.btnDoorSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnDoorSkipDontUse.TabIndex = 47;
            this.btnDoorSkipDontUse.Text = "사용안함";
            this.btnDoorSkipDontUse.UseVisualStyleBackColor = false;
            this.btnDoorSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtDoorSkip
            // 
            this.txtDoorSkip.Location = new System.Drawing.Point(6, 25);
            this.txtDoorSkip.Name = "txtDoorSkip";
            this.txtDoorSkip.Size = new System.Drawing.Size(177, 23);
            this.txtDoorSkip.TabIndex = 0;
            // 
            // btnDoorSkipUse
            // 
            this.btnDoorSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnDoorSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnDoorSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnDoorSkipUse.Name = "btnDoorSkipUse";
            this.btnDoorSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnDoorSkipUse.TabIndex = 46;
            this.btnDoorSkipUse.Text = "사용함";
            this.btnDoorSkipUse.UseVisualStyleBackColor = false;
            this.btnDoorSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblDoorSkip
            // 
            this.lblDoorSkip.AutoEllipsis = true;
            this.lblDoorSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDoorSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDoorSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblDoorSkip.ForeColor = System.Drawing.Color.Black;
            this.lblDoorSkip.Location = new System.Drawing.Point(0, 0);
            this.lblDoorSkip.Name = "lblDoorSkip";
            this.lblDoorSkip.Size = new System.Drawing.Size(413, 20);
            this.lblDoorSkip.TabIndex = 9;
            this.lblDoorSkip.Text = "■ 도어 스킵";
            this.lblDoorSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.btnGrabSwitchSkipDontUse);
            this.panel23.Controls.Add(this.txtGrabSwitchSkip);
            this.panel23.Controls.Add(this.btnGrabSwitchSkipUse);
            this.panel23.Controls.Add(this.lblGrabSwitchSkip);
            this.panel23.Location = new System.Drawing.Point(425, 526);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(415, 56);
            this.panel23.TabIndex = 474;
            // 
            // btnGrabSwitchSkipDontUse
            // 
            this.btnGrabSwitchSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnGrabSwitchSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnGrabSwitchSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnGrabSwitchSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnGrabSwitchSkipDontUse.Name = "btnGrabSwitchSkipDontUse";
            this.btnGrabSwitchSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnGrabSwitchSkipDontUse.TabIndex = 47;
            this.btnGrabSwitchSkipDontUse.Text = "사용안함";
            this.btnGrabSwitchSkipDontUse.UseVisualStyleBackColor = false;
            this.btnGrabSwitchSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtGrabSwitchSkip
            // 
            this.txtGrabSwitchSkip.Location = new System.Drawing.Point(6, 25);
            this.txtGrabSwitchSkip.Name = "txtGrabSwitchSkip";
            this.txtGrabSwitchSkip.Size = new System.Drawing.Size(177, 23);
            this.txtGrabSwitchSkip.TabIndex = 0;
            // 
            // btnGrabSwitchSkipUse
            // 
            this.btnGrabSwitchSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnGrabSwitchSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnGrabSwitchSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnGrabSwitchSkipUse.Name = "btnGrabSwitchSkipUse";
            this.btnGrabSwitchSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnGrabSwitchSkipUse.TabIndex = 46;
            this.btnGrabSwitchSkipUse.Text = "사용함";
            this.btnGrabSwitchSkipUse.UseVisualStyleBackColor = false;
            this.btnGrabSwitchSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblGrabSwitchSkip
            // 
            this.lblGrabSwitchSkip.AutoEllipsis = true;
            this.lblGrabSwitchSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblGrabSwitchSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblGrabSwitchSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblGrabSwitchSkip.ForeColor = System.Drawing.Color.Black;
            this.lblGrabSwitchSkip.Location = new System.Drawing.Point(0, 0);
            this.lblGrabSwitchSkip.Name = "lblGrabSwitchSkip";
            this.lblGrabSwitchSkip.Size = new System.Drawing.Size(413, 20);
            this.lblGrabSwitchSkip.TabIndex = 9;
            this.lblGrabSwitchSkip.Text = "■ 그랩 스위치 스킵";
            this.lblGrabSwitchSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel22
            // 
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.btnMcDownSkipDontUse);
            this.panel22.Controls.Add(this.txtMcDownSkip);
            this.panel22.Controls.Add(this.btnMcDownSkipUse);
            this.panel22.Controls.Add(this.lblMcDownSkip);
            this.panel22.Location = new System.Drawing.Point(4, 650);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(415, 56);
            this.panel22.TabIndex = 475;
            // 
            // btnMcDownSkipDontUse
            // 
            this.btnMcDownSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcDownSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcDownSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnMcDownSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnMcDownSkipDontUse.Name = "btnMcDownSkipDontUse";
            this.btnMcDownSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnMcDownSkipDontUse.TabIndex = 47;
            this.btnMcDownSkipDontUse.Text = "사용안함";
            this.btnMcDownSkipDontUse.UseVisualStyleBackColor = false;
            this.btnMcDownSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtMcDownSkip
            // 
            this.txtMcDownSkip.Location = new System.Drawing.Point(6, 25);
            this.txtMcDownSkip.Name = "txtMcDownSkip";
            this.txtMcDownSkip.Size = new System.Drawing.Size(177, 23);
            this.txtMcDownSkip.TabIndex = 0;
            // 
            // btnMcDownSkipUse
            // 
            this.btnMcDownSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcDownSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnMcDownSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnMcDownSkipUse.Name = "btnMcDownSkipUse";
            this.btnMcDownSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnMcDownSkipUse.TabIndex = 46;
            this.btnMcDownSkipUse.Text = "사용함";
            this.btnMcDownSkipUse.UseVisualStyleBackColor = false;
            this.btnMcDownSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblMcDownSkip
            // 
            this.lblMcDownSkip.AutoEllipsis = true;
            this.lblMcDownSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMcDownSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMcDownSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMcDownSkip.ForeColor = System.Drawing.Color.Black;
            this.lblMcDownSkip.Location = new System.Drawing.Point(0, 0);
            this.lblMcDownSkip.Name = "lblMcDownSkip";
            this.lblMcDownSkip.Size = new System.Drawing.Size(413, 20);
            this.lblMcDownSkip.TabIndex = 9;
            this.lblMcDownSkip.Text = "■ MC 다운 스킵";
            this.lblMcDownSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.btnTeachSkipDontUse);
            this.panel21.Controls.Add(this.txtTeachSkip);
            this.panel21.Controls.Add(this.btnTeachSkipUse);
            this.panel21.Controls.Add(this.lblTeachSkip);
            this.panel21.Location = new System.Drawing.Point(4, 588);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(415, 56);
            this.panel21.TabIndex = 474;
            // 
            // btnTeachSkipDontUse
            // 
            this.btnTeachSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnTeachSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTeachSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnTeachSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnTeachSkipDontUse.Name = "btnTeachSkipDontUse";
            this.btnTeachSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnTeachSkipDontUse.TabIndex = 47;
            this.btnTeachSkipDontUse.Text = "사용안함";
            this.btnTeachSkipDontUse.UseVisualStyleBackColor = false;
            this.btnTeachSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtTeachSkip
            // 
            this.txtTeachSkip.Location = new System.Drawing.Point(6, 25);
            this.txtTeachSkip.Name = "txtTeachSkip";
            this.txtTeachSkip.Size = new System.Drawing.Size(177, 23);
            this.txtTeachSkip.TabIndex = 0;
            // 
            // btnTeachSkipUse
            // 
            this.btnTeachSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnTeachSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnTeachSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnTeachSkipUse.Name = "btnTeachSkipUse";
            this.btnTeachSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnTeachSkipUse.TabIndex = 46;
            this.btnTeachSkipUse.Text = "사용함";
            this.btnTeachSkipUse.UseVisualStyleBackColor = false;
            this.btnTeachSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblTeachSkip
            // 
            this.lblTeachSkip.AutoEllipsis = true;
            this.lblTeachSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTeachSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTeachSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblTeachSkip.ForeColor = System.Drawing.Color.Black;
            this.lblTeachSkip.Location = new System.Drawing.Point(0, 0);
            this.lblTeachSkip.Name = "lblTeachSkip";
            this.lblTeachSkip.Size = new System.Drawing.Size(413, 20);
            this.lblTeachSkip.TabIndex = 9;
            this.lblTeachSkip.Text = "■ 티치 스킵";
            this.lblTeachSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.btnMutingSkipDontUse);
            this.panel20.Controls.Add(this.txtMutingSkip);
            this.panel20.Controls.Add(this.btnMutingSkipUse);
            this.panel20.Controls.Add(this.lblMutingSkip);
            this.panel20.Location = new System.Drawing.Point(4, 526);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(415, 56);
            this.panel20.TabIndex = 473;
            // 
            // btnMutingSkipDontUse
            // 
            this.btnMutingSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnMutingSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMutingSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnMutingSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnMutingSkipDontUse.Name = "btnMutingSkipDontUse";
            this.btnMutingSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnMutingSkipDontUse.TabIndex = 47;
            this.btnMutingSkipDontUse.Text = "사용안함";
            this.btnMutingSkipDontUse.UseVisualStyleBackColor = false;
            this.btnMutingSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtMutingSkip
            // 
            this.txtMutingSkip.Location = new System.Drawing.Point(6, 25);
            this.txtMutingSkip.Name = "txtMutingSkip";
            this.txtMutingSkip.Size = new System.Drawing.Size(177, 23);
            this.txtMutingSkip.TabIndex = 0;
            // 
            // btnMutingSkipUse
            // 
            this.btnMutingSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnMutingSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnMutingSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnMutingSkipUse.Name = "btnMutingSkipUse";
            this.btnMutingSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnMutingSkipUse.TabIndex = 46;
            this.btnMutingSkipUse.Text = "사용함";
            this.btnMutingSkipUse.UseVisualStyleBackColor = false;
            this.btnMutingSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblMutingSkip
            // 
            this.lblMutingSkip.AutoEllipsis = true;
            this.lblMutingSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMutingSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMutingSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMutingSkip.ForeColor = System.Drawing.Color.Black;
            this.lblMutingSkip.Location = new System.Drawing.Point(0, 0);
            this.lblMutingSkip.Name = "lblMutingSkip";
            this.lblMutingSkip.Size = new System.Drawing.Size(413, 20);
            this.lblMutingSkip.TabIndex = 9;
            this.lblMutingSkip.Text = "■ 뮤팅 스킵";
            this.lblMutingSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.btnLaserMeasureSkipDontUse);
            this.panel19.Controls.Add(this.txtLaserMeasureSkip);
            this.panel19.Controls.Add(this.btnLaserMeasureSkipUse);
            this.panel19.Controls.Add(this.lblLaserMeasureSkip);
            this.panel19.Location = new System.Drawing.Point(4, 464);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(415, 56);
            this.panel19.TabIndex = 470;
            // 
            // btnLaserMeasureSkipDontUse
            // 
            this.btnLaserMeasureSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLaserMeasureSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLaserMeasureSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnLaserMeasureSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnLaserMeasureSkipDontUse.Name = "btnLaserMeasureSkipDontUse";
            this.btnLaserMeasureSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnLaserMeasureSkipDontUse.TabIndex = 47;
            this.btnLaserMeasureSkipDontUse.Text = "사용안함";
            this.btnLaserMeasureSkipDontUse.UseVisualStyleBackColor = false;
            this.btnLaserMeasureSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtLaserMeasureSkip
            // 
            this.txtLaserMeasureSkip.Location = new System.Drawing.Point(6, 25);
            this.txtLaserMeasureSkip.Name = "txtLaserMeasureSkip";
            this.txtLaserMeasureSkip.Size = new System.Drawing.Size(177, 23);
            this.txtLaserMeasureSkip.TabIndex = 0;
            // 
            // btnLaserMeasureSkipUse
            // 
            this.btnLaserMeasureSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLaserMeasureSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnLaserMeasureSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnLaserMeasureSkipUse.Name = "btnLaserMeasureSkipUse";
            this.btnLaserMeasureSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnLaserMeasureSkipUse.TabIndex = 46;
            this.btnLaserMeasureSkipUse.Text = "사용함";
            this.btnLaserMeasureSkipUse.UseVisualStyleBackColor = false;
            this.btnLaserMeasureSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblLaserMeasureSkip
            // 
            this.lblLaserMeasureSkip.AutoEllipsis = true;
            this.lblLaserMeasureSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLaserMeasureSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLaserMeasureSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLaserMeasureSkip.ForeColor = System.Drawing.Color.Black;
            this.lblLaserMeasureSkip.Location = new System.Drawing.Point(0, 0);
            this.lblLaserMeasureSkip.Name = "lblLaserMeasureSkip";
            this.lblLaserMeasureSkip.Size = new System.Drawing.Size(413, 20);
            this.lblLaserMeasureSkip.TabIndex = 9;
            this.lblLaserMeasureSkip.Text = "■ 레이져 측정 스킵";
            this.lblLaserMeasureSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.btnUnloaderInputSkipBDontUse);
            this.panel17.Controls.Add(this.txtUnloaderInputSkipB);
            this.panel17.Controls.Add(this.btnUnloaderInputSkipBUse);
            this.panel17.Controls.Add(this.btnUnloaderInputSkipADontUse);
            this.panel17.Controls.Add(this.txtUnloaderInputSkipA);
            this.panel17.Controls.Add(this.btnUnloaderInputSkipAUse);
            this.panel17.Controls.Add(this.lblUnloaderInputSkip);
            this.panel17.Location = new System.Drawing.Point(425, 402);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(415, 56);
            this.panel17.TabIndex = 472;
            // 
            // btnUnloaderInputSkipBDontUse
            // 
            this.btnUnloaderInputSkipBDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderInputSkipBDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderInputSkipBDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderInputSkipBDontUse.Location = new System.Drawing.Point(342, 25);
            this.btnUnloaderInputSkipBDontUse.Name = "btnUnloaderInputSkipBDontUse";
            this.btnUnloaderInputSkipBDontUse.Size = new System.Drawing.Size(64, 23);
            this.btnUnloaderInputSkipBDontUse.TabIndex = 50;
            this.btnUnloaderInputSkipBDontUse.Text = "사용안함";
            this.btnUnloaderInputSkipBDontUse.UseVisualStyleBackColor = false;
            this.btnUnloaderInputSkipBDontUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // txtUnloaderInputSkipB
            // 
            this.txtUnloaderInputSkipB.Location = new System.Drawing.Point(209, 25);
            this.txtUnloaderInputSkipB.Name = "txtUnloaderInputSkipB";
            this.txtUnloaderInputSkipB.Size = new System.Drawing.Size(57, 23);
            this.txtUnloaderInputSkipB.TabIndex = 48;
            // 
            // btnUnloaderInputSkipBUse
            // 
            this.btnUnloaderInputSkipBUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderInputSkipBUse.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderInputSkipBUse.Location = new System.Drawing.Point(272, 25);
            this.btnUnloaderInputSkipBUse.Name = "btnUnloaderInputSkipBUse";
            this.btnUnloaderInputSkipBUse.Size = new System.Drawing.Size(64, 23);
            this.btnUnloaderInputSkipBUse.TabIndex = 49;
            this.btnUnloaderInputSkipBUse.Text = "사용함";
            this.btnUnloaderInputSkipBUse.UseVisualStyleBackColor = false;
            this.btnUnloaderInputSkipBUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // btnUnloaderInputSkipADontUse
            // 
            this.btnUnloaderInputSkipADontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderInputSkipADontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderInputSkipADontUse.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderInputSkipADontUse.Location = new System.Drawing.Point(139, 25);
            this.btnUnloaderInputSkipADontUse.Name = "btnUnloaderInputSkipADontUse";
            this.btnUnloaderInputSkipADontUse.Size = new System.Drawing.Size(64, 23);
            this.btnUnloaderInputSkipADontUse.TabIndex = 47;
            this.btnUnloaderInputSkipADontUse.Text = "사용안함";
            this.btnUnloaderInputSkipADontUse.UseVisualStyleBackColor = false;
            this.btnUnloaderInputSkipADontUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // txtUnloaderInputSkipA
            // 
            this.txtUnloaderInputSkipA.Location = new System.Drawing.Point(6, 25);
            this.txtUnloaderInputSkipA.Name = "txtUnloaderInputSkipA";
            this.txtUnloaderInputSkipA.Size = new System.Drawing.Size(57, 23);
            this.txtUnloaderInputSkipA.TabIndex = 0;
            // 
            // btnUnloaderInputSkipAUse
            // 
            this.btnUnloaderInputSkipAUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnUnloaderInputSkipAUse.ForeColor = System.Drawing.Color.Black;
            this.btnUnloaderInputSkipAUse.Location = new System.Drawing.Point(69, 25);
            this.btnUnloaderInputSkipAUse.Name = "btnUnloaderInputSkipAUse";
            this.btnUnloaderInputSkipAUse.Size = new System.Drawing.Size(64, 23);
            this.btnUnloaderInputSkipAUse.TabIndex = 46;
            this.btnUnloaderInputSkipAUse.Text = "사용함";
            this.btnUnloaderInputSkipAUse.UseVisualStyleBackColor = false;
            this.btnUnloaderInputSkipAUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // lblUnloaderInputSkip
            // 
            this.lblUnloaderInputSkip.AutoEllipsis = true;
            this.lblUnloaderInputSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblUnloaderInputSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblUnloaderInputSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblUnloaderInputSkip.ForeColor = System.Drawing.Color.Black;
            this.lblUnloaderInputSkip.Location = new System.Drawing.Point(0, 0);
            this.lblUnloaderInputSkip.Name = "lblUnloaderInputSkip";
            this.lblUnloaderInputSkip.Size = new System.Drawing.Size(413, 20);
            this.lblUnloaderInputSkip.TabIndex = 9;
            this.lblUnloaderInputSkip.Text = "■ 언로더부 카세트 A / B 투입 스킵";
            this.lblUnloaderInputSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.btnLoaderInputSkipBDontUse);
            this.panel18.Controls.Add(this.txtLoaderInputSkipB);
            this.panel18.Controls.Add(this.btnLoaderInputSkipBUse);
            this.panel18.Controls.Add(this.btnLoaderInputSkipADontUse);
            this.panel18.Controls.Add(this.txtLoaderInputSkipA);
            this.panel18.Controls.Add(this.btnLoaderInputSkipAUse);
            this.panel18.Controls.Add(this.lblLoaderInputSkip);
            this.panel18.Location = new System.Drawing.Point(4, 402);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(415, 56);
            this.panel18.TabIndex = 471;
            // 
            // btnLoaderInputSkipBDontUse
            // 
            this.btnLoaderInputSkipBDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderInputSkipBDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderInputSkipBDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderInputSkipBDontUse.Location = new System.Drawing.Point(342, 25);
            this.btnLoaderInputSkipBDontUse.Name = "btnLoaderInputSkipBDontUse";
            this.btnLoaderInputSkipBDontUse.Size = new System.Drawing.Size(64, 23);
            this.btnLoaderInputSkipBDontUse.TabIndex = 50;
            this.btnLoaderInputSkipBDontUse.Text = "사용안함";
            this.btnLoaderInputSkipBDontUse.UseVisualStyleBackColor = false;
            this.btnLoaderInputSkipBDontUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // txtLoaderInputSkipB
            // 
            this.txtLoaderInputSkipB.Location = new System.Drawing.Point(209, 25);
            this.txtLoaderInputSkipB.Name = "txtLoaderInputSkipB";
            this.txtLoaderInputSkipB.Size = new System.Drawing.Size(57, 23);
            this.txtLoaderInputSkipB.TabIndex = 48;
            // 
            // btnLoaderInputSkipBUse
            // 
            this.btnLoaderInputSkipBUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderInputSkipBUse.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderInputSkipBUse.Location = new System.Drawing.Point(272, 25);
            this.btnLoaderInputSkipBUse.Name = "btnLoaderInputSkipBUse";
            this.btnLoaderInputSkipBUse.Size = new System.Drawing.Size(64, 23);
            this.btnLoaderInputSkipBUse.TabIndex = 49;
            this.btnLoaderInputSkipBUse.Text = "사용함";
            this.btnLoaderInputSkipBUse.UseVisualStyleBackColor = false;
            this.btnLoaderInputSkipBUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // btnLoaderInputSkipADontUse
            // 
            this.btnLoaderInputSkipADontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderInputSkipADontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLoaderInputSkipADontUse.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderInputSkipADontUse.Location = new System.Drawing.Point(139, 25);
            this.btnLoaderInputSkipADontUse.Name = "btnLoaderInputSkipADontUse";
            this.btnLoaderInputSkipADontUse.Size = new System.Drawing.Size(64, 23);
            this.btnLoaderInputSkipADontUse.TabIndex = 47;
            this.btnLoaderInputSkipADontUse.Text = "사용안함";
            this.btnLoaderInputSkipADontUse.UseVisualStyleBackColor = false;
            this.btnLoaderInputSkipADontUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // txtLoaderInputSkipA
            // 
            this.txtLoaderInputSkipA.Location = new System.Drawing.Point(6, 25);
            this.txtLoaderInputSkipA.Name = "txtLoaderInputSkipA";
            this.txtLoaderInputSkipA.Size = new System.Drawing.Size(57, 23);
            this.txtLoaderInputSkipA.TabIndex = 0;
            // 
            // btnLoaderInputSkipAUse
            // 
            this.btnLoaderInputSkipAUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoaderInputSkipAUse.ForeColor = System.Drawing.Color.Black;
            this.btnLoaderInputSkipAUse.Location = new System.Drawing.Point(69, 25);
            this.btnLoaderInputSkipAUse.Name = "btnLoaderInputSkipAUse";
            this.btnLoaderInputSkipAUse.Size = new System.Drawing.Size(64, 23);
            this.btnLoaderInputSkipAUse.TabIndex = 46;
            this.btnLoaderInputSkipAUse.Text = "사용함";
            this.btnLoaderInputSkipAUse.UseVisualStyleBackColor = false;
            this.btnLoaderInputSkipAUse.Click += new System.EventHandler(this.btnCstSkip_Click);
            // 
            // lblLoaderInputSkip
            // 
            this.lblLoaderInputSkip.AutoEllipsis = true;
            this.lblLoaderInputSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLoaderInputSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLoaderInputSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLoaderInputSkip.ForeColor = System.Drawing.Color.Black;
            this.lblLoaderInputSkip.Location = new System.Drawing.Point(0, 0);
            this.lblLoaderInputSkip.Name = "lblLoaderInputSkip";
            this.lblLoaderInputSkip.Size = new System.Drawing.Size(413, 20);
            this.lblLoaderInputSkip.TabIndex = 9;
            this.lblLoaderInputSkip.Text = "■ 로더부 카세트 A / B 투입 스킵";
            this.lblLoaderInputSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btnBufferPickerSkipDontUse);
            this.panel14.Controls.Add(this.txtBufferPickerSkip);
            this.panel14.Controls.Add(this.btnBufferPickerSkipUse);
            this.panel14.Controls.Add(this.lblBufferPickerSkip);
            this.panel14.Location = new System.Drawing.Point(425, 340);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(415, 56);
            this.panel14.TabIndex = 470;
            // 
            // btnBufferPickerSkipDontUse
            // 
            this.btnBufferPickerSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBufferPickerSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBufferPickerSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnBufferPickerSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnBufferPickerSkipDontUse.Name = "btnBufferPickerSkipDontUse";
            this.btnBufferPickerSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnBufferPickerSkipDontUse.TabIndex = 47;
            this.btnBufferPickerSkipDontUse.Text = "사용안함";
            this.btnBufferPickerSkipDontUse.UseVisualStyleBackColor = false;
            this.btnBufferPickerSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtBufferPickerSkip
            // 
            this.txtBufferPickerSkip.Location = new System.Drawing.Point(6, 25);
            this.txtBufferPickerSkip.Name = "txtBufferPickerSkip";
            this.txtBufferPickerSkip.Size = new System.Drawing.Size(177, 23);
            this.txtBufferPickerSkip.TabIndex = 0;
            // 
            // btnBufferPickerSkipUse
            // 
            this.btnBufferPickerSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBufferPickerSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnBufferPickerSkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnBufferPickerSkipUse.Name = "btnBufferPickerSkipUse";
            this.btnBufferPickerSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnBufferPickerSkipUse.TabIndex = 46;
            this.btnBufferPickerSkipUse.Text = "사용함";
            this.btnBufferPickerSkipUse.UseVisualStyleBackColor = false;
            this.btnBufferPickerSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblBufferPickerSkip
            // 
            this.lblBufferPickerSkip.AutoEllipsis = true;
            this.lblBufferPickerSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBufferPickerSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBufferPickerSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBufferPickerSkip.ForeColor = System.Drawing.Color.Black;
            this.lblBufferPickerSkip.Location = new System.Drawing.Point(0, 0);
            this.lblBufferPickerSkip.Name = "lblBufferPickerSkip";
            this.lblBufferPickerSkip.Size = new System.Drawing.Size(413, 20);
            this.lblBufferPickerSkip.TabIndex = 9;
            this.lblBufferPickerSkip.Text = "■ 버퍼 피커 스킵";
            this.lblBufferPickerSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.btnTiltMeasureSkipDontUse);
            this.panel11.Controls.Add(this.txtTiltMeasureSkip);
            this.panel11.Controls.Add(this.btnTiltMeasureSkipUse);
            this.panel11.Controls.Add(this.lblTiltMeasureSkip);
            this.panel11.Location = new System.Drawing.Point(425, 278);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(415, 56);
            this.panel11.TabIndex = 468;
            // 
            // btnTiltMeasureSkipDontUse
            // 
            this.btnTiltMeasureSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnTiltMeasureSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTiltMeasureSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnTiltMeasureSkipDontUse.Location = new System.Drawing.Point(301, 24);
            this.btnTiltMeasureSkipDontUse.Name = "btnTiltMeasureSkipDontUse";
            this.btnTiltMeasureSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnTiltMeasureSkipDontUse.TabIndex = 47;
            this.btnTiltMeasureSkipDontUse.Text = "사용안함";
            this.btnTiltMeasureSkipDontUse.UseVisualStyleBackColor = false;
            this.btnTiltMeasureSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtTiltMeasureSkip
            // 
            this.txtTiltMeasureSkip.Location = new System.Drawing.Point(6, 25);
            this.txtTiltMeasureSkip.Name = "txtTiltMeasureSkip";
            this.txtTiltMeasureSkip.Size = new System.Drawing.Size(177, 23);
            this.txtTiltMeasureSkip.TabIndex = 0;
            // 
            // btnTiltMeasureSkipUse
            // 
            this.btnTiltMeasureSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnTiltMeasureSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnTiltMeasureSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnTiltMeasureSkipUse.Name = "btnTiltMeasureSkipUse";
            this.btnTiltMeasureSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnTiltMeasureSkipUse.TabIndex = 46;
            this.btnTiltMeasureSkipUse.Text = "사용함";
            this.btnTiltMeasureSkipUse.UseVisualStyleBackColor = false;
            this.btnTiltMeasureSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblTiltMeasureSkip
            // 
            this.lblTiltMeasureSkip.AutoEllipsis = true;
            this.lblTiltMeasureSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTiltMeasureSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTiltMeasureSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblTiltMeasureSkip.ForeColor = System.Drawing.Color.Black;
            this.lblTiltMeasureSkip.Location = new System.Drawing.Point(0, 0);
            this.lblTiltMeasureSkip.Name = "lblTiltMeasureSkip";
            this.lblTiltMeasureSkip.Size = new System.Drawing.Size(413, 20);
            this.lblTiltMeasureSkip.TabIndex = 9;
            this.lblTiltMeasureSkip.Text = "■ 틸트 측정 스킵";
            this.lblTiltMeasureSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.btnBcrSkipDontUse);
            this.panel16.Controls.Add(this.txtBcrSkip);
            this.panel16.Controls.Add(this.btnBcrSkipUse);
            this.panel16.Controls.Add(this.lblBcrSkip);
            this.panel16.Location = new System.Drawing.Point(4, 340);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(415, 56);
            this.panel16.TabIndex = 469;
            // 
            // btnBcrSkipDontUse
            // 
            this.btnBcrSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBcrSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBcrSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnBcrSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnBcrSkipDontUse.Name = "btnBcrSkipDontUse";
            this.btnBcrSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnBcrSkipDontUse.TabIndex = 47;
            this.btnBcrSkipDontUse.Text = "사용안함";
            this.btnBcrSkipDontUse.UseVisualStyleBackColor = false;
            this.btnBcrSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtBcrSkip
            // 
            this.txtBcrSkip.Location = new System.Drawing.Point(6, 25);
            this.txtBcrSkip.Name = "txtBcrSkip";
            this.txtBcrSkip.Size = new System.Drawing.Size(177, 23);
            this.txtBcrSkip.TabIndex = 0;
            // 
            // btnBcrSkipUse
            // 
            this.btnBcrSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBcrSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnBcrSkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnBcrSkipUse.Name = "btnBcrSkipUse";
            this.btnBcrSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnBcrSkipUse.TabIndex = 46;
            this.btnBcrSkipUse.Text = "사용함";
            this.btnBcrSkipUse.UseVisualStyleBackColor = false;
            this.btnBcrSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblBcrSkip
            // 
            this.lblBcrSkip.AutoEllipsis = true;
            this.lblBcrSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBcrSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBcrSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBcrSkip.ForeColor = System.Drawing.Color.Black;
            this.lblBcrSkip.Location = new System.Drawing.Point(0, 0);
            this.lblBcrSkip.Name = "lblBcrSkip";
            this.lblBcrSkip.Size = new System.Drawing.Size(413, 20);
            this.lblBcrSkip.TabIndex = 9;
            this.lblBcrSkip.Text = "■ BCR 스킵";
            this.lblBcrSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.btnMcrSkipDontUse);
            this.panel12.Controls.Add(this.txtMcrSkip);
            this.panel12.Controls.Add(this.btnMcrSkipUse);
            this.panel12.Controls.Add(this.lblMcrSkip);
            this.panel12.Location = new System.Drawing.Point(4, 278);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(415, 56);
            this.panel12.TabIndex = 467;
            // 
            // btnMcrSkipDontUse
            // 
            this.btnMcrSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcrSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnMcrSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnMcrSkipDontUse.Name = "btnMcrSkipDontUse";
            this.btnMcrSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnMcrSkipDontUse.TabIndex = 47;
            this.btnMcrSkipDontUse.Text = "사용안함";
            this.btnMcrSkipDontUse.UseVisualStyleBackColor = false;
            this.btnMcrSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtMcrSkip
            // 
            this.txtMcrSkip.Location = new System.Drawing.Point(6, 25);
            this.txtMcrSkip.Name = "txtMcrSkip";
            this.txtMcrSkip.Size = new System.Drawing.Size(177, 23);
            this.txtMcrSkip.TabIndex = 0;
            // 
            // btnMcrSkipUse
            // 
            this.btnMcrSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnMcrSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnMcrSkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnMcrSkipUse.Name = "btnMcrSkipUse";
            this.btnMcrSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnMcrSkipUse.TabIndex = 46;
            this.btnMcrSkipUse.Text = "사용함";
            this.btnMcrSkipUse.UseVisualStyleBackColor = false;
            this.btnMcrSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblMcrSkip
            // 
            this.lblMcrSkip.AutoEllipsis = true;
            this.lblMcrSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMcrSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMcrSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMcrSkip.ForeColor = System.Drawing.Color.Black;
            this.lblMcrSkip.Location = new System.Drawing.Point(0, 0);
            this.lblMcrSkip.Name = "lblMcrSkip";
            this.lblMcrSkip.Size = new System.Drawing.Size(413, 20);
            this.lblMcrSkip.TabIndex = 9;
            this.lblMcrSkip.Text = "■ MCR 스킵";
            this.lblMcrSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnBreakingAlignSkipDontUse);
            this.panel9.Controls.Add(this.txtBreakingAlignSkip);
            this.panel9.Controls.Add(this.btnBreakingAlignSkipUse);
            this.panel9.Controls.Add(this.lblBreakingAlignSkip);
            this.panel9.Location = new System.Drawing.Point(425, 216);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(415, 56);
            this.panel9.TabIndex = 466;
            // 
            // btnBreakingAlignSkipDontUse
            // 
            this.btnBreakingAlignSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingAlignSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingAlignSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingAlignSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnBreakingAlignSkipDontUse.Name = "btnBreakingAlignSkipDontUse";
            this.btnBreakingAlignSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingAlignSkipDontUse.TabIndex = 47;
            this.btnBreakingAlignSkipDontUse.Text = "사용안함";
            this.btnBreakingAlignSkipDontUse.UseVisualStyleBackColor = false;
            this.btnBreakingAlignSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtBreakingAlignSkip
            // 
            this.txtBreakingAlignSkip.Location = new System.Drawing.Point(6, 25);
            this.txtBreakingAlignSkip.Name = "txtBreakingAlignSkip";
            this.txtBreakingAlignSkip.Size = new System.Drawing.Size(177, 23);
            this.txtBreakingAlignSkip.TabIndex = 0;
            // 
            // btnBreakingAlignSkipUse
            // 
            this.btnBreakingAlignSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingAlignSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingAlignSkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnBreakingAlignSkipUse.Name = "btnBreakingAlignSkipUse";
            this.btnBreakingAlignSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingAlignSkipUse.TabIndex = 46;
            this.btnBreakingAlignSkipUse.Text = "사용함";
            this.btnBreakingAlignSkipUse.UseVisualStyleBackColor = false;
            this.btnBreakingAlignSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblBreakingAlignSkip
            // 
            this.lblBreakingAlignSkip.AutoEllipsis = true;
            this.lblBreakingAlignSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakingAlignSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakingAlignSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakingAlignSkip.ForeColor = System.Drawing.Color.Black;
            this.lblBreakingAlignSkip.Location = new System.Drawing.Point(0, 0);
            this.lblBreakingAlignSkip.Name = "lblBreakingAlignSkip";
            this.lblBreakingAlignSkip.Size = new System.Drawing.Size(413, 20);
            this.lblBreakingAlignSkip.TabIndex = 9;
            this.lblBreakingAlignSkip.Text = "■ 브레이킹 얼라인 스킵";
            this.lblBreakingAlignSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btnInspectionSkipDontUse);
            this.panel10.Controls.Add(this.txtInspectionSkip);
            this.panel10.Controls.Add(this.btnInspectionSkipUse);
            this.panel10.Controls.Add(this.lblInspectionSkip);
            this.panel10.Location = new System.Drawing.Point(4, 216);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(415, 56);
            this.panel10.TabIndex = 465;
            // 
            // btnInspectionSkipDontUse
            // 
            this.btnInspectionSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspectionSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspectionSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnInspectionSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnInspectionSkipDontUse.Name = "btnInspectionSkipDontUse";
            this.btnInspectionSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnInspectionSkipDontUse.TabIndex = 47;
            this.btnInspectionSkipDontUse.Text = "사용안함";
            this.btnInspectionSkipDontUse.UseVisualStyleBackColor = false;
            this.btnInspectionSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtInspectionSkip
            // 
            this.txtInspectionSkip.Location = new System.Drawing.Point(6, 25);
            this.txtInspectionSkip.Name = "txtInspectionSkip";
            this.txtInspectionSkip.Size = new System.Drawing.Size(177, 23);
            this.txtInspectionSkip.TabIndex = 0;
            // 
            // btnInspectionSkipUse
            // 
            this.btnInspectionSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspectionSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnInspectionSkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnInspectionSkipUse.Name = "btnInspectionSkipUse";
            this.btnInspectionSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnInspectionSkipUse.TabIndex = 46;
            this.btnInspectionSkipUse.Text = "사용함";
            this.btnInspectionSkipUse.UseVisualStyleBackColor = false;
            this.btnInspectionSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblInspectionSkip
            // 
            this.lblInspectionSkip.AutoEllipsis = true;
            this.lblInspectionSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblInspectionSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblInspectionSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblInspectionSkip.ForeColor = System.Drawing.Color.Black;
            this.lblInspectionSkip.Location = new System.Drawing.Point(0, 0);
            this.lblInspectionSkip.Name = "lblInspectionSkip";
            this.lblInspectionSkip.Size = new System.Drawing.Size(413, 20);
            this.lblInspectionSkip.TabIndex = 9;
            this.lblInspectionSkip.Text = "■ 인스펙션 스킵";
            this.lblInspectionSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnBreakingMotionSkipDontUse);
            this.panel7.Controls.Add(this.txtBreakingMotionSkip);
            this.panel7.Controls.Add(this.btnBreakingMotionSkipUse);
            this.panel7.Controls.Add(this.lblBreakingMotionSkip);
            this.panel7.Location = new System.Drawing.Point(425, 154);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(415, 56);
            this.panel7.TabIndex = 464;
            // 
            // btnBreakingMotionSkipDontUse
            // 
            this.btnBreakingMotionSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingMotionSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingMotionSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingMotionSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnBreakingMotionSkipDontUse.Name = "btnBreakingMotionSkipDontUse";
            this.btnBreakingMotionSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingMotionSkipDontUse.TabIndex = 47;
            this.btnBreakingMotionSkipDontUse.Text = "사용안함";
            this.btnBreakingMotionSkipDontUse.UseVisualStyleBackColor = false;
            this.btnBreakingMotionSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtBreakingMotionSkip
            // 
            this.txtBreakingMotionSkip.Location = new System.Drawing.Point(6, 25);
            this.txtBreakingMotionSkip.Name = "txtBreakingMotionSkip";
            this.txtBreakingMotionSkip.Size = new System.Drawing.Size(177, 23);
            this.txtBreakingMotionSkip.TabIndex = 0;
            // 
            // btnBreakingMotionSkipUse
            // 
            this.btnBreakingMotionSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingMotionSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingMotionSkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnBreakingMotionSkipUse.Name = "btnBreakingMotionSkipUse";
            this.btnBreakingMotionSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingMotionSkipUse.TabIndex = 46;
            this.btnBreakingMotionSkipUse.Text = "사용함";
            this.btnBreakingMotionSkipUse.UseVisualStyleBackColor = false;
            this.btnBreakingMotionSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblBreakingMotionSkip
            // 
            this.lblBreakingMotionSkip.AutoEllipsis = true;
            this.lblBreakingMotionSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakingMotionSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakingMotionSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakingMotionSkip.ForeColor = System.Drawing.Color.Black;
            this.lblBreakingMotionSkip.Location = new System.Drawing.Point(0, 0);
            this.lblBreakingMotionSkip.Name = "lblBreakingMotionSkip";
            this.lblBreakingMotionSkip.Size = new System.Drawing.Size(413, 20);
            this.lblBreakingMotionSkip.TabIndex = 9;
            this.lblBreakingMotionSkip.Text = "■ 브레이킹 모션 스킵";
            this.lblBreakingMotionSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnLaserSkipDontUse);
            this.panel8.Controls.Add(this.txtLaserSkip);
            this.panel8.Controls.Add(this.btnLaserSkipUse);
            this.panel8.Controls.Add(this.lblLaserSkip);
            this.panel8.Location = new System.Drawing.Point(4, 154);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(415, 56);
            this.panel8.TabIndex = 463;
            // 
            // btnLaserSkipDontUse
            // 
            this.btnLaserSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLaserSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLaserSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnLaserSkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnLaserSkipDontUse.Name = "btnLaserSkipDontUse";
            this.btnLaserSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnLaserSkipDontUse.TabIndex = 47;
            this.btnLaserSkipDontUse.Text = "사용안함";
            this.btnLaserSkipDontUse.UseVisualStyleBackColor = false;
            this.btnLaserSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtLaserSkip
            // 
            this.txtLaserSkip.Location = new System.Drawing.Point(6, 25);
            this.txtLaserSkip.Name = "txtLaserSkip";
            this.txtLaserSkip.Size = new System.Drawing.Size(177, 23);
            this.txtLaserSkip.TabIndex = 0;
            // 
            // btnLaserSkipUse
            // 
            this.btnLaserSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnLaserSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnLaserSkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnLaserSkipUse.Name = "btnLaserSkipUse";
            this.btnLaserSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnLaserSkipUse.TabIndex = 46;
            this.btnLaserSkipUse.Text = "사용함";
            this.btnLaserSkipUse.UseVisualStyleBackColor = false;
            this.btnLaserSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblLaserSkip
            // 
            this.lblLaserSkip.AutoEllipsis = true;
            this.lblLaserSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLaserSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLaserSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLaserSkip.ForeColor = System.Drawing.Color.Black;
            this.lblLaserSkip.Location = new System.Drawing.Point(0, 0);
            this.lblLaserSkip.Name = "lblLaserSkip";
            this.lblLaserSkip.Size = new System.Drawing.Size(413, 20);
            this.lblLaserSkip.TabIndex = 9;
            this.lblLaserSkip.Text = "■ Laser 스킵";
            this.lblLaserSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnBreakingBSkipDontUse);
            this.panel5.Controls.Add(this.txtBreakingBSkip);
            this.panel5.Controls.Add(this.btnBreakingBSkipUse);
            this.panel5.Controls.Add(this.lblBreakingBSkip);
            this.panel5.Location = new System.Drawing.Point(425, 91);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(415, 56);
            this.panel5.TabIndex = 462;
            // 
            // btnBreakingBSkipDontUse
            // 
            this.btnBreakingBSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingBSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingBSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingBSkipDontUse.Location = new System.Drawing.Point(301, 24);
            this.btnBreakingBSkipDontUse.Name = "btnBreakingBSkipDontUse";
            this.btnBreakingBSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingBSkipDontUse.TabIndex = 47;
            this.btnBreakingBSkipDontUse.Text = "사용안함";
            this.btnBreakingBSkipDontUse.UseVisualStyleBackColor = false;
            this.btnBreakingBSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtBreakingBSkip
            // 
            this.txtBreakingBSkip.Location = new System.Drawing.Point(6, 25);
            this.txtBreakingBSkip.Name = "txtBreakingBSkip";
            this.txtBreakingBSkip.Size = new System.Drawing.Size(177, 23);
            this.txtBreakingBSkip.TabIndex = 0;
            // 
            // btnBreakingBSkipUse
            // 
            this.btnBreakingBSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingBSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingBSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnBreakingBSkipUse.Name = "btnBreakingBSkipUse";
            this.btnBreakingBSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingBSkipUse.TabIndex = 46;
            this.btnBreakingBSkipUse.Text = "사용함";
            this.btnBreakingBSkipUse.UseVisualStyleBackColor = false;
            this.btnBreakingBSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblBreakingBSkip
            // 
            this.lblBreakingBSkip.AutoEllipsis = true;
            this.lblBreakingBSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakingBSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakingBSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakingBSkip.ForeColor = System.Drawing.Color.Black;
            this.lblBreakingBSkip.Location = new System.Drawing.Point(0, 0);
            this.lblBreakingBSkip.Name = "lblBreakingBSkip";
            this.lblBreakingBSkip.Size = new System.Drawing.Size(413, 20);
            this.lblBreakingBSkip.TabIndex = 9;
            this.lblBreakingBSkip.Text = "■ 브레이킹 B 물류 스킵";
            this.lblBreakingBSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnBreakingASkipDontUse);
            this.panel6.Controls.Add(this.txtBreakingASkip);
            this.panel6.Controls.Add(this.btnBreakingASkipUse);
            this.panel6.Controls.Add(this.lblBreakingASkip);
            this.panel6.Location = new System.Drawing.Point(4, 91);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(415, 56);
            this.panel6.TabIndex = 461;
            // 
            // btnBreakingASkipDontUse
            // 
            this.btnBreakingASkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingASkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingASkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingASkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnBreakingASkipDontUse.Name = "btnBreakingASkipDontUse";
            this.btnBreakingASkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingASkipDontUse.TabIndex = 47;
            this.btnBreakingASkipDontUse.Text = "사용안함";
            this.btnBreakingASkipDontUse.UseVisualStyleBackColor = false;
            this.btnBreakingASkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtBreakingASkip
            // 
            this.txtBreakingASkip.Location = new System.Drawing.Point(6, 25);
            this.txtBreakingASkip.Name = "txtBreakingASkip";
            this.txtBreakingASkip.Size = new System.Drawing.Size(177, 23);
            this.txtBreakingASkip.TabIndex = 0;
            // 
            // btnBreakingASkipUse
            // 
            this.btnBreakingASkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingASkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingASkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnBreakingASkipUse.Name = "btnBreakingASkipUse";
            this.btnBreakingASkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnBreakingASkipUse.TabIndex = 46;
            this.btnBreakingASkipUse.Text = "사용함";
            this.btnBreakingASkipUse.UseVisualStyleBackColor = false;
            this.btnBreakingASkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblBreakingASkip
            // 
            this.lblBreakingASkip.AutoEllipsis = true;
            this.lblBreakingASkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakingASkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakingASkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakingASkip.ForeColor = System.Drawing.Color.Black;
            this.lblBreakingASkip.Location = new System.Drawing.Point(0, 0);
            this.lblBreakingASkip.Name = "lblBreakingASkip";
            this.lblBreakingASkip.Size = new System.Drawing.Size(413, 20);
            this.lblBreakingASkip.TabIndex = 9;
            this.lblBreakingASkip.Text = "■ 브레이킹 A 물류 스킵";
            this.lblBreakingASkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnProcessBSkipDontUse);
            this.panel4.Controls.Add(this.txtProcessBSkip);
            this.panel4.Controls.Add(this.btnProcessBSkipUse);
            this.panel4.Controls.Add(this.lblProcessBSkip);
            this.panel4.Location = new System.Drawing.Point(425, 29);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(415, 56);
            this.panel4.TabIndex = 460;
            // 
            // btnProcessBSkipDontUse
            // 
            this.btnProcessBSkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBSkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBSkipDontUse.Location = new System.Drawing.Point(301, 24);
            this.btnProcessBSkipDontUse.Name = "btnProcessBSkipDontUse";
            this.btnProcessBSkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnProcessBSkipDontUse.TabIndex = 47;
            this.btnProcessBSkipDontUse.Text = "사용안함";
            this.btnProcessBSkipDontUse.UseVisualStyleBackColor = false;
            this.btnProcessBSkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtProcessBSkip
            // 
            this.txtProcessBSkip.Location = new System.Drawing.Point(6, 25);
            this.txtProcessBSkip.Name = "txtProcessBSkip";
            this.txtProcessBSkip.Size = new System.Drawing.Size(177, 23);
            this.txtProcessBSkip.TabIndex = 0;
            // 
            // btnProcessBSkipUse
            // 
            this.btnProcessBSkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessBSkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnProcessBSkipUse.Location = new System.Drawing.Point(189, 24);
            this.btnProcessBSkipUse.Name = "btnProcessBSkipUse";
            this.btnProcessBSkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnProcessBSkipUse.TabIndex = 46;
            this.btnProcessBSkipUse.Text = "사용함";
            this.btnProcessBSkipUse.UseVisualStyleBackColor = false;
            this.btnProcessBSkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblProcessBSkip
            // 
            this.lblProcessBSkip.AutoEllipsis = true;
            this.lblProcessBSkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessBSkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessBSkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessBSkip.ForeColor = System.Drawing.Color.Black;
            this.lblProcessBSkip.Location = new System.Drawing.Point(0, 0);
            this.lblProcessBSkip.Name = "lblProcessBSkip";
            this.lblProcessBSkip.Size = new System.Drawing.Size(413, 20);
            this.lblProcessBSkip.TabIndex = 9;
            this.lblProcessBSkip.Text = "■ 프로세스 B 물류 스킵";
            this.lblProcessBSkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnProcessASkipDontUse);
            this.panel3.Controls.Add(this.txtProcessASkip);
            this.panel3.Controls.Add(this.btnProcessASkipUse);
            this.panel3.Controls.Add(this.lblProcessASkip);
            this.panel3.Location = new System.Drawing.Point(4, 29);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(415, 56);
            this.panel3.TabIndex = 459;
            // 
            // btnProcessASkipDontUse
            // 
            this.btnProcessASkipDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessASkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessASkipDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnProcessASkipDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnProcessASkipDontUse.Name = "btnProcessASkipDontUse";
            this.btnProcessASkipDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnProcessASkipDontUse.TabIndex = 47;
            this.btnProcessASkipDontUse.Text = "사용안함";
            this.btnProcessASkipDontUse.UseVisualStyleBackColor = false;
            this.btnProcessASkipDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // txtProcessASkip
            // 
            this.txtProcessASkip.Location = new System.Drawing.Point(6, 25);
            this.txtProcessASkip.Name = "txtProcessASkip";
            this.txtProcessASkip.Size = new System.Drawing.Size(177, 23);
            this.txtProcessASkip.TabIndex = 0;
            // 
            // btnProcessASkipUse
            // 
            this.btnProcessASkipUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessASkipUse.ForeColor = System.Drawing.Color.Black;
            this.btnProcessASkipUse.Location = new System.Drawing.Point(189, 25);
            this.btnProcessASkipUse.Name = "btnProcessASkipUse";
            this.btnProcessASkipUse.Size = new System.Drawing.Size(106, 23);
            this.btnProcessASkipUse.TabIndex = 46;
            this.btnProcessASkipUse.Text = "사용함";
            this.btnProcessASkipUse.UseVisualStyleBackColor = false;
            this.btnProcessASkipUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // lblProcessASkip
            // 
            this.lblProcessASkip.AutoEllipsis = true;
            this.lblProcessASkip.BackColor = System.Drawing.Color.Gainsboro;
            this.lblProcessASkip.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProcessASkip.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblProcessASkip.ForeColor = System.Drawing.Color.Black;
            this.lblProcessASkip.Location = new System.Drawing.Point(0, 0);
            this.lblProcessASkip.Name = "lblProcessASkip";
            this.lblProcessASkip.Size = new System.Drawing.Size(413, 20);
            this.lblProcessASkip.TabIndex = 9;
            this.lblProcessASkip.Text = "■ 프로세스 A 물류 스킵";
            this.lblProcessASkip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSkipMode
            // 
            this.lblSkipMode.AutoEllipsis = true;
            this.lblSkipMode.BackColor = System.Drawing.Color.Gainsboro;
            this.lblSkipMode.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSkipMode.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblSkipMode.ForeColor = System.Drawing.Color.Black;
            this.lblSkipMode.Location = new System.Drawing.Point(0, 0);
            this.lblSkipMode.Name = "lblSkipMode";
            this.lblSkipMode.Size = new System.Drawing.Size(844, 22);
            this.lblSkipMode.TabIndex = 9;
            this.lblSkipMode.Text = "■ 스킵 모드";
            this.lblSkipMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnDistributionModeDontUse);
            this.panel1.Controls.Add(this.txtDistributionMode);
            this.panel1.Controls.Add(this.btnDistributionModeUse);
            this.panel1.Controls.Add(this.lblDistributionMode);
            this.panel1.Location = new System.Drawing.Point(437, 32);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 56);
            this.panel1.TabIndex = 459;
            // 
            // btnDistributionModeDontUse
            // 
            this.btnDistributionModeDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnDistributionModeDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDistributionModeDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnDistributionModeDontUse.Location = new System.Drawing.Point(303, 25);
            this.btnDistributionModeDontUse.Name = "btnDistributionModeDontUse";
            this.btnDistributionModeDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnDistributionModeDontUse.TabIndex = 47;
            this.btnDistributionModeDontUse.Text = "사용안함";
            this.btnDistributionModeDontUse.UseVisualStyleBackColor = false;
            this.btnDistributionModeDontUse.Click += new System.EventHandler(this.btnMode_Click);
            // 
            // txtDistributionMode
            // 
            this.txtDistributionMode.Location = new System.Drawing.Point(8, 25);
            this.txtDistributionMode.Name = "txtDistributionMode";
            this.txtDistributionMode.Size = new System.Drawing.Size(177, 23);
            this.txtDistributionMode.TabIndex = 0;
            // 
            // btnDistributionModeUse
            // 
            this.btnDistributionModeUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnDistributionModeUse.ForeColor = System.Drawing.Color.Black;
            this.btnDistributionModeUse.Location = new System.Drawing.Point(191, 25);
            this.btnDistributionModeUse.Name = "btnDistributionModeUse";
            this.btnDistributionModeUse.Size = new System.Drawing.Size(106, 23);
            this.btnDistributionModeUse.TabIndex = 46;
            this.btnDistributionModeUse.Text = "사용함";
            this.btnDistributionModeUse.UseVisualStyleBackColor = false;
            this.btnDistributionModeUse.Click += new System.EventHandler(this.btnMode_Click);
            // 
            // lblDistributionMode
            // 
            this.lblDistributionMode.AutoEllipsis = true;
            this.lblDistributionMode.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDistributionMode.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDistributionMode.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblDistributionMode.ForeColor = System.Drawing.Color.Black;
            this.lblDistributionMode.Location = new System.Drawing.Point(0, 0);
            this.lblDistributionMode.Name = "lblDistributionMode";
            this.lblDistributionMode.Size = new System.Drawing.Size(418, 20);
            this.lblDistributionMode.TabIndex = 9;
            this.lblDistributionMode.Text = "■ 물류 모드";
            this.lblDistributionMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnSimulationModeDontUse);
            this.panel15.Controls.Add(this.txtSimulationMode);
            this.panel15.Controls.Add(this.btnSimulationModeUse);
            this.panel15.Controls.Add(this.lblSimulationMode);
            this.panel15.Location = new System.Drawing.Point(11, 32);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(420, 56);
            this.panel15.TabIndex = 458;
            // 
            // btnSimulationModeDontUse
            // 
            this.btnSimulationModeDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnSimulationModeDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSimulationModeDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnSimulationModeDontUse.Location = new System.Drawing.Point(303, 25);
            this.btnSimulationModeDontUse.Name = "btnSimulationModeDontUse";
            this.btnSimulationModeDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnSimulationModeDontUse.TabIndex = 47;
            this.btnSimulationModeDontUse.Text = "사용안함";
            this.btnSimulationModeDontUse.UseVisualStyleBackColor = false;
            this.btnSimulationModeDontUse.Click += new System.EventHandler(this.btnMode_Click);
            // 
            // txtSimulationMode
            // 
            this.txtSimulationMode.Location = new System.Drawing.Point(8, 25);
            this.txtSimulationMode.Name = "txtSimulationMode";
            this.txtSimulationMode.Size = new System.Drawing.Size(177, 23);
            this.txtSimulationMode.TabIndex = 0;
            // 
            // btnSimulationModeUse
            // 
            this.btnSimulationModeUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnSimulationModeUse.ForeColor = System.Drawing.Color.Black;
            this.btnSimulationModeUse.Location = new System.Drawing.Point(191, 25);
            this.btnSimulationModeUse.Name = "btnSimulationModeUse";
            this.btnSimulationModeUse.Size = new System.Drawing.Size(106, 23);
            this.btnSimulationModeUse.TabIndex = 46;
            this.btnSimulationModeUse.Text = "사용함";
            this.btnSimulationModeUse.UseVisualStyleBackColor = false;
            this.btnSimulationModeUse.Click += new System.EventHandler(this.btnMode_Click);
            // 
            // lblSimulationMode
            // 
            this.lblSimulationMode.AutoEllipsis = true;
            this.lblSimulationMode.BackColor = System.Drawing.Color.Gainsboro;
            this.lblSimulationMode.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSimulationMode.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblSimulationMode.ForeColor = System.Drawing.Color.Black;
            this.lblSimulationMode.Location = new System.Drawing.Point(0, 0);
            this.lblSimulationMode.Name = "lblSimulationMode";
            this.lblSimulationMode.Size = new System.Drawing.Size(418, 20);
            this.lblSimulationMode.TabIndex = 9;
            this.lblSimulationMode.Text = "■ 시뮬레이션 모드";
            this.lblSimulationMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMode
            // 
            this.lblMode.AutoEllipsis = true;
            this.lblMode.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMode.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMode.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMode.ForeColor = System.Drawing.Color.Black;
            this.lblMode.Location = new System.Drawing.Point(0, 0);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(867, 26);
            this.lblMode.TabIndex = 9;
            this.lblMode.Text = "■ Mode";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.Control;
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.panel36);
            this.panel25.Controls.Add(this.panel35);
            this.panel25.Controls.Add(this.panel33);
            this.panel25.Controls.Add(this.panel32);
            this.panel25.Controls.Add(this.panel34);
            this.panel25.Controls.Add(this.panel31);
            this.panel25.Controls.Add(this.panel30);
            this.panel25.Controls.Add(this.panel29);
            this.panel25.Controls.Add(this.panel27);
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Controls.Add(this.panel28);
            this.panel25.Controls.Add(this.panel47);
            this.panel25.Controls.Add(this.panel48);
            this.panel25.Controls.Add(this.lblSetting);
            this.panel25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.panel25.Location = new System.Drawing.Point(870, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(869, 815);
            this.panel25.TabIndex = 463;
            // 
            // panel36
            // 
            this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel36.Controls.Add(this.lblErrorRange);
            this.panel36.Controls.Add(this.txtLDSErrorRange);
            this.panel36.Controls.Add(this.lblLDSMeasureTime);
            this.panel36.Controls.Add(this.txtLDSMeasureTime);
            this.panel36.Controls.Add(this.lblWaitingTime);
            this.panel36.Controls.Add(this.txtLDSWaitingTime);
            this.panel36.Controls.Add(this.lblLDSMeasureCycle);
            this.panel36.Controls.Add(this.txtLDSMeasureCycle);
            this.panel36.Controls.Add(this.lblLDS);
            this.panel36.Location = new System.Drawing.Point(437, 653);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(420, 140);
            this.panel36.TabIndex = 469;
            // 
            // lblErrorRange
            // 
            this.lblErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblErrorRange.ForeColor = System.Drawing.Color.Black;
            this.lblErrorRange.Location = new System.Drawing.Point(5, 112);
            this.lblErrorRange.Name = "lblErrorRange";
            this.lblErrorRange.Size = new System.Drawing.Size(210, 21);
            this.lblErrorRange.TabIndex = 65;
            this.lblErrorRange.Text = "오차 범위[mm] : ";
            this.lblErrorRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLDSErrorRange
            // 
            this.txtLDSErrorRange.Location = new System.Drawing.Point(221, 111);
            this.txtLDSErrorRange.Name = "txtLDSErrorRange";
            this.txtLDSErrorRange.Size = new System.Drawing.Size(190, 23);
            this.txtLDSErrorRange.TabIndex = 64;
            // 
            // lblLDSMeasureTime
            // 
            this.lblLDSMeasureTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLDSMeasureTime.ForeColor = System.Drawing.Color.Black;
            this.lblLDSMeasureTime.Location = new System.Drawing.Point(5, 82);
            this.lblLDSMeasureTime.Name = "lblLDSMeasureTime";
            this.lblLDSMeasureTime.Size = new System.Drawing.Size(210, 21);
            this.lblLDSMeasureTime.TabIndex = 63;
            this.lblLDSMeasureTime.Text = "측정 시간[s] :  ";
            this.lblLDSMeasureTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLDSMeasureTime
            // 
            this.txtLDSMeasureTime.Location = new System.Drawing.Point(221, 82);
            this.txtLDSMeasureTime.Name = "txtLDSMeasureTime";
            this.txtLDSMeasureTime.Size = new System.Drawing.Size(190, 23);
            this.txtLDSMeasureTime.TabIndex = 62;
            // 
            // lblWaitingTime
            // 
            this.lblWaitingTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblWaitingTime.ForeColor = System.Drawing.Color.Black;
            this.lblWaitingTime.Location = new System.Drawing.Point(5, 54);
            this.lblWaitingTime.Name = "lblWaitingTime";
            this.lblWaitingTime.Size = new System.Drawing.Size(210, 21);
            this.lblWaitingTime.TabIndex = 61;
            this.lblWaitingTime.Text = "Waiting Time[s] :  ";
            this.lblWaitingTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLDSWaitingTime
            // 
            this.txtLDSWaitingTime.Location = new System.Drawing.Point(221, 53);
            this.txtLDSWaitingTime.Name = "txtLDSWaitingTime";
            this.txtLDSWaitingTime.Size = new System.Drawing.Size(190, 23);
            this.txtLDSWaitingTime.TabIndex = 60;
            // 
            // lblLDSMeasureCycle
            // 
            this.lblLDSMeasureCycle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLDSMeasureCycle.ForeColor = System.Drawing.Color.Black;
            this.lblLDSMeasureCycle.Location = new System.Drawing.Point(5, 25);
            this.lblLDSMeasureCycle.Name = "lblLDSMeasureCycle";
            this.lblLDSMeasureCycle.Size = new System.Drawing.Size(210, 21);
            this.lblLDSMeasureCycle.TabIndex = 59;
            this.lblLDSMeasureCycle.Text = "측정 주기[h] : ";
            this.lblLDSMeasureCycle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLDSMeasureCycle
            // 
            this.txtLDSMeasureCycle.Location = new System.Drawing.Point(221, 24);
            this.txtLDSMeasureCycle.Name = "txtLDSMeasureCycle";
            this.txtLDSMeasureCycle.Size = new System.Drawing.Size(190, 23);
            this.txtLDSMeasureCycle.TabIndex = 0;
            // 
            // lblLDS
            // 
            this.lblLDS.AutoEllipsis = true;
            this.lblLDS.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLDS.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLDS.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLDS.ForeColor = System.Drawing.Color.Black;
            this.lblLDS.Location = new System.Drawing.Point(0, 0);
            this.lblLDS.Name = "lblLDS";
            this.lblLDS.Size = new System.Drawing.Size(418, 20);
            this.lblLDS.TabIndex = 9;
            this.lblLDS.Text = "■ LDS";
            this.lblLDS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel35
            // 
            this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel35.Controls.Add(this.lblLossErrorRateRange);
            this.panel35.Controls.Add(this.txtLossErrorRateRange);
            this.panel35.Controls.Add(this.lbLossRateErrorRange);
            this.panel35.Controls.Add(this.txtLossRateErrorRange);
            this.panel35.Controls.Add(this.lblTargetErrorRange);
            this.panel35.Controls.Add(this.txtTargetErrorRange);
            this.panel35.Controls.Add(this.lblCorrectionErrorRange);
            this.panel35.Controls.Add(this.txtCorrectionErrorRange);
            this.panel35.Controls.Add(this.lblMeasureTime);
            this.panel35.Controls.Add(this.txtMeasureTime);
            this.panel35.Controls.Add(this.lblWarmUpTime);
            this.panel35.Controls.Add(this.txtWarmUpTime);
            this.panel35.Controls.Add(this.lblTargetPower);
            this.panel35.Controls.Add(this.txtTargetPower);
            this.panel35.Controls.Add(this.lblTargetPEC);
            this.panel35.Controls.Add(this.txtTargetPEC);
            this.panel35.Controls.Add(this.lblLaserPowerMeasureMeasureCycle);
            this.panel35.Controls.Add(this.txtMeasureCycle);
            this.panel35.Controls.Add(this.lblLaserPowerMeasure);
            this.panel35.Location = new System.Drawing.Point(437, 362);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(420, 285);
            this.panel35.TabIndex = 468;
            // 
            // lblLossErrorRateRange
            // 
            this.lblLossErrorRateRange.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLossErrorRateRange.ForeColor = System.Drawing.Color.Black;
            this.lblLossErrorRateRange.Location = new System.Drawing.Point(6, 258);
            this.lblLossErrorRateRange.Name = "lblLossErrorRateRange";
            this.lblLossErrorRateRange.Size = new System.Drawing.Size(210, 21);
            this.lblLossErrorRateRange.TabIndex = 75;
            this.lblLossErrorRateRange.Text = "LOSS ERROR RATE 범위";
            this.lblLossErrorRateRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLossErrorRateRange
            // 
            this.txtLossErrorRateRange.Location = new System.Drawing.Point(222, 257);
            this.txtLossErrorRateRange.Name = "txtLossErrorRateRange";
            this.txtLossErrorRateRange.Size = new System.Drawing.Size(190, 23);
            this.txtLossErrorRateRange.TabIndex = 74;
            // 
            // lbLossRateErrorRange
            // 
            this.lbLossRateErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbLossRateErrorRange.ForeColor = System.Drawing.Color.Black;
            this.lbLossRateErrorRange.Location = new System.Drawing.Point(6, 229);
            this.lbLossRateErrorRange.Name = "lbLossRateErrorRange";
            this.lbLossRateErrorRange.Size = new System.Drawing.Size(210, 21);
            this.lbLossRateErrorRange.TabIndex = 73;
            this.lbLossRateErrorRange.Text = "LOSS RATE 오차 범위";
            this.lbLossRateErrorRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLossRateErrorRange
            // 
            this.txtLossRateErrorRange.Location = new System.Drawing.Point(222, 228);
            this.txtLossRateErrorRange.Name = "txtLossRateErrorRange";
            this.txtLossRateErrorRange.Size = new System.Drawing.Size(190, 23);
            this.txtLossRateErrorRange.TabIndex = 72;
            // 
            // lblTargetErrorRange
            // 
            this.lblTargetErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTargetErrorRange.ForeColor = System.Drawing.Color.Black;
            this.lblTargetErrorRange.Location = new System.Drawing.Point(5, 200);
            this.lblTargetErrorRange.Name = "lblTargetErrorRange";
            this.lblTargetErrorRange.Size = new System.Drawing.Size(210, 21);
            this.lblTargetErrorRange.TabIndex = 71;
            this.lblTargetErrorRange.Text = "Target 오차 범위[w] : ";
            this.lblTargetErrorRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTargetErrorRange
            // 
            this.txtTargetErrorRange.Location = new System.Drawing.Point(221, 199);
            this.txtTargetErrorRange.Name = "txtTargetErrorRange";
            this.txtTargetErrorRange.Size = new System.Drawing.Size(190, 23);
            this.txtTargetErrorRange.TabIndex = 70;
            // 
            // lblCorrectionErrorRange
            // 
            this.lblCorrectionErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCorrectionErrorRange.ForeColor = System.Drawing.Color.Black;
            this.lblCorrectionErrorRange.Location = new System.Drawing.Point(5, 171);
            this.lblCorrectionErrorRange.Name = "lblCorrectionErrorRange";
            this.lblCorrectionErrorRange.Size = new System.Drawing.Size(210, 21);
            this.lblCorrectionErrorRange.TabIndex = 69;
            this.lblCorrectionErrorRange.Text = "보정 오차 범위[w] : ";
            this.lblCorrectionErrorRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCorrectionErrorRange
            // 
            this.txtCorrectionErrorRange.Location = new System.Drawing.Point(221, 170);
            this.txtCorrectionErrorRange.Name = "txtCorrectionErrorRange";
            this.txtCorrectionErrorRange.Size = new System.Drawing.Size(190, 23);
            this.txtCorrectionErrorRange.TabIndex = 68;
            // 
            // lblMeasureTime
            // 
            this.lblMeasureTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMeasureTime.ForeColor = System.Drawing.Color.Black;
            this.lblMeasureTime.Location = new System.Drawing.Point(5, 142);
            this.lblMeasureTime.Name = "lblMeasureTime";
            this.lblMeasureTime.Size = new System.Drawing.Size(210, 21);
            this.lblMeasureTime.TabIndex = 67;
            this.lblMeasureTime.Text = "측정 시간[s] : ";
            this.lblMeasureTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMeasureTime
            // 
            this.txtMeasureTime.Location = new System.Drawing.Point(221, 141);
            this.txtMeasureTime.Name = "txtMeasureTime";
            this.txtMeasureTime.Size = new System.Drawing.Size(190, 23);
            this.txtMeasureTime.TabIndex = 66;
            // 
            // lblWarmUpTime
            // 
            this.lblWarmUpTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblWarmUpTime.ForeColor = System.Drawing.Color.Black;
            this.lblWarmUpTime.Location = new System.Drawing.Point(5, 113);
            this.lblWarmUpTime.Name = "lblWarmUpTime";
            this.lblWarmUpTime.Size = new System.Drawing.Size(210, 21);
            this.lblWarmUpTime.TabIndex = 65;
            this.lblWarmUpTime.Text = "WarmUp Time[s] : ";
            this.lblWarmUpTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWarmUpTime
            // 
            this.txtWarmUpTime.Location = new System.Drawing.Point(221, 112);
            this.txtWarmUpTime.Name = "txtWarmUpTime";
            this.txtWarmUpTime.Size = new System.Drawing.Size(190, 23);
            this.txtWarmUpTime.TabIndex = 64;
            // 
            // lblTargetPower
            // 
            this.lblTargetPower.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTargetPower.ForeColor = System.Drawing.Color.Black;
            this.lblTargetPower.Location = new System.Drawing.Point(5, 84);
            this.lblTargetPower.Name = "lblTargetPower";
            this.lblTargetPower.Size = new System.Drawing.Size(210, 21);
            this.lblTargetPower.TabIndex = 63;
            this.lblTargetPower.Text = "Target 파워[w] : ";
            this.lblTargetPower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTargetPower
            // 
            this.txtTargetPower.Location = new System.Drawing.Point(221, 83);
            this.txtTargetPower.Name = "txtTargetPower";
            this.txtTargetPower.Size = new System.Drawing.Size(190, 23);
            this.txtTargetPower.TabIndex = 62;
            // 
            // lblTargetPEC
            // 
            this.lblTargetPEC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTargetPEC.ForeColor = System.Drawing.Color.Black;
            this.lblTargetPEC.Location = new System.Drawing.Point(5, 55);
            this.lblTargetPEC.Name = "lblTargetPEC";
            this.lblTargetPEC.Size = new System.Drawing.Size(210, 21);
            this.lblTargetPEC.TabIndex = 61;
            this.lblTargetPEC.Text = "Target PEC[%] : ";
            this.lblTargetPEC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTargetPEC
            // 
            this.txtTargetPEC.Location = new System.Drawing.Point(221, 54);
            this.txtTargetPEC.Name = "txtTargetPEC";
            this.txtTargetPEC.Size = new System.Drawing.Size(190, 23);
            this.txtTargetPEC.TabIndex = 60;
            // 
            // lblLaserPowerMeasureMeasureCycle
            // 
            this.lblLaserPowerMeasureMeasureCycle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLaserPowerMeasureMeasureCycle.ForeColor = System.Drawing.Color.Black;
            this.lblLaserPowerMeasureMeasureCycle.Location = new System.Drawing.Point(5, 26);
            this.lblLaserPowerMeasureMeasureCycle.Name = "lblLaserPowerMeasureMeasureCycle";
            this.lblLaserPowerMeasureMeasureCycle.Size = new System.Drawing.Size(210, 21);
            this.lblLaserPowerMeasureMeasureCycle.TabIndex = 59;
            this.lblLaserPowerMeasureMeasureCycle.Text = "측정 주기[h] : ";
            this.lblLaserPowerMeasureMeasureCycle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMeasureCycle
            // 
            this.txtMeasureCycle.Location = new System.Drawing.Point(221, 25);
            this.txtMeasureCycle.Name = "txtMeasureCycle";
            this.txtMeasureCycle.Size = new System.Drawing.Size(190, 23);
            this.txtMeasureCycle.TabIndex = 0;
            // 
            // lblLaserPowerMeasure
            // 
            this.lblLaserPowerMeasure.AutoEllipsis = true;
            this.lblLaserPowerMeasure.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLaserPowerMeasure.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLaserPowerMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLaserPowerMeasure.ForeColor = System.Drawing.Color.Black;
            this.lblLaserPowerMeasure.Location = new System.Drawing.Point(0, 0);
            this.lblLaserPowerMeasure.Name = "lblLaserPowerMeasure";
            this.lblLaserPowerMeasure.Size = new System.Drawing.Size(418, 20);
            this.lblLaserPowerMeasure.TabIndex = 9;
            this.lblLaserPowerMeasure.Text = "■ 레이져 파워 측정";
            this.lblLaserPowerMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel33
            // 
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Controls.Add(this.txtAutoDeleteCycle);
            this.panel33.Controls.Add(this.lblAutoDeleteCycle);
            this.panel33.Controls.Add(this.lblNameAutoDeleteCycle);
            this.panel33.Location = new System.Drawing.Point(11, 674);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(420, 60);
            this.panel33.TabIndex = 467;
            // 
            // txtAutoDeleteCycle
            // 
            this.txtAutoDeleteCycle.Location = new System.Drawing.Point(222, 27);
            this.txtAutoDeleteCycle.Name = "txtAutoDeleteCycle";
            this.txtAutoDeleteCycle.Size = new System.Drawing.Size(192, 23);
            this.txtAutoDeleteCycle.TabIndex = 69;
            // 
            // lblAutoDeleteCycle
            // 
            this.lblAutoDeleteCycle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAutoDeleteCycle.ForeColor = System.Drawing.Color.Black;
            this.lblAutoDeleteCycle.Location = new System.Drawing.Point(6, 28);
            this.lblAutoDeleteCycle.Name = "lblAutoDeleteCycle";
            this.lblAutoDeleteCycle.Size = new System.Drawing.Size(210, 21);
            this.lblAutoDeleteCycle.TabIndex = 65;
            this.lblAutoDeleteCycle.Text = "Auto Delete 주기[Day] : ";
            this.lblAutoDeleteCycle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNameAutoDeleteCycle
            // 
            this.lblNameAutoDeleteCycle.AutoEllipsis = true;
            this.lblNameAutoDeleteCycle.BackColor = System.Drawing.Color.Gainsboro;
            this.lblNameAutoDeleteCycle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNameAutoDeleteCycle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblNameAutoDeleteCycle.ForeColor = System.Drawing.Color.Black;
            this.lblNameAutoDeleteCycle.Location = new System.Drawing.Point(0, 0);
            this.lblNameAutoDeleteCycle.Name = "lblNameAutoDeleteCycle";
            this.lblNameAutoDeleteCycle.Size = new System.Drawing.Size(418, 20);
            this.lblNameAutoDeleteCycle.TabIndex = 9;
            this.lblNameAutoDeleteCycle.Text = "■ Auto Delete 주기";
            this.lblNameAutoDeleteCycle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.txtDoorOpenSpeed);
            this.panel32.Controls.Add(this.lblDoorOpenSpeed);
            this.panel32.Controls.Add(this.lblNameDoorOpenSpeed);
            this.panel32.Location = new System.Drawing.Point(11, 512);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(420, 60);
            this.panel32.TabIndex = 465;
            // 
            // txtDoorOpenSpeed
            // 
            this.txtDoorOpenSpeed.Location = new System.Drawing.Point(222, 27);
            this.txtDoorOpenSpeed.Name = "txtDoorOpenSpeed";
            this.txtDoorOpenSpeed.Size = new System.Drawing.Size(192, 23);
            this.txtDoorOpenSpeed.TabIndex = 69;
            // 
            // lblDoorOpenSpeed
            // 
            this.lblDoorOpenSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDoorOpenSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblDoorOpenSpeed.Location = new System.Drawing.Point(6, 28);
            this.lblDoorOpenSpeed.Name = "lblDoorOpenSpeed";
            this.lblDoorOpenSpeed.Size = new System.Drawing.Size(210, 21);
            this.lblDoorOpenSpeed.TabIndex = 65;
            this.lblDoorOpenSpeed.Text = "Door Open Speed[mm/sec] : ";
            this.lblDoorOpenSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNameDoorOpenSpeed
            // 
            this.lblNameDoorOpenSpeed.AutoEllipsis = true;
            this.lblNameDoorOpenSpeed.BackColor = System.Drawing.Color.Gainsboro;
            this.lblNameDoorOpenSpeed.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNameDoorOpenSpeed.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblNameDoorOpenSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblNameDoorOpenSpeed.Location = new System.Drawing.Point(0, 0);
            this.lblNameDoorOpenSpeed.Name = "lblNameDoorOpenSpeed";
            this.lblNameDoorOpenSpeed.Size = new System.Drawing.Size(418, 20);
            this.lblNameDoorOpenSpeed.TabIndex = 9;
            this.lblNameDoorOpenSpeed.Text = "■ 도어 오픈 스피드";
            this.lblNameDoorOpenSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.lblCriticalAlarm);
            this.panel34.Controls.Add(this.txtCriticalAlarm);
            this.panel34.Controls.Add(this.lblBreakingCount);
            this.panel34.Controls.Add(this.lblLightAlarm);
            this.panel34.Controls.Add(this.txtLightAlarm);
            this.panel34.Location = new System.Drawing.Point(11, 578);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(420, 90);
            this.panel34.TabIndex = 466;
            // 
            // lblCriticalAlarm
            // 
            this.lblCriticalAlarm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCriticalAlarm.ForeColor = System.Drawing.Color.Black;
            this.lblCriticalAlarm.Location = new System.Drawing.Point(6, 58);
            this.lblCriticalAlarm.Name = "lblCriticalAlarm";
            this.lblCriticalAlarm.Size = new System.Drawing.Size(210, 21);
            this.lblCriticalAlarm.TabIndex = 70;
            this.lblCriticalAlarm.Text = "중알림[횟수] : ";
            this.lblCriticalAlarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCriticalAlarm
            // 
            this.txtCriticalAlarm.Location = new System.Drawing.Point(222, 57);
            this.txtCriticalAlarm.Name = "txtCriticalAlarm";
            this.txtCriticalAlarm.Size = new System.Drawing.Size(192, 23);
            this.txtCriticalAlarm.TabIndex = 69;
            // 
            // lblBreakingCount
            // 
            this.lblBreakingCount.AutoEllipsis = true;
            this.lblBreakingCount.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBreakingCount.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBreakingCount.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblBreakingCount.ForeColor = System.Drawing.Color.Black;
            this.lblBreakingCount.Location = new System.Drawing.Point(0, 0);
            this.lblBreakingCount.Name = "lblBreakingCount";
            this.lblBreakingCount.Size = new System.Drawing.Size(418, 20);
            this.lblBreakingCount.TabIndex = 9;
            this.lblBreakingCount.Text = "■ 브레이킹 횟수";
            this.lblBreakingCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLightAlarm
            // 
            this.lblLightAlarm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLightAlarm.ForeColor = System.Drawing.Color.Black;
            this.lblLightAlarm.Location = new System.Drawing.Point(6, 29);
            this.lblLightAlarm.Name = "lblLightAlarm";
            this.lblLightAlarm.Size = new System.Drawing.Size(210, 21);
            this.lblLightAlarm.TabIndex = 68;
            this.lblLightAlarm.Text = "경알림[횟수] : ";
            this.lblLightAlarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLightAlarm
            // 
            this.txtLightAlarm.Location = new System.Drawing.Point(222, 28);
            this.txtLightAlarm.Name = "txtLightAlarm";
            this.txtLightAlarm.Size = new System.Drawing.Size(192, 23);
            this.txtLightAlarm.TabIndex = 67;
            // 
            // panel31
            // 
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Controls.Add(this.lblINSP);
            this.panel31.Controls.Add(this.txtINSP);
            this.panel31.Controls.Add(this.lblCountError);
            this.panel31.Controls.Add(this.lblMCR);
            this.panel31.Controls.Add(this.txtMCR);
            this.panel31.Location = new System.Drawing.Point(11, 416);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(420, 90);
            this.panel31.TabIndex = 464;
            // 
            // lblINSP
            // 
            this.lblINSP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblINSP.ForeColor = System.Drawing.Color.Black;
            this.lblINSP.Location = new System.Drawing.Point(6, 58);
            this.lblINSP.Name = "lblINSP";
            this.lblINSP.Size = new System.Drawing.Size(210, 21);
            this.lblINSP.TabIndex = 70;
            this.lblINSP.Text = "INSP :  ";
            this.lblINSP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtINSP
            // 
            this.txtINSP.Location = new System.Drawing.Point(222, 57);
            this.txtINSP.Name = "txtINSP";
            this.txtINSP.Size = new System.Drawing.Size(192, 23);
            this.txtINSP.TabIndex = 69;
            // 
            // lblCountError
            // 
            this.lblCountError.AutoEllipsis = true;
            this.lblCountError.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCountError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCountError.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblCountError.ForeColor = System.Drawing.Color.Black;
            this.lblCountError.Location = new System.Drawing.Point(0, 0);
            this.lblCountError.Name = "lblCountError";
            this.lblCountError.Size = new System.Drawing.Size(418, 20);
            this.lblCountError.TabIndex = 9;
            this.lblCountError.Text = "■ Count Error";
            this.lblCountError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMCR
            // 
            this.lblMCR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMCR.ForeColor = System.Drawing.Color.Black;
            this.lblMCR.Location = new System.Drawing.Point(6, 29);
            this.lblMCR.Name = "lblMCR";
            this.lblMCR.Size = new System.Drawing.Size(210, 21);
            this.lblMCR.TabIndex = 68;
            this.lblMCR.Text = "MCR : ";
            this.lblMCR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMCR
            // 
            this.txtMCR.Location = new System.Drawing.Point(222, 28);
            this.txtMCR.Name = "txtMCR";
            this.txtMCR.Size = new System.Drawing.Size(192, 23);
            this.txtMCR.TabIndex = 67;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.lblMutingTimeout);
            this.panel30.Controls.Add(this.txtMutingTimeout);
            this.panel30.Controls.Add(this.lblSequenceMoveTimeout);
            this.panel30.Controls.Add(this.txtSequenceMoveTimeout);
            this.panel30.Controls.Add(this.lblInspectionTimeout);
            this.panel30.Controls.Add(this.txtInspectiongTimeout);
            this.panel30.Controls.Add(this.lblBreakingAlignTimeout);
            this.panel30.Controls.Add(this.txtBreakingAlignTimeout);
            this.panel30.Controls.Add(this.lblPreAlignTimeout);
            this.panel30.Controls.Add(this.txtPreAlignTimeout);
            this.panel30.Controls.Add(this.lblCylinderTimeout);
            this.panel30.Controls.Add(this.txtCylinderTimeout);
            this.panel30.Controls.Add(this.lblVacuumTimeout);
            this.panel30.Controls.Add(this.txtVacuumTimeout);
            this.panel30.Controls.Add(this.lblTimeout);
            this.panel30.Location = new System.Drawing.Point(11, 154);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(420, 246);
            this.panel30.TabIndex = 459;
            // 
            // lblMutingTimeout
            // 
            this.lblMutingTimeout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMutingTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblMutingTimeout.Location = new System.Drawing.Point(5, 209);
            this.lblMutingTimeout.Name = "lblMutingTimeout";
            this.lblMutingTimeout.Size = new System.Drawing.Size(210, 21);
            this.lblMutingTimeout.TabIndex = 71;
            this.lblMutingTimeout.Text = "Muting Timeout[s] : ";
            this.lblMutingTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMutingTimeout
            // 
            this.txtMutingTimeout.Location = new System.Drawing.Point(221, 208);
            this.txtMutingTimeout.Name = "txtMutingTimeout";
            this.txtMutingTimeout.Size = new System.Drawing.Size(190, 23);
            this.txtMutingTimeout.TabIndex = 70;
            // 
            // lblSequenceMoveTimeout
            // 
            this.lblSequenceMoveTimeout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSequenceMoveTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblSequenceMoveTimeout.Location = new System.Drawing.Point(5, 180);
            this.lblSequenceMoveTimeout.Name = "lblSequenceMoveTimeout";
            this.lblSequenceMoveTimeout.Size = new System.Drawing.Size(210, 21);
            this.lblSequenceMoveTimeout.TabIndex = 69;
            this.lblSequenceMoveTimeout.Text = "Sequence Move Timeout[s] : ";
            this.lblSequenceMoveTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSequenceMoveTimeout
            // 
            this.txtSequenceMoveTimeout.Location = new System.Drawing.Point(221, 179);
            this.txtSequenceMoveTimeout.Name = "txtSequenceMoveTimeout";
            this.txtSequenceMoveTimeout.Size = new System.Drawing.Size(190, 23);
            this.txtSequenceMoveTimeout.TabIndex = 68;
            // 
            // lblInspectionTimeout
            // 
            this.lblInspectionTimeout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspectionTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblInspectionTimeout.Location = new System.Drawing.Point(5, 151);
            this.lblInspectionTimeout.Name = "lblInspectionTimeout";
            this.lblInspectionTimeout.Size = new System.Drawing.Size(210, 21);
            this.lblInspectionTimeout.TabIndex = 67;
            this.lblInspectionTimeout.Text = "Inspection Timeout[s] : ";
            this.lblInspectionTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInspectiongTimeout
            // 
            this.txtInspectiongTimeout.Location = new System.Drawing.Point(221, 150);
            this.txtInspectiongTimeout.Name = "txtInspectiongTimeout";
            this.txtInspectiongTimeout.Size = new System.Drawing.Size(190, 23);
            this.txtInspectiongTimeout.TabIndex = 66;
            // 
            // lblBreakingAlignTimeout
            // 
            this.lblBreakingAlignTimeout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBreakingAlignTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblBreakingAlignTimeout.Location = new System.Drawing.Point(5, 122);
            this.lblBreakingAlignTimeout.Name = "lblBreakingAlignTimeout";
            this.lblBreakingAlignTimeout.Size = new System.Drawing.Size(210, 21);
            this.lblBreakingAlignTimeout.TabIndex = 65;
            this.lblBreakingAlignTimeout.Text = "BreakingAilgn Timeout[s] : ";
            this.lblBreakingAlignTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBreakingAlignTimeout
            // 
            this.txtBreakingAlignTimeout.Location = new System.Drawing.Point(221, 121);
            this.txtBreakingAlignTimeout.Name = "txtBreakingAlignTimeout";
            this.txtBreakingAlignTimeout.Size = new System.Drawing.Size(190, 23);
            this.txtBreakingAlignTimeout.TabIndex = 64;
            // 
            // lblPreAlignTimeout
            // 
            this.lblPreAlignTimeout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPreAlignTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblPreAlignTimeout.Location = new System.Drawing.Point(5, 93);
            this.lblPreAlignTimeout.Name = "lblPreAlignTimeout";
            this.lblPreAlignTimeout.Size = new System.Drawing.Size(210, 21);
            this.lblPreAlignTimeout.TabIndex = 63;
            this.lblPreAlignTimeout.Text = "PreAlign Timeout[s] : ";
            this.lblPreAlignTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPreAlignTimeout
            // 
            this.txtPreAlignTimeout.Location = new System.Drawing.Point(221, 92);
            this.txtPreAlignTimeout.Name = "txtPreAlignTimeout";
            this.txtPreAlignTimeout.Size = new System.Drawing.Size(190, 23);
            this.txtPreAlignTimeout.TabIndex = 62;
            // 
            // lblCylinderTimeout
            // 
            this.lblCylinderTimeout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCylinderTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblCylinderTimeout.Location = new System.Drawing.Point(5, 64);
            this.lblCylinderTimeout.Name = "lblCylinderTimeout";
            this.lblCylinderTimeout.Size = new System.Drawing.Size(210, 21);
            this.lblCylinderTimeout.TabIndex = 61;
            this.lblCylinderTimeout.Text = "Cylinder Timeout[s] : ";
            this.lblCylinderTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCylinderTimeout
            // 
            this.txtCylinderTimeout.Location = new System.Drawing.Point(221, 63);
            this.txtCylinderTimeout.Name = "txtCylinderTimeout";
            this.txtCylinderTimeout.Size = new System.Drawing.Size(190, 23);
            this.txtCylinderTimeout.TabIndex = 60;
            // 
            // lblVacuumTimeout
            // 
            this.lblVacuumTimeout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblVacuumTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblVacuumTimeout.Location = new System.Drawing.Point(5, 35);
            this.lblVacuumTimeout.Name = "lblVacuumTimeout";
            this.lblVacuumTimeout.Size = new System.Drawing.Size(210, 21);
            this.lblVacuumTimeout.TabIndex = 59;
            this.lblVacuumTimeout.Text = "Vacuum Timeout[s] : ";
            this.lblVacuumTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtVacuumTimeout
            // 
            this.txtVacuumTimeout.Location = new System.Drawing.Point(221, 34);
            this.txtVacuumTimeout.Name = "txtVacuumTimeout";
            this.txtVacuumTimeout.Size = new System.Drawing.Size(190, 23);
            this.txtVacuumTimeout.TabIndex = 0;
            // 
            // lblTimeout
            // 
            this.lblTimeout.AutoEllipsis = true;
            this.lblTimeout.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTimeout.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblTimeout.ForeColor = System.Drawing.Color.Black;
            this.lblTimeout.Location = new System.Drawing.Point(0, 0);
            this.lblTimeout.Name = "lblTimeout";
            this.lblTimeout.Size = new System.Drawing.Size(418, 20);
            this.lblTimeout.TabIndex = 9;
            this.lblTimeout.Text = "■ 타임아웃";
            this.lblTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.lblLaserPD7ErrorRange);
            this.panel29.Controls.Add(this.txtLaserPD7ErrorRange);
            this.panel29.Controls.Add(this.lblLaserPD7Error);
            this.panel29.Controls.Add(this.lblLaserPD7Power);
            this.panel29.Controls.Add(this.txtLaserPD7Power);
            this.panel29.Location = new System.Drawing.Point(437, 276);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(420, 80);
            this.panel29.TabIndex = 463;
            // 
            // lblLaserPD7ErrorRange
            // 
            this.lblLaserPD7ErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLaserPD7ErrorRange.ForeColor = System.Drawing.Color.Black;
            this.lblLaserPD7ErrorRange.Location = new System.Drawing.Point(6, 52);
            this.lblLaserPD7ErrorRange.Name = "lblLaserPD7ErrorRange";
            this.lblLaserPD7ErrorRange.Size = new System.Drawing.Size(210, 21);
            this.lblLaserPD7ErrorRange.TabIndex = 70;
            this.lblLaserPD7ErrorRange.Text = "레이져 PD7 오차 범위[%] :  ";
            this.lblLaserPD7ErrorRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLaserPD7ErrorRange
            // 
            this.txtLaserPD7ErrorRange.Location = new System.Drawing.Point(222, 51);
            this.txtLaserPD7ErrorRange.Name = "txtLaserPD7ErrorRange";
            this.txtLaserPD7ErrorRange.Size = new System.Drawing.Size(192, 23);
            this.txtLaserPD7ErrorRange.TabIndex = 69;
            // 
            // lblLaserPD7Error
            // 
            this.lblLaserPD7Error.AutoEllipsis = true;
            this.lblLaserPD7Error.BackColor = System.Drawing.Color.Gainsboro;
            this.lblLaserPD7Error.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLaserPD7Error.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblLaserPD7Error.ForeColor = System.Drawing.Color.Black;
            this.lblLaserPD7Error.Location = new System.Drawing.Point(0, 0);
            this.lblLaserPD7Error.Name = "lblLaserPD7Error";
            this.lblLaserPD7Error.Size = new System.Drawing.Size(418, 20);
            this.lblLaserPD7Error.TabIndex = 9;
            this.lblLaserPD7Error.Text = "■ 레이져 PD7 에러";
            this.lblLaserPD7Error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLaserPD7Power
            // 
            this.lblLaserPD7Power.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLaserPD7Power.ForeColor = System.Drawing.Color.Black;
            this.lblLaserPD7Power.Location = new System.Drawing.Point(6, 25);
            this.lblLaserPD7Power.Name = "lblLaserPD7Power";
            this.lblLaserPD7Power.Size = new System.Drawing.Size(210, 21);
            this.lblLaserPD7Power.TabIndex = 68;
            this.lblLaserPD7Power.Text = "레이져 PD7 파워[w] : ";
            this.lblLaserPD7Power.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLaserPD7Power
            // 
            this.txtLaserPD7Power.Location = new System.Drawing.Point(222, 24);
            this.txtLaserPD7Power.Name = "txtLaserPD7Power";
            this.txtLaserPD7Power.Size = new System.Drawing.Size(192, 23);
            this.txtLaserPD7Power.TabIndex = 67;
            // 
            // panel27
            // 
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.lblNameRetryCount);
            this.panel27.Controls.Add(this.lblRetryCount);
            this.panel27.Controls.Add(this.txtRetryCount);
            this.panel27.Location = new System.Drawing.Point(437, 215);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(420, 55);
            this.panel27.TabIndex = 462;
            // 
            // lblNameRetryCount
            // 
            this.lblNameRetryCount.AutoEllipsis = true;
            this.lblNameRetryCount.BackColor = System.Drawing.Color.Gainsboro;
            this.lblNameRetryCount.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNameRetryCount.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblNameRetryCount.ForeColor = System.Drawing.Color.Black;
            this.lblNameRetryCount.Location = new System.Drawing.Point(0, 0);
            this.lblNameRetryCount.Name = "lblNameRetryCount";
            this.lblNameRetryCount.Size = new System.Drawing.Size(418, 20);
            this.lblNameRetryCount.TabIndex = 9;
            this.lblNameRetryCount.Text = "■ Retry Count";
            this.lblNameRetryCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRetryCount
            // 
            this.lblRetryCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRetryCount.ForeColor = System.Drawing.Color.Black;
            this.lblRetryCount.Location = new System.Drawing.Point(6, 25);
            this.lblRetryCount.Name = "lblRetryCount";
            this.lblRetryCount.Size = new System.Drawing.Size(210, 21);
            this.lblRetryCount.TabIndex = 68;
            this.lblRetryCount.Text = "Retry Count : ";
            this.lblRetryCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRetryCount
            // 
            this.txtRetryCount.Location = new System.Drawing.Point(222, 24);
            this.txtRetryCount.Name = "txtRetryCount";
            this.txtRetryCount.Size = new System.Drawing.Size(192, 23);
            this.txtRetryCount.TabIndex = 67;
            // 
            // panel26
            // 
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel26.Controls.Add(this.lblMeasureCycle);
            this.panel26.Controls.Add(this.lblCutLineMeasure);
            this.panel26.Controls.Add(this.txtCutLineMeasure);
            this.panel26.Location = new System.Drawing.Point(437, 93);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(420, 55);
            this.panel26.TabIndex = 460;
            // 
            // lblMeasureCycle
            // 
            this.lblMeasureCycle.AutoEllipsis = true;
            this.lblMeasureCycle.BackColor = System.Drawing.Color.Gainsboro;
            this.lblMeasureCycle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMeasureCycle.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblMeasureCycle.ForeColor = System.Drawing.Color.Black;
            this.lblMeasureCycle.Location = new System.Drawing.Point(0, 0);
            this.lblMeasureCycle.Name = "lblMeasureCycle";
            this.lblMeasureCycle.Size = new System.Drawing.Size(418, 20);
            this.lblMeasureCycle.TabIndex = 9;
            this.lblMeasureCycle.Text = "■ 측정주기";
            this.lblMeasureCycle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCutLineMeasure
            // 
            this.lblCutLineMeasure.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCutLineMeasure.ForeColor = System.Drawing.Color.Black;
            this.lblCutLineMeasure.Location = new System.Drawing.Point(6, 25);
            this.lblCutLineMeasure.Name = "lblCutLineMeasure";
            this.lblCutLineMeasure.Size = new System.Drawing.Size(210, 21);
            this.lblCutLineMeasure.TabIndex = 68;
            this.lblCutLineMeasure.Text = "컷 라인 주기";
            this.lblCutLineMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCutLineMeasure
            // 
            this.txtCutLineMeasure.Location = new System.Drawing.Point(222, 24);
            this.txtCutLineMeasure.Name = "txtCutLineMeasure";
            this.txtCutLineMeasure.Size = new System.Drawing.Size(192, 23);
            this.txtCutLineMeasure.TabIndex = 67;
            // 
            // panel28
            // 
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.txtBlowingCheck);
            this.panel28.Controls.Add(this.lblBlowingCheck);
            this.panel28.Controls.Add(this.lblNameBlowingCheck);
            this.panel28.Location = new System.Drawing.Point(437, 154);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(420, 55);
            this.panel28.TabIndex = 461;
            // 
            // txtBlowingCheck
            // 
            this.txtBlowingCheck.Location = new System.Drawing.Point(222, 24);
            this.txtBlowingCheck.Name = "txtBlowingCheck";
            this.txtBlowingCheck.Size = new System.Drawing.Size(192, 23);
            this.txtBlowingCheck.TabIndex = 69;
            // 
            // lblBlowingCheck
            // 
            this.lblBlowingCheck.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBlowingCheck.ForeColor = System.Drawing.Color.Black;
            this.lblBlowingCheck.Location = new System.Drawing.Point(6, 25);
            this.lblBlowingCheck.Name = "lblBlowingCheck";
            this.lblBlowingCheck.Size = new System.Drawing.Size(210, 21);
            this.lblBlowingCheck.TabIndex = 65;
            this.lblBlowingCheck.Text = "Blowing 체크 : ";
            this.lblBlowingCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNameBlowingCheck
            // 
            this.lblNameBlowingCheck.AutoEllipsis = true;
            this.lblNameBlowingCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.lblNameBlowingCheck.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNameBlowingCheck.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblNameBlowingCheck.ForeColor = System.Drawing.Color.Black;
            this.lblNameBlowingCheck.Location = new System.Drawing.Point(0, 0);
            this.lblNameBlowingCheck.Name = "lblNameBlowingCheck";
            this.lblNameBlowingCheck.Size = new System.Drawing.Size(418, 20);
            this.lblNameBlowingCheck.TabIndex = 9;
            this.lblNameBlowingCheck.Text = "■ Blowing 체크";
            this.lblNameBlowingCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel47
            // 
            this.panel47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel47.Controls.Add(this.txtTemperatureCriticalAlarm);
            this.panel47.Controls.Add(this.lblTemperatureAlarm);
            this.panel47.Controls.Add(this.txtTemperatureLightAlarm);
            this.panel47.Controls.Add(this.lblNameTemperatureAlarm);
            this.panel47.Location = new System.Drawing.Point(437, 32);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(420, 55);
            this.panel47.TabIndex = 459;
            // 
            // txtTemperatureCriticalAlarm
            // 
            this.txtTemperatureCriticalAlarm.Location = new System.Drawing.Point(321, 24);
            this.txtTemperatureCriticalAlarm.Name = "txtTemperatureCriticalAlarm";
            this.txtTemperatureCriticalAlarm.Size = new System.Drawing.Size(93, 23);
            this.txtTemperatureCriticalAlarm.TabIndex = 66;
            // 
            // lblTemperatureAlarm
            // 
            this.lblTemperatureAlarm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTemperatureAlarm.ForeColor = System.Drawing.Color.Black;
            this.lblTemperatureAlarm.Location = new System.Drawing.Point(6, 25);
            this.lblTemperatureAlarm.Name = "lblTemperatureAlarm";
            this.lblTemperatureAlarm.Size = new System.Drawing.Size(210, 21);
            this.lblTemperatureAlarm.TabIndex = 65;
            this.lblTemperatureAlarm.Text = "경알람 / 중알람[ºC]";
            this.lblTemperatureAlarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTemperatureLightAlarm
            // 
            this.txtTemperatureLightAlarm.Location = new System.Drawing.Point(222, 24);
            this.txtTemperatureLightAlarm.Name = "txtTemperatureLightAlarm";
            this.txtTemperatureLightAlarm.Size = new System.Drawing.Size(93, 23);
            this.txtTemperatureLightAlarm.TabIndex = 64;
            // 
            // lblNameTemperatureAlarm
            // 
            this.lblNameTemperatureAlarm.AutoEllipsis = true;
            this.lblNameTemperatureAlarm.BackColor = System.Drawing.Color.Gainsboro;
            this.lblNameTemperatureAlarm.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNameTemperatureAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblNameTemperatureAlarm.ForeColor = System.Drawing.Color.Black;
            this.lblNameTemperatureAlarm.Location = new System.Drawing.Point(0, 0);
            this.lblNameTemperatureAlarm.Name = "lblNameTemperatureAlarm";
            this.lblNameTemperatureAlarm.Size = new System.Drawing.Size(418, 20);
            this.lblNameTemperatureAlarm.TabIndex = 9;
            this.lblNameTemperatureAlarm.Text = "■ 온도 알림";
            this.lblNameTemperatureAlarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel48
            // 
            this.panel48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel48.Controls.Add(this.lblBreakingAlignSpeed);
            this.panel48.Controls.Add(this.txtBreakingAlignSpeed);
            this.panel48.Controls.Add(this.lblFineAlignSpeed);
            this.panel48.Controls.Add(this.txtFineAlignSpeed);
            this.panel48.Controls.Add(this.lblPreAlignSpeed);
            this.panel48.Controls.Add(this.txtPreAlignSpeed);
            this.panel48.Controls.Add(this.lblSettingDistributionMode);
            this.panel48.Location = new System.Drawing.Point(11, 32);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(420, 116);
            this.panel48.TabIndex = 458;
            // 
            // lblBreakingAlignSpeed
            // 
            this.lblBreakingAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBreakingAlignSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblBreakingAlignSpeed.Location = new System.Drawing.Point(5, 85);
            this.lblBreakingAlignSpeed.Name = "lblBreakingAlignSpeed";
            this.lblBreakingAlignSpeed.Size = new System.Drawing.Size(210, 21);
            this.lblBreakingAlignSpeed.TabIndex = 63;
            this.lblBreakingAlignSpeed.Text = "BreakingAlign Speed[mm/sec] : ";
            this.lblBreakingAlignSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBreakingAlignSpeed
            // 
            this.txtBreakingAlignSpeed.Location = new System.Drawing.Point(221, 84);
            this.txtBreakingAlignSpeed.Name = "txtBreakingAlignSpeed";
            this.txtBreakingAlignSpeed.Size = new System.Drawing.Size(190, 23);
            this.txtBreakingAlignSpeed.TabIndex = 62;
            // 
            // lblFineAlignSpeed
            // 
            this.lblFineAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFineAlignSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblFineAlignSpeed.Location = new System.Drawing.Point(5, 56);
            this.lblFineAlignSpeed.Name = "lblFineAlignSpeed";
            this.lblFineAlignSpeed.Size = new System.Drawing.Size(210, 21);
            this.lblFineAlignSpeed.TabIndex = 61;
            this.lblFineAlignSpeed.Text = "FineAlign Speed[mm/sec] : ";
            this.lblFineAlignSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFineAlignSpeed
            // 
            this.txtFineAlignSpeed.Location = new System.Drawing.Point(221, 55);
            this.txtFineAlignSpeed.Name = "txtFineAlignSpeed";
            this.txtFineAlignSpeed.Size = new System.Drawing.Size(190, 23);
            this.txtFineAlignSpeed.TabIndex = 60;
            // 
            // lblPreAlignSpeed
            // 
            this.lblPreAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPreAlignSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblPreAlignSpeed.Location = new System.Drawing.Point(5, 27);
            this.lblPreAlignSpeed.Name = "lblPreAlignSpeed";
            this.lblPreAlignSpeed.Size = new System.Drawing.Size(210, 21);
            this.lblPreAlignSpeed.TabIndex = 59;
            this.lblPreAlignSpeed.Text = "PreAlign Speed[mm/sec] : ";
            this.lblPreAlignSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPreAlignSpeed
            // 
            this.txtPreAlignSpeed.Location = new System.Drawing.Point(221, 26);
            this.txtPreAlignSpeed.Name = "txtPreAlignSpeed";
            this.txtPreAlignSpeed.Size = new System.Drawing.Size(190, 23);
            this.txtPreAlignSpeed.TabIndex = 0;
            // 
            // lblSettingDistributionMode
            // 
            this.lblSettingDistributionMode.AutoEllipsis = true;
            this.lblSettingDistributionMode.BackColor = System.Drawing.Color.Gainsboro;
            this.lblSettingDistributionMode.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSettingDistributionMode.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblSettingDistributionMode.ForeColor = System.Drawing.Color.Black;
            this.lblSettingDistributionMode.Location = new System.Drawing.Point(0, 0);
            this.lblSettingDistributionMode.Name = "lblSettingDistributionMode";
            this.lblSettingDistributionMode.Size = new System.Drawing.Size(418, 20);
            this.lblSettingDistributionMode.TabIndex = 9;
            this.lblSettingDistributionMode.Text = "■ 물류 모드";
            this.lblSettingDistributionMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSetting
            // 
            this.lblSetting.AutoEllipsis = true;
            this.lblSetting.BackColor = System.Drawing.Color.Gainsboro;
            this.lblSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.lblSetting.ForeColor = System.Drawing.Color.Black;
            this.lblSetting.Location = new System.Drawing.Point(0, 0);
            this.lblSetting.Name = "lblSetting";
            this.lblSetting.Size = new System.Drawing.Size(867, 26);
            this.lblSetting.TabIndex = 9;
            this.lblSetting.Text = "■ Setting";
            this.lblSetting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel37
            // 
            this.panel37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel37.Controls.Add(this.btnInterlockDontUse);
            this.panel37.Controls.Add(this.textBox1);
            this.panel37.Controls.Add(this.btnInterlockUse);
            this.panel37.Controls.Add(this.label1);
            this.panel37.Location = new System.Drawing.Point(425, 650);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(415, 56);
            this.panel37.TabIndex = 475;
            // 
            // btnInterlockDontUse
            // 
            this.btnInterlockDontUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnInterlockDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInterlockDontUse.ForeColor = System.Drawing.Color.Black;
            this.btnInterlockDontUse.Location = new System.Drawing.Point(301, 25);
            this.btnInterlockDontUse.Name = "btnInterlockDontUse";
            this.btnInterlockDontUse.Size = new System.Drawing.Size(106, 23);
            this.btnInterlockDontUse.TabIndex = 47;
            this.btnInterlockDontUse.Text = "사용안함";
            this.btnInterlockDontUse.UseVisualStyleBackColor = false;
            this.btnInterlockDontUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(177, 23);
            this.textBox1.TabIndex = 0;
            // 
            // btnInterlockUse
            // 
            this.btnInterlockUse.BackColor = System.Drawing.SystemColors.Control;
            this.btnInterlockUse.ForeColor = System.Drawing.Color.Black;
            this.btnInterlockUse.Location = new System.Drawing.Point(189, 24);
            this.btnInterlockUse.Name = "btnInterlockUse";
            this.btnInterlockUse.Size = new System.Drawing.Size(106, 23);
            this.btnInterlockUse.TabIndex = 46;
            this.btnInterlockUse.Text = "사용함";
            this.btnInterlockUse.UseVisualStyleBackColor = false;
            this.btnInterlockUse.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(413, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "■  인터락 미사용";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlParameterSystem
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.btnSave);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlParameterSystem";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel13.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDistributionModeDontUse;
        private System.Windows.Forms.TextBox txtDistributionMode;
        private System.Windows.Forms.Button btnDistributionModeUse;
        internal System.Windows.Forms.Label lblDistributionMode;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnSimulationModeDontUse;
        private System.Windows.Forms.TextBox txtSimulationMode;
        private System.Windows.Forms.Button btnSimulationModeUse;
        internal System.Windows.Forms.Label lblSimulationMode;
        internal System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnProcessBSkipDontUse;
        private System.Windows.Forms.TextBox txtProcessBSkip;
        private System.Windows.Forms.Button btnProcessBSkipUse;
        internal System.Windows.Forms.Label lblProcessBSkip;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnProcessASkipDontUse;
        private System.Windows.Forms.TextBox txtProcessASkip;
        private System.Windows.Forms.Button btnProcessASkipUse;
        internal System.Windows.Forms.Label lblProcessASkip;
        internal System.Windows.Forms.Label lblSkipMode;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnBreakingAlignSkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingAlignSkip;
        private System.Windows.Forms.Button btnBreakingAlignSkipUse;
        internal System.Windows.Forms.Label lblBreakingAlignSkip;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnInspectionSkipDontUse;
        private System.Windows.Forms.TextBox txtInspectionSkip;
        private System.Windows.Forms.Button btnInspectionSkipUse;
        internal System.Windows.Forms.Label lblInspectionSkip;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnBreakingMotionSkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingMotionSkip;
        private System.Windows.Forms.Button btnBreakingMotionSkipUse;
        internal System.Windows.Forms.Label lblBreakingMotionSkip;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnLaserSkipDontUse;
        private System.Windows.Forms.TextBox txtLaserSkip;
        private System.Windows.Forms.Button btnLaserSkipUse;
        internal System.Windows.Forms.Label lblLaserSkip;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnBreakingBSkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingBSkip;
        private System.Windows.Forms.Button btnBreakingBSkipUse;
        internal System.Windows.Forms.Label lblBreakingBSkip;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnBreakingASkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingASkip;
        private System.Windows.Forms.Button btnBreakingASkipUse;
        internal System.Windows.Forms.Label lblBreakingASkip;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnTiltMeasureSkipDontUse;
        private System.Windows.Forms.TextBox txtTiltMeasureSkip;
        private System.Windows.Forms.Button btnTiltMeasureSkipUse;
        internal System.Windows.Forms.Label lblTiltMeasureSkip;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnMcrSkipDontUse;
        private System.Windows.Forms.TextBox txtMcrSkip;
        private System.Windows.Forms.Button btnMcrSkipUse;
        internal System.Windows.Forms.Label lblMcrSkip;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnBufferPickerSkipDontUse;
        private System.Windows.Forms.TextBox txtBufferPickerSkip;
        private System.Windows.Forms.Button btnBufferPickerSkipUse;
        internal System.Windows.Forms.Label lblBufferPickerSkip;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btnBcrSkipDontUse;
        private System.Windows.Forms.TextBox txtBcrSkip;
        private System.Windows.Forms.Button btnBcrSkipUse;
        internal System.Windows.Forms.Label lblBcrSkip;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnLoaderInputSkipADontUse;
        private System.Windows.Forms.TextBox txtLoaderInputSkipA;
        private System.Windows.Forms.Button btnLoaderInputSkipAUse;
        internal System.Windows.Forms.Label lblLoaderInputSkip;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnUnloaderInputSkipBDontUse;
        private System.Windows.Forms.TextBox txtUnloaderInputSkipB;
        private System.Windows.Forms.Button btnUnloaderInputSkipBUse;
        private System.Windows.Forms.Button btnUnloaderInputSkipADontUse;
        private System.Windows.Forms.TextBox txtUnloaderInputSkipA;
        private System.Windows.Forms.Button btnUnloaderInputSkipAUse;
        internal System.Windows.Forms.Label lblUnloaderInputSkip;
        private System.Windows.Forms.Button btnLoaderInputSkipBDontUse;
        private System.Windows.Forms.TextBox txtLoaderInputSkipB;
        private System.Windows.Forms.Button btnLoaderInputSkipBUse;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnLaserMeasureSkipDontUse;
        private System.Windows.Forms.TextBox txtLaserMeasureSkip;
        private System.Windows.Forms.Button btnLaserMeasureSkipUse;
        internal System.Windows.Forms.Label lblLaserMeasureSkip;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Button btnDoorSkipDontUse;
        private System.Windows.Forms.TextBox txtDoorSkip;
        private System.Windows.Forms.Button btnDoorSkipUse;
        internal System.Windows.Forms.Label lblDoorSkip;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Button btnGrabSwitchSkipDontUse;
        private System.Windows.Forms.TextBox txtGrabSwitchSkip;
        private System.Windows.Forms.Button btnGrabSwitchSkipUse;
        internal System.Windows.Forms.Label lblGrabSwitchSkip;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button btnMcDownSkipDontUse;
        private System.Windows.Forms.TextBox txtMcDownSkip;
        private System.Windows.Forms.Button btnMcDownSkipUse;
        internal System.Windows.Forms.Label lblMcDownSkip;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnTeachSkipDontUse;
        private System.Windows.Forms.TextBox txtTeachSkip;
        private System.Windows.Forms.Button btnTeachSkipUse;
        internal System.Windows.Forms.Label lblTeachSkip;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button btnMutingSkipDontUse;
        private System.Windows.Forms.TextBox txtMutingSkip;
        private System.Windows.Forms.Button btnMutingSkipUse;
        internal System.Windows.Forms.Label lblMutingSkip;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel47;
        internal System.Windows.Forms.Label lblNameTemperatureAlarm;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.TextBox txtPreAlignSpeed;
        internal System.Windows.Forms.Label lblSettingDistributionMode;
        internal System.Windows.Forms.Label lblSetting;
        private System.Windows.Forms.Label lblPreAlignSpeed;
        private System.Windows.Forms.Label lblFineAlignSpeed;
        private System.Windows.Forms.TextBox txtFineAlignSpeed;
        private System.Windows.Forms.Panel panel26;
        internal System.Windows.Forms.Label lblMeasureCycle;
        private System.Windows.Forms.Label lblBreakingAlignSpeed;
        private System.Windows.Forms.TextBox txtBreakingAlignSpeed;
        private System.Windows.Forms.Label lblTemperatureAlarm;
        private System.Windows.Forms.TextBox txtTemperatureLightAlarm;
        private System.Windows.Forms.TextBox txtTemperatureCriticalAlarm;
        private System.Windows.Forms.Label lblCutLineMeasure;
        private System.Windows.Forms.TextBox txtCutLineMeasure;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label lblMutingTimeout;
        private System.Windows.Forms.TextBox txtMutingTimeout;
        private System.Windows.Forms.Label lblSequenceMoveTimeout;
        private System.Windows.Forms.TextBox txtSequenceMoveTimeout;
        private System.Windows.Forms.Label lblInspectionTimeout;
        private System.Windows.Forms.TextBox txtInspectiongTimeout;
        private System.Windows.Forms.Label lblBreakingAlignTimeout;
        private System.Windows.Forms.TextBox txtBreakingAlignTimeout;
        private System.Windows.Forms.Label lblPreAlignTimeout;
        private System.Windows.Forms.TextBox txtPreAlignTimeout;
        private System.Windows.Forms.Label lblCylinderTimeout;
        private System.Windows.Forms.TextBox txtCylinderTimeout;
        private System.Windows.Forms.Label lblVacuumTimeout;
        private System.Windows.Forms.TextBox txtVacuumTimeout;
        internal System.Windows.Forms.Label lblTimeout;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label lblLaserPD7ErrorRange;
        private System.Windows.Forms.TextBox txtLaserPD7ErrorRange;
        internal System.Windows.Forms.Label lblLaserPD7Error;
        private System.Windows.Forms.Label lblLaserPD7Power;
        private System.Windows.Forms.TextBox txtLaserPD7Power;
        private System.Windows.Forms.Panel panel27;
        internal System.Windows.Forms.Label lblNameRetryCount;
        private System.Windows.Forms.Label lblRetryCount;
        private System.Windows.Forms.TextBox txtRetryCount;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.TextBox txtBlowingCheck;
        private System.Windows.Forms.Label lblBlowingCheck;
        internal System.Windows.Forms.Label lblNameBlowingCheck;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label lblErrorRange;
        private System.Windows.Forms.TextBox txtLDSErrorRange;
        private System.Windows.Forms.Label lblLDSMeasureTime;
        private System.Windows.Forms.TextBox txtLDSMeasureTime;
        private System.Windows.Forms.Label lblWaitingTime;
        private System.Windows.Forms.TextBox txtLDSWaitingTime;
        private System.Windows.Forms.Label lblLDSMeasureCycle;
        private System.Windows.Forms.TextBox txtLDSMeasureCycle;
        internal System.Windows.Forms.Label lblLDS;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label lblTargetErrorRange;
        private System.Windows.Forms.TextBox txtTargetErrorRange;
        private System.Windows.Forms.Label lblCorrectionErrorRange;
        private System.Windows.Forms.TextBox txtCorrectionErrorRange;
        private System.Windows.Forms.Label lblMeasureTime;
        private System.Windows.Forms.TextBox txtMeasureTime;
        private System.Windows.Forms.Label lblWarmUpTime;
        private System.Windows.Forms.TextBox txtWarmUpTime;
        private System.Windows.Forms.Label lblTargetPower;
        private System.Windows.Forms.TextBox txtTargetPower;
        private System.Windows.Forms.Label lblTargetPEC;
        private System.Windows.Forms.TextBox txtTargetPEC;
        private System.Windows.Forms.Label lblLaserPowerMeasureMeasureCycle;
        private System.Windows.Forms.TextBox txtMeasureCycle;
        internal System.Windows.Forms.Label lblLaserPowerMeasure;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.TextBox txtAutoDeleteCycle;
        private System.Windows.Forms.Label lblAutoDeleteCycle;
        internal System.Windows.Forms.Label lblNameAutoDeleteCycle;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.TextBox txtDoorOpenSpeed;
        private System.Windows.Forms.Label lblDoorOpenSpeed;
        internal System.Windows.Forms.Label lblNameDoorOpenSpeed;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label lblCriticalAlarm;
        private System.Windows.Forms.TextBox txtCriticalAlarm;
        internal System.Windows.Forms.Label lblBreakingCount;
        private System.Windows.Forms.Label lblLightAlarm;
        private System.Windows.Forms.TextBox txtLightAlarm;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label lblINSP;
        private System.Windows.Forms.TextBox txtINSP;
        internal System.Windows.Forms.Label lblCountError;
        private System.Windows.Forms.Label lblMCR;
        private System.Windows.Forms.TextBox txtMCR;
        private System.Windows.Forms.Label lblLossErrorRateRange;
        private System.Windows.Forms.TextBox txtLossErrorRateRange;
        private System.Windows.Forms.Label lbLossRateErrorRange;
        private System.Windows.Forms.TextBox txtLossRateErrorRange;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Button btnInterlockDontUse;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnInterlockUse;
        internal System.Windows.Forms.Label label1;
    }
}

﻿namespace DIT.TLC.UI.Parameter
{
    partial class ParameterTest
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plCellOutTransfer01 = new System.Windows.Forms.Panel();
            this.gbox_Cellpurge_Move = new System.Windows.Forms.GroupBox();
            this.gbox_Cellpurge_MoveCenter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_MoveCenter_Y2C = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_BBuffer = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_Y2Uld = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_Y1C = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_ABuffer = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_Y1Uld = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_MoveB_X2BC = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveB_X2BW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveB_X1BW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveB_X1BC = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_MoveA_X2AC = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveA_X2AW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveA_X1AW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveA_X1AC = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Cellpurge_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_B_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_B_PneumaticOn = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Cellpurge_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_A_PneumaticOn = new System.Windows.Forms.Button();
            this.plCstLoad01 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_A_BCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_BCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_BCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_BCG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LTSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LTSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCG = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CasseteLoad_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_Z2CellOut = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_Z2Tilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_Z2Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_Z2InWait = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_Z2In = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_T4Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_T3Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_T4Move = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_T3In = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_Z1CellOut = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_Z1Tilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_Z1Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_Z1InWait = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_Z1In = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_T2Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_T1Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_T2Move = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_T1In = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_B_BCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_BCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_BCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_BCG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LTSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LTSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCG = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_A = new System.Windows.Forms.GroupBox();
            this.plAfterIRCutTransfer01 = new System.Windows.Forms.Panel();
            this.gbox_BreakTransfer_Move = new System.Windows.Forms.GroupBox();
            this.gbox_BreakTransfer_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_MoveB_Unload = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_MoveB_Load = new System.Windows.Forms.Button();
            this.gbox_BreakTransfer_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel43 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_MoveA_Unload = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_MoveA_Load = new System.Windows.Forms.Button();
            this.gbox_BreakTransfer_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel42 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_B_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerUp = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerDown = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PneumaticOff = new System.Windows.Forms.Button();
            this.gbox_BreakTransfer_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel41 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_A_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerUp = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerDown = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerPneumaticOff = new System.Windows.Forms.Button();
            this.plUnloaderTransfer01 = new System.Windows.Forms.Panel();
            this.gbox_UnloaderTransfer_Move = new System.Windows.Forms.GroupBox();
            this.gbox_UnloaderTransfer_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel61 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel62 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveB_T40Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T4P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T4M90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T30Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T3P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T3M90Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel63 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveB_BStage = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_BUnloading = new System.Windows.Forms.Button();
            this.gbox_UnloaderTransfer_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel58 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel60 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveA_T20Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T2P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T2M90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T10Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T1P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T1M90Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel59 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveA_AStage = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_AUnloading = new System.Windows.Forms.Button();
            this.gbox_UnloaderTransfer_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_B_PickerDestruction2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDestructionOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDestructionOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDestructionOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDown2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerUp2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDown1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerUp1 = new System.Windows.Forms.Button();
            this.gbox_UnloaderTransfer_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_A_PickerDestructionOff2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDestructionOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDestructionOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDestructionOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDown2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerUp2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDown1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerUp1 = new System.Windows.Forms.Button();
            this.plBreakUnit03 = new System.Windows.Forms.Panel();
            this.gbox_BreakUnitTY_Shutter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel53 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_Shutter_Close = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Shutter_Open = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_DummyBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel52 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_DummyBox_Output = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_DummyBox_Input = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_Move = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel54 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitTY_Theta = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel57 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_Theta_T2Wait = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Theta_T4Wait = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Theta_T1Wait = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Theta_T3Wait = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel56 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_MoveB_Unload = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_MoveB_Load = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel55 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_MoveA_Unload = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_MoveA_Load = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel50 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_B_1Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_1Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_1Pneumatic_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_1Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Pneumatic_Off = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel51 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_A_1Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_1Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_1Pneumatic_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_1Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Pneumatic_Off = new System.Windows.Forms.Button();
            this.plBreakUnit01 = new System.Windows.Forms.Panel();
            this.gbox_BreakUnitXZ_Move = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel45 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel49 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel48 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel47 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel46 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent = new System.Windows.Forms.Button();
            this.plCellLoadTransfer01 = new System.Windows.Forms.Panel();
            this.gbox_CellLoad_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CellLoad_MoveLorUn = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveLorUn_PreAlignMark1 = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_PreAlignMark2 = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_Y1UnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_Y1L = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_X2L = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_X1L = new System.Windows.Forms.Button();
            this.gbox_CellLoad_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveB_BPickerM90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_BPickerP90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_BPicker0Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveB_BPickerUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_BPickerL = new System.Windows.Forms.Button();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveB_X2BStageUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_X1BStageUnL = new System.Windows.Forms.Button();
            this.gbox_CellLoad_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveA_APickerM90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_APickerP90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_APicker0Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveA_APickerUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_APickerL = new System.Windows.Forms.Button();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveA_X2AStageUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_X1AStageUnL = new System.Windows.Forms.Button();
            this.gbox_CellLoad_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_B_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerUp = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerDown = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerPneumaticOff = new System.Windows.Forms.Button();
            this.gbox_CellLoad_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_A_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerUp = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerDown = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerPneumaticOff = new System.Windows.Forms.Button();
            this.plIRCutProcess01 = new System.Windows.Forms.Panel();
            this.gbox_IRCutProcess_Move = new System.Windows.Forms.GroupBox();
            this.gbox_IRCutProcess_RightOffset = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_LeftOffset = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel39 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_MoveB_R1Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_R2Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_RightCellLoad = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_RightCellUnload = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_R1Laser = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_R2Laser = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_MoveA_L1Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_L2Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_LeftCellLoad = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_LeftCellUnload = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_L1Laser = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_L2Laser = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch2On = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Destruction_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.plCellInTransfer01 = new System.Windows.Forms.Panel();
            this.gbox_CameraUnit_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CameraUnit_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel65 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CameraUnit_MoveB_InspectionMove4 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_MoveB_InspectionMove3 = new System.Windows.Forms.Button();
            this.gbox_CameraUnit_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel64 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CameraUnit_MoveA_InspectionMove2 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_MoveA_InspectionMove1 = new System.Windows.Forms.Button();
            this.plCstUnloader01 = new System.Windows.Forms.Panel();
            this.gbox_CellInput_Buffer = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel66 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_Buffer_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PickerUp = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PickerDown = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PneumaticOn = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PneumaticOff = new System.Windows.Forms.Button();
            this.gbox_CellInput_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CellInput_MoveCenter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel69 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_MoveCenter_ABuffer = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveCenter_BBuffer = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveCenter_Y1CstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveCenter_Y2CstMid = new System.Windows.Forms.Button();
            this.gbox_CellInput_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel68 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_MoveB_X2BUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_Y2BUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X2BCstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X2BCstWait = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X1AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_Y1BUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X1BCstMId = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X1BCstWait = new System.Windows.Forms.Button();
            this.gbox_CellInput_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel67 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_MoveA_X2AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_Y2AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X2ACstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X2ACstWait = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X1AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_Y1AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X1ACstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X1ACstWait = new System.Windows.Forms.Button();
            this.gbox_CellInput_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_B_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_CellInput_B_PneumaticOn = new System.Windows.Forms.Button();
            this.gbox_CellInput_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_CellInput_A_PneumaticOn = new System.Windows.Forms.Button();
            this.plCstLoad02 = new System.Windows.Forms.Panel();
            this.gbox_CasseteUnload = new System.Windows.Forms.GroupBox();
            this.btn_CasseteUnload_Muting4 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting2 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting3 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting1 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MutingOut = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MutingIn = new System.Windows.Forms.Button();
            this.plCellInTransfer02 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.plBreakUnit04 = new System.Windows.Forms.Panel();
            this.gbox_BreakUnitTY_Ionizer = new System.Windows.Forms.GroupBox();
            this.btn_BreakUnitTY_DestructionOff = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_IonizerOff = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_DestructionOn = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_IonizerOn = new System.Windows.Forms.Button();
            this.plIRCutProcess02 = new System.Windows.Forms.Panel();
            this.gbox_IRCutProcess_Ionizer = new System.Windows.Forms.GroupBox();
            this.btn_IRCutProcess_Destruction = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_IonizerOff = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_DestructionOn = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_IonizerOn = new System.Windows.Forms.Button();
            this.plCstUnloader02 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.plCellOutTransfer02 = new System.Windows.Forms.Panel();
            this.gbox_CellInput_Ionizer = new System.Windows.Forms.GroupBox();
            this.btn_CellInput_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_IonizerOff = new System.Windows.Forms.Button();
            this.btn_CellInput_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_IonizerOn = new System.Windows.Forms.Button();
            this.plCellOutTransfer01.SuspendLayout();
            this.gbox_Cellpurge_Move.SuspendLayout();
            this.gbox_Cellpurge_MoveCenter.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.gbox_Cellpurge_MoveB.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.gbox_Cellpurge_MoveA.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.gbox_Cellpurge_B.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.gbox_Cellpurge_A.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.plCstLoad01.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbox_CasseteLoad_Move.SuspendLayout();
            this.gbox_CasseteLoad_MoveB.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.gbox_CasseteLoad_MoveA.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.gbox_CasseteLoad_B.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.plAfterIRCutTransfer01.SuspendLayout();
            this.gbox_BreakTransfer_Move.SuspendLayout();
            this.gbox_BreakTransfer_MoveB.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            this.gbox_BreakTransfer_MoveA.SuspendLayout();
            this.tableLayoutPanel43.SuspendLayout();
            this.gbox_BreakTransfer_B.SuspendLayout();
            this.tableLayoutPanel42.SuspendLayout();
            this.gbox_BreakTransfer_A.SuspendLayout();
            this.tableLayoutPanel41.SuspendLayout();
            this.plUnloaderTransfer01.SuspendLayout();
            this.gbox_UnloaderTransfer_Move.SuspendLayout();
            this.gbox_UnloaderTransfer_MoveB.SuspendLayout();
            this.tableLayoutPanel61.SuspendLayout();
            this.tableLayoutPanel62.SuspendLayout();
            this.tableLayoutPanel63.SuspendLayout();
            this.gbox_UnloaderTransfer_MoveA.SuspendLayout();
            this.tableLayoutPanel58.SuspendLayout();
            this.tableLayoutPanel60.SuspendLayout();
            this.tableLayoutPanel59.SuspendLayout();
            this.gbox_UnloaderTransfer_B.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gbox_UnloaderTransfer_A.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.plBreakUnit03.SuspendLayout();
            this.gbox_BreakUnitTY_Shutter.SuspendLayout();
            this.tableLayoutPanel53.SuspendLayout();
            this.gbox_BreakUnitTY_DummyBox.SuspendLayout();
            this.tableLayoutPanel52.SuspendLayout();
            this.gbox_BreakUnitTY_Move.SuspendLayout();
            this.tableLayoutPanel54.SuspendLayout();
            this.gbox_BreakUnitTY_Theta.SuspendLayout();
            this.tableLayoutPanel57.SuspendLayout();
            this.gbox_BreakUnitTY_MoveB.SuspendLayout();
            this.tableLayoutPanel56.SuspendLayout();
            this.gbox_BreakUnitTY_MoveA.SuspendLayout();
            this.tableLayoutPanel55.SuspendLayout();
            this.gbox_BreakUnitTY_B.SuspendLayout();
            this.tableLayoutPanel50.SuspendLayout();
            this.gbox_BreakUnitTY_A.SuspendLayout();
            this.tableLayoutPanel51.SuspendLayout();
            this.plBreakUnit01.SuspendLayout();
            this.gbox_BreakUnitXZ_Move.SuspendLayout();
            this.tableLayoutPanel45.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ4.SuspendLayout();
            this.tableLayoutPanel49.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ3.SuspendLayout();
            this.tableLayoutPanel48.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ2.SuspendLayout();
            this.tableLayoutPanel47.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ1.SuspendLayout();
            this.tableLayoutPanel46.SuspendLayout();
            this.plCellLoadTransfer01.SuspendLayout();
            this.gbox_CellLoad_Move.SuspendLayout();
            this.gbox_CellLoad_MoveLorUn.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.gbox_CellLoad_MoveB.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            this.tableLayoutPanel34.SuspendLayout();
            this.gbox_CellLoad_MoveA.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.gbox_CellLoad_B.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.gbox_CellLoad_A.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.plIRCutProcess01.SuspendLayout();
            this.gbox_IRCutProcess_Move.SuspendLayout();
            this.gbox_IRCutProcess_RightOffset.SuspendLayout();
            this.tableLayoutPanel40.SuspendLayout();
            this.gbox_IRCutProcess_LeftOffset.SuspendLayout();
            this.tableLayoutPanel39.SuspendLayout();
            this.gbox_IRCutProcess_MoveB.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.gbox_IRCutProcess_MoveA.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            this.gbox_IRCutProcess_B.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.gbox_IRCutProcess_A.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.plCellInTransfer01.SuspendLayout();
            this.gbox_CameraUnit_Move.SuspendLayout();
            this.gbox_CameraUnit_MoveB.SuspendLayout();
            this.tableLayoutPanel65.SuspendLayout();
            this.gbox_CameraUnit_MoveA.SuspendLayout();
            this.tableLayoutPanel64.SuspendLayout();
            this.plCstUnloader01.SuspendLayout();
            this.gbox_CellInput_Buffer.SuspendLayout();
            this.tableLayoutPanel66.SuspendLayout();
            this.gbox_CellInput_Move.SuspendLayout();
            this.gbox_CellInput_MoveCenter.SuspendLayout();
            this.tableLayoutPanel69.SuspendLayout();
            this.gbox_CellInput_MoveB.SuspendLayout();
            this.tableLayoutPanel68.SuspendLayout();
            this.gbox_CellInput_MoveA.SuspendLayout();
            this.tableLayoutPanel67.SuspendLayout();
            this.gbox_CellInput_B.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.gbox_CellInput_A.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.plCstLoad02.SuspendLayout();
            this.gbox_CasseteUnload.SuspendLayout();
            this.plCellInTransfer02.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.plBreakUnit04.SuspendLayout();
            this.gbox_BreakUnitTY_Ionizer.SuspendLayout();
            this.plIRCutProcess02.SuspendLayout();
            this.gbox_IRCutProcess_Ionizer.SuspendLayout();
            this.plCstUnloader02.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.plCellOutTransfer02.SuspendLayout();
            this.gbox_CellInput_Ionizer.SuspendLayout();
            this.SuspendLayout();
            // 
            // plCellOutTransfer01
            // 
            this.plCellOutTransfer01.Controls.Add(this.gbox_Cellpurge_Move);
            this.plCellOutTransfer01.Controls.Add(this.gbox_Cellpurge_B);
            this.plCellOutTransfer01.Controls.Add(this.gbox_Cellpurge_A);
            this.plCellOutTransfer01.Location = new System.Drawing.Point(22, 416);
            this.plCellOutTransfer01.Name = "plCellOutTransfer01";
            this.plCellOutTransfer01.Size = new System.Drawing.Size(922, 371);
            this.plCellOutTransfer01.TabIndex = 72;
            // 
            // gbox_Cellpurge_Move
            // 
            this.gbox_Cellpurge_Move.Controls.Add(this.gbox_Cellpurge_MoveCenter);
            this.gbox_Cellpurge_Move.Controls.Add(this.gbox_Cellpurge_MoveB);
            this.gbox_Cellpurge_Move.Controls.Add(this.gbox_Cellpurge_MoveA);
            this.gbox_Cellpurge_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_Move.Location = new System.Drawing.Point(9, 188);
            this.gbox_Cellpurge_Move.Name = "gbox_Cellpurge_Move";
            this.gbox_Cellpurge_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_Cellpurge_Move.TabIndex = 28;
            this.gbox_Cellpurge_Move.TabStop = false;
            this.gbox_Cellpurge_Move.Text = "     이동     ";
            // 
            // gbox_Cellpurge_MoveCenter
            // 
            this.gbox_Cellpurge_MoveCenter.Controls.Add(this.tableLayoutPanel21);
            this.gbox_Cellpurge_MoveCenter.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_MoveCenter.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_MoveCenter.Location = new System.Drawing.Point(604, 28);
            this.gbox_Cellpurge_MoveCenter.Name = "gbox_Cellpurge_MoveCenter";
            this.gbox_Cellpurge_MoveCenter.Size = new System.Drawing.Size(293, 145);
            this.gbox_Cellpurge_MoveCenter.TabIndex = 33;
            this.gbox_Cellpurge_MoveCenter.TabStop = false;
            this.gbox_Cellpurge_MoveCenter.Text = "     중간, 언로딩    ";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 3;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y2C, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_BBuffer, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y2Uld, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y1C, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_ABuffer, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y1Uld, 2, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel21.TabIndex = 32;
            // 
            // btn_Cellpurge_MoveCenter_Y2C
            // 
            this.btn_Cellpurge_MoveCenter_Y2C.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y2C.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y2C.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y2C.Location = new System.Drawing.Point(3, 58);
            this.btn_Cellpurge_MoveCenter_Y2C.Name = "btn_Cellpurge_MoveCenter_Y2C";
            this.btn_Cellpurge_MoveCenter_Y2C.Size = new System.Drawing.Size(87, 50);
            this.btn_Cellpurge_MoveCenter_Y2C.TabIndex = 36;
            this.btn_Cellpurge_MoveCenter_Y2C.Text = "Y2\r\n카세트\r\n중간";
            this.btn_Cellpurge_MoveCenter_Y2C.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_BBuffer
            // 
            this.btn_Cellpurge_MoveCenter_BBuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_BBuffer.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_BBuffer.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_BBuffer.Location = new System.Drawing.Point(96, 58);
            this.btn_Cellpurge_MoveCenter_BBuffer.Name = "btn_Cellpurge_MoveCenter_BBuffer";
            this.btn_Cellpurge_MoveCenter_BBuffer.Size = new System.Drawing.Size(87, 50);
            this.btn_Cellpurge_MoveCenter_BBuffer.TabIndex = 37;
            this.btn_Cellpurge_MoveCenter_BBuffer.Text = "B\r\nBUFFER";
            this.btn_Cellpurge_MoveCenter_BBuffer.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_Y2Uld
            // 
            this.btn_Cellpurge_MoveCenter_Y2Uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y2Uld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y2Uld.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y2Uld.Location = new System.Drawing.Point(189, 58);
            this.btn_Cellpurge_MoveCenter_Y2Uld.Name = "btn_Cellpurge_MoveCenter_Y2Uld";
            this.btn_Cellpurge_MoveCenter_Y2Uld.Size = new System.Drawing.Size(87, 50);
            this.btn_Cellpurge_MoveCenter_Y2Uld.TabIndex = 38;
            this.btn_Cellpurge_MoveCenter_Y2Uld.Text = "Y2\r\n언로딩 이재기";
            this.btn_Cellpurge_MoveCenter_Y2Uld.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_Y1C
            // 
            this.btn_Cellpurge_MoveCenter_Y1C.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y1C.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y1C.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y1C.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_MoveCenter_Y1C.Name = "btn_Cellpurge_MoveCenter_Y1C";
            this.btn_Cellpurge_MoveCenter_Y1C.Size = new System.Drawing.Size(87, 49);
            this.btn_Cellpurge_MoveCenter_Y1C.TabIndex = 33;
            this.btn_Cellpurge_MoveCenter_Y1C.Text = "Y1\r\n카세트\r\n중간";
            this.btn_Cellpurge_MoveCenter_Y1C.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_ABuffer
            // 
            this.btn_Cellpurge_MoveCenter_ABuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_ABuffer.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_ABuffer.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_ABuffer.Location = new System.Drawing.Point(96, 3);
            this.btn_Cellpurge_MoveCenter_ABuffer.Name = "btn_Cellpurge_MoveCenter_ABuffer";
            this.btn_Cellpurge_MoveCenter_ABuffer.Size = new System.Drawing.Size(87, 49);
            this.btn_Cellpurge_MoveCenter_ABuffer.TabIndex = 34;
            this.btn_Cellpurge_MoveCenter_ABuffer.Text = "A\r\nBUFFER";
            this.btn_Cellpurge_MoveCenter_ABuffer.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_Y1Uld
            // 
            this.btn_Cellpurge_MoveCenter_Y1Uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y1Uld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y1Uld.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y1Uld.Location = new System.Drawing.Point(189, 3);
            this.btn_Cellpurge_MoveCenter_Y1Uld.Name = "btn_Cellpurge_MoveCenter_Y1Uld";
            this.btn_Cellpurge_MoveCenter_Y1Uld.Size = new System.Drawing.Size(89, 49);
            this.btn_Cellpurge_MoveCenter_Y1Uld.TabIndex = 35;
            this.btn_Cellpurge_MoveCenter_Y1Uld.Text = "Y1\r\n언로딩 이재기";
            this.btn_Cellpurge_MoveCenter_Y1Uld.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_MoveB
            // 
            this.gbox_Cellpurge_MoveB.Controls.Add(this.tableLayoutPanel20);
            this.gbox_Cellpurge_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_MoveB.Location = new System.Drawing.Point(305, 28);
            this.gbox_Cellpurge_MoveB.Name = "gbox_Cellpurge_MoveB";
            this.gbox_Cellpurge_MoveB.Size = new System.Drawing.Size(293, 145);
            this.gbox_Cellpurge_MoveB.TabIndex = 29;
            this.gbox_Cellpurge_MoveB.TabStop = false;
            this.gbox_Cellpurge_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X2BC, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X2BW, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X1BW, 1, 0);
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X1BC, 0, 0);
            this.tableLayoutPanel20.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel20.TabIndex = 56;
            // 
            // btn_Cellpurge_MoveB_X2BC
            // 
            this.btn_Cellpurge_MoveB_X2BC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X2BC.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X2BC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X2BC.Location = new System.Drawing.Point(3, 58);
            this.btn_Cellpurge_MoveB_X2BC.Name = "btn_Cellpurge_MoveB_X2BC";
            this.btn_Cellpurge_MoveB_X2BC.Size = new System.Drawing.Size(134, 50);
            this.btn_Cellpurge_MoveB_X2BC.TabIndex = 31;
            this.btn_Cellpurge_MoveB_X2BC.Text = "X2\r\nB 카세트\r\n중간";
            this.btn_Cellpurge_MoveB_X2BC.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveB_X2BW
            // 
            this.btn_Cellpurge_MoveB_X2BW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X2BW.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X2BW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X2BW.Location = new System.Drawing.Point(143, 58);
            this.btn_Cellpurge_MoveB_X2BW.Name = "btn_Cellpurge_MoveB_X2BW";
            this.btn_Cellpurge_MoveB_X2BW.Size = new System.Drawing.Size(135, 50);
            this.btn_Cellpurge_MoveB_X2BW.TabIndex = 32;
            this.btn_Cellpurge_MoveB_X2BW.Text = "X2\r\nB 카세트\r\n대기";
            this.btn_Cellpurge_MoveB_X2BW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveB_X1BW
            // 
            this.btn_Cellpurge_MoveB_X1BW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X1BW.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X1BW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X1BW.Location = new System.Drawing.Point(143, 3);
            this.btn_Cellpurge_MoveB_X1BW.Name = "btn_Cellpurge_MoveB_X1BW";
            this.btn_Cellpurge_MoveB_X1BW.Size = new System.Drawing.Size(135, 49);
            this.btn_Cellpurge_MoveB_X1BW.TabIndex = 30;
            this.btn_Cellpurge_MoveB_X1BW.Text = "X1\r\nB 카세트\r\n대기";
            this.btn_Cellpurge_MoveB_X1BW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveB_X1BC
            // 
            this.btn_Cellpurge_MoveB_X1BC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X1BC.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X1BC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X1BC.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_MoveB_X1BC.Name = "btn_Cellpurge_MoveB_X1BC";
            this.btn_Cellpurge_MoveB_X1BC.Size = new System.Drawing.Size(134, 49);
            this.btn_Cellpurge_MoveB_X1BC.TabIndex = 29;
            this.btn_Cellpurge_MoveB_X1BC.Text = "X1\r\nB 카세트\r\n중간";
            this.btn_Cellpurge_MoveB_X1BC.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_MoveA
            // 
            this.gbox_Cellpurge_MoveA.Controls.Add(this.tableLayoutPanel19);
            this.gbox_Cellpurge_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_Cellpurge_MoveA.Name = "gbox_Cellpurge_MoveA";
            this.gbox_Cellpurge_MoveA.Size = new System.Drawing.Size(293, 145);
            this.gbox_Cellpurge_MoveA.TabIndex = 25;
            this.gbox_Cellpurge_MoveA.TabStop = false;
            this.gbox_Cellpurge_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X2AC, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X2AW, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X1AW, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X1AC, 0, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel19.TabIndex = 55;
            // 
            // btn_Cellpurge_MoveA_X2AC
            // 
            this.btn_Cellpurge_MoveA_X2AC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X2AC.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X2AC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X2AC.Location = new System.Drawing.Point(3, 58);
            this.btn_Cellpurge_MoveA_X2AC.Name = "btn_Cellpurge_MoveA_X2AC";
            this.btn_Cellpurge_MoveA_X2AC.Size = new System.Drawing.Size(134, 50);
            this.btn_Cellpurge_MoveA_X2AC.TabIndex = 27;
            this.btn_Cellpurge_MoveA_X2AC.Text = "X2\r\nA 카세트\r\n중간";
            this.btn_Cellpurge_MoveA_X2AC.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveA_X2AW
            // 
            this.btn_Cellpurge_MoveA_X2AW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X2AW.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X2AW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X2AW.Location = new System.Drawing.Point(143, 58);
            this.btn_Cellpurge_MoveA_X2AW.Name = "btn_Cellpurge_MoveA_X2AW";
            this.btn_Cellpurge_MoveA_X2AW.Size = new System.Drawing.Size(135, 50);
            this.btn_Cellpurge_MoveA_X2AW.TabIndex = 28;
            this.btn_Cellpurge_MoveA_X2AW.Text = "X2\r\nA 카세트\r\n대기";
            this.btn_Cellpurge_MoveA_X2AW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveA_X1AW
            // 
            this.btn_Cellpurge_MoveA_X1AW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X1AW.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X1AW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X1AW.Location = new System.Drawing.Point(143, 3);
            this.btn_Cellpurge_MoveA_X1AW.Name = "btn_Cellpurge_MoveA_X1AW";
            this.btn_Cellpurge_MoveA_X1AW.Size = new System.Drawing.Size(135, 49);
            this.btn_Cellpurge_MoveA_X1AW.TabIndex = 26;
            this.btn_Cellpurge_MoveA_X1AW.Text = "X1\r\nA 카세트\r\n대기";
            this.btn_Cellpurge_MoveA_X1AW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveA_X1AC
            // 
            this.btn_Cellpurge_MoveA_X1AC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X1AC.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X1AC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X1AC.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_MoveA_X1AC.Name = "btn_Cellpurge_MoveA_X1AC";
            this.btn_Cellpurge_MoveA_X1AC.Size = new System.Drawing.Size(134, 49);
            this.btn_Cellpurge_MoveA_X1AC.TabIndex = 25;
            this.btn_Cellpurge_MoveA_X1AC.Text = "X1\r\nA 카세트\r\n중간";
            this.btn_Cellpurge_MoveA_X1AC.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_B
            // 
            this.gbox_Cellpurge_B.Controls.Add(this.tableLayoutPanel18);
            this.gbox_Cellpurge_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_B.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_B.Location = new System.Drawing.Point(464, 3);
            this.gbox_Cellpurge_B.Name = "gbox_Cellpurge_B";
            this.gbox_Cellpurge_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_Cellpurge_B.TabIndex = 27;
            this.gbox_Cellpurge_B.TabStop = false;
            this.gbox_Cellpurge_B.Text = "     B     ";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_DestructionOn, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_DestructionOff, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_PneumaticOff, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_PneumaticOn, 0, 0);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel18.TabIndex = 55;
            // 
            // btn_Cellpurge_B_DestructionOn
            // 
            this.btn_Cellpurge_B_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_DestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_Cellpurge_B_DestructionOn.Name = "btn_Cellpurge_B_DestructionOn";
            this.btn_Cellpurge_B_DestructionOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_DestructionOn.TabIndex = 23;
            this.btn_Cellpurge_B_DestructionOn.Text = "파기 온";
            this.btn_Cellpurge_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_B_DestructionOff
            // 
            this.btn_Cellpurge_B_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_DestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_DestructionOff.Location = new System.Drawing.Point(221, 75);
            this.btn_Cellpurge_B_DestructionOff.Name = "btn_Cellpurge_B_DestructionOff";
            this.btn_Cellpurge_B_DestructionOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_DestructionOff.TabIndex = 24;
            this.btn_Cellpurge_B_DestructionOff.Text = "파기 오프";
            this.btn_Cellpurge_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_B_PneumaticOff
            // 
            this.btn_Cellpurge_B_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_PneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_PneumaticOff.Location = new System.Drawing.Point(221, 3);
            this.btn_Cellpurge_B_PneumaticOff.Name = "btn_Cellpurge_B_PneumaticOff";
            this.btn_Cellpurge_B_PneumaticOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_PneumaticOff.TabIndex = 22;
            this.btn_Cellpurge_B_PneumaticOff.Text = "공압 오프";
            this.btn_Cellpurge_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_B_PneumaticOn
            // 
            this.btn_Cellpurge_B_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_PneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_B_PneumaticOn.Name = "btn_Cellpurge_B_PneumaticOn";
            this.btn_Cellpurge_B_PneumaticOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_PneumaticOn.TabIndex = 21;
            this.btn_Cellpurge_B_PneumaticOn.Text = "공압 온";
            this.btn_Cellpurge_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_A
            // 
            this.gbox_Cellpurge_A.Controls.Add(this.tableLayoutPanel17);
            this.gbox_Cellpurge_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_A.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_A.Location = new System.Drawing.Point(9, 3);
            this.gbox_Cellpurge_A.Name = "gbox_Cellpurge_A";
            this.gbox_Cellpurge_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_Cellpurge_A.TabIndex = 26;
            this.gbox_Cellpurge_A.TabStop = false;
            this.gbox_Cellpurge_A.Text = "     A    ";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_DestructionOn, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_DestructionOff, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_PneumaticOff, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_PneumaticOn, 0, 0);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel17.TabIndex = 54;
            // 
            // btn_Cellpurge_A_DestructionOn
            // 
            this.btn_Cellpurge_A_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_DestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_Cellpurge_A_DestructionOn.Name = "btn_Cellpurge_A_DestructionOn";
            this.btn_Cellpurge_A_DestructionOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_DestructionOn.TabIndex = 19;
            this.btn_Cellpurge_A_DestructionOn.Text = "파기 온";
            this.btn_Cellpurge_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_A_DestructionOff
            // 
            this.btn_Cellpurge_A_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_DestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_DestructionOff.Location = new System.Drawing.Point(221, 75);
            this.btn_Cellpurge_A_DestructionOff.Name = "btn_Cellpurge_A_DestructionOff";
            this.btn_Cellpurge_A_DestructionOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_DestructionOff.TabIndex = 20;
            this.btn_Cellpurge_A_DestructionOff.Text = "파기 오프";
            this.btn_Cellpurge_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_A_PneumaticOff
            // 
            this.btn_Cellpurge_A_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_PneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_PneumaticOff.Location = new System.Drawing.Point(221, 3);
            this.btn_Cellpurge_A_PneumaticOff.Name = "btn_Cellpurge_A_PneumaticOff";
            this.btn_Cellpurge_A_PneumaticOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_PneumaticOff.TabIndex = 18;
            this.btn_Cellpurge_A_PneumaticOff.Text = "공압 오프";
            this.btn_Cellpurge_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_A_PneumaticOn
            // 
            this.btn_Cellpurge_A_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_PneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_A_PneumaticOn.Name = "btn_Cellpurge_A_PneumaticOn";
            this.btn_Cellpurge_A_PneumaticOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_PneumaticOn.TabIndex = 17;
            this.btn_Cellpurge_A_PneumaticOn.Text = "공압 온";
            this.btn_Cellpurge_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // plCstLoad01
            // 
            this.plCstLoad01.Controls.Add(this.tableLayoutPanel1);
            this.plCstLoad01.Controls.Add(this.gbox_CasseteLoad_Move);
            this.plCstLoad01.Controls.Add(this.gbox_CasseteLoad_B);
            this.plCstLoad01.Controls.Add(this.gbox_CasseteLoad_A);
            this.plCstLoad01.Location = new System.Drawing.Point(22, 23);
            this.plCstLoad01.Name = "plCstLoad01";
            this.plCstLoad01.Size = new System.Drawing.Size(922, 373);
            this.plCstLoad01.TabIndex = 73;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCSD, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCSU, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCUG, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCG, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LTSD, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LTSU, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LUG, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LG, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCSD, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCSU, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCUG, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCG, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(17, 30);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel1.TabIndex = 45;
            // 
            // btn_CasseteLoad_A_BCSD
            // 
            this.btn_CasseteLoad_A_BCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCSD.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCSD.Location = new System.Drawing.Point(330, 99);
            this.btn_CasseteLoad_A_BCSD.Name = "btn_CasseteLoad_A_BCSD";
            this.btn_CasseteLoad_A_BCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCSD.TabIndex = 31;
            this.btn_CasseteLoad_A_BCSD.Text = "하단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_A_BCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_BCSU
            // 
            this.btn_CasseteLoad_A_BCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCSU.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCSU.Location = new System.Drawing.Point(221, 99);
            this.btn_CasseteLoad_A_BCSU.Name = "btn_CasseteLoad_A_BCSU";
            this.btn_CasseteLoad_A_BCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCSU.TabIndex = 30;
            this.btn_CasseteLoad_A_BCSU.Text = "하단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_A_BCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_BCUG
            // 
            this.btn_CasseteLoad_A_BCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCUG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCUG.Location = new System.Drawing.Point(112, 99);
            this.btn_CasseteLoad_A_BCUG.Name = "btn_CasseteLoad_A_BCUG";
            this.btn_CasseteLoad_A_BCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCUG.TabIndex = 29;
            this.btn_CasseteLoad_A_BCUG.Text = "하단 카세트\r\n언그립";
            this.btn_CasseteLoad_A_BCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_BCG
            // 
            this.btn_CasseteLoad_A_BCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCG.Location = new System.Drawing.Point(3, 99);
            this.btn_CasseteLoad_A_BCG.Name = "btn_CasseteLoad_A_BCG";
            this.btn_CasseteLoad_A_BCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCG.TabIndex = 28;
            this.btn_CasseteLoad_A_BCG.Text = "하단 카세트\r\n그립";
            this.btn_CasseteLoad_A_BCG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LTSD
            // 
            this.btn_CasseteLoad_A_LTSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LTSD.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LTSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LTSD.Location = new System.Drawing.Point(330, 51);
            this.btn_CasseteLoad_A_LTSD.Name = "btn_CasseteLoad_A_LTSD";
            this.btn_CasseteLoad_A_LTSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LTSD.TabIndex = 27;
            this.btn_CasseteLoad_A_LTSD.Text = "리프트 틸트\r\n실린더 다운";
            this.btn_CasseteLoad_A_LTSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LTSU
            // 
            this.btn_CasseteLoad_A_LTSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LTSU.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LTSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LTSU.Location = new System.Drawing.Point(221, 51);
            this.btn_CasseteLoad_A_LTSU.Name = "btn_CasseteLoad_A_LTSU";
            this.btn_CasseteLoad_A_LTSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LTSU.TabIndex = 26;
            this.btn_CasseteLoad_A_LTSU.Text = "리프트 틸트\r\n실린더 업";
            this.btn_CasseteLoad_A_LTSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LUG
            // 
            this.btn_CasseteLoad_A_LUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LUG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LUG.Location = new System.Drawing.Point(112, 51);
            this.btn_CasseteLoad_A_LUG.Name = "btn_CasseteLoad_A_LUG";
            this.btn_CasseteLoad_A_LUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LUG.TabIndex = 25;
            this.btn_CasseteLoad_A_LUG.Text = "리프트\r\n언그립";
            this.btn_CasseteLoad_A_LUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LG
            // 
            this.btn_CasseteLoad_A_LG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LG.Location = new System.Drawing.Point(3, 51);
            this.btn_CasseteLoad_A_LG.Name = "btn_CasseteLoad_A_LG";
            this.btn_CasseteLoad_A_LG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LG.TabIndex = 24;
            this.btn_CasseteLoad_A_LG.Text = "리프트\r\n그립";
            this.btn_CasseteLoad_A_LG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCSD
            // 
            this.btn_CasseteLoad_A_TCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCSD.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCSD.Location = new System.Drawing.Point(330, 3);
            this.btn_CasseteLoad_A_TCSD.Name = "btn_CasseteLoad_A_TCSD";
            this.btn_CasseteLoad_A_TCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCSD.TabIndex = 23;
            this.btn_CasseteLoad_A_TCSD.Text = "상단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_A_TCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCSU
            // 
            this.btn_CasseteLoad_A_TCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCSU.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCSU.Location = new System.Drawing.Point(221, 3);
            this.btn_CasseteLoad_A_TCSU.Name = "btn_CasseteLoad_A_TCSU";
            this.btn_CasseteLoad_A_TCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCSU.TabIndex = 22;
            this.btn_CasseteLoad_A_TCSU.Text = "상단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_A_TCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCUG
            // 
            this.btn_CasseteLoad_A_TCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCUG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCUG.Location = new System.Drawing.Point(112, 3);
            this.btn_CasseteLoad_A_TCUG.Name = "btn_CasseteLoad_A_TCUG";
            this.btn_CasseteLoad_A_TCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCUG.TabIndex = 21;
            this.btn_CasseteLoad_A_TCUG.Text = "상단 카세트\r\n언그립";
            this.btn_CasseteLoad_A_TCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCG
            // 
            this.btn_CasseteLoad_A_TCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCG.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_A_TCG.Name = "btn_CasseteLoad_A_TCG";
            this.btn_CasseteLoad_A_TCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCG.TabIndex = 20;
            this.btn_CasseteLoad_A_TCG.Text = "상단 카세트\r\n그립";
            this.btn_CasseteLoad_A_TCG.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_Move
            // 
            this.gbox_CasseteLoad_Move.Controls.Add(this.gbox_CasseteLoad_MoveB);
            this.gbox_CasseteLoad_Move.Controls.Add(this.gbox_CasseteLoad_MoveA);
            this.gbox_CasseteLoad_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_Move.Location = new System.Drawing.Point(11, 187);
            this.gbox_CasseteLoad_Move.Name = "gbox_CasseteLoad_Move";
            this.gbox_CasseteLoad_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CasseteLoad_Move.TabIndex = 48;
            this.gbox_CasseteLoad_Move.TabStop = false;
            this.gbox_CasseteLoad_Move.Text = "     이동     ";
            // 
            // gbox_CasseteLoad_MoveB
            // 
            this.gbox_CasseteLoad_MoveB.Controls.Add(this.tableLayoutPanel12);
            this.gbox_CasseteLoad_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_MoveB.Location = new System.Drawing.Point(455, 21);
            this.gbox_CasseteLoad_MoveB.Name = "gbox_CasseteLoad_MoveB";
            this.gbox_CasseteLoad_MoveB.Size = new System.Drawing.Size(443, 152);
            this.gbox_CasseteLoad_MoveB.TabIndex = 53;
            this.gbox_CasseteLoad_MoveB.TabStop = false;
            this.gbox_CasseteLoad_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 4;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel14, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel15, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel16, 0, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(436, 124);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.btn_CasseteLoad_MoveB_Z2CellOut, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.btn_CasseteLoad_MoveB_Z2Tilt, 0, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel13.TabIndex = 3;
            // 
            // btn_CasseteLoad_MoveB_Z2CellOut
            // 
            this.btn_CasseteLoad_MoveB_Z2CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2CellOut.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2CellOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2CellOut.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveB_Z2CellOut.Name = "btn_CasseteLoad_MoveB_Z2CellOut";
            this.btn_CasseteLoad_MoveB_Z2CellOut.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_Z2CellOut.TabIndex = 62;
            this.btn_CasseteLoad_MoveB_Z2CellOut.Text = "리프트 Z2\r\n셀 배출 시작";
            this.btn_CasseteLoad_MoveB_Z2CellOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_Z2Tilt
            // 
            this.btn_CasseteLoad_MoveB_Z2Tilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2Tilt.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2Tilt.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2Tilt.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_Z2Tilt.Name = "btn_CasseteLoad_MoveB_Z2Tilt";
            this.btn_CasseteLoad_MoveB_Z2Tilt.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_Z2Tilt.TabIndex = 57;
            this.btn_CasseteLoad_MoveB_Z2Tilt.Text = "리프트 Z2\r\n틸트 측정";
            this.btn_CasseteLoad_MoveB_Z2Tilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.btn_CasseteLoad_MoveB_Z2Out, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.btn_CasseteLoad_MoveB_Z2InWait, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.btn_CasseteLoad_MoveB_Z2In, 0, 1);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel14.TabIndex = 2;
            // 
            // btn_CasseteLoad_MoveB_Z2Out
            // 
            this.btn_CasseteLoad_MoveB_Z2Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2Out.Font = new System.Drawing.Font("Malgun Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2Out.Location = new System.Drawing.Point(3, 81);
            this.btn_CasseteLoad_MoveB_Z2Out.Name = "btn_CasseteLoad_MoveB_Z2Out";
            this.btn_CasseteLoad_MoveB_Z2Out.Size = new System.Drawing.Size(97, 34);
            this.btn_CasseteLoad_MoveB_Z2Out.TabIndex = 61;
            this.btn_CasseteLoad_MoveB_Z2Out.Text = "리프트 Z2\r\n카세트 배출";
            this.btn_CasseteLoad_MoveB_Z2Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_Z2InWait
            // 
            this.btn_CasseteLoad_MoveB_Z2InWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2InWait.Font = new System.Drawing.Font("Malgun Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2InWait.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2InWait.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_Z2InWait.Name = "btn_CasseteLoad_MoveB_Z2InWait";
            this.btn_CasseteLoad_MoveB_Z2InWait.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveB_Z2InWait.TabIndex = 56;
            this.btn_CasseteLoad_MoveB_Z2InWait.Text = "카세트 Z2\r\n투입 대기";
            this.btn_CasseteLoad_MoveB_Z2InWait.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_Z2In
            // 
            this.btn_CasseteLoad_MoveB_Z2In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2In.Font = new System.Drawing.Font("Malgun Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2In.Location = new System.Drawing.Point(3, 42);
            this.btn_CasseteLoad_MoveB_Z2In.Name = "btn_CasseteLoad_MoveB_Z2In";
            this.btn_CasseteLoad_MoveB_Z2In.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveB_Z2In.TabIndex = 60;
            this.btn_CasseteLoad_MoveB_Z2In.Text = "카세트 Z2\r\n카세트 투입";
            this.btn_CasseteLoad_MoveB_Z2In.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.btn_CasseteLoad_MoveB_T4Out, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.btn_CasseteLoad_MoveB_T3Move, 0, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // btn_CasseteLoad_MoveB_T4Out
            // 
            this.btn_CasseteLoad_MoveB_T4Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T4Out.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T4Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T4Out.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveB_T4Out.Name = "btn_CasseteLoad_MoveB_T4Out";
            this.btn_CasseteLoad_MoveB_T4Out.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T4Out.TabIndex = 59;
            this.btn_CasseteLoad_MoveB_T4Out.Text = "하부 카세트 T4\r\n카세트 배출";
            this.btn_CasseteLoad_MoveB_T4Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_T3Move
            // 
            this.btn_CasseteLoad_MoveB_T3Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T3Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T3Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T3Move.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_T3Move.Name = "btn_CasseteLoad_MoveB_T3Move";
            this.btn_CasseteLoad_MoveB_T3Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T3Move.TabIndex = 55;
            this.btn_CasseteLoad_MoveB_T3Move.Text = "상부 카세트 T3\r\n리프트 이동";
            this.btn_CasseteLoad_MoveB_T3Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.btn_CasseteLoad_MoveB_T4Move, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.btn_CasseteLoad_MoveB_T3In, 0, 0);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // btn_CasseteLoad_MoveB_T4Move
            // 
            this.btn_CasseteLoad_MoveB_T4Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T4Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T4Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T4Move.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveB_T4Move.Name = "btn_CasseteLoad_MoveB_T4Move";
            this.btn_CasseteLoad_MoveB_T4Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T4Move.TabIndex = 58;
            this.btn_CasseteLoad_MoveB_T4Move.Text = "하부 카세트 T4\r\n카세트 이동";
            this.btn_CasseteLoad_MoveB_T4Move.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_T3In
            // 
            this.btn_CasseteLoad_MoveB_T3In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T3In.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T3In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T3In.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_T3In.Name = "btn_CasseteLoad_MoveB_T3In";
            this.btn_CasseteLoad_MoveB_T3In.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T3In.TabIndex = 54;
            this.btn_CasseteLoad_MoveB_T3In.Text = "상부 카세트 T3\r\n카세트 투입";
            this.btn_CasseteLoad_MoveB_T3In.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_MoveA
            // 
            this.gbox_CasseteLoad_MoveA.Controls.Add(this.tableLayoutPanel7);
            this.gbox_CasseteLoad_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_MoveA.Location = new System.Drawing.Point(6, 21);
            this.gbox_CasseteLoad_MoveA.Name = "gbox_CasseteLoad_MoveA";
            this.gbox_CasseteLoad_MoveA.Size = new System.Drawing.Size(443, 152);
            this.gbox_CasseteLoad_MoveA.TabIndex = 44;
            this.gbox_CasseteLoad_MoveA.TabStop = false;
            this.gbox_CasseteLoad_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel11, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel10, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(437, 124);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.btn_CasseteLoad_MoveA_Z1CellOut, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.btn_CasseteLoad_MoveA_Z1Tilt, 0, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel11.TabIndex = 3;
            // 
            // btn_CasseteLoad_MoveA_Z1CellOut
            // 
            this.btn_CasseteLoad_MoveA_Z1CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1CellOut.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1CellOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1CellOut.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveA_Z1CellOut.Name = "btn_CasseteLoad_MoveA_Z1CellOut";
            this.btn_CasseteLoad_MoveA_Z1CellOut.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_Z1CellOut.TabIndex = 52;
            this.btn_CasseteLoad_MoveA_Z1CellOut.Text = "리프트 Z1\r\n셀 배출 시작";
            this.btn_CasseteLoad_MoveA_Z1CellOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_Z1Tilt
            // 
            this.btn_CasseteLoad_MoveA_Z1Tilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1Tilt.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1Tilt.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1Tilt.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_Z1Tilt.Name = "btn_CasseteLoad_MoveA_Z1Tilt";
            this.btn_CasseteLoad_MoveA_Z1Tilt.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_Z1Tilt.TabIndex = 47;
            this.btn_CasseteLoad_MoveA_Z1Tilt.Text = "리프트 Z1\r\n틸트 측정";
            this.btn_CasseteLoad_MoveA_Z1Tilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.btn_CasseteLoad_MoveA_Z1Out, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.btn_CasseteLoad_MoveA_Z1InWait, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.btn_CasseteLoad_MoveA_Z1In, 0, 1);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel10.TabIndex = 2;
            // 
            // btn_CasseteLoad_MoveA_Z1Out
            // 
            this.btn_CasseteLoad_MoveA_Z1Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1Out.Font = new System.Drawing.Font("Malgun Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1Out.Location = new System.Drawing.Point(3, 81);
            this.btn_CasseteLoad_MoveA_Z1Out.Name = "btn_CasseteLoad_MoveA_Z1Out";
            this.btn_CasseteLoad_MoveA_Z1Out.Size = new System.Drawing.Size(97, 34);
            this.btn_CasseteLoad_MoveA_Z1Out.TabIndex = 51;
            this.btn_CasseteLoad_MoveA_Z1Out.Text = "리프트 Z1\r\n카세트 배출";
            this.btn_CasseteLoad_MoveA_Z1Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_Z1InWait
            // 
            this.btn_CasseteLoad_MoveA_Z1InWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1InWait.Font = new System.Drawing.Font("Malgun Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1InWait.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1InWait.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_Z1InWait.Name = "btn_CasseteLoad_MoveA_Z1InWait";
            this.btn_CasseteLoad_MoveA_Z1InWait.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveA_Z1InWait.TabIndex = 46;
            this.btn_CasseteLoad_MoveA_Z1InWait.Text = "카세트 Z1\r\n투입 대기";
            this.btn_CasseteLoad_MoveA_Z1InWait.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_Z1In
            // 
            this.btn_CasseteLoad_MoveA_Z1In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1In.Font = new System.Drawing.Font("Malgun Gothic", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1In.Location = new System.Drawing.Point(3, 42);
            this.btn_CasseteLoad_MoveA_Z1In.Name = "btn_CasseteLoad_MoveA_Z1In";
            this.btn_CasseteLoad_MoveA_Z1In.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveA_Z1In.TabIndex = 50;
            this.btn_CasseteLoad_MoveA_Z1In.Text = "카세트 Z1\r\n카세트 투입";
            this.btn_CasseteLoad_MoveA_Z1In.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.btn_CasseteLoad_MoveA_T2Out, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.btn_CasseteLoad_MoveA_T1Move, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btn_CasseteLoad_MoveA_T2Out
            // 
            this.btn_CasseteLoad_MoveA_T2Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T2Out.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T2Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T2Out.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveA_T2Out.Name = "btn_CasseteLoad_MoveA_T2Out";
            this.btn_CasseteLoad_MoveA_T2Out.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T2Out.TabIndex = 49;
            this.btn_CasseteLoad_MoveA_T2Out.Text = "하부 카세트 T2\r\n카세트 배출";
            this.btn_CasseteLoad_MoveA_T2Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_T1Move
            // 
            this.btn_CasseteLoad_MoveA_T1Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T1Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T1Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T1Move.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_T1Move.Name = "btn_CasseteLoad_MoveA_T1Move";
            this.btn_CasseteLoad_MoveA_T1Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T1Move.TabIndex = 45;
            this.btn_CasseteLoad_MoveA_T1Move.Text = "상부 카세트 T1\r\n리프트 이동";
            this.btn_CasseteLoad_MoveA_T1Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.btn_CasseteLoad_MoveA_T2Move, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.btn_CasseteLoad_MoveA_T1In, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // btn_CasseteLoad_MoveA_T2Move
            // 
            this.btn_CasseteLoad_MoveA_T2Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T2Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T2Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T2Move.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveA_T2Move.Name = "btn_CasseteLoad_MoveA_T2Move";
            this.btn_CasseteLoad_MoveA_T2Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T2Move.TabIndex = 48;
            this.btn_CasseteLoad_MoveA_T2Move.Text = "하부 카세트 T2\r\n카세트 이동";
            this.btn_CasseteLoad_MoveA_T2Move.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_T1In
            // 
            this.btn_CasseteLoad_MoveA_T1In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T1In.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T1In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T1In.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_T1In.Name = "btn_CasseteLoad_MoveA_T1In";
            this.btn_CasseteLoad_MoveA_T1In.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T1In.TabIndex = 44;
            this.btn_CasseteLoad_MoveA_T1In.Text = "상부 카세트 T1\r\n카세트 투입";
            this.btn_CasseteLoad_MoveA_T1In.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_B
            // 
            this.gbox_CasseteLoad_B.Controls.Add(this.tableLayoutPanel2);
            this.gbox_CasseteLoad_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_B.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_B.Location = new System.Drawing.Point(466, 2);
            this.gbox_CasseteLoad_B.Name = "gbox_CasseteLoad_B";
            this.gbox_CasseteLoad_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_CasseteLoad_B.TabIndex = 47;
            this.gbox_CasseteLoad_B.TabStop = false;
            this.gbox_CasseteLoad_B.Text = "     B     ";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCSD, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCSU, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCUG, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCG, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LTSD, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LTSU, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LUG, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LG, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCSD, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCSU, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCUG, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCG, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btn_CasseteLoad_B_BCSD
            // 
            this.btn_CasseteLoad_B_BCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCSD.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCSD.Location = new System.Drawing.Point(330, 99);
            this.btn_CasseteLoad_B_BCSD.Name = "btn_CasseteLoad_B_BCSD";
            this.btn_CasseteLoad_B_BCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCSD.TabIndex = 43;
            this.btn_CasseteLoad_B_BCSD.Text = "하단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_B_BCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_BCSU
            // 
            this.btn_CasseteLoad_B_BCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCSU.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCSU.Location = new System.Drawing.Point(221, 99);
            this.btn_CasseteLoad_B_BCSU.Name = "btn_CasseteLoad_B_BCSU";
            this.btn_CasseteLoad_B_BCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCSU.TabIndex = 42;
            this.btn_CasseteLoad_B_BCSU.Text = "하단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_B_BCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_BCUG
            // 
            this.btn_CasseteLoad_B_BCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCUG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCUG.Location = new System.Drawing.Point(112, 99);
            this.btn_CasseteLoad_B_BCUG.Name = "btn_CasseteLoad_B_BCUG";
            this.btn_CasseteLoad_B_BCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCUG.TabIndex = 41;
            this.btn_CasseteLoad_B_BCUG.Text = "하단 카세트\r\n언그립";
            this.btn_CasseteLoad_B_BCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_BCG
            // 
            this.btn_CasseteLoad_B_BCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCG.Location = new System.Drawing.Point(3, 99);
            this.btn_CasseteLoad_B_BCG.Name = "btn_CasseteLoad_B_BCG";
            this.btn_CasseteLoad_B_BCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCG.TabIndex = 40;
            this.btn_CasseteLoad_B_BCG.Text = "하단 카세트\r\n그립";
            this.btn_CasseteLoad_B_BCG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LTSD
            // 
            this.btn_CasseteLoad_B_LTSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LTSD.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LTSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LTSD.Location = new System.Drawing.Point(330, 51);
            this.btn_CasseteLoad_B_LTSD.Name = "btn_CasseteLoad_B_LTSD";
            this.btn_CasseteLoad_B_LTSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LTSD.TabIndex = 39;
            this.btn_CasseteLoad_B_LTSD.Text = "리프트 틸트\r\n실린더 다운";
            this.btn_CasseteLoad_B_LTSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LTSU
            // 
            this.btn_CasseteLoad_B_LTSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LTSU.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LTSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LTSU.Location = new System.Drawing.Point(221, 51);
            this.btn_CasseteLoad_B_LTSU.Name = "btn_CasseteLoad_B_LTSU";
            this.btn_CasseteLoad_B_LTSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LTSU.TabIndex = 38;
            this.btn_CasseteLoad_B_LTSU.Text = "리프트 틸트\r\n실린더 업";
            this.btn_CasseteLoad_B_LTSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LUG
            // 
            this.btn_CasseteLoad_B_LUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LUG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LUG.Location = new System.Drawing.Point(112, 51);
            this.btn_CasseteLoad_B_LUG.Name = "btn_CasseteLoad_B_LUG";
            this.btn_CasseteLoad_B_LUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LUG.TabIndex = 37;
            this.btn_CasseteLoad_B_LUG.Text = "리프트\r\n언그립";
            this.btn_CasseteLoad_B_LUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LG
            // 
            this.btn_CasseteLoad_B_LG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LG.Location = new System.Drawing.Point(3, 51);
            this.btn_CasseteLoad_B_LG.Name = "btn_CasseteLoad_B_LG";
            this.btn_CasseteLoad_B_LG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LG.TabIndex = 36;
            this.btn_CasseteLoad_B_LG.Text = "리프트\r\n그립";
            this.btn_CasseteLoad_B_LG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCSD
            // 
            this.btn_CasseteLoad_B_TCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCSD.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCSD.Location = new System.Drawing.Point(330, 3);
            this.btn_CasseteLoad_B_TCSD.Name = "btn_CasseteLoad_B_TCSD";
            this.btn_CasseteLoad_B_TCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCSD.TabIndex = 35;
            this.btn_CasseteLoad_B_TCSD.Text = "상단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_B_TCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCSU
            // 
            this.btn_CasseteLoad_B_TCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCSU.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCSU.Location = new System.Drawing.Point(221, 3);
            this.btn_CasseteLoad_B_TCSU.Name = "btn_CasseteLoad_B_TCSU";
            this.btn_CasseteLoad_B_TCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCSU.TabIndex = 34;
            this.btn_CasseteLoad_B_TCSU.Text = "상단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_B_TCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCUG
            // 
            this.btn_CasseteLoad_B_TCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCUG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCUG.Location = new System.Drawing.Point(112, 3);
            this.btn_CasseteLoad_B_TCUG.Name = "btn_CasseteLoad_B_TCUG";
            this.btn_CasseteLoad_B_TCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCUG.TabIndex = 33;
            this.btn_CasseteLoad_B_TCUG.Text = "상단 카세트\r\n언그립";
            this.btn_CasseteLoad_B_TCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCG
            // 
            this.btn_CasseteLoad_B_TCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCG.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCG.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_B_TCG.Name = "btn_CasseteLoad_B_TCG";
            this.btn_CasseteLoad_B_TCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCG.TabIndex = 32;
            this.btn_CasseteLoad_B_TCG.Text = "상단 카세트\r\n그립";
            this.btn_CasseteLoad_B_TCG.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_A
            // 
            this.gbox_CasseteLoad_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_A.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_A.Location = new System.Drawing.Point(11, 2);
            this.gbox_CasseteLoad_A.Name = "gbox_CasseteLoad_A";
            this.gbox_CasseteLoad_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_CasseteLoad_A.TabIndex = 46;
            this.gbox_CasseteLoad_A.TabStop = false;
            this.gbox_CasseteLoad_A.Text = "     A    ";
            // 
            // plAfterIRCutTransfer01
            // 
            this.plAfterIRCutTransfer01.Controls.Add(this.gbox_BreakTransfer_Move);
            this.plAfterIRCutTransfer01.Controls.Add(this.gbox_BreakTransfer_B);
            this.plAfterIRCutTransfer01.Controls.Add(this.gbox_BreakTransfer_A);
            this.plAfterIRCutTransfer01.Location = new System.Drawing.Point(22, 1190);
            this.plAfterIRCutTransfer01.Name = "plAfterIRCutTransfer01";
            this.plAfterIRCutTransfer01.Size = new System.Drawing.Size(922, 371);
            this.plAfterIRCutTransfer01.TabIndex = 74;
            // 
            // gbox_BreakTransfer_Move
            // 
            this.gbox_BreakTransfer_Move.Controls.Add(this.gbox_BreakTransfer_MoveB);
            this.gbox_BreakTransfer_Move.Controls.Add(this.gbox_BreakTransfer_MoveA);
            this.gbox_BreakTransfer_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_Move.Location = new System.Drawing.Point(9, 188);
            this.gbox_BreakTransfer_Move.Name = "gbox_BreakTransfer_Move";
            this.gbox_BreakTransfer_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_BreakTransfer_Move.TabIndex = 56;
            this.gbox_BreakTransfer_Move.TabStop = false;
            this.gbox_BreakTransfer_Move.Text = "     이동     ";
            // 
            // gbox_BreakTransfer_MoveB
            // 
            this.gbox_BreakTransfer_MoveB.Controls.Add(this.tableLayoutPanel44);
            this.gbox_BreakTransfer_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gbox_BreakTransfer_MoveB.Name = "gbox_BreakTransfer_MoveB";
            this.gbox_BreakTransfer_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gbox_BreakTransfer_MoveB.TabIndex = 31;
            this.gbox_BreakTransfer_MoveB.TabStop = false;
            this.gbox_BreakTransfer_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 2;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.Controls.Add(this.btn_BreakTransfer_MoveB_Unload, 0, 0);
            this.tableLayoutPanel44.Controls.Add(this.btn_BreakTransfer_MoveB_Load, 0, 0);
            this.tableLayoutPanel44.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 1;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(430, 112);
            this.tableLayoutPanel44.TabIndex = 55;
            // 
            // btn_BreakTransfer_MoveB_Unload
            // 
            this.btn_BreakTransfer_MoveB_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveB_Unload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveB_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveB_Unload.Location = new System.Drawing.Point(218, 3);
            this.btn_BreakTransfer_MoveB_Unload.Name = "btn_BreakTransfer_MoveB_Unload";
            this.btn_BreakTransfer_MoveB_Unload.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveB_Unload.TabIndex = 67;
            this.btn_BreakTransfer_MoveB_Unload.Text = "언로드";
            this.btn_BreakTransfer_MoveB_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_MoveB_Load
            // 
            this.btn_BreakTransfer_MoveB_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveB_Load.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveB_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveB_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_MoveB_Load.Name = "btn_BreakTransfer_MoveB_Load";
            this.btn_BreakTransfer_MoveB_Load.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveB_Load.TabIndex = 66;
            this.btn_BreakTransfer_MoveB_Load.Text = "로드";
            this.btn_BreakTransfer_MoveB_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakTransfer_MoveA
            // 
            this.gbox_BreakTransfer_MoveA.Controls.Add(this.tableLayoutPanel43);
            this.gbox_BreakTransfer_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_BreakTransfer_MoveA.Name = "gbox_BreakTransfer_MoveA";
            this.gbox_BreakTransfer_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gbox_BreakTransfer_MoveA.TabIndex = 30;
            this.gbox_BreakTransfer_MoveA.TabStop = false;
            this.gbox_BreakTransfer_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel43
            // 
            this.tableLayoutPanel43.ColumnCount = 2;
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.Controls.Add(this.btn_BreakTransfer_MoveA_Unload, 0, 0);
            this.tableLayoutPanel43.Controls.Add(this.btn_BreakTransfer_MoveA_Load, 0, 0);
            this.tableLayoutPanel43.Location = new System.Drawing.Point(7, 27);
            this.tableLayoutPanel43.Name = "tableLayoutPanel43";
            this.tableLayoutPanel43.RowCount = 1;
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel43.Size = new System.Drawing.Size(430, 112);
            this.tableLayoutPanel43.TabIndex = 54;
            // 
            // btn_BreakTransfer_MoveA_Unload
            // 
            this.btn_BreakTransfer_MoveA_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveA_Unload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveA_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveA_Unload.Location = new System.Drawing.Point(218, 3);
            this.btn_BreakTransfer_MoveA_Unload.Name = "btn_BreakTransfer_MoveA_Unload";
            this.btn_BreakTransfer_MoveA_Unload.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveA_Unload.TabIndex = 67;
            this.btn_BreakTransfer_MoveA_Unload.Text = "언로드";
            this.btn_BreakTransfer_MoveA_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_MoveA_Load
            // 
            this.btn_BreakTransfer_MoveA_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveA_Load.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveA_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveA_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_MoveA_Load.Name = "btn_BreakTransfer_MoveA_Load";
            this.btn_BreakTransfer_MoveA_Load.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveA_Load.TabIndex = 66;
            this.btn_BreakTransfer_MoveA_Load.Text = "로드";
            this.btn_BreakTransfer_MoveA_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakTransfer_B
            // 
            this.gbox_BreakTransfer_B.Controls.Add(this.tableLayoutPanel42);
            this.gbox_BreakTransfer_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_B.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_B.Location = new System.Drawing.Point(464, 3);
            this.gbox_BreakTransfer_B.Name = "gbox_BreakTransfer_B";
            this.gbox_BreakTransfer_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_BreakTransfer_B.TabIndex = 55;
            this.gbox_BreakTransfer_B.TabStop = false;
            this.gbox_BreakTransfer_B.Text = "     B     ";
            // 
            // tableLayoutPanel42
            // 
            this.tableLayoutPanel42.ColumnCount = 2;
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerUp, 0, 0);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerDown, 1, 0);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PneumaticOff, 1, 1);
            this.tableLayoutPanel42.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel42.Name = "tableLayoutPanel42";
            this.tableLayoutPanel42.RowCount = 3;
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel42.TabIndex = 55;
            // 
            // btn_BreakTransfer_B_PickerDestructionOn
            // 
            this.btn_BreakTransfer_B_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerDestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_BreakTransfer_B_PickerDestructionOn.Name = "btn_BreakTransfer_B_PickerDestructionOn";
            this.btn_BreakTransfer_B_PickerDestructionOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerDestructionOn.TabIndex = 65;
            this.btn_BreakTransfer_B_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_BreakTransfer_B_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerDestructionOff
            // 
            this.btn_BreakTransfer_B_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerDestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_BreakTransfer_B_PickerDestructionOff.Name = "btn_BreakTransfer_B_PickerDestructionOff";
            this.btn_BreakTransfer_B_PickerDestructionOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerDestructionOff.TabIndex = 64;
            this.btn_BreakTransfer_B_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_BreakTransfer_B_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerUp
            // 
            this.btn_BreakTransfer_B_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerUp.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_B_PickerUp.Name = "btn_BreakTransfer_B_PickerUp";
            this.btn_BreakTransfer_B_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerUp.TabIndex = 60;
            this.btn_BreakTransfer_B_PickerUp.Text = "피커\r\n업";
            this.btn_BreakTransfer_B_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerDown
            // 
            this.btn_BreakTransfer_B_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerDown.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_BreakTransfer_B_PickerDown.Name = "btn_BreakTransfer_B_PickerDown";
            this.btn_BreakTransfer_B_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerDown.TabIndex = 61;
            this.btn_BreakTransfer_B_PickerDown.Text = "피커\r\n다운";
            this.btn_BreakTransfer_B_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerPneumaticOn
            // 
            this.btn_BreakTransfer_B_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerPneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_BreakTransfer_B_PickerPneumaticOn.Name = "btn_BreakTransfer_B_PickerPneumaticOn";
            this.btn_BreakTransfer_B_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerPneumaticOn.TabIndex = 62;
            this.btn_BreakTransfer_B_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_BreakTransfer_B_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PneumaticOff
            // 
            this.btn_BreakTransfer_B_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_BreakTransfer_B_PneumaticOff.Name = "btn_BreakTransfer_B_PneumaticOff";
            this.btn_BreakTransfer_B_PneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PneumaticOff.TabIndex = 63;
            this.btn_BreakTransfer_B_PneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_BreakTransfer_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakTransfer_A
            // 
            this.gbox_BreakTransfer_A.Controls.Add(this.tableLayoutPanel41);
            this.gbox_BreakTransfer_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_A.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_A.Location = new System.Drawing.Point(9, 3);
            this.gbox_BreakTransfer_A.Name = "gbox_BreakTransfer_A";
            this.gbox_BreakTransfer_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_BreakTransfer_A.TabIndex = 54;
            this.gbox_BreakTransfer_A.TabStop = false;
            this.gbox_BreakTransfer_A.Text = "     A    ";
            // 
            // tableLayoutPanel41
            // 
            this.tableLayoutPanel41.ColumnCount = 2;
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerUp, 0, 0);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerDown, 1, 0);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerPneumaticOff, 1, 1);
            this.tableLayoutPanel41.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel41.Name = "tableLayoutPanel41";
            this.tableLayoutPanel41.RowCount = 3;
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel41.TabIndex = 54;
            // 
            // btn_BreakTransfer_A_PickerDestructionOn
            // 
            this.btn_BreakTransfer_A_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerDestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_BreakTransfer_A_PickerDestructionOn.Name = "btn_BreakTransfer_A_PickerDestructionOn";
            this.btn_BreakTransfer_A_PickerDestructionOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerDestructionOn.TabIndex = 65;
            this.btn_BreakTransfer_A_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_BreakTransfer_A_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerDestructionOff
            // 
            this.btn_BreakTransfer_A_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerDestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_BreakTransfer_A_PickerDestructionOff.Name = "btn_BreakTransfer_A_PickerDestructionOff";
            this.btn_BreakTransfer_A_PickerDestructionOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerDestructionOff.TabIndex = 64;
            this.btn_BreakTransfer_A_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_BreakTransfer_A_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerUp
            // 
            this.btn_BreakTransfer_A_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerUp.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_A_PickerUp.Name = "btn_BreakTransfer_A_PickerUp";
            this.btn_BreakTransfer_A_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerUp.TabIndex = 60;
            this.btn_BreakTransfer_A_PickerUp.Text = "피커\r\n업";
            this.btn_BreakTransfer_A_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerDown
            // 
            this.btn_BreakTransfer_A_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerDown.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_BreakTransfer_A_PickerDown.Name = "btn_BreakTransfer_A_PickerDown";
            this.btn_BreakTransfer_A_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerDown.TabIndex = 61;
            this.btn_BreakTransfer_A_PickerDown.Text = "피커\r\n다운";
            this.btn_BreakTransfer_A_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerPneumaticOn
            // 
            this.btn_BreakTransfer_A_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerPneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_BreakTransfer_A_PickerPneumaticOn.Name = "btn_BreakTransfer_A_PickerPneumaticOn";
            this.btn_BreakTransfer_A_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerPneumaticOn.TabIndex = 62;
            this.btn_BreakTransfer_A_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_BreakTransfer_A_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerPneumaticOff
            // 
            this.btn_BreakTransfer_A_PickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerPneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_BreakTransfer_A_PickerPneumaticOff.Name = "btn_BreakTransfer_A_PickerPneumaticOff";
            this.btn_BreakTransfer_A_PickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerPneumaticOff.TabIndex = 63;
            this.btn_BreakTransfer_A_PickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_BreakTransfer_A_PickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // plUnloaderTransfer01
            // 
            this.plUnloaderTransfer01.Controls.Add(this.gbox_UnloaderTransfer_Move);
            this.plUnloaderTransfer01.Controls.Add(this.gbox_UnloaderTransfer_B);
            this.plUnloaderTransfer01.Controls.Add(this.gbox_UnloaderTransfer_A);
            this.plUnloaderTransfer01.Location = new System.Drawing.Point(22, 2737);
            this.plUnloaderTransfer01.Name = "plUnloaderTransfer01";
            this.plUnloaderTransfer01.Size = new System.Drawing.Size(922, 371);
            this.plUnloaderTransfer01.TabIndex = 75;
            // 
            // gbox_UnloaderTransfer_Move
            // 
            this.gbox_UnloaderTransfer_Move.Controls.Add(this.gbox_UnloaderTransfer_MoveB);
            this.gbox_UnloaderTransfer_Move.Controls.Add(this.gbox_UnloaderTransfer_MoveA);
            this.gbox_UnloaderTransfer_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_Move.Location = new System.Drawing.Point(9, 188);
            this.gbox_UnloaderTransfer_Move.Name = "gbox_UnloaderTransfer_Move";
            this.gbox_UnloaderTransfer_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_UnloaderTransfer_Move.TabIndex = 56;
            this.gbox_UnloaderTransfer_Move.TabStop = false;
            this.gbox_UnloaderTransfer_Move.Text = "     이동     ";
            // 
            // gbox_UnloaderTransfer_MoveB
            // 
            this.gbox_UnloaderTransfer_MoveB.Controls.Add(this.tableLayoutPanel61);
            this.gbox_UnloaderTransfer_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gbox_UnloaderTransfer_MoveB.Name = "gbox_UnloaderTransfer_MoveB";
            this.gbox_UnloaderTransfer_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gbox_UnloaderTransfer_MoveB.TabIndex = 31;
            this.gbox_UnloaderTransfer_MoveB.TabStop = false;
            this.gbox_UnloaderTransfer_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel61
            // 
            this.tableLayoutPanel61.ColumnCount = 1;
            this.tableLayoutPanel61.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel61.Controls.Add(this.tableLayoutPanel62, 0, 1);
            this.tableLayoutPanel61.Controls.Add(this.tableLayoutPanel63, 0, 0);
            this.tableLayoutPanel61.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel61.Name = "tableLayoutPanel61";
            this.tableLayoutPanel61.RowCount = 2;
            this.tableLayoutPanel61.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel61.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel61.Size = new System.Drawing.Size(434, 112);
            this.tableLayoutPanel61.TabIndex = 55;
            // 
            // tableLayoutPanel62
            // 
            this.tableLayoutPanel62.ColumnCount = 6;
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T40Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T4P90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T4M90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T30Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T3P90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T3M90Angle, 0, 0);
            this.tableLayoutPanel62.Location = new System.Drawing.Point(3, 59);
            this.tableLayoutPanel62.Name = "tableLayoutPanel62";
            this.tableLayoutPanel62.RowCount = 1;
            this.tableLayoutPanel62.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel62.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel62.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel62.TabIndex = 56;
            // 
            // btn_UnloaderTransfer_MoveB_T40Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T40Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T40Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T40Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T40Angle.Location = new System.Drawing.Point(216, 3);
            this.btn_UnloaderTransfer_MoveB_T40Angle.Name = "btn_UnloaderTransfer_MoveB_T40Angle";
            this.btn_UnloaderTransfer_MoveB_T40Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T40Angle.TabIndex = 61;
            this.btn_UnloaderTransfer_MoveB_T40Angle.Text = "T4\n0도";
            this.btn_UnloaderTransfer_MoveB_T40Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T4P90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Location = new System.Drawing.Point(287, 3);
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Name = "btn_UnloaderTransfer_MoveB_T4P90Angle";
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.TabIndex = 60;
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Text = "T4\r\n+90도";
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T4M90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Location = new System.Drawing.Point(358, 3);
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Name = "btn_UnloaderTransfer_MoveB_T4M90Angle";
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Size = new System.Drawing.Size(67, 44);
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.TabIndex = 59;
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Text = "T4\r\n-90도";
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T30Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T30Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T30Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T30Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T30Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveB_T30Angle.Name = "btn_UnloaderTransfer_MoveB_T30Angle";
            this.btn_UnloaderTransfer_MoveB_T30Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T30Angle.TabIndex = 58;
            this.btn_UnloaderTransfer_MoveB_T30Angle.Text = "T3\r\n0도";
            this.btn_UnloaderTransfer_MoveB_T30Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T3P90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Location = new System.Drawing.Point(74, 3);
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Name = "btn_UnloaderTransfer_MoveB_T3P90Angle";
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.TabIndex = 57;
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Text = "T3\r\n+90도";
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T3M90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Location = new System.Drawing.Point(145, 3);
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Name = "btn_UnloaderTransfer_MoveB_T3M90Angle";
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Text = "T3\r\n-90도";
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel63
            // 
            this.tableLayoutPanel63.ColumnCount = 5;
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel63.Controls.Add(this.btn_UnloaderTransfer_MoveB_BStage, 0, 0);
            this.tableLayoutPanel63.Controls.Add(this.btn_UnloaderTransfer_MoveB_BUnloading, 4, 0);
            this.tableLayoutPanel63.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel63.Name = "tableLayoutPanel63";
            this.tableLayoutPanel63.RowCount = 1;
            this.tableLayoutPanel63.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel63.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel63.TabIndex = 55;
            // 
            // btn_UnloaderTransfer_MoveB_BStage
            // 
            this.btn_UnloaderTransfer_MoveB_BStage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_BStage.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_BStage.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_BStage.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveB_BStage.Name = "btn_UnloaderTransfer_MoveB_BStage";
            this.btn_UnloaderTransfer_MoveB_BStage.Size = new System.Drawing.Size(79, 44);
            this.btn_UnloaderTransfer_MoveB_BStage.TabIndex = 55;
            this.btn_UnloaderTransfer_MoveB_BStage.Text = "B 스테이지";
            this.btn_UnloaderTransfer_MoveB_BStage.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_BUnloading
            // 
            this.btn_UnloaderTransfer_MoveB_BUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_BUnloading.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_BUnloading.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_BUnloading.Location = new System.Drawing.Point(343, 3);
            this.btn_UnloaderTransfer_MoveB_BUnloading.Name = "btn_UnloaderTransfer_MoveB_BUnloading";
            this.btn_UnloaderTransfer_MoveB_BUnloading.Size = new System.Drawing.Size(82, 44);
            this.btn_UnloaderTransfer_MoveB_BUnloading.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveB_BUnloading.Text = "B 언로딩";
            this.btn_UnloaderTransfer_MoveB_BUnloading.UseVisualStyleBackColor = false;
            // 
            // gbox_UnloaderTransfer_MoveA
            // 
            this.gbox_UnloaderTransfer_MoveA.Controls.Add(this.tableLayoutPanel58);
            this.gbox_UnloaderTransfer_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_UnloaderTransfer_MoveA.Name = "gbox_UnloaderTransfer_MoveA";
            this.gbox_UnloaderTransfer_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gbox_UnloaderTransfer_MoveA.TabIndex = 30;
            this.gbox_UnloaderTransfer_MoveA.TabStop = false;
            this.gbox_UnloaderTransfer_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel58
            // 
            this.tableLayoutPanel58.ColumnCount = 1;
            this.tableLayoutPanel58.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel58.Controls.Add(this.tableLayoutPanel60, 0, 1);
            this.tableLayoutPanel58.Controls.Add(this.tableLayoutPanel59, 0, 0);
            this.tableLayoutPanel58.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel58.Name = "tableLayoutPanel58";
            this.tableLayoutPanel58.RowCount = 2;
            this.tableLayoutPanel58.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel58.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel58.Size = new System.Drawing.Size(434, 112);
            this.tableLayoutPanel58.TabIndex = 54;
            // 
            // tableLayoutPanel60
            // 
            this.tableLayoutPanel60.ColumnCount = 6;
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T20Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T2P90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T2M90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T10Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T1P90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T1M90Angle, 0, 0);
            this.tableLayoutPanel60.Location = new System.Drawing.Point(3, 59);
            this.tableLayoutPanel60.Name = "tableLayoutPanel60";
            this.tableLayoutPanel60.RowCount = 1;
            this.tableLayoutPanel60.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel60.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel60.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel60.TabIndex = 56;
            // 
            // btn_UnloaderTransfer_MoveA_T20Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T20Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T20Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T20Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T20Angle.Location = new System.Drawing.Point(216, 3);
            this.btn_UnloaderTransfer_MoveA_T20Angle.Name = "btn_UnloaderTransfer_MoveA_T20Angle";
            this.btn_UnloaderTransfer_MoveA_T20Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T20Angle.TabIndex = 61;
            this.btn_UnloaderTransfer_MoveA_T20Angle.Text = "T2\r\n0도";
            this.btn_UnloaderTransfer_MoveA_T20Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T2P90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Location = new System.Drawing.Point(287, 3);
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Name = "btn_UnloaderTransfer_MoveA_T2P90Angle";
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.TabIndex = 60;
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Text = "T2\r\n+90도";
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T2M90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Location = new System.Drawing.Point(358, 3);
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Name = "btn_UnloaderTransfer_MoveA_T2M90Angle";
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Size = new System.Drawing.Size(67, 44);
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.TabIndex = 59;
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Text = "T2\r\n-90도";
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T10Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T10Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T10Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T10Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T10Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveA_T10Angle.Name = "btn_UnloaderTransfer_MoveA_T10Angle";
            this.btn_UnloaderTransfer_MoveA_T10Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T10Angle.TabIndex = 58;
            this.btn_UnloaderTransfer_MoveA_T10Angle.Text = "T1\r\n0도";
            this.btn_UnloaderTransfer_MoveA_T10Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T1P90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Location = new System.Drawing.Point(74, 3);
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Name = "btn_UnloaderTransfer_MoveA_T1P90Angle";
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.TabIndex = 57;
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Text = "T1\r\n+90도";
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T1M90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Location = new System.Drawing.Point(145, 3);
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Name = "btn_UnloaderTransfer_MoveA_T1M90Angle";
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Text = "T1\r\n-90도";
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel59
            // 
            this.tableLayoutPanel59.ColumnCount = 5;
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel59.Controls.Add(this.btn_UnloaderTransfer_MoveA_AStage, 0, 0);
            this.tableLayoutPanel59.Controls.Add(this.btn_UnloaderTransfer_MoveA_AUnloading, 4, 0);
            this.tableLayoutPanel59.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel59.Name = "tableLayoutPanel59";
            this.tableLayoutPanel59.RowCount = 1;
            this.tableLayoutPanel59.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel59.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel59.TabIndex = 55;
            // 
            // btn_UnloaderTransfer_MoveA_AStage
            // 
            this.btn_UnloaderTransfer_MoveA_AStage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_AStage.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_AStage.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_AStage.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveA_AStage.Name = "btn_UnloaderTransfer_MoveA_AStage";
            this.btn_UnloaderTransfer_MoveA_AStage.Size = new System.Drawing.Size(79, 44);
            this.btn_UnloaderTransfer_MoveA_AStage.TabIndex = 55;
            this.btn_UnloaderTransfer_MoveA_AStage.Text = "A 스테이지";
            this.btn_UnloaderTransfer_MoveA_AStage.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_AUnloading
            // 
            this.btn_UnloaderTransfer_MoveA_AUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_AUnloading.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_AUnloading.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_AUnloading.Location = new System.Drawing.Point(343, 3);
            this.btn_UnloaderTransfer_MoveA_AUnloading.Name = "btn_UnloaderTransfer_MoveA_AUnloading";
            this.btn_UnloaderTransfer_MoveA_AUnloading.Size = new System.Drawing.Size(82, 44);
            this.btn_UnloaderTransfer_MoveA_AUnloading.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveA_AUnloading.Text = "A 언로딩";
            this.btn_UnloaderTransfer_MoveA_AUnloading.UseVisualStyleBackColor = false;
            // 
            // gbox_UnloaderTransfer_B
            // 
            this.gbox_UnloaderTransfer_B.Controls.Add(this.tableLayoutPanel4);
            this.gbox_UnloaderTransfer_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_B.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_B.Location = new System.Drawing.Point(464, 3);
            this.gbox_UnloaderTransfer_B.Name = "gbox_UnloaderTransfer_B";
            this.gbox_UnloaderTransfer_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_UnloaderTransfer_B.TabIndex = 55;
            this.gbox_UnloaderTransfer_B.TabStop = false;
            this.gbox_UnloaderTransfer_B.Text = "     B     ";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestruction2, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestructionOn2, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestructionOff1, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestructionOn1, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOff2, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOn2, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOff1, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOn1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDown2, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerUp2, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDown1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerUp1, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // btn_UnloaderTransfer_B_PickerDestruction2
            // 
            this.btn_UnloaderTransfer_B_PickerDestruction2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestruction2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestruction2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestruction2.Location = new System.Drawing.Point(330, 99);
            this.btn_UnloaderTransfer_B_PickerDestruction2.Name = "btn_UnloaderTransfer_B_PickerDestruction2";
            this.btn_UnloaderTransfer_B_PickerDestruction2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestruction2.TabIndex = 65;
            this.btn_UnloaderTransfer_B_PickerDestruction2.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_B_PickerDestruction2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDestructionOn2
            // 
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Location = new System.Drawing.Point(221, 99);
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Name = "btn_UnloaderTransfer_B_PickerDestructionOn2";
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.TabIndex = 64;
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDestructionOff1
            // 
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Location = new System.Drawing.Point(112, 99);
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Name = "btn_UnloaderTransfer_B_PickerDestructionOff1";
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.TabIndex = 63;
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDestructionOn1
            // 
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Location = new System.Drawing.Point(3, 99);
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Name = "btn_UnloaderTransfer_B_PickerDestructionOn1";
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.TabIndex = 62;
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOff2
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Location = new System.Drawing.Point(330, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Name = "btn_UnloaderTransfer_B_PickerPneumaticOff2";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.TabIndex = 61;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOn2
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Location = new System.Drawing.Point(221, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Name = "btn_UnloaderTransfer_B_PickerPneumaticOn2";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.TabIndex = 60;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOff1
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Location = new System.Drawing.Point(112, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Name = "btn_UnloaderTransfer_B_PickerPneumaticOff1";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.TabIndex = 59;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOn1
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Location = new System.Drawing.Point(3, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Name = "btn_UnloaderTransfer_B_PickerPneumaticOn1";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.TabIndex = 58;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDown2
            // 
            this.btn_UnloaderTransfer_B_PickerDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDown2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDown2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDown2.Location = new System.Drawing.Point(330, 3);
            this.btn_UnloaderTransfer_B_PickerDown2.Name = "btn_UnloaderTransfer_B_PickerDown2";
            this.btn_UnloaderTransfer_B_PickerDown2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDown2.TabIndex = 57;
            this.btn_UnloaderTransfer_B_PickerDown2.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_B_PickerDown2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerUp2
            // 
            this.btn_UnloaderTransfer_B_PickerUp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerUp2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerUp2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerUp2.Location = new System.Drawing.Point(221, 3);
            this.btn_UnloaderTransfer_B_PickerUp2.Name = "btn_UnloaderTransfer_B_PickerUp2";
            this.btn_UnloaderTransfer_B_PickerUp2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerUp2.TabIndex = 56;
            this.btn_UnloaderTransfer_B_PickerUp2.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_B_PickerUp2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDown1
            // 
            this.btn_UnloaderTransfer_B_PickerDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDown1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDown1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDown1.Location = new System.Drawing.Point(112, 3);
            this.btn_UnloaderTransfer_B_PickerDown1.Name = "btn_UnloaderTransfer_B_PickerDown1";
            this.btn_UnloaderTransfer_B_PickerDown1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDown1.TabIndex = 55;
            this.btn_UnloaderTransfer_B_PickerDown1.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_B_PickerDown1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerUp1
            // 
            this.btn_UnloaderTransfer_B_PickerUp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerUp1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerUp1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerUp1.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_B_PickerUp1.Name = "btn_UnloaderTransfer_B_PickerUp1";
            this.btn_UnloaderTransfer_B_PickerUp1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerUp1.TabIndex = 54;
            this.btn_UnloaderTransfer_B_PickerUp1.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_B_PickerUp1.UseVisualStyleBackColor = false;
            // 
            // gbox_UnloaderTransfer_A
            // 
            this.gbox_UnloaderTransfer_A.Controls.Add(this.tableLayoutPanel3);
            this.gbox_UnloaderTransfer_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_A.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_A.Location = new System.Drawing.Point(9, 3);
            this.gbox_UnloaderTransfer_A.Name = "gbox_UnloaderTransfer_A";
            this.gbox_UnloaderTransfer_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_UnloaderTransfer_A.TabIndex = 54;
            this.gbox_UnloaderTransfer_A.TabStop = false;
            this.gbox_UnloaderTransfer_A.Text = "     A    ";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOff2, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOn2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOff1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOn1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOff2, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOn2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOff1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOn1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDown2, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerUp2, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDown1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerUp1, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOff2
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Location = new System.Drawing.Point(330, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Name = "btn_UnloaderTransfer_A_PickerDestructionOff2";
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.TabIndex = 65;
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOn2
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Location = new System.Drawing.Point(221, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Name = "btn_UnloaderTransfer_A_PickerDestructionOn2";
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.TabIndex = 64;
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOff1
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Location = new System.Drawing.Point(112, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Name = "btn_UnloaderTransfer_A_PickerDestructionOff1";
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.TabIndex = 63;
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOn1
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Location = new System.Drawing.Point(3, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Name = "btn_UnloaderTransfer_A_PickerDestructionOn1";
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.TabIndex = 62;
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOff2
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Location = new System.Drawing.Point(330, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Name = "btn_UnloaderTransfer_A_PickerPneumaticOff2";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.TabIndex = 61;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOn2
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Location = new System.Drawing.Point(221, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Name = "btn_UnloaderTransfer_A_PickerPneumaticOn2";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.TabIndex = 60;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOff1
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Location = new System.Drawing.Point(112, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Name = "btn_UnloaderTransfer_A_PickerPneumaticOff1";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.TabIndex = 59;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOn1
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Location = new System.Drawing.Point(3, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Name = "btn_UnloaderTransfer_A_PickerPneumaticOn1";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.TabIndex = 58;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDown2
            // 
            this.btn_UnloaderTransfer_A_PickerDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDown2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDown2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDown2.Location = new System.Drawing.Point(330, 3);
            this.btn_UnloaderTransfer_A_PickerDown2.Name = "btn_UnloaderTransfer_A_PickerDown2";
            this.btn_UnloaderTransfer_A_PickerDown2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDown2.TabIndex = 57;
            this.btn_UnloaderTransfer_A_PickerDown2.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_A_PickerDown2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerUp2
            // 
            this.btn_UnloaderTransfer_A_PickerUp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerUp2.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerUp2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerUp2.Location = new System.Drawing.Point(221, 3);
            this.btn_UnloaderTransfer_A_PickerUp2.Name = "btn_UnloaderTransfer_A_PickerUp2";
            this.btn_UnloaderTransfer_A_PickerUp2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerUp2.TabIndex = 56;
            this.btn_UnloaderTransfer_A_PickerUp2.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_A_PickerUp2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDown1
            // 
            this.btn_UnloaderTransfer_A_PickerDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDown1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDown1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDown1.Location = new System.Drawing.Point(112, 3);
            this.btn_UnloaderTransfer_A_PickerDown1.Name = "btn_UnloaderTransfer_A_PickerDown1";
            this.btn_UnloaderTransfer_A_PickerDown1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDown1.TabIndex = 55;
            this.btn_UnloaderTransfer_A_PickerDown1.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_A_PickerDown1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerUp1
            // 
            this.btn_UnloaderTransfer_A_PickerUp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerUp1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerUp1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerUp1.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_A_PickerUp1.Name = "btn_UnloaderTransfer_A_PickerUp1";
            this.btn_UnloaderTransfer_A_PickerUp1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerUp1.TabIndex = 54;
            this.btn_UnloaderTransfer_A_PickerUp1.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_A_PickerUp1.UseVisualStyleBackColor = false;
            // 
            // plBreakUnit03
            // 
            this.plBreakUnit03.Controls.Add(this.gbox_BreakUnitTY_Shutter);
            this.plBreakUnit03.Controls.Add(this.gbox_BreakUnitTY_DummyBox);
            this.plBreakUnit03.Controls.Add(this.gbox_BreakUnitTY_Move);
            this.plBreakUnit03.Controls.Add(this.gbox_BreakUnitTY_B);
            this.plBreakUnit03.Controls.Add(this.gbox_BreakUnitTY_A);
            this.plBreakUnit03.Location = new System.Drawing.Point(22, 2351);
            this.plBreakUnit03.Name = "plBreakUnit03";
            this.plBreakUnit03.Size = new System.Drawing.Size(922, 371);
            this.plBreakUnit03.TabIndex = 76;
            // 
            // gbox_BreakUnitTY_Shutter
            // 
            this.gbox_BreakUnitTY_Shutter.Controls.Add(this.tableLayoutPanel53);
            this.gbox_BreakUnitTY_Shutter.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Shutter.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Shutter.Location = new System.Drawing.Point(701, 73);
            this.gbox_BreakUnitTY_Shutter.Name = "gbox_BreakUnitTY_Shutter";
            this.gbox_BreakUnitTY_Shutter.Size = new System.Drawing.Size(212, 109);
            this.gbox_BreakUnitTY_Shutter.TabIndex = 60;
            this.gbox_BreakUnitTY_Shutter.TabStop = false;
            this.gbox_BreakUnitTY_Shutter.Text = "     셔터    ";
            // 
            // tableLayoutPanel53
            // 
            this.tableLayoutPanel53.ColumnCount = 2;
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel53.Controls.Add(this.btn_BreakUnitTY_Shutter_Close, 0, 0);
            this.tableLayoutPanel53.Controls.Add(this.btn_BreakUnitTY_Shutter_Open, 0, 0);
            this.tableLayoutPanel53.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel53.Name = "tableLayoutPanel53";
            this.tableLayoutPanel53.RowCount = 1;
            this.tableLayoutPanel53.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel53.Size = new System.Drawing.Size(200, 75);
            this.tableLayoutPanel53.TabIndex = 57;
            // 
            // btn_BreakUnitTY_Shutter_Close
            // 
            this.btn_BreakUnitTY_Shutter_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Shutter_Close.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Shutter_Close.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Shutter_Close.Location = new System.Drawing.Point(103, 3);
            this.btn_BreakUnitTY_Shutter_Close.Name = "btn_BreakUnitTY_Shutter_Close";
            this.btn_BreakUnitTY_Shutter_Close.Size = new System.Drawing.Size(94, 69);
            this.btn_BreakUnitTY_Shutter_Close.TabIndex = 58;
            this.btn_BreakUnitTY_Shutter_Close.Text = "더미 셔터\r\nClose";
            this.btn_BreakUnitTY_Shutter_Close.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Shutter_Open
            // 
            this.btn_BreakUnitTY_Shutter_Open.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Shutter_Open.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Shutter_Open.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Shutter_Open.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_Shutter_Open.Name = "btn_BreakUnitTY_Shutter_Open";
            this.btn_BreakUnitTY_Shutter_Open.Size = new System.Drawing.Size(94, 69);
            this.btn_BreakUnitTY_Shutter_Open.TabIndex = 57;
            this.btn_BreakUnitTY_Shutter_Open.Text = "더미 셔터\r\nOpen";
            this.btn_BreakUnitTY_Shutter_Open.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_DummyBox
            // 
            this.gbox_BreakUnitTY_DummyBox.Controls.Add(this.tableLayoutPanel52);
            this.gbox_BreakUnitTY_DummyBox.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_DummyBox.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_DummyBox.Location = new System.Drawing.Point(701, 3);
            this.gbox_BreakUnitTY_DummyBox.Name = "gbox_BreakUnitTY_DummyBox";
            this.gbox_BreakUnitTY_DummyBox.Size = new System.Drawing.Size(212, 64);
            this.gbox_BreakUnitTY_DummyBox.TabIndex = 56;
            this.gbox_BreakUnitTY_DummyBox.TabStop = false;
            this.gbox_BreakUnitTY_DummyBox.Text = "     더미 박스    ";
            // 
            // tableLayoutPanel52
            // 
            this.tableLayoutPanel52.ColumnCount = 2;
            this.tableLayoutPanel52.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel52.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel52.Controls.Add(this.btn_BreakUnitTY_DummyBox_Output, 0, 0);
            this.tableLayoutPanel52.Controls.Add(this.btn_BreakUnitTY_DummyBox_Input, 0, 0);
            this.tableLayoutPanel52.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel52.Name = "tableLayoutPanel52";
            this.tableLayoutPanel52.RowCount = 1;
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel52.Size = new System.Drawing.Size(200, 30);
            this.tableLayoutPanel52.TabIndex = 56;
            // 
            // btn_BreakUnitTY_DummyBox_Output
            // 
            this.btn_BreakUnitTY_DummyBox_Output.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DummyBox_Output.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakUnitTY_DummyBox_Output.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_DummyBox_Output.Location = new System.Drawing.Point(103, 3);
            this.btn_BreakUnitTY_DummyBox_Output.Name = "btn_BreakUnitTY_DummyBox_Output";
            this.btn_BreakUnitTY_DummyBox_Output.Size = new System.Drawing.Size(94, 24);
            this.btn_BreakUnitTY_DummyBox_Output.TabIndex = 58;
            this.btn_BreakUnitTY_DummyBox_Output.Text = "더미 배출";
            this.btn_BreakUnitTY_DummyBox_Output.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_DummyBox_Input
            // 
            this.btn_BreakUnitTY_DummyBox_Input.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DummyBox_Input.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakUnitTY_DummyBox_Input.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_DummyBox_Input.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_DummyBox_Input.Name = "btn_BreakUnitTY_DummyBox_Input";
            this.btn_BreakUnitTY_DummyBox_Input.Size = new System.Drawing.Size(94, 24);
            this.btn_BreakUnitTY_DummyBox_Input.TabIndex = 57;
            this.btn_BreakUnitTY_DummyBox_Input.Text = "더미 투입";
            this.btn_BreakUnitTY_DummyBox_Input.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_Move
            // 
            this.gbox_BreakUnitTY_Move.Controls.Add(this.tableLayoutPanel54);
            this.gbox_BreakUnitTY_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Move.Location = new System.Drawing.Point(9, 188);
            this.gbox_BreakUnitTY_Move.Name = "gbox_BreakUnitTY_Move";
            this.gbox_BreakUnitTY_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_BreakUnitTY_Move.TabIndex = 59;
            this.gbox_BreakUnitTY_Move.TabStop = false;
            this.gbox_BreakUnitTY_Move.Text = "     이동     ";
            // 
            // tableLayoutPanel54
            // 
            this.tableLayoutPanel54.ColumnCount = 3;
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.Controls.Add(this.gbox_BreakUnitTY_Theta, 0, 0);
            this.tableLayoutPanel54.Controls.Add(this.gbox_BreakUnitTY_MoveB, 0, 0);
            this.tableLayoutPanel54.Controls.Add(this.gbox_BreakUnitTY_MoveA, 0, 0);
            this.tableLayoutPanel54.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel54.Name = "tableLayoutPanel54";
            this.tableLayoutPanel54.RowCount = 1;
            this.tableLayoutPanel54.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel54.Size = new System.Drawing.Size(892, 145);
            this.tableLayoutPanel54.TabIndex = 0;
            // 
            // gbox_BreakUnitTY_Theta
            // 
            this.gbox_BreakUnitTY_Theta.Controls.Add(this.tableLayoutPanel57);
            this.gbox_BreakUnitTY_Theta.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Theta.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Theta.Location = new System.Drawing.Point(597, 3);
            this.gbox_BreakUnitTY_Theta.Name = "gbox_BreakUnitTY_Theta";
            this.gbox_BreakUnitTY_Theta.Size = new System.Drawing.Size(290, 139);
            this.gbox_BreakUnitTY_Theta.TabIndex = 54;
            this.gbox_BreakUnitTY_Theta.TabStop = false;
            this.gbox_BreakUnitTY_Theta.Text = "     세타    ";
            // 
            // tableLayoutPanel57
            // 
            this.tableLayoutPanel57.ColumnCount = 2;
            this.tableLayoutPanel57.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T2Wait, 0, 1);
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T4Wait, 0, 1);
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T1Wait, 0, 0);
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T3Wait, 1, 0);
            this.tableLayoutPanel57.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel57.Name = "tableLayoutPanel57";
            this.tableLayoutPanel57.RowCount = 2;
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel57.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel57.TabIndex = 0;
            // 
            // btn_BreakUnitTY_Theta_T2Wait
            // 
            this.btn_BreakUnitTY_Theta_T2Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T2Wait.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T2Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T2Wait.Location = new System.Drawing.Point(3, 57);
            this.btn_BreakUnitTY_Theta_T2Wait.Name = "btn_BreakUnitTY_Theta_T2Wait";
            this.btn_BreakUnitTY_Theta_T2Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T2Wait.TabIndex = 65;
            this.btn_BreakUnitTY_Theta_T2Wait.Text = "T2 대기";
            this.btn_BreakUnitTY_Theta_T2Wait.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Theta_T4Wait
            // 
            this.btn_BreakUnitTY_Theta_T4Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T4Wait.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T4Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T4Wait.Location = new System.Drawing.Point(143, 57);
            this.btn_BreakUnitTY_Theta_T4Wait.Name = "btn_BreakUnitTY_Theta_T4Wait";
            this.btn_BreakUnitTY_Theta_T4Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T4Wait.TabIndex = 64;
            this.btn_BreakUnitTY_Theta_T4Wait.Text = "T4 대기";
            this.btn_BreakUnitTY_Theta_T4Wait.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Theta_T1Wait
            // 
            this.btn_BreakUnitTY_Theta_T1Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T1Wait.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T1Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T1Wait.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_Theta_T1Wait.Name = "btn_BreakUnitTY_Theta_T1Wait";
            this.btn_BreakUnitTY_Theta_T1Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T1Wait.TabIndex = 62;
            this.btn_BreakUnitTY_Theta_T1Wait.Text = "T1 대기";
            this.btn_BreakUnitTY_Theta_T1Wait.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Theta_T3Wait
            // 
            this.btn_BreakUnitTY_Theta_T3Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T3Wait.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T3Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T3Wait.Location = new System.Drawing.Point(143, 3);
            this.btn_BreakUnitTY_Theta_T3Wait.Name = "btn_BreakUnitTY_Theta_T3Wait";
            this.btn_BreakUnitTY_Theta_T3Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T3Wait.TabIndex = 63;
            this.btn_BreakUnitTY_Theta_T3Wait.Text = "T3 대기";
            this.btn_BreakUnitTY_Theta_T3Wait.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_MoveB
            // 
            this.gbox_BreakUnitTY_MoveB.Controls.Add(this.tableLayoutPanel56);
            this.gbox_BreakUnitTY_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_MoveB.Location = new System.Drawing.Point(300, 3);
            this.gbox_BreakUnitTY_MoveB.Name = "gbox_BreakUnitTY_MoveB";
            this.gbox_BreakUnitTY_MoveB.Size = new System.Drawing.Size(290, 139);
            this.gbox_BreakUnitTY_MoveB.TabIndex = 53;
            this.gbox_BreakUnitTY_MoveB.TabStop = false;
            this.gbox_BreakUnitTY_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel56
            // 
            this.tableLayoutPanel56.ColumnCount = 2;
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel56.Controls.Add(this.btn_BreakUnitTY_MoveB_Unload, 0, 0);
            this.tableLayoutPanel56.Controls.Add(this.btn_BreakUnitTY_MoveB_Load, 0, 0);
            this.tableLayoutPanel56.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel56.Name = "tableLayoutPanel56";
            this.tableLayoutPanel56.RowCount = 1;
            this.tableLayoutPanel56.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel56.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel56.TabIndex = 0;
            // 
            // btn_BreakUnitTY_MoveB_Unload
            // 
            this.btn_BreakUnitTY_MoveB_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveB_Unload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveB_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveB_Unload.Location = new System.Drawing.Point(143, 3);
            this.btn_BreakUnitTY_MoveB_Unload.Name = "btn_BreakUnitTY_MoveB_Unload";
            this.btn_BreakUnitTY_MoveB_Unload.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveB_Unload.TabIndex = 63;
            this.btn_BreakUnitTY_MoveB_Unload.Text = "언로드";
            this.btn_BreakUnitTY_MoveB_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_MoveB_Load
            // 
            this.btn_BreakUnitTY_MoveB_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveB_Load.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveB_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveB_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_MoveB_Load.Name = "btn_BreakUnitTY_MoveB_Load";
            this.btn_BreakUnitTY_MoveB_Load.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveB_Load.TabIndex = 62;
            this.btn_BreakUnitTY_MoveB_Load.Text = "로드";
            this.btn_BreakUnitTY_MoveB_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_MoveA
            // 
            this.gbox_BreakUnitTY_MoveA.Controls.Add(this.tableLayoutPanel55);
            this.gbox_BreakUnitTY_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_MoveA.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitTY_MoveA.Name = "gbox_BreakUnitTY_MoveA";
            this.gbox_BreakUnitTY_MoveA.Size = new System.Drawing.Size(290, 139);
            this.gbox_BreakUnitTY_MoveA.TabIndex = 52;
            this.gbox_BreakUnitTY_MoveA.TabStop = false;
            this.gbox_BreakUnitTY_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel55
            // 
            this.tableLayoutPanel55.ColumnCount = 2;
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel55.Controls.Add(this.btn_BreakUnitTY_MoveA_Unload, 0, 0);
            this.tableLayoutPanel55.Controls.Add(this.btn_BreakUnitTY_MoveA_Load, 0, 0);
            this.tableLayoutPanel55.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel55.Name = "tableLayoutPanel55";
            this.tableLayoutPanel55.RowCount = 1;
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel55.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel55.TabIndex = 0;
            // 
            // btn_BreakUnitTY_MoveA_Unload
            // 
            this.btn_BreakUnitTY_MoveA_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveA_Unload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveA_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveA_Unload.Location = new System.Drawing.Point(143, 3);
            this.btn_BreakUnitTY_MoveA_Unload.Name = "btn_BreakUnitTY_MoveA_Unload";
            this.btn_BreakUnitTY_MoveA_Unload.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveA_Unload.TabIndex = 63;
            this.btn_BreakUnitTY_MoveA_Unload.Text = "언로드";
            this.btn_BreakUnitTY_MoveA_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_MoveA_Load
            // 
            this.btn_BreakUnitTY_MoveA_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveA_Load.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveA_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveA_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_MoveA_Load.Name = "btn_BreakUnitTY_MoveA_Load";
            this.btn_BreakUnitTY_MoveA_Load.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveA_Load.TabIndex = 62;
            this.btn_BreakUnitTY_MoveA_Load.Text = "로드";
            this.btn_BreakUnitTY_MoveA_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_B
            // 
            this.gbox_BreakUnitTY_B.Controls.Add(this.tableLayoutPanel50);
            this.gbox_BreakUnitTY_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_B.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_B.Location = new System.Drawing.Point(355, 3);
            this.gbox_BreakUnitTY_B.Name = "gbox_BreakUnitTY_B";
            this.gbox_BreakUnitTY_B.Size = new System.Drawing.Size(340, 179);
            this.gbox_BreakUnitTY_B.TabIndex = 58;
            this.gbox_BreakUnitTY_B.TabStop = false;
            this.gbox_BreakUnitTY_B.Text = "     B     ";
            // 
            // tableLayoutPanel50
            // 
            this.tableLayoutPanel50.ColumnCount = 4;
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Destruction_On, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Destruction_Off, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Destruction_On, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Destruction_Off, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Pneumatic_Off, 1, 0);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Pneumatic_On, 0, 0);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Pneumatic_On, 2, 0);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Pneumatic_Off, 3, 0);
            this.tableLayoutPanel50.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel50.Name = "tableLayoutPanel50";
            this.tableLayoutPanel50.RowCount = 2;
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel50.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel50.TabIndex = 56;
            // 
            // btn_BreakUnitTY_B_1Destruction_On
            // 
            this.btn_BreakUnitTY_B_1Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Destruction_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Destruction_On.Location = new System.Drawing.Point(3, 75);
            this.btn_BreakUnitTY_B_1Destruction_On.Name = "btn_BreakUnitTY_B_1Destruction_On";
            this.btn_BreakUnitTY_B_1Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Destruction_On.TabIndex = 62;
            this.btn_BreakUnitTY_B_1Destruction_On.Text = "1 파기\r\n온";
            this.btn_BreakUnitTY_B_1Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_1Destruction_Off
            // 
            this.btn_BreakUnitTY_B_1Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Destruction_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Destruction_Off.Location = new System.Drawing.Point(85, 75);
            this.btn_BreakUnitTY_B_1Destruction_Off.Name = "btn_BreakUnitTY_B_1Destruction_Off";
            this.btn_BreakUnitTY_B_1Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Destruction_Off.TabIndex = 61;
            this.btn_BreakUnitTY_B_1Destruction_Off.Text = "1 파기\r\n오프";
            this.btn_BreakUnitTY_B_1Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Destruction_On
            // 
            this.btn_BreakUnitTY_B_2Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Destruction_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Destruction_On.Location = new System.Drawing.Point(167, 75);
            this.btn_BreakUnitTY_B_2Destruction_On.Name = "btn_BreakUnitTY_B_2Destruction_On";
            this.btn_BreakUnitTY_B_2Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Destruction_On.TabIndex = 60;
            this.btn_BreakUnitTY_B_2Destruction_On.Text = "2 파기\r\n온";
            this.btn_BreakUnitTY_B_2Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Destruction_Off
            // 
            this.btn_BreakUnitTY_B_2Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Destruction_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Destruction_Off.Location = new System.Drawing.Point(249, 75);
            this.btn_BreakUnitTY_B_2Destruction_Off.Name = "btn_BreakUnitTY_B_2Destruction_Off";
            this.btn_BreakUnitTY_B_2Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Destruction_Off.TabIndex = 58;
            this.btn_BreakUnitTY_B_2Destruction_Off.Text = "2 파기\r\n오프";
            this.btn_BreakUnitTY_B_2Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_1Pneumatic_Off
            // 
            this.btn_BreakUnitTY_B_1Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Location = new System.Drawing.Point(85, 3);
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Name = "btn_BreakUnitTY_B_1Pneumatic_Off";
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Pneumatic_Off.TabIndex = 55;
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Text = "1 공압\r\n오프";
            this.btn_BreakUnitTY_B_1Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_1Pneumatic_On
            // 
            this.btn_BreakUnitTY_B_1Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Pneumatic_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Pneumatic_On.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_B_1Pneumatic_On.Name = "btn_BreakUnitTY_B_1Pneumatic_On";
            this.btn_BreakUnitTY_B_1Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Pneumatic_On.TabIndex = 57;
            this.btn_BreakUnitTY_B_1Pneumatic_On.Text = "1 공압\r\n온";
            this.btn_BreakUnitTY_B_1Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Pneumatic_On
            // 
            this.btn_BreakUnitTY_B_2Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Pneumatic_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Pneumatic_On.Location = new System.Drawing.Point(167, 3);
            this.btn_BreakUnitTY_B_2Pneumatic_On.Name = "btn_BreakUnitTY_B_2Pneumatic_On";
            this.btn_BreakUnitTY_B_2Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Pneumatic_On.TabIndex = 59;
            this.btn_BreakUnitTY_B_2Pneumatic_On.Text = "2 공압\r\n온";
            this.btn_BreakUnitTY_B_2Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Pneumatic_Off
            // 
            this.btn_BreakUnitTY_B_2Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Location = new System.Drawing.Point(249, 3);
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Name = "btn_BreakUnitTY_B_2Pneumatic_Off";
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Pneumatic_Off.TabIndex = 56;
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Text = "2 공압\r\n오프";
            this.btn_BreakUnitTY_B_2Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_A
            // 
            this.gbox_BreakUnitTY_A.Controls.Add(this.tableLayoutPanel51);
            this.gbox_BreakUnitTY_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_A.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_A.Location = new System.Drawing.Point(9, 3);
            this.gbox_BreakUnitTY_A.Name = "gbox_BreakUnitTY_A";
            this.gbox_BreakUnitTY_A.Size = new System.Drawing.Size(340, 179);
            this.gbox_BreakUnitTY_A.TabIndex = 57;
            this.gbox_BreakUnitTY_A.TabStop = false;
            this.gbox_BreakUnitTY_A.Text = "     A    ";
            // 
            // tableLayoutPanel51
            // 
            this.tableLayoutPanel51.ColumnCount = 4;
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Destruction_On, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Destruction_Off, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Destruction_On, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Destruction_Off, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Pneumatic_Off, 1, 0);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Pneumatic_On, 0, 0);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Pneumatic_On, 2, 0);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Pneumatic_Off, 3, 0);
            this.tableLayoutPanel51.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel51.Name = "tableLayoutPanel51";
            this.tableLayoutPanel51.RowCount = 2;
            this.tableLayoutPanel51.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel51.TabIndex = 55;
            // 
            // btn_BreakUnitTY_A_1Destruction_On
            // 
            this.btn_BreakUnitTY_A_1Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Destruction_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Destruction_On.Location = new System.Drawing.Point(3, 75);
            this.btn_BreakUnitTY_A_1Destruction_On.Name = "btn_BreakUnitTY_A_1Destruction_On";
            this.btn_BreakUnitTY_A_1Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Destruction_On.TabIndex = 62;
            this.btn_BreakUnitTY_A_1Destruction_On.Text = "1 파기\r\n온";
            this.btn_BreakUnitTY_A_1Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_1Destruction_Off
            // 
            this.btn_BreakUnitTY_A_1Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Destruction_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Destruction_Off.Location = new System.Drawing.Point(85, 75);
            this.btn_BreakUnitTY_A_1Destruction_Off.Name = "btn_BreakUnitTY_A_1Destruction_Off";
            this.btn_BreakUnitTY_A_1Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Destruction_Off.TabIndex = 61;
            this.btn_BreakUnitTY_A_1Destruction_Off.Text = "1 파기\r\n오프";
            this.btn_BreakUnitTY_A_1Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Destruction_On
            // 
            this.btn_BreakUnitTY_A_2Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Destruction_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Destruction_On.Location = new System.Drawing.Point(167, 75);
            this.btn_BreakUnitTY_A_2Destruction_On.Name = "btn_BreakUnitTY_A_2Destruction_On";
            this.btn_BreakUnitTY_A_2Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Destruction_On.TabIndex = 60;
            this.btn_BreakUnitTY_A_2Destruction_On.Text = "2 파기\r\n온";
            this.btn_BreakUnitTY_A_2Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Destruction_Off
            // 
            this.btn_BreakUnitTY_A_2Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Destruction_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Destruction_Off.Location = new System.Drawing.Point(249, 75);
            this.btn_BreakUnitTY_A_2Destruction_Off.Name = "btn_BreakUnitTY_A_2Destruction_Off";
            this.btn_BreakUnitTY_A_2Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Destruction_Off.TabIndex = 58;
            this.btn_BreakUnitTY_A_2Destruction_Off.Text = "2 파기\r\n오프";
            this.btn_BreakUnitTY_A_2Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_1Pneumatic_Off
            // 
            this.btn_BreakUnitTY_A_1Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Location = new System.Drawing.Point(85, 3);
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Name = "btn_BreakUnitTY_A_1Pneumatic_Off";
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Pneumatic_Off.TabIndex = 55;
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Text = "1 공압\r\n오프";
            this.btn_BreakUnitTY_A_1Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_1Pneumatic_On
            // 
            this.btn_BreakUnitTY_A_1Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Pneumatic_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Pneumatic_On.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_A_1Pneumatic_On.Name = "btn_BreakUnitTY_A_1Pneumatic_On";
            this.btn_BreakUnitTY_A_1Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Pneumatic_On.TabIndex = 57;
            this.btn_BreakUnitTY_A_1Pneumatic_On.Text = "1 공압\r\n온";
            this.btn_BreakUnitTY_A_1Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Pneumatic_On
            // 
            this.btn_BreakUnitTY_A_2Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Pneumatic_On.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Pneumatic_On.Location = new System.Drawing.Point(167, 3);
            this.btn_BreakUnitTY_A_2Pneumatic_On.Name = "btn_BreakUnitTY_A_2Pneumatic_On";
            this.btn_BreakUnitTY_A_2Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Pneumatic_On.TabIndex = 59;
            this.btn_BreakUnitTY_A_2Pneumatic_On.Text = "2 공압\r\n온";
            this.btn_BreakUnitTY_A_2Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Pneumatic_Off
            // 
            this.btn_BreakUnitTY_A_2Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Location = new System.Drawing.Point(249, 3);
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Name = "btn_BreakUnitTY_A_2Pneumatic_Off";
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Pneumatic_Off.TabIndex = 56;
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Text = "2 공압\r\n오프";
            this.btn_BreakUnitTY_A_2Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // plBreakUnit01
            // 
            this.plBreakUnit01.Controls.Add(this.gbox_BreakUnitXZ_Move);
            this.plBreakUnit01.Location = new System.Drawing.Point(22, 1969);
            this.plBreakUnit01.Name = "plBreakUnit01";
            this.plBreakUnit01.Size = new System.Drawing.Size(922, 371);
            this.plBreakUnit01.TabIndex = 77;
            // 
            // gbox_BreakUnitXZ_Move
            // 
            this.gbox_BreakUnitXZ_Move.Controls.Add(this.tableLayoutPanel45);
            this.gbox_BreakUnitXZ_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitXZ_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_Move.Location = new System.Drawing.Point(9, 184);
            this.gbox_BreakUnitXZ_Move.Name = "gbox_BreakUnitXZ_Move";
            this.gbox_BreakUnitXZ_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_BreakUnitXZ_Move.TabIndex = 54;
            this.gbox_BreakUnitXZ_Move.TabStop = false;
            this.gbox_BreakUnitXZ_Move.Text = "     이동     ";
            // 
            // tableLayoutPanel45
            // 
            this.tableLayoutPanel45.ColumnCount = 4;
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4, 3, 0);
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3, 2, 0);
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2, 1, 0);
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1, 0, 0);
            this.tableLayoutPanel45.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel45.Name = "tableLayoutPanel45";
            this.tableLayoutPanel45.RowCount = 1;
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel45.Size = new System.Drawing.Size(661, 154);
            this.tableLayoutPanel45.TabIndex = 54;
            // 
            // gbox_BreakUnitXZ_MoveZ4
            // 
            this.gbox_BreakUnitXZ_MoveZ4.Controls.Add(this.tableLayoutPanel49);
            this.gbox_BreakUnitXZ_MoveZ4.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4.Location = new System.Drawing.Point(498, 3);
            this.gbox_BreakUnitXZ_MoveZ4.Name = "gbox_BreakUnitXZ_MoveZ4";
            this.gbox_BreakUnitXZ_MoveZ4.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ4.TabIndex = 57;
            this.gbox_BreakUnitXZ_MoveZ4.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ4.Text = "     Z4     ";
            // 
            // tableLayoutPanel49
            // 
            this.tableLayoutPanel49.ColumnCount = 1;
            this.tableLayoutPanel49.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel49.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4_SlowDescent, 0, 2);
            this.tableLayoutPanel49.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4_VisionCheck, 0, 0);
            this.tableLayoutPanel49.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4_FastDescent, 0, 1);
            this.tableLayoutPanel49.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel49.Name = "tableLayoutPanel49";
            this.tableLayoutPanel49.RowCount = 3;
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel49.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel49.TabIndex = 55;
            // 
            // gbox_BreakUnitXZ_MoveZ4_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ4_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ4_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ4_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ4_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ4_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ3
            // 
            this.gbox_BreakUnitXZ_MoveZ3.Controls.Add(this.tableLayoutPanel48);
            this.gbox_BreakUnitXZ_MoveZ3.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3.Location = new System.Drawing.Point(333, 3);
            this.gbox_BreakUnitXZ_MoveZ3.Name = "gbox_BreakUnitXZ_MoveZ3";
            this.gbox_BreakUnitXZ_MoveZ3.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ3.TabIndex = 56;
            this.gbox_BreakUnitXZ_MoveZ3.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ3.Text = "     Z3     ";
            // 
            // tableLayoutPanel48
            // 
            this.tableLayoutPanel48.ColumnCount = 1;
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel48.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3_SlowDescent, 0, 2);
            this.tableLayoutPanel48.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3_VisionCheck, 0, 0);
            this.tableLayoutPanel48.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3_FastDescent, 0, 1);
            this.tableLayoutPanel48.Location = new System.Drawing.Point(4, 28);
            this.tableLayoutPanel48.Name = "tableLayoutPanel48";
            this.tableLayoutPanel48.RowCount = 3;
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel48.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel48.TabIndex = 55;
            // 
            // gbox_BreakUnitXZ_MoveZ3_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ3_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ3_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ3_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ3_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ3_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ2
            // 
            this.gbox_BreakUnitXZ_MoveZ2.Controls.Add(this.tableLayoutPanel47);
            this.gbox_BreakUnitXZ_MoveZ2.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2.Location = new System.Drawing.Point(168, 3);
            this.gbox_BreakUnitXZ_MoveZ2.Name = "gbox_BreakUnitXZ_MoveZ2";
            this.gbox_BreakUnitXZ_MoveZ2.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ2.TabIndex = 55;
            this.gbox_BreakUnitXZ_MoveZ2.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ2.Text = "     Z2     ";
            // 
            // tableLayoutPanel47
            // 
            this.tableLayoutPanel47.ColumnCount = 1;
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel47.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2_SlowDescent, 0, 2);
            this.tableLayoutPanel47.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2_VisionCheck, 0, 0);
            this.tableLayoutPanel47.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2_FastDescent, 0, 1);
            this.tableLayoutPanel47.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel47.Name = "tableLayoutPanel47";
            this.tableLayoutPanel47.RowCount = 3;
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel47.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel47.TabIndex = 55;
            // 
            // gbox_BreakUnitXZ_MoveZ2_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ2_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ2_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ2_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ2_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ2_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ1
            // 
            this.gbox_BreakUnitXZ_MoveZ1.Controls.Add(this.tableLayoutPanel46);
            this.gbox_BreakUnitXZ_MoveZ1.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ1.Name = "gbox_BreakUnitXZ_MoveZ1";
            this.gbox_BreakUnitXZ_MoveZ1.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ1.TabIndex = 54;
            this.gbox_BreakUnitXZ_MoveZ1.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ1.Text = "     Z1     ";
            // 
            // tableLayoutPanel46
            // 
            this.tableLayoutPanel46.ColumnCount = 1;
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel46.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1_SlowDescent, 0, 2);
            this.tableLayoutPanel46.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1_VisionCheck, 0, 0);
            this.tableLayoutPanel46.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1_FastDescent, 0, 1);
            this.tableLayoutPanel46.Location = new System.Drawing.Point(4, 28);
            this.tableLayoutPanel46.Name = "tableLayoutPanel46";
            this.tableLayoutPanel46.RowCount = 3;
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel46.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel46.TabIndex = 54;
            // 
            // gbox_BreakUnitXZ_MoveZ1_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ1_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ1_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ1_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ1_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ1_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.UseVisualStyleBackColor = false;
            // 
            // plCellLoadTransfer01
            // 
            this.plCellLoadTransfer01.Controls.Add(this.gbox_CellLoad_Move);
            this.plCellLoadTransfer01.Controls.Add(this.gbox_CellLoad_B);
            this.plCellLoadTransfer01.Controls.Add(this.gbox_CellLoad_A);
            this.plCellLoadTransfer01.Location = new System.Drawing.Point(22, 803);
            this.plCellLoadTransfer01.Name = "plCellLoadTransfer01";
            this.plCellLoadTransfer01.Size = new System.Drawing.Size(922, 371);
            this.plCellLoadTransfer01.TabIndex = 78;
            // 
            // gbox_CellLoad_Move
            // 
            this.gbox_CellLoad_Move.Controls.Add(this.gbox_CellLoad_MoveLorUn);
            this.gbox_CellLoad_Move.Controls.Add(this.gbox_CellLoad_MoveB);
            this.gbox_CellLoad_Move.Controls.Add(this.gbox_CellLoad_MoveA);
            this.gbox_CellLoad_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_Move.Location = new System.Drawing.Point(9, 188);
            this.gbox_CellLoad_Move.Name = "gbox_CellLoad_Move";
            this.gbox_CellLoad_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CellLoad_Move.TabIndex = 56;
            this.gbox_CellLoad_Move.TabStop = false;
            this.gbox_CellLoad_Move.Text = "     이동     ";
            // 
            // gbox_CellLoad_MoveLorUn
            // 
            this.gbox_CellLoad_MoveLorUn.Controls.Add(this.tableLayoutPanel28);
            this.gbox_CellLoad_MoveLorUn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_MoveLorUn.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_MoveLorUn.Location = new System.Drawing.Point(7, 21);
            this.gbox_CellLoad_MoveLorUn.Name = "gbox_CellLoad_MoveLorUn";
            this.gbox_CellLoad_MoveLorUn.Size = new System.Drawing.Size(293, 152);
            this.gbox_CellLoad_MoveLorUn.TabIndex = 57;
            this.gbox_CellLoad_MoveLorUn.TabStop = false;
            this.gbox_CellLoad_MoveLorUn.Text = "     로딩/언로딩    ";
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 2;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_PreAlignMark1, 0, 2);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_PreAlignMark2, 0, 2);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_Y1UnL, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_Y1L, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_X2L, 1, 0);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_X1L, 0, 0);
            this.tableLayoutPanel28.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 3;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel28.TabIndex = 55;
            // 
            // btn_CellLoad_MoveLorUn_PreAlignMark1
            // 
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Location = new System.Drawing.Point(3, 85);
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Name = "btn_CellLoad_MoveLorUn_PreAlignMark1";
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Size = new System.Drawing.Size(134, 37);
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.TabIndex = 60;
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Text = "PreAlign\r\nMark1";
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_PreAlignMark2
            // 
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Location = new System.Drawing.Point(143, 85);
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Name = "btn_CellLoad_MoveLorUn_PreAlignMark2";
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Size = new System.Drawing.Size(134, 37);
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.TabIndex = 59;
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Text = "PreAlign\r\nMark2";
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_Y1UnL
            // 
            this.btn_CellLoad_MoveLorUn_Y1UnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_Y1UnL.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_Y1UnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_Y1UnL.Location = new System.Drawing.Point(143, 44);
            this.btn_CellLoad_MoveLorUn_Y1UnL.Name = "btn_CellLoad_MoveLorUn_Y1UnL";
            this.btn_CellLoad_MoveLorUn_Y1UnL.Size = new System.Drawing.Size(134, 35);
            this.btn_CellLoad_MoveLorUn_Y1UnL.TabIndex = 58;
            this.btn_CellLoad_MoveLorUn_Y1UnL.Text = "Y1\r\n언로딩";
            this.btn_CellLoad_MoveLorUn_Y1UnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_Y1L
            // 
            this.btn_CellLoad_MoveLorUn_Y1L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_Y1L.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_Y1L.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_Y1L.Location = new System.Drawing.Point(3, 44);
            this.btn_CellLoad_MoveLorUn_Y1L.Name = "btn_CellLoad_MoveLorUn_Y1L";
            this.btn_CellLoad_MoveLorUn_Y1L.Size = new System.Drawing.Size(134, 35);
            this.btn_CellLoad_MoveLorUn_Y1L.TabIndex = 56;
            this.btn_CellLoad_MoveLorUn_Y1L.Text = "Y1\r\n로딩";
            this.btn_CellLoad_MoveLorUn_Y1L.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_X2L
            // 
            this.btn_CellLoad_MoveLorUn_X2L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_X2L.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_X2L.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_X2L.Location = new System.Drawing.Point(143, 3);
            this.btn_CellLoad_MoveLorUn_X2L.Name = "btn_CellLoad_MoveLorUn_X2L";
            this.btn_CellLoad_MoveLorUn_X2L.Size = new System.Drawing.Size(135, 35);
            this.btn_CellLoad_MoveLorUn_X2L.TabIndex = 55;
            this.btn_CellLoad_MoveLorUn_X2L.Text = "X2\r\n로딩";
            this.btn_CellLoad_MoveLorUn_X2L.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_X1L
            // 
            this.btn_CellLoad_MoveLorUn_X1L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_X1L.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_X1L.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_X1L.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveLorUn_X1L.Name = "btn_CellLoad_MoveLorUn_X1L";
            this.btn_CellLoad_MoveLorUn_X1L.Size = new System.Drawing.Size(134, 35);
            this.btn_CellLoad_MoveLorUn_X1L.TabIndex = 57;
            this.btn_CellLoad_MoveLorUn_X1L.Text = "X1\r\n로딩";
            this.btn_CellLoad_MoveLorUn_X1L.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_MoveB
            // 
            this.gbox_CellLoad_MoveB.Controls.Add(this.tableLayoutPanel31);
            this.gbox_CellLoad_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_MoveB.Location = new System.Drawing.Point(605, 21);
            this.gbox_CellLoad_MoveB.Name = "gbox_CellLoad_MoveB";
            this.gbox_CellLoad_MoveB.Size = new System.Drawing.Size(293, 152);
            this.gbox_CellLoad_MoveB.TabIndex = 56;
            this.gbox_CellLoad_MoveB.TabStop = false;
            this.gbox_CellLoad_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 1;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel32, 0, 2);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel33, 0, 1);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel34, 0, 0);
            this.tableLayoutPanel31.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 3;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel31.TabIndex = 1;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.ColumnCount = 3;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.Controls.Add(this.btn_CellLoad_MoveB_BPickerM90Angle, 0, 0);
            this.tableLayoutPanel32.Controls.Add(this.btn_CellLoad_MoveB_BPickerP90Angle, 0, 0);
            this.tableLayoutPanel32.Controls.Add(this.btn_CellLoad_MoveB_BPicker0Angle, 0, 0);
            this.tableLayoutPanel32.Location = new System.Drawing.Point(3, 85);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 1;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel32.TabIndex = 2;
            // 
            // btn_CellLoad_MoveB_BPickerM90Angle
            // 
            this.btn_CellLoad_MoveB_BPickerM90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerM90Angle.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerM90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerM90Angle.Location = new System.Drawing.Point(185, 3);
            this.btn_CellLoad_MoveB_BPickerM90Angle.Name = "btn_CellLoad_MoveB_BPickerM90Angle";
            this.btn_CellLoad_MoveB_BPickerM90Angle.Size = new System.Drawing.Size(87, 29);
            this.btn_CellLoad_MoveB_BPickerM90Angle.TabIndex = 60;
            this.btn_CellLoad_MoveB_BPickerM90Angle.Text = "B 피커 -90도";
            this.btn_CellLoad_MoveB_BPickerM90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_BPickerP90Angle
            // 
            this.btn_CellLoad_MoveB_BPickerP90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerP90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerP90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerP90Angle.Location = new System.Drawing.Point(94, 3);
            this.btn_CellLoad_MoveB_BPickerP90Angle.Name = "btn_CellLoad_MoveB_BPickerP90Angle";
            this.btn_CellLoad_MoveB_BPickerP90Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveB_BPickerP90Angle.TabIndex = 59;
            this.btn_CellLoad_MoveB_BPickerP90Angle.Text = "B 피커 +90도";
            this.btn_CellLoad_MoveB_BPickerP90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_BPicker0Angle
            // 
            this.btn_CellLoad_MoveB_BPicker0Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPicker0Angle.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPicker0Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPicker0Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveB_BPicker0Angle.Name = "btn_CellLoad_MoveB_BPicker0Angle";
            this.btn_CellLoad_MoveB_BPicker0Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveB_BPicker0Angle.TabIndex = 58;
            this.btn_CellLoad_MoveB_BPicker0Angle.Text = "B 피커 0도";
            this.btn_CellLoad_MoveB_BPicker0Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 2;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Controls.Add(this.btn_CellLoad_MoveB_BPickerUnL, 0, 0);
            this.tableLayoutPanel33.Controls.Add(this.btn_CellLoad_MoveB_BPickerL, 0, 0);
            this.tableLayoutPanel33.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 1;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel33.TabIndex = 1;
            // 
            // btn_CellLoad_MoveB_BPickerUnL
            // 
            this.btn_CellLoad_MoveB_BPickerUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerUnL.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveB_BPickerUnL.Name = "btn_CellLoad_MoveB_BPickerUnL";
            this.btn_CellLoad_MoveB_BPickerUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveB_BPickerUnL.TabIndex = 59;
            this.btn_CellLoad_MoveB_BPickerUnL.Text = "B 피커 언로딩";
            this.btn_CellLoad_MoveB_BPickerUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_BPickerL
            // 
            this.btn_CellLoad_MoveB_BPickerL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerL.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveB_BPickerL.Name = "btn_CellLoad_MoveB_BPickerL";
            this.btn_CellLoad_MoveB_BPickerL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveB_BPickerL.TabIndex = 58;
            this.btn_CellLoad_MoveB_BPickerL.Text = "B 피커 로딩";
            this.btn_CellLoad_MoveB_BPickerL.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 2;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.Controls.Add(this.btn_CellLoad_MoveB_X2BStageUnL, 0, 0);
            this.tableLayoutPanel34.Controls.Add(this.btn_CellLoad_MoveB_X1BStageUnL, 0, 0);
            this.tableLayoutPanel34.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 1;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel34.TabIndex = 0;
            // 
            // btn_CellLoad_MoveB_X2BStageUnL
            // 
            this.btn_CellLoad_MoveB_X2BStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_X2BStageUnL.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_X2BStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_X2BStageUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveB_X2BStageUnL.Name = "btn_CellLoad_MoveB_X2BStageUnL";
            this.btn_CellLoad_MoveB_X2BStageUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveB_X2BStageUnL.TabIndex = 59;
            this.btn_CellLoad_MoveB_X2BStageUnL.Text = "X2 B 스테이지 언로딩";
            this.btn_CellLoad_MoveB_X2BStageUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_X1BStageUnL
            // 
            this.btn_CellLoad_MoveB_X1BStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_X1BStageUnL.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_X1BStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_X1BStageUnL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveB_X1BStageUnL.Name = "btn_CellLoad_MoveB_X1BStageUnL";
            this.btn_CellLoad_MoveB_X1BStageUnL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveB_X1BStageUnL.TabIndex = 58;
            this.btn_CellLoad_MoveB_X1BStageUnL.Text = "X1 B 스테이지 언로딩";
            this.btn_CellLoad_MoveB_X1BStageUnL.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_MoveA
            // 
            this.gbox_CellLoad_MoveA.Controls.Add(this.tableLayoutPanel26);
            this.gbox_CellLoad_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_MoveA.Location = new System.Drawing.Point(306, 21);
            this.gbox_CellLoad_MoveA.Name = "gbox_CellLoad_MoveA";
            this.gbox_CellLoad_MoveA.Size = new System.Drawing.Size(293, 152);
            this.gbox_CellLoad_MoveA.TabIndex = 31;
            this.gbox_CellLoad_MoveA.TabStop = false;
            this.gbox_CellLoad_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 1;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel30, 0, 2);
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel29, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel27, 0, 0);
            this.tableLayoutPanel26.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 3;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel26.TabIndex = 0;
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 3;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.Controls.Add(this.btn_CellLoad_MoveA_APickerM90Angle, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btn_CellLoad_MoveA_APickerP90Angle, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btn_CellLoad_MoveA_APicker0Angle, 0, 0);
            this.tableLayoutPanel30.Location = new System.Drawing.Point(3, 85);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel30.TabIndex = 2;
            // 
            // btn_CellLoad_MoveA_APickerM90Angle
            // 
            this.btn_CellLoad_MoveA_APickerM90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerM90Angle.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerM90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerM90Angle.Location = new System.Drawing.Point(185, 3);
            this.btn_CellLoad_MoveA_APickerM90Angle.Name = "btn_CellLoad_MoveA_APickerM90Angle";
            this.btn_CellLoad_MoveA_APickerM90Angle.Size = new System.Drawing.Size(87, 29);
            this.btn_CellLoad_MoveA_APickerM90Angle.TabIndex = 60;
            this.btn_CellLoad_MoveA_APickerM90Angle.Text = "A 피커 -90도";
            this.btn_CellLoad_MoveA_APickerM90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_APickerP90Angle
            // 
            this.btn_CellLoad_MoveA_APickerP90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerP90Angle.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerP90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerP90Angle.Location = new System.Drawing.Point(94, 3);
            this.btn_CellLoad_MoveA_APickerP90Angle.Name = "btn_CellLoad_MoveA_APickerP90Angle";
            this.btn_CellLoad_MoveA_APickerP90Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveA_APickerP90Angle.TabIndex = 59;
            this.btn_CellLoad_MoveA_APickerP90Angle.Text = "A 피커 +90도";
            this.btn_CellLoad_MoveA_APickerP90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_APicker0Angle
            // 
            this.btn_CellLoad_MoveA_APicker0Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APicker0Angle.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APicker0Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APicker0Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveA_APicker0Angle.Name = "btn_CellLoad_MoveA_APicker0Angle";
            this.btn_CellLoad_MoveA_APicker0Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveA_APicker0Angle.TabIndex = 58;
            this.btn_CellLoad_MoveA_APicker0Angle.Text = "A 피커 0도";
            this.btn_CellLoad_MoveA_APicker0Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 2;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.Controls.Add(this.btn_CellLoad_MoveA_APickerUnL, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.btn_CellLoad_MoveA_APickerL, 0, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 1;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel29.TabIndex = 1;
            // 
            // btn_CellLoad_MoveA_APickerUnL
            // 
            this.btn_CellLoad_MoveA_APickerUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerUnL.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveA_APickerUnL.Name = "btn_CellLoad_MoveA_APickerUnL";
            this.btn_CellLoad_MoveA_APickerUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveA_APickerUnL.TabIndex = 59;
            this.btn_CellLoad_MoveA_APickerUnL.Text = "A 피커 언로딩";
            this.btn_CellLoad_MoveA_APickerUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_APickerL
            // 
            this.btn_CellLoad_MoveA_APickerL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerL.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveA_APickerL.Name = "btn_CellLoad_MoveA_APickerL";
            this.btn_CellLoad_MoveA_APickerL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveA_APickerL.TabIndex = 58;
            this.btn_CellLoad_MoveA_APickerL.Text = "A 피커 로딩";
            this.btn_CellLoad_MoveA_APickerL.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 2;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Controls.Add(this.btn_CellLoad_MoveA_X2AStageUnL, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.btn_CellLoad_MoveA_X1AStageUnL, 0, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel27.TabIndex = 0;
            // 
            // btn_CellLoad_MoveA_X2AStageUnL
            // 
            this.btn_CellLoad_MoveA_X2AStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_X2AStageUnL.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_X2AStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_X2AStageUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveA_X2AStageUnL.Name = "btn_CellLoad_MoveA_X2AStageUnL";
            this.btn_CellLoad_MoveA_X2AStageUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveA_X2AStageUnL.TabIndex = 59;
            this.btn_CellLoad_MoveA_X2AStageUnL.Text = "X2 A 스테이지 언로딩";
            this.btn_CellLoad_MoveA_X2AStageUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_X1AStageUnL
            // 
            this.btn_CellLoad_MoveA_X1AStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_X1AStageUnL.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_X1AStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_X1AStageUnL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveA_X1AStageUnL.Name = "btn_CellLoad_MoveA_X1AStageUnL";
            this.btn_CellLoad_MoveA_X1AStageUnL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveA_X1AStageUnL.TabIndex = 58;
            this.btn_CellLoad_MoveA_X1AStageUnL.Text = "X1 A 스테이지 언로딩";
            this.btn_CellLoad_MoveA_X1AStageUnL.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_B
            // 
            this.gbox_CellLoad_B.Controls.Add(this.tableLayoutPanel25);
            this.gbox_CellLoad_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_B.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_B.Location = new System.Drawing.Point(464, 3);
            this.gbox_CellLoad_B.Name = "gbox_CellLoad_B";
            this.gbox_CellLoad_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_CellLoad_B.TabIndex = 55;
            this.gbox_CellLoad_B.TabStop = false;
            this.gbox_CellLoad_B.Text = "     B     ";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerUp, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerDown, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerPneumaticOff, 1, 1);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 3;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel25.TabIndex = 55;
            // 
            // btn_CellLoad_B_PickerDestructionOn
            // 
            this.btn_CellLoad_B_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerDestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_CellLoad_B_PickerDestructionOn.Name = "btn_CellLoad_B_PickerDestructionOn";
            this.btn_CellLoad_B_PickerDestructionOn.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_B_PickerDestructionOn.TabIndex = 63;
            this.btn_CellLoad_B_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_CellLoad_B_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerDestructionOff
            // 
            this.btn_CellLoad_B_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerDestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_CellLoad_B_PickerDestructionOff.Name = "btn_CellLoad_B_PickerDestructionOff";
            this.btn_CellLoad_B_PickerDestructionOff.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_B_PickerDestructionOff.TabIndex = 62;
            this.btn_CellLoad_B_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_CellLoad_B_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerUp
            // 
            this.btn_CellLoad_B_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerUp.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_B_PickerUp.Name = "btn_CellLoad_B_PickerUp";
            this.btn_CellLoad_B_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerUp.TabIndex = 58;
            this.btn_CellLoad_B_PickerUp.Text = "피커\r\n업";
            this.btn_CellLoad_B_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerDown
            // 
            this.btn_CellLoad_B_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerDown.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_CellLoad_B_PickerDown.Name = "btn_CellLoad_B_PickerDown";
            this.btn_CellLoad_B_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerDown.TabIndex = 59;
            this.btn_CellLoad_B_PickerDown.Text = "피커\r\n다운";
            this.btn_CellLoad_B_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerPneumaticOn
            // 
            this.btn_CellLoad_B_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerPneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_CellLoad_B_PickerPneumaticOn.Name = "btn_CellLoad_B_PickerPneumaticOn";
            this.btn_CellLoad_B_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerPneumaticOn.TabIndex = 60;
            this.btn_CellLoad_B_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_CellLoad_B_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerPneumaticOff
            // 
            this.btn_CellLoad_B_PickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerPneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_CellLoad_B_PickerPneumaticOff.Name = "btn_CellLoad_B_PickerPneumaticOff";
            this.btn_CellLoad_B_PickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerPneumaticOff.TabIndex = 61;
            this.btn_CellLoad_B_PickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_CellLoad_B_PickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_A
            // 
            this.gbox_CellLoad_A.Controls.Add(this.tableLayoutPanel24);
            this.gbox_CellLoad_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_A.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_A.Location = new System.Drawing.Point(9, 3);
            this.gbox_CellLoad_A.Name = "gbox_CellLoad_A";
            this.gbox_CellLoad_A.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_CellLoad_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_CellLoad_A.TabIndex = 54;
            this.gbox_CellLoad_A.TabStop = false;
            this.gbox_CellLoad_A.Text = "     A    ";
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerUp, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerDown, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerPneumaticOff, 1, 1);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 3;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel24.TabIndex = 54;
            // 
            // btn_CellLoad_A_PickerDestructionOn
            // 
            this.btn_CellLoad_A_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerDestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_CellLoad_A_PickerDestructionOn.Name = "btn_CellLoad_A_PickerDestructionOn";
            this.btn_CellLoad_A_PickerDestructionOn.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_A_PickerDestructionOn.TabIndex = 63;
            this.btn_CellLoad_A_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_CellLoad_A_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerDestructionOff
            // 
            this.btn_CellLoad_A_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerDestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_CellLoad_A_PickerDestructionOff.Name = "btn_CellLoad_A_PickerDestructionOff";
            this.btn_CellLoad_A_PickerDestructionOff.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_A_PickerDestructionOff.TabIndex = 62;
            this.btn_CellLoad_A_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_CellLoad_A_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerUp
            // 
            this.btn_CellLoad_A_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerUp.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_A_PickerUp.Name = "btn_CellLoad_A_PickerUp";
            this.btn_CellLoad_A_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerUp.TabIndex = 58;
            this.btn_CellLoad_A_PickerUp.Text = "피커\r\n업";
            this.btn_CellLoad_A_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerDown
            // 
            this.btn_CellLoad_A_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerDown.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_CellLoad_A_PickerDown.Name = "btn_CellLoad_A_PickerDown";
            this.btn_CellLoad_A_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerDown.TabIndex = 59;
            this.btn_CellLoad_A_PickerDown.Text = "피커\r\n다운";
            this.btn_CellLoad_A_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerPneumaticOn
            // 
            this.btn_CellLoad_A_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerPneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_CellLoad_A_PickerPneumaticOn.Name = "btn_CellLoad_A_PickerPneumaticOn";
            this.btn_CellLoad_A_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerPneumaticOn.TabIndex = 60;
            this.btn_CellLoad_A_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_CellLoad_A_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerPneumaticOff
            // 
            this.btn_CellLoad_A_PickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerPneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_CellLoad_A_PickerPneumaticOff.Name = "btn_CellLoad_A_PickerPneumaticOff";
            this.btn_CellLoad_A_PickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerPneumaticOff.TabIndex = 61;
            this.btn_CellLoad_A_PickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_CellLoad_A_PickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // plIRCutProcess01
            // 
            this.plIRCutProcess01.Controls.Add(this.gbox_IRCutProcess_Move);
            this.plIRCutProcess01.Controls.Add(this.gbox_IRCutProcess_B);
            this.plIRCutProcess01.Controls.Add(this.gbox_IRCutProcess_A);
            this.plIRCutProcess01.Location = new System.Drawing.Point(22, 1578);
            this.plIRCutProcess01.Name = "plIRCutProcess01";
            this.plIRCutProcess01.Size = new System.Drawing.Size(922, 377);
            this.plIRCutProcess01.TabIndex = 79;
            // 
            // gbox_IRCutProcess_Move
            // 
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_RightOffset);
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_LeftOffset);
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_MoveB);
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_MoveA);
            this.gbox_IRCutProcess_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_Move.Location = new System.Drawing.Point(9, 195);
            this.gbox_IRCutProcess_Move.Name = "gbox_IRCutProcess_Move";
            this.gbox_IRCutProcess_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_IRCutProcess_Move.TabIndex = 56;
            this.gbox_IRCutProcess_Move.TabStop = false;
            this.gbox_IRCutProcess_Move.Text = "     이동     ";
            // 
            // gbox_IRCutProcess_RightOffset
            // 
            this.gbox_IRCutProcess_RightOffset.Controls.Add(this.tableLayoutPanel40);
            this.gbox_IRCutProcess_RightOffset.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_RightOffset.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_RightOffset.Location = new System.Drawing.Point(678, 19);
            this.gbox_IRCutProcess_RightOffset.Name = "gbox_IRCutProcess_RightOffset";
            this.gbox_IRCutProcess_RightOffset.Size = new System.Drawing.Size(212, 154);
            this.gbox_IRCutProcess_RightOffset.TabIndex = 32;
            this.gbox_IRCutProcess_RightOffset.TabStop = false;
            this.gbox_IRCutProcess_RightOffset.Text = "     우측 오프셋    ";
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 2;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Right_Vision2Focus, 0, 1);
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Right_Focus2Vision, 0, 1);
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Left_Vision2Focus, 0, 0);
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Left_Focus2Vision, 1, 0);
            this.tableLayoutPanel40.Location = new System.Drawing.Point(6, 25);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 2;
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(202, 118);
            this.tableLayoutPanel40.TabIndex = 34;
            // 
            // btn_IRCutProcess_RightOffset_Right_Vision2Focus
            // 
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Location = new System.Drawing.Point(3, 62);
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Name = "btn_IRCutProcess_RightOffset_Right_Vision2Focus";
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.TabIndex = 66;
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Text = "우측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_RightOffset_Right_Focus2Vision
            // 
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Location = new System.Drawing.Point(104, 62);
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Name = "btn_IRCutProcess_RightOffset_Right_Focus2Vision";
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.TabIndex = 65;
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Text = "우측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_RightOffset_Left_Vision2Focus
            // 
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Name = "btn_IRCutProcess_RightOffset_Left_Vision2Focus";
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.TabIndex = 63;
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Text = "좌측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_RightOffset_Left_Focus2Vision
            // 
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Location = new System.Drawing.Point(104, 3);
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Name = "btn_IRCutProcess_RightOffset_Left_Focus2Vision";
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.TabIndex = 64;
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Text = "좌측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_LeftOffset
            // 
            this.gbox_IRCutProcess_LeftOffset.Controls.Add(this.tableLayoutPanel39);
            this.gbox_IRCutProcess_LeftOffset.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_LeftOffset.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_LeftOffset.Location = new System.Drawing.Point(457, 19);
            this.gbox_IRCutProcess_LeftOffset.Name = "gbox_IRCutProcess_LeftOffset";
            this.gbox_IRCutProcess_LeftOffset.Size = new System.Drawing.Size(212, 154);
            this.gbox_IRCutProcess_LeftOffset.TabIndex = 32;
            this.gbox_IRCutProcess_LeftOffset.TabStop = false;
            this.gbox_IRCutProcess_LeftOffset.Text = "     좌측 오프셋    ";
            // 
            // tableLayoutPanel39
            // 
            this.tableLayoutPanel39.ColumnCount = 2;
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus, 0, 0);
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision, 1, 0);
            this.tableLayoutPanel39.Location = new System.Drawing.Point(4, 25);
            this.tableLayoutPanel39.Name = "tableLayoutPanel39";
            this.tableLayoutPanel39.RowCount = 2;
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Size = new System.Drawing.Size(202, 118);
            this.tableLayoutPanel39.TabIndex = 33;
            // 
            // btn_IRCutProcess_LeftOffset_Right_Visoin2Focus
            // 
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Location = new System.Drawing.Point(3, 62);
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Name = "btn_IRCutProcess_LeftOffset_Right_Visoin2Focus";
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.TabIndex = 66;
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Text = "우측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_LeftOffset_Right_Focus2Visoin
            // 
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Location = new System.Drawing.Point(104, 62);
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Name = "btn_IRCutProcess_LeftOffset_Right_Focus2Visoin";
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.TabIndex = 65;
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Text = "우측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_LeftOffset_Left_Vision2Focus
            // 
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Name = "btn_IRCutProcess_LeftOffset_Left_Vision2Focus";
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.TabIndex = 63;
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Text = "좌측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_LeftOffset_Left_Focus2Vision
            // 
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Location = new System.Drawing.Point(104, 3);
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Name = "btn_IRCutProcess_LeftOffset_Left_Focus2Vision";
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.TabIndex = 64;
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Text = "좌측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_MoveB
            // 
            this.gbox_IRCutProcess_MoveB.Controls.Add(this.tableLayoutPanel38);
            this.gbox_IRCutProcess_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_MoveB.Location = new System.Drawing.Point(237, 19);
            this.gbox_IRCutProcess_MoveB.Name = "gbox_IRCutProcess_MoveB";
            this.gbox_IRCutProcess_MoveB.Size = new System.Drawing.Size(212, 154);
            this.gbox_IRCutProcess_MoveB.TabIndex = 31;
            this.gbox_IRCutProcess_MoveB.TabStop = false;
            this.gbox_IRCutProcess_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 2;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R1Camera, 0, 2);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R2Camera, 0, 2);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_RightCellLoad, 0, 0);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_RightCellUnload, 1, 0);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R1Laser, 0, 1);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R2Laser, 1, 1);
            this.tableLayoutPanel38.Location = new System.Drawing.Point(4, 25);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 3;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(206, 123);
            this.tableLayoutPanel38.TabIndex = 55;
            // 
            // btn_IRCutProcess_MoveB_R1Camera
            // 
            this.btn_IRCutProcess_MoveB_R1Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R1Camera.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R1Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R1Camera.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_MoveB_R1Camera.Name = "btn_IRCutProcess_MoveB_R1Camera";
            this.btn_IRCutProcess_MoveB_R1Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveB_R1Camera.TabIndex = 65;
            this.btn_IRCutProcess_MoveB_R1Camera.Text = "우측1\r\n카메라 확인";
            this.btn_IRCutProcess_MoveB_R1Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_R2Camera
            // 
            this.btn_IRCutProcess_MoveB_R2Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R2Camera.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R2Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R2Camera.Location = new System.Drawing.Point(106, 85);
            this.btn_IRCutProcess_MoveB_R2Camera.Name = "btn_IRCutProcess_MoveB_R2Camera";
            this.btn_IRCutProcess_MoveB_R2Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveB_R2Camera.TabIndex = 64;
            this.btn_IRCutProcess_MoveB_R2Camera.Text = "우측2\r\n카메라 확인";
            this.btn_IRCutProcess_MoveB_R2Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_RightCellLoad
            // 
            this.btn_IRCutProcess_MoveB_RightCellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_RightCellLoad.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_RightCellLoad.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_RightCellLoad.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_MoveB_RightCellLoad.Name = "btn_IRCutProcess_MoveB_RightCellLoad";
            this.btn_IRCutProcess_MoveB_RightCellLoad.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_RightCellLoad.TabIndex = 60;
            this.btn_IRCutProcess_MoveB_RightCellLoad.Text = "우측 셀 로드";
            this.btn_IRCutProcess_MoveB_RightCellLoad.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_RightCellUnload
            // 
            this.btn_IRCutProcess_MoveB_RightCellUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_RightCellUnload.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_RightCellUnload.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_RightCellUnload.Location = new System.Drawing.Point(106, 3);
            this.btn_IRCutProcess_MoveB_RightCellUnload.Name = "btn_IRCutProcess_MoveB_RightCellUnload";
            this.btn_IRCutProcess_MoveB_RightCellUnload.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_RightCellUnload.TabIndex = 62;
            this.btn_IRCutProcess_MoveB_RightCellUnload.Text = "우측 셀 언로드";
            this.btn_IRCutProcess_MoveB_RightCellUnload.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_R1Laser
            // 
            this.btn_IRCutProcess_MoveB_R1Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R1Laser.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R1Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R1Laser.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_MoveB_R1Laser.Name = "btn_IRCutProcess_MoveB_R1Laser";
            this.btn_IRCutProcess_MoveB_R1Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_R1Laser.TabIndex = 61;
            this.btn_IRCutProcess_MoveB_R1Laser.Text = "우측1 레이저샷";
            this.btn_IRCutProcess_MoveB_R1Laser.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_R2Laser
            // 
            this.btn_IRCutProcess_MoveB_R2Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R2Laser.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R2Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R2Laser.Location = new System.Drawing.Point(106, 44);
            this.btn_IRCutProcess_MoveB_R2Laser.Name = "btn_IRCutProcess_MoveB_R2Laser";
            this.btn_IRCutProcess_MoveB_R2Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_R2Laser.TabIndex = 63;
            this.btn_IRCutProcess_MoveB_R2Laser.Text = "우측2 레이저샷";
            this.btn_IRCutProcess_MoveB_R2Laser.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_MoveA
            // 
            this.gbox_IRCutProcess_MoveA.Controls.Add(this.tableLayoutPanel37);
            this.gbox_IRCutProcess_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_MoveA.Location = new System.Drawing.Point(14, 19);
            this.gbox_IRCutProcess_MoveA.Name = "gbox_IRCutProcess_MoveA";
            this.gbox_IRCutProcess_MoveA.Size = new System.Drawing.Size(215, 154);
            this.gbox_IRCutProcess_MoveA.TabIndex = 30;
            this.gbox_IRCutProcess_MoveA.TabStop = false;
            this.gbox_IRCutProcess_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 2;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L1Camera, 0, 2);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L2Camera, 0, 2);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_LeftCellLoad, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_LeftCellUnload, 1, 0);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L1Laser, 0, 1);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L2Laser, 1, 1);
            this.tableLayoutPanel37.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 3;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(206, 123);
            this.tableLayoutPanel37.TabIndex = 54;
            // 
            // btn_IRCutProcess_MoveA_L1Camera
            // 
            this.btn_IRCutProcess_MoveA_L1Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L1Camera.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L1Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L1Camera.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_MoveA_L1Camera.Name = "btn_IRCutProcess_MoveA_L1Camera";
            this.btn_IRCutProcess_MoveA_L1Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveA_L1Camera.TabIndex = 65;
            this.btn_IRCutProcess_MoveA_L1Camera.Text = "좌측1\r\n카메라 확인";
            this.btn_IRCutProcess_MoveA_L1Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_L2Camera
            // 
            this.btn_IRCutProcess_MoveA_L2Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L2Camera.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L2Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L2Camera.Location = new System.Drawing.Point(106, 85);
            this.btn_IRCutProcess_MoveA_L2Camera.Name = "btn_IRCutProcess_MoveA_L2Camera";
            this.btn_IRCutProcess_MoveA_L2Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveA_L2Camera.TabIndex = 64;
            this.btn_IRCutProcess_MoveA_L2Camera.Text = "좌측2\r\n카메라 확인";
            this.btn_IRCutProcess_MoveA_L2Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_LeftCellLoad
            // 
            this.btn_IRCutProcess_MoveA_LeftCellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_LeftCellLoad.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Name = "btn_IRCutProcess_MoveA_LeftCellLoad";
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_LeftCellLoad.TabIndex = 60;
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Text = "좌측 셀 로드";
            this.btn_IRCutProcess_MoveA_LeftCellLoad.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_LeftCellUnload
            // 
            this.btn_IRCutProcess_MoveA_LeftCellUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_LeftCellUnload.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Location = new System.Drawing.Point(106, 3);
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Name = "btn_IRCutProcess_MoveA_LeftCellUnload";
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_LeftCellUnload.TabIndex = 62;
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Text = "좌측 셀 언로드";
            this.btn_IRCutProcess_MoveA_LeftCellUnload.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_L1Laser
            // 
            this.btn_IRCutProcess_MoveA_L1Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L1Laser.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L1Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L1Laser.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_MoveA_L1Laser.Name = "btn_IRCutProcess_MoveA_L1Laser";
            this.btn_IRCutProcess_MoveA_L1Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_L1Laser.TabIndex = 61;
            this.btn_IRCutProcess_MoveA_L1Laser.Text = "좌측1 레이저샷";
            this.btn_IRCutProcess_MoveA_L1Laser.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_L2Laser
            // 
            this.btn_IRCutProcess_MoveA_L2Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L2Laser.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L2Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L2Laser.Location = new System.Drawing.Point(106, 44);
            this.btn_IRCutProcess_MoveA_L2Laser.Name = "btn_IRCutProcess_MoveA_L2Laser";
            this.btn_IRCutProcess_MoveA_L2Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_L2Laser.TabIndex = 63;
            this.btn_IRCutProcess_MoveA_L2Laser.Text = "좌측2 레이저샷";
            this.btn_IRCutProcess_MoveA_L2Laser.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_B
            // 
            this.gbox_IRCutProcess_B.Controls.Add(this.tableLayoutPanel36);
            this.gbox_IRCutProcess_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_B.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_B.Location = new System.Drawing.Point(464, -3);
            this.gbox_IRCutProcess_B.Name = "gbox_IRCutProcess_B";
            this.gbox_IRCutProcess_B.Size = new System.Drawing.Size(449, 192);
            this.gbox_IRCutProcess_B.TabIndex = 55;
            this.gbox_IRCutProcess_B.TabStop = false;
            this.gbox_IRCutProcess_B.Text = "     B     ";
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.ColumnCount = 4;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off, 1, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch1On, 2, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off, 3, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off, 1, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch2On, 2, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off, 3, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch2On, 0, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch1On, 0, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch1On, 0, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch1Off, 1, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch1On, 2, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch1Off, 3, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch2On, 0, 3);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch2Off, 1, 3);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch2Off, 3, 3);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch2On, 2, 3);
            this.tableLayoutPanel36.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 4;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(437, 165);
            this.tableLayoutPanel36.TabIndex = 55;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Location = new System.Drawing.Point(112, 3);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch1Off";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.TabIndex = 60;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Text = "우측1 공압\r\n채널1 오프";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Location = new System.Drawing.Point(221, 3);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch1On";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.TabIndex = 63;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Text = "우측2 공압\r\n채널1 온";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Location = new System.Drawing.Point(330, 3);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch1Off";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.TabIndex = 62;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Text = "우측2 공압\r\n채널1 오프";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Location = new System.Drawing.Point(112, 44);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch2Off";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.TabIndex = 65;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Text = "우측1 공압\r\n채널2 오프";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Location = new System.Drawing.Point(221, 44);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch2On";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.TabIndex = 64;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Text = "우측2 공압\r\n채널2 온";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Location = new System.Drawing.Point(330, 44);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch2Off";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Size = new System.Drawing.Size(104, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.TabIndex = 68;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Text = "우측2 공압\r\n채널2 오프";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch2On";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.TabIndex = 61;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Text = "우측1 공압\r\n채널2 온";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch1On";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.TabIndex = 59;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Text = "우측1 공압\r\n채널1 온";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Name = "btn_IRCutProcess_A_R1Destruction_Ch1On";
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.TabIndex = 69;
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Text = "우측1 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Location = new System.Drawing.Point(112, 85);
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Name = "btn_IRCutProcess_A_R1Destruction_Ch1Off";
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.TabIndex = 70;
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Text = "우측1 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Location = new System.Drawing.Point(221, 85);
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Name = "btn_IRCutProcess_A_R2Destruction_Ch1On";
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.TabIndex = 71;
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Text = "우측2 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Location = new System.Drawing.Point(330, 85);
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Name = "btn_IRCutProcess_A_R2Destruction_Ch1Off";
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.TabIndex = 72;
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Text = "우측2 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Location = new System.Drawing.Point(3, 126);
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Name = "btn_IRCutProcess_A_R1Destruction_Ch2On";
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.TabIndex = 73;
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Text = "우측1 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Location = new System.Drawing.Point(112, 126);
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Name = "btn_IRCutProcess_A_R1Destruction_Ch2Off";
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.TabIndex = 74;
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Text = "우측1 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Location = new System.Drawing.Point(330, 126);
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Name = "btn_IRCutProcess_A_R2Destruction_Ch2Off";
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.TabIndex = 76;
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Text = "우측2 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Location = new System.Drawing.Point(221, 126);
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Name = "btn_IRCutProcess_A_R2Destruction_Ch2On";
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.TabIndex = 75;
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Text = "우측2 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_A
            // 
            this.gbox_IRCutProcess_A.Controls.Add(this.tableLayoutPanel35);
            this.gbox_IRCutProcess_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_A.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_A.Location = new System.Drawing.Point(9, -3);
            this.gbox_IRCutProcess_A.Name = "gbox_IRCutProcess_A";
            this.gbox_IRCutProcess_A.Size = new System.Drawing.Size(449, 192);
            this.gbox_IRCutProcess_A.TabIndex = 54;
            this.gbox_IRCutProcess_A.TabStop = false;
            this.gbox_IRCutProcess_A.Text = "     A    ";
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 4;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch2Off, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch2On, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch2Off, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch2On, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off, 1, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch1On, 2, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off, 3, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off, 1, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch2On, 2, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off, 3, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch2On, 0, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch1On, 0, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch1On, 0, 2);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch1Off, 1, 2);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch1On, 2, 2);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch1Off, 3, 2);
            this.tableLayoutPanel35.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 4;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(437, 165);
            this.tableLayoutPanel35.TabIndex = 54;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Location = new System.Drawing.Point(112, 126);
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Name = "btn_IRCutProcess_A_L1Destruction_Ch2Off";
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.TabIndex = 76;
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Text = "좌측1 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Location = new System.Drawing.Point(3, 126);
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Name = "btn_IRCutProcess_A_L1Destruction_Ch2On";
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.TabIndex = 75;
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Text = "좌측1 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Location = new System.Drawing.Point(330, 126);
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Name = "btn_IRCutProcess_A_L2Destruction_Ch2Off";
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.TabIndex = 74;
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Text = "좌측2 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Location = new System.Drawing.Point(221, 126);
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Name = "btn_IRCutProcess_A_L2Destruction_Ch2On";
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.TabIndex = 73;
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Text = "좌측2 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Location = new System.Drawing.Point(112, 3);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch1Off";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.TabIndex = 60;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Text = "좌측1 공압\r\n채널1 오프";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Location = new System.Drawing.Point(221, 3);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch1On";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.TabIndex = 63;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Text = "좌측2 공압\r\n채널1 온";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Location = new System.Drawing.Point(330, 3);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch1Off";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.TabIndex = 62;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Text = "좌측2 공압\r\n채널1 오프";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Location = new System.Drawing.Point(112, 44);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch2Off";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.TabIndex = 65;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Text = "좌측1 공압\r\n채널2 오프";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Location = new System.Drawing.Point(221, 44);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch2On";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.TabIndex = 64;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Text = "좌측2 공압\r\n채널2 온";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Location = new System.Drawing.Point(330, 44);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch2Off";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Size = new System.Drawing.Size(104, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.TabIndex = 68;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Text = "좌측2 공압\r\n채널2 오프";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch2On";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.TabIndex = 61;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Text = "좌측1 공압\r\n채널2 온";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch1On";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.TabIndex = 59;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Text = "좌측1 공압\r\n채널1 온";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Name = "btn_IRCutProcess_A_L1Destruction_Ch1On";
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.TabIndex = 69;
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Text = "좌측1 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Location = new System.Drawing.Point(112, 85);
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Name = "btn_IRCutProcess_A_L1Destruction_Ch1Off";
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.TabIndex = 70;
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Text = "좌측1 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Location = new System.Drawing.Point(221, 85);
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Name = "btn_IRCutProcess_A_L2Destruction_Ch1On";
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.TabIndex = 71;
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Text = "좌측2 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Location = new System.Drawing.Point(330, 85);
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Name = "btn_IRCutProcess_A_L2Destruction_Ch1Off";
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.TabIndex = 72;
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Text = "좌측2 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // plCellInTransfer01
            // 
            this.plCellInTransfer01.Controls.Add(this.gbox_CameraUnit_Move);
            this.plCellInTransfer01.Location = new System.Drawing.Point(22, 3124);
            this.plCellInTransfer01.Name = "plCellInTransfer01";
            this.plCellInTransfer01.Size = new System.Drawing.Size(922, 371);
            this.plCellInTransfer01.TabIndex = 80;
            // 
            // gbox_CameraUnit_Move
            // 
            this.gbox_CameraUnit_Move.Controls.Add(this.gbox_CameraUnit_MoveB);
            this.gbox_CameraUnit_Move.Controls.Add(this.gbox_CameraUnit_MoveA);
            this.gbox_CameraUnit_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CameraUnit_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CameraUnit_Move.Location = new System.Drawing.Point(8, 184);
            this.gbox_CameraUnit_Move.Name = "gbox_CameraUnit_Move";
            this.gbox_CameraUnit_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CameraUnit_Move.TabIndex = 54;
            this.gbox_CameraUnit_Move.TabStop = false;
            this.gbox_CameraUnit_Move.Text = "     이동     ";
            // 
            // gbox_CameraUnit_MoveB
            // 
            this.gbox_CameraUnit_MoveB.Controls.Add(this.tableLayoutPanel65);
            this.gbox_CameraUnit_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CameraUnit_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CameraUnit_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gbox_CameraUnit_MoveB.Name = "gbox_CameraUnit_MoveB";
            this.gbox_CameraUnit_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gbox_CameraUnit_MoveB.TabIndex = 31;
            this.gbox_CameraUnit_MoveB.TabStop = false;
            this.gbox_CameraUnit_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel65
            // 
            this.tableLayoutPanel65.ColumnCount = 2;
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel65.Controls.Add(this.btn_CameraUnit_MoveB_InspectionMove4, 0, 0);
            this.tableLayoutPanel65.Controls.Add(this.btn_CameraUnit_MoveB_InspectionMove3, 0, 0);
            this.tableLayoutPanel65.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel65.Name = "tableLayoutPanel65";
            this.tableLayoutPanel65.RowCount = 1;
            this.tableLayoutPanel65.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel65.Size = new System.Drawing.Size(431, 112);
            this.tableLayoutPanel65.TabIndex = 55;
            // 
            // btn_CameraUnit_MoveB_InspectionMove4
            // 
            this.btn_CameraUnit_MoveB_InspectionMove4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveB_InspectionMove4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveB_InspectionMove4.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveB_InspectionMove4.Location = new System.Drawing.Point(218, 3);
            this.btn_CameraUnit_MoveB_InspectionMove4.Name = "btn_CameraUnit_MoveB_InspectionMove4";
            this.btn_CameraUnit_MoveB_InspectionMove4.Size = new System.Drawing.Size(210, 106);
            this.btn_CameraUnit_MoveB_InspectionMove4.TabIndex = 65;
            this.btn_CameraUnit_MoveB_InspectionMove4.Text = "Inspection 위치\r\n이동 4";
            this.btn_CameraUnit_MoveB_InspectionMove4.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_MoveB_InspectionMove3
            // 
            this.btn_CameraUnit_MoveB_InspectionMove3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveB_InspectionMove3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveB_InspectionMove3.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveB_InspectionMove3.Location = new System.Drawing.Point(3, 3);
            this.btn_CameraUnit_MoveB_InspectionMove3.Name = "btn_CameraUnit_MoveB_InspectionMove3";
            this.btn_CameraUnit_MoveB_InspectionMove3.Size = new System.Drawing.Size(209, 106);
            this.btn_CameraUnit_MoveB_InspectionMove3.TabIndex = 64;
            this.btn_CameraUnit_MoveB_InspectionMove3.Text = "Inspection 위치\r\n이동 3";
            this.btn_CameraUnit_MoveB_InspectionMove3.UseVisualStyleBackColor = false;
            // 
            // gbox_CameraUnit_MoveA
            // 
            this.gbox_CameraUnit_MoveA.Controls.Add(this.tableLayoutPanel64);
            this.gbox_CameraUnit_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CameraUnit_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CameraUnit_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_CameraUnit_MoveA.Name = "gbox_CameraUnit_MoveA";
            this.gbox_CameraUnit_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gbox_CameraUnit_MoveA.TabIndex = 30;
            this.gbox_CameraUnit_MoveA.TabStop = false;
            this.gbox_CameraUnit_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel64
            // 
            this.tableLayoutPanel64.ColumnCount = 2;
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel64.Controls.Add(this.btn_CameraUnit_MoveA_InspectionMove2, 0, 0);
            this.tableLayoutPanel64.Controls.Add(this.btn_CameraUnit_MoveA_InspectionMove1, 0, 0);
            this.tableLayoutPanel64.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel64.Name = "tableLayoutPanel64";
            this.tableLayoutPanel64.RowCount = 1;
            this.tableLayoutPanel64.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel64.Size = new System.Drawing.Size(431, 112);
            this.tableLayoutPanel64.TabIndex = 54;
            // 
            // btn_CameraUnit_MoveA_InspectionMove2
            // 
            this.btn_CameraUnit_MoveA_InspectionMove2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveA_InspectionMove2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveA_InspectionMove2.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveA_InspectionMove2.Location = new System.Drawing.Point(218, 3);
            this.btn_CameraUnit_MoveA_InspectionMove2.Name = "btn_CameraUnit_MoveA_InspectionMove2";
            this.btn_CameraUnit_MoveA_InspectionMove2.Size = new System.Drawing.Size(210, 106);
            this.btn_CameraUnit_MoveA_InspectionMove2.TabIndex = 65;
            this.btn_CameraUnit_MoveA_InspectionMove2.Text = "Inspection 위치\r\n이동 2";
            this.btn_CameraUnit_MoveA_InspectionMove2.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_MoveA_InspectionMove1
            // 
            this.btn_CameraUnit_MoveA_InspectionMove1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveA_InspectionMove1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveA_InspectionMove1.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveA_InspectionMove1.Location = new System.Drawing.Point(3, 3);
            this.btn_CameraUnit_MoveA_InspectionMove1.Name = "btn_CameraUnit_MoveA_InspectionMove1";
            this.btn_CameraUnit_MoveA_InspectionMove1.Size = new System.Drawing.Size(209, 106);
            this.btn_CameraUnit_MoveA_InspectionMove1.TabIndex = 64;
            this.btn_CameraUnit_MoveA_InspectionMove1.Text = "Inspection 위치\r\n이동 1";
            this.btn_CameraUnit_MoveA_InspectionMove1.UseVisualStyleBackColor = false;
            // 
            // plCstUnloader01
            // 
            this.plCstUnloader01.Controls.Add(this.gbox_CellInput_Buffer);
            this.plCstUnloader01.Controls.Add(this.gbox_CellInput_Move);
            this.plCstUnloader01.Controls.Add(this.gbox_CellInput_B);
            this.plCstUnloader01.Controls.Add(this.gbox_CellInput_A);
            this.plCstUnloader01.Location = new System.Drawing.Point(22, 3511);
            this.plCstUnloader01.Name = "plCstUnloader01";
            this.plCstUnloader01.Size = new System.Drawing.Size(922, 371);
            this.plCstUnloader01.TabIndex = 81;
            // 
            // gbox_CellInput_Buffer
            // 
            this.gbox_CellInput_Buffer.Controls.Add(this.tableLayoutPanel66);
            this.gbox_CellInput_Buffer.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_Buffer.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_Buffer.Location = new System.Drawing.Point(701, 3);
            this.gbox_CellInput_Buffer.Name = "gbox_CellInput_Buffer";
            this.gbox_CellInput_Buffer.Size = new System.Drawing.Size(212, 179);
            this.gbox_CellInput_Buffer.TabIndex = 58;
            this.gbox_CellInput_Buffer.TabStop = false;
            this.gbox_CellInput_Buffer.Text = "     BUFFER    ";
            // 
            // tableLayoutPanel66
            // 
            this.tableLayoutPanel66.ColumnCount = 2;
            this.tableLayoutPanel66.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel66.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_DestructionOn, 0, 2);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_DestructionOff, 0, 2);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PickerUp, 0, 0);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PickerDown, 1, 0);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PneumaticOn, 0, 1);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PneumaticOff, 1, 1);
            this.tableLayoutPanel66.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel66.Name = "tableLayoutPanel66";
            this.tableLayoutPanel66.RowCount = 3;
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.Size = new System.Drawing.Size(200, 141);
            this.tableLayoutPanel66.TabIndex = 55;
            // 
            // btn_CellInput_Buffer_DestructionOn
            // 
            this.btn_CellInput_Buffer_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_DestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_DestructionOn.Location = new System.Drawing.Point(3, 97);
            this.btn_CellInput_Buffer_DestructionOn.Name = "btn_CellInput_Buffer_DestructionOn";
            this.btn_CellInput_Buffer_DestructionOn.Size = new System.Drawing.Size(94, 41);
            this.btn_CellInput_Buffer_DestructionOn.TabIndex = 63;
            this.btn_CellInput_Buffer_DestructionOn.Text = "파기 온";
            this.btn_CellInput_Buffer_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_DestructionOff
            // 
            this.btn_CellInput_Buffer_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_DestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_DestructionOff.Location = new System.Drawing.Point(103, 97);
            this.btn_CellInput_Buffer_DestructionOff.Name = "btn_CellInput_Buffer_DestructionOff";
            this.btn_CellInput_Buffer_DestructionOff.Size = new System.Drawing.Size(94, 41);
            this.btn_CellInput_Buffer_DestructionOff.TabIndex = 62;
            this.btn_CellInput_Buffer_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_Buffer_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PickerUp
            // 
            this.btn_CellInput_Buffer_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PickerUp.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_Buffer_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_Buffer_PickerUp.Name = "btn_CellInput_Buffer_PickerUp";
            this.btn_CellInput_Buffer_PickerUp.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PickerUp.TabIndex = 58;
            this.btn_CellInput_Buffer_PickerUp.Text = "버퍼 피커\r\n업";
            this.btn_CellInput_Buffer_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PickerDown
            // 
            this.btn_CellInput_Buffer_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PickerDown.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_Buffer_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PickerDown.Location = new System.Drawing.Point(103, 3);
            this.btn_CellInput_Buffer_PickerDown.Name = "btn_CellInput_Buffer_PickerDown";
            this.btn_CellInput_Buffer_PickerDown.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PickerDown.TabIndex = 60;
            this.btn_CellInput_Buffer_PickerDown.Text = "버퍼 피커\r\n다운";
            this.btn_CellInput_Buffer_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PneumaticOn
            // 
            this.btn_CellInput_Buffer_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PneumaticOn.Location = new System.Drawing.Point(3, 50);
            this.btn_CellInput_Buffer_PneumaticOn.Name = "btn_CellInput_Buffer_PneumaticOn";
            this.btn_CellInput_Buffer_PneumaticOn.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PneumaticOn.TabIndex = 59;
            this.btn_CellInput_Buffer_PneumaticOn.Text = "공압 온";
            this.btn_CellInput_Buffer_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PneumaticOff
            // 
            this.btn_CellInput_Buffer_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PneumaticOff.Location = new System.Drawing.Point(103, 50);
            this.btn_CellInput_Buffer_PneumaticOff.Name = "btn_CellInput_Buffer_PneumaticOff";
            this.btn_CellInput_Buffer_PneumaticOff.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PneumaticOff.TabIndex = 61;
            this.btn_CellInput_Buffer_PneumaticOff.Text = "공압 오프";
            this.btn_CellInput_Buffer_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_Move
            // 
            this.gbox_CellInput_Move.Controls.Add(this.gbox_CellInput_MoveCenter);
            this.gbox_CellInput_Move.Controls.Add(this.gbox_CellInput_MoveB);
            this.gbox_CellInput_Move.Controls.Add(this.gbox_CellInput_MoveA);
            this.gbox_CellInput_Move.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_Move.Location = new System.Drawing.Point(9, 188);
            this.gbox_CellInput_Move.Name = "gbox_CellInput_Move";
            this.gbox_CellInput_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CellInput_Move.TabIndex = 57;
            this.gbox_CellInput_Move.TabStop = false;
            this.gbox_CellInput_Move.Text = "     이동     ";
            // 
            // gbox_CellInput_MoveCenter
            // 
            this.gbox_CellInput_MoveCenter.Controls.Add(this.tableLayoutPanel69);
            this.gbox_CellInput_MoveCenter.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_MoveCenter.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_MoveCenter.Location = new System.Drawing.Point(692, 21);
            this.gbox_CellInput_MoveCenter.Name = "gbox_CellInput_MoveCenter";
            this.gbox_CellInput_MoveCenter.Size = new System.Drawing.Size(206, 152);
            this.gbox_CellInput_MoveCenter.TabIndex = 32;
            this.gbox_CellInput_MoveCenter.TabStop = false;
            this.gbox_CellInput_MoveCenter.Text = "     중간    ";
            // 
            // tableLayoutPanel69
            // 
            this.tableLayoutPanel69.ColumnCount = 2;
            this.tableLayoutPanel69.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_ABuffer, 0, 1);
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_BBuffer, 0, 1);
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_Y1CstMid, 0, 0);
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_Y2CstMid, 1, 0);
            this.tableLayoutPanel69.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel69.Name = "tableLayoutPanel69";
            this.tableLayoutPanel69.RowCount = 2;
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel69.Size = new System.Drawing.Size(200, 115);
            this.tableLayoutPanel69.TabIndex = 0;
            // 
            // btn_CellInput_MoveCenter_ABuffer
            // 
            this.btn_CellInput_MoveCenter_ABuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_ABuffer.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_ABuffer.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_ABuffer.Location = new System.Drawing.Point(3, 60);
            this.btn_CellInput_MoveCenter_ABuffer.Name = "btn_CellInput_MoveCenter_ABuffer";
            this.btn_CellInput_MoveCenter_ABuffer.Size = new System.Drawing.Size(94, 52);
            this.btn_CellInput_MoveCenter_ABuffer.TabIndex = 60;
            this.btn_CellInput_MoveCenter_ABuffer.Text = "A\r\nBUFFER";
            this.btn_CellInput_MoveCenter_ABuffer.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveCenter_BBuffer
            // 
            this.btn_CellInput_MoveCenter_BBuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_BBuffer.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_BBuffer.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_BBuffer.Location = new System.Drawing.Point(103, 60);
            this.btn_CellInput_MoveCenter_BBuffer.Name = "btn_CellInput_MoveCenter_BBuffer";
            this.btn_CellInput_MoveCenter_BBuffer.Size = new System.Drawing.Size(94, 52);
            this.btn_CellInput_MoveCenter_BBuffer.TabIndex = 59;
            this.btn_CellInput_MoveCenter_BBuffer.Text = "B\r\nBUFFER";
            this.btn_CellInput_MoveCenter_BBuffer.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveCenter_Y1CstMid
            // 
            this.btn_CellInput_MoveCenter_Y1CstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_Y1CstMid.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_Y1CstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_Y1CstMid.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_MoveCenter_Y1CstMid.Name = "btn_CellInput_MoveCenter_Y1CstMid";
            this.btn_CellInput_MoveCenter_Y1CstMid.Size = new System.Drawing.Size(94, 51);
            this.btn_CellInput_MoveCenter_Y1CstMid.TabIndex = 57;
            this.btn_CellInput_MoveCenter_Y1CstMid.Text = "Y1\r\n카세트\r\n중간";
            this.btn_CellInput_MoveCenter_Y1CstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveCenter_Y2CstMid
            // 
            this.btn_CellInput_MoveCenter_Y2CstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_Y2CstMid.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_Y2CstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_Y2CstMid.Location = new System.Drawing.Point(103, 3);
            this.btn_CellInput_MoveCenter_Y2CstMid.Name = "btn_CellInput_MoveCenter_Y2CstMid";
            this.btn_CellInput_MoveCenter_Y2CstMid.Size = new System.Drawing.Size(94, 51);
            this.btn_CellInput_MoveCenter_Y2CstMid.TabIndex = 58;
            this.btn_CellInput_MoveCenter_Y2CstMid.Text = "Y2\r\n카세트\r\n중간";
            this.btn_CellInput_MoveCenter_Y2CstMid.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_MoveB
            // 
            this.gbox_CellInput_MoveB.Controls.Add(this.tableLayoutPanel68);
            this.gbox_CellInput_MoveB.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_MoveB.Location = new System.Drawing.Point(346, 21);
            this.gbox_CellInput_MoveB.Name = "gbox_CellInput_MoveB";
            this.gbox_CellInput_MoveB.Size = new System.Drawing.Size(340, 152);
            this.gbox_CellInput_MoveB.TabIndex = 31;
            this.gbox_CellInput_MoveB.TabStop = false;
            this.gbox_CellInput_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel68
            // 
            this.tableLayoutPanel68.ColumnCount = 4;
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X2BUld, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_Y2BUld, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X2BCstMid, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X2BCstWait, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X1AUld, 0, 0);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_Y1BUld, 1, 0);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X1BCstMId, 2, 0);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X1BCstWait, 3, 0);
            this.tableLayoutPanel68.Location = new System.Drawing.Point(9, 28);
            this.tableLayoutPanel68.Name = "tableLayoutPanel68";
            this.tableLayoutPanel68.RowCount = 2;
            this.tableLayoutPanel68.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel68.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel68.Size = new System.Drawing.Size(318, 118);
            this.tableLayoutPanel68.TabIndex = 56;
            // 
            // btn_CellInput_MoveB_X2BUld
            // 
            this.btn_CellInput_MoveB_X2BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X2BUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X2BUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X2BUld.Location = new System.Drawing.Point(3, 62);
            this.btn_CellInput_MoveB_X2BUld.Name = "btn_CellInput_MoveB_X2BUld";
            this.btn_CellInput_MoveB_X2BUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X2BUld.TabIndex = 65;
            this.btn_CellInput_MoveB_X2BUld.Text = "X2\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_X2BUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_Y2BUld
            // 
            this.btn_CellInput_MoveB_Y2BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_Y2BUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_Y2BUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_Y2BUld.Location = new System.Drawing.Point(82, 62);
            this.btn_CellInput_MoveB_Y2BUld.Name = "btn_CellInput_MoveB_Y2BUld";
            this.btn_CellInput_MoveB_Y2BUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_Y2BUld.TabIndex = 64;
            this.btn_CellInput_MoveB_Y2BUld.Text = "Y2\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_Y2BUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X2BCstMid
            // 
            this.btn_CellInput_MoveB_X2BCstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X2BCstMid.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X2BCstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X2BCstMid.Location = new System.Drawing.Point(161, 62);
            this.btn_CellInput_MoveB_X2BCstMid.Name = "btn_CellInput_MoveB_X2BCstMid";
            this.btn_CellInput_MoveB_X2BCstMid.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X2BCstMid.TabIndex = 63;
            this.btn_CellInput_MoveB_X2BCstMid.Text = "X2\r\nB 카세트\r\n중간";
            this.btn_CellInput_MoveB_X2BCstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X2BCstWait
            // 
            this.btn_CellInput_MoveB_X2BCstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X2BCstWait.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X2BCstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X2BCstWait.Location = new System.Drawing.Point(240, 62);
            this.btn_CellInput_MoveB_X2BCstWait.Name = "btn_CellInput_MoveB_X2BCstWait";
            this.btn_CellInput_MoveB_X2BCstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X2BCstWait.TabIndex = 62;
            this.btn_CellInput_MoveB_X2BCstWait.Text = "X2\r\nB 카세트\r\n대기";
            this.btn_CellInput_MoveB_X2BCstWait.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X1AUld
            // 
            this.btn_CellInput_MoveB_X1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X1AUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X1AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X1AUld.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_MoveB_X1AUld.Name = "btn_CellInput_MoveB_X1AUld";
            this.btn_CellInput_MoveB_X1AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X1AUld.TabIndex = 58;
            this.btn_CellInput_MoveB_X1AUld.Text = "X1\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_X1AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_Y1BUld
            // 
            this.btn_CellInput_MoveB_Y1BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_Y1BUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_Y1BUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_Y1BUld.Location = new System.Drawing.Point(82, 3);
            this.btn_CellInput_MoveB_Y1BUld.Name = "btn_CellInput_MoveB_Y1BUld";
            this.btn_CellInput_MoveB_Y1BUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_Y1BUld.TabIndex = 59;
            this.btn_CellInput_MoveB_Y1BUld.Text = "Y1\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_Y1BUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X1BCstMId
            // 
            this.btn_CellInput_MoveB_X1BCstMId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X1BCstMId.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X1BCstMId.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X1BCstMId.Location = new System.Drawing.Point(161, 3);
            this.btn_CellInput_MoveB_X1BCstMId.Name = "btn_CellInput_MoveB_X1BCstMId";
            this.btn_CellInput_MoveB_X1BCstMId.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X1BCstMId.TabIndex = 60;
            this.btn_CellInput_MoveB_X1BCstMId.Text = "X1\r\nB 카세트\r\n중간";
            this.btn_CellInput_MoveB_X1BCstMId.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X1BCstWait
            // 
            this.btn_CellInput_MoveB_X1BCstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X1BCstWait.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X1BCstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X1BCstWait.Location = new System.Drawing.Point(240, 3);
            this.btn_CellInput_MoveB_X1BCstWait.Name = "btn_CellInput_MoveB_X1BCstWait";
            this.btn_CellInput_MoveB_X1BCstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X1BCstWait.TabIndex = 61;
            this.btn_CellInput_MoveB_X1BCstWait.Text = "X1\r\nB 카세트\r\n대기";
            this.btn_CellInput_MoveB_X1BCstWait.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_MoveA
            // 
            this.gbox_CellInput_MoveA.Controls.Add(this.tableLayoutPanel67);
            this.gbox_CellInput_MoveA.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_MoveA.Location = new System.Drawing.Point(6, 21);
            this.gbox_CellInput_MoveA.Name = "gbox_CellInput_MoveA";
            this.gbox_CellInput_MoveA.Size = new System.Drawing.Size(334, 152);
            this.gbox_CellInput_MoveA.TabIndex = 30;
            this.gbox_CellInput_MoveA.TabStop = false;
            this.gbox_CellInput_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel67
            // 
            this.tableLayoutPanel67.ColumnCount = 4;
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X2AUld, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_Y2AUld, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X2ACstMid, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X2ACstWait, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X1AUld, 0, 0);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_Y1AUld, 1, 0);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X1ACstMid, 2, 0);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X1ACstWait, 3, 0);
            this.tableLayoutPanel67.Location = new System.Drawing.Point(7, 28);
            this.tableLayoutPanel67.Name = "tableLayoutPanel67";
            this.tableLayoutPanel67.RowCount = 2;
            this.tableLayoutPanel67.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel67.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel67.Size = new System.Drawing.Size(318, 118);
            this.tableLayoutPanel67.TabIndex = 55;
            // 
            // btn_CellInput_MoveA_X2AUld
            // 
            this.btn_CellInput_MoveA_X2AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X2AUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X2AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X2AUld.Location = new System.Drawing.Point(3, 62);
            this.btn_CellInput_MoveA_X2AUld.Name = "btn_CellInput_MoveA_X2AUld";
            this.btn_CellInput_MoveA_X2AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X2AUld.TabIndex = 65;
            this.btn_CellInput_MoveA_X2AUld.Text = "X2\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_X2AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_Y2AUld
            // 
            this.btn_CellInput_MoveA_Y2AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_Y2AUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_Y2AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_Y2AUld.Location = new System.Drawing.Point(82, 62);
            this.btn_CellInput_MoveA_Y2AUld.Name = "btn_CellInput_MoveA_Y2AUld";
            this.btn_CellInput_MoveA_Y2AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_Y2AUld.TabIndex = 64;
            this.btn_CellInput_MoveA_Y2AUld.Text = "Y2\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_Y2AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X2ACstMid
            // 
            this.btn_CellInput_MoveA_X2ACstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X2ACstMid.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X2ACstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X2ACstMid.Location = new System.Drawing.Point(161, 62);
            this.btn_CellInput_MoveA_X2ACstMid.Name = "btn_CellInput_MoveA_X2ACstMid";
            this.btn_CellInput_MoveA_X2ACstMid.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X2ACstMid.TabIndex = 63;
            this.btn_CellInput_MoveA_X2ACstMid.Text = "X2\r\nA 카세트\r\n중간";
            this.btn_CellInput_MoveA_X2ACstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X2ACstWait
            // 
            this.btn_CellInput_MoveA_X2ACstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X2ACstWait.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X2ACstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X2ACstWait.Location = new System.Drawing.Point(240, 62);
            this.btn_CellInput_MoveA_X2ACstWait.Name = "btn_CellInput_MoveA_X2ACstWait";
            this.btn_CellInput_MoveA_X2ACstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X2ACstWait.TabIndex = 62;
            this.btn_CellInput_MoveA_X2ACstWait.Text = "X2\r\nA 카세트\r\n대기";
            this.btn_CellInput_MoveA_X2ACstWait.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X1AUld
            // 
            this.btn_CellInput_MoveA_X1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X1AUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X1AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X1AUld.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_MoveA_X1AUld.Name = "btn_CellInput_MoveA_X1AUld";
            this.btn_CellInput_MoveA_X1AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X1AUld.TabIndex = 58;
            this.btn_CellInput_MoveA_X1AUld.Text = "X1\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_X1AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_Y1AUld
            // 
            this.btn_CellInput_MoveA_Y1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_Y1AUld.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_Y1AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_Y1AUld.Location = new System.Drawing.Point(82, 3);
            this.btn_CellInput_MoveA_Y1AUld.Name = "btn_CellInput_MoveA_Y1AUld";
            this.btn_CellInput_MoveA_Y1AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_Y1AUld.TabIndex = 59;
            this.btn_CellInput_MoveA_Y1AUld.Text = "Y1\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_Y1AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X1ACstMid
            // 
            this.btn_CellInput_MoveA_X1ACstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X1ACstMid.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X1ACstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X1ACstMid.Location = new System.Drawing.Point(161, 3);
            this.btn_CellInput_MoveA_X1ACstMid.Name = "btn_CellInput_MoveA_X1ACstMid";
            this.btn_CellInput_MoveA_X1ACstMid.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X1ACstMid.TabIndex = 60;
            this.btn_CellInput_MoveA_X1ACstMid.Text = "X1\r\nA 카세트\r\n중간";
            this.btn_CellInput_MoveA_X1ACstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X1ACstWait
            // 
            this.btn_CellInput_MoveA_X1ACstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X1ACstWait.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X1ACstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X1ACstWait.Location = new System.Drawing.Point(240, 3);
            this.btn_CellInput_MoveA_X1ACstWait.Name = "btn_CellInput_MoveA_X1ACstWait";
            this.btn_CellInput_MoveA_X1ACstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X1ACstWait.TabIndex = 61;
            this.btn_CellInput_MoveA_X1ACstWait.Text = "X1\r\nA 카세트\r\n대기";
            this.btn_CellInput_MoveA_X1ACstWait.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_B
            // 
            this.gbox_CellInput_B.Controls.Add(this.tableLayoutPanel23);
            this.gbox_CellInput_B.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_B.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_B.Location = new System.Drawing.Point(355, 3);
            this.gbox_CellInput_B.Name = "gbox_CellInput_B";
            this.gbox_CellInput_B.Size = new System.Drawing.Size(340, 179);
            this.gbox_CellInput_B.TabIndex = 56;
            this.gbox_CellInput_B.TabStop = false;
            this.gbox_CellInput_B.Text = "     B     ";
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_DestructionOn, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_DestructionOff, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_PneumaticOff, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_PneumaticOn, 0, 0);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel23.TabIndex = 55;
            // 
            // btn_CellInput_B_DestructionOn
            // 
            this.btn_CellInput_B_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_DestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_CellInput_B_DestructionOn.Name = "btn_CellInput_B_DestructionOn";
            this.btn_CellInput_B_DestructionOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_DestructionOn.TabIndex = 58;
            this.btn_CellInput_B_DestructionOn.Text = "파기 온";
            this.btn_CellInput_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_B_DestructionOff
            // 
            this.btn_CellInput_B_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_DestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_DestructionOff.Location = new System.Drawing.Point(167, 75);
            this.btn_CellInput_B_DestructionOff.Name = "btn_CellInput_B_DestructionOff";
            this.btn_CellInput_B_DestructionOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_DestructionOff.TabIndex = 56;
            this.btn_CellInput_B_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_B_PneumaticOff
            // 
            this.btn_CellInput_B_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_PneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_PneumaticOff.Location = new System.Drawing.Point(167, 3);
            this.btn_CellInput_B_PneumaticOff.Name = "btn_CellInput_B_PneumaticOff";
            this.btn_CellInput_B_PneumaticOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_PneumaticOff.TabIndex = 55;
            this.btn_CellInput_B_PneumaticOff.Text = "공압 오프";
            this.btn_CellInput_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_B_PneumaticOn
            // 
            this.btn_CellInput_B_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_PneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_B_PneumaticOn.Name = "btn_CellInput_B_PneumaticOn";
            this.btn_CellInput_B_PneumaticOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_PneumaticOn.TabIndex = 57;
            this.btn_CellInput_B_PneumaticOn.Text = "공압 온";
            this.btn_CellInput_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_A
            // 
            this.gbox_CellInput_A.Controls.Add(this.tableLayoutPanel22);
            this.gbox_CellInput_A.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_A.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_A.Location = new System.Drawing.Point(9, 3);
            this.gbox_CellInput_A.Name = "gbox_CellInput_A";
            this.gbox_CellInput_A.Size = new System.Drawing.Size(340, 179);
            this.gbox_CellInput_A.TabIndex = 55;
            this.gbox_CellInput_A.TabStop = false;
            this.gbox_CellInput_A.Text = "     A    ";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_DestructionOn, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_DestructionOff, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_PneumaticOff, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_PneumaticOn, 0, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel22.TabIndex = 55;
            // 
            // btn_CellInput_A_DestructionOn
            // 
            this.btn_CellInput_A_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_DestructionOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_CellInput_A_DestructionOn.Name = "btn_CellInput_A_DestructionOn";
            this.btn_CellInput_A_DestructionOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_DestructionOn.TabIndex = 58;
            this.btn_CellInput_A_DestructionOn.Text = "파기 온";
            this.btn_CellInput_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_A_DestructionOff
            // 
            this.btn_CellInput_A_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_DestructionOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_DestructionOff.Location = new System.Drawing.Point(167, 75);
            this.btn_CellInput_A_DestructionOff.Name = "btn_CellInput_A_DestructionOff";
            this.btn_CellInput_A_DestructionOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_DestructionOff.TabIndex = 56;
            this.btn_CellInput_A_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_A_PneumaticOff
            // 
            this.btn_CellInput_A_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_PneumaticOff.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_PneumaticOff.Location = new System.Drawing.Point(167, 3);
            this.btn_CellInput_A_PneumaticOff.Name = "btn_CellInput_A_PneumaticOff";
            this.btn_CellInput_A_PneumaticOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_PneumaticOff.TabIndex = 55;
            this.btn_CellInput_A_PneumaticOff.Text = "공압 오프";
            this.btn_CellInput_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_A_PneumaticOn
            // 
            this.btn_CellInput_A_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_PneumaticOn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_A_PneumaticOn.Name = "btn_CellInput_A_PneumaticOn";
            this.btn_CellInput_A_PneumaticOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_PneumaticOn.TabIndex = 57;
            this.btn_CellInput_A_PneumaticOn.Text = "공압 온";
            this.btn_CellInput_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // plCstLoad02
            // 
            this.plCstLoad02.Controls.Add(this.gbox_CasseteUnload);
            this.plCstLoad02.Location = new System.Drawing.Point(1023, 98);
            this.plCstLoad02.Name = "plCstLoad02";
            this.plCstLoad02.Size = new System.Drawing.Size(220, 193);
            this.plCstLoad02.TabIndex = 82;
            // 
            // gbox_CasseteUnload
            // 
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting4);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting2);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting3);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting1);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_MutingOut);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_MutingIn);
            this.gbox_CasseteUnload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteUnload.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteUnload.Location = new System.Drawing.Point(4, 4);
            this.gbox_CasseteUnload.Name = "gbox_CasseteUnload";
            this.gbox_CasseteUnload.Size = new System.Drawing.Size(212, 181);
            this.gbox_CasseteUnload.TabIndex = 50;
            this.gbox_CasseteUnload.TabStop = false;
            this.gbox_CasseteUnload.Text = "     뮤팅     ";
            // 
            // btn_CasseteUnload_Muting4
            // 
            this.btn_CasseteUnload_Muting4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting4.Location = new System.Drawing.Point(107, 125);
            this.btn_CasseteUnload_Muting4.Name = "btn_CasseteUnload_Muting4";
            this.btn_CasseteUnload_Muting4.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting4.TabIndex = 5;
            this.btn_CasseteUnload_Muting4.Text = "뮤팅 4";
            this.btn_CasseteUnload_Muting4.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting2
            // 
            this.btn_CasseteUnload_Muting2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting2.Location = new System.Drawing.Point(6, 125);
            this.btn_CasseteUnload_Muting2.Name = "btn_CasseteUnload_Muting2";
            this.btn_CasseteUnload_Muting2.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting2.TabIndex = 4;
            this.btn_CasseteUnload_Muting2.Text = "뮤팅 2";
            this.btn_CasseteUnload_Muting2.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting3
            // 
            this.btn_CasseteUnload_Muting3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting3.Location = new System.Drawing.Point(107, 75);
            this.btn_CasseteUnload_Muting3.Name = "btn_CasseteUnload_Muting3";
            this.btn_CasseteUnload_Muting3.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting3.TabIndex = 3;
            this.btn_CasseteUnload_Muting3.Text = "뮤팅 3";
            this.btn_CasseteUnload_Muting3.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting1
            // 
            this.btn_CasseteUnload_Muting1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting1.Location = new System.Drawing.Point(6, 75);
            this.btn_CasseteUnload_Muting1.Name = "btn_CasseteUnload_Muting1";
            this.btn_CasseteUnload_Muting1.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting1.TabIndex = 2;
            this.btn_CasseteUnload_Muting1.Text = "뮤팅 1";
            this.btn_CasseteUnload_Muting1.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MutingOut
            // 
            this.btn_CasseteUnload_MutingOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MutingOut.Location = new System.Drawing.Point(107, 29);
            this.btn_CasseteUnload_MutingOut.Name = "btn_CasseteUnload_MutingOut";
            this.btn_CasseteUnload_MutingOut.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_MutingOut.TabIndex = 1;
            this.btn_CasseteUnload_MutingOut.Text = "뮤팅 아웃";
            this.btn_CasseteUnload_MutingOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MutingIn
            // 
            this.btn_CasseteUnload_MutingIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MutingIn.Location = new System.Drawing.Point(6, 28);
            this.btn_CasseteUnload_MutingIn.Name = "btn_CasseteUnload_MutingIn";
            this.btn_CasseteUnload_MutingIn.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_MutingIn.TabIndex = 0;
            this.btn_CasseteUnload_MutingIn.Text = "뮤팅 인";
            this.btn_CasseteUnload_MutingIn.UseVisualStyleBackColor = false;
            // 
            // plCellInTransfer02
            // 
            this.plCellInTransfer02.Controls.Add(this.groupBox1);
            this.plCellInTransfer02.Location = new System.Drawing.Point(1033, 3196);
            this.plCellInTransfer02.Name = "plCellInTransfer02";
            this.plCellInTransfer02.Size = new System.Drawing.Size(220, 193);
            this.plCellInTransfer02.TabIndex = 85;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(4, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 181);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "     이오나이저     ";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(107, 103);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 72);
            this.button1.TabIndex = 6;
            this.button1.Text = "파기 오프";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(107, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 72);
            this.button2.TabIndex = 5;
            this.button2.Text = "이오나이저\r\n오프";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button3.Location = new System.Drawing.Point(6, 103);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(99, 72);
            this.button3.TabIndex = 4;
            this.button3.Text = "파기 온";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button4.Location = new System.Drawing.Point(6, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(99, 72);
            this.button4.TabIndex = 3;
            this.button4.Text = "이오나이저\r\n온";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // plBreakUnit04
            // 
            this.plBreakUnit04.Controls.Add(this.gbox_BreakUnitTY_Ionizer);
            this.plBreakUnit04.Location = new System.Drawing.Point(1033, 2424);
            this.plBreakUnit04.Name = "plBreakUnit04";
            this.plBreakUnit04.Size = new System.Drawing.Size(220, 193);
            this.plBreakUnit04.TabIndex = 86;
            // 
            // gbox_BreakUnitTY_Ionizer
            // 
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_DestructionOff);
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_IonizerOff);
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_DestructionOn);
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_IonizerOn);
            this.gbox_BreakUnitTY_Ionizer.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Ionizer.Location = new System.Drawing.Point(4, 6);
            this.gbox_BreakUnitTY_Ionizer.Name = "gbox_BreakUnitTY_Ionizer";
            this.gbox_BreakUnitTY_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gbox_BreakUnitTY_Ionizer.TabIndex = 55;
            this.gbox_BreakUnitTY_Ionizer.TabStop = false;
            this.gbox_BreakUnitTY_Ionizer.Text = "     이오나이저     ";
            // 
            // btn_BreakUnitTY_DestructionOff
            // 
            this.btn_BreakUnitTY_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btn_BreakUnitTY_DestructionOff.Name = "btn_BreakUnitTY_DestructionOff";
            this.btn_BreakUnitTY_DestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_DestructionOff.TabIndex = 6;
            this.btn_BreakUnitTY_DestructionOff.Text = "파기 오프";
            this.btn_BreakUnitTY_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_IonizerOff
            // 
            this.btn_BreakUnitTY_IonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_IonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btn_BreakUnitTY_IonizerOff.Name = "btn_BreakUnitTY_IonizerOff";
            this.btn_BreakUnitTY_IonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_IonizerOff.TabIndex = 5;
            this.btn_BreakUnitTY_IonizerOff.Text = "이오나이저\r\n오프";
            this.btn_BreakUnitTY_IonizerOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_DestructionOn
            // 
            this.btn_BreakUnitTY_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btn_BreakUnitTY_DestructionOn.Name = "btn_BreakUnitTY_DestructionOn";
            this.btn_BreakUnitTY_DestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_DestructionOn.TabIndex = 4;
            this.btn_BreakUnitTY_DestructionOn.Text = "파기 온";
            this.btn_BreakUnitTY_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_IonizerOn
            // 
            this.btn_BreakUnitTY_IonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_IonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btn_BreakUnitTY_IonizerOn.Name = "btn_BreakUnitTY_IonizerOn";
            this.btn_BreakUnitTY_IonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_IonizerOn.TabIndex = 3;
            this.btn_BreakUnitTY_IonizerOn.Text = "이오나이저\r\n온";
            this.btn_BreakUnitTY_IonizerOn.UseVisualStyleBackColor = false;
            // 
            // plIRCutProcess02
            // 
            this.plIRCutProcess02.Controls.Add(this.gbox_IRCutProcess_Ionizer);
            this.plIRCutProcess02.Location = new System.Drawing.Point(1023, 1661);
            this.plIRCutProcess02.Name = "plIRCutProcess02";
            this.plIRCutProcess02.Size = new System.Drawing.Size(220, 193);
            this.plIRCutProcess02.TabIndex = 87;
            // 
            // gbox_IRCutProcess_Ionizer
            // 
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_Destruction);
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_IonizerOff);
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_DestructionOn);
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_IonizerOn);
            this.gbox_IRCutProcess_Ionizer.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_Ionizer.Location = new System.Drawing.Point(4, 6);
            this.gbox_IRCutProcess_Ionizer.Name = "gbox_IRCutProcess_Ionizer";
            this.gbox_IRCutProcess_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gbox_IRCutProcess_Ionizer.TabIndex = 51;
            this.gbox_IRCutProcess_Ionizer.TabStop = false;
            this.gbox_IRCutProcess_Ionizer.Text = "     이오나이저     ";
            // 
            // btn_IRCutProcess_Destruction
            // 
            this.btn_IRCutProcess_Destruction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_Destruction.Location = new System.Drawing.Point(107, 103);
            this.btn_IRCutProcess_Destruction.Name = "btn_IRCutProcess_Destruction";
            this.btn_IRCutProcess_Destruction.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_Destruction.TabIndex = 6;
            this.btn_IRCutProcess_Destruction.Text = "파기 오프";
            this.btn_IRCutProcess_Destruction.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_IonizerOff
            // 
            this.btn_IRCutProcess_IonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_IonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btn_IRCutProcess_IonizerOff.Name = "btn_IRCutProcess_IonizerOff";
            this.btn_IRCutProcess_IonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_IonizerOff.TabIndex = 5;
            this.btn_IRCutProcess_IonizerOff.Text = "이오나이저\r\n오프";
            this.btn_IRCutProcess_IonizerOff.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_DestructionOn
            // 
            this.btn_IRCutProcess_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_DestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btn_IRCutProcess_DestructionOn.Name = "btn_IRCutProcess_DestructionOn";
            this.btn_IRCutProcess_DestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_DestructionOn.TabIndex = 4;
            this.btn_IRCutProcess_DestructionOn.Text = "파기 온";
            this.btn_IRCutProcess_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_IonizerOn
            // 
            this.btn_IRCutProcess_IonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_IonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btn_IRCutProcess_IonizerOn.Name = "btn_IRCutProcess_IonizerOn";
            this.btn_IRCutProcess_IonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_IonizerOn.TabIndex = 3;
            this.btn_IRCutProcess_IonizerOn.Text = "이오나이저\r\n온";
            this.btn_IRCutProcess_IonizerOn.UseVisualStyleBackColor = false;
            // 
            // plCstUnloader02
            // 
            this.plCstUnloader02.Controls.Add(this.groupBox2);
            this.plCstUnloader02.Location = new System.Drawing.Point(1033, 3592);
            this.plCstUnloader02.Name = "plCstUnloader02";
            this.plCstUnloader02.Size = new System.Drawing.Size(220, 193);
            this.plCstUnloader02.TabIndex = 88;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.button9);
            this.groupBox2.Controls.Add(this.button10);
            this.groupBox2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(4, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 181);
            this.groupBox2.TabIndex = 50;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "     뮤팅     ";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button5.Location = new System.Drawing.Point(107, 125);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(99, 44);
            this.button5.TabIndex = 5;
            this.button5.Text = "뮤팅 4";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button6.Location = new System.Drawing.Point(6, 125);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(99, 44);
            this.button6.TabIndex = 4;
            this.button6.Text = "뮤팅 2";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button7.Location = new System.Drawing.Point(107, 75);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(99, 44);
            this.button7.TabIndex = 3;
            this.button7.Text = "뮤팅 3";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button8.Location = new System.Drawing.Point(6, 75);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(99, 44);
            this.button8.TabIndex = 2;
            this.button8.Text = "뮤팅 1";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button9.Location = new System.Drawing.Point(107, 29);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(99, 44);
            this.button9.TabIndex = 1;
            this.button9.Text = "뮤팅 아웃";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button10.Location = new System.Drawing.Point(6, 28);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(99, 44);
            this.button10.TabIndex = 0;
            this.button10.Text = "뮤팅 인";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // plCellOutTransfer02
            // 
            this.plCellOutTransfer02.Controls.Add(this.gbox_CellInput_Ionizer);
            this.plCellOutTransfer02.Location = new System.Drawing.Point(1013, 506);
            this.plCellOutTransfer02.Name = "plCellOutTransfer02";
            this.plCellOutTransfer02.Size = new System.Drawing.Size(220, 193);
            this.plCellOutTransfer02.TabIndex = 84;
            // 
            // gbox_CellInput_Ionizer
            // 
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_DestructionOff);
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_IonizerOff);
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_DestructionOn);
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_IonizerOn);
            this.gbox_CellInput_Ionizer.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_Ionizer.Location = new System.Drawing.Point(4, 6);
            this.gbox_CellInput_Ionizer.Name = "gbox_CellInput_Ionizer";
            this.gbox_CellInput_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gbox_CellInput_Ionizer.TabIndex = 51;
            this.gbox_CellInput_Ionizer.TabStop = false;
            this.gbox_CellInput_Ionizer.Text = "     이오나이저     ";
            // 
            // btn_CellInput_DestructionOff
            // 
            this.btn_CellInput_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_DestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btn_CellInput_DestructionOff.Name = "btn_CellInput_DestructionOff";
            this.btn_CellInput_DestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_DestructionOff.TabIndex = 6;
            this.btn_CellInput_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_IonizerOff
            // 
            this.btn_CellInput_IonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_IonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btn_CellInput_IonizerOff.Name = "btn_CellInput_IonizerOff";
            this.btn_CellInput_IonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_IonizerOff.TabIndex = 5;
            this.btn_CellInput_IonizerOff.Text = "이오나이저\r\n오프";
            this.btn_CellInput_IonizerOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_DestructionOn
            // 
            this.btn_CellInput_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_DestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btn_CellInput_DestructionOn.Name = "btn_CellInput_DestructionOn";
            this.btn_CellInput_DestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_DestructionOn.TabIndex = 4;
            this.btn_CellInput_DestructionOn.Text = "파기 온";
            this.btn_CellInput_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_IonizerOn
            // 
            this.btn_CellInput_IonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_IonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btn_CellInput_IonizerOn.Name = "btn_CellInput_IonizerOn";
            this.btn_CellInput_IonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_IonizerOn.TabIndex = 3;
            this.btn_CellInput_IonizerOn.Text = "이오나이저\r\n온";
            this.btn_CellInput_IonizerOn.UseVisualStyleBackColor = false;
            // 
            // ParameterTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.plCellInTransfer02);
            this.Controls.Add(this.plCstLoad02);
            this.Controls.Add(this.plCstUnloader01);
            this.Controls.Add(this.plBreakUnit04);
            this.Controls.Add(this.plCellInTransfer01);
            this.Controls.Add(this.plIRCutProcess02);
            this.Controls.Add(this.plIRCutProcess01);
            this.Controls.Add(this.plCellOutTransfer02);
            this.Controls.Add(this.plCellLoadTransfer01);
            this.Controls.Add(this.plCstUnloader02);
            this.Controls.Add(this.plBreakUnit01);
            this.Controls.Add(this.plBreakUnit03);
            this.Controls.Add(this.plUnloaderTransfer01);
            this.Controls.Add(this.plAfterIRCutTransfer01);
            this.Controls.Add(this.plCstLoad01);
            this.Controls.Add(this.plCellOutTransfer01);
            this.Name = "ParameterTest";
            this.Size = new System.Drawing.Size(1546, 4000);
            this.plCellOutTransfer01.ResumeLayout(false);
            this.gbox_Cellpurge_Move.ResumeLayout(false);
            this.gbox_Cellpurge_MoveCenter.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.gbox_Cellpurge_MoveB.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.gbox_Cellpurge_MoveA.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.gbox_Cellpurge_B.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.gbox_Cellpurge_A.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.plCstLoad01.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbox_CasseteLoad_Move.ResumeLayout(false);
            this.gbox_CasseteLoad_MoveB.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.gbox_CasseteLoad_MoveA.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.gbox_CasseteLoad_B.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.plAfterIRCutTransfer01.ResumeLayout(false);
            this.gbox_BreakTransfer_Move.ResumeLayout(false);
            this.gbox_BreakTransfer_MoveB.ResumeLayout(false);
            this.tableLayoutPanel44.ResumeLayout(false);
            this.gbox_BreakTransfer_MoveA.ResumeLayout(false);
            this.tableLayoutPanel43.ResumeLayout(false);
            this.gbox_BreakTransfer_B.ResumeLayout(false);
            this.tableLayoutPanel42.ResumeLayout(false);
            this.gbox_BreakTransfer_A.ResumeLayout(false);
            this.tableLayoutPanel41.ResumeLayout(false);
            this.plUnloaderTransfer01.ResumeLayout(false);
            this.gbox_UnloaderTransfer_Move.ResumeLayout(false);
            this.gbox_UnloaderTransfer_MoveB.ResumeLayout(false);
            this.tableLayoutPanel61.ResumeLayout(false);
            this.tableLayoutPanel62.ResumeLayout(false);
            this.tableLayoutPanel63.ResumeLayout(false);
            this.gbox_UnloaderTransfer_MoveA.ResumeLayout(false);
            this.tableLayoutPanel58.ResumeLayout(false);
            this.tableLayoutPanel60.ResumeLayout(false);
            this.tableLayoutPanel59.ResumeLayout(false);
            this.gbox_UnloaderTransfer_B.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.gbox_UnloaderTransfer_A.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.plBreakUnit03.ResumeLayout(false);
            this.gbox_BreakUnitTY_Shutter.ResumeLayout(false);
            this.tableLayoutPanel53.ResumeLayout(false);
            this.gbox_BreakUnitTY_DummyBox.ResumeLayout(false);
            this.tableLayoutPanel52.ResumeLayout(false);
            this.gbox_BreakUnitTY_Move.ResumeLayout(false);
            this.tableLayoutPanel54.ResumeLayout(false);
            this.gbox_BreakUnitTY_Theta.ResumeLayout(false);
            this.tableLayoutPanel57.ResumeLayout(false);
            this.gbox_BreakUnitTY_MoveB.ResumeLayout(false);
            this.tableLayoutPanel56.ResumeLayout(false);
            this.gbox_BreakUnitTY_MoveA.ResumeLayout(false);
            this.tableLayoutPanel55.ResumeLayout(false);
            this.gbox_BreakUnitTY_B.ResumeLayout(false);
            this.tableLayoutPanel50.ResumeLayout(false);
            this.gbox_BreakUnitTY_A.ResumeLayout(false);
            this.tableLayoutPanel51.ResumeLayout(false);
            this.plBreakUnit01.ResumeLayout(false);
            this.gbox_BreakUnitXZ_Move.ResumeLayout(false);
            this.tableLayoutPanel45.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ4.ResumeLayout(false);
            this.tableLayoutPanel49.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ3.ResumeLayout(false);
            this.tableLayoutPanel48.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ2.ResumeLayout(false);
            this.tableLayoutPanel47.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ1.ResumeLayout(false);
            this.tableLayoutPanel46.ResumeLayout(false);
            this.plCellLoadTransfer01.ResumeLayout(false);
            this.gbox_CellLoad_Move.ResumeLayout(false);
            this.gbox_CellLoad_MoveLorUn.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.gbox_CellLoad_MoveB.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel32.ResumeLayout(false);
            this.tableLayoutPanel33.ResumeLayout(false);
            this.tableLayoutPanel34.ResumeLayout(false);
            this.gbox_CellLoad_MoveA.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.gbox_CellLoad_B.ResumeLayout(false);
            this.tableLayoutPanel25.ResumeLayout(false);
            this.gbox_CellLoad_A.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.plIRCutProcess01.ResumeLayout(false);
            this.gbox_IRCutProcess_Move.ResumeLayout(false);
            this.gbox_IRCutProcess_RightOffset.ResumeLayout(false);
            this.tableLayoutPanel40.ResumeLayout(false);
            this.gbox_IRCutProcess_LeftOffset.ResumeLayout(false);
            this.tableLayoutPanel39.ResumeLayout(false);
            this.gbox_IRCutProcess_MoveB.ResumeLayout(false);
            this.tableLayoutPanel38.ResumeLayout(false);
            this.gbox_IRCutProcess_MoveA.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.gbox_IRCutProcess_B.ResumeLayout(false);
            this.tableLayoutPanel36.ResumeLayout(false);
            this.gbox_IRCutProcess_A.ResumeLayout(false);
            this.tableLayoutPanel35.ResumeLayout(false);
            this.plCellInTransfer01.ResumeLayout(false);
            this.gbox_CameraUnit_Move.ResumeLayout(false);
            this.gbox_CameraUnit_MoveB.ResumeLayout(false);
            this.tableLayoutPanel65.ResumeLayout(false);
            this.gbox_CameraUnit_MoveA.ResumeLayout(false);
            this.tableLayoutPanel64.ResumeLayout(false);
            this.plCstUnloader01.ResumeLayout(false);
            this.gbox_CellInput_Buffer.ResumeLayout(false);
            this.tableLayoutPanel66.ResumeLayout(false);
            this.gbox_CellInput_Move.ResumeLayout(false);
            this.gbox_CellInput_MoveCenter.ResumeLayout(false);
            this.tableLayoutPanel69.ResumeLayout(false);
            this.gbox_CellInput_MoveB.ResumeLayout(false);
            this.tableLayoutPanel68.ResumeLayout(false);
            this.gbox_CellInput_MoveA.ResumeLayout(false);
            this.tableLayoutPanel67.ResumeLayout(false);
            this.gbox_CellInput_B.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.gbox_CellInput_A.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.plCstLoad02.ResumeLayout(false);
            this.gbox_CasseteUnload.ResumeLayout(false);
            this.plCellInTransfer02.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.plBreakUnit04.ResumeLayout(false);
            this.gbox_BreakUnitTY_Ionizer.ResumeLayout(false);
            this.plIRCutProcess02.ResumeLayout(false);
            this.gbox_IRCutProcess_Ionizer.ResumeLayout(false);
            this.plCstUnloader02.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.plCellOutTransfer02.ResumeLayout(false);
            this.gbox_CellInput_Ionizer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel plCellOutTransfer01;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_Move;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_MoveCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y2C;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_BBuffer;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y2Uld;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y1C;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_ABuffer;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y1Uld;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X2BC;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X2BW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X1BW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X1BC;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X2AC;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X2AW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X1AW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X1AC;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Button btn_Cellpurge_B_DestructionOn;
        private System.Windows.Forms.Button btn_Cellpurge_B_DestructionOff;
        private System.Windows.Forms.Button btn_Cellpurge_B_PneumaticOff;
        private System.Windows.Forms.Button btn_Cellpurge_B_PneumaticOn;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Button btn_Cellpurge_A_DestructionOn;
        private System.Windows.Forms.Button btn_Cellpurge_A_DestructionOff;
        private System.Windows.Forms.Button btn_Cellpurge_A_PneumaticOff;
        private System.Windows.Forms.Button btn_Cellpurge_A_PneumaticOn;
        private System.Windows.Forms.Panel plCstLoad01;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LTSD;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LTSU;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LUG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCG;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_Move;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2CellOut;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2Tilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2InWait;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T4Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T3Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T4Move;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T3In;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1CellOut;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1Tilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1InWait;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T2Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T1Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T2Move;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T1In;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LTSD;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LTSU;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LUG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCG;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_A;
        private System.Windows.Forms.Panel plAfterIRCutTransfer01;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_Move;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveB_Unload;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveB_Load;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel43;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveA_Unload;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveA_Load;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel42;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerDestructionOn;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerDestructionOff;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerUp;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerDown;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PneumaticOff;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel41;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerDestructionOn;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerDestructionOff;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerUp;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerDown;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerPneumaticOff;
        private System.Windows.Forms.Panel plUnloaderTransfer01;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_Move;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel61;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel62;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T40Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T4P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T4M90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T30Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T3P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T3M90Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel63;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_BStage;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_BUnloading;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel58;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel60;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T20Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T2P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T2M90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T10Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T1P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T1M90Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel59;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_AStage;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_AUnloading;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestruction2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestructionOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestructionOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestructionOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOff2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDown2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerUp2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDown1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerUp1;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOff2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOff2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDown2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerUp2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDown1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerUp1;
        private System.Windows.Forms.Panel plBreakUnit03;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Shutter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel53;
        private System.Windows.Forms.Button btn_BreakUnitTY_Shutter_Close;
        private System.Windows.Forms.Button btn_BreakUnitTY_Shutter_Open;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_DummyBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel52;
        private System.Windows.Forms.Button btn_BreakUnitTY_DummyBox_Output;
        private System.Windows.Forms.Button btn_BreakUnitTY_DummyBox_Input;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel54;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Theta;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel57;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T2Wait;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T4Wait;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T1Wait;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T3Wait;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel56;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveB_Unload;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveB_Load;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel55;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveA_Unload;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveA_Load;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel50;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Pneumatic_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Pneumatic_Off;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel51;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Pneumatic_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Pneumatic_Off;
        private System.Windows.Forms.Panel plBreakUnit01;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel45;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel49;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ4_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ4_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ4_FastDescent;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel48;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ3_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ3_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ3_FastDescent;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel47;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ2_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ2_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ2_FastDescent;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel46;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ1_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ1_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ1_FastDescent;
        private System.Windows.Forms.Panel plCellLoadTransfer01;
        private System.Windows.Forms.GroupBox gbox_CellLoad_Move;
        private System.Windows.Forms.GroupBox gbox_CellLoad_MoveLorUn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_PreAlignMark1;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_PreAlignMark2;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_Y1UnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_Y1L;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_X2L;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_X1L;
        private System.Windows.Forms.GroupBox gbox_CellLoad_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerM90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerP90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPicker0Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_X2BStageUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_X1BStageUnL;
        private System.Windows.Forms.GroupBox gbox_CellLoad_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerM90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerP90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APicker0Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_X2AStageUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_X1AStageUnL;
        private System.Windows.Forms.GroupBox gbox_CellLoad_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerDestructionOn;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerDestructionOff;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerUp;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerDown;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerPneumaticOff;
        private System.Windows.Forms.GroupBox gbox_CellLoad_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerDestructionOn;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerDestructionOff;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerUp;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerDown;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerPneumaticOff;
        private System.Windows.Forms.Panel plIRCutProcess01;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_Move;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_RightOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Right_Vision2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Right_Focus2Vision;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Left_Vision2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Left_Focus2Vision;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_LeftOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel39;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Right_Visoin2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Right_Focus2Visoin;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Left_Vision2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Left_Focus2Vision;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R1Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R2Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_RightCellLoad;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_RightCellUnload;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R1Laser;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R2Laser;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L1Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L2Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_LeftCellLoad;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_LeftCellUnload;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L1Laser;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L2Laser;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch2On;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch1Off;
        private System.Windows.Forms.Panel plCellInTransfer01;
        private System.Windows.Forms.GroupBox gbox_CameraUnit_Move;
        private System.Windows.Forms.GroupBox gbox_CameraUnit_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel65;
        private System.Windows.Forms.Button btn_CameraUnit_MoveB_InspectionMove4;
        private System.Windows.Forms.Button btn_CameraUnit_MoveB_InspectionMove3;
        private System.Windows.Forms.GroupBox gbox_CameraUnit_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel64;
        private System.Windows.Forms.Button btn_CameraUnit_MoveA_InspectionMove2;
        private System.Windows.Forms.Button btn_CameraUnit_MoveA_InspectionMove1;
        private System.Windows.Forms.Panel plCstUnloader01;
        private System.Windows.Forms.GroupBox gbox_CellInput_Buffer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel66;
        private System.Windows.Forms.Button btn_CellInput_Buffer_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_Buffer_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PickerUp;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PickerDown;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PneumaticOn;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PneumaticOff;
        private System.Windows.Forms.GroupBox gbox_CellInput_Move;
        private System.Windows.Forms.GroupBox gbox_CellInput_MoveCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel69;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_ABuffer;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_BBuffer;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_Y1CstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_Y2CstMid;
        private System.Windows.Forms.GroupBox gbox_CellInput_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel68;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X2BUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_Y2BUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X2BCstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X2BCstWait;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X1AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_Y1BUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X1BCstMId;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X1BCstWait;
        private System.Windows.Forms.GroupBox gbox_CellInput_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel67;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X2AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_Y2AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X2ACstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X2ACstWait;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X1AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_Y1AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X1ACstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X1ACstWait;
        private System.Windows.Forms.GroupBox gbox_CellInput_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Button btn_CellInput_B_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_B_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_B_PneumaticOff;
        private System.Windows.Forms.Button btn_CellInput_B_PneumaticOn;
        private System.Windows.Forms.GroupBox gbox_CellInput_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Button btn_CellInput_A_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_A_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_A_PneumaticOff;
        private System.Windows.Forms.Button btn_CellInput_A_PneumaticOn;
        private System.Windows.Forms.Panel plCstLoad02;
        private System.Windows.Forms.GroupBox gbox_CasseteUnload;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting4;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting2;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting3;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting1;
        private System.Windows.Forms.Button btn_CasseteUnload_MutingOut;
        private System.Windows.Forms.Button btn_CasseteUnload_MutingIn;
        private System.Windows.Forms.Panel plCellInTransfer02;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel plBreakUnit04;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Ionizer;
        private System.Windows.Forms.Button btn_BreakUnitTY_DestructionOff;
        private System.Windows.Forms.Button btn_BreakUnitTY_IonizerOff;
        private System.Windows.Forms.Button btn_BreakUnitTY_DestructionOn;
        private System.Windows.Forms.Button btn_BreakUnitTY_IonizerOn;
        private System.Windows.Forms.Panel plIRCutProcess02;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_Ionizer;
        private System.Windows.Forms.Button btn_IRCutProcess_Destruction;
        private System.Windows.Forms.Button btn_IRCutProcess_IonizerOff;
        private System.Windows.Forms.Button btn_IRCutProcess_DestructionOn;
        private System.Windows.Forms.Button btn_IRCutProcess_IonizerOn;
        private System.Windows.Forms.Panel plCstUnloader02;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Panel plCellOutTransfer02;
        private System.Windows.Forms.GroupBox gbox_CellInput_Ionizer;
        private System.Windows.Forms.Button btn_CellInput_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_IonizerOff;
        private System.Windows.Forms.Button btn_CellInput_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_IonizerOn;
    }
}

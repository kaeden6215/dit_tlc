﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;
using Dit.Framework.Comm;

namespace DIT.TLC.UI
{
    public partial class UcrlRecipeWindows : UserControl, IUIUpdate
    {
        public UcrlRecipeWindows()
        {
            InitializeComponent();

        }
        public void FillLstRecipe()
        {
            int iPos = 0;
            lvRecipe.Items.Clear();

            foreach (EqpRecipe recp in GG.Equip.EqpRecipeMgr.LstEqpRecipe)
                lvRecipe.Items.Add(new ListViewItem(new string[] { iPos.ToString(), recp.Name, recp.Cim ? "O" : "X", recp.Used ? "O" : "X" }));
        }

        private void txtInterval_KeyPress(object sender, KeyPressEventArgs e)
        {
            UserExtension.TypingOnlyNumber(sender, e, true, true, true);
        }

        //public void FillComboBox()
        //{
        //    if (comxtalignmentLayer.Items.Count > 0)
        //        comxtalignmentLayer.SelectedIndex = 0;
        //    if (comxtprocessLayer.Items.Count > 0)
        //        comxtprocessLayer.SelectedIndex = 0;
        //    if (comxtguideLayer.Items.Count > 0)
        //        comxtguideLayer.SelectedIndex = 0;
        //    //아래꺼 2개
        //    //if (comxtprocessParam.Items.Count > 0)
        //    //    comxtprocessParam.SelectedIndex = 0;
        //    //if (comxtcellType.Items.Count > 0)
        //    //    comxtcellType.SelectedIndex = 0;
        //}

        public void ProcessCellTypeList()
        {
            if (GG.Equip.ProcessRecipeMgr.LstProcessRecipe == null) return;
            if (GG.Equip.BreakRecipeMgr.LstBreakRecipe == null) return;
            comxtprocessParam.Items.Clear();
            comxtcellType.Items.Clear();

            foreach (ProcessRecipe precp in GG.Equip.ProcessRecipeMgr.LstProcessRecipe)
                comxtprocessParam.Items.Add(precp.Name);

            foreach (BreakRecipe brecp in GG.Equip.BreakRecipeMgr.LstBreakRecipe)
                comxtcellType.Items.Add(brecp.Name);
        }

        private void btnRecipeMake_Click(object sender, EventArgs e)
        {

            FrmRecipeNameInput ff = new FrmRecipeNameInput();
            ff.StartPosition = FormStartPosition.CenterParent;
            if (ff.ShowDialog() == DialogResult.Cancel)
                return;

            if (string.IsNullOrEmpty(ff.RecipeName))
            {
                MessageBox.Show("레시피 이름이 없습니다.!");
                return;
            }

            if (GG.Equip.EqpRecipeMgr.LstEqpRecipe.Count(f => f.Name == ff.RecipeName) > 0)
            {
                MessageBox.Show("중복된 레시피 이름이 있습니다.");
                return;
            }


            EqpRecipe recp = new EqpRecipe() { Name = ff.RecipeName };
            FillRecipeData(recp);
            GG.Equip.EqpRecipeMgr.LstEqpRecipe.Add(recp);
            GG.Equip.EqpRecipeMgr.Save();
            FillLstRecipe();
            ProcessCellTypeList();
            lvRecipe.Items[lvRecipe.Items.Count - 1].Selected = true;
        }

        private void btnRecipeCopy_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;

            FrmRecipeNameInput ff = new FrmRecipeNameInput();
            ff.StartPosition = FormStartPosition.CenterParent;
            if (ff.ShowDialog() == DialogResult.Cancel)
                return;

            if (string.IsNullOrEmpty(ff.RecipeName))
            {
                MessageBox.Show("레시피 이름이 없습니다.!");
                return;
            }

            if (GG.Equip.EqpRecipeMgr.LstEqpRecipe.Count(f => f.Name == ff.RecipeName) > 0)
            {
                MessageBox.Show("중복된 레시피 이름이 있습니다.");
                return;
            }

            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            EqpRecipe recpCopy = (EqpRecipe)recp.Clone();

            //"이름 입력 요망"
            recpCopy.Name = ff.RecipeName;
            GG.Equip.EqpRecipeMgr.LstEqpRecipe.Add(recpCopy);
            GG.Equip.EqpRecipeMgr.Save();
            FillLstRecipe();
            FillRecipeData(recpCopy);
        }

        private void btnRecipeSave_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            UpdateRecipe(ref recp);
            GG.Equip.EqpRecipeMgr.Save();

        }

        private void btnRecipeApply_Click(object sender, EventArgs e)
        {
            //save apply 차이점?
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            UpdateRecipe(ref recp);
            GG.Equip.EqpRecipeMgr.Save();
            GG.Equip.CurrentRecipe = recp;
        }

        private void btnRecipeDelete_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            GG.Equip.EqpRecipeMgr.LstEqpRecipe.Remove(recp);
            GG.Equip.EqpRecipeMgr.Save();
            recp = new EqpRecipe() { Name = string.Empty };

            UpdateRecipe(ref recp);
            FillLstRecipe();
        }

        private void btnRecipeCimApply_Click(object sender, EventArgs e)
        {

        }
        private void btnRecipeCimDelete_Click(object sender, EventArgs e)
        {

        }

        private void lvRecipe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe rcep = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            FillRecipeData(rcep);
        }

        public void FillRecipeData(EqpRecipe rcep)
        {
            #region FillData
            txtrecipe.Text = rcep.Name.ToString();
            txttable1Alignmark11.Text = rcep.Table1AlignMark1.X.ToString();
            txttable1Alignmark12.Text = rcep.Table1AlignMark1.Y.ToString();
            txttable1Alignmark21.Text = rcep.Table1AlignMark2.X.ToString();
            txttable1Alignmark22.Text = rcep.Table1AlignMark2.Y.ToString();
            txttable2Alignmark11.Text = rcep.Table2AlignMark1.X.ToString();
            txttable2Alignmark12.Text = rcep.Table2AlignMark1.Y.ToString();
            txttable2Alignmark21.Text = rcep.Table2AlignMark2.X.ToString();
            txttable2Alignmark22.Text = rcep.Table2AlignMark2.Y.ToString();
            txtbreakLeft11.Text = rcep.BreakLeft1.X.ToString();
            txtbreakLeft12.Text = rcep.BreakLeft1.Y.ToString();
            txtbreakLeft21.Text = rcep.BreakLeft2.X.ToString();
            txtbreakLeft22.Text = rcep.BreakLeft2.Y.ToString();
            txtbreakLeftY.Text = rcep.BreakLeftY.ToString();
            txtbreakRight11.Text = rcep.BreakRight1.X.ToString();
            txtbreakRight12.Text = rcep.BreakRight1.Y.ToString();
            txtbreakRight21.Text = rcep.BreakRight2.X.ToString();
            txtbreakRight22.Text = rcep.BreakRight2.Y.ToString();
            txtbreakRightY.Text = rcep.BreakRightY.ToString();
            txtcellSize1.Text = rcep.AMatrixSize.X.ToString();
            txtcellSize2.Text = rcep.AMatrixSize.Y.ToString();
            comxtalignmentLayer.Text = rcep.AlignMentLayer.ToString();
            comxtprocessLayer.Text = rcep.ProcessLayer.ToString();
            comxtguideLayer.Text = rcep.GuideLayer.ToString();
            comxtprocessParam.Text = rcep.ProcessParam.ToString();
            comxtcellType.Text = rcep.CellType.ToString();
            txttThickness.Text = rcep.TaThickness.ToString();

            //Cassette
            txtwidth.Text = rcep.CassetteWidth.ToString();
            txtheight.Text = rcep.CassetteHeight.ToString();
            txtmaxHeightCount.Text = rcep.CassetteMaxHeightCount.ToString();
            txtcellCount.Text = rcep.CassetteCellCount.ToString();
            btnCassetteNormal.BackColor = rcep.CstNormal ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            btnCassette1.BackColor = (rcep.CstType == emCstType.Cst1) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCassette2.BackColor = (rcep.CstType == emCstType.Cst2) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCassette4.BackColor = (rcep.CstType == emCstType.Cst4) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnCassette8.BackColor = (rcep.CstType == emCstType.Cst8) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            //Piker
            btnPickerNormal1.BackColor = (rcep.LDPickerType == emPickerType.Normal) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnPickerCW901.BackColor = (rcep.LDPickerType == emPickerType.CW90) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnPickerCCW901.BackColor = (rcep.LDPickerType == emPickerType.CCW90) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnPickerCW1801.BackColor = (rcep.LDPickerType == emPickerType.CW180) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            btnPickerNormal2.BackColor = (rcep.ULDPickerType == emPickerType.Normal) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnPickerCW902.BackColor = (rcep.ULDPickerType == emPickerType.CW90) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnPickerCCW902.BackColor = (rcep.ULDPickerType == emPickerType.CCW90) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            btnPickerCW1802.BackColor = (rcep.ULDPickerType == emPickerType.CW180) ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;

            //LDS Process
            txtldsProcess11.Text = rcep.LDSProcess01.X.ToString();
            txtldsProcess12.Text = rcep.LDSProcess01.Y.ToString();
            txtldsProcess21.Text = rcep.LDSProcess02.X.ToString();
            txtldsProcess22.Text = rcep.LDSProcess02.Y.ToString();
            txtldsProcess31.Text = rcep.LDSProcess03.X.ToString();
            txtldsProcess32.Text = rcep.LDSProcess03.Y.ToString();
            txtldsProcess41.Text = rcep.LDSProcess04.X.ToString();
            txtldsProcess42.Text = rcep.LDSProcess04.Y.ToString();

            //LDS Break
            txtldsBreak11.Text = rcep.LDSBreak01.X.ToString();
            txtldsBreak12.Text = rcep.LDSBreak01.Y.ToString();
            txtldsBreak21.Text = rcep.LDSBreak02.X.ToString();
            txtldsBreak22.Text = rcep.LDSBreak02.Y.ToString();
            txtldsBreak31.Text = rcep.LDSBreak03.X.ToString();
            txtldsBreak32.Text = rcep.LDSBreak03.Y.ToString();
            txtldsBreak41.Text = rcep.LDSBreak04.X.ToString();
            txtldsBreak42.Text = rcep.LDSBreak04.Y.ToString();

            //Y-Offset
            txtyOffsetPre.Text = rcep.OffsetPre.ToString();
            txtyOffsetBreak.Text = rcep.OffsetBreak.ToString();
            #endregion
        }

        public void UpdateRecipe(ref EqpRecipe rcep)
        {
            #region UpdateRecipe
            rcep.Name = txtrecipe.Text.ToString();
            rcep.Table1AlignMark1.X = double.Parse(txttable1Alignmark11.Text);
            rcep.Table1AlignMark1.Y = double.Parse(txttable1Alignmark12.Text);
            rcep.Table1AlignMark2.X = double.Parse(txttable1Alignmark21.Text);
            rcep.Table1AlignMark2.Y = double.Parse(txttable1Alignmark22.Text);
            rcep.Table2AlignMark1.X = double.Parse(txttable2Alignmark11.Text);
            rcep.Table2AlignMark1.Y = double.Parse(txttable2Alignmark12.Text);
            rcep.Table2AlignMark2.X = double.Parse(txttable2Alignmark21.Text);
            rcep.Table2AlignMark2.Y = double.Parse(txttable2Alignmark22.Text);
            rcep.BreakLeft1.X = double.Parse(txtbreakLeft11.Text);
            rcep.BreakLeft1.Y = double.Parse(txtbreakLeft12.Text);
            rcep.BreakLeft2.X = double.Parse(txtbreakLeft21.Text);
            rcep.BreakLeft2.Y = double.Parse(txtbreakLeft22.Text);
            rcep.BreakLeftY = double.Parse(txtbreakLeftY.Text);
            rcep.BreakRight1.X = double.Parse(txtbreakRight11.Text);
            rcep.BreakRight1.Y = double.Parse(txtbreakRight12.Text);
            rcep.BreakRight2.X = double.Parse(txtbreakRight21.Text);
            rcep.BreakRight2.Y = double.Parse(txtbreakRight22.Text);
            rcep.BreakRightY = double.Parse(txtbreakRightY.Text);
            rcep.AMatrixSize.X = double.Parse(txtcellSize1.Text);
            rcep.AMatrixSize.Y = double.Parse(txtcellSize2.Text);
            rcep.AlignMentLayer = comxtalignmentLayer.Text.ToString();
            rcep.ProcessLayer = comxtprocessLayer.Text.ToString();
            rcep.GuideLayer = comxtguideLayer.Text.ToString();
            rcep.ProcessParam = comxtprocessParam.Text.ToString();
            rcep.CellType = comxtcellType.Text.ToString();
            rcep.TaThickness = double.Parse(txttThickness.Text);

            //Cassette
            rcep.CassetteWidth = double.Parse(txtwidth.Text);
            rcep.CassetteHeight = double.Parse(txtheight.Text);
            rcep.CassetteMaxHeightCount = int.Parse(txtmaxHeightCount.Text);
            rcep.CassetteCellCount = int.Parse(txtcellCount.Text);

            rcep.CstNormal = (btnCassetteNormal.BackColor == UiGlobal.SELECT_C) ? true : false;

            rcep.CstType = (btnCassette1.BackColor == UiGlobal.SELECT_C) ? emCstType.Cst1 :
                (btnCassette2.BackColor == UiGlobal.SELECT_C) ? emCstType.Cst2 :
                (btnCassette4.BackColor == UiGlobal.SELECT_C) ? emCstType.Cst4 :
                (btnCassette8.BackColor == UiGlobal.SELECT_C) ? emCstType.Cst8 : emCstType.Cst1;


            //Picker
            rcep.LDPickerType = (btnPickerNormal1.BackColor == UiGlobal.SELECT_C) ? emPickerType.Normal :
                (btnPickerCW901.BackColor == UiGlobal.SELECT_C) ? emPickerType.CW90 :
                (btnPickerCW1801.BackColor == UiGlobal.SELECT_C) ? emPickerType.CW180 :
                (btnPickerCCW901.BackColor == UiGlobal.SELECT_C) ? emPickerType.CCW90 : emPickerType.Normal;

            rcep.ULDPickerType = (btnPickerNormal2.BackColor == UiGlobal.SELECT_C) ? emPickerType.Normal :
                (btnPickerCW902.BackColor == UiGlobal.SELECT_C) ? emPickerType.CW90 :
                (btnPickerCW1802.BackColor == UiGlobal.SELECT_C) ? emPickerType.CW180 :
                (btnPickerCCW902.BackColor == UiGlobal.SELECT_C) ? emPickerType.CCW90 : emPickerType.Normal;

            //LDS Process
            rcep.LDSProcess01.X = double.Parse(txtldsProcess11.Text);
            rcep.LDSProcess01.Y = double.Parse(txtldsProcess12.Text);
            rcep.LDSProcess02.X = double.Parse(txtldsProcess21.Text);
            rcep.LDSProcess02.Y = double.Parse(txtldsProcess22.Text);
            rcep.LDSProcess03.X = double.Parse(txtldsProcess31.Text);
            rcep.LDSProcess03.Y = double.Parse(txtldsProcess32.Text);
            rcep.LDSProcess04.X = double.Parse(txtldsProcess41.Text);
            rcep.LDSProcess04.Y = double.Parse(txtldsProcess42.Text);

            //LDS Break
            rcep.LDSBreak01.X = double.Parse(txtldsBreak11.Text);
            rcep.LDSBreak01.Y = double.Parse(txtldsBreak12.Text);
            rcep.LDSBreak02.X = double.Parse(txtldsBreak21.Text);
            rcep.LDSBreak02.Y = double.Parse(txtldsBreak22.Text);
            rcep.LDSBreak03.X = double.Parse(txtldsBreak31.Text);
            rcep.LDSBreak03.Y = double.Parse(txtldsBreak32.Text);
            rcep.LDSBreak04.X = double.Parse(txtldsBreak41.Text);
            rcep.LDSBreak04.Y = double.Parse(txtldsBreak42.Text);

            //Y-Offset
            rcep.OffsetPre = double.Parse(txtyOffsetPre.Text);
            rcep.OffsetBreak = double.Parse(txtyOffsetBreak.Text);
            #endregion
        }

        private void btnRecipeLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Image File (*.bmp, *.jpeg, *.jpg, *.png) | *.bmp; *.jpeg; *.jpg; *.png;";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtrecipePath.Text = ofd.FileName;
            }
        }

        private void btnDxfPathLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "DXF File(*.dxf)|*.dxf";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtdxfpath.Text = ofd.FileName;
            }
        }

        public void UIUpdate()
        {
            //btnCassette1.BackColor = CstType1 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnCassette2.BackColor = CstType2 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnCassette4.BackColor = CstType4 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnCassette8.BackColor = CstType8 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //
            //btnPickerNormal1.BackColor = LDPikerType1 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnPickerCW901.BackColor = LDPikerType2 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnPickerCCW901.BackColor = LDPikerType3 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnPickerCW1801.BackColor = LDPikerType4 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //
            //btnPickerNormal2.BackColor = ULDPikerType1 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnPickerCW902.BackColor = ULDPikerType2 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnPickerCCW902.BackColor = ULDPikerType3 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
            //btnPickerCW1802.BackColor = ULDPikerType4 == 1 ? UiGlobal.SELECT_C : UiGlobal.UNSELECT_C;
        }

        private void btnCutLineMeasure_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;

            new FrmCutLineMeasure(recipeName).ShowDialog();
        }

        private void btnInspPosition_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;

            new FrmInspPosition(recipeName).ShowDialog();
        }


        private void btnRead_Click(object sender, EventArgs e)
        {
            //Item
            //FINEALIGN
            //X Y X Y
            if (btnTable1Alignmark1Read == (Button)sender)
            {
                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txttable1Alignmark11.Text);
                GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txttable1Alignmark12.Text);
            }
            else if (btnTable1AlignMark2Read == (Button)sender)
            {

                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txttable1Alignmark21.Text);
                GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txttable1Alignmark21.Text);
            }

            else if (btnTable2Alignmark1Read == (Button)sender)
            {
                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txttable2Alignmark11.Text);
                GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition = float.Parse(txttable2Alignmark12.Text);
            }
            else if (btnTable2AlignMark2Read == (Button)sender)
            {
                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txttable2Alignmark21.Text);
                GG.Equip.PROC.IRCutStage_B.YAxis.XF_CurrMotorPosition = float.Parse(txttable2Alignmark22.Text);
            }
            //BREAKALIGN
            // X11 X21  X21 X22  Y
            // A01 A02  B01 B02  Y
            else if (btnBreakLeft1Read == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.SubX1Axis.XF_CurrMotorPosition = float.Parse(txtbreakLeft11.Text);
                GG.Equip.PROC.BreakStage_A.SubX2Axis.XF_CurrMotorPosition = float.Parse(txtbreakLeft12.Text);
            }
            else if (btnBreakLeft2Read == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.SubX1Axis.XF_CurrMotorPosition = float.Parse(txtbreakLeft21.Text);
                GG.Equip.PROC.BreakStage_A.SubX2Axis.XF_CurrMotorPosition = float.Parse(txtbreakLeft22.Text);
            }
            else if (btnBreakLeftYRead == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txtbreakLeftY.Text);
            }
            else if (btnBreakRight1Read == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.SubX1Axis.XF_CurrMotorPosition = float.Parse(txtbreakRight11.Text);
                GG.Equip.PROC.BreakStage_B.SubX2Axis.XF_CurrMotorPosition = float.Parse(txtbreakRight12.Text);
            }
            else if (btnBreakRight2Read == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.SubX1Axis.XF_CurrMotorPosition = float.Parse(txtbreakRight21.Text);
                GG.Equip.PROC.BreakStage_B.SubX2Axis.XF_CurrMotorPosition = float.Parse(txtbreakRight22.Text);
            }
            else if (btnBreakRightYRead == (Button)sender)
            {
                GG.Equip.PROC.BreakStage_B.YAxis.XF_CurrMotorPosition = float.Parse(txtbreakRightY.Text);
            }
            //LDS Process
            // X Y
            //finealign
            //가공테이블
            else if (btnLDSProcess1Read == (Button)sender)
            {
                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess11.Text);
                GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess12.Text);
            }
            else if (btnLDSProcess2Read == (Button)sender)
            {
                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess21.Text);
                GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess22.Text);
            }
            else if (btnLDSProcess3Read == (Button)sender)
            {
                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess31.Text);
                GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess32.Text);
            }
            else if (btnLDSProcess4Read == (Button)sender)
            {
                GG.Equip.PROC.FineAlign.XAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess41.Text);
                GG.Equip.PROC.IRCutStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txtldsProcess42.Text);
            }
            //LDS Break
            //BREAKALIGN
            //BREAKTABLE
            else if (btnLDSBreak1Read == (Button)sender)
            {
                GG.Equip.PROC.BreakAlign_A.XAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak11.Text);
                GG.Equip.PROC.BreakStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak12.Text);
            }
            else if (btnLDSBreak2Read == (Button)sender)
            {
                GG.Equip.PROC.BreakAlign_A.XAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak21.Text);
                GG.Equip.PROC.BreakStage_A.YAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak22.Text);
            }
            else if (btnLDSBreak3Read == (Button)sender)
            {
                GG.Equip.PROC.BreakAlign_B.XAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak31.Text);
                GG.Equip.PROC.BreakStage_B.YAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak32.Text);
            }
            else if (btnLDSBreak4Read == (Button)sender)
            {
                GG.Equip.PROC.BreakAlign_B.XAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak41.Text);
                GG.Equip.PROC.BreakStage_B.YAxis.XF_CurrMotorPosition = float.Parse(txtldsBreak42.Text);
            }
        }



        private void btnMove_Click(object sender, EventArgs e)
        {
            //Item
            //FINEALIGN
            //X Y X Y
            if (btnTable1Alignmark1Move == (Button)sender)
            {
                float ACol01TableAlignMarkX = float.Parse(txttable1Alignmark11.Text);
                float ACol01TableAlignMarkY = float.Parse(txttable1Alignmark12.Text);

                GG.Equip.PROC.IRCutStage_A.YAxis.PtpMoveCmd(GG.Equip, ACol01TableAlignMarkX, 0, 0);
                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, ACol01TableAlignMarkY, 0, 0);
            }
            else if (btnTable1AlignMark2Move == (Button)sender)
            {
                float ACol02TableAlignMarkX = float.Parse(txttable1Alignmark21.Text);
                float ACol02TableAlignMarkY = float.Parse(txttable1Alignmark21.Text);

                GG.Equip.PROC.IRCutStage_A.YAxis.PtpMoveCmd(GG.Equip, ACol02TableAlignMarkX, 0, 0);
                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, ACol02TableAlignMarkY, 0, 0);
            }

            else if (btnTable2Alignmark1Move == (Button)sender)
            {
                float BCol01TableAlignMarkX = float.Parse(txttable2Alignmark11.Text);
                float BCol01TableAlignMarkY = float.Parse(txttable2Alignmark12.Text);

                GG.Equip.PROC.IRCutStage_B.YAxis.PtpMoveCmd(GG.Equip, BCol01TableAlignMarkX, 0, 0);
                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, BCol01TableAlignMarkY, 0, 0);
            }
            else if (btnTable2AlignMark2Move == (Button)sender)
            {
                float BCol02TableAlignMarkX = float.Parse(txttable2Alignmark21.Text);
                float BCol02TableAlignMarkY = float.Parse(txttable2Alignmark22.Text);

                GG.Equip.PROC.IRCutStage_B.YAxis.PtpMoveCmd(GG.Equip, BCol02TableAlignMarkX, 0, 0);
                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, BCol02TableAlignMarkY, 0, 0);
            }
            //BREAKALIGN
            // X11 X21  X21 X22  Y
            // A01 A02  B01 B02  Y
            else if (btnBreakLeft1Move == (Button)sender)
            {
                float ACol01BreakLeftX1 = float.Parse(txtbreakLeft11.Text);
                float ACol01BreakLeftX2 = float.Parse(txtbreakLeft12.Text);

                GG.Equip.PROC.BreakStage_A.SubX1Axis.PtpMoveCmd(GG.Equip, ACol01BreakLeftX1, 0, 0);
                GG.Equip.PROC.BreakStage_A.SubX2Axis.PtpMoveCmd(GG.Equip, ACol01BreakLeftX2, 0, 0);
            }
            else if (btnBreakLeft2Move == (Button)sender)
            {
                float ACol02BreakLeftX1 = float.Parse(txtbreakLeft21.Text);
                float ACol02BreakLeftX2 = float.Parse(txtbreakLeft22.Text);

                GG.Equip.PROC.BreakStage_A.SubX1Axis.PtpMoveCmd(GG.Equip, ACol02BreakLeftX1, 0, 0);
                GG.Equip.PROC.BreakStage_A.SubX2Axis.PtpMoveCmd(GG.Equip, ACol02BreakLeftX2, 0, 0);
            }
            else if (btnBreakLeftYMove == (Button)sender)
            {
                float AColBreakLeftY = float.Parse(txtbreakLeftY.Text);

                GG.Equip.PROC.BreakStage_A.YAxis.PtpMoveCmd(GG.Equip, AColBreakLeftY, 0, 0);
            }
            else if (btnBreakRight1Move == (Button)sender)
            {
                float BCol01BreakRightX1 = float.Parse(txtbreakRight11.Text);
                float BCol01BreakRightX2 = float.Parse(txtbreakRight12.Text);

                GG.Equip.PROC.BreakStage_B.SubX1Axis.PtpMoveCmd(GG.Equip, BCol01BreakRightX1, 0, 0);
                GG.Equip.PROC.BreakStage_B.SubX2Axis.PtpMoveCmd(GG.Equip, BCol01BreakRightX2, 0, 0);
            }
            else if (btnBreakRight2Move == (Button)sender)
            {
                float BCol02BreakRightX1 = float.Parse(txtbreakRight21.Text);
                float BCol02BreakRightX2 = float.Parse(txtbreakRight22.Text);

                GG.Equip.PROC.BreakStage_B.SubX1Axis.PtpMoveCmd(GG.Equip, BCol02BreakRightX1, 0, 0);
                GG.Equip.PROC.BreakStage_B.SubX2Axis.PtpMoveCmd(GG.Equip, BCol02BreakRightX2, 0, 0);
            }
            else if (btnBreakRightYMove == (Button)sender)
            {
                float BColBreakRightY = float.Parse(txtbreakRightY.Text);

                GG.Equip.PROC.BreakStage_B.YAxis.PtpMoveCmd(GG.Equip, BColBreakRightY, 0, 0);
            }
            //LDS Process
            // X Y
            //finealign
            //가공테이블
            else if (btnLDSProcess1Move == (Button)sender)
            {
                float ACol01ProcessX = float.Parse(txtldsProcess11.Text);
                float ACol01ProcessY = float.Parse(txtldsProcess12.Text);

                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, ACol01ProcessX, 0, 0);
                GG.Equip.PROC.IRCutStage_A.YAxis.PtpMoveCmd(GG.Equip, ACol01ProcessY, 0, 0);
            }
            else if (btnLDSProcess2Move == (Button)sender)
            {
                float ACol02ProcessX = float.Parse(txtldsProcess21.Text);
                float ACol02ProcessY = float.Parse(txtldsProcess22.Text);

                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, ACol02ProcessX, 0, 0);
                GG.Equip.PROC.IRCutStage_A.YAxis.PtpMoveCmd(GG.Equip, ACol02ProcessY, 0, 0);
            }
            else if (btnLDSProcess3Move == (Button)sender)
            {
                float BCol01ProcessX = float.Parse(txtldsProcess31.Text);
                float BCol01ProcessY = float.Parse(txtldsProcess32.Text);

                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, BCol01ProcessX, 0, 0);
                GG.Equip.PROC.IRCutStage_A.YAxis.PtpMoveCmd(GG.Equip, BCol01ProcessY, 0, 0);
            }
            else if (btnLDSProcess4Move == (Button)sender)
            {
                float BCol02ProcessX = float.Parse(txtldsProcess41.Text);
                float BCol02ProcessY = float.Parse(txtldsProcess42.Text);

                GG.Equip.PROC.FineAlign.XAxis.PtpMoveCmd(GG.Equip, BCol02ProcessX, 0, 0);
                GG.Equip.PROC.IRCutStage_A.YAxis.PtpMoveCmd(GG.Equip, BCol02ProcessY, 0, 0);
            }
            //LDS Break
            //BREAKALIGN
            //BREAKTABLE
            else if (btnLDSBreak1Move == (Button)sender)
            {
                float ACol01BreakX = float.Parse(txtldsBreak11.Text);
                float ACol01BreakY = float.Parse(txtldsBreak12.Text);

                GG.Equip.PROC.BreakAlign_A.XAxis.PtpMoveCmd(GG.Equip, ACol01BreakX, 0, 0);
                GG.Equip.PROC.BreakStage_A.YAxis.PtpMoveCmd(GG.Equip, ACol01BreakY, 0, 0);
            }
            else if (btnLDSBreak2Move == (Button)sender)
            {
                float ACol02BreakX = float.Parse(txtldsBreak21.Text);
                float ACol02BreakY = float.Parse(txtldsBreak22.Text);

                GG.Equip.PROC.BreakAlign_A.XAxis.PtpMoveCmd(GG.Equip, ACol02BreakX, 0, 0);
                GG.Equip.PROC.BreakStage_A.YAxis.PtpMoveCmd(GG.Equip, ACol02BreakY, 0, 0);
            }
            else if (btnLDSBreak3Move == (Button)sender)
            {
                float BCol01BreakX = float.Parse(txtldsBreak31.Text);
                float BCol01BreakY = float.Parse(txtldsBreak32.Text);

                GG.Equip.PROC.BreakAlign_A.XAxis.PtpMoveCmd(GG.Equip, BCol01BreakX, 0, 0);
                GG.Equip.PROC.BreakStage_A.YAxis.PtpMoveCmd(GG.Equip, BCol01BreakY, 0, 0);
            }
            else if (btnLDSBreak4Move == (Button)sender)
            {
                float BCol02BreakX = float.Parse(txtldsBreak41.Text);
                float BCol02BreakY = float.Parse(txtldsBreak42.Text);

                GG.Equip.PROC.BreakAlign_A.XAxis.PtpMoveCmd(GG.Equip, BCol02BreakX, 0, 0);
                GG.Equip.PROC.BreakStage_A.YAxis.PtpMoveCmd(GG.Equip, BCol02BreakY, 0, 0);
            }
        }

        private void btnCstTypeButton_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;

            Button[] btns = { btnCassette1, btnCassette2, btnCassette4, btnCassette8 };
            ((Button)sender).BackColor = UiGlobal.SELECT_C;
            for (int i = 0; i < btns.Length; i++)
            {
                if (((Button)sender).Name != btns[i].Name)
                    btns[i].BackColor = UiGlobal.UNSELECT_C;
            }
        }

        private void btnLDPikerButton_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;

            Button[] btns = { btnPickerNormal1, btnPickerCW901, btnPickerCW1801, btnPickerCCW901 };
            ((Button)sender).BackColor = UiGlobal.SELECT_C;
            for (int i = 0; i < btns.Length; i++)
            {
                if (((Button)sender).Name != btns[i].Name)
                    btns[i].BackColor = UiGlobal.UNSELECT_C;
            }
        }

        private void btnULDPikerButton_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;

            Button[] btns = { btnPickerNormal2, btnPickerCW902, btnPickerCW1802, btnPickerCCW902 };
            ((Button)sender).BackColor = UiGlobal.SELECT_C;
            for (int i = 0; i < btns.Length; i++)
            {
                if (((Button)sender).Name != btns[i].Name)
                    btns[i].BackColor = UiGlobal.UNSELECT_C;
            }
        }
    }
}
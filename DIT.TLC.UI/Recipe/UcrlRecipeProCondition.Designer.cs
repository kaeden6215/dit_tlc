﻿namespace DIT.TLC.UI
{
    partial class UcrlRecipeProCondition
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.btnBreakingZigSave = new System.Windows.Forms.Button();
            this.txtbreakingZigCamToPin = new System.Windows.Forms.TextBox();
            this.txtbreakingZigThickness = new System.Windows.Forms.TextBox();
            this.txtbreakingZigY = new System.Windows.Forms.TextBox();
            this.txtbreakingZigX = new System.Windows.Forms.TextBox();
            this.lb_breaking_zig_cam_to_pin = new System.Windows.Forms.Label();
            this.lb_breaking_zig_thickness = new System.Windows.Forms.Label();
            this.lb_breaking_zig_y = new System.Windows.Forms.Label();
            this.lb_breaking_zig_x = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDefaultProcessSave = new System.Windows.Forms.Button();
            this.txtdefaultProcessPreAlignErrorY = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessAlignAngle = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessPreAlignErrorX = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessAlignDistance = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessBreakErrorY = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessPreAlignErrorAngle = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessAlignMatch = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessDefaultZ = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessJumpSpeed = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnProcessCopy = new System.Windows.Forms.Button();
            this.btnProcessDelete = new System.Windows.Forms.Button();
            this.btnProcessMake = new System.Windows.Forms.Button();
            this.btnProcessSave = new System.Windows.Forms.Button();
            this.txtprocessAccDec = new System.Windows.Forms.TextBox();
            this.txtprocessScan = new System.Windows.Forms.TextBox();
            this.txtprocessG = new System.Windows.Forms.TextBox();
            this.txtprocessSegmentValue = new System.Windows.Forms.TextBox();
            this.txtprocessOverlap = new System.Windows.Forms.TextBox();
            this.txtprocessZpos = new System.Windows.Forms.TextBox();
            this.txtprocessSpeed = new System.Windows.Forms.TextBox();
            this.txtprocessFrequence = new System.Windows.Forms.TextBox();
            this.txtprocessBurst = new System.Windows.Forms.TextBox();
            this.txtprocessError = new System.Windows.Forms.TextBox();
            this.txtprocessPowerRatio = new System.Windows.Forms.TextBox();
            this.txtprocessPower = new System.Windows.Forms.TextBox();
            this.txtprocessDivider = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lvProcess = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtdefaultLaserCstPitchReverse = new System.Windows.Forms.TextBox();
            this.txtdefaultLaserCstPitchNormal = new System.Windows.Forms.TextBox();
            this.txtdefaultLaserOntime = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.btndefaultLaserSave = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txtbreakingLoadTransSequenceOffsetBY2 = new System.Windows.Forms.TextBox();
            this.txtbreakingLoadTransSequenceOffsetBY1 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtbreakingLoadTransSequenceOffsetAY2 = new System.Windows.Forms.TextBox();
            this.txtbreakingLoadTransSequenceOffsetAY1 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.btnBreakingXytBreaking4Calc = new System.Windows.Forms.Button();
            this.btnBreakingXytBreaking3Calc = new System.Windows.Forms.Button();
            this.btnBreakingXytBreaking2Calc = new System.Windows.Forms.Button();
            this.btnBreakingXytBreaking1Calc = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txtbreakingFastdownSpeed = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.panelBreaking2pass = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtbreakingSlowdownSpeed = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtbreakingSlowdownPitch = new System.Windows.Forms.TextBox();
            this.txtbreakingFastdownPitch = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtbreakingBMcrOffsetXy2 = new System.Windows.Forms.TextBox();
            this.txtbreakingAMcrOffsetXy2 = new System.Windows.Forms.TextBox();
            this.txtbreakingBMcrOffsetXy1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtbreakingAMcrOffsetXy1 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtbreakingB2XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingB2XyOffset1 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtbreakingB1XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingB1XyOffset1 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtbreakingA2XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingA2XyOffset1 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtbreakingA1XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingA1XyOffset1 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking43 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking42 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking41 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking33 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking32 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking31 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking23 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking22 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking21 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking13 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking12 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking11 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyMcr2 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyAlignMark22 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyAlignMark12 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyPinCenter2 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtbreakingPinDistanceB = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtbreakingPinDistanceA = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtbreakingXyMcr1 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyAlignMark21 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyAlignMark11 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyPinCenter1 = new System.Windows.Forms.TextBox();
            this.lvBreaking = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnBreakingDelete = new System.Windows.Forms.Button();
            this.btnBreakingCopy = new System.Windows.Forms.Button();
            this.btnBreakingMake = new System.Windows.Forms.Button();
            this.btnBreakingSave = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(6, 35);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(16, 13);
            this.label51.TabIndex = 139;
            this.label51.Text = "X";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(6, 67);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(16, 13);
            this.label52.TabIndex = 140;
            this.label52.Text = "Y";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(6, 99);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(81, 13);
            this.label53.TabIndex = 141;
            this.label53.Text = "Thickness";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(6, 131);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(89, 13);
            this.label54.TabIndex = 142;
            this.label54.Text = "Cam To Pin";
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(1, 1);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(77, 21);
            this.textBox56.TabIndex = 138;
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(105, 60);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(77, 21);
            this.textBox57.TabIndex = 143;
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(105, 92);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(77, 21);
            this.textBox58.TabIndex = 144;
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(105, 124);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(77, 21);
            this.textBox59.TabIndex = 145;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(1, 1);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(77, 30);
            this.button9.TabIndex = 146;
            this.button9.Text = "Save";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // btnBreakingZigSave
            // 
            this.btnBreakingZigSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingZigSave.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingZigSave.Location = new System.Drawing.Point(168, 167);
            this.btnBreakingZigSave.Name = "btnBreakingZigSave";
            this.btnBreakingZigSave.Size = new System.Drawing.Size(130, 30);
            this.btnBreakingZigSave.TabIndex = 146;
            this.btnBreakingZigSave.Text = "Save";
            this.btnBreakingZigSave.UseVisualStyleBackColor = false;
            this.btnBreakingZigSave.Click += new System.EventHandler(this.btnBreakingZigSave_Click);
            // 
            // txtbreakingZigCamToPin
            // 
            this.txtbreakingZigCamToPin.Location = new System.Drawing.Point(169, 126);
            this.txtbreakingZigCamToPin.Name = "txtbreakingZigCamToPin";
            this.txtbreakingZigCamToPin.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingZigCamToPin.TabIndex = 145;
            this.txtbreakingZigCamToPin.Text = "0.000";
            this.txtbreakingZigCamToPin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingZigThickness
            // 
            this.txtbreakingZigThickness.Location = new System.Drawing.Point(169, 95);
            this.txtbreakingZigThickness.Name = "txtbreakingZigThickness";
            this.txtbreakingZigThickness.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingZigThickness.TabIndex = 144;
            this.txtbreakingZigThickness.Text = "0.000";
            this.txtbreakingZigThickness.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingZigY
            // 
            this.txtbreakingZigY.Location = new System.Drawing.Point(169, 64);
            this.txtbreakingZigY.Name = "txtbreakingZigY";
            this.txtbreakingZigY.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingZigY.TabIndex = 143;
            this.txtbreakingZigY.Text = "0.000";
            this.txtbreakingZigY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingZigX
            // 
            this.txtbreakingZigX.Location = new System.Drawing.Point(169, 33);
            this.txtbreakingZigX.Name = "txtbreakingZigX";
            this.txtbreakingZigX.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingZigX.TabIndex = 138;
            this.txtbreakingZigX.Text = "0.000";
            this.txtbreakingZigX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_breaking_zig_cam_to_pin
            // 
            this.lb_breaking_zig_cam_to_pin.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_cam_to_pin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_breaking_zig_cam_to_pin.ForeColor = System.Drawing.Color.Black;
            this.lb_breaking_zig_cam_to_pin.Location = new System.Drawing.Point(8, 126);
            this.lb_breaking_zig_cam_to_pin.Name = "lb_breaking_zig_cam_to_pin";
            this.lb_breaking_zig_cam_to_pin.Size = new System.Drawing.Size(152, 23);
            this.lb_breaking_zig_cam_to_pin.TabIndex = 142;
            this.lb_breaking_zig_cam_to_pin.Text = "Cam To Pin";
            this.lb_breaking_zig_cam_to_pin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_breaking_zig_thickness
            // 
            this.lb_breaking_zig_thickness.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_thickness.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_breaking_zig_thickness.ForeColor = System.Drawing.Color.Black;
            this.lb_breaking_zig_thickness.Location = new System.Drawing.Point(8, 95);
            this.lb_breaking_zig_thickness.Name = "lb_breaking_zig_thickness";
            this.lb_breaking_zig_thickness.Size = new System.Drawing.Size(152, 23);
            this.lb_breaking_zig_thickness.TabIndex = 141;
            this.lb_breaking_zig_thickness.Text = "Thickness";
            this.lb_breaking_zig_thickness.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_breaking_zig_y
            // 
            this.lb_breaking_zig_y.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_breaking_zig_y.ForeColor = System.Drawing.Color.Black;
            this.lb_breaking_zig_y.Location = new System.Drawing.Point(8, 66);
            this.lb_breaking_zig_y.Name = "lb_breaking_zig_y";
            this.lb_breaking_zig_y.Size = new System.Drawing.Size(152, 23);
            this.lb_breaking_zig_y.TabIndex = 140;
            this.lb_breaking_zig_y.Text = "Y";
            this.lb_breaking_zig_y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_breaking_zig_x
            // 
            this.lb_breaking_zig_x.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_x.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_breaking_zig_x.ForeColor = System.Drawing.Color.Black;
            this.lb_breaking_zig_x.Location = new System.Drawing.Point(8, 33);
            this.lb_breaking_zig_x.Name = "lb_breaking_zig_x";
            this.lb_breaking_zig_x.Size = new System.Drawing.Size(152, 23);
            this.lb_breaking_zig_x.TabIndex = 139;
            this.lb_breaking_zig_x.Text = "X";
            this.lb_breaking_zig_x.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnBreakingZigSave);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtbreakingZigCamToPin);
            this.panel2.Controls.Add(this.lb_breaking_zig_thickness);
            this.panel2.Controls.Add(this.txtbreakingZigThickness);
            this.panel2.Controls.Add(this.lb_breaking_zig_x);
            this.panel2.Controls.Add(this.txtbreakingZigY);
            this.panel2.Controls.Add(this.lb_breaking_zig_y);
            this.panel2.Controls.Add(this.txtbreakingZigX);
            this.panel2.Controls.Add(this.lb_breaking_zig_cam_to_pin);
            this.panel2.Location = new System.Drawing.Point(137, 483);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(303, 202);
            this.panel2.TabIndex = 456;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(301, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ 브레이킹 Zig";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Controls.Add(this.label70);
            this.panel1.Controls.Add(this.label71);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.label68);
            this.panel1.Controls.Add(this.label69);
            this.panel1.Controls.Add(this.btnDefaultProcessSave);
            this.panel1.Controls.Add(this.txtdefaultProcessPreAlignErrorY);
            this.panel1.Controls.Add(this.txtdefaultProcessAlignAngle);
            this.panel1.Controls.Add(this.txtdefaultProcessPreAlignErrorX);
            this.panel1.Controls.Add(this.txtdefaultProcessAlignDistance);
            this.panel1.Controls.Add(this.txtdefaultProcessBreakErrorY);
            this.panel1.Controls.Add(this.txtdefaultProcessPreAlignErrorAngle);
            this.panel1.Controls.Add(this.txtdefaultProcessAlignMatch);
            this.panel1.Controls.Add(this.txtdefaultProcessDefaultZ);
            this.panel1.Controls.Add(this.txtdefaultProcessJumpSpeed);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Location = new System.Drawing.Point(446, 587);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1291, 163);
            this.panel1.TabIndex = 457;
            // 
            // btnDefaultProcessSave
            // 
            this.btnDefaultProcessSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnDefaultProcessSave.ForeColor = System.Drawing.Color.Black;
            this.btnDefaultProcessSave.Location = new System.Drawing.Point(1129, 124);
            this.btnDefaultProcessSave.Name = "btnDefaultProcessSave";
            this.btnDefaultProcessSave.Size = new System.Drawing.Size(152, 30);
            this.btnDefaultProcessSave.TabIndex = 179;
            this.btnDefaultProcessSave.Text = "Save";
            this.btnDefaultProcessSave.UseVisualStyleBackColor = false;
            this.btnDefaultProcessSave.Click += new System.EventHandler(this.btnDefaultProcessSave_Click);
            // 
            // txtdefaultProcessPreAlignErrorY
            // 
            this.txtdefaultProcessPreAlignErrorY.Location = new System.Drawing.Point(1130, 36);
            this.txtdefaultProcessPreAlignErrorY.Name = "txtdefaultProcessPreAlignErrorY";
            this.txtdefaultProcessPreAlignErrorY.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessPreAlignErrorY.TabIndex = 178;
            this.txtdefaultProcessPreAlignErrorY.Text = "0.000";
            this.txtdefaultProcessPreAlignErrorY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessAlignAngle
            // 
            this.txtdefaultProcessAlignAngle.Location = new System.Drawing.Point(1130, 6);
            this.txtdefaultProcessAlignAngle.Name = "txtdefaultProcessAlignAngle";
            this.txtdefaultProcessAlignAngle.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessAlignAngle.TabIndex = 177;
            this.txtdefaultProcessAlignAngle.Text = "0.000";
            this.txtdefaultProcessAlignAngle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessPreAlignErrorX
            // 
            this.txtdefaultProcessPreAlignErrorX.Location = new System.Drawing.Point(812, 36);
            this.txtdefaultProcessPreAlignErrorX.Name = "txtdefaultProcessPreAlignErrorX";
            this.txtdefaultProcessPreAlignErrorX.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessPreAlignErrorX.TabIndex = 176;
            this.txtdefaultProcessPreAlignErrorX.Text = "0.000";
            this.txtdefaultProcessPreAlignErrorX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessAlignDistance
            // 
            this.txtdefaultProcessAlignDistance.Location = new System.Drawing.Point(812, 6);
            this.txtdefaultProcessAlignDistance.Name = "txtdefaultProcessAlignDistance";
            this.txtdefaultProcessAlignDistance.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessAlignDistance.TabIndex = 175;
            this.txtdefaultProcessAlignDistance.Text = "0.000";
            this.txtdefaultProcessAlignDistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessBreakErrorY
            // 
            this.txtdefaultProcessBreakErrorY.Location = new System.Drawing.Point(494, 65);
            this.txtdefaultProcessBreakErrorY.Name = "txtdefaultProcessBreakErrorY";
            this.txtdefaultProcessBreakErrorY.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessBreakErrorY.TabIndex = 174;
            this.txtdefaultProcessBreakErrorY.Text = "0.000";
            this.txtdefaultProcessBreakErrorY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessPreAlignErrorAngle
            // 
            this.txtdefaultProcessPreAlignErrorAngle.Location = new System.Drawing.Point(494, 36);
            this.txtdefaultProcessPreAlignErrorAngle.Name = "txtdefaultProcessPreAlignErrorAngle";
            this.txtdefaultProcessPreAlignErrorAngle.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessPreAlignErrorAngle.TabIndex = 173;
            this.txtdefaultProcessPreAlignErrorAngle.Text = "0.000";
            this.txtdefaultProcessPreAlignErrorAngle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessAlignMatch
            // 
            this.txtdefaultProcessAlignMatch.Location = new System.Drawing.Point(494, 6);
            this.txtdefaultProcessAlignMatch.Name = "txtdefaultProcessAlignMatch";
            this.txtdefaultProcessAlignMatch.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessAlignMatch.TabIndex = 172;
            this.txtdefaultProcessAlignMatch.Text = "0.000";
            this.txtdefaultProcessAlignMatch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessDefaultZ
            // 
            this.txtdefaultProcessDefaultZ.Location = new System.Drawing.Point(176, 63);
            this.txtdefaultProcessDefaultZ.Name = "txtdefaultProcessDefaultZ";
            this.txtdefaultProcessDefaultZ.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessDefaultZ.TabIndex = 171;
            this.txtdefaultProcessDefaultZ.Text = "0.000";
            this.txtdefaultProcessDefaultZ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultProcessJumpSpeed
            // 
            this.txtdefaultProcessJumpSpeed.Location = new System.Drawing.Point(176, 33);
            this.txtdefaultProcessJumpSpeed.Name = "txtdefaultProcessJumpSpeed";
            this.txtdefaultProcessJumpSpeed.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultProcessJumpSpeed.TabIndex = 170;
            this.txtdefaultProcessJumpSpeed.Text = "0.000";
            this.txtdefaultProcessJumpSpeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(971, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(152, 23);
            this.label15.TabIndex = 169;
            this.label15.Text = "PreAlign Error Y";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(971, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(152, 23);
            this.label16.TabIndex = 168;
            this.label16.Text = "Align angle";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(653, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(152, 23);
            this.label17.TabIndex = 167;
            this.label17.Text = "PreAlign Error X";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(653, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(152, 23);
            this.label18.TabIndex = 166;
            this.label18.Text = "Align distance[mm]";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(335, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(152, 23);
            this.label19.TabIndex = 165;
            this.label19.Text = "Break Error Y";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(335, 36);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(152, 23);
            this.label20.TabIndex = 164;
            this.label20.Text = "PreAlign Error Angle";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(335, 6);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(152, 23);
            this.label21.TabIndex = 163;
            this.label21.Text = "Align match[mm]";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(17, 63);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(152, 23);
            this.label22.TabIndex = 162;
            this.label22.Text = "Default Z[mm]";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(17, 33);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(152, 23);
            this.label23.TabIndex = 161;
            this.label23.Text = "Jump speed";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnProcessCopy);
            this.panel3.Controls.Add(this.btnProcessDelete);
            this.panel3.Controls.Add(this.btnProcessMake);
            this.panel3.Controls.Add(this.btnProcessSave);
            this.panel3.Controls.Add(this.txtprocessAccDec);
            this.panel3.Controls.Add(this.txtprocessScan);
            this.panel3.Controls.Add(this.txtprocessG);
            this.panel3.Controls.Add(this.txtprocessSegmentValue);
            this.panel3.Controls.Add(this.txtprocessOverlap);
            this.panel3.Controls.Add(this.txtprocessZpos);
            this.panel3.Controls.Add(this.txtprocessSpeed);
            this.panel3.Controls.Add(this.txtprocessFrequence);
            this.panel3.Controls.Add(this.txtprocessBurst);
            this.panel3.Controls.Add(this.txtprocessError);
            this.panel3.Controls.Add(this.txtprocessPowerRatio);
            this.panel3.Controls.Add(this.txtprocessPower);
            this.panel3.Controls.Add(this.txtprocessDivider);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.lvProcess);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1734, 173);
            this.panel3.TabIndex = 458;
            // 
            // btnProcessCopy
            // 
            this.btnProcessCopy.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessCopy.ForeColor = System.Drawing.Color.Black;
            this.btnProcessCopy.Location = new System.Drawing.Point(142, 138);
            this.btnProcessCopy.Name = "btnProcessCopy";
            this.btnProcessCopy.Size = new System.Drawing.Size(130, 30);
            this.btnProcessCopy.TabIndex = 150;
            this.btnProcessCopy.Text = "Copy";
            this.btnProcessCopy.UseVisualStyleBackColor = false;
            this.btnProcessCopy.Click += new System.EventHandler(this.btnProcessCopy_Click);
            // 
            // btnProcessDelete
            // 
            this.btnProcessDelete.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessDelete.ForeColor = System.Drawing.Color.Black;
            this.btnProcessDelete.Location = new System.Drawing.Point(282, 138);
            this.btnProcessDelete.Name = "btnProcessDelete";
            this.btnProcessDelete.Size = new System.Drawing.Size(130, 30);
            this.btnProcessDelete.TabIndex = 149;
            this.btnProcessDelete.Text = "Delete";
            this.btnProcessDelete.UseVisualStyleBackColor = false;
            this.btnProcessDelete.Click += new System.EventHandler(this.btnProcessDelete_Click);
            // 
            // btnProcessMake
            // 
            this.btnProcessMake.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessMake.ForeColor = System.Drawing.Color.Black;
            this.btnProcessMake.Location = new System.Drawing.Point(2, 138);
            this.btnProcessMake.Name = "btnProcessMake";
            this.btnProcessMake.Size = new System.Drawing.Size(130, 30);
            this.btnProcessMake.TabIndex = 148;
            this.btnProcessMake.Text = "Make";
            this.btnProcessMake.UseVisualStyleBackColor = false;
            this.btnProcessMake.Click += new System.EventHandler(this.btnProcessMake_Click);
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnProcessSave.ForeColor = System.Drawing.Color.Black;
            this.btnProcessSave.Location = new System.Drawing.Point(1573, 138);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Size = new System.Drawing.Size(152, 30);
            this.btnProcessSave.TabIndex = 147;
            this.btnProcessSave.Text = "Save";
            this.btnProcessSave.UseVisualStyleBackColor = false;
            this.btnProcessSave.Click += new System.EventHandler(this.btnProcessSave_Click);
            // 
            // txtprocessAccDec
            // 
            this.txtprocessAccDec.Location = new System.Drawing.Point(1573, 104);
            this.txtprocessAccDec.Name = "txtprocessAccDec";
            this.txtprocessAccDec.Size = new System.Drawing.Size(152, 23);
            this.txtprocessAccDec.TabIndex = 93;
            this.txtprocessAccDec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessScan
            // 
            this.txtprocessScan.Location = new System.Drawing.Point(1573, 71);
            this.txtprocessScan.Name = "txtprocessScan";
            this.txtprocessScan.Size = new System.Drawing.Size(152, 23);
            this.txtprocessScan.TabIndex = 92;
            this.txtprocessScan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessG
            // 
            this.txtprocessG.Location = new System.Drawing.Point(1573, 11);
            this.txtprocessG.Name = "txtprocessG";
            this.txtprocessG.Size = new System.Drawing.Size(152, 23);
            this.txtprocessG.TabIndex = 91;
            this.txtprocessG.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessSegmentValue
            // 
            this.txtprocessSegmentValue.Location = new System.Drawing.Point(1255, 102);
            this.txtprocessSegmentValue.Name = "txtprocessSegmentValue";
            this.txtprocessSegmentValue.Size = new System.Drawing.Size(152, 23);
            this.txtprocessSegmentValue.TabIndex = 90;
            this.txtprocessSegmentValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessOverlap
            // 
            this.txtprocessOverlap.Location = new System.Drawing.Point(1255, 71);
            this.txtprocessOverlap.Name = "txtprocessOverlap";
            this.txtprocessOverlap.Size = new System.Drawing.Size(152, 23);
            this.txtprocessOverlap.TabIndex = 89;
            this.txtprocessOverlap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessZpos
            // 
            this.txtprocessZpos.Location = new System.Drawing.Point(1255, 40);
            this.txtprocessZpos.Name = "txtprocessZpos";
            this.txtprocessZpos.Size = new System.Drawing.Size(152, 23);
            this.txtprocessZpos.TabIndex = 88;
            this.txtprocessZpos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessSpeed
            // 
            this.txtprocessSpeed.Location = new System.Drawing.Point(1255, 9);
            this.txtprocessSpeed.Name = "txtprocessSpeed";
            this.txtprocessSpeed.Size = new System.Drawing.Size(152, 23);
            this.txtprocessSpeed.TabIndex = 87;
            this.txtprocessSpeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessFrequence
            // 
            this.txtprocessFrequence.Location = new System.Drawing.Point(937, 74);
            this.txtprocessFrequence.Name = "txtprocessFrequence";
            this.txtprocessFrequence.Size = new System.Drawing.Size(152, 23);
            this.txtprocessFrequence.TabIndex = 86;
            this.txtprocessFrequence.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessBurst
            // 
            this.txtprocessBurst.Location = new System.Drawing.Point(619, 73);
            this.txtprocessBurst.Name = "txtprocessBurst";
            this.txtprocessBurst.Size = new System.Drawing.Size(152, 23);
            this.txtprocessBurst.TabIndex = 85;
            this.txtprocessBurst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessError
            // 
            this.txtprocessError.Location = new System.Drawing.Point(619, 43);
            this.txtprocessError.Name = "txtprocessError";
            this.txtprocessError.Size = new System.Drawing.Size(152, 23);
            this.txtprocessError.TabIndex = 84;
            this.txtprocessError.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessPowerRatio
            // 
            this.txtprocessPowerRatio.Location = new System.Drawing.Point(301, 72);
            this.txtprocessPowerRatio.Name = "txtprocessPowerRatio";
            this.txtprocessPowerRatio.Size = new System.Drawing.Size(152, 23);
            this.txtprocessPowerRatio.TabIndex = 83;
            this.txtprocessPowerRatio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessPower
            // 
            this.txtprocessPower.Location = new System.Drawing.Point(301, 41);
            this.txtprocessPower.Name = "txtprocessPower";
            this.txtprocessPower.Size = new System.Drawing.Size(152, 23);
            this.txtprocessPower.TabIndex = 82;
            this.txtprocessPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtprocessDivider
            // 
            this.txtprocessDivider.Location = new System.Drawing.Point(301, 10);
            this.txtprocessDivider.Name = "txtprocessDivider";
            this.txtprocessDivider.Size = new System.Drawing.Size(152, 23);
            this.txtprocessDivider.TabIndex = 81;
            this.txtprocessDivider.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(1414, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 23);
            this.label1.TabIndex = 80;
            this.label1.Text = "Acc dec";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(1414, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 23);
            this.label3.TabIndex = 79;
            this.label3.Text = "Scan";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(1414, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 23);
            this.label4.TabIndex = 78;
            this.label4.Text = "G";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(1096, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 23);
            this.label5.TabIndex = 77;
            this.label5.Text = "Segment Value";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(1096, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 23);
            this.label6.TabIndex = 76;
            this.label6.Text = "Overlap";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(1096, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 23);
            this.label7.TabIndex = 75;
            this.label7.Text = "ZPos[mm]";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(1096, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 23);
            this.label8.TabIndex = 74;
            this.label8.Text = "Speed";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(778, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 23);
            this.label9.TabIndex = 73;
            this.label9.Text = "Frequence";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(460, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(152, 23);
            this.label10.TabIndex = 72;
            this.label10.Text = "Burst";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(460, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(152, 23);
            this.label11.TabIndex = 71;
            this.label11.Text = "ERROR";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(142, 72);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(152, 23);
            this.label12.TabIndex = 70;
            this.label12.Text = "Power ratio";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(142, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(152, 23);
            this.label13.TabIndex = 69;
            this.label13.Text = "Power[w]";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(142, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 23);
            this.label14.TabIndex = 68;
            this.label14.Text = "Divider";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lvProcess
            // 
            this.lvProcess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3});
            this.lvProcess.FullRowSelect = true;
            this.lvProcess.GridLines = true;
            this.lvProcess.Location = new System.Drawing.Point(3, 3);
            this.lvProcess.MultiSelect = false;
            this.lvProcess.Name = "lvProcess";
            this.lvProcess.Size = new System.Drawing.Size(129, 130);
            this.lvProcess.TabIndex = 1;
            this.lvProcess.UseCompatibleStateImageBehavior = false;
            this.lvProcess.View = System.Windows.Forms.View.Details;
            this.lvProcess.SelectedIndexChanged += new System.EventHandler(this.lvProcess_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 120;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.txtdefaultLaserCstPitchReverse);
            this.panel4.Controls.Add(this.txtdefaultLaserCstPitchNormal);
            this.panel4.Controls.Add(this.txtdefaultLaserOntime);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.btndefaultLaserSave);
            this.panel4.Location = new System.Drawing.Point(446, 483);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1291, 98);
            this.panel4.TabIndex = 458;
            // 
            // txtdefaultLaserCstPitchReverse
            // 
            this.txtdefaultLaserCstPitchReverse.Location = new System.Drawing.Point(494, 41);
            this.txtdefaultLaserCstPitchReverse.Name = "txtdefaultLaserCstPitchReverse";
            this.txtdefaultLaserCstPitchReverse.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultLaserCstPitchReverse.TabIndex = 185;
            this.txtdefaultLaserCstPitchReverse.Text = "0.000";
            this.txtdefaultLaserCstPitchReverse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultLaserCstPitchNormal
            // 
            this.txtdefaultLaserCstPitchNormal.Location = new System.Drawing.Point(176, 41);
            this.txtdefaultLaserCstPitchNormal.Name = "txtdefaultLaserCstPitchNormal";
            this.txtdefaultLaserCstPitchNormal.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultLaserCstPitchNormal.TabIndex = 184;
            this.txtdefaultLaserCstPitchNormal.Text = "0.000";
            this.txtdefaultLaserCstPitchNormal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdefaultLaserOntime
            // 
            this.txtdefaultLaserOntime.Location = new System.Drawing.Point(176, 14);
            this.txtdefaultLaserOntime.Name = "txtdefaultLaserOntime";
            this.txtdefaultLaserOntime.Size = new System.Drawing.Size(152, 23);
            this.txtdefaultLaserOntime.TabIndex = 183;
            this.txtdefaultLaserOntime.Text = "0.000";
            this.txtdefaultLaserOntime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(335, 41);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(152, 46);
            this.label24.TabIndex = 182;
            this.label24.Text = "Cassette pitch\r\n(Reverse)";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(17, 41);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(152, 46);
            this.label25.TabIndex = 181;
            this.label25.Text = "Cassette pitch\r\n(Normal)";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(17, 13);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(152, 23);
            this.label26.TabIndex = 180;
            this.label26.Text = "On time[s]";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btndefaultLaserSave
            // 
            this.btndefaultLaserSave.BackColor = System.Drawing.SystemColors.Control;
            this.btndefaultLaserSave.ForeColor = System.Drawing.Color.Black;
            this.btndefaultLaserSave.Location = new System.Drawing.Point(1130, 63);
            this.btndefaultLaserSave.Name = "btndefaultLaserSave";
            this.btndefaultLaserSave.Size = new System.Drawing.Size(152, 30);
            this.btndefaultLaserSave.TabIndex = 179;
            this.btndefaultLaserSave.Text = "Save";
            this.btndefaultLaserSave.UseVisualStyleBackColor = false;
            this.btndefaultLaserSave.Click += new System.EventHandler(this.btnDefaultLaserSave_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.btnBreakingXytBreaking4Calc);
            this.panel5.Controls.Add(this.btnBreakingXytBreaking3Calc);
            this.panel5.Controls.Add(this.btnBreakingXytBreaking2Calc);
            this.panel5.Controls.Add(this.btnBreakingXytBreaking1Calc);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.label56);
            this.panel5.Controls.Add(this.txtbreakingFastdownSpeed);
            this.panel5.Controls.Add(this.label55);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.label48);
            this.panel5.Controls.Add(this.label44);
            this.panel5.Controls.Add(this.panelBreaking2pass);
            this.panel5.Controls.Add(this.label41);
            this.panel5.Controls.Add(this.txtbreakingSlowdownSpeed);
            this.panel5.Controls.Add(this.label27);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.label29);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.label31);
            this.panel5.Controls.Add(this.txtbreakingSlowdownPitch);
            this.panel5.Controls.Add(this.txtbreakingFastdownPitch);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Controls.Add(this.txtbreakingBMcrOffsetXy2);
            this.panel5.Controls.Add(this.txtbreakingAMcrOffsetXy2);
            this.panel5.Controls.Add(this.txtbreakingBMcrOffsetXy1);
            this.panel5.Controls.Add(this.label33);
            this.panel5.Controls.Add(this.txtbreakingAMcrOffsetXy1);
            this.panel5.Controls.Add(this.label34);
            this.panel5.Controls.Add(this.txtbreakingB2XyOffset2);
            this.panel5.Controls.Add(this.txtbreakingB2XyOffset1);
            this.panel5.Controls.Add(this.label35);
            this.panel5.Controls.Add(this.txtbreakingB1XyOffset2);
            this.panel5.Controls.Add(this.txtbreakingB1XyOffset1);
            this.panel5.Controls.Add(this.label36);
            this.panel5.Controls.Add(this.txtbreakingA2XyOffset2);
            this.panel5.Controls.Add(this.txtbreakingA2XyOffset1);
            this.panel5.Controls.Add(this.label37);
            this.panel5.Controls.Add(this.txtbreakingA1XyOffset2);
            this.panel5.Controls.Add(this.txtbreakingA1XyOffset1);
            this.panel5.Controls.Add(this.label38);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking43);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking42);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking41);
            this.panel5.Controls.Add(this.label39);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking33);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking32);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking31);
            this.panel5.Controls.Add(this.label40);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking23);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking22);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking21);
            this.panel5.Controls.Add(this.label43);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking13);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking12);
            this.panel5.Controls.Add(this.txtbreakingXytBreaking11);
            this.panel5.Controls.Add(this.txtbreakingXyMcr2);
            this.panel5.Controls.Add(this.txtbreakingXyAlignMark22);
            this.panel5.Controls.Add(this.txtbreakingXyAlignMark12);
            this.panel5.Controls.Add(this.txtbreakingXyPinCenter2);
            this.panel5.Controls.Add(this.label45);
            this.panel5.Controls.Add(this.txtbreakingPinDistanceB);
            this.panel5.Controls.Add(this.label46);
            this.panel5.Controls.Add(this.txtbreakingPinDistanceA);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.txtbreakingXyMcr1);
            this.panel5.Controls.Add(this.txtbreakingXyAlignMark21);
            this.panel5.Controls.Add(this.txtbreakingXyAlignMark11);
            this.panel5.Controls.Add(this.txtbreakingXyPinCenter1);
            this.panel5.Controls.Add(this.lvBreaking);
            this.panel5.Controls.Add(this.btnBreakingDelete);
            this.panel5.Controls.Add(this.btnBreakingCopy);
            this.panel5.Controls.Add(this.btnBreakingMake);
            this.panel5.Controls.Add(this.btnBreakingSave);
            this.panel5.Location = new System.Drawing.Point(3, 178);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1734, 299);
            this.panel5.TabIndex = 458;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.label42);
            this.panel6.Location = new System.Drawing.Point(1350, 134);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(375, 123);
            this.panel6.TabIndex = 457;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label64);
            this.panel8.Controls.Add(this.label65);
            this.panel8.Controls.Add(this.txtbreakingLoadTransSequenceOffsetBY2);
            this.panel8.Controls.Add(this.txtbreakingLoadTransSequenceOffsetBY1);
            this.panel8.Controls.Add(this.label66);
            this.panel8.Controls.Add(this.label67);
            this.panel8.Controls.Add(this.label59);
            this.panel8.Location = new System.Drawing.Point(190, 27);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(175, 91);
            this.panel8.TabIndex = 459;
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(124, 60);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(46, 23);
            this.label64.TabIndex = 154;
            this.label64.Text = "(mm)";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(124, 29);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(46, 23);
            this.label65.TabIndex = 153;
            this.label65.Text = "(mm)";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingLoadTransSequenceOffsetBY2
            // 
            this.txtbreakingLoadTransSequenceOffsetBY2.Location = new System.Drawing.Point(58, 60);
            this.txtbreakingLoadTransSequenceOffsetBY2.Name = "txtbreakingLoadTransSequenceOffsetBY2";
            this.txtbreakingLoadTransSequenceOffsetBY2.Size = new System.Drawing.Size(58, 23);
            this.txtbreakingLoadTransSequenceOffsetBY2.TabIndex = 152;
            this.txtbreakingLoadTransSequenceOffsetBY2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingLoadTransSequenceOffsetBY1
            // 
            this.txtbreakingLoadTransSequenceOffsetBY1.Location = new System.Drawing.Point(59, 29);
            this.txtbreakingLoadTransSequenceOffsetBY1.Name = "txtbreakingLoadTransSequenceOffsetBY1";
            this.txtbreakingLoadTransSequenceOffsetBY1.Size = new System.Drawing.Size(58, 23);
            this.txtbreakingLoadTransSequenceOffsetBY1.TabIndex = 149;
            this.txtbreakingLoadTransSequenceOffsetBY1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(7, 60);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(46, 23);
            this.label66.TabIndex = 151;
            this.label66.Text = "Y2";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(7, 29);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(46, 23);
            this.label67.TabIndex = 150;
            this.label67.Text = "Y1";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.AutoEllipsis = true;
            this.label59.BackColor = System.Drawing.Color.Gainsboro;
            this.label59.Dock = System.Windows.Forms.DockStyle.Top;
            this.label59.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(0, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(173, 24);
            this.label59.TabIndex = 9;
            this.label59.Text = "■ B";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label60);
            this.panel7.Controls.Add(this.label61);
            this.panel7.Controls.Add(this.txtbreakingLoadTransSequenceOffsetAY2);
            this.panel7.Controls.Add(this.txtbreakingLoadTransSequenceOffsetAY1);
            this.panel7.Controls.Add(this.label62);
            this.panel7.Controls.Add(this.label63);
            this.panel7.Controls.Add(this.label58);
            this.panel7.Location = new System.Drawing.Point(8, 27);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(175, 91);
            this.panel7.TabIndex = 458;
            // 
            // label60
            // 
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(124, 60);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(46, 23);
            this.label60.TabIndex = 148;
            this.label60.Text = "(mm)";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label61
            // 
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(124, 29);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(46, 23);
            this.label61.TabIndex = 147;
            this.label61.Text = "(mm)";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingLoadTransSequenceOffsetAY2
            // 
            this.txtbreakingLoadTransSequenceOffsetAY2.Location = new System.Drawing.Point(58, 60);
            this.txtbreakingLoadTransSequenceOffsetAY2.Name = "txtbreakingLoadTransSequenceOffsetAY2";
            this.txtbreakingLoadTransSequenceOffsetAY2.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingLoadTransSequenceOffsetAY2.TabIndex = 146;
            this.txtbreakingLoadTransSequenceOffsetAY2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingLoadTransSequenceOffsetAY1
            // 
            this.txtbreakingLoadTransSequenceOffsetAY1.Location = new System.Drawing.Point(59, 29);
            this.txtbreakingLoadTransSequenceOffsetAY1.Name = "txtbreakingLoadTransSequenceOffsetAY1";
            this.txtbreakingLoadTransSequenceOffsetAY1.Size = new System.Drawing.Size(58, 23);
            this.txtbreakingLoadTransSequenceOffsetAY1.TabIndex = 143;
            this.txtbreakingLoadTransSequenceOffsetAY1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label62
            // 
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(7, 60);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(47, 23);
            this.label62.TabIndex = 145;
            this.label62.Text = "Y2";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label63
            // 
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(7, 29);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(46, 23);
            this.label63.TabIndex = 144;
            this.label63.Text = "Y1";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.AutoEllipsis = true;
            this.label58.BackColor = System.Drawing.Color.Gainsboro;
            this.label58.Dock = System.Windows.Forms.DockStyle.Top;
            this.label58.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(0, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(173, 24);
            this.label58.TabIndex = 9;
            this.label58.Text = "■ A";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.AutoEllipsis = true;
            this.label42.BackColor = System.Drawing.Color.Gainsboro;
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(0, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(373, 24);
            this.label42.TabIndex = 9;
            this.label42.Text = "■ 로드 이재기 Sequence Offset";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakingXytBreaking4Calc
            // 
            this.btnBreakingXytBreaking4Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingXytBreaking4Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingXytBreaking4Calc.Location = new System.Drawing.Point(996, 134);
            this.btnBreakingXytBreaking4Calc.Name = "btnBreakingXytBreaking4Calc";
            this.btnBreakingXytBreaking4Calc.Size = new System.Drawing.Size(54, 23);
            this.btnBreakingXytBreaking4Calc.TabIndex = 259;
            this.btnBreakingXytBreaking4Calc.Text = "Calc";
            this.btnBreakingXytBreaking4Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakingXytBreaking3Calc
            // 
            this.btnBreakingXytBreaking3Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingXytBreaking3Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingXytBreaking3Calc.Location = new System.Drawing.Point(996, 105);
            this.btnBreakingXytBreaking3Calc.Name = "btnBreakingXytBreaking3Calc";
            this.btnBreakingXytBreaking3Calc.Size = new System.Drawing.Size(54, 23);
            this.btnBreakingXytBreaking3Calc.TabIndex = 258;
            this.btnBreakingXytBreaking3Calc.Text = "Calc";
            this.btnBreakingXytBreaking3Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakingXytBreaking2Calc
            // 
            this.btnBreakingXytBreaking2Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingXytBreaking2Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingXytBreaking2Calc.Location = new System.Drawing.Point(996, 75);
            this.btnBreakingXytBreaking2Calc.Name = "btnBreakingXytBreaking2Calc";
            this.btnBreakingXytBreaking2Calc.Size = new System.Drawing.Size(54, 23);
            this.btnBreakingXytBreaking2Calc.TabIndex = 257;
            this.btnBreakingXytBreaking2Calc.Text = "Calc";
            this.btnBreakingXytBreaking2Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakingXytBreaking1Calc
            // 
            this.btnBreakingXytBreaking1Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingXytBreaking1Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingXytBreaking1Calc.Location = new System.Drawing.Point(996, 45);
            this.btnBreakingXytBreaking1Calc.Name = "btnBreakingXytBreaking1Calc";
            this.btnBreakingXytBreaking1Calc.Size = new System.Drawing.Size(54, 23);
            this.btnBreakingXytBreaking1Calc.TabIndex = 256;
            this.btnBreakingXytBreaking1Calc.Text = "Calc";
            this.btnBreakingXytBreaking1Calc.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.Color.Red;
            this.label57.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(142, 104);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(152, 25);
            this.label57.TabIndex = 255;
            this.label57.Text = "XY MCR[mm]";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.Color.Blue;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(142, 74);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(152, 25);
            this.label56.TabIndex = 254;
            this.label56.Text = "XY Align Mark2[mm]";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingFastdownSpeed
            // 
            this.txtbreakingFastdownSpeed.Location = new System.Drawing.Point(720, 190);
            this.txtbreakingFastdownSpeed.Name = "txtbreakingFastdownSpeed";
            this.txtbreakingFastdownSpeed.Size = new System.Drawing.Size(115, 23);
            this.txtbreakingFastdownSpeed.TabIndex = 244;
            this.txtbreakingFastdownSpeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.Color.LimeGreen;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(142, 44);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(152, 25);
            this.label55.TabIndex = 253;
            this.label55.Text = "XY Align Mark1[mm]";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.Color.Yellow;
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(142, 14);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(152, 25);
            this.label50.TabIndex = 252;
            this.label50.Text = "XY Pin Center[mm]";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.Aquamarine;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(904, 15);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(83, 23);
            this.label49.TabIndex = 251;
            this.label49.Text = "Half";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.Color.Aquamarine;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(812, 15);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(83, 23);
            this.label48.TabIndex = 250;
            this.label48.Text = "Right";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.Color.Aquamarine;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(720, 15);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(83, 23);
            this.label44.TabIndex = 249;
            this.label44.Text = "Left";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelBreaking2pass
            // 
            this.panelBreaking2pass.BackColor = System.Drawing.Color.Aquamarine;
            this.panelBreaking2pass.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelBreaking2pass.ForeColor = System.Drawing.Color.Black;
            this.panelBreaking2pass.Location = new System.Drawing.Point(1702, 105);
            this.panelBreaking2pass.Name = "panelBreaking2pass";
            this.panelBreaking2pass.Size = new System.Drawing.Size(23, 23);
            this.panelBreaking2pass.TabIndex = 248;
            this.panelBreaking2pass.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(1573, 105);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(123, 23);
            this.label41.TabIndex = 247;
            this.label41.Text = "상하부 가공";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingSlowdownSpeed
            // 
            this.txtbreakingSlowdownSpeed.Location = new System.Drawing.Point(720, 218);
            this.txtbreakingSlowdownSpeed.Name = "txtbreakingSlowdownSpeed";
            this.txtbreakingSlowdownSpeed.Size = new System.Drawing.Size(115, 23);
            this.txtbreakingSlowdownSpeed.TabIndex = 246;
            this.txtbreakingSlowdownSpeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(581, 218);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(130, 23);
            this.label27.TabIndex = 245;
            this.label27.Text = "Z축 저속 하강 속도";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(581, 190);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(130, 23);
            this.label28.TabIndex = 243;
            this.label28.Text = "Z축 고속 하강 속도";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(442, 218);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(126, 23);
            this.label29.TabIndex = 242;
            this.label29.Text = "(mm)";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(442, 190);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(126, 23);
            this.label30.TabIndex = 241;
            this.label30.Text = "(mm)";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(142, 218);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(152, 23);
            this.label31.TabIndex = 240;
            this.label31.Text = "Z축 저속하강 pitch";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingSlowdownPitch
            // 
            this.txtbreakingSlowdownPitch.Location = new System.Drawing.Point(303, 218);
            this.txtbreakingSlowdownPitch.Name = "txtbreakingSlowdownPitch";
            this.txtbreakingSlowdownPitch.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingSlowdownPitch.TabIndex = 239;
            this.txtbreakingSlowdownPitch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingFastdownPitch
            // 
            this.txtbreakingFastdownPitch.Location = new System.Drawing.Point(303, 189);
            this.txtbreakingFastdownPitch.Name = "txtbreakingFastdownPitch";
            this.txtbreakingFastdownPitch.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingFastdownPitch.TabIndex = 238;
            this.txtbreakingFastdownPitch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(142, 190);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(152, 23);
            this.label32.TabIndex = 237;
            this.label32.Text = "Z축 고속하강 pitch";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingBMcrOffsetXy2
            // 
            this.txtbreakingBMcrOffsetXy2.Location = new System.Drawing.Point(1635, 75);
            this.txtbreakingBMcrOffsetXy2.Name = "txtbreakingBMcrOffsetXy2";
            this.txtbreakingBMcrOffsetXy2.Size = new System.Drawing.Size(90, 23);
            this.txtbreakingBMcrOffsetXy2.TabIndex = 236;
            this.txtbreakingBMcrOffsetXy2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingAMcrOffsetXy2
            // 
            this.txtbreakingAMcrOffsetXy2.Location = new System.Drawing.Point(1635, 45);
            this.txtbreakingAMcrOffsetXy2.Name = "txtbreakingAMcrOffsetXy2";
            this.txtbreakingAMcrOffsetXy2.Size = new System.Drawing.Size(90, 23);
            this.txtbreakingAMcrOffsetXy2.TabIndex = 235;
            this.txtbreakingAMcrOffsetXy2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingBMcrOffsetXy1
            // 
            this.txtbreakingBMcrOffsetXy1.Location = new System.Drawing.Point(1536, 75);
            this.txtbreakingBMcrOffsetXy1.Name = "txtbreakingBMcrOffsetXy1";
            this.txtbreakingBMcrOffsetXy1.Size = new System.Drawing.Size(90, 23);
            this.txtbreakingBMcrOffsetXy1.TabIndex = 234;
            this.txtbreakingBMcrOffsetXy1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(1350, 75);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(177, 23);
            this.label33.TabIndex = 233;
            this.label33.Text = "B열 MCR Offset XY[mm]";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingAMcrOffsetXy1
            // 
            this.txtbreakingAMcrOffsetXy1.Location = new System.Drawing.Point(1536, 45);
            this.txtbreakingAMcrOffsetXy1.Name = "txtbreakingAMcrOffsetXy1";
            this.txtbreakingAMcrOffsetXy1.Size = new System.Drawing.Size(90, 23);
            this.txtbreakingAMcrOffsetXy1.TabIndex = 232;
            this.txtbreakingAMcrOffsetXy1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(1350, 45);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(177, 23);
            this.label34.TabIndex = 231;
            this.label34.Text = "A열 MCR Offset XY[mm]";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingB2XyOffset2
            // 
            this.txtbreakingB2XyOffset2.Location = new System.Drawing.Point(1282, 134);
            this.txtbreakingB2XyOffset2.Name = "txtbreakingB2XyOffset2";
            this.txtbreakingB2XyOffset2.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingB2XyOffset2.TabIndex = 230;
            this.txtbreakingB2XyOffset2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingB2XyOffset1
            // 
            this.txtbreakingB2XyOffset1.Location = new System.Drawing.Point(1214, 134);
            this.txtbreakingB2XyOffset1.Name = "txtbreakingB2XyOffset1";
            this.txtbreakingB2XyOffset1.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingB2XyOffset1.TabIndex = 229;
            this.txtbreakingB2XyOffset1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(1059, 134);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(146, 23);
            this.label35.TabIndex = 228;
            this.label35.Text = "B열2 XY offset[mm]";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingB1XyOffset2
            // 
            this.txtbreakingB1XyOffset2.Location = new System.Drawing.Point(1282, 105);
            this.txtbreakingB1XyOffset2.Name = "txtbreakingB1XyOffset2";
            this.txtbreakingB1XyOffset2.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingB1XyOffset2.TabIndex = 227;
            this.txtbreakingB1XyOffset2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingB1XyOffset1
            // 
            this.txtbreakingB1XyOffset1.Location = new System.Drawing.Point(1214, 105);
            this.txtbreakingB1XyOffset1.Name = "txtbreakingB1XyOffset1";
            this.txtbreakingB1XyOffset1.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingB1XyOffset1.TabIndex = 226;
            this.txtbreakingB1XyOffset1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(1059, 105);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(146, 23);
            this.label36.TabIndex = 225;
            this.label36.Text = "B열1 XY offset[mm]";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingA2XyOffset2
            // 
            this.txtbreakingA2XyOffset2.Location = new System.Drawing.Point(1282, 75);
            this.txtbreakingA2XyOffset2.Name = "txtbreakingA2XyOffset2";
            this.txtbreakingA2XyOffset2.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingA2XyOffset2.TabIndex = 224;
            this.txtbreakingA2XyOffset2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingA2XyOffset1
            // 
            this.txtbreakingA2XyOffset1.Location = new System.Drawing.Point(1214, 75);
            this.txtbreakingA2XyOffset1.Name = "txtbreakingA2XyOffset1";
            this.txtbreakingA2XyOffset1.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingA2XyOffset1.TabIndex = 223;
            this.txtbreakingA2XyOffset1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(1059, 75);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(146, 23);
            this.label37.TabIndex = 222;
            this.label37.Text = "A열2 XY offset[mm]";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingA1XyOffset2
            // 
            this.txtbreakingA1XyOffset2.Location = new System.Drawing.Point(1282, 45);
            this.txtbreakingA1XyOffset2.Name = "txtbreakingA1XyOffset2";
            this.txtbreakingA1XyOffset2.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingA1XyOffset2.TabIndex = 221;
            this.txtbreakingA1XyOffset2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingA1XyOffset1
            // 
            this.txtbreakingA1XyOffset1.Location = new System.Drawing.Point(1214, 45);
            this.txtbreakingA1XyOffset1.Name = "txtbreakingA1XyOffset1";
            this.txtbreakingA1XyOffset1.Size = new System.Drawing.Size(59, 23);
            this.txtbreakingA1XyOffset1.TabIndex = 220;
            this.txtbreakingA1XyOffset1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(1059, 45);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(146, 23);
            this.label38.TabIndex = 219;
            this.label38.Text = "A열1 XY offset[mm]";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingXytBreaking43
            // 
            this.txtbreakingXytBreaking43.Location = new System.Drawing.Point(904, 134);
            this.txtbreakingXytBreaking43.Name = "txtbreakingXytBreaking43";
            this.txtbreakingXytBreaking43.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking43.TabIndex = 218;
            this.txtbreakingXytBreaking43.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking42
            // 
            this.txtbreakingXytBreaking42.Location = new System.Drawing.Point(812, 134);
            this.txtbreakingXytBreaking42.Name = "txtbreakingXytBreaking42";
            this.txtbreakingXytBreaking42.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking42.TabIndex = 217;
            this.txtbreakingXytBreaking42.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking41
            // 
            this.txtbreakingXytBreaking41.Location = new System.Drawing.Point(720, 134);
            this.txtbreakingXytBreaking41.Name = "txtbreakingXytBreaking41";
            this.txtbreakingXytBreaking41.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking41.TabIndex = 216;
            this.txtbreakingXytBreaking41.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(581, 134);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(130, 23);
            this.label39.TabIndex = 215;
            this.label39.Text = "XYT Breaking4[mm]";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingXytBreaking33
            // 
            this.txtbreakingXytBreaking33.Location = new System.Drawing.Point(904, 105);
            this.txtbreakingXytBreaking33.Name = "txtbreakingXytBreaking33";
            this.txtbreakingXytBreaking33.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking33.TabIndex = 214;
            this.txtbreakingXytBreaking33.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking32
            // 
            this.txtbreakingXytBreaking32.Location = new System.Drawing.Point(812, 105);
            this.txtbreakingXytBreaking32.Name = "txtbreakingXytBreaking32";
            this.txtbreakingXytBreaking32.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking32.TabIndex = 213;
            this.txtbreakingXytBreaking32.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking31
            // 
            this.txtbreakingXytBreaking31.Location = new System.Drawing.Point(720, 105);
            this.txtbreakingXytBreaking31.Name = "txtbreakingXytBreaking31";
            this.txtbreakingXytBreaking31.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking31.TabIndex = 212;
            this.txtbreakingXytBreaking31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(581, 105);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(130, 23);
            this.label40.TabIndex = 211;
            this.label40.Text = "XYT Breaking3[mm]";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingXytBreaking23
            // 
            this.txtbreakingXytBreaking23.Location = new System.Drawing.Point(904, 75);
            this.txtbreakingXytBreaking23.Name = "txtbreakingXytBreaking23";
            this.txtbreakingXytBreaking23.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking23.TabIndex = 210;
            this.txtbreakingXytBreaking23.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking22
            // 
            this.txtbreakingXytBreaking22.Location = new System.Drawing.Point(812, 75);
            this.txtbreakingXytBreaking22.Name = "txtbreakingXytBreaking22";
            this.txtbreakingXytBreaking22.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking22.TabIndex = 209;
            this.txtbreakingXytBreaking22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking21
            // 
            this.txtbreakingXytBreaking21.Location = new System.Drawing.Point(720, 75);
            this.txtbreakingXytBreaking21.Name = "txtbreakingXytBreaking21";
            this.txtbreakingXytBreaking21.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking21.TabIndex = 208;
            this.txtbreakingXytBreaking21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(581, 75);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(130, 23);
            this.label43.TabIndex = 207;
            this.label43.Text = "XYT Breaking2[mm]";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingXytBreaking13
            // 
            this.txtbreakingXytBreaking13.Location = new System.Drawing.Point(904, 45);
            this.txtbreakingXytBreaking13.Name = "txtbreakingXytBreaking13";
            this.txtbreakingXytBreaking13.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking13.TabIndex = 206;
            this.txtbreakingXytBreaking13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking12
            // 
            this.txtbreakingXytBreaking12.Location = new System.Drawing.Point(812, 45);
            this.txtbreakingXytBreaking12.Name = "txtbreakingXytBreaking12";
            this.txtbreakingXytBreaking12.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking12.TabIndex = 205;
            this.txtbreakingXytBreaking12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXytBreaking11
            // 
            this.txtbreakingXytBreaking11.Location = new System.Drawing.Point(720, 45);
            this.txtbreakingXytBreaking11.Name = "txtbreakingXytBreaking11";
            this.txtbreakingXytBreaking11.Size = new System.Drawing.Size(83, 23);
            this.txtbreakingXytBreaking11.TabIndex = 204;
            this.txtbreakingXytBreaking11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXyMcr2
            // 
            this.txtbreakingXyMcr2.Location = new System.Drawing.Point(442, 105);
            this.txtbreakingXyMcr2.Name = "txtbreakingXyMcr2";
            this.txtbreakingXyMcr2.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyMcr2.TabIndex = 203;
            this.txtbreakingXyMcr2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXyAlignMark22
            // 
            this.txtbreakingXyAlignMark22.Location = new System.Drawing.Point(442, 75);
            this.txtbreakingXyAlignMark22.Name = "txtbreakingXyAlignMark22";
            this.txtbreakingXyAlignMark22.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyAlignMark22.TabIndex = 202;
            this.txtbreakingXyAlignMark22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXyAlignMark12
            // 
            this.txtbreakingXyAlignMark12.Location = new System.Drawing.Point(442, 45);
            this.txtbreakingXyAlignMark12.Name = "txtbreakingXyAlignMark12";
            this.txtbreakingXyAlignMark12.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyAlignMark12.TabIndex = 201;
            this.txtbreakingXyAlignMark12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXyPinCenter2
            // 
            this.txtbreakingXyPinCenter2.Location = new System.Drawing.Point(442, 15);
            this.txtbreakingXyPinCenter2.Name = "txtbreakingXyPinCenter2";
            this.txtbreakingXyPinCenter2.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyPinCenter2.TabIndex = 200;
            this.txtbreakingXyPinCenter2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(581, 45);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(130, 23);
            this.label45.TabIndex = 185;
            this.label45.Text = "XYT Breaking1[mm]";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingPinDistanceB
            // 
            this.txtbreakingPinDistanceB.Location = new System.Drawing.Point(303, 160);
            this.txtbreakingPinDistanceB.Name = "txtbreakingPinDistanceB";
            this.txtbreakingPinDistanceB.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingPinDistanceB.TabIndex = 199;
            this.txtbreakingPinDistanceB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(142, 162);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(153, 23);
            this.label46.TabIndex = 198;
            this.label46.Text = "Pin Distance B[mm]";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingPinDistanceA
            // 
            this.txtbreakingPinDistanceA.Location = new System.Drawing.Point(303, 131);
            this.txtbreakingPinDistanceA.Name = "txtbreakingPinDistanceA";
            this.txtbreakingPinDistanceA.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingPinDistanceA.TabIndex = 189;
            this.txtbreakingPinDistanceA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(142, 134);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(152, 23);
            this.label47.TabIndex = 186;
            this.label47.Text = "Pin Distance A[mm]";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakingXyMcr1
            // 
            this.txtbreakingXyMcr1.Location = new System.Drawing.Point(303, 102);
            this.txtbreakingXyMcr1.Name = "txtbreakingXyMcr1";
            this.txtbreakingXyMcr1.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyMcr1.TabIndex = 196;
            this.txtbreakingXyMcr1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXyAlignMark21
            // 
            this.txtbreakingXyAlignMark21.Location = new System.Drawing.Point(303, 73);
            this.txtbreakingXyAlignMark21.Name = "txtbreakingXyAlignMark21";
            this.txtbreakingXyAlignMark21.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyAlignMark21.TabIndex = 194;
            this.txtbreakingXyAlignMark21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXyAlignMark11
            // 
            this.txtbreakingXyAlignMark11.Location = new System.Drawing.Point(303, 44);
            this.txtbreakingXyAlignMark11.Name = "txtbreakingXyAlignMark11";
            this.txtbreakingXyAlignMark11.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyAlignMark11.TabIndex = 191;
            this.txtbreakingXyAlignMark11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakingXyPinCenter1
            // 
            this.txtbreakingXyPinCenter1.Location = new System.Drawing.Point(303, 15);
            this.txtbreakingXyPinCenter1.Name = "txtbreakingXyPinCenter1";
            this.txtbreakingXyPinCenter1.Size = new System.Drawing.Size(130, 23);
            this.txtbreakingXyPinCenter1.TabIndex = 187;
            this.txtbreakingXyPinCenter1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lvBreaking
            // 
            this.lvBreaking.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lvBreaking.FullRowSelect = true;
            this.lvBreaking.GridLines = true;
            this.lvBreaking.Location = new System.Drawing.Point(3, 3);
            this.lvBreaking.MultiSelect = false;
            this.lvBreaking.Name = "lvBreaking";
            this.lvBreaking.Size = new System.Drawing.Size(129, 257);
            this.lvBreaking.TabIndex = 183;
            this.lvBreaking.UseCompatibleStateImageBehavior = false;
            this.lvBreaking.View = System.Windows.Forms.View.Details;
            this.lvBreaking.SelectedIndexChanged += new System.EventHandler(this.lvBreaking_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 118;
            // 
            // btnBreakingDelete
            // 
            this.btnBreakingDelete.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingDelete.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingDelete.Location = new System.Drawing.Point(282, 264);
            this.btnBreakingDelete.Name = "btnBreakingDelete";
            this.btnBreakingDelete.Size = new System.Drawing.Size(130, 30);
            this.btnBreakingDelete.TabIndex = 182;
            this.btnBreakingDelete.Text = "Delete";
            this.btnBreakingDelete.UseVisualStyleBackColor = false;
            this.btnBreakingDelete.Click += new System.EventHandler(this.btnBreakingDelete_Click);
            // 
            // btnBreakingCopy
            // 
            this.btnBreakingCopy.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingCopy.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingCopy.Location = new System.Drawing.Point(142, 264);
            this.btnBreakingCopy.Name = "btnBreakingCopy";
            this.btnBreakingCopy.Size = new System.Drawing.Size(130, 30);
            this.btnBreakingCopy.TabIndex = 181;
            this.btnBreakingCopy.Text = "Copy";
            this.btnBreakingCopy.UseVisualStyleBackColor = false;
            this.btnBreakingCopy.Click += new System.EventHandler(this.btnBreakingCopy_Click);
            // 
            // btnBreakingMake
            // 
            this.btnBreakingMake.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingMake.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingMake.Location = new System.Drawing.Point(3, 264);
            this.btnBreakingMake.Name = "btnBreakingMake";
            this.btnBreakingMake.Size = new System.Drawing.Size(129, 30);
            this.btnBreakingMake.TabIndex = 180;
            this.btnBreakingMake.Text = "Make";
            this.btnBreakingMake.UseVisualStyleBackColor = false;
            this.btnBreakingMake.Click += new System.EventHandler(this.btnBreakingMake_Click);
            // 
            // btnBreakingSave
            // 
            this.btnBreakingSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakingSave.ForeColor = System.Drawing.Color.Black;
            this.btnBreakingSave.Location = new System.Drawing.Point(1573, 264);
            this.btnBreakingSave.Name = "btnBreakingSave";
            this.btnBreakingSave.Size = new System.Drawing.Size(152, 30);
            this.btnBreakingSave.TabIndex = 179;
            this.btnBreakingSave.Text = "Save";
            this.btnBreakingSave.UseVisualStyleBackColor = false;
            this.btnBreakingSave.Click += new System.EventHandler(this.btnBreakingSave_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(494, 94);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(152, 23);
            this.textBox1.TabIndex = 183;
            this.textBox1.Text = "0.000";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(176, 92);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(152, 23);
            this.textBox2.TabIndex = 182;
            this.textBox2.Text = "0.000";
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(335, 94);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(152, 23);
            this.label68.TabIndex = 181;
            this.label68.Text = "Left Table2";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label69
            // 
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(17, 92);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(152, 23);
            this.label69.TabIndex = 180;
            this.label69.Text = "Left Table1";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(1130, 95);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(152, 23);
            this.textBox3.TabIndex = 187;
            this.textBox3.Text = "0.000";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(812, 93);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(152, 23);
            this.textBox4.TabIndex = 186;
            this.textBox4.Text = "0.000";
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(971, 95);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(152, 23);
            this.label70.TabIndex = 185;
            this.label70.Text = "Right Table2";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(653, 93);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(152, 23);
            this.label71.TabIndex = 184;
            this.label71.Text = "Right Table1";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlRecipeProCondition
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlRecipeProCondition";
            this.Size = new System.Drawing.Size(1740, 860);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button btnBreakingZigSave;
        private System.Windows.Forms.TextBox txtbreakingZigCamToPin;
        private System.Windows.Forms.TextBox txtbreakingZigThickness;
        private System.Windows.Forms.TextBox txtbreakingZigY;
        private System.Windows.Forms.TextBox txtbreakingZigX;
        private System.Windows.Forms.Label lb_breaking_zig_cam_to_pin;
        private System.Windows.Forms.Label lb_breaking_zig_thickness;
        private System.Windows.Forms.Label lb_breaking_zig_y;
        private System.Windows.Forms.Label lb_breaking_zig_x;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnProcessCopy;
        private System.Windows.Forms.Button btnProcessDelete;
        private System.Windows.Forms.Button btnProcessMake;
        private System.Windows.Forms.TextBox txtprocessAccDec;
        private System.Windows.Forms.TextBox txtprocessScan;
        private System.Windows.Forms.TextBox txtprocessG;
        private System.Windows.Forms.TextBox txtprocessSegmentValue;
        private System.Windows.Forms.TextBox txtprocessOverlap;
        private System.Windows.Forms.TextBox txtprocessZpos;
        private System.Windows.Forms.TextBox txtprocessSpeed;
        private System.Windows.Forms.TextBox txtprocessFrequence;
        private System.Windows.Forms.TextBox txtprocessBurst;
        private System.Windows.Forms.TextBox txtprocessError;
        private System.Windows.Forms.TextBox txtprocessPowerRatio;
        private System.Windows.Forms.TextBox txtprocessPower;
        private System.Windows.Forms.TextBox txtprocessDivider;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListView lvProcess;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnDefaultProcessSave;
        private System.Windows.Forms.TextBox txtdefaultProcessPreAlignErrorY;
        private System.Windows.Forms.TextBox txtdefaultProcessAlignAngle;
        private System.Windows.Forms.TextBox txtdefaultProcessPreAlignErrorX;
        private System.Windows.Forms.TextBox txtdefaultProcessAlignDistance;
        private System.Windows.Forms.TextBox txtdefaultProcessBreakErrorY;
        private System.Windows.Forms.TextBox txtdefaultProcessPreAlignErrorAngle;
        private System.Windows.Forms.TextBox txtdefaultProcessAlignMatch;
        private System.Windows.Forms.TextBox txtdefaultProcessDefaultZ;
        private System.Windows.Forms.TextBox txtdefaultProcessJumpSpeed;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtdefaultLaserCstPitchReverse;
        private System.Windows.Forms.TextBox txtdefaultLaserCstPitchNormal;
        private System.Windows.Forms.TextBox txtdefaultLaserOntime;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btndefaultLaserSave;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtbreakingSlowdownSpeed;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtbreakingFastdownSpeed;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtbreakingSlowdownPitch;
        private System.Windows.Forms.TextBox txtbreakingFastdownPitch;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtbreakingBMcrOffsetXy2;
        private System.Windows.Forms.TextBox txtbreakingAMcrOffsetXy2;
        private System.Windows.Forms.TextBox txtbreakingBMcrOffsetXy1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtbreakingAMcrOffsetXy1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtbreakingB2XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingB2XyOffset1;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtbreakingB1XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingB1XyOffset1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtbreakingA2XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingA2XyOffset1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtbreakingA1XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingA1XyOffset1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking43;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking42;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking41;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking33;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking32;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking31;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking23;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking22;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking21;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking13;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking12;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking11;
        private System.Windows.Forms.TextBox txtbreakingXyMcr2;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark22;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark12;
        private System.Windows.Forms.TextBox txtbreakingXyPinCenter2;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtbreakingPinDistanceB;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtbreakingPinDistanceA;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtbreakingXyMcr1;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark21;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark11;
        private System.Windows.Forms.TextBox txtbreakingXyPinCenter1;
        private System.Windows.Forms.ListView lvBreaking;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button btnBreakingDelete;
        private System.Windows.Forms.Button btnBreakingCopy;
        private System.Windows.Forms.Button btnBreakingMake;
        private System.Windows.Forms.Button btnBreakingSave;
        private System.Windows.Forms.Label panelBreaking2pass;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnBreakingXytBreaking4Calc;
        private System.Windows.Forms.Button btnBreakingXytBreaking3Calc;
        private System.Windows.Forms.Button btnBreakingXytBreaking2Calc;
        private System.Windows.Forms.Button btnBreakingXytBreaking1Calc;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetBY2;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetBY1;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        internal System.Windows.Forms.Label label59;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetAY2;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetAY1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        internal System.Windows.Forms.Label label58;
        internal System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button btnProcessSave;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
    }
}

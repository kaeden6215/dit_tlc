﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class UcrlRecipeSubMenu : UserControl, IUIUpdate
    {
        UcrlRecipeWindows _ucrlRecipeReform = new UcrlRecipeWindows();                  // 레시피-레시피 UI
        UcrlRecipeProCondition _ucrlRecipeProconform = new UcrlRecipeProCondition();  // 레시피-가공조건 UI

        public UcrlRecipeSubMenu()
        {
            InitializeComponent();
            splitContainer1.Panel1.Controls.Add(_ucrlRecipeReform); //맨처음 레시피 화면
        }

        // 서브메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void SubMenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = Color.DarkSlateGray;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        // 레시피 버튼 클릭
        private void btn_sub_recipe_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlRecipeReform);
            _ucrlRecipeReform.Location = new Point(0, 0);
            _ucrlRecipeReform.FillLstRecipe();
            //SubMenuButtonColorChange(sender, e);
        }

        // 가공조건 버튼 클릭
        private void btn_sub_procondition_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(_ucrlRecipeProconform);
            _ucrlRecipeProconform.Location = new Point(0, 0);
            _ucrlRecipeProconform.FillLstProcessRecipe();
            _ucrlRecipeProconform.FillLstBreakRecipe();
            //SubMenuButtonColorChange(sender, e);
        }

        public void UIUpdate()
        {
            
        }
    }
}

﻿namespace DIT.TLC.UI
{
    partial class UcrlRecipeWindows
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtyOffsetBreak = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lb_y_offset_break = new System.Windows.Forms.Label();
            this.txtyOffsetPre = new System.Windows.Forms.TextBox();
            this.lb_y_offset_pre = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnLDSProcess4Move = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtldsProcess42 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess3Move = new System.Windows.Forms.Button();
            this.lb_lds_process1 = new System.Windows.Forms.Label();
            this.txtldsProcess41 = new System.Windows.Forms.TextBox();
            this.txtldsProcess11 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess2Move = new System.Windows.Forms.Button();
            this.lb_lds_process2 = new System.Windows.Forms.Label();
            this.txtldsProcess32 = new System.Windows.Forms.TextBox();
            this.lb_lds_process3 = new System.Windows.Forms.Label();
            this.btnLDSProcess1Move = new System.Windows.Forms.Button();
            this.lb_lds_process4 = new System.Windows.Forms.Label();
            this.txtldsProcess31 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess1Read = new System.Windows.Forms.Button();
            this.btnLDSProcess4Read = new System.Windows.Forms.Button();
            this.txtldsProcess12 = new System.Windows.Forms.TextBox();
            this.txtldsProcess22 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess2Read = new System.Windows.Forms.Button();
            this.btnLDSProcess3Read = new System.Windows.Forms.Button();
            this.txtldsProcess21 = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnLDSBreak4Move = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtldsBreak42 = new System.Windows.Forms.TextBox();
            this.lb_lds_break1 = new System.Windows.Forms.Label();
            this.txtldsBreak41 = new System.Windows.Forms.TextBox();
            this.btnLDSBreak4Read = new System.Windows.Forms.Button();
            this.btnLDSBreak1Move = new System.Windows.Forms.Button();
            this.txtldsBreak11 = new System.Windows.Forms.TextBox();
            this.btnLDSBreak3Move = new System.Windows.Forms.Button();
            this.btnLDSBreak3Read = new System.Windows.Forms.Button();
            this.txtldsBreak32 = new System.Windows.Forms.TextBox();
            this.lb_lds_break2 = new System.Windows.Forms.Label();
            this.txtldsBreak31 = new System.Windows.Forms.TextBox();
            this.lb_lds_break3 = new System.Windows.Forms.Label();
            this.btnLDSBreak1Read = new System.Windows.Forms.Button();
            this.btnLDSBreak2Read = new System.Windows.Forms.Button();
            this.txtldsBreak22 = new System.Windows.Forms.TextBox();
            this.lb_lds_break4 = new System.Windows.Forms.Label();
            this.txtldsBreak21 = new System.Windows.Forms.TextBox();
            this.txtldsBreak12 = new System.Windows.Forms.TextBox();
            this.btnLDSBreak2Move = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.btnPickerNormal2 = new System.Windows.Forms.Button();
            this.btnPickerCW1802 = new System.Windows.Forms.Button();
            this.btnPickerCW902 = new System.Windows.Forms.Button();
            this.btnPickerCCW902 = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnPickerCCW901 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnPickerNormal1 = new System.Windows.Forms.Button();
            this.btnPickerCW1801 = new System.Windows.Forms.Button();
            this.btnPickerCW901 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCassette8 = new System.Windows.Forms.Button();
            this.btnCassette4 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCassette1 = new System.Windows.Forms.Button();
            this.btnCassetteNormal = new System.Windows.Forms.Button();
            this.btnCassette2 = new System.Windows.Forms.Button();
            this.lb_width = new System.Windows.Forms.Label();
            this.txtwidth = new System.Windows.Forms.TextBox();
            this.lb_height = new System.Windows.Forms.Label();
            this.txtcellCount = new System.Windows.Forms.TextBox();
            this.txtheight = new System.Windows.Forms.TextBox();
            this.lb_cell_count = new System.Windows.Forms.Label();
            this.lb_max_height_count = new System.Windows.Forms.Label();
            this.txtmaxHeightCount = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDxfPathLoad = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_ta_thickness = new System.Windows.Forms.Label();
            this.lb_dxfpath = new System.Windows.Forms.Label();
            this.txttThickness = new System.Windows.Forms.TextBox();
            this.txtdxfpath = new System.Windows.Forms.TextBox();
            this.comxtcellType = new System.Windows.Forms.ComboBox();
            this.lb_alignment_layer = new System.Windows.Forms.Label();
            this.lb_cell_type = new System.Windows.Forms.Label();
            this.comxtalignmentLayer = new System.Windows.Forms.ComboBox();
            this.comxtprocessParam = new System.Windows.Forms.ComboBox();
            this.lb_process_layer = new System.Windows.Forms.Label();
            this.lb_process_param = new System.Windows.Forms.Label();
            this.comxtprocessLayer = new System.Windows.Forms.ComboBox();
            this.comxtguideLayer = new System.Windows.Forms.ComboBox();
            this.lb_guide_layer = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnInspPosition = new System.Windows.Forms.Button();
            this.btnCutLineMeasure = new System.Windows.Forms.Button();
            this.btnBreakRight2Move = new System.Windows.Forms.Button();
            this.btnBreakRight2Read = new System.Windows.Forms.Button();
            this.txtrecipe = new System.Windows.Forms.TextBox();
            this.btnBreakRight2Calc = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBreakLeft2Move = new System.Windows.Forms.Button();
            this.btnTable2AlignMark2Move = new System.Windows.Forms.Button();
            this.btnAlign = new System.Windows.Forms.Button();
            this.btnTable1AlignMark2Move = new System.Windows.Forms.Button();
            this.lb_recipe = new System.Windows.Forms.Label();
            this.btnBreakLeft2Read = new System.Windows.Forms.Button();
            this.btnRecipeLoad = new System.Windows.Forms.Button();
            this.btnTable2AlignMark2Read = new System.Windows.Forms.Button();
            this.txtrecipePath = new System.Windows.Forms.TextBox();
            this.btnTable1AlignMark2Read = new System.Windows.Forms.Button();
            this.lb_table1_alignmark1 = new System.Windows.Forms.Label();
            this.btnBreakLeft2Calc = new System.Windows.Forms.Button();
            this.txttable1Alignmark11 = new System.Windows.Forms.TextBox();
            this.btnTable2AlignMark2Calc = new System.Windows.Forms.Button();
            this.txttable1Alignmark12 = new System.Windows.Forms.TextBox();
            this.btnTable1AlignMark2Calc = new System.Windows.Forms.Button();
            this.lb_table2_alignmark1 = new System.Windows.Forms.Label();
            this.txtbreakRight22 = new System.Windows.Forms.TextBox();
            this.txttable2Alignmark11 = new System.Windows.Forms.TextBox();
            this.txtbreakLeft22 = new System.Windows.Forms.TextBox();
            this.txttable2Alignmark12 = new System.Windows.Forms.TextBox();
            this.txtbreakRight21 = new System.Windows.Forms.TextBox();
            this.btnBreakRightYMove = new System.Windows.Forms.Button();
            this.lb_break_right_2 = new System.Windows.Forms.Label();
            this.lb_break_left_1 = new System.Windows.Forms.Label();
            this.txtbreakLeft21 = new System.Windows.Forms.TextBox();
            this.btnBreakRight1Move = new System.Windows.Forms.Button();
            this.lb_break_left_2 = new System.Windows.Forms.Label();
            this.txtbreakLeft11 = new System.Windows.Forms.TextBox();
            this.txttable2Alignmark22 = new System.Windows.Forms.TextBox();
            this.btnBreakLeftYMove = new System.Windows.Forms.Button();
            this.txttable2Alignmark21 = new System.Windows.Forms.TextBox();
            this.txtbreakLeft12 = new System.Windows.Forms.TextBox();
            this.lb_table2_alignmark2 = new System.Windows.Forms.Label();
            this.btnBreakLeft1Move = new System.Windows.Forms.Button();
            this.txttable1Alignmark22 = new System.Windows.Forms.TextBox();
            this.lb_break_right_1 = new System.Windows.Forms.Label();
            this.txttable1Alignmark21 = new System.Windows.Forms.TextBox();
            this.btnTable2Alignmark1Move = new System.Windows.Forms.Button();
            this.lb_table1_alignmark2 = new System.Windows.Forms.Label();
            this.txtbreakRight11 = new System.Windows.Forms.TextBox();
            this.btnTable1Alignmark1Move = new System.Windows.Forms.Button();
            this.txtbreakRight12 = new System.Windows.Forms.TextBox();
            this.btnBreakRightYRead = new System.Windows.Forms.Button();
            this.lb_break_left_y = new System.Windows.Forms.Label();
            this.btnBreakRight1Read = new System.Windows.Forms.Button();
            this.lb_break_right_y = new System.Windows.Forms.Label();
            this.btnBreakLeftYRead = new System.Windows.Forms.Button();
            this.txtbreakLeftY = new System.Windows.Forms.TextBox();
            this.btnBreakLeft1Read = new System.Windows.Forms.Button();
            this.txtbreakRightY = new System.Windows.Forms.TextBox();
            this.btnTable2Alignmark1Read = new System.Windows.Forms.Button();
            this.btnTable1Alignmark1Read = new System.Windows.Forms.Button();
            this.lb_cell_size = new System.Windows.Forms.Label();
            this.btnBreakRightYCalc = new System.Windows.Forms.Button();
            this.txtcellSize1 = new System.Windows.Forms.TextBox();
            this.btnBreakRight1Calc = new System.Windows.Forms.Button();
            this.txtcellSize2 = new System.Windows.Forms.TextBox();
            this.btnBreakLeftYCalc = new System.Windows.Forms.Button();
            this.btnTable1Alignmark1Calc = new System.Windows.Forms.Button();
            this.btnBreakLeft1Calc = new System.Windows.Forms.Button();
            this.btnTable2Alignmark1Calc = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnRecipeCimDelete = new System.Windows.Forms.Button();
            this.btnRecipeCimApply = new System.Windows.Forms.Button();
            this.btnRecipeDelete = new System.Windows.Forms.Button();
            this.btnRecipeApply = new System.Windows.Forms.Button();
            this.btnRecipeSave = new System.Windows.Forms.Button();
            this.btnRecipeCopy = new System.Windows.Forms.Button();
            this.btnRecipeMake = new System.Windows.Forms.Button();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvRecipe = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel19.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.DimGray;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.panel8);
            this.splitContainer1.Panel1.Controls.Add(this.panel6);
            this.splitContainer1.Panel1.Controls.Add(this.panel7);
            this.splitContainer1.Panel1.Controls.Add(this.panel5);
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.panel19);
            this.splitContainer1.Panel1.Controls.Add(this.listView2);
            this.splitContainer1.Panel1.Controls.Add(this.lvRecipe);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(1740, 860);
            this.splitContainer1.SplitterDistance = 835;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.txtyOffsetBreak);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.lb_y_offset_break);
            this.panel8.Controls.Add(this.txtyOffsetPre);
            this.panel8.Controls.Add(this.lb_y_offset_pre);
            this.panel8.Location = new System.Drawing.Point(1393, 697);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(343, 89);
            this.panel8.TabIndex = 460;
            // 
            // txtyOffsetBreak
            // 
            this.txtyOffsetBreak.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.txtyOffsetBreak.Location = new System.Drawing.Point(172, 57);
            this.txtyOffsetBreak.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtyOffsetBreak.Name = "txtyOffsetBreak";
            this.txtyOffsetBreak.Size = new System.Drawing.Size(166, 25);
            this.txtyOffsetBreak.TabIndex = 6;
            this.txtyOffsetBreak.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(341, 24);
            this.label8.TabIndex = 9;
            this.label8.Text = "■ Y Offset";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_y_offset_break
            // 
            this.lb_y_offset_break.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_y_offset_break.Location = new System.Drawing.Point(4, 57);
            this.lb_y_offset_break.Name = "lb_y_offset_break";
            this.lb_y_offset_break.Size = new System.Drawing.Size(166, 25);
            this.lb_y_offset_break.TabIndex = 5;
            this.lb_y_offset_break.Text = "Offset Break[mm]";
            this.lb_y_offset_break.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtyOffsetPre
            // 
            this.txtyOffsetPre.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.txtyOffsetPre.Location = new System.Drawing.Point(172, 28);
            this.txtyOffsetPre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtyOffsetPre.Name = "txtyOffsetPre";
            this.txtyOffsetPre.Size = new System.Drawing.Size(166, 25);
            this.txtyOffsetPre.TabIndex = 4;
            this.txtyOffsetPre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_y_offset_pre
            // 
            this.lb_y_offset_pre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_y_offset_pre.Location = new System.Drawing.Point(4, 28);
            this.lb_y_offset_pre.Name = "lb_y_offset_pre";
            this.lb_y_offset_pre.Size = new System.Drawing.Size(166, 25);
            this.lb_y_offset_pre.TabIndex = 1;
            this.lb_y_offset_pre.Text = "Offset Pre[mm]";
            this.lb_y_offset_pre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnLDSProcess4Move);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.txtldsProcess42);
            this.panel6.Controls.Add(this.btnLDSProcess3Move);
            this.panel6.Controls.Add(this.lb_lds_process1);
            this.panel6.Controls.Add(this.txtldsProcess41);
            this.panel6.Controls.Add(this.txtldsProcess11);
            this.panel6.Controls.Add(this.btnLDSProcess2Move);
            this.panel6.Controls.Add(this.lb_lds_process2);
            this.panel6.Controls.Add(this.txtldsProcess32);
            this.panel6.Controls.Add(this.lb_lds_process3);
            this.panel6.Controls.Add(this.btnLDSProcess1Move);
            this.panel6.Controls.Add(this.lb_lds_process4);
            this.panel6.Controls.Add(this.txtldsProcess31);
            this.panel6.Controls.Add(this.btnLDSProcess1Read);
            this.panel6.Controls.Add(this.btnLDSProcess4Read);
            this.panel6.Controls.Add(this.txtldsProcess12);
            this.panel6.Controls.Add(this.txtldsProcess22);
            this.panel6.Controls.Add(this.btnLDSProcess2Read);
            this.panel6.Controls.Add(this.btnLDSProcess3Read);
            this.panel6.Controls.Add(this.txtldsProcess21);
            this.panel6.Location = new System.Drawing.Point(1392, 383);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(343, 151);
            this.panel6.TabIndex = 462;
            // 
            // btnLDSProcess4Move
            // 
            this.btnLDSProcess4Move.Location = new System.Drawing.Point(289, 117);
            this.btnLDSProcess4Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess4Move.Name = "btnLDSProcess4Move";
            this.btnLDSProcess4Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess4Move.TabIndex = 178;
            this.btnLDSProcess4Move.Text = "이동";
            this.btnLDSProcess4Move.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(341, 24);
            this.label6.TabIndex = 9;
            this.label6.Text = "■ LDS Process";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsProcess42
            // 
            this.txtldsProcess42.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess42.Location = new System.Drawing.Point(171, 117);
            this.txtldsProcess42.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess42.Name = "txtldsProcess42";
            this.txtldsProcess42.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess42.TabIndex = 27;
            this.txtldsProcess42.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnLDSProcess3Move
            // 
            this.btnLDSProcess3Move.Location = new System.Drawing.Point(289, 87);
            this.btnLDSProcess3Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess3Move.Name = "btnLDSProcess3Move";
            this.btnLDSProcess3Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess3Move.TabIndex = 177;
            this.btnLDSProcess3Move.Text = "이동";
            this.btnLDSProcess3Move.UseVisualStyleBackColor = false;
            // 
            // lb_lds_process1
            // 
            this.lb_lds_process1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_process1.Location = new System.Drawing.Point(4, 27);
            this.lb_lds_process1.Name = "lb_lds_process1";
            this.lb_lds_process1.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_process1.TabIndex = 1;
            this.lb_lds_process1.Text = "Process1[mm]";
            this.lb_lds_process1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsProcess41
            // 
            this.txtldsProcess41.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess41.Location = new System.Drawing.Point(99, 117);
            this.txtldsProcess41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess41.Name = "txtldsProcess41";
            this.txtldsProcess41.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess41.TabIndex = 26;
            this.txtldsProcess41.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtldsProcess11
            // 
            this.txtldsProcess11.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess11.Location = new System.Drawing.Point(99, 27);
            this.txtldsProcess11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess11.Name = "txtldsProcess11";
            this.txtldsProcess11.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess11.TabIndex = 4;
            this.txtldsProcess11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnLDSProcess2Move
            // 
            this.btnLDSProcess2Move.Location = new System.Drawing.Point(289, 57);
            this.btnLDSProcess2Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess2Move.Name = "btnLDSProcess2Move";
            this.btnLDSProcess2Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess2Move.TabIndex = 176;
            this.btnLDSProcess2Move.Text = "이동";
            this.btnLDSProcess2Move.UseVisualStyleBackColor = false;
            // 
            // lb_lds_process2
            // 
            this.lb_lds_process2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_process2.Location = new System.Drawing.Point(4, 57);
            this.lb_lds_process2.Name = "lb_lds_process2";
            this.lb_lds_process2.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_process2.TabIndex = 12;
            this.lb_lds_process2.Text = "Process2[mm]";
            this.lb_lds_process2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsProcess32
            // 
            this.txtldsProcess32.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess32.Location = new System.Drawing.Point(171, 87);
            this.txtldsProcess32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess32.Name = "txtldsProcess32";
            this.txtldsProcess32.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess32.TabIndex = 23;
            this.txtldsProcess32.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_lds_process3
            // 
            this.lb_lds_process3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_process3.Location = new System.Drawing.Point(4, 87);
            this.lb_lds_process3.Name = "lb_lds_process3";
            this.lb_lds_process3.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_process3.TabIndex = 13;
            this.lb_lds_process3.Text = "Process3[mm]";
            this.lb_lds_process3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLDSProcess1Move
            // 
            this.btnLDSProcess1Move.Location = new System.Drawing.Point(289, 27);
            this.btnLDSProcess1Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess1Move.Name = "btnLDSProcess1Move";
            this.btnLDSProcess1Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess1Move.TabIndex = 175;
            this.btnLDSProcess1Move.Text = "이동";
            this.btnLDSProcess1Move.UseVisualStyleBackColor = false;
            // 
            // lb_lds_process4
            // 
            this.lb_lds_process4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_process4.Location = new System.Drawing.Point(4, 117);
            this.lb_lds_process4.Name = "lb_lds_process4";
            this.lb_lds_process4.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_process4.TabIndex = 14;
            this.lb_lds_process4.Text = "Process4[mm]";
            this.lb_lds_process4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsProcess31
            // 
            this.txtldsProcess31.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess31.Location = new System.Drawing.Point(99, 87);
            this.txtldsProcess31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess31.Name = "txtldsProcess31";
            this.txtldsProcess31.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess31.TabIndex = 22;
            this.txtldsProcess31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnLDSProcess1Read
            // 
            this.btnLDSProcess1Read.Location = new System.Drawing.Point(242, 27);
            this.btnLDSProcess1Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess1Read.Name = "btnLDSProcess1Read";
            this.btnLDSProcess1Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess1Read.TabIndex = 171;
            this.btnLDSProcess1Read.Text = "읽기";
            this.btnLDSProcess1Read.UseVisualStyleBackColor = false;
            // 
            // btnLDSProcess4Read
            // 
            this.btnLDSProcess4Read.Location = new System.Drawing.Point(241, 117);
            this.btnLDSProcess4Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess4Read.Name = "btnLDSProcess4Read";
            this.btnLDSProcess4Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess4Read.TabIndex = 174;
            this.btnLDSProcess4Read.Text = "읽기";
            this.btnLDSProcess4Read.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess12
            // 
            this.txtldsProcess12.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess12.Location = new System.Drawing.Point(171, 27);
            this.txtldsProcess12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess12.Name = "txtldsProcess12";
            this.txtldsProcess12.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess12.TabIndex = 15;
            this.txtldsProcess12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtldsProcess22
            // 
            this.txtldsProcess22.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess22.Location = new System.Drawing.Point(171, 57);
            this.txtldsProcess22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess22.Name = "txtldsProcess22";
            this.txtldsProcess22.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess22.TabIndex = 19;
            this.txtldsProcess22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnLDSProcess2Read
            // 
            this.btnLDSProcess2Read.Location = new System.Drawing.Point(242, 57);
            this.btnLDSProcess2Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess2Read.Name = "btnLDSProcess2Read";
            this.btnLDSProcess2Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess2Read.TabIndex = 172;
            this.btnLDSProcess2Read.Text = "읽기";
            this.btnLDSProcess2Read.UseVisualStyleBackColor = false;
            // 
            // btnLDSProcess3Read
            // 
            this.btnLDSProcess3Read.Location = new System.Drawing.Point(241, 87);
            this.btnLDSProcess3Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSProcess3Read.Name = "btnLDSProcess3Read";
            this.btnLDSProcess3Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSProcess3Read.TabIndex = 173;
            this.btnLDSProcess3Read.Text = "읽기";
            this.btnLDSProcess3Read.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess21
            // 
            this.txtldsProcess21.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsProcess21.Location = new System.Drawing.Point(99, 57);
            this.txtldsProcess21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsProcess21.Name = "txtldsProcess21";
            this.txtldsProcess21.Size = new System.Drawing.Size(69, 27);
            this.txtldsProcess21.TabIndex = 18;
            this.txtldsProcess21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnLDSBreak4Move);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.txtldsBreak42);
            this.panel7.Controls.Add(this.lb_lds_break1);
            this.panel7.Controls.Add(this.txtldsBreak41);
            this.panel7.Controls.Add(this.btnLDSBreak4Read);
            this.panel7.Controls.Add(this.btnLDSBreak1Move);
            this.panel7.Controls.Add(this.txtldsBreak11);
            this.panel7.Controls.Add(this.btnLDSBreak3Move);
            this.panel7.Controls.Add(this.btnLDSBreak3Read);
            this.panel7.Controls.Add(this.txtldsBreak32);
            this.panel7.Controls.Add(this.lb_lds_break2);
            this.panel7.Controls.Add(this.txtldsBreak31);
            this.panel7.Controls.Add(this.lb_lds_break3);
            this.panel7.Controls.Add(this.btnLDSBreak1Read);
            this.panel7.Controls.Add(this.btnLDSBreak2Read);
            this.panel7.Controls.Add(this.txtldsBreak22);
            this.panel7.Controls.Add(this.lb_lds_break4);
            this.panel7.Controls.Add(this.txtldsBreak21);
            this.panel7.Controls.Add(this.txtldsBreak12);
            this.panel7.Controls.Add(this.btnLDSBreak2Move);
            this.panel7.Location = new System.Drawing.Point(1393, 540);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(343, 151);
            this.panel7.TabIndex = 463;
            // 
            // btnLDSBreak4Move
            // 
            this.btnLDSBreak4Move.Location = new System.Drawing.Point(289, 117);
            this.btnLDSBreak4Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak4Move.Name = "btnLDSBreak4Move";
            this.btnLDSBreak4Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak4Move.TabIndex = 186;
            this.btnLDSBreak4Move.Text = "이동";
            this.btnLDSBreak4Move.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(341, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "■ LDS Break";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsBreak42
            // 
            this.txtldsBreak42.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak42.Location = new System.Drawing.Point(171, 117);
            this.txtldsBreak42.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak42.Name = "txtldsBreak42";
            this.txtldsBreak42.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak42.TabIndex = 47;
            this.txtldsBreak42.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_lds_break1
            // 
            this.lb_lds_break1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_break1.Location = new System.Drawing.Point(4, 27);
            this.lb_lds_break1.Name = "lb_lds_break1";
            this.lb_lds_break1.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_break1.TabIndex = 28;
            this.lb_lds_break1.Text = "Break1[mm]";
            this.lb_lds_break1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsBreak41
            // 
            this.txtldsBreak41.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak41.Location = new System.Drawing.Point(99, 117);
            this.txtldsBreak41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak41.Name = "txtldsBreak41";
            this.txtldsBreak41.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak41.TabIndex = 46;
            this.txtldsBreak41.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnLDSBreak4Read
            // 
            this.btnLDSBreak4Read.Location = new System.Drawing.Point(241, 117);
            this.btnLDSBreak4Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak4Read.Name = "btnLDSBreak4Read";
            this.btnLDSBreak4Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak4Read.TabIndex = 182;
            this.btnLDSBreak4Read.Text = "읽기";
            this.btnLDSBreak4Read.UseVisualStyleBackColor = false;
            // 
            // btnLDSBreak1Move
            // 
            this.btnLDSBreak1Move.Location = new System.Drawing.Point(289, 27);
            this.btnLDSBreak1Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak1Move.Name = "btnLDSBreak1Move";
            this.btnLDSBreak1Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak1Move.TabIndex = 183;
            this.btnLDSBreak1Move.Text = "이동";
            this.btnLDSBreak1Move.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak11
            // 
            this.txtldsBreak11.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak11.Location = new System.Drawing.Point(99, 27);
            this.txtldsBreak11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak11.Name = "txtldsBreak11";
            this.txtldsBreak11.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak11.TabIndex = 30;
            this.txtldsBreak11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnLDSBreak3Move
            // 
            this.btnLDSBreak3Move.Location = new System.Drawing.Point(289, 87);
            this.btnLDSBreak3Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak3Move.Name = "btnLDSBreak3Move";
            this.btnLDSBreak3Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak3Move.TabIndex = 185;
            this.btnLDSBreak3Move.Text = "이동";
            this.btnLDSBreak3Move.UseVisualStyleBackColor = false;
            // 
            // btnLDSBreak3Read
            // 
            this.btnLDSBreak3Read.Location = new System.Drawing.Point(241, 87);
            this.btnLDSBreak3Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak3Read.Name = "btnLDSBreak3Read";
            this.btnLDSBreak3Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak3Read.TabIndex = 181;
            this.btnLDSBreak3Read.Text = "읽기";
            this.btnLDSBreak3Read.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak32
            // 
            this.txtldsBreak32.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak32.Location = new System.Drawing.Point(171, 87);
            this.txtldsBreak32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak32.Name = "txtldsBreak32";
            this.txtldsBreak32.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak32.TabIndex = 43;
            this.txtldsBreak32.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_lds_break2
            // 
            this.lb_lds_break2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_break2.Location = new System.Drawing.Point(4, 57);
            this.lb_lds_break2.Name = "lb_lds_break2";
            this.lb_lds_break2.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_break2.TabIndex = 32;
            this.lb_lds_break2.Text = "Break2[mm]";
            this.lb_lds_break2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsBreak31
            // 
            this.txtldsBreak31.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak31.Location = new System.Drawing.Point(99, 87);
            this.txtldsBreak31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak31.Name = "txtldsBreak31";
            this.txtldsBreak31.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak31.TabIndex = 42;
            this.txtldsBreak31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_lds_break3
            // 
            this.lb_lds_break3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_break3.Location = new System.Drawing.Point(4, 87);
            this.lb_lds_break3.Name = "lb_lds_break3";
            this.lb_lds_break3.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_break3.TabIndex = 33;
            this.lb_lds_break3.Text = "Break3[mm]";
            this.lb_lds_break3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLDSBreak1Read
            // 
            this.btnLDSBreak1Read.Location = new System.Drawing.Point(242, 27);
            this.btnLDSBreak1Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak1Read.Name = "btnLDSBreak1Read";
            this.btnLDSBreak1Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak1Read.TabIndex = 179;
            this.btnLDSBreak1Read.Text = "읽기";
            this.btnLDSBreak1Read.UseVisualStyleBackColor = false;
            // 
            // btnLDSBreak2Read
            // 
            this.btnLDSBreak2Read.Location = new System.Drawing.Point(242, 57);
            this.btnLDSBreak2Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak2Read.Name = "btnLDSBreak2Read";
            this.btnLDSBreak2Read.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak2Read.TabIndex = 180;
            this.btnLDSBreak2Read.Text = "읽기";
            this.btnLDSBreak2Read.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak22
            // 
            this.txtldsBreak22.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak22.Location = new System.Drawing.Point(171, 57);
            this.txtldsBreak22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak22.Name = "txtldsBreak22";
            this.txtldsBreak22.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak22.TabIndex = 39;
            this.txtldsBreak22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_lds_break4
            // 
            this.lb_lds_break4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_lds_break4.Location = new System.Drawing.Point(4, 117);
            this.lb_lds_break4.Name = "lb_lds_break4";
            this.lb_lds_break4.Size = new System.Drawing.Size(88, 27);
            this.lb_lds_break4.TabIndex = 34;
            this.lb_lds_break4.Text = "Break4[mm]";
            this.lb_lds_break4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtldsBreak21
            // 
            this.txtldsBreak21.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak21.Location = new System.Drawing.Point(99, 57);
            this.txtldsBreak21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak21.Name = "txtldsBreak21";
            this.txtldsBreak21.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak21.TabIndex = 38;
            this.txtldsBreak21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtldsBreak12
            // 
            this.txtldsBreak12.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtldsBreak12.Location = new System.Drawing.Point(171, 27);
            this.txtldsBreak12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtldsBreak12.Name = "txtldsBreak12";
            this.txtldsBreak12.Size = new System.Drawing.Size(69, 27);
            this.txtldsBreak12.TabIndex = 35;
            this.txtldsBreak12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnLDSBreak2Move
            // 
            this.btnLDSBreak2Move.Location = new System.Drawing.Point(289, 57);
            this.btnLDSBreak2Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLDSBreak2Move.Name = "btnLDSBreak2Move";
            this.btnLDSBreak2Move.Size = new System.Drawing.Size(49, 27);
            this.btnLDSBreak2Move.TabIndex = 184;
            this.btnLDSBreak2Move.Text = "이동";
            this.btnLDSBreak2Move.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.panel9);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(1392, 178);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(343, 199);
            this.panel5.TabIndex = 461;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.btnPickerNormal2);
            this.panel10.Controls.Add(this.btnPickerCW1802);
            this.panel10.Controls.Add(this.btnPickerCW902);
            this.panel10.Controls.Add(this.btnPickerCCW902);
            this.panel10.Location = new System.Drawing.Point(171, 27);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(165, 167);
            this.panel10.TabIndex = 463;
            // 
            // label10
            // 
            this.label10.AutoEllipsis = true;
            this.label10.BackColor = System.Drawing.Color.Gainsboro;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(163, 24);
            this.label10.TabIndex = 9;
            this.label10.Text = "■ Unloader";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPickerNormal2
            // 
            this.btnPickerNormal2.Location = new System.Drawing.Point(3, 28);
            this.btnPickerNormal2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerNormal2.Name = "btnPickerNormal2";
            this.btnPickerNormal2.Size = new System.Drawing.Size(156, 32);
            this.btnPickerNormal2.TabIndex = 172;
            this.btnPickerNormal2.Text = "Normal";
            this.btnPickerNormal2.UseVisualStyleBackColor = false;
            this.btnPickerNormal2.Click += new System.EventHandler(this.btnULDPikerButton_Click);
            // 
            // btnPickerCW1802
            // 
            this.btnPickerCW1802.Location = new System.Drawing.Point(3, 129);
            this.btnPickerCW1802.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerCW1802.Name = "btnPickerCW1802";
            this.btnPickerCW1802.Size = new System.Drawing.Size(156, 32);
            this.btnPickerCW1802.TabIndex = 178;
            this.btnPickerCW1802.Text = "CW_180";
            this.btnPickerCW1802.UseVisualStyleBackColor = false;
            this.btnPickerCW1802.Click += new System.EventHandler(this.btnULDPikerButton_Click);
            // 
            // btnPickerCW902
            // 
            this.btnPickerCW902.Location = new System.Drawing.Point(3, 61);
            this.btnPickerCW902.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerCW902.Name = "btnPickerCW902";
            this.btnPickerCW902.Size = new System.Drawing.Size(156, 32);
            this.btnPickerCW902.TabIndex = 174;
            this.btnPickerCW902.Text = "CW_90";
            this.btnPickerCW902.UseVisualStyleBackColor = false;
            this.btnPickerCW902.Click += new System.EventHandler(this.btnULDPikerButton_Click);
            // 
            // btnPickerCCW902
            // 
            this.btnPickerCCW902.Location = new System.Drawing.Point(3, 95);
            this.btnPickerCCW902.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerCCW902.Name = "btnPickerCCW902";
            this.btnPickerCCW902.Size = new System.Drawing.Size(156, 32);
            this.btnPickerCCW902.TabIndex = 176;
            this.btnPickerCCW902.Text = "CCW_90";
            this.btnPickerCCW902.UseVisualStyleBackColor = false;
            this.btnPickerCCW902.Click += new System.EventHandler(this.btnULDPikerButton_Click);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnPickerCCW901);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.btnPickerNormal1);
            this.panel9.Controls.Add(this.btnPickerCW1801);
            this.panel9.Controls.Add(this.btnPickerCW901);
            this.panel9.Location = new System.Drawing.Point(3, 27);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(165, 167);
            this.panel9.TabIndex = 462;
            // 
            // btnPickerCCW901
            // 
            this.btnPickerCCW901.Location = new System.Drawing.Point(3, 95);
            this.btnPickerCCW901.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerCCW901.Name = "btnPickerCCW901";
            this.btnPickerCCW901.Size = new System.Drawing.Size(156, 32);
            this.btnPickerCCW901.TabIndex = 175;
            this.btnPickerCCW901.Text = "CCW_90";
            this.btnPickerCCW901.UseVisualStyleBackColor = false;
            this.btnPickerCCW901.Click += new System.EventHandler(this.btnLDPikerButton_Click);
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.BackColor = System.Drawing.Color.Gainsboro;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(163, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "■ Loader";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPickerNormal1
            // 
            this.btnPickerNormal1.Location = new System.Drawing.Point(3, 28);
            this.btnPickerNormal1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerNormal1.Name = "btnPickerNormal1";
            this.btnPickerNormal1.Size = new System.Drawing.Size(156, 32);
            this.btnPickerNormal1.TabIndex = 171;
            this.btnPickerNormal1.Text = "Normal";
            this.btnPickerNormal1.UseVisualStyleBackColor = false;
            this.btnPickerNormal1.Click += new System.EventHandler(this.btnLDPikerButton_Click);
            // 
            // btnPickerCW1801
            // 
            this.btnPickerCW1801.Location = new System.Drawing.Point(3, 129);
            this.btnPickerCW1801.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerCW1801.Name = "btnPickerCW1801";
            this.btnPickerCW1801.Size = new System.Drawing.Size(156, 32);
            this.btnPickerCW1801.TabIndex = 177;
            this.btnPickerCW1801.Text = "CW_180";
            this.btnPickerCW1801.UseVisualStyleBackColor = false;
            this.btnPickerCW1801.Click += new System.EventHandler(this.btnLDPikerButton_Click);
            // 
            // btnPickerCW901
            // 
            this.btnPickerCW901.Location = new System.Drawing.Point(3, 61);
            this.btnPickerCW901.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPickerCW901.Name = "btnPickerCW901";
            this.btnPickerCW901.Size = new System.Drawing.Size(156, 32);
            this.btnPickerCW901.TabIndex = 173;
            this.btnPickerCW901.Text = "CW_90";
            this.btnPickerCW901.UseVisualStyleBackColor = false;
            this.btnPickerCW901.Click += new System.EventHandler(this.btnLDPikerButton_Click);
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(341, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "■ Picker";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnCassette8);
            this.panel4.Controls.Add(this.btnCassette4);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.btnCassette1);
            this.panel4.Controls.Add(this.btnCassetteNormal);
            this.panel4.Controls.Add(this.btnCassette2);
            this.panel4.Controls.Add(this.lb_width);
            this.panel4.Controls.Add(this.txtwidth);
            this.panel4.Controls.Add(this.lb_height);
            this.panel4.Controls.Add(this.txtcellCount);
            this.panel4.Controls.Add(this.txtheight);
            this.panel4.Controls.Add(this.lb_cell_count);
            this.panel4.Controls.Add(this.lb_max_height_count);
            this.panel4.Controls.Add(this.txtmaxHeightCount);
            this.panel4.Location = new System.Drawing.Point(1392, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(343, 171);
            this.panel4.TabIndex = 460;
            // 
            // btnCassette8
            // 
            this.btnCassette8.BackColor = System.Drawing.SystemColors.Control;
            this.btnCassette8.ForeColor = System.Drawing.Color.Black;
            this.btnCassette8.Location = new System.Drawing.Point(117, 132);
            this.btnCassette8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCassette8.Name = "btnCassette8";
            this.btnCassette8.Size = new System.Drawing.Size(110, 34);
            this.btnCassette8.TabIndex = 174;
            this.btnCassette8.Text = "Cassette_8";
            this.btnCassette8.UseVisualStyleBackColor = false;
            this.btnCassette8.Click += new System.EventHandler(this.btnCstTypeButton_Click);
            // 
            // btnCassette4
            // 
            this.btnCassette4.BackColor = System.Drawing.SystemColors.Control;
            this.btnCassette4.ForeColor = System.Drawing.Color.Black;
            this.btnCassette4.Location = new System.Drawing.Point(4, 132);
            this.btnCassette4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCassette4.Name = "btnCassette4";
            this.btnCassette4.Size = new System.Drawing.Size(110, 34);
            this.btnCassette4.TabIndex = 173;
            this.btnCassette4.Text = "Cassette_4";
            this.btnCassette4.UseVisualStyleBackColor = false;
            this.btnCassette4.Click += new System.EventHandler(this.btnCstTypeButton_Click);
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(341, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "■ DxfInfo";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCassette1
            // 
            this.btnCassette1.BackColor = System.Drawing.SystemColors.Control;
            this.btnCassette1.ForeColor = System.Drawing.Color.Black;
            this.btnCassette1.Location = new System.Drawing.Point(4, 96);
            this.btnCassette1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCassette1.Name = "btnCassette1";
            this.btnCassette1.Size = new System.Drawing.Size(110, 34);
            this.btnCassette1.TabIndex = 171;
            this.btnCassette1.Text = "Cassette_1";
            this.btnCassette1.UseVisualStyleBackColor = false;
            this.btnCassette1.Click += new System.EventHandler(this.btnCstTypeButton_Click);
            // 
            // btnCassetteNormal
            // 
            this.btnCassetteNormal.BackColor = System.Drawing.SystemColors.Control;
            this.btnCassetteNormal.ForeColor = System.Drawing.Color.Black;
            this.btnCassetteNormal.Location = new System.Drawing.Point(230, 96);
            this.btnCassetteNormal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCassetteNormal.Name = "btnCassetteNormal";
            this.btnCassetteNormal.Size = new System.Drawing.Size(109, 70);
            this.btnCassetteNormal.TabIndex = 171;
            this.btnCassetteNormal.Text = "NORMAL(40)";
            this.btnCassetteNormal.UseVisualStyleBackColor = false;
            // 
            // btnCassette2
            // 
            this.btnCassette2.BackColor = System.Drawing.SystemColors.Control;
            this.btnCassette2.ForeColor = System.Drawing.Color.Black;
            this.btnCassette2.Location = new System.Drawing.Point(117, 96);
            this.btnCassette2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCassette2.Name = "btnCassette2";
            this.btnCassette2.Size = new System.Drawing.Size(110, 34);
            this.btnCassette2.TabIndex = 172;
            this.btnCassette2.Text = "Cassette_2";
            this.btnCassette2.UseVisualStyleBackColor = false;
            this.btnCassette2.Click += new System.EventHandler(this.btnCstTypeButton_Click);
            // 
            // lb_width
            // 
            this.lb_width.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_width.Location = new System.Drawing.Point(4, 32);
            this.lb_width.Name = "lb_width";
            this.lb_width.Size = new System.Drawing.Size(110, 27);
            this.lb_width.TabIndex = 1;
            this.lb_width.Text = "Width[mm]";
            this.lb_width.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtwidth
            // 
            this.txtwidth.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtwidth.Location = new System.Drawing.Point(116, 32);
            this.txtwidth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtwidth.Name = "txtwidth";
            this.txtwidth.Size = new System.Drawing.Size(52, 27);
            this.txtwidth.TabIndex = 4;
            this.txtwidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_height
            // 
            this.lb_height.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_height.Location = new System.Drawing.Point(4, 64);
            this.lb_height.Name = "lb_height";
            this.lb_height.Size = new System.Drawing.Size(110, 27);
            this.lb_height.TabIndex = 5;
            this.lb_height.Text = "Height[mm]";
            this.lb_height.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtcellCount
            // 
            this.txtcellCount.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtcellCount.Location = new System.Drawing.Point(286, 64);
            this.txtcellCount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtcellCount.Name = "txtcellCount";
            this.txtcellCount.Size = new System.Drawing.Size(52, 27);
            this.txtcellCount.TabIndex = 11;
            this.txtcellCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtheight
            // 
            this.txtheight.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtheight.Location = new System.Drawing.Point(116, 64);
            this.txtheight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtheight.Name = "txtheight";
            this.txtheight.Size = new System.Drawing.Size(52, 27);
            this.txtheight.TabIndex = 6;
            this.txtheight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_cell_count
            // 
            this.lb_cell_count.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_cell_count.Location = new System.Drawing.Point(174, 64);
            this.lb_cell_count.Name = "lb_cell_count";
            this.lb_cell_count.Size = new System.Drawing.Size(110, 27);
            this.lb_cell_count.TabIndex = 10;
            this.lb_cell_count.Text = "Cell Count";
            this.lb_cell_count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_max_height_count
            // 
            this.lb_max_height_count.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_max_height_count.Location = new System.Drawing.Point(174, 32);
            this.lb_max_height_count.Name = "lb_max_height_count";
            this.lb_max_height_count.Size = new System.Drawing.Size(110, 27);
            this.lb_max_height_count.TabIndex = 7;
            this.lb_max_height_count.Text = "Max Height Count";
            this.lb_max_height_count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtmaxHeightCount
            // 
            this.txtmaxHeightCount.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtmaxHeightCount.Location = new System.Drawing.Point(286, 32);
            this.txtmaxHeightCount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtmaxHeightCount.Name = "txtmaxHeightCount";
            this.txtmaxHeightCount.Size = new System.Drawing.Size(52, 27);
            this.txtmaxHeightCount.TabIndex = 9;
            this.txtmaxHeightCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(285, 540);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1104, 246);
            this.panel3.TabIndex = 459;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1102, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "■ DxfInfo";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnDxfPathLoad);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lb_ta_thickness);
            this.panel1.Controls.Add(this.lb_dxfpath);
            this.panel1.Controls.Add(this.txttThickness);
            this.panel1.Controls.Add(this.txtdxfpath);
            this.panel1.Controls.Add(this.comxtcellType);
            this.panel1.Controls.Add(this.lb_alignment_layer);
            this.panel1.Controls.Add(this.lb_cell_type);
            this.panel1.Controls.Add(this.comxtalignmentLayer);
            this.panel1.Controls.Add(this.comxtprocessParam);
            this.panel1.Controls.Add(this.lb_process_layer);
            this.panel1.Controls.Add(this.lb_process_param);
            this.panel1.Controls.Add(this.comxtprocessLayer);
            this.panel1.Controls.Add(this.comxtguideLayer);
            this.panel1.Controls.Add(this.lb_guide_layer);
            this.panel1.Location = new System.Drawing.Point(285, 383);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1104, 151);
            this.panel1.TabIndex = 458;
            // 
            // btnDxfPathLoad
            // 
            this.btnDxfPathLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnDxfPathLoad.ForeColor = System.Drawing.Color.Black;
            this.btnDxfPathLoad.Location = new System.Drawing.Point(943, 26);
            this.btnDxfPathLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDxfPathLoad.Name = "btnDxfPathLoad";
            this.btnDxfPathLoad.Size = new System.Drawing.Size(154, 27);
            this.btnDxfPathLoad.TabIndex = 168;
            this.btnDxfPathLoad.Text = "불러오기";
            this.btnDxfPathLoad.UseVisualStyleBackColor = false;
            this.btnDxfPathLoad.Click += new System.EventHandler(this.btnDxfPathLoad_Click);
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1102, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ LayerInfo";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_ta_thickness
            // 
            this.lb_ta_thickness.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_ta_thickness.Location = new System.Drawing.Point(16, 119);
            this.lb_ta_thickness.Name = "lb_ta_thickness";
            this.lb_ta_thickness.Size = new System.Drawing.Size(147, 27);
            this.lb_ta_thickness.TabIndex = 75;
            this.lb_ta_thickness.Text = "TA/Thickness[mm]";
            this.lb_ta_thickness.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_dxfpath
            // 
            this.lb_dxfpath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_dxfpath.Location = new System.Drawing.Point(16, 26);
            this.lb_dxfpath.Name = "lb_dxfpath";
            this.lb_dxfpath.Size = new System.Drawing.Size(147, 27);
            this.lb_dxfpath.TabIndex = 4;
            this.lb_dxfpath.Text = "DxfPath";
            this.lb_dxfpath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txttThickness
            // 
            this.txttThickness.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttThickness.Location = new System.Drawing.Point(170, 119);
            this.txttThickness.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttThickness.Name = "txttThickness";
            this.txttThickness.Size = new System.Drawing.Size(105, 27);
            this.txttThickness.TabIndex = 65;
            this.txttThickness.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtdxfpath
            // 
            this.txtdxfpath.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtdxfpath.Location = new System.Drawing.Point(170, 26);
            this.txtdxfpath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtdxfpath.Name = "txtdxfpath";
            this.txtdxfpath.ReadOnly = true;
            this.txtdxfpath.Size = new System.Drawing.Size(767, 27);
            this.txtdxfpath.TabIndex = 5;
            // 
            // comxtcellType
            // 
            this.comxtcellType.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.comxtcellType.FormattingEnabled = true;
            this.comxtcellType.Location = new System.Drawing.Point(439, 87);
            this.comxtcellType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comxtcellType.Name = "comxtcellType";
            this.comxtcellType.Size = new System.Drawing.Size(105, 28);
            this.comxtcellType.TabIndex = 74;
            // 
            // lb_alignment_layer
            // 
            this.lb_alignment_layer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_alignment_layer.Location = new System.Drawing.Point(16, 57);
            this.lb_alignment_layer.Name = "lb_alignment_layer";
            this.lb_alignment_layer.Size = new System.Drawing.Size(147, 27);
            this.lb_alignment_layer.TabIndex = 65;
            this.lb_alignment_layer.Text = "AlignMent Layer";
            this.lb_alignment_layer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_cell_type
            // 
            this.lb_cell_type.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_cell_type.Location = new System.Drawing.Point(290, 88);
            this.lb_cell_type.Name = "lb_cell_type";
            this.lb_cell_type.Size = new System.Drawing.Size(147, 27);
            this.lb_cell_type.TabIndex = 73;
            this.lb_cell_type.Text = "Cell Type";
            this.lb_cell_type.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comxtalignmentLayer
            // 
            this.comxtalignmentLayer.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.comxtalignmentLayer.FormattingEnabled = true;
            this.comxtalignmentLayer.Items.AddRange(new object[] {
            "align"});
            this.comxtalignmentLayer.Location = new System.Drawing.Point(170, 56);
            this.comxtalignmentLayer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comxtalignmentLayer.Name = "comxtalignmentLayer";
            this.comxtalignmentLayer.Size = new System.Drawing.Size(105, 28);
            this.comxtalignmentLayer.TabIndex = 66;
            // 
            // comxtprocessParam
            // 
            this.comxtprocessParam.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.comxtprocessParam.FormattingEnabled = true;
            this.comxtprocessParam.Location = new System.Drawing.Point(170, 87);
            this.comxtprocessParam.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comxtprocessParam.Name = "comxtprocessParam";
            this.comxtprocessParam.Size = new System.Drawing.Size(105, 28);
            this.comxtprocessParam.TabIndex = 72;
            // 
            // lb_process_layer
            // 
            this.lb_process_layer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_process_layer.Location = new System.Drawing.Point(290, 56);
            this.lb_process_layer.Name = "lb_process_layer";
            this.lb_process_layer.Size = new System.Drawing.Size(147, 27);
            this.lb_process_layer.TabIndex = 67;
            this.lb_process_layer.Text = "Process Layer";
            this.lb_process_layer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_process_param
            // 
            this.lb_process_param.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_process_param.Location = new System.Drawing.Point(16, 88);
            this.lb_process_param.Name = "lb_process_param";
            this.lb_process_param.Size = new System.Drawing.Size(147, 27);
            this.lb_process_param.TabIndex = 71;
            this.lb_process_param.Text = "Process Param";
            this.lb_process_param.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comxtprocessLayer
            // 
            this.comxtprocessLayer.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.comxtprocessLayer.FormattingEnabled = true;
            this.comxtprocessLayer.Items.AddRange(new object[] {
            "CUT"});
            this.comxtprocessLayer.Location = new System.Drawing.Point(439, 56);
            this.comxtprocessLayer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comxtprocessLayer.Name = "comxtprocessLayer";
            this.comxtprocessLayer.Size = new System.Drawing.Size(105, 28);
            this.comxtprocessLayer.TabIndex = 68;
            // 
            // comxtguideLayer
            // 
            this.comxtguideLayer.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.comxtguideLayer.FormattingEnabled = true;
            this.comxtguideLayer.Items.AddRange(new object[] {
            "guide"});
            this.comxtguideLayer.Location = new System.Drawing.Point(723, 56);
            this.comxtguideLayer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comxtguideLayer.Name = "comxtguideLayer";
            this.comxtguideLayer.Size = new System.Drawing.Size(105, 28);
            this.comxtguideLayer.TabIndex = 70;
            // 
            // lb_guide_layer
            // 
            this.lb_guide_layer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_guide_layer.Location = new System.Drawing.Point(569, 57);
            this.lb_guide_layer.Name = "lb_guide_layer";
            this.lb_guide_layer.Size = new System.Drawing.Size(147, 27);
            this.lb_guide_layer.TabIndex = 69;
            this.lb_guide_layer.Text = "Guide Layer";
            this.lb_guide_layer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnInspPosition);
            this.panel2.Controls.Add(this.btnCutLineMeasure);
            this.panel2.Controls.Add(this.btnBreakRight2Move);
            this.panel2.Controls.Add(this.btnBreakRight2Read);
            this.panel2.Controls.Add(this.txtrecipe);
            this.panel2.Controls.Add(this.btnBreakRight2Calc);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnBreakLeft2Move);
            this.panel2.Controls.Add(this.btnTable2AlignMark2Move);
            this.panel2.Controls.Add(this.btnAlign);
            this.panel2.Controls.Add(this.btnTable1AlignMark2Move);
            this.panel2.Controls.Add(this.lb_recipe);
            this.panel2.Controls.Add(this.btnBreakLeft2Read);
            this.panel2.Controls.Add(this.btnRecipeLoad);
            this.panel2.Controls.Add(this.btnTable2AlignMark2Read);
            this.panel2.Controls.Add(this.txtrecipePath);
            this.panel2.Controls.Add(this.btnTable1AlignMark2Read);
            this.panel2.Controls.Add(this.lb_table1_alignmark1);
            this.panel2.Controls.Add(this.btnBreakLeft2Calc);
            this.panel2.Controls.Add(this.txttable1Alignmark11);
            this.panel2.Controls.Add(this.btnTable2AlignMark2Calc);
            this.panel2.Controls.Add(this.txttable1Alignmark12);
            this.panel2.Controls.Add(this.btnTable1AlignMark2Calc);
            this.panel2.Controls.Add(this.lb_table2_alignmark1);
            this.panel2.Controls.Add(this.txtbreakRight22);
            this.panel2.Controls.Add(this.txttable2Alignmark11);
            this.panel2.Controls.Add(this.txtbreakLeft22);
            this.panel2.Controls.Add(this.txttable2Alignmark12);
            this.panel2.Controls.Add(this.txtbreakRight21);
            this.panel2.Controls.Add(this.btnBreakRightYMove);
            this.panel2.Controls.Add(this.lb_break_right_2);
            this.panel2.Controls.Add(this.lb_break_left_1);
            this.panel2.Controls.Add(this.txtbreakLeft21);
            this.panel2.Controls.Add(this.btnBreakRight1Move);
            this.panel2.Controls.Add(this.lb_break_left_2);
            this.panel2.Controls.Add(this.txtbreakLeft11);
            this.panel2.Controls.Add(this.txttable2Alignmark22);
            this.panel2.Controls.Add(this.btnBreakLeftYMove);
            this.panel2.Controls.Add(this.txttable2Alignmark21);
            this.panel2.Controls.Add(this.txtbreakLeft12);
            this.panel2.Controls.Add(this.lb_table2_alignmark2);
            this.panel2.Controls.Add(this.btnBreakLeft1Move);
            this.panel2.Controls.Add(this.txttable1Alignmark22);
            this.panel2.Controls.Add(this.lb_break_right_1);
            this.panel2.Controls.Add(this.txttable1Alignmark21);
            this.panel2.Controls.Add(this.btnTable2Alignmark1Move);
            this.panel2.Controls.Add(this.lb_table1_alignmark2);
            this.panel2.Controls.Add(this.txtbreakRight11);
            this.panel2.Controls.Add(this.btnTable1Alignmark1Move);
            this.panel2.Controls.Add(this.txtbreakRight12);
            this.panel2.Controls.Add(this.btnBreakRightYRead);
            this.panel2.Controls.Add(this.lb_break_left_y);
            this.panel2.Controls.Add(this.btnBreakRight1Read);
            this.panel2.Controls.Add(this.lb_break_right_y);
            this.panel2.Controls.Add(this.btnBreakLeftYRead);
            this.panel2.Controls.Add(this.txtbreakLeftY);
            this.panel2.Controls.Add(this.btnBreakLeft1Read);
            this.panel2.Controls.Add(this.txtbreakRightY);
            this.panel2.Controls.Add(this.btnTable2Alignmark1Read);
            this.panel2.Controls.Add(this.btnTable1Alignmark1Read);
            this.panel2.Controls.Add(this.lb_cell_size);
            this.panel2.Controls.Add(this.btnBreakRightYCalc);
            this.panel2.Controls.Add(this.txtcellSize1);
            this.panel2.Controls.Add(this.btnBreakRight1Calc);
            this.panel2.Controls.Add(this.txtcellSize2);
            this.panel2.Controls.Add(this.btnBreakLeftYCalc);
            this.panel2.Controls.Add(this.btnTable1Alignmark1Calc);
            this.panel2.Controls.Add(this.btnBreakLeft1Calc);
            this.panel2.Controls.Add(this.btnTable2Alignmark1Calc);
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(285, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1104, 373);
            this.panel2.TabIndex = 457;
            // 
            // btnInspPosition
            // 
            this.btnInspPosition.BackColor = System.Drawing.SystemColors.Control;
            this.btnInspPosition.ForeColor = System.Drawing.Color.Black;
            this.btnInspPosition.Location = new System.Drawing.Point(680, 329);
            this.btnInspPosition.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnInspPosition.Name = "btnInspPosition";
            this.btnInspPosition.Size = new System.Drawing.Size(105, 27);
            this.btnInspPosition.TabIndex = 174;
            this.btnInspPosition.Text = "검사포지션";
            this.btnInspPosition.UseVisualStyleBackColor = false;
            this.btnInspPosition.Click += new System.EventHandler(this.btnInspPosition_Click);
            // 
            // btnCutLineMeasure
            // 
            this.btnCutLineMeasure.BackColor = System.Drawing.SystemColors.Control;
            this.btnCutLineMeasure.ForeColor = System.Drawing.Color.Black;
            this.btnCutLineMeasure.Location = new System.Drawing.Point(569, 329);
            this.btnCutLineMeasure.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCutLineMeasure.Name = "btnCutLineMeasure";
            this.btnCutLineMeasure.Size = new System.Drawing.Size(105, 27);
            this.btnCutLineMeasure.TabIndex = 173;
            this.btnCutLineMeasure.Text = "컷라인계측";
            this.btnCutLineMeasure.UseVisualStyleBackColor = false;
            this.btnCutLineMeasure.Click += new System.EventHandler(this.btnCutLineMeasure_Click);
            // 
            // btnBreakRight2Move
            // 
            this.btnBreakRight2Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRight2Move.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRight2Move.Location = new System.Drawing.Point(995, 259);
            this.btnBreakRight2Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRight2Move.Name = "btnBreakRight2Move";
            this.btnBreakRight2Move.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRight2Move.TabIndex = 168;
            this.btnBreakRight2Move.Text = "이동";
            this.btnBreakRight2Move.UseVisualStyleBackColor = false;
            // 
            // btnBreakRight2Read
            // 
            this.btnBreakRight2Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRight2Read.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRight2Read.Location = new System.Drawing.Point(943, 259);
            this.btnBreakRight2Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRight2Read.Name = "btnBreakRight2Read";
            this.btnBreakRight2Read.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRight2Read.TabIndex = 167;
            this.btnBreakRight2Read.Text = "읽기";
            this.btnBreakRight2Read.UseVisualStyleBackColor = false;
            // 
            // txtrecipe
            // 
            this.txtrecipe.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.txtrecipe.Location = new System.Drawing.Point(170, 34);
            this.txtrecipe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtrecipe.Name = "txtrecipe";
            this.txtrecipe.ReadOnly = true;
            this.txtrecipe.Size = new System.Drawing.Size(767, 25);
            this.txtrecipe.TabIndex = 172;
            // 
            // btnBreakRight2Calc
            // 
            this.btnBreakRight2Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRight2Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRight2Calc.Location = new System.Drawing.Point(1047, 259);
            this.btnBreakRight2Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRight2Calc.Name = "btnBreakRight2Calc";
            this.btnBreakRight2Calc.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRight2Calc.TabIndex = 166;
            this.btnBreakRight2Calc.Text = "CALC";
            this.btnBreakRight2Calc.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1102, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "■ Item";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakLeft2Move
            // 
            this.btnBreakLeft2Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeft2Move.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeft2Move.Location = new System.Drawing.Point(995, 189);
            this.btnBreakLeft2Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeft2Move.Name = "btnBreakLeft2Move";
            this.btnBreakLeft2Move.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeft2Move.TabIndex = 165;
            this.btnBreakLeft2Move.Text = "이동";
            this.btnBreakLeft2Move.UseVisualStyleBackColor = false;
            // 
            // btnTable2AlignMark2Move
            // 
            this.btnTable2AlignMark2Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable2AlignMark2Move.ForeColor = System.Drawing.Color.Black;
            this.btnTable2AlignMark2Move.Location = new System.Drawing.Point(995, 154);
            this.btnTable2AlignMark2Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable2AlignMark2Move.Name = "btnTable2AlignMark2Move";
            this.btnTable2AlignMark2Move.Size = new System.Drawing.Size(50, 27);
            this.btnTable2AlignMark2Move.TabIndex = 164;
            this.btnTable2AlignMark2Move.Text = "이동";
            this.btnTable2AlignMark2Move.UseVisualStyleBackColor = false;
            // 
            // btnAlign
            // 
            this.btnAlign.BackColor = System.Drawing.SystemColors.Control;
            this.btnAlign.ForeColor = System.Drawing.Color.Black;
            this.btnAlign.Location = new System.Drawing.Point(832, 63);
            this.btnAlign.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAlign.Name = "btnAlign";
            this.btnAlign.Size = new System.Drawing.Size(105, 30);
            this.btnAlign.TabIndex = 170;
            this.btnAlign.Text = "Align";
            this.btnAlign.UseVisualStyleBackColor = false;
            // 
            // btnTable1AlignMark2Move
            // 
            this.btnTable1AlignMark2Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable1AlignMark2Move.ForeColor = System.Drawing.Color.Black;
            this.btnTable1AlignMark2Move.Location = new System.Drawing.Point(995, 119);
            this.btnTable1AlignMark2Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable1AlignMark2Move.Name = "btnTable1AlignMark2Move";
            this.btnTable1AlignMark2Move.Size = new System.Drawing.Size(50, 27);
            this.btnTable1AlignMark2Move.TabIndex = 163;
            this.btnTable1AlignMark2Move.Text = "이동";
            this.btnTable1AlignMark2Move.UseVisualStyleBackColor = false;
            // 
            // lb_recipe
            // 
            this.lb_recipe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_recipe.Location = new System.Drawing.Point(16, 35);
            this.lb_recipe.Name = "lb_recipe";
            this.lb_recipe.Size = new System.Drawing.Size(147, 25);
            this.lb_recipe.TabIndex = 2;
            this.lb_recipe.Text = "Recipe";
            this.lb_recipe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakLeft2Read
            // 
            this.btnBreakLeft2Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeft2Read.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeft2Read.Location = new System.Drawing.Point(943, 189);
            this.btnBreakLeft2Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeft2Read.Name = "btnBreakLeft2Read";
            this.btnBreakLeft2Read.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeft2Read.TabIndex = 162;
            this.btnBreakLeft2Read.Text = "읽기";
            this.btnBreakLeft2Read.UseVisualStyleBackColor = false;
            // 
            // btnRecipeLoad
            // 
            this.btnRecipeLoad.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeLoad.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeLoad.Location = new System.Drawing.Point(723, 63);
            this.btnRecipeLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeLoad.Name = "btnRecipeLoad";
            this.btnRecipeLoad.Size = new System.Drawing.Size(105, 30);
            this.btnRecipeLoad.TabIndex = 169;
            this.btnRecipeLoad.Text = "불러오기";
            this.btnRecipeLoad.UseVisualStyleBackColor = false;
            this.btnRecipeLoad.Click += new System.EventHandler(this.btnRecipeLoad_Click);
            // 
            // btnTable2AlignMark2Read
            // 
            this.btnTable2AlignMark2Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable2AlignMark2Read.ForeColor = System.Drawing.Color.Black;
            this.btnTable2AlignMark2Read.Location = new System.Drawing.Point(943, 154);
            this.btnTable2AlignMark2Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable2AlignMark2Read.Name = "btnTable2AlignMark2Read";
            this.btnTable2AlignMark2Read.Size = new System.Drawing.Size(50, 27);
            this.btnTable2AlignMark2Read.TabIndex = 161;
            this.btnTable2AlignMark2Read.Text = "읽기";
            this.btnTable2AlignMark2Read.UseVisualStyleBackColor = false;
            // 
            // txtrecipePath
            // 
            this.txtrecipePath.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.txtrecipePath.Location = new System.Drawing.Point(170, 66);
            this.txtrecipePath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtrecipePath.Name = "txtrecipePath";
            this.txtrecipePath.ReadOnly = true;
            this.txtrecipePath.Size = new System.Drawing.Size(546, 25);
            this.txtrecipePath.TabIndex = 4;
            // 
            // btnTable1AlignMark2Read
            // 
            this.btnTable1AlignMark2Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable1AlignMark2Read.ForeColor = System.Drawing.Color.Black;
            this.btnTable1AlignMark2Read.Location = new System.Drawing.Point(943, 119);
            this.btnTable1AlignMark2Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable1AlignMark2Read.Name = "btnTable1AlignMark2Read";
            this.btnTable1AlignMark2Read.Size = new System.Drawing.Size(50, 27);
            this.btnTable1AlignMark2Read.TabIndex = 160;
            this.btnTable1AlignMark2Read.Text = "읽기";
            this.btnTable1AlignMark2Read.UseVisualStyleBackColor = false;
            // 
            // lb_table1_alignmark1
            // 
            this.lb_table1_alignmark1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_table1_alignmark1.Location = new System.Drawing.Point(16, 119);
            this.lb_table1_alignmark1.Name = "lb_table1_alignmark1";
            this.lb_table1_alignmark1.Size = new System.Drawing.Size(147, 27);
            this.lb_table1_alignmark1.TabIndex = 20;
            this.lb_table1_alignmark1.Text = "Table1AlignMark 1[mm]";
            this.lb_table1_alignmark1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakLeft2Calc
            // 
            this.btnBreakLeft2Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeft2Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeft2Calc.Location = new System.Drawing.Point(1047, 189);
            this.btnBreakLeft2Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeft2Calc.Name = "btnBreakLeft2Calc";
            this.btnBreakLeft2Calc.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeft2Calc.TabIndex = 159;
            this.btnBreakLeft2Calc.Text = "CALC";
            this.btnBreakLeft2Calc.UseVisualStyleBackColor = false;
            // 
            // txttable1Alignmark11
            // 
            this.txttable1Alignmark11.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable1Alignmark11.Location = new System.Drawing.Point(170, 119);
            this.txttable1Alignmark11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable1Alignmark11.Name = "txttable1Alignmark11";
            this.txttable1Alignmark11.Size = new System.Drawing.Size(105, 27);
            this.txttable1Alignmark11.TabIndex = 21;
            this.txttable1Alignmark11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnTable2AlignMark2Calc
            // 
            this.btnTable2AlignMark2Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable2AlignMark2Calc.ForeColor = System.Drawing.Color.Black;
            this.btnTable2AlignMark2Calc.Location = new System.Drawing.Point(1047, 154);
            this.btnTable2AlignMark2Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable2AlignMark2Calc.Name = "btnTable2AlignMark2Calc";
            this.btnTable2AlignMark2Calc.Size = new System.Drawing.Size(50, 27);
            this.btnTable2AlignMark2Calc.TabIndex = 158;
            this.btnTable2AlignMark2Calc.Text = "CALC";
            this.btnTable2AlignMark2Calc.UseVisualStyleBackColor = false;
            // 
            // txttable1Alignmark12
            // 
            this.txttable1Alignmark12.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable1Alignmark12.Location = new System.Drawing.Point(279, 119);
            this.txttable1Alignmark12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable1Alignmark12.Name = "txttable1Alignmark12";
            this.txttable1Alignmark12.Size = new System.Drawing.Size(105, 27);
            this.txttable1Alignmark12.TabIndex = 22;
            this.txttable1Alignmark12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnTable1AlignMark2Calc
            // 
            this.btnTable1AlignMark2Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable1AlignMark2Calc.ForeColor = System.Drawing.Color.Black;
            this.btnTable1AlignMark2Calc.Location = new System.Drawing.Point(1047, 119);
            this.btnTable1AlignMark2Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable1AlignMark2Calc.Name = "btnTable1AlignMark2Calc";
            this.btnTable1AlignMark2Calc.Size = new System.Drawing.Size(50, 27);
            this.btnTable1AlignMark2Calc.TabIndex = 157;
            this.btnTable1AlignMark2Calc.Text = "CALC";
            this.btnTable1AlignMark2Calc.UseVisualStyleBackColor = false;
            // 
            // lb_table2_alignmark1
            // 
            this.lb_table2_alignmark1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_table2_alignmark1.Location = new System.Drawing.Point(16, 154);
            this.lb_table2_alignmark1.Name = "lb_table2_alignmark1";
            this.lb_table2_alignmark1.Size = new System.Drawing.Size(147, 27);
            this.lb_table2_alignmark1.TabIndex = 26;
            this.lb_table2_alignmark1.Text = "Table2AlignMark 1[mm]";
            this.lb_table2_alignmark1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakRight22
            // 
            this.txtbreakRight22.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakRight22.Location = new System.Drawing.Point(832, 259);
            this.txtbreakRight22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakRight22.Name = "txtbreakRight22";
            this.txtbreakRight22.Size = new System.Drawing.Size(105, 27);
            this.txtbreakRight22.TabIndex = 54;
            this.txtbreakRight22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txttable2Alignmark11
            // 
            this.txttable2Alignmark11.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable2Alignmark11.Location = new System.Drawing.Point(170, 154);
            this.txttable2Alignmark11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable2Alignmark11.Name = "txttable2Alignmark11";
            this.txttable2Alignmark11.Size = new System.Drawing.Size(105, 27);
            this.txttable2Alignmark11.TabIndex = 27;
            this.txttable2Alignmark11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakLeft22
            // 
            this.txtbreakLeft22.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakLeft22.Location = new System.Drawing.Point(832, 189);
            this.txtbreakLeft22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakLeft22.Name = "txtbreakLeft22";
            this.txtbreakLeft22.Size = new System.Drawing.Size(105, 27);
            this.txtbreakLeft22.TabIndex = 40;
            this.txtbreakLeft22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txttable2Alignmark12
            // 
            this.txttable2Alignmark12.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable2Alignmark12.Location = new System.Drawing.Point(279, 154);
            this.txttable2Alignmark12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable2Alignmark12.Name = "txttable2Alignmark12";
            this.txttable2Alignmark12.Size = new System.Drawing.Size(105, 27);
            this.txttable2Alignmark12.TabIndex = 28;
            this.txttable2Alignmark12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakRight21
            // 
            this.txtbreakRight21.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakRight21.Location = new System.Drawing.Point(723, 259);
            this.txtbreakRight21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakRight21.Name = "txtbreakRight21";
            this.txtbreakRight21.Size = new System.Drawing.Size(105, 27);
            this.txtbreakRight21.TabIndex = 52;
            this.txtbreakRight21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnBreakRightYMove
            // 
            this.btnBreakRightYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRightYMove.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRightYMove.Location = new System.Drawing.Point(442, 294);
            this.btnBreakRightYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRightYMove.Name = "btnBreakRightYMove";
            this.btnBreakRightYMove.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRightYMove.TabIndex = 156;
            this.btnBreakRightYMove.Text = "이동";
            this.btnBreakRightYMove.UseVisualStyleBackColor = false;
            // 
            // lb_break_right_2
            // 
            this.lb_break_right_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_break_right_2.Location = new System.Drawing.Point(569, 259);
            this.lb_break_right_2.Name = "lb_break_right_2";
            this.lb_break_right_2.Size = new System.Drawing.Size(147, 27);
            this.lb_break_right_2.TabIndex = 50;
            this.lb_break_right_2.Text = "Break_right_2[mm]";
            this.lb_break_right_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_break_left_1
            // 
            this.lb_break_left_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_break_left_1.Location = new System.Drawing.Point(16, 189);
            this.lb_break_left_1.Name = "lb_break_left_1";
            this.lb_break_left_1.Size = new System.Drawing.Size(147, 27);
            this.lb_break_left_1.TabIndex = 32;
            this.lb_break_left_1.Text = "Break_left_1[mm]";
            this.lb_break_left_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakLeft21
            // 
            this.txtbreakLeft21.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakLeft21.Location = new System.Drawing.Point(723, 189);
            this.txtbreakLeft21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakLeft21.Name = "txtbreakLeft21";
            this.txtbreakLeft21.Size = new System.Drawing.Size(105, 27);
            this.txtbreakLeft21.TabIndex = 39;
            this.txtbreakLeft21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnBreakRight1Move
            // 
            this.btnBreakRight1Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRight1Move.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRight1Move.Location = new System.Drawing.Point(442, 259);
            this.btnBreakRight1Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRight1Move.Name = "btnBreakRight1Move";
            this.btnBreakRight1Move.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRight1Move.TabIndex = 155;
            this.btnBreakRight1Move.Text = "이동";
            this.btnBreakRight1Move.UseVisualStyleBackColor = false;
            // 
            // lb_break_left_2
            // 
            this.lb_break_left_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_break_left_2.Location = new System.Drawing.Point(569, 189);
            this.lb_break_left_2.Name = "lb_break_left_2";
            this.lb_break_left_2.Size = new System.Drawing.Size(147, 27);
            this.lb_break_left_2.TabIndex = 38;
            this.lb_break_left_2.Text = "Break_left_2[mm]";
            this.lb_break_left_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakLeft11
            // 
            this.txtbreakLeft11.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakLeft11.Location = new System.Drawing.Point(170, 189);
            this.txtbreakLeft11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakLeft11.Name = "txtbreakLeft11";
            this.txtbreakLeft11.Size = new System.Drawing.Size(105, 27);
            this.txtbreakLeft11.TabIndex = 33;
            this.txtbreakLeft11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txttable2Alignmark22
            // 
            this.txttable2Alignmark22.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable2Alignmark22.Location = new System.Drawing.Point(832, 154);
            this.txttable2Alignmark22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable2Alignmark22.Name = "txttable2Alignmark22";
            this.txttable2Alignmark22.Size = new System.Drawing.Size(105, 27);
            this.txttable2Alignmark22.TabIndex = 34;
            this.txttable2Alignmark22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnBreakLeftYMove
            // 
            this.btnBreakLeftYMove.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeftYMove.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeftYMove.Location = new System.Drawing.Point(442, 224);
            this.btnBreakLeftYMove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeftYMove.Name = "btnBreakLeftYMove";
            this.btnBreakLeftYMove.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeftYMove.TabIndex = 154;
            this.btnBreakLeftYMove.Text = "이동";
            this.btnBreakLeftYMove.UseVisualStyleBackColor = false;
            // 
            // txttable2Alignmark21
            // 
            this.txttable2Alignmark21.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable2Alignmark21.Location = new System.Drawing.Point(723, 154);
            this.txttable2Alignmark21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable2Alignmark21.Name = "txttable2Alignmark21";
            this.txttable2Alignmark21.Size = new System.Drawing.Size(105, 27);
            this.txttable2Alignmark21.TabIndex = 33;
            this.txttable2Alignmark21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // txtbreakLeft12
            // 
            this.txtbreakLeft12.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakLeft12.Location = new System.Drawing.Point(279, 189);
            this.txtbreakLeft12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakLeft12.Name = "txtbreakLeft12";
            this.txtbreakLeft12.Size = new System.Drawing.Size(105, 27);
            this.txtbreakLeft12.TabIndex = 34;
            this.txtbreakLeft12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_table2_alignmark2
            // 
            this.lb_table2_alignmark2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_table2_alignmark2.Location = new System.Drawing.Point(569, 154);
            this.lb_table2_alignmark2.Name = "lb_table2_alignmark2";
            this.lb_table2_alignmark2.Size = new System.Drawing.Size(147, 27);
            this.lb_table2_alignmark2.TabIndex = 32;
            this.lb_table2_alignmark2.Text = "Table2AlignMark 2[mm]";
            this.lb_table2_alignmark2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakLeft1Move
            // 
            this.btnBreakLeft1Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeft1Move.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeft1Move.Location = new System.Drawing.Point(442, 189);
            this.btnBreakLeft1Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeft1Move.Name = "btnBreakLeft1Move";
            this.btnBreakLeft1Move.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeft1Move.TabIndex = 153;
            this.btnBreakLeft1Move.Text = "이동";
            this.btnBreakLeft1Move.UseVisualStyleBackColor = false;
            // 
            // txttable1Alignmark22
            // 
            this.txttable1Alignmark22.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable1Alignmark22.Location = new System.Drawing.Point(832, 119);
            this.txttable1Alignmark22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable1Alignmark22.Name = "txttable1Alignmark22";
            this.txttable1Alignmark22.Size = new System.Drawing.Size(105, 27);
            this.txttable1Alignmark22.TabIndex = 28;
            this.txttable1Alignmark22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // lb_break_right_1
            // 
            this.lb_break_right_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_break_right_1.Location = new System.Drawing.Point(16, 259);
            this.lb_break_right_1.Name = "lb_break_right_1";
            this.lb_break_right_1.Size = new System.Drawing.Size(147, 27);
            this.lb_break_right_1.TabIndex = 44;
            this.lb_break_right_1.Text = "Break_right_1[mm]";
            this.lb_break_right_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txttable1Alignmark21
            // 
            this.txttable1Alignmark21.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txttable1Alignmark21.Location = new System.Drawing.Point(723, 119);
            this.txttable1Alignmark21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttable1Alignmark21.Name = "txttable1Alignmark21";
            this.txttable1Alignmark21.Size = new System.Drawing.Size(105, 27);
            this.txttable1Alignmark21.TabIndex = 27;
            this.txttable1Alignmark21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnTable2Alignmark1Move
            // 
            this.btnTable2Alignmark1Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable2Alignmark1Move.ForeColor = System.Drawing.Color.Black;
            this.btnTable2Alignmark1Move.Location = new System.Drawing.Point(442, 154);
            this.btnTable2Alignmark1Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable2Alignmark1Move.Name = "btnTable2Alignmark1Move";
            this.btnTable2Alignmark1Move.Size = new System.Drawing.Size(50, 27);
            this.btnTable2Alignmark1Move.TabIndex = 152;
            this.btnTable2Alignmark1Move.Text = "이동";
            this.btnTable2Alignmark1Move.UseVisualStyleBackColor = false;
            // 
            // lb_table1_alignmark2
            // 
            this.lb_table1_alignmark2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_table1_alignmark2.Location = new System.Drawing.Point(569, 119);
            this.lb_table1_alignmark2.Name = "lb_table1_alignmark2";
            this.lb_table1_alignmark2.Size = new System.Drawing.Size(147, 27);
            this.lb_table1_alignmark2.TabIndex = 26;
            this.lb_table1_alignmark2.Text = "Table1AlignMark 2[mm]";
            this.lb_table1_alignmark2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbreakRight11
            // 
            this.txtbreakRight11.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakRight11.Location = new System.Drawing.Point(170, 259);
            this.txtbreakRight11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakRight11.Name = "txtbreakRight11";
            this.txtbreakRight11.Size = new System.Drawing.Size(105, 27);
            this.txtbreakRight11.TabIndex = 45;
            this.txtbreakRight11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnTable1Alignmark1Move
            // 
            this.btnTable1Alignmark1Move.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable1Alignmark1Move.ForeColor = System.Drawing.Color.Black;
            this.btnTable1Alignmark1Move.Location = new System.Drawing.Point(442, 119);
            this.btnTable1Alignmark1Move.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable1Alignmark1Move.Name = "btnTable1Alignmark1Move";
            this.btnTable1Alignmark1Move.Size = new System.Drawing.Size(50, 27);
            this.btnTable1Alignmark1Move.TabIndex = 151;
            this.btnTable1Alignmark1Move.Text = "이동";
            this.btnTable1Alignmark1Move.UseVisualStyleBackColor = false;
            // 
            // txtbreakRight12
            // 
            this.txtbreakRight12.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakRight12.Location = new System.Drawing.Point(279, 259);
            this.txtbreakRight12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakRight12.Name = "txtbreakRight12";
            this.txtbreakRight12.Size = new System.Drawing.Size(105, 27);
            this.txtbreakRight12.TabIndex = 46;
            this.txtbreakRight12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnBreakRightYRead
            // 
            this.btnBreakRightYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRightYRead.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRightYRead.Location = new System.Drawing.Point(390, 294);
            this.btnBreakRightYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRightYRead.Name = "btnBreakRightYRead";
            this.btnBreakRightYRead.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRightYRead.TabIndex = 150;
            this.btnBreakRightYRead.Text = "읽기";
            this.btnBreakRightYRead.UseVisualStyleBackColor = false;
            // 
            // lb_break_left_y
            // 
            this.lb_break_left_y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_break_left_y.Location = new System.Drawing.Point(16, 224);
            this.lb_break_left_y.Name = "lb_break_left_y";
            this.lb_break_left_y.Size = new System.Drawing.Size(147, 27);
            this.lb_break_left_y.TabIndex = 38;
            this.lb_break_left_y.Text = "Break_left_Y[mm]";
            this.lb_break_left_y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakRight1Read
            // 
            this.btnBreakRight1Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRight1Read.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRight1Read.Location = new System.Drawing.Point(390, 259);
            this.btnBreakRight1Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRight1Read.Name = "btnBreakRight1Read";
            this.btnBreakRight1Read.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRight1Read.TabIndex = 149;
            this.btnBreakRight1Read.Text = "읽기";
            this.btnBreakRight1Read.UseVisualStyleBackColor = false;
            // 
            // lb_break_right_y
            // 
            this.lb_break_right_y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_break_right_y.Location = new System.Drawing.Point(16, 294);
            this.lb_break_right_y.Name = "lb_break_right_y";
            this.lb_break_right_y.Size = new System.Drawing.Size(147, 27);
            this.lb_break_right_y.TabIndex = 51;
            this.lb_break_right_y.Text = "Break_right_Y[mm]";
            this.lb_break_right_y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakLeftYRead
            // 
            this.btnBreakLeftYRead.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeftYRead.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeftYRead.Location = new System.Drawing.Point(390, 224);
            this.btnBreakLeftYRead.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeftYRead.Name = "btnBreakLeftYRead";
            this.btnBreakLeftYRead.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeftYRead.TabIndex = 148;
            this.btnBreakLeftYRead.Text = "읽기";
            this.btnBreakLeftYRead.UseVisualStyleBackColor = false;
            // 
            // txtbreakLeftY
            // 
            this.txtbreakLeftY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakLeftY.Location = new System.Drawing.Point(170, 224);
            this.txtbreakLeftY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakLeftY.Name = "txtbreakLeftY";
            this.txtbreakLeftY.Size = new System.Drawing.Size(105, 27);
            this.txtbreakLeftY.TabIndex = 39;
            this.txtbreakLeftY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnBreakLeft1Read
            // 
            this.btnBreakLeft1Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeft1Read.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeft1Read.Location = new System.Drawing.Point(390, 189);
            this.btnBreakLeft1Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeft1Read.Name = "btnBreakLeft1Read";
            this.btnBreakLeft1Read.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeft1Read.TabIndex = 147;
            this.btnBreakLeft1Read.Text = "읽기";
            this.btnBreakLeft1Read.UseVisualStyleBackColor = false;
            // 
            // txtbreakRightY
            // 
            this.txtbreakRightY.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtbreakRightY.Location = new System.Drawing.Point(170, 294);
            this.txtbreakRightY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbreakRightY.Name = "txtbreakRightY";
            this.txtbreakRightY.Size = new System.Drawing.Size(105, 27);
            this.txtbreakRightY.TabIndex = 53;
            this.txtbreakRightY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnTable2Alignmark1Read
            // 
            this.btnTable2Alignmark1Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable2Alignmark1Read.ForeColor = System.Drawing.Color.Black;
            this.btnTable2Alignmark1Read.Location = new System.Drawing.Point(390, 154);
            this.btnTable2Alignmark1Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable2Alignmark1Read.Name = "btnTable2Alignmark1Read";
            this.btnTable2Alignmark1Read.Size = new System.Drawing.Size(50, 27);
            this.btnTable2Alignmark1Read.TabIndex = 146;
            this.btnTable2Alignmark1Read.Text = "읽기";
            this.btnTable2Alignmark1Read.UseVisualStyleBackColor = false;
            // 
            // btnTable1Alignmark1Read
            // 
            this.btnTable1Alignmark1Read.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable1Alignmark1Read.ForeColor = System.Drawing.Color.Black;
            this.btnTable1Alignmark1Read.Location = new System.Drawing.Point(390, 119);
            this.btnTable1Alignmark1Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable1Alignmark1Read.Name = "btnTable1Alignmark1Read";
            this.btnTable1Alignmark1Read.Size = new System.Drawing.Size(50, 27);
            this.btnTable1Alignmark1Read.TabIndex = 145;
            this.btnTable1Alignmark1Read.Text = "읽기";
            this.btnTable1Alignmark1Read.UseVisualStyleBackColor = false;
            // 
            // lb_cell_size
            // 
            this.lb_cell_size.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_cell_size.Location = new System.Drawing.Point(279, 329);
            this.lb_cell_size.Name = "lb_cell_size";
            this.lb_cell_size.Size = new System.Drawing.Size(105, 27);
            this.lb_cell_size.TabIndex = 62;
            this.lb_cell_size.Text = "Size[mm]";
            this.lb_cell_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBreakRightYCalc
            // 
            this.btnBreakRightYCalc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRightYCalc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRightYCalc.Location = new System.Drawing.Point(494, 294);
            this.btnBreakRightYCalc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRightYCalc.Name = "btnBreakRightYCalc";
            this.btnBreakRightYCalc.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRightYCalc.TabIndex = 144;
            this.btnBreakRightYCalc.Text = "CALC";
            this.btnBreakRightYCalc.UseVisualStyleBackColor = false;
            // 
            // txtcellSize1
            // 
            this.txtcellSize1.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtcellSize1.Location = new System.Drawing.Point(390, 329);
            this.txtcellSize1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtcellSize1.Name = "txtcellSize1";
            this.txtcellSize1.Size = new System.Drawing.Size(75, 27);
            this.txtcellSize1.TabIndex = 63;
            this.txtcellSize1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnBreakRight1Calc
            // 
            this.btnBreakRight1Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakRight1Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakRight1Calc.Location = new System.Drawing.Point(494, 259);
            this.btnBreakRight1Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakRight1Calc.Name = "btnBreakRight1Calc";
            this.btnBreakRight1Calc.Size = new System.Drawing.Size(50, 27);
            this.btnBreakRight1Calc.TabIndex = 143;
            this.btnBreakRight1Calc.Text = "CALC";
            this.btnBreakRight1Calc.UseVisualStyleBackColor = false;
            // 
            // txtcellSize2
            // 
            this.txtcellSize2.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.txtcellSize2.Location = new System.Drawing.Point(469, 329);
            this.txtcellSize2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtcellSize2.Name = "txtcellSize2";
            this.txtcellSize2.Size = new System.Drawing.Size(75, 27);
            this.txtcellSize2.TabIndex = 64;
            this.txtcellSize2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // btnBreakLeftYCalc
            // 
            this.btnBreakLeftYCalc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeftYCalc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeftYCalc.Location = new System.Drawing.Point(494, 224);
            this.btnBreakLeftYCalc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeftYCalc.Name = "btnBreakLeftYCalc";
            this.btnBreakLeftYCalc.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeftYCalc.TabIndex = 142;
            this.btnBreakLeftYCalc.Text = "CALC";
            this.btnBreakLeftYCalc.UseVisualStyleBackColor = false;
            // 
            // btnTable1Alignmark1Calc
            // 
            this.btnTable1Alignmark1Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable1Alignmark1Calc.ForeColor = System.Drawing.Color.Black;
            this.btnTable1Alignmark1Calc.Location = new System.Drawing.Point(494, 119);
            this.btnTable1Alignmark1Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable1Alignmark1Calc.Name = "btnTable1Alignmark1Calc";
            this.btnTable1Alignmark1Calc.Size = new System.Drawing.Size(50, 27);
            this.btnTable1Alignmark1Calc.TabIndex = 139;
            this.btnTable1Alignmark1Calc.Text = "CALC";
            this.btnTable1Alignmark1Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeft1Calc
            // 
            this.btnBreakLeft1Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnBreakLeft1Calc.ForeColor = System.Drawing.Color.Black;
            this.btnBreakLeft1Calc.Location = new System.Drawing.Point(494, 189);
            this.btnBreakLeft1Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBreakLeft1Calc.Name = "btnBreakLeft1Calc";
            this.btnBreakLeft1Calc.Size = new System.Drawing.Size(50, 27);
            this.btnBreakLeft1Calc.TabIndex = 141;
            this.btnBreakLeft1Calc.Text = "CALC";
            this.btnBreakLeft1Calc.UseVisualStyleBackColor = false;
            // 
            // btnTable2Alignmark1Calc
            // 
            this.btnTable2Alignmark1Calc.BackColor = System.Drawing.SystemColors.Control;
            this.btnTable2Alignmark1Calc.ForeColor = System.Drawing.Color.Black;
            this.btnTable2Alignmark1Calc.Location = new System.Drawing.Point(494, 154);
            this.btnTable2Alignmark1Calc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTable2Alignmark1Calc.Name = "btnTable2Alignmark1Calc";
            this.btnTable2Alignmark1Calc.Size = new System.Drawing.Size(50, 27);
            this.btnTable2Alignmark1Calc.TabIndex = 140;
            this.btnTable2Alignmark1Calc.Text = "CALC";
            this.btnTable2Alignmark1Calc.UseVisualStyleBackColor = false;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.btnRecipeCimDelete);
            this.panel19.Controls.Add(this.btnRecipeCimApply);
            this.panel19.Controls.Add(this.btnRecipeDelete);
            this.panel19.Controls.Add(this.btnRecipeApply);
            this.panel19.Controls.Add(this.btnRecipeSave);
            this.panel19.Controls.Add(this.btnRecipeCopy);
            this.panel19.Controls.Add(this.btnRecipeMake);
            this.panel19.ForeColor = System.Drawing.Color.White;
            this.panel19.Location = new System.Drawing.Point(4, 788);
            this.panel19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1733, 69);
            this.panel19.TabIndex = 65;
            // 
            // btnRecipeCimDelete
            // 
            this.btnRecipeCimDelete.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeCimDelete.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeCimDelete.Location = new System.Drawing.Point(1246, 4);
            this.btnRecipeCimDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeCimDelete.Name = "btnRecipeCimDelete";
            this.btnRecipeCimDelete.Size = new System.Drawing.Size(132, 62);
            this.btnRecipeCimDelete.TabIndex = 43;
            this.btnRecipeCimDelete.Text = "CIM Delete";
            this.btnRecipeCimDelete.UseVisualStyleBackColor = false;
            this.btnRecipeCimDelete.Click += new System.EventHandler(this.btnRecipeCimDelete_Click);
            // 
            // btnRecipeCimApply
            // 
            this.btnRecipeCimApply.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeCimApply.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeCimApply.Location = new System.Drawing.Point(1108, 4);
            this.btnRecipeCimApply.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeCimApply.Name = "btnRecipeCimApply";
            this.btnRecipeCimApply.Size = new System.Drawing.Size(132, 62);
            this.btnRecipeCimApply.TabIndex = 42;
            this.btnRecipeCimApply.Text = "CIM Apply";
            this.btnRecipeCimApply.UseVisualStyleBackColor = false;
            this.btnRecipeCimApply.Click += new System.EventHandler(this.btnRecipeCimApply_Click);
            // 
            // btnRecipeDelete
            // 
            this.btnRecipeDelete.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeDelete.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeDelete.Location = new System.Drawing.Point(424, 4);
            this.btnRecipeDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeDelete.Name = "btnRecipeDelete";
            this.btnRecipeDelete.Size = new System.Drawing.Size(132, 62);
            this.btnRecipeDelete.TabIndex = 41;
            this.btnRecipeDelete.Text = "Delete";
            this.btnRecipeDelete.UseVisualStyleBackColor = false;
            this.btnRecipeDelete.Click += new System.EventHandler(this.btnRecipeDelete_Click);
            // 
            // btnRecipeApply
            // 
            this.btnRecipeApply.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeApply.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeApply.Location = new System.Drawing.Point(850, 4);
            this.btnRecipeApply.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeApply.Name = "btnRecipeApply";
            this.btnRecipeApply.Size = new System.Drawing.Size(132, 62);
            this.btnRecipeApply.TabIndex = 40;
            this.btnRecipeApply.Text = "Apply";
            this.btnRecipeApply.UseVisualStyleBackColor = false;
            this.btnRecipeApply.Click += new System.EventHandler(this.btnRecipeApply_Click);
            // 
            // btnRecipeSave
            // 
            this.btnRecipeSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeSave.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeSave.Location = new System.Drawing.Point(284, 5);
            this.btnRecipeSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeSave.Name = "btnRecipeSave";
            this.btnRecipeSave.Size = new System.Drawing.Size(132, 62);
            this.btnRecipeSave.TabIndex = 39;
            this.btnRecipeSave.Text = "Save";
            this.btnRecipeSave.UseVisualStyleBackColor = false;
            this.btnRecipeSave.Click += new System.EventHandler(this.btnRecipeSave_Click);
            // 
            // btnRecipeCopy
            // 
            this.btnRecipeCopy.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeCopy.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeCopy.Location = new System.Drawing.Point(144, 5);
            this.btnRecipeCopy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeCopy.Name = "btnRecipeCopy";
            this.btnRecipeCopy.Size = new System.Drawing.Size(132, 62);
            this.btnRecipeCopy.TabIndex = 38;
            this.btnRecipeCopy.Text = "Copy";
            this.btnRecipeCopy.UseVisualStyleBackColor = false;
            this.btnRecipeCopy.Click += new System.EventHandler(this.btnRecipeCopy_Click);
            // 
            // btnRecipeMake
            // 
            this.btnRecipeMake.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipeMake.ForeColor = System.Drawing.Color.Black;
            this.btnRecipeMake.Location = new System.Drawing.Point(4, 4);
            this.btnRecipeMake.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecipeMake.Name = "btnRecipeMake";
            this.btnRecipeMake.Size = new System.Drawing.Size(132, 62);
            this.btnRecipeMake.TabIndex = 37;
            this.btnRecipeMake.Text = "Make";
            this.btnRecipeMake.UseVisualStyleBackColor = false;
            this.btnRecipeMake.Click += new System.EventHandler(this.btnRecipeMake_Click);
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.Location = new System.Drawing.Point(4, 540);
            this.listView2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(275, 246);
            this.listView2.TabIndex = 1;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "No.";
            this.columnHeader5.Width = 45;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Name";
            this.columnHeader6.Width = 130;
            // 
            // lvRecipe
            // 
            this.lvRecipe.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvRecipe.FullRowSelect = true;
            this.lvRecipe.GridLines = true;
            this.lvRecipe.Location = new System.Drawing.Point(3, 4);
            this.lvRecipe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lvRecipe.Name = "lvRecipe";
            this.lvRecipe.Size = new System.Drawing.Size(275, 530);
            this.lvRecipe.TabIndex = 0;
            this.lvRecipe.UseCompatibleStateImageBehavior = false;
            this.lvRecipe.View = System.Windows.Forms.View.Details;
            this.lvRecipe.SelectedIndexChanged += new System.EventHandler(this.lvRecipe_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No.";
            this.columnHeader1.Width = 45;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 130;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "CIM";
            this.columnHeader3.Width = 45;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Use";
            this.columnHeader4.Width = 45;
            // 
            // UcrlRecipeWindows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UcrlRecipeWindows";
            this.Size = new System.Drawing.Size(1740, 860);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView lvRecipe;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label lb_width;
        private System.Windows.Forms.TextBox txtcellCount;
        private System.Windows.Forms.Label lb_cell_count;
        private System.Windows.Forms.TextBox txtmaxHeightCount;
        private System.Windows.Forms.Label lb_max_height_count;
        private System.Windows.Forms.TextBox txtheight;
        private System.Windows.Forms.Label lb_height;
        private System.Windows.Forms.TextBox txtwidth;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnRecipeDelete;
        private System.Windows.Forms.Button btnRecipeApply;
        private System.Windows.Forms.Button btnRecipeSave;
        private System.Windows.Forms.Button btnRecipeCopy;
        private System.Windows.Forms.Button btnRecipeMake;
        private System.Windows.Forms.Button btnRecipeCimDelete;
        private System.Windows.Forms.Button btnRecipeCimApply;
        private System.Windows.Forms.TextBox txtyOffsetBreak;
        private System.Windows.Forms.Label lb_y_offset_break;
        private System.Windows.Forms.TextBox txtyOffsetPre;
        private System.Windows.Forms.Label lb_y_offset_pre;
        private System.Windows.Forms.TextBox txtldsBreak42;
        private System.Windows.Forms.TextBox txtldsBreak41;
        private System.Windows.Forms.TextBox txtldsBreak32;
        private System.Windows.Forms.TextBox txtldsBreak31;
        private System.Windows.Forms.TextBox txtldsBreak22;
        private System.Windows.Forms.TextBox txtldsBreak21;
        private System.Windows.Forms.TextBox txtldsBreak12;
        private System.Windows.Forms.Label lb_lds_break4;
        private System.Windows.Forms.Label lb_lds_break3;
        private System.Windows.Forms.Label lb_lds_break2;
        private System.Windows.Forms.TextBox txtldsBreak11;
        private System.Windows.Forms.Label lb_lds_break1;
        private System.Windows.Forms.TextBox txtrecipePath;
        private System.Windows.Forms.Label lb_recipe;
        private System.Windows.Forms.TextBox txttable2Alignmark22;
        private System.Windows.Forms.TextBox txttable2Alignmark21;
        private System.Windows.Forms.Label lb_table2_alignmark2;
        private System.Windows.Forms.TextBox txttable2Alignmark12;
        private System.Windows.Forms.TextBox txttable2Alignmark11;
        private System.Windows.Forms.Label lb_table2_alignmark1;
        private System.Windows.Forms.TextBox txttable1Alignmark22;
        private System.Windows.Forms.TextBox txttable1Alignmark21;
        private System.Windows.Forms.TextBox txttable1Alignmark12;
        private System.Windows.Forms.Label lb_table1_alignmark2;
        private System.Windows.Forms.TextBox txttable1Alignmark11;
        private System.Windows.Forms.Label lb_table1_alignmark1;
        private System.Windows.Forms.TextBox txtcellSize2;
        private System.Windows.Forms.TextBox txtcellSize1;
        private System.Windows.Forms.Label lb_cell_size;
        private System.Windows.Forms.TextBox txtbreakRightY;
        private System.Windows.Forms.TextBox txtbreakLeftY;
        private System.Windows.Forms.Label lb_break_right_y;
        private System.Windows.Forms.Label lb_break_left_y;
        private System.Windows.Forms.TextBox txtbreakRight22;
        private System.Windows.Forms.TextBox txtbreakLeft22;
        private System.Windows.Forms.TextBox txtbreakRight21;
        private System.Windows.Forms.Label lb_break_right_2;
        private System.Windows.Forms.TextBox txtbreakLeft21;
        private System.Windows.Forms.TextBox txtbreakRight12;
        private System.Windows.Forms.TextBox txtbreakRight11;
        private System.Windows.Forms.Label lb_break_left_2;
        private System.Windows.Forms.Label lb_break_right_1;
        private System.Windows.Forms.TextBox txtbreakLeft12;
        private System.Windows.Forms.TextBox txtbreakLeft11;
        private System.Windows.Forms.Label lb_break_left_1;
        private System.Windows.Forms.TextBox txtdxfpath;
        private System.Windows.Forms.Label lb_dxfpath;
        private System.Windows.Forms.Label lb_ta_thickness;
        private System.Windows.Forms.TextBox txttThickness;
        private System.Windows.Forms.ComboBox comxtcellType;
        private System.Windows.Forms.Label lb_cell_type;
        private System.Windows.Forms.ComboBox comxtprocessParam;
        private System.Windows.Forms.Label lb_process_param;
        private System.Windows.Forms.ComboBox comxtguideLayer;
        private System.Windows.Forms.Label lb_guide_layer;
        private System.Windows.Forms.ComboBox comxtprocessLayer;
        private System.Windows.Forms.Label lb_process_layer;
        private System.Windows.Forms.ComboBox comxtalignmentLayer;
        private System.Windows.Forms.Label lb_alignment_layer;
        private System.Windows.Forms.Button btnCassette1;
        private System.Windows.Forms.Button btnAlign;
        private System.Windows.Forms.Button btnRecipeLoad;
        private System.Windows.Forms.Button btnBreakRight2Move;
        private System.Windows.Forms.Button btnBreakRight2Read;
        private System.Windows.Forms.Button btnBreakRight2Calc;
        private System.Windows.Forms.Button btnBreakLeft2Move;
        private System.Windows.Forms.Button btnTable2AlignMark2Move;
        private System.Windows.Forms.Button btnTable1AlignMark2Move;
        private System.Windows.Forms.Button btnBreakLeft2Read;
        private System.Windows.Forms.Button btnTable2AlignMark2Read;
        private System.Windows.Forms.Button btnTable1AlignMark2Read;
        private System.Windows.Forms.Button btnBreakLeft2Calc;
        private System.Windows.Forms.Button btnTable2AlignMark2Calc;
        private System.Windows.Forms.Button btnTable1AlignMark2Calc;
        private System.Windows.Forms.Button btnBreakRightYMove;
        private System.Windows.Forms.Button btnBreakRight1Move;
        private System.Windows.Forms.Button btnBreakLeftYMove;
        private System.Windows.Forms.Button btnBreakLeft1Move;
        private System.Windows.Forms.Button btnTable2Alignmark1Move;
        private System.Windows.Forms.Button btnTable1Alignmark1Move;
        private System.Windows.Forms.Button btnBreakRightYRead;
        private System.Windows.Forms.Button btnBreakRight1Read;
        private System.Windows.Forms.Button btnBreakLeftYRead;
        private System.Windows.Forms.Button btnBreakLeft1Read;
        private System.Windows.Forms.Button btnTable2Alignmark1Read;
        private System.Windows.Forms.Button btnTable1Alignmark1Read;
        private System.Windows.Forms.Button btnBreakLeft1Calc;
        private System.Windows.Forms.Button btnTable2Alignmark1Calc;
        private System.Windows.Forms.Button btnTable1Alignmark1Calc;
        private System.Windows.Forms.Button btnCassetteNormal;
        private System.Windows.Forms.Button btnCassette4;
        private System.Windows.Forms.Button btnCassette2;
        private System.Windows.Forms.Button btnDxfPathLoad;
        private System.Windows.Forms.Button btnLDSBreak4Move;
        private System.Windows.Forms.Button btnLDSBreak1Move;
        private System.Windows.Forms.Button btnLDSBreak3Move;
        private System.Windows.Forms.Button btnLDSBreak1Read;
        private System.Windows.Forms.Button btnLDSBreak2Move;
        private System.Windows.Forms.Button btnLDSBreak2Read;
        private System.Windows.Forms.Button btnLDSBreak3Read;
        private System.Windows.Forms.Button btnLDSBreak4Read;
        private System.Windows.Forms.Button btnPickerCCW901;
        private System.Windows.Forms.Button btnPickerNormal1;
        private System.Windows.Forms.Button btnPickerCCW902;
        private System.Windows.Forms.Button btnPickerCW902;
        private System.Windows.Forms.Button btnPickerCW1801;
        private System.Windows.Forms.Button btnPickerCW901;
        private System.Windows.Forms.Button btnPickerCW1802;
        private System.Windows.Forms.Button btnPickerNormal2;
        private System.Windows.Forms.Button btnBreakRightYCalc;
        private System.Windows.Forms.Button btnBreakRight1Calc;
        private System.Windows.Forms.Button btnBreakLeftYCalc;
        private System.Windows.Forms.TextBox txtrecipe;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel8;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnLDSProcess4Move;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtldsProcess42;
        private System.Windows.Forms.Button btnLDSProcess3Move;
        private System.Windows.Forms.Label lb_lds_process1;
        private System.Windows.Forms.TextBox txtldsProcess41;
        private System.Windows.Forms.TextBox txtldsProcess11;
        private System.Windows.Forms.Button btnLDSProcess2Move;
        private System.Windows.Forms.Label lb_lds_process2;
        private System.Windows.Forms.TextBox txtldsProcess32;
        private System.Windows.Forms.Label lb_lds_process3;
        private System.Windows.Forms.Button btnLDSProcess1Move;
        private System.Windows.Forms.Label lb_lds_process4;
        private System.Windows.Forms.TextBox txtldsProcess31;
        private System.Windows.Forms.Button btnLDSProcess1Read;
        private System.Windows.Forms.Button btnLDSProcess4Read;
        private System.Windows.Forms.TextBox txtldsProcess12;
        private System.Windows.Forms.TextBox txtldsProcess22;
        private System.Windows.Forms.Button btnLDSProcess2Read;
        private System.Windows.Forms.Button btnLDSProcess3Read;
        private System.Windows.Forms.TextBox txtldsProcess21;
        private System.Windows.Forms.Panel panel7;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel5;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCutLineMeasure;
        private System.Windows.Forms.Button btnInspPosition;
        private System.Windows.Forms.Panel panel10;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel9;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnCassette8;
    }
}

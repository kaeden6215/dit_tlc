﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dit.Framework.Alalog
{
    public partial class UcrlDAItem : UserControl
    {
        private int Index { get; set; }
        private ADConverter_AJ65VBTCU_68DAVN _davn = null;

        public UcrlDAItem()
        {
            InitializeComponent();
        }
        public void Initialize(ADConverter_AJ65VBTCU_68DAVN davn, int idx)
        {
            this.Index = idx;
            lblTitle.Text = string.Format("■ T{0}", idx+1);
            _davn = davn;
            LoadValue();
        }
        private void LoadValue()
        {
            txtDaCh1Val.Text = _davn.DASetting[Index].DA1_CH1_WriteData.ToString();
            txtDaCh2Val.Text = _davn.DASetting[Index].DA1_CH2_WriteData.ToString();
            txtDaCh3Val.Text = _davn.DASetting[Index].DA1_CH3_WriteData.ToString();
            txtDaCh4Val.Text = _davn.DASetting[Index].DA1_CH4_WriteData.ToString();
            txtDaCh5Val.Text = _davn.DASetting[Index].DA1_CH5_WriteData.ToString();
            txtDaCh6Val.Text = _davn.DASetting[Index].DA1_CH6_WriteData.ToString();
            txtDaCh7Val.Text = _davn.DASetting[Index].DA1_CH7_WriteData.ToString();
            txtDaCh8Val.Text = _davn.DASetting[Index].DA1_CH8_WriteData.ToString();
        }
        private void btnDaSetValueSave_Click(object sender, EventArgs e)
        {
            Func<string, float> convert = delegate(string value)
            {
                return float.Parse(value.ToString() != "" ? value.ToString() : "0");   
            };
            float daValCh1 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh1Val.Text));
            float daValCh2 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh2Val.Text));
            float daValCh3 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh3Val.Text));
            float daValCh4 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh4Val.Text));
            float daValCh5 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh5Val.Text));
            float daValCh6 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh6Val.Text));
            float daValCh7 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh7Val.Text));
            float daValCh8 = _davn.DASetting[Index].CheckDALimitValue(convert(txtDaCh8Val.Text));

            _davn.DASetting[Index].SetDAValue(0, daValCh1);
            _davn.DASetting[Index].SetDAValue(1, daValCh2);
            _davn.DASetting[Index].SetDAValue(2, daValCh3);
            _davn.DASetting[Index].SetDAValue(3, daValCh4);
            _davn.DASetting[Index].SetDAValue(4, daValCh5);
            _davn.DASetting[Index].SetDAValue(5, daValCh6);
            _davn.DASetting[Index].SetDAValue(6, daValCh7);
            _davn.DASetting[Index].SetDAValue(7, daValCh8);
        }
    }
}

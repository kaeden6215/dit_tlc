using Dit.Framework.Comm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Dit.Framework.PLC._05._PLC;
using System.Windows.Forms;
using System.Diagnostics;

namespace Dit.Framework.PLC
{
    public enum EmAjinParam
    {
        AxisNo,
        PulseOutput,
        EncInput,
        Inposition,
        Alarm,
        MinusEndLimit,
        PlusEndLimit,
        MinVelocitu,
        MaxVelocitu,
        HomeSignal,
        HomeLevel,
        HomeDirection,
        ZPhaseLevel,
        HomeZPhaseType,
        StopMode,
        StopLevel,
        HomeVel1,
        HomeVel2,
        HomeVel3,
        HomeVelLast,
        HomeAccelation1,
        HomeAccelation2,
        HomeClearTime,
        HomeOffset,
        SWMinusLimit,
        SWPlusLimit,
        Pulse,
        Unit,
        InitPosition,
        InitVelocity,
        InitAccelation,
        InitDecelation,
        AbsRelMode,
        VelProfileMode,
        ServoOnLevel,
        AlarmResetLevel,
        ///
        EncoderType,
        SWLimitMode,
        SWLimitStopMode,
        SWLimitEnable,


        InitPtpSpeed,
        InitAccelationTime,
        InitInposition
    }
    public class AjinSvoMotorProxy
    {
        public VirtualAjinDirect Ajin { get; set; }
        public AjinMotorSetting MotorSetting { get; set; }
        public string Name { get; set; }
        public string SlayerName { get; set; }
        public int AjinAxisNo { get; set; }
        public double InPositionRange { get; set; }

        /// <summary>
        /// ServoMotorControl에서 가져올 때 곱해지고, ServoMotorControl로 전달할 때 나눔
        /// </summary>
        public float CtrllerToPcPositionScale = 1000;

        public PlcAddr XB_StatusHomeCompleteBit { get; set; }
        public PlcAddr XB_StatusHomeInPosition { get; set; }
        public PlcAddr XB_StatusMotorMoving { get; set; }
        public PlcAddr XB_StatusMotorInPosition { get; set; }
        public PlcAddr XB_StatusNegativeLimitSet { get; set; }
        public PlcAddr XB_StatusPositiveLimitSet { get; set; }
        public PlcAddr XB_StatusMotorServoOn { get; set; }
        public PlcAddr XB_ErrFatalFollowingError { get; set; }
        public PlcAddr XB_ErrAmpFaultError { get; set; }
        public PlcAddr XB_ErrI2TAmpFaultError { get; set; }

        public PlcAddr XF_CurrMotorPosition { get; set; }
        public PlcAddr XF_CurrMotorSpeed { get; set; }
        public PlcAddr XF_CurrMotorActualLoad { get; set; }
        public PlcAddr XF_CurrMotorLow { get; set; }
        public PlcAddr XF_CurrMotorHight { get; set; }
        public PlcAddr XF_CurrMotorError { get; set; }


        //서보 OnOff Cmd
        public PlcAddr YB_ServoOnOffCmd { get; set; }
        public PlcAddr YB_ServoOnOff { get; set; }
        public PlcAddr XB_ServoOnOffCmdAck { get; set; }

        //서보 Motor Stop
        public PlcAddr YB_MotorStopCmd { get; set; }
        public PlcAddr XB_MotorStopCmdAck { get; set; }


        //Home Cmd
        public PlcAddr YB_HomeCmd { get; set; }
        public PlcAddr XB_HomeCmdAck { get; set; }

        //Jog Command
        public PlcAddr YB_MotorJogMinusMove { get; set; }
        public PlcAddr YB_MotorJogPlusMove { get; set; }
        public PlcAddr YF_MotorJogSpeedCmd { get; set; }
        public PlcAddr XF_MotorJogSpeedCmdAck { get; set; }


        //Point To Point Move Cmd
        public PlcAddr YB_PTPMoveCmd { get; set; }
        public PlcAddr YB_PTPMoveCmdCancel { get; set; }
        public PlcAddr XB_PTPMoveCmdAck { get; set; }
        public PlcAddr YF_PTPMoveSpeed { get; set; }
        public PlcAddr XF_PTPMoveSpeedAck { get; set; }
        public PlcAddr YF_PTPMovePosition { get; set; }
        public PlcAddr XF_PTPMovePositionAck { get; set; }
        public PlcAddr YF_PTPMoveAccel { get; set; }
        public PlcAddr XF_PTPMoveAccelAck { get; set; }

        public double JogSpeed { get; set; }
        /// <summary>
        /// AXT_MOTION_QIMECHANICAL_SIGNAL enum 참고.
        /// </summary>
        public PlcAddr XI_CurrMotorLow { get; set; }
        /// <summary>
        /// AXT_MOTION_QIEND_STATUS enum 참고.
        /// </summary>
        public PlcAddr XI_CurrMotorHight { get; set; }
        /// <summary>
        /// AXT_MOTION_QIDRIVE_STATUS enum 참고.
        /// </summary>
        public PlcAddr XI_CurrMotorStatus1 { get; set; }

        public AjinSvoMotorProxy()
        {
            YB_PTPMoveCmd = null;
            XB_PTPMoveCmdAck = null;


            YF_PTPMoveSpeed = null;
            XF_PTPMoveSpeedAck = null;



        }

        private int _aliveStd = 1000;
        public bool IsAlive { get { return _scanTime.ElapsedMilliseconds < _aliveStd; } }
        private Stopwatch _scanTime = new Stopwatch();
        public void LogicWorking()
        {
            _scanTime.Restart();
            StatusLogic();
            JogStepLogic();
            HomeStepLogic();
            PTPMoveLogic();
            ServoOnOffLogic();
            MotorStopLogic();
            _scanTime.Stop();
        }

        private void StatusLogic()
        {
            this.XI_CurrMotorLow.vInt = (int)AjinProxy.GetMechanicalStatus(this.AjinAxisNo);
            this.XI_CurrMotorHight.vInt = (int)AjinProxy.GetMotionEndStatus(this.AjinAxisNo);
            this.XI_CurrMotorStatus1.vInt = (int)AjinProxy.GetDriveStatus(this.AjinAxisNo);

            XF_CurrMotorSpeed.vFloat = Ajin.CurrSpeed(this.AjinAxisNo) / CtrllerToPcPositionScale;
            XF_CurrMotorPosition.vFloat = Ajin.CurrPos(this.AjinAxisNo);
            //XF_CurrMotorActualLoad        .vFloat   = XF_CurrMotorActualLoad                 

            XB_StatusMotorServoOn.vBit = Ajin.IsServoOn(this.AjinAxisNo);
            XB_StatusMotorMoving.vBit = Ajin.IsMoving(this.AjinAxisNo);

            XB_StatusNegativeLimitSet.vBit = Ajin.IsNegLimit(this.AjinAxisNo);
            XB_StatusPositiveLimitSet.vBit = Ajin.IsPosLimit(this.AjinAxisNo);

            XB_StatusHomeInPosition.vBit = XB_StatusMotorServoOn.vBit
                && XB_StatusHomeCompleteBit.vBit
                && Math.Abs(Ajin.CurrPos(this.AjinAxisNo) - 0) < 3;
            XB_StatusMotorInPosition.vBit = XB_StatusMotorServoOn.vBit
                && Ajin.IsMoving(this.AjinAxisNo) == false;

            //XB_ErrI2TAmpFaultError           = XB_ErrI2TAmpFaultError                 
            //XB_ErrFatalFollowingError        = XB_ErrFatalFollowingError              
            //XB_ErrAmpFaultError              = XB_ErrAmpFaultError                    
        }

        private int _motorStopTimeoverStd = 10000;
        private Stopwatch _motorStopTimeoverCheck = new Stopwatch();
        public int SetpMotorStop = 0;
        public void MotorStopLogic()
        {
            if (_motorStopTimeoverCheck.ElapsedMilliseconds > _motorStopTimeoverStd)
            {
                _motorStopTimeoverCheck.Reset();
                SetpMotorStop = 0;
            }

            if (SetpMotorStop == 0)
            {
                if (YB_MotorStopCmd.vBit)
                    SetpMotorStop = 10;
            }
            else if (SetpMotorStop == 10)
            {
                XB_MotorStopCmdAck.vBit = true;

                bool servoOnOff = YB_ServoOnOff.vBit;

                Ajin.EmergencyStop(AjinAxisNo);
                //if (servoOnOff)
                //{
                //    Ajin.JogStop(AjinAxisNo);
                //}
                //else
                //{
                //    Ajin.MotorKill(AjinAxisNo);
                //}
                SetpMotorStop = 20;
            }
            else if (SetpMotorStop == 20)
            {
                if (YB_MotorStopCmd.vBit == false)
                {
                    XB_MotorStopCmdAck.vBit = false;
                    SetpMotorStop = 0;
                    PTPMoveStepNo = 0;
                }
            }
        }

        public void StepMoveLogic()
        {
        }

        private int _servoOnOffTimeoverStd = 10000;
        private Stopwatch _servoOnOffTimeoverCheck = new Stopwatch();
        public int SetpServoOnOff = 0;
        public void ServoOnOffLogic()
        {
            if (_servoOnOffTimeoverCheck.ElapsedMilliseconds > _servoOnOffTimeoverStd)
            {
                _servoOnOffTimeoverCheck.Reset();
                SetpMotorStop = 0;
            }

            if (SetpServoOnOff == 0)
            {
                if (YB_ServoOnOffCmd.vBit)
                    SetpServoOnOff = 10;
            }
            else if (SetpServoOnOff == 10)
            {
                XB_ServoOnOffCmdAck.vBit = true;

                bool servoOnOff = YB_ServoOnOff.vBit;
                if (servoOnOff)
                {
                    Ajin.MotorOn(AjinAxisNo);
                }
                else
                {
                    Ajin.MotorKill(AjinAxisNo);
                }
                SetpServoOnOff = 20;
            }
            else if (SetpServoOnOff == 20)
            {
                if (YB_ServoOnOffCmd.vBit == false)
                {
                    XB_ServoOnOffCmdAck.vBit = false;
                    SetpServoOnOff = 0;
                }
            }
        }

        private int _homeTimeoverStd = 120000;
        private Stopwatch _homeTimeoverCheck = new Stopwatch();
        private int HomeStepNo = 0;
        private PlcTimer _plcTmrHome = new PlcTimer("Home Seq Timer");
        public void HomeStepLogic()
        {
            if (_homeTimeoverCheck.ElapsedMilliseconds > _homeTimeoverStd)
            {
                _homeTimeoverCheck.Reset();
                HomeStepNo = 0;
            }

            if (HomeStepNo == 0)
            {
                if (YB_HomeCmd.vBit == true)
                {
                    XB_HomeCmdAck.vBit = true;
                    HomeStepNo = 10;
                }
            }
            else if (HomeStepNo == 10)
            {
                if (YB_HomeCmd.vBit == false)
                {
                    XB_HomeCmdAck.vBit = false;
                    HomeStepNo = 20;
                }
            }
            else if (HomeStepNo == 20)
            {
                //if (Ajin.SetAccelUnit(this.AjinAxisNo) == false)
                //{
                //    AjinLogger.Log(string.Format("fail {0} home stop", AjinAxisNo));
                //    HomeStepNo = 0;
                //}
                //else
                    HomeStepNo = 23;
            }
            else if (HomeStepNo == 23)
            {
                if (Ajin.JogStop(this.AjinAxisNo) == false)
                {
                    AjinLogger.Log(string.Format("fail {0} home stop", AjinAxisNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 25;
            }
            else if (HomeStepNo == 25)
            {
                if (Ajin.ErrorReset(this.AjinAxisNo) == false)
                {
                    EziLogger.Log(string.Format("fail {0} home motor error reset", AjinAxisNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 30;
            }
            else if (HomeStepNo == 30)
            {
                if (Ajin.MotorKill(this.AjinAxisNo) == false)
                {
                    AjinLogger.Log(string.Format("fail {0} home motor kill", AjinAxisNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 35;
            }
            else if (HomeStepNo == 35)
            {
                if (Ajin.MotorOn(this.AjinAxisNo) == false)
                {
                    AjinLogger.Log(string.Format("fail {0} home motor on", AjinAxisNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 40;
            }
            else if (HomeStepNo == 40)
            {
                //if (this.AjinAxisNo == 0 || this.AjinAxisNo == 1 || this.AjinAxisNo == 2 || this.AjinAxisNo == 3 )
                //{   
                //    if (Ajin.MotorHommingSetting(AjinAxisNo, 1000, 1000, 200, 100, 0.3, 0.3) == false)
                //    //                    if (Ajin.MotorHommingSetting(AjinAxisNo, 50, 30, 20, 10, 100, 100) == false)
                //    {
                //        AjinLogger.Log(string.Format("fail {0} home setting", AjinAxisNo));
                //        HomeStepNo = 0;
                //    }
                //    else
                //        HomeStepNo = 50;
                //}
                //else 
                if (this.AjinAxisNo == 6
                    || this.AjinAxisNo == 7
                    || this.AjinAxisNo == 0
                    || this.AjinAxisNo == 1
                    || this.AjinAxisNo == 2
                    || this.AjinAxisNo == 3
                    || this.AjinAxisNo == 18
                    || this.AjinAxisNo == 19
                    || this.AjinAxisNo == 20
                    || this.AjinAxisNo == 21
             
                    )
                {
                    if (Ajin.MotorHommingSetting(AjinAxisNo, 500, 300, 200, 100, 1000, 1000) == false)
                    //                    if (Ajin.MotorHommingSetting(AjinAxisNo, 50, 30, 20, 10, 100, 100) == false)
                    {
                        AjinLogger.Log(string.Format("fail {0} home setting", AjinAxisNo));
                        HomeStepNo = 0;
                    }
                    else
                        HomeStepNo = 50;
                }
                else if (this.AjinAxisNo == 13 || this.AjinAxisNo == 14  )
                {
                    if (Ajin.MotorHommingSetting(AjinAxisNo, 1000, 1000, 5000, 500, 2000, 2000) == false)
                    {
                        AjinLogger.Log(string.Format("fail {0} home setting", AjinAxisNo));
                        HomeStepNo = 0;
                    }
                    else
                        HomeStepNo = 50;
                }
                else if (  this.AjinAxisNo == 27 || this.AjinAxisNo == 28 || this.AjinAxisNo == 29 || this.AjinAxisNo == 30
                )
                {
                    if (Ajin.MotorHommingSetting(AjinAxisNo, 5000, 3000, 1500, 500, 10000, 10000) == false)
                    {
                        AjinLogger.Log(string.Format("fail {0} home setting", AjinAxisNo));
                        HomeStepNo = 0;
                    }
                    else
                        HomeStepNo = 50;
                }
                else
                {
                    if (Ajin.MotorHommingSetting(AjinAxisNo, 50000, 30000, 20000, 10000, 100000, 100000) == false)
                    {
                        AjinLogger.Log(string.Format("fail {0} home setting", AjinAxisNo));
                        HomeStepNo = 0;
                    }
                    else
                        HomeStepNo = 50;
                }
            }
            else if (HomeStepNo == 50)
            {
                if (Ajin.MotorHomming(AjinAxisNo) == false)
                {
                    AjinLogger.Log(string.Format("fail {0} start home", AjinAxisNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 60;
            }
            else if (HomeStepNo == 60)
            {
                uint rate = 0;
                Ajin.GetHomeRate(AjinAxisNo, out rate);
                if (rate == 100)
                {
                    HomeStepNo = 70;
                }
            }
            else if (HomeStepNo == 70)
            {
                AXT_MOTION_HOME_RESULT ret = Ajin.IsMotorHommingCompleted(AjinAxisNo);
                if (ret != AXT_MOTION_HOME_RESULT.HOME_SUCCESS)
                {
                    AjinLogger.Log(string.Format("fail {0} home : {1}", AjinAxisNo, ret.ToString()));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 80;
            }
            else if (HomeStepNo == 80)
            {
                if (Ajin.PositionClear(AjinAxisNo) == false)
                {
                    AjinLogger.Log(string.Format("fail {0} home position clear", AjinAxisNo));
                    HomeStepNo = 0;
                }
                else
                {
                    XB_StatusHomeCompleteBit.vBit = true;
                    HomeStepNo = 0;
                }
            }
        }

        private int _ptpTimeoverStd = 120000;
        private Stopwatch _ptpTimeoverCheck = new Stopwatch();
        private int PTPMoveStepNo = 0;
        public void PTPMoveLogic()
        {
            //인포지션 체크. 
            //XB_PTPMoveComplete.vBit = (Math.Abs(XF_PTPMovePositionAck.vFloat - XF_CurrMotorPosition.vFloat) < InPositionRange);

            if (_ptpTimeoverCheck.ElapsedMilliseconds > _ptpTimeoverStd)
            {
                _ptpTimeoverCheck.Reset();
                PTPMoveStepNo = 0;
            }

            if (PTPMoveStepNo == 0)
            {
                if (YB_PTPMoveCmd.vBit == true)
                {
                    XF_PTPMovePositionAck.vFloat = YF_PTPMovePosition.vFloat;
                    XF_PTPMoveSpeedAck.vFloat = YF_PTPMoveSpeed.vFloat;
                    XF_PTPMoveAccelAck.vFloat = YF_PTPMoveAccel.vFloat;

                    _ptpTimeoverStd = (int)(Math.Abs(YF_PTPMovePosition.vFloat - XF_CurrMotorPosition.vFloat) / YF_PTPMoveSpeed.vFloat);
                    _ptpTimeoverStd *= 1500;
                    _ptpTimeoverStd += 3000;
                    _ptpTimeoverCheck.Restart();

                    XB_PTPMoveCmdAck.vBit = true;
                    PTPMoveStepNo = 10;
                }
            }
            else if (PTPMoveStepNo == 10)
            {
                if (YB_PTPMoveCmd.vBit == false)
                {
                    XB_PTPMoveCmdAck.vBit = false;
                    PTPMoveStepNo = 20;
                }
            }
            else if (PTPMoveStepNo == 20)
            {
                Ajin.PTPMoveSet(AjinAxisNo);
                PTPMoveStepNo = 25;
            }
            else if (PTPMoveStepNo == 25)
            {
                Ajin.PTPMoveSpeed(AjinAxisNo, XF_PTPMovePositionAck.vFloat, XF_PTPMoveSpeedAck.vFloat * CtrllerToPcPositionScale);
                PTPMoveStepNo = 30;
            }
            else if (PTPMoveStepNo == 30)
            {
                if (XB_StatusMotorInPosition.vBit == true)
                {
                    PTPMoveStepNo = 40;
                }
            }
            else if (PTPMoveStepNo == 40)
            {
                if (XB_StatusMotorInPosition.vBit == true)
                {
                    PTPMoveStepNo = 50;
                }
            }
            else if (PTPMoveStepNo == 50)
            {
                PTPMoveStepNo = 0;
            }
        }

        private int JogPlusStep = 0;
        private int JogMinusStep = 0;
        public void JogStepLogic()
        {
            XF_MotorJogSpeedCmdAck.vFloat = YF_MotorJogSpeedCmd.vFloat;
            // 아진은 조그 스피드 수정이 없나요 ? 

            if (JogPlusStep == 0)
            {
                if (YB_MotorJogPlusMove.vBit == true)
                {
                    Ajin.JogMove(AjinAxisNo, XF_MotorJogSpeedCmdAck.vFloat, 1);
                    JogPlusStep = 10;
                }
            }
            else if (JogPlusStep == 10)
            {
                if (YB_MotorJogPlusMove.vBit == false)
                {
                    Ajin.JogStop(AjinAxisNo);
                    JogPlusStep = 0;
                }
            }

            if (JogMinusStep == 0)
            {
                if (YB_MotorJogMinusMove.vBit == true)
                {
                    Ajin.JogMove(AjinAxisNo, XF_MotorJogSpeedCmdAck.vFloat, -1);
                    JogMinusStep = 10;
                }
            }
            else if (JogMinusStep == 10)
            {
                if (YB_MotorJogMinusMove.vBit == false)
                {
                    Ajin.JogStop(AjinAxisNo);
                    JogMinusStep = 0;
                }
            }
        }

    }
    public class AjinSetting
    {
        public int DIOCount { get; set; }
        public int AIOCount { get; set; }
        public int MotorCount { get; set; }
        public List<AjinMotorSetting> MotorSetting { get; set; }
    }
    public class AjinMotorSetting : ICloneable
    {
        public int AxisNo;
        public AXT_MOTION_PULSE_OUTPUT PulseOutMethod = AXT_MOTION_PULSE_OUTPUT.TwoCcwCwHigh;
        public AXT_MOTION_EXTERNAL_COUNTER_INPUT EncoderMethod = AXT_MOTION_EXTERNAL_COUNTER_INPUT.ObverseSqr4Mode;
        public AXT_MOTION_LEVEL_MODE UseInposition = AXT_MOTION_LEVEL_MODE.USED; // 아진 - High, Unuse만 사용. -> 1, 0만 쓴다는건지?
        public AXT_MOTION_LEVEL_MODE AlarmStopSignal = AXT_MOTION_LEVEL_MODE.HIGH;
        public AXT_MOTION_LEVEL_MODE NegLimitType = AXT_MOTION_LEVEL_MODE.HIGH;
        public AXT_MOTION_LEVEL_MODE PosLimitType = AXT_MOTION_LEVEL_MODE.HIGH;
        public double MinSpeed = 1.0;
        public double MaxSpeed = 700000.0;
        public AXT_MOTION_HOME_DETECT HomeSignalSensor = AXT_MOTION_HOME_DETECT.HomeSensor;
        public bool IsHomeSignalNormalOpen = true;
        public AXT_MOTION_MOVE_DIR HomeDirection = AXT_MOTION_MOVE_DIR.DIR_CCW;
        public bool IsZPhaseNormalOpen = true;
        /// <summary>
        /// Home진행 시 0 : Z상 검출 X, 1 : Z상 검출 +방향, 2 : Z상 검출 -방향
        /// </summary>
        public uint UseFindZPhaseType = 0;
        public AXT_MOTION_STOPMODE StopMode = AXT_MOTION_STOPMODE.EMERGENCY_STOP;
        public AXT_MOTION_LEVEL_MODE StopModeLevel = AXT_MOTION_LEVEL_MODE.LOW;
        public double HomeVel1 = 10000.0;
        public double HomeVel2 = 10000.0;
        public double HomeVel3 = 2000.0;
        public double HomeVelLast = 100.0;
        public double HomeAcc1 = 40000.0;
        public double HomeAcc2 = 40000.0;
        public double HomeClearTime = 1000.0;
        public double HomeOffset = 0.0;
        public double SoftLimitNeg = 0.0;
        public double SoftLimitPos = 0.0;
        public int Pulse = 1;
        public double Unit = 1;
        public double InitPos = 200;
        public double InitSpeed = 200;
        public double InitAcc = 400;
        public double InitDec = 400;
        public AXT_MOTION_ABSREL AbsRelMode = AXT_MOTION_ABSREL.POS_ABS_MODE;
        public AXT_MOTION_PROFILE_MODE ProfileMode = AXT_MOTION_PROFILE_MODE.ASYM_S_CURVE_MODE;

        //public bool IsServoOnNormalOpen; 아진 - 네트워크타입에서 미사용
        //public bool IsAlarmResetSignalNormalOpen; 아진 - 네트워크타입에서 미사용
        //public uint EncoderType; 아진 - TLC에 Inc타입만 있고 기본값이 Inc이므로 설정할 필요 없다.
        public AXT_USE IsUseSoftLimit = AXT_USE.DISABLE;
        public AXT_MOTION_STOPMODE SoftLimitStopMode = AXT_MOTION_STOPMODE.EMERGENCY_STOP;
        public AXT_MOTION_SELECTION SoftLimitSource = AXT_MOTION_SELECTION.COMMAND;

        public object Clone()
        {
            AjinMotorSetting clone = new AjinMotorSetting();
            clone.PulseOutMethod = this.PulseOutMethod;
            clone.EncoderMethod = this.EncoderMethod;
            clone.UseInposition = this.UseInposition;
            clone.AlarmStopSignal = this.AlarmStopSignal;
            clone.NegLimitType = this.NegLimitType;
            clone.PosLimitType = this.PosLimitType;
            clone.MinSpeed = this.MinSpeed;
            clone.MaxSpeed = this.MaxSpeed;
            clone.HomeSignalSensor = this.HomeSignalSensor;
            clone.IsHomeSignalNormalOpen = this.IsHomeSignalNormalOpen;
            clone.HomeDirection = this.HomeDirection;
            clone.IsZPhaseNormalOpen = this.IsZPhaseNormalOpen;
            clone.UseFindZPhaseType = this.UseFindZPhaseType;
            clone.StopMode = this.StopMode;
            clone.StopModeLevel = this.StopModeLevel;
            clone.HomeVel1 = this.HomeVel1;
            clone.HomeVel2 = this.HomeVel2;
            clone.HomeVel3 = this.HomeVel3;
            clone.HomeVelLast = this.HomeVelLast;
            clone.HomeAcc1 = this.HomeAcc1;
            clone.HomeAcc2 = this.HomeAcc2;
            clone.HomeClearTime = this.HomeClearTime;
            clone.HomeOffset = this.HomeOffset;
            clone.SoftLimitNeg = this.SoftLimitNeg;
            clone.SoftLimitPos = this.SoftLimitPos;
            clone.Pulse = this.Pulse;
            clone.Unit = this.Unit;
            clone.InitPos = this.InitPos;
            clone.InitSpeed = this.InitSpeed;
            clone.InitAcc = this.InitAcc;
            clone.InitDec = this.InitDec;
            clone.AbsRelMode = this.AbsRelMode;
            clone.ProfileMode = this.ProfileMode;
            clone.IsUseSoftLimit = this.IsUseSoftLimit;
            clone.SoftLimitStopMode = this.SoftLimitStopMode;
            clone.SoftLimitSource = this.SoftLimitSource;

            return clone;
        }
    }

    public class VirtualAjinDirect : IVirtualMem
    {

        private const int MEM_SIZE = 102400;
        public int[] BYTE_JO = new int[MEM_SIZE];
        public int[] BYTE_JI = new int[MEM_SIZE];
        public double[] BYTE_JR = new double[MEM_SIZE];
        public double[] BYTE_JW = new double[MEM_SIZE];
        public int[] BYTE_JM = new int[MEM_SIZE];

        private string _ip = string.Empty;
        private int _port = 0;

        private UInt32 _dwDevice;
        private Int32 _bDriverOpen;

        private bool _isRunning = false;

        // ACS 연결 및 명령어 처리를 위한 인터페이스 클래스 생성

        private Thread _ajinWorker = null;
        private Thread _motorStatusWorker = null;
        private Thread _ioWorker = null;


        public List<PlcAddr> LstReadAddr { get; set; }
        public List<PlcAddr> LstWriteAddr { get; set; }

        public double CurrWorkingTime { get; set; }
        public double MaxWorkingTime { get; set; }

        public AjinSetting BoardSetting { get; set; }

        public VirtualAjinDirect(string name, string ip, int port, AjinSetting setting)
        {
            LstReadAddr = new List<PlcAddr>();
            LstWriteAddr = new List<PlcAddr>();

            BoardSetting = setting;
            _ip = ip;
            _port = port;

            _ajinWorker = new Thread(new ThreadStart(AjinWorker_DoWork));
            _ajinWorker.Priority = ThreadPriority.Highest;
        }

        private int _adInputInitStep;
        private void ADCardInitStep()
        {
            switch (_adInputInitStep)
            {
                case 0:
                    //                     if (XB_Init...)
                    //                     {
                    //                         _adInputInitStep = 10;
                    //                     }
                    break;
                case 10: // set volt range
                    //채널은 인풋/아웃풋 끼리 연속된다 ex) input station1 0~15 ch, output station1 0~15, input station2 16~32
                    //CAXA.AxaiSetRange(0, minVolt, maxVolt) // set vol range .. iointerfaceinfo에 담아서 설정과 다른 경우 초기화 하던지.. 별도 주소 잡아서 CCLINK처럼 처리하던지..
                    //_adInputInitStep = 20;
                    break;
                case 20: // set sampling period
                    //샘플링 주파수는 반드시 측정하고자 하는 아날로그 아날로그 신호의 최고 주파수의 2배 이상이 되어야 하 며,
                    //실제로는 측정하고자 하는 신호의 최고주파수의 5 ~ 10 5 ~ 105 ~ 105 ~ 10 배 정도의 주파수를 사용하는 것이 좋다 .
                    //CAXA.AxaiSetTriggerMode(0, AXT_AIO_TRIGGER_MODE.NORMAL_MODE);
                    //CAXA.AxaiHwSetSamplePeriod(0, 100); // 100us 단위로 샘플링
                    //_adInputInitStep = 30;
                    break;
                case 30: // set buffer
                    //CAXA.AxaiHwSetBufferOverflowMode(0, (uint)AXT_AIO_FULL_MODE.NEW_DATA_KEEP);
                    break;
            }
        }

        private bool _aliveSignal = false;
        private int _aliveStd = 1000;
        public bool AliveSignal
        {
            get
            {
                lock (this)
                {
                    return _aliveSignal;
                }
            }
        }
        private Stopwatch _aliveCheck = new Stopwatch();
        private void AjinWorker_DoWork()
        {
            DateTime start, end;
            while (_isRunning)
            {
                start = DateTime.Now;

                LogicWorking();

                CurrWorkingTime = (DateTime.Now - start).TotalMilliseconds;
                if (MaxWorkingTime < CurrWorkingTime)
                    MaxWorkingTime = CurrWorkingTime;

                if (_aliveCheck.IsRunning == false)
                    _aliveCheck.Restart();

                if (_aliveCheck.ElapsedMilliseconds > _aliveStd)
                {
                    _aliveSignal = !_aliveSignal;
                    _aliveCheck.Restart();
                }
            }
        }

        //메소드 연결
        public override int Open()
        {
            try
            {
                //_dwDevice = 0;// UMacProxy.PmacSelect(0);
                _bDriverOpen = AjinProxy.Initialize(BoardSetting.DIOCount, BoardSetting.AIOCount, BoardSetting.MotorCount) ? TRUE : FALSE;

                //jys:: 모두 초기화 성공 시에만 구동하려면.
                //if (_bDriverOpen == FALSE)
                //    return FALSE;

                _isRunning = true;
                _ajinWorker.Start();
                return TRUE;
            }
            catch (Exception ex)
            {
                return FALSE;
            }
        }
        public override int Close()
        {
            try
            {
                _isRunning = false;
                AjinProxy.Release();
                _ajinWorker.Join();

                //ACSChannel.CloseComm();

                return TRUE;
            }
            catch
            {
                return FALSE;
            }
        }

        //메소드 동기화
        public override int ReadFromPLC(PlcAddr addr, int wordSize)
        {
            switch (addr.Type)
            {
                case PlcMemType.JI:
                    uint retValue;
                    if (AjinProxy.ReadDIOByModule(addr, out retValue) == false)
                        return (int)Em_Mt_Er.Failed;
                    addr.vInt = (int)retValue;
                    break;
                case PlcMemType.JR:
                    double[] tempUints = null;
                    if (AjinProxy.ReadAIOByModule(addr, out tempUints) == false) return (int)Em_Mt_Er.Failed;

                    if (tempUints != null && tempUints.Length > 0)
                    {
                        for (int iPos = 0; iPos < 16; iPos++)
                            BYTE_JR[addr.Addr * 16 + iPos] = tempUints[iPos];
                    }
                    else
                        return (int)Em_Mt_Er.Failed;
                    break;
            }
            return (int)Em_Mt_Er.OK;
        }
        public override int WriteToPLC(PlcAddr addr, int wordSize)
        {
            switch (addr.Type)
            {
                case PlcMemType.JO:
                    AjinProxy.WriteDIOByModule(addr, (uint)addr.vInt); // 안되면 byte->uint->write필요
                    break;
            }

            return (int)Em_Mt_Er.OK;
        }

        //메소드 비트
        public override bool GetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void ClearBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr, bool value)
        {
            throw new Exception("미 구현");
        }
        public override void Toggle(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public bool[] GetBists(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
            //return new bool[0];
        }

        //메소드 - STRING
        public override int SetAscii(PlcAddr addr, string text)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        //메소드 - SHORT
        public override short GetShort(PlcAddr addr)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        public override void SetShort(PlcAddr addr, short value)
        {
            throw new Exception("미 구현");
        }
        //메소드 - SHORT        
        public override short[] GetShorts(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
        }
        public override void SetShorts(PlcAddr addr, short[] values, out int result)
        {
            throw new Exception("미 구현");
        }

        //메소드 - INT32
        public override int GetInt32(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetInt32(PlcAddr addr, int value)
        {
            throw new Exception("미 구현");
        }

        //읽어온 메모리에서 읽어오는 함수.
        public override bool VirGetBit(PlcAddr addr)
        {
            return VirGetInt32(addr).GetBit(addr.Bit);
        }
        public override void VirSetBit(PlcAddr addr, bool value)
        {
            int vv = VirGetInt32(addr).SetBit(addr.Bit, value);
            VirSetInt32(addr, vv);
        }
        public override short VirGetShort(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetShort(PlcAddr addr, short value)
        {
            throw new Exception("미구현 메모리");
        }
        public override string VirGetAscii(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetAscii(PlcAddr addr, string value)
        {
            throw new Exception("미구현 메모리");
        }
        public override int VirGetInt32(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.JO)
            {
                return BYTE_JO[addr.Addr];
            }
            else if (addr.Type == PlcMemType.JI)
            {
                return BYTE_JI[addr.Addr];
            }
            else if (addr.Type == PlcMemType.JM)
            {
                return BYTE_JM[addr.Addr];
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetInt32(PlcAddr addr, int value)
        {
            if (addr.Type == PlcMemType.JO)
            {
                BYTE_JO[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.JI)
            {
                BYTE_JI[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.JM)
            {
                BYTE_JM[addr.Addr] = value;
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }

        public override bool[] VirGetBits(PlcAddr addr, int wordSize)
        {
            throw new Exception("미구현 메모리");
        }
        public override short[] VirGetShorts(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override float VirGetFloat(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.JO)
            {
                return (float)BYTE_JO[addr.Addr];
            }
            else if (addr.Type == PlcMemType.JI)
            {
                return (float)BYTE_JI[addr.Addr];
            }
            else if (addr.Type == PlcMemType.JR)
            {
                return (float)BYTE_JR[addr.Addr * 16 + addr.Bit];
            }
            else if (addr.Type == PlcMemType.JM)
            {
                return (float)BYTE_JM[addr.Addr];
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetFloat(PlcAddr addr, float value)
        {
            if (addr.Type == PlcMemType.JO)
            {
                //BYTE_JO[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.JI)
            {
                //BYTE_JI[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.JR)
            {
                BYTE_JR[addr.Addr * 16 + addr.Bit] = (double)value;
            }
            else if (addr.Type == PlcMemType.JM)
            {
                BYTE_JM[addr.Addr] = (int)value;
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }


        public void LogicWorking()
        {
            int[] read = new int[0];

            foreach (PlcAddr addr in LstReadAddr)
                ReadFromPLC(addr, addr.Length);

            foreach (PlcAddr addr in LstWriteAddr)
                WriteToPLC(addr, addr.Length);

            Motors.ForEach(f => f.LogicWorking());
            SettingStep();
            ADCardInitStep();
        }

        #region Motor Access
        //모터 제어 로직 추가. 
        public List<AjinSvoMotorProxy> Motors = new List<AjinSvoMotorProxy>();

        #region Motor Status
        public bool ErrorReset(int axis)
        {
            if (CAXM.AxmSignalServoAlarmReset(axis, 1) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return true;
        }
        public bool IsServoOn(int axis)
        {
            uint tempUint = 0;
            if (CAXM.AxmSignalIsServoOn(axis, ref tempUint) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                tempUint = 0;
            return tempUint == 1 ? true : false;
        }
        public bool IsMoving(int axis)
        {
            uint tempUint = 0;
            if (CAXM.AxmStatusReadInMotion(axis, ref tempUint) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                tempUint = 0;
            return tempUint == 1 ? true : false;
        }
        public bool IsPosLimit(int axis)
        {
            uint temp1 = 0, temp2 = 0;
            if (CAXM.AxmSignalReadLimit(axis, ref temp1, ref temp2) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                temp1 = 0;
            return temp1 == 1 ? true : false;
        }
        public bool IsNegLimit(int axis)
        {
            uint temp1 = 0, temp2 = 0;
            if (CAXM.AxmSignalReadLimit(axis, ref temp1, ref temp2) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                temp2 = 0;
            return temp2 == 1 ? true : false;
        }
        public float CurrPos(int axis)
        {
            double temp = 0;
            if (CAXM.AxmStatusGetActPos(axis, ref temp) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                temp = float.MaxValue;
            return (float)temp;
        }
        public float CurrSpeed(int axis)
        {
            double temp = 0;
            if (CAXM.AxmStatusReadVel(axis, ref temp) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                temp = float.MaxValue;
            return (float)temp;
        }
        #endregion

        private int SettingStepNo = 0;
        public void SettingStep()
        {
            //if (SettingStepNo == 0)
            //{
            //    if (YB_PmacValueSave.vBit == true)
            //    {
            //        XB_PmacValueSaveAck.vBit = true;

            //        SettingStepNo = 10;
            //    }
            //}
            //else if (SettingStepNo == 10)
            //{
            //    if (YB_PmacValueSave.vBit == false)
            //    {

            //        for (int jPos = 0; jPos < motors.Count; jPos++)
            //        {

            //            for (int iPos = 1; iPos <= motors[jPos].PositionCount; iPos++)
            //            {
            //                motors[jPos].XF_Position1stPointAck[iPos - 1].vFloat = motors[jPos].YF_Position1stPoint[iPos - 1].vFloat;
            //                motors[jPos].XF_Position1stSpeedAck[iPos - 1].vFloat = motors[jPos].YF_Position1stSpeed[iPos - 1].vFloat;
            //                motors[jPos].XF_Position1stAccelAck[iPos - 1].vFloat = motors[jPos].YF_Position1stAccel[iPos - 1].vFloat;

            //            }
            //        }

            //        XB_PmacValueSaveAck.vBit = false;
            //        SettingStepNo = 20;
            //    }
            //}
            //else if (SettingStepNo == 20)
            //{

            //    SettingStepNo = 30;
            //}
            //else if (SettingStepNo == 30)
            //{
            //    SettingStepNo = 0;

            //}
        }


        private int InterlockCmdStepNo = 0;
        private int AcsResetStepNo = 0;
        public void CommCmdLogic()
        {
            ////Interlock 처리 
            //if (InterlockCmdStepNo == 0)
            //{
            //    if (YB_PinUpMotorInterlockOffCmd.vBit == true)
            //    {
            //        XB_PinUpMotorInterlockOffCmdAck.vBit = true;
            //        XB_PinUpInterlockOff.vBit = true;
            //        InterlockCmdStepNo = 10;
            //    }
            //}
            //else if (InterlockCmdStepNo == 10)
            //{
            //    if (YB_PinUpMotorInterlockOffCmd.vBit == false)
            //    {
            //        XB_PinUpMotorInterlockOffCmdAck.vBit = false;
            //        InterlockCmdStepNo = 20;
            //    }
            //}
            //else if (InterlockCmdStepNo == 20)
            //{
            //    InterlockCmdStepNo = 0;
            //}

            ////Acs Reset Step No 처리 
            //if (AcsResetStepNo == 0)
            //{
            //    if (YB_PmacResetCmd.vBit == true)
            //    {
            //        XB_PmacResetCmdAck.vBit = true;
            //        AcsResetStepNo = 10;
            //    }
            //}
            //else if (AcsResetStepNo == 10)
            //{
            //    if (YB_PmacResetCmd.vBit == false)
            //    {
            //        XB_PmacResetCmdAck.vBit = false;
            //        AcsResetStepNo = 20;
            //    }
            //}
            //else if (InterlockCmdStepNo == 20)
            //{
            //    //ACS RESET 처리. 요함. 

            //    AcsResetStepNo = 30;
            //}
            //else if (InterlockCmdStepNo == 30)
            //{
            //    AcsResetStepNo = 0;
            //}
        }

        public void MoveInterlockLogic()
        {
            //if (XB_PinUpInterlockOff.vBit == false)
            //{
            //    //인터락 조직 추가. 
            //}
        }
        public bool MotorOn(int axis)
        {
            uint duRetCode = 0;

            duRetCode = CAXM.AxmSignalServoOn(axis, 1);
            if (duRetCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return duRetCode == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS ? true : false;
        }
        public bool MotorKill(int axis)
        {
            uint duRetCode = 0;

            duRetCode = CAXM.AxmSignalServoOn(axis, 0);
            if (duRetCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return duRetCode == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS ? true : false;
        }
        public bool MotorHommingSetting(int axis, double vel1st, double vel2nd, double vel3rd, double velLast, double acc1st, double acc2nd)
        {
            //int nHmDir = 0; // 원점검색 1단계의 이동방향, (-)방향을 입력 
            //uint uHomeSignal = (uint)AXT_MOTION_HOME_DETECT.HomeSensor; // 신호검색구동에서의 신호들을 사용 
            //uint uZphas = 1;            // Z상 검출 유무, 1이면 검출 하고, 0이면 안함 
            //double dHomeClrTime = 2000.0;             // 원점 검색 Enc 값 Set하기 위한 대기시간
            //double dHomeOffset = 0.0;             // 원점검색후 Offset값만큼 추가적으로 이동
            //if (CAXM.AxmHomeSetMethod(axis, nHmDir, uHomeSignal, uZphas, dHomeClrTime, dHomeOffset) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
            //    return false;

            //double dVelFirst = 100;             // 1단계에서의 속도 
            //double dVelSecond = 20;             // 2단계에서의 속도 
            //double dVelThird = 10;             // 3단계에서의 속도
            //double dvelLast = 10;             // index검색및 정밀하게 검색하기위한 속도 (Offset값 이동시 적용) 
            //double dAccFirst = 200; // 1단계에서의 가속도
            //double dAccSecond = 40; // 2단계에서의 가속도
            if (CAXM.AxmHomeSetVel(axis, vel1st, vel2nd, vel3rd, velLast, acc1st, acc2nd) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;

            return true;
        }
        public bool MotorHomming(int axis)
        {
            uint duRetCode = 0;

            duRetCode = CAXM.AxmHomeSetStart(axis);
            if (duRetCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return duRetCode == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS ? true : false;
        }
        public bool PositionClear(int axis)
        {
            uint duRetCode = 0;

            duRetCode = CAXM.AxmStatusSetPosMatch(axis, 0);
            if (duRetCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return duRetCode == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS ? true : false;
        }
        public bool MotorHommingCheck(int axis)
        {
            uint homeResult = 0;
            CAXM.AxmHomeGetResult(axis, ref homeResult);

            switch ((AXT_MOTION_HOME_RESULT)homeResult)
            {
                case AXT_MOTION_HOME_RESULT.HOME_ERR_UNKNOWN:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_ERR_VELOCITY:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_ERR_NEG_LIMIT:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_ERR_POS_LIMIT:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_SEARCHING:
                    return true;
                case AXT_MOTION_HOME_RESULT.HOME_SUCCESS:
                    break;
                default:
                    break;
            }
            return false;
        }
        public bool GetHomeRate(int axis, out uint rate)
        {
            rate = 0;
            uint homeMainStep = 0;
            CAXM.AxmHomeGetRate(axis, ref homeMainStep, ref rate);
            return true;
        }
        public AXT_MOTION_HOME_RESULT IsMotorHommingCompleted(int axis)
        {
            uint homeResult = 0;
            CAXM.AxmHomeGetResult(axis, ref homeResult);
            return (AXT_MOTION_HOME_RESULT)homeResult;
            //             switch ((AXT_MOTION_HOME_RESULT)homeResult)
            //             {
            //                 case AXT_MOTION_HOME_RESULT.HOME_ERR_UNKNOWN:
            //                     break;
            //                 case AXT_MOTION_HOME_RESULT.HOME_ERR_VELOCITY:
            //                     break;
            //                 case AXT_MOTION_HOME_RESULT.HOME_ERR_NEG_LIMIT:
            //                     break;
            //                 case AXT_MOTION_HOME_RESULT.HOME_ERR_POS_LIMIT:
            //                     break;
            //                 case AXT_MOTION_HOME_RESULT.HOME_SEARCHING:
            //                     break;
            //                 case AXT_MOTION_HOME_RESULT.HOME_SUCCESS:
            //                     return true;
            //                 default:
            //                     break;
            //             }
            //             return false;            
        }
        public bool JogSpeed(int axis, double speed)
        {
            if (speed < 0)
                speed = 1;
            AjinSvoMotorProxy motor = Motors.FirstOrDefault(m => m.AjinAxisNo == axis);
            if (motor == null)
                return false;
            motor.JogSpeed = speed;
            return true;
        }
        public bool EmergencyStop(int axis)
        {
            CAXM.AxmMoveEStop(axis);
            return true;
        }
        public bool JogStop(int axis)
        {
            CAXM.AxmMoveSStop(axis);
            return true;
        }

        public bool SetAccelUnit(int axis)
        {
            CAXM.AxmMotSetAccelUnit(axis, 1);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="array">direction</param>
        /// <returns></returns>
        public bool JogMove(int axis, double speed, int array)
        {
            uint duRetCode = 0;
            double jogSpd = 0;
            AjinSvoMotorProxy motor = Motors.FirstOrDefault(m => m.AjinAxisNo == axis);

            if (motor == null)
                return false;

            jogSpd = speed;
            jogSpd *= array < 0 ? -1 : 1;

            duRetCode = CAXM.AxmMoveVel(axis, jogSpd, jogSpd * 10, jogSpd * 10);
            if (duRetCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return duRetCode == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS ? true : false;
        }

        public bool PTPMove(int axis, double position)
        {
            //             string cmd = string.Format("#{0}J={1}", axis, position);
            //             string cmdResult = string.Empty;
            //             return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
            return false;
        }
        public bool PTPMoveSet(int axis)
        {
            uint duRetCode = 0;

            duRetCode = CAXM.AxmMotSetAbsRelMode(axis, (uint)AXT_MOTION_ABSREL.POS_ABS_MODE);
            if (duRetCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return duRetCode == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS ? true : false;
        }

        public bool PTPMoveSpeed(int axis, double position, double speed)
        {
            uint duRetCode = 0;
            duRetCode = CAXM.AxmMoveStartPos(axis, position, speed, speed * 10, speed * 10);
            if (duRetCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return false;
            return duRetCode == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS ? true : false;
        }
        public bool StepMove(int axis, double distance)
        {
            //             string cmd = string.Format("#{0}J:{1}", axis, distance);
            //             string cmdResult = string.Empty;
            //             return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
            return false;
        }
        #endregion
    }
}



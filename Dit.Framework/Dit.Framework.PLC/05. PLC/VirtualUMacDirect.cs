﻿using Dit.Framework.Comm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Dit.Framework.PLC
{
    public class UmacSvoMotorProxy
    {
        public VirtualUMacDirect Umac { get; set; }
        public string Name { get; set; }
        public string SlayerName { get; set; }
        public int UmacAxisNo { get; set; }
        public int InteralAxisNo { get; set; }
        public double InPositionRange { get; set; }

        public PlcAddr XB_StatusHomeCompleteBit { get; set; }
        public PlcAddr XB_StatusHomeInPosition { get; set; }
        public PlcAddr XB_StatusMotorMoving { get; set; }
        public PlcAddr XB_StatusMotorInPosition { get; set; }
        public PlcAddr XB_StatusNegativeLimitSet { get; set; }
        public PlcAddr XB_StatusPositiveLimitSet { get; set; }
        public PlcAddr XB_StatusMotorServoOn { get; set; }
        public PlcAddr XB_ErrFatalFollowingError { get; set; }
        public PlcAddr XB_ErrAmpFaultError { get; set; }
        public PlcAddr XB_ErrI2TAmpFaultError { get; set; }

        public PlcAddr XF_CurrMotorPosition { get; set; }
        public PlcAddr XF_CurrMotorSpeed { get; set; }
        public PlcAddr XF_CurrMotorActualLoad { get; set; }
        public PlcAddr XF_CurrMotorLow { get; set; }
        public PlcAddr XF_CurrMotorHight { get; set; }
        public PlcAddr XF_CurrMotorError { get; set; }




        //서보 OnOff Cmd
        public PlcAddr YB_ServoOnOffCmd { get; set; }
        public PlcAddr YB_ServoOnOff { get; set; }
        public PlcAddr XB_ServoOnOffCmdAck { get; set; }

        //서보 Motor Stop
        public PlcAddr YB_MotorStopCmd { get; set; }
        public PlcAddr XB_MotorStopCmdAck { get; set; }


        //Home Cmd
        public PlcAddr YB_HomeCmd { get; set; }
        public PlcAddr XB_HomeCmdAck { get; set; }

        //Jog Command
        public PlcAddr YB_MotorJogMinusMove { get; set; }
        public PlcAddr YB_MotorJogPlusMove { get; set; }
        public PlcAddr YF_MotorJogSpeedCmd { get; set; }


        //Point To Point Move Cmd
        public PlcAddr YB_PTPMoveCmd { get; set; }
        public PlcAddr YB_PTPMoveCmdCancel { get; set; }
        public PlcAddr XB_PTPMoveCmdAck { get; set; }
        public PlcAddr YF_PTPMoveSpeed { get; set; }
        public PlcAddr XF_PTPMoveSpeedAck { get; set; }
        public PlcAddr YF_PTPMovePosition { get; set; }
        public PlcAddr XF_PTPMovePositionAck { get; set; }
        public PlcAddr YF_PTPMoveAccel { get; set; }
        public PlcAddr XF_PTPMoveAccelAck { get; set; }

        public PlcAddr XB_PTPMoveComplete { get; set; }



        //UMAC ADDRESS
        public PlcAddr UMAC_XF_CurrlMotorPosition { get; set; }
        public PlcAddr UMAC_XF_CmmdMotorPosition { get; set; }
        public PlcAddr UMAC_XF_CurrMotorSpeed { get; set; }
        public PlcAddr UMAC_XF_CurrMotorActualLoad { get; set; }
        public PlcAddr UMAC_XF_CurrMotorLow { get; set; }
        public PlcAddr UMAC_XF_CurrMotorHight { get; set; }
        public PlcAddr UMAC_XF_CurrMotorError { get; set; }



        public UmacSvoMotorProxy()
        {
            YB_PTPMoveCmd = null;
            XB_PTPMoveCmdAck = null;


            YF_PTPMoveSpeed = null;
            XF_PTPMoveSpeedAck = null;

            XB_PTPMoveComplete = null;


        }
        public void LogicWorking()
        {

            XF_CurrMotorPosition.vFloat = UMAC_XF_CurrlMotorPosition.vFloat;
            XF_CurrMotorSpeed.vFloat = UMAC_XF_CurrMotorSpeed.vFloat;

            //상태 정보. 
            XB_StatusHomeCompleteBit.vBit = UMAC_XF_CurrMotorHight.vInt.GetBit(10);

            XB_StatusHomeInPosition.vBit = UMAC_XF_CurrMotorHight.vInt.GetBit(10) && Math.Abs(XF_CurrMotorPosition.vFloat) < 20;
            XB_StatusMotorMoving.vBit = UMAC_XF_CurrMotorHight.vInt.GetBit(0) == false && UMAC_XF_CurrMotorLow.vInt.GetBit(13) == false;

            XB_StatusMotorInPosition.vBit = UMAC_XF_CurrMotorHight.vInt.GetBit(0);
            XB_StatusNegativeLimitSet.vBit = UMAC_XF_CurrMotorLow.vInt.GetBit(22);
            XB_StatusPositiveLimitSet.vBit = UMAC_XF_CurrMotorLow.vInt.GetBit(21);

            XB_StatusMotorServoOn.vBit = UMAC_XF_CurrMotorLow.vInt.GetBit(19);
            XB_ErrFatalFollowingError.vBit = UMAC_XF_CurrMotorHight.vInt.GetBit(2);
            XB_ErrAmpFaultError.vBit = UMAC_XF_CurrMotorHight.vInt.GetBit(3);
            XB_ErrI2TAmpFaultError.vBit = UMAC_XF_CurrMotorHight.vInt.GetBit(5);


            JogStepLogic();
            HomeStepLogic();
            PTPMoveLogic();

            ServoOnOffLogic();
            MotorStopLogic();
        }


        private int _motorStopTimeoverStd = 10000;
        private Stopwatch _motorStopTimeoverCheck = new Stopwatch();
        public int SetpMotorStop = 0;
        public void MotorStopLogic()
        {
            if (_motorStopTimeoverCheck.ElapsedMilliseconds > _motorStopTimeoverStd)
            {                
                _motorStopTimeoverCheck.Reset();
                SetpMotorStop = 0;
            }

            if (SetpMotorStop == 0)
            {
                if (YB_MotorStopCmd.vBit)
                    SetpMotorStop = 10;
            }
            else if (SetpMotorStop == 10)
            {
                _motorStopTimeoverCheck.Restart();
                XB_MotorStopCmdAck.vBit = true;
                Umac.JogStop(InteralAxisNo);
                SetpMotorStop = 20;
            }
            else if (SetpMotorStop == 20)
            {
                if (YB_MotorStopCmd.vBit == false)
                {
                    XB_MotorStopCmdAck.vBit = false;
                    SetpMotorStop = 0;
                }
            }
        }

        public void StepMoveLogic()
        {
        }

        private int _servoOnOffTimeoverStd = 10000;
        private Stopwatch _servoOnOffTimeoverCheck = new Stopwatch();        
        public int SetpServoOnOff = 0;
        public void ServoOnOffLogic()
        {
            if (_servoOnOffTimeoverCheck.ElapsedMilliseconds > _servoOnOffTimeoverStd)
            {
                _servoOnOffTimeoverCheck.Reset();
                SetpMotorStop = 0;
            }

            if (SetpServoOnOff == 0)
            {
                if (YB_ServoOnOffCmd.vBit)
                    SetpServoOnOff = 10;
            }
            else if (SetpServoOnOff == 10)
            {
                _servoOnOffTimeoverCheck.Restart();

                XB_ServoOnOffCmdAck.vBit = true;

                bool servoOnOff = YB_ServoOnOff.vBit;
                if (servoOnOff)
                {
                    Umac.JogStop(InteralAxisNo);
                }
                else
                {
                    Umac.MotorKill(InteralAxisNo);
                }
                SetpServoOnOff = 20;
            }
            else if (SetpServoOnOff == 20)
            {
                if (YB_ServoOnOffCmd.vBit == false)
                {
                    XB_ServoOnOffCmdAck.vBit = false;
                    SetpServoOnOff = 0;
                }
            }
        }


        private int _homeTimeoverStd = 120000;
        private Stopwatch _homeTimeoverCheck = new Stopwatch();
        public int Setphome = 0;
        private int HomeStepNo = 0;
        private PlcTimer _plcTmrHome = new PlcTimer("Home Seq Timer");
        public void HomeStepLogic()
        {
            if (_homeTimeoverCheck.ElapsedMilliseconds > _homeTimeoverStd)
            {
                _homeTimeoverCheck.Reset();
                HomeStepNo = 0;
            }

            if (HomeStepNo == 0)
            {
                if (YB_HomeCmd.vBit == true)
                {
                    XB_HomeCmdAck.vBit = true;
                    _homeTimeoverCheck.Restart();
                    HomeStepNo = 10;
                }
            }
            else if (HomeStepNo == 10)
            {
                if (YB_HomeCmd.vBit == false)
                {
                    XB_HomeCmdAck.vBit = false;
                    HomeStepNo = 20;
                }
            }
            else if (HomeStepNo == 20)
            {
                Umac.MotorHomming(InteralAxisNo);

                HomeStepNo = 30;
            }
            else if (HomeStepNo == 30)
            {
                if (Umac.IsMotorHommingCompleted(InteralAxisNo))
                {
                    HomeStepNo = 40;
                }
            }
            else if (HomeStepNo == 40)
            {
                XB_StatusHomeInPosition.vBit = true;
                XB_StatusHomeCompleteBit.vBit = true;
                XB_StatusMotorInPosition.vBit = true;

                HomeStepNo = 0;
            }
        }

        private int _ptpTimeoverStd = 120000;
        private Stopwatch _ptpTimeoverCheck = new Stopwatch();
        private int PTPMoveStepNo = 0;
        public void PTPMoveLogic()
        {
            //인포지션 체크. 
            XB_PTPMoveComplete.vBit = (Math.Abs(XF_PTPMovePositionAck.vFloat - XF_CurrMotorPosition.vFloat) < InPositionRange);

            if (_ptpTimeoverCheck.ElapsedMilliseconds > _ptpTimeoverStd)
            {
                _ptpTimeoverCheck.Reset();
                PTPMoveStepNo = 0;
            }

            if (PTPMoveStepNo == 0)
            {
                if (YB_PTPMoveCmd.vBit == true)
                {
                    XF_PTPMovePositionAck.vFloat = YF_PTPMovePosition.vFloat;
                    XF_PTPMoveSpeedAck.vFloat = YF_PTPMoveSpeed.vFloat;
                    XF_PTPMoveAccelAck.vFloat = YF_PTPMoveAccel.vFloat;

                    _ptpTimeoverStd = (int)(Math.Abs(YF_PTPMovePosition.vFloat - XF_CurrMotorPosition.vFloat) / YF_PTPMoveSpeed.vFloat);
                    _ptpTimeoverStd *= 1500;
                    _ptpTimeoverStd += 3000;
                    _ptpTimeoverCheck.Restart();

                    XB_PTPMoveCmdAck.vBit = true;
                    PTPMoveStepNo = 10;
                }
            }
            else if (PTPMoveStepNo == 10)
            {
                if (YB_PTPMoveCmd.vBit == false)
                {
                    XB_PTPMoveCmdAck.vBit = false;
                    PTPMoveStepNo = 20;
                }
            }
            else if (PTPMoveStepNo == 20)
            {
                Umac.PTPMoveSpeed(InteralAxisNo, XF_PTPMovePositionAck.vFloat, XF_PTPMoveSpeedAck.vFloat);
                PTPMoveStepNo = 30;
            }
            else if (PTPMoveStepNo == 30)
            {
                //if (XB_StatusMotorInPosition.vBit == true)
                {
                    PTPMoveStepNo = 40;
                }
            }
            else if (PTPMoveStepNo == 40)
            {
                //if (XB_PTPMoveComplete.vBit == true && XB_StatusMotorInPosition.vBit == true)
                {
                    PTPMoveStepNo = 50;
                }
            }
            else if (PTPMoveStepNo == 50)
            {
                PTPMoveStepNo = 0;
            }
        }

        private int JogPlusStep = 0;
        private int JogMinusStep = 0;
        public void JogStepLogic()
        {
            if (JogPlusStep == 0)
            {
                if (YB_MotorJogPlusMove.vBit == true)
                {
                    Umac.JogMove(InteralAxisNo, -1);
                    JogPlusStep = 10;
                }
            }
            else if (JogPlusStep == 10)
            {
                if (YB_MotorJogPlusMove.vBit == false)
                {
                    Umac.JogStop(InteralAxisNo);
                    JogPlusStep = 0;
                }
            }

            if (JogMinusStep == 0)
            {
                if (YB_MotorJogMinusMove.vBit == true)
                {
                    Umac.JogMove(InteralAxisNo, 1);
                    JogMinusStep = 10;
                }
            }
            else if (JogMinusStep == 10)
            {
                if (YB_MotorJogMinusMove.vBit == false)
                {
                    Umac.JogStop(InteralAxisNo);
                    JogMinusStep = 0;
                }
            }
        }
        public void SetUmacAddress()
        {
            int nParamCnt = 6;

            UMAC_XF_CurrlMotorPosition  /**/= new PlcAddr(PlcMemType.M, 6300 + nParamCnt * (UmacAxisNo - 1) + 0) { PLC = this.Umac };
            UMAC_XF_CmmdMotorPosition   /**/= new PlcAddr(PlcMemType.M, 6300 + nParamCnt * (UmacAxisNo - 1) + 1) { PLC = this.Umac };
            UMAC_XF_CurrMotorSpeed      /**/= new PlcAddr(PlcMemType.M, 6300 + nParamCnt * (UmacAxisNo - 1) + 2) { PLC = this.Umac };
            UMAC_XF_CurrMotorActualLoad /**/= new PlcAddr(PlcMemType.M, 6300 + nParamCnt * (UmacAxisNo - 1) + 3) { PLC = this.Umac };
            UMAC_XF_CurrMotorLow        /**/= new PlcAddr(PlcMemType.M, 6300 + nParamCnt * (UmacAxisNo - 1) + 4) { PLC = this.Umac };
            UMAC_XF_CurrMotorHight      /**/= new PlcAddr(PlcMemType.M, 6300 + nParamCnt * (UmacAxisNo - 1) + 5) { PLC = this.Umac };
            UMAC_XF_CurrMotorError      /**/= new PlcAddr(PlcMemType.M, 6300 + nParamCnt * (UmacAxisNo - 1) + 6) { PLC = this.Umac };
        }
    }

    public class VirtualUMacDirect : IVirtualMem
    {

        private const int MEM_SIZE = 102400;
        public int[] BYTE_M = new int[MEM_SIZE];
        public int[] BYTE_S = new int[MEM_SIZE];

        private string _ip = string.Empty;
        private int _port = 0;

        private UInt32 _dwDevice;
        private Int32 _bDriverOpen;

        private bool _isRunning = false;        

        // ACS 연결 및 명령어 처리를 위한 인터페이스 클래스 생성

        private Thread _motorWorker = null;
        private Thread _motorStatusWorker = null;


        public List<PlcAddr> LstReadAddr { get; set; }
        public List<PlcAddr> LstWriteAddr { get; set; }

        public double CurrWorkingTime { get; set; }
        public double MaxWorkingTime { get; set; }

        public VirtualUMacDirect(string name, string ip, int port)
        {
            LstReadAddr = new List<PlcAddr>();
            LstWriteAddr = new List<PlcAddr>();

            _ip = ip;
            _port = port;
            //_motorStatusWorker = new Thread(new ThreadStart(MotorStatusWorker_DoWork));
            //_motorStatusWorker.Priority = ThreadPriority.Highest;

            _motorWorker = new Thread(new ThreadStart(MotorWorker_DoWork));
            _motorWorker.Priority = ThreadPriority.Highest;
        }
        private void MotorStatusWorker_DoWork()
        {
            //while (_isRunning)
            //{
            //    float vv = (float)ACSChannel.GetFPosition(0);
            //}
        }

        private bool _aliveSignal = false;
        private int _aliveStd = 1000;
        public bool AliveSignal
        {
            get
            {
                lock (this)
                {
                    return _aliveSignal;
                }
            }
        }
        private Stopwatch _aliveCheck = new Stopwatch();
        private void MotorWorker_DoWork()
        {
            DateTime start, end;
            while (_isRunning)
            {
                start = DateTime.Now;
                LogicWorking();

                CurrWorkingTime = (DateTime.Now - start).TotalMilliseconds;
                if (MaxWorkingTime < CurrWorkingTime)
                    MaxWorkingTime = CurrWorkingTime;
            }

            if (_aliveCheck.IsRunning == false)
                _aliveCheck.Restart();

            if (_aliveCheck.ElapsedMilliseconds > _aliveStd)
            {
                _aliveSignal = !_aliveSignal;
                _aliveCheck.Restart();
            }
        }

        //메소드 연결
        public override int Open()
        {
            try
            {
                _dwDevice = UMacProxy.PmacSelect(0);
                _bDriverOpen = UMacProxy.OpenPmacDevice(_dwDevice);
                _bDriverOpen = UMacProxy.OpenPmacDevice(_dwDevice);

                //motors.ForEach(f => f.VirAcs = this);
                //ACSChannel.OpenCommEthernetTCP(_ip.ToString(), _port);

                //var strTemp1 = ACSChannel.Transaction("?SYSINFO(13)");
                //var strTemp2 = ACSChannel.Transaction("?SYSINFO(10)");

                _isRunning = true;
                _motorWorker.Start();
                _motorStatusWorker.Start();

                return TRUE;
            }
            catch (Exception ex)
            {
                return FALSE;
            }
        }
        public override int Close()
        {
            try
            {
                _isRunning = false;

                _motorWorker.Join();
                _motorStatusWorker.Join();

                //ACSChannel.CloseComm();

                return TRUE;
            }
            catch
            {
                return FALSE;
            }
        }

        //메소드 동기화
        public override int ReadFromPLC(PlcAddr addr, int wordSize)
        {
            int[] readValues = new int[addr.Length];
            if (UMacProxy.ReadMemory(_dwDevice, addr, out readValues) == true)
            {
                Array.Copy(readValues, 0, BYTE_M, addr.Addr, addr.Length);
                Console.WriteLine("OK");
            }
            else
            {
                Console.WriteLine("FAIL");
            }

            return (int)Em_Mt_Er.OK;
        }
        public override int WriteToPLC(PlcAddr addr, int wordSize)
        {
            int[] readValues = new int[addr.Length];

            Array.Copy(BYTE_M, addr.Addr, readValues, 0, addr.Length);            
            if (UMacProxy.WriteMemory(_dwDevice, addr, readValues) == true)
            {
                Console.WriteLine("OK");
            }
            else
            {
                Console.WriteLine("FAIL");
            }


            return (int)Em_Mt_Er.OK;
        }

        //메소드 비트
        public override bool GetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void ClearBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr, bool value)
        {
            throw new Exception("미 구현");
        }
        public override void Toggle(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public bool[] GetBists(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
            //return new bool[0];
        }

        //메소드 - STRING
        public override int SetAscii(PlcAddr addr, string text)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        //메소드 - SHORT
        public override short GetShort(PlcAddr addr)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        public override void SetShort(PlcAddr addr, short value)
        {
            throw new Exception("미 구현");
        }
        //메소드 - SHORT        
        public override short[] GetShorts(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
        }
        public override void SetShorts(PlcAddr addr, short[] values, out int result)
        {
            throw new Exception("미 구현");
        }

        //메소드 - INT32
        public override int GetInt32(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetInt32(PlcAddr addr, int value)
        {
            throw new Exception("미 구현");
        }

        //읽어온 메모리에서 읽어오는 함수.
        public override bool VirGetBit(PlcAddr addr)
        {            //jys:: addr 무조건 int변환. 필요시, 변환 자료형 확인 필요            
            return VirGetInt32(addr).GetBit(addr.Bit);
        }
        public override void VirSetBit(PlcAddr addr, bool value)
        {
            int vv = VirGetInt32(addr).SetBit(addr.Bit, value);
            VirSetInt32(addr, vv);
        }
        public override short VirGetShort(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetShort(PlcAddr addr, short value)
        {
            throw new Exception("미구현 메모리");
        }
        public override string VirGetAscii(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetAscii(PlcAddr addr, string value)
        {
            throw new Exception("미구현 메모리");
        }
        public override int VirGetInt32(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.M)
            {
                return BYTE_M[addr.Addr];
            }
            else if (addr.Type == PlcMemType.S)
            {
                return BYTE_S[addr.Addr];
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetInt32(PlcAddr addr, int value)
        {
            if (addr.Type == PlcMemType.M)
            {
                BYTE_M[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.S)
            {
                BYTE_S[addr.Addr] = value;
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }

        public override bool[] VirGetBits(PlcAddr addr, int wordSize)
        {
            throw new Exception("미구현 메모리");
        }
        public override short[] VirGetShorts(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override float VirGetFloat(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.M)
            {
                return (float)BYTE_M[addr.Addr];
            }
            else if (addr.Type == PlcMemType.S)
            {
                return (float)BYTE_S[addr.Addr];
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetFloat(PlcAddr addr, float value)
        {
            if (addr.Type == PlcMemType.M)
            {
                BYTE_M[addr.Addr] = (int)value;
            }
            else if (addr.Type == PlcMemType.S)
            {
                BYTE_S[addr.Addr] = (int)value;
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }



        public void LogicWorking()
        {
            int[] read = new int[0];

            foreach (PlcAddr addr in LstReadAddr)
                ReadFromPLC(addr, addr.Length);

            foreach (PlcAddr addr in LstWriteAddr)
                WriteToPLC(addr, addr.Length);

            Motors.ForEach(f => f.LogicWorking());
            SettingStep();
        }


        //모터 제어 로직 추가. 
        public List<UmacSvoMotorProxy> Motors = new List<UmacSvoMotorProxy>();

        private int SettingStepNo = 0;
        public void SettingStep()
        {
            //if (SettingStepNo == 0)
            //{
            //    if (YB_PmacValueSave.vBit == true)
            //    {
            //        XB_PmacValueSaveAck.vBit = true;

            //        SettingStepNo = 10;
            //    }
            //}
            //else if (SettingStepNo == 10)
            //{
            //    if (YB_PmacValueSave.vBit == false)
            //    {

            //        for (int jPos = 0; jPos < motors.Count; jPos++)
            //        {

            //            for (int iPos = 1; iPos <= motors[jPos].PositionCount; iPos++)
            //            {
            //                motors[jPos].XF_Position1stPointAck[iPos - 1].vFloat = motors[jPos].YF_Position1stPoint[iPos - 1].vFloat;
            //                motors[jPos].XF_Position1stSpeedAck[iPos - 1].vFloat = motors[jPos].YF_Position1stSpeed[iPos - 1].vFloat;
            //                motors[jPos].XF_Position1stAccelAck[iPos - 1].vFloat = motors[jPos].YF_Position1stAccel[iPos - 1].vFloat;

            //            }
            //        }

            //        XB_PmacValueSaveAck.vBit = false;
            //        SettingStepNo = 20;
            //    }
            //}
            //else if (SettingStepNo == 20)
            //{

            //    SettingStepNo = 30;
            //}
            //else if (SettingStepNo == 30)
            //{
            //    SettingStepNo = 0;

            //}
        }


        private int InterlockCmdStepNo = 0;
        private int AcsResetStepNo = 0;
        public void CommCmdLogic()
        {
            ////Interlock 처리 
            //if (InterlockCmdStepNo == 0)
            //{
            //    if (YB_PinUpMotorInterlockOffCmd.vBit == true)
            //    {
            //        XB_PinUpMotorInterlockOffCmdAck.vBit = true;
            //        XB_PinUpInterlockOff.vBit = true;
            //        InterlockCmdStepNo = 10;
            //    }
            //}
            //else if (InterlockCmdStepNo == 10)
            //{
            //    if (YB_PinUpMotorInterlockOffCmd.vBit == false)
            //    {
            //        XB_PinUpMotorInterlockOffCmdAck.vBit = false;
            //        InterlockCmdStepNo = 20;
            //    }
            //}
            //else if (InterlockCmdStepNo == 20)
            //{
            //    InterlockCmdStepNo = 0;
            //}

            ////Acs Reset Step No 처리 
            //if (AcsResetStepNo == 0)
            //{
            //    if (YB_PmacResetCmd.vBit == true)
            //    {
            //        XB_PmacResetCmdAck.vBit = true;
            //        AcsResetStepNo = 10;
            //    }
            //}
            //else if (AcsResetStepNo == 10)
            //{
            //    if (YB_PmacResetCmd.vBit == false)
            //    {
            //        XB_PmacResetCmdAck.vBit = false;
            //        AcsResetStepNo = 20;
            //    }
            //}
            //else if (InterlockCmdStepNo == 20)
            //{
            //    //ACS RESET 처리. 요함. 

            //    AcsResetStepNo = 30;
            //}
            //else if (InterlockCmdStepNo == 30)
            //{
            //    AcsResetStepNo = 0;
            //}
        }

        public void MoveInterlockLogic()
        {
            //if (XB_PinUpInterlockOff.vBit == false)
            //{
            //    //인터락 조직 추가. 
            //}
        }

        public bool IVariableCommand(int axis, int variable, double value)
        {
            string cmd = string.Format("i{0}{1}-{2}", axis, variable, value);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }
        public bool MotorKill(int axis)
        {
            string cmd = string.Format("#{0}k", axis);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }
        public bool MotorHomming(int axis)
        {
            int M_INIT_MOTOR = 6000;
            int eTO_CMD_1_HOME_START_CMD = 1;

            //string cmd = string.Format("#{0}hm", axis );
            string cmd = string.Format("M{0}=1", M_INIT_MOTOR + eTO_CMD_1_HOME_START_CMD + axis);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }
        public bool MotorHommingCheck(int axis)
        {
            return true;
        }
        public bool IsMotorHommingCompleted(int axis)
        {
            return true;
        }
        public bool JogSpeed(int axis, double speed)
        {
            if (speed < 0)
                speed = 1;

            //string cmd = string.Format("#{0}hm", axis );
            string cmd = string.Format("i{0}22={1}", axis, speed);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }
        public bool JogStop(int axis)
        {
            string cmd = string.Format("#{0}J/", axis);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }


        public bool JogMove(int axis, int array)
        {
            string cmd = array <= 0 ? string.Format("#{0}J-", axis) : string.Format("#{0}J+", axis);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }

        public bool PTPMove(int axis, double position)
        {
            string cmd = string.Format("#{0}J={1}", axis, position);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }
        public bool PTPMoveSpeed(int axis, double position, double speed)
        {
            string cmd = string.Format("i{0}22={1} #{2}J={3}", axis, speed, axis, position);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }
        public bool StepMove(int axis, double distance)
        {
            string cmd = string.Format("#{0}J:{1}", axis, distance);
            string cmdResult = string.Empty;
            return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
        }

    }
}



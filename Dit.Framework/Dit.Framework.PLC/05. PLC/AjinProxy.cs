﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dit.Framework.PLC._05._PLC
{
    public static class AjinLogger
    {
        public static void Log(string str)
        {
            Console.WriteLine(str);
        }
    }
    public static class AjinProxy
    {
        #region INIT
        public static bool Initialize(int dioCnt, int aioCnt, int motorCnt)
        {
            bool result = true;
            //TODO:: 파일경로 변경
            string defaultPath = @"C:\DIT.TLC.CTRL\Exec\Ctrl\Setting\\CurrentAjin.mot";
            try
            {
                bool manualSetting = false;

                result &= InitLibrary();
                result &= InitDIO(dioCnt);
                result &= InitAIO(aioCnt);
                if (manualSetting)
                    result &= InitMotors(motorCnt);
                else
                    result &= LoadFileByAjin(defaultPath);
                //result &= AddAxis();
                return result;
            }
            catch (System.Exception ex)
            {
                return false;
            }
            if (false == result)
                return false;
            return true;
        }
        private static bool InitLibrary()
        {
            int iRet = 0;
            if (CAXL.AxlOpenNoReset(7) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
            {
                AjinLogger.Log("Fail Ajin Board Open1");
                return false;
            }
            if (CAXL.AxlIsOpened() != 1)
            {
                AjinLogger.Log("Fail Ajin Board Open2");
                return false;
            }
            if (CAXL.AxlGetBoardCount(ref iRet) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
            {
                AjinLogger.Log("Ajin Board Count Error1");
                return false;
            }
            if (iRet < 1)
            {
                AjinLogger.Log("Ajin Board Count Error2");
                return false;
            }
            return true;
        }
        public static bool Release()
        {
            CAXL.AxlClose();
            return false;
        }
        private static bool InitAIO(int count)
        {
            uint ret = 0;
            int iRet = 0;
            if (CAXA.AxaInfoIsAIOModule(ref ret) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                || ret != (uint)AXT_EXISTENCE.STATUS_EXIST)
            {
                AjinLogger.Log("No AIO Module");
                return false;
            }

            if (CAXA.AxaInfoGetModuleCount(ref iRet) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                || count != iRet)
            {
                AjinLogger.Log("AIO Count Error");
                return false;
            }

            bool lvlSettingFail = false;

            for (int moduleNo = 0; moduleNo < count; ++moduleNo)
            {
                if (CAXA.AxaInfoGetInputCount(moduleNo, ref iRet) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                    || iRet < 1)
                {
                    AjinLogger.Log(string.Format("{0} Module Analog Input Count 0 Pass Level Setting", moduleNo));
                    lvlSettingFail |= true;
                    break;
                }
            }

            return lvlSettingFail;
        }
        private static bool InitDIO(int count)
        {
            uint ret = 0;
            int iRet = 0;
            if (CAXD.AxdInfoIsDIOModule(ref ret) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                || ret != (uint)AXT_EXISTENCE.STATUS_EXIST)
            {
                AjinLogger.Log("No DIO Module");
                return false;
            }

            if (CAXD.AxdInfoGetModuleCount(ref iRet) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                || count != iRet)
            {
                AjinLogger.Log("DIO Count Error");
                return false;
            }

            bool lvlSettingFail = false;

            for (int moduleNo = 0; moduleNo < count; ++moduleNo)
            {
                if (CAXD.AxdInfoGetInputCount(moduleNo, ref iRet) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                    || iRet < 1)
                {
                    AjinLogger.Log(string.Format("{0} Module Digital Input Count 0 Pass Level Setting", moduleNo));
                    lvlSettingFail |= true;
                    break;
                }
                else
                {
                    if (CAXD.AxdiLevelSetInportDword(moduleNo, 0, 0xFFFFFFFF) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                    {
                        AjinLogger.Log(string.Format("{0} Digital Input Module Level Setting Error", moduleNo));
                        lvlSettingFail |= true;
                        break;
                    }
                }

                if (CAXD.AxdInfoGetOutputCount(moduleNo, ref iRet) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                    || iRet < 1)
                {
                    AjinLogger.Log(string.Format("{0} Module Digital Output Count 0 Pass Level Setting", moduleNo));
                    lvlSettingFail |= true;
                    break;
                }
                else
                {
                    if (CAXD.AxdoLevelSetOutportDword(moduleNo, 0, 0xFFFFFFFF) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                    {
                        AjinLogger.Log(string.Format("{0} Digital Output Module Level Setting Error", moduleNo));
                        lvlSettingFail |= true;
                        break;
                    }
                }
            }

            return lvlSettingFail;
        }
        private static bool InitMotors(int count)
        {
            uint ret = 0;
            int iRet = 0;
            if (CAXM.AxmInfoIsMotionModule(ref ret) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
                || ret != (uint)AXT_EXISTENCE.STATUS_EXIST)
            {
                AjinLogger.Log("No Motion Module");
                return false;
            }

            if (CAXM.AxmInfoGetAxisCount(ref iRet) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
               || count != iRet)
            {
                AjinLogger.Log("Motion Count Error");
                return false;
            }

            return true;
        }
        #endregion


        #region DIO, AIO
        public static bool ReadDIOByModule(PlcAddr addr, out uint retValue)
        {
            uint ret = 0;
            retValue = 0;
            if (addr.Type == PlcMemType.JO)
            {
                //jys :: 추후 스테이션 구분 시 if (CAXD.AxdoReadOutportDword(addr.Station, addr.Addr, ref ret) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                if (CAXD.AxdoReadOutportDword(addr.Addr, 0, ref ret) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    retValue = 0;
                    return false;
                }
                retValue = ret;
            }
            else if (addr.Type == PlcMemType.JI)
            {
                if (CAXD.AxdiReadInportDword(addr.Addr, 0, ref ret) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    retValue = 0;
                    return false;
                }
                retValue = ret;
            }
            return true;
        }
        public static bool WriteDIOByModule(PlcAddr addr, uint writeValue)
        {
            if (addr.Type == PlcMemType.JO)
            {
                if (CAXD.AxdoWriteOutportDword(addr.Addr, 0, writeValue) == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                    return true;
            }
            return false;
        }
        public static bool ReadAIOByModule(PlcAddr addr, out double[] retValue)
        {
            retValue = null;
            if (addr.Type == PlcMemType.JR)
            {
                retValue = new double[16];
                int chStart = addr.Addr * 16;
                double value = 0;
                for (int ch = 0; ch < 16; ch++)
                {
                    if (CAXA.AxaiSwReadVoltage(addr.Addr * 16 + ch, ref value) == (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                        retValue[ch] = value;
                }

                return true;

                //int maxCH = 16;
                //int[] _adChannels = new int[maxCH];
                //uint[] _adInputValue = new uint[maxCH];
                //for (int i = 0; i < _adChannels.Length; ++i)
                //    _adChannels[i] = (addr.Addr * maxCH) + i;

                //retValue = null;
                //if (CAXA.AxaiSwReadMultiDigit(maxCH, _adChannels, retValue) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                //    return false;
            }
            return true;
        }
        #endregion

        #region Motor
        public static bool SaveFileByAjin(string path)
        {
            if (CAXM.AxmMotSaveParaAll(path) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
            {
                AjinLogger.Log("File save failed");
                return false;
            }
            return true;
        }
        public static bool LoadFileByAjin(string path)
        {
            if (CAXM.AxmMotLoadParaAll(path) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
            {
                AjinLogger.Log("File load failed");
                return false;
            }
            return true;
        }

        public static bool IsServoSettingAbnormal(AjinMotorSetting setting)
        {
            AjinMotorSetting a = new AjinMotorSetting();
            a = (AjinMotorSetting)setting.Clone();
            return GetAppliedSetting(ref a);
        }
        private static bool IsRetAbnormal(uint retVal)
        {
            return retVal != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS
            && retVal != (uint)AXT_FUNC_RESULT.AXT_RT_NOT_SUPPORT_VERSION
            ;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnOrCompare"></param>
        /// <returns>true = error</returns>
        public static bool GetAppliedSetting(ref AjinMotorSetting setting)
        {
            string errStr = "";
            int idx = 0;

            uint ret = 0;

            uint temp1, temp2, temp3, temp4;
            temp1 = temp2 = temp3 = temp4 = 0;
            double dTemp1, dTemp2, dTemp3, dTemp4, dTemp5, dTemp6;
            dTemp1 = dTemp2 = dTemp3 = dTemp4 = dTemp5 = dTemp6 = 0;
            int iTemp1 = 0;

            //네트워크 타입 미사용 - 아진 문의
            //ret = CAXM.AxmMotGetPulseOutMethod(setting.AxisNo, ref temp1);
            //if (IsRetAbnormal(ret) == true) funcRet |= true;
            //if ((AXT_MOTION_PULSE_OUTPUT)temp1 != setting.PulseOutMethod) funcRet |= true;

            //네트워크 타입 미사용 - 아진 문의
            //ret = CAXM.AxmMotGetEncInputMethod(setting.AxisNo, ref temp1);
            //if (IsRetAbnormal(ret) == true) funcRet |= true;
            //if ((AXT_MOTION_EXTERNAL_COUNTER_INPUT)temp1 != setting.EncoderMethod) funcRet |= true;

            ret = CAXM.AxmSignalGetInpos(setting.AxisNo, ref temp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((AXT_MOTION_LEVEL_MODE)temp1 != setting.UseInposition) errStr += string.Format("{0}diff\r\n", idx++);
            setting.UseInposition = (AXT_MOTION_LEVEL_MODE)temp1;

            ret = CAXM.AxmSignalGetServoAlarm(setting.AxisNo, ref temp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((AXT_MOTION_LEVEL_MODE)temp1 != setting.AlarmStopSignal) errStr += string.Format("{0}diff\r\n", idx++);
            setting.AlarmStopSignal = (AXT_MOTION_LEVEL_MODE)temp1;

            ret = CAXM.AxmSignalGetLimit(setting.AxisNo, ref temp1, ref temp2, ref temp3);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            //if ((AXT_MOTION_LEVEL_MODE)temp2 != setting.StopMode) errStr += string.Format("{0}diff\r\n", idx++); 밑에서 확인
            if ((AXT_MOTION_LEVEL_MODE)temp2 != setting.NegLimitType) errStr += string.Format("{0}diff\r\n", idx++);
            if ((AXT_MOTION_LEVEL_MODE)temp3 != setting.PosLimitType) errStr += string.Format("{0}diff\r\n", idx++);
            setting.NegLimitType = (AXT_MOTION_LEVEL_MODE)temp2;
            setting.PosLimitType = (AXT_MOTION_LEVEL_MODE)temp3;

            ret = CAXM.AxmMotGetMinVel(setting.AxisNo, ref dTemp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if (Math.Abs(dTemp1 - setting.MinSpeed) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            setting.MinSpeed = dTemp1;

            ret = CAXM.AxmMotGetMaxVel(setting.AxisNo, ref dTemp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if (Math.Abs(dTemp1 - setting.MaxSpeed) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            setting.MaxSpeed = dTemp1;

            ret = CAXM.AxmHomeGetMethod(setting.AxisNo, ref iTemp1, ref temp2, ref temp3, ref dTemp1, ref dTemp2);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((AXT_MOTION_HOME_DETECT)temp2 != setting.HomeSignalSensor) errStr += string.Format("{0}diff\r\n", idx++);
            if ((AXT_MOTION_MOVE_DIR)iTemp1 != setting.HomeDirection) errStr += string.Format("{0}diff\r\n", idx++);
            if (temp3 != setting.UseFindZPhaseType) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp1 - setting.HomeClearTime) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp2 - setting.HomeOffset) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            setting.HomeSignalSensor = (AXT_MOTION_HOME_DETECT)temp2;
            setting.HomeDirection = (AXT_MOTION_MOVE_DIR)iTemp1;
            setting.UseFindZPhaseType = temp3;
            setting.HomeClearTime = dTemp1;
            setting.HomeOffset = dTemp2;

            ret = CAXM.AxmHomeGetSignalLevel(setting.AxisNo, ref temp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((temp1 == 1 ? true : false) != setting.IsHomeSignalNormalOpen) errStr += string.Format("{0}diff\r\n", idx++);
            setting.IsHomeSignalNormalOpen = (temp1 == 1 ? true : false);

            ret = CAXM.AxmSignalGetZphaseLevel(setting.AxisNo, ref temp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((temp1 == 1 ? true : false) != setting.IsZPhaseNormalOpen) errStr += string.Format("{0}diff\r\n", idx++);
            setting.IsZPhaseNormalOpen = (temp1 == 1 ? true : false);

            ret = CAXM.AxmSignalGetStop(setting.AxisNo, ref temp1, ref temp2);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((AXT_MOTION_STOPMODE)temp1 != setting.StopMode) errStr += string.Format("{0}diff\r\n", idx++);
            if ((AXT_MOTION_LEVEL_MODE)temp2 != setting.StopModeLevel) errStr += string.Format("{0}diff\r\n", idx++);
            setting.StopMode = (AXT_MOTION_STOPMODE)temp1;
            setting.StopModeLevel = (AXT_MOTION_LEVEL_MODE)temp2;

            ret = CAXM.AxmHomeGetVel(setting.AxisNo, ref dTemp1, ref dTemp2, ref dTemp3, ref dTemp4, ref dTemp5, ref dTemp6);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if (Math.Abs(dTemp1 - setting.HomeVel1) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp2 - setting.HomeVel2) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp3 - setting.HomeVel3) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp4 - setting.HomeVelLast) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp5 - setting.HomeAcc1) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp6 - setting.HomeAcc2) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            setting.HomeVel1 = dTemp1;
            setting.HomeVel2 = dTemp2;
            setting.HomeVel3 = dTemp3;
            setting.HomeVelLast = dTemp4;
            setting.HomeAcc1 = dTemp5;
            setting.HomeAcc2 = dTemp6;


            ret = CAXM.AxmSignalGetSoftLimit(setting.AxisNo, ref temp1, ref temp2, ref temp3, ref dTemp1, ref dTemp2);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if (Math.Abs(dTemp1 - setting.SoftLimitPos) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp2 - setting.SoftLimitNeg) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if ((AXT_MOTION_SELECTION)temp3 != setting.SoftLimitSource) errStr += string.Format("{0}diff\r\n", idx++);
            if ((AXT_MOTION_STOPMODE)temp2 != setting.SoftLimitStopMode) errStr += string.Format("{0}diff\r\n", idx++);
            if ((AXT_USE)temp1 != setting.IsUseSoftLimit) errStr += string.Format("{0}diff\r\n", idx++);
            setting.SoftLimitNeg = dTemp1;
            setting.SoftLimitPos = dTemp2;
            setting.SoftLimitSource = (AXT_MOTION_SELECTION)temp3;
            setting.SoftLimitStopMode = (AXT_MOTION_STOPMODE)temp2;
            setting.IsUseSoftLimit = (AXT_USE)temp1;

            ret = CAXM.AxmMotGetMoveUnitPerPulse(setting.AxisNo, ref dTemp1, ref iTemp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if (iTemp1 != setting.Pulse) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp1 - setting.Unit) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            setting.Pulse = iTemp1;
            setting.Unit = dTemp1;

            ret = CAXM.AxmMotGetParaLoad(setting.AxisNo, ref dTemp1, ref dTemp2, ref dTemp3, ref dTemp4);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if (Math.Abs(dTemp1 - setting.InitPos) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp2 - setting.InitSpeed) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp3 - setting.InitAcc) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            if (Math.Abs(dTemp4 - setting.InitDec) >= 1) errStr += string.Format("{0}diff\r\n", idx++);
            setting.InitPos = dTemp1;
            setting.InitSpeed = dTemp2;
            setting.InitAcc = dTemp3;
            setting.InitDec = dTemp4;

            ret = CAXM.AxmMotGetAbsRelMode(setting.AxisNo, ref temp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((AXT_MOTION_ABSREL)temp1 != setting.AbsRelMode) errStr += string.Format("{0}diff\r\n", idx++);
            setting.AbsRelMode = (AXT_MOTION_ABSREL)temp1;

            ret = CAXM.AxmMotGetProfileMode(setting.AxisNo, ref temp1);
            if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            if ((AXT_MOTION_PROFILE_MODE)temp1 != setting.ProfileMode) errStr += string.Format("{0}diff\r\n", idx++);
            setting.ProfileMode = (AXT_MOTION_PROFILE_MODE)temp1;

            return errStr.Length > 0;
            //AxmSignalGetServoAlarmResetLevel
            //AxmSignalGetServoOnLevel
        }
        public static bool SetMotorParameter(AjinMotorSetting setting)
        {
            string errStr = "";
            int idx = 0;

            uint ret = 0;

            uint temp1, temp2, temp3, temp4;
            temp1 = temp2 = temp3 = temp4 = 0;
            double dTemp1, dTemp2, dTemp3, dTemp4, dTemp5, dTemp6;
            dTemp1 = dTemp2 = dTemp3 = dTemp4 = dTemp5 = dTemp6 = 0;
            int iTemp1 = 0;


            //네트워크 타입 미사용 - 아진 문의
            //ret = CAXM.AxmMotSetPulseOutMethod(setting.AxisNo, ref temp1);
            //if (IsRetAbnormal(ret) == true) funcRet |= true;
            //if ((AXT_MOTION_PULSE_OUTPUT)temp1 != setting.PulseOutMethod) funcRet |= true;

            //네트워크 타입 미사용 - 아진 문의
            //ret = CAXM.AxmMotSetEncInputMethod(setting.AxisNo, ref temp1);
            //if (IsRetAbnormal(ret) == true) funcRet |= true;
            //if ((AXT_MOTION_EXTERNAL_COUNTER_INPUT)temp1 != setting.EncoderMethod) funcRet |= true;

            ret = CAXM.AxmSignalSetInpos(setting.AxisNo, (uint)setting.UseInposition); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmSignalSetServoAlarm(setting.AxisNo, (uint)setting.AlarmStopSignal); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmSignalSetLimit(setting.AxisNo, (uint)setting.StopMode, (uint)setting.NegLimitType, (uint)setting.PosLimitType); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmMotSetMinVel(setting.AxisNo, setting.MinSpeed); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmMotSetMaxVel(setting.AxisNo, setting.MaxSpeed); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmHomeSetMethod(setting.AxisNo, (int)setting.HomeDirection, (uint)setting.HomeSignalSensor, setting.UseFindZPhaseType, setting.HomeClearTime, setting.HomeOffset); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmHomeSetSignalLevel(setting.AxisNo, setting.IsHomeSignalNormalOpen ? 1u : 0); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmSignalSetZphaseLevel(setting.AxisNo, setting.IsZPhaseNormalOpen ? 1u : 0); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmSignalSetStop(setting.AxisNo, (uint)setting.StopMode, (uint)setting.StopModeLevel); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmHomeSetVel(setting.AxisNo, setting.HomeVel1, setting.HomeVel2, setting.HomeVel3, setting.HomeVelLast, setting.HomeAcc1, setting.HomeAcc2); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmSignalSetSoftLimit(setting.AxisNo, (uint)setting.IsUseSoftLimit, (uint)setting.SoftLimitStopMode, (uint)setting.SoftLimitSource, setting.SoftLimitPos, setting.SoftLimitNeg); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmMotSetMoveUnitPerPulse(setting.AxisNo, setting.Unit, setting.Pulse); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmMotSetParaLoad(setting.AxisNo, setting.InitPos, setting.InitSpeed, setting.InitAcc, setting.InitDec); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmMotSetAbsRelMode(setting.AxisNo, (uint)setting.AbsRelMode); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());
            ret = CAXM.AxmMotSetProfileMode(setting.AxisNo, (uint)setting.ProfileMode); if (IsRetAbnormal(ret) == true) errStr += string.Format("{0}-{1}\r\n", idx++, ((AXT_FUNC_RESULT)ret).ToString());

            return errStr.Length > 0 || GetAppliedSetting(ref setting);
        }

        public static uint GetDriveStatus(int axisNo)
        {
            uint temp = 0;

            if (CAXM.AxmStatusReadMotion(axisNo, ref temp) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return 0;

            return temp;
        }

        public static uint GetMotionEndStatus(int axisNo)
        {
            uint temp = 0;

            if (CAXM.AxmStatusReadStop(axisNo, ref temp) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return 0;

            return temp;
        }
        public static uint GetMechanicalStatus(int axisNo)
        {
            uint temp = 0;

            if (CAXM.AxmStatusReadMechanical(axisNo, ref temp) != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                return 0;

            return temp;
        }


        #endregion
    }
}

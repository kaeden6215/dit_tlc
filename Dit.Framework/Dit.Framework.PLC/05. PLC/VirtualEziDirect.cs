using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Dit.Framework.Comm;
using Dit.Framework.PLC._05._PLC;
using EziNetworkModule;

namespace Dit.Framework.PLC
{

    public enum EmEziParam
    {
        AxisNo,
        PulsePerRevolution,
        AxisMaxSpeed,
        AxisStartSpeed,
        AxisAccTime,
        AxisDecTime,
        SpeedOverride,
        JogSpeed,
        JogStartSpeed,
        JogAccDecTime,
        SWLimitPlusValue,
        SWLimitMinusValue,
        SWLimitStopMethod,
        HWLimitStopMethod,
        LimitSensorLogic,
        OrgSpeed,
        OrgSearchSpeed,
        OrgAccDecTime,
        OrgMethod,
        OrgDir,
        OrgOffset,
        OrgPositionSet,
        OrgSensorLogic,
        PositionLoopGain,
        InposValue,
        PosTrackingLimit,
        MotionDir,
        LimitSensorDir,
        OrgTorqueRatio,
        PosErrorOverflowLimit,
        BrakeDelayTime,
        RunCurrent,
        BoostCurrent,
        StopCurrent
    }
    public class EziSvoMotorProxy
    {
        private int _homeInpos = 10;
        private int _ptpInpos = 10;

        public VirtualEziDirect Ezi { get; set; }
        public EziMotorSetting MotorSetting { get; set; }
        public string Name { get; set; }
        public string SlayerName { get; set; }
        public int AxisNo { get; set; }
        public int RotaryNo { get; set; }

        /// <summary>
        /// ServoMotorControl에서 가져올 때 곱해지고, ServoMotorControl로 전달할 때 나눔
        /// </summary>
        public float CtrllerToPcPositionScale = 1;

        public PlcAddr XI_CurrMotorStatus1 { get; set; }
        public PlcAddr XB_StatusHomeCompleteBit { get; set; }
        public PlcAddr XB_StatusHomeInPosition { get; set; }
        public PlcAddr XB_StatusMotorMoving { get; set; }
        public PlcAddr XB_StatusMotorInPosition { get; set; }
        public PlcAddr XB_StatusNegativeLimitSet { get; set; }
        public PlcAddr XB_StatusPositiveLimitSet { get; set; }
        public PlcAddr XB_StatusMotorServoOn { get; set; }
        public PlcAddr XB_ErrFatalFollowingError { get; set; }
        public PlcAddr XB_ErrAmpFaultError { get; set; }
        public PlcAddr XB_ErrI2TAmpFaultError { get; set; }

        public PlcAddr XF_CurrMotorPosition { get; set; }
        public PlcAddr XF_CurrMotorSpeed { get; set; }
        public PlcAddr XF_CurrMotorActualLoad { get; set; }
        public PlcAddr XF_CurrMotorLow { get; set; }
        public PlcAddr XF_CurrMotorHight { get; set; }
        public PlcAddr XF_CurrMotorError { get; set; }




        //서보 OnOff Cmd
        public PlcAddr YB_ServoOnOffCmd { get; set; }
        public PlcAddr YB_ServoOnOff { get; set; }
        public PlcAddr XB_ServoOnOffCmdAck { get; set; }
        //서보 Motor Stop
        public PlcAddr YB_MotorStopCmd { get; set; }
        public PlcAddr XB_MotorStopCmdAck { get; set; }


        //Home Cmd
        public PlcAddr YB_HomeCmd { get; set; }
        public PlcAddr XB_HomeCmdAck { get; set; }

        //Jog Command
        public PlcAddr YB_MotorJogMinusMove { get; set; }
        public PlcAddr YB_MotorJogPlusMove { get; set; }
        public PlcAddr YF_MotorJogSpeedCmd { get; set; }
        public PlcAddr YF_MotorJogSpeedCmdAck { get; set; }


        //Point To Point Move Cmd
        public PlcAddr YB_PTPMoveCmd { get; set; }
        public PlcAddr YB_PTPMoveCmdCancel { get; set; }
        public PlcAddr XB_PTPMoveCmdAck { get; set; }
        public PlcAddr YF_PTPMoveSpeed { get; set; }
        public PlcAddr XF_PTPMoveSpeedAck { get; set; }
        public PlcAddr YF_PTPMovePosition { get; set; }
        public PlcAddr XF_PTPMovePositionAck { get; set; }
        public PlcAddr YF_PTPMoveAccel { get; set; }
        public PlcAddr XF_PTPMoveAccelAck { get; set; }

        public PlcAddr XB_PTPMoveComplete { get; set; }

        public double JogSpeed { get; set; }
        public PlcAddr XI_CurrMotorLow { get; set; }
        public PlcAddr XI_CurrMotorHight { get; set; }

        public EziSvoMotorProxy()
        {
            YB_PTPMoveCmd = null;
            XB_PTPMoveCmdAck = null;


            YF_PTPMoveSpeed = null;
            XF_PTPMoveSpeedAck = null;

            XB_PTPMoveComplete = null;

        }

        public void LogicWorking()
        {
            if (Ezi.GetStatus(this.RotaryNo, out _dwInStatus, out _dwOutStatus, out _dwAxisStatus,
                out _cmdPos, out _curPos, out _posErr, out _curSpeed, out _wPosItemNo))
            {
                StatusLogic();
                JogStepLogic();
                HomeStepLogic();
                PTPMoveLogic();
                ServoOnOffLogic();
            }
            else
            {
                XB_StatusMotorServoOn.vBit = false;
            }
            MotorStopLogic();
        }

        private uint _dwInStatus;
        private uint _dwOutStatus;
        private uint _dwAxisStatus;
        private int _cmdPos;
        private int _curPos;
        private int _posErr;
        private int _curSpeed;
        private ushort _wPosItemNo;

        private void StatusLogic()
        {
            XI_CurrMotorLow.vInt = (int)EziProxy.GetStatus(this.RotaryNo);

            XF_CurrMotorSpeed.vFloat = _curSpeed / CtrllerToPcPositionScale;
            XF_CurrMotorPosition.vFloat = _curPos;
            //XF_CurrMotorActualLoad        .vFloat   = XF_CurrMotorActualLoad                 

            XB_StatusMotorServoOn.vBit = Ezi.IsServoOn(_dwAxisStatus);
            XB_StatusMotorMoving.vBit = Ezi.IsMoving(_dwAxisStatus);

            XB_StatusNegativeLimitSet.vBit = Ezi.IsNegLimit(_dwAxisStatus);
            XB_StatusPositiveLimitSet.vBit = Ezi.IsPosLimit(_dwAxisStatus);

            XB_StatusHomeInPosition.vBit = XB_StatusMotorServoOn.vBit
                && XB_StatusHomeCompleteBit.vBit
                && Math.Abs(_curPos - 0) < _homeInpos
                ;
            XB_StatusMotorInPosition.vBit = XB_StatusMotorServoOn.vBit
                && Ezi.IsInpos(_dwAxisStatus);

            //XB_ErrI2TAmpFaultError           = XB_ErrI2TAmpFaultError                 
            //XB_ErrFatalFollowingError        = XB_ErrFatalFollowingError              
            //XB_ErrAmpFaultError              = XB_ErrAmpFaultError                    
        }

        private int _motorStopTimeoverStd = 10000;
        private Stopwatch _motorStopTimeoverCheck = new Stopwatch();
        public int SetpMotorStop = 0;
        public void MotorStopLogic()
        {
            if (_motorStopTimeoverCheck.ElapsedMilliseconds > _motorStopTimeoverStd)
            {
                _motorStopTimeoverCheck.Reset();
                SetpMotorStop = 0;
            }

            if (SetpMotorStop == 0)
            {
                if (YB_MotorStopCmd.vBit)
                    SetpMotorStop = 10;
            }
            else if (SetpMotorStop == 10)
            {
                XB_MotorStopCmdAck.vBit = true;

                //bool servoOnOff = YB_ServoOnOff.vBit;
                //if (servoOnOff)
                //{
                Ezi.EmergencyStop(this.RotaryNo);
                //}
                //else
                //{
                //    Ezi.MotorKill(AxisNo);
                //}
                SetpMotorStop = 20;
            }
            else if (SetpMotorStop == 20)
            {
                if (YB_MotorStopCmd.vBit == false)
                {
                    XB_MotorStopCmdAck.vBit = false;
                    SetpMotorStop = 0;
                }
            }
        }

        public void StepMoveLogic()
        {
        }

        private int _servoOnOffTimeoverStd = 10000;
        private Stopwatch _servoOnOffTimeoverCheck = new Stopwatch();
        public int SetpServoOnOff = 0;
        public void ServoOnOffLogic()
        {
            if (_servoOnOffTimeoverCheck.ElapsedMilliseconds > _servoOnOffTimeoverStd)
            {
                _servoOnOffTimeoverCheck.Reset();
                SetpMotorStop = 0;
            }

            if (SetpServoOnOff == 0)
            {
                if (YB_ServoOnOffCmd.vBit)
                    SetpServoOnOff = 10;
            }
            else if (SetpServoOnOff == 10)
            {
                XB_ServoOnOffCmdAck.vBit = true;

                bool servoOnOff = YB_ServoOnOff.vBit;
                if (servoOnOff)
                {
                    Ezi.MotorOn(this.RotaryNo);
                }
                else
                {
                    Ezi.MotorKill(this.RotaryNo);
                }
                SetpServoOnOff = 20;
            }
            else if (SetpServoOnOff == 20)
            {
                if (YB_ServoOnOffCmd.vBit == false)
                {
                    XB_ServoOnOffCmdAck.vBit = false;
                    SetpServoOnOff = 0;
                }
            }
        }


        private int _homeTimeoverStd = 120000;
        private Stopwatch _homeTimeoverCheck = new Stopwatch();
        private int HomeStepNo = 0;
        private PlcTimer _plcTmrHome = new PlcTimer("Home Seq Timer");
        public void HomeStepLogic()
        {
            if (_homeTimeoverCheck.ElapsedMilliseconds > _homeTimeoverStd)
            {
                _homeTimeoverCheck.Reset();
                HomeStepNo = 0;
            }

            if (HomeStepNo == 0)
            {
                if (YB_HomeCmd.vBit == true)
                {
                    XB_HomeCmdAck.vBit = true;
                    HomeStepNo = 10;
                }
            }
            else if (HomeStepNo == 10)
            {
                if (YB_HomeCmd.vBit == false)
                {
                    XB_HomeCmdAck.vBit = false;
                    HomeStepNo = 20;
                }
            }
            else if (HomeStepNo == 20)
            {
                if (Ezi.JogStop(this.RotaryNo) == false)
                {
                    EziLogger.Log(string.Format("fail {0} home stop", this.RotaryNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 25;
            }

            else if (HomeStepNo == 25)
            {
                if (this.XB_StatusMotorServoOn.vBit == true)
                    HomeStepNo = 40;
                else if (Ezi.MotorKill(this.RotaryNo) == false)
                {
                    EziLogger.Log(string.Format("fail {0} home motor kill", this.RotaryNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 30;
            }
            else if (HomeStepNo == 30)
            {
                if (Ezi.ErrorReset(this.RotaryNo) == false)
                {
                    EziLogger.Log(string.Format("fail {0} home motor error reset", this.RotaryNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 35;
            }
            else if (HomeStepNo == 35)
            {
                if (Ezi.MotorOn(this.RotaryNo) == false)
                {
                    EziLogger.Log(string.Format("fail {0} home motor on", this.RotaryNo));
                    HomeStepNo = 0;
                }
                else
                    HomeStepNo = 40;
            }
            else if (HomeStepNo == 40)
            {
                if (Ezi.MotorHommingSetting(this.RotaryNo, 5000, 1000, 50) == false)
                {
                    EziLogger.Log(string.Format("fail {0} home setting", this.RotaryNo));
                    HomeStepNo = 0;
                }
                else
                {
                    _plcTmrHome.Start(0, 1000);
                    HomeStepNo = 50;
                }
            }
            else if (HomeStepNo == 50)
            {
                if (_plcTmrHome)
                {
                    _plcTmrHome.Stop();
                    if (Ezi.MotorHomming(this.RotaryNo) == false)
                    {
                        EziLogger.Log(string.Format("fail {0} start home", this.RotaryNo));
                        HomeStepNo = 0;
                    }
                    else
                        HomeStepNo = 60;
                }
            }
            else if (HomeStepNo == 60)
            {
                uint rate = 0;
                Ezi.GetHomeRate(this.RotaryNo, out rate);
                if (rate == 100)
                {
                    HomeStepNo = 70;
                }
            }
            else if (HomeStepNo == 70)
            {
                if (Ezi.IsMotorHommingCompleted(_dwAxisStatus) == true)
                    HomeStepNo = 80;
            }
            else if (HomeStepNo == 80)
            {
                //jys:: pos clear 필요하면 할 것.
                //                 if (Ezi.PositionClear(this.RotaryNo) == false)
                //                 {
                //                     EziLogger.Log(string.Format("fail {0} home position clear", this.RotaryNo));
                //                     HomeStepNo = 0;
                //                 }
                //                 else
                {
                    XB_StatusHomeCompleteBit.vBit = true;
                    HomeStepNo = 0;
                }
            }
        }

        private int _ptpTimeoverStd = 120000;
        private Stopwatch _ptpTimeoverCheck = new Stopwatch();
        private int PTPMoveStepNo = 0;
        public void PTPMoveLogic()
        {
            if (_ptpTimeoverCheck.ElapsedMilliseconds > _ptpTimeoverStd)
            {
                _ptpTimeoverCheck.Reset();
                PTPMoveStepNo = 0;
            }

            if (PTPMoveStepNo == 0)
            {
                if (YB_PTPMoveCmd.vBit == true)
                {
                    XF_PTPMovePositionAck.vFloat = YF_PTPMovePosition.vFloat;
                    XF_PTPMoveSpeedAck.vFloat = YF_PTPMoveSpeed.vFloat;
                    XF_PTPMoveAccelAck.vFloat = YF_PTPMoveAccel.vFloat;

                    _ptpTimeoverStd = (int)(Math.Abs(YF_PTPMovePosition.vFloat - XF_CurrMotorPosition.vFloat) / YF_PTPMoveSpeed.vFloat);
                    _ptpTimeoverStd *= 1500;
                    _ptpTimeoverStd += 3000;

                    XB_PTPMoveCmdAck.vBit = true;
                    PTPMoveStepNo = 10;
                }
            }
            else if (PTPMoveStepNo == 10)
            {
                if (YB_PTPMoveCmd.vBit == false)
                {
                    XB_PTPMoveCmdAck.vBit = false;
                    PTPMoveStepNo = 20;
                }
            }
            else if (PTPMoveStepNo == 20)
            {
                Ezi.PTPMoveSet(this.RotaryNo);
                PTPMoveStepNo = 25;
            }
            else if (PTPMoveStepNo == 25)
            {
                if (XF_PTPMoveSpeedAck.vFloat < 0)
                {
                    EziLogger.Log("PTP Move Speed Error");
                    PTPMoveStepNo = 0;
                }
                else
                {
                    Ezi.PTPMoveSpeed(this.RotaryNo, (int)XF_PTPMovePositionAck.vFloat, (uint)XF_PTPMoveSpeedAck.vFloat * (uint)CtrllerToPcPositionScale);
                    PTPMoveStepNo = 30;
                }
            }
            else if (PTPMoveStepNo == 30)
            {
                if (XB_StatusMotorInPosition.vBit == true)
                {
                    PTPMoveStepNo = 40;
                }
            }
            else if (PTPMoveStepNo == 40)
            {
                if (/*XB_PTPMoveComplete.vBit == true && */XB_StatusMotorInPosition.vBit == true)
                {
                    PTPMoveStepNo = 50;
                }
            }
            else if (PTPMoveStepNo == 50)
            {
                PTPMoveStepNo = 0;
            }
        }

        private int JogPlusStep = 0;
        private int JogMinusStep = 0;
        public void JogStepLogic()
        {
            JogSpeed = YF_MotorJogSpeedCmd.vFloat;
            YF_MotorJogSpeedCmdAck.vFloat = YF_MotorJogSpeedCmd.vFloat;

            if (JogPlusStep == 0)
            {
                if (YB_MotorJogPlusMove.vBit == true)
                {
                    Ezi.JogMove(this.RotaryNo, JogSpeed, 1);
                    JogPlusStep = 10;
                }
            }
            else if (JogPlusStep == 10)
            {
                if (YB_MotorJogPlusMove.vBit == false)
                {
                    Ezi.JogStop(this.RotaryNo);
                    JogPlusStep = 0;
                }
            }

            if (JogMinusStep == 0)
            {
                if (YB_MotorJogMinusMove.vBit == true)
                {
                    Ezi.JogMove(this.RotaryNo, JogSpeed, -1);
                    JogMinusStep = 10;
                }
            }
            else if (JogMinusStep == 10)
            {
                if (YB_MotorJogMinusMove.vBit == false)
                {
                    Ezi.JogStop(this.RotaryNo);
                    JogMinusStep = 0;
                }
            }
        }

    }
    public class EziSetting
    {
        public int[] MotorRotaryNo { get; set; }
        public List<EziMotorSetting> MotorSetting { get; set; }
    }
    public class EziMotorSetting : ICloneable
    {
        public int AxisNo;
        public int PulsePerRevolution;
        public int AxisMaxSpeed;
        public int AxisStartSpeed;
        public int AxisAccTime;
        public int AxisDecTime;
        public int SpeedOverride;
        public int JogSpeed;
        public int JogStartSpeed;
        public int JogAccDecTime;
        public int SWLimitPlusValue;
        public int SWLimitMinusValue;
        public int SWLimitStopMethod;
        public int HWLimitStopMethod;
        public int LimitSensorLogic;
        public int OrgSpeed;
        public int OrgSearchSpeed;
        public int OrgAccDecTime;
        public int OrgMethod;
        public int OrgDir;
        public int OrgOffSet;
        public int OrgPositionSet;
        public int OrgSensorLogic;
        public int PositionLoopGain;
        public int InposValue;
        public int PosTrackingLimit;
        public int MotionDir;
        public int LimitSensorDir;
        public int OrgTorqueRatio;
        public int PosErrorOverflowLimit;
        public int BrakeDelayTime;
        public int RunCurrent;
        public int BoostCurrent;
        public int StopCurrent;

        public object Clone()
        {
            EziMotorSetting clone = new EziMotorSetting();

            clone.AxisNo = this.AxisNo;
            clone.PulsePerRevolution = this.PulsePerRevolution;
            clone.AxisMaxSpeed = this.AxisMaxSpeed;
            clone.AxisStartSpeed = this.AxisStartSpeed;
            clone.AxisAccTime = this.AxisAccTime;
            clone.AxisDecTime = this.AxisDecTime;
            clone.SpeedOverride = this.SpeedOverride;
            clone.JogSpeed = this.JogSpeed;
            clone.JogStartSpeed = this.JogStartSpeed;
            clone.JogAccDecTime = this.JogAccDecTime;
            clone.SWLimitPlusValue = this.SWLimitPlusValue;
            clone.SWLimitMinusValue = this.SWLimitMinusValue;
            clone.SWLimitStopMethod = this.SWLimitStopMethod;
            clone.HWLimitStopMethod = this.HWLimitStopMethod;
            clone.LimitSensorLogic = this.LimitSensorLogic;
            clone.OrgSpeed = this.OrgSpeed;
            clone.OrgSearchSpeed = this.OrgSearchSpeed;
            clone.OrgAccDecTime = this.OrgAccDecTime;
            clone.OrgMethod = this.OrgMethod;
            clone.OrgDir = this.OrgDir;
            clone.OrgOffSet = this.OrgOffSet;
            clone.OrgPositionSet = this.OrgPositionSet;
            clone.OrgSensorLogic = this.OrgSensorLogic;
            clone.PositionLoopGain = this.PositionLoopGain;
            clone.InposValue = this.InposValue;
            clone.PosTrackingLimit = this.PosTrackingLimit;
            clone.MotionDir = this.MotionDir;
            clone.LimitSensorDir = this.LimitSensorDir;
            clone.OrgTorqueRatio = this.OrgTorqueRatio;
            clone.PosErrorOverflowLimit = this.PosErrorOverflowLimit;
            clone.BrakeDelayTime = this.BrakeDelayTime;
            clone.RunCurrent = this.RunCurrent;
            clone.BoostCurrent = this.BoostCurrent;
            clone.StopCurrent = this.StopCurrent;

            return clone;
        }
    }
    public class VirtualEziDirect : IVirtualMem
    {

        private const int MEM_SIZE = 102400;
        public int[] BYTE_EM = new int[MEM_SIZE];
        public int[] BYTE_ES = new int[MEM_SIZE];

        private string _ip = string.Empty;
        private int _port = 0;

        private UInt32 _dwDevice;
        private Int32 _bDriverOpen;

        private bool _isRunning = false;

        private Thread _eziWorker = null;


        public List<PlcAddr> LstReadAddr { get; set; }
        public List<PlcAddr> LstWriteAddr { get; set; }

        public double CurrWorkingTime { get; set; }
        public double MaxWorkingTime { get; set; }

        public EziSetting BoardSetting { get; set; }

        public VirtualEziDirect(string name, string headIp, int port, EziSetting setting)
        {
            LstReadAddr = new List<PlcAddr>();
            LstWriteAddr = new List<PlcAddr>();

            BoardSetting = setting;
            _ip = headIp;
            _port = port;

            _eziWorker = new Thread(new ThreadStart(EziWorker_DoWork));
            _eziWorker.Priority = ThreadPriority.Highest;
        }

        private bool _aliveSignal = false;
        private int _aliveStd = 1000;
        public bool AliveSignal
        {
            get
            {
                lock (this)
                {
                    return _aliveSignal;
                }
            }
        }
        private Stopwatch _aliveCheck = new Stopwatch();
        private void EziWorker_DoWork()
        {
            DateTime start, end;
            while (_isRunning)
            {
                start = DateTime.Now;

                LogicWorking();

                CurrWorkingTime = (DateTime.Now - start).TotalMilliseconds;
                if (MaxWorkingTime < CurrWorkingTime)
                    MaxWorkingTime = CurrWorkingTime;

                if (_aliveCheck.IsRunning == false)
                    _aliveCheck.Restart();

                if (_aliveCheck.ElapsedMilliseconds > _aliveStd)
                {
                    _aliveSignal = !_aliveSignal;
                    _aliveCheck.Restart();
                }
            }
        }

        //메소드 연결
        public override int Open()
        {
            try
            {
                if (EziProxy.Connect(_ip, BoardSetting.MotorRotaryNo) != (int)Em_Mt_Er.OK)
                    return FALSE;

                if (EziProxy.Initialize(BoardSetting.MotorRotaryNo) != (int)Em_Mt_Er.OK)
                    return FALSE;

                _isRunning = true;
                _eziWorker.Start();
                return TRUE;
            }
            catch (Exception ex)
            {
                return FALSE;
            }
        }
        public override int Close()
        {
            try
            {
                _isRunning = false;

                _eziWorker.Join();

                //ACSChannel.CloseComm();

                return TRUE;
            }
            catch
            {
                return FALSE;
            }
        }

        //메소드 동기화
        public override int ReadFromPLC(PlcAddr addr, int wordSize)
        {
            //          switch (addr.Type)
            //             {
            //                 case PlcMemType.JI:
            //                     uint retValue;
            //                     if (AjinProxy.ReadDIOByModule(addr, out retValue) == false)
            //                         return (int)Em_Mt_Er.Failed;
            //                     addr.vInt = (int)retValue;
            //                     break;
            //                 case PlcMemType.AI:
            //                     uint[] tempUints = null;
            //                     if (AjinProxy.ReadAIOByModule(addr, out tempUints) == false)
            //                         return (int)Em_Mt_Er.Failed;
            //                     if (tempUints != null
            //                         && tempUints.Length > 0)
            //                     {
            //                         foreach (uint val in tempUints)
            //                             BYTE_JR[addr.Addr] = (int)val;
            //                     }
            //                     else
            //                         return (int)Em_Mt_Er.Failed;
            //                     break;
            //             }
            return (int)Em_Mt_Er.OK;
        }
        public override int WriteToPLC(PlcAddr addr, int wordSize)
        {
            //             switch (addr.Type)
            //             {
            //                 case PlcMemType.JO:
            //                     AjinProxy.WriteDIOByModule(addr, (uint)addr.vInt); // 안되면 byte->uint->write필요
            //                     break;
            //             }

            return (int)Em_Mt_Er.OK;
        }

        //메소드 비트
        public override bool GetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void ClearBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr, bool value)
        {
            throw new Exception("미 구현");
        }
        public override void Toggle(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public bool[] GetBists(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
            //return new bool[0];
        }

        //메소드 - STRING
        public override int SetAscii(PlcAddr addr, string text)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        //메소드 - SHORT
        public override short GetShort(PlcAddr addr)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        public override void SetShort(PlcAddr addr, short value)
        {
            throw new Exception("미 구현");
        }
        //메소드 - SHORT        
        public override short[] GetShorts(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
        }
        public override void SetShorts(PlcAddr addr, short[] values, out int result)
        {
            throw new Exception("미 구현");
        }

        //메소드 - INT32
        public override int GetInt32(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetInt32(PlcAddr addr, int value)
        {
            throw new Exception("미 구현");
        }

        //읽어온 메모리에서 읽어오는 함수.
        public override bool VirGetBit(PlcAddr addr)
        {
            return VirGetInt32(addr).GetBit(addr.Bit);
        }
        public override void VirSetBit(PlcAddr addr, bool value)
        {
            int vv = VirGetInt32(addr).SetBit(addr.Bit, value);
            VirSetInt32(addr, vv);
        }
        public override short VirGetShort(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetShort(PlcAddr addr, short value)
        {
            throw new Exception("미구현 메모리");
        }
        public override string VirGetAscii(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetAscii(PlcAddr addr, string value)
        {
            throw new Exception("미구현 메모리");
        }
        public override int VirGetInt32(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.EM)
            {
                return BYTE_EM[addr.Addr];
            }
            else if (addr.Type == PlcMemType.ES)
            {
                return BYTE_ES[addr.Addr];
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetInt32(PlcAddr addr, int value)
        {
            if (addr.Type == PlcMemType.EM)
            {
                BYTE_EM[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.ES)
            {
                BYTE_ES[addr.Addr] = value;
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }

        public override bool[] VirGetBits(PlcAddr addr, int wordSize)
        {
            throw new Exception("미구현 메모리");
        }
        public override short[] VirGetShorts(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override float VirGetFloat(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.EM)
            {
                return (float)BYTE_EM[addr.Addr];
            }
            else if (addr.Type == PlcMemType.ES)
            {
                return (float)BYTE_ES[addr.Addr];
            }
            //             else if (addr.Type == PlcMemType.JR)
            //             {
            //                 return (float)BYTE_JR[addr.Addr + addr.Bit];
            //             }
            //             else if (addr.Type == PlcMemType.JM)
            //             {
            //                 return (float)BYTE_JM[addr.Addr];
            //             }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetFloat(PlcAddr addr, float value)
        {
            if (addr.Type == PlcMemType.EM)
            {
                BYTE_EM[addr.Addr] = (int)value;
            }
            else if (addr.Type == PlcMemType.ES)
            {
                BYTE_ES[addr.Addr] = (int)value;
            }
            //             else if (addr.Type == PlcMemType.JR)
            //             {
            //                 BYTE_JR[addr.Addr + addr.Bit] = (int)value;
            //             }
            //             else if (addr.Type == PlcMemType.JM)
            //             {
            //                 BYTE_JM[addr.Addr] = (int)value;
            //             }
            else
                throw new Exception("ADDR TYPE ERROR");
        }


        public void LogicWorking()
        {
            int[] read = new int[0];

            foreach (PlcAddr addr in LstReadAddr)
                ReadFromPLC(addr, addr.Length);

            foreach (PlcAddr addr in LstWriteAddr)
                WriteToPLC(addr, addr.Length);

            Motors.ForEach(f => f.LogicWorking());
            SettingStep();
        }

        #region Motor Access
        //모터 제어 로직 추가. 
        public List<EziSvoMotorProxy> Motors = new List<EziSvoMotorProxy>();

        #region Motor Status           

        public bool GetStatus(int axis, out uint dwInStatus, out uint dwOutStatus, out uint dwAxisStatus,
            out int lCmdPos, out int lActPos, out int lPosErr, out int lActVel, out ushort wPosItemNo)
        {
            dwInStatus = dwOutStatus = dwAxisStatus = 0;
            lCmdPos = lActPos = lPosErr = lActVel = wPosItemNo = 0;
            int ret = EziMOTIONPlusELib.FAS_GetAllStatus(axis, ref dwInStatus, ref dwOutStatus, ref dwAxisStatus, ref lCmdPos, ref lActPos, ref lPosErr, ref lActVel, ref wPosItemNo);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("GetStatus Error : {0}", ret));
                return false;
            }
            return true;
        }
        public bool IsInpos(uint status)
        {
            return (status & EziSERVO2PlusE.FFLAG_INPOSITION) != 0;
        }
        public bool IsError(uint status)
        {
            return (status & EziSERVO2PlusE.FFLAG_ERRORALL) != 0;
        }
        public bool IsServoOn(uint status)
        {
            return (status & EziSERVO2PlusE.FFLAG_SERVOON) != 0;
        }
        public bool IsMoving(uint status)
        {
            return (status & EziSERVO2PlusE.FFLAG_MOTIONING) != 0;
        }
        public bool IsPosLimit(uint status)
        {
            return (status & EziSERVO2PlusE.FFLAG_HWPOSILMT) != 0;
        }
        public bool IsNegLimit(uint status)
        {
            return (status & EziSERVO2PlusE.FFLAG_HWNEGALMT) != 0;
        }
        //         public float CurrPos()
        //         {
        //             new Exception("StatusLogic에서 얻은 값을 쓰시오");
        //         }
        //         public float CurrSpeed()
        //         {
        //             new Exception("StatusLogic에서 얻은 값을 쓰시오");
        //         }
        #endregion

        private int SettingStepNo = 0;
        public void SettingStep()
        {
            //if (SettingStepNo == 0)
            //{
            //    if (YB_PmacValueSave.vBit == true)
            //    {
            //        XB_PmacValueSaveAck.vBit = true;

            //        SettingStepNo = 10;
            //    }
            //}
            //else if (SettingStepNo == 10)
            //{
            //    if (YB_PmacValueSave.vBit == false)
            //    {

            //        for (int jPos = 0; jPos < motors.Count; jPos++)
            //        {

            //            for (int iPos = 1; iPos <= motors[jPos].PositionCount; iPos++)
            //            {
            //                motors[jPos].XF_Position1stPointAck[iPos - 1].vFloat = motors[jPos].YF_Position1stPoint[iPos - 1].vFloat;
            //                motors[jPos].XF_Position1stSpeedAck[iPos - 1].vFloat = motors[jPos].YF_Position1stSpeed[iPos - 1].vFloat;
            //                motors[jPos].XF_Position1stAccelAck[iPos - 1].vFloat = motors[jPos].YF_Position1stAccel[iPos - 1].vFloat;

            //            }
            //        }

            //        XB_PmacValueSaveAck.vBit = false;
            //        SettingStepNo = 20;
            //    }
            //}
            //else if (SettingStepNo == 20)
            //{

            //    SettingStepNo = 30;
            //}
            //else if (SettingStepNo == 30)
            //{
            //    SettingStepNo = 0;

            //}
        }


        private int InterlockCmdStepNo = 0;
        private int AcsResetStepNo = 0;
        public void CommCmdLogic()
        {
            ////Interlock 처리 
            //if (InterlockCmdStepNo == 0)
            //{
            //    if (YB_PinUpMotorInterlockOffCmd.vBit == true)
            //    {
            //        XB_PinUpMotorInterlockOffCmdAck.vBit = true;
            //        XB_PinUpInterlockOff.vBit = true;
            //        InterlockCmdStepNo = 10;
            //    }
            //}
            //else if (InterlockCmdStepNo == 10)
            //{
            //    if (YB_PinUpMotorInterlockOffCmd.vBit == false)
            //    {
            //        XB_PinUpMotorInterlockOffCmdAck.vBit = false;
            //        InterlockCmdStepNo = 20;
            //    }
            //}
            //else if (InterlockCmdStepNo == 20)
            //{
            //    InterlockCmdStepNo = 0;
            //}

            ////Acs Reset Step No 처리 
            //if (AcsResetStepNo == 0)
            //{
            //    if (YB_PmacResetCmd.vBit == true)
            //    {
            //        XB_PmacResetCmdAck.vBit = true;
            //        AcsResetStepNo = 10;
            //    }
            //}
            //else if (AcsResetStepNo == 10)
            //{
            //    if (YB_PmacResetCmd.vBit == false)
            //    {
            //        XB_PmacResetCmdAck.vBit = false;
            //        AcsResetStepNo = 20;
            //    }
            //}
            //else if (InterlockCmdStepNo == 20)
            //{
            //    //ACS RESET 처리. 요함. 

            //    AcsResetStepNo = 30;
            //}
            //else if (InterlockCmdStepNo == 30)
            //{
            //    AcsResetStepNo = 0;
            //}
        }

        public void MoveInterlockLogic()
        {
            //if (XB_PinUpInterlockOff.vBit == false)
            //{
            //    //인터락 조직 추가. 
            //}
        }
        public bool MotorOn(int rotaryNo)
        {
            int ret = EziMOTIONPlusELib.FAS_ServoEnable(rotaryNo, 1);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorOn Error : {0}", ret));
                return false;
            }
            return true;
        }
        public bool MotorKill(int rotaryNo)
        {
            int ret = EziMOTIONPlusELib.FAS_ServoEnable(rotaryNo, 0);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorKill Error : {0}", ret));
                return false;
            }
            return true;
        }
        public bool ErrorReset(int rotaryNo)
        {
            int ret = EziMOTIONPlusELib.FAS_ServoAlarmReset(rotaryNo);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("ErrorReset Error : {0}", ret));
                return false;
            }
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotaryNo"></param>
        /// <param name="vel1st"></param>
        /// <param name="velLast"></param>
        /// <param name="accDec">time (ms)</param>
        /// <returns></returns>
        public bool MotorHommingSetting(int rotaryNo, double vel1st, double velLast, double accDec)
        {
            int ret = EziMOTIONPlusELib.FAS_SetParameter(rotaryNo, (byte)EziSERVO2PlusE.PARAM.ORGMETHOD, 3);// 3 = - limit 감지 후 첫번째 index, 5 = Z상
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHommingSetting-ORGMETHOD Error : {0}", ret));
                return false;
            }

            ret = EziMOTIONPlusELib.FAS_SetParameter(rotaryNo, (byte)EziSERVO2PlusE.PARAM.ORGDIR, 1);// - limit 방향으로
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHommingSetting-ORGDIR Error : {0}", ret));
                return false;
            }

            ret = EziMOTIONPlusELib.FAS_SetParameter(rotaryNo, (byte)EziSERVO2PlusE.PARAM.ORGSPEED, (int)vel1st);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHommingSetting-ORGSPEED Error : {0}", ret));
                return false;
            }

            ret = EziMOTIONPlusELib.FAS_SetParameter(rotaryNo, (byte)EziSERVO2PlusE.PARAM.ORGSEARCHSPEED, (int)velLast);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHommingSetting-ORGSEARCHSPEED Error : {0}", ret));
                return false;
            }

            ret = EziMOTIONPlusELib.FAS_SetParameter(rotaryNo, (byte)EziSERVO2PlusE.PARAM.ORGACCDECTIME, (int)velLast);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHommingSetting-ORGSEARCHSPEED Error : {0}", ret));
                return false;
            }

            ret = EziMOTIONPlusELib.FAS_SetParameter(rotaryNo, (byte)EziSERVO2PlusE.PARAM.ORGOFFSET, 0);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHommingSetting-ORGSEARCHSPEED Error : {0}", ret));
                return false;
            }

            ret = EziMOTIONPlusELib.FAS_SetParameter(rotaryNo, (byte)EziSERVO2PlusE.PARAM.ORGPOSITIONSET, 0);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHommingSetting-ORGSEARCHSPEED Error : {0}", ret));
                return false;
            }

            return true;
        }
        public bool MotorHomming(int rotaryNo)
        {
            int ret = EziMOTIONPlusELib.FAS_MoveOriginSingleAxis(rotaryNo);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("MotorHomming Error : {0}", ret));
                return false;
            }
            return true;
        }
        public bool PositionClear(int rotaryNo)
        {
            int ret = EziMOTIONPlusELib.FAS_ClearPosition(rotaryNo);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("PositionClear Error : {0}", ret));
                return false;
            }
            return true;
        }
        public bool MotorHommingCheck(int axis)
        {
            uint homeResult = 0;
            CAXM.AxmHomeGetResult(axis, ref homeResult);

            switch ((AXT_MOTION_HOME_RESULT)homeResult)
            {
                case AXT_MOTION_HOME_RESULT.HOME_ERR_UNKNOWN:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_ERR_VELOCITY:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_ERR_NEG_LIMIT:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_ERR_POS_LIMIT:
                    break;
                case AXT_MOTION_HOME_RESULT.HOME_SEARCHING:
                    return true;
                case AXT_MOTION_HOME_RESULT.HOME_SUCCESS:
                    break;
                default:
                    break;
            }
            return false;
        }
        public bool GetHomeRate(int axis, out uint rate)
        {
            rate = 100;
            return false;
            //             rate = 0;
            //             uint homeMainStep = 0;
            //             CAXM.AxmHomeGetRate(axis, ref homeMainStep, ref rate);
            //             return true;
        }
        public bool IsMotorHommingCompleted(uint status)
        {
            return (status & EziSERVO2PlusE.FFLAG_ORIGINRETURNING/*원점 복귀 운전 중?*/) == 0
                && (status & EziSERVO2PlusE.FFLAG_ORIGINRETOK/*원점 복귀 운전 완료?*/) != 0;
        }
        public bool JogSpeed(int rotaryNo, double speed)
        {
            if (speed < 0)
                speed = 1;
            EziSvoMotorProxy motor = Motors.FirstOrDefault(m => m.RotaryNo == rotaryNo);
            if (motor == null)
                return false;
            motor.JogSpeed = speed;
            return true;
        }
        public bool EmergencyStop(int rotaryNo)
        {
            int ret = EziMOTIONPlusELib.FAS_EmergencyStop(rotaryNo);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("EmergencyStop Error : {0}", ret));
                return false;
            }
            return true;
        }
        public bool JogStop(int rotaryNo)
        {
            int ret = EziMOTIONPlusELib.FAS_MoveStop(rotaryNo);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("JogStop Error : {0}", ret));
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="array">direction 1 = pos, 0 = neg</param>
        /// <returns></returns>
        public bool JogMove(int rotaryNo, double speed, int array)
        {
            uint jogSpd = 0;
            EziSvoMotorProxy motor = Motors.FirstOrDefault(m => m.RotaryNo == rotaryNo);

            if (motor == null
                || motor.JogSpeed < 0)
                return false;

            jogSpd = (uint)speed;

            //             EziMOTIONPlusELib.VELOCITY_OPTION_EX ex = new EziMOTIONPlusELib.VELOCITY_OPTION_EX();
            //             ex.BIT_USE_CUSTOMACCDEC = true;
            //             ex.wCustomAccDecTime = 50;
            // 
            //             int ret = EziMOTIONPlusELib.FAS_MoveVelocityEx(rotaryNo, jogSpd, array == 1 ? EziSERVO2PlusE.DIR_INC : EziSERVO2PlusE.DIR_DEC, ex);
            int ret = EziMOTIONPlusELib.FAS_MoveVelocity(rotaryNo, jogSpd, array == 1 ? EziSERVO2PlusE.DIR_INC : EziSERVO2PlusE.DIR_DEC);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("JogMove Error : {0}", ret));
                return false;
            }
            return true;
        }

        public bool PTPMove(int rotaryNo, double position)
        {
            //             string cmd = string.Format("#{0}J={1}", axis, position);
            //             string cmdResult = string.Empty;
            //             return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
            return false;
        }
        public bool PTPMoveSet(int rotaryNo)
        {
            return true;
        }

        public bool PTPMoveSpeed(int rotaryNo, int position, uint speed)
        {
            EziMOTIONPlusELib.MOTION_OPTION_EX ex = new EziMOTIONPlusELib.MOTION_OPTION_EX();
            ex.BIT_USE_CUSTOMACCEL = true;
            ex.BIT_USE_CUSTOMDECEL = true;
            ex.wCustomAccelTime = 50;
            ex.wCustomDecelTime = 200;
            int ret = EziMOTIONPlusELib.FAS_MoveSingleAxisAbsPosEx(rotaryNo, position, speed, ex);
            if (ret != EziMOTIONPlusELib.FMM_OK)
            {
                EziLogger.Log(string.Format("PTPMoveSpeed Error : {0}", ret));
                return false;
            }
            return true;
        }
        public bool StepMove(int rotaryNo, double distance)
        {
            //             string cmd = string.Format("#{0}J:{1}", axis, distance);
            //             string cmdResult = string.Empty;
            //             return UMacProxy.DeviceCommand(_dwDevice, cmd, out cmdResult);
            return false;
        }
        #endregion

    }
}



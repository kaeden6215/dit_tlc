﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Net;
using EziNetworkModule;
using System.IO;

namespace Dit.Framework.PLC._05._PLC
{
    public static class EziLogger
    {
        public static void Log(string str)
        {
            Console.WriteLine(str);
        }
    }

    public static class EziProxy
    {
        public static int Connect(string headIP, int[] rotaryNum)
        {
            int ret = (int)Em_Mt_Er.OK;
            IPAddress ipaddr;

            if (IPAddress.TryParse(headIP, out ipaddr) == false)
            {
                EziLogger.Log("Fail connect, ip error");
                return (int)Em_Mt_Er.Failed;
            }

            byte[] byAddr = ipaddr.GetAddressBytes();
            byte axisByte = 0;
            int funcRet;
            foreach (int axis in rotaryNum)
            {
                axisByte = (byte)axis;
                funcRet = EziMOTIONPlusELib.FAS_Connect(byAddr[0], byAddr[1], byAddr[2], axisByte, axis);
                if (funcRet != 1)
                {
                    EziLogger.Log(string.Format("{0} Fail connect : {1}", axis, funcRet));
                    ret = (int)Em_Mt_Er.NotConnected;
                    continue;
                }
            }
            
            return ret;
        }
        public static int Initialize(int[] rotaryNum)
        {
            int ret = (int)Em_Mt_Er.OK;

            int funcRet;
            foreach (int axis in rotaryNum)
            {
                funcRet = EziMOTIONPlusELib.FAS_SetParameter(axis, (byte)EziSERVO2PlusE.PARAM.PULSEPERREVOLUTION, 8); // 1회전당 펄스 수
                if (funcRet != EziMOTIONPlusELib.FMM_OK)
                {
                    EziLogger.Log(string.Format("{0} Fail Init PULSEPERREVOLUTION : {1}", axis, funcRet));
                    ret = (int)Em_Mt_Er.Failed;
                    continue;
                }
            }

            return ret;
        }
        public static int Close(int[] axisNum)
        {
            foreach (int axis in axisNum)
                EziMOTIONPlusELib.FAS_Close(axis);
            return (int)Em_Mt_Er.OK;
        }

        public static bool CommErrorLog(bool isOn, string path)
        {
            if (isOn)
            {
                if (EziMOTIONPlusELib.FAS_SetLogPath(path) == 1)
                {
                    EziMOTIONPlusELib.FAS_EnableLog(1);
                    return true;
                }                
            }
            else
            {
                EziMOTIONPlusELib.FAS_EnableLog(0);
            }
            return false;
        }

        #region Motor
        public static bool IsServoSettingAbnormal(EziMotorSetting setting)
        {
            EziMotorSetting a = new EziMotorSetting();
            a = (EziMotorSetting)setting.Clone();
            return GetAppliedSetting(ref a);
        }
        public static bool GetAppliedSetting(ref EziMotorSetting setting)
        {
            string errStr = "";
            int idx = 0, ret = 0, retValue = 0;
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 00, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.PulsePerRevolution)    errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 01, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.AxisMaxSpeed)          errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 02, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.AxisStartSpeed)        errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 03, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.AxisAccTime)           errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 04, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.AxisDecTime)           errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 05, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.SpeedOverride)         errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 06, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.JogSpeed)              errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 07, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.JogStartSpeed)         errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 08, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.JogAccDecTime)         errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 09, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.SWLimitPlusValue)      errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 10, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.SWLimitMinusValue)     errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 11, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.SWLimitStopMethod)     errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 12, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.HWLimitStopMethod)     errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 13, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.LimitSensorLogic)      errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 14, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgSpeed)              errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 15, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgSearchSpeed)        errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 16, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgAccDecTime)         errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 17, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgMethod)             errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 18, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgDir)                errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 19, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgOffSet)             errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 20, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgPositionSet)        errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 21, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgSensorLogic)        errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 22, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.PositionLoopGain)      errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 23, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.InposValue)            errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 24, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.PosTrackingLimit)      errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 25, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.MotionDir)             errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 26, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.LimitSensorDir)        errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 27, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.OrgTorqueRatio)        errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 28, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.PosErrorOverflowLimit) errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 29, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.BrakeDelayTime)        errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 30, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.RunCurrent)            errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 31, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.BoostCurrent)          errStr += string.Format("{0}diff.", idx++);
            ret = EziMOTIONPlusELib.FAS_GetParameter(setting.AxisNo, 32, ref retValue); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n", idx, ret); if (retValue != setting.StopCurrent)           errStr += string.Format("{0}diff.", idx++);

            return errStr.Length > 0;
        }
        public static bool SetMotorParameter(EziMotorSetting setting)
        {
            string errStr = "";
            int idx = 0, ret = 0; 

            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 00, setting.PulsePerRevolution);    if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 01, setting.AxisMaxSpeed);          if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 02, setting.AxisStartSpeed);        if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 03, setting.AxisAccTime);           if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 04, setting.AxisDecTime);           if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 05, setting.SpeedOverride);         if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 06, setting.JogSpeed);              if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 07, setting.JogStartSpeed);         if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 08, setting.JogAccDecTime);         if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 09, setting.SWLimitPlusValue);      if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 10, setting.SWLimitMinusValue);     if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 11, setting.SWLimitStopMethod);     if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 12, setting.HWLimitStopMethod);     if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 13, setting.LimitSensorLogic);      if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 14, setting.OrgSpeed);              if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 15, setting.OrgSearchSpeed);        if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 16, setting.OrgAccDecTime);         if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 17, setting.OrgMethod);             if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 18, setting.OrgDir);                if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 19, setting.OrgOffSet);             if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 20, setting.OrgPositionSet);        if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 21, setting.OrgSensorLogic);        if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 22, setting.PositionLoopGain);      if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 23, setting.InposValue);            if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 24, setting.PosTrackingLimit);      if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 25, setting.MotionDir);             if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 26, setting.LimitSensorDir);        if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 27, setting.OrgTorqueRatio);        if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 28, setting.PosErrorOverflowLimit); if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 29, setting.BrakeDelayTime);        if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 30, setting.RunCurrent);            if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 31, setting.BoostCurrent);          if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);
            ret = EziMOTIONPlusELib.FAS_SetParameter(setting.AxisNo, 32, setting.StopCurrent);           if (ret != EziMOTIONPlusELib.FMM_OK) errStr += string.Format("{0}-{1}\r\n",idx++, ret);

            return errStr.Length > 0;
        }       

        public static bool SaveToRom(int axisNo)
        {
            if (EziMOTIONPlusELib.FAS_SaveAllParameters(axisNo) != EziMOTIONPlusELib.FMM_OK)
                return false;
            return true;
        }

        public static uint GetStatus(int axisNo)
        {
            uint temp = 0;
            if (EziMOTIONPlusELib.FAS_GetAxisStatus(axisNo, ref temp) != EziMOTIONPlusELib.FMM_OK)
                return 0;
            return temp;
        }
        #endregion
    }
}

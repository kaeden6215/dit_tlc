
public class EziSERVO2PlusE
{
	////------------------------------------------------------------------
	////                 Device Type Defines.
	////------------------------------------------------------------------
	public const byte DEVTYPE = 100;
	public const string DEVNAME = "Ezi-SERVO II Plus-E-ST";

	//------------------------------------------------------------------
	//                 Axis Status Flag Defines.
	//------------------------------------------------------------------
	public const uint FFLAG_ERRORALL = 0x00000001;
	public const uint FFLAG_HWPOSILMT = 0x00000002;
	public const uint FFLAG_HWNEGALMT = 0x00000004;
	public const uint FFLAG_SWPOGILMT = 0x00000008;
	public const uint FFLAG_SWNEGALMT = 0x00000010;
	public const uint FFLAG_RESERVED0 = 0x00000020;
	public const uint FFLAG_RESERVED1 = 0x00000040;
	public const uint FFLAG_ERRPOSOVERFLOW = 0x00000080;
	public const uint FFLAG_ERROVERCURRENT = 0x00000100;
	public const uint FFLAG_ERROVERSPEED = 0x00000200;
	public const uint FFLAG_ERRPOSTRACKING = 0x00000400;
	public const uint FFLAG_ERROVERLOAD = 0x00000800;
	public const uint FFLAG_ERROVERHEAT = 0x00001000;
	public const uint FFLAG_ERRBACKEMF = 0x00002000;
	public const uint FFLAG_ERRMOTORPOWER = 0x00004000;
	public const uint FFLAG_ERRINPOSITION = 0x00008000;
	public const uint FFLAG_EMGSTOP = 0x00010000;
	public const uint FFLAG_SLOWSTOP = 0x00020000;
	public const uint FFLAG_ORIGINRETURNING = 0x00040000;
	public const uint FFLAG_INPOSITION = 0x00080000;
	public const uint FFLAG_SERVOON = 0x00100000;
	public const uint FFLAG_ALARMRESET = 0x00200000;
	public const uint FFLAG_PTSTOPPED = 0x00400000;
	public const uint FFLAG_ORIGINSENSOR = 0x00800000;
	public const uint FFLAG_ZPULSE = 0x01000000;
	public const uint FFLAG_ORIGINRETOK = 0x02000000;
	public const uint FFLAG_MOTIONDIR = 0x04000000;
	public const uint FFLAG_MOTIONING = 0x08000000;
	public const uint FFLAG_MOTIONPAUSE = 0x10000000;
	public const uint FFLAG_MOTIONACCEL = 0x20000000;
	public const uint FFLAG_MOTIONDECEL = 0x40000000;
	public const uint FFLAG_MOTIONCONST = 0x80000000;

	public enum PARAM
	{
		PULSEPERREVOLUTION = 0,
		AXISMAXSPEED,
		AXISSTARTSPEED,
		AXISACCTIME,
		AXISDECTIME,

		SPEEDOVERRIDE,
		JOGHIGHSPEED,
		JOGLOWSPEED,
		JOGACCDECTIME,

		SWLMTPLUSVALUE,
		SWLMTMINUSVALUE,
		SOFTLMTSTOPMETHOD,
		HARDLMTSTOPMETHOD,
		LIMITSENSORLOGIC,

		ORGSPEED,
		ORGSEARCHSPEED,
		ORGACCDECTIME,
		ORGMETHOD,
		ORGDIR,
		ORGOFFSET,
		ORGPOSITIONSET,
		ORGSENSORLOGIC,

		POSITIONLOOPGAIN,
		INPOSITIONVALUE,
		POSTRACKINGLIMIT,
		MOTIONDIR,

		LIMITSENSORDIR,
		ORGTORQUERATIO,

		POSERROVERFLOWLIMIT,
		BRAKEDELAYTIME,

		RUNCURRENT,
		BOOSTCURRENT,
		STOPCURRENT,

		MAX_PARAM_CNT,
	};

	//static const int	DIR_INC = 1;
	public const int DIR_INC = 1;
	//static const int	DIR_DEC = 0;
	public const int DIR_DEC = 0;
}

﻿namespace Dit.Framework.PLC
{
    partial class FrmCtrl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCtrl));
            this.axActEasyIF1 = new AxACTMULTILib.AxActEasyIF();
            ((System.ComponentModel.ISupportInitialize)(this.axActEasyIF1)).BeginInit();
            this.SuspendLayout();
            // 
            // axActEasyIF1
            // 
            this.axActEasyIF1.Enabled = true;
            this.axActEasyIF1.Location = new System.Drawing.Point(0, 0);
            this.axActEasyIF1.Name = "axActEasyIF1";
            this.axActEasyIF1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axActEasyIF1.OcxState")));
            this.axActEasyIF1.Size = new System.Drawing.Size(0, 0);
            this.axActEasyIF1.TabIndex = 0;
            // 
            // FrmCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.axActEasyIF1);
            this.Name = "FrmCtrl";
            this.Text = "FrmTest";
            ((System.ComponentModel.ISupportInitialize)(this.axActEasyIF1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxACTMULTILib.AxActEasyIF axActEasyIF1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Dit.Framework.UMAC
{
    public class PMAC
    {
        public delegate void DOWNLOADMSGPROC(String str, Int32 newline);
        public delegate void DOWNLOADGETPROC(Int32 nIndex, String lpszBuffer, Int32 nMaxLength);
        public delegate void DOWNLOADPROGRESS(Int32 nPercent);
        public delegate void DOWNLOADERRORPROC(String fname, Int32 err, Int32 line, String szLine);

        [DllImport("Pcomm32.dll")]
        public static extern Int32 OpenPmacDevice(UInt32 dwDevice);
        [DllImport("Pcomm32.dll")]
        public static extern UInt32 PmacSelect(UInt32 dwDevice);
        [DllImport("Pcomm32.dll")]
        public static extern UInt32 ClosePmacDevice(UInt32 dwDevice);

        [DllImport("Pcomm32.dll")]
        //public static extern Int32 PmacGetResponseA(UInt32 dwDevice, StringBuilder s, UInt32 maxchar, StringBuilder outstr);
        public static extern Int32 PmacGetResponseA(UInt32 dwDevice, Byte[] s, UInt32 maxchar, Byte[] outstr);

        [DllImport("Pcomm32.dll")]
        public static extern Int32 PmacDownloadA(UInt32 dwDevice, DOWNLOADMSGPROC msgp, DOWNLOADGETPROC getp, DOWNLOADPROGRESS pprg, Byte[] filename, Int32 macro, Int32 map, Int32 log, Int32 dnld);

        [DllImport("Pcomm32.dll")]
        public static extern IntPtr PmacDPRSetMem(UInt32 dwDevice, UInt32 offset, UInt32 count, IntPtr val);

        [DllImport("Pcomm32.dll")]
        public static extern IntPtr PmacDPRGetMem(UInt32 dwDevice, UInt32 offset, UInt32 count, IntPtr val);


    }

    public class UMACConnector
    {

        public const int MAX_COMMAND_BUFFER = 256;
        public const int MAX_GET_STATUS_BUFFER = 512;
        public const int MAX_RESULT_BUFFER = 16384;

        public const int M_IO_IN = 4000;
        public const int M_IO_OUT = 5000;
        public const int M_TO_CMD = 6000;
        public const int M_FROM_CMD = 6200;

        public const int MAX_IO_IN_PROCESS = 96;    // UMAC
        public const int MAX_IO_OUT_PROCESS = 96;	// UMAC

        public bool UmacOpened;
        public UInt32 DeviceNumber = 0;
        public bool Initialize(string ip, string port)
        {


            return false;
        }

        public int[] UMAC_IN_DATA = new int[96];
        public int[] UMAC_OUT_DATA = new int[96];

        public bool StatusT()
        {
            GetBits(M_IO_IN, MAX_IO_IN_PROCESS, ref UMAC_IN_DATA);
            GetBits(M_IO_OUT, MAX_IO_OUT_PROCESS, ref UMAC_OUT_DATA);

            return true;
        }
        public bool IOCmdT()
        {
            return false;
        }
        public bool ETCT()
        {
            return false;
        }
        public bool MainT()
        {
            return false;
        }
        public bool AlarmT()
        {
            return false;
        }


        public bool GetBits(int address, int size, ref int[] readData)
        {
            string cmd = string.Format("M{0},{1}", address, size);
            string cmdResult = string.Empty;
            bool result = DeviceCommand(cmd, out cmdResult);

            for (int iPos = 0; iPos < cmdResult.Length; iPos++)
                readData[iPos] = cmdResult[iPos] == '1' ? 1 : 0;

            return true;
        }


        public bool DeviceCommand(string cmd, out string cmdResult)
        {
            bool result = false;
            string response = string.Empty;
            cmdResult = string.Empty;

            if (UmacOpened == false)
            {
                //로그 추가. 
                return false;
            }

            Byte[] byCommand = new Byte[255];
            Byte[] byResponse = new Byte[MAX_RESULT_BUFFER];

            byCommand = Encoding.GetEncoding("euc-kr").GetBytes(cmd); ;
            PMAC.PmacGetResponseA(DeviceNumber, byResponse, Convert.ToUInt32(byResponse.Length - 1), byCommand);
            cmdResult = Encoding.GetEncoding("euc-kr").GetString(byResponse);

            if (byResponse[0] == 0x07)
            {
                if (response.Substring(1, 3) == "ERR")
                {
                    string errno = response.Substring(4);
                    string errMsg = string.Format("PMAC Respond Error : {0:00#}", errno);
                }
            }
            else
            {
                result = true;
            }

            return result;
        }
        public bool IVariableCommand(int axis, int variable, double value)
        {
            string cmd = string.Format("i{0}{1}-{2}", axis, variable, value);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }
        public bool MotorKill(int axis)
        {
            string cmd = string.Format("{0}dk", axis);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }
        public bool MotorHomming(int axis)
        {
            int M_INIT_MOTOR = 6000;
            int eTO_CMD_1_HOME_START_CMD = 1;

            //string cmd = string.Format("#{0}hm", axis );
            string cmd = string.Format("M{0}=1", M_INIT_MOTOR + eTO_CMD_1_HOME_START_CMD + axis);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }
        public bool MotorHommingCheck(int axis)
        {
            return true;
        }
        public bool MotorHommingCompleted(int axis)
        {


            return true;
        }
        public bool JogSpeed(int axis, double speed)
        {
            if (speed < 0)
                speed = 1;

            //string cmd = string.Format("#{0}hm", axis );
            string cmd = string.Format("i{0}22={1}", axis, speed);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }
        public bool JogStop(int axis)
        {
            string cmd = string.Format("#{0}J/", axis);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }


        public bool JogMove(int axis, int array)
        {
            string cmd = array <= 0 ? string.Format("#{0}J-", axis) : string.Format("#{0}J+", axis);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }

        public bool PTPMove(int axis, double position)
        {
            string cmd = string.Format("#{0}J={1}", axis, position);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }
        public bool PTPMoveSpeed(int axis, double position, double speed)
        {
            string cmd = string.Format("i{0}22={1} #{1}J={2}", axis, speed, axis, position);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }
        public bool StepMove(int axis, double distance)
        {
            string cmd = string.Format("#{0}J={1}", axis, distance);
            string cmdResult = string.Empty;
            return DeviceCommand(cmd, out cmdResult);
        }

    }
}


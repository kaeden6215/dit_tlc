﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace Dit.FrameworkSingle.Net.SNet
{
    public delegate void SNetErrorEventHandler(object sender, SNetErrorEventArgs e);
    public delegate void SNetConnectEventHandler(object sender, SNetConnectionEventArgs e);
    public delegate void SNetCloseEventHandler(object sender, SNetConnectionEventArgs e);
    public delegate void SNetSendEventHandler(object sender, SNetSendEventArgs e);
    public delegate void SNetReceiveEventHandler(object sender, SNetReceiveEventArgs e);
    
    public class SNetErrorEventArgs : EventArgs
    {
        private readonly Exception exception;
        private readonly int id = 0;
        public SNetErrorEventArgs(int id, Exception exception)
        {
            this.id = id;
            this.exception = exception;
        }
        public Exception AsSocketException
        {
            get { return this.exception; }
        }
        public int ID
        {
            get { return this.id; }
        }
    }

    public class SNetConnectionEventArgs : EventArgs
    {
        private readonly int id = 0;
        public SNetConnectionEventArgs(int id)
        {
            this.id = id;
        }
        public int ID
        {
            get { return this.id; }
        }
    }

    public class SNetSendEventArgs : EventArgs
    {
        private readonly int id = 0;
        private readonly int sendBytes;

        public SNetSendEventArgs(int id, int sendBytes)
        {
            this.id = id;
            this.sendBytes = sendBytes;
        }
        public int SendBytes
        {
            get { return this.sendBytes; }
        }
        public int ID
        {
            get { return this.id; }
        }
    }

    public class SNetReceiveEventArgs : EventArgs
    {
        private readonly int id = 0;
        
        public byte[] Buffer
        {
            get;
            set;
        }
        public int ReceiveBytes { get; }

        public SNetReceiveEventArgs(int id, byte[] buffer, int receiveBytes)
        {
            this.id = id;
            this.Buffer = buffer;
            this.ReceiveBytes = receiveBytes;
        }
        public int ID
        {
            get { return this.id; }
        }
    }    
}

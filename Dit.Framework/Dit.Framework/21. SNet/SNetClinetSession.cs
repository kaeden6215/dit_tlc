﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework;
using Dit.Framework.Net.AsSocket;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using System.Timers;
using Dit.Framework.Comm;

namespace Dit.FrameworkSingle.Net.SNet
{
    public enum EmSpliterType
    {
        Spliter,
        Length,
        None,
    }

    public class SNetClinetSession
    {
        //이벤트
        public event SNetErrorEventHandler OnError;
        public event SNetConnectEventHandler OnConnect;
        public event SNetCloseEventHandler OnClose;
        public event SNetSendEventHandler OnSend;
        public event SNetReceiveEventHandler OnReceive;
        private AsSocketClient _socket; // 설비 접속 클라이언트

        public string IpAddress { get; set; }
        public int Port { get; set; }
        public byte[] Spliter = new byte[] { 0x0A, 0x0D };
        public int SplitLength = 0;
        public EmSpliterType SpliterType { get; set; }

        //통신용 메모리 버퍼.
        private byte[] _allReciveDataBuffer = new byte[1024000];
        private int _allReciveDataBufferSize = 1024000;
        private int _totalBytesSize = 0;

        //재연결 처리용
        private Timer _tmrConnector = new Timer(5000);

        private bool _isStarted = false;


        public SNetClinetSession()
        {
            SpliterType = EmSpliterType.None;
            _tmrConnector.Elapsed += new ElapsedEventHandler(TmrConnector_Elapsed);
        }

        public bool IsConnection
        {
            get
            {
                if (_socket == null) return false;
                if (_socket.Connection == null)
                {
                    return false;
                }
                else
                {
                    return _socket.Connection.Connected;
                }
            }
        }

        public void TmrConnector_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (IsConnection == false && _isStarted)
            {
                IntializeSocket();
                _socket.Connect(IpAddress, Port);
            }
        }
        public void Start()
        {
            _isStarted = true;
            _socket = new AsSocketClient(0);
            _socket.OnConnet += new AsSocketConnectEventHandler(SocketEquip_OnConnect);
            _socket.OnClose += new AsSocketCloseEventHandler(SocketEquip_OnClose);
            _socket.OnError += new AsSocketErrorEventHandler(SocketEquip_OnError);
            _socket.OnReceive += new AsSocketReceiveEventHandler(SocketEquip_OnReceive);


            TmrConnector_Elapsed(null, null);
            _tmrConnector.Start();
        }
        public void Stop()
        {
            _isStarted = false;
            _tmrConnector.Stop();
            _socket.Close();
        }
        public bool SendPacket(string str)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(str);
            return SendPacket(buffer);
        }
        public bool SendPacket(byte[] buffer)
        {
            lock (this)
            {
                if (IsConnection)
                {
                    return _socket.Send(buffer);
                }
                else
                    return false;
            }
        }
        private void SocketEquip_OnError(object sender, AsSocketErrorEventArgs e)
        {
            //IntPtr handle = ((AsSocketClient)sender).Connection.Handle;

            if(_socket.Connection != null)
            _socket.Connection.Close();

            if (OnError != null)
                OnError(this, new SNetErrorEventArgs(0, e.AsSocketException));
        }
        private void SocketEquip_OnConnect(object sender, AsSocketConnectionEventArgs e)
        {
            if (OnConnect != null)
                OnConnect(0, new SNetConnectionEventArgs(0));
        }
        private void SocketEquip_OnClose(object sender, AsSocketConnectionEventArgs e)
        {
            if (OnClose != null)
                OnClose(0, new SNetConnectionEventArgs(0));
        }

        public void IntializeSocket()
        {
            _allReciveDataBuffer = new byte[1024000];
            _allReciveDataBufferSize = 1024000;
            _totalBytesSize = 0;
        }

        private void SocketEquip_OnReceive(object sender, AsSocketReceiveEventArgs e)
        {
            lock (this)
            {
                try
                {
                    if (SpliterType == EmSpliterType.Spliter) // Laser용 (패킷의 마지막에 구분자가 있는 경우)
                    {
                        if (_totalBytesSize + e.ReceiveBytes > _allReciveDataBufferSize)
                        {
                            _allReciveDataBufferSize = _totalBytesSize + e.ReceiveBytes + 1024;
                            byte[] tBuff = new byte[_allReciveDataBufferSize];
                            Array.Copy(_allReciveDataBuffer, 0, tBuff, 0, _allReciveDataBuffer.Length);

                            _allReciveDataBuffer = tBuff;
                        }


                        Array.Copy(e.ReceiveData, 0, _allReciveDataBuffer, _totalBytesSize, e.ReceiveBytes);
                        _totalBytesSize = _totalBytesSize + e.ReceiveBytes;


                        int inx = MemoryExtension.GetIndex(_allReciveDataBuffer, Spliter);
                        while (inx > 0)
                        {
                            byte[] buff = new byte[inx + Spliter.Length];
                            Array.Copy(_allReciveDataBuffer, 0, buff, 0, inx + Spliter.Length);

                            _totalBytesSize = _totalBytesSize - inx - Spliter.Length;
                            Array.Copy(_allReciveDataBuffer, inx + Spliter.Length, _allReciveDataBuffer, 0, _totalBytesSize);
                            OnRecivePacket(buff.Length, buff);

                            inx = MemoryExtension.GetIndex(_allReciveDataBuffer, Spliter);
                        }
                    }
                    else if (SpliterType == EmSpliterType.Length) // SEM용 (받는 패킷의 길이가 고정인 경우)
                    {
                        if (_totalBytesSize + e.ReceiveBytes > _allReciveDataBufferSize)
                        {
                            _allReciveDataBufferSize = _totalBytesSize + e.ReceiveBytes + 1024;
                            byte[] tBuff = new byte[_allReciveDataBufferSize];
                            Array.Copy(_allReciveDataBuffer, 0, tBuff, 0, _allReciveDataBuffer.Length);

                            _allReciveDataBuffer = tBuff;
                        }


                        Array.Copy(e.ReceiveData, 0, _allReciveDataBuffer, _totalBytesSize, e.ReceiveBytes);
                        _totalBytesSize = _totalBytesSize + e.ReceiveBytes;


                        while (SplitLength <= _totalBytesSize)
                        {
                            byte[] buff = new byte[SplitLength];
                            Array.Copy(_allReciveDataBuffer, 0, buff, 0, SplitLength);

                            _totalBytesSize = _totalBytesSize - SplitLength;
                            Array.Copy(_allReciveDataBuffer, SplitLength, _allReciveDataBuffer, 0, _totalBytesSize);
                            OnRecivePacket(buff.Length, buff);
                        }
                    }
                    else if (SpliterType == EmSpliterType.None)
                    {
                        OnRecivePacket(e.ReceiveBytes, e.ReceiveData);
                    }
                }
                catch (Exception)
                {
                    _socket.Close();
                }
            }
        }

        private void OnRecivePacket(int receiveBytes, byte[] buffer)
        {
            if (OnReceive != null)
                OnReceive(this, new SNetReceiveEventArgs(0, buffer, receiveBytes));
        }
    }
}

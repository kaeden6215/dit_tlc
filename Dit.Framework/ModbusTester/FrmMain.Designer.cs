﻿namespace ModbusTester
{
    partial class FrmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnConnectTcp = new System.Windows.Forms.Button();
            this.btnReadDiscreteInputs = new System.Windows.Forms.Button();
            this.btnReadWrite = new System.Windows.Forms.Button();
            this.btnConnectRTU = new System.Windows.Forms.Button();
            this.tmWorker = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnConnectTcp
            // 
            this.btnConnectTcp.Location = new System.Drawing.Point(12, 12);
            this.btnConnectTcp.Name = "btnConnectTcp";
            this.btnConnectTcp.Size = new System.Drawing.Size(115, 48);
            this.btnConnectTcp.TabIndex = 0;
            this.btnConnectTcp.Text = "Connect(TCP)";
            this.btnConnectTcp.UseVisualStyleBackColor = true;
            this.btnConnectTcp.Click += new System.EventHandler(this.btnConnectTcp_Click);
            // 
            // btnReadDiscreteInputs
            // 
            this.btnReadDiscreteInputs.Location = new System.Drawing.Point(12, 75);
            this.btnReadDiscreteInputs.Name = "btnReadDiscreteInputs";
            this.btnReadDiscreteInputs.Size = new System.Drawing.Size(115, 48);
            this.btnReadDiscreteInputs.TabIndex = 0;
            this.btnReadDiscreteInputs.Text = "ReadDiscreteInputs";
            this.btnReadDiscreteInputs.UseVisualStyleBackColor = true;
            this.btnReadDiscreteInputs.Click += new System.EventHandler(this.btnReadDiscreteInputs_Click);
            // 
            // btnReadWrite
            // 
            this.btnReadWrite.Location = new System.Drawing.Point(12, 159);
            this.btnReadWrite.Name = "btnReadWrite";
            this.btnReadWrite.Size = new System.Drawing.Size(115, 48);
            this.btnReadWrite.TabIndex = 0;
            this.btnReadWrite.Text = "Read Write";
            this.btnReadWrite.UseVisualStyleBackColor = true;
            this.btnReadWrite.Click += new System.EventHandler(this.btnReadWrite_Click);
            // 
            // btnConnectRTU
            // 
            this.btnConnectRTU.Location = new System.Drawing.Point(133, 12);
            this.btnConnectRTU.Name = "btnConnectRTU";
            this.btnConnectRTU.Size = new System.Drawing.Size(115, 48);
            this.btnConnectRTU.TabIndex = 0;
            this.btnConnectRTU.Text = "Connect(RTU)";
            this.btnConnectRTU.UseVisualStyleBackColor = true;
            this.btnConnectRTU.Click += new System.EventHandler(this.btnConnectRTU_Click);
            // 
            // tmWorker
            // 
            this.tmWorker.Tick += new System.EventHandler(this.tmWorker_Tick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(324, 151);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 22);
            this.textBox1.TabIndex = 1;
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(574, 409);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnReadWrite);
            this.Controls.Add(this.btnReadDiscreteInputs);
            this.Controls.Add(this.btnConnectRTU);
            this.Controls.Add(this.btnConnectTcp);
            this.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "FrmMain";
            this.Text = "Modbus Tester";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnectTcp;
        private System.Windows.Forms.Button btnReadDiscreteInputs;
        private System.Windows.Forms.Button btnReadWrite;
        private System.Windows.Forms.Button btnConnectRTU;
        private System.Windows.Forms.Timer tmWorker;
        private System.Windows.Forms.TextBox textBox1;
    }
}


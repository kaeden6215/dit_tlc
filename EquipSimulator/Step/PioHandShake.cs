﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EquipSimulator.Log;
using Dit.Framework.Log;
using EquipSimulator;
using EquipSimulator.Addr;
using Dit.Framework.PLC;

namespace EquipMainUi.Struct
{
    public class PioHandShake
    {
        
        ////HAND SHAKE TO UPPER
        //public PlcAddr LoHeartBit                    /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x0);
        //public PlcAddr LoMachinePause                /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x1);
        //public PlcAddr LoMachineDown                 /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x2);
        //public PlcAddr LoMachineAlarm                /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x3);
        //public PlcAddr LoReceiveAble                 /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x4);
        //public PlcAddr LoReceiveStart                /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x5);
        //public PlcAddr LoReceiveComplete             /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x6);
        //public PlcAddr LoExchangeFlag                /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x7);
        //public PlcAddr LoReturnSendStart             /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x8);
        //public PlcAddr LoReturnSendComplete          /*  */ = new PlcAddr(PlcMemType.R, 3460, 0x9);
        //public PlcAddr LoImmediatelyPauseRequest     /*  */ = new PlcAddr(PlcMemType.R, 3460, 0xA);
        //public PlcAddr LoImmediatelyStopRequest      /*  */ = new PlcAddr(PlcMemType.R, 3460, 0xB);
        //public PlcAddr LoReceiveAbleRemainedStep1    /*  */ = new PlcAddr(PlcMemType.R, 3460, 0xC);
        //public PlcAddr LoReceiveAbleRemainedStep2    /*  */ = new PlcAddr(PlcMemType.R, 3460, 0xD);
        //public PlcAddr LoReceiveAbleRemainedStep3    /*  */ = new PlcAddr(PlcMemType.R, 3460, 0xE);
        //public PlcAddr LoReceiveAbleRemainedStep4    /*  */ = new PlcAddr(PlcMemType.R, 3460, 0xF);
        //
        //public PlcAddr LoGlassIDReadComplete         /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x0);
        //public PlcAddr LoLoadingStop                 /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x1);
        //public PlcAddr LoTransferStop                /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x2);
        //public PlcAddr LoExchangeFailFlag            /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x3);
        //public PlcAddr LoProcessTimeUp               /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x4);
        ////public	 	PlcAddr	UPReserved	            /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x5);
        ////public	 	PlcAddr	UPReserved	            /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x6);
        //public PlcAddr LoReceiveAbleReserveRequest   /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x7);
        //public PlcAddr LoHandShakeCancelRequest      /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x8);
        //public PlcAddr LoHandShakeAbortRequest       /*  */ = new PlcAddr(PlcMemType.R, 3461, 0x9);
        //public PlcAddr LoHandShakeResumeRequest      /*  */ = new PlcAddr(PlcMemType.R, 3461, 0xA);
        //public PlcAddr LoHandShakeRecoveryAckReply   /*  */ = new PlcAddr(PlcMemType.R, 3461, 0xB);
        //public PlcAddr LoHandShakeRecoveryNakReply   /*  */ = new PlcAddr(PlcMemType.R, 3461, 0xC);
        //public PlcAddr LoReceiveJobReady             /*  */ = new PlcAddr(PlcMemType.R, 3461, 0xD);
        //public PlcAddr LoReceiveActionMove           /*  */ = new PlcAddr(PlcMemType.R, 3461, 0xE);
        //public PlcAddr LoReceiveActionRemove         /*  */ = new PlcAddr(PlcMemType.R, 3461, 0xF);
        //
        ////Upper	Contact	Point	        
        //public PlcAddr LoAbnormal                    /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x0);
        //public PlcAddr LoTypeofArm                   /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x1);
        //public PlcAddr LoTypeofStageConveyor         /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x2);
        //public PlcAddr LoArmStretchUpMoving          /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x3);
        //public PlcAddr LoArmStretchUpComplete        /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x4);
        //public PlcAddr LoArmStretchDownMoving        /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x5);
        //public PlcAddr LoArmStretchDownComplete      /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x6);
        //public PlcAddr LoArmStretching               /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x7);
        //public PlcAddr LoArmStretchComplete          /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x8);
        //public PlcAddr LoArmFolding                  /*  */ = new PlcAddr(PlcMemType.R, 3462, 0x9);
        //public PlcAddr LoArmFoldComplete             /*  */ = new PlcAddr(PlcMemType.R, 3462, 0xA);
        ////public   PlcAddr LoReserved                  /*  */ = new PlcAddr(PlcMemType.R, 3462, 0xB);
        ////public   PlcAddr LoReserved                  /*  */ = new PlcAddr(PlcMemType.R, 3462, 0xC);
        //public PlcAddr LoArm1Folded                  /*  */ = new PlcAddr(PlcMemType.R, 3462, 0xD);
        //public PlcAddr LoArm2Folded                  /*  */ = new PlcAddr(PlcMemType.R, 3462, 0xE);
        //public PlcAddr LoArm1GlassDetect             /*  */ = new PlcAddr(PlcMemType.R, 3462, 0xF);
        //
        //public PlcAddr LoArm2GlassDetect             /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x0);
        //public PlcAddr LoArm1GlassVacuum             /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x1);
        //public PlcAddr LoArm2GlassVacuum             /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x2);
        //public PlcAddr LoRobotDirection              /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x3);
        //public PlcAddr LoManualOperation             /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x4);
        //public PlcAddr LoPinUp                       /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x5);
        //public PlcAddr LoPinDown                     /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x6);
        //public PlcAddr LoDoorOpen                    /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x7);
        //public PlcAddr LoDoorClose                   /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x8);
        //public PlcAddr LoGlassDetect                 /*  */ = new PlcAddr(PlcMemType.R, 3463, 0x9);
        //public PlcAddr LoBodyMoving                  /*  */ = new PlcAddr(PlcMemType.R, 3463, 0xA);
        //public PlcAddr LoBodyOriginPosition          /*  */ = new PlcAddr(PlcMemType.R, 3463, 0xB);
        //public PlcAddr LoEmergency                   /*  */ = new PlcAddr(PlcMemType.R, 3463, 0xC);
        //public PlcAddr LoVertical                    /*  */ = new PlcAddr(PlcMemType.R, 3463, 0xD);
        //public PlcAddr LoHorizontal                  /*  */ = new PlcAddr(PlcMemType.R, 3463, 0xE);
        //
        ////Lower	EQ						         
        //public PlcAddr UpHeartBit                    /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x0);
        //public PlcAddr UpMachinePause                /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x1);
        //public PlcAddr UpMachineDown                 /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x2);
        //public PlcAddr UpMachineAlarm                /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x3);
        //public PlcAddr UpSendAble                    /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x4);
        //public PlcAddr UpSendStart                   /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x5);
        //public PlcAddr UpSendComplete                /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x6);
        //public PlcAddr UpExchangeFlag                /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x7);
        //public PlcAddr UpReturnReceiveStart          /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x8);
        //public PlcAddr UpReturnReceiveComplete       /*  */ = new PlcAddr(PlcMemType.R, 3464, 0x9);
        //public PlcAddr UpImmediatelyPauseRequest     /*  */ = new PlcAddr(PlcMemType.R, 3464, 0xA);
        //public PlcAddr UpImmediatelyStopRequest      /*  */ = new PlcAddr(PlcMemType.R, 3464, 0xB);
        //public PlcAddr UpSendAbleRemainedStep1       /*  */ = new PlcAddr(PlcMemType.R, 3464, 0xC);
        //public PlcAddr UpSendAbleRemainedStep2       /*  */ = new PlcAddr(PlcMemType.R, 3464, 0xD);
        //public PlcAddr UpSendAbleRemainedStep3       /*  */ = new PlcAddr(PlcMemType.R, 3464, 0xE);
        //public PlcAddr UpSendAbleRemainedStep4       /*  */ = new PlcAddr(PlcMemType.R, 3464, 0xF);
        //
        //public PlcAddr UpWorkStart                   /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x0);
        //public PlcAddr UpWorkCancel                  /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x1);
        //public PlcAddr UpWorkSkip                    /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x2);
        //public PlcAddr UpJobStart                    /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x3);
        //public PlcAddr UpJobEnd                      /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x4);
        //public PlcAddr UpHotFlow                     /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x5);
        ////public	 	PlcAddr	LOReserved	         /*  */ = new PlcAddr(PlcMemType.R, 3465,  0x6);
        //public PlcAddr UpSendAbleReserveRequest      /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x7);
        //public PlcAddr UpHandShakeCancelRequest      /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x8);
        //public PlcAddr UpHandShakeAbortRequest       /*  */ = new PlcAddr(PlcMemType.R, 3465, 0x9);
        //public PlcAddr UpHandShakeResumeRequest      /*  */ = new PlcAddr(PlcMemType.R, 3465, 0xA);
        //public PlcAddr UpHandShakeRecoveryAckReply   /*  */ = new PlcAddr(PlcMemType.R, 3465, 0xB);
        //public PlcAddr UpHandShakeRecoveryNakReply   /*  */ = new PlcAddr(PlcMemType.R, 3465, 0xC);
        //public PlcAddr UpSendJobReady                /*  */ = new PlcAddr(PlcMemType.R, 3465, 0xD);
        //public PlcAddr UpSendActionMove              /*  */ = new PlcAddr(PlcMemType.R, 3465, 0xE);
        //public PlcAddr UpSendActionRemove            /*  */ = new PlcAddr(PlcMemType.R, 3465, 0xF);
        //
        //
        ////Lower	Contact	Point		
        //public PlcAddr UpAbnormal                    /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x0);
        //public PlcAddr UpTypeofArm                   /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x1);
        //public PlcAddr UpTypeofStageConveyor         /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x2);
        //public PlcAddr UpArmStretchUpMoving          /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x3);
        //public PlcAddr UpArmStretchUpComplete        /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x4);
        //public PlcAddr UpArmStretchDownMoving        /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x5);
        //public PlcAddr UpArmStretchDownComplete      /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x6);
        //public PlcAddr UpArmStretching               /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x7);
        //public PlcAddr UpArmStretchComplete          /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x8);
        //public PlcAddr UpArmFolding                  /*  */ = new PlcAddr(PlcMemType.R, 3466, 0x9);
        //public PlcAddr UpArmFoldComplete             /*  */ = new PlcAddr(PlcMemType.R, 3466, 0xA);
        ////public  	PlcAddr	LOReserved	                /*  */ = new PlcAddr(PlcMemType.R, 3466,  0xB);
        ////public  	PlcAddr	LOReserved	                /*  */ = new PlcAddr(PlcMemType.R, 3466,  0xC);
        //public PlcAddr UpArm1Folded                  /*  */ = new PlcAddr(PlcMemType.R, 3466, 0xD);
        //public PlcAddr UpArm2Folded                  /*  */ = new PlcAddr(PlcMemType.R, 3466, 0xE);
        //public PlcAddr UpArm1GlassDetect             /*  */ = new PlcAddr(PlcMemType.R, 3466, 0xF);
        //
        //public PlcAddr UpArm2GlassDetect             /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x0);
        //public PlcAddr UpArm1GlassVacuum             /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x1);
        //public PlcAddr UpArm2GlassVacuum             /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x2);
        //public PlcAddr UpRobotDirection              /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x3);
        //public PlcAddr UpManualOperation             /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x4);
        //public PlcAddr UpPinUp                       /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x5);
        //public PlcAddr UpPinDown                     /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x6);
        //public PlcAddr UpDoorOpen                    /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x7);
        //public PlcAddr UpDoorClose                   /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x8);
        //public PlcAddr UpGlassDetect                 /*  */ = new PlcAddr(PlcMemType.R, 3467, 0x9);
        //public PlcAddr UpBodyMoving                  /*  */ = new PlcAddr(PlcMemType.R, 3467, 0xA);
        //public PlcAddr UpBodyOriginPosition          /*  */ = new PlcAddr(PlcMemType.R, 3467, 0xB);
        //public PlcAddr UpEmergency                   /*  */ = new PlcAddr(PlcMemType.R, 3467, 0xC);
        //public PlcAddr UpVertical                    /*  */ = new PlcAddr(PlcMemType.R, 3467, 0xD);
        //public PlcAddr UpHorizontal                  /*  */ = new PlcAddr(PlcMemType.R, 3467, 0xE);

        private bool _isAoiToIndexComplete = false;
        public void StartAoiToIndex()
        {
            _isAoiToIndexComplete = false;
            StepPioSend = 10;
        }
        public bool IsAoiToIndexComplete()
        {
            return _isAoiToIndexComplete && StepPioSend == 0;
        }

        private bool _isIndexToAoiComplete = false;
        public void StartIndexToAoi()
        {


            //Glass 감지 OFF, Auto Mode ON, Heavy Alarm OFF, Pause OFF
            //Cycle Stop OFF
            _isIndexToAoiComplete = false;
            StepPioRecv = 10;

        }
        public bool IsIndexToAoiComplete()
        {
            return _isIndexToAoiComplete && StepPioRecv == 0;
        }

        private bool _isAoiToIndexExchangeComplete = false;
        public void StartAoiToIndexExchange()
        {
            _isAoiToIndexExchangeComplete = false;
            StepPioAoiToIndexExchage = 10;
        }
        public bool IsAoiToIndexExchangeComplete()
        {
            return _isAoiToIndexExchangeComplete && StepPioAoiToIndexExchage == 0;
        }
        public int StepPioSend = 0;
        private PlcTimer UpperToLowerDelay = new PlcTimer();
        public void LogPioSend(EquipSimul equip)
        {
            //A --> I
            if (StepPioSend == 0)
            {
                
            }
            else if (StepPioSend == 10)
            {
                //if (equip.IsGlassCheckOk)
                StepPioSend = 20;
            }
            else if (StepPioSend == 20)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPExchangeFailFlag == false)
                {
                    CIM_ADDR.LOSendAble.vBit = true;
                    StepPioSend = 30;
                }
            }
            else if (StepPioSend == 30)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true)
                {
                    CIM_ADDR.LOSendAble.vBit = true;
                    CIM_ADDR.LOSendStart.vBit = true;
                    StepPioSend = 40;
                }
            }
            else if (StepPioSend == 40)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 이동 시작");
                    UpperToLowerDelay.Start(3);
                    StepPioSend = 41;
                }
            }
            else if (StepPioSend == 41)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true && UpperToLowerDelay)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 설비 진입 시작");
                    equip.ROBOT_ARM_DETECT.OnOff(false);
                    UpperToLowerDelay.Stop();
                    UpperToLowerDelay.Start(2);
                    StepPioSend = 42;
                }
            }
            else if (StepPioSend == 42)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true && UpperToLowerDelay)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 설비 글라스 투입");

                    equip.ROBOT_ARM_DETECT.OnOff(false);
                    equip.GLASS_DETECT_SENSOR_1.OnOff(true);

                    UpperToLowerDelay.Stop();
                    UpperToLowerDelay.Start(5);
                    StepPioSend = 50;
                }
            }
            else if (StepPioSend == 50)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true && UpperToLowerDelay)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 설비 진입 종료");
                    equip.ROBOT_ARM_DETECT.OnOff(true);
                    UpperToLowerDelay.Stop();
                    CIM_ADDR.LOSendComplete.vBit = true;
                    StepPioSend = 60;
                }
            }
            else if (StepPioSend == 60)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true && AOI_ADDR.UPReceiveComplete == true)
                {
                    CIM_ADDR.LOSendAble.vBit = false;
                    CIM_ADDR.LOSendStart.vBit = false;
                    CIM_ADDR.LOSendComplete.vBit = false;

                    StepPioSend = 70;
                }
            }
            else if (StepPioSend == 70)
            {
                if (AOI_ADDR.UPReceiveAble == false && AOI_ADDR.UPReceiveStart == false && AOI_ADDR.UPReceiveComplete == false)
                {
                    StepPioSend = 80;
                }
            }
            else if (StepPioSend == 80)
            {
                //Logger.Log.AppendLine(LogLevel.Info, "Glass UnLoading");
                //equip.ROBOT_ARM_DETECT.OnOff(true);
                StepPioSend = 0;               
            }

        }




        public int StepPioRecv = 0;
        private PlcTimer IndexToAoiDelay = new PlcTimer();
        public void LogPioRecv(EquipSimul equip)
        {
            if (StepPioRecv == 0)
            {
            }
            else if (StepPioRecv == 10)
            {
                StepPioRecv = 20;
            }
            else if (StepPioRecv == 20)
            {
                if (AOI_ADDR.LOSendAble == true)
                {
                    CIM_ADDR.UPReceiveAble.vBit = true;
                    StepPioRecv = 30;
                }
            }
            else if (StepPioRecv == 30)
            {
                if (AOI_ADDR.LOSendAble == true)
                {
                    CIM_ADDR.UPReceiveAble.vBit = true;
                    IndexToAoiDelay.Start(2);
                    StepPioRecv = 35;
                }
            }
            else if (StepPioRecv == 35)
            {
                if (AOI_ADDR.LOSendAble == true && IndexToAoiDelay)
                {
                    IndexToAoiDelay.Stop();
                    CIM_ADDR.UPReceiveAble.vBit = true;
                    CIM_ADDR.UPReceiveStart.vBit = true;
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 이동 시작");
                    StepPioRecv = 40;
                }

            }
            else if (StepPioRecv == 40)
            {
                if (AOI_ADDR.LOSendAble == true && AOI_ADDR.LOSendStart == true)
                {
                    CIM_ADDR.UPReceiveAble.vBit = true;
                    CIM_ADDR.UPReceiveStart.vBit = true;
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 이동 대기");
                    IndexToAoiDelay.Start(3);
                    StepPioRecv = 50;
                }
            }
            else if (StepPioRecv == 50)
            {
                if (AOI_ADDR.LOSendAble == true && AOI_ADDR.LOSendStart == true && IndexToAoiDelay)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 설비 진입 시작");
                    equip.ROBOT_ARM_DETECT.OnOff(false);
                    IndexToAoiDelay.Stop();
                    IndexToAoiDelay.Start(2);
                    StepPioRecv = 60;
                }
            }
            else if (StepPioRecv == 60)
            {
                if (AOI_ADDR.LOSendAble == true && AOI_ADDR.LOSendStart == true && IndexToAoiDelay)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 설비 진입 시작");
                    equip.ROBOT_ARM_DETECT.OnOff(false);
                    IndexToAoiDelay.Stop();
                    IndexToAoiDelay.Start(2);
                    StepPioRecv = 70;
                }
            }
            else if (StepPioRecv == 70)
            {
                if (AOI_ADDR.LOSendAble == true && AOI_ADDR.LOSendStart == true && IndexToAoiDelay)
                {
                    IndexToAoiDelay.Stop();
                    equip.GLASS_DETECT_SENSOR_1.OnOff(false);

                    IndexToAoiDelay.Start(2);
                    StepPioRecv = 75;
                }
            }
            else if (StepPioRecv == 75)
            {
                if (AOI_ADDR.LOSendAble == true && AOI_ADDR.LOSendStart == true && IndexToAoiDelay)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 설비 진입 종료");
                    equip.ROBOT_ARM_DETECT.OnOff(true);

                    CIM_ADDR.UPReceiveAble.vBit = true;
                    CIM_ADDR.UPReceiveStart.vBit = true;
                    CIM_ADDR.UPReceiveComplete.vBit = true;
                    IndexToAoiDelay.Stop();
                    IndexToAoiDelay.Start(2);
                    StepPioRecv = 80;
                }
            }
            else if (StepPioRecv == 80)
            {
                if (AOI_ADDR.LOSendAble == true && AOI_ADDR.LOSendStart == true && AOI_ADDR.LOSendComplete == true && IndexToAoiDelay)
                {
                    IndexToAoiDelay.Stop();

                    CIM_ADDR.UPReceiveAble.vBit = false;
                    CIM_ADDR.UPReceiveStart.vBit = false;
                    CIM_ADDR.UPReceiveComplete.vBit = false;

                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 이동 종료");
                    StepPioRecv = 90;
                }
            }
            else if (StepPioRecv == 90)
            {
                if (AOI_ADDR.LOSendAble == false && AOI_ADDR.LOSendStart == false && AOI_ADDR.LOSendComplete == false && IndexToAoiDelay)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "인덱서 이동 종료");

                    StepPioRecv = 100;
                }
            }


            else if (StepPioRecv == 100)
            {
                //Logger.Log.AppendLine(LogLevel.Info, "Glass Loading");
                StepPioRecv = 0;
            }

        }

        public int StepPioAoiToIndexExchage = 0;
        private PlcTimer ExchangeDelay = new PlcTimer();
        public void LogPioExchangeStep()
        {
            //A --> I
            if (StepPioAoiToIndexExchage == 0)
            {
            }
            else if (StepPioAoiToIndexExchage == 10)
            {
                //if (equip.IsGlassCheckOk)
                StepPioSend = 20;
            }
            else if (StepPioSend == 20)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPExchangeFailFlag == true)
                {
                    CIM_ADDR.LOSendAble.vBit = true;
                    StepPioSend = 30;
                }
            }
            else if (StepPioSend == 30)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPExchangeFailFlag == true)
                {
                    CIM_ADDR.LOExchangeFlag.vBit = true;
                    CIM_ADDR.LOSendAble.vBit = true;
                    CIM_ADDR.LOSendStart.vBit = true;

                    StepPioSend = 40;
                }
            }
            else if (StepPioSend == 40)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true && AOI_ADDR.UPExchangeFailFlag == true)
                {
                    CIM_ADDR.LOExchangeFlag.vBit = true;
                    CIM_ADDR.LOSendAble.vBit = true;
                    CIM_ADDR.LOSendStart.vBit = true;

                    StepPioSend = 50;
                }
            }
            else if (StepPioSend == 50)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true && AOI_ADDR.UPReceiveComplete == true &&
                    AOI_ADDR.UPExchangeFailFlag == true)
                {
                    CIM_ADDR.LOExchangeFlag.vBit = true;
                    CIM_ADDR.LOSendAble.vBit = true;
                    CIM_ADDR.LOSendStart.vBit = true;

                    ExchangeDelay.Start(0, 100);
                    StepPioSend = 60;
                }
            }
            else if (StepPioSend == 60)
            {
                if (AOI_ADDR.UPReceiveAble == true && AOI_ADDR.UPReceiveStart == true && AOI_ADDR.UPReceiveComplete == true)
                {
                    CIM_ADDR.LOExchangeFlag.vBit = true;
                    CIM_ADDR.LOSendAble.vBit = true;
                    CIM_ADDR.LOSendStart.vBit = true;
                    CIM_ADDR.LOSendComplete.vBit = true;
                    StepPioSend = 70;
                }
            }
            else if (StepPioSend == 60)
            {

            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;


namespace EquipSimulator.Addr
{
    class CIM_ADDR
    {
        //HAND SHAKE TO UPPER
        public static PlcAddr UPHeartBit                    /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x0);
        public static PlcAddr UPMachinePause                /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x1);
        public static PlcAddr UPMachineDown                 /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x2);
        public static PlcAddr UPMachineAlarm                /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x3);
        public static PlcAddr UPReceiveAble                 /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x4);
        public static PlcAddr UPReceiveStart                /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x5);
        public static PlcAddr UPReceiveComplete             /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x6);
        public static PlcAddr UPExchangeFlag                /*  */ = new PlcAddr(PlcMemType.S, 5000, 0x7);
        public static PlcAddr UPReturnSendStart             /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x0);
        public static PlcAddr UPReturnSendComplete          /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x1);
        public static PlcAddr UPImmediatelyPauseRequest     /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x2);
        public static PlcAddr UPImmediatelyStopRequest      /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x3);
        public static PlcAddr UPReceiveAbleRemainedStep1    /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x4);
        public static PlcAddr UPReceiveAbleRemainedStep2    /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x5);
        public static PlcAddr UPReceiveAbleRemainedStep3    /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x6);
        public static PlcAddr UPReceiveAbleRemainedStep4    /*  */ = new PlcAddr(PlcMemType.S, 5001, 0x7);

        public static PlcAddr UPGlassIDReadComplete         /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x0);
        public static PlcAddr UPLoadingStop                 /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x1);
        public static PlcAddr UPTransferStop                /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x2);
        public static PlcAddr UPExchangeFailFlag            /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x3);
        public static PlcAddr UPProcessTimeUp               /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x4);
        public static PlcAddr UPReserved1	                /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x5);
        public static PlcAddr UPReserved2	                /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x6);
        public static PlcAddr UPReceiveAbleReserveRequest   /*  */ = new PlcAddr(PlcMemType.S, 5002, 0x7);
        public static PlcAddr UPHandShakeCancelRequest      /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x0);
        public static PlcAddr UPHandShakeAbortRequest       /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x1);
        public static PlcAddr UPHandShakeResumeRequest      /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x2);
        public static PlcAddr UPHandShakeRecoveryAckReply   /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x3);
        public static PlcAddr UPHandShakeRecoveryNakReply   /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x4);
        public static PlcAddr UPReceiveJobReady             /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x5);
        public static PlcAddr UPReceiveActionMove           /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x6);
        public static PlcAddr UPReceiveActionRemove         /*  */ = new PlcAddr(PlcMemType.S, 5003, 0x7);

        //Upper	Contact	Point	        
        public static PlcAddr UPAbnormal                    /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x0);
        public static PlcAddr UPTypeofArm                   /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x1);
        public static PlcAddr UPTypeofStageConveyor         /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x2);
        public static PlcAddr UPArmStretchUpMoving          /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x3);
        public static PlcAddr UPArmStretchUpComplete        /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x4);
        public static PlcAddr UPArmStretchDownMoving        /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x5);
        public static PlcAddr UPArmStretchDownComplete      /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x6);
        public static PlcAddr UPArmStretching               /*  */ = new PlcAddr(PlcMemType.S, 5004, 0x7);
        public static PlcAddr UPArmStretchComplete          /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x0);
        public static PlcAddr UPArmFolding                  /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x1);
        public static PlcAddr UPArmFoldComplete             /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x2);
        //public static PlcAddr UPReserved                  /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x3);
        //public static PlcAddr UPReserved                  /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x4);
        public static PlcAddr UPArm1Folded                  /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x5);
        public static PlcAddr UPArm2Folded                  /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x6);
        public static PlcAddr UPArm1GlassDetect             /*  */ = new PlcAddr(PlcMemType.S, 5005, 0x7);

        public static PlcAddr UPArm2GlassDetect             /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x0);
        public static PlcAddr UPArm1GlassVacuum             /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x1);
        public static PlcAddr UPArm2GlassVacuum             /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x2);
        public static PlcAddr UPRobotDirection              /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x3);
        public static PlcAddr UPManualOperation             /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x4);
        public static PlcAddr UPPinUp                       /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x5);
        public static PlcAddr UPPinDown                     /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x6);
        public static PlcAddr UPDoorOpen                    /*  */ = new PlcAddr(PlcMemType.S, 5006, 0x7);
        public static PlcAddr UPDoorClose                   /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x0);
        public static PlcAddr UPGlassDetect                 /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x1);
        public static PlcAddr UPBodyMoving                  /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x2);
        public static PlcAddr UPBodyOriginPosition          /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x3);
        public static PlcAddr UPEmergency                   /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x4);
        public static PlcAddr UPVertical                    /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x5);
        public static PlcAddr UPHorizontal                  /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x6);
        public static PlcAddr UPCpReserved3                 /*  */ = new PlcAddr(PlcMemType.S, 5007, 0x7);

        //Lower	EQ						         
        public static PlcAddr LOHeartBit                    /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x0);
        public static PlcAddr LOMachinePause                /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x1);
        public static PlcAddr LOMachineDown                 /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x2);
        public static PlcAddr LOMachineAlarm                /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x3);
        public static PlcAddr LOSendAble                    /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x4);
        public static PlcAddr LOSendStart                   /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x5);
        public static PlcAddr LOSendComplete                /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x6);
        public static PlcAddr LOExchangeFlag                /*  */ = new PlcAddr(PlcMemType.S, 5008, 0x7);
        public static PlcAddr LOReturnReceiveStart          /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x0);
        public static PlcAddr LOReturnReceiveComplete       /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x1);
        public static PlcAddr LOImmediatelyPauseRequest     /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x2);
        public static PlcAddr LOImmediatelyStopRequest      /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x3);
        public static PlcAddr LOSendAbleRemainedStep1       /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x4);
        public static PlcAddr LOSendAbleRemainedStep2       /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x5);
        public static PlcAddr LOSendAbleRemainedStep3       /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x6);
        public static PlcAddr LOSendAbleRemainedStep4       /*  */ = new PlcAddr(PlcMemType.S, 5009, 0x7);

        public static PlcAddr LOWorkStart                   /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x0);
        public static PlcAddr LOWorkCancel                  /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x1);
        public static PlcAddr LOWorkSkip                    /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x2);
        public static PlcAddr LOJobStart                    /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x3);
        public static PlcAddr LOJobEnd                      /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x4);
        public static PlcAddr LOHotFlow                     /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x5);
        public static PlcAddr LOReserved	                /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x6);
        public static PlcAddr LOSendAbleReserveRequest      /*  */ = new PlcAddr(PlcMemType.S, 5010, 0x7);
        public static PlcAddr LOHandShakeCancelRequest      /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x0);
        public static PlcAddr LOHandShakeAbortRequest       /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x1);
        public static PlcAddr LOHandShakeResumeRequest      /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x2);
        public static PlcAddr LOHandShakeRecoveryAckReply   /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x3);
        public static PlcAddr LOHandShakeRecoveryNakReply   /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x4);
        public static PlcAddr LOSendJobReady                /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x5);
        public static PlcAddr LOSendActionMove              /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x6);
        public static PlcAddr LOSendActionRemove            /*  */ = new PlcAddr(PlcMemType.S, 5011, 0x7);


        //Lower	Contact	Point		
        public static PlcAddr LOAbnormal                    /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x0);
        public static PlcAddr LOTypeofArm                   /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x1);
        public static PlcAddr LOTypeofStageConveyor         /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x2);
        public static PlcAddr LOArmStretchUpMoving          /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x3);
        public static PlcAddr LOArmStretchUpComplete        /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x4);
        public static PlcAddr LOArmStretchDownMoving        /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x5);
        public static PlcAddr LOArmStretchDownComplete      /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x6);
        public static PlcAddr LOArmStretching               /*  */ = new PlcAddr(PlcMemType.S, 5012, 0x7);
        public static PlcAddr LOArmStretchComplete          /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x0);
        public static PlcAddr LOArmFolding                  /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x1);
        public static PlcAddr LOArmFoldComplete             /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x2);
        public static PlcAddr LOReserved1	                /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x3);
        public static PlcAddr LOReserved2	                /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x4);
        public static PlcAddr LOArm1Folded                  /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x5);
        public static PlcAddr LOArm2Folded                  /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x6);
        public static PlcAddr LOArm1GlassDetect             /*  */ = new PlcAddr(PlcMemType.S, 5013, 0x7);

        public static PlcAddr LOArm2GlassDetect             /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x0);
        public static PlcAddr LOArm1GlassVacuum             /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x1);
        public static PlcAddr LOArm2GlassVacuum             /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x2);
        public static PlcAddr LORobotDirection              /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x3);
        public static PlcAddr LOManualOperation             /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x4);
        public static PlcAddr LOPinUp                       /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x5);
        public static PlcAddr LOPinDown                     /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x6);
        public static PlcAddr LODoorOpen                    /*  */ = new PlcAddr(PlcMemType.S, 5014, 0x7);
        public static PlcAddr LODoorClose                   /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x0);
        public static PlcAddr LOGlassDetect                 /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x1);
        public static PlcAddr LOBodyMoving                  /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x2);
        public static PlcAddr LOBodyOriginPosition          /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x3);
        public static PlcAddr LOEmergency                   /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x4);
        public static PlcAddr LOVertical                    /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x5);
        public static PlcAddr LOHorizontal                  /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x6);
        public static PlcAddr LOReserved3                   /*  */ = new PlcAddr(PlcMemType.S, 5015, 0x7);

        public static void Initailize(IVirtualMem plc)
        {
            UPHeartBit.PLC = plc;
            UPMachinePause.PLC = plc;
            UPMachineDown.PLC = plc;
            UPMachineAlarm.PLC = plc;
            UPReceiveAble.PLC = plc;
            UPReceiveStart.PLC = plc;
            UPReceiveComplete.PLC = plc;
            UPExchangeFlag.PLC = plc;
            UPReturnSendStart.PLC = plc;
            UPReturnSendComplete.PLC = plc;
            UPImmediatelyPauseRequest.PLC = plc;
            UPImmediatelyStopRequest.PLC = plc;
            UPReceiveAbleRemainedStep1.PLC = plc;
            UPReceiveAbleRemainedStep2.PLC = plc;
            UPReceiveAbleRemainedStep3.PLC = plc;
            UPReceiveAbleRemainedStep4.PLC = plc;

            UPGlassIDReadComplete.PLC = plc;
            UPLoadingStop.PLC = plc;
            UPTransferStop.PLC = plc;
            UPExchangeFailFlag.PLC = plc;
            UPProcessTimeUp.PLC = plc;
            UPReserved1.PLC = plc;
            UPReserved2.PLC = plc;
            UPReceiveAbleReserveRequest.PLC = plc;
            UPHandShakeCancelRequest.PLC = plc;
            UPHandShakeAbortRequest.PLC = plc;
            UPHandShakeResumeRequest.PLC = plc;
            UPHandShakeRecoveryAckReply.PLC = plc;
            UPHandShakeRecoveryNakReply.PLC = plc;
            UPReceiveJobReady.PLC = plc;
            UPReceiveActionMove.PLC = plc;
            UPReceiveActionRemove.PLC = plc;

            UPAbnormal.PLC = plc;
            UPTypeofArm.PLC = plc;
            UPTypeofStageConveyor.PLC = plc;
            UPArmStretchUpMoving.PLC = plc;
            UPArmStretchUpComplete.PLC = plc;
            UPArmStretchDownMoving.PLC = plc;
            UPArmStretchDownComplete.PLC = plc;
            UPArmStretching.PLC = plc;
            UPArmStretchComplete.PLC = plc;
            UPArmFolding.PLC = plc;
            UPArmFoldComplete.PLC = plc;
            UPReserved1.PLC = plc;
            UPReserved2.PLC = plc;
            UPArm1Folded.PLC = plc;
            UPArm2Folded.PLC = plc;
            UPArm1GlassDetect.PLC = plc;

            UPArm2GlassDetect.PLC = plc;
            UPArm1GlassVacuum.PLC = plc;
            UPArm2GlassVacuum.PLC = plc;
            UPRobotDirection.PLC = plc;
            UPManualOperation.PLC = plc;
            UPPinUp.PLC = plc;
            UPPinDown.PLC = plc;
            UPDoorOpen.PLC = plc;
            UPDoorClose.PLC = plc;
            UPGlassDetect.PLC = plc;
            UPBodyMoving.PLC = plc;
            UPBodyOriginPosition.PLC = plc;
            UPEmergency.PLC = plc;
            UPVertical.PLC = plc;
            UPHorizontal.PLC = plc;
            UPCpReserved3.PLC = plc;

            LOHeartBit.PLC = plc;
            LOMachinePause.PLC = plc;
            LOMachineDown.PLC = plc;
            LOMachineAlarm.PLC = plc;
            LOSendAble.PLC = plc;
            LOSendStart.PLC = plc;
            LOSendComplete.PLC = plc;
            LOExchangeFlag.PLC = plc;
            LOReturnReceiveStart.PLC = plc;
            LOReturnReceiveComplete.PLC = plc;
            LOImmediatelyPauseRequest.PLC = plc;
            LOImmediatelyStopRequest.PLC = plc;
            LOSendAbleRemainedStep1.PLC = plc;
            LOSendAbleRemainedStep2.PLC = plc;
            LOSendAbleRemainedStep3.PLC = plc;
            LOSendAbleRemainedStep4.PLC = plc;

            LOWorkStart.PLC = plc;
            LOWorkCancel.PLC = plc;
            LOWorkSkip.PLC = plc;
            LOJobStart.PLC = plc;
            LOJobEnd.PLC = plc;
            LOHotFlow.PLC = plc;
            LOReserved.PLC = plc;
            LOSendAbleReserveRequest.PLC = plc;
            LOHandShakeCancelRequest.PLC = plc;
            LOHandShakeAbortRequest.PLC = plc;
            LOHandShakeResumeRequest.PLC = plc;
            LOHandShakeRecoveryAckReply.PLC = plc;
            LOHandShakeRecoveryNakReply.PLC = plc;
            LOSendJobReady.PLC = plc;
            LOSendActionMove.PLC = plc;
            LOSendActionRemove.PLC = plc;

            LOAbnormal.PLC = plc;
            LOTypeofArm.PLC = plc;
            LOTypeofStageConveyor.PLC = plc;
            LOArmStretchUpMoving.PLC = plc;
            LOArmStretchUpComplete.PLC = plc;
            LOArmStretchDownMoving.PLC = plc;
            LOArmStretchDownComplete.PLC = plc;
            LOArmStretching.PLC = plc;
            LOArmStretchComplete.PLC = plc;
            LOArmFolding.PLC = plc;
            LOArmFoldComplete.PLC = plc;
            LOReserved1.PLC = plc;
            LOReserved2.PLC = plc;
            LOArm1Folded.PLC = plc;
            LOArm2Folded.PLC = plc;
            LOArm1GlassDetect.PLC = plc;

            LOArm2GlassDetect.PLC = plc;
            LOArm1GlassVacuum.PLC = plc;
            LOArm2GlassVacuum.PLC = plc;
            LORobotDirection.PLC = plc;
            LOManualOperation.PLC = plc;
            LOPinUp.PLC = plc;
            LOPinDown.PLC = plc;
            LODoorOpen.PLC = plc;
            LODoorClose.PLC = plc;
            LOGlassDetect.PLC = plc;
            LOBodyMoving.PLC = plc;
            LOBodyOriginPosition.PLC = plc;
            LOEmergency.PLC = plc;
            LOVertical.PLC = plc;
            LOHorizontal.PLC = plc;
            LOReserved3.PLC = plc;

        }
    }
}

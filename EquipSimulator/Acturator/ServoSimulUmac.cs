﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;
using EquipSimulator.Log;
using Dit.Framework.Log;
using Dit.Framework.PLC;


namespace EquipSimulator.Acturator
{
    public class ServoSimulUmac
    {
        public EM_MOTOR_TYPE MotorType { get; set; }
        public int InnerAxisNo { get; set; }
        public int OutterAxisNo { get; set; }
        public VirtualMem PLC;
        public string Name;
        public int Axis;

        public PlcAddr XB_StatusHomeCompleteBit { get; set; }
        public PlcAddr XB_StatusHomeInPosition { get; set; }
        public PlcAddr XB_StatusMotorMoving { get; set; }
        public PlcAddr XB_StatusMotorInPosition { get; set; }
        public PlcAddr XB_StatusNegativeLimitSet { get; set; }
        public PlcAddr XB_StatusPositiveLimitSet { get; set; }
        public PlcAddr XB_StatusMotorServoOn { get; set; }
        public PlcAddr XB_ErrFatalFollowingError { get; set; }
        public PlcAddr XB_ErrAmpFaultError { get; set; }
        public PlcAddr XB_ErrI2TAmpFaultError { get; set; }
        public PlcAddr XF_CurrMotorActualLoad { get; set; }
        public PlcAddr XF_CurrMotorPosition { get; set; }
        public PlcAddr XF_CurrMotorSpeed { get; set; }
        public PlcAddr XF_CurrMotorStress { get; set; }


        //서보 OnOff Cmd
        public PlcAddr YB_ServoOnOffCmd { get; set; }
        public PlcAddr YB_ServoOnOff { get; set; }
        public PlcAddr XB_ServoOnOffCmdAck { get; set; }

        //서보 Motor Stop
        public PlcAddr YB_MotorStopCmd { get; set; }
        public PlcAddr XB_MotorStopCmdAck { get; set; }

        //Point To Point Move Cmd        
        public PlcAddr YB_PTPMoveCmd { get; set; }
        public PlcAddr XB_PTPMoveCmdAck { get; set; }
        public PlcAddr YF_PTPMovePosition { get; set; }
        public PlcAddr XF_PTPMovePositionAck { get; set; }
        public PlcAddr YF_PTPMoveSpeed { get; set; }
        public PlcAddr XF_PTPMoveSpeedAck { get; set; }
        public PlcAddr YF_PTPMoveAccel { get; set; }
        public PlcAddr XF_PTPMoveAccelAck { get; set; }

        public PlcAddr YB_HomeCmd { get; set; }
        public PlcAddr XB_HomeCmdAck { get; set; }

        public PlcAddr YB_MotorJogMinusMove { get; set; }
        public PlcAddr YB_MotorJogPlusMove { get; set; }
        public PlcAddr YF_MotorJogSpeedCmd { get; set; }
        public PlcAddr XF_MotorJogSpeedCmdAck { get; set; }

        public PlcAddr YB_PmacValueSave { get; set; }
        public PlcAddr YB_PmacValueSaveAck { get; set; }

        public PlcAddr[] YB_QuickSave0PosCmd { get; set; }

        public PlcAddr[] YB_Position0MoveCmd { get; set; }
        public PlcAddr[] XB_Position0MoveCmdAck { get; set; }
        public PlcAddr[] YF_PositionPoint { get; set; }
        public PlcAddr[] XF_PositionPointAck { get; set; }
        public PlcAddr[] YF_PositionSpeed { get; set; }
        public PlcAddr[] XF_PositionSpeedAck { get; set; }
        public PlcAddr[] YF_PositionAccel { get; set; }
        public PlcAddr[] XF_PositionAccelAck { get; set; }

        public PlcAddr YB_AxisImmediateStopCmd { get; set; }
        public PlcAddr XB_AxisImmediateStopCmdAck { get; set; }

        public PlcAddr[] XB_PositionComplete { get; set; }
        public PlcTimer[] _moveDelay = new PlcTimer[] { new PlcTimer() };
        public PlcTimer _homeDelay = new PlcTimer();
        public PlcTimer _startPosiDelay = new PlcTimer();
        public Func<bool> InterLockFunc { get; set; }

        public int[] _moveStep = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        public int _homeStep = 0;
        public int _ptpStep = 0;
        public int _startPosiStep = 0;
        public int TotalLength { get; set; }

        protected bool _moving = false;
        protected bool _moveComplete { get; set; }
        protected bool _isMinuseGo = true;
        protected int _jogGo { get; set; }
        protected int _stepSetting = 0;
        protected double _inTargetPosi { get; set; }
        protected double _inSpeed { get; set; }

        public bool OutHomeAck { get; set; }

        public bool _isHomeCompleteBit = false;
        public bool NotReply { get; set; }

        public int PositionCount { get; set; }

        //simulation option
        public bool UseVirtualSpeed = false;
        /// </summary>
        /// 
        public ServoSimulUmac(int axis, string name, int length, int speed, int posiCount = 7)
        {
            InnerAxisNo = Axis = axis;
            PositionCount = posiCount;

            _moveStep = new int[posiCount];
            YB_QuickSave0PosCmd = new PlcAddr[PositionCount];
            YB_Position0MoveCmd = new PlcAddr[PositionCount];
            XB_Position0MoveCmdAck = new PlcAddr[PositionCount];
            YF_PositionPoint = new PlcAddr[PositionCount];
            XF_PositionPointAck = new PlcAddr[PositionCount];
            YF_PositionSpeed = new PlcAddr[PositionCount];
            XF_PositionSpeedAck = new PlcAddr[PositionCount];
            YF_PositionAccel = new PlcAddr[PositionCount];
            XF_PositionAccelAck = new PlcAddr[PositionCount];
            XB_PositionComplete = new PlcAddr[PositionCount];

            Name = name;
            TotalLength = length;

            _moveDelay = new PlcTimer[PositionCount];
            for (int iPos = 0; iPos < PositionCount; iPos++)
                _moveDelay[iPos] = new PlcTimer();

            _inSpeed = speed;
        }
        public void Initialize()
        {
            //XF_CurrMotorPosition.vFloat = 0;
            //XB_ErrMotorServoOn.vBit = false;

        }
        public void HomeOn()
        {
            XB_StatusMotorServoOn.vBit = true;
            XB_StatusHomeCompleteBit.vBit = true;
        }
        public virtual void LogicWorking(EquipSimul equip)
        {
            MoveWorking();
            Working();
            ServoOnOffLogic();




            //UpdatePositionState();
        }
        private void UpdatePositionState()
        {
            for (int iPos = 0; iPos < PositionCount; iPos++)
            {
                if (YF_PositionPoint[iPos].vFloat == XF_CurrMotorPosition.vFloat && XB_StatusHomeCompleteBit.vBit == true)
                {
                    XB_PositionComplete[iPos].vBit = true;
                }
                else
                    XB_PositionComplete[iPos].vBit = false;

            }
        }
        private void QuickSaveStep()
        {
            for (int iter = 0; iter < YB_QuickSave0PosCmd.Length; ++iter)
            {
                if (YB_QuickSave0PosCmd[iter].vBit == true)
                {
                    XF_PositionPointAck[iter].vFloat = YF_PositionPoint[iter].vFloat;
                }
            }
        }
        public virtual void SettingWorking(EquipSimul equip)
        {
            string result = "Save Pos ";
            if (_stepSetting == 0)
            {
                if (equip.PMac.YB_PmacValueSave.vBit == true)
                    _stepSetting = 10;
            }
            else if (_stepSetting == 10)
            {
                foreach (ServoSimulUmac motor in equip.Motors)
                {
                    for (int iPos = 0; iPos < motor.PositionCount; iPos++)
                    {
                        motor.XF_PositionPointAck[iPos].vFloat = motor.YF_PositionPoint[iPos].vFloat;
                        motor.XF_PositionSpeedAck[iPos].vFloat = motor.YF_PositionSpeed[iPos].vFloat;
                        motor.XF_PositionAccelAck[iPos].vFloat = motor.YF_PositionAccel[iPos].vFloat;
                    }
                }
                Logger.Log.AppendLine(LogLevel.Info, "모터 설정 쓰기");

                equip.PMac.XB_PmacValueSaveAck.vBit = true;
                _stepSetting = 20;
            }
            else if (_stepSetting == 20)
            {
                if (equip.PMac.YB_PmacValueSave.vBit == false)
                {
                    equip.PMac.XB_PmacValueSaveAck.vBit = false;
                    _stepSetting = 0;
                }
            }
        }
        private void MoveWorking()
        {
            PositionWorking(20, ref _homeStep, _homeDelay, YB_HomeCmd, XB_HomeCmdAck, XB_StatusHomeCompleteBit, 10, _inSpeed, "HOME");
            PositionWorking(10, ref _ptpStep, _homeDelay, YB_PTPMoveCmd, XB_PTPMoveCmdAck, null, 10, _inSpeed, "PTP");
        }
        public int SetpServoOnOff = 0;
        public void ServoOnOffLogic()
        {
            if (SetpServoOnOff == 0)
            {
                if (YB_ServoOnOffCmd.vBit)
                    SetpServoOnOff = 10;
            }
            else if (SetpServoOnOff == 10)
            {
                XB_ServoOnOffCmdAck.vBit = true;

                bool servoOnOff = YB_ServoOnOff.vBit;
                if (servoOnOff)
                {
                    XB_StatusMotorServoOn.vBit = true;
                }
                else
                {
                    XB_StatusMotorServoOn.vBit = false;
                }
                SetpServoOnOff = 20;
            }
            else if (SetpServoOnOff == 20)
            {
                if (YB_ServoOnOffCmd.vBit == false)
                {
                    XB_ServoOnOffCmdAck.vBit = false;
                    SetpServoOnOff = 0;
                }
            }
        }
        public virtual void PositionWorking(int p, ref int stepMove, PlcTimer moveDelay,
            PlcAddr yb_Position0MoveCmd,
            PlcAddr xb_Position0MoveCmdAck,
            PlcAddr xb_Position0Complete,
            double posi, double speed, string desc)
        {
            if (YB_MotorStopCmd.vBit == true || (YB_ServoOnOffCmd.vBit && YB_ServoOnOff.vBit == false))
            {
                XB_MotorStopCmdAck.vBit = true;
                _moveComplete = false;
                _moving = false;
                stepMove = 0;
            }
            else
                XB_MotorStopCmdAck.vBit = false;

            bool isHomeSeq = p == 20;

            if (stepMove == 0)
            {

                XF_PTPMoveAccelAck.vFloat = YF_PTPMoveAccel.vFloat;
                XF_PTPMoveSpeedAck.vFloat = YF_PTPMoveSpeed.vFloat;
                XF_PTPMovePositionAck.vFloat = YF_PTPMovePosition.vFloat;

                if (yb_Position0MoveCmd.vBit == true)
                {
                    //XB_StatusHomeInPosition.vBit = false;
                    xb_Position0MoveCmdAck.vBit = true;
                    XB_StatusMotorMoving.vBit = false;
                    XB_StatusMotorInPosition.vBit = false;
                    if (isHomeSeq)
                        xb_Position0Complete.vBit = false;
                    stepMove = 10;
                }
            }
            else if (stepMove == 10)
            {
                if (yb_Position0MoveCmd.vBit == false)
                {
                    xb_Position0MoveCmdAck.vBit = false;

                    if (isHomeSeq)
                        _inTargetPosi = 0;
                    else
                        _inTargetPosi = YF_PTPMovePosition.vFloat;

                    _inSpeed = YF_PTPMoveSpeed.vFloat;

                    if (_inSpeed <= 0)
                        _inSpeed = 100;

                    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1} 이동 시퀀스 시작 {2} => {3}", Name, desc, XF_CurrMotorSpeed.vFloat, posi);

                    stepMove = 20;
                }
            }
            else if (stepMove == 20)
            {
                //기본. 
                XB_StatusMotorMoving.vBit = true;

                Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1}  이동 시작", Name, desc);

                //속도 및 셋팅
                _isMinuseGo = _inTargetPosi < XF_CurrMotorPosition.vFloat;
                _moveComplete = false;
                _moving = true;
                stepMove = 30;
            }
            else if (stepMove == 30)
            {
                if (isHomeSeq)
                {
                    XB_StatusMotorServoOn.vBit = true;
                }

                moveDelay.Start(1);
                stepMove = 40;
            }
            else if (stepMove == 40)
            {
                if (moveDelay)
                {
                    moveDelay.Stop();
                    stepMove = 50;
                }
            }
            else if (stepMove == 50)
            {
                if (_moveComplete && NotReply == false)
                {
                    if (isHomeSeq)
                    {
                        if (0 == XF_CurrMotorPosition.vFloat)
                            xb_Position0Complete.vBit = true;
                    }
                    else
                    {
                    }


                    stepMove = 60;
                }

                //Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1} 이동 시퀀스 종료", Name, desc);
            }
            else if (stepMove == 60)
            {
                if (isHomeSeq)
                    xb_Position0Complete.vBit = true;
                XB_StatusMotorInPosition.vBit = true;
                XB_StatusMotorMoving.vBit = false;

                if (isHomeSeq)
                {
                    XB_StatusHomeCompleteBit.vBit = true;
                    XB_StatusHomeInPosition.vBit = true;
                }
                else
                {

                    if (YF_PTPMovePosition.vFloat == 0)
                    {
                        XB_StatusHomeInPosition.vBit = true;
                    }
                }
                //UpdatePositionState();
                Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1} 이동 완료", Name, desc);
                stepMove = 0;
            }
        }
        public virtual void ConvWorking(PlcAddr yb_MoveCmd, PlcAddr xb_MoveCmdAck, bool isForward)
        {
            if (yb_MoveCmd.vBit == true)
            {
                XB_StatusHomeInPosition.vBit = false;
                xb_MoveCmdAck.vBit = true;
                XB_StatusMotorMoving.vBit = true;

                int position = 0;
                position = Convert.ToInt32(isForward == false ? XF_CurrMotorPosition.vFloat - _inSpeed :
                                                               XF_CurrMotorPosition.vFloat + _inSpeed);


                //if (position < 0)
                //    position = 0;

                if (position > TotalLength)
                    position = 0;

                XF_CurrMotorPosition.vFloat = position;
            }
            else
            {
                xb_MoveCmdAck.vBit = false;

                for (int iPos = 0; iPos < PositionCount; iPos++)
                    if (YB_Position0MoveCmd[iPos].vBit == true) return;

                XB_StatusMotorMoving.vBit = false;
            }
        }
        private bool _oldJogP;
        private bool _oldJogN;
        private int _oldPos;
        public virtual void Working()
        {
            try
            {
                if (YB_MotorJogMinusMove.vBit == true && _oldJogP == false)
                {
                    //if (InJogPlus)
                    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 MINUS JOG 이동 시작", Name);
                    //else
                    //    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 PLUS JOG 이동 종료", Name);

                    //_inJogPlus = InJogPlus;
                }
                else if (YB_MotorJogMinusMove.vBit == true && _oldJogP == false)
                    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 MINUS JOG 이동 종료", Name);
                if (YB_MotorJogPlusMove.vBit == true && _oldJogN == false)
                {
                    //if (InJogMinus)
                    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 PLUS JOG 이동 시작", Name);
                    //else
                    //    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 MINUS JOG 이동 종료", Name);

                    //_inJogMinus = InJogMinus;
                }
                else if (YB_MotorJogPlusMove.vBit == true && _oldJogN == false)
                    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 PLUS JOG 이동 종료", Name);

                _oldJogN = YB_MotorJogMinusMove.vBit;
                _oldJogP = YB_MotorJogPlusMove.vBit;



                XB_StatusNegativeLimitSet.vBit = (XF_CurrMotorPosition.vFloat < 3);
                XB_StatusPositiveLimitSet.vBit = (XF_CurrMotorPosition.vFloat > TotalLength - 3);
                XF_MotorJogSpeedCmdAck.vFloat = YF_MotorJogSpeedCmd.vFloat;

                //Console.WriteLine(YI_MotorJog.vInt);
                if (_moving || YB_MotorJogMinusMove.vBit == true || YB_MotorJogPlusMove.vBit == true)
                {
                    XB_StatusHomeInPosition.vBit = false;
                    if ((XF_CurrMotorPosition.vFloat > TotalLength && YB_MotorJogPlusMove.vBit == true) || (XF_CurrMotorPosition.vFloat <= 0 && YB_MotorJogMinusMove.vBit == true))
                        return;


                    int position = 0;
                    if (YB_MotorJogPlusMove.vBit == true || YB_MotorJogMinusMove.vBit == true)
                    {
                        _inSpeed = YF_MotorJogSpeedCmd.vFloat; ;
                        position = Convert.ToInt32(YB_MotorJogMinusMove.vBit == true ? XF_CurrMotorPosition.vFloat - _inSpeed :
                                                      YB_MotorJogPlusMove.vBit == true ? XF_CurrMotorPosition.vFloat + _inSpeed : XF_CurrMotorPosition.vFloat);


                    }
                    else
                    {
                        double remain = Math.Abs(XF_CurrMotorPosition.vFloat - _inTargetPosi);
                        double speed = _inSpeed;
                        if (UseVirtualSpeed == true)
                        {
                            if (remain > _inSpeed)
                                speed = _inSpeed;
                            else if (remain > _inSpeed / 2)
                                speed = _inSpeed / 2.1;
                            else if (remain > 100000)
                                speed = 50000;
                            else if (remain > 50000)
                                speed = 25000;
                            else
                                speed = 10000;
                        }
                        position = Convert.ToInt32(_isMinuseGo ? XF_CurrMotorPosition.vFloat - speed : XF_CurrMotorPosition.vFloat + speed);
                    }

                    //if (position < 0)
                    //    position = 0;

                    //if (position > TotalLength)
                    //    position = TotalLength;

                    // jys, 위치,속도가 어긋나면 계속증가/감소하는 버그 수정 코드인데 _moveComplete/_moving 처리가 안돼있어서 일단 제외
                    //오정석
                    //                 if (position < 0) position = 0;
                    //                 else if (position > _inTargetPosi) position = (int)_inTargetPosi;

                    XF_CurrMotorPosition.vFloat = position;

                    if (XF_CurrMotorPosition.vFloat <= _inTargetPosi && _isMinuseGo && YB_MotorJogMinusMove.vBit == false && YB_MotorJogPlusMove.vBit == false)
                    {
                        XF_CurrMotorPosition.vFloat = (float)_inTargetPosi;
                        _moveComplete = true;
                        _moving = false;

                    }
                    else if (XF_CurrMotorPosition.vFloat >= _inTargetPosi && _isMinuseGo == false && YB_MotorJogMinusMove.vBit == false && YB_MotorJogPlusMove.vBit == false)
                    {
                        XF_CurrMotorPosition.vFloat = (float)_inTargetPosi;
                        _moveComplete = true;
                        _moving = false;

                    }
                }

                XB_StatusMotorMoving.vBit = Math.Abs(_oldPos - XF_CurrMotorPosition.vFloat) > 5;
                _oldPos = (int)XF_CurrMotorPosition.vFloat;

            }
            catch (Exception ex)
            {

                Console.WriteLine();
            }
        }
    }
}

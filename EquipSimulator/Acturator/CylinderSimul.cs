﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Timers;
using EquipSimulator.Log;
using Dit.Framework.Log;
using Dit.Framework.PLC;


namespace EquipSimulator.Acturator
{
    public class CylinderSimul
    {
        private Timer _tmrF = new Timer();
        private Timer _tmrB = new Timer();
        public string Name { get; set; }
        private DateTime _moveStartTime = DateTime.Now;


        public int TotalLength { get; set; }
        public int CurrPosition { get; set; }

        public PlcAddr YB_ForwardCmd { get; set; }
        public PlcAddr YB_BackwardCmd { get; set; }

        public PlcAddr XB_ForwardComplete { get; set; }
        public PlcAddr XB_BackwardComplete { get; set; }

        public bool _XB_ForwardComplete;
        public bool _XB_BackwardComplete;

        public double Position
        {
            get
            {
                return CurrPosition;
            }
        }
        public void Initialize()
        {
        }
        public CylinderSimul()
        {
        }
        public void LogicWorking()
        {
            Working();
        }
        private void Working()
        {
            try
            {
                if (YB_ForwardCmd.vBit)
                {
                    int tmpCurrPosition = CurrPosition + 10;
                    if (tmpCurrPosition >= TotalLength)
                    {
                        tmpCurrPosition = TotalLength;
                        if (XB_ForwardComplete.vBit == false)
                            Logger.Log.AppendLine(LogLevel.Error, "{0} 전진 완료", Name);
                        _XB_ForwardComplete = XB_ForwardComplete.vBit = true;
                        _XB_BackwardComplete = XB_BackwardComplete.vBit = false;
                    }
                    else
                    {
                        _XB_ForwardComplete = XB_ForwardComplete.vBit = false;
                        _XB_BackwardComplete = XB_BackwardComplete.vBit = false;
                    }
                    CurrPosition = tmpCurrPosition;
                }

                if (YB_BackwardCmd.vBit)
                {
                    int tmpCurrPosition = CurrPosition - 10;
                    if (tmpCurrPosition <= 0)
                    {
                        tmpCurrPosition = 0;
                        if (XB_BackwardComplete.vBit == false)
                            Logger.Log.AppendLine(LogLevel.Error, "{0} 후진 완료", Name);
                        _XB_ForwardComplete = XB_ForwardComplete.vBit = false;
                        _XB_BackwardComplete = XB_BackwardComplete.vBit = true;
                        
                    }
                    else
                    {
                        _XB_ForwardComplete = XB_ForwardComplete.vBit = false;
                        _XB_BackwardComplete = XB_BackwardComplete.vBit = false;
                    }
                    CurrPosition = tmpCurrPosition;
                }
                // XB_ForwardComplete.vBit = _XB_ForwardComplete;
                // XB_BackwardComplete.vBit = _XB_BackwardComplete;
            }
            catch (Exception ex)
            {
                Console.WriteLine("");
            }
        }
    }
}

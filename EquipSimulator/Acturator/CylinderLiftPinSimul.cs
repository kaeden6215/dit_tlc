﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Timers;
using EquipSimulator.Log;
using Dit.Framework.Log;
using Dit.Framework.PLC;

namespace EquipSimulator.Acturator
{
    public class CylinderLiftPinSimul
    {
        private Timer _tmrF = new Timer();
        private Timer _tmrB = new Timer();
        public string Name { get; set; }
        private DateTime _moveStartTime = DateTime.Now;


        public int TotalLength { get; set; }
        public int CurrPosition { get; set; }

        public PlcAddr YB_Up1Cmd { get; set; }
        public PlcAddr YB_Down1Cmd { get; set; }
        public PlcAddr YB_Up2Cmd { get; set; }
        public PlcAddr YB_Down2Cmd { get; set; }

        public PlcAddr XB_UpComplete { get; set; }
        public PlcAddr XB_MiddleComplete { get; set; }
        public PlcAddr XB_DownComplete { get; set; }


        public double Position
        {
            get
            {
                return CurrPosition;
            }
        }
        public void Initialize()
        {

        }
        public CylinderLiftPinSimul()
        {
            CurrPosition = 0;
        }
        public void LogicWorking()
        {
            Working();
        }
        private void Working()
        {
            if (YB_Up1Cmd.vBit && YB_Down1Cmd.vBit == false)
            {
                if (CurrPosition == 0)
                    CurrPosition = 30;

                if (CurrPosition == 70)
                    CurrPosition = 100;
            }
            else if (YB_Up1Cmd.vBit == false && YB_Down1Cmd.vBit)
            {
                if (CurrPosition == 30)
                    CurrPosition = 0;

                if (CurrPosition == 100)
                    CurrPosition = 70;
            }

            if (YB_Up2Cmd.vBit && YB_Down2Cmd.vBit == false)
            {
                if (CurrPosition == 0)
                    CurrPosition = 70;

                if (CurrPosition == 30)
                    CurrPosition = 100;
            }
            else if (YB_Up2Cmd.vBit == false && YB_Down2Cmd.vBit)
            {
                if (CurrPosition == 70)
                    CurrPosition = 0;

                if (CurrPosition == 100)
                    CurrPosition = 30;
            }

            if (CurrPosition == 0)
            {
                XB_DownComplete.vBit = true;
                XB_MiddleComplete.vBit = false;
                XB_UpComplete.vBit = false;
            }
            else if (CurrPosition == 30)
            {
                XB_DownComplete.vBit = false;
                XB_MiddleComplete.vBit = true;
                XB_UpComplete.vBit = false;
            }
            else if (CurrPosition == 100)
            {
                XB_DownComplete.vBit = false;
                XB_MiddleComplete.vBit = false;
                XB_UpComplete.vBit = true;
            }


            //if (YB_Up1Cmd.vBit)
            //{
            //    int tmpCurrPosition = CurrPosition + 60;
            //    if (tmpCurrPosition >= TotalLength)
            //    {
            //        tmpCurrPosition = TotalLength;
            //        XB_MiddleComplete.vBit = true;
            //        XB_DownComplete.vBit = false;
            //    }
            //    else
            //    {
            //        XB_UpComplete.vBit = false;
            //        XB_MiddleComplete.vBit = false;
            //        XB_DownComplete.vBit = false;
            //    }
            //    CurrPosition = tmpCurrPosition;
            //}
            //
            //if (YB_Up2Cmd.vBit)
            //{
            //    int tmpCurrPosition = CurrPosition + 50;
            //    if (tmpCurrPosition >= TotalLength)
            //    {
            //        tmpCurrPosition = TotalLength;
            //        XB_MiddleComplete.vBit = false;
            //        XB_UpComplete.vBit = true;
            //    }
            //    else
            //    {
            //        XB_UpComplete.vBit = false;
            //        XB_MiddleComplete.vBit = false;
            //        XB_DownComplete.vBit = false;
            //    }
            //    CurrPosition = tmpCurrPosition;
            //}
            //
            //if (YB_Down2Cmd.vBit)
            //{
            //    int tmpCurrPosition = CurrPosition - 10;
            //    if (tmpCurrPosition <= 0)
            //    {
            //        tmpCurrPosition = TotalLength;
            //        XB_MiddleComplete.vBit = true;
            //        XB_UpComplete.vBit = false;
            //    }
            //    else
            //    {
            //        XB_UpComplete.vBit = false;
            //        XB_MiddleComplete.vBit = false;
            //        XB_DownComplete.vBit = false;
            //    }
            //    CurrPosition = tmpCurrPosition;
            //}
            //
            //if (YB_Down1Cmd.vBit)
            //{
            //    int tmpCurrPosition = CurrPosition - 100;
            //    if (tmpCurrPosition <= 0)
            //    {
            //        tmpCurrPosition = 0;
            //        XB_MiddleComplete.vBit = false;
            //        XB_DownComplete.vBit = true;
            //    }
            //    else
            //    {
            //        XB_UpComplete.vBit = false;
            //        XB_MiddleComplete.vBit = false;
            //        XB_DownComplete.vBit = false;
            //    }
            //    CurrPosition = tmpCurrPosition;
            //}
        }
    }
}

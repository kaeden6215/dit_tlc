﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;


namespace EquipSimulator.Acturator
{
    public class PMacSimul
    {
        public PlcAddr YB_EquipMode { get; set; }
        public PlcAddr YB_CheckAlarmStatus { get; set; }
        public PlcAddr YB_UpperInterfaceWorking { get; set; }
        public PlcAddr YB_LowerInterfaceWorking { get; set; }

        public PlcAddr XB_PmacReady { get; set; }
        public PlcAddr XB_PmacAlive { get; set; }
        public PlcAddr XB_PmacHeavyAlarm { get; set; }

        public PlcAddr YB_PinUpMotorInterlockDisable { get; set; }
        public PlcAddr XB_PinUpMotorInterlockDiableAck { get; set; }
        public PlcAddr YB_ReviewTimerOverCmd { get; set; }
        public PlcAddr XB_ReviewTimerOverCmdAck { get; set; }

        public PlcAddr YB_PmacResetCmd { get; set; }
        public PlcAddr XB_PmacResetCmdAck { get; set; }

        public PlcAddr YB_PmacValueSave { get; set; }
        public PlcAddr XB_PmacValueSaveAck { get; set; }

        public void LogicWorking()
        {
            //XB_PinUpMotorInterlockDiableAck.vInt = YB_PinUpMotorInterlockDisable.vInt;
        }
    }
}

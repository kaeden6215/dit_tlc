﻿using EquipSimulator.Common;
namespace EquipSimulator
{
    partial class FrmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label11 = new System.Windows.Forms.Label();
            this.tmrWorker = new System.Windows.Forms.Timer(this.components);
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.pnlView = new System.Windows.Forms.Panel();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblScanTime = new System.Windows.Forms.Label();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.tmrState = new System.Windows.Forms.Timer(this.components);
            this.lstLogger = new System.Windows.Forms.ListView();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.chkVaccum6 = new System.Windows.Forms.CheckBox();
            this.chkVaccum5 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkVaccum2 = new System.Windows.Forms.CheckBox();
            this.chkVaccum3 = new System.Windows.Forms.CheckBox();
            this.chkVaccum4 = new System.Windows.Forms.CheckBox();
            this.chkVaccum1 = new System.Windows.Forms.CheckBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.chkBlower6 = new System.Windows.Forms.CheckBox();
            this.chkBlower5 = new System.Windows.Forms.CheckBox();
            this.chkBlower4 = new System.Windows.Forms.CheckBox();
            this.chkBlower3 = new System.Windows.Forms.CheckBox();
            this.chkBlower2 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkBlower1 = new System.Windows.Forms.CheckBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.chkGlassCheckSensor6 = new System.Windows.Forms.CheckBox();
            this.chkGlassCheckSensor5 = new System.Windows.Forms.CheckBox();
            this.chkGlassCheckSensor4 = new System.Windows.Forms.CheckBox();
            this.chkGlassCheckSensor3 = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkGlassCheckSensor1 = new System.Windows.Forms.CheckBox();
            this.chkGlassCheckSensor2 = new System.Windows.Forms.CheckBox();
            this.txtReadValue = new System.Windows.Forms.TextBox();
            this.txtReadAddr = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkDoor02 = new System.Windows.Forms.CheckBox();
            this.chkDoorAll = new System.Windows.Forms.CheckBox();
            this.chkDoor05 = new System.Windows.Forms.CheckBox();
            this.chkDoor04 = new System.Windows.Forms.CheckBox();
            this.chkDoor01 = new System.Windows.Forms.CheckBox();
            this.chkDoor03 = new System.Windows.Forms.CheckBox();
            this.chkDoor06 = new System.Windows.Forms.CheckBox();
            this.chkDoor07 = new System.Windows.Forms.CheckBox();
            this.chkDoor08 = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.chkIonizer1On = new System.Windows.Forms.CheckBox();
            this.chkIonizer2On = new System.Windows.Forms.CheckBox();
            this.chkIonizer4On = new System.Windows.Forms.CheckBox();
            this.chkIonizer3On = new System.Windows.Forms.CheckBox();
            this.chkReadHsms = new System.Windows.Forms.CheckBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.chkIsol01 = new System.Windows.Forms.CheckBox();
            this.chkIsol02 = new System.Windows.Forms.CheckBox();
            this.chkIsol04 = new System.Windows.Forms.CheckBox();
            this.chkIsol03 = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.chkGlassEdgeCheckSensor1 = new System.Windows.Forms.CheckBox();
            this.chkGlassEdgeCheckSensor3 = new System.Windows.Forms.CheckBox();
            this.chkGlassEdgeCheckSensor2 = new System.Windows.Forms.CheckBox();
            this.chkGlassEdgeCheckSensor4 = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.chkEmo07 = new System.Windows.Forms.CheckBox();
            this.chkEmo06 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkEmo02 = new System.Windows.Forms.CheckBox();
            this.chkEmo01 = new System.Windows.Forms.CheckBox();
            this.chkEmo03 = new System.Windows.Forms.CheckBox();
            this.chkEmo05 = new System.Windows.Forms.CheckBox();
            this.chkEmo04 = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.chkUDBMute = new System.Windows.Forms.CheckBox();
            this.chkUDAMute = new System.Windows.Forms.CheckBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.chkLdStageOutBtn2 = new System.Windows.Forms.CheckBox();
            this.chkLdStageOutBtn1 = new System.Windows.Forms.CheckBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.lblLoRecvComplete = new System.Windows.Forms.Label();
            this.chkRobotArmCheck = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnRecvAbleStart = new System.Windows.Forms.Button();
            this.btnSendAbleStart = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lblLoRecvStart = new System.Windows.Forms.Label();
            this.lblLoRecvAble = new System.Windows.Forms.Label();
            this.lblAoiSendComplete = new System.Windows.Forms.Label();
            this.lblAoiSendStart = new System.Windows.Forms.Label();
            this.lblAoiSendAble = new System.Windows.Forms.Label();
            this.lblAoiRecvComplete = new System.Windows.Forms.Label();
            this.lblAoiRecvStart = new System.Windows.Forms.Label();
            this.lblAoiRecvAble = new System.Windows.Forms.Label();
            this.lblUpSendComplete = new System.Windows.Forms.Label();
            this.lblUpSendStart = new System.Windows.Forms.Label();
            this.lblUpSendAble = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.btnCtrlPcXy = new System.Windows.Forms.Button();
            this.btnSimulPcXy = new System.Windows.Forms.Button();
            this.btnTester = new System.Windows.Forms.Button();
            this.btnXyIoAddr = new System.Windows.Forms.Button();
            this.btnMemDetail = new System.Windows.Forms.Button();
            this.btnXyIo = new System.Windows.Forms.Button();
            this.chkUseInterlock = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblScanStep = new System.Windows.Forms.Label();
            this.lblInspGUnloadingOk = new System.Windows.Forms.Label();
            this.lblInspGUnloading = new System.Windows.Forms.Label();
            this.lblInspScanEnd = new System.Windows.Forms.Label();
            this.lblInspScanEndOk = new System.Windows.Forms.Label();
            this.lblInspScanStartOk = new System.Windows.Forms.Label();
            this.lblInspGLoadingOk = new System.Windows.Forms.Label();
            this.lblInspScanStart = new System.Windows.Forms.Label();
            this.lblInspScanReadyOk = new System.Windows.Forms.Label();
            this.lblInspScanReady = new System.Windows.Forms.Label();
            this.lblInspGLoading = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkInspXError = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkReviLotEndAck = new System.Windows.Forms.CheckBox();
            this.chkReviLotStartAck = new System.Windows.Forms.CheckBox();
            this.chkReviReviEnd = new System.Windows.Forms.CheckBox();
            this.chkReviReviStartAck = new System.Windows.Forms.CheckBox();
            this.chkReviAlignStartAck = new System.Windows.Forms.CheckBox();
            this.chkReviUnloadingAck = new System.Windows.Forms.CheckBox();
            this.chkReviLoadingAck = new System.Windows.Forms.CheckBox();
            this.chkReviLotEnd = new System.Windows.Forms.CheckBox();
            this.chkReviLotStart = new System.Windows.Forms.CheckBox();
            this.chkReviReviEndAck = new System.Windows.Forms.CheckBox();
            this.chkReviReviStart = new System.Windows.Forms.CheckBox();
            this.chkReviUnloading = new System.Windows.Forms.CheckBox();
            this.chkReviAlignStart = new System.Windows.Forms.CheckBox();
            this.chkReviLoading = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.chkCoolineMainAir = new System.Windows.Forms.CheckBox();
            this.chkIonizerMainAir = new System.Windows.Forms.CheckBox();
            this.chkCameraCooling = new System.Windows.Forms.CheckBox();
            this.chkEnableGripSwOn = new System.Windows.Forms.CheckBox();
            this.chkStageGlassSensor1 = new System.Windows.Forms.CheckBox();
            this.chkSafetyModeKeySW = new System.Windows.Forms.CheckBox();
            this.chkIonizerCover = new System.Windows.Forms.CheckBox();
            this.chkIonizer = new System.Windows.Forms.CheckBox();
            this.chkUnldStageOutBtn2 = new System.Windows.Forms.CheckBox();
            this.chkUnldStageOutBtn1 = new System.Windows.Forms.CheckBox();
            this.chkUDAReset = new System.Windows.Forms.CheckBox();
            this.chkUDBReset = new System.Windows.Forms.CheckBox();
            this.chkLdStageBtn1 = new System.Windows.Forms.CheckBox();
            this.chkLdStageBtn2 = new System.Windows.Forms.CheckBox();
            this.chkUnldStageBtn2 = new System.Windows.Forms.CheckBox();
            this.chkUnldStageBtn1 = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.chkLoaderAOGamji = new System.Windows.Forms.CheckBox();
            this.lblAlive = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.chkLoaderAExist = new System.Windows.Forms.CheckBox();
            this.chkLoaderBExist = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.chkLoaderBOGamji = new System.Windows.Forms.CheckBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.chkUnloaderAExist = new System.Windows.Forms.CheckBox();
            this.chkUnloaderBExist = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.chkUnloaderAOGamji = new System.Windows.Forms.CheckBox();
            this.chkUnloaderBOGamji = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.lstViewMotorInfo = new EquipSimulator.Common.ListViewEx();
            this.shInspCam = new EquipSimulator.Ctrl.TransparentControl();
            this.shReviewCam = new EquipSimulator.Ctrl.TransparentControl();
            this.shStage = new EquipSimulator.Ctrl.TransparentControl();
            this.SpThelta = new EquipSimulator.Ctrl.TransparentControl();
            this.spLiftPin = new EquipSimulator.Ctrl.TransparentControl();
            this.pnlView.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(7, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(186, 25);
            this.label11.TabIndex = 5;
            this.label11.Text = "EQUIP SIMULATOR";
            // 
            // tmrWorker
            // 
            this.tmrWorker.Enabled = true;
            this.tmrWorker.Tick += new System.EventHandler(this.tmrWorker_Tick);
            // 
            // bgWorker
            // 
            this.bgWorker.WorkerReportsProgress = true;
            this.bgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_DoWork);
            this.bgWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorker_ProgressChanged);
            // 
            // pnlView
            // 
            this.pnlView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlView.Controls.Add(this.elementHost1);
            this.pnlView.Controls.Add(this.label14);
            this.pnlView.Controls.Add(this.panel1);
            this.pnlView.Location = new System.Drawing.Point(3, 45);
            this.pnlView.Name = "pnlView";
            this.pnlView.Size = new System.Drawing.Size(1056, 670);
            this.pnlView.TabIndex = 89;
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 66);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(1054, 602);
            this.elementHost1.TabIndex = 10;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // label14
            // 
            this.label14.AutoEllipsis = true;
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(1054, 66);
            this.label14.TabIndex = 9;
            this.label14.Text = "■ EQUIP  STATUS";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.BackgroundImage = global::EquipSimulator.Properties.Resources.EquipBase;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.shInspCam);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.shReviewCam);
            this.panel1.Controls.Add(this.shStage);
            this.panel1.Controls.Add(this.SpThelta);
            this.panel1.Controls.Add(this.spLiftPin);
            this.panel1.Location = new System.Drawing.Point(43, 305);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(49, 34);
            this.panel1.TabIndex = 0;
            this.panel1.Visible = false;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(23, 177);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 15);
            this.label26.TabIndex = 10;
            this.label26.Text = "Litft Pin";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(189, 646);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 15);
            this.label27.TabIndex = 11;
            this.label27.Text = "Thelta";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::EquipSimulator.Properties.Resources.GentryInspY;
            this.pictureBox1.Location = new System.Drawing.Point(138, 61);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(123, 530);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // lblScanTime
            // 
            this.lblScanTime.AutoEllipsis = true;
            this.lblScanTime.BackColor = System.Drawing.Color.Gainsboro;
            this.lblScanTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblScanTime.ForeColor = System.Drawing.Color.Black;
            this.lblScanTime.Location = new System.Drawing.Point(203, 15);
            this.lblScanTime.Name = "lblScanTime";
            this.lblScanTime.Size = new System.Drawing.Size(153, 16);
            this.lblScanTime.TabIndex = 85;
            this.lblScanTime.Text = "-";
            this.lblScanTime.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tbStatus
            // 
            this.tbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStatus.Location = new System.Drawing.Point(3, 935);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.ReadOnly = true;
            this.tbStatus.Size = new System.Drawing.Size(1974, 23);
            this.tbStatus.TabIndex = 91;
            // 
            // tmrState
            // 
            this.tmrState.Enabled = true;
            this.tmrState.Interval = 1000;
            this.tmrState.Tick += new System.EventHandler(this.tmrState_Tick);
            // 
            // lstLogger
            // 
            this.lstLogger.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstLogger.Location = new System.Drawing.Point(3, 719);
            this.lstLogger.Name = "lstLogger";
            this.lstLogger.Size = new System.Drawing.Size(1055, 212);
            this.lstLogger.TabIndex = 94;
            this.lstLogger.UseCompatibleStateImageBehavior = false;
            this.lstLogger.View = System.Windows.Forms.View.Details;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label17.Location = new System.Drawing.Point(731, 14);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 19);
            this.label17.TabIndex = 4;
            this.label17.Text = "Pos\'";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Location = new System.Drawing.Point(780, 14);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 19);
            this.label22.TabIndex = 2;
            this.label22.Text = "0.0";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.checkBox1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.checkBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.checkBox1.Location = new System.Drawing.Point(5, 31);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 28);
            this.checkBox1.TabIndex = 16;
            this.checkBox1.Text = "LdStage1";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // checkBox4
            // 
            this.checkBox4.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.checkBox4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.checkBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.checkBox4.Location = new System.Drawing.Point(91, 31);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(80, 28);
            this.checkBox4.TabIndex = 16;
            this.checkBox4.Text = "UnldStage1";
            this.checkBox4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox4.UseVisualStyleBackColor = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Controls.Add(this.panel11);
            this.tabPage2.Controls.Add(this.txtReadValue);
            this.tabPage2.Controls.Add(this.txtReadAddr);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.chkReadHsms);
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(488, 835);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.chkVaccum6);
            this.panel9.Controls.Add(this.chkVaccum5);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.chkVaccum2);
            this.panel9.Controls.Add(this.chkVaccum3);
            this.panel9.Controls.Add(this.chkVaccum4);
            this.panel9.Controls.Add(this.chkVaccum1);
            this.panel9.Location = new System.Drawing.Point(6, 8);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(92, 210);
            this.panel9.TabIndex = 89;
            // 
            // chkVaccum6
            // 
            this.chkVaccum6.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkVaccum6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkVaccum6.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkVaccum6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkVaccum6.Location = new System.Drawing.Point(4, 179);
            this.chkVaccum6.Name = "chkVaccum6";
            this.chkVaccum6.Size = new System.Drawing.Size(80, 28);
            this.chkVaccum6.TabIndex = 12;
            this.chkVaccum6.Text = "Vaccum6";
            this.chkVaccum6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkVaccum6.UseVisualStyleBackColor = false;
            this.chkVaccum6.Visible = false;
            // 
            // chkVaccum5
            // 
            this.chkVaccum5.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkVaccum5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkVaccum5.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkVaccum5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkVaccum5.Location = new System.Drawing.Point(5, 148);
            this.chkVaccum5.Name = "chkVaccum5";
            this.chkVaccum5.Size = new System.Drawing.Size(80, 28);
            this.chkVaccum5.TabIndex = 11;
            this.chkVaccum5.Text = "Vaccum5";
            this.chkVaccum5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkVaccum5.UseVisualStyleBackColor = false;
            this.chkVaccum5.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 24);
            this.label8.TabIndex = 9;
            this.label8.Text = "■ VACCUM";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkVaccum2
            // 
            this.chkVaccum2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkVaccum2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkVaccum2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkVaccum2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkVaccum2.Location = new System.Drawing.Point(7, 55);
            this.chkVaccum2.Name = "chkVaccum2";
            this.chkVaccum2.Size = new System.Drawing.Size(80, 28);
            this.chkVaccum2.TabIndex = 10;
            this.chkVaccum2.Text = "Vaccum2";
            this.chkVaccum2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkVaccum2.UseVisualStyleBackColor = false;
            // 
            // chkVaccum3
            // 
            this.chkVaccum3.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkVaccum3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkVaccum3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkVaccum3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkVaccum3.Location = new System.Drawing.Point(5, 86);
            this.chkVaccum3.Name = "chkVaccum3";
            this.chkVaccum3.Size = new System.Drawing.Size(80, 28);
            this.chkVaccum3.TabIndex = 10;
            this.chkVaccum3.Text = "Vaccum3";
            this.chkVaccum3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkVaccum3.UseVisualStyleBackColor = false;
            // 
            // chkVaccum4
            // 
            this.chkVaccum4.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkVaccum4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkVaccum4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkVaccum4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkVaccum4.Location = new System.Drawing.Point(5, 116);
            this.chkVaccum4.Name = "chkVaccum4";
            this.chkVaccum4.Size = new System.Drawing.Size(80, 28);
            this.chkVaccum4.TabIndex = 10;
            this.chkVaccum4.Text = "Vaccum4";
            this.chkVaccum4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkVaccum4.UseVisualStyleBackColor = false;
            // 
            // chkVaccum1
            // 
            this.chkVaccum1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkVaccum1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkVaccum1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkVaccum1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkVaccum1.Location = new System.Drawing.Point(7, 27);
            this.chkVaccum1.Name = "chkVaccum1";
            this.chkVaccum1.Size = new System.Drawing.Size(80, 28);
            this.chkVaccum1.TabIndex = 10;
            this.chkVaccum1.Text = "Vaccum1";
            this.chkVaccum1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkVaccum1.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.chkBlower6);
            this.panel10.Controls.Add(this.chkBlower5);
            this.panel10.Controls.Add(this.chkBlower4);
            this.panel10.Controls.Add(this.chkBlower3);
            this.panel10.Controls.Add(this.chkBlower2);
            this.panel10.Controls.Add(this.label9);
            this.panel10.Controls.Add(this.chkBlower1);
            this.panel10.Location = new System.Drawing.Point(102, 8);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(92, 210);
            this.panel10.TabIndex = 89;
            // 
            // chkBlower6
            // 
            this.chkBlower6.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkBlower6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkBlower6.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower6.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkBlower6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkBlower6.Location = new System.Drawing.Point(3, 177);
            this.chkBlower6.Name = "chkBlower6";
            this.chkBlower6.Size = new System.Drawing.Size(80, 28);
            this.chkBlower6.TabIndex = 15;
            this.chkBlower6.Text = "Blower 6";
            this.chkBlower6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower6.UseVisualStyleBackColor = false;
            this.chkBlower6.Visible = false;
            // 
            // chkBlower5
            // 
            this.chkBlower5.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkBlower5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkBlower5.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower5.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkBlower5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkBlower5.Location = new System.Drawing.Point(3, 148);
            this.chkBlower5.Name = "chkBlower5";
            this.chkBlower5.Size = new System.Drawing.Size(80, 28);
            this.chkBlower5.TabIndex = 14;
            this.chkBlower5.Text = "Blower 5";
            this.chkBlower5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower5.UseVisualStyleBackColor = false;
            this.chkBlower5.Visible = false;
            // 
            // chkBlower4
            // 
            this.chkBlower4.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkBlower4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkBlower4.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkBlower4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkBlower4.Location = new System.Drawing.Point(3, 117);
            this.chkBlower4.Name = "chkBlower4";
            this.chkBlower4.Size = new System.Drawing.Size(80, 28);
            this.chkBlower4.TabIndex = 13;
            this.chkBlower4.Text = "Blower 4";
            this.chkBlower4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower4.UseVisualStyleBackColor = false;
            // 
            // chkBlower3
            // 
            this.chkBlower3.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkBlower3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkBlower3.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkBlower3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkBlower3.Location = new System.Drawing.Point(3, 87);
            this.chkBlower3.Name = "chkBlower3";
            this.chkBlower3.Size = new System.Drawing.Size(80, 28);
            this.chkBlower3.TabIndex = 12;
            this.chkBlower3.Text = "Blower 3";
            this.chkBlower3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower3.UseVisualStyleBackColor = false;
            // 
            // chkBlower2
            // 
            this.chkBlower2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkBlower2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkBlower2.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkBlower2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkBlower2.Location = new System.Drawing.Point(3, 57);
            this.chkBlower2.Name = "chkBlower2";
            this.chkBlower2.Size = new System.Drawing.Size(80, 28);
            this.chkBlower2.TabIndex = 11;
            this.chkBlower2.Text = "Blower 2";
            this.chkBlower2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower2.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.BackColor = System.Drawing.Color.Gainsboro;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "■ BLOWER";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkBlower1
            // 
            this.chkBlower1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkBlower1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkBlower1.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkBlower1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkBlower1.Location = new System.Drawing.Point(3, 28);
            this.chkBlower1.Name = "chkBlower1";
            this.chkBlower1.Size = new System.Drawing.Size(80, 28);
            this.chkBlower1.TabIndex = 10;
            this.chkBlower1.Text = "Blower 1";
            this.chkBlower1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBlower1.UseVisualStyleBackColor = false;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.chkGlassCheckSensor6);
            this.panel11.Controls.Add(this.chkGlassCheckSensor5);
            this.panel11.Controls.Add(this.chkGlassCheckSensor4);
            this.panel11.Controls.Add(this.chkGlassCheckSensor3);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.chkGlassCheckSensor1);
            this.panel11.Controls.Add(this.chkGlassCheckSensor2);
            this.panel11.Location = new System.Drawing.Point(197, 8);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(92, 210);
            this.panel11.TabIndex = 89;
            // 
            // chkGlassCheckSensor6
            // 
            this.chkGlassCheckSensor6.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassCheckSensor6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassCheckSensor6.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassCheckSensor6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassCheckSensor6.Location = new System.Drawing.Point(4, 177);
            this.chkGlassCheckSensor6.Name = "chkGlassCheckSensor6";
            this.chkGlassCheckSensor6.Size = new System.Drawing.Size(80, 28);
            this.chkGlassCheckSensor6.TabIndex = 27;
            this.chkGlassCheckSensor6.Text = "Sensor 6";
            this.chkGlassCheckSensor6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassCheckSensor6.UseVisualStyleBackColor = false;
            this.chkGlassCheckSensor6.Visible = false;
            // 
            // chkGlassCheckSensor5
            // 
            this.chkGlassCheckSensor5.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassCheckSensor5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassCheckSensor5.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassCheckSensor5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassCheckSensor5.Location = new System.Drawing.Point(3, 148);
            this.chkGlassCheckSensor5.Name = "chkGlassCheckSensor5";
            this.chkGlassCheckSensor5.Size = new System.Drawing.Size(80, 28);
            this.chkGlassCheckSensor5.TabIndex = 26;
            this.chkGlassCheckSensor5.Text = "Sensor 5";
            this.chkGlassCheckSensor5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassCheckSensor5.UseVisualStyleBackColor = false;
            this.chkGlassCheckSensor5.Visible = false;
            // 
            // chkGlassCheckSensor4
            // 
            this.chkGlassCheckSensor4.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassCheckSensor4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassCheckSensor4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassCheckSensor4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassCheckSensor4.Location = new System.Drawing.Point(4, 118);
            this.chkGlassCheckSensor4.Name = "chkGlassCheckSensor4";
            this.chkGlassCheckSensor4.Size = new System.Drawing.Size(80, 28);
            this.chkGlassCheckSensor4.TabIndex = 25;
            this.chkGlassCheckSensor4.Text = "Sensor 4";
            this.chkGlassCheckSensor4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassCheckSensor4.UseVisualStyleBackColor = false;
            this.chkGlassCheckSensor4.Visible = false;
            // 
            // chkGlassCheckSensor3
            // 
            this.chkGlassCheckSensor3.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassCheckSensor3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassCheckSensor3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassCheckSensor3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassCheckSensor3.Location = new System.Drawing.Point(4, 86);
            this.chkGlassCheckSensor3.Name = "chkGlassCheckSensor3";
            this.chkGlassCheckSensor3.Size = new System.Drawing.Size(80, 28);
            this.chkGlassCheckSensor3.TabIndex = 24;
            this.chkGlassCheckSensor3.Text = "Sensor 3";
            this.chkGlassCheckSensor3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassCheckSensor3.UseVisualStyleBackColor = false;
            this.chkGlassCheckSensor3.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoEllipsis = true;
            this.label12.BackColor = System.Drawing.Color.Gainsboro;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 24);
            this.label12.TabIndex = 9;
            this.label12.Text = "■ GLASS DETE";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkGlassCheckSensor1
            // 
            this.chkGlassCheckSensor1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassCheckSensor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassCheckSensor1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassCheckSensor1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassCheckSensor1.Location = new System.Drawing.Point(4, 30);
            this.chkGlassCheckSensor1.Name = "chkGlassCheckSensor1";
            this.chkGlassCheckSensor1.Size = new System.Drawing.Size(80, 28);
            this.chkGlassCheckSensor1.TabIndex = 22;
            this.chkGlassCheckSensor1.Text = "Sensor 1";
            this.chkGlassCheckSensor1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassCheckSensor1.UseVisualStyleBackColor = false;
            // 
            // chkGlassCheckSensor2
            // 
            this.chkGlassCheckSensor2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassCheckSensor2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassCheckSensor2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassCheckSensor2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassCheckSensor2.Location = new System.Drawing.Point(4, 58);
            this.chkGlassCheckSensor2.Name = "chkGlassCheckSensor2";
            this.chkGlassCheckSensor2.Size = new System.Drawing.Size(80, 28);
            this.chkGlassCheckSensor2.TabIndex = 23;
            this.chkGlassCheckSensor2.Text = "Sensor 2";
            this.chkGlassCheckSensor2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassCheckSensor2.UseVisualStyleBackColor = false;
            // 
            // txtReadValue
            // 
            this.txtReadValue.Location = new System.Drawing.Point(6, 251);
            this.txtReadValue.Name = "txtReadValue";
            this.txtReadValue.Size = new System.Drawing.Size(100, 23);
            this.txtReadValue.TabIndex = 105;
            // 
            // txtReadAddr
            // 
            this.txtReadAddr.Location = new System.Drawing.Point(6, 222);
            this.txtReadAddr.Name = "txtReadAddr";
            this.txtReadAddr.Size = new System.Drawing.Size(100, 23);
            this.txtReadAddr.TabIndex = 105;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.chkDoor02);
            this.panel3.Controls.Add(this.chkDoorAll);
            this.panel3.Controls.Add(this.chkDoor05);
            this.panel3.Controls.Add(this.chkDoor04);
            this.panel3.Controls.Add(this.chkDoor01);
            this.panel3.Controls.Add(this.chkDoor03);
            this.panel3.Controls.Add(this.chkDoor06);
            this.panel3.Controls.Add(this.chkDoor07);
            this.panel3.Controls.Add(this.chkDoor08);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Location = new System.Drawing.Point(295, 8);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(138, 210);
            this.panel3.TabIndex = 89;
            // 
            // chkDoor02
            // 
            this.chkDoor02.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor02.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor02.Location = new System.Drawing.Point(7, 58);
            this.chkDoor02.Name = "chkDoor02";
            this.chkDoor02.Size = new System.Drawing.Size(61, 28);
            this.chkDoor02.TabIndex = 51;
            this.chkDoor02.Text = "T Door 02";
            this.chkDoor02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor02.UseVisualStyleBackColor = false;
            // 
            // chkDoorAll
            // 
            this.chkDoorAll.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoorAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoorAll.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoorAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoorAll.Location = new System.Drawing.Point(74, 176);
            this.chkDoorAll.Name = "chkDoorAll";
            this.chkDoorAll.Size = new System.Drawing.Size(61, 28);
            this.chkDoorAll.TabIndex = 52;
            this.chkDoorAll.Text = "All";
            this.chkDoorAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoorAll.UseVisualStyleBackColor = false;
            this.chkDoorAll.CheckedChanged += new System.EventHandler(this.chkDoorAll_CheckedChanged);
            // 
            // chkDoor05
            // 
            this.chkDoor05.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor05.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor05.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor05.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor05.Location = new System.Drawing.Point(7, 148);
            this.chkDoor05.Name = "chkDoor05";
            this.chkDoor05.Size = new System.Drawing.Size(61, 28);
            this.chkDoor05.TabIndex = 52;
            this.chkDoor05.Text = "T Door 05";
            this.chkDoor05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor05.UseVisualStyleBackColor = false;
            // 
            // chkDoor04
            // 
            this.chkDoor04.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor04.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor04.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor04.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor04.Location = new System.Drawing.Point(7, 118);
            this.chkDoor04.Name = "chkDoor04";
            this.chkDoor04.Size = new System.Drawing.Size(61, 28);
            this.chkDoor04.TabIndex = 53;
            this.chkDoor04.Text = "T Door 04";
            this.chkDoor04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor04.UseVisualStyleBackColor = false;
            // 
            // chkDoor01
            // 
            this.chkDoor01.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor01.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor01.Location = new System.Drawing.Point(7, 30);
            this.chkDoor01.Name = "chkDoor01";
            this.chkDoor01.Size = new System.Drawing.Size(61, 28);
            this.chkDoor01.TabIndex = 55;
            this.chkDoor01.Text = "T Door 01";
            this.chkDoor01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor01.UseVisualStyleBackColor = false;
            // 
            // chkDoor03
            // 
            this.chkDoor03.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor03.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor03.Location = new System.Drawing.Point(7, 88);
            this.chkDoor03.Name = "chkDoor03";
            this.chkDoor03.Size = new System.Drawing.Size(61, 28);
            this.chkDoor03.TabIndex = 54;
            this.chkDoor03.Text = "T Door 03";
            this.chkDoor03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor03.UseVisualStyleBackColor = false;
            // 
            // chkDoor06
            // 
            this.chkDoor06.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor06.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor06.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor06.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor06.Location = new System.Drawing.Point(68, 30);
            this.chkDoor06.Name = "chkDoor06";
            this.chkDoor06.Size = new System.Drawing.Size(61, 28);
            this.chkDoor06.TabIndex = 47;
            this.chkDoor06.Text = "T Door 06";
            this.chkDoor06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor06.UseVisualStyleBackColor = false;
            // 
            // chkDoor07
            // 
            this.chkDoor07.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor07.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor07.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor07.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor07.Location = new System.Drawing.Point(68, 58);
            this.chkDoor07.Name = "chkDoor07";
            this.chkDoor07.Size = new System.Drawing.Size(61, 28);
            this.chkDoor07.TabIndex = 48;
            this.chkDoor07.Text = "T Door 07";
            this.chkDoor07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor07.UseVisualStyleBackColor = false;
            // 
            // chkDoor08
            // 
            this.chkDoor08.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkDoor08.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkDoor08.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkDoor08.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkDoor08.Location = new System.Drawing.Point(68, 88);
            this.chkDoor08.Name = "chkDoor08";
            this.chkDoor08.Size = new System.Drawing.Size(61, 28);
            this.chkDoor08.TabIndex = 49;
            this.chkDoor08.Text = "T Door 08";
            this.chkDoor08.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDoor08.UseVisualStyleBackColor = false;
            // 
            // label15
            // 
            this.label15.AutoEllipsis = true;
            this.label15.BackColor = System.Drawing.Color.Gainsboro;
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(136, 24);
            this.label15.TabIndex = 9;
            this.label15.Text = "■ DOOR";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.chkIonizer1On);
            this.panel8.Controls.Add(this.chkIonizer2On);
            this.panel8.Controls.Add(this.chkIonizer4On);
            this.panel8.Controls.Add(this.chkIonizer3On);
            this.panel8.Location = new System.Drawing.Point(310, 465);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(92, 161);
            this.panel8.TabIndex = 89;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "■ IONIZER";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkIonizer1On
            // 
            this.chkIonizer1On.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIonizer1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIonizer1On.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIonizer1On.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIonizer1On.Location = new System.Drawing.Point(5, 31);
            this.chkIonizer1On.Name = "chkIonizer1On";
            this.chkIonizer1On.Size = new System.Drawing.Size(80, 28);
            this.chkIonizer1On.TabIndex = 16;
            this.chkIonizer1On.Text = "Ionizer 1";
            this.chkIonizer1On.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIonizer1On.UseVisualStyleBackColor = false;
            // 
            // chkIonizer2On
            // 
            this.chkIonizer2On.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIonizer2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIonizer2On.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIonizer2On.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIonizer2On.Location = new System.Drawing.Point(5, 59);
            this.chkIonizer2On.Name = "chkIonizer2On";
            this.chkIonizer2On.Size = new System.Drawing.Size(80, 28);
            this.chkIonizer2On.TabIndex = 16;
            this.chkIonizer2On.Text = "Ionizer 2";
            this.chkIonizer2On.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIonizer2On.UseVisualStyleBackColor = false;
            // 
            // chkIonizer4On
            // 
            this.chkIonizer4On.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIonizer4On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIonizer4On.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIonizer4On.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIonizer4On.Location = new System.Drawing.Point(5, 119);
            this.chkIonizer4On.Name = "chkIonizer4On";
            this.chkIonizer4On.Size = new System.Drawing.Size(80, 28);
            this.chkIonizer4On.TabIndex = 16;
            this.chkIonizer4On.Text = "Ionizer 4";
            this.chkIonizer4On.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIonizer4On.UseVisualStyleBackColor = false;
            // 
            // chkIonizer3On
            // 
            this.chkIonizer3On.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIonizer3On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIonizer3On.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIonizer3On.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIonizer3On.Location = new System.Drawing.Point(5, 89);
            this.chkIonizer3On.Name = "chkIonizer3On";
            this.chkIonizer3On.Size = new System.Drawing.Size(80, 28);
            this.chkIonizer3On.TabIndex = 16;
            this.chkIonizer3On.Text = "Ionizer 3";
            this.chkIonizer3On.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIonizer3On.UseVisualStyleBackColor = false;
            // 
            // chkReadHsms
            // 
            this.chkReadHsms.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReadHsms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReadHsms.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReadHsms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReadHsms.Location = new System.Drawing.Point(112, 222);
            this.chkReadHsms.Name = "chkReadHsms";
            this.chkReadHsms.Size = new System.Drawing.Size(61, 52);
            this.chkReadHsms.TabIndex = 47;
            this.chkReadHsms.Text = "Read From HSMS";
            this.chkReadHsms.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReadHsms.UseVisualStyleBackColor = false;
            this.chkReadHsms.Click += new System.EventHandler(this.btnReadHsmsValue_Click);
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.chkIsol01);
            this.panel7.Controls.Add(this.chkIsol02);
            this.panel7.Controls.Add(this.chkIsol04);
            this.panel7.Controls.Add(this.chkIsol03);
            this.panel7.Location = new System.Drawing.Point(214, 465);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(92, 161);
            this.panel7.TabIndex = 89;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 24);
            this.label6.TabIndex = 9;
            this.label6.Text = "■ ISOL";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkIsol01
            // 
            this.chkIsol01.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIsol01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIsol01.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIsol01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIsol01.Location = new System.Drawing.Point(5, 31);
            this.chkIsol01.Name = "chkIsol01";
            this.chkIsol01.Size = new System.Drawing.Size(80, 28);
            this.chkIsol01.TabIndex = 16;
            this.chkIsol01.Text = "ISOL 01";
            this.chkIsol01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIsol01.UseVisualStyleBackColor = false;
            // 
            // chkIsol02
            // 
            this.chkIsol02.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIsol02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIsol02.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIsol02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIsol02.Location = new System.Drawing.Point(5, 59);
            this.chkIsol02.Name = "chkIsol02";
            this.chkIsol02.Size = new System.Drawing.Size(80, 28);
            this.chkIsol02.TabIndex = 16;
            this.chkIsol02.Text = "ISOL 02";
            this.chkIsol02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIsol02.UseVisualStyleBackColor = false;
            // 
            // chkIsol04
            // 
            this.chkIsol04.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIsol04.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIsol04.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIsol04.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIsol04.Location = new System.Drawing.Point(5, 119);
            this.chkIsol04.Name = "chkIsol04";
            this.chkIsol04.Size = new System.Drawing.Size(80, 28);
            this.chkIsol04.TabIndex = 16;
            this.chkIsol04.Text = "ISOL 04";
            this.chkIsol04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIsol04.UseVisualStyleBackColor = false;
            // 
            // chkIsol03
            // 
            this.chkIsol03.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIsol03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIsol03.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIsol03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIsol03.Location = new System.Drawing.Point(5, 89);
            this.chkIsol03.Name = "chkIsol03";
            this.chkIsol03.Size = new System.Drawing.Size(80, 28);
            this.chkIsol03.TabIndex = 16;
            this.chkIsol03.Text = "ISOL 03";
            this.chkIsol03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIsol03.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.chkGlassEdgeCheckSensor1);
            this.panel5.Controls.Add(this.chkGlassEdgeCheckSensor3);
            this.panel5.Controls.Add(this.chkGlassEdgeCheckSensor2);
            this.panel5.Controls.Add(this.chkGlassEdgeCheckSensor4);
            this.panel5.Location = new System.Drawing.Point(119, 465);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(92, 161);
            this.panel5.TabIndex = 89;
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.BackColor = System.Drawing.Color.Gainsboro;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "■ GLASS EDGE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkGlassEdgeCheckSensor1
            // 
            this.chkGlassEdgeCheckSensor1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassEdgeCheckSensor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassEdgeCheckSensor1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassEdgeCheckSensor1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassEdgeCheckSensor1.Location = new System.Drawing.Point(5, 31);
            this.chkGlassEdgeCheckSensor1.Name = "chkGlassEdgeCheckSensor1";
            this.chkGlassEdgeCheckSensor1.Size = new System.Drawing.Size(80, 28);
            this.chkGlassEdgeCheckSensor1.TabIndex = 23;
            this.chkGlassEdgeCheckSensor1.Text = "Sensor 1";
            this.chkGlassEdgeCheckSensor1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassEdgeCheckSensor1.UseVisualStyleBackColor = false;
            // 
            // chkGlassEdgeCheckSensor3
            // 
            this.chkGlassEdgeCheckSensor3.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassEdgeCheckSensor3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassEdgeCheckSensor3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassEdgeCheckSensor3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassEdgeCheckSensor3.Location = new System.Drawing.Point(5, 89);
            this.chkGlassEdgeCheckSensor3.Name = "chkGlassEdgeCheckSensor3";
            this.chkGlassEdgeCheckSensor3.Size = new System.Drawing.Size(80, 28);
            this.chkGlassEdgeCheckSensor3.TabIndex = 23;
            this.chkGlassEdgeCheckSensor3.Text = "Sensor 3";
            this.chkGlassEdgeCheckSensor3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassEdgeCheckSensor3.UseVisualStyleBackColor = false;
            // 
            // chkGlassEdgeCheckSensor2
            // 
            this.chkGlassEdgeCheckSensor2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassEdgeCheckSensor2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassEdgeCheckSensor2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassEdgeCheckSensor2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassEdgeCheckSensor2.Location = new System.Drawing.Point(5, 59);
            this.chkGlassEdgeCheckSensor2.Name = "chkGlassEdgeCheckSensor2";
            this.chkGlassEdgeCheckSensor2.Size = new System.Drawing.Size(80, 28);
            this.chkGlassEdgeCheckSensor2.TabIndex = 24;
            this.chkGlassEdgeCheckSensor2.Text = "Sensor 2";
            this.chkGlassEdgeCheckSensor2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassEdgeCheckSensor2.UseVisualStyleBackColor = false;
            // 
            // chkGlassEdgeCheckSensor4
            // 
            this.chkGlassEdgeCheckSensor4.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGlassEdgeCheckSensor4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkGlassEdgeCheckSensor4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGlassEdgeCheckSensor4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkGlassEdgeCheckSensor4.Location = new System.Drawing.Point(5, 119);
            this.chkGlassEdgeCheckSensor4.Name = "chkGlassEdgeCheckSensor4";
            this.chkGlassEdgeCheckSensor4.Size = new System.Drawing.Size(80, 28);
            this.chkGlassEdgeCheckSensor4.TabIndex = 24;
            this.chkGlassEdgeCheckSensor4.Text = "Sensor 4";
            this.chkGlassEdgeCheckSensor4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkGlassEdgeCheckSensor4.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.chkEmo07);
            this.panel4.Controls.Add(this.chkEmo06);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.chkEmo02);
            this.panel4.Controls.Add(this.chkEmo01);
            this.panel4.Controls.Add(this.chkEmo03);
            this.panel4.Controls.Add(this.chkEmo05);
            this.panel4.Controls.Add(this.chkEmo04);
            this.panel4.Location = new System.Drawing.Point(25, 465);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(91, 220);
            this.panel4.TabIndex = 89;
            // 
            // chkEmo07
            // 
            this.chkEmo07.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEmo07.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmo07.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEmo07.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEmo07.Location = new System.Drawing.Point(6, 191);
            this.chkEmo07.Name = "chkEmo07";
            this.chkEmo07.Size = new System.Drawing.Size(80, 28);
            this.chkEmo07.TabIndex = 11;
            this.chkEmo07.Text = "EMO07";
            this.chkEmo07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEmo07.UseVisualStyleBackColor = false;
            this.chkEmo07.Visible = false;
            // 
            // chkEmo06
            // 
            this.chkEmo06.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEmo06.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmo06.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEmo06.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEmo06.Location = new System.Drawing.Point(6, 166);
            this.chkEmo06.Name = "chkEmo06";
            this.chkEmo06.Size = new System.Drawing.Size(80, 28);
            this.chkEmo06.TabIndex = 10;
            this.chkEmo06.Text = "EMO06";
            this.chkEmo06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEmo06.UseVisualStyleBackColor = false;
            this.chkEmo06.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "■ EMO";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkEmo02
            // 
            this.chkEmo02.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEmo02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmo02.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEmo02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEmo02.Location = new System.Drawing.Point(6, 54);
            this.chkEmo02.Name = "chkEmo02";
            this.chkEmo02.Size = new System.Drawing.Size(80, 28);
            this.chkEmo02.TabIndex = 7;
            this.chkEmo02.Text = "EMO02";
            this.chkEmo02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEmo02.UseVisualStyleBackColor = false;
            // 
            // chkEmo01
            // 
            this.chkEmo01.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEmo01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmo01.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEmo01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEmo01.Location = new System.Drawing.Point(6, 26);
            this.chkEmo01.Name = "chkEmo01";
            this.chkEmo01.Size = new System.Drawing.Size(80, 28);
            this.chkEmo01.TabIndex = 6;
            this.chkEmo01.Text = "EMO01";
            this.chkEmo01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEmo01.UseVisualStyleBackColor = false;
            // 
            // chkEmo03
            // 
            this.chkEmo03.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEmo03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmo03.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEmo03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEmo03.Location = new System.Drawing.Point(6, 82);
            this.chkEmo03.Name = "chkEmo03";
            this.chkEmo03.Size = new System.Drawing.Size(80, 28);
            this.chkEmo03.TabIndex = 8;
            this.chkEmo03.Text = "EMO03";
            this.chkEmo03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEmo03.UseVisualStyleBackColor = false;
            this.chkEmo03.Visible = false;
            // 
            // chkEmo05
            // 
            this.chkEmo05.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEmo05.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmo05.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEmo05.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEmo05.Location = new System.Drawing.Point(6, 138);
            this.chkEmo05.Name = "chkEmo05";
            this.chkEmo05.Size = new System.Drawing.Size(80, 28);
            this.chkEmo05.TabIndex = 9;
            this.chkEmo05.Text = "EMO05";
            this.chkEmo05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEmo05.UseVisualStyleBackColor = false;
            this.chkEmo05.Visible = false;
            // 
            // chkEmo04
            // 
            this.chkEmo04.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEmo04.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmo04.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEmo04.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEmo04.Location = new System.Drawing.Point(6, 110);
            this.chkEmo04.Name = "chkEmo04";
            this.chkEmo04.Size = new System.Drawing.Size(80, 28);
            this.chkEmo04.TabIndex = 9;
            this.chkEmo04.Text = "EMO04";
            this.chkEmo04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEmo04.UseVisualStyleBackColor = false;
            this.chkEmo04.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel18);
            this.tabPage1.Controls.Add(this.panel15);
            this.tabPage1.Controls.Add(this.panel14);
            this.tabPage1.Controls.Add(this.panel13);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.chkInspXError);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel12);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(488, 833);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.label20);
            this.panel18.Controls.Add(this.chkUDBMute);
            this.panel18.Controls.Add(this.chkUDAMute);
            this.panel18.Location = new System.Drawing.Point(193, 325);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(251, 83);
            this.panel18.TabIndex = 105;
            // 
            // label20
            // 
            this.label20.AutoEllipsis = true;
            this.label20.BackColor = System.Drawing.Color.Gainsboro;
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(249, 24);
            this.label20.TabIndex = 9;
            this.label20.Text = "■ 배출버튼";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkUDBMute
            // 
            this.chkUDBMute.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUDBMute.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUDBMute.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUDBMute.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUDBMute.Location = new System.Drawing.Point(166, 50);
            this.chkUDBMute.Name = "chkUDBMute";
            this.chkUDBMute.Size = new System.Drawing.Size(80, 28);
            this.chkUDBMute.TabIndex = 16;
            this.chkUDBMute.Text = "UD B Mute";
            this.chkUDBMute.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUDBMute.UseVisualStyleBackColor = false;
            // 
            // chkUDAMute
            // 
            this.chkUDAMute.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUDAMute.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUDAMute.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUDAMute.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUDAMute.Location = new System.Drawing.Point(156, 24);
            this.chkUDAMute.Name = "chkUDAMute";
            this.chkUDAMute.Size = new System.Drawing.Size(80, 28);
            this.chkUDAMute.TabIndex = 16;
            this.chkUDAMute.Text = "UD A Mute";
            this.chkUDAMute.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUDAMute.UseVisualStyleBackColor = false;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.label18);
            this.panel15.Controls.Add(this.chkLdStageOutBtn2);
            this.panel15.Controls.Add(this.chkLdStageOutBtn1);
            this.panel15.Location = new System.Drawing.Point(193, 237);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(251, 83);
            this.panel15.TabIndex = 104;
            // 
            // label18
            // 
            this.label18.AutoEllipsis = true;
            this.label18.BackColor = System.Drawing.Color.Gainsboro;
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(249, 24);
            this.label18.TabIndex = 9;
            this.label18.Text = "■ 투입버튼";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkLdStageOutBtn2
            // 
            this.chkLdStageOutBtn2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLdStageOutBtn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLdStageOutBtn2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLdStageOutBtn2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLdStageOutBtn2.Location = new System.Drawing.Point(166, 50);
            this.chkLdStageOutBtn2.Name = "chkLdStageOutBtn2";
            this.chkLdStageOutBtn2.Size = new System.Drawing.Size(80, 28);
            this.chkLdStageOutBtn2.TabIndex = 16;
            this.chkLdStageOutBtn2.Text = "Ld B Mute";
            this.chkLdStageOutBtn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLdStageOutBtn2.UseVisualStyleBackColor = false;
            // 
            // chkLdStageOutBtn1
            // 
            this.chkLdStageOutBtn1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLdStageOutBtn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLdStageOutBtn1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLdStageOutBtn1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLdStageOutBtn1.Location = new System.Drawing.Point(170, 25);
            this.chkLdStageOutBtn1.Name = "chkLdStageOutBtn1";
            this.chkLdStageOutBtn1.Size = new System.Drawing.Size(80, 28);
            this.chkLdStageOutBtn1.TabIndex = 16;
            this.chkLdStageOutBtn1.Text = "Ld A Mute";
            this.chkLdStageOutBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLdStageOutBtn1.UseVisualStyleBackColor = false;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.label23);
            this.panel14.Controls.Add(this.lblLoRecvComplete);
            this.panel14.Controls.Add(this.chkRobotArmCheck);
            this.panel14.Controls.Add(this.label33);
            this.panel14.Controls.Add(this.btnRecvAbleStart);
            this.panel14.Controls.Add(this.btnSendAbleStart);
            this.panel14.Controls.Add(this.label35);
            this.panel14.Controls.Add(this.label37);
            this.panel14.Controls.Add(this.lblLoRecvStart);
            this.panel14.Controls.Add(this.lblLoRecvAble);
            this.panel14.Controls.Add(this.lblAoiSendComplete);
            this.panel14.Controls.Add(this.lblAoiSendStart);
            this.panel14.Controls.Add(this.lblAoiSendAble);
            this.panel14.Controls.Add(this.lblAoiRecvComplete);
            this.panel14.Controls.Add(this.lblAoiRecvStart);
            this.panel14.Controls.Add(this.lblAoiRecvAble);
            this.panel14.Controls.Add(this.lblUpSendComplete);
            this.panel14.Controls.Add(this.lblUpSendStart);
            this.panel14.Controls.Add(this.lblUpSendAble);
            this.panel14.Controls.Add(this.label38);
            this.panel14.Location = new System.Drawing.Point(218, 6);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(296, 214);
            this.panel14.TabIndex = 103;
            // 
            // label23
            // 
            this.label23.AutoEllipsis = true;
            this.label23.BackColor = System.Drawing.Color.Silver;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.Firebrick;
            this.label23.Location = new System.Drawing.Point(87, 117);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 21);
            this.label23.TabIndex = 65;
            this.label23.Text = "AOI";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoRecvComplete
            // 
            this.lblLoRecvComplete.AutoEllipsis = true;
            this.lblLoRecvComplete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblLoRecvComplete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoRecvComplete.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoRecvComplete.ForeColor = System.Drawing.Color.White;
            this.lblLoRecvComplete.Location = new System.Drawing.Point(4, 183);
            this.lblLoRecvComplete.Name = "lblLoRecvComplete";
            this.lblLoRecvComplete.Size = new System.Drawing.Size(80, 20);
            this.lblLoRecvComplete.TabIndex = 64;
            this.lblLoRecvComplete.Text = "R_COMPLETE";
            this.lblLoRecvComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkRobotArmCheck
            // 
            this.chkRobotArmCheck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkRobotArmCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkRobotArmCheck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkRobotArmCheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkRobotArmCheck.Location = new System.Drawing.Point(230, 40);
            this.chkRobotArmCheck.Name = "chkRobotArmCheck";
            this.chkRobotArmCheck.Size = new System.Drawing.Size(64, 65);
            this.chkRobotArmCheck.TabIndex = 23;
            this.chkRobotArmCheck.Text = "Robot Arm Check";
            this.chkRobotArmCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkRobotArmCheck.UseVisualStyleBackColor = false;
            // 
            // label33
            // 
            this.label33.AutoEllipsis = true;
            this.label33.BackColor = System.Drawing.Color.Silver;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.Color.Firebrick;
            this.label33.Location = new System.Drawing.Point(87, 26);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(80, 21);
            this.label33.TabIndex = 10;
            this.label33.Text = "AOI";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRecvAbleStart
            // 
            this.btnRecvAbleStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRecvAbleStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRecvAbleStart.Location = new System.Drawing.Point(173, 131);
            this.btnRecvAbleStart.Name = "btnRecvAbleStart";
            this.btnRecvAbleStart.Size = new System.Drawing.Size(51, 72);
            this.btnRecvAbleStart.TabIndex = 92;
            this.btnRecvAbleStart.Text = "배출";
            this.btnRecvAbleStart.UseVisualStyleBackColor = false;
            this.btnRecvAbleStart.Click += new System.EventHandler(this.btnRecvAbleStart_Click);
            // 
            // btnSendAbleStart
            // 
            this.btnSendAbleStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSendAbleStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnSendAbleStart.Location = new System.Drawing.Point(173, 40);
            this.btnSendAbleStart.Name = "btnSendAbleStart";
            this.btnSendAbleStart.Size = new System.Drawing.Size(51, 72);
            this.btnSendAbleStart.TabIndex = 92;
            this.btnSendAbleStart.Text = "투입";
            this.btnSendAbleStart.UseVisualStyleBackColor = false;
            this.btnSendAbleStart.Click += new System.EventHandler(this.btnSendAbleStart_Click);
            // 
            // label35
            // 
            this.label35.AutoEllipsis = true;
            this.label35.BackColor = System.Drawing.Color.Silver;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label35.ForeColor = System.Drawing.Color.Firebrick;
            this.label35.Location = new System.Drawing.Point(4, 26);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(80, 21);
            this.label35.TabIndex = 10;
            this.label35.Text = "UPPER";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.AutoEllipsis = true;
            this.label37.BackColor = System.Drawing.Color.Silver;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label37.ForeColor = System.Drawing.Color.Firebrick;
            this.label37.Location = new System.Drawing.Point(4, 117);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(80, 21);
            this.label37.TabIndex = 10;
            this.label37.Text = "LOWER";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoRecvStart
            // 
            this.lblLoRecvStart.AutoEllipsis = true;
            this.lblLoRecvStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblLoRecvStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoRecvStart.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoRecvStart.ForeColor = System.Drawing.Color.White;
            this.lblLoRecvStart.Location = new System.Drawing.Point(4, 161);
            this.lblLoRecvStart.Name = "lblLoRecvStart";
            this.lblLoRecvStart.Size = new System.Drawing.Size(80, 20);
            this.lblLoRecvStart.TabIndex = 63;
            this.lblLoRecvStart.Text = "R_START";
            this.lblLoRecvStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLoRecvAble
            // 
            this.lblLoRecvAble.AutoEllipsis = true;
            this.lblLoRecvAble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblLoRecvAble.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLoRecvAble.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoRecvAble.ForeColor = System.Drawing.Color.White;
            this.lblLoRecvAble.Location = new System.Drawing.Point(4, 139);
            this.lblLoRecvAble.Name = "lblLoRecvAble";
            this.lblLoRecvAble.Size = new System.Drawing.Size(80, 20);
            this.lblLoRecvAble.TabIndex = 62;
            this.lblLoRecvAble.Text = "R_ABLE";
            this.lblLoRecvAble.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAoiSendComplete
            // 
            this.lblAoiSendComplete.AutoEllipsis = true;
            this.lblAoiSendComplete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAoiSendComplete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAoiSendComplete.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAoiSendComplete.ForeColor = System.Drawing.Color.White;
            this.lblAoiSendComplete.Location = new System.Drawing.Point(87, 183);
            this.lblAoiSendComplete.Name = "lblAoiSendComplete";
            this.lblAoiSendComplete.Size = new System.Drawing.Size(80, 20);
            this.lblAoiSendComplete.TabIndex = 61;
            this.lblAoiSendComplete.Text = "S_COMPLETE";
            this.lblAoiSendComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAoiSendStart
            // 
            this.lblAoiSendStart.AutoEllipsis = true;
            this.lblAoiSendStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAoiSendStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAoiSendStart.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAoiSendStart.ForeColor = System.Drawing.Color.White;
            this.lblAoiSendStart.Location = new System.Drawing.Point(87, 161);
            this.lblAoiSendStart.Name = "lblAoiSendStart";
            this.lblAoiSendStart.Size = new System.Drawing.Size(80, 20);
            this.lblAoiSendStart.TabIndex = 60;
            this.lblAoiSendStart.Text = "S_START";
            this.lblAoiSendStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAoiSendAble
            // 
            this.lblAoiSendAble.AutoEllipsis = true;
            this.lblAoiSendAble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAoiSendAble.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAoiSendAble.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAoiSendAble.ForeColor = System.Drawing.Color.White;
            this.lblAoiSendAble.Location = new System.Drawing.Point(87, 139);
            this.lblAoiSendAble.Name = "lblAoiSendAble";
            this.lblAoiSendAble.Size = new System.Drawing.Size(80, 20);
            this.lblAoiSendAble.TabIndex = 59;
            this.lblAoiSendAble.Text = "S_ABLE";
            this.lblAoiSendAble.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAoiRecvComplete
            // 
            this.lblAoiRecvComplete.AutoEllipsis = true;
            this.lblAoiRecvComplete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAoiRecvComplete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAoiRecvComplete.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAoiRecvComplete.ForeColor = System.Drawing.Color.White;
            this.lblAoiRecvComplete.Location = new System.Drawing.Point(87, 92);
            this.lblAoiRecvComplete.Name = "lblAoiRecvComplete";
            this.lblAoiRecvComplete.Size = new System.Drawing.Size(80, 20);
            this.lblAoiRecvComplete.TabIndex = 58;
            this.lblAoiRecvComplete.Text = "R_COMPLETE";
            this.lblAoiRecvComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAoiRecvStart
            // 
            this.lblAoiRecvStart.AutoEllipsis = true;
            this.lblAoiRecvStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAoiRecvStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAoiRecvStart.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAoiRecvStart.ForeColor = System.Drawing.Color.White;
            this.lblAoiRecvStart.Location = new System.Drawing.Point(87, 70);
            this.lblAoiRecvStart.Name = "lblAoiRecvStart";
            this.lblAoiRecvStart.Size = new System.Drawing.Size(80, 20);
            this.lblAoiRecvStart.TabIndex = 57;
            this.lblAoiRecvStart.Text = "R_START";
            this.lblAoiRecvStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAoiRecvAble
            // 
            this.lblAoiRecvAble.AutoEllipsis = true;
            this.lblAoiRecvAble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAoiRecvAble.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAoiRecvAble.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAoiRecvAble.ForeColor = System.Drawing.Color.White;
            this.lblAoiRecvAble.Location = new System.Drawing.Point(87, 48);
            this.lblAoiRecvAble.Name = "lblAoiRecvAble";
            this.lblAoiRecvAble.Size = new System.Drawing.Size(80, 20);
            this.lblAoiRecvAble.TabIndex = 56;
            this.lblAoiRecvAble.Text = "R_ABLE";
            this.lblAoiRecvAble.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUpSendComplete
            // 
            this.lblUpSendComplete.AutoEllipsis = true;
            this.lblUpSendComplete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUpSendComplete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUpSendComplete.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpSendComplete.ForeColor = System.Drawing.Color.White;
            this.lblUpSendComplete.Location = new System.Drawing.Point(4, 92);
            this.lblUpSendComplete.Name = "lblUpSendComplete";
            this.lblUpSendComplete.Size = new System.Drawing.Size(80, 20);
            this.lblUpSendComplete.TabIndex = 55;
            this.lblUpSendComplete.Text = "S_COMPLETE";
            this.lblUpSendComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUpSendStart
            // 
            this.lblUpSendStart.AutoEllipsis = true;
            this.lblUpSendStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUpSendStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUpSendStart.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpSendStart.ForeColor = System.Drawing.Color.White;
            this.lblUpSendStart.Location = new System.Drawing.Point(4, 70);
            this.lblUpSendStart.Name = "lblUpSendStart";
            this.lblUpSendStart.Size = new System.Drawing.Size(80, 20);
            this.lblUpSendStart.TabIndex = 54;
            this.lblUpSendStart.Text = "S_START";
            this.lblUpSendStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUpSendAble
            // 
            this.lblUpSendAble.AutoEllipsis = true;
            this.lblUpSendAble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUpSendAble.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUpSendAble.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpSendAble.ForeColor = System.Drawing.Color.White;
            this.lblUpSendAble.Location = new System.Drawing.Point(4, 48);
            this.lblUpSendAble.Name = "lblUpSendAble";
            this.lblUpSendAble.Size = new System.Drawing.Size(80, 20);
            this.lblUpSendAble.TabIndex = 53;
            this.lblUpSendAble.Text = "S_ABLE";
            this.lblUpSendAble.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.AutoEllipsis = true;
            this.label38.BackColor = System.Drawing.Color.Gainsboro;
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(294, 24);
            this.label38.TabIndex = 9;
            this.label38.Text = "■ PIO";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.label16);
            this.panel13.Controls.Add(this.btnCtrlPcXy);
            this.panel13.Controls.Add(this.btnSimulPcXy);
            this.panel13.Controls.Add(this.btnTester);
            this.panel13.Controls.Add(this.btnXyIoAddr);
            this.panel13.Controls.Add(this.btnMemDetail);
            this.panel13.Controls.Add(this.btnXyIo);
            this.panel13.Controls.Add(this.chkUseInterlock);
            this.panel13.Location = new System.Drawing.Point(6, 10);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(312, 183);
            this.panel13.TabIndex = 89;
            // 
            // label16
            // 
            this.label16.AutoEllipsis = true;
            this.label16.BackColor = System.Drawing.Color.Gainsboro;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(310, 24);
            this.label16.TabIndex = 9;
            this.label16.Text = "■ 버튼";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCtrlPcXy
            // 
            this.btnCtrlPcXy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCtrlPcXy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnCtrlPcXy.Location = new System.Drawing.Point(2, 37);
            this.btnCtrlPcXy.Name = "btnCtrlPcXy";
            this.btnCtrlPcXy.Size = new System.Drawing.Size(97, 50);
            this.btnCtrlPcXy.TabIndex = 92;
            this.btnCtrlPcXy.Text = "주소 로드 CTRL PC XY";
            this.btnCtrlPcXy.UseVisualStyleBackColor = false;
            this.btnCtrlPcXy.Click += new System.EventHandler(this.btnCtrlPcXy_Click);
            // 
            // btnSimulPcXy
            // 
            this.btnSimulPcXy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSimulPcXy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnSimulPcXy.Location = new System.Drawing.Point(105, 37);
            this.btnSimulPcXy.Name = "btnSimulPcXy";
            this.btnSimulPcXy.Size = new System.Drawing.Size(97, 50);
            this.btnSimulPcXy.TabIndex = 92;
            this.btnSimulPcXy.Text = "주소 로드 SIMUL PC XY";
            this.btnSimulPcXy.UseVisualStyleBackColor = false;
            this.btnSimulPcXy.Click += new System.EventHandler(this.btnSimulPcXy_Click);
            // 
            // btnTester
            // 
            this.btnTester.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTester.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnTester.Location = new System.Drawing.Point(208, 37);
            this.btnTester.Name = "btnTester";
            this.btnTester.Size = new System.Drawing.Size(97, 50);
            this.btnTester.TabIndex = 92;
            this.btnTester.Text = "주소 로드 TEST XY";
            this.btnTester.UseVisualStyleBackColor = false;
            this.btnTester.Click += new System.EventHandler(this.btnTester_Click);
            // 
            // btnXyIoAddr
            // 
            this.btnXyIoAddr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnXyIoAddr.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnXyIoAddr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnXyIoAddr.Location = new System.Drawing.Point(2, 93);
            this.btnXyIoAddr.Name = "btnXyIoAddr";
            this.btnXyIoAddr.Size = new System.Drawing.Size(97, 50);
            this.btnXyIoAddr.TabIndex = 90;
            this.btnXyIoAddr.Text = "모니터 XY IO(ADDR)";
            this.btnXyIoAddr.UseVisualStyleBackColor = false;
            this.btnXyIoAddr.Click += new System.EventHandler(this.btnXyIoAddr_Click);
            // 
            // btnMemDetail
            // 
            this.btnMemDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnMemDetail.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnMemDetail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnMemDetail.Location = new System.Drawing.Point(105, 93);
            this.btnMemDetail.Name = "btnMemDetail";
            this.btnMemDetail.Size = new System.Drawing.Size(97, 50);
            this.btnMemDetail.TabIndex = 90;
            this.btnMemDetail.Text = "모니터 INSP & REV IO";
            this.btnMemDetail.UseVisualStyleBackColor = false;
            this.btnMemDetail.Click += new System.EventHandler(this.btnMemDetail_Click);
            // 
            // btnXyIo
            // 
            this.btnXyIo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnXyIo.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnXyIo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnXyIo.Location = new System.Drawing.Point(208, 93);
            this.btnXyIo.Name = "btnXyIo";
            this.btnXyIo.Size = new System.Drawing.Size(97, 50);
            this.btnXyIo.TabIndex = 90;
            this.btnXyIo.Text = "모니터 XY IO";
            this.btnXyIo.UseVisualStyleBackColor = false;
            this.btnXyIo.Click += new System.EventHandler(this.btnXyIo_Click);
            // 
            // chkUseInterlock
            // 
            this.chkUseInterlock.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUseInterlock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUseInterlock.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUseInterlock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUseInterlock.Location = new System.Drawing.Point(4, 142);
            this.chkUseInterlock.Name = "chkUseInterlock";
            this.chkUseInterlock.Size = new System.Drawing.Size(138, 40);
            this.chkUseInterlock.TabIndex = 12;
            this.chkUseInterlock.Text = "인터락 사용";
            this.chkUseInterlock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUseInterlock.UseVisualStyleBackColor = false;
            this.chkUseInterlock.CheckedChanged += new System.EventHandler(this.chkUseInterlock_CheckedChanged);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lblScanStep);
            this.panel6.Controls.Add(this.lblInspGUnloadingOk);
            this.panel6.Controls.Add(this.lblInspGUnloading);
            this.panel6.Controls.Add(this.lblInspScanEnd);
            this.panel6.Controls.Add(this.lblInspScanEndOk);
            this.panel6.Controls.Add(this.lblInspScanStartOk);
            this.panel6.Controls.Add(this.lblInspGLoadingOk);
            this.panel6.Controls.Add(this.lblInspScanStart);
            this.panel6.Controls.Add(this.lblInspScanReadyOk);
            this.panel6.Controls.Add(this.lblInspScanReady);
            this.panel6.Controls.Add(this.lblInspGLoading);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Location = new System.Drawing.Point(10, 537);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(194, 270);
            this.panel6.TabIndex = 89;
            // 
            // lblScanStep
            // 
            this.lblScanStep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblScanStep.Location = new System.Drawing.Point(87, 161);
            this.lblScanStep.Name = "lblScanStep";
            this.lblScanStep.Size = new System.Drawing.Size(98, 19);
            this.lblScanStep.TabIndex = 39;
            this.lblScanStep.Text = "0.0";
            this.lblScanStep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspGUnloadingOk
            // 
            this.lblInspGUnloadingOk.AutoEllipsis = true;
            this.lblInspGUnloadingOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspGUnloadingOk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspGUnloadingOk.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspGUnloadingOk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspGUnloadingOk.Location = new System.Drawing.Point(87, 136);
            this.lblInspGUnloadingOk.Name = "lblInspGUnloadingOk";
            this.lblInspGUnloadingOk.Size = new System.Drawing.Size(98, 20);
            this.lblInspGUnloadingOk.TabIndex = 77;
            this.lblInspGUnloadingOk.Text = "G Unloading Ok";
            this.lblInspGUnloadingOk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspGUnloading
            // 
            this.lblInspGUnloading.AutoEllipsis = true;
            this.lblInspGUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspGUnloading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspGUnloading.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspGUnloading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspGUnloading.Location = new System.Drawing.Point(4, 136);
            this.lblInspGUnloading.Name = "lblInspGUnloading";
            this.lblInspGUnloading.Size = new System.Drawing.Size(80, 20);
            this.lblInspGUnloading.TabIndex = 76;
            this.lblInspGUnloading.Text = "G Unloading";
            this.lblInspGUnloading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspScanEnd
            // 
            this.lblInspScanEnd.AutoEllipsis = true;
            this.lblInspScanEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspScanEnd.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspScanEnd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspScanEnd.Location = new System.Drawing.Point(4, 114);
            this.lblInspScanEnd.Name = "lblInspScanEnd";
            this.lblInspScanEnd.Size = new System.Drawing.Size(80, 20);
            this.lblInspScanEnd.TabIndex = 74;
            this.lblInspScanEnd.Text = "Scan End";
            this.lblInspScanEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspScanEndOk
            // 
            this.lblInspScanEndOk.AutoEllipsis = true;
            this.lblInspScanEndOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspScanEndOk.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspScanEndOk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspScanEndOk.Location = new System.Drawing.Point(87, 114);
            this.lblInspScanEndOk.Name = "lblInspScanEndOk";
            this.lblInspScanEndOk.Size = new System.Drawing.Size(98, 20);
            this.lblInspScanEndOk.TabIndex = 75;
            this.lblInspScanEndOk.Text = "Scan End Ok";
            this.lblInspScanEndOk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspScanStartOk
            // 
            this.lblInspScanStartOk.AutoEllipsis = true;
            this.lblInspScanStartOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspScanStartOk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspScanStartOk.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspScanStartOk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspScanStartOk.Location = new System.Drawing.Point(87, 93);
            this.lblInspScanStartOk.Name = "lblInspScanStartOk";
            this.lblInspScanStartOk.Size = new System.Drawing.Size(98, 20);
            this.lblInspScanStartOk.TabIndex = 73;
            this.lblInspScanStartOk.Text = "Scan Start Ok";
            this.lblInspScanStartOk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblInspScanStartOk.Click += new System.EventHandler(this.lblInspScanStartOk_Click);
            // 
            // lblInspGLoadingOk
            // 
            this.lblInspGLoadingOk.AutoEllipsis = true;
            this.lblInspGLoadingOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspGLoadingOk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspGLoadingOk.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspGLoadingOk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspGLoadingOk.Location = new System.Drawing.Point(87, 49);
            this.lblInspGLoadingOk.Name = "lblInspGLoadingOk";
            this.lblInspGLoadingOk.Size = new System.Drawing.Size(98, 20);
            this.lblInspGLoadingOk.TabIndex = 72;
            this.lblInspGLoadingOk.Text = "G Loading Ok";
            this.lblInspGLoadingOk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspScanStart
            // 
            this.lblInspScanStart.AutoEllipsis = true;
            this.lblInspScanStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspScanStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspScanStart.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspScanStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspScanStart.Location = new System.Drawing.Point(4, 93);
            this.lblInspScanStart.Name = "lblInspScanStart";
            this.lblInspScanStart.Size = new System.Drawing.Size(80, 20);
            this.lblInspScanStart.TabIndex = 71;
            this.lblInspScanStart.Text = "Scan Start";
            this.lblInspScanStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspScanReadyOk
            // 
            this.lblInspScanReadyOk.AutoEllipsis = true;
            this.lblInspScanReadyOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspScanReadyOk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspScanReadyOk.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspScanReadyOk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspScanReadyOk.Location = new System.Drawing.Point(87, 71);
            this.lblInspScanReadyOk.Name = "lblInspScanReadyOk";
            this.lblInspScanReadyOk.Size = new System.Drawing.Size(98, 20);
            this.lblInspScanReadyOk.TabIndex = 69;
            this.lblInspScanReadyOk.Text = "Scan Ready Ok";
            this.lblInspScanReadyOk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspScanReady
            // 
            this.lblInspScanReady.AutoEllipsis = true;
            this.lblInspScanReady.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspScanReady.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspScanReady.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspScanReady.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspScanReady.Location = new System.Drawing.Point(4, 71);
            this.lblInspScanReady.Name = "lblInspScanReady";
            this.lblInspScanReady.Size = new System.Drawing.Size(80, 20);
            this.lblInspScanReady.TabIndex = 70;
            this.lblInspScanReady.Text = "Scan Ready";
            this.lblInspScanReady.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInspGLoading
            // 
            this.lblInspGLoading.AutoEllipsis = true;
            this.lblInspGLoading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInspGLoading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInspGLoading.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspGLoading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblInspGLoading.Location = new System.Drawing.Point(4, 49);
            this.lblInspGLoading.Name = "lblInspGLoading";
            this.lblInspGLoading.Size = new System.Drawing.Size(80, 20);
            this.lblInspGLoading.TabIndex = 68;
            this.lblInspGLoading.Text = "G Loading";
            this.lblInspGLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoEllipsis = true;
            this.label25.BackColor = System.Drawing.Color.Silver;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.Firebrick;
            this.label25.Location = new System.Drawing.Point(87, 26);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(98, 21);
            this.label25.TabIndex = 10;
            this.label25.Text = "OUT";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoEllipsis = true;
            this.label24.BackColor = System.Drawing.Color.Silver;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.Firebrick;
            this.label24.Location = new System.Drawing.Point(4, 26);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(80, 21);
            this.label24.TabIndex = 10;
            this.label24.Text = "IN";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "■ INSP PC";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkInspXError
            // 
            this.chkInspXError.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkInspXError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkInspXError.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkInspXError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkInspXError.Location = new System.Drawing.Point(10, 504);
            this.chkInspXError.Name = "chkInspXError";
            this.chkInspXError.Size = new System.Drawing.Size(169, 28);
            this.chkInspXError.TabIndex = 10;
            this.chkInspXError.Text = "InspX Error";
            this.chkInspXError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkInspXError.UseVisualStyleBackColor = false;
            this.chkInspXError.CheckedChanged += new System.EventHandler(this.chkInspXError_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.chkReviLotEndAck);
            this.panel2.Controls.Add(this.chkReviLotStartAck);
            this.panel2.Controls.Add(this.chkReviReviEnd);
            this.panel2.Controls.Add(this.chkReviReviStartAck);
            this.panel2.Controls.Add(this.chkReviAlignStartAck);
            this.panel2.Controls.Add(this.chkReviUnloadingAck);
            this.panel2.Controls.Add(this.chkReviLoadingAck);
            this.panel2.Controls.Add(this.chkReviLotEnd);
            this.panel2.Controls.Add(this.chkReviLotStart);
            this.panel2.Controls.Add(this.chkReviReviEndAck);
            this.panel2.Controls.Add(this.chkReviReviStart);
            this.panel2.Controls.Add(this.chkReviUnloading);
            this.panel2.Controls.Add(this.chkReviAlignStart);
            this.panel2.Controls.Add(this.chkReviLoading);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(209, 537);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(204, 270);
            this.panel2.TabIndex = 89;
            // 
            // chkReviLotEndAck
            // 
            this.chkReviLotEndAck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviLotEndAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviLotEndAck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviLotEndAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviLotEndAck.Location = new System.Drawing.Point(85, 185);
            this.chkReviLotEndAck.Name = "chkReviLotEndAck";
            this.chkReviLotEndAck.Size = new System.Drawing.Size(99, 27);
            this.chkReviLotEndAck.TabIndex = 36;
            this.chkReviLotEndAck.Text = "Lot End Ack";
            this.chkReviLotEndAck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviLotEndAck.UseVisualStyleBackColor = false;
            // 
            // chkReviLotStartAck
            // 
            this.chkReviLotStartAck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviLotStartAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviLotStartAck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviLotStartAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviLotStartAck.Location = new System.Drawing.Point(86, 158);
            this.chkReviLotStartAck.Name = "chkReviLotStartAck";
            this.chkReviLotStartAck.Size = new System.Drawing.Size(99, 27);
            this.chkReviLotStartAck.TabIndex = 37;
            this.chkReviLotStartAck.Text = "Lot Start Ack";
            this.chkReviLotStartAck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviLotStartAck.UseVisualStyleBackColor = false;
            // 
            // chkReviReviEnd
            // 
            this.chkReviReviEnd.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviReviEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviReviEnd.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviReviEnd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviReviEnd.Location = new System.Drawing.Point(106, 131);
            this.chkReviReviEnd.Name = "chkReviReviEnd";
            this.chkReviReviEnd.Size = new System.Drawing.Size(79, 27);
            this.chkReviReviEnd.TabIndex = 38;
            this.chkReviReviEnd.Text = "Review End";
            this.chkReviReviEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviReviEnd.UseVisualStyleBackColor = false;
            this.chkReviReviEnd.CheckedChanged += new System.EventHandler(this.chkReviReviEnd_CheckedChanged);
            // 
            // chkReviReviStartAck
            // 
            this.chkReviReviStartAck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviReviStartAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviReviStartAck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviReviStartAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviReviStartAck.Location = new System.Drawing.Point(86, 104);
            this.chkReviReviStartAck.Name = "chkReviReviStartAck";
            this.chkReviReviStartAck.Size = new System.Drawing.Size(99, 27);
            this.chkReviReviStartAck.TabIndex = 33;
            this.chkReviReviStartAck.Text = "Review Start Ack";
            this.chkReviReviStartAck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviReviStartAck.UseVisualStyleBackColor = false;
            // 
            // chkReviAlignStartAck
            // 
            this.chkReviAlignStartAck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviAlignStartAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviAlignStartAck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviAlignStartAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviAlignStartAck.Location = new System.Drawing.Point(86, 77);
            this.chkReviAlignStartAck.Name = "chkReviAlignStartAck";
            this.chkReviAlignStartAck.Size = new System.Drawing.Size(99, 27);
            this.chkReviAlignStartAck.TabIndex = 34;
            this.chkReviAlignStartAck.Text = "Align Start Ack";
            this.chkReviAlignStartAck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviAlignStartAck.UseVisualStyleBackColor = false;
            // 
            // chkReviUnloadingAck
            // 
            this.chkReviUnloadingAck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviUnloadingAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviUnloadingAck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviUnloadingAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviUnloadingAck.Location = new System.Drawing.Point(85, 212);
            this.chkReviUnloadingAck.Name = "chkReviUnloadingAck";
            this.chkReviUnloadingAck.Size = new System.Drawing.Size(99, 27);
            this.chkReviUnloadingAck.TabIndex = 35;
            this.chkReviUnloadingAck.Text = "Loading Ack";
            this.chkReviUnloadingAck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviUnloadingAck.UseVisualStyleBackColor = false;
            // 
            // chkReviLoadingAck
            // 
            this.chkReviLoadingAck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviLoadingAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviLoadingAck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviLoadingAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviLoadingAck.Location = new System.Drawing.Point(86, 50);
            this.chkReviLoadingAck.Name = "chkReviLoadingAck";
            this.chkReviLoadingAck.Size = new System.Drawing.Size(99, 27);
            this.chkReviLoadingAck.TabIndex = 35;
            this.chkReviLoadingAck.Text = "Loading Ack";
            this.chkReviLoadingAck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviLoadingAck.UseVisualStyleBackColor = false;
            // 
            // chkReviLotEnd
            // 
            this.chkReviLotEnd.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviLotEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviLotEnd.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviLotEnd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviLotEnd.Location = new System.Drawing.Point(6, 185);
            this.chkReviLotEnd.Name = "chkReviLotEnd";
            this.chkReviLotEnd.Size = new System.Drawing.Size(76, 27);
            this.chkReviLotEnd.TabIndex = 29;
            this.chkReviLotEnd.Text = "Lot End";
            this.chkReviLotEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviLotEnd.UseVisualStyleBackColor = false;
            // 
            // chkReviLotStart
            // 
            this.chkReviLotStart.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviLotStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviLotStart.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviLotStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviLotStart.Location = new System.Drawing.Point(7, 158);
            this.chkReviLotStart.Name = "chkReviLotStart";
            this.chkReviLotStart.Size = new System.Drawing.Size(76, 27);
            this.chkReviLotStart.TabIndex = 27;
            this.chkReviLotStart.Text = "Lot Start";
            this.chkReviLotStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviLotStart.UseVisualStyleBackColor = false;
            // 
            // chkReviReviEndAck
            // 
            this.chkReviReviEndAck.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviReviEndAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviReviEndAck.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviReviEndAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviReviEndAck.Location = new System.Drawing.Point(7, 131);
            this.chkReviReviEndAck.Name = "chkReviReviEndAck";
            this.chkReviReviEndAck.Size = new System.Drawing.Size(105, 27);
            this.chkReviReviEndAck.TabIndex = 28;
            this.chkReviReviEndAck.Text = "Review End Ack";
            this.chkReviReviEndAck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviReviEndAck.UseVisualStyleBackColor = false;
            // 
            // chkReviReviStart
            // 
            this.chkReviReviStart.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviReviStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviReviStart.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviReviStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviReviStart.Location = new System.Drawing.Point(7, 104);
            this.chkReviReviStart.Name = "chkReviReviStart";
            this.chkReviReviStart.Size = new System.Drawing.Size(76, 27);
            this.chkReviReviStart.TabIndex = 32;
            this.chkReviReviStart.Text = "Review Start";
            this.chkReviReviStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviReviStart.UseVisualStyleBackColor = false;
            // 
            // chkReviUnloading
            // 
            this.chkReviUnloading.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviUnloading.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviUnloading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviUnloading.Location = new System.Drawing.Point(6, 212);
            this.chkReviUnloading.Name = "chkReviUnloading";
            this.chkReviUnloading.Size = new System.Drawing.Size(76, 27);
            this.chkReviUnloading.TabIndex = 31;
            this.chkReviUnloading.Text = "Loading";
            this.chkReviUnloading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviUnloading.UseVisualStyleBackColor = false;
            // 
            // chkReviAlignStart
            // 
            this.chkReviAlignStart.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviAlignStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviAlignStart.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviAlignStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviAlignStart.Location = new System.Drawing.Point(7, 77);
            this.chkReviAlignStart.Name = "chkReviAlignStart";
            this.chkReviAlignStart.Size = new System.Drawing.Size(76, 27);
            this.chkReviAlignStart.TabIndex = 30;
            this.chkReviAlignStart.Text = "Align Start";
            this.chkReviAlignStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviAlignStart.UseVisualStyleBackColor = false;
            // 
            // chkReviLoading
            // 
            this.chkReviLoading.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReviLoading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkReviLoading.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkReviLoading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkReviLoading.Location = new System.Drawing.Point(7, 50);
            this.chkReviLoading.Name = "chkReviLoading";
            this.chkReviLoading.Size = new System.Drawing.Size(76, 27);
            this.chkReviLoading.TabIndex = 31;
            this.chkReviLoading.Text = "Loading";
            this.chkReviLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReviLoading.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Firebrick;
            this.label2.Location = new System.Drawing.Point(87, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 27);
            this.label2.TabIndex = 10;
            this.label2.Text = "OUT";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Firebrick;
            this.label3.Location = new System.Drawing.Point(4, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 21);
            this.label3.TabIndex = 10;
            this.label3.Text = "IN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoEllipsis = true;
            this.label10.BackColor = System.Drawing.Color.Gainsboro;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(202, 24);
            this.label10.TabIndex = 9;
            this.label10.Text = "■ REV PC";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.label13);
            this.panel12.Controls.Add(this.chkCoolineMainAir);
            this.panel12.Controls.Add(this.chkIonizerMainAir);
            this.panel12.Controls.Add(this.chkCameraCooling);
            this.panel12.Controls.Add(this.chkEnableGripSwOn);
            this.panel12.Controls.Add(this.chkStageGlassSensor1);
            this.panel12.Controls.Add(this.chkSafetyModeKeySW);
            this.panel12.Controls.Add(this.chkIonizerCover);
            this.panel12.Controls.Add(this.chkIonizer);
            this.panel12.Location = new System.Drawing.Point(6, 200);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(181, 302);
            this.panel12.TabIndex = 90;
            // 
            // label13
            // 
            this.label13.AutoEllipsis = true;
            this.label13.BackColor = System.Drawing.Color.Gainsboro;
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(179, 24);
            this.label13.TabIndex = 9;
            this.label13.Text = "■ GLASS CHK";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkCoolineMainAir
            // 
            this.chkCoolineMainAir.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkCoolineMainAir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCoolineMainAir.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkCoolineMainAir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkCoolineMainAir.Location = new System.Drawing.Point(2, 62);
            this.chkCoolineMainAir.Name = "chkCoolineMainAir";
            this.chkCoolineMainAir.Size = new System.Drawing.Size(170, 28);
            this.chkCoolineMainAir.TabIndex = 23;
            this.chkCoolineMainAir.Text = "Cooline Main Air";
            this.chkCoolineMainAir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkCoolineMainAir.UseVisualStyleBackColor = false;
            // 
            // chkIonizerMainAir
            // 
            this.chkIonizerMainAir.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIonizerMainAir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIonizerMainAir.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIonizerMainAir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIonizerMainAir.Location = new System.Drawing.Point(2, 91);
            this.chkIonizerMainAir.Name = "chkIonizerMainAir";
            this.chkIonizerMainAir.Size = new System.Drawing.Size(170, 28);
            this.chkIonizerMainAir.TabIndex = 23;
            this.chkIonizerMainAir.Text = "Ionizer Main Air";
            this.chkIonizerMainAir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIonizerMainAir.UseVisualStyleBackColor = false;
            // 
            // chkCameraCooling
            // 
            this.chkCameraCooling.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkCameraCooling.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCameraCooling.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkCameraCooling.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkCameraCooling.Location = new System.Drawing.Point(3, 120);
            this.chkCameraCooling.Name = "chkCameraCooling";
            this.chkCameraCooling.Size = new System.Drawing.Size(169, 28);
            this.chkCameraCooling.TabIndex = 10;
            this.chkCameraCooling.Text = "Camera Cooling";
            this.chkCameraCooling.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkCameraCooling.UseVisualStyleBackColor = false;
            // 
            // chkEnableGripSwOn
            // 
            this.chkEnableGripSwOn.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEnableGripSwOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEnableGripSwOn.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkEnableGripSwOn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkEnableGripSwOn.Location = new System.Drawing.Point(3, 269);
            this.chkEnableGripSwOn.Name = "chkEnableGripSwOn";
            this.chkEnableGripSwOn.Size = new System.Drawing.Size(169, 28);
            this.chkEnableGripSwOn.TabIndex = 10;
            this.chkEnableGripSwOn.Text = "ENABLE_GRIB_SW_ON";
            this.chkEnableGripSwOn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEnableGripSwOn.UseVisualStyleBackColor = false;
            // 
            // chkStageGlassSensor1
            // 
            this.chkStageGlassSensor1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkStageGlassSensor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkStageGlassSensor1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkStageGlassSensor1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkStageGlassSensor1.Location = new System.Drawing.Point(3, 236);
            this.chkStageGlassSensor1.Name = "chkStageGlassSensor1";
            this.chkStageGlassSensor1.Size = new System.Drawing.Size(169, 28);
            this.chkStageGlassSensor1.TabIndex = 10;
            this.chkStageGlassSensor1.Text = "Stage Glass Sensor 1";
            this.chkStageGlassSensor1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkStageGlassSensor1.UseVisualStyleBackColor = false;
            // 
            // chkSafetyModeKeySW
            // 
            this.chkSafetyModeKeySW.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkSafetyModeKeySW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkSafetyModeKeySW.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkSafetyModeKeySW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkSafetyModeKeySW.Location = new System.Drawing.Point(3, 210);
            this.chkSafetyModeKeySW.Name = "chkSafetyModeKeySW";
            this.chkSafetyModeKeySW.Size = new System.Drawing.Size(169, 28);
            this.chkSafetyModeKeySW.TabIndex = 23;
            this.chkSafetyModeKeySW.Text = "Safety Mode Key S/W";
            this.chkSafetyModeKeySW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkSafetyModeKeySW.UseVisualStyleBackColor = false;
            // 
            // chkIonizerCover
            // 
            this.chkIonizerCover.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIonizerCover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIonizerCover.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIonizerCover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIonizerCover.Location = new System.Drawing.Point(3, 149);
            this.chkIonizerCover.Name = "chkIonizerCover";
            this.chkIonizerCover.Size = new System.Drawing.Size(169, 28);
            this.chkIonizerCover.TabIndex = 83;
            this.chkIonizerCover.Text = "Ionizer Cover";
            this.chkIonizerCover.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIonizerCover.UseVisualStyleBackColor = false;
            // 
            // chkIonizer
            // 
            this.chkIonizer.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkIonizer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkIonizer.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkIonizer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkIonizer.Location = new System.Drawing.Point(3, 178);
            this.chkIonizer.Name = "chkIonizer";
            this.chkIonizer.Size = new System.Drawing.Size(169, 28);
            this.chkIonizer.TabIndex = 83;
            this.chkIonizer.Text = "Ionizer";
            this.chkIonizer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIonizer.UseVisualStyleBackColor = false;
            // 
            // chkUnldStageOutBtn2
            // 
            this.chkUnldStageOutBtn2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnldStageOutBtn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnldStageOutBtn2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnldStageOutBtn2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnldStageOutBtn2.Location = new System.Drawing.Point(4, 61);
            this.chkUnldStageOutBtn2.Name = "chkUnldStageOutBtn2";
            this.chkUnldStageOutBtn2.Size = new System.Drawing.Size(80, 28);
            this.chkUnldStageOutBtn2.TabIndex = 16;
            this.chkUnldStageOutBtn2.Text = "B";
            this.chkUnldStageOutBtn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnldStageOutBtn2.UseVisualStyleBackColor = false;
            // 
            // chkUnldStageOutBtn1
            // 
            this.chkUnldStageOutBtn1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnldStageOutBtn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnldStageOutBtn1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnldStageOutBtn1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnldStageOutBtn1.Location = new System.Drawing.Point(4, 27);
            this.chkUnldStageOutBtn1.Name = "chkUnldStageOutBtn1";
            this.chkUnldStageOutBtn1.Size = new System.Drawing.Size(80, 28);
            this.chkUnldStageOutBtn1.TabIndex = 16;
            this.chkUnldStageOutBtn1.Text = "A";
            this.chkUnldStageOutBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnldStageOutBtn1.UseVisualStyleBackColor = false;
            // 
            // chkUDAReset
            // 
            this.chkUDAReset.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUDAReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUDAReset.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUDAReset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUDAReset.Location = new System.Drawing.Point(6, 27);
            this.chkUDAReset.Name = "chkUDAReset";
            this.chkUDAReset.Size = new System.Drawing.Size(80, 28);
            this.chkUDAReset.TabIndex = 16;
            this.chkUDAReset.Text = "A";
            this.chkUDAReset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUDAReset.UseVisualStyleBackColor = false;
            // 
            // chkUDBReset
            // 
            this.chkUDBReset.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUDBReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUDBReset.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUDBReset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUDBReset.Location = new System.Drawing.Point(6, 61);
            this.chkUDBReset.Name = "chkUDBReset";
            this.chkUDBReset.Size = new System.Drawing.Size(80, 28);
            this.chkUDBReset.TabIndex = 16;
            this.chkUDBReset.Text = "B";
            this.chkUDBReset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUDBReset.UseVisualStyleBackColor = false;
            // 
            // chkLdStageBtn1
            // 
            this.chkLdStageBtn1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLdStageBtn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLdStageBtn1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLdStageBtn1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLdStageBtn1.Location = new System.Drawing.Point(4, 27);
            this.chkLdStageBtn1.Name = "chkLdStageBtn1";
            this.chkLdStageBtn1.Size = new System.Drawing.Size(80, 28);
            this.chkLdStageBtn1.TabIndex = 16;
            this.chkLdStageBtn1.Text = "A";
            this.chkLdStageBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLdStageBtn1.UseVisualStyleBackColor = false;
            // 
            // chkLdStageBtn2
            // 
            this.chkLdStageBtn2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLdStageBtn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLdStageBtn2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLdStageBtn2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLdStageBtn2.Location = new System.Drawing.Point(4, 62);
            this.chkLdStageBtn2.Name = "chkLdStageBtn2";
            this.chkLdStageBtn2.Size = new System.Drawing.Size(80, 28);
            this.chkLdStageBtn2.TabIndex = 16;
            this.chkLdStageBtn2.Text = "B";
            this.chkLdStageBtn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLdStageBtn2.UseVisualStyleBackColor = false;
            // 
            // chkUnldStageBtn2
            // 
            this.chkUnldStageBtn2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnldStageBtn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnldStageBtn2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnldStageBtn2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnldStageBtn2.Location = new System.Drawing.Point(5, 61);
            this.chkUnldStageBtn2.Name = "chkUnldStageBtn2";
            this.chkUnldStageBtn2.Size = new System.Drawing.Size(80, 28);
            this.chkUnldStageBtn2.TabIndex = 16;
            this.chkUnldStageBtn2.Text = "B";
            this.chkUnldStageBtn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnldStageBtn2.UseVisualStyleBackColor = false;
            // 
            // chkUnldStageBtn1
            // 
            this.chkUnldStageBtn1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnldStageBtn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnldStageBtn1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnldStageBtn1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnldStageBtn1.Location = new System.Drawing.Point(6, 27);
            this.chkUnldStageBtn1.Name = "chkUnldStageBtn1";
            this.chkUnldStageBtn1.Size = new System.Drawing.Size(80, 28);
            this.chkUnldStageBtn1.TabIndex = 16;
            this.chkUnldStageBtn1.Text = "A";
            this.chkUnldStageBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnldStageBtn1.UseVisualStyleBackColor = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-500, -500);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(496, 861);
            this.tabControl1.TabIndex = 107;
            // 
            // chkLoaderAOGamji
            // 
            this.chkLoaderAOGamji.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLoaderAOGamji.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLoaderAOGamji.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLoaderAOGamji.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLoaderAOGamji.Location = new System.Drawing.Point(5, 27);
            this.chkLoaderAOGamji.Name = "chkLoaderAOGamji";
            this.chkLoaderAOGamji.Size = new System.Drawing.Size(80, 28);
            this.chkLoaderAOGamji.TabIndex = 108;
            this.chkLoaderAOGamji.Text = "A";
            this.chkLoaderAOGamji.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLoaderAOGamji.UseVisualStyleBackColor = false;
            // 
            // lblAlive
            // 
            this.lblAlive.AutoEllipsis = true;
            this.lblAlive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAlive.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAlive.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlive.ForeColor = System.Drawing.Color.White;
            this.lblAlive.Location = new System.Drawing.Point(-5000, -5000);
            this.lblAlive.Name = "lblAlive";
            this.lblAlive.Size = new System.Drawing.Size(140, 33);
            this.lblAlive.TabIndex = 53;
            this.lblAlive.Text = "equip alive";
            this.lblAlive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.label19);
            this.panel16.Controls.Add(this.panel19);
            this.panel16.Location = new System.Drawing.Point(1623, 45);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(102, 229);
            this.panel16.TabIndex = 109;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.chkLoaderAExist);
            this.panel20.Controls.Add(this.chkLoaderBExist);
            this.panel20.Controls.Add(this.label29);
            this.panel20.Location = new System.Drawing.Point(3, 128);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(92, 95);
            this.panel20.TabIndex = 113;
            // 
            // chkLoaderAExist
            // 
            this.chkLoaderAExist.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLoaderAExist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLoaderAExist.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLoaderAExist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLoaderAExist.Location = new System.Drawing.Point(5, 27);
            this.chkLoaderAExist.Name = "chkLoaderAExist";
            this.chkLoaderAExist.Size = new System.Drawing.Size(80, 28);
            this.chkLoaderAExist.TabIndex = 110;
            this.chkLoaderAExist.Text = "A";
            this.chkLoaderAExist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLoaderAExist.UseVisualStyleBackColor = false;
            // 
            // chkLoaderBExist
            // 
            this.chkLoaderBExist.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLoaderBExist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLoaderBExist.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLoaderBExist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLoaderBExist.Location = new System.Drawing.Point(5, 61);
            this.chkLoaderBExist.Name = "chkLoaderBExist";
            this.chkLoaderBExist.Size = new System.Drawing.Size(80, 28);
            this.chkLoaderBExist.TabIndex = 112;
            this.chkLoaderBExist.Text = "B";
            this.chkLoaderBExist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLoaderBExist.UseVisualStyleBackColor = false;
            // 
            // label29
            // 
            this.label29.AutoEllipsis = true;
            this.label29.BackColor = System.Drawing.Color.Gainsboro;
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(0, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(90, 24);
            this.label29.TabIndex = 9;
            this.label29.Text = "■ 카세트 유무";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoEllipsis = true;
            this.label19.BackColor = System.Drawing.Color.Gainsboro;
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 24);
            this.label19.TabIndex = 9;
            this.label19.Text = "■ Loader";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.label28);
            this.panel19.Controls.Add(this.chkLoaderBOGamji);
            this.panel19.Controls.Add(this.chkLoaderAOGamji);
            this.panel19.Location = new System.Drawing.Point(3, 27);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(92, 95);
            this.panel19.TabIndex = 112;
            // 
            // label28
            // 
            this.label28.AutoEllipsis = true;
            this.label28.BackColor = System.Drawing.Color.Gainsboro;
            this.label28.Dock = System.Windows.Forms.DockStyle.Top;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 24);
            this.label28.TabIndex = 9;
            this.label28.Text = "■ 오 투입 감지";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkLoaderBOGamji
            // 
            this.chkLoaderBOGamji.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLoaderBOGamji.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkLoaderBOGamji.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkLoaderBOGamji.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkLoaderBOGamji.Location = new System.Drawing.Point(5, 62);
            this.chkLoaderBOGamji.Name = "chkLoaderBOGamji";
            this.chkLoaderBOGamji.Size = new System.Drawing.Size(80, 28);
            this.chkLoaderBOGamji.TabIndex = 112;
            this.chkLoaderBOGamji.Text = "B";
            this.chkLoaderBOGamji.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLoaderBOGamji.UseVisualStyleBackColor = false;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.panel22);
            this.panel17.Controls.Add(this.panel21);
            this.panel17.Controls.Add(this.label21);
            this.panel17.Location = new System.Drawing.Point(1731, 45);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(101, 229);
            this.panel17.TabIndex = 111;
            // 
            // panel22
            // 
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.chkUnloaderAExist);
            this.panel22.Controls.Add(this.chkUnloaderBExist);
            this.panel22.Controls.Add(this.label31);
            this.panel22.Location = new System.Drawing.Point(3, 128);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(92, 95);
            this.panel22.TabIndex = 114;
            // 
            // chkUnloaderAExist
            // 
            this.chkUnloaderAExist.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnloaderAExist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnloaderAExist.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnloaderAExist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnloaderAExist.Location = new System.Drawing.Point(5, 27);
            this.chkUnloaderAExist.Name = "chkUnloaderAExist";
            this.chkUnloaderAExist.Size = new System.Drawing.Size(80, 28);
            this.chkUnloaderAExist.TabIndex = 110;
            this.chkUnloaderAExist.Text = "A";
            this.chkUnloaderAExist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnloaderAExist.UseVisualStyleBackColor = false;
            // 
            // chkUnloaderBExist
            // 
            this.chkUnloaderBExist.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnloaderBExist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnloaderBExist.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnloaderBExist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnloaderBExist.Location = new System.Drawing.Point(5, 61);
            this.chkUnloaderBExist.Name = "chkUnloaderBExist";
            this.chkUnloaderBExist.Size = new System.Drawing.Size(80, 28);
            this.chkUnloaderBExist.TabIndex = 113;
            this.chkUnloaderBExist.Text = "B";
            this.chkUnloaderBExist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnloaderBExist.UseVisualStyleBackColor = false;
            // 
            // label31
            // 
            this.label31.AutoEllipsis = true;
            this.label31.BackColor = System.Drawing.Color.Gainsboro;
            this.label31.Dock = System.Windows.Forms.DockStyle.Top;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(0, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(90, 24);
            this.label31.TabIndex = 9;
            this.label31.Text = "■ 카세트 유무";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.label30);
            this.panel21.Controls.Add(this.chkUnloaderAOGamji);
            this.panel21.Controls.Add(this.chkUnloaderBOGamji);
            this.panel21.Location = new System.Drawing.Point(4, 27);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(92, 95);
            this.panel21.TabIndex = 113;
            // 
            // label30
            // 
            this.label30.AutoEllipsis = true;
            this.label30.BackColor = System.Drawing.Color.Gainsboro;
            this.label30.Dock = System.Windows.Forms.DockStyle.Top;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 24);
            this.label30.TabIndex = 9;
            this.label30.Text = "■ 오 투입 감지";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkUnloaderAOGamji
            // 
            this.chkUnloaderAOGamji.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnloaderAOGamji.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnloaderAOGamji.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnloaderAOGamji.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnloaderAOGamji.Location = new System.Drawing.Point(5, 27);
            this.chkUnloaderAOGamji.Name = "chkUnloaderAOGamji";
            this.chkUnloaderAOGamji.Size = new System.Drawing.Size(80, 28);
            this.chkUnloaderAOGamji.TabIndex = 108;
            this.chkUnloaderAOGamji.Text = "A";
            this.chkUnloaderAOGamji.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnloaderAOGamji.UseVisualStyleBackColor = false;
            // 
            // chkUnloaderBOGamji
            // 
            this.chkUnloaderBOGamji.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUnloaderBOGamji.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUnloaderBOGamji.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkUnloaderBOGamji.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkUnloaderBOGamji.Location = new System.Drawing.Point(5, 61);
            this.chkUnloaderBOGamji.Name = "chkUnloaderBOGamji";
            this.chkUnloaderBOGamji.Size = new System.Drawing.Size(80, 28);
            this.chkUnloaderBOGamji.TabIndex = 114;
            this.chkUnloaderBOGamji.Text = "B";
            this.chkUnloaderBOGamji.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkUnloaderBOGamji.UseVisualStyleBackColor = false;
            // 
            // label21
            // 
            this.label21.AutoEllipsis = true;
            this.label21.BackColor = System.Drawing.Color.Gainsboro;
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 24);
            this.label21.TabIndex = 9;
            this.label21.Text = "■ Unloader";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel23
            // 
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Controls.Add(this.label36);
            this.panel23.Controls.Add(this.panel28);
            this.panel23.Location = new System.Drawing.Point(1731, 280);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(101, 231);
            this.panel23.TabIndex = 117;
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.label32);
            this.panel24.Controls.Add(this.chkUDAReset);
            this.panel24.Controls.Add(this.chkUDBReset);
            this.panel24.Location = new System.Drawing.Point(3, 128);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(92, 95);
            this.panel24.TabIndex = 113;
            // 
            // label32
            // 
            this.label32.AutoEllipsis = true;
            this.label32.BackColor = System.Drawing.Color.Gainsboro;
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(90, 24);
            this.label32.TabIndex = 9;
            this.label32.Text = "■ Unloader";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.AutoEllipsis = true;
            this.label36.BackColor = System.Drawing.Color.Gainsboro;
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(99, 24);
            this.label36.TabIndex = 9;
            this.label36.Text = "■ Reset Switch";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel28
            // 
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.label41);
            this.panel28.Controls.Add(this.chkUnldStageBtn1);
            this.panel28.Controls.Add(this.chkUnldStageBtn2);
            this.panel28.Location = new System.Drawing.Point(3, 27);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(92, 95);
            this.panel28.TabIndex = 112;
            // 
            // label41
            // 
            this.label41.AutoEllipsis = true;
            this.label41.BackColor = System.Drawing.Color.Gainsboro;
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(0, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(90, 24);
            this.label41.TabIndex = 9;
            this.label41.Text = "■ Loader";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel25
            // 
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Controls.Add(this.label39);
            this.panel25.Controls.Add(this.panel27);
            this.panel25.Location = new System.Drawing.Point(1624, 280);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(101, 231);
            this.panel25.TabIndex = 116;
            // 
            // panel26
            // 
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel26.Controls.Add(this.label34);
            this.panel26.Controls.Add(this.chkUnldStageOutBtn1);
            this.panel26.Controls.Add(this.chkUnldStageOutBtn2);
            this.panel26.Location = new System.Drawing.Point(3, 128);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(92, 95);
            this.panel26.TabIndex = 113;
            // 
            // label34
            // 
            this.label34.AutoEllipsis = true;
            this.label34.BackColor = System.Drawing.Color.Gainsboro;
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(90, 24);
            this.label34.TabIndex = 9;
            this.label34.Text = "■ Unloader";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label39
            // 
            this.label39.AutoEllipsis = true;
            this.label39.BackColor = System.Drawing.Color.Gainsboro;
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(0, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(99, 24);
            this.label39.TabIndex = 9;
            this.label39.Text = "■ Muting Switch";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel27
            // 
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.label40);
            this.panel27.Controls.Add(this.chkLdStageBtn1);
            this.panel27.Controls.Add(this.chkLdStageBtn2);
            this.panel27.Location = new System.Drawing.Point(3, 27);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(92, 95);
            this.panel27.TabIndex = 112;
            // 
            // label40
            // 
            this.label40.AutoEllipsis = true;
            this.label40.BackColor = System.Drawing.Color.Gainsboro;
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(0, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(90, 24);
            this.label40.TabIndex = 9;
            this.label40.Text = "■ Loader";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstViewMotorInfo
            // 
            this.lstViewMotorInfo.Location = new System.Drawing.Point(1065, 45);
            this.lstViewMotorInfo.Name = "lstViewMotorInfo";
            this.lstViewMotorInfo.Size = new System.Drawing.Size(552, 884);
            this.lstViewMotorInfo.TabIndex = 106;
            this.lstViewMotorInfo.UseCompatibleStateImageBehavior = false;
            // 
            // shInspCam
            // 
            this.shInspCam.BackColor = System.Drawing.Color.Transparent;
            this.shInspCam.BackgroundImage = global::EquipSimulator.Properties.Resources.inspx1;
            this.shInspCam.Location = new System.Drawing.Point(267, 117);
            this.shInspCam.Name = "shInspCam";
            this.shInspCam.Size = new System.Drawing.Size(91, 403);
            this.shInspCam.TabIndex = 13;
            this.shInspCam.TabStop = false;
            this.shInspCam.Text = "transparentControl1";
            // 
            // shReviewCam
            // 
            this.shReviewCam.BackColor = System.Drawing.Color.Transparent;
            this.shReviewCam.BackgroundImage = global::EquipSimulator.Properties.Resources.revCam1;
            this.shReviewCam.Location = new System.Drawing.Point(359, 392);
            this.shReviewCam.Name = "shReviewCam";
            this.shReviewCam.Size = new System.Drawing.Size(169, 102);
            this.shReviewCam.TabIndex = 10;
            this.shReviewCam.TabStop = false;
            this.shReviewCam.Text = "transparentControl1";
            // 
            // shStage
            // 
            this.shStage.BackColor = System.Drawing.Color.Transparent;
            this.shStage.BackgroundImage = global::EquipSimulator.Properties.Resources.StateGlass;
            this.shStage.Location = new System.Drawing.Point(177, 255);
            this.shStage.Name = "shStage";
            this.shStage.Size = new System.Drawing.Size(194, 148);
            this.shStage.TabIndex = 11;
            this.shStage.TabStop = false;
            this.shStage.Text = "transparentControl1";
            // 
            // SpThelta
            // 
            this.SpThelta.BackColor = System.Drawing.Color.DarkRed;
            this.SpThelta.Location = new System.Drawing.Point(190, 633);
            this.SpThelta.Name = "SpThelta";
            this.SpThelta.Size = new System.Drawing.Size(10, 10);
            this.SpThelta.TabIndex = 12;
            this.SpThelta.TabStop = false;
            this.SpThelta.Text = "transparentControl1";
            // 
            // spLiftPin
            // 
            this.spLiftPin.BackColor = System.Drawing.Color.Red;
            this.spLiftPin.ForeColor = System.Drawing.Color.Red;
            this.spLiftPin.Location = new System.Drawing.Point(19, 150);
            this.spLiftPin.Name = "spLiftPin";
            this.spLiftPin.Size = new System.Drawing.Size(10, 10);
            this.spLiftPin.TabIndex = 12;
            this.spLiftPin.TabStop = false;
            this.spLiftPin.Text = "transparentControl1";
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1916, 962);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lstViewMotorInfo);
            this.Controls.Add(this.lstLogger);
            this.Controls.Add(this.pnlView);
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.lblAlive);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lblScanTime);
            this.Controls.Add(this.label11);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "FrmMain";
            this.Text = "EQUIP SIMULATOR";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.pnlView.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Timer tmrWorker;
        private System.ComponentModel.BackgroundWorker bgWorker;
        private System.Windows.Forms.Panel pnlView;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label lblScanTime;
        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.Timer tmrState;
        private Ctrl.TransparentControl shStage;
        private Ctrl.TransparentControl spLiftPin;
        private Ctrl.TransparentControl shInspCam;
        private Ctrl.TransparentControl SpThelta;
        private Ctrl.TransparentControl shReviewCam;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListView lstLogger;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private ListViewEx lstViewMotorInfo;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.CheckBox chkVaccum6;
        private System.Windows.Forms.CheckBox chkVaccum5;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkVaccum2;
        private System.Windows.Forms.CheckBox chkVaccum1;
        private System.Windows.Forms.CheckBox chkVaccum3;
        private System.Windows.Forms.CheckBox chkVaccum4;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.CheckBox chkBlower6;
        private System.Windows.Forms.CheckBox chkBlower5;
        private System.Windows.Forms.CheckBox chkBlower4;
        private System.Windows.Forms.CheckBox chkBlower3;
        private System.Windows.Forms.CheckBox chkBlower2;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkBlower1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.CheckBox chkGlassCheckSensor6;
        private System.Windows.Forms.CheckBox chkGlassCheckSensor5;
        private System.Windows.Forms.CheckBox chkGlassCheckSensor4;
        private System.Windows.Forms.CheckBox chkGlassCheckSensor3;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkGlassCheckSensor1;
        private System.Windows.Forms.CheckBox chkGlassCheckSensor2;
        private System.Windows.Forms.TextBox txtReadValue;
        private System.Windows.Forms.TextBox txtReadAddr;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chkDoor02;
        private System.Windows.Forms.CheckBox chkDoorAll;
        private System.Windows.Forms.CheckBox chkDoor05;
        private System.Windows.Forms.CheckBox chkDoor04;
        private System.Windows.Forms.CheckBox chkDoor01;
        private System.Windows.Forms.CheckBox chkDoor03;
        private System.Windows.Forms.CheckBox chkDoor06;
        private System.Windows.Forms.CheckBox chkDoor07;
        private System.Windows.Forms.CheckBox chkDoor08;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel8;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkIonizer1On;
        private System.Windows.Forms.CheckBox chkIonizer2On;
        private System.Windows.Forms.CheckBox chkIonizer4On;
        private System.Windows.Forms.CheckBox chkIonizer3On;
        private System.Windows.Forms.CheckBox chkReadHsms;
        private System.Windows.Forms.Panel panel7;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkIsol01;
        private System.Windows.Forms.CheckBox chkIsol02;
        private System.Windows.Forms.CheckBox chkIsol04;
        private System.Windows.Forms.CheckBox chkIsol03;
        private System.Windows.Forms.Panel panel5;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkGlassEdgeCheckSensor1;
        private System.Windows.Forms.CheckBox chkGlassEdgeCheckSensor3;
        private System.Windows.Forms.CheckBox chkGlassEdgeCheckSensor2;
        private System.Windows.Forms.CheckBox chkGlassEdgeCheckSensor4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox chkEmo07;
        private System.Windows.Forms.CheckBox chkEmo06;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkEmo02;
        private System.Windows.Forms.CheckBox chkEmo01;
        private System.Windows.Forms.CheckBox chkEmo03;
        private System.Windows.Forms.CheckBox chkEmo05;
        private System.Windows.Forms.CheckBox chkEmo04;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel18;
        internal System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chkUDAMute;
        private System.Windows.Forms.CheckBox chkUnldStageOutBtn2;
        private System.Windows.Forms.CheckBox chkUDBMute;
        private System.Windows.Forms.CheckBox chkUnldStageOutBtn1;
        private System.Windows.Forms.CheckBox chkUDAReset;
        private System.Windows.Forms.CheckBox chkUDBReset;
        private System.Windows.Forms.Panel panel15;
        internal System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkLdStageOutBtn1;
        private System.Windows.Forms.CheckBox chkLdStageOutBtn2;
        private System.Windows.Forms.CheckBox chkLdStageBtn1;
        private System.Windows.Forms.CheckBox chkLdStageBtn2;
        private System.Windows.Forms.CheckBox chkUnldStageBtn2;
        private System.Windows.Forms.CheckBox chkUnldStageBtn1;
        private System.Windows.Forms.Panel panel14;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label lblLoRecvComplete;
        private System.Windows.Forms.CheckBox chkRobotArmCheck;
        internal System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btnRecvAbleStart;
        private System.Windows.Forms.Button btnSendAbleStart;
        internal System.Windows.Forms.Label label35;
        internal System.Windows.Forms.Label label37;
        internal System.Windows.Forms.Label lblLoRecvStart;
        internal System.Windows.Forms.Label lblLoRecvAble;
        internal System.Windows.Forms.Label lblAoiSendComplete;
        internal System.Windows.Forms.Label lblAoiSendStart;
        internal System.Windows.Forms.Label lblAoiSendAble;
        internal System.Windows.Forms.Label lblAoiRecvComplete;
        internal System.Windows.Forms.Label lblAoiRecvStart;
        internal System.Windows.Forms.Label lblAoiRecvAble;
        internal System.Windows.Forms.Label lblUpSendComplete;
        internal System.Windows.Forms.Label lblUpSendStart;
        internal System.Windows.Forms.Label lblUpSendAble;
        internal System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel13;
        internal System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnCtrlPcXy;
        private System.Windows.Forms.Button btnSimulPcXy;
        private System.Windows.Forms.Button btnTester;
        private System.Windows.Forms.Button btnXyIoAddr;
        private System.Windows.Forms.Button btnMemDetail;
        private System.Windows.Forms.Button btnXyIo;
        private System.Windows.Forms.CheckBox chkUseInterlock;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblScanStep;
        internal System.Windows.Forms.Label lblInspGUnloadingOk;
        internal System.Windows.Forms.Label lblInspGUnloading;
        internal System.Windows.Forms.Label lblInspScanEnd;
        internal System.Windows.Forms.Label lblInspScanEndOk;
        internal System.Windows.Forms.Label lblInspScanStartOk;
        internal System.Windows.Forms.Label lblInspGLoadingOk;
        internal System.Windows.Forms.Label lblInspScanStart;
        internal System.Windows.Forms.Label lblInspScanReadyOk;
        internal System.Windows.Forms.Label lblInspScanReady;
        internal System.Windows.Forms.Label lblInspGLoading;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkInspXError;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkReviLotEndAck;
        private System.Windows.Forms.CheckBox chkReviLotStartAck;
        private System.Windows.Forms.CheckBox chkReviReviEnd;
        private System.Windows.Forms.CheckBox chkReviReviStartAck;
        private System.Windows.Forms.CheckBox chkReviAlignStartAck;
        private System.Windows.Forms.CheckBox chkReviUnloadingAck;
        private System.Windows.Forms.CheckBox chkReviLoadingAck;
        private System.Windows.Forms.CheckBox chkReviLotEnd;
        private System.Windows.Forms.CheckBox chkReviLotStart;
        private System.Windows.Forms.CheckBox chkReviReviEndAck;
        private System.Windows.Forms.CheckBox chkReviReviStart;
        private System.Windows.Forms.CheckBox chkReviUnloading;
        private System.Windows.Forms.CheckBox chkReviAlignStart;
        private System.Windows.Forms.CheckBox chkReviLoading;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel12;
        internal System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkCoolineMainAir;
        private System.Windows.Forms.CheckBox chkIonizerMainAir;
        private System.Windows.Forms.CheckBox chkCameraCooling;
        private System.Windows.Forms.CheckBox chkEnableGripSwOn;
        private System.Windows.Forms.CheckBox chkStageGlassSensor1;
        private System.Windows.Forms.CheckBox chkSafetyModeKeySW;
        private System.Windows.Forms.CheckBox chkIonizerCover;
        private System.Windows.Forms.CheckBox chkIonizer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.CheckBox chkLoaderAOGamji;
        internal System.Windows.Forms.Label lblAlive;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.CheckBox chkLoaderAExist;
        internal System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.CheckBox chkUnloaderAExist;
        internal System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkUnloaderAOGamji;
        private System.Windows.Forms.Panel panel19;
        internal System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.CheckBox chkLoaderBExist;
        private System.Windows.Forms.CheckBox chkLoaderBOGamji;
        internal System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.CheckBox chkUnloaderBExist;
        internal System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox chkUnloaderBOGamji;
        private System.Windows.Forms.Panel panel21;
        internal System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel28;
        internal System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        internal System.Windows.Forms.Label label34;
        internal System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel27;
        internal System.Windows.Forms.Label label40;
    }
}


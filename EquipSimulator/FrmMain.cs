﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EquipSimulator.Acturator;
using System.Threading;
using System.Diagnostics;
using EquipSimulator;
using System.Drawing.Imaging;
using EquipSimulator.Log;
using System.IO;
using Dit.Framework.Log;
using EquipSimulator.Addr;
using Dit.Framework.Comm;
using Dit.Framework.PLC;
using EquipView;

namespace EquipSimulator
{
    public partial class FrmMain : Form
    {
        private VirtualMem PLC;
        private bool _isRunning = false;
        private EquipSimul _equip = new EquipSimul();
        private FrmPcMonitor _frmPcMonitor = new FrmPcMonitor();
        private FrmCtrl _frmCtrl = new FrmCtrl();
        public static Panel ViewPanel = null;
        private EquipView.UcrlEquipView ucrlEquipView = new EquipView.UcrlEquipView();

        public FrmMain()
        {
            CheckForIllegalCrossThreadCalls = true;
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            InitializeComponent();
            //// C#

            //GG.PmacSetting.LiftPin.Save(@"D:\Setting.ini");

            elementHost1.Child = ucrlEquipView;


            Logger.Log = new Dit.Framework.Log.SimpleFileLoggerMark5(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20, lstLogger);

            GG.CCLINK = (IVirtualMem)new VirtualMem("DIT.MOTOR.MEM");
            GG.MEM_INSP = (IVirtualMem)new VirtualMem("PC_CTRL_MEM") { PLC_MEM_SIZE = 2000 };
            GG.PMAC = (IVirtualMem)new VirtualMem("DIT.MOTOR.MEM");
            //GG.HSMS = (IVirtualMem)new VirtualShare("DIT.PLC.HSMS_MEM.S", 102400);
            GG.HSMS = (IVirtualMem)new VirtualMem("HSMS_MEM.S");

            GG.CCLINK.Open();
            GG.MEM_INSP.Open();
            GG.PMAC.Open();
            GG.HSMS.Open();

            AddressMgr.Load(GG.ADDRESS_EZI_MOTOR, GG.PMAC);
            AddressMgr.Load(GG.ADDRESS_AJIN_IO, GG.PMAC);
            AddressMgr.Load(GG.ADDRESS_AJIN_MOTOR, GG.PMAC);
            AddressMgr.Load(GG.ADDRESS_UMAC_IO, GG.PMAC);
            AddressMgr.Load(GG.ADDRESS_UMAC_MOTOR, GG.PMAC);



            //_frmCtrl.axActEasyIF1.ActLogicalStationNumber = 150;
            //GG.PLC = (IVirtualMem)new VirtualPlc(_frmCtrl.axActEasyIF1, "PLC");



            //btnSimulPcXy_Click(null, null);
            CheckForIllegalCrossThreadCalls = false;
            ViewPanel = pnlView;

            _equip.LoaderAOGamji                        /**/ = new SenseSimul() { Name = "Cst_LoaderA_OGamji_SENSOR",       /**/ Acturator = chkLoaderAOGamji };
            _equip.LoaderAExist                       /**/ = new SenseSimul() { Name = "Cst_LoaderA_Exist_SENSOR",       /**/ Acturator = chkLoaderAExist };

            _equip.UnloaderAOGamji                        /**/ = new SenseSimul() { Name = "Cst_UnloaderA_OGamji_SENSOR",       /**/ Acturator = chkUnloaderAOGamji };
            _equip.UnloaderAExist                       /**/ = new SenseSimul() { Name = "Cst_UnloaderA_Exist_SENSOR",       /**/ Acturator = chkUnloaderAExist };

            _equip.LoaderBOGamji                        /**/ = new SenseSimul() { Name = "Cst_LoaderB_OGamji_SENSOR",       /**/ Acturator = chkLoaderBOGamji };
            _equip.LoaderBExist                       /**/ = new SenseSimul() { Name = "Cst_LoaderB_Exist_SENSOR",       /**/ Acturator = chkLoaderBExist };

            _equip.UnloaderBOGamji                        /**/ = new SenseSimul() { Name = "Cst_UnloaderB_OGamji_SENSOR",       /**/ Acturator = chkUnloaderBOGamji };
            _equip.UnloaderBExist                       /**/ = new SenseSimul() { Name = "Cst_UnloaderB_Exist_SENSOR",       /**/ Acturator = chkUnloaderBExist };

            _equip.LdAButton = new SenseSimul() { Name = "LD_A_MUTING_SWITCH_ON/OFF", /**/ Acturator = chkLdStageBtn1 };
            _equip.LdBButton = new SenseSimul() { Name = "LD_B_MUTING_SWITCH_ON/OFF", /**/ Acturator = chkLdStageBtn2 };
            _equip.LdAReset = new SenseSimul() { Name = "LD_A_RESET_SWITCH ", /**/ Acturator = chkUnldStageBtn1 };
            _equip.LdBReset = new SenseSimul() { Name = "LD_B_RESET_SWITCH ", /**/ Acturator = chkUnldStageBtn2 };
            _equip.UDAButton = new SenseSimul() { Name = "UD_A_MUTING_SWITCH_ON/OFF",  /**/ Acturator = chkUnldStageOutBtn1 };
            _equip.UDBButton = new SenseSimul() { Name = "UD_B_MUTING_SWITCH_ON/OFF",  /**/ Acturator = chkUnldStageOutBtn2 };
            _equip.UDAReset = new SenseSimul() { Name = "UD_A_RESET_SWITCH ",  /**/ Acturator = chkUDAReset };
            _equip.UDBReset = new SenseSimul() { Name = "UD_B_RESET_SWITCH ",  /**/ Acturator = chkUDBReset };

            _equip.Vaccum01 = new SwitchSimul() { Name = "VACCUM 01",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum02 = new SwitchSimul() { Name = "VACCUM 02",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum03 = new SwitchSimul() { Name = "VACCUM 03",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum04 = new SwitchSimul() { Name = "VACCUM 04",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum05 = new SwitchSimul() { Name = "VACCUM 05",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum06 = new SwitchSimul() { Name = "VACCUM 06",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum07 = new SwitchSimul() { Name = "VACCUM 07",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum08 = new SwitchSimul() { Name = "VACCUM 08",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum09 = new SwitchSimul() { Name = "VACCUM 09",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum10 = new SwitchSimul() { Name = "VACCUM 10",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum11 = new SwitchSimul() { Name = "VACCUM 11",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum12 = new SwitchSimul() { Name = "VACCUM 12",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum13 = new SwitchSimul() { Name = "VACCUM 13",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum14 = new SwitchSimul() { Name = "VACCUM 14",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum15 = new SwitchSimul() { Name = "VACCUM 15",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum16 = new SwitchSimul() { Name = "VACCUM 16",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum17 = new SwitchSimul() { Name = "VACCUM 17",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum18 = new SwitchSimul() { Name = "VACCUM 18",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum19 = new SwitchSimul() { Name = "VACCUM 19",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum20 = new SwitchSimul() { Name = "VACCUM 20",      /**/ Acturator = chkVaccum2 };
            _equip.Vaccum21 = new SwitchSimul() { Name = "VACCUM 21",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum22 = new SwitchSimul() { Name = "VACCUM 22",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum23 = new SwitchSimul() { Name = "VACCUM 23",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum24 = new SwitchSimul() { Name = "VACCUM 24",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum25 = new SwitchSimul() { Name = "VACCUM 25",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum26 = new SwitchSimul() { Name = "VACCUM 26",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum27 = new SwitchSimul() { Name = "VACCUM 27",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum28 = new SwitchSimul() { Name = "VACCUM 28",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum29 = new SwitchSimul() { Name = "VACCUM 29",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum30 = new SwitchSimul() { Name = "VACCUM 30",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum31 = new SwitchSimul() { Name = "VACCUM 31",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum32 = new SwitchSimul() { Name = "VACCUM 32",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum33 = new SwitchSimul() { Name = "VACCUM 33",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum34 = new SwitchSimul() { Name = "VACCUM 34",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum35 = new SwitchSimul() { Name = "VACCUM 35",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum36 = new SwitchSimul() { Name = "VACCUM 36",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum37 = new SwitchSimul() { Name = "VACCUM 37",      /**/ Acturator = chkVaccum1 };
            _equip.Vaccum38 = new SwitchSimul() { Name = "VACCUM 38",      /**/ Acturator = chkVaccum1 };
            _equip.Blower01 = new SwitchSimulNonX() { Name = "Blower01",   /**/ Acturator = chkBlower1 };
            _equip.Blower02 = new SwitchSimulNonX() { Name = "Blower02",   /**/ Acturator = chkBlower1 };
            _equip.Blower03 = new SwitchSimulNonX() { Name = "Blower03",   /**/ Acturator = chkBlower1 };
            _equip.Blower04 = new SwitchSimulNonX() { Name = "Blower04",   /**/ Acturator = chkBlower1 };
            _equip.Blower05 = new SwitchSimulNonX() { Name = "Blower05",   /**/ Acturator = chkBlower1 };
            _equip.Blower06 = new SwitchSimulNonX() { Name = "Blower06",   /**/ Acturator = chkBlower1 };
            _equip.Blower07 = new SwitchSimulNonX() { Name = "Blower07",   /**/ Acturator = chkBlower1 };
            _equip.Blower08 = new SwitchSimulNonX() { Name = "Blower08",   /**/ Acturator = chkBlower1 };
            _equip.Blower09 = new SwitchSimulNonX() { Name = "Blower09",   /**/ Acturator = chkBlower1 };
            _equip.Blower10 = new SwitchSimulNonX() { Name = "Blower10",   /**/ Acturator = chkBlower1 };
            _equip.Blower11 = new SwitchSimulNonX() { Name = "Blower11",   /**/ Acturator = chkBlower1 };
            _equip.Blower12 = new SwitchSimulNonX() { Name = "Blower12",   /**/ Acturator = chkBlower1 };
            _equip.Blower13 = new SwitchSimulNonX() { Name = "Blower13",   /**/ Acturator = chkBlower1 };
            _equip.Blower14 = new SwitchSimulNonX() { Name = "Blower14",   /**/ Acturator = chkBlower1 };
            _equip.Blower15 = new SwitchSimulNonX() { Name = "Blower15",   /**/ Acturator = chkBlower1 };
            _equip.Blower16 = new SwitchSimulNonX() { Name = "Blower16",   /**/ Acturator = chkBlower1 };
            _equip.Blower17 = new SwitchSimulNonX() { Name = "Blower17",   /**/ Acturator = chkBlower1 };
            _equip.Blower18 = new SwitchSimulNonX() { Name = "Blower18",   /**/ Acturator = chkBlower1 };
            _equip.Blower19 = new SwitchSimulNonX() { Name = "Blower19",   /**/ Acturator = chkBlower1 };
            _equip.Blower20 = new SwitchSimulNonX() { Name = "Blower20",   /**/ Acturator = chkBlower1 };
            _equip.Blower21 = new SwitchSimulNonX() { Name = "Blower21",   /**/ Acturator = chkBlower1 };
            _equip.Blower22 = new SwitchSimulNonX() { Name = "Blower22",   /**/ Acturator = chkBlower1 };
            _equip.Blower23 = new SwitchSimulNonX() { Name = "Blower23",   /**/ Acturator = chkBlower1 };
            _equip.Blower24 = new SwitchSimulNonX() { Name = "Blower24",   /**/ Acturator = chkBlower1 };
            _equip.Blower25 = new SwitchSimulNonX() { Name = "Blower25",   /**/ Acturator = chkBlower1 };
            _equip.Blower26 = new SwitchSimulNonX() { Name = "Blower26",   /**/ Acturator = chkBlower1 };
            _equip.Blower27 = new SwitchSimulNonX() { Name = "Blower27",   /**/ Acturator = chkBlower1 };
            _equip.Blower28 = new SwitchSimulNonX() { Name = "Blower28",   /**/ Acturator = chkBlower1 };
            _equip.Blower29 = new SwitchSimulNonX() { Name = "Blower29",   /**/ Acturator = chkBlower1 };
            _equip.Blower30 = new SwitchSimulNonX() { Name = "Blower30",   /**/ Acturator = chkBlower1 };
            _equip.Blower31 = new SwitchSimulNonX() { Name = "Blower31",   /**/ Acturator = chkBlower1 };
            _equip.Blower32 = new SwitchSimulNonX() { Name = "Blower32",   /**/ Acturator = chkBlower1 };
            _equip.Blower33 = new SwitchSimulNonX() { Name = "Blower33",   /**/ Acturator = chkBlower1 };
            _equip.Blower34 = new SwitchSimulNonX() { Name = "Blower34",   /**/ Acturator = chkBlower1 };
            _equip.Blower35 = new SwitchSimulNonX() { Name = "Blower35",   /**/ Acturator = chkBlower1 };
            _equip.Blower36 = new SwitchSimulNonX() { Name = "Blower36",   /**/ Acturator = chkBlower1 };
            _equip.Blower37 = new SwitchSimulNonX() { Name = "Blower37",   /**/ Acturator = chkBlower1 };
            _equip.Blower38 = new SwitchSimulNonX() { Name = "Blower38",   /**/ Acturator = chkBlower1 };

            _equip.CameraCooling                         /**/ = new SwitchSimul() { Name = "Camera Cooling",    /**/ Acturator = chkCameraCooling };
            _equip.IonizerCover                          /**/ = new SwitchSimul() { Name = "Ionizer Cover",     /**/ Acturator = chkIonizerCover };
            _equip.Ionizer                               /**/ = new SwitchSimul() { Name = "Ionizer",           /**/ Acturator = chkIonizer };

            _equip.EMERGENCY_STOP_1                      /**/ = new SwitchSimul() { Name = "EMERGENCY_STOP_1",    /**/ Acturator = chkEmo01 };
            _equip.EMERGENCY_STOP_2                      /**/ = new SwitchSimul() { Name = "EMERGENCY_STOP_2",    /**/ Acturator = chkEmo02 };
            _equip.MODE_SELECT_KEY_SW_AUTOTEACH         /**/ = new SwitchSimul() { Name = "MODE_SELECT_KEY_SW_AUTOTEACH",    /**/ Acturator = chkSafetyModeKeySW };
            _equip.ENABLE_GRIB_SW_ON                    /**/ = new SwitchSimul() { Name = "ENABLE_GRIB_SW_ON",    /**/ Acturator = chkEnableGripSwOn };

            _equip.DOOR01_SENSOR                        /**/ = new SenseSimul() { Name = "TOP_DOOR1_SENSOR",       /**/ Acturator = chkDoor01 };
            _equip.DOOR02_SENSOR                        /**/ = new SenseSimul() { Name = "TOP_DOOR2_SENSOR",       /**/ Acturator = chkDoor02 };
            _equip.DOOR03_SENSOR                        /**/ = new SenseSimul() { Name = "TOP_DOOR3_SENSOR",       /**/ Acturator = chkDoor03 };
            _equip.DOOR04_SENSOR/**/ = new SenseSimul() { Name = "TOP_DOOR4_SENSOR",       /**/ Acturator = chkDoor04 };
            _equip.DOOR05_SENSOR/**/ = new SenseSimul() { Name = "TOP_DOOR5_SENSOR",       /**/ Acturator = chkDoor05 };
            _equip.DOOR06_SENSOR/**/ = new SenseSimul() { Name = "TOP_DOOR6_SENSOR",       /**/ Acturator = chkDoor06 };
            _equip.DOOR07_SENSOR/**/ = new SenseSimul() { Name = "TOP_DOOR7_SENSOR",       /**/ Acturator = chkDoor07 };
            _equip.DOOR08_SENSOR/**/ = new SenseSimul() { Name = "TOP_DOOR8_SENSOR",       /**/ Acturator = chkDoor08 };

            _equip.LdAreaLightCurtain = new SenseSimul() { Name = "LdAreaLightCurtain",       /**/ Acturator = null };
            _equip.UnldAreaLightCurtain = new SenseSimul() { Name = "UnldAreaLightCurtain",       /**/ Acturator = null };


            _equip.LdAMuting = new SenseSimul() { Name = "LdAMuting",   /**/ Acturator = chkLdStageOutBtn1 };
            _equip.LdBMuting = new SenseSimul() { Name = "LdBMuting",   /**/ Acturator = chkLdStageOutBtn2 };
            _equip.UDAMuting = new SenseSimul() { Name = "UDAMuting",  /**/ Acturator = chkUDAMute };
            _equip.UDBMuting = new SenseSimul() { Name = "UDBMuting",  /**/ Acturator = chkUDBMute };

            _equip.GLASS_DETECT_SENSOR_1 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_1",  /**/ Acturator = chkGlassCheckSensor1 };
            _equip.GLASS_DETECT_SENSOR_2 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_2",  /**/ Acturator = chkGlassCheckSensor2 };
            _equip.GLASS_DETECT_SENSOR_3 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_3",  /**/ Acturator = chkGlassCheckSensor3 };
            _equip.GLASS_DETECT_SENSOR_4 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_4",  /**/ Acturator = chkGlassCheckSensor4 };
            _equip.GLASS_DETECT_SENSOR_5 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_5",  /**/ Acturator = chkGlassCheckSensor5 };
            _equip.GLASS_DETECT_SENSOR_6 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_6",  /**/ Acturator = chkGlassCheckSensor6 };
            _equip.GLASS_DETECT_SENSOR_7 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_7",  /**/ Acturator = chkGlassEdgeCheckSensor1 };
            _equip.GLASS_DETECT_SENSOR_8 = new SwitchSimul() { Name = "GLASS_DETECT_SENSOR_8",  /**/ Acturator = chkGlassEdgeCheckSensor2 };

            _equip.ROBOT_ARM_DETECT                       /**/ = new SwitchSimul() { Name = "ROBOT_ARM_DETECT          ",  /**/ Acturator = chkRobotArmCheck };

            _equip.ISOLATOR_1_ALARM                       /**/ = new SwitchSimul() { Name = "ISOLATOR_1_ALARM",  /**/ Acturator = chkIsol01 };
            _equip.ISOLATOR_2_ALARM                       /**/ = new SwitchSimul() { Name = "ISOLATOR_2_ALARM",  /**/ Acturator = chkIsol02 };
            _equip.ISOLATOR_3_ALARM                       /**/ = new SwitchSimul() { Name = "ISOLATOR_3_ALARM",  /**/ Acturator = chkIsol03 };
            _equip.ISOLATOR_4_ALARM                       /**/ = new SwitchSimul() { Name = "ISOLATOR_4_ALARM",  /**/ Acturator = chkIsol04 };

            _equip.IONIZER_1_ON                           /**/ = new SwitchSimul() { Name = "IONIZER_1_ON",  /**/ Acturator = chkIonizer1On };
            _equip.IONIZER_2_ON                           /**/ = new SwitchSimul() { Name = "IONIZER_2_ON",  /**/ Acturator = chkIonizer2On };
            _equip.IONIZER_3_ON                           /**/ = new SwitchSimul() { Name = "IONIZER_3_ON",  /**/ Acturator = chkIonizer3On };
            _equip.IONIZER_4_ON                           /**/ = new SwitchSimul() { Name = "IONIZER_4_ON",  /**/ Acturator = chkIonizer4On };
            _equip.InspPc                                 /**/  = new InspPcSimul();
            _equip.ReviPc                                  /**/  = new RevPCSimul();

            #region 실린더
            _equip.CstLoaderB_CstGrip_Cylinder = new CylinderSimul() { Name = "LDCstLoader_BCstGripCylinder", TotalLength = 100 };
            _equip.CstLoaderA_CstGrip_Cylinder = new CylinderSimul() { Name = "LDCstLoader_ACstGripCylinder", TotalLength = 100 };
            _equip.CstLoaderB_CstGrip_Cylinder_ = new CylinderSimul() { Name = "LDCstLoader_BCstGripCylinder_", TotalLength = 100 };
            _equip.CstLoaderA_CstGrip_Cylinder_ = new CylinderSimul() { Name = "LDCstLoader_ACstGripCylinder_", TotalLength = 100 };
            _equip.LoaderTRB1_UpDown_Cylinder = new CylinderSimul() { Name = "LDLoaderTransfer_BUpDonw1Cylinder", TotalLength = 100 };
            _equip.LoaderTRB2_UpDown_Cylinder = new CylinderSimul() { Name = "LDLoaderTransfer_BUpDonw2Cylinder", TotalLength = 100 };
            _equip.LoaderTRA1_UpDown_Cylinder = new CylinderSimul() { Name = "LDLoaderTransfer_AUpDonw1Cylinder", TotalLength = 100 };
            _equip.LoaderTRA2_UpDown_Cylinder = new CylinderSimul() { Name = "LDLoaderTransfer_AUpDonw2Cylinder", TotalLength = 100 };
            _equip.CstLoaderB_Tilt_Cylinder = new CylinderSimul() { Name = "LDCstLoader_BTiltCylinder", TotalLength = 100 };
            _equip.CstLoaderA_Tilt_Cylinder = new CylinderSimul() { Name = "LDCstLoader_ATiltCylinder", TotalLength = 100 };
            _equip.LaserHead1_UpDown_Cylinder = new CylinderSimul() { Name = "PROCLaserHeaDUnDownCylinder1", TotalLength = 100 };
            _equip.LaserHead2_UpDown_Cylinder = new CylinderSimul() { Name = "PROCLaserHeaDUnDownCylinder2", TotalLength = 100 };
            _equip.LaserHead_Shutter_Cylinder = new CylinderSimul() { Name = "PROCLaserHeaDLaserShutterCylinder", TotalLength = 100 };
            _equip.IRCutStageB_BrushUpDown_Cylinder = new CylinderSimul() { Name = "PROCIRCutStage_BBrushUpDownCylinder", TotalLength = 100 };
            _equip.IRCutStageA_BrushUpDown_Cylinder = new CylinderSimul() { Name = "PROCIRCutStage_ABrushUpDownCylinder", TotalLength = 100 };
            _equip.AfterIRCutTRB1_UpDown_Cylinder = new CylinderSimul() { Name = "PROCAfterIRCutTransfer_BUnDownCylinder1", TotalLength = 100 };
            _equip.AfterIRCutTRB2_UpDown_Cylinder = new CylinderSimul() { Name = "PROCAfterIRCutTransfer_BUnDownCylinder2", TotalLength = 100 };
            _equip.AfterIRCutTRA1_UpDown_Cylinder = new CylinderSimul() { Name = "PROCAfterIRCutTransfer_AUnDownCylinder1", TotalLength = 100 };
            _equip.AfterIRCutTRA2_UpDown_Cylinder = new CylinderSimul() { Name = "PROCAfterIRCutTransfer_AUnDownCylinder2", TotalLength = 100 };
            _equip.BreakStageB_BrushUpDown_Cylinder = new CylinderSimul() { Name = "PROCBreakStage_BBrushUpDownCylinder", TotalLength = 100 };
            _equip.BreakStageA_BrushUpDown_Cylinder = new CylinderSimul() { Name = "PROCBreakStage_ABrushUpDownCylinder", TotalLength = 100 };
            _equip.LaserHead1_DummyShutter_Cylinder = new CylinderSimul() { Name = "PROCLaserHeaDDummyShutterCylinder1", TotalLength = 100 };
            _equip.LaserHead2_DummyShutter_Cylinder = new CylinderSimul() { Name = "PROCLaserHeaDDummyShutterCylinder2", TotalLength = 100 };
            _equip.LaserHead_DummyTank_Cylinder = new CylinderSimul() { Name = "PROCLaserHeaDDummyTankCylinder", TotalLength = 100 };
            _equip.CstUnloaderB_Grip_Cylinder = new CylinderSimul() { Name = "UDCstUnloader_BCasstteGripCylinder", TotalLength = 100 };
            _equip.CstUnloaderA_Grip_Cylinder = new CylinderSimul() { Name = "UDCstUnloader_ACasstteGripCylinder", TotalLength = 100 };
            _equip.CstUnloaderB_Grip_Cylinder_ = new CylinderSimul() { Name = "UDCstUnloader_BCasstteGripCylinder_", TotalLength = 100 };
            _equip.CstUnloaderA_Grip_Cylinder_ = new CylinderSimul() { Name = "UDCstUnloader_ACasstteGripCylinder_", TotalLength = 100 };
            _equip.Buffer_UpDown_Cylinder = new CylinderSimul() { Name = "UDBufferUpDownCylinderXB_OnOff", TotalLength = 100 };
            _equip.BeforeInspUnloaderTRB_UpDonw_Cylinder = new CylinderSimul() { Name = "UDBeforeInspUnloaderTransfer_BUpDonw1Cylinder", TotalLength = 100 };
            _equip.AfterInspUnloaderTRB_UpDonw_Cylinder = new CylinderSimul() { Name = "UDAfterInspUnloaderTransfer_BUpDonw1Cylinder", TotalLength = 100 };
            _equip.BeforeInspUnloaderTRA_UpDonw_Cylinder = new CylinderSimul() { Name = "UDBeforeInspUnloaderTransfer_AUpDonw1Cylinder", TotalLength = 100 };
            _equip.AfterInspUnloaderTRA_UpDonw_Cylinder = new CylinderSimul() { Name = "UDAfterInspUnloaderTransfer_AUpDonw1Cylinder", TotalLength = 100 };

            _equip.BeforeInspUnloaderTRB_UpDonw_Cylinder_ = new CylinderSimul() { Name = "UDBeforeInspUnloaderTransfer_BUpDonw2Cylinder", TotalLength = 100 };
            _equip.AfterInspUnloaderTRB_UpDonw_Cylinder_ = new CylinderSimul() { Name = "UDAfterInspUnloaderTransfer_BUpDonw2Cylinder", TotalLength = 100 };
            _equip.BeforeInspUnloaderTRA_UpDonw_Cylinder_ = new CylinderSimul() { Name = "UDBeforeInspUnloaderTransfer_AUpDonw2Cylinder", TotalLength = 100 };
            _equip.AfterInspUnloaderTRA_UpDonw_Cylinder_ = new CylinderSimul() { Name = "UDAfterInspUnloaderTransfer_AUpDonw2Cylinder", TotalLength = 100 };

            _equip.CstUnloaderB_Tilt_Cylinder = new CylinderSimul() { Name = "UDCstUnloader_BTiltCylinder", TotalLength = 100 };
            _equip.CstUnloaderA_Tilt_Cylinder = new CylinderSimul() { Name = "UDCstUnloader_ATiltCylinder", TotalLength = 100 };
            #endregion

            _equip.SetAddress();

            CIM_ADDR.Initailize(GG.HSMS);
            AOI_ADDR.Initailize(GG.HSMS);

            _equip.LoaderAOGamji.Initialize();
            _equip.LoaderAExist.Initialize();
            _equip.LoaderBOGamji.Initialize();
            _equip.LoaderBExist.Initialize();

            _equip.UnloaderAOGamji.Initialize();
            _equip.UnloaderAExist.Initialize();
            _equip.UnloaderBOGamji.Initialize();
            _equip.UnloaderBExist.Initialize();

            _equip.LdAButton.Initialize();
            _equip.LdBButton.Initialize();
            _equip.LdAReset.Initialize();
            _equip.LdBReset.Initialize();
            _equip.LdAMuting.Initialize();
            _equip.LdBMuting.Initialize();
            _equip.UDAButton.Initialize();
            _equip.UDBButton.Initialize();
            _equip.UDAReset.Initialize();
            _equip.UDBReset.Initialize();
            _equip.UDAMuting.Initialize();
            _equip.UDBMuting.Initialize();

            _equip.Vaccum01.Initialize();
            _equip.Vaccum02.Initialize();
            _equip.Vaccum03.Initialize();
            _equip.Vaccum04.Initialize();
            _equip.Vaccum05.Initialize();
            _equip.Vaccum06.Initialize();
            _equip.Vaccum07.Initialize();
            _equip.Vaccum08.Initialize();
            _equip.Vaccum09.Initialize();
            _equip.Vaccum10.Initialize();
            _equip.Vaccum11.Initialize();
            _equip.Vaccum12.Initialize();
            _equip.Vaccum13.Initialize();
            _equip.Vaccum14.Initialize();
            _equip.Vaccum15.Initialize();
            _equip.Vaccum16.Initialize();
            _equip.Vaccum17.Initialize();
            _equip.Vaccum18.Initialize();
            _equip.Vaccum19.Initialize();
            _equip.Vaccum20.Initialize();
            _equip.Vaccum21.Initialize();
            _equip.Vaccum22.Initialize();
            _equip.Vaccum23.Initialize();
            _equip.Vaccum24.Initialize();
            _equip.Vaccum25.Initialize();
            _equip.Vaccum26.Initialize();
            _equip.Vaccum27.Initialize();
            _equip.Vaccum28.Initialize();
            _equip.Vaccum29.Initialize();
            _equip.Vaccum30.Initialize();
            _equip.Vaccum31.Initialize();
            _equip.Vaccum32.Initialize();
            _equip.Vaccum33.Initialize();
            _equip.Vaccum34.Initialize();
            _equip.Vaccum35.Initialize();
            _equip.Vaccum36.Initialize();
            _equip.Vaccum37.Initialize();
            _equip.Vaccum38.Initialize();

            #region 실린더
            _equip.CstLoaderB_CstGrip_Cylinder.Initialize();
            _equip.CstLoaderA_CstGrip_Cylinder.Initialize();
            _equip.CstLoaderB_CstGrip_Cylinder_.Initialize();
            _equip.CstLoaderA_CstGrip_Cylinder_.Initialize();

            _equip.LoaderTRB1_UpDown_Cylinder.Initialize();
            _equip.LoaderTRB2_UpDown_Cylinder.Initialize();
            _equip.LoaderTRA1_UpDown_Cylinder.Initialize();
            _equip.LoaderTRA2_UpDown_Cylinder.Initialize();
            _equip.CstLoaderB_Tilt_Cylinder.Initialize();
            _equip.CstLoaderA_Tilt_Cylinder.Initialize();

            _equip.LaserHead1_UpDown_Cylinder.Initialize();
            _equip.LaserHead2_UpDown_Cylinder.Initialize();
            _equip.LaserHead_Shutter_Cylinder.Initialize();
            _equip.IRCutStageB_BrushUpDown_Cylinder.Initialize();
            _equip.IRCutStageA_BrushUpDown_Cylinder.Initialize();
            _equip.AfterIRCutTRB1_UpDown_Cylinder.Initialize();
            _equip.AfterIRCutTRB2_UpDown_Cylinder.Initialize();
            _equip.AfterIRCutTRA1_UpDown_Cylinder.Initialize();
            _equip.AfterIRCutTRA2_UpDown_Cylinder.Initialize();
            _equip.BreakStageB_BrushUpDown_Cylinder.Initialize();
            _equip.BreakStageA_BrushUpDown_Cylinder.Initialize();
            _equip.LaserHead1_DummyShutter_Cylinder.Initialize();
            _equip.LaserHead2_DummyShutter_Cylinder.Initialize();
            _equip.LaserHead_DummyTank_Cylinder.Initialize();


            _equip.CstUnloaderB_Grip_Cylinder.Initialize();
            _equip.CstUnloaderA_Grip_Cylinder.Initialize();
            _equip.CstUnloaderB_Grip_Cylinder_.Initialize();
            _equip.CstUnloaderA_Grip_Cylinder_.Initialize();

            _equip.Buffer_UpDown_Cylinder.Initialize();
            _equip.BeforeInspUnloaderTRB_UpDonw_Cylinder.Initialize();
            _equip.AfterInspUnloaderTRB_UpDonw_Cylinder.Initialize();
            _equip.BeforeInspUnloaderTRA_UpDonw_Cylinder.Initialize();
            _equip.AfterInspUnloaderTRA_UpDonw_Cylinder.Initialize();

            _equip.BeforeInspUnloaderTRB_UpDonw_Cylinder_.Initialize();
            _equip.AfterInspUnloaderTRB_UpDonw_Cylinder_.Initialize();
            _equip.BeforeInspUnloaderTRA_UpDonw_Cylinder_.Initialize();
            _equip.AfterInspUnloaderTRA_UpDonw_Cylinder_.Initialize();

            _equip.CstUnloaderB_Tilt_Cylinder.Initialize();
            _equip.CstUnloaderA_Tilt_Cylinder.Initialize();
            #endregion


            _equip.Motors.ToList().ForEach(m => m.Initialize());


            lstViewMotorInfo.View = View.Details;
            lstViewMotorInfo.Columns.Add("No.").Width = 30;
            lstViewMotorInfo.Columns.Add("Name").Width = 229;
            lstViewMotorInfo.Columns.Add("HomeB").Width = 54;
            lstViewMotorInfo.Columns.Add("JogP").Width = 37;
            lstViewMotorInfo.Columns.Add("JogN").Width = 39;
            lstViewMotorInfo.Columns.Add("HomeS").Width = 53;
            lstViewMotorInfo.Columns.Add("PtpS").Width = 38;
            lstViewMotorInfo.Columns.Add("Pos").Width = 60;

            foreach (ServoSimulUmac s in _equip.Motors)
            {
                lstViewMotorInfo.Items.Add(s.InnerAxisNo.ToString()).SubItems.AddRange(new string[] { s.Name, "1", "2", "3", "4", "5", "6" });
            }

            _isRunning = true;
            bgWorker.RunWorkerAsync();

            Logger.Log.AppendLine(LogLevel.Info, "Simulator 시작.............");
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            _isRunning = false;
            while (bgWorker.IsBusy)
                Application.DoEvents();

            base.OnClosing(e);
        }


        int iPos = 0;
        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (_isRunning)
            {
                _equip.Working();
                Thread.Sleep(200);


                bgWorker.ReportProgress(iPos++);

                if (iPos > 10000)
                    iPos = 0;
            }
        }
        private void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //pnlView.Refresh();
        }

        private void btnMemDetail_Click(object sender, EventArgs e)
        {
            _frmPcMonitor.Show();
        }

        private void btnXyIo_Click(object sender, EventArgs e)
        {
            //_frmCtrl.axActEasyIF1.ActLogicalStationNumber = 150;
            //FrmXYMonitor2 ff = new FrmXYMonitor2();
            //ff.Show();
        }

        private void tmrState_Tick(object sender, EventArgs e)
        {
            tbStatus.Text = GetCurrentStatus();
        }
        private string GetCurrentStatus()
        {
            Process p = Process.GetCurrentProcess();
            string s = string.Format(" 스레드 {0}, 핸들 {1}, 메모리 사용 {2}kb, 피크 메모리 사용 {3}kb, 버전 {4}", p.Threads.Count, p.HandleCount, p.WorkingSet64 / 1024, p.PeakWorkingSet64 / 1024, Application.ProductVersion);
            p.Close();
            p.Dispose();
            return s;
        }


        public Bitmap ChangeOpacity(Image img, float opacityvalue)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height);
            Graphics graphics = Graphics.FromImage(bmp);
            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.Matrix33 = opacityvalue;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            graphics.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();
            return bmp;
        }

        private void tmrWorker_Tick(object sender, EventArgs e)
        {
            UpdateUI();
            UpdateMotorInfo();
            lblAlive.BackColor = AOI_ADDR.UPHeartBit.vBit ? Color.Red : Color.Gray;
        }

        private void UpdateMotorInfo()
        {
            lstViewMotorInfo.BeginUpdate();
            int idx;
            foreach (ServoSimulUmac s in _equip.Motors)
            {
                idx = Array.IndexOf(_equip.Motors, s);
                lstViewMotorInfo.Items[idx].SubItems[2].Text = s.XB_StatusHomeCompleteBit.vBit ? "1" : "0";
                lstViewMotorInfo.Items[idx].SubItems[3].Text = s.YB_MotorJogPlusMove.vBit ? "1" : "0";
                lstViewMotorInfo.Items[idx].SubItems[4].Text = s.YB_MotorJogMinusMove.vBit ? "1" : "0";
                lstViewMotorInfo.Items[idx].SubItems[5].Text = s._homeStep.ToString();
                lstViewMotorInfo.Items[idx].SubItems[6].Text = s._ptpStep.ToString();
                lstViewMotorInfo.Items[idx].SubItems[7].Text = s.XF_CurrMotorPosition.vFloat.ToString();
            }
            lstViewMotorInfo.EndUpdate();
        }



        private void btnCtrlPcXy_Click(object sender, EventArgs e)
        {
            btnCtrlPcXy.BackColor = Color.Blue;
            btnSimulPcXy.BackColor = Color.Gray;
            btnTester.BackColor = Color.Gray;

            //X000.SetAddr(0);
            //Y000.SetAddr(0);
            //UMAC.SetAddr(0);

        }
        private void btnSimulPcXy_Click(object sender, EventArgs e)
        {
            btnCtrlPcXy.BackColor = Color.Gray;
            btnSimulPcXy.BackColor = Color.Blue;
            btnTester.BackColor = Color.Gray;

            //X000.SetAddr(1);
            //Y000.SetAddr(1);
            //UMAC.SetAddr(1);

        }
        private void btnTester_Click(object sender, EventArgs e)
        {
            btnCtrlPcXy.BackColor = Color.Gray;
            btnSimulPcXy.BackColor = Color.Gray;
            btnTester.BackColor = Color.Blue;

            //X000.SetAddr(2);
            //Y000.SetAddr(2);
            //UMAC.SetAddr(2);
        }


        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnXyIoAddr_Click(object sender, EventArgs e)
        {
            //FrmXYMonitorTemp ff = new FrmXYMonitorTemp(btnCtrlPcXy.BackColor == Color.Blue);
            //ff.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }


        private void chkUseInterlock_CheckedChanged(object sender, EventArgs e)
        {
            chkUseInterlock.BackColor = chkUseInterlock.Checked ? Color.Red : Color.Gray;

            _equip.InterLock = chkUseInterlock.Checked;
        }

        private void btnSendAbleStart_Click(object sender, EventArgs e)
        {
            _equip.PioSimul.StepPioSend = 10;
            CIM_ADDR.LOSendAble.vBit = true;
        }

        private void btnRecvAbleStart_Click(object sender, EventArgs e)
        {
            _equip.PioSimul.StepPioRecv = 10;
            CIM_ADDR.UPReceiveAble.vBit = true;
        }


        private void chkReviReviEnd_CheckedChanged(object sender, EventArgs e)
        {
            _equip.ReviPc.LstReviPcEvent[(int)EmReviPcEvent.REVIEW_COMPLETE].StartEvent();
        }

        private void chkInspXError_CheckedChanged(object sender, EventArgs e)
        {
            _equip.ScanX1.XB_ErrAmpFaultError.vBit = !_equip.ScanX1.XB_ErrAmpFaultError.vBit;
        }
        private void btnReadHsmsValue_Click(object sender, EventArgs e)
        {
            PlcAddr addr = PlcAddr.Parsing(txtReadAddr.Text);

            bool vv = GG.HSMS.VirGetBit(addr);
            txtReadValue.Text = vv.ToString();
            //GG.HSMS.VirSetBit(addr, !vv);
        }

        private void lblInspScanStartOk_Click(object sender, EventArgs e)
        {
            _equip.InspPc.XB_ScanStartSuccess.vBit = !_equip.InspPc.XB_ScanStartSuccess.vBit;
        }

        private void chkDoorAll_CheckedChanged(object sender, EventArgs e)
        {
            _equip.DOOR01_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
            _equip.DOOR02_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
            _equip.DOOR03_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
            _equip.DOOR04_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
            _equip.DOOR05_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
            _equip.DOOR06_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
            _equip.DOOR07_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
            _equip.DOOR08_SENSOR.XB_OnOff.vBit = !_equip.DOOR08_SENSOR.XB_OnOff.vBit;
        }


        private void UpdateUI()
        {
            #region 축

            ucrlEquipView.CstLoaderARotation = _equip.LDCstLoader_ACstRotationAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.CstLoaderAUpDown = _equip.LDCstLoader_ACstUpDownAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.CstLoaderBRotation = _equip.LDCstLoader_BCstRotationAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.CstLoaderBUpDown = _equip.LDCstLoader_BCstUpDownAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderAX = _equip.LDLoaderX1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderBX = _equip.LDLoaderX2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderAY = _equip.LDLoaderY1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderBY = _equip.LDLoaderY2Axis.XF_CurrMotorPosition.vFloat;

            ucrlEquipView.LoaderTRB1Y = _equip.LDLoaderTransfer_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderTRB2Y = _equip.LDLoaderTransfer_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderTRB1X = _equip.LDLoaderTransfer_BX1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderTRB2X = _equip.LDLoaderTransfer_BX2Axis.XF_CurrMotorPosition.vFloat;

            //ucrlEquipView.Sub_LoaderTRB1Y = _equip.LDLoaderTransfer_BSubY1Axis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_LoaderTRB2Y = _equip.LDLoaderTransfer_BSubY1Axis.XF_CurrMotorPosition.vFloat;
            ////ucrlEquipView.Sub_LoaderTRB1T = _equip.LDLoaderTransfer_BSubT1Axis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_LoaderTRB2T = _equip.LDLoaderTransfer_BSubT2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderTRA1Y = _equip.LDLoaderTransfer_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderTRA2Y = _equip.LDLoaderTransfer_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderTRA1X = _equip.LDLoaderTransfer_AX1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LoaderTRA2X = _equip.LDLoaderTransfer_AX2Axis.XF_CurrMotorPosition.vFloat;

            //ucrlEquipView.Sub_LoaderTRA1Y = _equip.LDLoaderTransfer_ASubY1Axis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_LoaderTRA2Y = _equip.LDLoaderTransfer_ASubY1Axis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_LoaderTRA1T = _equip.LDLoaderTransfer_ASubT1Axis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_LoaderTRA2T = _equip.LDLoaderTransfer_ASubT2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.PreAlignX = _equip.LDPreAlignXAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.FineAlignX = _equip.PROCFineAlginXAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.IRCutStageA1Y = _equip.PROCIRCutStage_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.IRCutStageA2Y = _equip.PROCIRCutStage_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.IRCutStageB1Y = _equip.PROCIRCutStage_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.IRCutStageB2Y = _equip.PROCIRCutStage_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LaserHeadX = _equip.PROCLaserHeadXAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.LaserHeadZ = _equip.PROCLaserHeadZAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterIRCutTRAY = _equip.PROCAfterIRCutTransfer_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterIRCutTRBY = _equip.PROCAfterIRCutTransfer_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakTableB1Y = _equip.PROCBreakStage_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakTableB2Y = _equip.PROCBreakStage_BYAxis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_BreakTableB1T = _equip.PROCBreakStage_BSubT1Axis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_BreakTableB2T = _equip.PROCBreakStage_BSubT2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakTableA1Y = _equip.PROCBreakStage_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakTableA2Y = _equip.PROCBreakStage_AYAxis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_BreakTableA1T = _equip.PROCBreakStage_ASubT1Axis.XF_CurrMotorPosition.vFloat;
            //ucrlEquipView.Sub_BreakTableA2T = _equip.PROCBreakStage_ASubT2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakingHead1X = _equip.PROCBreakingHeadX1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakingHead2X = _equip.PROCBreakingHeadX2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakingHead1Z = _equip.PROCBreakingHeadZ1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakingHead2Z = _equip.PROCBreakingHeadZ2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakAlign1X = _equip.PROCBreakAlignXAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakAlign2X = _equip.PROCBreakAlignXAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakAlign1Z = _equip.PROCBreakAlignZAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BreakAlign2Z = _equip.PROCBreakAlignZAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BeforeInspTRA1Y = _equip.UDBeforeInspUnloaderTransfer_AY1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BeforeInspTRA2Y = _equip.UDBeforeInspUnloaderTransfer_AY1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BeforeInspTRB1Y = _equip.UDBeforeInspUnloaderTransfer_BY1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.BeforeInspTRB2Y = _equip.UDBeforeInspUnloaderTransfer_BY1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRA1Y = _equip.UDAfterInspUnloaderTransfer_AY2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRA2Y = _equip.UDAfterInspUnloaderTransfer_AY2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRA1T = _equip.UDAfterInspUnloaderTransfer_AT1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRA2T = _equip.UDAfterInspUnloaderTransfer_AT2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRB1Y = _equip.UDAfterInspUnloaderTransfer_BY2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRB2Y = _equip.UDAfterInspUnloaderTransfer_BY2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRB1T = _equip.UDAfterInspUnloaderTransfer_BT1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.AfterInspTRB2T = _equip.UDAfterInspUnloaderTransfer_BT2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.InspCameraX = _equip.UDInspCameraXAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.InspCameraZ = _equip.UDInspCameraZAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.InspStageA1Y = _equip.UDInspectionStage_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.InspStageA2Y = _equip.UDInspectionStage_AYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.InspStageB1Y = _equip.UDInspectionStage_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.InspStageB2Y = _equip.UDInspectionStage_BYAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.UnloaderAX = _equip.UDUnloaderX1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.UnloaderBX = _equip.UDUnloaderX2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.UnloaderAY = _equip.UDUnloaderY1Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.UnloaderBY = _equip.UDUnloaderY2Axis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.CstUnloaderARotation = _equip.UDCstUnloader_ACstRotationAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.CstUnloaderAUpDown = _equip.UDCstUnloader_ACstUpDownAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.CstUnloaderBRotation = _equip.UDCstUnloader_BCstRotationAxis.XF_CurrMotorPosition.vFloat;
            ucrlEquipView.CstUnloaderBUpDown = _equip.UDCstUnloader_BCstUpDownAxis.XF_CurrMotorPosition.vFloat;
            #endregion

            #region 실린더

            ucrlEquipView.CstLoaderBL_Grip_Cylinder = _equip.CstLoaderB_CstGrip_Cylinder.CurrPosition;
            ucrlEquipView.CstLoaderBR_Grip_Cylinder = _equip.CstLoaderB_CstGrip_Cylinder_.CurrPosition;
            ucrlEquipView.CstLoaderAL_Grip_Cylinder = _equip.CstLoaderA_CstGrip_Cylinder.CurrPosition;
            ucrlEquipView.CstLoaderAR_Grip_Cylinder = _equip.CstLoaderA_CstGrip_Cylinder_.CurrPosition;
            ucrlEquipView.LoaderTRB1_UpDown_Cylinder = _equip.LoaderTRB1_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.LoaderTRB2_UpDown_Cylinder = _equip.LoaderTRB2_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.LoaderTRA1_UpDown_Cylinder = _equip.LoaderTRA1_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.LoaderTRA2_UpDown_Cylinder = _equip.LoaderTRA2_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.CstLoaderA_Tilt_Cylinder = _equip.CstLoaderA_Tilt_Cylinder.CurrPosition;
            ucrlEquipView.CstLoaderB_Tilt_Cylinder = _equip.CstLoaderB_Tilt_Cylinder.CurrPosition;

            ucrlEquipView.LaserHead1_UpDown_Cylinder = _equip.LaserHead1_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.LaserHead2_UpDown_Cylinder = _equip.LaserHead2_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.LaserShutter_Cylinder = _equip.LaserHead_Shutter_Cylinder.CurrPosition;
            ucrlEquipView.IRCutStageA_UpDown_Cylinder = _equip.IRCutStageA_BrushUpDown_Cylinder.CurrPosition;
            ucrlEquipView.IRCutStageB_UpDown_Cylinder = _equip.IRCutStageB_BrushUpDown_Cylinder.CurrPosition;
            ucrlEquipView.BreakTableA_UpDown_Cylinder = _equip.BreakStageA_BrushUpDown_Cylinder.CurrPosition;
            ucrlEquipView.BreakTalbeB_UpDown_Cylinder = _equip.BreakStageB_BrushUpDown_Cylinder.CurrPosition;
            ucrlEquipView.BreakTRB1_UpDown_Cylinder = _equip.AfterIRCutTRB1_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.BreakTRB2_UpDown_Cylinder = _equip.AfterIRCutTRB2_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.BreakTRA1_UpDown_Cylinder = _equip.AfterIRCutTRA1_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.BreakTRA2_UpDown_Cylinder = _equip.AfterIRCutTRA2_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.LaserDummyShutter1_Cylinder = _equip.LaserHead1_DummyShutter_Cylinder.CurrPosition;
            ucrlEquipView.LaserDummyShutter2_Cylinder = _equip.LaserHead2_DummyShutter_Cylinder.CurrPosition;
            ucrlEquipView.DummyTank_Cylinder = _equip.LaserHead_DummyTank_Cylinder.CurrPosition;

            ucrlEquipView.Buffer_UpDown_Cylinder = _equip.Buffer_UpDown_Cylinder.CurrPosition;
            ucrlEquipView.CstUnloaderBL_Grip_Cylinder = _equip.CstUnloaderB_Grip_Cylinder.CurrPosition;
            ucrlEquipView.CstUnloaderBR_Grip_Cylinder = _equip.CstUnloaderB_Grip_Cylinder_.CurrPosition;
            ucrlEquipView.CstUnloaderAL_Grip_Cylinder = _equip.CstUnloaderA_Grip_Cylinder.CurrPosition;
            ucrlEquipView.CstUnloaderAR_Grip_Cylinder = _equip.CstUnloaderA_Grip_Cylinder_.CurrPosition;
            ucrlEquipView.BeforeInspTRB1_UpDown_Cylinder = _equip.BeforeInspUnloaderTRB_UpDonw_Cylinder.CurrPosition;
            ucrlEquipView.BeforeInspTRB2_UpDown_Cylinder = _equip.BeforeInspUnloaderTRB_UpDonw_Cylinder_.CurrPosition;
            ucrlEquipView.BeforeInspTRA1_UpDown_Cylinder = _equip.BeforeInspUnloaderTRA_UpDonw_Cylinder.CurrPosition;
            ucrlEquipView.BeforeInspTRA2_UpDown_Cylinder = _equip.BeforeInspUnloaderTRA_UpDonw_Cylinder_.CurrPosition;
            ucrlEquipView.AfterInspTRB1_UpDown_Cylinder = _equip.AfterInspUnloaderTRB_UpDonw_Cylinder.CurrPosition;
            ucrlEquipView.AfterInspTRB2_UpDown_Cylinder = _equip.AfterInspUnloaderTRB_UpDonw_Cylinder_.CurrPosition;
            ucrlEquipView.AfterInspTRA1_UpDown_Cylinder = _equip.AfterInspUnloaderTRA_UpDonw_Cylinder.CurrPosition;
            ucrlEquipView.AfterInspTRA2_UpDown_Cylinder = _equip.AfterInspUnloaderTRA_UpDonw_Cylinder_.CurrPosition;

            ucrlEquipView.CstUnloaderA_Tilt_Cylinder = _equip.CstUnloaderA_Tilt_Cylinder.CurrPosition;
            ucrlEquipView.CstUnloaderB_Tilt_Cylinder = _equip.CstUnloaderB_Tilt_Cylinder.CurrPosition;

            //ucrlEquipView.BreakingHead1_UpDown_Cylinder = _cylinder;
            //ucrlEquipView.BreakingHead2_UpDown_Cylinder = _cylinder;
            //ucrlEquipView.InspA1_UpDown_Cylinder = _cylinder;
            //ucrlEquipView.InspA2_UpDown_Cylinder = _cylinder;
            //ucrlEquipView.InspB1_UpDown_Cylinder = _cylinder;
            //ucrlEquipView.InspB2_UpDown_Cylinder = _cylinder;

            #endregion

            #region 버큠
            ucrlEquipView.LoaderA = _equip.Vaccum01.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderB = _equip.Vaccum02.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.LoaderTRA1 = _equip.Vaccum03.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRA2 = _equip.Vaccum04.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB1 = _equip.Vaccum05.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.LoaderTRB2 = _equip.Vaccum06.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;

               ucrlEquipView.ProcessTableA1_1 = _equip.Vaccum07.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_1 = _equip.Vaccum09.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_1 = _equip.Vaccum11.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_1 = _equip.Vaccum13.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA1_2 = _equip.Vaccum08.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableA2_2 = _equip.Vaccum10.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB1_2 = _equip.Vaccum12.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.ProcessTableB2_2 = _equip.Vaccum14.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;

            ucrlEquipView.BreakTRA = _equip.Vaccum15.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRA_ = _equip.Vaccum16.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB = _equip.Vaccum17.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BreakTRB_ = _equip.Vaccum18.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;

            ucrlEquipView.BreakTableA1 = _equip.Vaccum19.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableA2 = _equip.Vaccum20.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB1 = _equip.Vaccum21.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.BreakTableB2 = _equip.Vaccum22.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;

            ucrlEquipView.BeforeInspTRA1 = _equip.Vaccum25.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRA2 = _equip.Vaccum26.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB1 = _equip.Vaccum27.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.BeforeInspTRB2 = _equip.Vaccum28.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.InspStageA1 = _equip.Vaccum33.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageA2 = _equip.Vaccum34.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB1 = _equip.Vaccum35.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.InspStageB2 = _equip.Vaccum36.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.AfterInspTRA1 = _equip.Vaccum29.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRA2 = _equip.Vaccum30.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB1 = _equip.Vaccum31.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.AfterInspTRB2 = _equip.Vaccum32.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Gainsboro;
            ucrlEquipView.UnloaderA = _equip.Vaccum23.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            ucrlEquipView.UnloaderB = _equip.Vaccum24.XB_OnOff.vBit ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Black;
            #endregion
        }


    }
}

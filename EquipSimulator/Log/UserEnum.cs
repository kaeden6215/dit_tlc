﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dit.Framework.Log
{
    public enum LogLevel
    {
        AllLog = 1,
        NoLog = 2,
        Info = 4,
        Warning = 8,
        Error = 16,
        Fault = 32,
        Start = 64,
        End = 128,
        Stop = 256,
        Exception = 512,
        Closed = 1024,
        Connected = 2048,        
    }
}

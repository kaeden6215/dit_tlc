﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EquipSimulator.Acturator;
using EquipSimulator;
using EquipSimulator.Log;
using Dit.Framework.Log;
using EquipMainUi.Struct;
using Dit.Framework.Comm;

namespace EquipSimulator
{
    public class EquipSimul
    {
        public SenseSimul LoaderAOGamji;
        public SenseSimul LoaderAExist;
        public SenseSimul LoaderBOGamji;
        public SenseSimul LoaderBExist;

        public SenseSimul UnloaderAOGamji;
        public SenseSimul UnloaderAExist;
        public SenseSimul UnloaderBOGamji;
        public SenseSimul UnloaderBExist;

        public SenseSimul LdAButton;
        public SenseSimul LdBButton;
        public SenseSimul LdAReset;
        public SenseSimul LdBReset;
        public SenseSimul UDAButton;
        public SenseSimul UDBButton;
        public SenseSimul UDAReset;
        public SenseSimul UDBReset;

        #region 옛날꺼
        public InspPcSimul InspPc;
        public RevPCSimul ReviPc;

        
        public ServoSimulUmac ScanX1;
        public ServoSimulUmac[] Motors;

        

        public SwitchSimulNonX Blower01;
        public SwitchSimulNonX Blower02;
        public SwitchSimulNonX Blower03;
        public SwitchSimulNonX Blower04;
        public SwitchSimulNonX Blower05;
        public SwitchSimulNonX Blower06;
        public SwitchSimulNonX Blower07;
        public SwitchSimulNonX Blower08;
        public SwitchSimulNonX Blower09;
        public SwitchSimulNonX Blower10;
        public SwitchSimulNonX Blower11;
        public SwitchSimulNonX Blower12;
        public SwitchSimulNonX Blower13;
        public SwitchSimulNonX Blower14;
        public SwitchSimulNonX Blower15;
        public SwitchSimulNonX Blower16;
        public SwitchSimulNonX Blower17;
        public SwitchSimulNonX Blower18;
        public SwitchSimulNonX Blower19;
        public SwitchSimulNonX Blower20;
        public SwitchSimulNonX Blower21;
        public SwitchSimulNonX Blower22;
        public SwitchSimulNonX Blower23;
        public SwitchSimulNonX Blower24;
        public SwitchSimulNonX Blower25;
        public SwitchSimulNonX Blower26;
        public SwitchSimulNonX Blower27;
        public SwitchSimulNonX Blower28;
        public SwitchSimulNonX Blower29;
        public SwitchSimulNonX Blower30;
        public SwitchSimulNonX Blower31;
        public SwitchSimulNonX Blower32;
        public SwitchSimulNonX Blower33;
        public SwitchSimulNonX Blower34;
        public SwitchSimulNonX Blower35;
        public SwitchSimulNonX Blower36;
        public SwitchSimulNonX Blower37;
        public SwitchSimulNonX Blower38;

        public SwitchSimul CameraCooling;
        public SwitchSimul IonizerCover;
        public SwitchSimul Ionizer;

        public SwitchSimul EMERGENCY_STOP_1;
        public SwitchSimul EMERGENCY_STOP_2;
        public SwitchSimul EMERGENCY_STOP_3;
        public SwitchSimul EMERGENCY_STOP_4;
        public SwitchSimul EMERGENCY_STOP_5;
        public SwitchSimul EMERGENCY_STOP_6;
        public SwitchSimul MODE_SELECT_KEY_SW_AUTOTEACH;

        public SwitchSimul ENABLE_GRIB_SW_ON;
        public SwitchSimul SAFETY_PLC_ERROR;

        public SenseSimul DOOR01_SENSOR;
        public SenseSimul DOOR02_SENSOR;
        public SenseSimul DOOR03_SENSOR;
        public SenseSimul DOOR04_SENSOR;
        public SenseSimul DOOR05_SENSOR;
        public SenseSimul DOOR06_SENSOR;
        public SenseSimul DOOR07_SENSOR;
        public SenseSimul DOOR08_SENSOR;

        

        public SwitchSimul GLASS_DETECT_SENSOR_1;
        public SwitchSimul GLASS_DETECT_SENSOR_2;
        public SwitchSimul GLASS_DETECT_SENSOR_3;
        public SwitchSimul GLASS_DETECT_SENSOR_4;
        public SwitchSimul GLASS_DETECT_SENSOR_5;
        public SwitchSimul GLASS_DETECT_SENSOR_6;
        public SwitchSimul GLASS_DETECT_SENSOR_7;
        public SwitchSimul GLASS_DETECT_SENSOR_8;
        public SwitchSimul ROBOT_ARM_DETECT;

        public SwitchSimul ISOLATOR_1_ALARM;
        public SwitchSimul ISOLATOR_2_ALARM;
        public SwitchSimul ISOLATOR_3_ALARM;
        public SwitchSimul ISOLATOR_4_ALARM;

        public SwitchSimul IONIZER_1_ON;
        public SwitchSimul IONIZER_2_ON;
        public SwitchSimul IONIZER_3_ON;
        public SwitchSimul IONIZER_4_ON;

        public SenseSimul LdAreaLightCurtain;
        public SenseSimul UnldAreaLightCurtain;

        #endregion

        #region 축
        public ServoSimulUmac LDCstLoader_BCstRotationAxis;
        public ServoSimulUmac LDCstLoader_BCstUpDownAxis;
        public ServoSimulUmac LDCstLoader_ACstRotationAxis;
        public ServoSimulUmac LDCstLoader_ACstUpDownAxis;
        public ServoSimulUmac LDLoaderX1Axis;
        public ServoSimulUmac LDLoaderX2Axis;
        public ServoSimulUmac LDLoaderY1Axis;
        public ServoSimulUmac LDLoaderY2Axis;
        public ServoSimulUmac LDLoaderTransfer_BYAxis;
        public ServoSimulUmac LDLoaderTransfer_BX1Axis;
        public ServoSimulUmac LDLoaderTransfer_BX2Axis;
        public ServoSimulUmac LDLoaderTransfer_BSubY1Axis;
        public ServoSimulUmac LDLoaderTransfer_BSubY2Axis;
        public ServoSimulUmac LDLoaderTransfer_BSubT1Axis;
        public ServoSimulUmac LDLoaderTransfer_BSubT2Axis;
        public ServoSimulUmac LDLoaderTransfer_AYAxis;
        public ServoSimulUmac LDLoaderTransfer_AX1Axis;
        public ServoSimulUmac LDLoaderTransfer_AX2Axis;
        public ServoSimulUmac LDLoaderTransfer_ASubY1Axis;
        public ServoSimulUmac LDLoaderTransfer_ASubY2Axis;
        public ServoSimulUmac LDLoaderTransfer_ASubT1Axis;
        public ServoSimulUmac LDLoaderTransfer_ASubT2Axis;
        public ServoSimulUmac LDPreAlignXAxis;
        public ServoSimulUmac PROCFineAlginXAxis;
        public ServoSimulUmac PROCIRCutStage_BYAxis;
        public ServoSimulUmac PROCIRCutStage_AYAxis;
        public ServoSimulUmac PROCLaserHeadXAxis;
        public ServoSimulUmac PROCLaserHeadZAxis;
        public ServoSimulUmac PROCAfterIRCutTransfer_BYAxis;
        public ServoSimulUmac PROCAfterIRCutTransfer_AYAxis;
        public ServoSimulUmac PROCBreakStage_BYAxis;
        public ServoSimulUmac PROCBreakStage_BSubX1Axis;
        public ServoSimulUmac PROCBreakStage_BSubX2Axis;
        public ServoSimulUmac PROCBreakStage_BSubY1Axis;
        public ServoSimulUmac PROCBreakStage_BSubY2Axis;
        public ServoSimulUmac PROCBreakStage_BSubT1Axis;
        public ServoSimulUmac PROCBreakStage_BSubT2Axis;
        public ServoSimulUmac PROCBreakStage_AYAxis;
        public ServoSimulUmac PROCBreakStage_ASubX1Axis;
        public ServoSimulUmac PROCBreakStage_ASubX2Axis;
        public ServoSimulUmac PROCBreakStage_ASubY1Axis;
        public ServoSimulUmac PROCBreakStage_ASubY2Axis;
        public ServoSimulUmac PROCBreakStage_ASubT1Axis;
        public ServoSimulUmac PROCBreakStage_ASubT2Axis;
        public ServoSimulUmac PROCBreakingHeadX1Axis;
        public ServoSimulUmac PROCBreakingHeadX2Axis;
        public ServoSimulUmac PROCBreakingHeadZ1Axis;
        public ServoSimulUmac PROCBreakingHeadZ2Axis;
        public ServoSimulUmac PROCBreakAlignXAxis;
        public ServoSimulUmac PROCBreakAlignZAxis;
        public ServoSimulUmac UDBeforeInspUnloaderTransfer_BY1Axis;
        public ServoSimulUmac UDBeforeInspUnloaderTransfer_AY1Axis;
        public ServoSimulUmac UDAfterInspUnloaderTransfer_BY2Axis;
        public ServoSimulUmac UDAfterInspUnloaderTransfer_BT1Axis;
        public ServoSimulUmac UDAfterInspUnloaderTransfer_BT2Axis;
        public ServoSimulUmac UDAfterInspUnloaderTransfer_AY2Axis;
        public ServoSimulUmac UDAfterInspUnloaderTransfer_AT1Axis;
        public ServoSimulUmac UDAfterInspUnloaderTransfer_AT2Axis;
        public ServoSimulUmac UDInspCameraXAxis;
        public ServoSimulUmac UDInspCameraZAxis;
        public ServoSimulUmac UDInspectionStage_BYAxis;
        public ServoSimulUmac UDInspectionStage_AYAxis;
        public ServoSimulUmac UDUnloaderX1Axis;
        public ServoSimulUmac UDUnloaderX2Axis;
        public ServoSimulUmac UDUnloaderY1Axis;
        public ServoSimulUmac UDUnloaderY2Axis;
        public ServoSimulUmac UDCstUnloader_BCstRotationAxis;
        public ServoSimulUmac UDCstUnloader_BCstUpDownAxis;
        public ServoSimulUmac UDCstUnloader_ACstRotationAxis;
        public ServoSimulUmac UDCstUnloader_ACstUpDownAxis;
        #endregion
        #region 기타
        
        public SenseSimul LdAMuting;
        public SenseSimul LdBMuting;
        
        public SenseSimul UDAMuting;
        public SenseSimul UDBMuting;
        #endregion
        #region 실린더

        public CylinderSimul CstLoaderB_CstGrip_Cylinder;//
        public CylinderSimul CstLoaderA_CstGrip_Cylinder;//
        public CylinderSimul CstLoaderB_CstGrip_Cylinder_;//
        public CylinderSimul CstLoaderA_CstGrip_Cylinder_;//
        public CylinderSimul LoaderTRB1_UpDown_Cylinder;//
        public CylinderSimul LoaderTRB2_UpDown_Cylinder;//
        public CylinderSimul LoaderTRA1_UpDown_Cylinder;//
        public CylinderSimul LoaderTRA2_UpDown_Cylinder;//
        public CylinderSimul CstLoaderB_Tilt_Cylinder;//
        public CylinderSimul CstLoaderA_Tilt_Cylinder;//

        public CylinderSimul LaserHead1_UpDown_Cylinder;//
        public CylinderSimul LaserHead2_UpDown_Cylinder;//
        public CylinderSimul LaserHead_Shutter_Cylinder;//
        public CylinderSimul IRCutStageB_BrushUpDown_Cylinder;//
        public CylinderSimul IRCutStageA_BrushUpDown_Cylinder;//
        public CylinderSimul AfterIRCutTRB1_UpDown_Cylinder;//
        public CylinderSimul AfterIRCutTRB2_UpDown_Cylinder;//
        public CylinderSimul AfterIRCutTRA1_UpDown_Cylinder;//
        public CylinderSimul AfterIRCutTRA2_UpDown_Cylinder;//
        public CylinderSimul BreakStageB_BrushUpDown_Cylinder;//
        public CylinderSimul BreakStageA_BrushUpDown_Cylinder;//
        public CylinderSimul LaserHead1_DummyShutter_Cylinder;//
        public CylinderSimul LaserHead2_DummyShutter_Cylinder;//
        public CylinderSimul LaserHead_DummyTank_Cylinder;//

        public CylinderSimul Buffer_UpDown_Cylinder;//
        public CylinderSimul CstUnloaderB_Grip_Cylinder;//
        public CylinderSimul CstUnloaderA_Grip_Cylinder;//
        public CylinderSimul CstUnloaderB_Grip_Cylinder_;//
        public CylinderSimul CstUnloaderA_Grip_Cylinder_;//
        public CylinderSimul BeforeInspUnloaderTRB_UpDonw_Cylinder;//
        public CylinderSimul AfterInspUnloaderTRB_UpDonw_Cylinder;//
        public CylinderSimul BeforeInspUnloaderTRA_UpDonw_Cylinder;//
        public CylinderSimul AfterInspUnloaderTRA_UpDonw_Cylinder;//

        public CylinderSimul BeforeInspUnloaderTRB_UpDonw_Cylinder_;//
        public CylinderSimul AfterInspUnloaderTRB_UpDonw_Cylinder_;//
        public CylinderSimul BeforeInspUnloaderTRA_UpDonw_Cylinder_;//
        public CylinderSimul AfterInspUnloaderTRA_UpDonw_Cylinder_;//

        public CylinderSimul CstUnloaderB_Tilt_Cylinder;//
        public CylinderSimul CstUnloaderA_Tilt_Cylinder;//

        #endregion
        #region 버큠
        public SwitchSimul Vaccum01;
        public SwitchSimul Vaccum02;
        public SwitchSimul Vaccum03;
        public SwitchSimul Vaccum04;
        public SwitchSimul Vaccum05;
        public SwitchSimul Vaccum06;
        public SwitchSimul Vaccum07;
        public SwitchSimul Vaccum08;
        public SwitchSimul Vaccum09;
        public SwitchSimul Vaccum10;
        public SwitchSimul Vaccum11;
        public SwitchSimul Vaccum12;
        public SwitchSimul Vaccum13;
        public SwitchSimul Vaccum14;
        public SwitchSimul Vaccum15;
        public SwitchSimul Vaccum16;
        public SwitchSimul Vaccum17;
        public SwitchSimul Vaccum18;
        public SwitchSimul Vaccum19;
        public SwitchSimul Vaccum20;
        public SwitchSimul Vaccum21;
        public SwitchSimul Vaccum22;
        public SwitchSimul Vaccum23;
        public SwitchSimul Vaccum24;
        public SwitchSimul Vaccum25;
        public SwitchSimul Vaccum26;
        public SwitchSimul Vaccum27;
        public SwitchSimul Vaccum28;
        public SwitchSimul Vaccum29;
        public SwitchSimul Vaccum30;
        public SwitchSimul Vaccum31;
        public SwitchSimul Vaccum32;
        public SwitchSimul Vaccum33;
        public SwitchSimul Vaccum34;
        public SwitchSimul Vaccum35;
        public SwitchSimul Vaccum36;
        public SwitchSimul Vaccum37;
        public SwitchSimul Vaccum38;
        #endregion



        public bool InterLock;

        public PioHandShake PioSimul = new PioHandShake();
        //public PioHandShake PioA2I = new PioHandShake();
        //public PioHandShake PioI2A = new PioHandShake();
        public PMacSimul PMac = new PMacSimul();

        public EquipSimul()
        {
            int axisNo = 1;

            #region 축
            LDCstLoader_ACstRotationAxis            /*CstLoaderARotation*/  = new ServoSimulUmac(axisNo++, "LDCstLoader_ACstRotationAxis", 200, 83, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDCstLoader_ACstUpDownAxis              /*CstLoaderAUpDown*/    = new ServoSimulUmac(axisNo++, "LDCstLoader_ACstUpDownAxis", 400, 500, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDCstLoader_BCstRotationAxis            /*CstLoaderBRotation*/  = new ServoSimulUmac(axisNo++, "LDCstLoader_BCstRotationAxis", 200, 83, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDCstLoader_BCstUpDownAxis              /*CstLoaderBUpDown*/    = new ServoSimulUmac(axisNo++, "LDCstLoader_BCstUpDownAxis", 400, 500, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderX1Axis                          /*LoaderAX*/            = new ServoSimulUmac(axisNo++, "LDLoaderX1Axis", 850, 1000, 18);//
            LDLoaderX2Axis                          /*LoaderBX*/            = new ServoSimulUmac(axisNo++, "LDLoaderX2Axis", 850, 1000, 18);//
            LDLoaderY1Axis                          /*LoaderAY*/            = new ServoSimulUmac(axisNo++, "LDLoaderY1Axis", 500, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderY2Axis                          /*LoaderBY*/            = new ServoSimulUmac(axisNo++, "LDLoaderY2Axis", 500, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_AYAxis                 /*LoaderTRAY*/          = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_AYAxis", 1400, 1400, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_AX1Axis                /*LoaderTRA1X*/         = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_AX1Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_AX2Axis                /*LoaderTRA2X*/         = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_AX2Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_ASubY1Axis             /*Sub_LoaderTRA1Y*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_ASubY1Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_ASubY2Axis             /*Sub_LoaderTRA2Y*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_ASubY2Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_ASubT1Axis             /*Sub_LoaderTRA1T*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_ASubT1Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_ASubT2Axis             /*Sub_LoaderTRA2T*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_ASubT2Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_BYAxis                 /*LoaderTRBY*/          = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_BYAxis", 1400, 1400, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_BX1Axis                /*LoaderTRB1X*/         = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_BX1Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_BX2Axis                /*LoaderTRB2X*/         = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_BX2Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_BSubY1Axis             /*Sub_LoaderTRB1Y*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_BSubY1Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_BSubY2Axis             /*Sub_LoaderTRB2Y*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_BSubY2Axis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_BSubT1Axis             /*Sub_LoaderTRB1T*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_BSubT1Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDLoaderTransfer_BSubT2Axis             /*Sub_LoaderTRB2T*/     = new ServoSimulUmac(axisNo++, "LDLoaderTransfer_BSubT2Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            LDPreAlignXAxis                         /*PreAlignX*/           = new ServoSimulUmac(axisNo++, "LDPreAlignXAxis", 1000, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            PROCFineAlginXAxis                      /*FineAlignX*/          = new ServoSimulUmac(axisNo++, "PROCFineAlginXAxis", 1000, 500, 18) { MotorType = EM_MOTOR_TYPE.Umac };
            PROCIRCutStage_AYAxis                   /*IRCutStageAY*/        = new ServoSimulUmac(axisNo++, "PROCIRCutStage_AYAxis", 1350, 1200, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCIRCutStage_BYAxis                   /*IRCutStageBY*/        = new ServoSimulUmac(axisNo++, "PROCIRCutStage_BYAxis", 1350, 1200, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCLaserHeadXAxis                      /*LaserHeadX*/          = new ServoSimulUmac(axisNo++, "PROCLaserHeadXAxis", 1150, 1200, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCLaserHeadZAxis                      /*LaserHeadZ*/          = new ServoSimulUmac(axisNo++, "PROCLaserHeadZAxis", 100, 50, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCAfterIRCutTransfer_AYAxis           /*AfterIRCutTRAY*/      = new ServoSimulUmac(axisNo++, "PROCAfterIRCutTransfer_AYAxis", 450, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            PROCAfterIRCutTransfer_BYAxis           /*AfterIRCutTRBY*/      = new ServoSimulUmac(axisNo++, "PROCAfterIRCutTransfer_BYAxis", 450, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            PROCBreakStage_AYAxis                   /*BreakTableAY*/        = new ServoSimulUmac(axisNo++, "PROCBreakStage_AYAxis", 1350, 1200, 18) { MotorType = EM_MOTOR_TYPE.Umac };
            PROCBreakStage_ASubX1Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_ASubX1Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_ASubX2Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_ASubX2Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_ASubY1Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_ASubY1Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_ASubY2Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_ASubY2Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_ASubT1Axis               /*Sub_BreakTableA1T*/   = new ServoSimulUmac(axisNo++, "PROCBreakStage_ASubT1Axis", 10, 12, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_ASubT2Axis               /*Sub_BreakTableA2T*/   = new ServoSimulUmac(axisNo++, "PROCBreakStage_ASubT2Axis", 10, 12, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_BYAxis                   /*BreakTableBY*/        = new ServoSimulUmac(axisNo++, "PROCBreakStage_BYAxis", 1350, 1200, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCBreakStage_BSubX1Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_BSubX1Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_BSubX2Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_BSubX2Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_BSubY1Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_BSubY1Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_BSubY2Axis               /**/                    = new ServoSimulUmac(axisNo++, "PROCBreakStage_BSubY2Axis", 10, 10, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_BSubT1Axis               /*Sub_BreakTableB1T*/   = new ServoSimulUmac(axisNo++, "PROCBreakStage_BSubT1Axis", 10, 12, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakStage_BSubT2Axis               /*Sub_BreakTableB2T*/   = new ServoSimulUmac(axisNo++, "PROCBreakStage_BSubT2Axis", 10, 12, 18) { MotorType = EM_MOTOR_TYPE.EZi };//
            PROCBreakingHeadX1Axis                  /*BreakingHead1X*/      = new ServoSimulUmac(axisNo++, "PROCBreakingHeadX1Axis", 800, 1000, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCBreakingHeadX2Axis                  /*BreakingHead2X*/      = new ServoSimulUmac(axisNo++, "PROCBreakingHeadX2Axis", 800, 1000, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCBreakingHeadZ1Axis                  /*BreakingHead1Z*/      = new ServoSimulUmac(axisNo++, "PROCBreakingHeadZ1Axis", 100, 1000, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCBreakingHeadZ2Axis                  /*BreakingHead2Z*/      = new ServoSimulUmac(axisNo++, "PROCBreakingHeadZ2Axis", 100, 1000, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCBreakAlignXAxis                     /*BreakAlignX*/         = new ServoSimulUmac(axisNo++, "PROCBreakAlignXAxis", 1100, 1000, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            PROCBreakAlignZAxis                     /*BreakingAlignZ*/      = new ServoSimulUmac(axisNo++, "PROCBreakAlignZAxis", 100, 500, 18) { MotorType = EM_MOTOR_TYPE.Umac };//
            UDBeforeInspUnloaderTransfer_AY1Axis    /*BeforeInspTRAY*/      = new ServoSimulUmac(axisNo++, "UDBeforeInspUnloaderTransfer_AY1Axis", 1400, 500, 18) { MotorType = EM_MOTOR_TYPE.Ajin };
            UDBeforeInspUnloaderTransfer_BY1Axis    /*BeforeInspTRBY*/      = new ServoSimulUmac(axisNo++, "UDBeforeInspUnloaderTransfer_BY1Axis", 1400, 500, 18) { MotorType = EM_MOTOR_TYPE.Ajin };
            UDAfterInspUnloaderTransfer_AY2Axis     /*AfterInspTRAY*/       = new ServoSimulUmac(axisNo++, "UDAfterInspUnloaderTransfer_AY2Axis", 1400, 1400, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDAfterInspUnloaderTransfer_AT1Axis     /*AfterInspTRA1T*/      = new ServoSimulUmac(axisNo++, "UDAfterInspUnloaderTransfer_AT1Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDAfterInspUnloaderTransfer_AT2Axis     /*AfterInspTRA2T*/      = new ServoSimulUmac(axisNo++, "UDAfterInspUnloaderTransfer_AT2Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDAfterInspUnloaderTransfer_BY2Axis     /*AfterInspTRBY*/       = new ServoSimulUmac(axisNo++, "UDAfterInspUnloaderTransfer_BY2Axis", 1400, 1400, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDAfterInspUnloaderTransfer_BT1Axis     /*AfterInspTRB1T*/      = new ServoSimulUmac(axisNo++, "UDAfterInspUnloaderTransfer_BT1Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };///
            UDAfterInspUnloaderTransfer_BT2Axis     /*AfterInspTRB2T*/      = new ServoSimulUmac(axisNo++, "UDAfterInspUnloaderTransfer_BT2Axis", 360, 600, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDInspCameraXAxis                       /*InspCameraX*/         = new ServoSimulUmac(axisNo++, "UDInspCameraXAxis", 1050, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDInspCameraZAxis                       /*InspCameraZ*/                    = new ServoSimulUmac(axisNo++, "UDInspCameraZAxis", 30, 100, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDInspectionStage_AYAxis                /*InspStageAY*/         = new ServoSimulUmac(axisNo++, "UDInspectionStage_AYAxis", 250, 500, 18) { MotorType = EM_MOTOR_TYPE.Ajin };
            UDInspectionStage_BYAxis                /*InspStageBY*/         = new ServoSimulUmac(axisNo++, "UDInspectionStage_BYAxis", 250, 500, 18) { MotorType = EM_MOTOR_TYPE.Ajin };
            UDUnloaderX1Axis                        /*UnloaderAX*/          = new ServoSimulUmac(axisNo++, "UDUnloaderX1Axis", 850, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDUnloaderX2Axis                        /*UnloaderBX*/          = new ServoSimulUmac(axisNo++, "UDUnloaderX2Axis", 850, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDUnloaderY1Axis                        /*UnloaderAY*/          = new ServoSimulUmac(axisNo++, "UDUnloaderY1Axis", 500, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDUnloaderY2Axis                        /*UnloaderBY*/          = new ServoSimulUmac(axisNo++, "UDUnloaderY2Axis", 500, 1000, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDCstUnloader_ACstRotationAxis          /*CstUnloaderARotation*/= new ServoSimulUmac(axisNo++, "UDCstUnloader_ACstRotationAxis", 250, 83, 18) { MotorType = EM_MOTOR_TYPE.Ajin };
            UDCstUnloader_ACstUpDownAxis            /*CstUnloaderAUpDown*/  = new ServoSimulUmac(axisNo++, "UDCstUnloader_ACstUpDownAxis", 400, 250, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//
            UDCstUnloader_BCstRotationAxis          /*CstUnloaderBRotation*/= new ServoSimulUmac(axisNo++, "UDCstUnloader_BCstRotationAxis", 250, 83, 18) { MotorType = EM_MOTOR_TYPE.Ajin };
            UDCstUnloader_BCstUpDownAxis            /*CstUnloaderBUpDown*/  = new ServoSimulUmac(axisNo++, "UDCstUnloader_BCstUpDownAxis", 400, 250, 18) { MotorType = EM_MOTOR_TYPE.Ajin };//

            #endregion


        }


        public void SetAddress()
        {
            #region 옛날꺼
            LdAreaLightCurtain.XB_OnOff	                            /**/	    = AddressMgr.GetAddress("LD_AREA_SEN_DETECT");
            UnldAreaLightCurtain.XB_OnOff	                            /**/	    = AddressMgr.GetAddress("ULD_AREA_SEN_DETECT");

            

            LoaderAOGamji.XB_OnOff /**/  = AddressMgr.GetAddress("#1-1_L/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK");
            LoaderAExist.XB_OnOff/**/  = AddressMgr.GetAddress("#1-2_L/D_CASSETTE_DETECT_SENSOR_FEEDBACK");

            LoaderBOGamji.XB_OnOff/**/= AddressMgr.GetAddress("#2-1_L/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK");
            LoaderBExist.XB_OnOff/**/= AddressMgr.GetAddress("#2-2_L/D_CASSETTE_DETECT_SENSOR_FEEDBACK");

            UnloaderAOGamji.XB_OnOff /**/  = AddressMgr.GetAddress("#1-1_UL/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK");
            UnloaderAExist.XB_OnOff/**/  = AddressMgr.GetAddress("#1-2_UL/D_CASSETTE_DETECT_SENSOR_FEEDBACK");

            UnloaderBOGamji.XB_OnOff/**/= AddressMgr.GetAddress("#2-1_UL/D_CASSETTE_INPUT_DETECT_SENSOR_FEEDBACK");
            UnloaderBExist.XB_OnOff/**/= AddressMgr.GetAddress("#2-2_UL/D_CASSETTE_DETECT_SENSOR_FEEDBACK");

            LdAButton.XB_OnOff = AddressMgr.GetAddress("L/D_A열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK");
            LdBButton.XB_OnOff = AddressMgr.GetAddress("L/D_B열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK");
            LdAMuting.XB_OnOff = AddressMgr.GetAddress("L/D_A열_투입/배출_MUTING_ON_FEEDBACK", 0);
            LdBMuting.XB_OnOff = AddressMgr.GetAddress("L/D_B열_투입/배출_MUTING_ON_FEEDBACK", 0);
            UDAButton.XB_OnOff = AddressMgr.GetAddress("UL/D_A열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK", 0);
            UDBButton.XB_OnOff = AddressMgr.GetAddress("UL/D_B열_투입/배출_MUTING_SWITCH_ON/OFF_FEEDBACK", 0);
            UDAMuting.XB_OnOff = AddressMgr.GetAddress("UL/D_A열_투입/배출_MUTING_ON_FEEDBACK", 0);
            UDBMuting.XB_OnOff = AddressMgr.GetAddress("UL/D_B열_투입/배출_MUTING_ON_FEEDBACK", 0);

            DOOR01_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#1_Door_Open");
            DOOR02_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#2_Door_Open");
            DOOR03_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#3_Door_Open");
            DOOR04_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#4_Door_Open");
            DOOR05_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#5_Door_Open");
            DOOR06_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#6_Door_Open");
            DOOR07_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#7_Door_Open");
            DOOR08_SENSOR.XB_OnOff	                                                /**/	= AddressMgr.GetAddress("#8_Door_Open");

            
            
            

            GLASS_DETECT_SENSOR_1.YB_OnOff = GLASS_DETECT_SENSOR_1.XB_OnOff                       /**/ = AddressMgr.GetAddress("LD_TR_Z_FILM_DETECT");
            GLASS_DETECT_SENSOR_2.YB_OnOff = GLASS_DETECT_SENSOR_2.XB_OnOff                       /**/ = AddressMgr.GetAddress("#1_LD_FILM_BOX_DETECT");
            GLASS_DETECT_SENSOR_3.YB_OnOff = GLASS_DETECT_SENSOR_3.XB_OnOff                       /**/ = AddressMgr.GetAddress("#2_LD_FILM_BOX_DETECT");
            GLASS_DETECT_SENSOR_4.YB_OnOff = GLASS_DETECT_SENSOR_4.XB_OnOff                       /**/ = AddressMgr.GetAddress("#1_ULD_FILM_BOX_DETECT");
            GLASS_DETECT_SENSOR_5.YB_OnOff = GLASS_DETECT_SENSOR_5.XB_OnOff                       /**/ = AddressMgr.GetAddress("#2_ULD_FILM_BOX_DETECT");
            GLASS_DETECT_SENSOR_6.YB_OnOff = GLASS_DETECT_SENSOR_6.XB_OnOff                       /**/ = AddressMgr.GetAddress("#1_Cut_STAGE_FILM_DETECT");
            GLASS_DETECT_SENSOR_7.YB_OnOff = GLASS_DETECT_SENSOR_7.XB_OnOff                       /**/ = AddressMgr.GetAddress("#2_Cut_STAGE_FILM_DETECT");
            GLASS_DETECT_SENSOR_8.YB_OnOff = GLASS_DETECT_SENSOR_8.XB_OnOff                          /**/ = AddressMgr.GetAddress("PRE-ALIGN_STAGE_FILM_DETECT");
            #endregion
            #region 기타
            
            LdAReset.XB_OnOff = AddressMgr.GetAddress("L/D_A열_RESET_SWITCH_FEEDBACK", 0);
            LdBReset.XB_OnOff = AddressMgr.GetAddress("L/D_B열_RESET_SWITCH_FEEDBACK", 0);
            
            UDAReset.XB_OnOff = AddressMgr.GetAddress("UL/D_A열_RESET_SWITCH_FEEDBACK", 0);
            UDBReset.XB_OnOff = AddressMgr.GetAddress("UL/D_B열_RESET_SWITCH_FEEDBACK", 0);
            #endregion

            #region 실린더
            #endregion
            #region Loader            
            CstLoaderA_CstGrip_Cylinder.XB_ForwardComplete             /**/= AddressMgr.GetAddress("#1-1_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK");
            CstLoaderA_CstGrip_Cylinder.XB_BackwardComplete            /**/= AddressMgr.GetAddress("#1-1_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderA_CstGrip_Cylinder_.XB_ForwardComplete            /**/= AddressMgr.GetAddress("#1-2_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderA_CstGrip_Cylinder_.XB_BackwardComplete           /**/= AddressMgr.GetAddress("#1-2_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderB_CstGrip_Cylinder.XB_ForwardComplete             /**/= AddressMgr.GetAddress("#2-1_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderB_CstGrip_Cylinder.XB_BackwardComplete            /**/= AddressMgr.GetAddress("#2-1_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderB_CstGrip_Cylinder_.XB_ForwardComplete            /**/= AddressMgr.GetAddress("#2-2_L/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderB_CstGrip_Cylinder_.XB_BackwardComplete           /**/= AddressMgr.GetAddress("#2-2_L/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);

            LoaderTRA1_UpDown_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LoaderTRA1_UpDown_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            LoaderTRA2_UpDown_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LoaderTRA2_UpDown_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            LoaderTRB1_UpDown_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LoaderTRB1_UpDown_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            LoaderTRB2_UpDown_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            LoaderTRB2_UpDown_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            CstLoaderA_Tilt_Cylinder.XB_BackwardComplete               /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderA_Tilt_Cylinder.XB_ForwardComplete                /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderB_Tilt_Cylinder.XB_BackwardComplete               /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstLoaderB_Tilt_Cylinder.XB_ForwardComplete                /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            CstLoaderA_CstGrip_Cylinder.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_GRIP_CMD", 0);
            CstLoaderA_CstGrip_Cylinder.YB_BackwardCmd                 /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_UNGRIP_CMD", 0);
            CstLoaderB_CstGrip_Cylinder.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_GRIP_CMD", 0);
            CstLoaderB_CstGrip_Cylinder.YB_BackwardCmd                 /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_UNGRIP_CMD", 0);
            CstLoaderA_CstGrip_Cylinder_.YB_ForwardCmd                 /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_GRIP_CMD", 0);
            CstLoaderA_CstGrip_Cylinder_.YB_BackwardCmd                /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_UNGRIP_CMD", 0);
            CstLoaderB_CstGrip_Cylinder_.YB_ForwardCmd                 /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_GRIP_CMD", 0);
            CstLoaderB_CstGrip_Cylinder_.YB_BackwardCmd                /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_UNGRIP_CMD", 0);

            LoaderTRA1_UpDown_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_UP_CMD", 0);
            LoaderTRA1_UpDown_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_DOWN_CMD", 0);
            LoaderTRA2_UpDown_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_UP_CMD", 0);
            LoaderTRA2_UpDown_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_DOWN_CMD", 0);
            LoaderTRB1_UpDown_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_UP_CMD", 0);
            LoaderTRB1_UpDown_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_DOWN_CMD", 0);
            LoaderTRB2_UpDown_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_UP_CMD", 0);
            LoaderTRB2_UpDown_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_DOWN_CMD", 0);

            CstLoaderA_Tilt_Cylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            CstLoaderA_Tilt_Cylinder.YB_ForwardCmd                    /**/= AddressMgr.GetAddress("#1_L/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);
            CstLoaderB_Tilt_Cylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            CstLoaderB_Tilt_Cylinder.YB_ForwardCmd                    /**/= AddressMgr.GetAddress("#2_L/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);
            #endregion

            #region Proc            
            LaserHead2_UpDown_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("LASER_HEAD_UP_CYLINDER_SENSOR_FEEDBACK#1", 0);
            LaserHead2_UpDown_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("LASER_HEAD_DOWN_CYLINDER_SENSOR_FEEDBACK#1", 0);
            LaserHead1_UpDown_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("LASER_HEAD_UP_CYLINDER_SENSOR_FEEDBACK#2", 0);
            LaserHead1_UpDown_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("LASER_HEAD_DOWN_CYLINDER_SENSOR_FEEDBACK#2", 0);

            LaserHead_Shutter_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("LASER_SHUTTER_OPEN_CYLINDER_SENSOR_FEEDBACK", 0);
            LaserHead_Shutter_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("LASER_SHUTTER_CLOSE_CYLINDER_SENSOR_FEEDBACK", 0);

            IRCutStageA_BrushUpDown_Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#1_가공_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            IRCutStageA_BrushUpDown_Cylinder.XB_ForwardComplete       /**/= AddressMgr.GetAddress("#1_가공_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            IRCutStageB_BrushUpDown_Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("#2_가공_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            IRCutStageB_BrushUpDown_Cylinder.XB_ForwardComplete       /**/= AddressMgr.GetAddress("#2_가공_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            AfterIRCutTRA1_UpDown_Cylinder.XB_BackwardComplete         /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterIRCutTRA1_UpDown_Cylinder.XB_ForwardComplete          /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterIRCutTRA2_UpDown_Cylinder.XB_BackwardComplete         /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterIRCutTRA2_UpDown_Cylinder.XB_ForwardComplete          /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterIRCutTRB1_UpDown_Cylinder.XB_BackwardComplete         /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterIRCutTRB1_UpDown_Cylinder.XB_ForwardComplete          /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterIRCutTRB2_UpDown_Cylinder.XB_BackwardComplete         /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterIRCutTRB2_UpDown_Cylinder.XB_ForwardComplete          /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            BreakStageA_BrushUpDown_Cylinder.XB_BackwardComplete       /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            BreakStageA_BrushUpDown_Cylinder.XB_ForwardComplete        /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            BreakStageB_BrushUpDown_Cylinder.XB_BackwardComplete       /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            BreakStageB_BrushUpDown_Cylinder.XB_ForwardComplete        /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            LaserHead2_DummyShutter_Cylinder.XB_BackwardComplete       /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_OPEN_CYLINDER_SENSOR_FEEDBACK", 0);
            LaserHead2_DummyShutter_Cylinder.XB_ForwardComplete        /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_CLOSE_CYLINDER_SENSOR_FEEDBACK", 0);
            LaserHead1_DummyShutter_Cylinder.XB_BackwardComplete       /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_OPEN_CYLINDER_SENSOR_FEEDBACK", 0);
            LaserHead1_DummyShutter_Cylinder.XB_ForwardComplete        /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_CLOSE_CYLINDER_SENSOR_FEEDBACK", 0);

            LaserHead_DummyTank_Cylinder.XB_BackwardComplete           /**/= AddressMgr.GetAddress("DUMMY_TANK_투입_CYLINDER_SENSOR_FEEDBACK", 0);
            LaserHead_DummyTank_Cylinder.XB_ForwardComplete            /**/= AddressMgr.GetAddress("DUMMY_TANK_배출_CYLINDER_SENSOR_FEEDBACK", 0);

            LaserHead2_UpDown_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("LASER_HEAD_UP_CYLINDER_CMD", 0);
            LaserHead2_UpDown_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("LASER_HEAD_DOWN_CYLINDER_CMD", 0);
            LaserHead1_UpDown_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("LASER_HEAD_UP_CYLINDER_CMD", 0);
            LaserHead1_UpDown_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("LASER_HEAD_DOWN_CYLINDER_CMD", 0);

            LaserHead_Shutter_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("LASER_SHUTTER_OPEN_CMD", 0);
            LaserHead_Shutter_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("LASER_SHUTTER_CLOSE_CMD", 0);

            IRCutStageA_BrushUpDown_Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_BRUSH_UP_CMD", 0);
            IRCutStageA_BrushUpDown_Cylinder.YB_ForwardCmd            /**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_BRUSH_DOWN_CMD", 0);
            IRCutStageB_BrushUpDown_Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_BRUSH_UP_CMD", 0);
            IRCutStageB_BrushUpDown_Cylinder.YB_ForwardCmd            /**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_BRUSH_DOWN_CMD", 0);

            AfterIRCutTRA1_UpDown_Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            AfterIRCutTRA1_UpDown_Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_DOWN_CMD", 0);
            AfterIRCutTRA2_UpDown_Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            AfterIRCutTRA2_UpDown_Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_DOWN_CMD", 0);

            AfterIRCutTRB1_UpDown_Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            AfterIRCutTRB1_UpDown_Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_DOWN_CMD", 0);
            AfterIRCutTRB2_UpDown_Cylinder.YB_BackwardCmd              /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_UP_CMD", 0);
            AfterIRCutTRB2_UpDown_Cylinder.YB_ForwardCmd               /**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_DOWN_CM", 0);

            BreakStageA_BrushUpDown_Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_UP_CMD", 0);
            BreakStageA_BrushUpDown_Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_BRUSH_DOWN_CMD", 0);
            BreakStageB_BrushUpDown_Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_UP_CMD", 0);
            BreakStageB_BrushUpDown_Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_BRUSH_DOWN_CMD", 0);

            LaserHead2_DummyShutter_Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_OPEN_CMD", 0);
            LaserHead2_DummyShutter_Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#1_DUMMY_SHUTTER_CLOSE_CMD", 0);
            LaserHead1_DummyShutter_Cylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_OPEN_CMD", 0);
            LaserHead1_DummyShutter_Cylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("#2_DUMMY_SHUTTER_CLOSE_CMD", 0);

            LaserHead_DummyTank_Cylinder.YB_BackwardCmd                /**/= AddressMgr.GetAddress("DUMMY_TANK_투입_CMD", 0);
            LaserHead_DummyTank_Cylinder.YB_ForwardCmd                 /**/= AddressMgr.GetAddress("DUMMY_TANK_배출_CMD", 0);
            #endregion

            #region Unld            
            Buffer_UpDown_Cylinder.XB_ForwardComplete                  /**/= AddressMgr.GetAddress("BUFFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            Buffer_UpDown_Cylinder.XB_BackwardComplete                 /**/= AddressMgr.GetAddress("BUFFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            CstUnloaderA_Grip_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#1-1_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderA_Grip_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#1-1_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderA_Grip_Cylinder_.XB_ForwardComplete             /**/= AddressMgr.GetAddress("#1-2_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderA_Grip_Cylinder_.XB_BackwardComplete            /**/= AddressMgr.GetAddress("#1-2_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderB_Grip_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#2-1_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderB_Grip_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#2-1_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderB_Grip_Cylinder_.XB_ForwardComplete             /**/= AddressMgr.GetAddress("#2-2_UL/D_CASSETTE_GRIP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderB_Grip_Cylinder_.XB_BackwardComplete            /**/= AddressMgr.GetAddress("#2-2_UL/D_CASSETTE_UNGRIP_CYLINDER_SENSOR_FEEDBACK", 0);

            BeforeInspUnloaderTRA_UpDonw_Cylinder.XB_BackwardComplete  /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            BeforeInspUnloaderTRA_UpDonw_Cylinder.XB_ForwardComplete   /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            BeforeInspUnloaderTRA_UpDonw_Cylinder_.XB_BackwardComplete  /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            BeforeInspUnloaderTRA_UpDonw_Cylinder_.XB_ForwardComplete   /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            BeforeInspUnloaderTRB_UpDonw_Cylinder.XB_BackwardComplete  /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            BeforeInspUnloaderTRB_UpDonw_Cylinder.XB_ForwardComplete   /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            BeforeInspUnloaderTRB_UpDonw_Cylinder_.XB_BackwardComplete  /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            BeforeInspUnloaderTRB_UpDonw_Cylinder_.XB_ForwardComplete   /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            CstUnloaderA_Tilt_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderA_Tilt_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderB_Tilt_Cylinder.XB_BackwardComplete             /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_TILTING_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            CstUnloaderB_Tilt_Cylinder.XB_ForwardComplete              /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_TILTING_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            AfterInspUnloaderTRA_UpDonw_Cylinder.XB_BackwardComplete   /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterInspUnloaderTRA_UpDonw_Cylinder.XB_ForwardComplete    /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterInspUnloaderTRA_UpDonw_Cylinder_.XB_BackwardComplete   /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterInspUnloaderTRA_UpDonw_Cylinder_.XB_ForwardComplete    /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            AfterInspUnloaderTRB_UpDonw_Cylinder.XB_BackwardComplete   /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterInspUnloaderTRB_UpDonw_Cylinder.XB_ForwardComplete    /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterInspUnloaderTRB_UpDonw_Cylinder_.XB_BackwardComplete   /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_UP_CYLINDER_SENSOR_FEEDBACK", 0);
            AfterInspUnloaderTRB_UpDonw_Cylinder_.XB_ForwardComplete    /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_DOWN_CYLINDER_SENSOR_FEEDBACK", 0);

            Buffer_UpDown_Cylinder.YB_ForwardCmd                       /**/= AddressMgr.GetAddress("BUFFER_UP_CMD", 0);
            Buffer_UpDown_Cylinder.YB_BackwardCmd                      /**/= AddressMgr.GetAddress("BUFFER_DOWN_CMD", 0);

            CstUnloaderA_Grip_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_GRIP_CMD", 0);
            CstUnloaderA_Grip_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_UNGRIP_CMD", 0);
            CstUnloaderB_Grip_Cylinder.YB_ForwardCmd                   /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_GRIP_CMD", 0);
            CstUnloaderB_Grip_Cylinder.YB_BackwardCmd                  /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_UNGRIP_CMD", 0);

            CstUnloaderA_Grip_Cylinder_.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_GRIP_CMD", 0);
            CstUnloaderA_Grip_Cylinder_.YB_BackwardCmd                 /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_UNGRIP_CMD", 0);
            CstUnloaderB_Grip_Cylinder_.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_GRIP_CMD", 0);
            CstUnloaderB_Grip_Cylinder_.YB_BackwardCmd                 /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_UNGRIP_CMD", 0);

            BeforeInspUnloaderTRA_UpDonw_Cylinder.YB_BackwardCmd       /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_UP_CMD", 0);
            BeforeInspUnloaderTRA_UpDonw_Cylinder.YB_ForwardCmd        /**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_DOWN_CMD", 0);
            BeforeInspUnloaderTRA_UpDonw_Cylinder_.YB_BackwardCmd       /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_UP_CMD", 0);
            BeforeInspUnloaderTRA_UpDonw_Cylinder_.YB_ForwardCmd        /**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_DOWN_CMD", 0);

            BeforeInspUnloaderTRB_UpDonw_Cylinder.YB_BackwardCmd       /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_UP_CMD", 0);
            BeforeInspUnloaderTRB_UpDonw_Cylinder.YB_ForwardCmd        /**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_DOWN_CMD", 0);
            BeforeInspUnloaderTRB_UpDonw_Cylinder_.YB_BackwardCmd       /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_UP_CMD", 0);
            BeforeInspUnloaderTRB_UpDonw_Cylinder_.YB_ForwardCmd        /**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_DOWN_CMD", 0);

            AfterInspUnloaderTRA_UpDonw_Cylinder.YB_BackwardCmd        /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_UP_CMD", 0);
            AfterInspUnloaderTRA_UpDonw_Cylinder.YB_ForwardCmd         /**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_DOWN_CMD", 0);
            AfterInspUnloaderTRA_UpDonw_Cylinder_.YB_BackwardCmd        /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_UP_CMD", 0);
            AfterInspUnloaderTRA_UpDonw_Cylinder_.YB_ForwardCmd         /**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_DOWN_CMD", 0);

            AfterInspUnloaderTRB_UpDonw_Cylinder.YB_BackwardCmd        /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_UP_CMD", 0);
            AfterInspUnloaderTRB_UpDonw_Cylinder.YB_ForwardCmd         /**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_DOWN_CMD", 0);
            AfterInspUnloaderTRB_UpDonw_Cylinder_.YB_BackwardCmd        /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_UP_CMD", 0);
            AfterInspUnloaderTRB_UpDonw_Cylinder_.YB_ForwardCmd         /**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_DOWN_CMD", 0);

            CstUnloaderA_Tilt_Cylinder.YB_BackwardCmd                   /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            CstUnloaderA_Tilt_Cylinder.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#1_UL/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);

            CstUnloaderB_Tilt_Cylinder.YB_BackwardCmd                   /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_LIFTER_TILTING_UP_CMD", 0);
            CstUnloaderB_Tilt_Cylinder.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("#2_UL/D_CASSETTE_LIFTER_TILTING_DOWN_CMD", 0);
            #endregion

            #region vac
            Blower01.YB_OnOff/**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_파기_AIR_ON_CMD", 0);
            Blower02.YB_OnOff/**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_파기_AIR_ON_CMD", 0);
            Blower03.YB_OnOff/**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower04.YB_OnOff/**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower05.YB_OnOff/**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower06.YB_OnOff/**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower07.YB_OnOff/**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            Blower08.YB_OnOff/**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);
            Blower09.YB_OnOff/**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            Blower10.YB_OnOff/**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);
            Blower11.YB_OnOff/**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            Blower12.YB_OnOff/**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);
            Blower13.YB_OnOff/**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_1CH_파기_AIR_ON_CMD", 0);
            Blower14.YB_OnOff/**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_2CH_파기_AIR_ON_CMD", 0);
            Blower15.YB_OnOff/**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower16.YB_OnOff/**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower17.YB_OnOff/**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower18.YB_OnOff/**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_파기_AIR_ON_CMD", 0);

            Blower19.YB_OnOff/**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_파기_AIR_ON_CMD", 0);
            Blower20.YB_OnOff/**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_파기_AIR_ON_CMD", 0);
            Blower21.YB_OnOff/**/= AddressMgr.GetAddress("#3_BREAKING_STAGE_파기_AIR_ON_CMD", 0);
            Blower22.YB_OnOff/**/= AddressMgr.GetAddress("#4_BREAKING_STAGE_파기_AIR_ON_CMD", 0);
            Blower23.YB_OnOff/**/= AddressMgr.GetAddress("#1_CELL_투입_HAND_파기_AIR_ON_CMD", 0);
            Blower24.YB_OnOff/**/= AddressMgr.GetAddress("#2_CELL_투입_HAND_파기_AIR_ON_CMD", 0);
            Blower25.YB_OnOff/**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower26.YB_OnOff/**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower27.YB_OnOff/**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower28.YB_OnOff/**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower29.YB_OnOff/**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower30.YB_OnOff/**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower31.YB_OnOff/**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower32.YB_OnOff/**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_파기_AIR_ON_CMD", 0);
            Blower33.YB_OnOff/**/= AddressMgr.GetAddress("#1_INSPECTION_STAGE_파기_AIR_ON_CMD", 0);
            Blower34.YB_OnOff/**/= AddressMgr.GetAddress("#2_INSPECTION_STAGE_파기_AIR_ON_CMD", 0);
            Blower35.YB_OnOff/**/= AddressMgr.GetAddress("#3_INSPECTION_STAGE_파기_AIR_ON_CMD", 0);
            Blower36.YB_OnOff/**/= AddressMgr.GetAddress("#4_INSPECTION_STAGE_파기_AIR_ON_CMD", 0);
            Blower37.YB_OnOff/**/= AddressMgr.GetAddress("BUFFER_1CH_파기_AIR_ON_CMD", 0);
            Blower38.YB_OnOff/**/= AddressMgr.GetAddress("DLM_BLOWER_ON/OFF_CMD", 0);

            Vaccum01.YB_OnOff/**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_VACUUM_ON_CMD", 0);
            Vaccum02.YB_OnOff/**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_VACUUM_ON_CMD", 0);
            Vaccum03.YB_OnOff/**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum04.YB_OnOff/**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum05.YB_OnOff/**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum06.YB_OnOff/**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum07.YB_OnOff/**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            Vaccum08.YB_OnOff/**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            Vaccum09.YB_OnOff/**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            Vaccum10.YB_OnOff/**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            Vaccum11.YB_OnOff/**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            Vaccum12.YB_OnOff/**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            Vaccum13.YB_OnOff/**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_1CH_VACUUM_ON_CMD", 0);
            Vaccum14.YB_OnOff/**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_2CH_VACUUM_ON_CMD", 0);
            Vaccum15.YB_OnOff/**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum16.YB_OnOff/**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum17.YB_OnOff/**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum18.YB_OnOff/**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_VACUUM_ON_CMD", 0);

            Vaccum19.YB_OnOff/**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            Vaccum20.YB_OnOff/**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            Vaccum21.YB_OnOff/**/= AddressMgr.GetAddress("#3_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            Vaccum22.YB_OnOff/**/= AddressMgr.GetAddress("#4_BREAKING_STAGE_VACUUM_ON_CMD", 0);
            Vaccum23.YB_OnOff/**/= AddressMgr.GetAddress("#1_CELL_투입_HAND_VACUUM_ON_CMD", 0);
            Vaccum24.YB_OnOff/**/= AddressMgr.GetAddress("#2_CELL_투입_HAND_VACUUM_ON_CMD", 0);
            Vaccum25.YB_OnOff/**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum26.YB_OnOff/**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum27.YB_OnOff/**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum28.YB_OnOff/**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum29.YB_OnOff/**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum30.YB_OnOff/**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum31.YB_OnOff/**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum32.YB_OnOff/**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_VACUUM_ON_CMD", 0);
            Vaccum33.YB_OnOff/**/= AddressMgr.GetAddress("#1_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            Vaccum34.YB_OnOff/**/= AddressMgr.GetAddress("#2_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            Vaccum35.YB_OnOff/**/= AddressMgr.GetAddress("#3_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            Vaccum36.YB_OnOff/**/= AddressMgr.GetAddress("#4_INSPECTION_STAGE_VACUUM_ON_CMD", 0);
            Vaccum37.YB_OnOff/**/= AddressMgr.GetAddress("BUFFER_1CH_VACUUM_ON_CMD", 0);
            Vaccum38.YB_OnOff/**/= AddressMgr.GetAddress("DLM_BLOWER_ON/OFF_CMD", 0);
            //////////////////////////////////////////////////////////////////////////
            Vaccum01.XB_OnOff/**/= AddressMgr.GetAddress("#1_CELL_취출_HAND_PRESSURE_SW_FEEDBACK", 0);
            Vaccum02.XB_OnOff/**/= AddressMgr.GetAddress("#2_CELL_취출_HAND_PRESSURE_SW_FEEDBACK", 0);
            Vaccum03.XB_OnOff/**/= AddressMgr.GetAddress("#1_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum04.XB_OnOff/**/= AddressMgr.GetAddress("#2_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum05.XB_OnOff/**/= AddressMgr.GetAddress("#3_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum06.XB_OnOff/**/= AddressMgr.GetAddress("#4_L/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum07.XB_OnOff/**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum08.XB_OnOff/**/= AddressMgr.GetAddress("#1_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum09.XB_OnOff/**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum10.XB_OnOff/**/= AddressMgr.GetAddress("#2_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum11.XB_OnOff/**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum12.XB_OnOff/**/= AddressMgr.GetAddress("#3_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum13.XB_OnOff/**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_1CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum14.XB_OnOff/**/= AddressMgr.GetAddress("#4_CUTTING_STAGE_2CH_PRESSURE_SW_FEEDBACK", 0);
            Vaccum15.XB_OnOff/**/= AddressMgr.GetAddress("#1_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);
            Vaccum16.XB_OnOff/**/= AddressMgr.GetAddress("#2_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);
            Vaccum17.XB_OnOff/**/= AddressMgr.GetAddress("#3_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);
            Vaccum18.XB_OnOff/**/= AddressMgr.GetAddress("#4_AFTER_IR_CUT_TRANSFER_PRESSURE_SW_FEEDBACK", 0);

            Vaccum19.XB_OnOff/**/= AddressMgr.GetAddress("#1_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum20.XB_OnOff/**/= AddressMgr.GetAddress("#2_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum21.XB_OnOff/**/= AddressMgr.GetAddress("#3_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum22.XB_OnOff/**/= AddressMgr.GetAddress("#4_BREAKING_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum23.XB_OnOff/**/= AddressMgr.GetAddress("#1_CELL_투입_HAND_PRESSURE_SW_FEEDBACK", 0);
            Vaccum24.XB_OnOff/**/= AddressMgr.GetAddress("#2_CELL_투입_HAND_PRESSURE_SW_FEEDBACK", 0);
            Vaccum25.XB_OnOff/**/= AddressMgr.GetAddress("#1_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum26.XB_OnOff/**/= AddressMgr.GetAddress("#2_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum27.XB_OnOff/**/= AddressMgr.GetAddress("#3_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum28.XB_OnOff/**/= AddressMgr.GetAddress("#4_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum29.XB_OnOff/**/= AddressMgr.GetAddress("#5_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum30.XB_OnOff/**/= AddressMgr.GetAddress("#6_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum31.XB_OnOff/**/= AddressMgr.GetAddress("#7_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum32.XB_OnOff/**/= AddressMgr.GetAddress("#8_UL/D_TRANSFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);

            Vaccum33.XB_OnOff/**/= AddressMgr.GetAddress("#1_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum34.XB_OnOff/**/= AddressMgr.GetAddress("#2_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum35.XB_OnOff/**/= AddressMgr.GetAddress("#3_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum36.XB_OnOff/**/= AddressMgr.GetAddress("#4_INSPECTION_STAGE_PRESSURE_SW_FEEDBACK", 0);
            Vaccum37.XB_OnOff/**/= AddressMgr.GetAddress("BUFFER_VACUUM_PRESSURE_SW_FEEDBACK", 0);
            Vaccum38.XB_OnOff/**/= AddressMgr.GetAddress("DLM_BLOWER_PRESSURE_SW_FEEDBACK", 0);
#endregion
            #region PMAC
            Motors = new ServoSimulUmac[] 
                        {                 
                LDCstLoader_ACstRotationAxis                ,
                LDCstLoader_ACstUpDownAxis                  ,
                LDCstLoader_BCstRotationAxis                ,
                LDCstLoader_BCstUpDownAxis                  ,
                LDLoaderX1Axis                              ,
                LDLoaderX2Axis                              ,
                LDLoaderY1Axis                              ,
                LDLoaderY2Axis                              ,
                LDLoaderTransfer_AYAxis                     ,
                LDLoaderTransfer_AX1Axis                    ,
                LDLoaderTransfer_AX2Axis                    ,
                LDLoaderTransfer_ASubY1Axis                 ,
                LDLoaderTransfer_ASubY2Axis                 ,
                LDLoaderTransfer_ASubT1Axis                 ,
                LDLoaderTransfer_ASubT2Axis                 ,
                LDLoaderTransfer_BYAxis                     ,
                LDLoaderTransfer_BX1Axis                    ,
                LDLoaderTransfer_BX2Axis                    ,
                LDLoaderTransfer_BSubY1Axis                 ,
                LDLoaderTransfer_BSubY2Axis                 ,
                LDLoaderTransfer_BSubT1Axis                 ,
                LDLoaderTransfer_BSubT2Axis                 ,
                LDPreAlignXAxis                             ,
                PROCFineAlginXAxis                          ,
                PROCIRCutStage_AYAxis                       ,
                PROCIRCutStage_BYAxis                       ,
                PROCLaserHeadXAxis                          ,
                PROCLaserHeadZAxis                          ,
                PROCAfterIRCutTransfer_AYAxis               ,
                PROCAfterIRCutTransfer_BYAxis               ,
                PROCBreakStage_AYAxis                       ,
                PROCBreakStage_ASubX1Axis                   ,
                PROCBreakStage_ASubX2Axis                   ,
                PROCBreakStage_ASubY1Axis                   ,
                PROCBreakStage_ASubY2Axis                   ,
                PROCBreakStage_ASubT1Axis                   ,
                PROCBreakStage_ASubT2Axis                   ,
                PROCBreakStage_BYAxis                       ,
                PROCBreakStage_BSubX1Axis                   ,
                PROCBreakStage_BSubX2Axis                   ,
                PROCBreakStage_BSubY1Axis                   ,
                PROCBreakStage_BSubY2Axis                   ,
                PROCBreakStage_BSubT1Axis                   ,
                PROCBreakStage_BSubT2Axis                   ,
                PROCBreakingHeadX1Axis                      ,
                PROCBreakingHeadX2Axis                      ,
                PROCBreakingHeadZ1Axis                      ,
                PROCBreakingHeadZ2Axis                      ,
                PROCBreakAlignXAxis                         ,
                PROCBreakAlignZAxis                         ,
                UDBeforeInspUnloaderTransfer_AY1Axis        ,
                UDBeforeInspUnloaderTransfer_BY1Axis        ,
                UDAfterInspUnloaderTransfer_AY2Axis         ,
                UDAfterInspUnloaderTransfer_AT1Axis         ,
                UDAfterInspUnloaderTransfer_AT2Axis         ,
                UDAfterInspUnloaderTransfer_BY2Axis         ,
                UDAfterInspUnloaderTransfer_BT1Axis         ,
                UDAfterInspUnloaderTransfer_BT2Axis         ,
                UDInspCameraXAxis                           ,
                UDInspCameraZAxis                           ,
                UDInspectionStage_AYAxis                    ,
                UDInspectionStage_BYAxis                    ,
                UDUnloaderX1Axis                            ,
                UDUnloaderX2Axis                            ,
                UDUnloaderY1Axis                            ,
                UDUnloaderY2Axis                            ,
                UDCstUnloader_ACstRotationAxis              ,
                UDCstUnloader_ACstUpDownAxis                ,
                UDCstUnloader_BCstRotationAxis              ,
                UDCstUnloader_BCstUpDownAxis                ,
            };
            



            for (int jPos = 0; jPos < Motors.Length; jPos++)
            {
                //string motor = motors[jPos].Name;
                //string slayerMotor = motors[jPos].SlayerName;

                string axisStr = Motors[jPos].InnerAxisNo.ToString("0#");
                string motorType = Motors[jPos].MotorType.ToString();

                Motors[jPos].XB_StatusHomeCompleteBit                       /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_StatusHomeCompleteBit", axisStr, motorType), 0);
                Motors[jPos].XB_StatusHomeInPosition                        /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_StatusHomeInPosition", axisStr, motorType), 0);
                Motors[jPos].XB_StatusMotorMoving                           /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_StatusMotorMoving", axisStr, motorType), 0);
                Motors[jPos].XB_StatusMotorInPosition                       /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_StatusMotorInPosition", axisStr, motorType), 0);
                Motors[jPos].XB_StatusNegativeLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_StatusNegativeLimitSet", axisStr, motorType), 0);
                Motors[jPos].XB_StatusPositiveLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_StatusPositiveLimitSet", axisStr, motorType), 0);
                Motors[jPos].XB_StatusMotorServoOn                          /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_ErrMotorServoOn", axisStr, motorType), 0);
                Motors[jPos].XB_ErrFatalFollowingError                      /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_ErrFatalFollowingError", axisStr, motorType), 0);
                Motors[jPos].XB_ErrAmpFaultError                            /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_ErrAmpFaultError", axisStr, motorType), 0);
                Motors[jPos].XB_ErrI2TAmpFaultError                         /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_ErrI2TAmpFaultError", axisStr, motorType), 0);
                Motors[jPos].XF_CurrMotorPosition                           /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XF_CurrentMotorPosition", axisStr, motorType), 0);
                Motors[jPos].XF_CurrMotorSpeed                              /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XF_CurrentMotorSpeed", axisStr, motorType), 0);
                Motors[jPos].XF_CurrMotorActualLoad                         /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XI_CurrentMotorActualLoad", axisStr, motorType), 0);
                Motors[jPos].YB_HomeCmd                                     /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_HomeCmd", axisStr, motorType), 0);
                Motors[jPos].XB_HomeCmdAck                                  /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_HomeCmdAck", axisStr, motorType), 0);
                Motors[jPos].YB_MotorStopCmd                                /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_MotorStopCmd", axisStr, motorType), 0);
                Motors[jPos].XB_MotorStopCmdAck                             /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_MotorStopCmdAck", axisStr, motorType), 0);
                Motors[jPos].YB_ServoOnOffCmd                               /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_ServoOnOffCmd", axisStr, motorType), 0);
                Motors[jPos].XB_ServoOnOffCmdAck                            /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_ServoOnOffCmdAck", axisStr, motorType), 0);
                Motors[jPos].YB_ServoOnOff                                  /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_ServoOnOff", axisStr, motorType), 0);
                Motors[jPos].YB_MotorJogMinusMove                           /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_MinusJogCmd", axisStr, motorType), 0);
                Motors[jPos].YB_MotorJogPlusMove                            /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_PlusJogCmd", axisStr, motorType), 0);
                Motors[jPos].YF_MotorJogSpeedCmd                            /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YF_JogSpeed", axisStr, motorType), 0);
                Motors[jPos].XF_MotorJogSpeedCmdAck                         /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XF_JogSpeedAck", axisStr, motorType), 0);

                Motors[jPos].YB_PTPMoveCmd                                  /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_PTPMoveCmd", axisStr, motorType), 0);
                Motors[jPos].XB_PTPMoveCmdAck                               /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_PTPMoveCmdAck", axisStr, motorType), 0);
                //Motors[jPos].YB_PTPMoveCmdCancel                            /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YB_PTPMoveCmdCancel", axisStr, motorType), 0);
                Motors[jPos].YF_PTPMoveSpeed                                /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YF_PTPMoveSpeed", axisStr, motorType), 0);
                Motors[jPos].XF_PTPMoveSpeedAck                             /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XF_PTPMoveSpeedAck", axisStr, motorType), 0);
                Motors[jPos].YF_PTPMovePosition                             /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YF_PTPMovePosition", axisStr, motorType), 0);
                Motors[jPos].XF_PTPMovePositionAck                          /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XF_PTPMovePositionAck", axisStr, motorType), 0);
                Motors[jPos].YF_PTPMoveAccel                                /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_YF_PTPMoveAccel", axisStr, motorType), 0);
                Motors[jPos].XF_PTPMoveAccelAck                             /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XF_PTPMoveAccelAck", axisStr, motorType), 0);
                //Motors[jPos].XB_PTPMoveComplete                             /**/  = AddressMgr.GetAddress(string.Format("{1}_Axis{0}_XB_PTPMoveComplete", axisStr, motorType), 0);
            }
            #endregion

            //검사기 ADDRESS
            //S0.0
            InspPc.YB_ControlAlive	        /**/ = AddressMgr.GetAddress("C2A_CONTROL_ALIVE", 0);
            InspPc.YB_RunningMode	        /**/ = AddressMgr.GetAddress("C2A_RUNNING_MODE", 0);
            InspPc.YB_ReviewMode	        /**/ = AddressMgr.GetAddress("C2A_REVIEW_MODE", 0);
            InspPc.YB_ReviewManualMove	    /**/ = AddressMgr.GetAddress("C2A_REVIEW_MANUAL_MOVE", 0);
            InspPc.YB_BypassMode	        /**/ = AddressMgr.GetAddress("C2A_BYPASS_MODE", 0);
            InspPc.YB_VaripanelMode	        /**/ = AddressMgr.GetAddress("C2A_VARIPANEL_MODE", 0);
            InspPc.YB_DualMode	            /**/ = AddressMgr.GetAddress("C2A_DUAL_MODE", 0);
            InspPc.YB_PpidUseMode	        /**/ = AddressMgr.GetAddress("C2A_PPID_USE_MODE", 0);

            //S10.0
            InspPc.YB_Loading	            /**/ = AddressMgr.GetAddress("C2I_LOADING", 0);
            InspPc.YB_ScanReady	            /**/ = AddressMgr.GetAddress("C2I_SCAN_READY", 0);
            InspPc.YB_ScanStart	            /**/ = AddressMgr.GetAddress("C2I_SCAN_START", 0);
            InspPc.YB_ScanEnd	            /**/ = AddressMgr.GetAddress("C2I_SCAN_END", 0);
            InspPc.YB_Unloading	            /**/ = AddressMgr.GetAddress("C2I_UNLOADING", 0);
            InspPc.YB_InspectionEnd	        /**/ = AddressMgr.GetAddress("C2I_INSPECTION_END", 0);
            InspPc.YB_LotStart	            /**/ = AddressMgr.GetAddress("C2I_LOT_START", 0);
            InspPc.YB_LotEnd	            /**/ = AddressMgr.GetAddress("C2I_LOT_END", 0);
            InspPc.YB_Z1HomePos	            /**/ = AddressMgr.GetAddress("C2I_Z1_HOME_POS", 0);
            InspPc.YB_Z2HomePos	            /**/ = AddressMgr.GetAddress("C2I_Z2_HOME_POS", 0);
            InspPc.YB_Z1ZeroPos	            /**/ = AddressMgr.GetAddress("C2I_Z1_ZERO_POS", 0);
            InspPc.YB_Z2ZeroPos	            /**/ = AddressMgr.GetAddress("C2I_Z2_ZERO_POS", 0);
            InspPc.YB_Z1Jog	                /**/ = AddressMgr.GetAddress("C2I_Z1_JOG", 0);
            InspPc.YB_Z2Jog	                /**/ = AddressMgr.GetAddress("C2I_Z2_JOG", 0);
            InspPc.YB_Z1PosMove	            /**/ = AddressMgr.GetAddress("C2I_Z1_POS_MOVE", 0);
            InspPc.YB_Z2PosMove	            /**/ = AddressMgr.GetAddress("C2I_Z2_POS_MOVE", 0);
            InspPc.YB_InspectionCompleteAck	    /**/ = AddressMgr.GetAddress("C2I_Z1_INPOS_COMPLETE", 0);
            InspPc.YB_RpcPpidChange	        /**/ = AddressMgr.GetAddress("C2I_RPC_PPID_CHANGE", 0);
            InspPc.YB_NewPpid	            /**/ = AddressMgr.GetAddress("C2I_NEW_PPID", 0);
            InspPc.YB_JudgeCompleteAck	    /**/ = AddressMgr.GetAddress("C2I_JUDGE_COMPLETE", 0);

            //S1000.0
            InspPc.XB_InspectionAlive	    /**/ = AddressMgr.GetAddress("I2C_INSPECTION_ALIVE", 0);
            InspPc.XB_LoadingSuccess	    /**/ = AddressMgr.GetAddress("I2C_LOADING_SUCCESS", 0);
            InspPc.XB_LoadingFail	        /**/ = AddressMgr.GetAddress("I2C_LOADING_FAIL", 0);
            InspPc.XB_ScanReadySuccess	    /**/ = AddressMgr.GetAddress("I2C_SCAN_READY_SUCCESS", 0);
            InspPc.XB_ScanReadyFail	        /**/ = AddressMgr.GetAddress("I2C_SCAN_READY_FAIL", 0);
            InspPc.XB_ScanStartSuccess	    /**/ = AddressMgr.GetAddress("I2C_SCAN_START_SUCCESS", 0);
            InspPc.XB_ScanStartFail	        /**/ = AddressMgr.GetAddress("I2C_SCAN_START_FAIL", 0);
            InspPc.XB_ScanEndSuccess	    /**/ = AddressMgr.GetAddress("I2C_SCAN_END_SUCCESS", 0);
            InspPc.XB_ScanEndFail	        /**/ = AddressMgr.GetAddress("I2C_SCAN_END_FAIL", 0);
            InspPc.XB_UnloadingSuccess	    /**/ = AddressMgr.GetAddress("I2C_UNLOADING_SUCCESS", 0);
            InspPc.XB_UnloadingFail	        /**/ = AddressMgr.GetAddress("I2C_UNLOADING_FAIL", 0);
            InspPc.XB_LotStartSuccess	    /**/ = AddressMgr.GetAddress("I2C_LOT_START_SUCCESS", 0);
            InspPc.XB_LotStartFail	        /**/ = AddressMgr.GetAddress("I2C_LOT_START_FAIL", 0);
            InspPc.XB_LotEndSuccess	        /**/ = AddressMgr.GetAddress("I2C_LOT_END_SUCCESS", 0);
            InspPc.XB_LotEndFail	        /**/ = AddressMgr.GetAddress("I2C_LOT_END_FAIL", 0);
            InspPc.XB_ZxisMoveStart	        /**/ = AddressMgr.GetAddress("I2C_ZXIS_MOVE_START", 0);
            InspPc.XB_InspectionComplete	/**/ = AddressMgr.GetAddress("I2C_INSPECTION_COMPLETE", 0);
            InspPc.XB_JudgeComplete	        /**/ = AddressMgr.GetAddress("I2C_JUDGE_COMPLETE", 0);

            //S1500.0
            InspPc.XB_InspectorError	    /**/ = AddressMgr.GetAddress("I2C_INSPECTOR_ERROR", 0);
            InspPc.XB_PreAlignError	        /**/ = AddressMgr.GetAddress("I2C_PRE_ALIGN", 0);
            InspPc.XB_ServerOverflowError	/**/ = AddressMgr.GetAddress("I2C_SERVER_OVERFLOW", 0);
            InspPc.XB_InspectOverflowError	/**/ = AddressMgr.GetAddress("I2C_INSPECT_OVERFLOW", 0);
            InspPc.XB_StackLoadingFailError	/**/ = AddressMgr.GetAddress("I2C_STACK_LOADING_FAIL", 0);
            InspPc.XB_CommonDefectError	    /**/ = AddressMgr.GetAddress("I2C_COMMON_DEFECT", 0);
            InspPc.XB_MaskDefectError	    /**/ = AddressMgr.GetAddress("I2C_MASK_DEFECT", 0);
            InspPc.XB_EdgeCrackError	    /**/ = AddressMgr.GetAddress("I2C_EDGE_CRACK", 0);
            InspPc.XB_NoRecipeError	        /**/ = AddressMgr.GetAddress("I2C_NO_RECIPE", 0);
            InspPc.XB_FindEdgeFailError	    /**/ = AddressMgr.GetAddress("I2C_FIND_EDGE_FAIL", 0);
            InspPc.XB_LightErrorError	    /**/ = AddressMgr.GetAddress("I2C_LIGHT_ERROR", 0);
            InspPc.XB_UpperLimitError	    /**/ = AddressMgr.GetAddress("I2C_UPPER_LIMIT", 0);
            InspPc.XB_LowestLimitError	    /**/ = AddressMgr.GetAddress("I2C_LOWEST_LIMIT", 0);
            InspPc.XB_ZAxisFailError	    /**/ = AddressMgr.GetAddress("I2C_ZAXIS_FAIL", 0);
            InspPc.XB_NoImageError	        /**/ = AddressMgr.GetAddress("I2C_NO_IMAGE", 0);
            InspPc.XB_GlassDetectFailError	/**/ = AddressMgr.GetAddress("I2C_GLASS_DETECT_FAIL", 0);

            //추가 필요..
            InspPc.YI_ScanIndex	            /**/ = AddressMgr.GetAddress("C2I_F_ScanIndex", 0);
            InspPc.YI_ScanCount	            /**/ = AddressMgr.GetAddress("C2I_F_ScanCount", 0);

            InspPc.XF_Z1Pos	                /**/ = AddressMgr.GetAddress("I2C_F_Z1Pos", 0);
            InspPc.XF_Z1Speed	            /**/ = AddressMgr.GetAddress("I2C_F_Z1Speed", 0);
            InspPc.XF_Z1JogSpeed	        /**/ = AddressMgr.GetAddress("I2C_F_Z1JogSpeed", 0);
            InspPc.XF_Z2Pos	                /**/ = AddressMgr.GetAddress("I2C_F_Z2Pos", 0);
            InspPc.XF_Z2Speed	            /**/ = AddressMgr.GetAddress("I2C_F_Z2Speed", 0);
            InspPc.XF_Z2JogSpeed	        /**/ = AddressMgr.GetAddress("I2C_F_Z2JogSpeed", 0);

            //리뷰 ADDRESS
            ReviPc.YB_Loading	            /**/ = AddressMgr.GetAddress("C2R_LOADING", 0);
            ReviPc.YB_AlignStart	        /**/ = AddressMgr.GetAddress("C2R_ALIGN_START", 0);
            ReviPc.YB_ReviewStart	        /**/ = AddressMgr.GetAddress("C2R_REVIEW_START", 0);
            ReviPc.YB_ReviewTimeOver	    /**/ = AddressMgr.GetAddress("C2R_REVIEW_TIME_OVER", 0);
            ReviPc.YB_Unloading	            /**/ = AddressMgr.GetAddress("C2R_UNLOADING", 0);
            ReviPc.YB_LotStart	            /**/ = AddressMgr.GetAddress("C2R_LOT_START", 0);
            ReviPc.YB_LotEnd	            /**/ = AddressMgr.GetAddress("C2R_LOT_END", 0);
            ReviPc.YB_ReviewCompleteAck     /**/ = AddressMgr.GetAddress("C2R_REVIEW_COMPLETE_ACK", 0);

            ReviPc.XB_OnFocus	            /**/ = AddressMgr.GetAddress("R2C_ON_FOCUS", 0);
            ReviPc.XB_LoadingAck	        /**/ = AddressMgr.GetAddress("R2C_LOADING_ACK", 0);
            ReviPc.XB_AlignStartAck	        /**/ = AddressMgr.GetAddress("R2C_ALIGN_START_ACK", 0);
            ReviPc.XB_ReviewStartAck	    /**/ = AddressMgr.GetAddress("R2C_REVIEW_START_ACK", 0);
            ReviPc.XB_ReviewTimeOverAck	        /**/ = AddressMgr.GetAddress("R2C_TIME_OVER_ACK", 0);
            ReviPc.XB_UnloadingAck	        /**/ = AddressMgr.GetAddress("R2C_UNLOADING_ACK", 0);
            ReviPc.XB_LotStartAck	        /**/ = AddressMgr.GetAddress("R2C_LOT_START_ACK", 0);
            ReviPc.XB_LotEndAck	            /**/ = AddressMgr.GetAddress("R2C_LOT_END_ACK", 0);
            ReviPc.XB_ReviewComplete	    /**/ = AddressMgr.GetAddress("R2C_REVIEW_COMPLETE", 0);

            ReviPc.XB_LoadingFailError	    /**/ = AddressMgr.GetAddress("R2C_LOADING_FAIL", 0);
            ReviPc.XB_AlignFailError	    /**/ = AddressMgr.GetAddress("R2C_ALIGN_FAIL", 0);
            ReviPc.XB_ReviewFailError	    /**/ = AddressMgr.GetAddress("R2C_REVIEW_FAIL", 0);
            ReviPc.XB_UnloadingFailError	/**/ = AddressMgr.GetAddress("R2C_UNLOADING_FAIL", 0);


            //추가 필요..
            InspPc.YI_ScanIndex	            /**/ = AddressMgr.GetAddress("C2I_F_ScanIndex", 0);
            InspPc.YI_ScanCount	            /**/ = AddressMgr.GetAddress("C2I_F_ScanCount", 0);

            InspPc.XF_Z1Pos	                /**/ = AddressMgr.GetAddress("I2C_F_Z1Pos", 0);
            InspPc.XF_Z1Speed	            /**/ = AddressMgr.GetAddress("I2C_F_Z1Speed", 0);
            InspPc.XF_Z1JogSpeed	        /**/ = AddressMgr.GetAddress("I2C_F_Z1JogSpeed", 0);
            InspPc.XF_Z2Pos	                /**/ = AddressMgr.GetAddress("I2C_F_Z2Pos", 0);
            InspPc.XF_Z2Speed	            /**/ = AddressMgr.GetAddress("I2C_F_Z2Speed", 0);
            InspPc.XF_Z2JogSpeed	        /**/ = AddressMgr.GetAddress("I2C_F_Z2JogSpeed", 0);
        }


        private bool _isFirtst = true;
        public void Working()
        {
            try
            {
                //if (InterLock)
                //    CheckInterLock();
                if (_isFirtst == true)
                {
                    _isFirtst = false;
                }

                ReadFromPLC();
                
                #region 실린더
                Motors.ToList().ForEach(m => m.LogicWorking(this));

                //this.LoaderAOGamji.LogicWorking();
                //this.LoaderAExist.LogicWorking();
                //this.UnloaderBOGamji.LogicWorking();
                //this.UnloaderBExist.LogicWorking();
                //this.LoaderBOGamji.LogicWorking();
                //this.LoaderBExist.LogicWorking();
                //this.UnloaderBOGamji.LogicWorking();
                //this.UnloaderBExist.LogicWorking();

                this.Vaccum01.LogicWorking();
                this.Vaccum02.LogicWorking();
                this.Vaccum03.LogicWorking();
                this.Vaccum04.LogicWorking();
                this.Vaccum05.LogicWorking();
                this.Vaccum06.LogicWorking();
                this.Vaccum07.LogicWorking();
                this.Vaccum08.LogicWorking();
                this.Vaccum09.LogicWorking();
                this.Vaccum10.LogicWorking();
                this.Vaccum11.LogicWorking();
                this.Vaccum12.LogicWorking();
                this.Vaccum13.LogicWorking();
                this.Vaccum14.LogicWorking();
                this.Vaccum15.LogicWorking();
                this.Vaccum16.LogicWorking();
                this.Vaccum17.LogicWorking();
                this.Vaccum18.LogicWorking();
                this.Vaccum19.LogicWorking();
                this.Vaccum20.LogicWorking();
                this.Vaccum21.LogicWorking();
                this.Vaccum22.LogicWorking();
                this.Vaccum23.LogicWorking();
                this.Vaccum24.LogicWorking();
                this.Vaccum25.LogicWorking();
                this.Vaccum26.LogicWorking();
                this.Vaccum27.LogicWorking();
                this.Vaccum28.LogicWorking();
                this.Vaccum29.LogicWorking();
                this.Vaccum30.LogicWorking();
                this.Vaccum31.LogicWorking();
                this.Vaccum32.LogicWorking();
                this.Vaccum33.LogicWorking();
                this.Vaccum34.LogicWorking();
                this.Vaccum35.LogicWorking();
                this.Vaccum36.LogicWorking();
                this.Vaccum37.LogicWorking();
                this.Vaccum38.LogicWorking();

                this.Blower01.LogicWorking();
                this.Blower02.LogicWorking();
                this.Blower03.LogicWorking();
                this.Blower04.LogicWorking();
                this.Blower05.LogicWorking();
                this.Blower06.LogicWorking();
                this.Blower07.LogicWorking();
                this.Blower08.LogicWorking();
                this.Blower09.LogicWorking();
                this.Blower10.LogicWorking();
                this.Blower11.LogicWorking();
                this.Blower12.LogicWorking();
                this.Blower13.LogicWorking();
                this.Blower14.LogicWorking();
                this.Blower15.LogicWorking();
                this.Blower16.LogicWorking();
                this.Blower17.LogicWorking();
                this.Blower18.LogicWorking();
                this.Blower19.LogicWorking();
                this.Blower20.LogicWorking();
                this.Blower21.LogicWorking();
                this.Blower22.LogicWorking();
                this.Blower23.LogicWorking();
                this.Blower24.LogicWorking();
                this.Blower25.LogicWorking();
                this.Blower26.LogicWorking();
                this.Blower27.LogicWorking();
                this.Blower28.LogicWorking();
                this.Blower29.LogicWorking();
                this.Blower30.LogicWorking();
                this.Blower31.LogicWorking();
                this.Blower32.LogicWorking();
                this.Blower33.LogicWorking();
                this.Blower34.LogicWorking();
                this.Blower35.LogicWorking();
                this.Blower36.LogicWorking();
                this.Blower37.LogicWorking();
                this.Blower38.LogicWorking();

                CstLoaderB_CstGrip_Cylinder.LogicWorking();
                CstLoaderA_CstGrip_Cylinder.LogicWorking();
                CstLoaderB_CstGrip_Cylinder_.LogicWorking();
                CstLoaderA_CstGrip_Cylinder_.LogicWorking();

                LoaderTRB1_UpDown_Cylinder.LogicWorking();
                LoaderTRB2_UpDown_Cylinder.LogicWorking();
                LoaderTRA1_UpDown_Cylinder.LogicWorking();
                LoaderTRA2_UpDown_Cylinder.LogicWorking();
                CstLoaderB_Tilt_Cylinder.LogicWorking();
                CstLoaderA_Tilt_Cylinder.LogicWorking();

                LaserHead1_UpDown_Cylinder.LogicWorking();
                LaserHead2_UpDown_Cylinder.LogicWorking();
                LaserHead_Shutter_Cylinder.LogicWorking();
                IRCutStageB_BrushUpDown_Cylinder.LogicWorking();
                IRCutStageA_BrushUpDown_Cylinder.LogicWorking();
                AfterIRCutTRB1_UpDown_Cylinder.LogicWorking();
                AfterIRCutTRB2_UpDown_Cylinder.LogicWorking();
                AfterIRCutTRA1_UpDown_Cylinder.LogicWorking();
                AfterIRCutTRA2_UpDown_Cylinder.LogicWorking();
                BreakStageB_BrushUpDown_Cylinder.LogicWorking();
                BreakStageA_BrushUpDown_Cylinder.LogicWorking();
                LaserHead1_DummyShutter_Cylinder.LogicWorking();
                LaserHead2_DummyShutter_Cylinder.LogicWorking();
                LaserHead_DummyTank_Cylinder.LogicWorking();


                CstUnloaderB_Grip_Cylinder.LogicWorking();
                CstUnloaderA_Grip_Cylinder.LogicWorking();
                CstUnloaderB_Grip_Cylinder_.LogicWorking();
                CstUnloaderA_Grip_Cylinder_.LogicWorking();

                Buffer_UpDown_Cylinder.LogicWorking();
                BeforeInspUnloaderTRB_UpDonw_Cylinder.LogicWorking();
                AfterInspUnloaderTRB_UpDonw_Cylinder.LogicWorking();
                BeforeInspUnloaderTRA_UpDonw_Cylinder.LogicWorking();
                AfterInspUnloaderTRA_UpDonw_Cylinder.LogicWorking();

                BeforeInspUnloaderTRB_UpDonw_Cylinder_.LogicWorking();
                AfterInspUnloaderTRB_UpDonw_Cylinder_.LogicWorking();
                BeforeInspUnloaderTRA_UpDonw_Cylinder_.LogicWorking();
                AfterInspUnloaderTRA_UpDonw_Cylinder_.LogicWorking();

                CstUnloaderB_Tilt_Cylinder.LogicWorking();
                CstUnloaderA_Tilt_Cylinder.LogicWorking();
                #endregion

                WriteToPLC();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void AlwaysOn()
        {
            AddressMgr.GetAddress("CHILLER_STATUS-ON").vBit = true;
            AddressMgr.GetAddress("DUST_COLLECTOR-ON").vBit = true;
        }
        private void CheckInterLock()
        {
            Logger.Log.AppendLine(LogLevel.Error, "");
        }

        private void ReadFromPLC()
        {

        }
        private void WriteToPLC()
        {
        }
    }
}

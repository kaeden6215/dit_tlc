﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using DitInlineCim.PLC;
using DitInlineCim.Common;
using Dit.Framework.PLC;

namespace DitInlineCim
{
    public partial class FrmMonitor : Form
    {
        public VirtualShare PLC { get; set; }
        public FrmMonitor()
        {
            InitializeComponent();

            LoadAddress();
        }
        public class MonItem
        {
            public PlcAddr Addr { get; set; }
            public string Name { get; set; }
        }

        public void LoadAddress()
        {
            InitailizeAddress(dgvCimBit, new CIMB());
            InitailizeAddress(dgvCimWord, new CIMW());

            InitailizeAddress(dgvEqpiBit, new EQPB());
            InitailizeAddress(dgvEqpWord, new EQPW());
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            LoadAddress();
        }

        private void InitailizeAddress(DataGridView dgv, object idb)
        {
            dgv.ReadOnly = false;

            dgv.Columns[0].Width = 150;
            dgv.Columns[0].HeaderText = "Name";
            dgv.Columns[0].ReadOnly = true;

            dgv.Columns[1].Width = 70;
            dgv.Columns[1].HeaderText = "Address";
            dgv.Columns[1].ReadOnly = true;

            dgv.Columns[2].Width = 30;
            dgv.Columns[2].HeaderText = "Type";
            dgv.Columns[2].ReadOnly = true;


            dgv.Columns[3].Width = 60;
            dgv.Columns[3].HeaderText = "Value";
            dgv.Columns[3].ReadOnly = true;

            dgv.Columns[4].Width = 60;
            dgv.Columns[4].HeaderText = "Set Value";
            dgv.Columns[4].ReadOnly = false;

            dgv.Columns[5].Width = 50;
            dgv.Columns[5].HeaderText = "Set";
            dgv.Columns[5].ReadOnly = true;

            List<MonItem> lst = new List<MonItem>();
            var bindingFlags = BindingFlags.Public | BindingFlags.Static;
            foreach (FieldInfo info in idb.GetType().GetFields(bindingFlags))
            {
                if (chkHandSake.Checked == false)
                {
                    if (info.Name.Substring(0, 2) == "LO") continue;
                    if (info.Name.Substring(0, 2) == "UP") continue;
                }
                Console.WriteLine(info.Name);
                var plcAddr = info.GetValue(idb) as PlcAddr;
                if (plcAddr != null)
                    lst.Add(new MonItem() { Addr = plcAddr, Name = info.Name });
            }

            dgv.Rows.Clear();
            foreach (MonItem item in lst.OrderBy(f => f.Addr.Addr).ThenBy(g => g.Addr.Bit))
            {
                int irow = dgv.Rows.Add(new string[] 
                {
                    item.Name,
                    item.Addr.ToString(),
                    item.Addr.ValueType.ToString().Substring(0, 1),
                    "-", "-", "SET"
                });

                dgv.Rows[irow].Tag = item;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrIndex.Interval = 1000;
            tmrIndex.Start();
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            tmrIndex.Stop();
        }
        private void tmrIndex_Tick(object sender, EventArgs e)
        {
            lblTimer.Text = DateTime.Now.ToString("HH:mm:ss.fff");

            foreach (DataGridViewRow ff in dgvCimBit.Rows)
            {
                MonItem item = ff.Tag as MonItem;
                if (item == null) return;

                string value = GetStringValue(GG.PLC, item.Addr);
                ff.Cells[3].Value = value;
            }
            foreach (DataGridViewRow ff in dgvCimWord.Rows)
            {
                MonItem item = ff.Tag as MonItem;
                if (item == null) return;

                string value = GetStringValue(GG.PLC, item.Addr);
                ff.Cells[3].Value = value;
            }
            foreach (DataGridViewRow ff in dgvEqpiBit.Rows)
            {
                MonItem item = ff.Tag as MonItem;
                if (item == null) return;

                string value = GetStringValue(GG.PLC, item.Addr);
                ff.Cells[3].Value = value;
            }
            foreach (DataGridViewRow ff in dgvEqpWord.Rows)
            {
                MonItem item = ff.Tag as MonItem;
                if (item == null) return;

                string value = GetStringValue(GG.PLC, item.Addr);
                ff.Cells[3].Value = value;
            }
        }

        public string GetStringValue(VirtualShare PLC, PlcAddr Address)
        {
            string result = string.Empty;
            if (Address.ValueType == PlcValueType.BIT)
            {
                bool value = PLC.VirGetBit(Address);
                result = value ? "1" : "0";
            }
            else if (Address.ValueType == PlcValueType.SHORT)
            {
                short value = PLC.VirGetShort(Address);
                result = value.ToString();
            }
            else if (Address.ValueType == PlcValueType.INT32)
            {
                int value = PLC.VirGetInt32(Address);
                result = value.ToString();
            }
            else if (Address.ValueType == PlcValueType.ASCII)
            {
                string value = PLC.VirGetAsciiTrim(Address);
                result = value;
            }

            return result;
        }
        public void SetStringValue(VirtualShare PLC, PlcAddr Address, string strValue)
        {
            if (Address.ValueType == PlcValueType.BIT)
            {
                bool value = strValue == "1" ? true : false;
                PLC.SetBit(Address, value);
            }
            else if (Address.ValueType == PlcValueType.SHORT)
            {
                short value = 0;
                if (short.TryParse(strValue, out  value))
                {
                    PLC.SetShort(Address, value);
                }
                else
                {
                    //UserMessageBox.ShowAutoClose("이상값 입력함", "DIT CIM", 3);
                }
            }
            else if (Address.ValueType == PlcValueType.INT32)
            {
                int value = 0;
                if (int.TryParse(strValue, out  value))
                {
                    PLC.SetInt32(Address, value);
                }
                else
                {
                    //UserMessageBox.ShowAutoClose("이상값 입력함", "DIT CIM", 3);
                }
            }
            else if (Address.ValueType == PlcValueType.ASCII)
            {
                string value = strValue;
                PLC.SetAscii(Address, value);
            }
        }

        private void dgvCimBit_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null) return;

            if (e.ColumnIndex == 5)
            {
                if (dgv.Rows[e.RowIndex].Cells[4].Value != null)
                {
                    MonItem item = dgv.Rows[e.RowIndex].Tag as MonItem;

                    if (item != null)
                    {
                        string strValue = dgv.Rows[e.RowIndex].Cells[4].Value.ToString();
                        SetStringValue(GG.PLC, item.Addr, strValue);
                    }
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DitInlineCim.Log
{
    public class Logger
    {
        public static SimpleFileLogger FileLogger;
        public static int LogLineCount = 0;
        public static void Append(string format, params object[] args)
        {
            Append(string.Format(format, args));
        }
        public static void Append(string value)
        {
            string log = string.Format("{0:HH:mm:ss}\t{1}", DateTime.Now, value);
            if (LogControl != null)
                LogControl.AppendText(  Environment.NewLine + log);

            LogLineCount++;
            if (LogLineCount > 1000)
            {
                LogLineCount = 0;
                LogControl.Text = string.Empty;
            }

            if (FileLogger != null)
                FileLogger.AppendLine(log);
        }

        public static TextBox LogControl { get; set; }
    }
}

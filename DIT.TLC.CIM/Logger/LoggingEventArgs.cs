﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Log
{
    public class LoggingEventArgs : EventArgs
    {
        // 필드
        public string LogMessage { get; set; }
        public DateTime SignalTime { get; set; }
    }
}

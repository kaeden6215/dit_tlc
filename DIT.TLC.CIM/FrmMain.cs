﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.Struct;
using DitInlineCim.Common;
using DitInlineCim.Lib;
using DitInlineCim.HSMS;
using DitInlineCim.PLC;
using DitInlineCim.Log;
using System.Threading;
using DitInlineCim.UI;
using DitInlineCim.Struct.SV;
using DitInlineCim.Ucrl;
using DitInlineCim.EventArg;
using System.Diagnostics;
using System.IO;
using DitInlineCim.Struct.DV;
using DitInlineCim.Struct.RMM;
using DitInlineCim.Struct.EC;
using Dit.Framework.PLC;

namespace DitInlineCim
{
    public partial class FrmMain : Form
    {
        private ScenarioWorker _scenarioWorker;


        //일부 범위에서 사용        
        private DateTime _hostDisconnectedTime = DateTime.Now;
        private FrmCtrl ff = new FrmCtrl();

        public FrmMain()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            InitializProgram();


            StartCim();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (MessageBox.Show("Application Exit !! ", "DIT CIM", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                StotCim();
            }
            base.OnClosing(e);
        }
        private void InitializProgram()
        {
            CimSetting setting = new CimSetting();
            setting.Load(GG.SETTING_PATH);

            GG.Equip = new EquipmentInfo(setting.CIM_EQPID, GG.MAX_PORT, this, setting.CIM_FROMEQPID, setting.CIM_TOEQPID);
            GG.Equip.LoadSetting(GG.SETTING_PATH);



            //LOG 
            Logger.LogControl = txtLog;
            Logger.FileLogger = new SimpleFileLogger(setting.CIM_LOG_PATH, setting.CIM_LOG_NAME, 2000, 1024 * 20 * 1024, 30);

            ucrlEoid.Initailzie();
            axHsms.DeviceID = setting.HSMS_HOST_DEVICEID;
            axHsms.T3 = setting.HSMS_HOST_T3;
            axHsms.T5 = setting.HSMS_HOST_T5;
            axHsms.T6 = setting.HSMS_HOST_T6;
            axHsms.T7 = setting.HSMS_HOST_T7;
            axHsms.T8 = setting.HSMS_HOST_T8;
            axHsms.Retry = setting.HSMS_HOST_RETRY;
            axHsms.LinkTestInterval = setting.HSMS_HOST_LINK_TEST_INTERVAL;

            if (setting.HSMS_HOST_MODE == 1)
                axHsms.PassiveMode = true;
            else
                axHsms.PassiveMode = false;

            axHsms.IP = setting.HSMS_HOST_REMOTE_ADDRESS;
            axHsms.Port = setting.HSMS_HOST_CONNECT_PORT_NO;
            axHsms.HostMode = false;

            if (setting.HSMS_LOG_ENABLE == 1)
                axHsms.EnableLog();
            else
                axHsms.DisableLog();

            string strLogFile = setting.HSMS_HOST_LOG_FILE_PATH + "\\" + "HSMS.Log";
            axHsms.SetLogFile(strLogFile, 1);
            axHsms.SetLogRetention(setting.HSMS_HOST_LOG_KEEP_DAY);

            string strFormatFile = Path.Combine(Application.StartupPath, "Setting", "T8_Z.SML");
            axHsms.SetFormatFile(strFormatFile);

            _scenarioWorker = new ScenarioWorker();

            HSMS_SEND.AxHsms = axHsms;
            HSMS_RECV.AxHsms = axHsms;

            GG.Equip.Worker = _scenarioWorker;

            this.Text = string.Format("DIT EQ - {0}", setting.CIM_EQPID);
        }
        private void StartCim()
        {
            //PLC 로직 시작
            _scenarioWorker.Start();

            ucrlTimer.Start();
            ucrlScenarioMon.Start();

            //HOST 통신 START
            Logger.Append("HOST Communication Start");
            axHsms.Start();

            UIUpdate();
        }
        private void StotCim()
        {
            //HOST 통신 START
            Logger.Append("HOST Communication Stop");
            axHsms.Stop();

            _scenarioWorker.Stop();
        }

        //메소드 - UI 업데이트 
        public void UIUpdate()
        {
            UIEquipState();
            UIProcessState();
            UIMCmd();

            UIEqipModuleState();
            UIUpdateEoid();            
        }

        public void UIComConnState()
        {
            ucrlConnState.HostConnColor = GG.Equip.HostConnected ? CimColor.CoON_B : CimColor.CoOFF_B;
            ucrlConnState.EquipPLCConnColor = GG.Equip.EquipPlcConnected ? CimColor.CoON_B : CimColor.CoOFF_B;
        }
        public void UIEquipState()
        {
            btnEqStateNormal.BackColor = GG.Equip.EquipState == EMEquipState.Normal ? CimColor.CoON_B : CimColor.CoEQUIPST_DEFAULT_B;
            btnEqStateNormal.ForeColor = GG.Equip.EquipState == EMEquipState.Normal ? CimColor.CoON_F : CimColor.CoEQUIPST_DEFAULT_F;

            btnEqStateFault.BackColor = GG.Equip.EquipState == EMEquipState.Fault ? CimColor.CoON_B : CimColor.CoEQUIPST_DEFAULT_B;
            btnEqStateFault.ForeColor = GG.Equip.EquipState == EMEquipState.Fault ? CimColor.CoON_F : CimColor.CoEQUIPST_DEFAULT_F;

            btnEqStatePM.BackColor = GG.Equip.EquipState == EMEquipState.PM ? CimColor.CoON_B : CimColor.CoEQUIPST_DEFAULT_B;
            btnEqStatePM.ForeColor = GG.Equip.EquipState == EMEquipState.PM ? CimColor.CoON_F : CimColor.CoEQUIPST_DEFAULT_F;
        }
        public void UIProcessState()
        {
            btnProcStateIdle.BackColor = GG.Equip.ProcState == EMProcState.Idle ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;
            btnProcStateIdle.ForeColor = GG.Equip.ProcState == EMProcState.Idle ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;

            btnProcStateExecute.BackColor = GG.Equip.ProcState == EMProcState.Execute ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;
            btnProcStateExecute.ForeColor = GG.Equip.ProcState == EMProcState.Execute ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;

            btnProcStatePause.BackColor = GG.Equip.ProcState == EMProcState.Pause ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;
            btnProcStatePause.ForeColor = GG.Equip.ProcState == EMProcState.Pause ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;

            btnProcStateSetup.BackColor = GG.Equip.ProcState == EMProcState.Setup ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;
            btnProcStateSetup.ForeColor = GG.Equip.ProcState == EMProcState.Setup ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;

            btnProcStateReady.BackColor = GG.Equip.ProcState == EMProcState.Ready ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;
            btnProcStateReady.ForeColor = GG.Equip.ProcState == EMProcState.Ready ? CimColor.CoON_B : CimColor.CoPROCST_DEFAULT_B;

        }
        public void UIEqipModuleState()
        {
            ucrlModuleState.UiUpdate();
        }
        public void UIMCmd()
        {
            btnMCmdOffLine.BackColor = GG.Equip.MCMD == EMMCmd.OFFLINE ? CimColor.CoMCMD_OFFLINE_B : CimColor.CoMCMD_DEFAULT_B;
            btnMCmdOffLine.ForeColor = GG.Equip.MCMD == EMMCmd.OFFLINE ? CimColor.CoMCMD_OFFLINE_F : CimColor.CoMCMD_DEFAULT_F;

            btnMCmdLocal.BackColor = GG.Equip.MCMD == EMMCmd.ONLINE_LOCAL ? CimColor.CoMCMD_LOCAL_B : CimColor.CoMCMD_DEFAULT_B;
            btnMCmdLocal.ForeColor = GG.Equip.MCMD == EMMCmd.ONLINE_LOCAL ? CimColor.CoMCMD_LOCAL_F : CimColor.CoMCMD_DEFAULT_F;

            btnMCmdRemote.BackColor = GG.Equip.MCMD == EMMCmd.ONLINE_REMOTE ? CimColor.CoMCMD_REMOTE_B : CimColor.CoMCMD_DEFAULT_B;
            btnMCmdRemote.ForeColor = GG.Equip.MCMD == EMMCmd.ONLINE_REMOTE ? CimColor.CoMCMD_REMOTE_F : CimColor.CoMCMD_DEFAULT_F;

        }
        public void UIUpdateEoid()
        {
            ucrlEoid.Initailzie();
        } 

        //메소드 - 화면 변경 처리. 
        public void UIAddAlarmHistoryList(short setCode, short alarmCode, short alarmID, string alarmText, string moduleid)
        {
            if (setCode == 1)
            {
                lstAlarm.Items.Add(new ListViewItem(new string[]{
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    alarmText,
                    moduleid ,
                    alarmID.ToString(),
                    alarmCode.ToString()
                }));


                if (lstAlarm.Items.Count > 500)
                    lstAlarm.Items.RemoveAt(500);
            }
            else
            {
                List<ListViewItem> lstDelPos = new List<ListViewItem>();
                for (int iPos = 0; iPos < lstAlarm.Items.Count; iPos++)
                {
                    if (lstAlarm.Items[iPos].SubItems[3].Text == alarmID.ToString())
                        lstDelPos.Add(lstAlarm.Items[iPos]);
                }

                foreach (ListViewItem delPos in lstDelPos)
                    lstAlarm.Items.Remove(delPos);
            }
        }

        //메소드 - HSMS 통신
        private void axEZNetLight_OnConnected(object sender, AxEZNETLIGHTLib._DEZNetLightEvents_OnConnectedEvent e)
        {
            Logger.Append("[APP] HSMS Connected OK (HOST)");

            GG.Equip.HostConnected = true;
            GG.Equip.HostConnectedOneOff = true;

            this.btnMCmdRemote.Enabled = true;
            this.btnMCmdLocal.Enabled = true;
            this.btnMCmdOffLine.Enabled = true;

            GG.Equip.MCMD_OLD = GG.Equip.MCMD;
            HSMS_SEND.S1F1();

            tmrHostDisConnMsg.Stop();
        }
        private void axEZNetLight_OnDisconnected(object sender, AxEZNETLIGHTLib._DEZNetLightEvents_OnDisconnectedEvent e)
        {
            Logger.Append("[APP] HSMS Disonnected (HOST)");

            GG.Equip.HostConnected = false;
            GG.Equip.HostConnectedOneOff = false;

            this.btnMCmdRemote.Enabled = false;
            this.btnMCmdLocal.Enabled = false;
            this.btnMCmdOffLine.Enabled = false;

            GG.Equip.CmdSetMCMD(EMMCmd.OFFLINE, EMByWho.ByEqp);
            GG.Equip.TdMgr.Clear();
            tmrHostDisConnMsg.Start();
        }
        private void axEZNetLight_OnMsgReceived(object sender, AxEZNETLIGHTLib._DEZNetLightEvents_OnMsgReceivedEvent e)
        {
            int iMsgLength = 0;
            short stream = 0, function = 0, sWait = 0;

            short sRet = axHsms.GetMsgInfo(e.lMsgId, ref stream, ref function, ref sWait, ref iMsgLength);
            if (sRet != 0)
            {
                Logger.Append(string.Format("Receive Message error.. {0}", sRet));
                return;
            }

            string sf = string.Format("S{0}F{1}", stream, function);
            string ee = string.Empty;


            switch (sf)
            {
                case "S1F1": //Are You There
                    HSMS_RECV.S1F1(e.lMsgId);
                    break;

                case "S1F2": //On Line Data
                    HSMS_RECV.S1F2(e.lMsgId);
                    break;

                case "S1F3":  //Selected Equipment State Request
                    HSMS_RECV.S1F3(e.lMsgId);
                    break;

                case "S1F5": //Formatted State Request
                    HSMS_RECV.S1F5(e.lMsgId);
                    break;
                case "S1F17":
                    HSMS_RECV.S1F17(e.lMsgId);
                    break;
                    break;
                case "S2F15":
                    HSMS_RECV.S2F15(e.lMsgId);
                    break;

                case "S2F23": // Trace Initialize Send(TIS)
                    HSMS_RECV.S2F23(e.lMsgId);
                    break;

                case "S2F29": // Equipment Constant NameList Request (ECNR ) 
                    HSMS_RECV.S2F29(e.lMsgId);
                    break;

                case "S2F31": // Date and Time Set Request
                    HSMS_RECV.S2F31(e.lMsgId);
                    break;

                case "S2F41": // Host Command Send
                    HSMS_RECV.S2F41(e.lMsgId);
                    break;

                case "S2F101": // Operator Call
                    HSMS_RECV.S2F101(e.lMsgId);
                    break;

                case "S2F103": // Equipment Online Parameter Change
                    HSMS_RECV.S2F103(e.lMsgId);
                    break;

                case "S5F2": //  Acknowledge of Alarm ReportList(WRAL)
                    HSMS_RECV.S5F2(e.lMsgId);
                    break;

                case "S5F5": //  List Alarms Request(LAR)
                    HSMS_RECV.S5F5(e.lMsgId);
                    break;

                case "S5F101": // Waiting Reset Alarms List(WRAL)
                    HSMS_RECV.S5F101(e.lMsgId);
                    break;
                    

                case "S6F12":  // Event Report Acknowledge
                    HSMS_RECV.S6F12(e.lMsgId);
                    break;

                case "S6F14":  // Annotated Event Report Acknowledge 
                    HSMS_RECV.S6F14(e.lMsgId);
                    break;
                    

                case "S7F23":
                    HSMS_RECV.S7F23(e.lMsgId);
                    break;
                    

                case "S7F25": // Formatted Process Program Request
                    HSMS_RECV.S7F25(e.lMsgId);
                    break;

                case "S7F101": // Current Equipment PPID List Request
                    HSMS_RECV.S7F101(e.lMsgId);
                    break;

                case "S7F103": // PPID Existence Check
                    HSMS_RECV.S7F103(e.lMsgId);
                    break;
                    

                case "S7F108": // PPID Create/Delete/Modify Report Reply
                    HSMS_RECV.S7F108(e.lMsgId);
                    break;

                case "S7F109": // FPPID Create/Delete/Modify Report Reply
                    HSMS_RECV.S7F109(e.lMsgId);
                    break;
                    
                case "S10F3": // Terminal Display Single
                    HSMS_RECV.S10F3(e.lMsgId);
                    break;
                    
                case "S3F215": //오정석
                    HSMS_RECV.S3F215(e.lMsgId);
                    break;

                case "S7F1": //오정석
                    HSMS_RECV.S7F1(e.lMsgId);
                    break;

                default:
                    HSMS_SEND.S9F3(e.lMsgId);
                    break;
            }
        }
        private void axEZNetLight_OnOtherEvent(object sender, AxEZNETLIGHTLib._DEZNetLightEvents_OnOtherEventEvent e)
        {

            switch (e.nEventId)
            {
                case 101:
                    //AddLog(1, "[EVENT] LinkTest.Req"); 
                    break;
                case 102:
                    //AddLog(1, "[EVENT] LinkTest.Req"); 
                    break;
                case 103:
                    //AddLog(1, "[EVENT] LinkTest.Rsp"); 
                    break;
                case 104:
                    //AddLog(1, "[EVENT] LinkTest.Rsp"); 
                    break;
                case 202:
                    Logger.Append(string.Format("[EVENT] S9F9 Transaction Timeout. MsgId={0}", e.lParm));
                    break;
                case 203:
                    Logger.Append(string.Format("[EVENT] S9F1 Unrecognized Device ID {0}", e.lParm));
                    break;
                case 204:
                    Logger.Append(string.Format("[EVENT] S9F3 Unrecognized Stream {0}", e.lParm));
                    break;
                case 205:
                    Logger.Append(string.Format("[EVENT] S9F5 Unrecognized Function {0}", e.lParm));
                    break;
                case 206:
                    Logger.Append(string.Format("[EVENT] S9F7 Invalid Data. MsgId {0}", e.lParm));
                    break;
                case 207:
                    Logger.Append(string.Format("[EVENT] Message Id{0} to discarded", e.lParm));
                    break;
                case 303:
                    Logger.Append("[EVENT] T3 Timeout");
                    break;
                case 305:
                    Logger.Append("[EVENT] T5 Timeout");
                    break;
                case 306:
                    Logger.Append("[EVENT] T6 Timeout");
                    break;
                case 307:
                    Logger.Append("[EVENT] T7 Timeout");
                    break;
                case 308:
                    Logger.Append("[EVENT] T8 Timeout");
                    break;

                default:
                    Logger.Append(string.Format("[EVENT] {0}", e.nEventId));
                    break;
            }
        }

        // 메소드 - 설비 상태 변경 
        private void btnEqStateNormal_Click(object sender, EventArgs e)
        {
            if (GG.Equip.EquipState == EMEquipState.Normal) return;
            string boxstring = "Equipment status change to NORMAL ok ?";

            if (MessageBox.Show(boxstring, "DIT CIM", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            {
                GG.Equip.CmdEquipStateNormal(EMByWho.ByOper);
            }
        }
        private void btnEqStateFault_Click(object sender, EventArgs e)
        {
            if (GG.Equip.EquipState == EMEquipState.Fault) return;
            string boxstring = "Equipment status change to Fault ok ?";

            if (MessageBox.Show(boxstring, "DIT CIM", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            {
                GG.Equip.SetEqState(EMEquipState.Fault, "", EMByWho.ByOper);
            }
        }
        private void btnEqStatePM_Click(object sender, EventArgs e)
        {
            if (GG.Equip.EquipState == EMEquipState.PM) return;
            string boxstring = "Equipment status change to Fault ok ?";

            if (MessageBox.Show(boxstring, "DIT CIM", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            {
                GG.Equip.CmdEquipStatePM(EMByWho.ByOper);
            }
        }

        // 설비 온라인 상태 변경
        private void btnMCmdRemote_Click(object sender, EventArgs e)
        {
            //GG.Equip.CmdSetMCMD(EMMCmd.ONLINE_REMOTE, EMByWho.ByOper);
            GG.Equip.SetMCMD(EMMCmd.ONLINE_REMOTE, EMByWho.ByOper);
        }
        private void btnMCmdLocal_Click(object sender, EventArgs e)
        {
            //GG.Equip.CmdSetMCMD(EMMCmd.ONLINE_LOCAL, EMByWho.ByOper);
            GG.Equip.SetMCMD(EMMCmd.ONLINE_LOCAL, EMByWho.ByOper);
        }
        private void btnMCmdOffLine_Click(object sender, EventArgs e)
        {
            //GG.Equip.CmdSetMCMD(EMMCmd.OFFLINE, EMByWho.ByOper);
            GG.Equip.SetMCMD(EMMCmd.OFFLINE, EMByWho.ByOper);
        }

        // 프로세스 상태 변경
        private void btnProcStateIdle_Click(object sender, EventArgs e)
        {

        }
        private void btnProcStateExecute_Click(object sender, EventArgs e)
        {

        }
        private void btnProcStatePause_Click(object sender, EventArgs e)
        {

        }
        private void btnProcStateSetup_Click(object sender, EventArgs e)
        {

        }
        private void btnProcStateReady_Click(object sender, EventArgs e)
        {

        }

        private void tmTd_Tick(object sender, EventArgs e)
        {
            if (GG.Equip.TdMgr.Count() > 0)
            {
                foreach (KeyValuePair<int, TdItem> kvp in GG.Equip.TdMgr)
                {
                    TdItem tdItem = kvp.Value;
                    if (tdItem.SendingReport(GG.Equip.SvMgr))
                    {
                        HSMS_SEND.S6F1(tdItem);
                        tdItem.Clear();
                    }
                }
            }
        }

        private void tmrHostDisConnMsg_Tick(object sender, EventArgs e)
        {
            if (tmrHostDisConnMsg.Enabled == true)
            {
                tmrHostDisConnMsg.Stop();
                AddListTerminnalMessage(GG.Equip.ModuleID, 0, "HOST DISCONNECTION");
            }
        }

        private void btnEqPause_Click(object sender, EventArgs e)
        {
            if (GG.Equip.FlagProcStateHost == (int)EMRCmd.EQUIPMENT_PAUSE) return;

            if (MessageBox.Show("Equipment Pause OK ?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                GG.Equip.FlagProcStateHost = (int)EMRCmd.EQUIPMENT_PAUSE;
                GG.Equip.ByWhoEquipPause = EMByWho.ByHost;
                //GG.Equip.EqProcessPauseCmd();
            }
        }
        private void btnEqResume_Click(object sender, EventArgs e)
        {
            if (GG.Equip.FlagProcStateHost == (int)EMRCmd.EQUIPMENT_RESUME) return;
            if (MessageBox.Show("Equipment Resume OK ?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                GG.Equip.FlagProcStateHost = (int)EMRCmd.EQUIPMENT_RESUME;
                GG.Equip.ByWhoEquipResume = EMByWho.ByHost;
                //GG.Equip.EqProcessPauseCmd();

            }
        }

        public void AddListTerminnalMessage(string moduleID, short tID, string msg)
        {
            lstTermsg.Items.Insert(0, new ListViewItem(new string[] {
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), tID.ToString(), msg}));

            if (lstTermsg.Items.Count > 100)
                lstTermsg.Items.RemoveAt(100);

            lblTerminnalMsg.Text = msg;
        }
        private void btnClearTerMsg_Click(object sender, EventArgs e)
        {
            lblTerminnalMsg.Text = string.Empty;
        }

        // 테스트 코드. 
        private void btnTest_Click(object sender, EventArgs e)
        {
            //GG.Equip.LD01.UpperArm.MovePortToArm(GG.Equip.Ports[0], 1, GG.Equip.Ports[0].Cassette.Glasss[0]);
            //GG.Equip.PortCmd.Add(new PortCmdInfo() { PortNo = 0, PortID = CimConst.UsePortList[0], CMD = EMPortCmd.Start, StepPortCmd = 10 });
            //GG.Equip.PortCmd.Add(new PortCmdInfo() { PortNo = 0, PortID = CimConst.HostPortList[0], CMD = EMPortCmd.Complete, StepIndexPortCmd = 10 });
            //GG.Equip.Ports[0].Cassette.Glasss[0].PanelState = EMPanelState.ABORTED;
            //GG.Equip.Scenario.StepAoiOnlineStateSetCmd = 10;

            //GG.Equip.BODY.EqProcessResumeCmd();
        }


        private void tmrState_Tick(object sender, EventArgs e)
        {
            tbStatus.Text = GetCurrentStatus();
        }

        // 메서드 - 상태 표시
        private string GetCurrentStatus()
        {
            Process p = Process.GetCurrentProcess();
            string s = string.Format(" 스레드 {0}, 핸들 {1}, 메모리 사용 {2}kb, 피크 메모리 사용 {3}kb, 버전 {4} 스켄타임 {5}ms ", p.Threads.Count, p.HandleCount, p.WorkingSet64 / 1024, p.PeakWorkingSet64 / 1024, Application.ProductVersion, _scenarioWorker.ScanTime);
            p.Close();
            p.Dispose();
            return s;
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            FrmTester ff = new FrmTester();
            ff.PLC = GG.PLC;
            ff.Show();
        }

        private void btnTest_Click_1(object sender, EventArgs e)
        {
            FrmMonitor ff = new FrmMonitor();
            ff.PLC = GG.PLC;
            ff.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmTester ff = new FrmTester();
            ff.PLC = GG.PLC;
            ff.Show();
        }

        private void btnOpCall_Click(object sender, EventArgs e)
        {
            GG.Equip._frmOperatorCall.Show();
        }

        private void btnSvLoad_Click(object sender, EventArgs e)
        {
            gridSv.Rows.Clear();

            foreach (SvItem item in GG.Equip.SvMgr.Values)
                gridSv.Rows.Add(item.SvID, item.SvName, item.SvValue, item.SvBit, item.PlcAddr.ToString());
        }

        private void btnDvLoad_Click(object sender, EventArgs e)
        {
            if (GG.PLC.ReadFromPLC(EQPW.DV_VALUE, EQPW.DV_VALUE.Length) != 0) { GG.Equip.EquipPlcConnected = false; }
            else { GG.Equip.EquipPlcConnected = true; }

            gridDv.Rows.Clear();
            foreach (DvItem item in GG.Equip.DvMgr)
            {
                string value = string.Empty;
                if (item.DvBit == "ASCII")
                {
                    value = GG.PLC.VirGetAscii(new PlcAddr(item.PlcAddr.Type, item.PlcAddr.Addr, 0, item.PlcAddr.Length)).Trim().ToString();
                }
                else if (item.DvBit == "INT16")
                    if (item.Name == "GLS_IN_TM")
                    {
                        string year = Math.Round(GG.PLC.VirGetShort(item.PlcAddr) / Math.Pow(10, item.DvUnit), 3).ToString();
                        double month = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 1) / Math.Pow(10, item.DvUnit), 3);
                        double day = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 2) / Math.Pow(10, item.DvUnit), 3);
                        double hour = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 3) / Math.Pow(10, item.DvUnit), 3);
                        double min = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 4) / Math.Pow(10, item.DvUnit), 3);
                        double sec = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 5) / Math.Pow(10, item.DvUnit), 3);

                        int iMonth = Convert.ToInt16(month);
                        int iday = Convert.ToInt16(day);
                        int ihour = Convert.ToInt16(hour);
                        int imin = Convert.ToInt16(min);
                        int isec = Convert.ToInt16(sec);

                        string strMonth = iMonth.ToString("D2");
                        string strday = iday.ToString("D2");
                        string strhour = ihour.ToString("D2");
                        string strmin = imin.ToString("D2");
                        string strsec = isec.ToString("D2");

                        value = year + strMonth + strday + strhour + strmin + strsec;
                    }
                    else
                    {
                        value = Math.Round(GG.PLC.VirGetShort(item.PlcAddr) / Math.Pow(10, item.DvUnit), 3).ToString();
                    }
                else
                    value = Math.Round(GG.PLC.VirGetInt32(item.PlcAddr) / Math.Pow(10, item.DvUnit), 3).ToString();


                gridDv.Rows.Add(item.Name, value, item.DvBit, item.PlcAddr.ToString());
            }
        }

        private void button7_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Middle)
            {
                button2.Enabled = !button2.Enabled;
                btnTest.Enabled = !button2.Enabled;
            }
        }

        private void btnRecipeLoad_Click(object sender, EventArgs e)
        {
            //if (GG.PLC.SyncMemory(EQPW.DV_VALUE, EQPW.DV_VALUE.Length) != 0) { GG.Equip.EquipPlcConnected = false; }
            //else { GG.Equip.EquipPlcConnected = true; }

            dgvRecipe.Rows.Clear();
            foreach (string key in GG.Equip.RmmMgr.DicRmmEqp.Keys)
            {
                RmmEqpItem eqRecipe = GG.Equip.RmmMgr.DicRmmEqp[key];

                dgvRecipe.Rows.Add(eqRecipe.Seq, eqRecipe.Name, eqRecipe.SoftRev, eqRecipe.LastTime);
            }

            dgvPPID.Rows.Clear();
            foreach (string key in GG.Equip.RmmMgr.DicRmmHost.Keys)
            {
                RmmHostItem eqPpid = GG.Equip.RmmMgr.DicRmmHost[key];

                dgvPPID.Rows.Add(eqPpid.Seq, eqPpid.Name, eqPpid.EqpRecipeSeq, eqPpid.SoftRev, eqPpid.LastTime);
            }

            lblCurrentPPID.Text = GG.Equip.RmmMgr.CurrentNewPPID;
            lblCurrentEqpRecipeID.Text = GG.Equip.RmmMgr.CurrentNewEqpId;
        }

        private void dgvRecipe_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string recipeId = (string)dgvRecipe.Rows[e.RowIndex].Cells[1].Value;

            dgvRecipeBody.Rows.Clear();
            if (GG.Equip.RmmMgr.DicRmmEqp.ContainsKey(recipeId))
            {
                foreach (ParamInfo info in GG.Equip.RmmMgr.ParamNames)
                {
                    if (info.No < GG.Equip.RmmMgr.DicRmmEqp[recipeId].Params.Count)
                        dgvRecipeBody.Rows.Add(info.No, info.Name, GG.Equip.RmmMgr.DicRmmEqp[recipeId].Params[info.No]);
                    else
                        dgvRecipeBody.Rows.Add(info.No, info.Name, "");
                }
            }
        }

        private void btnEcLoad_Click(object sender, EventArgs e)
        {
            gridEc.Rows.Clear();

            foreach (EcidItem item in GG.Equip.EcIdMgr.Values)
            {
                PlcAddr addr = EQPW.ECID_001_DEF_POS + (5 * (item.PlcSeq - 1));
                gridEc.Rows.Add(item.EcID, item.EcName, item.EcDefine, item.EcSLL, item.EcSUL, item.EcWLL, item.EcWUL, addr.ToString());
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmTester ff = new FrmTester();
            ff.PLC = GG.PLC;
            ff.Show();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.Common;

namespace DitInlineCim
{
    public partial class FrmCtrl : Form
    {

        public FrmCtrl()
        {
            InitializeComponent();
            InitalizeColor();
        }

        private void InitalizeColor()
        {
            CimColor.CoDEFAULT = this.colorDEFAULT.BackColor;
            CimColor.CoGREEN = this.colorGREEN.BackColor;
            CimColor.CoYELLOW = this.colorYELLOW.BackColor;
            CimColor.CoRED = this.colorRED.BackColor;
            CimColor.CoSKY = this.colorSKY.BackColor;
            CimColor.CoBLUE = this.colorBLUE.BackColor;
            CimColor.CoVIOLET = this.colorVIOLET.BackColor;
            CimColor.CoWHITE = this.colorWHITE.BackColor;
            CimColor.CoBLACK = this.colorBLACK.BackColor;
            CimColor.CoGRAY = this.colorGRAY.BackColor;
            CimColor.CoDGRAY = this.colorDGRAY.BackColor;
            CimColor.CoWATER = this.colorWATER.BackColor;
            CimColor.CoCONTROL = this.colorCONTROL.BackColor;

            CimColor.CoCon = this.colorCon.BackColor;
            CimColor.CoDis = this.colorDis.BackColor;

            //Remote / Local / Offline
            CimColor.CoMCMD_REMOTE_B = this.colorMCMD_REMOTE.BackColor;
            CimColor.CoMCMD_REMOTE_F = this.colorMCMD_REMOTE.ForeColor;
            CimColor.CoMCMD_LOCAL_B = this.colorMCMD_LOCAL.BackColor;
            CimColor.CoMCMD_LOCAL_F = this.colorMCMD_LOCAL.ForeColor;
            CimColor.CoMCMD_OFFLINE_B = this.colorMCMD_OFFLINE.BackColor;
            CimColor.CoMCMD_OFFLINE_F = this.colorMCMD_OFFLINE.ForeColor;
            CimColor.CoMCMD_DEFAULT_B = this.colorMCMD_DEFAULT.BackColor;
            CimColor.CoMCMD_DEFAULT_F = this.colorMCMD_DEFAULT.ForeColor;

            //EQST ID (Equipment State)
            CimColor.CoEQUIPST_NORMAL_B = this.colorEQST_NORMAL.BackColor;
            CimColor.CoEQUIPST_NORMAL_F = this.colorEQST_NORMAL.ForeColor;
            CimColor.CoEQUIPST_FAULT_B = this.colorEQST_FAULT.BackColor;
            CimColor.CoEQUIPST_FAULT_F = this.colorEQST_FAULT.ForeColor;
            CimColor.CoEQUIPST_PM_B = this.colorEQST_PM.BackColor;
            CimColor.CoEQUIPST_PM_F = this.colorEQST_PM.ForeColor;
            CimColor.CoEQUIPST_DEFAULT_B = this.colorEQST_DEFAULT.BackColor;
            CimColor.CoEQUIPST_DEFAULT_F = this.colorEQST_DEFAULT.ForeColor;

            //PRST ID (Equipment Process State)
            CimColor.CoPROCST_INIT_B = this.colorPRST_INIT.BackColor;
            CimColor.CoPROCST_INIT_F = this.colorPRST_INIT.ForeColor;
            CimColor.CoPROCST_IDLE_B = this.colorPRST_IDLE.BackColor;
            CimColor.CoPROCST_IDLE_F = this.colorPRST_IDLE.ForeColor;
            CimColor.CoPROCST_SETUP_B = this.colorPRST_SETUP.BackColor;
            CimColor.CoPROCST_SETUP_F = this.colorPRST_SETUP.ForeColor;
            CimColor.CoPROCST_READY_B = this.colorPRST_READY.BackColor;
            CimColor.CoPROCST_READY_F = this.colorPRST_READY.ForeColor;
            CimColor.CoPROCST_EXECUTE_B = this.colorPRST_EXECUTE.BackColor;
            CimColor.CoPROCST_EXECUTE_F = this.colorPRST_EXECUTE.ForeColor;
            CimColor.CoPROCST_PAUSE_B = this.colorPRST_PAUSE.BackColor;
            CimColor.CoPROCST_PAUSE_F = this.colorPRST_PAUSE.ForeColor;
            CimColor.CoPROCST_DEFAULT_B = this.colorPRST_DEFAULT.BackColor;
            CimColor.CoPROCST_DEFAULT_F = this.colorPRST_DEFAULT.ForeColor;

            //Panel Status
            CimColor.CoPNLST_EMPTY_B = this.colorPNST_EMPTY.BackColor;
            CimColor.CoPNLST_EMPTY_F = this.colorPNST_EMPTY.ForeColor;
            CimColor.CoPNLST_IDLE_B = this.colorPNST_IDLE.BackColor;
            CimColor.CoPNLST_IDLE_F = this.colorPNST_IDLE.ForeColor;
            CimColor.CoPNLST_STP_B = this.colorPNST_STP.BackColor;
            CimColor.CoPNLST_STP_F = this.colorPNST_STP.ForeColor;
            CimColor.CoPNLST_PROCESSING_B = this.colorPNST_PROCESSING.BackColor;
            CimColor.CoPNLST_PROCESSING_F = this.colorPNST_PROCESSING.ForeColor;
            CimColor.CoPNLST_DONE_B = this.colorPNST_DONE.BackColor;
            CimColor.CoPNLST_DONE_F = this.colorPNST_DONE.ForeColor;
            CimColor.CoPNLST_ABORTING_B = this.colorPNST_ABORTING.BackColor;
            CimColor.CoPNLST_ABORTING_F = this.colorPNST_ABORTING.ForeColor;
            CimColor.CoPNLST_ABORTED_B = this.colorPNST_ABORTED.BackColor;
            CimColor.CoPNLST_ABORTED_F = this.colorPNST_ABORTED.ForeColor;
            CimColor.CoPNLST_CANCELED_B = this.colorPNST_CANCELED.BackColor;
            CimColor.CoPNLST_CANCELED_F = this.colorPNST_CANCELED.ForeColor;

            //On / Off Status
            CimColor.CoON_B = this.colorON.BackColor;
            CimColor.CoON_F = this.colorON.ForeColor;
            CimColor.CoOFF_B = this.colorOFF.BackColor;
            CimColor.CoOFF_F = this.colorOFF.ForeColor;
        }
    }
}

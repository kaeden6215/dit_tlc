﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim
{
    public enum EmModuleType
    {
        VY01,
        UV01,
        UV02,
        UV03,
        UV04,
        UV05,
        UV06,
        UV07,
        UV08,
        UV09,
        UV10,
        UV11,
    }
    public enum EmEinMode
    {
        Host = 0,
        InLine = 1
    }
    public enum EmGlassPosition
    {
        LOADING = 1,  //로딩 완료 
        UNLOADING = 2,   //SIO 시작 지점. 
        UNLOADCOMPLETE = 3, //장비완전빠져나감
        Broken = 2,
        UnBroken = 2,
        TO_LOWER1 = 5,
        TO_LOWER2 = 5,
    }

    public enum EMMCmd
    {
        OFFLINE = 1,
        ONLINE_LOCAL = 2,
        ONLINE_REMOTE = 3,
        Unknown
    }
    public enum EMEquipState
    {
        Normal = 1,
        Fault = 2,
        PM = 3,
        NotManage = 255,
        Unknown = 99 
    }
    public enum EMProcState
    {   
        Init = 1,
        Idle = 2,
        Setup = 3, 
        Ready = 4,
        Execute = 5,
        Pause = 6,
        NotManage = 255,
        Unknown = 99
    }
   

    public enum EMByWho
    {
        ByHost = 1,
        ByOper = 2,
        ByEqp = 3,
		ByIndexer = 4,
        Unknown =99
    }

    //Index PNST (Panel State)
    public enum EMPanelState
    {
        EMPTY = 0,
        IDLE = 1,
        STP = 2,
        PROCESSING = 3,
        DONE = 4,
        ABORTING = 5,
        ABORTED = 6,
        CANCELED = 7
    }

    public enum EmGlassPosi
    {
        UPPER_ARM = 1,
        LOWER_ARM = 2,
        STAGE = 3,
        PORT = 4,
        Unknown = 5,
        NOT_EXIST
    }


    public enum EMRCmd
    {
        // Equipment Command
        EQUIPMENT_PAUSE = 51,
        EQUIPMENT_RESUME = 52,
        EQUIPMENT_PM = 53,
        EQUIPMENT_NORMAL = 54,

        EQUIPMENT_CYCLE_STOP = 57,
        CELL_JOB_PROCESS_START = 201,
        CELL_JOB_PROCESS_CANCLE = 202,
        
    }

    public enum EMPortState
    {
        EMPTY = 0,
        IDLE = 1,
        WAIT = 2,
        RESERVE = 3,
        BUSY = 4,
        COMPLETE = 5,
        ABORT = 6,
        CANCEL = 7,
        PAUSE = 8,
        DISABLE = 9,
        ERROR = 98,
        UNKNOW = 99
    }
    public enum EMSortType
    {
        DumpFill = 0,
        Dump = 1,
        SlotNumber = 2,
        LOTID = 3
    }

    public enum EMCstDemand
    {
        NormalCST = 0,
        EmptyCST = 1,
    }

    public enum EMCstType
    {
        FullCst = 1,
        QuarterCst = 2,
        CellCst = 3,
        MaskCst = 4,
        MaskCase = 5,
        WireCst = 6,
        BoxCst = 7,
        TrayCst = 8,
        SmallBox = 9,
        LargeBox = 10,
        FilmRoll = 11,
        FilmCst = 12,
        PaperCst = 13,
        //OctaCst = 14,
    }


    public class MachinCmd
    {
        public static string START = "S";
        public static string STOP = "O";
        public static string PAUSE = "P";
        public static string RESUME = "R";
        public static string AUTO = "A";
        public static string MANUAL = "M";
        public static string RESET = "E";
    }

}




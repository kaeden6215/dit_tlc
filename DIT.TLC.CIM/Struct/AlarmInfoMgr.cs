﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DitInlineCim.Struct
{
    public class AlarmInfoMgr : SortedList<int, AlarmInfo>
    {
        public const string FILE_NAME = "DitCim.AlarmInfo.csv";
        public void LoadFromFile(string settingPath)
        {
            string fileName = Path.Combine(settingPath, FILE_NAME);
            
            //try
            //{
                foreach (string line in File.ReadAllLines(fileName, Encoding.ASCII))
                {
                    string trimLine = line.Trim();
                    if (trimLine.Contains("="))
                    {
                        //PLC=26
                        //0,SVID 1 ,PHOTO_IONIZER_POWER_ ON_OFF
                    
                    }
                    else if (trimLine.Length < 2)
                    {
                    }
                    else if (trimLine[0] == '#')
                    {
                    }
                    else
                    {  
                        string[] items = line.Split(',');
                        
                        if (items.Length != 6) continue;

                        AlarmInfo info = new AlarmInfo();
                        info.No = int.Parse(items[0]);
                        //20180907
                        info.AlarmWarningHappen = items[1];

                        if (string.IsNullOrEmpty(items[2]))
                            items[2] = "0";
                        info.Code = short.Parse(items[2]);
                        info.ID = short.Parse(items[3]);
                        info.ModuleID = items[4];
                        info.Text = items[5];

                        if (this.ContainsKey(info.No) == false)
                            this.Add(info.No, info);
                    }
                }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Erorr Alarm Info File", ex);
            //}
        }
    }
}

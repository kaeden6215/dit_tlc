﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using DitInlineCim.PLC;

namespace DitInlineCim.Struct.DV
{
    public class DvItem
    {        
        public string Name { get; set; }
        public string Value { get; set; }
        public string DvBit { get; set; }

        public int DvNo { get; set; }
        public double DvUnit { get; set; }
        public int DvID { get; set; }
        public string DvType { get; set; }
        public PlcAddr PlcAddr { get; set; }
        public DvItem()
        {
            DvNo = 0;
            DvID = 0;
            DvUnit = 1f;
            Value = "";
            Name = "";
            DvType = "";
        }

        

    }
}

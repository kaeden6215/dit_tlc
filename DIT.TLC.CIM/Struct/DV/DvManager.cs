﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DitInlineCim.PLC;
using Dit.Framework.PLC;

namespace DitInlineCim.Struct.DV
{
    public class DvManager : List<DvItem>
    {
        public const string FILE_NAME = "DitCim.DvInfo.csv";
        public void LoadFromFile(string settingPath)
        {
            string fileName = Path.Combine(settingPath, FILE_NAME);
            try
            {
                foreach (string line in File.ReadAllLines(fileName, Encoding.ASCII))
                {
                    string trimLine = line.Trim();

                    if (trimLine.Contains("="))
                    {
                        //PLC=26
                        //0,SVID 1 ,PHOTO_IONIZER_POWER_ ON_OFF,
                    }
                    else if (trimLine.Length < 2)
                    {
                    }
                    else if (trimLine[0] == '#')
                    {
                    }
                    else
                    {
                        
                        string[] items = trimLine.Split(',');

                        int dvno = int.Parse(items[0]);
                        PlcAddr addr = PlcAddr.Parsing(items[1]);
                        addr.Length = int.Parse(items[2]);
                        int dvid = int.Parse(items[3]);
                        double dvunit = double.Parse(items[5]);
                        string dvBit = items[6];

                        DvItem item = new DvItem();
                        item.PlcAddr = addr;
                        item.Name = items[4].Trim();
                        item.Value = "0";
                        item.DvID = dvid;
                        item.DvNo = dvno;
                        item.DvUnit = double.Parse(items[5]);
                        item.DvBit = dvBit;

                        this.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erorr Dv File", ex);
            }
        }

    }
}

﻿using DitInlineCim.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct
{
    // 오정석
    public class CassetteInfo
    {
        public short HotDevice;
        public short HotLevel;

        public string PortID;
        public short PortState;
        public short PortType;
        public string PortMode;
        public short SortType;

        public string CstID;
        public short CstType;
        public string MapStif;
        public string CurStif;
        public short BatchOrder;

        public string ModuleId;
        public short MCMD;
        public short ModuleState;
        public short ProcStae;
        public short ByWho;
        public string OperId;

         
        public string LotID;
        public string TotalCellQuantity;
        public string Model;
        public string ProductId;
        public string ProductType;
        public string BatchId;
        public string RunSpecId;
        public string FlowId;
        public string StepId;
        public string PPID;
        public string Result;
        public string Code;
        public string Comment;
        public string HGlassId;
                
        public CellInfo[] Cells = new CellInfo[GG.MAX_GLASS];
        public bool[] ExistsGlasss = new bool[GG.MAX_GLASS];


        public CassetteInfo()
        {
            CstID = string.Empty;
            CstType = 0;
            MapStif = "0000000000000000000000000";
            CurStif = "0000000000000000000000000";
            BatchOrder = 0;
            PortID = string.Empty;
            for (int iPos = 0; iPos < GG.MAX_GLASS; iPos++)
            {
                Cells[iPos] = new CellInfo();
                Cells[iPos].SlotID = string.Format("{0:0#}", iPos);
                Cells[iPos].PanelState_ = EMPanelState.EMPTY;
                ExistsGlasss[iPos] = false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct
{
    public class EoidItem
    {   
        public short EOID;
        public string EOMD;
        public short EOV;
        public short EAC;
        public short PmAck;
        public List<short> EOV_USABLE;
    } 
}

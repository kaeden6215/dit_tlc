﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DitInlineCim.Lib;
using DitInlineCim.Common;
using DitInlineCim.PLC;
using Dit.Framework.Ini;

namespace DitInlineCim.Struct
{
    public class EOIDInfoMgr : List<EoidItem>
    {
        public const short ComponentTrace = 1;
        public const short EquipmentStateTrace = 2;
        public const short EquipmentProcessStateTrace = 3;

        public const short MCCReportingMode = 17;

        public const short ProcessControlPriority = 202;

        public const string FILE_NAME = "DitCim.Eoid.ini";
        public EOIDInfoMgr()
        {
            this.Add(new EoidItem() { EOID = ComponentTrace, EOMD = "1", PmAck = 2, EOV = 1, EOV_USABLE = new List<short> { 1, 2 } });
            this.Add(new EoidItem() { EOID = ComponentTrace, EOMD = "2", PmAck = 2, EOV = 1, EOV_USABLE = new List<short> { 1, 2 } });

            this.Add(new EoidItem() { EOID = EquipmentStateTrace, EOMD = "0", PmAck = 2, EOV = 1, EOV_USABLE = new List<short> { 0, 1, 2 } });
            this.Add(new EoidItem() { EOID = EquipmentProcessStateTrace, EOMD = "0", PmAck = 2, EOV = 1, EOV_USABLE = new List<short> { 0, 1, 2 } }); 
            this.Add(new EoidItem() { EOID = ProcessControlPriority, EOMD = "1", PmAck = 2, EOV = 0, EOV_USABLE = new List<short> { 0, 1 } });

        }
        public void LoadFromFile(string settingPath)
        {
            string fileName = Path.Combine(settingPath, FILE_NAME);
            IniReader iniReader = new IniReader(fileName);

            foreach (EoidItem item in this)
            {
                string eoidkey = string.Format("EOID_{0}_{1}", item.EOID, item.EOMD);
                item.EOV = (short)iniReader.GetInteger("EOID", eoidkey, -1);

                if (item.EOV_USABLE.Contains(item.EOV) == false)
                    throw new Exception(string.Format("Error EOID {0}", item.EOID));
            }
        }
        public void SaveToFile(string settingPath)
        {
            string fileName = Path.Combine(settingPath, FILE_NAME);
            IniReader iniReader = new IniReader(fileName);

            foreach (EoidItem item in this)
            {
                string eoidkey = string.Format("EOID_{0}_{1}", item.EOID, item.EOMD);
                iniReader.SetInteger("EOID", eoidkey, item.EOV);
            }
        }
        public List<short> GetEoids()
        {
            return this.Select(f => f.EOID).Distinct().ToList();
        }
        public List<EoidItem> GetEomds(short eoid)
        {
            return this.Where(f => f.EOID == eoid).ToList();
        }
        public bool ContainEOID(short eoid)
        {
            EoidItem info = this.FirstOrDefault(f => f.EOID == eoid);
            return info != null ? true : false;
        }
        public bool ContainEOID(short eoid, string eomd)
        {
            EoidItem info = this.FirstOrDefault(f => f.EOID == eoid && f.EOMD == eomd);
            return info != null ? true : false;
        }
        public bool ValidateEOV(short eoid, string eomd, short eov)
        {
            EoidItem info = this.FirstOrDefault(f => f.EOID == eoid && f.EOMD == eomd);
            if (info == null) 
                throw new Exception(string.Format("not found eoid ={0}, eomd={1}", eoid, eomd));
            return info.EOV_USABLE.Contains(eov);
        }
        public short GetEov(short eoid, string eomd)
        {
            EoidItem info = this.FirstOrDefault(f => f.EOID == eoid && f.EOMD == eomd);
            if (info == null) 
                throw new Exception(string.Format("not found eoid ={0}, eomd={1}", eoid, eomd));
            return info.EOV;
        }
        public void SetEov(short eoid, string eomd, short eov)
        {
            EoidItem info = this.FirstOrDefault(f => f.EOID == eoid && f.EOMD == eomd);
            if (info == null) 
                throw new Exception(string.Format("not found eoid ={0}, eomd={1}", eoid, eomd));
            info.EOV = eov;
        }

        public static short APCOnOff { get; set; }

        public void LoadFromPlc()
        {
            //short einOnOff = GG.PLC.VirGetShort(EQPW.EIN_USED);
            //SetEov(ProcessControlPriority, "1", einOnOff);

        }
    }
}

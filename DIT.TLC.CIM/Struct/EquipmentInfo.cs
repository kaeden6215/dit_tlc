﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Common;
using DitInlineCim.PLC;
using DitInlineCim.UI;
using System.Windows.Forms;
using System.Drawing;
using DitInlineCim.HSMS;
using DitInlineCim.Log;
using DitInlineCim.Lib;
using DitInlineCim.Struct.SV;
using DitInlineCim.EventArg;
using DitInlineCim.Struct.EC;
using DitInlineCim.Struct.DV;
using DitInlineCim.Struct.RMM;
using Dit.Framework.PLC;
using Dit.Framework.Comm;

namespace DitInlineCim.Struct
{
    public class EquipmentInfo : ModuleInfo
    {
        #region 프로퍼티 

        public EMMCmd MCMD { get; set; }
        public EMMCmd MCMD_OLD { get; set; }
        public EmEinMode EinMode { get; set; }
        public ModuleInfo UV01 { get; set; }
        public ModuleInfo UV02 { get; set; }
        public ModuleInfo UV03 { get; set; }
        public ModuleInfo UV04 { get; set; }
        public ModuleInfo UV05 { get; set; }
        public ModuleInfo UV06 { get; set; }
        public ModuleInfo UV07 { get; set; }
        public ModuleInfo UV08 { get; set; }

        public FrmOperatorCall _frmOperatorCall = new FrmOperatorCall();


        public CimSetting EquipSetting { get; set; }
        public int FlagProcStateHost { get; set; }

        public AlarmInfoMgr AlarmInfoMgr { get; set; }

        public LockList<AlarmInfo> AlarmInfosOccur { get; set; }

        public BodyCommand BodyCmd { get; set; }
        public EOIDInfoMgr EoidMgr { get; set; }
        
        public List<TerminalMessageInfo> TerminnalMessagesCimPc { get; set; }

        public CimSetting Setting { get; set; }
        public string OperatorID { get; set; }

        public SvManager SvMgr { get; set; }
        public TdManager TdMgr { get; set; }
        public EcManager EcIdMgr { get; set; }
        public RmmManager RmmMgr { get; set; }
        public DvManager DvMgr { get; set; }

        public short Year { get; set; }
        public short Month { get; set; }
        public short Hour { get; set; }
        public short Day { get; set; }
        public short Minute { get; set; }
        public short Second { get; set; }
       
        public ScenarioWorker Worker { get; set; }
        
        //HOST 접속후 ON  
        public bool HostConnectedOneOff { get; set; }

        #endregion

        //메소드 - 생성자 
        public EquipmentInfo(string eqpid, int portCount, FrmMain uiMain, string fromModuleId, string toModuleId)
            : base(uiMain, eqpid)
        {
            UiMain = uiMain;

            this.FromModuleID1 = fromModuleId;
            this.ToModuleID1 = toModuleId;

            EquipState = EMEquipState.Normal;
            ProcState = EMProcState.Idle;

            //INLINE 설비 경우 REMOTE 모드만 있음. 
            MCMD = EMMCmd.ONLINE_REMOTE;
            MCMD_OLD = EMMCmd.OFFLINE;
            EinMode = EmEinMode.InLine;

            ModuleID = eqpid;
            OperatorID = string.Empty;

            Setting = new CimSetting();
            AlarmInfosOccur = new LockList<AlarmInfo>();

            //PORT 처리. 
            EoidMgr = new EOIDInfoMgr();

            SvMgr = new SvManager();
            Setting = new CimSetting();
            AlarmInfoMgr = new AlarmInfoMgr();
            TdMgr = new TdManager();

            EcIdMgr = new EcManager();
            RmmMgr = new RmmManager();
            DvMgr = new DvManager();
        }

        public void LoadSetting(string settingPath)
        {
            SvMgr.LoadFromFile(settingPath);
            DvMgr.LoadFromFile(settingPath);
            EoidMgr.LoadFromFile(settingPath);
            AlarmInfoMgr.LoadFromFile(settingPath);
            RmmMgr.LoadFromFile(settingPath);
            EcIdMgr.LoadFromFile(settingPath);
        }

        public short SetTime(short year, short month, short day, short hour, short minutes, short second)
        {
            if (UserTimeManager.SetTime(year, month, day, hour, minutes, second) == false)
                return 6;

            GG.Equip.Year = year;
            GG.Equip.Month = month;
            GG.Equip.Day = day;
            GG.Equip.Hour = hour;
            GG.Equip.Minute = minutes;
            GG.Equip.Second = second;

            //PLC 시간 변경 필요 
            if (GG.Equip.Worker.DateTimeSetCmd.StepProc == 0)
                GG.Equip.Worker.DateTimeSetCmd.RunCmd();
            else
                return 6;

            return 0;
        }
        public void SetMCMD(EMMCmd mcmd, EMByWho byWho)
        {
            this.MCMD_OLD = this.MCMD;
            this.MCMD = mcmd;
            this.MCMDByWho = byWho;
            string reasonCode = string.Empty;
            if (mcmd == EMMCmd.ONLINE_LOCAL)
            {
                HSMS_SEND.S6F11_EquipEvent(S6F11_CEID.CHANGE_TO_ONLINE_LOCAL_MODE,
                   this.ModuleID,
                    GG.Equip.MCMD,
                    GG.Equip.MCMD_OLD,
                    this.EquipState,
                    this.EquipStateOld,
                    this.ProcState,
                    this.ProcStateOld,
                    reasonCode,
                    byWho);
            }
            else if (mcmd == EMMCmd.ONLINE_REMOTE)
            {
                HSMS_SEND.S6F11_EquipEvent(S6F11_CEID.CHANGE_TO_ONLINE_LOCAL_MODE,
                   this.ModuleID,
                    GG.Equip.MCMD,
                    GG.Equip.MCMD_OLD,
                    this.EquipState,
                    this.EquipStateOld,
                    this.ProcState,
                    this.ProcStateOld,
                    reasonCode,
                    byWho);
            }
            else if (mcmd == EMMCmd.OFFLINE)
            {
                //HSMS_SEND.S6F11_EquipEvent(S6F11_CEID.CHANGE_TO_OFFLINE_MODE,
                //   GG.Equip.ModuleID,
                //   GG.Equip.MCMD,
                //   GG.Equip.EquipState,
                //   GG.Equip.ProcState,
                //   (short)GG.Equip.MCMD,
                //   (short)GG.Equip.MCMD_OLD,
                //   byWho);
            }
            UiMain.UIMCmd();
        }
        public void SetEoid(List<EoidItem> lstEoid, EMByWho byWho)
        {
            List<EoidItem> changedLstEoid = new List<EoidItem>();
            foreach (EoidItem info in lstEoid)
            {
                if (info.EOV != GG.Equip.EoidMgr.GetEov(info.EOID, info.EOMD))
                {
                    GG.Equip.EoidMgr.SetEov(info.EOID, info.EOMD, info.EOV);
                    changedLstEoid.Add(info);
                }
            }

            GG.Equip.EoidMgr.SaveToFile(GG.SETTING_PATH);

            HSMS_SEND.S6F11_EquipParamterEvent(S6F11_CEID.PARAMETER_CHANGED_EOID, this, changedLstEoid, new List<EcidItem>(), byWho);
            UiMain.UIUpdateEoid();
        }
        public void SetRCMD(string moduleid, string rCode, EMRCmd mrcmd, EMByWho byWho)
        {
            //PLC에 상태 변경...
            if (moduleid == GG.Equip.ModuleID)
            {
                if (mrcmd == EMRCmd.EQUIPMENT_PM)
                {
                    GG.Equip.CmdEquipReasonCode = rCode;
                    GG.Equip.CmdEquipStatePMByWho = byWho;
                    GG.Equip.Worker.EqpStatePmChangeCmd.RunCmd();
                }
                else if (mrcmd == EMRCmd.EQUIPMENT_NORMAL)
                {
                    GG.Equip.CmdEquipReasonCode = rCode;
                    GG.Equip.CmdEquipStateNormalByWho = byWho;
                    GG.Equip.Worker.EqpStateNormalChangeCmd.RunCmd();
                }
                else if (mrcmd == EMRCmd.EQUIPMENT_PAUSE)
                {
                    GG.Equip.CmdEquipReasonCode = rCode;
                    GG.Equip.CmdEquipProcessPauseByWho = byWho;
                    GG.Equip.Worker.EqpProcPauseCmd.RunCmd();
                }
                else if (mrcmd == EMRCmd.EQUIPMENT_RESUME)
                {
                    GG.Equip.CmdEquipReasonCode = rCode;
                    GG.Equip.CmdEquipProcessPauseByWho = byWho;
                    GG.Equip.Worker.EqpProcResumeCmd.RunCmd();
                }
                else if (mrcmd == EMRCmd.EQUIPMENT_CYCLE_STOP)
                {
                    GG.Equip.CmdEquipReasonCode = rCode;
                    GG.Equip.CmdEquipCycleStopByWho = byWho;
                    GG.Equip.Worker.EqpStateCycleStopRequset.RunCmd();
                }
            }
        }
        public void SetAlarm(short setCode, int alarmNo)
        {
            if (AlarmInfoMgr.ContainsKey(alarmNo))
            {
                AlarmInfo info = AlarmInfoMgr[alarmNo];
                SetAlarm(setCode, GG.Equip.ModuleID, info.Code, info.ID, info.Text, info.AlarmWarningHappen);
                AlarmLog(alarmNo, info.Text);
            }
            else
            {
                SetAlarm(setCode, GG.Equip.ModuleID, (short)0, (short)alarmNo, string.Empty, string.Empty);
            }
        }
        public void SetAlarmClear(string moduleid)
        {
            foreach (AlarmInfo info in GG.Equip.AlarmInfosOccur)
                SetAlarm(0, GG.Equip.ModuleID, info.Code, info.ID, info.Text, info.AlarmWarningHappen);

            var lst = GG.Equip.AlarmInfosOccur;
            lst.ToList().ForEach(f => GG.Equip.AlarmInfosOccur.Remove(f));
        }
        public void SetAlarm(short occur, string moduleID, short alarmCode, short alarmID, string alarmText, string alarmWarningHappen)
        {
            string alarmTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            AlarmInfo info = new AlarmInfo() { Occur = occur, ModuleID = moduleID, Code = alarmCode, Time = alarmTime, ID = alarmID, Text = alarmText, AlarmWarningHappen = alarmWarningHappen };
            if (occur == 1)
            {
                //기존 발생한 알람이면 넘어감. 
                if (AlarmInfosOccur.Count(f => f.ID == info.ID && f.ID == info.ID) > 0) return;

                AlarmInfosOccur.Add(info);
                HSMS_SEND.S5F1(occur, moduleID, alarmCode, alarmID, alarmText, alarmTime, alarmWarningHappen);
            }
            else
            {
                List<AlarmInfo> lstRemove = AlarmInfosOccur.Where(f => f.ID == alarmID && f.ModuleID == moduleID).ToList();
                foreach (AlarmInfo info2 in lstRemove)
                    AlarmInfosOccur.Remove(info2);
                HSMS_SEND.S5F1(occur, moduleID, alarmCode, alarmID, alarmText, alarmTime, alarmWarningHappen);
            }


            UiMain.UIAddAlarmHistoryList(occur, alarmCode, alarmID, alarmText, moduleID);
        }


        public void AddOperatorCallToCimPC(short tID, string[] msg, string moduleID)
        {
            Size scrSize = Screen.PrimaryScreen.Bounds.Size;

            _frmOperatorCall.AddOperatorCall(tID, msg, moduleID);
            _frmOperatorCall.StartPosition = FormStartPosition.CenterScreen;
            _frmOperatorCall.Location = new Point((scrSize.Width - _frmOperatorCall.Width) / 2, (scrSize.Height - _frmOperatorCall.Height) / 2);
            _frmOperatorCall.Show();
        }
        public void AddOperatorCallToEqpUi(short tID, string[] msg, string moduleID)
        {
            GG.Equip.OperacalMsgToEqp = msg;
            GG.Equip.Worker.OpCallCmd.RunCmd();
        }

        public string[] OperacalMsgToEqp { get; set; }
        public string TerminalMessageToEqp { get; set; }

        public void AddTerminalMessageToCimPC(short tID, string msg, string moduleID)
        {
            UiMain.AddListTerminnalMessage(moduleID, tID, msg);
        }
        public void AddTerminalMessageToEqpUi(short tID, string msg, string moduleID)
        {
            TerminalMessageToEqp = msg;
            GG.Equip.Worker.TerminalMessageCmd.RunCmd();
        }
        
        public void SetCurrentPPIDChange(string strOld, string strNew)
        {
            if (GG.Equip.RmmMgr.DicRmmHost.ContainsKey(strNew))
            {
                var item3 = GG.Equip.RmmMgr.DicRmmEqp.Values.FirstOrDefault(f => f.Seq == GG.Equip.RmmMgr.DicRmmHost[strNew].EqpRecipeSeq);

                GG.Equip.RmmMgr.CurrentOldPPID = strOld;
                GG.Equip.RmmMgr.CurrentNewPPID = strNew;
                GG.Equip.RmmMgr.CurrentNewEqpId = (item3 == null) ? string.Empty : item3.Name;

                Logger.Append(string.Format("미등록된 PPID로 변경됨 OldPPID ={0} , NewPPID = {1}", strOld, strNew));
            }
            string[] ayName = null;
            string[] ayValue = null;

            ayName = new string[2] { "OLDPPID", "NEWPPID" };
            ayValue = new string[2] { "", "" };

            ayValue[0] = strOld;
            ayValue[1] = strNew;

            HSMS_SEND.S6F11_EquipSpecControlEvent(S6F11_CEID.CURRENT_PPID_CHAGNE, this, ayName, ayValue, EMByWho.ByEqp);
        }
      
        public void CmdEquipStatePM(EMByWho byWho)
        {
            CmdEquipStatePMByWho = byWho;
            if (GG.Equip.Worker.EqpStatePmChangeCmd.StepProc == 0)
                GG.Equip.Worker.EqpStatePmChangeCmd.RunCmd();
        }
        public void CmdEquipStateNormal(EMByWho byWho)
        {
            CmdEquipStateNormalByWho = byWho;
            if (GG.Equip.Worker.EqpStateNormalChangeCmd.StepProc == 0)
                GG.Equip.Worker.EqpStateNormalChangeCmd.RunCmd();
        }
        
        public void RecipeCreateDeleteModifyEvent()
        {
            RmmManager changeRmmMgr = new RmmManager();
            if (!changeRmmMgr.LoadFromPlc())
            {
                Logger.Append(string.Format("RECIPE_CDM, DIDN'T READ RMM_INFO.INI [{0}]", GG.RMM_PATH));
            }
            else
            {
                if (changeRmmMgr.ActionMode == 1)
                {
                    //생성
                    HSMS_SEND.S7F107_CREATE(changeRmmMgr);
                }
                else if (changeRmmMgr.ActionMode == 2)
                {
                    //삭제
                    HSMS_SEND.S7F107_DELETE(changeRmmMgr);
                }
                else if (changeRmmMgr.ActionMode == 3)
                {
                    //수정
                    HSMS_SEND.S7F107_MODIFY(changeRmmMgr);
                }
            }
        }
        public List<CellInfo> GetListGlassInfo()
        {
            List<CellInfo> lstGlassInfo = new List<CellInfo>();

            return lstGlassInfo;
        }

        public CellInfo GetCellInfoFromPLC(PlcAddr plcAddr)
        {
            GG.PLC.ReadFromPLC(plcAddr, plcAddr.Length);
            CellInfo info = new CellInfo();

            for (int iPos = 0; iPos < 20; iPos++)
                info.RunLine[iPos] = GG.PLC.VirGetShort(EQPW.RUNLINE_POS + plcAddr.Addr + iPos);

            for (int iPos = 0; iPos < 4; iPos++)
                info.UniqueID[iPos] = GG.PLC.VirGetShort(EQPW.UNIQUEID_POS + plcAddr.Addr + iPos);

            info.Pair_H_PanelID = GG.PLC.VirGetAsciiTrim(EQPW.PAIR_H_PANELID_POS + plcAddr.Addr);
            info.Pair_E_PanelID = GG.PLC.VirGetAsciiTrim(EQPW.PAIR_E_PANELID_POS + plcAddr.Addr);
            info.Pair_Grade = GG.PLC.VirGetShort(EQPW.PAIR_GRADE_POS + plcAddr.Addr);

            for (int iPos = 0; iPos < 4; iPos++)
                info.Pair_UniqueID[iPos] = GG.PLC.VirGetShort(EQPW.PAIR_UNIQUEID_POS + plcAddr.Addr + iPos);

            info.FlowRecipe = GG.PLC.VirGetAsciiTrim(EQPW.FLOW_RECIPE + plcAddr.Addr);
            info.Reserved = GG.PLC.VirGetAsciiTrim(EQPW.RESERVED_POS + plcAddr.Addr);
            info.BitsSignals = GG.PLC.VirGetShort(EQPW.BITS_SIGNALS_POS + plcAddr.Addr);
            info.ReferData = GG.PLC.VirGetAsciiTrim(EQPW.REFER_DATA_POS + plcAddr.Addr);

            return info;
        }
        public PortInfo GetPortInfoFromPLC(PlcAddr plcAddr)
        {
            GG.PLC.ReadFromPLC(plcAddr, plcAddr.Length);
            PortInfo info = new PortInfo();
        
            return info;
        }
        //오정석
        public CassetteInfo GetCassetteInfoFromPLC(PlcAddr plcAddr)
        {
            GG.PLC.ReadFromPLC(plcAddr, plcAddr.Length);
            CassetteInfo info = new CassetteInfo();

            return info;
        }
        public LotInfo GetLotInfoFromPLC(PlcAddr plcAddr)
        {
            GG.PLC.ReadFromPLC(plcAddr, plcAddr.Length);
            LotInfo info = new LotInfo();
            
            return info;
        }

        public EMByWho CmdEquipProcessResumeByWho { get; set; }
        public EMByWho CmdEquipProcessPauseByWho { get; set; }
        public EMByWho CmdEquipStateNormalByWho { get; set; }
        public EMByWho CmdEquipStatePMByWho { get; set; }
        public EMByWho CmdEquipCycleStopByWho { get; set; }
        public string CmdEquipReasonCode { get; set; }
        private bool _hostConnected = false;
        private bool _equipPlcConnected = false;
        public bool HostConnected
        {
            get { return _hostConnected; }
            set
            {
                _hostConnected = value;
                UiMain.UIComConnState();
            }
        }
        public bool EquipPlcConnected
        {
            get { return _equipPlcConnected; }
            set
            {
                _equipPlcConnected = value;
                UiMain.UIComConnState();
            }
        }

        public void CmdEquipModifyRecipe(string ppid, string softRev, short ppidType, NewPPIDItem newPpId)
        {
            this.RmmMgr.ModifyRecipe(ppid, softRev, ppidType, newPpId);
        }
        public List<DvItem> GetListGlassDCollData()
        {
            if (GG.PLC.ReadFromPLC(EQPW.DV_VALUE, EQPW.DV_VALUE.Length) != 0) { GG.Equip.EquipPlcConnected = false; }
            else { GG.Equip.EquipPlcConnected = true; }

            foreach (DvItem item in GG.Equip.DvMgr)
            {
                if (item.DvBit == "INT16")
                    if (item.Name == "GLS_IN_TM")
                    {
                        string year = Math.Round(GG.PLC.VirGetShort(item.PlcAddr) / Math.Pow(10, item.DvUnit), 3).ToString();
                        double month = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 1) / Math.Pow(10, item.DvUnit), 3);
                        double day = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 2) / Math.Pow(10, item.DvUnit), 3);
                        double hour = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 3) / Math.Pow(10, item.DvUnit), 3);
                        double min = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 4) / Math.Pow(10, item.DvUnit), 3);
                        double sec = Math.Round(GG.PLC.VirGetShort(item.PlcAddr + 5) / Math.Pow(10, item.DvUnit), 3);

                        int iMonth = Convert.ToInt16(month);
                        int iday = Convert.ToInt16(day);
                        int ihour = Convert.ToInt16(hour);
                        int imin = Convert.ToInt16(min);
                        int isec = Convert.ToInt16(sec);

                        string strMonth = iMonth.ToString("D2");
                        string strday = iday.ToString("D2");
                        string strhour = ihour.ToString("D2");
                        string strmin = imin.ToString("D2");
                        string strsec = isec.ToString("D2");

                        item.Value = year + strMonth + strday + strhour + strmin + strsec;
                    }
                    else
                    {
                        item.Value = Math.Round(GG.PLC.VirGetShort(item.PlcAddr) / Math.Pow(10, item.DvUnit), 3).ToString();
                    }
                else if (item.DvBit == "INT32")
                    item.Value = Math.Round(GG.PLC.VirGetInt32(item.PlcAddr) / Math.Pow(10, item.DvUnit), 3).ToString();
                else if (item.DvBit == "ASCII")
                    item.Value = GG.PLC.VirGetAscii(new PlcAddr(item.PlcAddr.Type, item.PlcAddr.Addr, 0, item.PlcAddr.Length)).Trim().ToString();
                else
                    item.Value = "0";
            }
            return GG.Equip.DvMgr;
        }

        public void CellJobProcessStartReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLJOBProcessEvent( S6F11_CEID.CELL_JOG_PROCESS_CANCEL, port.Cassette);
        }
        //오정석
        public void CellJobProcessCancelReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLJOBProcessEvent(S6F11_CEID.CELL_JOG_PROCESS_CANCEL, port.Cassette);
        }
        //오정석
        public void CellJobProcessAbortReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLJOBProcessEvent(S6F11_CEID.CELL_JOG_PROCESS_ABORT, port.Cassette);
        }
        //오정석
        public void CellJobProcessEndReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLJOBProcessEvent(S6F11_CEID.CELL_JOG_PROCESS_END, port.Cassette);
        }
        //오정석
        public void CellCassetteOutReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLJOBProcessEvent(S6F11_CEID.CELL_CASSETTE_OUT, port.Cassette);
        }
        //오정석
        public void CellCassetteInReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLJOBProcessEvent(S6F11_CEID.CELL_CASSETTE_IN, port.Cassette);
        }
        //오정석
        public void CellIdReadReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLProcessEvent(S6F11_CEID.CELL_ID_READ, port.Cassette);
        }
        //오정석
        public void CellProcessStartForModuleReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLProcessEvent(S6F11_CEID.CELL_PROCESS_START_FOR_MODULE, port.Cassette);
        }
        //오정석
        public void CellProcessEndForModuleReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLProcessEvent(S6F11_CEID.CELL_PROCESS_END_FOR_MODULE, port.Cassette);
        }
        //오정석
        public void CellScrapReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLProcessEvent(S6F11_CEID.CELL_SCRAP, port.Cassette);
        }
        //오정석
        public void CellUnscrapReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLProcessEvent(S6F11_CEID.CELL_UNSCRAP, port.Cassette);
        }
        //오정석
        public void CellLoadRequestReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedLoadEvent(S6F11_CEID.CELL_LOAD_REQUEST, port.Cassette);
        }
        //오정석
        public void CellPreLoadCompleteReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedLoadEvent(S6F11_CEID.CELL_PRE_LOAD_COMPLETE, port.Cassette);
        }
        //오정석
        public void CellLoadCompleteReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedLoadEvent(S6F11_CEID.CELL_PRE_LOAD_COMPLETE, port.Cassette);
        }
        //오정석
        public void CellUnloadRequestReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedLoadEvent(S6F11_CEID.CELL_UNLOAD_REQUEST, port.Cassette);
        }
        //오정석
        public void CellLotUnloadCompleteReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedLoadEvent(S6F11_CEID.CELL_LOT_UNLOAD_COMPLETE, port.Cassette);
        }
        //오정석
        public void CellIdReadFailReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCELLJOBProcessEvent(S6F11_CEID.CELL_ID_READ_FAIL, port.Cassette);
        }
        //오정석
        public void CellIdReadingCompleteReport(PortInfo port, LotInfo lot)
        {
            HSMS_SEND.S6F11_RelatedCellIdReadingEvent(S6F11_CEID.CELL_ID_READING_COMPLETE, port.Cassette);
        }
        
        public void SetEcidChange(EMByWho byWho)
        {
            EcManager currEcIdMgr = new EcManager();
            currEcIdMgr.LoadFromFile(GG.SETTING_PATH);
            currEcIdMgr.LoadFromPlc();

            List<EcidItem> lstChangedEcid = new List<EcidItem>();
            
            int ecid = GG.PLC.VirGetShort(EQPW.ECID_CHANGE_NO);
            {
                if (EcIdMgr.ContainsKey(ecid) == true)
                {
                    lstChangedEcid.Add(currEcIdMgr[ecid]);
                }
            }

            if (lstChangedEcid.Count > 0)
            {
                HSMS_SEND.S6F11_EquipParamterEvent(S6F11_CEID.PARAMETER_CHANGED_ECID, this, new List<EoidItem>(), lstChangedEcid, byWho);
                EcIdMgr.LoadFromPlc();
            }
        }
        
        public void SetEquipStateReport(int CEID)
        {
            string[] ayName = null;
            string[] ayValue = null;
            short val = 0;

            switch (CEID)
            {
                case 171:
                    ayName = new string[3] { "IMMEDIATELY PAUSE", "", "" };
                    ayValue = new string[3] { "", "", "" };
                    ayValue[0] = val == 1 ? "ON" : "OFF";
                    HSMS_SEND.S6F11_EquipSpecControlEvent(S6F11_CEID.NEQ_NETWORK_ERROR_EVENT, this, ayName, ayValue, EMByWho.ByEqp);
                    break;

            }
        }

        public void CmdSetMCMD(EMMCmd eMMCmd, EMByWho eMByWho)
        {
            short mcmd = (short)eMMCmd;
            short bywho = (short)eMByWho;
        }
        
        public void CmdSetEoid(List<EoidItem> lstEoid, EMByWho byWho)
        {
            bool isEinMode = false;
            List<EoidItem> changedLstEoid = new List<EoidItem>();
            foreach (EoidItem info in lstEoid)
            {
                if (info.EOV != GG.Equip.EoidMgr.GetEov(info.EOID, info.EOMD))
                {
                    GG.Equip.EoidMgr.SetEov(info.EOID, info.EOMD, info.EOV);
                    changedLstEoid.Add(info);
                    if (info.EOID == EOIDInfoMgr.ProcessControlPriority)
                    {
                        GG.Equip.EinMode = (EmEinMode)info.EOV;
                        Worker.EinModeChangeCmd.RunCmd();
                        isEinMode = true;
                    }
                }
            }

            GG.Equip.EoidMgr.SaveToFile(GG.SETTING_PATH);

            if (isEinMode != true)
                HSMS_SEND.S6F11_EquipParamterEvent(S6F11_CEID.PARAMETER_CHANGED_EOID, this, changedLstEoid, new List<EcidItem>(), byWho);

            UiMain.UIUpdateEoid();
        }

        public void AlarmLog(int AlarmCode, string AlarmText)
        {
            StringBuilder AlarmHeader = new StringBuilder();
            StringBuilder AlarmContent = new StringBuilder();

            AlarmHeader.Append("TIME, CODE, TEXT, ");
            AlarmContent.AppendFormat("{0}, {1}, {2},", DateTime.Now.ToString("HH-mm-ss"), AlarmCode, AlarmText);

            AlarmHeader.AppendLine();
            AlarmContent.AppendLine();

            string filePath = System.IO.Path.Combine(@"D:\LOG\ALARM_LOG", string.Format("AlarmLog_{0}.csv", DateTime.Now.ToString("yyyy-MM-dd")));
            string FolderPath = @"D:\LOG\ALARM_LOG";

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(FolderPath);
            try
            {
                if (di.Exists == false)
                {
                    di.Create();
                }
                if (System.IO.File.Exists(filePath) == false)
                {
                    System.IO.File.AppendAllText(filePath, AlarmHeader.ToString());
                }

                System.IO.File.AppendAllText(filePath, AlarmContent.ToString());
            }
            catch
            {
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.S2F41
{
    //S2F41 Job Permission Event
    public class S2F41_JobPermision
    {
        public string JOIBID_NAME { get; set; }
        public string JOIBID_VALUE { get; set; }

        public string IPID_NAME { get; set; }
        public string IPID_VALUE { get; set; }

        public string ICID_NAME { get; set; }
        public string ICID_VALUE { get; set; }

        public string OPID_NAME { get; set; }
        public string OPID_VALUE { get; set; }

        public string OCID_NAME { get; set; }
        public string OCID_VALUE { get; set; }

        public string SLOTINFO_NAME { get; set; }
        public string SLOTINFO_VALUE { get; set; }

        public string ORDER_NAME { get; set; }
        public int SLOT_NO_COUNT { get; set; }
        public List<int> SLOT_NO_LST { get; set; }

        public S2F41_JobPermision()
        {
            SLOT_NO_LST = new List<int>();
        }

        public int HCACK { get; set; }

        public int JOBID_CPACK { get; set; }

        public int ORDER_CPACK { get; set; }

        public int SLOTINFO_CPACK { get; set; }

        public int OCID_CPACK { get; set; }

        public int OPID_CPACK { get; set; }

        public int ICID_CPACK { get; set; }

        public int IPID_CPACK { get; set; }

        public bool CPACK { get; set; }
    }
}

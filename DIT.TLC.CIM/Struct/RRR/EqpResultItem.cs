﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.RRR
{
    public class EqpResultItem
    {
        public string RawPath { get; set; }
        public string SumPath { get; set; }
        public string ImgPath { get; set; }
        public string Disk { get; set; }

        public EqpResultItem()
        {
            RawPath = string.Empty;
            SumPath = string.Empty;
            ImgPath = string.Empty;
            Disk = string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct
{
    public class AlarmInfo
    {
        public short Code { get; set; }
        public string Text { get; set; }
        public short ID { get; set; }
        public string ModuleID { get; set; }
        public short Occur { get; set; }
        public int No { get; set; }
        public string Time { get; set; }

        //20180907
        public string AlarmWarningHappen { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.RMM
{
    public class RmmHostItem
    {
        public string Name { get; set; }
        public string SoftRev { get; set; }
        public string LastTime { get; set; }
        public string EqpPPID { get; set; }
        public short EqpRecipeSeq { get; set; }

        public int Seq { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.RMM
{
    public class NewPPIDItem
    {
        public int CCode { get; set; }
        public Dictionary<string, string> Params { get; set; }
        public NewPPIDItem()
        {
            Params = new Dictionary<string, string>();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.RMM
{
    public class RmmEqpItem
    {
        public string Name { get; set; }
        public string SoftRev { get; set; }
        public string LastTime { get; set; }
        public Dictionary<int, double> Params { get; set; }
        public RmmEqpItem()
        {
            Params = new Dictionary<int, double>();
        }

        public int Seq { get; set; }
    }
}

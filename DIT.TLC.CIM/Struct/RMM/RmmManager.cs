﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Struct.RMM;
using System.IO;
using System.Collections;
using DitInlineCim.Lib;
using DitInlineCim.Common;
using DitInlineCim.PLC;

namespace DitInlineCim.Struct
{
    public class ParamInfo
    {
        public int No { get; set; }
        public string Name { get; set; }
        public int LSL { get; set; }
        public int USL { get; set; }
        public int UNIT { get; set; }
        public int Used { get; set; }

    }
    public class RmmManager
    {
        public int[] Arrunit = new int[100];
        public const string FILE_NAME = "DitCim.Rmm.ini";

        //private int RMM_PPID_COUNT = 200;
        //private int RMM_RECIPE_COUNT = 100;
        public string CurrentOldPPID { get; set; }
        public string CurrentNewPPID { get; set; }
        public string CurrentNewEqpId { get; set; }

        public short ActionType { get; set; }
        public int ActionMode { get; set; }
        public string ActionEqpPPID { get; set; }
        public string ActionHostPPID { get; set; }

        private Dictionary<string, RmmHostItem> _DicRmmHost = new Dictionary<string, RmmHostItem>();
        private Dictionary<string, RmmEqpItem> _DicRmmEqp = new Dictionary<string, RmmEqpItem>();

        public List<ParamInfo> ParamNames { get; set; }

        public RmmManager()
        {
            //CurrentNewPPID = "";

            ActionMode = 0;
            ActionEqpPPID = "";
            ActionHostPPID = "";

            ParamNames = new List<ParamInfo>();
        }

        public Dictionary<string, RmmHostItem> DicRmmHost
        {
            get { return _DicRmmHost; }
        }
        public Dictionary<string, RmmEqpItem> DicRmmEqp
        {
            get { return _DicRmmEqp; }
        }

        public void Clear()
        {
            _DicRmmHost.Clear();
            _DicRmmEqp.Clear();
        }

        public bool LoadFromFile(string settingPath)
        {            
            return true;
        }
        public bool LoadFromPlc()
        {
             
            return true;
        }
        public bool IsString(string str)
        {
            for (int iPos = 0; iPos < str.Length; iPos++)
            {
                if (str[iPos] >= 'A' && str[iPos] <= 'Z')
                    continue;
                else if (str[iPos] >= '0' && str[iPos] <= '9')
                    continue;
                else if (str[iPos] >= 'z' && str[iPos] <= 'z')
                    continue;
                else if (str[iPos] >= 'z' && str[iPos] <= 'z')
                    continue;
                else if (str[iPos] >= '!' && str[iPos] <= '~')
                    continue;
                else if (str[iPos] == ' ')
                    continue;
                else
                    return false;
            }
            return true;
        }

        public void ModifyRecipe(string ppId, string softRev, short ppIdType, NewPPIDItem newPpId)
        {
           
        }
        private void ModifyRecipeType1(string ppId, string softRev, NewPPIDItem newPpId)
        {
           
        }


        public void DeleteRecipe(string ppId, string softRev, short ppIdType, NewPPIDItem newPpId)
        {
             
        }
        private void DeleteRecipeType1(string ppId, string softRev, NewPPIDItem newPpId)
        {   
        }

        public void CreateRecipe(string ppId, string softRev, short ppIdType, NewPPIDItem newPpId)
        {
            
        }
        private void CreateRecipeType1(string ppId, string softRev, NewPPIDItem newPpId)
        {
           
        }
    }
}

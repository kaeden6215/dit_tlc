﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct
{
    public class TerminalMessageCommand : List<TerminalMessageInfo>
    {
        private object _lockobjct = new object();

        public void AddTerminalMessageCmd(TerminalMessageInfo cmd)
        {
            lock (_lockobjct)
                Add(cmd);
        }
        public TerminalMessageInfo GetTerminalMessageCmd()
        {
            lock (_lockobjct)
            {
                if (this.Count > 0)
                    return this[0];
                else
                    return null;
            }
        }
        public void DelTerminalMessageCmd()
        {
            lock (_lockobjct)
            {
                if (this.Count > 0)
                    this.RemoveAt(0);
            }
        }
    }
}

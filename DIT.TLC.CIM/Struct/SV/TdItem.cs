﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.SV
{
    public class TdItem : List<SvSmpItems>
    {
        public int TrID { get; set; }    //TR ID
        public int SamplingTime { get; set; } //
        public int TotalSmp { get; set; }
        public int GroupSize { get; set; }   //    
        public List<int> LstSv { get; set; }

        
        private int _SmdCount = 0;
        private int _sampleTotalCnt = 0;

        public int SmpLn { get; set; }

        DateTime LastSendTime = DateTime.Now;
        public TdItem()
        {
            Init();
        }

        public void Init()
        {
            TrID = 0;
            SamplingTime = 0;
            TotalSmp = 0;
            GroupSize = 0;

            _SmdCount = 0;
            _sampleTotalCnt = 0;
            SmpLn = 0;

            LstSv = new List<int>();
            LstSv.Clear();
            Clear();
        }


        public bool SendingReport(SvManager svMgr)
        {
            if (TotalSmp > 0)
            {
                if (_sampleTotalCnt >= TotalSmp)
                    return false;
            }

            bool bSendReport = false;

            if ((DateTime.Now - LastSendTime).TotalMilliseconds < SamplingTime)
            {
                return false;
            }
            else
            {
                LastSendTime = DateTime.Now;

                //Sampling
                SvSmpItems smpObj = new SvSmpItems();
                smpObj.SCTime = DateTime.Now.ToString("yyyyMMddHHmmssff");

                SvItem svObj, svTmpObj = null;
                foreach (int nSVID in LstSv)
                {
                    svTmpObj = svMgr[nSVID];
                    svObj = new SvItem();
                    svObj.SvID = svTmpObj.SvID;
                    svObj.SvValue = svTmpObj.SvValue;
                    svObj.SvName = svTmpObj.SvName;

                    smpObj.Add(svObj);
                }

                this.Add(smpObj);
                _SmdCount++;

                if (GroupSize <= _SmdCount)
                {
                    if (TotalSmp > 0)
                        _sampleTotalCnt++;

                    _SmdCount = 0;
                    bSendReport = true;
                    SmpLn++;
                }
            }

            return bSendReport;
        }
    }
}

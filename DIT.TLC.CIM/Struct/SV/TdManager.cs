﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.SV
{
    public class TdManager : Dictionary<int, TdItem>
    {
        public void AddTDItem(int nTRID, TdItem tdObj)
        {
            if (this.ContainsKey(nTRID))
            {
                this[nTRID].Init();

                this[nTRID].TrID = nTRID;
                this[nTRID].SamplingTime = tdObj.SamplingTime;
                this[nTRID].TotalSmp = tdObj.TotalSmp;
                this[nTRID].GroupSize = tdObj.GroupSize;
                this[nTRID].LstSv.AddRange(tdObj.LstSv);
            }
            else
            {
                this.Add(nTRID, tdObj);
            }
        }
    }
}

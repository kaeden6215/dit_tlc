﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Struct.SV;
using System.IO;
using DitInlineCim.PLC;
using Dit.Framework.PLC;

namespace DitInlineCim.Struct.SV
{
    public class SvManager : SortedDictionary<int, SvItem>
    {
        public const string FILE_NAME = "DitCim.SvInfo.csv";
        public void LoadFromFile(string settingPath)
        {
            string fileName = Path.Combine(settingPath, FILE_NAME);
            try
            {
                foreach (string line in File.ReadAllLines(fileName, Encoding.ASCII))
                {
                    string trimLine = line.Trim();

                    if (trimLine.Contains("="))
                    {
                        //PLC=26
                        //0,SVID 1 ,PHOTO_IONIZER_POWER_ ON_OFF,
                    }
                    else if (trimLine.Length < 2)
                    {
                    }
                    else if (trimLine[0] == '#')
                    {
                    }
                    else
                    {
                        string[] items = trimLine.Split(',');

                        int svno = int.Parse(items[0]);
                        PlcAddr addr = PlcAddr.Parsing(items[1]);
                        int svid = int.Parse(items[2]);
                        double svunit = double.Parse(items[4]);
                        string svBit = items[5];

                        SetSvItem(svno, svid, string.Empty, items[3], svunit, "FLOAT", addr, svBit);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erorr SV File", ex);
            }
        }
        public void SetSvItem(int svNo, int svID, string svValue, string svName, double svunit, string svType, PlcAddr addr, string svBit)
        {
            if (this.ContainsKey(svID))
            {
                this[svID].SvNo = svNo;
                this[svID].SvID = svID;
                this[svID].SvValue = svValue;
                this[svID].SvName = svName;
                this[svID].SvType = svType;
                this[svID].SvUnit = svunit;
                this[svID].SvBit = svBit;
                this[svID].PlcAddr = addr;

            }
            else
            {
                SvItem item = new SvItem();
                item.SvNo = svNo;
                item.SvID = svID;
                item.SvValue = svValue;
                item.SvName = svName;
                item.SvType = svType;
                item.SvUnit = svunit;
                item.SvBit = svBit;
                item.PlcAddr = addr;

                this.Add(svID, item);
            }
        }
    }
}

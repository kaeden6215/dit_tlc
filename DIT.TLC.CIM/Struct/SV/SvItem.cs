﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using DitInlineCim.PLC;

namespace DitInlineCim.Struct.SV
{
    public class SvItem
    {
        public int SvNo { get; set; }
        public double SvUnit { get; set; }
        public int SvID { get; set; }
        public string SvValue { get; set; }
        public string SvName { get; set; }
        public string SvType { get; set; }
        public PlcAddr PlcAddr { get; set; }
        
        public SvItem()
        {
            SvNo = 0;
            SvID = 0;
            SvUnit = 1f;
            SvValue = "";
            SvName = "";
            SvType = "";

        }

        public string SvBit { get; set; }
    }
}

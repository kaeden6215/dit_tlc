﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim;
using DitInlineCim.Common;

namespace DitInlineCim.Struct
{    
    //오정석
    public class Defect
    {
        public string CoorX;
        public string CoorY;
        public string DefectName;
    }


    //오정석
    [Serializable]
    public class CellInfo
    {
        public EMPanelState BeforScripPanelState;
        public EMPanelState PanelState_;

        public string HGlassID;
        public string EGlassID;
        public string LotID;
        public string BatchID;
        public string JobID;
        public string PortID;
        public string SlotID;

        public string ProductType;
        public string ProductKind;
        public string ProductID;
        public string RunSpecID;
        public string LayerID;
        public string StepID;
        public string PPID;
        public string FlowID;
        public int[] GlassSize;
        public int GlassThickness;
        //public int GlassState;
        public string GlassOrder;
        public string Comment;
        public string Code;
        public string UseCount;
        public string Judgement;
        public string ReasonCode;
        public string InsFlag;
        public string EncFlag;
        public string PrerunFlag;
        public string TurnDir;
        public string FlipState;
        public string WorkState;
        public string MultiUse;

        public string PairGlassID;
        public string PairPPID;

        public string OptionName1;
        public string OptionValue1;
        public string OptionName2;
        public string OptionValue2;
        public string OptionName3;
        public string OptionValue3;
        public string OptionName4;
        public string OptionValue4;
        public string OptionName5;
        public string OptionValue5;

        public int LotFlag;
        public string Etc;
        public string MainLotId;
        public string CstID;

        public int CSIF;
        public int AS;
        public int APS;
        public int UniqueID_;
        public short[] UniqueID;
        public int BitSignal;

        public string DeviceID;
        public int[] PanelSize;
        public string PanelPosition;
        public string Count1;
        public string Count2;
        public string Grade;
        public short CompCount;
        public string ReadingFlag;


        public short[] RunLine;

        public string Pair_H_PanelID;
        public string Pair_E_PanelID;
        public short Pair_Grade;
        public short[] Pair_UniqueID;
        public string FlowRecipe;
        public string Reserved;
        public short BitsSignals;
        public string ReferData;
        public short PanelState;

        //프로그램상에서 사용 
        public int SlotSeq = 0;
        public int PortSeq = 0;

        public EmGlassPosi GlassPosi = EmGlassPosi.Unknown;

        public int S2F41GlassStartOrder { get; set; }
        public bool Exist { get; set; }


        public string CellId;
        public string VcrResult;
        public string CJudgement;
        public string SCode;
        public string CCode;
      

        public Defect[] Defects = new Defect[GG.MAX_GLASS];
 

        public CellInfo()
        {
            GlassSize = new int[2];

            HGlassID = string.Empty;
            EGlassID = string.Empty;
            LotID = string.Empty;
            BatchID = string.Empty;
            JobID = string.Empty;
            PortID = string.Empty;
            SlotID = string.Empty;
            ProductType = string.Empty;
            ProductKind = string.Empty;
            ProductID = string.Empty;
            RunSpecID = string.Empty;
            LayerID = string.Empty;
            StepID = string.Empty;
            PPID = string.Empty;
            FlowID = string.Empty;
            GlassSize[0] = 0;
            GlassSize[1] = 0;
            GlassThickness = 0;
            PanelState_ = EMPanelState.EMPTY;
            GlassOrder = string.Empty;
            Comment = string.Empty;

            UseCount = string.Empty;
            Judgement = string.Empty;
            ReasonCode = string.Empty;
            InsFlag = string.Empty;
            EncFlag = string.Empty;
            PrerunFlag = string.Empty;
            TurnDir = string.Empty;
            FlipState = string.Empty;
            WorkState = string.Empty;
            MultiUse = string.Empty;

            PairGlassID = string.Empty;
            PairPPID = string.Empty;

            OptionName1 = string.Empty;
            OptionValue1 = string.Empty;
            OptionName2 = string.Empty;
            OptionValue2 = string.Empty;
            OptionName3 = string.Empty;
            OptionValue3 = string.Empty;
            OptionName4 = string.Empty;
            OptionValue4 = string.Empty;
            OptionName5 = string.Empty;
            OptionValue5 = string.Empty;
            Code = string.Empty;
            LotFlag = 0;
            Etc = string.Empty;

            CSIF = 0;
            AS = 0;
            APS = 0;
            UniqueID_ = 0;
            BitSignal = 0;

            S2F41GlassStartOrder = 99;

            MainLotId = string.Empty;
            CstID = string.Empty;

            PanelSize = new int[2];
            RunLine = new short[20];
            UniqueID = new short[4];
            Pair_UniqueID = new short[4];
        }

        public void CopyData(CellInfo targetGlassInfo)
        {
            this.HGlassID = targetGlassInfo.HGlassID;
            this.EGlassID = targetGlassInfo.EGlassID;
            this.LotID = targetGlassInfo.LotID;
            this.BatchID = targetGlassInfo.BatchID;
            this.JobID = targetGlassInfo.JobID;
            this.PortID = targetGlassInfo.PortID;
            this.SlotID = targetGlassInfo.SlotID;
            this.ProductType = targetGlassInfo.ProductType;
            this.ProductKind = targetGlassInfo.ProductKind;
            this.ProductID = targetGlassInfo.ProductID;
            this.RunSpecID = targetGlassInfo.RunSpecID;
            this.LayerID = targetGlassInfo.LayerID;
            this.StepID = targetGlassInfo.StepID;
            this.PPID = targetGlassInfo.PPID;
            this.FlowID = targetGlassInfo.FlowID;
            this.GlassSize[0] = targetGlassInfo.GlassSize[0];
            this.GlassSize[1] = targetGlassInfo.GlassSize[1];
            this.GlassThickness = targetGlassInfo.GlassThickness;
            this.PanelState = targetGlassInfo.PanelState;
            this.GlassOrder = targetGlassInfo.GlassOrder;
            this.Comment = targetGlassInfo.Comment;

            this.UseCount = targetGlassInfo.UseCount;
            this.Judgement = targetGlassInfo.Judgement;
            this.ReasonCode = targetGlassInfo.ReasonCode;
            this.InsFlag = targetGlassInfo.InsFlag;
            this.EncFlag = targetGlassInfo.EncFlag;
            this.PrerunFlag = targetGlassInfo.PrerunFlag;
            this.TurnDir = targetGlassInfo.TurnDir;
            this.FlipState = targetGlassInfo.FlipState;
            this.WorkState = targetGlassInfo.WorkState;
            this.MultiUse = targetGlassInfo.MultiUse;

            this.PairGlassID = targetGlassInfo.PairGlassID;
            this.PairPPID = targetGlassInfo.PairPPID;

            this.OptionName1 = targetGlassInfo.OptionName1;
            this.OptionValue1 = targetGlassInfo.OptionValue1;
            this.OptionName2 = targetGlassInfo.OptionName2;
            this.OptionValue2 = targetGlassInfo.OptionValue2;
            this.OptionName3 = targetGlassInfo.OptionName3;
            this.OptionValue3 = targetGlassInfo.OptionValue3;
            this.OptionName4 = targetGlassInfo.OptionName4;
            this.OptionValue4 = targetGlassInfo.OptionValue4;
            this.OptionName5 = targetGlassInfo.OptionName5;
            this.OptionValue5 = targetGlassInfo.OptionValue5;

            this.LotFlag = targetGlassInfo.LotFlag;
            this.Etc = targetGlassInfo.Etc;

            this.CSIF = targetGlassInfo.CSIF;
            this.AS = targetGlassInfo.AS;
            this.APS = targetGlassInfo.APS;
            this.UniqueID = targetGlassInfo.UniqueID;
            this.BitSignal = targetGlassInfo.BitSignal;
            this.GlassPosi = targetGlassInfo.GlassPosi;
            this.MainLotId = targetGlassInfo.MainLotId;
            this.CstID = targetGlassInfo.CstID;

            this.DeviceID = targetGlassInfo.DeviceID;
            this.PanelSize = targetGlassInfo.PanelSize;
            this.PanelPosition = targetGlassInfo.PanelPosition;
            this.Count1 = targetGlassInfo.Count1;
            this.Count2 = targetGlassInfo.Count2;
            this.Grade = targetGlassInfo.Grade;
            this.CompCount = targetGlassInfo.CompCount;
            this.ReadingFlag = targetGlassInfo.ReadingFlag;


            this.RunLine = targetGlassInfo.RunLine;

            this.Pair_H_PanelID = targetGlassInfo.Pair_H_PanelID;
            this.Pair_E_PanelID = targetGlassInfo.Pair_E_PanelID;
            this.Pair_Grade = targetGlassInfo.Pair_Grade;
            this.Pair_UniqueID = targetGlassInfo.Pair_UniqueID;
            this.FlowRecipe = targetGlassInfo.FlowRecipe;
            this.Reserved = targetGlassInfo.Reserved;
            this.BitsSignals = targetGlassInfo.BitsSignals;
            this.ReferData = targetGlassInfo.ReferData;
            this.PanelState = targetGlassInfo.PanelState;
    }

        public void UpdateResult(CellInfo targetGlassInfo)
        {
            if (this.HGlassID != targetGlassInfo.HGlassID)
            {
                Log.Logger.Append(" Glass ID Not Match  CIM AOUI = {0}, PLC AOUI  = {1}", this.HGlassID, targetGlassInfo.HGlassID);
                return;
            }

            this.Judgement = targetGlassInfo.Judgement;
            this.ReasonCode = targetGlassInfo.ReasonCode;

            this.LotFlag = targetGlassInfo.LotFlag;
            this.Etc = targetGlassInfo.Etc;
        }


    }
}

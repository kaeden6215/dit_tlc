﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.HSMS;
using DitInlineCim.Struct.RRR;
using DitInlineCim.Common;
using DitInlineCim.Struct.DV;
using DitInlineCim.Log;

namespace DitInlineCim.Struct
{
    public class ModuleInfo
    {
        //EQP_STATE (MODULE STATE) / PROC_STATE (MODULE STATE)
        public EMEquipState EquipState { get; set; }     //Normal / Fault / PM (Equipment State)
        public EMEquipState EquipStateOld { get; set; }

        public EMProcState ProcState { get; set; }       //Init / Idle / Setup / Ready / Executing / Pause
        public EMProcState ProcStateOld { get; set; }

        public EMByWho ByWhoEquipStop { get; set; }
        public EMByWho ByWhoEquipPause { get; set; }
        public EMByWho ByWhoEquipResume { get; set; }
        public EMByWho MCMDByWho { get; set; }

        public string ModuleID { get; set; }

        public string ToModuleID1 { get; set; }
        public string ToModuleID2 { get; set; }

        public string FromModuleID1 { get; set; }
        public string FromModuleID2 { get; set; }

        public CellInfo Cell { get; set; }
        public PortInfo Port { get; set; }
        public CassetteInfo Cassette { get; set; }
        

        public bool ExistGlass { get; set; }

        public FrmMain UiMain { get; set; }

        public int Layer { get; set; }

        public ModuleInfo(FrmMain uiMain, string moduleID)
        {
            UiMain = uiMain;
            ModuleID = moduleID;

            EquipState = EMEquipState.Normal;
            EquipStateOld = EMEquipState.PM;

            ProcState = EMProcState.Idle;
            ProcStateOld = EMProcState.Pause;
            Layer = 1;
            ExistGlass = false;
            Cell = new CellInfo();
            Port = new PortInfo();
            Cassette = new CassetteInfo();            
        }

        public ModuleInfo()
        {
        }

        public void PanelLoading(CellInfo glass)
        {
            //GLASS MODULE IN            
            if (Layer == GG.Equip.EoidMgr.GetEov(EOIDInfoMgr.ComponentTrace, "1"))
                HSMS_SEND.S6F11_PanelProcess(S6F11_CEID.PANEL_PROCESS_START, this, glass, EMByWho.ByEqp);

            Cell = glass;
        }
        public void PanelUnloading(CellInfo glass)
        {
            //GLASS UNIT PROCESS END
            if (Layer == GG.Equip.EoidMgr.GetEov(EOIDInfoMgr.ComponentTrace, "2"))
                HSMS_SEND.S6F11_PanelProcess(S6F11_CEID.PANEL_PROCESS_END, this, glass, EMByWho.ByEqp);


            List<DvItem> lstDvItem = GG.Equip.GetListGlassDCollData();
            //HSMS_SEND.S6F13(this, glass, lstDvItem);
            HSMS_SEND.S6F13(this, glass, lstDvItem);

            Cell = glass;
            Cell.Exist = false;
        }
        public void PanelUnloadComplete(CellInfo glass)
        {
            if (Layer == GG.Equip.EoidMgr.GetEov(EOIDInfoMgr.ComponentTrace, "2"))
                HSMS_SEND.S6F11_PanelProcess(S6F11_CEID.PANEL_PROCESS_END, this, glass, EMByWho.ByEqp);
        }

        public void SetEqState(EMEquipState state, string reasonCode, EMByWho byWho)
        {

            this.EquipStateOld = this.EquipState;
            this.EquipState = state;

            Logger.Append("Event {0} ProcState Change {1} → {2}", this.ModuleID, this.EquipStateOld.ToString(), this.EquipState.ToString());

            if (this.EquipState == EMEquipState.Normal)
            {
                reasonCode = string.Empty;
                if (Layer == GG.Equip.EoidMgr.GetEov(EOIDInfoMgr.EquipmentStateTrace, "0"))
                    HSMS_SEND.S6F11_EquipEvent(S6F11_CEID.EQ_MODULE_STATE_CHANGED,
                      this.ModuleID,
                     GG.Equip.MCMD,
                     GG.Equip.MCMD_OLD,
                     this.EquipState,
                     this.EquipStateOld,
                     this.ProcState,
                     this.ProcStateOld,
                     reasonCode,
                     byWho);
            }
            else if (this.EquipState == EMEquipState.Fault)
            {
                reasonCode = string.Empty;
                if (Layer == GG.Equip.EoidMgr.GetEov(EOIDInfoMgr.EquipmentStateTrace, "0"))
                    HSMS_SEND.S6F11_EquipEvent(S6F11_CEID.EQ_MODULE_STATE_CHANGED,
                     this.ModuleID,
                    GG.Equip.MCMD,
                    GG.Equip.MCMD_OLD,
                    this.EquipState,
                    this.EquipStateOld,
                    this.ProcState,
                    this.ProcStateOld,
                    reasonCode,
                    byWho);
            }
            else if (this.EquipState == EMEquipState.PM)
            {
                if (Layer == GG.Equip.EoidMgr.GetEov(EOIDInfoMgr.EquipmentStateTrace, "0"))
                    HSMS_SEND.S6F11_EquipEvent(S6F11_CEID.EQ_MODULE_STATE_CHANGED,
                     this.ModuleID,
                    GG.Equip.MCMD,
                    GG.Equip.MCMD_OLD,
                    this.EquipState,
                    this.EquipStateOld,
                    this.ProcState,
                    this.ProcStateOld,
                    reasonCode,
                    byWho);
            }
        }
        public void SetProcState(EMProcState procState, string reasonCode, EMByWho byWho)
        {
            this.ProcStateOld = this.ProcState;
            this.ProcState = procState;

            Logger.Append(" {0} ProcState Change {1} → {2}", this.ModuleID, this.ProcStateOld.ToString(), this.ProcState.ToString());

            if (Layer == GG.Equip.EoidMgr.GetEov(EOIDInfoMgr.EquipmentProcessStateTrace, "0"))
                HSMS_SEND.S6F11_EquipEvent(S6F11_CEID.EQ_MODULE_PROCESS_STATE_CHANGED,
                     this.ModuleID,
                    GG.Equip.MCMD,
                    GG.Equip.MCMD_OLD,
                    this.EquipState,
                    this.EquipStateOld,
                    this.ProcState,
                    this.ProcStateOld,
                    reasonCode,
                    byWho);
        }

        public EmModuleType ModuleType { get; set; }
    }
}

﻿using DitInlineCim.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct
{
    [Serializable]
    public class PortInfo
    {
        // Equipment Object Set
        public int StartOrder { get; set; }
        public string ModuleID { get; set; }
        public string OperID { get; set; }

        //Port Object Set
        public string PortID { get; set; }
        public short PortSeq { get; set; }

        public int ModuleState { get; set; }

        public EMPortState PortState { get; set; }
        //public EMPortState NowPortState { get; set; }
        //public string PortStateStr { get; set; }
        //public EMPortType ReadPortType { get; set; }
        public string ReadPortMode { get; set; }
        public EMSortType SortType { get; set; }
        public EMCstDemand CstDemand { get; set; }
        public EMByWho ByWhoReload { get; set; }
        public EMByWho ByWhoStart { get; set; }
        public EMByWho ByWhoCancel { get; set; }
        public EMByWho ByWhoAbort { get; set; }
        public CassetteInfo Cassette { get; set; }

        public EMPortState PortScenarioState { get; set; }

        public bool FlagRecvCstInfoS3F101 { get; set; }
        public int ManAlreadyReserve { get; set; }
        public bool FlagLoadRejectCancel { get; set; }
        public string mS2F41_ICID { get; set; }

        public EMCstType PortCstType { get; set; }
        public PortInfo()
        { }
        public PortInfo(int portSeq)
        {
            PortID = portSeq == 0 ? CimConst.BP01 :
                     portSeq == 1 ? CimConst.BP02 :
                     portSeq == 2 ? CimConst.BP03 : CimConst.BP04;
            PortSeq = (short)portSeq;

            Clear(PortSeq);
        }

        public void Clear(int portSeq)
        {
            //ReadPortType = EMPortType.Both;  // 0 : Both, 1 : Input, 2 : Output, 3 : Reserved
            SortType = EMSortType.DumpFill;  // 0 : Dump Fill, 2 : Dump, 3 : Slot Number, 4 : LOT ID
            CstDemand = EMCstDemand.NormalCST; // 0 : Normal CST for robot Access, 1 : Empty CST for robot Access
            ReadPortMode = "OK";
            PortState = EMPortState.UNKNOW;
            ReadPortState = EMPortState.UNKNOW;
            PortScenarioState = EMPortState.UNKNOW;
            PortCstType = EMCstType.FullCst;
            mS2F41_ICID = string.Empty;

            FlagRecvCstInfoS3F101 = false;
            IsCeid30Request = false;
            IsCeid30Respones = false;
            FlagLoadRejectCancel = false;

            PortGlassSizeH = 0;
            PortGlassSizeW = 0;

            //ReadMapStif = "".PadRight(56, '0');
            //ReadCurStif = "".PadRight(56, '0');

            //Cassette = new CassetteInfo(portSeq);
        }

        public bool IsCeid30Request { get; set; }
        public bool IsCeid30Respones { get; set; }

        public short PortGlassSizeH { get; set; }
        public short PortGlassSizeW { get; set; }
        public EMPortState ReadPortState { get; set; }

        //public string ReadCstID { get; set; }
        //public int ReadCstType { get; set; }
        //public string ReadMapStif { get; set; }
        //public string ReadCurStif { get; set; }
        //public int ReadBatchOrder { get; set; }

        public string GetLogStif()
        {
            string stif = string.Empty;
            for (int iPos = 0; iPos < GG.CST_SLOT_CNT; iPos++)
            {
                stif = stif + (Cassette.ExistsGlasss[iPos] ? "1" : "0");
            }

            stif = stif.PadRight(56, '0').Substring(0, 56);
            return stif;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.MULTI
{
    public class MultiItem
    {
        public string DataType { get; set; }
        public string Reference { get; set; }
        public string ItemName { get; set; }
        public string ItemValue { get; set; }
        public short ItemEac { get; set; }
    }
}

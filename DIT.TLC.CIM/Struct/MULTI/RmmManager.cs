﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;

namespace DitInlineCim.Struct.MULTI
{
    public class MultiManager
    {
        private Dictionary<string, MultiItem> _DicMulti = new Dictionary<string, MultiItem>();
        public Dictionary<string, MultiItem> DicMulti
        {
            get { return _DicMulti; }
        }
        public bool Load()
        {
            _DicMulti.Clear();

            FileInfo exefileinfo = new FileInfo(Application.ExecutablePath);//실행결로

            string PathINI = "";
            string fileName = "";
            string filePath = "";
            string section = "";


            PathINI = @"z:\";//exefileinfo.Directory.FullName.ToString(); //프로그램 실행위치 경로 가져오기
            fileName = @"\MULTI_CIM_ASK.ini";

            filePath = PathINI + fileName; //ini 파일 전체 경로

            IniReader ini = new IniReader(filePath);
            MultiItem multiObj = null;
            //////////////////////////////////////////////////////////////////////////////////

            filePath = PathINI + fileName; //ini 파일 전체 경로

            section = "MCC";
            multiObj = new MultiItem();

            multiObj.DataType = section;
            multiObj.ItemName = "MCC_BASIC_PATH";
            multiObj.ItemValue = ini.GetString(section, "MCC_BASIC_PATH").Trim();
            multiObj.Reference = "PATH";
            DicMulti.Add(multiObj.ItemName, multiObj);

            multiObj = new MultiItem();
            multiObj.DataType = section;
            multiObj.ItemName = "MCC_SAMPLING_TIME";
            multiObj.ItemValue = ini.GetString(section, "MCC_SAMPLING_TIME").Trim();
            multiObj.Reference = "MINUTE";
            DicMulti.Add(multiObj.ItemName, multiObj);

            multiObj = new MultiItem();
            multiObj.DataType = section;
            multiObj.ItemName = "MCC_HOST_IP";
            multiObj.ItemValue = ini.GetString(section, "MCC_HOST_IP").Trim();
            multiObj.Reference = "IP";
            DicMulti.Add(multiObj.ItemName, multiObj);

            multiObj = new MultiItem();
            multiObj.DataType = section;
            multiObj.ItemName = "MCC_LOGIN_ID";
            multiObj.ItemValue = ini.GetString(section, "MCC_LOGIN_ID").Trim();
            multiObj.Reference = "ID";
            DicMulti.Add(multiObj.ItemName, multiObj);

            multiObj = new MultiItem();
            multiObj.DataType = section;
            multiObj.ItemName = "MCC_LOGIN_PW";
            multiObj.ItemValue = ini.GetString(section, "MCC_LOGIN_PW").Trim();
            multiObj.Reference = "PW";
            DicMulti.Add(multiObj.ItemName, multiObj);


            section = "DES";
            multiObj = new MultiItem();

            multiObj.DataType = section;
            multiObj.ItemName = "DES_BASIC_PATH";
            multiObj.ItemValue = ini.GetString(section, "DES_BASIC_PATH").Trim();
            multiObj.Reference = "PATH";
            DicMulti.Add(multiObj.ItemName, multiObj);

            multiObj = new MultiItem();
            multiObj.DataType = section;
            multiObj.ItemName = "DES_HOST_IP";
            multiObj.ItemValue = ini.GetString(section, "DES_HOST_IP").Trim();
            multiObj.Reference = "IP";
            DicMulti.Add(multiObj.ItemName, multiObj);

            multiObj = new MultiItem();
            multiObj.DataType = section;
            multiObj.ItemName = "DES_LOGIN_ID";
            multiObj.ItemValue = ini.GetString(section, "DES_LOGIN_ID").Trim();
            multiObj.Reference = "ID";
            DicMulti.Add(multiObj.ItemName, multiObj);

            multiObj = new MultiItem();
            multiObj.DataType = section;
            multiObj.ItemName = "DES_LOGIN_PW";
            multiObj.ItemValue = ini.GetString(section, "DES_LOGIN_PW").Trim();
            multiObj.Reference = "PW";
            DicMulti.Add(multiObj.ItemName, multiObj);

            return true;
        }


        public bool Save()
        {
            FileInfo exefileinfo = new FileInfo(Application.ExecutablePath);//실행결로

            string PathINI = "";
            string fileName = "";
            string filePath = "";

            PathINI = @"z:\";//exefileinfo.Directory.FullName.ToString(); //프로그램 실행위치 경로 가져오기
            fileName = @"\MULTI_CIM_ASK.ini";

            filePath = PathINI + fileName; //ini 파일 전체 경로

            IniReader ini = new IniReader(filePath);


            MultiItem multiObj = null;
            foreach (KeyValuePair<string, MultiItem> kvp in DicMulti)
            {
                multiObj = kvp.Value;
                ini.SetString(multiObj.DataType, multiObj.ItemName, multiObj.ItemValue);

            }

            return true;
        }

    }
}

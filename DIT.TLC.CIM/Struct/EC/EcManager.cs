﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DitInlineCim.Common;
using DitInlineCim.PLC;

namespace DitInlineCim.Struct.EC
{
    public class EcManager : SortedDictionary<int, EcidItem>
    {
        public EcidItem GetECObject(int nECID)
        {
            return this[nECID];
        }

        public bool IsContainKey(int nECID) { return this.ContainsKey(nECID); }
        public void SetECItems(List<EcidItem> lst)
        {

        }

        public void SetECItem(int plcSeq, int ecId,
            string ecName,
            int unit,
            string ecDefine,
            string ecSLL,
            string ecSUL,
            string ecWLL,
            string ecWUL)
        {
            if (this.ContainsKey(ecId))
            {
                this[ecId].PlcSeq = plcSeq;
                this[ecId].EcID = ecId;
                this[ecId].EcName = ecName;
                this[ecId].EcDefine = ecDefine;
                this[ecId].EcUnit = unit;

                this[ecId].EcSLL = ecSLL;
                this[ecId].EcSUL = ecSUL;
                this[ecId].EcWLL = ecWLL;
                this[ecId].EcWUL = ecWUL;

            }
            else
            {
                EcidItem ecObj = new EcidItem();
                ecObj.PlcSeq = plcSeq;
                ecObj.EcID = ecId;
                ecObj.EcName = ecName;
                ecObj.EcDefine = ecDefine;
                ecObj.EcUnit = unit;
                ecObj.EcSLL = ecSLL;
                ecObj.EcSUL = ecSUL;
                ecObj.EcWLL = ecWLL;
                ecObj.EcWUL = ecWUL;
                this.Add(ecId, ecObj);
            }
        }

        public const string FILE_NAME = "DitCim.ECID.csv";
        public void LoadFromFile(string settingPath)
        {
            string fileName = Path.Combine(settingPath, FILE_NAME);
            try
            {
                foreach (string line in File.ReadAllLines(fileName, Encoding.ASCII))
                {
                    if (this.Count >= 50) continue;

                    string[] items = line.Split(',');
                    if (items.Length <= 7) continue;
                    if (items[0].Trim() != "1") continue;

                    //USED	PLCSEQ	ECIDECNAME	DEFINE	SLL	SUL	WLL	WUL
                    int plcSeq = int.Parse(items[1]);
                    int ecId = int.Parse(items[2]);
                    string ecName = items[3];
                    int unit = int.Parse(items[4]);

                    SetECItem(plcSeq, ecId, ecName, unit, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Loading Erorr ( DitCim.ECID.ini )", ex);
            }
        }

        public bool LoadFromPlc()
        {
            GG.PLC.ReadFromPLC(EQPW.ECID_SYNC_ADDR, EQPW.ECID_SYNC_ADDR.Length);

            foreach (int ecid in this.Keys)
            {
                EcidItem item = this[ecid];

                item.EcDefine = Math.Round(GG.PLC.VirGetShort(EQPW.ECID_001_DEF_POS + (5 * (item.PlcSeq - 1))) / Math.Pow(10, item.EcUnit), 3).ToString();
                item.EcSLL = Math.Round(GG.PLC.VirGetShort(EQPW.ECID_001_SLL_POS + (5 * (item.PlcSeq - 1))) / Math.Pow(10, item.EcUnit), 3).ToString();
                item.EcSUL = Math.Round(GG.PLC.VirGetShort(EQPW.ECID_001_SUL_POS + (5 * (item.PlcSeq - 1))) / Math.Pow(10, item.EcUnit), 3).ToString();
                item.EcWLL = Math.Round(GG.PLC.VirGetShort(EQPW.ECID_001_WLL_POS + (5 * (item.PlcSeq - 1))) / Math.Pow(10, item.EcUnit), 3).ToString();
                item.EcWUL = Math.Round(GG.PLC.VirGetShort(EQPW.ECID_001_WUL_POS + (5 * (item.PlcSeq - 1))) / Math.Pow(10, item.EcUnit), 3).ToString();
            }

            return true;
        }
    }
}

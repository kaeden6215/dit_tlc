﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct.EC
{
    public class EcidItem
    {
        public EcidItem()
        {
            EcID = 0;
            EcName = "";
            EcDefine = "";
            EcSLL = "";
            EcSUL = "";
            EcWLL = "";
            EcWUL = "";        
        }

        public int EcID { get; set; }
        public string EcName { get; set; }
        public string EcDefine { get; set; }

        public string EcSLL { get; set; }
        public string EcSUL { get; set; }
        public string EcWLL { get; set; }
        public string EcWUL { get; set; }

        public short EcID_PMACK{ get; set; }
        public short EcName_PMACK { get; set; }
        public short EcDefine_PMACK { get; set; }
        public short EcSLL_PMACK { get; set; }
        public short EcSUL_PMACK { get; set; }
        public short EcWLL_PMACK { get; set; }
        public short EcWUL_PMACK { get; set; }

        public int PlcSeq { get; set; }

        public int EcUnit { get; set; }
    }
}

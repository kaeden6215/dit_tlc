﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct
{
    public class TerminalMessageInfo
    {
        public short TID { get; set; }
        public string[] MSG { get; set; }
        public string ModuleID { get; set; }
        public bool OnOff { get; set; }

        public int StepIndexTerminalMsgCmd { get; set; }
    }
}

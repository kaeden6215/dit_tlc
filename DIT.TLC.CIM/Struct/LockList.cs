﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Struct
{
    public class LockList<T> : List<T>
    {
        private object _lockobjct = new object();

        public new void Insert(int index, T item)
        {
            lock (_lockobjct)
            {
                base.Insert(index, item);
            }
        }
        public new void Add(T item)
        {
            lock (_lockobjct)
            {
                base.Add(item);
            }
        }
        public new void Remove(T item)
        {
            lock (_lockobjct)
            {
                base.Remove(item);
            }
        }
        public new void RemoveAt(int index)
        {
            lock (_lockobjct)
            {
                base.RemoveAt(index);
            }
        }

        public T Get(int index)
        {
            lock (_lockobjct)
            {
                return base[index];
            }
        }
    }
}

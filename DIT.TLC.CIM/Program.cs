﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DitInlineCim.Common;
using DitInlineCim.Log;
using System.Threading;

namespace DitInlineCim
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool bRunning = false;
            Mutex _mt = new Mutex(true, "___HSMS___", out bRunning);

            if (!bRunning)
            {
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.Run(new FrmMain());
            _mt.ReleaseMutex();
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {            
            Logger.Append(e.Exception.Message + Environment.NewLine + e.Exception.StackTrace);            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.Common;

namespace DitInlineCim.Struct
{
    [System.ComponentModel.ToolboxItem(false)]
    public partial class UcrlEoid : UserControl
    {
        public UcrlEoid()
        {
            InitializeComponent();
        }

        public void Initailzie()
        {
            rdoEoid_01_1_1.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.ComponentTrace, "1") == 1);
            rdoEoid_01_1_2.Checked = !rdoEoid_01_1_1.Checked;

            rdoEoid_01_2_1.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.ComponentTrace, "2") == 1);
            rdoEoid_01_2_2.Checked = !rdoEoid_01_2_1.Checked;


            rdoEoid_02_0_0.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.EquipmentStateTrace, "0") == 0);
            rdoEoid_02_0_1.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.EquipmentStateTrace, "0") == 1);
            rdoEoid_02_0_2.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.EquipmentStateTrace, "0") == 2);

            rdoEoid_03_0_0.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.EquipmentProcessStateTrace, "0") == 0);
            rdoEoid_03_0_1.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.EquipmentProcessStateTrace, "0") == 1);
            rdoEoid_03_0_2.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.EquipmentProcessStateTrace, "0") == 2);


            rdoEoid_202_1_0.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.ProcessControlPriority, "1") == 0);
            rdoEoid_202_1_1.Checked = !rdoEoid_202_1_0.Checked;

            if (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.ProcessControlPriority, "1") == 0)
                GG.Equip.EinMode = EmEinMode.Host;
            else
                GG.Equip.EinMode = EmEinMode.InLine;

            //rdoEoid_202_2_0.Checked = (GG.Equip.EoidMgr.GetEov((short)EOIDInfoMgr.ProcessControlPriority, "2") == 0);
            //rdoEoid_202_2_1.Checked = !rdoEoid_202_2_0.Checked;

        }
        private void btnEOIDSave_Click(object sender, EventArgs e)
        {
            EOIDInfoMgr eoidMgr = new EOIDInfoMgr();

            eoidMgr.SetEov((short)EOIDInfoMgr.ComponentTrace, "1", rdoEoid_01_1_1.Checked ? (short)1 : (short)2);
            eoidMgr.SetEov((short)EOIDInfoMgr.ComponentTrace, "2", rdoEoid_01_2_1.Checked ? (short)1 : (short)2);

            eoidMgr.SetEov((short)EOIDInfoMgr.EquipmentStateTrace, "0", rdoEoid_02_0_0.Checked ? (short)0 : rdoEoid_02_0_1.Checked ? (short)1 : (short)2);
            eoidMgr.SetEov((short)EOIDInfoMgr.EquipmentProcessStateTrace, "0", rdoEoid_03_0_0.Checked ? (short)0 : rdoEoid_03_0_1.Checked ? (short)1 : (short)2);
            eoidMgr.SetEov((short)EOIDInfoMgr.ProcessControlPriority, "1", rdoEoid_202_1_0.Checked ? (short)0 : (short)1);

            GG.Equip.SetEoid(eoidMgr, EMByWho.ByOper);
        }
        private void btnEOIDCancel_Click(object sender, EventArgs e)
        {
            Initailzie();
        }
    }
}

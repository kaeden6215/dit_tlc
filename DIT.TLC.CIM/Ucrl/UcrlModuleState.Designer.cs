﻿namespace DitInlineCim.Ucrl
{
    partial class UcrlModuleState
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblL0ProcState = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.lblL1UV05ProcState = new System.Windows.Forms.Label();
            this.lblL1UV04ProcState = new System.Windows.Forms.Label();
            this.lblL1UV08ProcState = new System.Windows.Forms.Label();
            this.lblL1UV07ProcState = new System.Windows.Forms.Label();
            this.lblL1UV05EqpState = new System.Windows.Forms.Label();
            this.lblL1UV04EqpState = new System.Windows.Forms.Label();
            this.lblL1UV08EqpState = new System.Windows.Forms.Label();
            this.lblL1UV07EqpState = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lblL1UV06ProcState = new System.Windows.Forms.Label();
            this.lblL1UV06EqpState = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblL1UV03ProcState = new System.Windows.Forms.Label();
            this.lblL1UV02ProcState = new System.Windows.Forms.Label();
            this.lblL1UV01ProcState = new System.Windows.Forms.Label();
            this.lblL1UV03EqpState = new System.Windows.Forms.Label();
            this.lblL1UV02EqpState = new System.Windows.Forms.Label();
            this.lblL1UV01EqpState = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.lblL0EqpState = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.lblEqpID = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblL0ProcState
            // 
            this.lblL0ProcState.BackColor = System.Drawing.Color.White;
            this.lblL0ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL0ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL0ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL0ProcState.Location = new System.Drawing.Point(183, 77);
            this.lblL0ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL0ProcState.Name = "lblL0ProcState";
            this.lblL0ProcState.Size = new System.Drawing.Size(89, 130);
            this.lblL0ProcState.TabIndex = 16;
            this.lblL0ProcState.Text = "-";
            this.lblL0ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label2.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label2.ForeColor = System.Drawing.Color.White;
            this.Label2.Location = new System.Drawing.Point(456, 34);
            this.Label2.Margin = new System.Windows.Forms.Padding(1);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(89, 41);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "PROC\r\nSTATE";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label9.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label9.ForeColor = System.Drawing.Color.White;
            this.Label9.Location = new System.Drawing.Point(274, 34);
            this.Label9.Margin = new System.Windows.Forms.Padding(1);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(89, 41);
            this.Label9.TabIndex = 9;
            this.Label9.Text = "ID";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel1
            // 
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.lblL1UV05ProcState);
            this.Panel1.Controls.Add(this.lblL1UV04ProcState);
            this.Panel1.Controls.Add(this.lblL1UV08ProcState);
            this.Panel1.Controls.Add(this.label28);
            this.Panel1.Controls.Add(this.lblL1UV05EqpState);
            this.Panel1.Controls.Add(this.lblL1UV07ProcState);
            this.Panel1.Controls.Add(this.lblL1UV04EqpState);
            this.Panel1.Controls.Add(this.lblL1UV08EqpState);
            this.Panel1.Controls.Add(this.lblL1UV07EqpState);
            this.Panel1.Controls.Add(this.label29);
            this.Panel1.Controls.Add(this.label30);
            this.Panel1.Controls.Add(this.label31);
            this.Panel1.Controls.Add(this.lblL1UV06ProcState);
            this.Panel1.Controls.Add(this.lblL1UV06EqpState);
            this.Panel1.Controls.Add(this.label18);
            this.Panel1.Controls.Add(this.label6);
            this.Panel1.Controls.Add(this.label11);
            this.Panel1.Controls.Add(this.label13);
            this.Panel1.Controls.Add(this.lblL1UV03ProcState);
            this.Panel1.Controls.Add(this.lblL1UV02ProcState);
            this.Panel1.Controls.Add(this.lblL1UV01ProcState);
            this.Panel1.Controls.Add(this.lblL1UV03EqpState);
            this.Panel1.Controls.Add(this.lblL1UV02EqpState);
            this.Panel1.Controls.Add(this.lblL1UV01EqpState);
            this.Panel1.Controls.Add(this.label19);
            this.Panel1.Controls.Add(this.label15);
            this.Panel1.Controls.Add(this.label12);
            this.Panel1.Controls.Add(this.lblL0ProcState);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.Label3);
            this.Panel1.Controls.Add(this.Label9);
            this.Panel1.Controls.Add(this.Label10);
            this.Panel1.Controls.Add(this.Label8);
            this.Panel1.Controls.Add(this.lblL0EqpState);
            this.Panel1.Controls.Add(this.Label7);
            this.Panel1.Controls.Add(this.lblEqpID);
            this.Panel1.Controls.Add(this.Label4);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Panel1.Location = new System.Drawing.Point(3, 3);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(822, 212);
            this.Panel1.TabIndex = 3;
            // 
            // lblL1UV05ProcState
            // 
            this.lblL1UV05ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV05ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV05ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV05ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV05ProcState.Location = new System.Drawing.Point(729, 77);
            this.lblL1UV05ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV05ProcState.Name = "lblL1UV05ProcState";
            this.lblL1UV05ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV05ProcState.TabIndex = 51;
            this.lblL1UV05ProcState.Text = "-";
            this.lblL1UV05ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV04ProcState
            // 
            this.lblL1UV04ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV04ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV04ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV04ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV04ProcState.Location = new System.Drawing.Point(456, 176);
            this.lblL1UV04ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV04ProcState.Name = "lblL1UV04ProcState";
            this.lblL1UV04ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV04ProcState.TabIndex = 50;
            this.lblL1UV04ProcState.Text = "-";
            this.lblL1UV04ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV08ProcState
            // 
            this.lblL1UV08ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV08ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV08ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV08ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV08ProcState.Location = new System.Drawing.Point(729, 176);
            this.lblL1UV08ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV08ProcState.Name = "lblL1UV08ProcState";
            this.lblL1UV08ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV08ProcState.TabIndex = 53;
            this.lblL1UV08ProcState.Text = "-";
            this.lblL1UV08ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV07ProcState
            // 
            this.lblL1UV07ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV07ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV07ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV07ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV07ProcState.Location = new System.Drawing.Point(729, 143);
            this.lblL1UV07ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV07ProcState.Name = "lblL1UV07ProcState";
            this.lblL1UV07ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV07ProcState.TabIndex = 52;
            this.lblL1UV07ProcState.Text = "-";
            this.lblL1UV07ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV05EqpState
            // 
            this.lblL1UV05EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV05EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV05EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV05EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV05EqpState.Location = new System.Drawing.Point(638, 77);
            this.lblL1UV05EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV05EqpState.Name = "lblL1UV05EqpState";
            this.lblL1UV05EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV05EqpState.TabIndex = 48;
            this.lblL1UV05EqpState.Text = "-";
            this.lblL1UV05EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV04EqpState
            // 
            this.lblL1UV04EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV04EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV04EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV04EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV04EqpState.Location = new System.Drawing.Point(365, 176);
            this.lblL1UV04EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV04EqpState.Name = "lblL1UV04EqpState";
            this.lblL1UV04EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV04EqpState.TabIndex = 49;
            this.lblL1UV04EqpState.Text = "-";
            this.lblL1UV04EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV08EqpState
            // 
            this.lblL1UV08EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV08EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV08EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV08EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV08EqpState.Location = new System.Drawing.Point(638, 176);
            this.lblL1UV08EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV08EqpState.Name = "lblL1UV08EqpState";
            this.lblL1UV08EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV08EqpState.TabIndex = 46;
            this.lblL1UV08EqpState.Text = "-";
            this.lblL1UV08EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV07EqpState
            // 
            this.lblL1UV07EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV07EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV07EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV07EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV07EqpState.Location = new System.Drawing.Point(638, 143);
            this.lblL1UV07EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV07EqpState.Name = "lblL1UV07EqpState";
            this.lblL1UV07EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV07EqpState.TabIndex = 47;
            this.lblL1UV07EqpState.Text = "-";
            this.lblL1UV07EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(547, 77);
            this.label28.Margin = new System.Windows.Forms.Padding(1);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 31);
            this.label28.TabIndex = 43;
            this.label28.Text = "UV05";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label29.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label29.Location = new System.Drawing.Point(274, 176);
            this.label29.Margin = new System.Windows.Forms.Padding(1);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 31);
            this.label29.TabIndex = 42;
            this.label29.Text = "UV04";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label30.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label30.Location = new System.Drawing.Point(547, 176);
            this.label30.Margin = new System.Windows.Forms.Padding(1);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(89, 31);
            this.label30.TabIndex = 45;
            this.label30.Text = "UV08";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label31.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label31.Location = new System.Drawing.Point(547, 143);
            this.label31.Margin = new System.Windows.Forms.Padding(1);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 31);
            this.label31.TabIndex = 44;
            this.label31.Text = "UV07";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV06ProcState
            // 
            this.lblL1UV06ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV06ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV06ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV06ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV06ProcState.Location = new System.Drawing.Point(729, 110);
            this.lblL1UV06ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV06ProcState.Name = "lblL1UV06ProcState";
            this.lblL1UV06ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV06ProcState.TabIndex = 41;
            this.lblL1UV06ProcState.Text = "-";
            this.lblL1UV06ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV06EqpState
            // 
            this.lblL1UV06EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV06EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV06EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV06EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV06EqpState.Location = new System.Drawing.Point(638, 110);
            this.lblL1UV06EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV06EqpState.Name = "lblL1UV06EqpState";
            this.lblL1UV06EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV06EqpState.TabIndex = 40;
            this.lblL1UV06EqpState.Text = "-";
            this.lblL1UV06EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label18.Location = new System.Drawing.Point(547, 110);
            this.label18.Margin = new System.Windows.Forms.Padding(1);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 31);
            this.label18.TabIndex = 39;
            this.label18.Text = "UV06";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(730, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 41);
            this.label6.TabIndex = 38;
            this.label6.Text = "PROC\r\nSTATE";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(639, 34);
            this.label11.Margin = new System.Windows.Forms.Padding(1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 41);
            this.label11.TabIndex = 37;
            this.label11.Text = "EQ\r\nSTATE";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(548, 34);
            this.label13.Margin = new System.Windows.Forms.Padding(1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 41);
            this.label13.TabIndex = 36;
            this.label13.Text = "ID";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV03ProcState
            // 
            this.lblL1UV03ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV03ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV03ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV03ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV03ProcState.Location = new System.Drawing.Point(456, 143);
            this.lblL1UV03ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV03ProcState.Name = "lblL1UV03ProcState";
            this.lblL1UV03ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV03ProcState.TabIndex = 35;
            this.lblL1UV03ProcState.Text = "-";
            this.lblL1UV03ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV02ProcState
            // 
            this.lblL1UV02ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV02ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV02ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV02ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV02ProcState.Location = new System.Drawing.Point(456, 110);
            this.lblL1UV02ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV02ProcState.Name = "lblL1UV02ProcState";
            this.lblL1UV02ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV02ProcState.TabIndex = 35;
            this.lblL1UV02ProcState.Text = "-";
            this.lblL1UV02ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV01ProcState
            // 
            this.lblL1UV01ProcState.BackColor = System.Drawing.Color.White;
            this.lblL1UV01ProcState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV01ProcState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV01ProcState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV01ProcState.Location = new System.Drawing.Point(456, 77);
            this.lblL1UV01ProcState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV01ProcState.Name = "lblL1UV01ProcState";
            this.lblL1UV01ProcState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV01ProcState.TabIndex = 35;
            this.lblL1UV01ProcState.Text = "-";
            this.lblL1UV01ProcState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV03EqpState
            // 
            this.lblL1UV03EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV03EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV03EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV03EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV03EqpState.Location = new System.Drawing.Point(365, 143);
            this.lblL1UV03EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV03EqpState.Name = "lblL1UV03EqpState";
            this.lblL1UV03EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV03EqpState.TabIndex = 34;
            this.lblL1UV03EqpState.Text = "-";
            this.lblL1UV03EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV02EqpState
            // 
            this.lblL1UV02EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV02EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV02EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV02EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV02EqpState.Location = new System.Drawing.Point(365, 110);
            this.lblL1UV02EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV02EqpState.Name = "lblL1UV02EqpState";
            this.lblL1UV02EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV02EqpState.TabIndex = 34;
            this.lblL1UV02EqpState.Text = "-";
            this.lblL1UV02EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL1UV01EqpState
            // 
            this.lblL1UV01EqpState.BackColor = System.Drawing.Color.White;
            this.lblL1UV01EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL1UV01EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL1UV01EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL1UV01EqpState.Location = new System.Drawing.Point(365, 77);
            this.lblL1UV01EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL1UV01EqpState.Name = "lblL1UV01EqpState";
            this.lblL1UV01EqpState.Size = new System.Drawing.Size(89, 31);
            this.lblL1UV01EqpState.TabIndex = 34;
            this.lblL1UV01EqpState.Text = "-";
            this.lblL1UV01EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(274, 143);
            this.label19.Margin = new System.Windows.Forms.Padding(1);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 31);
            this.label19.TabIndex = 17;
            this.label19.Text = "UV03";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(274, 110);
            this.label15.Margin = new System.Windows.Forms.Padding(1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 31);
            this.label15.TabIndex = 17;
            this.label15.Text = "UV02";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(274, 77);
            this.label12.Margin = new System.Windows.Forms.Padding(1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 31);
            this.label12.TabIndex = 17;
            this.label12.Text = "UV01";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label3.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label3.ForeColor = System.Drawing.Color.White;
            this.Label3.Location = new System.Drawing.Point(365, 34);
            this.Label3.Margin = new System.Windows.Forms.Padding(1);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(89, 41);
            this.Label3.TabIndex = 10;
            this.Label3.Text = "EQ\r\nSTATE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label10
            // 
            this.Label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label10.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label10.ForeColor = System.Drawing.Color.White;
            this.Label10.Location = new System.Drawing.Point(274, 1);
            this.Label10.Margin = new System.Windows.Forms.Padding(1);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(544, 31);
            this.Label10.TabIndex = 8;
            this.Label10.Text = "MODULE 1 LAYER";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label8
            // 
            this.Label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label8.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label8.ForeColor = System.Drawing.Color.White;
            this.Label8.Location = new System.Drawing.Point(183, 34);
            this.Label8.Margin = new System.Windows.Forms.Padding(1);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(89, 41);
            this.Label8.TabIndex = 7;
            this.Label8.Text = "PROC\r\nSTATE";
            this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblL0EqpState
            // 
            this.lblL0EqpState.BackColor = System.Drawing.Color.White;
            this.lblL0EqpState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblL0EqpState.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblL0EqpState.ForeColor = System.Drawing.Color.Black;
            this.lblL0EqpState.Location = new System.Drawing.Point(92, 77);
            this.lblL0EqpState.Margin = new System.Windows.Forms.Padding(1);
            this.lblL0EqpState.Name = "lblL0EqpState";
            this.lblL0EqpState.Size = new System.Drawing.Size(89, 130);
            this.lblL0EqpState.TabIndex = 6;
            this.lblL0EqpState.Text = "-";
            this.lblL0EqpState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label7
            // 
            this.Label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label7.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label7.ForeColor = System.Drawing.Color.White;
            this.Label7.Location = new System.Drawing.Point(92, 34);
            this.Label7.Margin = new System.Windows.Forms.Padding(1);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(89, 41);
            this.Label7.TabIndex = 5;
            this.Label7.Text = "EQ\r\nSTATE";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEqpID
            // 
            this.lblEqpID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblEqpID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEqpID.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblEqpID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblEqpID.Location = new System.Drawing.Point(1, 77);
            this.lblEqpID.Margin = new System.Windows.Forms.Padding(1);
            this.lblEqpID.Name = "lblEqpID";
            this.lblEqpID.Size = new System.Drawing.Size(89, 130);
            this.lblEqpID.TabIndex = 4;
            this.lblEqpID.Text = "A1AOI04N";
            this.lblEqpID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label4
            // 
            this.Label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label4.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label4.ForeColor = System.Drawing.Color.White;
            this.Label4.Location = new System.Drawing.Point(1, 34);
            this.Label4.Margin = new System.Windows.Forms.Padding(1);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(89, 41);
            this.Label4.TabIndex = 3;
            this.Label4.Text = "ID";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label1.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.Label1.ForeColor = System.Drawing.Color.White;
            this.Label1.Location = new System.Drawing.Point(1, 1);
            this.Label1.Margin = new System.Windows.Forms.Padding(1);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(271, 31);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "EQPID";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlModuleState
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Panel1);
            this.Name = "UcrlModuleState";
            this.Size = new System.Drawing.Size(828, 220);
            this.Panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblL0ProcState;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label lblL0EqpState;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label lblEqpID;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label lblL1UV03ProcState;
        internal System.Windows.Forms.Label lblL1UV02ProcState;
        internal System.Windows.Forms.Label lblL1UV01ProcState;
        internal System.Windows.Forms.Label lblL1UV03EqpState;
        internal System.Windows.Forms.Label lblL1UV02EqpState;
        internal System.Windows.Forms.Label lblL1UV01EqpState;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label lblL1UV05ProcState;
        internal System.Windows.Forms.Label lblL1UV04ProcState;
        internal System.Windows.Forms.Label lblL1UV08ProcState;
        internal System.Windows.Forms.Label lblL1UV07ProcState;
        internal System.Windows.Forms.Label lblL1UV05EqpState;
        internal System.Windows.Forms.Label lblL1UV04EqpState;
        internal System.Windows.Forms.Label lblL1UV08EqpState;
        internal System.Windows.Forms.Label lblL1UV07EqpState;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.Label label31;
        internal System.Windows.Forms.Label lblL1UV06ProcState;
        internal System.Windows.Forms.Label lblL1UV06EqpState;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label13;
    }
}

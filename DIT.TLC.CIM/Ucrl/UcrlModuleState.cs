﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.Common;

namespace DitInlineCim.Ucrl
{
    [System.ComponentModel.ToolboxItem(false)]
    public partial class UcrlModuleState : UserControl
    {
        public UcrlModuleState()
        {
            InitializeComponent();
        }

        public void UiUpdate()
        {
            lblEqpID.Text = GG.Equip.ModuleID;

            //Equipment State
            SetEquipStateColor(lblL0EqpState, GG.Equip.ModuleID, GG.Equip.EquipState);
            //SetEquipStateColor(lblL1VY01EqpState, GG.Equip.VY01.ModuleID, GG.Equip.VY01.EquipState);
            SetEquipStateColor(lblL1UV01EqpState, GG.Equip.UV01.ModuleID, GG.Equip.UV01.EquipState);
            SetEquipStateColor(lblL1UV02EqpState, GG.Equip.UV02.ModuleID, GG.Equip.UV02.EquipState);
            SetEquipStateColor(lblL1UV03EqpState, GG.Equip.UV03.ModuleID, GG.Equip.UV03.EquipState);
            SetEquipStateColor(lblL1UV04EqpState, GG.Equip.UV04.ModuleID, GG.Equip.UV04.EquipState);
            SetEquipStateColor(lblL1UV05EqpState, GG.Equip.UV05.ModuleID, GG.Equip.UV05.EquipState);
            SetEquipStateColor(lblL1UV06EqpState, GG.Equip.UV06.ModuleID, GG.Equip.UV06.EquipState);
            SetEquipStateColor(lblL1UV07EqpState, GG.Equip.UV07.ModuleID, GG.Equip.UV07.EquipState);
            SetEquipStateColor(lblL1UV08EqpState, GG.Equip.UV08.ModuleID, GG.Equip.UV08.EquipState);
            //SetEquipStateColor(lblL1UV01EqpState, GG.Equip.UV09.ModuleID, GG.Equip.UV09.EquipState);
            //SetEquipStateColor(lblL1UV01EqpState, GG.Equip.UV10.ModuleID, GG.Equip.UV10.EquipState);
            //SetEquipStateColor(lblL1UV01EqpState, GG.Equip.UV11.ModuleID, GG.Equip.UV11.EquipState);

            //Equipment State
            SetProcStateColor(lblL0ProcState, GG.Equip.ModuleID, GG.Equip.ProcState);
            //SetProcStateColor(lblL1VY01ProcState, GG.Equip.VY01.ModuleID, GG.Equip.VY01.ProcState);
            SetProcStateColor(lblL1UV01ProcState, GG.Equip.UV01.ModuleID, GG.Equip.UV01.ProcState);
            SetProcStateColor(lblL1UV02ProcState, GG.Equip.UV02.ModuleID, GG.Equip.UV02.ProcState);
            SetProcStateColor(lblL1UV03ProcState, GG.Equip.UV03.ModuleID, GG.Equip.UV03.ProcState);
            SetProcStateColor(lblL1UV04ProcState, GG.Equip.UV04.ModuleID, GG.Equip.UV04.ProcState);
            SetProcStateColor(lblL1UV05ProcState, GG.Equip.UV05.ModuleID, GG.Equip.UV05.ProcState);
            SetProcStateColor(lblL1UV06ProcState, GG.Equip.UV06.ModuleID, GG.Equip.UV06.ProcState);
            SetProcStateColor(lblL1UV07ProcState, GG.Equip.UV07.ModuleID, GG.Equip.UV07.ProcState);
            SetProcStateColor(lblL1UV08ProcState, GG.Equip.UV08.ModuleID, GG.Equip.UV08.ProcState);
            //SetProcStateColor(lblL1UV01ProcState, GG.Equip.UV09.ModuleID, GG.Equip.UV09.ProcState);
            //SetProcStateColor(lblL1UV01ProcState, GG.Equip.UV10.ModuleID, GG.Equip.UV10.ProcState);
            //SetProcStateColor(lblL1UV01ProcState, GG.Equip.UV11.ModuleID, GG.Equip.UV11.ProcState);
        }

        private void SetEquipStateColor(Label lbl, string moduleID, EMEquipState equipstate)
        {
            switch (equipstate)
            {
                case EMEquipState.Normal:
                    lbl.ForeColor = CimColor.CoEQUIPST_NORMAL_F;
                    lbl.BackColor = CimColor.CoEQUIPST_NORMAL_B;
                    lbl.Text = "NORMAL";
                    break;
                case EMEquipState.Fault:
                    lbl.ForeColor = CimColor.CoEQUIPST_FAULT_F;
                    lbl.BackColor = CimColor.CoEQUIPST_FAULT_B;
                    lbl.Text = "FAULT";
                    break;
                case EMEquipState.PM:
                    lbl.ForeColor = CimColor.CoEQUIPST_PM_F;
                    lbl.BackColor = CimColor.CoEQUIPST_PM_B;
                    lbl.Text = "PM";
                    break;
                case EMEquipState.Unknown:
                default:
                    lbl.ForeColor = Color.Red;
                    lbl.BackColor = Color.White;
                    lbl.Text = "UNKNOWN";
                    break;
            }
        }

        private void SetProcStateColor(Label lbl, string moduleID, EMProcState procState)
        {
            switch (procState)
            {
                case EMProcState.Init:
                    lbl.ForeColor = CimColor.CoPROCST_INIT_F;
                    lbl.BackColor = CimColor.CoPROCST_INIT_B;
                    lbl.Text = "INIT";
                    break;
                case EMProcState.Idle:
                    lbl.ForeColor = CimColor.CoPROCST_IDLE_F;
                    lbl.BackColor = CimColor.CoPROCST_IDLE_B;
                    lbl.Text = "IDLE";
                    break;
                case EMProcState.Setup:
                    lbl.ForeColor = CimColor.CoPROCST_SETUP_F;
                    lbl.BackColor = CimColor.CoPROCST_SETUP_B;
                    lbl.Text = "SETUP";
                    break;
                case EMProcState.Ready:
                    lbl.ForeColor = CimColor.CoPROCST_READY_F;
                    lbl.BackColor = CimColor.CoPROCST_READY_B;
                    lbl.Text = "READY";
                    break;
                case EMProcState.Execute:
                    lbl.ForeColor = CimColor.CoPROCST_EXECUTE_F;
                    lbl.BackColor = CimColor.CoPROCST_EXECUTE_B;
                    lbl.Text = "EXECUTE";
                    break;
                case EMProcState.Pause:
                    lbl.ForeColor = CimColor.CoPROCST_PAUSE_F;
                    lbl.BackColor = CimColor.CoPROCST_PAUSE_B;
                    lbl.Text = "PAUSE";
                    break;
                case EMProcState.Unknown:
                default:
                    lbl.ForeColor = Color.Red;
                    lbl.BackColor = Color.White;
                    lbl.Text = "UNKNOWN";
                    break;
            }
        }
    }
}

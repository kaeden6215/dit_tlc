﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DitInlineCim.Ucrl
{
    [System.ComponentModel.ToolboxItem(false)]
    public partial class UcrlTimer : UserControl
    {
        public UcrlTimer()
        {
            InitializeComponent();
        }

        private void tmrStart_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public void Start()
        {
            tmrStart.Start();
        }
        public void Stop()
        {
            tmrStart.Stop();
        }
    }
}

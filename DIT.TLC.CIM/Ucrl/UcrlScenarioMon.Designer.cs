﻿namespace DitInlineCim.Ucrl
{
    partial class UcrlScenarioMon
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblStepEquipAlarmHappen = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblStepEquipCurrentPPidChange = new System.Windows.Forms.Label();
            this.lblStepEquipAlarmClear = new System.Windows.Forms.Label();
            this.lblStepEquipPanelUnscrap = new System.Windows.Forms.Label();
            this.lblStepEquipPanelScrap = new System.Windows.Forms.Label();
            this.lblStepEquipPanelunloadingComplete = new System.Windows.Forms.Label();
            this.lblStepEquipPanelunloading = new System.Windows.Forms.Label();
            this.lblStepEquipPanelLoading = new System.Windows.Forms.Label();
            this.lblStepEquipEqpProcChange = new System.Windows.Forms.Label();
            this.lblStepEquipEqpStateChange = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.btnAoiStepIntialize = new System.Windows.Forms.Button();
            this.tmrMonitor = new System.Windows.Forms.Timer(this.components);
            this.lblStepEquipStateChangeToNormal = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblStepEquipAlarmClearCmd = new System.Windows.Forms.Label();
            this.lblStepEquipProcChangeToResume = new System.Windows.Forms.Label();
            this.lblStepEquipProcChangeToPause = new System.Windows.Forms.Label();
            this.lblStepEquipStateChangeToPM = new System.Windows.Forms.Label();
            this.lblStepEquipEcidChangeEvent = new System.Windows.Forms.Label();
            this.lblStepEquipRecipeCreateDeleteModifyEvent = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.lblStepEquipOpcallSetCmd = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.lblStepEquipTerminalMessageSetCmd = new System.Windows.Forms.Label();
            this.lblStepEquipTimeSetCmd = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.lblStepEquipEinModeSetCmd = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblStepEquipEoidChangeEvent = new System.Windows.Forms.Label();
            this.lblStepEquipCycleStopCmd = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblStepEquipAlarmHappen
            // 
            this.lblStepEquipAlarmHappen.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipAlarmHappen.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipAlarmHappen.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipAlarmHappen.Location = new System.Drawing.Point(282, 177);
            this.lblStepEquipAlarmHappen.Name = "lblStepEquipAlarmHappen";
            this.lblStepEquipAlarmHappen.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipAlarmHappen.TabIndex = 74;
            this.lblStepEquipAlarmHappen.Text = "0";
            this.lblStepEquipAlarmHappen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Black;
            this.label9.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(1, 177);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(277, 18);
            this.label9.TabIndex = 73;
            this.label9.Text = "Event - Alarm Happen";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStepEquipCurrentPPidChange
            // 
            this.lblStepEquipCurrentPPidChange.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipCurrentPPidChange.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipCurrentPPidChange.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipCurrentPPidChange.Location = new System.Drawing.Point(282, 309);
            this.lblStepEquipCurrentPPidChange.Name = "lblStepEquipCurrentPPidChange";
            this.lblStepEquipCurrentPPidChange.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipCurrentPPidChange.TabIndex = 72;
            this.lblStepEquipCurrentPPidChange.Text = "0";
            this.lblStepEquipCurrentPPidChange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipAlarmClear
            // 
            this.lblStepEquipAlarmClear.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipAlarmClear.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipAlarmClear.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipAlarmClear.Location = new System.Drawing.Point(282, 196);
            this.lblStepEquipAlarmClear.Name = "lblStepEquipAlarmClear";
            this.lblStepEquipAlarmClear.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipAlarmClear.TabIndex = 70;
            this.lblStepEquipAlarmClear.Text = "0";
            this.lblStepEquipAlarmClear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipPanelUnscrap
            // 
            this.lblStepEquipPanelUnscrap.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipPanelUnscrap.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipPanelUnscrap.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipPanelUnscrap.Location = new System.Drawing.Point(282, 158);
            this.lblStepEquipPanelUnscrap.Name = "lblStepEquipPanelUnscrap";
            this.lblStepEquipPanelUnscrap.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipPanelUnscrap.TabIndex = 69;
            this.lblStepEquipPanelUnscrap.Text = "0";
            this.lblStepEquipPanelUnscrap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipPanelScrap
            // 
            this.lblStepEquipPanelScrap.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipPanelScrap.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipPanelScrap.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipPanelScrap.Location = new System.Drawing.Point(282, 139);
            this.lblStepEquipPanelScrap.Name = "lblStepEquipPanelScrap";
            this.lblStepEquipPanelScrap.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipPanelScrap.TabIndex = 68;
            this.lblStepEquipPanelScrap.Text = "0";
            this.lblStepEquipPanelScrap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipPanelunloadingComplete
            // 
            this.lblStepEquipPanelunloadingComplete.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipPanelunloadingComplete.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipPanelunloadingComplete.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipPanelunloadingComplete.Location = new System.Drawing.Point(282, 120);
            this.lblStepEquipPanelunloadingComplete.Name = "lblStepEquipPanelunloadingComplete";
            this.lblStepEquipPanelunloadingComplete.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipPanelunloadingComplete.TabIndex = 67;
            this.lblStepEquipPanelunloadingComplete.Text = "0";
            this.lblStepEquipPanelunloadingComplete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipPanelunloading
            // 
            this.lblStepEquipPanelunloading.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipPanelunloading.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipPanelunloading.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipPanelunloading.Location = new System.Drawing.Point(282, 101);
            this.lblStepEquipPanelunloading.Name = "lblStepEquipPanelunloading";
            this.lblStepEquipPanelunloading.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipPanelunloading.TabIndex = 66;
            this.lblStepEquipPanelunloading.Text = "0";
            this.lblStepEquipPanelunloading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipPanelLoading
            // 
            this.lblStepEquipPanelLoading.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipPanelLoading.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipPanelLoading.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipPanelLoading.Location = new System.Drawing.Point(282, 82);
            this.lblStepEquipPanelLoading.Name = "lblStepEquipPanelLoading";
            this.lblStepEquipPanelLoading.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipPanelLoading.TabIndex = 65;
            this.lblStepEquipPanelLoading.Text = "0";
            this.lblStepEquipPanelLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipEqpProcChange
            // 
            this.lblStepEquipEqpProcChange.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipEqpProcChange.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipEqpProcChange.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipEqpProcChange.Location = new System.Drawing.Point(282, 63);
            this.lblStepEquipEqpProcChange.Name = "lblStepEquipEqpProcChange";
            this.lblStepEquipEqpProcChange.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipEqpProcChange.TabIndex = 64;
            this.lblStepEquipEqpProcChange.Text = "0";
            this.lblStepEquipEqpProcChange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipEqpStateChange
            // 
            this.lblStepEquipEqpStateChange.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipEqpStateChange.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipEqpStateChange.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipEqpStateChange.Location = new System.Drawing.Point(282, 44);
            this.lblStepEquipEqpStateChange.Name = "lblStepEquipEqpStateChange";
            this.lblStepEquipEqpStateChange.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipEqpStateChange.TabIndex = 63;
            this.lblStepEquipEqpStateChange.Text = "0";
            this.lblStepEquipEqpStateChange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Black;
            this.label24.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(1, 309);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(277, 18);
            this.label24.TabIndex = 62;
            this.label24.Text = "Event - Current PPID Change Event";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Black;
            this.label26.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(1, 196);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(277, 18);
            this.label26.TabIndex = 60;
            this.label26.Text = "Event - Alarm Clear";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Black;
            this.label27.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(1, 158);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(277, 18);
            this.label27.TabIndex = 59;
            this.label27.Text = "Event - Panel Unscrap";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Black;
            this.label28.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(1, 139);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(277, 18);
            this.label28.TabIndex = 58;
            this.label28.Text = "Event - Panel Scrap";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Black;
            this.label29.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(1, 120);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(277, 18);
            this.label29.TabIndex = 57;
            this.label29.Text = "Event - Panel Unloading Complete";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Black;
            this.label30.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(1, 101);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(277, 18);
            this.label30.TabIndex = 56;
            this.label30.Text = "Event - Panel Unloading";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Black;
            this.label31.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(1, 82);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(277, 18);
            this.label31.TabIndex = 55;
            this.label31.Text = "Event - Panel Loading";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Black;
            this.label32.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(1, 63);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(277, 18);
            this.label32.TabIndex = 54;
            this.label32.Text = "Event - Eqp Proc Change";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.Black;
            this.label33.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(1, 44);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(277, 18);
            this.label33.TabIndex = 53;
            this.label33.Text = "Event - Eqp State Change";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.AutoEllipsis = true;
            this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label34.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(311, 32);
            this.label34.TabIndex = 76;
            this.label34.Text = "CIM ↔ AOI";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAoiStepIntialize
            // 
            this.btnAoiStepIntialize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnAoiStepIntialize.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAoiStepIntialize.ForeColor = System.Drawing.Color.Black;
            this.btnAoiStepIntialize.Location = new System.Drawing.Point(317, -1);
            this.btnAoiStepIntialize.Name = "btnAoiStepIntialize";
            this.btnAoiStepIntialize.Size = new System.Drawing.Size(109, 33);
            this.btnAoiStepIntialize.TabIndex = 75;
            this.btnAoiStepIntialize.Text = "Step Intialize";
            this.btnAoiStepIntialize.UseVisualStyleBackColor = false;
            this.btnAoiStepIntialize.Click += new System.EventHandler(this.btnAoiStepIntialize_Click);
            // 
            // tmrMonitor
            // 
            this.tmrMonitor.Interval = 1000;
            this.tmrMonitor.Tick += new System.EventHandler(this.tmrMonitor_Tick);
            // 
            // lblStepEquipStateChangeToNormal
            // 
            this.lblStepEquipStateChangeToNormal.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipStateChangeToNormal.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipStateChangeToNormal.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipStateChangeToNormal.Location = new System.Drawing.Point(596, 63);
            this.lblStepEquipStateChangeToNormal.Name = "lblStepEquipStateChangeToNormal";
            this.lblStepEquipStateChangeToNormal.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipStateChangeToNormal.TabIndex = 100;
            this.lblStepEquipStateChangeToNormal.Text = "0";
            this.lblStepEquipStateChangeToNormal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Black;
            this.label38.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(315, 63);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(277, 18);
            this.label38.TabIndex = 99;
            this.label38.Text = "Command - Eqp State Change To Normal";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStepEquipAlarmClearCmd
            // 
            this.lblStepEquipAlarmClearCmd.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipAlarmClearCmd.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipAlarmClearCmd.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipAlarmClearCmd.Location = new System.Drawing.Point(596, 254);
            this.lblStepEquipAlarmClearCmd.Name = "lblStepEquipAlarmClearCmd";
            this.lblStepEquipAlarmClearCmd.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipAlarmClearCmd.TabIndex = 98;
            this.lblStepEquipAlarmClearCmd.Text = "0";
            this.lblStepEquipAlarmClearCmd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipProcChangeToResume
            // 
            this.lblStepEquipProcChangeToResume.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipProcChangeToResume.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipProcChangeToResume.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipProcChangeToResume.Location = new System.Drawing.Point(596, 101);
            this.lblStepEquipProcChangeToResume.Name = "lblStepEquipProcChangeToResume";
            this.lblStepEquipProcChangeToResume.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipProcChangeToResume.TabIndex = 97;
            this.lblStepEquipProcChangeToResume.Text = "0";
            this.lblStepEquipProcChangeToResume.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipProcChangeToPause
            // 
            this.lblStepEquipProcChangeToPause.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipProcChangeToPause.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipProcChangeToPause.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipProcChangeToPause.Location = new System.Drawing.Point(596, 82);
            this.lblStepEquipProcChangeToPause.Name = "lblStepEquipProcChangeToPause";
            this.lblStepEquipProcChangeToPause.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipProcChangeToPause.TabIndex = 96;
            this.lblStepEquipProcChangeToPause.Text = "0";
            this.lblStepEquipProcChangeToPause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipStateChangeToPM
            // 
            this.lblStepEquipStateChangeToPM.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipStateChangeToPM.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipStateChangeToPM.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipStateChangeToPM.Location = new System.Drawing.Point(596, 44);
            this.lblStepEquipStateChangeToPM.Name = "lblStepEquipStateChangeToPM";
            this.lblStepEquipStateChangeToPM.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipStateChangeToPM.TabIndex = 95;
            this.lblStepEquipStateChangeToPM.Text = "0";
            this.lblStepEquipStateChangeToPM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipEcidChangeEvent
            // 
            this.lblStepEquipEcidChangeEvent.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipEcidChangeEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipEcidChangeEvent.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipEcidChangeEvent.Location = new System.Drawing.Point(282, 226);
            this.lblStepEquipEcidChangeEvent.Name = "lblStepEquipEcidChangeEvent";
            this.lblStepEquipEcidChangeEvent.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipEcidChangeEvent.TabIndex = 94;
            this.lblStepEquipEcidChangeEvent.Text = "0";
            this.lblStepEquipEcidChangeEvent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipRecipeCreateDeleteModifyEvent
            // 
            this.lblStepEquipRecipeCreateDeleteModifyEvent.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipRecipeCreateDeleteModifyEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipRecipeCreateDeleteModifyEvent.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipRecipeCreateDeleteModifyEvent.Location = new System.Drawing.Point(282, 275);
            this.lblStepEquipRecipeCreateDeleteModifyEvent.Name = "lblStepEquipRecipeCreateDeleteModifyEvent";
            this.lblStepEquipRecipeCreateDeleteModifyEvent.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipRecipeCreateDeleteModifyEvent.TabIndex = 90;
            this.lblStepEquipRecipeCreateDeleteModifyEvent.Text = "0";
            this.lblStepEquipRecipeCreateDeleteModifyEvent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.Color.Black;
            this.label48.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(315, 254);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(277, 18);
            this.label48.TabIndex = 89;
            this.label48.Text = "Command - Alarm Clear Cmd";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.Black;
            this.label49.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(315, 101);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(277, 18);
            this.label49.TabIndex = 88;
            this.label49.Text = "Command - Eqp Proc Change To Resume";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.Color.Black;
            this.label50.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(315, 82);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(277, 18);
            this.label50.TabIndex = 87;
            this.label50.Text = "Command - Eqp Proc Change To Pause";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.Color.Black;
            this.label51.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(315, 44);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(277, 18);
            this.label51.TabIndex = 86;
            this.label51.Text = "Command - Eqp State Change To PM ";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.Black;
            this.label52.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(1, 226);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(277, 18);
            this.label52.TabIndex = 85;
            this.label52.Text = "Event - ECID Change Event";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.Color.Black;
            this.label56.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(1, 275);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(277, 18);
            this.label56.TabIndex = 81;
            this.label56.Text = "Event - Recipe Create/Delete/Modify Event";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStepEquipOpcallSetCmd
            // 
            this.lblStepEquipOpcallSetCmd.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipOpcallSetCmd.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipOpcallSetCmd.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipOpcallSetCmd.Location = new System.Drawing.Point(596, 160);
            this.lblStepEquipOpcallSetCmd.Name = "lblStepEquipOpcallSetCmd";
            this.lblStepEquipOpcallSetCmd.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipOpcallSetCmd.TabIndex = 110;
            this.lblStepEquipOpcallSetCmd.Text = "0";
            this.lblStepEquipOpcallSetCmd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.Black;
            this.label58.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(315, 160);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(277, 18);
            this.label58.TabIndex = 109;
            this.label58.Text = "Command - Opcall Set Cmd";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStepEquipTerminalMessageSetCmd
            // 
            this.lblStepEquipTerminalMessageSetCmd.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipTerminalMessageSetCmd.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipTerminalMessageSetCmd.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipTerminalMessageSetCmd.Location = new System.Drawing.Point(596, 179);
            this.lblStepEquipTerminalMessageSetCmd.Name = "lblStepEquipTerminalMessageSetCmd";
            this.lblStepEquipTerminalMessageSetCmd.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipTerminalMessageSetCmd.TabIndex = 106;
            this.lblStepEquipTerminalMessageSetCmd.Text = "0";
            this.lblStepEquipTerminalMessageSetCmd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipTimeSetCmd
            // 
            this.lblStepEquipTimeSetCmd.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipTimeSetCmd.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipTimeSetCmd.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipTimeSetCmd.Location = new System.Drawing.Point(596, 141);
            this.lblStepEquipTimeSetCmd.Name = "lblStepEquipTimeSetCmd";
            this.lblStepEquipTimeSetCmd.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipTimeSetCmd.TabIndex = 105;
            this.lblStepEquipTimeSetCmd.Text = "0";
            this.lblStepEquipTimeSetCmd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.Color.Black;
            this.label65.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label65.ForeColor = System.Drawing.Color.White;
            this.label65.Location = new System.Drawing.Point(315, 179);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(277, 18);
            this.label65.TabIndex = 102;
            this.label65.Text = "Command - Terminal Message Set Cmd";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.Color.Black;
            this.label66.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(315, 141);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(277, 18);
            this.label66.TabIndex = 101;
            this.label66.Text = "Command - Time Set Cmd";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStepEquipEinModeSetCmd
            // 
            this.lblStepEquipEinModeSetCmd.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipEinModeSetCmd.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipEinModeSetCmd.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipEinModeSetCmd.Location = new System.Drawing.Point(596, 226);
            this.lblStepEquipEinModeSetCmd.Name = "lblStepEquipEinModeSetCmd";
            this.lblStepEquipEinModeSetCmd.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipEinModeSetCmd.TabIndex = 114;
            this.lblStepEquipEinModeSetCmd.Text = "0";
            this.lblStepEquipEinModeSetCmd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.Color.Black;
            this.label70.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label70.ForeColor = System.Drawing.Color.White;
            this.label70.Location = new System.Drawing.Point(315, 226);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(277, 18);
            this.label70.TabIndex = 113;
            this.label70.Text = "Command - EIN Mode Set Cmd";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(1, 245);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 18);
            this.label1.TabIndex = 85;
            this.label1.Text = "Event - EOID Change Event";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStepEquipEoidChangeEvent
            // 
            this.lblStepEquipEoidChangeEvent.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipEoidChangeEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipEoidChangeEvent.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipEoidChangeEvent.Location = new System.Drawing.Point(282, 245);
            this.lblStepEquipEoidChangeEvent.Name = "lblStepEquipEoidChangeEvent";
            this.lblStepEquipEoidChangeEvent.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipEoidChangeEvent.TabIndex = 94;
            this.lblStepEquipEoidChangeEvent.Text = "0";
            this.lblStepEquipEoidChangeEvent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStepEquipCycleStopCmd
            // 
            this.lblStepEquipCycleStopCmd.BackColor = System.Drawing.Color.Black;
            this.lblStepEquipCycleStopCmd.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblStepEquipCycleStopCmd.ForeColor = System.Drawing.Color.White;
            this.lblStepEquipCycleStopCmd.Location = new System.Drawing.Point(596, 120);
            this.lblStepEquipCycleStopCmd.Name = "lblStepEquipCycleStopCmd";
            this.lblStepEquipCycleStopCmd.Size = new System.Drawing.Size(27, 18);
            this.lblStepEquipCycleStopCmd.TabIndex = 116;
            this.lblStepEquipCycleStopCmd.Text = "0";
            this.lblStepEquipCycleStopCmd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(315, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(277, 18);
            this.label3.TabIndex = 115;
            this.label3.Text = "Command - Eqp Cycle Stop Cmd";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UcrlScenarioMon
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblStepEquipCycleStopCmd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblStepEquipEinModeSetCmd);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.lblStepEquipOpcallSetCmd);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.lblStepEquipTerminalMessageSetCmd);
            this.Controls.Add(this.lblStepEquipTimeSetCmd);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.lblStepEquipStateChangeToNormal);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.lblStepEquipAlarmClearCmd);
            this.Controls.Add(this.lblStepEquipProcChangeToResume);
            this.Controls.Add(this.lblStepEquipProcChangeToPause);
            this.Controls.Add(this.lblStepEquipStateChangeToPM);
            this.Controls.Add(this.lblStepEquipEoidChangeEvent);
            this.Controls.Add(this.lblStepEquipEcidChangeEvent);
            this.Controls.Add(this.lblStepEquipRecipeCreateDeleteModifyEvent);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.btnAoiStepIntialize);
            this.Controls.Add(this.lblStepEquipAlarmHappen);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblStepEquipCurrentPPidChange);
            this.Controls.Add(this.lblStepEquipAlarmClear);
            this.Controls.Add(this.lblStepEquipPanelUnscrap);
            this.Controls.Add(this.lblStepEquipPanelScrap);
            this.Controls.Add(this.lblStepEquipPanelunloadingComplete);
            this.Controls.Add(this.lblStepEquipPanelunloading);
            this.Controls.Add(this.lblStepEquipPanelLoading);
            this.Controls.Add(this.lblStepEquipEqpProcChange);
            this.Controls.Add(this.lblStepEquipEqpStateChange);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Name = "UcrlScenarioMon";
            this.Size = new System.Drawing.Size(667, 500);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblStepEquipAlarmHappen;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label lblStepEquipCurrentPPidChange;
        internal System.Windows.Forms.Label lblStepEquipAlarmClear;
        internal System.Windows.Forms.Label lblStepEquipPanelUnscrap;
        internal System.Windows.Forms.Label lblStepEquipPanelScrap;
        internal System.Windows.Forms.Label lblStepEquipPanelunloadingComplete;
        internal System.Windows.Forms.Label lblStepEquipPanelunloading;
        internal System.Windows.Forms.Label lblStepEquipPanelLoading;
        internal System.Windows.Forms.Label lblStepEquipEqpProcChange;
        internal System.Windows.Forms.Label lblStepEquipEqpStateChange;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.Label label27;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.Label label31;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label label33;
        internal System.Windows.Forms.Label label34;
        internal System.Windows.Forms.Button btnAoiStepIntialize;
        private System.Windows.Forms.Timer tmrMonitor;
        internal System.Windows.Forms.Label lblStepEquipStateChangeToNormal;
        internal System.Windows.Forms.Label label38;
        internal System.Windows.Forms.Label lblStepEquipAlarmClearCmd;
        internal System.Windows.Forms.Label lblStepEquipProcChangeToResume;
        internal System.Windows.Forms.Label lblStepEquipProcChangeToPause;
        internal System.Windows.Forms.Label lblStepEquipStateChangeToPM;
        internal System.Windows.Forms.Label lblStepEquipEcidChangeEvent;
        internal System.Windows.Forms.Label lblStepEquipRecipeCreateDeleteModifyEvent;
        internal System.Windows.Forms.Label label48;
        internal System.Windows.Forms.Label label49;
        internal System.Windows.Forms.Label label50;
        internal System.Windows.Forms.Label label51;
        internal System.Windows.Forms.Label label52;
        internal System.Windows.Forms.Label label56;
        internal System.Windows.Forms.Label lblStepEquipOpcallSetCmd;
        internal System.Windows.Forms.Label label58;
        internal System.Windows.Forms.Label lblStepEquipTerminalMessageSetCmd;
        internal System.Windows.Forms.Label lblStepEquipTimeSetCmd;
        internal System.Windows.Forms.Label label65;
        internal System.Windows.Forms.Label label66;
        internal System.Windows.Forms.Label lblStepEquipEinModeSetCmd;
        internal System.Windows.Forms.Label label70;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label lblStepEquipEoidChangeEvent;
        internal System.Windows.Forms.Label lblStepEquipCycleStopCmd;
        internal System.Windows.Forms.Label label3;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.Common;

namespace DitInlineCim.Ucrl
{
    [System.ComponentModel.ToolboxItem(false)]
    public partial class UcrlRPC : UserControl
    {
        public UcrlRPC()
        {
            InitializeComponent();
 
        }

        private void Intialize()
        {
            //////////////////////////////////////////////////////////////////////////
            DataGridViewCellStyle headerCellStyle1 = new DataGridViewCellStyle();
            headerCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;

            gridRpc.ColumnHeadersDefaultCellStyle = headerCellStyle1;
            gridRpc.DefaultCellStyle = headerCellStyle1;
            //////////////////////////////////////////////////////////////////////////


            gridRpc.RowHeadersVisible = false;

            string colName = "SET_TIME,H_GLASSID,JOBID,RPC_PPID,ORIGINAL_PPID,RPC_STATE";

            string[] colHeaders = colName.Split(',');
            for (int i = 0; i < colHeaders.Length; i++)
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

                column.Name = colHeaders[i];
                column.HeaderText = colHeaders[i];
                column.Width = 120;
                gridRpc.Columns.Add(column);

            }
        }


        public void UpdateData()
        {
            // original ppid 
            string orgPPID = "";
            string rpcState = "";

            gridRpc.Rows.Clear(); 
            Dictionary<string, RpcItem> dicRpc = GG.Equip.RpcMgr.DicRpc;
            foreach (KeyValuePair<string, RpcItem> kvp in dicRpc)
            {
                RpcItem rpcObj = kvp.Value;
                switch (rpcObj.RpcState)
                {
                    case 1:
                        rpcState = "Waiting";
                        break;
                    case 2:
                        rpcState = "Running";
                        //orgPPID = GG.Equip.BODY.RmmMgr.CurrentPPID;
                        break;
                    case 3:
                        rpcState = "Done";
                        break;

                }

                gridRpc.Rows.Add(rpcObj.SetTime, rpcObj.HGlassID, rpcObj.JobID, rpcObj.RpcPPID, orgPPID, rpcState);
            }

            if (gridRpc.RowCount > 1)
            {
                gridRpc.FirstDisplayedScrollingRowIndex = gridRpc.RowCount - 1;
            }
            gridRpc.ClearSelection();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.Common;
using DitInlineCim.Struct;

namespace DitInlineCim.Ucrl
{
    [System.ComponentModel.ToolboxItem(false)]
    public partial class UcrlScenarioMon : UserControl
    {
        public UcrlScenarioMon()
        {
            InitializeComponent();
        }

        public void Start()
        {
            tmrMonitor.Start();
        }
        public void Stop()
        {
            tmrMonitor.Stop();
        }

        private void tmrMonitor_Tick(object sender, EventArgs e)
        {
            //AOI 

            //lblStepEquipEqpStateChange.Text = GG.Equip.Scenario.StepEquipStateChangeEvent.ToString();
            //lblStepEquipEqpProcChange.Text = GG.Equip.Scenario.StepEquipProcChangeEvent.ToString();

            //lblStepEquipPanelLoading.Text = GG.Equip.Scenario.StepEquipPanelLoadingEvent.ToString();
            //lblStepEquipPanelunloading.Text = GG.Equip.Scenario.StepEquipPanelUnloadingEvent.ToString();
            //lblStepEquipPanelunloadingComplete.Text = GG.Equip.Scenario.StepEquipPanelUnloadCompleteEvent.ToString();
            //lblStepEquipPanelScrap.Text = GG.Equip.Scenario.StepEquipPanelBrokenEvent.ToString();
            //lblStepEquipPanelUnscrap.Text = GG.Equip.Scenario.StepEquipPanelUnBrokenEvent.ToString();

            //lblStepEquipAlarmHappen.Text = GG.Equip.Scenario.StepAlarmHappen.ToString();
            //lblStepEquipAlarmClear.Text = GG.Equip.Scenario.StepAlarmClear.ToString();

            //lblStepEquipEoidChangeEvent.Text = GG.Equip.Scenario.StepEquipEoidChangeEvent.ToString(); 
            //lblStepEquipEcidChangeEvent.Text = GG.Equip.Scenario.StepEquipEcidChangeEvent.ToString();
            
            //lblStepEquipRecipeCreateDeleteModifyEvent.Text = GG.Equip.Scenario.StepEquipRecipeCreateDeleteModifyEvent.ToString();
            //lblStepEquipCurrentPPidChange.Text = GG.Equip.Scenario.StepEquipCurrentPPIDChangeEvent.ToString();


            //lblStepEquipProcChangeToPause.Text = GG.Equip.Scenario.StepEquipProcPauseCmd.ToString();
            //lblStepEquipProcChangeToResume.Text = GG.Equip.Scenario.StepEquipProcResumeCmd.ToString();
            //lblStepEquipStateChangeToPM.Text = GG.Equip.Scenario.StepEquipStatePMCmd.ToString();
            //lblStepEquipStateChangeToNormal.Text = GG.Equip.Scenario.StepEquipStateNormalCmd.ToString();
            //lblStepEquipCycleStopCmd.Text = GG.Equip.Scenario.StepEquipCycleStopCmd.ToString();
            
            //lblStepEquipTimeSetCmd.Text = GG.Equip.Scenario.StepEqipTimeSetCmd.ToString();
            //lblStepEquipOpcallSetCmd.Text = GG.Equip.Scenario.StepEquipOpCallSetCmd.ToString();
            //lblStepEquipTerminalMessageSetCmd.Text = GG.Equip.Scenario.StepEquipTerMsgSetCmd.ToString();
            //lblStepEquipEinModeSetCmd.Text = GG.Equip.Scenario.StepEquipEinModeChangeCmd.ToString();

            //lblStepEquipAlarmClearCmd.Text = GG.Equip.Scenario.StepAoiAlarmClearCmd.ToString();            
        }

        private void btnIndexStepIntialize_Click(object sender, EventArgs e)
        {
            //GG.Equip.Scenario.IntiaizlieIndexScenario();
        }

        private void btnAoiStepIntialize_Click(object sender, EventArgs e)
        {
            //GG.Equip.Scenario.IntiaizlieAoiScenario();
        }
    }
}

﻿namespace DitInlineCim.Ucrl
{
    partial class UcrlAPC
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridApc = new System.Windows.Forms.DataGridView();
            this.SET_TIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H_GLASSID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JOBID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECIPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_PARM_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_PARM_VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.APC_STATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.lbl01 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridApc)).BeginInit();
            this.SuspendLayout();
            // 
            // gridApc
            // 
            this.gridApc.AllowUserToAddRows = false;
            this.gridApc.AllowUserToDeleteRows = false;
            this.gridApc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridApc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SET_TIME,
            this.H_GLASSID,
            this.JOBID,
            this.RECIPE,
            this.P_PARM_NAME,
            this.P_PARM_VALUE,
            this.APC_STATE});
            this.gridApc.Location = new System.Drawing.Point(5, 40);
            this.gridApc.Name = "gridApc";
            this.gridApc.ReadOnly = true;
            this.gridApc.RowTemplate.Height = 23;
            this.gridApc.Size = new System.Drawing.Size(756, 392);
            this.gridApc.TabIndex = 6;
            // 
            // SET_TIME
            // 
            this.SET_TIME.HeaderText = "SET_TIME";
            this.SET_TIME.Name = "SET_TIME";
            this.SET_TIME.ReadOnly = true;
            // 
            // H_GLASSID
            // 
            this.H_GLASSID.HeaderText = "H_GLASSID";
            this.H_GLASSID.Name = "H_GLASSID";
            this.H_GLASSID.ReadOnly = true;
            // 
            // JOBID
            // 
            this.JOBID.HeaderText = "JOBID";
            this.JOBID.Name = "JOBID";
            this.JOBID.ReadOnly = true;
            // 
            // RECIPE
            // 
            this.RECIPE.HeaderText = "RECIPE";
            this.RECIPE.Name = "RECIPE";
            this.RECIPE.ReadOnly = true;
            // 
            // P_PARM_NAME
            // 
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.P_PARM_NAME.DefaultCellStyle = dataGridViewCellStyle1;
            this.P_PARM_NAME.HeaderText = "P_PARM_NAME";
            this.P_PARM_NAME.Name = "P_PARM_NAME";
            this.P_PARM_NAME.ReadOnly = true;
            // 
            // P_PARM_VALUE
            // 
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.P_PARM_VALUE.DefaultCellStyle = dataGridViewCellStyle2;
            this.P_PARM_VALUE.HeaderText = "P_PARM_VALUE";
            this.P_PARM_VALUE.Name = "P_PARM_VALUE";
            this.P_PARM_VALUE.ReadOnly = true;
            // 
            // APC_STATE
            // 
            this.APC_STATE.HeaderText = "APC_STATE";
            this.APC_STATE.Name = "APC_STATE";
            this.APC_STATE.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 438);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 35);
            this.button1.TabIndex = 7;
            this.button1.Text = "History";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lbl01
            // 
            this.lbl01.AutoEllipsis = true;
            this.lbl01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl01.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl01.ForeColor = System.Drawing.Color.White;
            this.lbl01.Location = new System.Drawing.Point(5, 5);
            this.lbl01.Name = "lbl01";
            this.lbl01.Size = new System.Drawing.Size(756, 32);
            this.lbl01.TabIndex = 53;
            this.lbl01.Text = "Advanced Process Control (APC)";
            this.lbl01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UcrlAPC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lbl01);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gridApc);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlAPC";
            this.Size = new System.Drawing.Size(764, 477);
            ((System.ComponentModel.ISupportInitialize)(this.gridApc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridApc;
        private System.Windows.Forms.DataGridViewTextBoxColumn SET_TIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn H_GLASSID;
        private System.Windows.Forms.DataGridViewTextBoxColumn JOBID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RECIPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_PARM_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_PARM_VALUE;
        private System.Windows.Forms.DataGridViewTextBoxColumn APC_STATE;
        private System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Label lbl01;
    }
}

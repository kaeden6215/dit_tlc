﻿namespace DitInlineCim.Struct
{
    partial class UcrlEoid
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnEOID31 = new System.Windows.Forms.Panel();
            this.rdoEoid_01_1_2 = new System.Windows.Forms.RadioButton();
            this.rdoEoid_01_1_1 = new System.Windows.Forms.RadioButton();
            this.Label19 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdoEoid_01_2_2 = new System.Windows.Forms.RadioButton();
            this.rdoEoid_01_2_1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rdoEoid_02_0_1 = new System.Windows.Forms.RadioButton();
            this.rdoEoid_02_0_0 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rdoEoid_03_0_1 = new System.Windows.Forms.RadioButton();
            this.rdoEoid_03_0_0 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rdoEoid_202_1_1 = new System.Windows.Forms.RadioButton();
            this.rdoEoid_202_1_0 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.btnEOIDCancel = new System.Windows.Forms.Button();
            this.btnEOIDSave = new System.Windows.Forms.Button();
            this.lblEoidTitle = new System.Windows.Forms.Label();
            this.rdoEoid_02_0_2 = new System.Windows.Forms.RadioButton();
            this.rdoEoid_03_0_2 = new System.Windows.Forms.RadioButton();
            this.pnEOID31.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnEOID31
            // 
            this.pnEOID31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnEOID31.Controls.Add(this.rdoEoid_01_1_2);
            this.pnEOID31.Controls.Add(this.rdoEoid_01_1_1);
            this.pnEOID31.Controls.Add(this.Label19);
            this.pnEOID31.Location = new System.Drawing.Point(6, 42);
            this.pnEOID31.Name = "pnEOID31";
            this.pnEOID31.Size = new System.Drawing.Size(870, 25);
            this.pnEOID31.TabIndex = 10;
            // 
            // rdoEoid_01_1_2
            // 
            this.rdoEoid_01_1_2.AutoSize = true;
            this.rdoEoid_01_1_2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_01_1_2.Location = new System.Drawing.Point(359, 1);
            this.rdoEoid_01_1_2.Name = "rdoEoid_01_1_2";
            this.rdoEoid_01_1_2.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_01_1_2.TabIndex = 2;
            this.rdoEoid_01_1_2.TabStop = true;
            this.rdoEoid_01_1_2.Text = "2L";
            this.rdoEoid_01_1_2.UseVisualStyleBackColor = true;
            // 
            // rdoEoid_01_1_1
            // 
            this.rdoEoid_01_1_1.AutoSize = true;
            this.rdoEoid_01_1_1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_01_1_1.Location = new System.Drawing.Point(281, 1);
            this.rdoEoid_01_1_1.Name = "rdoEoid_01_1_1";
            this.rdoEoid_01_1_1.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_01_1_1.TabIndex = 1;
            this.rdoEoid_01_1_1.TabStop = true;
            this.rdoEoid_01_1_1.Text = "1L";
            this.rdoEoid_01_1_1.UseVisualStyleBackColor = true;
            // 
            // Label19
            // 
            this.Label19.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.Label19.ForeColor = System.Drawing.Color.Blue;
            this.Label19.Location = new System.Drawing.Point(4, 1);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(265, 20);
            this.Label19.TabIndex = 0;
            this.Label19.Text = "01. Component Trace (1)";
            this.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rdoEoid_01_2_2);
            this.panel1.Controls.Add(this.rdoEoid_01_2_1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(6, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 25);
            this.panel1.TabIndex = 11;
            // 
            // rdoEoid_01_2_2
            // 
            this.rdoEoid_01_2_2.AutoSize = true;
            this.rdoEoid_01_2_2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_01_2_2.Location = new System.Drawing.Point(359, 3);
            this.rdoEoid_01_2_2.Name = "rdoEoid_01_2_2";
            this.rdoEoid_01_2_2.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_01_2_2.TabIndex = 2;
            this.rdoEoid_01_2_2.TabStop = true;
            this.rdoEoid_01_2_2.Text = "2L";
            this.rdoEoid_01_2_2.UseVisualStyleBackColor = true;
            // 
            // rdoEoid_01_2_1
            // 
            this.rdoEoid_01_2_1.AutoSize = true;
            this.rdoEoid_01_2_1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_01_2_1.Location = new System.Drawing.Point(281, 3);
            this.rdoEoid_01_2_1.Name = "rdoEoid_01_2_1";
            this.rdoEoid_01_2_1.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_01_2_1.TabIndex = 1;
            this.rdoEoid_01_2_1.TabStop = true;
            this.rdoEoid_01_2_1.Text = "1L";
            this.rdoEoid_01_2_1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(4, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(265, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "01. Component Trace (2)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.rdoEoid_02_0_2);
            this.panel2.Controls.Add(this.rdoEoid_02_0_1);
            this.panel2.Controls.Add(this.rdoEoid_02_0_0);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(6, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 25);
            this.panel2.TabIndex = 12;
            // 
            // rdoEoid_02_0_1
            // 
            this.rdoEoid_02_0_1.AutoSize = true;
            this.rdoEoid_02_0_1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_02_0_1.Location = new System.Drawing.Point(359, 3);
            this.rdoEoid_02_0_1.Name = "rdoEoid_02_0_1";
            this.rdoEoid_02_0_1.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_02_0_1.TabIndex = 2;
            this.rdoEoid_02_0_1.TabStop = true;
            this.rdoEoid_02_0_1.Text = "1L";
            this.rdoEoid_02_0_1.UseVisualStyleBackColor = true;
            // 
            // rdoEoid_02_0_0
            // 
            this.rdoEoid_02_0_0.AutoSize = true;
            this.rdoEoid_02_0_0.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_02_0_0.Location = new System.Drawing.Point(281, 3);
            this.rdoEoid_02_0_0.Name = "rdoEoid_02_0_0";
            this.rdoEoid_02_0_0.Size = new System.Drawing.Size(60, 19);
            this.rdoEoid_02_0_0.TabIndex = 1;
            this.rdoEoid_02_0_0.TabStop = true;
            this.rdoEoid_02_0_0.Text = "0 : Off";
            this.rdoEoid_02_0_0.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(4, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "02. Equipment State Trace";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.rdoEoid_03_0_2);
            this.panel3.Controls.Add(this.rdoEoid_03_0_1);
            this.panel3.Controls.Add(this.rdoEoid_03_0_0);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(6, 135);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(870, 25);
            this.panel3.TabIndex = 13;
            // 
            // rdoEoid_03_0_1
            // 
            this.rdoEoid_03_0_1.AutoSize = true;
            this.rdoEoid_03_0_1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_03_0_1.Location = new System.Drawing.Point(359, 3);
            this.rdoEoid_03_0_1.Name = "rdoEoid_03_0_1";
            this.rdoEoid_03_0_1.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_03_0_1.TabIndex = 2;
            this.rdoEoid_03_0_1.TabStop = true;
            this.rdoEoid_03_0_1.Text = "1L";
            this.rdoEoid_03_0_1.UseVisualStyleBackColor = true;
            // 
            // rdoEoid_03_0_0
            // 
            this.rdoEoid_03_0_0.AutoSize = true;
            this.rdoEoid_03_0_0.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_03_0_0.Location = new System.Drawing.Point(281, 3);
            this.rdoEoid_03_0_0.Name = "rdoEoid_03_0_0";
            this.rdoEoid_03_0_0.Size = new System.Drawing.Size(60, 19);
            this.rdoEoid_03_0_0.TabIndex = 1;
            this.rdoEoid_03_0_0.TabStop = true;
            this.rdoEoid_03_0_0.Text = "0 : Off";
            this.rdoEoid_03_0_0.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(4, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(265, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "03. Equipment Process State Trace";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.rdoEoid_202_1_1);
            this.panel4.Controls.Add(this.rdoEoid_202_1_0);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(6, 166);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(870, 25);
            this.panel4.TabIndex = 14;
            // 
            // rdoEoid_202_1_1
            // 
            this.rdoEoid_202_1_1.AutoSize = true;
            this.rdoEoid_202_1_1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_202_1_1.Location = new System.Drawing.Point(359, 3);
            this.rdoEoid_202_1_1.Name = "rdoEoid_202_1_1";
            this.rdoEoid_202_1_1.Size = new System.Drawing.Size(79, 19);
            this.rdoEoid_202_1_1.TabIndex = 2;
            this.rdoEoid_202_1_1.TabStop = true;
            this.rdoEoid_202_1_1.Text = "1 : INLINE";
            this.rdoEoid_202_1_1.UseVisualStyleBackColor = true;
            // 
            // rdoEoid_202_1_0
            // 
            this.rdoEoid_202_1_0.AutoSize = true;
            this.rdoEoid_202_1_0.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_202_1_0.Location = new System.Drawing.Point(281, 3);
            this.rdoEoid_202_1_0.Name = "rdoEoid_202_1_0";
            this.rdoEoid_202_1_0.Size = new System.Drawing.Size(74, 19);
            this.rdoEoid_202_1_0.TabIndex = 1;
            this.rdoEoid_202_1_0.TabStop = true;
            this.rdoEoid_202_1_0.Text = "0 : HOST";
            this.rdoEoid_202_1_0.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(4, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(265, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "202. Process Control Priority (1)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnEOIDCancel
            // 
            this.btnEOIDCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnEOIDCancel.Font = new System.Drawing.Font("Arial Black", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEOIDCancel.ForeColor = System.Drawing.Color.Black;
            this.btnEOIDCancel.Location = new System.Drawing.Point(775, 228);
            this.btnEOIDCancel.Name = "btnEOIDCancel";
            this.btnEOIDCancel.Size = new System.Drawing.Size(101, 33);
            this.btnEOIDCancel.TabIndex = 16;
            this.btnEOIDCancel.Tag = "3";
            this.btnEOIDCancel.Text = "CANCEL";
            this.btnEOIDCancel.UseVisualStyleBackColor = false;
            this.btnEOIDCancel.Click += new System.EventHandler(this.btnEOIDCancel_Click);
            // 
            // btnEOIDSave
            // 
            this.btnEOIDSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnEOIDSave.Font = new System.Drawing.Font("Arial Black", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEOIDSave.ForeColor = System.Drawing.Color.Black;
            this.btnEOIDSave.Location = new System.Drawing.Point(667, 228);
            this.btnEOIDSave.Name = "btnEOIDSave";
            this.btnEOIDSave.Size = new System.Drawing.Size(101, 33);
            this.btnEOIDSave.TabIndex = 15;
            this.btnEOIDSave.Tag = "3";
            this.btnEOIDSave.Text = "Save";
            this.btnEOIDSave.UseVisualStyleBackColor = false;
            this.btnEOIDSave.Click += new System.EventHandler(this.btnEOIDSave_Click);
            // 
            // lblEoidTitle
            // 
            this.lblEoidTitle.AutoEllipsis = true;
            this.lblEoidTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEoidTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEoidTitle.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEoidTitle.ForeColor = System.Drawing.Color.White;
            this.lblEoidTitle.Location = new System.Drawing.Point(6, 7);
            this.lblEoidTitle.Name = "lblEoidTitle";
            this.lblEoidTitle.Size = new System.Drawing.Size(378, 32);
            this.lblEoidTitle.TabIndex = 17;
            this.lblEoidTitle.Text = "Equipment Online Parameter";
            this.lblEoidTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rdoEoid_02_0_2
            // 
            this.rdoEoid_02_0_2.AutoSize = true;
            this.rdoEoid_02_0_2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_02_0_2.Location = new System.Drawing.Point(415, 2);
            this.rdoEoid_02_0_2.Name = "rdoEoid_02_0_2";
            this.rdoEoid_02_0_2.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_02_0_2.TabIndex = 3;
            this.rdoEoid_02_0_2.TabStop = true;
            this.rdoEoid_02_0_2.Text = "2L";
            this.rdoEoid_02_0_2.UseVisualStyleBackColor = true;
            // 
            // rdoEoid_03_0_2
            // 
            this.rdoEoid_03_0_2.AutoSize = true;
            this.rdoEoid_03_0_2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rdoEoid_03_0_2.Location = new System.Drawing.Point(415, 2);
            this.rdoEoid_03_0_2.Name = "rdoEoid_03_0_2";
            this.rdoEoid_03_0_2.Size = new System.Drawing.Size(38, 19);
            this.rdoEoid_03_0_2.TabIndex = 3;
            this.rdoEoid_03_0_2.TabStop = true;
            this.rdoEoid_03_0_2.Text = "2L";
            this.rdoEoid_03_0_2.UseVisualStyleBackColor = true;
            // 
            // UcrlEoid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblEoidTitle);
            this.Controls.Add(this.btnEOIDCancel);
            this.Controls.Add(this.btnEOIDSave);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnEOID31);
            this.Name = "UcrlEoid";
            this.Size = new System.Drawing.Size(886, 283);
            this.pnEOID31.ResumeLayout(false);
            this.pnEOID31.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel pnEOID31;
        internal System.Windows.Forms.RadioButton rdoEoid_01_1_2;
        internal System.Windows.Forms.RadioButton rdoEoid_01_1_1;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.RadioButton rdoEoid_01_2_2;
        internal System.Windows.Forms.RadioButton rdoEoid_01_2_1;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.RadioButton rdoEoid_02_0_1;
        internal System.Windows.Forms.RadioButton rdoEoid_02_0_0;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.RadioButton rdoEoid_03_0_1;
        internal System.Windows.Forms.RadioButton rdoEoid_03_0_0;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Panel panel4;
        internal System.Windows.Forms.RadioButton rdoEoid_202_1_1;
        internal System.Windows.Forms.RadioButton rdoEoid_202_1_0;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Button btnEOIDCancel;
        internal System.Windows.Forms.Button btnEOIDSave;
        internal System.Windows.Forms.Label lblEoidTitle;
        internal System.Windows.Forms.RadioButton rdoEoid_02_0_2;
        internal System.Windows.Forms.RadioButton rdoEoid_03_0_2;
    }
}

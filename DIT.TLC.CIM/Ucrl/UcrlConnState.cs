﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DitInlineCim.Ucrl
{
    [System.ComponentModel.ToolboxItem(false)]
    public partial class UcrlConnState : UserControl
    {
        public UcrlConnState()
        {
            InitializeComponent();
        }

        public Color EquipPLCConnColor
        {
            get { return lblEquipPlc.BackColor; }
            set { lblEquipPlc.BackColor = value; }
        }
        public Color HostConnColor
        {
            get { return lblHost.BackColor; }
            set { lblHost.BackColor = value; }
        }        
    }
}

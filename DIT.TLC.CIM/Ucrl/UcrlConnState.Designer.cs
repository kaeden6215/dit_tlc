﻿namespace DitInlineCim.Ucrl
{
    partial class UcrlConnState
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEquipPlc = new System.Windows.Forms.Panel();
            this.Label2 = new System.Windows.Forms.Label();
            this.lblHost = new System.Windows.Forms.Panel();
            this.Label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblEquipPlc.SuspendLayout();
            this.lblHost.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblIndex
            // 
            this.lblEquipPlc.BackColor = System.Drawing.Color.Red;
            this.lblEquipPlc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEquipPlc.Controls.Add(this.Label2);
            this.lblEquipPlc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEquipPlc.Location = new System.Drawing.Point(119, 3);
            this.lblEquipPlc.Name = "lblIndex";
            this.lblEquipPlc.Size = new System.Drawing.Size(110, 39);
            this.lblEquipPlc.TabIndex = 301;
            // 
            // Label2
            // 
            this.Label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label2.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(0, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(108, 37);
            this.Label2.TabIndex = 300;
            this.Label2.Text = "PLC";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHost
            // 
            this.lblHost.BackColor = System.Drawing.Color.Red;
            this.lblHost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHost.Controls.Add(this.Label1);
            this.lblHost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHost.Location = new System.Drawing.Point(3, 3);
            this.lblHost.Name = "lblHost";
            this.lblHost.Size = new System.Drawing.Size(110, 39);
            this.lblHost.TabIndex = 299;
            // 
            // Label1
            // 
            this.Label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label1.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(0, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(108, 37);
            this.Label1.TabIndex = 298;
            this.Label1.Text = "HOST";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.453415F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.453415F));
            this.tableLayoutPanel1.Controls.Add(this.lblHost, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblEquipPlc, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(232, 45);
            this.tableLayoutPanel1.TabIndex = 304;
            // 
            // UcrlConnState
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.Name = "UcrlConnState";
            this.Size = new System.Drawing.Size(232, 45);
            this.lblEquipPlc.ResumeLayout(false);
            this.lblHost.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel lblEquipPlc;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Panel lblHost;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.Common;
using DitInlineCim.Struct.APC;

namespace DitInlineCim.Ucrl
{
    [System.ComponentModel.ToolboxItem(false)]
    public partial class UcrlAPC : UserControl
    {
        public UcrlAPC()
        {
            InitializeComponent();
        }

        public void UpdateData()
        {
            string apcState = "";
            string p_parm_name = "";
            string p_parm_value = "";

            gridApc.Rows.Clear();

            foreach (KeyValuePair<int, ApcItem> kvp in GG.Equip.ApcMgr.DicApcItems)
            {
                ApcItem apcObj = kvp.Value;
                p_parm_name = "";
                p_parm_value = "";

               if (apcObj == null) continue;

                switch (apcObj.ApcState)
                {
                    case 1:
                        apcState = "Waiting";
                        break;
                    case 2:
                        apcState = "Running";
                        break;
                    case 3:
                        apcState = "Done";
                        break;

                }

                foreach (ApcParamItem ApcBodykvp in kvp.Value.LstParams)
                {
                    p_parm_name += ApcBodykvp.Name + "\n";
                    p_parm_value += ApcBodykvp.Value + "\n";
                }

                gridApc.Rows.Add(apcObj.SetTime, apcObj.GlassId, apcObj.JobId, apcObj.PpId, p_parm_name, p_parm_value, apcState);
            }

            if (gridApc.RowCount > 1)
                gridApc.FirstDisplayedScrollingRowIndex = gridApc.RowCount - 1;

            gridApc.ClearSelection();
        }
    }
}

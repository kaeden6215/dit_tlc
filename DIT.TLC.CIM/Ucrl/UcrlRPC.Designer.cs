﻿namespace DitInlineCim.Ucrl
{
    partial class UcrlRPC
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.lbl01 = new System.Windows.Forms.Label();
            this.gridRpc = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridRpc)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 438);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 35);
            this.button1.TabIndex = 55;
            this.button1.Text = "History";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lbl01
            // 
            this.lbl01.AutoEllipsis = true;
            this.lbl01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl01.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl01.ForeColor = System.Drawing.Color.White;
            this.lbl01.Location = new System.Drawing.Point(5, 5);
            this.lbl01.Name = "lbl01";
            this.lbl01.Size = new System.Drawing.Size(756, 32);
            this.lbl01.TabIndex = 56;
            this.lbl01.Text = "Recipe Process Control (RPC)";
            this.lbl01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gridRpc
            // 
            this.gridRpc.AllowUserToAddRows = false;
            this.gridRpc.AllowUserToDeleteRows = false;
            this.gridRpc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRpc.Location = new System.Drawing.Point(5, 40);
            this.gridRpc.Name = "gridRpc";
            this.gridRpc.ReadOnly = true;
            this.gridRpc.RowTemplate.Height = 23;
            this.gridRpc.Size = new System.Drawing.Size(756, 392);
            this.gridRpc.TabIndex = 57;
            // 
            // UcrlRPC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.gridRpc);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl01);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "UcrlRPC";
            this.Size = new System.Drawing.Size(791, 496);
            ((System.ComponentModel.ISupportInitialize)(this.gridRpc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Label lbl01;
        private System.Windows.Forms.DataGridView gridRpc;
    }
}

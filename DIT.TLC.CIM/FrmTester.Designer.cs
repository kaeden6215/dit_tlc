﻿namespace DitInlineCim
{
    partial class FrmTester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEqStateChangeEvent = new System.Windows.Forms.Button();
            this.txtProcByWho = new System.Windows.Forms.TextBox();
            this.txtEquipByWho = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGlassLoadingEvent = new System.Windows.Forms.Button();
            this.btnGlassUnLoadingEvent = new System.Windows.Forms.Button();
            this.btnGlassUnLoadingCompleteEvent = new System.Windows.Forms.Button();
            this.btnGlassUnLoadComplete = new System.Windows.Forms.Button();
            this.btnLdIn = new System.Windows.Forms.Button();
            this.btnLdOut = new System.Windows.Forms.Button();
            this.btnAlarmHappenEvent = new System.Windows.Forms.Button();
            this.btnAlarmClearEvent = new System.Windows.Forms.Button();
            this.btnPpidCRUDEvent = new System.Windows.Forms.Button();
            this.btnCurrPpIdChangeEvent = new System.Windows.Forms.Button();
            this.btnMessageBox = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.nudHostRmmNo = new System.Windows.Forms.NumericUpDown();
            this.nudEqpRmmNo = new System.Windows.Forms.NumericUpDown();
            this.txtParam03 = new System.Windows.Forms.TextBox();
            this.txtParam02 = new System.Windows.Forms.TextBox();
            this.txtPpidName = new System.Windows.Forms.TextBox();
            this.txtRecipeName = new System.Windows.Forms.TextBox();
            this.txtRecipeNo = new System.Windows.Forms.TextBox();
            this.txtParam01 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRmmHostWrite = new System.Windows.Forms.Button();
            this.btnRmmEqpWrite = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rdoEqp = new System.Windows.Forms.RadioButton();
            this.rdoOper = new System.Windows.Forms.RadioButton();
            this.rdoHost = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlProc = new System.Windows.Forms.Panel();
            this.rdoReady = new System.Windows.Forms.RadioButton();
            this.rdoSetup = new System.Windows.Forms.RadioButton();
            this.rdoPause = new System.Windows.Forms.RadioButton();
            this.rdoExecute = new System.Windows.Forms.RadioButton();
            this.rdoIdle = new System.Windows.Forms.RadioButton();
            this.btnEqProcChangeEvent = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdoEqStatePm = new System.Windows.Forms.RadioButton();
            this.rdoEqStateFault = new System.Windows.Forms.RadioButton();
            this.rdoEqStateNormal = new System.Windows.Forms.RadioButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnPlcEqSateChange = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHostRmmNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqpRmmNo)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlProc.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEqStateChangeEvent
            // 
            this.btnEqStateChangeEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnEqStateChangeEvent.Location = new System.Drawing.Point(434, 2);
            this.btnEqStateChangeEvent.Name = "btnEqStateChangeEvent";
            this.btnEqStateChangeEvent.Size = new System.Drawing.Size(110, 33);
            this.btnEqStateChangeEvent.TabIndex = 0;
            this.btnEqStateChangeEvent.Text = "Eq State Change";
            this.btnEqStateChangeEvent.UseVisualStyleBackColor = true;
            this.btnEqStateChangeEvent.Click += new System.EventHandler(this.btnEqStateChangeEvent_Click);
            // 
            // txtProcByWho
            // 
            this.txtProcByWho.Location = new System.Drawing.Point(438, 115);
            this.txtProcByWho.Name = "txtProcByWho";
            this.txtProcByWho.Size = new System.Drawing.Size(88, 23);
            this.txtProcByWho.TabIndex = 14;
            this.txtProcByWho.Text = "1";
            // 
            // txtEquipByWho
            // 
            this.txtEquipByWho.Location = new System.Drawing.Point(438, 74);
            this.txtEquipByWho.Name = "txtEquipByWho";
            this.txtEquipByWho.Size = new System.Drawing.Size(88, 23);
            this.txtEquipByWho.TabIndex = 14;
            this.txtEquipByWho.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(693, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 15;
            this.label1.Text = "BY WHO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(693, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "BY WHO";
            // 
            // btnGlassLoadingEvent
            // 
            this.btnGlassLoadingEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnGlassLoadingEvent.Location = new System.Drawing.Point(3, 8);
            this.btnGlassLoadingEvent.Name = "btnGlassLoadingEvent";
            this.btnGlassLoadingEvent.Size = new System.Drawing.Size(132, 40);
            this.btnGlassLoadingEvent.TabIndex = 0;
            this.btnGlassLoadingEvent.Text = "GLASS LOADING EVENT";
            this.btnGlassLoadingEvent.UseVisualStyleBackColor = true;
            this.btnGlassLoadingEvent.Click += new System.EventHandler(this.btnGlassLoadingEvent_Click);
            // 
            // btnGlassUnLoadingEvent
            // 
            this.btnGlassUnLoadingEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnGlassUnLoadingEvent.Location = new System.Drawing.Point(142, 8);
            this.btnGlassUnLoadingEvent.Name = "btnGlassUnLoadingEvent";
            this.btnGlassUnLoadingEvent.Size = new System.Drawing.Size(132, 40);
            this.btnGlassUnLoadingEvent.TabIndex = 0;
            this.btnGlassUnLoadingEvent.Text = "GLASS UNLOADING EVENT";
            this.btnGlassUnLoadingEvent.UseVisualStyleBackColor = true;
            this.btnGlassUnLoadingEvent.Click += new System.EventHandler(this.btnGlassUnLoadingEvent_Click);
            // 
            // btnGlassUnLoadingCompleteEvent
            // 
            this.btnGlassUnLoadingCompleteEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnGlassUnLoadingCompleteEvent.Location = new System.Drawing.Point(280, 8);
            this.btnGlassUnLoadingCompleteEvent.Name = "btnGlassUnLoadingCompleteEvent";
            this.btnGlassUnLoadingCompleteEvent.Size = new System.Drawing.Size(132, 40);
            this.btnGlassUnLoadingCompleteEvent.TabIndex = 0;
            this.btnGlassUnLoadingCompleteEvent.Text = "GLASS UNLOADING COMPLETE EVENT";
            this.btnGlassUnLoadingCompleteEvent.UseVisualStyleBackColor = true;
            this.btnGlassUnLoadingCompleteEvent.Click += new System.EventHandler(this.btnGlassUnLoadCompleteEvent_Click);
            // 
            // btnGlassUnLoadComplete
            // 
            this.btnGlassUnLoadComplete.Location = new System.Drawing.Point(933, 67);
            this.btnGlassUnLoadComplete.Name = "btnGlassUnLoadComplete";
            this.btnGlassUnLoadComplete.Size = new System.Drawing.Size(132, 40);
            this.btnGlassUnLoadComplete.TabIndex = 0;
            this.btnGlassUnLoadComplete.Text = "GLASS UNLOADING COMPLETE EVENT";
            this.btnGlassUnLoadComplete.UseVisualStyleBackColor = true;
            this.btnGlassUnLoadComplete.Click += new System.EventHandler(this.btnGlassUnLoadCompleteEvent_Click);
            // 
            // btnLdIn
            // 
            this.btnLdIn.Location = new System.Drawing.Point(933, 113);
            this.btnLdIn.Name = "btnLdIn";
            this.btnLdIn.Size = new System.Drawing.Size(132, 40);
            this.btnLdIn.TabIndex = 0;
            this.btnLdIn.Text = "LD IN";
            this.btnLdIn.UseVisualStyleBackColor = true;
            this.btnLdIn.Click += new System.EventHandler(this.btnLdIn_Click);
            // 
            // btnLdOut
            // 
            this.btnLdOut.Location = new System.Drawing.Point(933, 166);
            this.btnLdOut.Name = "btnLdOut";
            this.btnLdOut.Size = new System.Drawing.Size(132, 40);
            this.btnLdOut.TabIndex = 0;
            this.btnLdOut.Text = "LD OUT";
            this.btnLdOut.UseVisualStyleBackColor = true;
            this.btnLdOut.Click += new System.EventHandler(this.btnLdOut_Click);
            // 
            // btnAlarmHappenEvent
            // 
            this.btnAlarmHappenEvent.Location = new System.Drawing.Point(933, 212);
            this.btnAlarmHappenEvent.Name = "btnAlarmHappenEvent";
            this.btnAlarmHappenEvent.Size = new System.Drawing.Size(132, 40);
            this.btnAlarmHappenEvent.TabIndex = 0;
            this.btnAlarmHappenEvent.Text = "Alarm Happen Event";
            this.btnAlarmHappenEvent.UseVisualStyleBackColor = true;
            this.btnAlarmHappenEvent.Click += new System.EventHandler(this.btnAlarmHappenEvent_Click);
            // 
            // btnAlarmClearEvent
            // 
            this.btnAlarmClearEvent.Location = new System.Drawing.Point(933, 258);
            this.btnAlarmClearEvent.Name = "btnAlarmClearEvent";
            this.btnAlarmClearEvent.Size = new System.Drawing.Size(132, 40);
            this.btnAlarmClearEvent.TabIndex = 0;
            this.btnAlarmClearEvent.Text = "Alarm Clear Event";
            this.btnAlarmClearEvent.UseVisualStyleBackColor = true;
            this.btnAlarmClearEvent.Click += new System.EventHandler(this.btnAlarmClearEvent_Click);
            // 
            // btnPpidCRUDEvent
            // 
            this.btnPpidCRUDEvent.Location = new System.Drawing.Point(933, 304);
            this.btnPpidCRUDEvent.Name = "btnPpidCRUDEvent";
            this.btnPpidCRUDEvent.Size = new System.Drawing.Size(132, 40);
            this.btnPpidCRUDEvent.TabIndex = 0;
            this.btnPpidCRUDEvent.Text = "PPID CRUD Event";
            this.btnPpidCRUDEvent.UseVisualStyleBackColor = true;
            this.btnPpidCRUDEvent.Click += new System.EventHandler(this.btnPpidCRUDEvent_Click);
            // 
            // btnCurrPpIdChangeEvent
            // 
            this.btnCurrPpIdChangeEvent.Location = new System.Drawing.Point(933, 350);
            this.btnCurrPpIdChangeEvent.Name = "btnCurrPpIdChangeEvent";
            this.btnCurrPpIdChangeEvent.Size = new System.Drawing.Size(132, 40);
            this.btnCurrPpIdChangeEvent.TabIndex = 0;
            this.btnCurrPpIdChangeEvent.Text = "Curr PPID Change Event";
            this.btnCurrPpIdChangeEvent.UseVisualStyleBackColor = true;
            this.btnCurrPpIdChangeEvent.Click += new System.EventHandler(this.btnCurrPpIdChangeEvent_Click);
            // 
            // btnMessageBox
            // 
            this.btnMessageBox.Location = new System.Drawing.Point(671, 324);
            this.btnMessageBox.Name = "btnMessageBox";
            this.btnMessageBox.Size = new System.Drawing.Size(132, 40);
            this.btnMessageBox.TabIndex = 0;
            this.btnMessageBox.Text = "Message Box";
            this.btnMessageBox.UseVisualStyleBackColor = true;
            this.btnMessageBox.Click += new System.EventHandler(this.btnMessageBox_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 43);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(915, 518);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.nudHostRmmNo);
            this.tabPage1.Controls.Add(this.nudEqpRmmNo);
            this.tabPage1.Controls.Add(this.txtParam03);
            this.tabPage1.Controls.Add(this.txtParam02);
            this.tabPage1.Controls.Add(this.txtPpidName);
            this.tabPage1.Controls.Add(this.txtRecipeName);
            this.tabPage1.Controls.Add(this.txtRecipeNo);
            this.tabPage1.Controls.Add(this.txtParam01);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.btnRmmHostWrite);
            this.tabPage1.Controls.Add(this.btnRmmEqpWrite);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(907, 490);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "TO PLC";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // nudHostRmmNo
            // 
            this.nudHostRmmNo.Location = new System.Drawing.Point(42, 349);
            this.nudHostRmmNo.Name = "nudHostRmmNo";
            this.nudHostRmmNo.Size = new System.Drawing.Size(58, 23);
            this.nudHostRmmNo.TabIndex = 72;
            // 
            // nudEqpRmmNo
            // 
            this.nudEqpRmmNo.Location = new System.Drawing.Point(42, 166);
            this.nudEqpRmmNo.Name = "nudEqpRmmNo";
            this.nudEqpRmmNo.Size = new System.Drawing.Size(58, 23);
            this.nudEqpRmmNo.TabIndex = 72;
            // 
            // txtParam03
            // 
            this.txtParam03.Location = new System.Drawing.Point(106, 130);
            this.txtParam03.Name = "txtParam03";
            this.txtParam03.Size = new System.Drawing.Size(77, 23);
            this.txtParam03.TabIndex = 71;
            this.txtParam03.Text = "3";
            // 
            // txtParam02
            // 
            this.txtParam02.Location = new System.Drawing.Point(106, 101);
            this.txtParam02.Name = "txtParam02";
            this.txtParam02.Size = new System.Drawing.Size(77, 23);
            this.txtParam02.TabIndex = 71;
            this.txtParam02.Text = "2";
            // 
            // txtPpidName
            // 
            this.txtPpidName.Location = new System.Drawing.Point(106, 275);
            this.txtPpidName.Name = "txtPpidName";
            this.txtPpidName.Size = new System.Drawing.Size(77, 23);
            this.txtPpidName.TabIndex = 71;
            this.txtPpidName.Text = "TEST01";
            // 
            // txtRecipeName
            // 
            this.txtRecipeName.Location = new System.Drawing.Point(106, 46);
            this.txtRecipeName.Name = "txtRecipeName";
            this.txtRecipeName.Size = new System.Drawing.Size(77, 23);
            this.txtRecipeName.TabIndex = 71;
            this.txtRecipeName.Text = "TEST01";
            // 
            // txtRecipeNo
            // 
            this.txtRecipeNo.Location = new System.Drawing.Point(106, 303);
            this.txtRecipeNo.Name = "txtRecipeNo";
            this.txtRecipeNo.Size = new System.Drawing.Size(77, 23);
            this.txtRecipeNo.TabIndex = 71;
            this.txtRecipeNo.Text = "1";
            // 
            // txtParam01
            // 
            this.txtParam01.Location = new System.Drawing.Point(106, 74);
            this.txtParam01.Name = "txtParam01";
            this.txtParam01.Size = new System.Drawing.Size(77, 23);
            this.txtParam01.TabIndex = 71;
            this.txtParam01.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(16, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 22);
            this.label5.TabIndex = 70;
            this.label5.Text = "Param 03";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(16, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 22);
            this.label4.TabIndex = 70;
            this.label4.Text = "Param 02";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(16, 237);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 22);
            this.label8.TabIndex = 70;
            this.label8.Text = "HOST RECIPE";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(16, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 22);
            this.label7.TabIndex = 70;
            this.label7.Text = "EQ RECIPE";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoEllipsis = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(16, 275);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 22);
            this.label10.TabIndex = 70;
            this.label10.Text = "PPID Name";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(16, 303);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 22);
            this.label9.TabIndex = 70;
            this.label9.Text = "Recipe Seq";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(16, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 22);
            this.label6.TabIndex = 70;
            this.label6.Text = "Recipe Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(16, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 22);
            this.label3.TabIndex = 70;
            this.label3.Text = "Param 01";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRmmHostWrite
            // 
            this.btnRmmHostWrite.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnRmmHostWrite.Location = new System.Drawing.Point(106, 342);
            this.btnRmmHostWrite.Name = "btnRmmHostWrite";
            this.btnRmmHostWrite.Size = new System.Drawing.Size(110, 33);
            this.btnRmmHostWrite.TabIndex = 1;
            this.btnRmmHostWrite.Text = "RMM WRITE";
            this.btnRmmHostWrite.UseVisualStyleBackColor = true;
            this.btnRmmHostWrite.Click += new System.EventHandler(this.btnRmmHostWrite_Click);
            // 
            // btnRmmEqpWrite
            // 
            this.btnRmmEqpWrite.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnRmmEqpWrite.Location = new System.Drawing.Point(106, 159);
            this.btnRmmEqpWrite.Name = "btnRmmEqpWrite";
            this.btnRmmEqpWrite.Size = new System.Drawing.Size(110, 33);
            this.btnRmmEqpWrite.TabIndex = 1;
            this.btnRmmEqpWrite.Text = "RMM WRITE";
            this.btnRmmEqpWrite.UseVisualStyleBackColor = true;
            this.btnRmmEqpWrite.Click += new System.EventHandler(this.btnRmmEqpWrite_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(400, 105);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 33);
            this.button1.TabIndex = 1;
            this.button1.Text = "Eq State Change";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnRmmEqpWrite_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.pnlProc);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(907, 492);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "TO HOST";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rdoEqp);
            this.panel2.Controls.Add(this.rdoOper);
            this.panel2.Controls.Add(this.rdoHost);
            this.panel2.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(550, 45);
            this.panel2.TabIndex = 9;
            // 
            // rdoEqp
            // 
            this.rdoEqp.AutoSize = true;
            this.rdoEqp.Location = new System.Drawing.Point(130, 14);
            this.rdoEqp.Name = "rdoEqp";
            this.rdoEqp.Size = new System.Drawing.Size(44, 16);
            this.rdoEqp.TabIndex = 1;
            this.rdoEqp.TabStop = true;
            this.rdoEqp.Text = "Eqp";
            this.rdoEqp.UseVisualStyleBackColor = true;
            // 
            // rdoOper
            // 
            this.rdoOper.AutoSize = true;
            this.rdoOper.Location = new System.Drawing.Point(66, 14);
            this.rdoOper.Name = "rdoOper";
            this.rdoOper.Size = new System.Drawing.Size(49, 16);
            this.rdoOper.TabIndex = 1;
            this.rdoOper.TabStop = true;
            this.rdoOper.Text = "Oper";
            this.rdoOper.UseVisualStyleBackColor = true;
            // 
            // rdoHost
            // 
            this.rdoHost.AutoSize = true;
            this.rdoHost.Location = new System.Drawing.Point(3, 13);
            this.rdoHost.Name = "rdoHost";
            this.rdoHost.Size = new System.Drawing.Size(48, 16);
            this.rdoHost.TabIndex = 1;
            this.rdoHost.TabStop = true;
            this.rdoHost.Text = "Host";
            this.rdoHost.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnGlassUnLoadingEvent);
            this.panel3.Controls.Add(this.btnGlassUnLoadingCompleteEvent);
            this.panel3.Controls.Add(this.btnGlassLoadingEvent);
            this.panel3.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.panel3.Location = new System.Drawing.Point(6, 136);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(550, 62);
            this.panel3.TabIndex = 9;
            // 
            // pnlProc
            // 
            this.pnlProc.Controls.Add(this.rdoReady);
            this.pnlProc.Controls.Add(this.rdoSetup);
            this.pnlProc.Controls.Add(this.rdoPause);
            this.pnlProc.Controls.Add(this.rdoExecute);
            this.pnlProc.Controls.Add(this.rdoIdle);
            this.pnlProc.Controls.Add(this.btnEqProcChangeEvent);
            this.pnlProc.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.pnlProc.Location = new System.Drawing.Point(6, 94);
            this.pnlProc.Name = "pnlProc";
            this.pnlProc.Size = new System.Drawing.Size(550, 36);
            this.pnlProc.TabIndex = 9;
            // 
            // rdoReady
            // 
            this.rdoReady.AutoSize = true;
            this.rdoReady.Location = new System.Drawing.Point(283, 11);
            this.rdoReady.Name = "rdoReady";
            this.rdoReady.Size = new System.Drawing.Size(56, 16);
            this.rdoReady.TabIndex = 1;
            this.rdoReady.TabStop = true;
            this.rdoReady.Text = "Ready";
            this.rdoReady.UseVisualStyleBackColor = true;
            // 
            // rdoSetup
            // 
            this.rdoSetup.AutoSize = true;
            this.rdoSetup.Location = new System.Drawing.Point(213, 11);
            this.rdoSetup.Name = "rdoSetup";
            this.rdoSetup.Size = new System.Drawing.Size(55, 16);
            this.rdoSetup.TabIndex = 1;
            this.rdoSetup.TabStop = true;
            this.rdoSetup.Text = "Setup";
            this.rdoSetup.UseVisualStyleBackColor = true;
            // 
            // rdoPause
            // 
            this.rdoPause.AutoSize = true;
            this.rdoPause.Location = new System.Drawing.Point(142, 11);
            this.rdoPause.Name = "rdoPause";
            this.rdoPause.Size = new System.Drawing.Size(56, 16);
            this.rdoPause.TabIndex = 1;
            this.rdoPause.TabStop = true;
            this.rdoPause.Text = "Pause";
            this.rdoPause.UseVisualStyleBackColor = true;
            // 
            // rdoExecute
            // 
            this.rdoExecute.AutoSize = true;
            this.rdoExecute.Location = new System.Drawing.Point(62, 11);
            this.rdoExecute.Name = "rdoExecute";
            this.rdoExecute.Size = new System.Drawing.Size(65, 16);
            this.rdoExecute.TabIndex = 1;
            this.rdoExecute.TabStop = true;
            this.rdoExecute.Text = "Execute";
            this.rdoExecute.UseVisualStyleBackColor = true;
            // 
            // rdoIdle
            // 
            this.rdoIdle.AutoSize = true;
            this.rdoIdle.Location = new System.Drawing.Point(3, 11);
            this.rdoIdle.Name = "rdoIdle";
            this.rdoIdle.Size = new System.Drawing.Size(44, 16);
            this.rdoIdle.TabIndex = 1;
            this.rdoIdle.TabStop = true;
            this.rdoIdle.Text = "Idle";
            this.rdoIdle.UseVisualStyleBackColor = true;
            // 
            // btnEqProcChangeEvent
            // 
            this.btnEqProcChangeEvent.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnEqProcChangeEvent.Location = new System.Drawing.Point(434, 2);
            this.btnEqProcChangeEvent.Name = "btnEqProcChangeEvent";
            this.btnEqProcChangeEvent.Size = new System.Drawing.Size(110, 33);
            this.btnEqProcChangeEvent.TabIndex = 0;
            this.btnEqProcChangeEvent.Text = "Proc State Change";
            this.btnEqProcChangeEvent.UseVisualStyleBackColor = true;
            this.btnEqProcChangeEvent.Click += new System.EventHandler(this.btnEqProcChangeEvent_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdoEqStatePm);
            this.panel1.Controls.Add(this.rdoEqStateFault);
            this.panel1.Controls.Add(this.rdoEqStateNormal);
            this.panel1.Controls.Add(this.btnEqStateChangeEvent);
            this.panel1.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.panel1.Location = new System.Drawing.Point(6, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 36);
            this.panel1.TabIndex = 9;
            // 
            // rdoEqStatePm
            // 
            this.rdoEqStatePm.AutoSize = true;
            this.rdoEqStatePm.Location = new System.Drawing.Point(129, 10);
            this.rdoEqStatePm.Name = "rdoEqStatePm";
            this.rdoEqStatePm.Size = new System.Drawing.Size(41, 16);
            this.rdoEqStatePm.TabIndex = 1;
            this.rdoEqStatePm.TabStop = true;
            this.rdoEqStatePm.Text = "PM";
            this.rdoEqStatePm.UseVisualStyleBackColor = true;
            // 
            // rdoEqStateFault
            // 
            this.rdoEqStateFault.AutoSize = true;
            this.rdoEqStateFault.Location = new System.Drawing.Point(72, 10);
            this.rdoEqStateFault.Name = "rdoEqStateFault";
            this.rdoEqStateFault.Size = new System.Drawing.Size(51, 16);
            this.rdoEqStateFault.TabIndex = 1;
            this.rdoEqStateFault.TabStop = true;
            this.rdoEqStateFault.Text = "Fault";
            this.rdoEqStateFault.UseVisualStyleBackColor = true;
            // 
            // rdoEqStateNormal
            // 
            this.rdoEqStateNormal.AutoSize = true;
            this.rdoEqStateNormal.Location = new System.Drawing.Point(3, 9);
            this.rdoEqStateNormal.Name = "rdoEqStateNormal";
            this.rdoEqStateNormal.Size = new System.Drawing.Size(63, 16);
            this.rdoEqStateNormal.TabIndex = 1;
            this.rdoEqStateNormal.TabStop = true;
            this.rdoEqStateNormal.Text = "Normal";
            this.rdoEqStateNormal.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button15);
            this.tabPage3.Controls.Add(this.button14);
            this.tabPage3.Controls.Add(this.button13);
            this.tabPage3.Controls.Add(this.button12);
            this.tabPage3.Controls.Add(this.button11);
            this.tabPage3.Controls.Add(this.button10);
            this.tabPage3.Controls.Add(this.button9);
            this.tabPage3.Controls.Add(this.button8);
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.btnPlcEqSateChange);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(907, 492);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "GLASS DATA";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button15.Location = new System.Drawing.Point(354, 385);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(110, 33);
            this.button15.TabIndex = 15;
            this.button15.Text = "UnScrap";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button14.Location = new System.Drawing.Point(354, 346);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(110, 33);
            this.button14.TabIndex = 14;
            this.button14.Text = "Scrap";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button13.Location = new System.Drawing.Point(238, 385);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(110, 33);
            this.button13.TabIndex = 13;
            this.button13.Text = "PPID Create";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button12.Location = new System.Drawing.Point(238, 346);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(110, 33);
            this.button12.TabIndex = 12;
            this.button12.Text = "H/S";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button11.Location = new System.Drawing.Point(122, 385);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(110, 33);
            this.button11.TabIndex = 11;
            this.button11.Text = "Loading Stop";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button10.Location = new System.Drawing.Point(122, 346);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(110, 33);
            this.button10.TabIndex = 10;
            this.button10.Text = "Transfer Stop";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button9.Location = new System.Drawing.Point(6, 385);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(110, 33);
            this.button9.TabIndex = 9;
            this.button9.Text = "Imm";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button8.Location = new System.Drawing.Point(6, 346);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(110, 33);
            this.button8.TabIndex = 8;
            this.button8.Text = "Current PPID ";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button7.Location = new System.Drawing.Point(6, 290);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(110, 33);
            this.button7.TabIndex = 7;
            this.button7.Text = "ECID Change";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button6.Location = new System.Drawing.Point(6, 243);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(110, 33);
            this.button6.TabIndex = 6;
            this.button6.Text = "Alarm Clear";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button5.Location = new System.Drawing.Point(6, 198);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(110, 33);
            this.button5.TabIndex = 5;
            this.button5.Text = "Alarm Happen";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(6, 151);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 33);
            this.button4.TabIndex = 4;
            this.button4.Text = "UV01 OUT";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(6, 106);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 33);
            this.button3.TabIndex = 3;
            this.button3.Text = "UV01 IN";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(6, 66);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 33);
            this.button2.TabIndex = 2;
            this.button2.Text = "EQ_Process";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnPlcEqSateChange
            // 
            this.btnPlcEqSateChange.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPlcEqSateChange.Location = new System.Drawing.Point(6, 18);
            this.btnPlcEqSateChange.Name = "btnPlcEqSateChange";
            this.btnPlcEqSateChange.Size = new System.Drawing.Size(110, 33);
            this.btnPlcEqSateChange.TabIndex = 1;
            this.btnPlcEqSateChange.Text = "EQ_STATE";
            this.btnPlcEqSateChange.UseVisualStyleBackColor = true;
            this.btnPlcEqSateChange.Click += new System.EventHandler(this.btnPlcEqSateChange_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(933, 409);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(132, 40);
            this.button16.TabIndex = 17;
            this.button16.Text = "TEST";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // FrmTester
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1101, 587);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnGlassUnLoadComplete);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtEquipByWho);
            this.Controls.Add(this.txtProcByWho);
            this.Controls.Add(this.btnLdOut);
            this.Controls.Add(this.btnMessageBox);
            this.Controls.Add(this.btnCurrPpIdChangeEvent);
            this.Controls.Add(this.btnPpidCRUDEvent);
            this.Controls.Add(this.btnAlarmClearEvent);
            this.Controls.Add(this.btnAlarmHappenEvent);
            this.Controls.Add(this.btnLdIn);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "FrmTester";
            this.Text = "FrmTester";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHostRmmNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqpRmmNo)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.pnlProc.ResumeLayout(false);
            this.pnlProc.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEqStateChangeEvent;
        private System.Windows.Forms.TextBox txtProcByWho;
        private System.Windows.Forms.TextBox txtEquipByWho;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGlassLoadingEvent;
        private System.Windows.Forms.Button btnGlassUnLoadingEvent;
        private System.Windows.Forms.Button btnGlassUnLoadingCompleteEvent;
        private System.Windows.Forms.Button btnGlassUnLoadComplete;
        private System.Windows.Forms.Button btnLdIn;
        private System.Windows.Forms.Button btnLdOut;
        private System.Windows.Forms.Button btnAlarmHappenEvent;
        private System.Windows.Forms.Button btnAlarmClearEvent;
        private System.Windows.Forms.Button btnPpidCRUDEvent;
        private System.Windows.Forms.Button btnCurrPpIdChangeEvent;
        private System.Windows.Forms.Button btnMessageBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdoEqStateNormal;
        private System.Windows.Forms.RadioButton rdoEqStatePm;
        private System.Windows.Forms.RadioButton rdoEqStateFault;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rdoEqp;
        private System.Windows.Forms.RadioButton rdoOper;
        private System.Windows.Forms.RadioButton rdoHost;
        private System.Windows.Forms.Panel pnlProc;
        private System.Windows.Forms.RadioButton rdoPause;
        private System.Windows.Forms.RadioButton rdoExecute;
        private System.Windows.Forms.RadioButton rdoIdle;
        private System.Windows.Forms.Button btnEqProcChangeEvent;
        private System.Windows.Forms.RadioButton rdoSetup;
        private System.Windows.Forms.RadioButton rdoReady;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnRmmEqpWrite;
        private System.Windows.Forms.Button btnPlcEqSateChange;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtParam03;
        private System.Windows.Forms.TextBox txtParam02;
        private System.Windows.Forms.TextBox txtParam01;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudEqpRmmNo;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRecipeName;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPpidName;
        private System.Windows.Forms.TextBox txtRecipeNo;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudHostRmmNo;
        private System.Windows.Forms.Button btnRmmHostWrite;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button16;

    }
}
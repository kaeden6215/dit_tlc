﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DitInlineCim.Common;
using DitInlineCim.Struct;
using DitInlineCim.Lib;
using DitInlineCim.PLC;
using DitInlineCim.HSMS;
using DitInlineCim.Log;
using System.Threading;
using DitInlineCim.Struct.SV;
using System.Windows.Forms;
using DitInlineCim.Struct.DV;
using System.IO;
using Dit.Framework.PLC;
//using DitInlineCim.PLC.Command;
//using DitInlineCim.PLC.Event;

namespace DitInlineCim
{
    public class ScenarioWorker
    {
        private bool _running = false;
        private DateTime _lastSvUpdateTime = DateTime.Now;
        private DateTime _lastReadTime = DateTime.Now;
        private DateTime _befPlcAliveTime = DateTime.Now;
        private BackgroundWorker bgwokerMemory = new BackgroundWorker();

        public double ScanTime { get; set; }

        private List<PlcEvent> _lstPlcEvent = new List<PlcEvent>();
        private List<PlcCommand> _lstPlcCommand = new List<PlcCommand>();


        //이벤트 클래스. 
        public PlcEvent OnlineModeChange; //Map 항목 없음..

        public PlcEvent EquipStateChange;

        public PlcEvent ProcStateChange;

        public PlcEvent CellIn;
        public PlcEvent CellOut;


        public PlcEvent AlarmHappen;
        public PlcEvent AlarmClear;
        public PlcEvent Specific; //작성안함
        public PlcEvent EcidChange;
        public PlcEvent CurrentPPIDChange;
        public PlcEvent RecipeCrDeMo;
        public PlcEvent EqPioNetworkError;
        public PlcEvent CellScrap;
        public PlcEvent CellUnscrap;

        public PlcEvent CellJobProcessStart;
        public PlcEvent CellJobProcessCancel;
        public PlcEvent CellJobProcessAbort;
        public PlcEvent CellJobProcessEnd;
        public PlcEvent CellCassetteOut;
        public PlcEvent CellCassetteIn;
        public PlcEvent CellIdRead;
        public PlcEvent CellProcessStartForModule;
        public PlcEvent CellProcessEndForModule;
        public PlcEvent CellLoadRequest;
        public PlcEvent CellPreLoadComplete;
        public PlcEvent CellLoadComplete;
        public PlcEvent CellUnloadRequest;
        public PlcEvent CellLotUnloadComplete;
        public PlcEvent CellIdReadFail;
        public PlcEvent CellReadingComplete;


        public PlcCommand EqpProcPauseCmd;
        public PlcCommand EqpProcResumeCmd;
        public PlcCommand EqpStatePmChangeCmd;
        public PlcCommand EqpStateNormalChangeCmd;

        public PlcCommand EqpStateCycleStopRequset;
        public PlcCommand DateTimeSetCmd;
        public PlcCommand OpCallCmd;
        public PlcCommand TerminalMessageCmd;
        public PlcCommand EinModeChangeCmd;

        public bool[] _beforAlarmBits = new bool[EQPB.AlarmBitWordLength * 16];
        public bool[] _currAlarmBits = new bool[EQPB.AlarmBitWordLength * 16];
        
        //public PlcCommand AlarmResetCmd;
        //public PlcCommand RecipeCrDeMoCmd;

        public ScenarioWorker()
        {
            EquipStateChange                            /* */= new PlcEvent() { Name = "Equip State Change",                /* */ OnAddr = EQPB.StateChangEvent,                /* */  AckAddr = CIMB.StateChangEventAck,               /* */ OnEventRun = OnEquipStateChangeEvent };         /**/_lstPlcEvent.Add(EquipStateChange);
            ProcStateChange                             /* */= new PlcEvent() { Name = "Proc State Change",                 /* */ OnAddr = EQPB.ProcessStateChangEvent,         /* */  AckAddr = CIMB.ProcessStateChangEventAck,        /* */ OnEventRun = OnProcChangeEvent };               /**/_lstPlcEvent.Add(ProcStateChange);

            CellIn                                      /* */= new PlcEvent() { Name = "Cell Panel In",                     /* */ OnAddr = EQPB.CellInEvent,                    /* */  AckAddr = CIMB.CellInEventAck,                   /* */ OnEventRun = OnCellInEvent };                   /**/_lstPlcEvent.Add(CellIn);
            CellOut                                     /* */= new PlcEvent() { Name = "Cell Panel Out",                    /* */ OnAddr = EQPB.CellOutEvent,                   /* */  AckAddr = CIMB.CellOutEventAck,                  /* */ OnEventRun = OnCellOutEvent };                  /**/_lstPlcEvent.Add(CellIn);

            AlarmHappen                                 /* */= new PlcEvent() { Name = "Alarm Happen",                      /* */ OnAddr = EQPB.AlarmHappenEvent,               /* */  AckAddr = CIMB.AlarmHappenEventAck,              /* */ OnEventRun = OnEquipAlarmHappenClearEvent };    /**/_lstPlcEvent.Add(AlarmHappen);
            AlarmClear                                  /* */= new PlcEvent() { Name = "Alarm Clear",                       /* */ OnAddr = EQPB.AlarmClearEvent,                /* */  AckAddr = CIMB.AlarmClearEventAck,               /* */ OnEventRun = OnEquipAlarmHappenClearEvent };    /**/_lstPlcEvent.Add(AlarmClear);
            
            EcidChange                                  /* */= new PlcEvent() { Name = "ECID Change",                       /* */ OnAddr = EQPB.EqEcidChangeEvent,              /* */  AckAddr = CIMB.EqEcidChangeEventAck,              /* */ OnEventRun = OnEcidChangeEvent };              /**/_lstPlcEvent.Add(EcidChange);

            RecipeCrDeMo                                /* */= new PlcEvent() { Name = "Recipe Create Delete Modify",       /* */ OnAddr = EQPB.PpidCreateDeleteModifyEvent,    /* */  AckAddr = CIMB.PpidCreateDeleteModifyEventAck,   /* */ OnEventRun = OnRecipeCreateDeleteModifyEvent }; /**/_lstPlcEvent.Add(RecipeCrDeMo);
            CurrentPPIDChange                           /* */= new PlcEvent() { Name = "Current PPID Change",               /* */ OnAddr = EQPB.CurrentPpidChangeEvent,         /* */  AckAddr = CIMB.CurrentPpidChangeEventAck,        /* */ OnEventRun = OnCurrentPPIDChangeEvent };        /**/_lstPlcEvent.Add(CurrentPPIDChange);
            EqPioNetworkError                           /* */= new PlcEvent() { Name = "Eq PIO Network Error",              /* */ OnAddr = EQPB.EqPioNetworkErrorEvent,         /* */  AckAddr = CIMB.EqPioNetworkErrorEventAck,        /* */ Tag = 704, OnEventRun = OnS6F11EventReport };   /**/_lstPlcEvent.Add(EqPioNetworkError);


            CellJobProcessStart         /* */= new PlcEvent() { Name = "Cell Job Process Start",         /* */ OnAddr = EQPB.CellJobProcessStart,       /* */  AckAddr = CIMB.CellJobProcessStartAck,       /* */ OnEventRun = OnCellJobProcessStart };             /**/_lstPlcEvent.Add(CellJobProcessStart);
            CellJobProcessCancel        /* */= new PlcEvent() { Name = "Cell Job Process Cancel",        /* */ OnAddr = EQPB.CellJobProcessCancel,      /* */  AckAddr = CIMB.CellJobProcessCancelAck,      /* */ OnEventRun = OnCellJobProcessCancel };            /**/_lstPlcEvent.Add(CellJobProcessCancel);
            CellJobProcessAbort         /* */= new PlcEvent() { Name = "Cell Job Process Abort",         /* */ OnAddr = EQPB.CellJobProcessAbort,       /* */  AckAddr = CIMB.CellJobProcessAbortAck,       /* */ OnEventRun = OnCellJobProcessAbort };             /**/_lstPlcEvent.Add(CellJobProcessAbort);
            CellJobProcessEnd           /* */= new PlcEvent() { Name = "Cell Job Process End",           /* */ OnAddr = EQPB.CellJobProcessEnd,         /* */  AckAddr = CIMB.CellJobProcessEndAck,         /* */ OnEventRun = OnCellJobProcessEnd };               /**/_lstPlcEvent.Add(CellJobProcessEnd);
            CellCassetteOut             /* */= new PlcEvent() { Name = "Cell Cassette Out",              /* */ OnAddr = EQPB.CellCassetteOut,           /* */  AckAddr = CIMB.CellCassetteOutAck,           /* */ OnEventRun = OnCellCassetteOut };                 /**/_lstPlcEvent.Add(CellCassetteOut);
            CellCassetteIn              /* */= new PlcEvent() { Name = "Cell Cassette In",               /* */ OnAddr = EQPB.CellCassetteIn,            /* */  AckAddr = CIMB.CellCassetteInAck,            /* */ OnEventRun = OnCellCassetteIn };                  /**/_lstPlcEvent.Add(CellCassetteIn);
            CellIdRead                  /* */= new PlcEvent() { Name = "Cell Id Read",                   /* */ OnAddr = EQPB.CellIdRead,                /* */  AckAddr = CIMB.CellIdReadAck,                /* */ OnEventRun = OnCellIdRead };                      /**/_lstPlcEvent.Add(CellIdRead);
            CellProcessStartForModule   /* */= new PlcEvent() { Name = "Cell Process Start For Module",  /* */ OnAddr = EQPB.CellProcessStartForModule, /* */  AckAddr = CIMB.CellProcessStartForModuleAck, /* */ OnEventRun = OnCellProcessStartForModule };       /**/_lstPlcEvent.Add(CellProcessStartForModule);
            CellProcessEndForModule     /* */= new PlcEvent() { Name = "Cell Process End For Module",    /* */ OnAddr = EQPB.CellProcessEndForModule,   /* */  AckAddr = CIMB.CellProcessEndForModuleAck,   /* */ OnEventRun = OnCellProcessEndForModule };         /**/_lstPlcEvent.Add(CellProcessEndForModule);
            CellLoadRequest             /* */= new PlcEvent() { Name = "Cell Load Request",              /* */ OnAddr = EQPB.CellLoadRequest,           /* */  AckAddr = CIMB.CellLoadRequestAck,           /* */ OnEventRun = OnCellLoadRequest };                 /**/_lstPlcEvent.Add(CellLoadRequest);
            CellPreLoadComplete         /* */= new PlcEvent() { Name = "Cell Pre Load Complete",         /* */ OnAddr = EQPB.CellPreLoadComplete,       /* */  AckAddr = CIMB.CellPreLoadCompleteAck,       /* */ OnEventRun = OnCellPreLoadComplete };             /**/_lstPlcEvent.Add(CellPreLoadComplete);
            CellLoadComplete            /* */= new PlcEvent() { Name = "Cell Load Complete",             /* */ OnAddr = EQPB.CellLoadComplete,          /* */  AckAddr = CIMB.CellLoadCompleteAck,          /* */ OnEventRun = OnCellLoadComplete };                /**/_lstPlcEvent.Add(CellLoadComplete);
            CellUnloadRequest           /* */= new PlcEvent() { Name = "Cell Unload Request",            /* */ OnAddr = EQPB.CellUnloadRequest,         /* */  AckAddr = CIMB.CellUnloadRequestAck,         /* */ OnEventRun = OnCellUnloadRequest };               /**/_lstPlcEvent.Add(CellUnloadRequest);
            CellLotUnloadComplete       /* */= new PlcEvent() { Name = "Cell Lot Unload Complete",       /* */ OnAddr = EQPB.CellLotUnloadComplete,     /* */  AckAddr = CIMB.CellLotUnloadCompleteAck,     /* */ OnEventRun = OnCellLotUnloadComplete };           /**/_lstPlcEvent.Add(CellLotUnloadComplete);
            CellIdReadFail              /* */= new PlcEvent() { Name = "Cell Id Read Fail",              /* */ OnAddr = EQPB.CellIdReadFail,            /* */  AckAddr = CIMB.CellIdReadFailAck,            /* */ OnEventRun = OnCellIdReadFail };                  /**/_lstPlcEvent.Add(CellIdReadFail);
            CellReadingComplete         /* */= new PlcEvent() { Name = "Cell Reading Complete",          /* */ OnAddr = EQPB.CellReadingComplete,       /* */  AckAddr = CIMB.CellReadingCompleteAck,       /* */ OnEventRun = OnCellReadingComplete };             /**/_lstPlcEvent.Add(CellReadingComplete);
            CellScrap                   /* */= new PlcEvent() { Name = "Cell Scrap",                     /* */ OnAddr = EQPB.CellScrapEvent,            /* */  AckAddr = CIMB.CellScrapEventAck,            /* */ OnEventRun = OnCellScrap };                       /**/_lstPlcEvent.Add(CellScrap);
            CellUnscrap                 /* */= new PlcEvent() { Name = "Cell UnScrap",                   /* */ OnAddr = EQPB.CellUnScrapEvent,          /* */  AckAddr = CIMB.CellUnScrapEventAck,          /* */ OnEventRun = OnCellUnscrap };                     /**/_lstPlcEvent.Add(CellUnscrap);



            EqpProcPauseCmd                             /* */= new PlcCommand() { Name = "Equip Proc Pause(51)",             /* */OnAddr = CIMB.EqpProcPauseCmd,                /* */  AckAddr = EQPB.EqpProcPauseCmdAck,               /* */ OnCommand = EquipProcPauseCmd };              /**/_lstPlcCommand.Add(EqpProcPauseCmd);
            EqpProcResumeCmd                            /* */= new PlcCommand() { Name = "Equip Proc Resume(52)",            /* */OnAddr = CIMB.EqpProcResumeCmd,               /* */  AckAddr = EQPB.EqpProcResumeCmdAck,              /* */ OnCommand = EquipProcResumeCmd };             /**/_lstPlcCommand.Add(EqpProcResumeCmd);
            EqpStatePmChangeCmd                         /* */= new PlcCommand() { Name = "Equip State Pm Change(53)",        /* */OnAddr = CIMB.EqpStatePmChangeCmd,            /* */  AckAddr = EQPB.EqpStatePmChangeCmdAck,           /* */ OnCommand = EquipStatePMCmd };                /**/_lstPlcCommand.Add(EqpStatePmChangeCmd);
            EqpStateNormalChangeCmd                     /* */= new PlcCommand() { Name = "Equip State Normal Change(54)",    /* */OnAddr = CIMB.EqpStateNormalChangeCmd,        /* */  AckAddr = EQPB.EqpStateNormalChangeCmdAck,       /* */ OnCommand = EquipStateNormalCmd };            /**/_lstPlcCommand.Add(EqpStateNormalChangeCmd);
            EqpStateCycleStopRequset                    /* */= new PlcCommand() { Name = "Equip Cycle Stop Request(57)",     /* */OnAddr = CIMB.EqpCycleStopCmd,                /* */  AckAddr = EQPB.EqpCycleStopCmdAck,               /* */ OnCommand = EquipCycleStopCmd };              /**/_lstPlcCommand.Add(EqpStateCycleStopRequset);
            DateTimeSetCmd                              /* */= new PlcCommand() { Name = "DataTime Set",                     /* */OnAddr = CIMB.TimeSetCmd,                     /* */  AckAddr = EQPB.TimeSetCmdAck,                    /* */ OnCommand = EquipTimeSetCmd };                /**/_lstPlcCommand.Add(DateTimeSetCmd);
            OpCallCmd                                   /* */= new PlcCommand() { Name = "Opcall Cmd",                       /* */OnAddr = CIMB.OpcallSetCmd,                   /* */  AckAddr = EQPB.OpcallSetCmdAck,                  /* */ OnCommand = EquipOpCallCmd };                 /**/_lstPlcCommand.Add(OpCallCmd);
            TerminalMessageCmd                          /* */= new PlcCommand() { Name = "Terminal Message Cmd",             /* */OnAddr = CIMB.TerminalMessageSetCmd,          /* */  AckAddr = EQPB.TerminalMessageSetCmdAck,         /* */ OnCommand = EquipTerminnalMessageSetCmd };    /**/_lstPlcCommand.Add(TerminalMessageCmd);

            GG.PLC = new VirtualShare("DIT.TLC.CIM", 1024000);
            bgwokerMemory.DoWork += new DoWorkEventHandler(bgwokerMemory_DoWork);
            bgwokerMemory.ProgressChanged += new ProgressChangedEventHandler(bgwokerMemory_ProgressChanged);
        }
        
        public void Start()
        {
            if (GG.PLC.Open() != 0)
            {
                //PLC 연결 오류
                GG.Equip.EquipPlcConnected = false;
                Logger.Append("PLC Connected Fail");
            }
            else
            {
                Logger.Append("PLC Connected OK");
                if (GG.PLC.ReadFromPLC(EQPB.CIM_BIT_START_ADDR, EQPB.CIM_BIT_START_ADDR.Length) != 0) { GG.Equip.EquipPlcConnected = false; }
                if (GG.PLC.ReadFromPLC(EQPW.CIM_WORD_START_ADDR, EQPW.CIM_WORD_START_ADDR.Length) != 0) { GG.Equip.EquipPlcConnected = false; }

                GG.Equip.RmmMgr.LoadFromPlc();
                Logger.Append("RMM MGR LOAD OK");

                GG.Equip.EcIdMgr.LoadFromPlc();
                Logger.Append("RMM MGR LOAD OK");

                GG.Equip.EoidMgr.LoadFromPlc();

                GG.Equip.EquipPlcConnected = true;

                bgwokerMemory.WorkerSupportsCancellation = true;
                bgwokerMemory.WorkerReportsProgress = true;
                if (bgwokerMemory.IsBusy == false)
                    bgwokerMemory.RunWorkerAsync();
            }
        }
        public void Stop()
        {
            if (bgwokerMemory.IsBusy)
                bgwokerMemory.CancelAsync();

            GG.PLC.Close();
        }

        private void bgwokerMemory_DoWork(object sender, DoWorkEventArgs e)
        {
            int iPos = 0;
            while (bgwokerMemory.CancellationPending == false)
            {
                if (GG.PLC.ReadFromPLC(EQPB.CIM_BIT_START_ADDR, EQPB.CIM_BIT_START_ADDR.Length) != 0)
                {
                    GG.Equip.EquipPlcConnected = false;
                }
                else
                {
                    GG.Equip.EquipPlcConnected = true;
                }

                bgwokerMemory.ReportProgress(iPos++);
            }
        }
        private void bgwokerMemory_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (_running == false)
            {

                _running = true;
                try
                {
                    ScanTime = (DateTime.Now - _lastReadTime).TotalMilliseconds;
                    _lastReadTime = DateTime.Now;

                    InitailzieLoadPlc();
                    //AOI EVENT
                    EquipAlivePlc();
                    EquipStateValue();

                    foreach (PlcEvent ev in _lstPlcEvent)
                        ev.Process();

                    foreach (PlcCommand cmd in _lstPlcCommand)
                        cmd.Process();



                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                _running = false;
            }
        }
        private bool _isInitailized = false;
        private void InitailzieLoadPlc()
        {
            if (_isInitailized == false)
            {
                Logger.Append("Initailzie Start");

                if (_beforAlarmBits.Length == 0)
                    _beforAlarmBits = new bool[EQPB.AlarmBitWordLength];

                Logger.Append("Initailzie State End");

                OnEquipAlarmHappenClearEvent("", null);
                GG.Equip.UiMain.UIUpdate();

                _isInitailized = true;
            }
        }
        //EQUIP ↔ CIM EVENT
        private void EquipAlivePlc()
        {
            if (GG.PLC[CIMB.PcAlive] == true)
                GG.PLC.ClearBit(CIMB.PcAlive);
            else
                GG.PLC.SetBit(CIMB.PcAlive);
        }
        //SV DATA 로딩
        private void EquipStateValue()
        {
            if ((DateTime.Now - _lastSvUpdateTime).TotalMilliseconds > 800)
            {
                _lastSvUpdateTime = DateTime.Now;
                if (GG.PLC.ReadFromPLC(EQPW.STATE_VALUE, EQPW.STATE_VALUE.Length) != 0) { GG.Equip.EquipPlcConnected = false; }
                else { GG.Equip.EquipPlcConnected = true; }

                foreach (SvItem item in GG.Equip.SvMgr.Values)
                {
                    if (item.SvBit == "INT16")
                        item.SvValue = Math.Round(GG.PLC.VirGetShort(item.PlcAddr) / Math.Pow(10, item.SvUnit), 3).ToString();
                    else
                        item.SvValue = Math.Round(GG.PLC.VirGetInt32(item.PlcAddr) / Math.Pow(10, item.SvUnit), 3).ToString();
                }
            }
        }

        #region

        public void OnEquipStateChangeEvent(string name, object tag)
        {
            int iPos = (int)tag;
            
            EMEquipState equipState = CimConvertCode.GetEquipState(GG.PLC.VirGetShort(EQPW.EquipProcState));
            EMByWho equipStateWho = CimConvertCode.GetByWho(GG.PLC.VirGetShort(EQPW.EquipProcState));

            string resonCode = (equipState == EMEquipState.PM) ? GG.PLC.VirGetAsciiTrim(EQPW.PM_CODE) : string.Empty;
            
            GG.Equip.UiMain.UIEquipState();
            GG.Equip.UiMain.UIEqipModuleState();
        }
        public void OnProcChangeEvent(string name, object tag)
        {
            int iPos = (int)tag;
            
            EMProcState equipProc = CimConvertCode.GetProcState(GG.PLC.VirGetShort(EQPW.EquipProcState));
            EMByWho equipProcWho = CimConvertCode.GetByWho(GG.PLC.VirGetShort(EQPW.BywhoEqState));

            string reasonCode = (equipProc == EMProcState.Pause) ? GG.PLC.VirGetAsciiTrim(EQPW.PAUSE_CODE) : string.Empty;
            
            GG.Equip.UiMain.UIProcessState();
            GG.Equip.UiMain.UIEqipModuleState();
        }
        
        public void OnCellInEvent(string name, object tag)
        {
            CellInfo Cell = GG.Equip.GetCellInfoFromPLC(EQPW.CellInCellData);
            Logger.Append("Cell ID = {0}, {1}, {2} In", Cell.HGlassID, name, Cell.PPID);
            GG.Equip.PanelLoading(Cell);
        }
        public void OnCellOutEvent(string name, object tag)
        {
            CellInfo cell = GG.Equip.GetCellInfoFromPLC(EQPW.CellOutCellData);
            Logger.Append("Cell ID = {0}, {1}, {2} Out", cell.HGlassID, name, cell.PPID);
            GG.Equip.PanelUnloading(cell);
        }
        
        private void OnEquipAlarmHappenClearEvent(string name, object tag)
        {
            if (GG.PLC.ReadFromPLC(EQPB.AlarmBitArea, EQPB.AlarmBitArea.Length) != 0) { }

            _currAlarmBits = GG.PLC.VirGetBits(EQPB.AlarmBitArea, EQPB.AlarmBitArea.Length);
            for (int iPos = 0; iPos < (EQPB.AlarmBitArea.Length * 16); iPos++)
            {
                if (iPos >= _beforAlarmBits.Length) continue;
                if (_beforAlarmBits[iPos] != _currAlarmBits[iPos])
                {
                    if (_currAlarmBits[iPos] == true)
                    {
                        // 발생
                        GG.Equip.SetAlarm(1, iPos);
                        Logger.Append("[AOI → CIM] Alarm {0} Set", iPos);
                    }
                    else
                    {   // 해지                        
                        GG.Equip.SetAlarm(0, iPos);
                        Logger.Append("[AOI → CIM] Alarm {0} Reset", iPos);
                    }
                }
            }

            _beforAlarmBits = _currAlarmBits;
        }

        public void OnEcidChangeEvent(string name, object tag)
        {
            GG.Equip.SetEcidChange(EMByWho.ByEqp);
        }

        public void OnRecipeCreateDeleteModifyEvent(string name, object tag)
        {
            GG.Equip.RecipeCreateDeleteModifyEvent();
        }
        public void OnCurrentPPIDChangeEvent(string name, object tag)
        {
            string oldPPid = GG.PLC.VirGetAsciiTrim(EQPW.CurrentOldPPID);
            string newPPid = GG.PLC.VirGetAsciiTrim(EQPW.CurrentNewPPID);

            GG.Equip.SetCurrentPPIDChange(oldPPid, newPPid);
        }
        public void OnS6F11EventReport(string name, object tag)
        {
            int CEID = (int)tag;
            GG.Equip.SetEquipStateReport(CEID);
        }

        public void OnCellJobProcessStart(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellJobProcessStartPort);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellJobProcessStartPort);

            GG.Equip.CellJobProcessStartReport(port, lot);
        }
        //오정석
        public void OnCellJobProcessCancel(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellJobProcessCancel);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellJobProcessCancel);

            GG.Equip.CellJobProcessCancelReport(port, lot);
        }
        //오정석
        public void OnCellJobProcessAbort(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellJobProcessAbort);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellJobProcessAbort);

            GG.Equip.CellJobProcessAbortReport(port, lot);
        }
        //오정석
        public void OnCellJobProcessEnd(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellJobProcessEnd);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellJobProcessEnd);

            GG.Equip.CellJobProcessEndReport(port, lot);
        }
        //오정석
        public void OnCellCassetteOut(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellCassetteOut);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellCassetteOut);

            GG.Equip.CellCassetteOutReport(port, lot);
        }
        //오정석
        public void OnCellCassetteIn(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellCassetteIn);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellCassetteIn);

            GG.Equip.CellCassetteInReport(port, lot);
        }
        //오정석
        public void OnCellIdRead(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellIdRead);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellIdRead);

            GG.Equip.CellIdReadReport(port, lot);
        }
        //오정석
        public void OnCellProcessStartForModule(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellProcessStartForModule);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellProcessStartForModule);

            GG.Equip.CellProcessStartForModuleReport(port, lot);
        }
        //오정석
        public void OnCellProcessEndForModule(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellProcessEndForModule);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellProcessEndForModule);

            GG.Equip.CellProcessEndForModuleReport(port, lot);
        }
        //오정석
        public void OnCellScrap(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellScrap);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellScrap);

            GG.Equip.CellScrapReport(port, lot);
        }
        //오정석
        public void OnCellUnscrap(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellUnscrap);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellUnscrap);

            GG.Equip.CellUnscrapReport(port, lot);
        }
        //오정석
        public void OnCellLoadRequest(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellLoadRequest);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellLoadRequest);

            GG.Equip.CellLoadRequestReport(port, lot);
        }
        //오정석
        public void OnCellPreLoadComplete(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellPreLoadComplete);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellPreLoadComplete);

            GG.Equip.CellPreLoadCompleteReport(port, lot);
        }
        //오정석
        public void OnCellLoadComplete(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellLoadComplete);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellLoadComplete);

            GG.Equip.CellLoadCompleteReport(port, lot);
        }
        //오정석
        public void OnCellUnloadRequest(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellUnloadRequest);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellUnloadRequest);

            GG.Equip.CellUnloadRequestReport(port, lot);
        }
        //오정석
        public void OnCellLotUnloadComplete(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellLotUnloadComplete);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellLotUnloadComplete);

            GG.Equip.CellLotUnloadCompleteReport(port, lot);
        }
        //오정석
        public void OnCellIdReadFail(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellIdReadFail);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellIdReadFail);

            GG.Equip.CellIdReadFailReport(port, lot);
        }
        //오정석
        public void OnCellReadingComplete(string name, object tag)
        {
            PortInfo port = GG.Equip.GetPortInfoFromPLC(EQPW.CellReadingComplete);
            LotInfo lot = GG.Equip.GetLotInfoFromPLC(EQPW.CellReadingComplete);

            GG.Equip.CellIdReadingCompleteReport(port, lot);
        }


        //메소드 EQP ↔ CIM COMMAND  
        private void EquipProcPauseCmd()
        {
            short procPause = (short)GG.Equip.CmdEquipProcessPauseByWho;
            GG.PLC.SetAscii(CIMW.ReasonCode, GG.Equip.CmdEquipReasonCode);
        }
        private void EquipProcResumeCmd()
        {
            short procResume = (short)GG.Equip.CmdEquipProcessResumeByWho;
            GG.PLC.SetAscii(CIMW.ReasonCode, GG.Equip.CmdEquipReasonCode);
        }
        private void EquipStatePMCmd()
        {
            short eqpstaepm = (short)GG.Equip.CmdEquipStatePMByWho;
            GG.PLC.SetAscii(CIMW.ReasonCode, GG.Equip.CmdEquipReasonCode);

        }
        private void EquipStateNormalCmd()
        {
            short eqpstaeNormal = (short)GG.Equip.CmdEquipStateNormalByWho;
            GG.PLC.SetAscii(CIMW.ReasonCode, GG.Equip.CmdEquipReasonCode);
        }
        private void EquipCycleStopCmd()
        {
            short cycleByWho = (short)GG.Equip.CmdEquipCycleStopByWho;
            GG.PLC.SetAscii(CIMW.ReasonCode, GG.Equip.CmdEquipReasonCode);
        }
        private void EquipTimeSetCmd()
        {
            GG.PLC.SetShort(CIMW.TimeSet + 0, GG.Equip.Year);
            GG.PLC.SetShort(CIMW.TimeSet + 1, GG.Equip.Month);
            GG.PLC.SetShort(CIMW.TimeSet + 2, GG.Equip.Day);
            GG.PLC.SetShort(CIMW.TimeSet + 3, GG.Equip.Hour);
            GG.PLC.SetShort(CIMW.TimeSet + 4, GG.Equip.Minute);
            GG.PLC.SetShort(CIMW.TimeSet + 5, GG.Equip.Second);
        }
        private void EquipOpCallCmd()
        {
            for (int iPos = 0; iPos < GG.Equip.OperacalMsgToEqp.Length; iPos++)
            {
                GG.PLC.SetAscii(CIMW.OperCallMsg + (50 * iPos), GG.Equip.OperacalMsgToEqp[iPos]);
            }
        }
        private void EquipTerminnalMessageSetCmd()
        {
            GG.PLC.SetAscii(CIMW.TerminalCallMsg, GG.Equip.TerminalMessageToEqp);
        }

#endregion
    }
}

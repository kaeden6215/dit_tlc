﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DitInlineCim.PLC;
using DitInlineCim.Common;
using DitInlineCim.Struct;
using Dit.Framework.PLC;

namespace DitInlineCim
{
    public partial class FrmTester : Form
    {
        public VirtualShare PLC = null;
        public FrmTester()
        {
            InitializeComponent();
        }

        private void btnEqStateChangeEvent_Click(object sender, EventArgs e)
        {
            EMEquipState aoiEquipState = rdoEqStateNormal.Checked ? EMEquipState.Normal :
                                       rdoEqStateFault.Checked ? EMEquipState.Fault :
                                       rdoEqStatePm.Checked ? EMEquipState.PM : EMEquipState.Unknown;

            EMByWho aoiEquipStateWho = rdoHost.Checked ? EMByWho.ByHost :
                                       rdoOper.Checked ? EMByWho.ByOper :
                                       rdoEqp.Checked ? EMByWho.ByEqp : EMByWho.Unknown;


            GG.Equip.SetEqState(aoiEquipState, string.Empty, aoiEquipStateWho);
        }
        private void btnEqProcChangeEvent_Click(object sender, EventArgs e)
        {
            //Process State Change 
            EMProcState equipProc = rdoIdle.Checked ? EMProcState.Idle :
                                   rdoSetup.Checked ? EMProcState.Setup :
                                   rdoReady.Checked ? EMProcState.Ready :
                                   rdoExecute.Checked ? EMProcState.Execute :
                                   rdoPause.Checked ? EMProcState.Pause : EMProcState.Unknown;


            EMByWho aoiEquipProcWho = rdoHost.Checked ? EMByWho.ByHost :
                                      rdoOper.Checked ? EMByWho.ByOper :
                                      rdoEqp.Checked ? EMByWho.ByEqp : EMByWho.Unknown;

            if (equipProc != EMProcState.Unknown)
                GG.Equip.SetProcState(equipProc, string.Empty, aoiEquipProcWho);
        }

        private void btnGlassLoadingEvent_Click(object sender, EventArgs e)
        {
            CellInfo glass = new CellInfo();
            GG.Equip.PanelLoading(glass);
        }

        private void btnGlassUnLoadingEvent_Click(object sender, EventArgs e)
        {
            CellInfo glass = new CellInfo();
            GG.Equip.PanelUnloading(glass);
        }

        private void btnGlassUnLoadCompleteEvent_Click(object sender, EventArgs e)
        {
            CellInfo glass = new CellInfo();
            GG.Equip.PanelUnloadComplete(glass);
        }

        private void btnLdIn_Click(object sender, EventArgs e)
        {
            //PLC.Toggle(EQPB.ModLoaderPanelInEnvent);
            //btnLdIn.BackColor = PLC.GetBit(EQPB.ModLoaderPanelInEnvent) == true ? Color.Red : this.BackColor;
        }

        private void btnLdOut_Click(object sender, EventArgs e)
        {
            //PLC.Toggle(EQPB.ModLoaderPanelOutEnvent);
            //btnLdOut.BackColor = PLC.GetBit(EQPB.ModLoaderPanelOutEnvent) == true ? Color.Red : this.BackColor;
        }

        private void btnAlarmHappenEvent_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.AlarmHappenEvent);
            btnAlarmHappenEvent.BackColor = PLC.GetBit(EQPB.AlarmHappenEvent) == true ? Color.Red : this.BackColor;
        }

        private void btnAlarmClearEvent_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.AlarmClearEvent);
            btnAlarmClearEvent.BackColor = PLC.GetBit(EQPB.AlarmClearEvent) == true ? Color.Red : this.BackColor;
        }

        private void btnPpidCRUDEvent_Click(object sender, EventArgs e)
        {
            //PLC.Toggle(EQPB.RecipeCreateDeleteModifyEvent);
            //btnPpidCRUDEvent.BackColor = PLC.GetBit(EQPB.RecipeCreateDeleteModifyEvent) == true ? Color.Red : this.BackColor;
        }

        private void btnCurrPpIdChangeEvent_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.CurrentPpidChangeEvent);
            btnCurrPpIdChangeEvent.BackColor = PLC.GetBit(EQPB.CurrentPpidChangeEvent) == true ? Color.Red : this.BackColor;
        }

        private void btnMessageBox_Click(object sender, EventArgs e)
        {
            GG.Equip.Year = 2015;
            GG.Equip.Month = 10;
            GG.Equip.Day = 11;
            GG.Equip.Hour = 12;
            GG.Equip.Minute = 20;
            GG.Equip.Second = 35;

            GG.Equip.Worker.DateTimeSetCmd.RunCmd();

        }

        private void btnRmmEqpWrite_Click(object sender, EventArgs e)
        {



        }

        private void btnRmmHostWrite_Click(object sender, EventArgs e)
        {

        }

        private void btnPlcEqSateChange_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.StateChangEvent);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.ProcessStateChangEvent);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.CellCassetteIn);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.CellCassetteOut);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.AlarmHappenEvent);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.AlarmClearEvent);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.EqEcidChangeEvent);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.CurrentPpidChangeEvent);
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.PpidCreateDeleteModifyEvent);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.CellScrapEvent);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            PLC.Toggle(EQPB.CellUnScrapEvent);
        }

        private void button22_Click(object sender, EventArgs e)
        {

        }

        private void button16_Click(object sender, EventArgs e)
        {
            CellInfo glass = GG.Equip.GetCellInfoFromPLC(EQPW.CellInCellData);

            GG.Equip.PanelUnloading(glass);
        }
    }
}

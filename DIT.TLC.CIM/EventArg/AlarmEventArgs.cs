﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Struct;

namespace DitInlineCim.EventArg
{
    public delegate void AlarmEventHandler(object sender, AlarmEventArgs e);
    public class AlarmEventArgs : EventArgs
    {
        public AlarmInfo Alarm { get; set; }
    }
}

﻿namespace DitInlineCim
{
    partial class FrmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpMain = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.axHsms = new AxEZNETLIGHTLib.AxEZNetLight();
            this.btnClearTerMsg = new System.Windows.Forms.Button();
            this.lblTerminnalMsg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lstTermsg = new System.Windows.Forms.ListView();
            this.ColumnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstAlarm = new System.Windows.Forms.ListView();
            this.ColumnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtLog = new System.Windows.Forms.TextBox();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.btnOpCall = new System.Windows.Forms.Button();
            this.tpEqpState = new System.Windows.Forms.TabPage();
            this.ucrlModuleState = new DitInlineCim.Ucrl.UcrlModuleState();
            this.tbEoid = new System.Windows.Forms.TabPage();
            this.ucrlEoid = new DitInlineCim.Struct.UcrlEoid();
            this.tpAPC = new System.Windows.Forms.TabPage();            
            this.tpStep = new System.Windows.Forms.TabPage();
            this.ucrlScenarioMon = new DitInlineCim.Ucrl.UcrlScenarioMon();
            this.tbSV = new System.Windows.Forms.TabPage();
            this.btnSvLoad = new System.Windows.Forms.Button();
            this.gridSv = new System.Windows.Forms.DataGridView();
            this.SVID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLC_BIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLCADDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbDv = new System.Windows.Forms.TabPage();
            this.btnDvLoad = new System.Windows.Forms.Button();
            this.gridDv = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLC_BIT_DV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpRecipe = new System.Windows.Forms.TabPage();
            this.lblCurrentEqpRecipeID = new System.Windows.Forms.Label();
            this.lblCurrentPPID = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRecipeLoad = new System.Windows.Forms.Button();
            this.dgvPPID = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chPPIDRecipID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvRecipeBody = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvRecipe = new System.Windows.Forms.DataGridView();
            this.chRecipeSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chRecipeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chRecipeVer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnEcLoad = new System.Windows.Forms.Button();
            this.gridEc = new System.Windows.Forms.DataGridView();
            this.CoEcNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEcDefine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEcSLL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEcSUL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEcWLL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEcWUL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAddr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.btnProcStateSetup = new System.Windows.Forms.Button();
            this.btnProcStateReady = new System.Windows.Forms.Button();
            this.btnProcStateExecute = new System.Windows.Forms.Button();
            this.btnProcStatePause = new System.Windows.Forms.Button();
            this.btnProcStateIdle = new System.Windows.Forms.Button();
            this.Button22 = new System.Windows.Forms.Button();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.btnEqStatePM = new System.Windows.Forms.Button();
            this.btnEqStateFault = new System.Windows.Forms.Button();
            this.btnEqStateNormal = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.btnMCmdOffLine = new System.Windows.Forms.Button();
            this.btnMCmdLocal = new System.Windows.Forms.Button();
            this.btnMCmdRemote = new System.Windows.Forms.Button();
            this.ucrlTimer = new DitInlineCim.Ucrl.UcrlTimer();
            this.ucrlConnState = new DitInlineCim.Ucrl.UcrlConnState();
            this.tmTd = new System.Windows.Forms.Timer(this.components);
            this.tmrStart = new System.Windows.Forms.Timer(this.components);
            this.tmrHostDisConnMsg = new System.Windows.Forms.Timer(this.components);
            this.tmrState = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axHsms)).BeginInit();
            this.GroupBox5.SuspendLayout();
            this.tpEqpState.SuspendLayout();
            this.tbEoid.SuspendLayout();
            this.tpAPC.SuspendLayout();
            this.tpStep.SuspendLayout();
            this.tbSV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSv)).BeginInit();
            this.tbDv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDv)).BeginInit();
            this.tpRecipe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPPID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipeBody)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipe)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEc)).BeginInit();
            this.Panel3.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 221F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 214F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 359F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 167F));
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Panel3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.Panel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucrlTimer, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.ucrlConnState, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 730);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 4);
            this.panel4.Controls.Add(this.tcMain);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 103);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1002, 624);
            this.panel4.TabIndex = 28;
            // 
            // tcMain
            // 
            this.tcMain.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tcMain.Controls.Add(this.tpMain);
            this.tcMain.Controls.Add(this.tpEqpState);
            this.tcMain.Controls.Add(this.tbEoid);
            this.tcMain.Controls.Add(this.tpAPC);
            this.tcMain.Controls.Add(this.tpStep);
            this.tcMain.Controls.Add(this.tbSV);
            this.tcMain.Controls.Add(this.tbDv);
            this.tcMain.Controls.Add(this.tpRecipe);
            this.tcMain.Controls.Add(this.tabPage1);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Font = new System.Drawing.Font("휴먼모음T", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tcMain.ItemSize = new System.Drawing.Size(96, 30);
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1002, 624);
            this.tcMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tcMain.TabIndex = 0;
            // 
            // tpMain
            // 
            this.tpMain.Controls.Add(this.button1);
            this.tpMain.Controls.Add(this.button2);
            this.tpMain.Controls.Add(this.btnTest);
            this.tpMain.Controls.Add(this.tbStatus);
            this.tpMain.Controls.Add(this.axHsms);
            this.tpMain.Controls.Add(this.btnClearTerMsg);
            this.tpMain.Controls.Add(this.lblTerminnalMsg);
            this.tpMain.Controls.Add(this.label1);
            this.tpMain.Controls.Add(this.lstTermsg);
            this.tpMain.Controls.Add(this.lstAlarm);
            this.tpMain.Controls.Add(this.txtLog);
            this.tpMain.Controls.Add(this.GroupBox5);
            this.tpMain.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tpMain.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpMain.Location = new System.Drawing.Point(4, 34);
            this.tpMain.Name = "tpMain";
            this.tpMain.Padding = new System.Windows.Forms.Padding(3);
            this.tpMain.Size = new System.Drawing.Size(994, 586);
            this.tpMain.TabIndex = 0;
            this.tpMain.Text = "Main";
            this.tpMain.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Silver;
            this.button1.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.DarkBlue;
            this.button1.Location = new System.Drawing.Point(692, 87);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 44);
            this.button1.TabIndex = 40;
            this.button1.Tag = "1";
            this.button1.Text = "OP. CALL";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Silver;
            this.button2.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.DarkBlue;
            this.button2.Location = new System.Drawing.Point(943, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(45, 44);
            this.button2.TabIndex = 39;
            this.button2.Tag = "1";
            this.button2.Text = "RR";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnTest
            // 
            this.btnTest.BackColor = System.Drawing.Color.Silver;
            this.btnTest.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold);
            this.btnTest.ForeColor = System.Drawing.Color.DarkBlue;
            this.btnTest.Location = new System.Drawing.Point(855, 6);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(88, 44);
            this.btnTest.TabIndex = 39;
            this.btnTest.Tag = "1";
            this.btnTest.Text = "PLC Monitor";
            this.btnTest.UseVisualStyleBackColor = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click_1);
            // 
            // tbStatus
            // 
            this.tbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStatus.Location = new System.Drawing.Point(3, 559);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.ReadOnly = true;
            this.tbStatus.Size = new System.Drawing.Size(988, 25);
            this.tbStatus.TabIndex = 23;
            // 
            // axHsms
            // 
            this.axHsms.Enabled = true;
            this.axHsms.Location = new System.Drawing.Point(834, 2);
            this.axHsms.Name = "axHsms";
            this.axHsms.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axHsms.OcxState")));
            this.axHsms.Size = new System.Drawing.Size(21, 20);
            this.axHsms.TabIndex = 54;
            this.axHsms.OnConnected += new AxEZNETLIGHTLib._DEZNetLightEvents_OnConnectedEventHandler(this.axEZNetLight_OnConnected);
            this.axHsms.OnDisconnected += new AxEZNETLIGHTLib._DEZNetLightEvents_OnDisconnectedEventHandler(this.axEZNetLight_OnDisconnected);
            this.axHsms.OnMsgReceived += new AxEZNETLIGHTLib._DEZNetLightEvents_OnMsgReceivedEventHandler(this.axEZNetLight_OnMsgReceived);
            this.axHsms.OnOtherEvent += new AxEZNETLIGHTLib._DEZNetLightEvents_OnOtherEventEventHandler(this.axEZNetLight_OnOtherEvent);
            // 
            // btnClearTerMsg
            // 
            this.btnClearTerMsg.BackColor = System.Drawing.Color.Silver;
            this.btnClearTerMsg.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold);
            this.btnClearTerMsg.ForeColor = System.Drawing.Color.DarkBlue;
            this.btnClearTerMsg.Location = new System.Drawing.Point(703, 0);
            this.btnClearTerMsg.Name = "btnClearTerMsg";
            this.btnClearTerMsg.Size = new System.Drawing.Size(125, 44);
            this.btnClearTerMsg.TabIndex = 55;
            this.btnClearTerMsg.Tag = "1";
            this.btnClearTerMsg.Text = "TER. MSG";
            this.btnClearTerMsg.UseVisualStyleBackColor = false;
            this.btnClearTerMsg.Visible = false;
            this.btnClearTerMsg.Click += new System.EventHandler(this.btnClearTerMsg_Click);
            // 
            // lblTerminnalMsg
            // 
            this.lblTerminnalMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTerminnalMsg.BackColor = System.Drawing.Color.White;
            this.lblTerminnalMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTerminnalMsg.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblTerminnalMsg.Location = new System.Drawing.Point(3, 46);
            this.lblTerminnalMsg.Margin = new System.Windows.Forms.Padding(0);
            this.lblTerminnalMsg.Name = "lblTerminnalMsg";
            this.lblTerminnalMsg.Size = new System.Drawing.Size(824, 71);
            this.lblTerminnalMsg.TabIndex = 70;
            this.lblTerminnalMsg.Text = "-";
            this.lblTerminnalMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("휴먼모음T", 11.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(703, 38);
            this.label1.TabIndex = 69;
            this.label1.Text = "Terminal Message";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstTermsg
            // 
            this.lstTermsg.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lstTermsg.AutoArrange = false;
            this.lstTermsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lstTermsg.BackgroundImageTiled = true;
            this.lstTermsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstTermsg.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader6,
            this.ColumnHeader7,
            this.ColumnHeader10});
            this.lstTermsg.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lstTermsg.ForeColor = System.Drawing.Color.White;
            this.lstTermsg.FullRowSelect = true;
            this.lstTermsg.GridLines = true;
            this.lstTermsg.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstTermsg.Location = new System.Drawing.Point(3, 121);
            this.lstTermsg.MultiSelect = false;
            this.lstTermsg.Name = "lstTermsg";
            this.lstTermsg.Size = new System.Drawing.Size(985, 152);
            this.lstTermsg.TabIndex = 24;
            this.lstTermsg.TileSize = new System.Drawing.Size(1, 1);
            this.lstTermsg.UseCompatibleStateImageBehavior = false;
            this.lstTermsg.View = System.Windows.Forms.View.Details;
            // 
            // ColumnHeader6
            // 
            this.ColumnHeader6.Text = "Time";
            this.ColumnHeader6.Width = 70;
            // 
            // ColumnHeader7
            // 
            this.ColumnHeader7.Text = "TID";
            this.ColumnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColumnHeader7.Width = 40;
            // 
            // ColumnHeader10
            // 
            this.ColumnHeader10.Text = "Message";
            this.ColumnHeader10.Width = 745;
            // 
            // lstAlarm
            // 
            this.lstAlarm.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lstAlarm.AutoArrange = false;
            this.lstAlarm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lstAlarm.BackgroundImageTiled = true;
            this.lstAlarm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstAlarm.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader1,
            this.ColumnHeader5,
            this.ColumnHeader2,
            this.ColumnHeader4,
            this.ColumnHeader3});
            this.lstAlarm.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lstAlarm.ForeColor = System.Drawing.Color.Red;
            this.lstAlarm.FullRowSelect = true;
            this.lstAlarm.GridLines = true;
            this.lstAlarm.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstAlarm.Location = new System.Drawing.Point(3, 278);
            this.lstAlarm.MultiSelect = false;
            this.lstAlarm.Name = "lstAlarm";
            this.lstAlarm.Size = new System.Drawing.Size(985, 162);
            this.lstAlarm.TabIndex = 23;
            this.lstAlarm.TileSize = new System.Drawing.Size(1, 1);
            this.lstAlarm.UseCompatibleStateImageBehavior = false;
            this.lstAlarm.View = System.Windows.Forms.View.Details;
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "Time";
            this.ColumnHeader1.Width = 215;
            // 
            // ColumnHeader5
            // 
            this.ColumnHeader5.Text = "Alarm Text";
            this.ColumnHeader5.Width = 322;
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "MODULEID";
            this.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColumnHeader2.Width = 160;
            // 
            // ColumnHeader4
            // 
            this.ColumnHeader4.Text = "ALID";
            this.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColumnHeader4.Width = 80;
            // 
            // ColumnHeader3
            // 
            this.ColumnHeader3.Text = "ALCD";
            this.ColumnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColumnHeader3.Width = 80;
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.txtLog.Location = new System.Drawing.Point(3, 446);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(989, 108);
            this.txtLog.TabIndex = 57;
            // 
            // GroupBox5
            // 
            this.GroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.GroupBox5.Controls.Add(this.btnOpCall);
            this.GroupBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GroupBox5.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold);
            this.GroupBox5.Location = new System.Drawing.Point(847, 52);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(140, 65);
            this.GroupBox5.TabIndex = 45;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "History";
            // 
            // btnOpCall
            // 
            this.btnOpCall.BackColor = System.Drawing.Color.Silver;
            this.btnOpCall.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Bold);
            this.btnOpCall.ForeColor = System.Drawing.Color.DarkBlue;
            this.btnOpCall.Location = new System.Drawing.Point(8, 17);
            this.btnOpCall.Name = "btnOpCall";
            this.btnOpCall.Size = new System.Drawing.Size(125, 44);
            this.btnOpCall.TabIndex = 39;
            this.btnOpCall.Tag = "1";
            this.btnOpCall.Text = "OP. CALL";
            this.btnOpCall.UseVisualStyleBackColor = false;
            this.btnOpCall.Click += new System.EventHandler(this.btnOpCall_Click);
            // 
            // tpEqpState
            // 
            this.tpEqpState.Controls.Add(this.ucrlModuleState);
            this.tpEqpState.Font = new System.Drawing.Font("맑은 고딕", 9.75F);
            this.tpEqpState.Location = new System.Drawing.Point(4, 34);
            this.tpEqpState.Name = "tpEqpState";
            this.tpEqpState.Padding = new System.Windows.Forms.Padding(3);
            this.tpEqpState.Size = new System.Drawing.Size(994, 586);
            this.tpEqpState.TabIndex = 2;
            this.tpEqpState.Text = "State";
            this.tpEqpState.UseVisualStyleBackColor = true;
            // 
            // ucrlModuleState
            // 
            this.ucrlModuleState.Location = new System.Drawing.Point(0, 0);
            this.ucrlModuleState.Margin = new System.Windows.Forms.Padding(0);
            this.ucrlModuleState.Name = "ucrlModuleState";
            this.ucrlModuleState.Size = new System.Drawing.Size(994, 65535);
            this.ucrlModuleState.TabIndex = 0;
            // 
            // tbEoid
            // 
            this.tbEoid.Controls.Add(this.ucrlEoid);
            this.tbEoid.Location = new System.Drawing.Point(4, 34);
            this.tbEoid.Name = "tbEoid";
            this.tbEoid.Padding = new System.Windows.Forms.Padding(3);
            this.tbEoid.Size = new System.Drawing.Size(994, 586);
            this.tbEoid.TabIndex = 3;
            this.tbEoid.Text = "EOID";
            this.tbEoid.UseVisualStyleBackColor = true;
            // 
            // ucrlEoid
            // 
            this.ucrlEoid.Font = new System.Drawing.Font("맑은 고딕", 9.75F);
            this.ucrlEoid.Location = new System.Drawing.Point(0, 0);
            this.ucrlEoid.Margin = new System.Windows.Forms.Padding(0);
            this.ucrlEoid.Name = "ucrlEoid";
            this.ucrlEoid.Size = new System.Drawing.Size(994, 594);
            this.ucrlEoid.TabIndex = 0;
            // 
            // tpAPC
            // 
                        this.tpAPC.Font = new System.Drawing.Font("맑은 고딕", 9.75F);
            this.tpAPC.Location = new System.Drawing.Point(4, 34);
            this.tpAPC.Name = "tpAPC";
            this.tpAPC.Padding = new System.Windows.Forms.Padding(3);
            this.tpAPC.Size = new System.Drawing.Size(994, 586);
            this.tpAPC.TabIndex = 5;
            this.tpAPC.Text = "APC";
            this.tpAPC.UseVisualStyleBackColor = true;            
            // 
            // tpStep
            // 
            this.tpStep.Controls.Add(this.ucrlScenarioMon);
            this.tpStep.Font = new System.Drawing.Font("맑은 고딕", 9.75F);
            this.tpStep.Location = new System.Drawing.Point(4, 34);
            this.tpStep.Name = "tpStep";
            this.tpStep.Padding = new System.Windows.Forms.Padding(3);
            this.tpStep.Size = new System.Drawing.Size(994, 586);
            this.tpStep.TabIndex = 4;
            this.tpStep.Text = "    Step";
            // 
            // ucrlScenarioMon
            // 
            this.ucrlScenarioMon.Location = new System.Drawing.Point(0, 0);
            this.ucrlScenarioMon.Margin = new System.Windows.Forms.Padding(0);
            this.ucrlScenarioMon.Name = "ucrlScenarioMon";
            this.ucrlScenarioMon.Size = new System.Drawing.Size(697, 602);
            this.ucrlScenarioMon.TabIndex = 0;
            // 
            // tbSV
            // 
            this.tbSV.Controls.Add(this.btnSvLoad);
            this.tbSV.Controls.Add(this.gridSv);
            this.tbSV.Location = new System.Drawing.Point(4, 34);
            this.tbSV.Name = "tbSV";
            this.tbSV.Padding = new System.Windows.Forms.Padding(3);
            this.tbSV.Size = new System.Drawing.Size(994, 586);
            this.tbSV.TabIndex = 6;
            this.tbSV.Text = "SV";
            this.tbSV.UseVisualStyleBackColor = true;
            // 
            // btnSvLoad
            // 
            this.btnSvLoad.Location = new System.Drawing.Point(6, 414);
            this.btnSvLoad.Name = "btnSvLoad";
            this.btnSvLoad.Size = new System.Drawing.Size(107, 35);
            this.btnSvLoad.TabIndex = 9;
            this.btnSvLoad.Text = "LOAD";
            this.btnSvLoad.UseVisualStyleBackColor = true;
            this.btnSvLoad.Click += new System.EventHandler(this.btnSvLoad_Click);
            // 
            // gridSv
            // 
            this.gridSv.AllowUserToAddRows = false;
            this.gridSv.AllowUserToDeleteRows = false;
            this.gridSv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SVID,
            this.NAME,
            this.VALUE,
            this.PLC_BIT,
            this.PLCADDR});
            this.gridSv.Location = new System.Drawing.Point(6, 16);
            this.gridSv.Name = "gridSv";
            this.gridSv.ReadOnly = true;
            this.gridSv.RowTemplate.Height = 23;
            this.gridSv.Size = new System.Drawing.Size(756, 392);
            this.gridSv.TabIndex = 8;
            // 
            // SVID
            // 
            this.SVID.HeaderText = "ID";
            this.SVID.Name = "SVID";
            this.SVID.ReadOnly = true;
            // 
            // NAME
            // 
            this.NAME.HeaderText = "NAME";
            this.NAME.Name = "NAME";
            this.NAME.ReadOnly = true;
            this.NAME.Width = 300;
            // 
            // VALUE
            // 
            this.VALUE.HeaderText = "VALUE";
            this.VALUE.Name = "VALUE";
            this.VALUE.ReadOnly = true;
            this.VALUE.Width = 150;
            // 
            // PLC_BIT
            // 
            this.PLC_BIT.HeaderText = "PLC_BIT";
            this.PLC_BIT.Name = "PLC_BIT";
            this.PLC_BIT.ReadOnly = true;
            // 
            // PLCADDR
            // 
            this.PLCADDR.HeaderText = "PLC_ADDR";
            this.PLCADDR.Name = "PLCADDR";
            this.PLCADDR.ReadOnly = true;
            // 
            // tbDv
            // 
            this.tbDv.Controls.Add(this.btnDvLoad);
            this.tbDv.Controls.Add(this.gridDv);
            this.tbDv.Location = new System.Drawing.Point(4, 34);
            this.tbDv.Name = "tbDv";
            this.tbDv.Padding = new System.Windows.Forms.Padding(3);
            this.tbDv.Size = new System.Drawing.Size(994, 586);
            this.tbDv.TabIndex = 7;
            this.tbDv.Text = "DV";
            this.tbDv.UseVisualStyleBackColor = true;
            // 
            // btnDvLoad
            // 
            this.btnDvLoad.Location = new System.Drawing.Point(6, 404);
            this.btnDvLoad.Name = "btnDvLoad";
            this.btnDvLoad.Size = new System.Drawing.Size(107, 35);
            this.btnDvLoad.TabIndex = 11;
            this.btnDvLoad.Text = "LOAD";
            this.btnDvLoad.UseVisualStyleBackColor = true;
            this.btnDvLoad.Click += new System.EventHandler(this.btnDvLoad_Click);
            // 
            // gridDv
            // 
            this.gridDv.AllowUserToAddRows = false;
            this.gridDv.AllowUserToDeleteRows = false;
            this.gridDv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.PLC_BIT_DV,
            this.dataGridViewTextBoxColumn3});
            this.gridDv.Location = new System.Drawing.Point(6, 6);
            this.gridDv.Name = "gridDv";
            this.gridDv.ReadOnly = true;
            this.gridDv.RowTemplate.Height = 23;
            this.gridDv.Size = new System.Drawing.Size(756, 392);
            this.gridDv.TabIndex = 10;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "NAME";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 300;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "VALUE";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // PLC_BIT_DV
            // 
            this.PLC_BIT_DV.HeaderText = "PLC_BIT";
            this.PLC_BIT_DV.Name = "PLC_BIT_DV";
            this.PLC_BIT_DV.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "PLC_ADDR";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // tpRecipe
            // 
            this.tpRecipe.Controls.Add(this.lblCurrentEqpRecipeID);
            this.tpRecipe.Controls.Add(this.lblCurrentPPID);
            this.tpRecipe.Controls.Add(this.label5);
            this.tpRecipe.Controls.Add(this.label19);
            this.tpRecipe.Controls.Add(this.label4);
            this.tpRecipe.Controls.Add(this.label3);
            this.tpRecipe.Controls.Add(this.label2);
            this.tpRecipe.Controls.Add(this.btnRecipeLoad);
            this.tpRecipe.Controls.Add(this.dgvPPID);
            this.tpRecipe.Controls.Add(this.dgvRecipeBody);
            this.tpRecipe.Controls.Add(this.dgvRecipe);
            this.tpRecipe.Location = new System.Drawing.Point(4, 34);
            this.tpRecipe.Name = "tpRecipe";
            this.tpRecipe.Padding = new System.Windows.Forms.Padding(3);
            this.tpRecipe.Size = new System.Drawing.Size(994, 586);
            this.tpRecipe.TabIndex = 8;
            this.tpRecipe.Text = "RECIPE";
            this.tpRecipe.UseVisualStyleBackColor = true;
            // 
            // lblCurrentEqpRecipeID
            // 
            this.lblCurrentEqpRecipeID.BackColor = System.Drawing.Color.White;
            this.lblCurrentEqpRecipeID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCurrentEqpRecipeID.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblCurrentEqpRecipeID.ForeColor = System.Drawing.Color.Black;
            this.lblCurrentEqpRecipeID.Location = new System.Drawing.Point(97, 270);
            this.lblCurrentEqpRecipeID.Margin = new System.Windows.Forms.Padding(1);
            this.lblCurrentEqpRecipeID.Name = "lblCurrentEqpRecipeID";
            this.lblCurrentEqpRecipeID.Size = new System.Drawing.Size(138, 31);
            this.lblCurrentEqpRecipeID.TabIndex = 76;
            this.lblCurrentEqpRecipeID.Text = "-";
            this.lblCurrentEqpRecipeID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCurrentPPID
            // 
            this.lblCurrentPPID.BackColor = System.Drawing.Color.White;
            this.lblCurrentPPID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCurrentPPID.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.lblCurrentPPID.ForeColor = System.Drawing.Color.Black;
            this.lblCurrentPPID.Location = new System.Drawing.Point(97, 237);
            this.lblCurrentPPID.Margin = new System.Windows.Forms.Padding(1);
            this.lblCurrentPPID.Name = "lblCurrentPPID";
            this.lblCurrentPPID.Size = new System.Drawing.Size(138, 31);
            this.lblCurrentPPID.TabIndex = 77;
            this.lblCurrentPPID.Text = "-";
            this.lblCurrentPPID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(6, 270);
            this.label5.Margin = new System.Windows.Forms.Padding(1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 31);
            this.label5.TabIndex = 74;
            this.label5.Text = "Current EqpID";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("휴먼모음T", 8.25F);
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(6, 237);
            this.label19.Margin = new System.Windows.Forms.Padding(1);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 31);
            this.label19.TabIndex = 75;
            this.label19.Text = "Current PPID";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(445, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(359, 38);
            this.label4.TabIndex = 73;
            this.label4.Text = "EQP RECIPE BODY";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(445, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(359, 38);
            this.label3.TabIndex = 72;
            this.label3.Text = "EQP RECIPE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("휴먼모음T", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(433, 38);
            this.label2.TabIndex = 71;
            this.label2.Text = "Host PPID";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRecipeLoad
            // 
            this.btnRecipeLoad.Location = new System.Drawing.Point(852, 6);
            this.btnRecipeLoad.Name = "btnRecipeLoad";
            this.btnRecipeLoad.Size = new System.Drawing.Size(107, 35);
            this.btnRecipeLoad.TabIndex = 12;
            this.btnRecipeLoad.Text = "LOAD";
            this.btnRecipeLoad.UseVisualStyleBackColor = true;
            this.btnRecipeLoad.Click += new System.EventHandler(this.btnRecipeLoad_Click);
            // 
            // dgvPPID
            // 
            this.dgvPPID.AllowUserToAddRows = false;
            this.dgvPPID.AllowUserToDeleteRows = false;
            this.dgvPPID.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPPID.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn4,
            this.chPPIDRecipID,
            this.dataGridViewTextBoxColumn6});
            this.dgvPPID.Location = new System.Drawing.Point(5, 56);
            this.dgvPPID.Name = "dgvPPID";
            this.dgvPPID.ReadOnly = true;
            this.dgvPPID.RowTemplate.Height = 23;
            this.dgvPPID.Size = new System.Drawing.Size(434, 160);
            this.dgvPPID.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Seq";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 50;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Name";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 120;
            // 
            // chPPIDRecipID
            // 
            this.chPPIDRecipID.HeaderText = "Recipe Seq";
            this.chPPIDRecipID.Name = "chPPIDRecipID";
            this.chPPIDRecipID.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "SoftVer";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dgvRecipeBody
            // 
            this.dgvRecipeBody.AllowUserToAddRows = false;
            this.dgvRecipeBody.AllowUserToDeleteRows = false;
            this.dgvRecipeBody.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecipeBody.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.dgvRecipeBody.Location = new System.Drawing.Point(445, 278);
            this.dgvRecipeBody.Name = "dgvRecipeBody";
            this.dgvRecipeBody.ReadOnly = true;
            this.dgvRecipeBody.RowTemplate.Height = 23;
            this.dgvRecipeBody.Size = new System.Drawing.Size(359, 312);
            this.dgvRecipeBody.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Seq";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Name";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "SoftVer";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dgvRecipe
            // 
            this.dgvRecipe.AllowUserToAddRows = false;
            this.dgvRecipe.AllowUserToDeleteRows = false;
            this.dgvRecipe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecipe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chRecipeSeq,
            this.chRecipeName,
            this.chRecipeVer});
            this.dgvRecipe.Location = new System.Drawing.Point(445, 56);
            this.dgvRecipe.Name = "dgvRecipe";
            this.dgvRecipe.ReadOnly = true;
            this.dgvRecipe.RowTemplate.Height = 23;
            this.dgvRecipe.Size = new System.Drawing.Size(359, 160);
            this.dgvRecipe.TabIndex = 11;
            this.dgvRecipe.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRecipe_CellDoubleClick);
            // 
            // chRecipeSeq
            // 
            this.chRecipeSeq.HeaderText = "Seq";
            this.chRecipeSeq.Name = "chRecipeSeq";
            this.chRecipeSeq.ReadOnly = true;
            this.chRecipeSeq.Width = 50;
            // 
            // chRecipeName
            // 
            this.chRecipeName.HeaderText = "Name";
            this.chRecipeName.Name = "chRecipeName";
            this.chRecipeName.ReadOnly = true;
            this.chRecipeName.Width = 120;
            // 
            // chRecipeVer
            // 
            this.chRecipeVer.HeaderText = "SoftVer";
            this.chRecipeVer.Name = "chRecipeVer";
            this.chRecipeVer.ReadOnly = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnEcLoad);
            this.tabPage1.Controls.Add(this.gridEc);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(994, 586);
            this.tabPage1.TabIndex = 9;
            this.tabPage1.Text = "EC";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnEcLoad
            // 
            this.btnEcLoad.Location = new System.Drawing.Point(6, 404);
            this.btnEcLoad.Name = "btnEcLoad";
            this.btnEcLoad.Size = new System.Drawing.Size(107, 35);
            this.btnEcLoad.TabIndex = 12;
            this.btnEcLoad.Text = "LOAD";
            this.btnEcLoad.UseVisualStyleBackColor = true;
            this.btnEcLoad.Click += new System.EventHandler(this.btnEcLoad_Click);
            // 
            // gridEc
            // 
            this.gridEc.AllowUserToAddRows = false;
            this.gridEc.AllowUserToDeleteRows = false;
            this.gridEc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridEc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CoEcNo,
            this.dataGridViewTextBoxColumn10,
            this.ColEcDefine,
            this.ColEcSLL,
            this.ColEcSUL,
            this.ColEcWLL,
            this.ColEcWUL,
            this.ColAddr});
            this.gridEc.Location = new System.Drawing.Point(6, 6);
            this.gridEc.Name = "gridEc";
            this.gridEc.ReadOnly = true;
            this.gridEc.RowTemplate.Height = 23;
            this.gridEc.Size = new System.Drawing.Size(957, 392);
            this.gridEc.TabIndex = 11;
            // 
            // CoEcNo
            // 
            this.CoEcNo.HeaderText = "No";
            this.CoEcNo.Name = "CoEcNo";
            this.CoEcNo.ReadOnly = true;
            this.CoEcNo.Width = 30;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.FillWeight = 200F;
            this.dataGridViewTextBoxColumn10.HeaderText = "Name";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 200;
            // 
            // ColEcDefine
            // 
            this.ColEcDefine.HeaderText = "Define";
            this.ColEcDefine.Name = "ColEcDefine";
            this.ColEcDefine.ReadOnly = true;
            this.ColEcDefine.Width = 70;
            // 
            // ColEcSLL
            // 
            this.ColEcSLL.HeaderText = "EcSLL";
            this.ColEcSLL.Name = "ColEcSLL";
            this.ColEcSLL.ReadOnly = true;
            this.ColEcSLL.Width = 70;
            // 
            // ColEcSUL
            // 
            this.ColEcSUL.HeaderText = "EcSUL";
            this.ColEcSUL.Name = "ColEcSUL";
            this.ColEcSUL.ReadOnly = true;
            this.ColEcSUL.Width = 70;
            // 
            // ColEcWLL
            // 
            this.ColEcWLL.HeaderText = "EcWLL";
            this.ColEcWLL.Name = "ColEcWLL";
            this.ColEcWLL.ReadOnly = true;
            this.ColEcWLL.Width = 70;
            // 
            // ColEcWUL
            // 
            this.ColEcWUL.HeaderText = "EcWUL";
            this.ColEcWUL.Name = "ColEcWUL";
            this.ColEcWUL.ReadOnly = true;
            this.ColEcWUL.Width = 70;
            // 
            // ColAddr
            // 
            this.ColAddr.HeaderText = "Addr";
            this.ColAddr.Name = "ColAddr";
            this.ColAddr.ReadOnly = true;
            this.ColAddr.Width = 150;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.Transparent;
            this.Panel3.Controls.Add(this.btnProcStateSetup);
            this.Panel3.Controls.Add(this.btnProcStateReady);
            this.Panel3.Controls.Add(this.btnProcStateExecute);
            this.Panel3.Controls.Add(this.btnProcStatePause);
            this.Panel3.Controls.Add(this.btnProcStateIdle);
            this.Panel3.Controls.Add(this.Button22);
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel3.Location = new System.Drawing.Point(438, 3);
            this.Panel3.Name = "Panel3";
            this.tableLayoutPanel1.SetRowSpan(this.Panel3, 2);
            this.Panel3.Size = new System.Drawing.Size(353, 94);
            this.Panel3.TabIndex = 27;
            // 
            // btnProcStateSetup
            // 
            this.btnProcStateSetup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnProcStateSetup.Enabled = false;
            this.btnProcStateSetup.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnProcStateSetup.ForeColor = System.Drawing.Color.Black;
            this.btnProcStateSetup.Location = new System.Drawing.Point(73, 54);
            this.btnProcStateSetup.Name = "btnProcStateSetup";
            this.btnProcStateSetup.Size = new System.Drawing.Size(70, 33);
            this.btnProcStateSetup.TabIndex = 8;
            this.btnProcStateSetup.Text = "SETUP";
            this.btnProcStateSetup.UseVisualStyleBackColor = false;
            this.btnProcStateSetup.Click += new System.EventHandler(this.btnProcStateSetup_Click);
            // 
            // btnProcStateReady
            // 
            this.btnProcStateReady.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnProcStateReady.Enabled = false;
            this.btnProcStateReady.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnProcStateReady.ForeColor = System.Drawing.Color.Black;
            this.btnProcStateReady.Location = new System.Drawing.Point(142, 54);
            this.btnProcStateReady.Name = "btnProcStateReady";
            this.btnProcStateReady.Size = new System.Drawing.Size(70, 33);
            this.btnProcStateReady.TabIndex = 7;
            this.btnProcStateReady.Text = "READY";
            this.btnProcStateReady.UseVisualStyleBackColor = false;
            this.btnProcStateReady.Click += new System.EventHandler(this.btnProcStateReady_Click);
            // 
            // btnProcStateExecute
            // 
            this.btnProcStateExecute.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnProcStateExecute.Enabled = false;
            this.btnProcStateExecute.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnProcStateExecute.ForeColor = System.Drawing.Color.Black;
            this.btnProcStateExecute.Location = new System.Drawing.Point(211, 54);
            this.btnProcStateExecute.Name = "btnProcStateExecute";
            this.btnProcStateExecute.Size = new System.Drawing.Size(70, 33);
            this.btnProcStateExecute.TabIndex = 5;
            this.btnProcStateExecute.Text = "EXECUTE";
            this.btnProcStateExecute.UseVisualStyleBackColor = false;
            this.btnProcStateExecute.Click += new System.EventHandler(this.btnProcStateExecute_Click);
            // 
            // btnProcStatePause
            // 
            this.btnProcStatePause.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnProcStatePause.Enabled = false;
            this.btnProcStatePause.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnProcStatePause.ForeColor = System.Drawing.Color.Black;
            this.btnProcStatePause.Location = new System.Drawing.Point(280, 54);
            this.btnProcStatePause.Name = "btnProcStatePause";
            this.btnProcStatePause.Size = new System.Drawing.Size(70, 33);
            this.btnProcStatePause.TabIndex = 6;
            this.btnProcStatePause.Text = "PAUSE";
            this.btnProcStatePause.UseVisualStyleBackColor = false;
            this.btnProcStatePause.Click += new System.EventHandler(this.btnProcStatePause_Click);
            // 
            // btnProcStateIdle
            // 
            this.btnProcStateIdle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnProcStateIdle.Enabled = false;
            this.btnProcStateIdle.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnProcStateIdle.ForeColor = System.Drawing.Color.Black;
            this.btnProcStateIdle.Location = new System.Drawing.Point(3, 54);
            this.btnProcStateIdle.Name = "btnProcStateIdle";
            this.btnProcStateIdle.Size = new System.Drawing.Size(70, 33);
            this.btnProcStateIdle.TabIndex = 4;
            this.btnProcStateIdle.Text = "IDLE";
            this.btnProcStateIdle.UseVisualStyleBackColor = false;
            this.btnProcStateIdle.Click += new System.EventHandler(this.btnProcStateIdle_Click);
            // 
            // Button22
            // 
            this.Button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(217)))), ((int)(((byte)(187)))));
            this.Button22.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.Button22.Location = new System.Drawing.Point(1, 2);
            this.Button22.Name = "Button22";
            this.Button22.Size = new System.Drawing.Size(350, 50);
            this.Button22.TabIndex = 1;
            this.Button22.TabStop = false;
            this.Button22.Text = "EQ PROCESS STATUS";
            this.Button22.UseVisualStyleBackColor = false;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.Panel2.Controls.Add(this.btnEqStatePM);
            this.Panel2.Controls.Add(this.btnEqStateFault);
            this.Panel2.Controls.Add(this.btnEqStateNormal);
            this.Panel2.Controls.Add(this.button6);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel2.Location = new System.Drawing.Point(224, 3);
            this.Panel2.Name = "Panel2";
            this.tableLayoutPanel1.SetRowSpan(this.Panel2, 2);
            this.Panel2.Size = new System.Drawing.Size(208, 94);
            this.Panel2.TabIndex = 26;
            // 
            // btnEqStatePM
            // 
            this.btnEqStatePM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnEqStatePM.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEqStatePM.ForeColor = System.Drawing.Color.Black;
            this.btnEqStatePM.Location = new System.Drawing.Point(143, 54);
            this.btnEqStatePM.Name = "btnEqStatePM";
            this.btnEqStatePM.Size = new System.Drawing.Size(65, 33);
            this.btnEqStatePM.TabIndex = 5;
            this.btnEqStatePM.Text = "PM";
            this.btnEqStatePM.UseVisualStyleBackColor = false;
            this.btnEqStatePM.Click += new System.EventHandler(this.btnEqStatePM_Click);
            // 
            // btnEqStateFault
            // 
            this.btnEqStateFault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnEqStateFault.Enabled = false;
            this.btnEqStateFault.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEqStateFault.ForeColor = System.Drawing.Color.Black;
            this.btnEqStateFault.Location = new System.Drawing.Point(74, 54);
            this.btnEqStateFault.Name = "btnEqStateFault";
            this.btnEqStateFault.Size = new System.Drawing.Size(65, 33);
            this.btnEqStateFault.TabIndex = 4;
            this.btnEqStateFault.Text = "FAULT";
            this.btnEqStateFault.UseVisualStyleBackColor = false;
            this.btnEqStateFault.Click += new System.EventHandler(this.btnEqStateFault_Click);
            // 
            // btnEqStateNormal
            // 
            this.btnEqStateNormal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnEqStateNormal.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEqStateNormal.ForeColor = System.Drawing.Color.Black;
            this.btnEqStateNormal.Location = new System.Drawing.Point(6, 54);
            this.btnEqStateNormal.Name = "btnEqStateNormal";
            this.btnEqStateNormal.Size = new System.Drawing.Size(65, 33);
            this.btnEqStateNormal.TabIndex = 3;
            this.btnEqStateNormal.Text = "NORMAL";
            this.btnEqStateNormal.UseVisualStyleBackColor = false;
            this.btnEqStateNormal.Click += new System.EventHandler(this.btnEqStateNormal_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(217)))), ((int)(((byte)(187)))));
            this.button6.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button6.Location = new System.Drawing.Point(5, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(203, 50);
            this.button6.TabIndex = 1;
            this.button6.TabStop = false;
            this.button6.Text = "EQ STATUS";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.Panel1.Controls.Add(this.button7);
            this.Panel1.Controls.Add(this.btnMCmdOffLine);
            this.Panel1.Controls.Add(this.btnMCmdLocal);
            this.Panel1.Controls.Add(this.btnMCmdRemote);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(3, 3);
            this.Panel1.Name = "Panel1";
            this.tableLayoutPanel1.SetRowSpan(this.Panel1, 2);
            this.Panel1.Size = new System.Drawing.Size(215, 94);
            this.Panel1.TabIndex = 25;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(217)))), ((int)(((byte)(187)))));
            this.button7.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button7.Location = new System.Drawing.Point(4, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(210, 50);
            this.button7.TabIndex = 6;
            this.button7.TabStop = false;
            this.button7.Text = "MACHINE MODE";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button7_MouseDown);
            // 
            // btnMCmdOffLine
            // 
            this.btnMCmdOffLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnMCmdOffLine.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnMCmdOffLine.ForeColor = System.Drawing.Color.Black;
            this.btnMCmdOffLine.Location = new System.Drawing.Point(144, 54);
            this.btnMCmdOffLine.Name = "btnMCmdOffLine";
            this.btnMCmdOffLine.Size = new System.Drawing.Size(70, 33);
            this.btnMCmdOffLine.TabIndex = 5;
            this.btnMCmdOffLine.Tag = "1";
            this.btnMCmdOffLine.Text = "OFFLINE";
            this.btnMCmdOffLine.UseVisualStyleBackColor = false;
            this.btnMCmdOffLine.Click += new System.EventHandler(this.btnMCmdOffLine_Click);
            // 
            // btnMCmdLocal
            // 
            this.btnMCmdLocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnMCmdLocal.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnMCmdLocal.ForeColor = System.Drawing.Color.Black;
            this.btnMCmdLocal.Location = new System.Drawing.Point(74, 54);
            this.btnMCmdLocal.Name = "btnMCmdLocal";
            this.btnMCmdLocal.Size = new System.Drawing.Size(70, 33);
            this.btnMCmdLocal.TabIndex = 4;
            this.btnMCmdLocal.Tag = "2";
            this.btnMCmdLocal.Text = "LOCAL";
            this.btnMCmdLocal.UseVisualStyleBackColor = false;
            this.btnMCmdLocal.Click += new System.EventHandler(this.btnMCmdLocal_Click);
            // 
            // btnMCmdRemote
            // 
            this.btnMCmdRemote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnMCmdRemote.Font = new System.Drawing.Font("휴먼모음T", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnMCmdRemote.ForeColor = System.Drawing.Color.Black;
            this.btnMCmdRemote.Location = new System.Drawing.Point(4, 54);
            this.btnMCmdRemote.Name = "btnMCmdRemote";
            this.btnMCmdRemote.Size = new System.Drawing.Size(70, 33);
            this.btnMCmdRemote.TabIndex = 3;
            this.btnMCmdRemote.Tag = "3";
            this.btnMCmdRemote.Text = "REMOTE";
            this.btnMCmdRemote.UseVisualStyleBackColor = false;
            this.btnMCmdRemote.Click += new System.EventHandler(this.btnMCmdRemote_Click);
            // 
            // ucrlTimer
            // 
            this.ucrlTimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucrlTimer.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.ucrlTimer.Location = new System.Drawing.Point(795, 1);
            this.ucrlTimer.Margin = new System.Windows.Forms.Padding(1);
            this.ucrlTimer.Name = "ucrlTimer";
            this.ucrlTimer.Size = new System.Drawing.Size(212, 48);
            this.ucrlTimer.TabIndex = 30;
            // 
            // ucrlConnState
            // 
            this.ucrlConnState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucrlConnState.EquipPLCConnColor = System.Drawing.Color.Red;
            this.ucrlConnState.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.ucrlConnState.HostConnColor = System.Drawing.Color.Red;
            this.ucrlConnState.Location = new System.Drawing.Point(797, 53);
            this.ucrlConnState.Name = "ucrlConnState";
            this.ucrlConnState.Size = new System.Drawing.Size(208, 44);
            this.ucrlConnState.TabIndex = 31;
            // 
            // tmTd
            // 
            this.tmTd.Enabled = true;
            this.tmTd.Tick += new System.EventHandler(this.tmTd_Tick);
            // 
            // tmrStart
            // 
            this.tmrStart.Interval = 5000;
            // 
            // tmrHostDisConnMsg
            // 
            this.tmrHostDisConnMsg.Interval = 45000;
            this.tmrHostDisConnMsg.Tick += new System.EventHandler(this.tmrHostDisConnMsg_Tick);
            // 
            // tmrState
            // 
            this.tmrState.Enabled = true;
            this.tmrState.Interval = 1000;
            this.tmrState.Tick += new System.EventHandler(this.tmrState_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "DIT CIM";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tcMain.ResumeLayout(false);
            this.tpMain.ResumeLayout(false);
            this.tpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axHsms)).EndInit();
            this.GroupBox5.ResumeLayout(false);
            this.tpEqpState.ResumeLayout(false);
            this.tbEoid.ResumeLayout(false);
            this.tpAPC.ResumeLayout(false);
            this.tpStep.ResumeLayout(false);
            this.tbSV.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSv)).EndInit();
            this.tbDv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDv)).EndInit();
            this.tpRecipe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPPID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipeBody)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipe)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEc)).EndInit();
            this.Panel3.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Button btnMCmdOffLine;
        internal System.Windows.Forms.Button btnMCmdLocal;
        internal System.Windows.Forms.Button btnMCmdRemote;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Button btnEqStatePM;
        internal System.Windows.Forms.Button btnEqStateFault;
        internal System.Windows.Forms.Button btnEqStateNormal;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Button btnProcStateSetup;
        internal System.Windows.Forms.Button btnProcStateReady;
        internal System.Windows.Forms.Button btnProcStatePause;
        internal System.Windows.Forms.Button btnProcStateExecute;
        internal System.Windows.Forms.Button btnProcStateIdle;
        internal System.Windows.Forms.Button Button22;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpMain;
        
        private DitInlineCim.Ucrl.UcrlTimer ucrlTimer;
        private DitInlineCim.Ucrl.UcrlConnState ucrlConnState;
        private AxEZNETLIGHTLib.AxEZNetLight axHsms;
        private System.Windows.Forms.TabPage tpEqpState;
        private Ucrl.UcrlModuleState ucrlModuleState;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.TabPage tbEoid;
        private Struct.UcrlEoid ucrlEoid;
        private System.Windows.Forms.Timer tmTd;
        private System.Windows.Forms.TabPage tpStep;
        private Ucrl.UcrlScenarioMon ucrlScenarioMon;
        private System.Windows.Forms.Timer tmrStart;
        private System.Windows.Forms.TabPage tpAPC;        
        internal System.Windows.Forms.Button button6;
        internal System.Windows.Forms.Button button7;
        internal System.Windows.Forms.ListView lstTermsg;
        internal System.Windows.Forms.ColumnHeader ColumnHeader6;
        internal System.Windows.Forms.ColumnHeader ColumnHeader7;
        internal System.Windows.Forms.ColumnHeader ColumnHeader10;
        internal System.Windows.Forms.ListView lstAlarm;
        internal System.Windows.Forms.ColumnHeader ColumnHeader1;
        internal System.Windows.Forms.ColumnHeader ColumnHeader5;
        internal System.Windows.Forms.ColumnHeader ColumnHeader2;
        internal System.Windows.Forms.ColumnHeader ColumnHeader4;
        internal System.Windows.Forms.ColumnHeader ColumnHeader3;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label lblTerminnalMsg;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.Button btnOpCall;
        private System.Windows.Forms.Timer tmrHostDisConnMsg;
        internal System.Windows.Forms.Button btnClearTerMsg;
        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.Timer tmrState;
        internal System.Windows.Forms.Button btnTest;
        internal System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tbSV;
        private System.Windows.Forms.Button btnSvLoad;
        private System.Windows.Forms.DataGridView gridSv;
        private System.Windows.Forms.TabPage tbDv;
        private System.Windows.Forms.Button btnDvLoad;
        private System.Windows.Forms.DataGridView gridDv;
        private System.Windows.Forms.DataGridViewTextBoxColumn SVID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn VALUE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLC_BIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLCADDR;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLC_BIT_DV;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TabPage tpRecipe;
        private System.Windows.Forms.DataGridView dgvRecipe;
        private System.Windows.Forms.Button btnRecipeLoad;
        private System.Windows.Forms.DataGridView dgvPPID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn chPPIDRecipID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn chRecipeSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn chRecipeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn chRecipeVer;
        private System.Windows.Forms.DataGridView dgvRecipeBody;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnEcLoad;
        private System.Windows.Forms.DataGridView gridEc;
        private System.Windows.Forms.DataGridViewTextBoxColumn CoEcNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEcDefine;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEcSLL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEcSUL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEcWLL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEcWUL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAddr;
        internal System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Label lblCurrentEqpRecipeID;
        internal System.Windows.Forms.Label lblCurrentPPID;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label2;
    }
}


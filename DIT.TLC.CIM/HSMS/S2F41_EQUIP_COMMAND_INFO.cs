﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.HSMS
{

    public class S2F41_EQUIP_COMMAND_INFO
    {
        public int MODULE_COUNT;

        public string[] MODULE_NAME_ARR;
        public string[] MODULE_ARR;
        public short[] MODULE_PMACK_ARR;

        public string[] RCODE_NAME_ARR;
        public string[] RCODE_ARR;
        public short[] RCODE_PMACK_ARR;
        
        //public short[] PORTID_HCACK_ARR;

        public bool PMACK;
        public short TMACK;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Common;
using DitInlineCim.Log;
using DitInlineCim.Struct;
using System.Windows.Forms;
using DitInlineCim.Struct.RMM;
using DitInlineCim.Struct.SV;
using DitInlineCim.Struct.EC;
using DitInlineCim.Struct.DV;

namespace DitInlineCim.HSMS
{
    public class HSMS_SEND
    {
        public static AxEZNETLIGHTLib.AxEZNetLight AxHsms { get; set; }
        
        #region S1

        /// <summary>
        /// 메소드 - Are you there Request
        /// </summary>
        public static void S1F1()
        {
            int msgid = AxHsms.CreateMsg(1, 1, 1);

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append("[HSMS E→H][S1F1] Establish if the equipment is on the line");
            else
                Logger.Append("[HSMS E→H][S1F1], failed.");
        }

        /// <summary>
        /// 메소드 - On Line Data
        /// </summary>
        /// <param name="msgID"></param>
        public static void S1F2(int msgID)
        {
            string version = string.Empty;
            if (GG.Equip.MCMD == EMMCmd.OFFLINE) return;

            string onlineMode = GG.Equip.MCMD == EMMCmd.ONLINE_REMOTE ? "REMOTE" : "LOCAL";

            int msgid = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddListItem(msgid, 2);
            {
                AxHsms.AddAsciiItem(msgid, version, 20);
                AxHsms.AddAsciiItem(msgid, GG.Equip.ModuleID, 28);
                AxHsms.AddU1Item(msgid, (short)GG.Equip.MCMD, 1);
            }

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append("[HSMS E→H][S1F2][MCMD=" + onlineMode + "] On Line Data");
            else
                Logger.Append("[HSMS E→H][S1F2], failed.");
        }

        /// <summary>
        /// 메소드 - Selected Equipment State Data
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleId"></param>
        /// <param name="lstSv"></param>
        public static void S1F4(int msgID, string moduleId, List<int> lstSv)
        {
            int rpId = AxHsms.CreateReplyMsg(msgID);

            AxHsms.AddListItem(rpId, 2);
            {
                AxHsms.AddAsciiItem(rpId, moduleId, 28);

                if (moduleId != GG.Equip.ModuleID)
                {
                    AxHsms.AddListItem(rpId, 0);
                }
                else
                {
                    if (lstSv.Count() == 0)
                    {
                        AxHsms.AddListItem(rpId, GG.Equip.SvMgr.Count);
                        foreach (KeyValuePair<int, SvItem> svitem in GG.Equip.SvMgr)
                        {
                            AxHsms.AddListItem(rpId, 5);
                            {
                                AxHsms.AddU2Item(rpId, svitem.Value.SvID, 1);
                                AxHsms.AddAsciiItem(rpId, svitem.Value.SvValue, 40);
                                AxHsms.AddAsciiItem(rpId, svitem.Value.SvName, 40);
                            }
                        }
                    }
                    else
                    {
                        int nExistCount = 0;
                        foreach (int svid in lstSv)
                        {
                            if (GG.Equip.SvMgr.ContainsKey(svid))
                                nExistCount++;
                        }

                        if (nExistCount != lstSv.Count)
                        {
                            AxHsms.AddListItem(rpId, 0);
                        }
                        else
                        {
                            AxHsms.AddListItem(rpId, lstSv.Count);

                            foreach (int svid in lstSv)
                            {
                                if (GG.Equip.SvMgr.ContainsKey(svid))
                                {
                                    SvItem item = GG.Equip.SvMgr[svid];

                                    AxHsms.AddListItem(rpId, 3);
                                    {
                                        AxHsms.AddU2Item(rpId, item.SvID, 1);
                                        AxHsms.AddAsciiItem(rpId, item.SvValue, 40);
                                        AxHsms.AddAsciiItem(rpId, item.SvName, 40);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (AxHsms.SendMsg(rpId) == 0)
                Logger.Append(string.Format("[HSMS E→H][S1F4], SVID COUNT {0}", lstSv.Count));
            else
                Logger.Append("[HSMS E→H][S1F4], failed.");
        }

        /// <summary>
        /// 메소드 - Formatted State Data _ SFCD = 1.Equipment Online Parameter 
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="sfcd"></param>
        /// <param name="moduleID"></param>
        public static void S1F6_EqOnlineParameter(int msgId, short sfcd, string moduleID)
        {
            int repId = AxHsms.CreateReplyMsg(msgId);

            AxHsms.AddListItem(repId, 3);
            AxHsms.AddAsciiItem(repId, moduleID, 28);
            AxHsms.AddU1Item(repId, sfcd, 1);

            if (GG.Equip.ModuleID != moduleID)
            {
                AxHsms.AddListItem(repId, 0);
            }
            else
            {
                List<short> lstEoid = GG.Equip.EoidMgr.GetEoids();

                AxHsms.AddListItem(repId, lstEoid.Count);
                foreach (short eoid in lstEoid)
                {
                    List<EoidItem> lstEomd = GG.Equip.EoidMgr.GetEomds(eoid);
                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddU2Item(repId, eoid, 1);
                        AxHsms.AddListItem(repId, lstEomd.Count);
                        {
                            foreach (EoidItem eomd in lstEomd)
                            {
                                AxHsms.AddListItem(repId, 2);
                                {
                                    AxHsms.AddAsciiItem(repId, eomd.EOMD, 1); 
                                    AxHsms.AddU1Item(repId, eomd.EOV, 1);
                                }
                            }
                        }
                    }
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S1F6][SFCD=" + sfcd + "][moduleID=" + moduleID + "] Formatted State Data (Online Parameter)");
            else
                Logger.Append("[HSMS E→H][S1F6] failed.");
        }
        /// <summary>
        /// 메소드 - Formatted State Data _ SFCD = 3.Glass Tracking
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="sfcd"></param>
        /// <param name="moduleId"></param>
        public static void S1F6_GlassTracking(int msgID, short sfcd, string moduleId)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);

            if (GG.Equip.ModuleID != moduleId)
            {
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddAsciiItem(repId, moduleId, 28);
                    AxHsms.AddU1Item(repId, ref sfcd, 1);

                    AxHsms.AddListItem(repId, 1);
                    {
                        AxHsms.AddListItem(repId, 2);
                        {
                            AxHsms.AddAsciiItem(repId, moduleId, 22);
                            AxHsms.AddListItem(repId, 0);
                        }
                    }
                }
            }
            else
            {
                List<CellInfo> lstGlassInfo = GG.Equip.GetListGlassInfo();
                
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddAsciiItem(repId, moduleId, 28);
                    AxHsms.AddU1Item(repId, ref sfcd, 1);
                    AxHsms.AddListItem(repId, lstGlassInfo.Count);
                    {
                        foreach (CellInfo info in lstGlassInfo)
                        {
                            S1F6_GlassTracking_Sub(repId, info);
                        }
                    }
                }
            }
            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S1F6][SFCD=" + sfcd + "][moduleID=" + moduleId + " Formatted State Data (Glass Tracking)");
            else
                Logger.Append("[HSMS E→H][S1F6] failed.");
        }
        private static void S1F6_GlassTracking_Sub(int rpId, CellInfo g)
        {
            #region AddGlassToHSMS

            string HGlassID = g.HGlassID;
            string EGlassID = g.EGlassID;
            string LotID = g.LotID;

            string BatchID = g.BatchID;
            string JobID = g.JobID;
            string PortID = g.PortID;
            string SlotNo = g.SlotID;
            string ProductType = g.ProductType;
            string ProductKind = g.ProductKind;
            string ProductID = g.ProductID;
            string RunSpecID = g.RunSpecID;
            string LayerID = g.LayerID;
            string StepID = g.StepID;
            string PPID = g.PPID;
            string FlowID = g.FlowID;

            int[] GlassSize = g.GlassSize;
            int GlassThickness = g.GlassThickness;
            int GlassState = (int)g.PanelState;
            string GlassOrder = g.GlassOrder;
            string Comment = g.Comment;

            string UseCount = g.UseCount;
            string Judgement = g.Judgement;
            string ReasonCode = g.ReasonCode;
            string InspFlag = g.InsFlag;
            string EncFlag = g.EncFlag;
            string PrerunFlag = g.PrerunFlag;
            string TurnDir = g.TurnDir;
            string FlipState = g.FlipState;
            string WorkState = g.WorkState;
            string MultiUse = g.MultiUse;

            string PairGlassID = g.PairGlassID;
            string PairPPID = g.PairPPID;

            string OpetionName1 = g.OptionName1;
            string OptionValue1 = g.OptionValue1;
            string OpetionName2 = g.OptionName2;
            string OptionValue2 = g.OptionValue2;
            string OpetionName3 = g.OptionName3;
            string OptionValue3 = g.OptionValue3;
            string OpetionName4 = g.OptionName4;
            string OptionValue4 = g.OptionValue4;
            string OpetionName5 = g.OptionName5;
            string OptionValue5 = g.OptionValue5;
            #endregion

            AxHsms.AddListItem(rpId, 21);
            AxHsms.AddAsciiItem(rpId, HGlassID, 16);
            AxHsms.AddAsciiItem(rpId, EGlassID, 16);
            AxHsms.AddAsciiItem(rpId, LotID, 16);
            AxHsms.AddAsciiItem(rpId, BatchID, 16);
            AxHsms.AddAsciiItem(rpId, JobID, 16);
            AxHsms.AddAsciiItem(rpId, PortID, 4);
            AxHsms.AddAsciiItem(rpId, SlotNo, 2);
            AxHsms.AddAsciiItem(rpId, ProductType, 4);
            AxHsms.AddAsciiItem(rpId, ProductKind, 4);
            AxHsms.AddAsciiItem(rpId, ProductID, 16);
            AxHsms.AddAsciiItem(rpId, RunSpecID, 16);
            AxHsms.AddAsciiItem(rpId, LayerID, 8);
            AxHsms.AddAsciiItem(rpId, StepID, 8);
            AxHsms.AddAsciiItem(rpId, PPID, 20);
            AxHsms.AddAsciiItem(rpId, FlowID, 20);
            AxHsms.AddArrayItem(rpId, "U2", 2, string.Format("{0},{1}", GlassSize[0], GlassSize[1]));
            AxHsms.AddU2Item(rpId, ref GlassThickness, 1);
            AxHsms.AddU2Item(rpId, ref GlassState, 1);
            AxHsms.AddAsciiItem(rpId, GlassOrder, 4);
            AxHsms.AddAsciiItem(rpId, Comment, 16);

            AxHsms.AddListItem(rpId, 3);
            AxHsms.AddListItem(rpId, 10);
            AxHsms.AddAsciiItem(rpId, UseCount, 4);
            AxHsms.AddAsciiItem(rpId, Judgement, 4);
            AxHsms.AddAsciiItem(rpId, ReasonCode, 4);
            AxHsms.AddAsciiItem(rpId, InspFlag, 2);
            AxHsms.AddAsciiItem(rpId, EncFlag, 2);
            AxHsms.AddAsciiItem(rpId, PrerunFlag, 2);
            AxHsms.AddAsciiItem(rpId, TurnDir, 2);
            AxHsms.AddAsciiItem(rpId, FlipState, 2);
            AxHsms.AddAsciiItem(rpId, WorkState, 4);
            AxHsms.AddAsciiItem(rpId, MultiUse, 16);

            AxHsms.AddListItem(rpId, 2);
            AxHsms.AddAsciiItem(rpId, PairGlassID, 16);
            AxHsms.AddAsciiItem(rpId, PairPPID, 20);


            AxHsms.AddListItem(rpId, 5);
            AxHsms.AddListItem(rpId, 2);
            AxHsms.AddAsciiItem(rpId, OpetionName1, 40);
            AxHsms.AddAsciiItem(rpId, OptionValue1, 40);

            AxHsms.AddListItem(rpId, 2);
            AxHsms.AddAsciiItem(rpId, OpetionName2, 40);
            AxHsms.AddAsciiItem(rpId, OptionValue2, 40);

            AxHsms.AddListItem(rpId, 2);
            AxHsms.AddAsciiItem(rpId, OpetionName3, 40);
            AxHsms.AddAsciiItem(rpId, OptionValue3, 40);

            AxHsms.AddListItem(rpId, 2);
            AxHsms.AddAsciiItem(rpId, OpetionName4, 40);
            AxHsms.AddAsciiItem(rpId, OptionValue4, 40);

            AxHsms.AddListItem(rpId, 2);
            AxHsms.AddAsciiItem(rpId, OpetionName5, 40);
            AxHsms.AddAsciiItem(rpId, OptionValue5, 40);

           
        }
        /// <summary>
        /// 메소드 - Formatted State Data _ SFCD = 4.Module State
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="sfcd"></param>
        /// <param name="moduleID"></param>
        public static void S1F6_ModuleState(int msgID, short sfcd, string moduleID)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);

            AxHsms.AddListItem(repId, 2);
            AxHsms.AddU1Item(repId, ref sfcd, 1);

            if (moduleID == GG.Equip.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.ModuleID, 1, 8, GG.Equip.EquipState, GG.Equip.ProcState, GG.Equip.MCMD);
                S1F6_MoudleStateSub(repId, GG.Equip.UV01.ModuleID, 1, 0, GG.Equip.UV01.EquipState, GG.Equip.UV01.ProcState, GG.Equip.MCMD); 
                S1F6_MoudleStateSub(repId, GG.Equip.UV02.ModuleID, 1, 0, GG.Equip.UV02.EquipState, GG.Equip.UV02.ProcState, GG.Equip.MCMD);
                S1F6_MoudleStateSub(repId, GG.Equip.UV03.ModuleID, 1, 0, GG.Equip.UV03.EquipState, GG.Equip.UV03.ProcState, GG.Equip.MCMD);
                S1F6_MoudleStateSub(repId, GG.Equip.UV04.ModuleID, 1, 0, GG.Equip.UV04.EquipState, GG.Equip.UV04.ProcState, GG.Equip.MCMD);
                S1F6_MoudleStateSub(repId, GG.Equip.UV05.ModuleID, 1, 0, GG.Equip.UV05.EquipState, GG.Equip.UV05.ProcState, GG.Equip.MCMD);
                S1F6_MoudleStateSub(repId, GG.Equip.UV06.ModuleID, 1, 0, GG.Equip.UV06.EquipState, GG.Equip.UV06.ProcState, GG.Equip.MCMD);
                S1F6_MoudleStateSub(repId, GG.Equip.UV07.ModuleID, 1, 0, GG.Equip.UV07.EquipState, GG.Equip.UV07.ProcState, GG.Equip.MCMD);
                S1F6_MoudleStateSub(repId, GG.Equip.UV08.ModuleID, 1, 0, GG.Equip.UV08.EquipState, GG.Equip.UV08.ProcState, GG.Equip.MCMD);
            }
            else if (moduleID == GG.Equip.UV01.ModuleID)
            {
                AxHsms.AddListItem(repId, 1);
                S1F6_MoudleStateSub(repId, GG.Equip.UV01.ModuleID, 0, 0, GG.Equip.UV01.EquipState, GG.Equip.UV01.ProcState, GG.Equip.MCMD); 
            }
            else if (moduleID == GG.Equip.UV02.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.UV02.ModuleID, 0, 0, GG.Equip.UV02.EquipState, GG.Equip.UV02.ProcState, GG.Equip.MCMD);
            }
            else if (moduleID == GG.Equip.UV03.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.UV03.ModuleID, 0, 0, GG.Equip.UV03.EquipState, GG.Equip.UV03.ProcState, GG.Equip.MCMD);
            }
            else if (moduleID == GG.Equip.UV04.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.UV04.ModuleID, 0, 0, GG.Equip.UV04.EquipState, GG.Equip.UV04.ProcState, GG.Equip.MCMD);
            }
            else if (moduleID == GG.Equip.UV05.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.UV05.ModuleID, 0, 0, GG.Equip.UV05.EquipState, GG.Equip.UV05.ProcState, GG.Equip.MCMD);
            }
            else if (moduleID == GG.Equip.UV06.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.UV06.ModuleID, 0, 0, GG.Equip.UV06.EquipState, GG.Equip.UV06.ProcState, GG.Equip.MCMD);
            }
            else if (moduleID == GG.Equip.UV07.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.UV07.ModuleID, 0, 0, GG.Equip.UV07.EquipState, GG.Equip.UV07.ProcState, GG.Equip.MCMD);
            }
            else if (moduleID == GG.Equip.UV08.ModuleID)
            {
                S1F6_MoudleStateSub(repId, GG.Equip.UV08.ModuleID, 0, 0, GG.Equip.UV08.EquipState, GG.Equip.UV08.ProcState, GG.Equip.MCMD);
            }
            else  
            {
                AxHsms.AddListItem(repId, 0);
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S1F6][SFCD=" + sfcd + "][moduleID=" + moduleID + " Formatted State Data (Module States )");
            else
                Logger.Append("[HSMS E→H][S1F6] failed.");
        }
        private static void S1F6_MoudleStateSub(int repid, string moduleID, int layerNo, int nextLayerCount, EMEquipState emModuleState, EMProcState emModuleProcStae, EMMCmd emMoouleMCmd)
        {
            short moduleState = (short)emModuleState;
            short moduleProcStae = (short)emModuleProcStae;
            short moouleMCmd = (short)emMoouleMCmd;

            if (layerNo == 0)
            {
                AxHsms.AddListItem(repid, 5);
                {
                    AxHsms.AddAsciiItem(repid, moduleID, 28);
                    AxHsms.AddU1Item(repid, ref moduleState, 1);
                    AxHsms.AddU1Item(repid, ref moduleProcStae, 1);
                    AxHsms.AddU1Item(repid, ref moouleMCmd, 1);
                    AxHsms.AddListItem(repid, nextLayerCount);
                }
            }
            else if (layerNo == 4)
            {
                AxHsms.AddListItem(repid, 3);
                {
                    AxHsms.AddAsciiItem(repid, moduleID, 28);
                    AxHsms.AddU1Item(repid, ref moduleState, 1);
                    AxHsms.AddU1Item(repid, ref moduleProcStae, 1);
                }

            }
            else
            {
                AxHsms.AddListItem(repid, 4);
                {
                    AxHsms.AddAsciiItem(repid, moduleID, 28); 
                    AxHsms.AddU1Item(repid, ref moduleState, 1);
                    AxHsms.AddU1Item(repid, ref moduleProcStae, 1);
                    AxHsms.AddListItem(repid, nextLayerCount);
                }
            }
        }
        /// <summary>
        /// 메소드 - Formatted State Data _ Not Found
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="sfcd"></param>
        /// <param name="moduleID"></param>
        public static void S1F6_CfcdNotFound(int msgID, short sfcd, string moduleID)
        {
            short tmAck = 24; ;
            int repId = AxHsms.CreateReplyMsg(msgID);

            AxHsms.AddListItem(repId, 4);
            {
                AxHsms.AddAsciiItem(repId, moduleID, 22);
                AxHsms.AddU1Item(repId, sfcd, 1);
                AxHsms.AddU1Item(repId, tmAck, 1);
                AxHsms.AddListItem(repId, 0);
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S1F6][SFCD=" + sfcd + "][moduleID=" + moduleID + "  CFCD Not Found ");
            else
                Logger.Append("[HSMS E→H][S1F6] failed.");
        }

        /// <summary>
        /// 메소드 - ONLINE Acknowledgement
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="mcmd"></param>
        /// <param name="ack"></param>
        /// <param name="moduleId"></param>
        public static void S1F18(int msgID, short mcmd, short ack, string moduleId)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddListItem(repId, 2);
            {
                AxHsms.AddAsciiItem(repId, moduleId, 28); 
                AxHsms.AddU1Item(repId, ref mcmd, 1);
                AxHsms.AddU1Item(repId, ref ack, 1);
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S1F18][MCMD=" + mcmd + "][ACK=" + ack + "] ON_LINE Acknowledge");
            else
                Logger.Append("[HSMS E→H][S1F18] failed.");
        }

        #endregion

        #region S2

        /// <summary>
        /// 메소드 - New Equipment Constant Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="tmAck"></param>
        /// <param name="lstEcItem"></param>
        /// <param name="moduleId"></param>
        public static void S2F16(int msgID, short tmAck, List<EcidItem> lstEcItem, string moduleId)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddAsciiItem(repId, moduleId, 28);
                AxHsms.AddU1Item(repId, ref tmAck, 1);
                AxHsms.AddListItem(repId, lstEcItem.Count);

                EcidItem TEAC;
                foreach (EcidItem item in lstEcItem)
                {
                    AxHsms.AddListItem(repId, 8);
                    TEAC = GG.Equip.EcIdMgr[lstEcItem.Count];
                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddU2Item(repId, item.EcID, 1);
                        AxHsms.AddU1Item(repId, item.EcID_PMACK, 1);
                    }

                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddAsciiItem(repId, item.EcName, 40);
                        AxHsms.AddU1Item(repId, item.EcName_PMACK, 1);
                    }

                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddAsciiItem(repId, item.EcDefine, 20);
                        AxHsms.AddU1Item(repId, item.EcDefine_PMACK, 1);
                    }

                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddAsciiItem(repId, item.EcSLL, 20);
                        AxHsms.AddU1Item(repId, item.EcSLL_PMACK, 1);
                    }

                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddAsciiItem(repId, item.EcSUL, 20);
                        AxHsms.AddU1Item(repId, item.EcSUL_PMACK, 1);
                    }

                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddAsciiItem(repId, item.EcWLL, 20);
                        AxHsms.AddU1Item(repId, item.EcWLL_PMACK, 1);
                    }

                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddAsciiItem(repId, item.EcWUL, 20);
                        AxHsms.AddU1Item(repId, item.EcWUL_PMACK, 1);
                    }
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S2F16][EC COUNT =" + GG.Equip.EcIdMgr.Count + "][ACK=" + tmAck + "]  New Equipment Constant Acknowledge");
            else
                Logger.Append("[HSMS E→H][S2F16] failed.");

        }

        /// <summary>
        /// 메소드 - Trace Initialize Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="tmAck"></param>
        public static void S2F24(int msgID, short tmAck)
        {
            int msgid = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddU1Item(msgid, ref tmAck, 1);

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("[HSMS E→H][S2F24] TIACK={0}", tmAck));
            else
                Logger.Append(string.Format("[HSMS E→H][S2F24] TIACK={0} Failed.", tmAck));
        }
        
        /// <summary>
        /// 메소드 - Equipment Constant Namelist
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleId"></param>
        /// <param name="lstReqEcItems"></param>
        /// <param name="findNonEcid"></param>
        public static void S2F30(int msgID, string moduleId, List<int> lstReqEcItems, bool findNonEcid)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);
            if (findNonEcid)
            {
                AxHsms.AddListItem(repId, 1);
                AxHsms.AddAsciiItem(repId, moduleId, 28);
                AxHsms.AddListItem(repId, 0);
            }
            else if (moduleId != GG.Equip.ModuleID)
            {
                AxHsms.AddListItem(repId, 2);
                AxHsms.AddAsciiItem(repId, moduleId, 28);
                AxHsms.AddListItem(repId, 0);
            }
            else
            {
                List<EcidItem> lstSendEcitems = GG.Equip.EcIdMgr.Where(f => lstReqEcItems.Count == 0 || lstReqEcItems.Contains(f.Key)).Select(g => g.Value).ToList();
                
                AxHsms.AddListItem(repId, 2);
                {
                    AxHsms.AddAsciiItem(repId, moduleId, 28);
                    AxHsms.AddListItem(repId, lstSendEcitems.Count);  

                    foreach (EcidItem item in lstSendEcitems)
                    {
                        AxHsms.AddListItem(repId, 7);
                        {
                            AxHsms.AddU2Item(repId, item.EcID, 1);
                            AxHsms.AddAsciiItem(repId, item.EcName, 40);
                            AxHsms.AddAsciiItem(repId, item.EcDefine, 40);
                            AxHsms.AddAsciiItem(repId, item.EcSLL, 40);
                            AxHsms.AddAsciiItem(repId, item.EcSUL, 40);
                            AxHsms.AddAsciiItem(repId, item.EcWLL, 40);
                            AxHsms.AddAsciiItem(repId, item.EcWUL, 40);
                        }
                    }
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S2F30][ECID COUND =" + lstReqEcItems.Count + "][ACK=" + " Equipment Constant Namelist ");
            else
                Logger.Append("[HSMS E→H][S2F30] failed.");
        }

        /// <summary>
        /// 메소드 - Date and Time Set Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="datetime"></param>
        /// <param name="ack"></param>
        public static void S2F32(int msgID, string datetime, short ack)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddU1Item(repId, ref ack, 1);

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S2F32][TIME=" + datetime + "][ACK=" + ack + " Date and Time Set Acknowledge");
            else
                Logger.Append("[HSMS E→H][S2F32] failed.");
        }

        /// <summary>
        /// 오정석 - Host Command Acknowledge _ RCMD = 51, 52, 53, 54
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="rcmd"></param>
        /// <param name="s2f41_equipCmd"></param>
        public static void S2F42_EquipCommand(int msgID, short rcmd, S2F41_EQUIP_COMMAND_INFO s2f41_equipCmd)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);

            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddU1Item(repId, ref rcmd, 1);
                AxHsms.AddU1Item(repId, ref s2f41_equipCmd.TMACK, 1);
                AxHsms.AddListItem(repId, s2f41_equipCmd.MODULE_COUNT);
                {
                    for (int iPos = 0; iPos < s2f41_equipCmd.MODULE_COUNT; iPos++)
                    {
                        AxHsms.AddListItem(repId, 3);
                        {
                            AxHsms.AddAsciiItem(repId, "MODULEID", 10);
                            AxHsms.AddAsciiItem(repId, s2f41_equipCmd.MODULE_ARR[iPos], 28);
                            AxHsms.AddBinaryItem(repId, ref s2f41_equipCmd.MODULE_PMACK_ARR[iPos], 1);
                        }
                    }
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S2F42][RCMD=" + rcmd + "][HCACK=" + s2f41_equipCmd.TMACK + "][CPACK=" + s2f41_equipCmd.PMACK + "] Host Command Reply (Equipment Command)");
            else
                Logger.Append("[HSMS E→H][S2F42] Equipment Command  failed.");
        }
        /// <summary>
        /// 오정석 - Host Command Acknowledge _ RCMD = 201, 202
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="rcmd"></param>
        /// <param name="s2f41_equipCmd"></param>
        public static void S2F42_CellLotProcessCommandReply(int msgid, int rcmd, S2F41_PROCESS_COMMAND_INFO s2f41_ProCmd)
        {
            int repId = AxHsms.CreateReplyMsg(msgid);

            AxHsms.AddListItem(repId, 3);
            AxHsms.AddU2Item(repId, ref rcmd, 1);
            AxHsms.AddU1Item(repId, ref s2f41_ProCmd.HCACK, 1);

            AxHsms.AddListItem(repId, 5);
            {
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddAsciiItem(repId, "MODULEID", 10);
                    AxHsms.AddAsciiItem(repId, s2f41_ProCmd.ModuleId, 23);
                    AxHsms.AddU1Item(repId, ref s2f41_ProCmd.JobidCpack, 1);
                }
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddAsciiItem(repId, "LOT_ID", 10);
                    AxHsms.AddAsciiItem(repId, s2f41_ProCmd.LotId, 20);
                    AxHsms.AddU1Item(repId, ref s2f41_ProCmd.LotIdCpack, 1);
                }
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddAsciiItem(repId, "MODEL", 10);
                    AxHsms.AddAsciiItem(repId, s2f41_ProCmd.Model, 20);
                    AxHsms.AddU1Item(repId, ref s2f41_ProCmd.ModelCpack, 1);
                }
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddAsciiItem(repId, "PPID", 10);
                    AxHsms.AddAsciiItem(repId, s2f41_ProCmd.PPID, 20);
                    AxHsms.AddU1Item(repId, ref s2f41_ProCmd.PPIDCPack, 1);
                }
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddAsciiItem(repId, "CSTID", 10);
                    AxHsms.AddAsciiItem(repId, s2f41_ProCmd.CstId, 20);
                    AxHsms.AddU1Item(repId, ref s2f41_ProCmd.CstIdCpack, 1);
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S2F42][RCMD=" + rcmd + "][HCACK=" + s2f41_ProCmd.TMACK + "][CPACK=" + s2f41_ProCmd.PMACK + "] Host Command Reply (Equipment Command)");
            else
                Logger.Append("[HSMS E→H][S2F42] Equipment Command  failed.");
        }

        /// <summary>
        /// 메소드 - Operator Call Reply
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="tid"></param>
        /// <param name="moduleid"></param>
        /// <param name="msgid"></param>
        /// <param name="ack"></param>
        public static void S2F102(int msgID, short tid, string moduleid, string msgid, short ack)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);

            AxHsms.AddU1Item(repId, ref ack, 1);

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S2F102][TID=" + tid + "][MODULEID=" + moduleid + "] [MSG=" + msgID + "][ACK =" + ack + "] Operator Call");
            else
                Logger.Append("[HSMS E→H][S2F102] Operator Call.");
        }

        /// <summary>
        /// 메소드 - Equipment Online Parameter Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleid"></param>
        /// <param name="miack"></param>
        /// <param name="s2f103EoidInfos"></param>
        public static void S2F104(int msgID, string moduleid, short miack, List<EoidItem> s2f103EoidInfos)
        {
            short[] arrEoid = s2f103EoidInfos.Select(f => f.EOID).Distinct().ToArray();

            int repId = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddAsciiItem(repId, moduleid, 28);
                AxHsms.AddU1Item(repId, miack, 1);
                AxHsms.AddListItem(repId, arrEoid.Length);
                {
                    foreach (int eoid in arrEoid)
                    {
                        EoidItem[] eomdlist = s2f103EoidInfos.Where(f => f.EOID == eoid).ToArray();

                        AxHsms.AddListItem(repId, 2);
                        AxHsms.AddU2Item(repId, eoid, 1);
                        AxHsms.AddListItem(repId, eomdlist.Length);

                        foreach (EoidItem info in eomdlist)
                        {
                            AxHsms.AddListItem(repId, 2);
                            AxHsms.AddAsciiItem(repId, info.EOMD, 4);
                            AxHsms.AddU1Item(repId, info.EAC, 1);
                        }
                    }
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S2F104][MODULEID=" + moduleid + "][MIACK=" + miack + "] Equip Onlin Parameter Change");
            else
                Logger.Append("[HSMS E→H][S2F104] Equipment Command  failed.");
        }

        #endregion

        #region S3

        /// <summary>
        /// 메소드 - CELL Information Reply
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="ackc3"></param>
        public static void S3F216(int msgID, short ackc3)
        {
            int msgid = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddU1Item(msgid, ref ackc3, 1);

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("[HSMS E→H][S3F216] ACKC3={0}", ackc3));
            else
                Logger.Append(string.Format("[HSMS E→H][S3F216] ACKC3={0} Failed.", ackc3));
        }

        #endregion

        #region S5

        /// <summary>
        /// 메소드 - Alarm Report Send
        /// </summary>
        /// <param name="occur"></param>
        /// <param name="mdouleID"></param>
        /// <param name="alarmCode"></param>
        /// <param name="alarmID"></param>
        /// <param name="alarmText"></param>
        /// <param name="alarmTime"></param>
        /// <param name="alarmWarningHappen"></param>
        public static void S5F1(short occur, string mdouleID, short alarmCode, double alarmID, string alarmText, string alarmTime, string alarmWarningHappen)
        {
            int repId = AxHsms.CreateMsg(5, 1, 1);
            AxHsms.AddListItem(repId, 2);
            {
                AxHsms.AddU1Item(repId, ref occur, 1);
                AxHsms.AddListItem(repId, 4);
                {
                    short code = alarmCode;

                    if (occur == 1)
                    {
                        if (alarmWarningHappen == "1")
                            code |= 128;
                        else if (alarmWarningHappen == "0")
                            code |= 64;
                    }

                    AxHsms.AddAsciiItem(repId, mdouleID, 28);
                    AxHsms.AddU1Item(repId, ref code, 1);
                    AxHsms.AddU4Item(repId, ref alarmID, 1);
                    AxHsms.AddAsciiItem(repId, alarmText, 100);

                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S5F1][SETCODE=" + occur + "][MODULEID=" + mdouleID + "[ALARMCODE=" + alarmCode + "]  [ALARMID=" + alarmID + "] [ALARMTEXT=" + alarmText + "]  Alarm Report Send ");
            else
                Logger.Append("[HSMS E→H][S5F1] Alarm Report Send failed.");
        }

        /// <summary>
        /// 메소드 - Acknowledge to Alarm List Request
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleid"></param>
        /// <param name="lstReqAlarm"></param>
        public static void S5F6(int msgID, string moduleid, List<double> lstReqAlarm)
        {

            int msgid = AxHsms.CreateReplyMsg(msgID);
            if (moduleid != GG.Equip.ModuleID)
            {
                AxHsms.AddListItem(msgid, 0);
            }
            else
            {
                AxHsms.AddAsciiItem(msgid, moduleid, 28); 
                foreach (AlarmInfo info in GG.Equip.AlarmInfoMgr.Values)
                {
                    if (lstReqAlarm.Count != 0 && lstReqAlarm.Contains(info.ID) == false)
                        continue;

                    short vALCD = info.Code;
                    double vALID = info.ID;
                    string vALTX = info.Text;
                    string vMODULEID = info.ModuleID;

                    AxHsms.AddListItem(msgid, 4);
                    {
                        AxHsms.AddAsciiItem(msgid, vMODULEID, 28); 
                        AxHsms.AddU1Item(msgid, ref vALCD, 1);
                        AxHsms.AddU4Item(msgid, ref vALID, 1);
                        AxHsms.AddAsciiItem(msgid, vALTX, 100);
                    }
                }
            }

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("Tx S5F5, AAcknowledge to Alarm List Request"));
            else
                Logger.Append(string.Format("Tx S5F5, Failed."));
        }

        /// <summary>
        /// 메소드 - Waiting Reset Alarms List Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleid"></param>
        public static void S5F102(int msgID, string moduleid)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);
            if (moduleid != GG.Equip.ModuleID)
            {
                AxHsms.AddListItem(repId, 0);
            }
            else
            {

                AxHsms.AddListItem(repId, GG.Equip.AlarmInfosOccur.Count);
                foreach (AlarmInfo info in GG.Equip.AlarmInfosOccur)
                {
                    AxHsms.AddListItem(repId, 4);
                    {
                        short code = info.Code;
                        code |= 128;

                        AxHsms.AddAsciiItem(repId, info.ModuleID, 28);
                        AxHsms.AddU1Item(repId, ref code, 1);
                        AxHsms.AddU4Item(repId, info.ID, 1);
                        AxHsms.AddAsciiItem(repId, info.Text, 100);
                    }
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append(string.Format("[HSMS E→H][S5F102], Alarm lists which wait for reset."));
            else
                Logger.Append(string.Format("[HSMS E→H][S5F102], Failed."));
        }

        #endregion

        #region S6

        /// <summary>
        /// 메소드 - Trace Data Send
        /// </summary>
        /// <param name="tdItem"></param>
        public static void S6F1(TdItem tdItem)
        {
            int msgid = AxHsms.CreateMsg(6, 1, 0);

            AxHsms.AddListItem(msgid, 2);
            AxHsms.AddAsciiItem(msgid, GG.Equip.ModuleID, 28);

            AxHsms.AddListItem(msgid, 3);
            AxHsms.AddU2Item(msgid, tdItem.TrID, 1);
            AxHsms.AddU2Item(msgid, tdItem.SmpLn, 1);
            AxHsms.AddListItem(msgid, tdItem.GroupSize);

            foreach (SvSmpItems svSmpItem in tdItem)
            {
                AxHsms.AddListItem(msgid, 2);
                AxHsms.AddAsciiItem(msgid, svSmpItem.SCTime, 16);

                AxHsms.AddListItem(msgid, svSmpItem.Count);
                for (int i = 0; i < svSmpItem.Count; ++i)
                {
                    SvItem item = svSmpItem[i];
                    AxHsms.AddListItem(msgid, 3);
                    AxHsms.AddU2Item(msgid, item.SvID, 1);
                    AxHsms.AddAsciiItem(msgid, item.SvValue, 40);
                    AxHsms.AddAsciiItem(msgid, item.SvName, 40);
                }
            }

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("Tx S6F1, Trace Data Send."));
            else
                Logger.Append(string.Format("Tx S6F1, Failed."));
        }
        
        public static void S6F11_PanelProcess(int ceid, ModuleInfo moduleInfo, CellInfo glass, EMByWho eMByWho)
        {
            short DATAID = 0;
            int msgid = AxHsms.CreateMsg(6, 11, 1);
            AxHsms.AddListItem(msgid, 3);
            {
                AxHsms.AddU1Item(msgid, ref DATAID, 1);
                AxHsms.AddU2Item(msgid, ref ceid, 1);
                AxHsms.AddListItem(msgid, 2);
                {
                    S6F11_SUB_0(msgid, ceid, moduleInfo, (short)eMByWho);
                    S6F11_PanelProcess_SUB(msgid, glass);
                }
            }

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("Tx S6F11, MODULE CEID={0}", ceid));
            else
                Logger.Append(string.Format("Tx S6F11, MODULE CEID={0} failed.", ceid));
        }
        private static void S6F11_PanelProcess_SUB(int msgID, CellInfo glass)
        {
            short rptId = 3;
            AxHsms.AddListItem(msgID, 2);
            {
                AxHsms.AddU1Item(msgID, rptId, 1);
                AxHsms.AddListItem(msgID, 1);
                {
                    AxHsms.AddListItem(msgID, 14);
                    {
                        AxHsms.AddAsciiItem(msgID, glass.HGlassID, 12);
                        AxHsms.AddAsciiItem(msgID, glass.EGlassID, 12);
                        AxHsms.AddAsciiItem(msgID, glass.BatchID, 12);
                        AxHsms.AddAsciiItem(msgID, "", 2);

                        AxHsms.AddAsciiItem(msgID, glass.ProductType, 2);
                        AxHsms.AddAsciiItem(msgID, glass.ProductKind, 2);
                        AxHsms.AddAsciiItem(msgID, glass.DeviceID, 18);
                        AxHsms.AddAsciiItem(msgID, glass.StepID, 10);
                        AxHsms.AddAsciiItem(msgID, glass.PPID, 16);
                        AxHsms.AddU2Item(msgID, glass.GlassThickness, 1);
                        AxHsms.AddAsciiItem(msgID, glass.InsFlag, 2);

                        AxHsms.AddListItem(msgID, 1);
                        {
                            AxHsms.AddListItem(msgID, 12);
                            {
                                AxHsms.AddArrayItem(msgID, "U2", 2, string.Format("{0},{1}", glass.PanelSize[0], glass.PanelSize[1]));
                                AxHsms.AddAsciiItem(msgID, glass.PanelPosition, 2);
                                AxHsms.AddAsciiItem(msgID, glass.Count1, 2);
                                AxHsms.AddAsciiItem(msgID, glass.Count2, 2);
                                AxHsms.AddAsciiItem(msgID, glass.Grade, 48);
                                AxHsms.AddU1Item(msgID, 0, 1);
                                AxHsms.AddU1Item(msgID, 0, 1);
                                AxHsms.AddAsciiItem(msgID, glass.Comment, 16);
                                AxHsms.AddU1Item(msgID, 0, 1);
                                AxHsms.AddU1Item(msgID, glass.CompCount, 1);
                                AxHsms.AddU1Item(msgID, 0, 1);
                                AxHsms.AddAsciiItem(msgID, "", 2);
                            }
                        }
                        AxHsms.AddListItem(msgID, 1);
                        {
                            AxHsms.AddListItem(msgID, 5);
                            {
                                string runLine = string.Join(",", glass.RunLine);
                                string uniqueId = string.Join(",", glass.UniqueID);

                                AxHsms.AddU1Item(msgID, ref glass.PanelState, 1);
                                AxHsms.AddAsciiItem(msgID, glass.Judgement, 4);
                                AxHsms.AddAsciiItem(msgID, glass.Code, 4);
                                AxHsms.AddArrayItem(msgID, "U1", 20, runLine);
                                AxHsms.AddArrayItem(msgID, "U1", 4, uniqueId);
                            }
                        }
                        AxHsms.AddListItem(msgID, 1);
                        {
                            AxHsms.AddListItem(msgID, 6);
                            {
                                AxHsms.AddAsciiItem(msgID, glass.Pair_H_PanelID, 12);
                                AxHsms.AddAsciiItem(msgID, glass.Pair_E_PanelID, 12);
                                AxHsms.AddU1Item(msgID, glass.Pair_Grade, 1);
                                string uniqueId = string.Join(",", glass.Pair_UniqueID);
                                AxHsms.AddArrayItem(msgID, "U1", 4, uniqueId);
                                AxHsms.AddAsciiItem(msgID, "", 16);
                                AxHsms.AddAsciiItem(msgID, "", 18);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 메소드 - Event Report Send _ Related Equipment Event (CEID = 51, 52, 73)
        /// </summary>
        /// <param name="ceid"></param>
        /// <param name="moduleID"></param>
        /// <param name="mCmd"></param>
        /// <param name="mCmdOld"></param>
        /// <param name="eqpState"></param>
        /// <param name="eqpStateOld"></param>
        /// <param name="procState"></param>
        /// <param name="procStateOld"></param>
        /// <param name="reasonCode"></param>
        /// <param name="byWho"></param>
        public static void S6F11_EquipEvent(int ceid, string moduleID, EMMCmd mCmd, EMMCmd mCmdOld,
            EMEquipState eqpState, EMEquipState eqpStateOld, EMProcState procState, EMProcState procStateOld, string reasonCode, EMByWho byWho)
        {
            string OperID = GG.Equip.OperatorID;
            short dataId = 0;
            short rptid = 0;
            int repId = AxHsms.CreateMsg(6, 11, 1);
            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddU1Item(repId, dataId, 1);
                AxHsms.AddU2Item(repId, ceid, 1);
                AxHsms.AddListItem(repId, 3);
                {
                    AxHsms.AddListItem(repId, 2);
                    {
                        rptid = 1;
                        AxHsms.AddU1Item(repId, (short)rptid, 1);
                        AxHsms.AddListItem(repId, 6);
                        {
                            AxHsms.AddAsciiItem(repId, moduleID, 28);
                            AxHsms.AddU1Item(repId, (short)mCmd, 1);
                            AxHsms.AddU1Item(repId, (short)eqpState, 1);
                            AxHsms.AddU1Item(repId, (short)procState, 1);
                            AxHsms.AddU1Item(repId, (short)byWho, 1);
                            AxHsms.AddAsciiItem(repId, OperID, 16);
                        }

                    }
                    AxHsms.AddListItem(repId, 2);
                    {
                        rptid = 4;
                        string limitTime = string.Empty;

                        AxHsms.AddU1Item(repId, (short)rptid, 1);
                        AxHsms.AddListItem(repId, 2);
                        {
                            short oldState = ceid == 51 ? (short)procStateOld :
                                             ceid == 53 ? (short)eqpStateOld :
                                             ceid == 52 ? (short)0 : (short)mCmdOld;

                            short newState = ceid == 51 ? (short)procState :
                                             ceid == 53 ? (short)eqpState :
                                             ceid == 52 ? (short)0 : (short)mCmd;


                            AxHsms.AddU1Item(repId, oldState, 1);
                            AxHsms.AddU1Item(repId, newState, 1);
                        }
                    }
                    S6F11_EquipEvent_SUB(ceid, eqpState, repId);
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S6F11][MCMD=" + GG.Equip.MCMD + "][moduleID=" + moduleID + " Formatted State Data (Module States )");
            else
                Logger.Append("[HSMS E→H][S6F11], failed.");
        }
        static short Old_alCD = 0;
        static double Old_alID = 0;
        static string Old_alTX = string.Empty;
        static string Old_alModuleID = string.Empty;
        static short AlarmCode = 0;
        private static void S6F11_EquipEvent_SUB(int ceid, EMEquipState eqpState, int repId)
        {
            short alCD = 0;
            double alID = 0;
            string alTX = string.Empty;
            string alModuleID = string.Empty;
            string alTM = string.Empty;
            short rptId = 8;

            if (eqpState == EMEquipState.Fault)
            {
                AlarmInfo alarmInfo = GG.Equip.AlarmInfosOccur.LastOrDefault(f => (f.Code & 64) > 0);
                AlarmCode = alarmInfo.Code;

                if (alarmInfo.AlarmWarningHappen == "0")
                    alarmInfo.Code |= 64;
                else if (alarmInfo.AlarmWarningHappen == "1")
                    alarmInfo.Code |= 128;
                alCD = alarmInfo.Code;

                alID = alarmInfo == null ? 0 : alarmInfo.ID;
                alTX = alarmInfo == null ? string.Empty : alarmInfo.Text;
                alTM = alarmInfo == null ? string.Empty : alarmInfo.Time;
                alModuleID = alarmInfo == null ? string.Empty : alarmInfo.ModuleID;

                Old_alCD = AlarmCode;
                Old_alID = alID;
                Old_alTX = alTX;
                Old_alModuleID = alModuleID;
            }
            else if (eqpState == EMEquipState.Normal)
            {
                alCD = Old_alCD;
                alID = Old_alID;
                alTX = Old_alTX;
                alModuleID = Old_alModuleID;
            }

            AxHsms.AddListItem(repId, 2);
            {
                AxHsms.AddU1Item(repId, rptId, 1);
                AxHsms.AddListItem(repId, 4);
                {
                    AxHsms.AddAsciiItem(repId, alModuleID, 28);
                    AxHsms.AddU1Item(repId, ref alCD, 1);
                    AxHsms.AddU4Item(repId, ref alID, 1);
                    AxHsms.AddAsciiItem(repId, alTX, 100);
                }
            }
        }
        /// <summary>
        /// 메소드 - Event Report Send _ Related Equipment Parameter Event (CEID = 101, 102)
        /// </summary>
        /// <param name="ceid"></param>
        /// <param name="moduleInfo"></param>
        /// <param name="lstEoid"></param>
        /// <param name="lstEcid"></param>
        /// <param name="byWho"></param>
        public static void S6F11_EquipParamterEvent(int ceid, ModuleInfo moduleInfo, List<EoidItem> lstEoid, List<EcidItem> lstEcid, EMByWho byWho)
        {
            int repId = AxHsms.CreateMsg(6, 11, 1);

            AxHsms.AddListItem(repId, 3);
            {
                short DATAID = 0;

                AxHsms.AddU1Item(repId, ref DATAID, 1);
                AxHsms.AddU2Item(repId, ref ceid, 1);
                AxHsms.AddListItem(repId, 3);
                {
                    S6F11_SUB_0(repId, ceid, moduleInfo, (short)byWho);
                    S6F11_EquipParamterEvent_SUB1(repId, lstEoid);
                    S6F11_EquipParamterEvent_SUB2(repId, lstEcid);
                }
            }

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append(string.Format("Tx S6F11, CEID={0} EOID Change", ceid));
            else
                Logger.Append(string.Format("Tx S6F11, CEID={0} failed.", ceid));

        }
        private static void S6F11_EquipParamterEvent_SUB1(int msgID, List<EoidItem> lstChengedEoid)
        {
            short rptID = 5;

            AxHsms.AddListItem(msgID, 2);
            {
                AxHsms.AddU1Item(msgID, ref rptID, 1);
                AxHsms.AddListItem(msgID, lstChengedEoid.Select(f => f.EOID).Distinct().Count());
                {
                    foreach (short eoid in lstChengedEoid.Select(f => f.EOID).Distinct())
                    {
                        List<EoidItem> lstEomd = lstChengedEoid.Where(f => f.EOID == eoid).ToList();
                        AxHsms.AddListItem(msgID, 2);
                        {
                            AxHsms.AddU1Item(msgID, eoid, 1);
                            AxHsms.AddListItem(msgID, lstEomd.Count);
                            {
                                foreach (EoidItem item in lstEomd)
                                {
                                    AxHsms.AddListItem(msgID, 2);
                                    {
                                        AxHsms.AddAsciiItem(msgID, item.EOMD, 4);
                                        AxHsms.AddU1Item(msgID, item.EOV, 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private static void S6F11_EquipParamterEvent_SUB2(int msgID, List<EcidItem> lstChengedEcid)
        {
            short rptID = 6;
            List<int> lstEcid = GG.Equip.EcIdMgr.Where(f => lstChengedEcid.Exists((e) => { return e.EcID == f.Key; }))
                                                  .Select(t => t.Key)
                                                  .ToList();
            AxHsms.AddListItem(msgID, 2);
            {
                AxHsms.AddU1Item(msgID, ref rptID, 1);
                AxHsms.AddListItem(msgID, lstEcid.Count);
                {
                    foreach (short ecid in lstEcid)
                    {
                        EcidItem ecidItem = GG.Equip.EcIdMgr[ecid];
                        AxHsms.AddListItem(msgID, 7);
                        {
                            AxHsms.AddU2Item(msgID, ecidItem.EcID, 1);
                            AxHsms.AddAsciiItem(msgID, ecidItem.EcName, 40);
                            AxHsms.AddAsciiItem(msgID, ecidItem.EcDefine, 40);
                            AxHsms.AddAsciiItem(msgID, ecidItem.EcSLL, 40);
                            AxHsms.AddAsciiItem(msgID, ecidItem.EcSUL, 40);
                            AxHsms.AddAsciiItem(msgID, ecidItem.EcWLL, 40);
                            AxHsms.AddAsciiItem(msgID, ecidItem.EcWUL, 40);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 메소드 - Event Report Send _ Equipment Specified Control Event (CEID = 131, 171)
        /// </summary>
        /// <param name="ceid"></param>
        /// <param name="moduleInfo"></param>
        /// <param name="ayItemName"></param>
        /// <param name="ayItemValue"></param>
        /// <param name="byWho"></param>
        public static void S6F11_EquipSpecControlEvent(int ceid, ModuleInfo moduleInfo, string[] ayItemName, string[] ayItemValue, EMByWho byWho)
        {
            short dataId = 0;
            int repId = AxHsms.CreateMsg(6, 11, 1);
            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddU1Item(repId, ref dataId, 1);
                AxHsms.AddU2Item(repId, ref ceid, 1);
                AxHsms.AddListItem(repId, 2);
                {
                    S6F11_SUB_0(repId, ceid, moduleInfo, (short)byWho);
                    S6F11_EquipSpecControlEvent_SUB(repId, ceid, ayItemName, ayItemValue);
                }
            }


            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append(string.Format("Tx S6F11, CEID={0} OLDPPID={1} NEWPPID={2} Inline Flow Event_Specific Control", ceid, ayItemValue[0], ayItemValue[1]));
            else
                Logger.Append(string.Format("Tx S6F11, CEID={0} failed.", ceid));
        }
        private static void S6F11_EquipSpecControlEvent_SUB(int msgID, int nCEID, string[] ayItemName, string[] ayItemValue)
        {
            short vRPTID = 7;
            int nCount = ayItemName.Length;

            AxHsms.AddListItem(msgID, 2);
            {
                AxHsms.AddU1Item(msgID, ref vRPTID, 1);
                AxHsms.AddListItem(msgID, nCount);
                for (int i = 0; i < nCount; ++i)
                {
                    AxHsms.AddListItem(msgID, 2);
                    {
                        AxHsms.AddAsciiItem(msgID, ayItemName[i], 40);
                        AxHsms.AddAsciiItem(msgID, ayItemValue[i], 40);
                    }
                }
            }

        }
        /// <summary>
        /// 메소드 - Event Report Send _ Related CELL JOB Process Event (CEID = 2001, 2002, 2003, 2004, 2006, 2007, 2111)
        /// </summary>
        /// <param name="ceid"></param>
        /// <param name="cinfo"></param>
        public static void S6F11_RelatedCELLJOBProcessEvent(int ceid, CassetteInfo cinfo)
        {
            short dataId = 0;

            int msgid = AxHsms.CreateMsg(6, 11, 1);

            AxHsms.AddListItem(msgid, 3);
            {
                AxHsms.AddU1Item(msgid, dataId, 1);
                AxHsms.AddU2Item(msgid, ceid, 1);
                AxHsms.AddListItem(msgid, 4);
                {
                    S6F11_Sub_1(msgid, ceid, cinfo);
                    S6F11_Sub_3(ceid, cinfo);
                    S6F11_Sub_4(ceid, cinfo);
                    S6F11_Sub_2(msgid, cinfo);
                }
            }
        }
        /// <summary>
        /// 메소드 - Event Report Send _ Related CELL Process Event (CEID = 2010, 2016, 2017, 2018, 2019)
        /// </summary>
        /// <param name="ceid"></param>
        /// <param name="cinfo"></param>
        public static void S6F11_RelatedCELLProcessEvent(int ceid, CassetteInfo cinfo)
        {
            short dataId = 0;

            int repId = AxHsms.CreateMsg(6, 11, 1);

            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddU1Item(repId, dataId, 1);
                AxHsms.AddU2Item(repId, ceid, 1);

                AxHsms.AddListItem(repId, 2);
                {
                    S6F11_Sub_1(repId, ceid, cinfo);
                    S6F11_Sub_2(repId, cinfo);
                }

            }
        }
        /// <summary>
        /// 메소드 - Event Report Send _ Related Load Event (CEID = 2031, 2032, 2033, 2034, 2035)
        /// </summary>
        /// <param name="ceid"></param>
        /// <param name="cinfo"></param>
        public static void S6F11_RelatedLoadEvent(int ceid, CassetteInfo cinfo)
        {
            short dataId = 0;

            int repId = AxHsms.CreateMsg(6, 11, 1);

            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddU1Item(repId, dataId, 1);
                AxHsms.AddU2Item(repId, ceid, 1);
                AxHsms.AddListItem(repId, 3);
                {
                    S6F11_Sub_1(repId, ceid, cinfo);
                    S6F11_Sub_3(repId, cinfo);

                    AxHsms.AddListItem(repId, 2);
                    {
                        short rptid = 1;
                        AxHsms.AddU1Item(repId, rptid, 1);

                        int count = cinfo.Cells.Count();
                        for (int i = 0; i < count; i++)
                        {
                            AxHsms.AddListItem(repId, 3);
                            {
                                AxHsms.AddAsciiItem(repId, cinfo.LotID, 20);
                                AxHsms.AddAsciiItem(repId, cinfo.CstID, 20);
                                AxHsms.AddAsciiItem(repId, cinfo.Cells[i].CellId, 20);
                            }
                        }

                    }

                }
            }
        }
        /// <summary>
        /// 메소드 - Event Report Send _ Related Cell ID Reading Event (CEID = 2111)
        /// </summary>
        /// <param name="ceid"></param>
        /// <param name="cinfo"></param>
        public static void S6F11_RelatedCellIdReadingEvent(int ceid, CassetteInfo cinfo)
        {
            short dataId = 0;

            int repId = AxHsms.CreateMsg(6, 11, 1);

            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddU1Item(repId, dataId, 1);
                AxHsms.AddU2Item(repId, ceid, 1);
                AxHsms.AddListItem(repId, 2);
                {
                    S6F11_Sub_1(repId, ceid, cinfo);
                    AxHsms.AddListItem(repId, 2);
                    {
                        AxHsms.AddArrayItem(repId, "STRING", 20, cinfo.Cells[0].CellId);
                        AxHsms.AddArrayItem(repId, "STRING", 5, cinfo.Cells[0].VcrResult);
                    }
                }
            }
        }
        private static void S6F11_SUB_0(int msgID, int ceid, ModuleInfo moduleInfo, short byWho)
        {
            short rptid = 1;
            AxHsms.AddListItem(msgID, 2);
            {
                AxHsms.AddU1Item(msgID, rptid, 1);
                AxHsms.AddListItem(msgID, 6);
                {
                    AxHsms.AddAsciiItem(msgID, moduleInfo.ModuleID, 28);
                    AxHsms.AddU1Item(msgID, (short)GG.Equip.MCMD, 1);
                    AxHsms.AddU1Item(msgID, (short)moduleInfo.EquipState, 1);
                    AxHsms.AddU1Item(msgID, (short)moduleInfo.ProcState, 1);
                    AxHsms.AddU1Item(msgID, byWho, 1);
                    AxHsms.AddAsciiItem(msgID, GG.Equip.OperatorID, 16);
                }
            }
        }
        private static void S6F11_Sub_1(int msgID, int ceid, CassetteInfo info)
        {
            short rptid = 0;
            AxHsms.AddListItem(msgID, 2);
            {
                AxHsms.AddU1Item(msgID, rptid, 1);
                AxHsms.AddListItem(msgID, 6);
                {
                    AxHsms.AddAsciiItem(msgID, info.ModuleId, 23);
                    AxHsms.AddU1Item(msgID, info.MCMD, 1);
                    AxHsms.AddU1Item(msgID, info.ModuleState, 1);
                    AxHsms.AddU1Item(msgID, info.ProcStae, 1);
                    AxHsms.AddU1Item(msgID, info.ByWho, 1);
                    AxHsms.AddAsciiItem(msgID, info.OperId, 16);
                }
            }
        }
        private static void S6F11_Sub_2(int repId, CassetteInfo info)
        {

            AxHsms.AddListItem(repId, 15);
            {
                AxHsms.AddAsciiItem(repId, info.LotID, 20);
                AxHsms.AddAsciiItem(repId, info.TotalCellQuantity, 5);
                AxHsms.AddAsciiItem(repId, info.Model, 20);
                AxHsms.AddAsciiItem(repId, info.ProductId, 20);
                AxHsms.AddAsciiItem(repId, info.ProductType, 5);
                AxHsms.AddAsciiItem(repId, info.BatchId, 12);
                AxHsms.AddAsciiItem(repId, info.RunSpecId, 20);
                AxHsms.AddAsciiItem(repId, info.FlowId, 10);
                AxHsms.AddAsciiItem(repId, info.StepId, 6);
                AxHsms.AddAsciiItem(repId, info.PPID, 20);
                AxHsms.AddAsciiItem(repId, info.Result, 6);
                AxHsms.AddAsciiItem(repId, info.Code, 10);
                AxHsms.AddAsciiItem(repId, info.Comment, 20);
                AxHsms.AddAsciiItem(repId, info.HGlassId, 20);

                int count = info.Cells.Count();
                for (int i = 0; i < count; i++)
                {
                    AxHsms.AddListItem(repId, 7);
                    {
                        AxHsms.AddAsciiItem(repId, info.Cells[count].CellId, 20);
                        AxHsms.AddAsciiItem(repId, info.Cells[count].VcrResult, 5);
                        AxHsms.AddAsciiItem(repId, info.Cells[count].CJudgement, 8);
                        AxHsms.AddAsciiItem(repId, info.Cells[count].SCode, 10);
                        AxHsms.AddAsciiItem(repId, info.Cells[count].CCode, 10);
                        AxHsms.AddAsciiItem(repId, info.Cells[count].Comment, 20);

                        int defectCount = info.Cells[count].Defects.Count();
                        for (int j = 0; j < defectCount; j++)
                        {
                            {
                                AxHsms.AddListItem(repId, 3);
                                {
                                    AxHsms.AddAsciiItem(repId, info.Cells[count].Defects[j].CoorX, 20);
                                    AxHsms.AddAsciiItem(repId, info.Cells[count].Defects[j].CoorY, 20);
                                    AxHsms.AddAsciiItem(repId, info.Cells[count].Defects[j].DefectName, 20);
                                }

                            }
                        }

                    }
                }

            }
        }
        private static void S6F11_Sub_3(int repId, CassetteInfo info)
        {
            AxHsms.AddListItem(repId, 5);
            {
                AxHsms.AddAsciiItem(repId, info.PortID, 4);
                AxHsms.AddU1Item(repId, info.PortState, 1);
                AxHsms.AddU1Item(repId, info.PortType, 1);
                AxHsms.AddAsciiItem(repId, info.PortMode, 2);
                AxHsms.AddU1Item(repId, info.SortType, 1);
            }
        }
        private static void S6F11_Sub_4(int repId, CassetteInfo info)
        {
            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddAsciiItem(repId, info.CstID, 20);
                AxHsms.AddU1Item(repId, info.CstType, 1);
                AxHsms.AddU1Item(repId, info.BatchOrder, 1);
            }
        }
        
        /// <summary>
        /// 메소드 - (Name list Variable Data Send
        /// </summary>
        /// <param name="moduleInfo"></param>
        /// <param name="glass"></param>
        /// <param name="lstDvItem"></param>
        public static void S6F13(ModuleInfo moduleInfo, CellInfo glass, List<DvItem> lstDvItem)
        {
            short dateId = 0;
            int ceid = 2;

            string moduleid = GG.Equip.ModuleID;
            string operid = GG.Equip.OperatorID;

            int msgid = AxHsms.CreateMsg(6, 13, 1);
            AxHsms.AddListItem(msgid, 3);
            {
                AxHsms.AddU1Item(msgid, ref dateId, 1);
                AxHsms.AddU2Item(msgid, ref ceid, 1);
                AxHsms.AddListItem(msgid, 2);
                {
                    S6F13_SUB1(msgid, operid, glass);
                    S6F13_SUB2(msgid, moduleid, lstDvItem, glass);
                }
            }

            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("Tx S6F13, CEID={0} Name list Variable Data Send", ceid));
            else
                Logger.Append(string.Format("Tx S6F13, CEID={0} failed.", ceid));

        }
        private static void S6F13_SUB1(int repId, string rpcppid, CellInfo g)
        {
            short rptId = 10;
            AxHsms.AddListItem(repId, 2);
            {
                AxHsms.AddU1Item(repId, rptId, 1);
                AxHsms.AddListItem(repId, 21);
                {
                    AxHsms.AddAsciiItem(repId, g.HGlassID, 16);
                    AxHsms.AddAsciiItem(repId, g.EGlassID, 16);
                    AxHsms.AddAsciiItem(repId, g.LotID, 16);
                    AxHsms.AddAsciiItem(repId, g.BatchID, 16);
                    AxHsms.AddAsciiItem(repId, g.JobID, 16);
                    AxHsms.AddAsciiItem(repId, g.PortID, 4);
                    AxHsms.AddAsciiItem(repId, g.SlotID, 4);
                    AxHsms.AddAsciiItem(repId, g.ProductType, 4);
                    AxHsms.AddAsciiItem(repId, g.ProductKind, 4);
                    AxHsms.AddAsciiItem(repId, g.ProductID, 16);
                    AxHsms.AddAsciiItem(repId, g.RunSpecID, 16);
                    AxHsms.AddAsciiItem(repId, g.LayerID, 8);
                    AxHsms.AddAsciiItem(repId, g.StepID, 8);
                    AxHsms.AddAsciiItem(repId, g.PPID, 20);
                    AxHsms.AddAsciiItem(repId, g.FlowID, 20);
                    AxHsms.AddArrayItem(repId, "U2", 2, string.Format("{0},{1}", g.GlassSize[0], g.GlassSize[1]));
                    AxHsms.AddU2Item(repId, g.GlassThickness, 1);
                    AxHsms.AddU2Item(repId, (int)g.PanelState, 1);
                    AxHsms.AddAsciiItem(repId, g.GlassOrder, 2);
                    AxHsms.AddAsciiItem(repId, g.Comment, 16);
                    AxHsms.AddListItem(repId, 3);
                    {
                        AxHsms.AddListItem(repId, 0);
                        AxHsms.AddListItem(repId, 0);
                        AxHsms.AddListItem(repId, 0);
                    }
                }
            }
        }
        private static void S6F13_SUB2(int msgid, string moduleId, List<DvItem> lstDvItem, CellInfo g)
        {
            short modulecount = 1;
            short rptid = 9;
            AxHsms.AddListItem(msgid, 2);
            {
                AxHsms.AddU1Item(msgid, ref rptid, 1);
                AxHsms.AddListItem(msgid, modulecount);
                {
                    AxHsms.AddListItem(msgid, 2);
                    {
                        AxHsms.AddAsciiItem(msgid, moduleId, 28);
                        AxHsms.AddListItem(msgid, lstDvItem.Count);
                        {
                            foreach (DvItem item in lstDvItem)
                            {
                                AxHsms.AddListItem(msgid, 2);
                                {
                                    AxHsms.AddAsciiItem(msgid, item.Name, 40);
                                    AxHsms.AddAsciiItem(msgid, item.Value, 40);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region S7

        /// <summary>
        /// 메소드 - Process Program Load Grant
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="tmAck"></param>
        public static void S7F2(int msgID, short tmAck)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddU1Item(repId, tmAck, 1);

            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append("[HSMS E→H][S7F2], Process Program Load Grant");
            else
                Logger.Append("[HSMS E→H][S7F2], failed.");
        }

        /// <summary>
        /// 메소드 - Formatted Process Program Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="tmAck"></param>
        public static void S7F24(int msgID, short tmAck)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddU1Item(repId, ref tmAck, 1);
            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append(string.Format("[HSMS E→H][S7F24], TMACK={0}.", tmAck));
            else
                Logger.Append(string.Format("[HSMS E→H][S7F24], Fail."));
        }
        
        /// <summary>
        /// 메소드 - Formatted Process Program Data
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleid"></param>
        /// <param name="ppid"></param>
        /// <param name="ppidType"></param>
        public static void S7F26(int msgID, string moduleid, string ppid, short ppidType)
        {
            int vCCODE = 0;
            int msgid = AxHsms.CreateReplyMsg(msgID);
            string softRev = string.Empty;

            AxHsms.AddListItem(msgid, 4);
            {
                AxHsms.AddAsciiItem(msgid, moduleid, 28);
                AxHsms.AddAsciiItem(msgid, ppid, 20);
                AxHsms.AddU1Item(msgid, ppidType, 1);

                if (moduleid != GG.Equip.ModuleID)
                {
                    AxHsms.AddAsciiItem(msgid, softRev, 6);
                    AxHsms.AddListItem(msgid, 0);
                }
                else
                {
                    RmmManager rmmMgr = GG.Equip.RmmMgr;

                    int nCount = 0;
                    if (ppidType == 1) // eq
                    {
                        if (!rmmMgr.DicRmmEqp.ContainsKey(ppid))
                        {
                            AxHsms.AddAsciiItem(msgid, softRev, 6);
                            AxHsms.AddListItem(msgid, 0);
                        }
                        else
                        {
                            RmmEqpItem eqObj = rmmMgr.DicRmmEqp[ppid];

                            AxHsms.AddListItem(msgid, 2);
                            {
                                AxHsms.AddU2Item(msgid, ref vCCODE, 1);
                                AxHsms.AddListItem(msgid, rmmMgr.ParamNames.Count);
                                foreach (ParamInfo pinfo in rmmMgr.ParamNames)
                                {
                                    AxHsms.AddListItem(msgid, 2);
                                    {
                                        AxHsms.AddAsciiItem(msgid, pinfo.Name, 40);
                                        AxHsms.AddAsciiItem(msgid, eqObj.Params[pinfo.No].ToString(), 40);
                                    }
                                }

                            }
                        }
                    }
                    else if (ppidType == 2) // host
                    {
                        nCount = rmmMgr.DicRmmHost.Count;
                        if (rmmMgr.DicRmmHost.ContainsKey(ppid) == false)
                        {
                            AxHsms.AddAsciiItem(msgid, softRev, 6);
                            AxHsms.AddListItem(msgid, 0);
                        }
                        else
                        {
                            RmmHostItem eqObj = rmmMgr.DicRmmHost[ppid];

                            AxHsms.AddAsciiItem(msgid, eqObj.SoftRev, 6);
                            AxHsms.AddListItem(msgid, 2);
                            {
                                AxHsms.AddU2Item(msgid, ref vCCODE, 1);
                                AxHsms.AddListItem(msgid, 2);
                                {
                                    AxHsms.AddAsciiItem(msgid, GG.Equip.ModuleID, 40);
                                    AxHsms.AddAsciiItem(msgid, rmmMgr.DicRmmHost[ppid].EqpPPID, 40);
                                }
                            }
                        }
                    }
                    else
                    {
                        AxHsms.AddAsciiItem(msgid, softRev, 6);
                        AxHsms.AddListItem(msgid, 0);
                    }
                }
            }


            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("Tx S7F26, PPID List Send."));
            else
                Logger.Append(string.Format("Tx S7F26, Fail."));
        }

        /// <summary>
        /// 메소드 - Current Equipment PPID Data
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleid"></param>
        /// <param name="ppidType"></param>
        public static void S7F102(int msgID, string moduleid, short ppidType)
        {
            int repId = AxHsms.CreateReplyMsg(msgID);

            AxHsms.AddListItem(repId, 3);
            {
                AxHsms.AddAsciiItem(repId, moduleid, 28);
                AxHsms.AddU1Item(repId, ppidType, 1);

                if (moduleid != GG.Equip.ModuleID)
                {
                    AxHsms.AddListItem(repId, 0);
                }
                else
                {
                    RmmManager rmmMgr = GG.Equip.RmmMgr;

                    int nCount = 0;
                    if (ppidType == 1) // eq
                    {
                        nCount = rmmMgr.DicRmmEqp.Count;
                        AxHsms.AddListItem(repId, nCount);
                        foreach (KeyValuePair<string, RmmEqpItem> kvp in rmmMgr.DicRmmEqp)
                        {
                            AxHsms.AddAsciiItem(repId, kvp.Key, 20);
                        }
                    }
                    else if (ppidType == 2) // host
                    {
                        nCount = rmmMgr.DicRmmHost.Count;
                        AxHsms.AddListItem(repId, nCount);
                        foreach (KeyValuePair<string, RmmHostItem> kvp in rmmMgr.DicRmmHost)
                        {
                            AxHsms.AddAsciiItem(repId, kvp.Key, 20);
                        }
                    }
                    else
                    {
                        AxHsms.AddListItem(repId, 0);
                    }
                }
            }


            if (AxHsms.SendMsg(repId) == 0)
                Logger.Append(string.Format("Tx S7F102, PPID List Send."));
            else
                Logger.Append(string.Format("Tx S7F102, Fail."));
        }

        /// <summary>
        /// 메소드 - PPID Existence Check Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleid"></param>
        /// <param name="ppid"></param>
        /// <param name="ppidType"></param>
        public static void S7F104(int msgID, string moduleid, string ppid, short ppidType)
        {
            short tmAck = 0;
            if (moduleid != GG.Equip.ModuleID)
            {
                tmAck = 1;
            }
            else
            {
                RmmManager rmmMgr = GG.Equip.RmmMgr;
                if (ppidType == 1)
                {
                    if (rmmMgr.DicRmmEqp.ContainsKey(ppid) == false)
                        tmAck = 3;
                }
                else if (ppidType == 2)
                {
                    if (rmmMgr.DicRmmHost.ContainsKey(ppid) == false)
                        tmAck = 3;
                }
                else
                {
                    tmAck = 2;
                }
            }

            int rpmsgid = AxHsms.CreateReplyMsg(msgID);
            AxHsms.AddU1Item(rpmsgid, ref tmAck, 1);

            if (AxHsms.SendMsg(rpmsgid) == 0)
                Logger.Append(string.Format("Tx S7F104, PPID Existence check.ACK={0},PPID={1} Send.", tmAck, ppid));
            else
                Logger.Append(string.Format("Tx S7F104, Failed."));
        }


        /// <summary>
        /// 메소드 - PPID Create/Delete, PP Body Modify Report _ CREATE
        /// </summary>
        /// <param name="changeRmmMgr"></param>
        public static void S7F107_CREATE(RmmManager changeRmmMgr)
        {
            string moduleId = GG.Equip.ModuleID;
            string ppId = "";
            string softRev = "";
            short ppIdType = 0;
            int cCode = 0;
            short mode = 1;
            
            RmmManager rmmMgr = GG.Equip.RmmMgr;

            // host event report
            if (changeRmmMgr.ActionType == 2)
            {
                string hostPPID = changeRmmMgr.ActionHostPPID;

                if (string.IsNullOrEmpty(hostPPID) == false)
                {
                    ppIdType = 2;

                    if (changeRmmMgr.DicRmmHost.ContainsKey(hostPPID.Trim()))
                    {
                        RmmHostItem item = changeRmmMgr.DicRmmHost[hostPPID];
                        softRev = item.SoftRev;
                        int msgid = AxHsms.CreateMsg(7, 107, 1);

                        AxHsms.AddListItem(msgid, 5);
                        {
                            AxHsms.AddU1Item(msgid, mode, 1);
                            AxHsms.AddAsciiItem(msgid, moduleId, 28);
                            AxHsms.AddAsciiItem(msgid, hostPPID, 20);
                            AxHsms.AddU1Item(msgid, ppIdType, 1);


                            AxHsms.AddListItem(msgid, 1); //Number of process commands

                            AxHsms.AddListItem(msgid, 2);
                            AxHsms.AddU2Item(msgid, cCode, 1);
                            AxHsms.AddListItem(msgid, 2);
                            AxHsms.AddAsciiItem(msgid, GG.Equip.ModuleID, 40);

                            RmmEqpItem eqitem = changeRmmMgr.DicRmmEqp.Values.FirstOrDefault(f => f.Seq == item.EqpRecipeSeq - 1);
                            if (eqitem == null)
                                AxHsms.AddAsciiItem(msgid, "", 40);
                            else
                                AxHsms.AddAsciiItem(msgid, eqitem.Name, 40);
                        }

                        if (AxHsms.SendMsg(msgid) == 0)
                            Logger.Append(string.Format("S7F107, Send Report PPID Creation."));
                        else
                            Logger.Append(string.Format("S7F107, Send Report PPID Creation Fail."));
                    }
                }
            }

            // Eqp  event report
            if (changeRmmMgr.ActionType == 1)
            {
                ppIdType = 1;

                if (changeRmmMgr.DicRmmEqp.Count > 0)
                {
                    foreach (KeyValuePair<string, RmmEqpItem> dicKVP in changeRmmMgr.DicRmmEqp)
                    {
                        ppId = dicKVP.Key;
                        softRev = dicKVP.Value.SoftRev;

                        if (changeRmmMgr.ActionEqpPPID == ppId)
                        {
                            int msgid = AxHsms.CreateMsg(7, 107, 1);

                            AxHsms.AddListItem(msgid, 5);
                            AxHsms.AddU1Item(msgid, mode, 1);
                            AxHsms.AddAsciiItem(msgid, moduleId, 28);
                            AxHsms.AddAsciiItem(msgid, ppId, 20);
                            AxHsms.AddU1Item(msgid, ppIdType, 1);

                            AxHsms.AddListItem(msgid, 1);
                            AxHsms.AddListItem(msgid, 2);
                            AxHsms.AddU2Item(msgid, cCode, 1);
                            AxHsms.AddListItem(msgid, GG.Equip.RmmMgr.ParamNames.Count);

                            foreach (ParamInfo paramInfo in GG.Equip.RmmMgr.ParamNames)
                            {
                                AxHsms.AddListItem(msgid, 2);
                                AxHsms.AddAsciiItem(msgid, paramInfo.Name, 40);
                                AxHsms.AddAsciiItem(msgid, dicKVP.Value.Params[paramInfo.No].ToString(), 40);
                            }

                            if (AxHsms.SendMsg(msgid) == 0)
                                Logger.Append(string.Format("S7F107, Send Report PPID Creation."));
                            else
                                Logger.Append(string.Format("S7F107, Send Report PPID Creation Fail."));
                        }
                    }
                }
            }
            changeRmmMgr.Clear();

            rmmMgr = GG.Equip.RmmMgr;
            if (!rmmMgr.LoadFromPlc())
            {
                Logger.Append(string.Format("S7F107 CREATE, AFTER EVENT DIDN'T READ PLC"));
            }
        }
        /// <summary>
        /// 메소드 - PPID Create/Delete, PP Body Modify Report _ MODIFY
        /// </summary>
        /// <param name="changeRmmMgr"></param>
        public static void S7F107_MODIFY(RmmManager changeRmmMgr)
        {
            string moduleId = GG.Equip.ModuleID;
            string ppId = "";
            string softRev = "";
            short ppIdType = 0;
            int cCode = 0;
            short mode = 3;

            
            // host  event report
            if (changeRmmMgr.ActionType == 2)
            {
                string hostPPID = changeRmmMgr.ActionHostPPID.Trim();

                if (string.IsNullOrEmpty(hostPPID) == false)
                {
                    ppIdType = 2;

                    if (changeRmmMgr.DicRmmHost.ContainsKey(hostPPID))
                    {
                        RmmHostItem item = changeRmmMgr.DicRmmHost[hostPPID];
                        softRev = item.SoftRev;

                        int msgid = AxHsms.CreateMsg(7, 107, 1);

                        AxHsms.AddListItem(msgid, 5);
                        AxHsms.AddU1Item(msgid, mode, 1);
                        AxHsms.AddAsciiItem(msgid, moduleId, 28);
                        AxHsms.AddAsciiItem(msgid, ppId, 20);
                        AxHsms.AddU1Item(msgid, ppIdType, 1);

                        AxHsms.AddListItem(msgid, 1);
                        AxHsms.AddListItem(msgid, 2);
                        AxHsms.AddU2Item(msgid, cCode, 1);
                        AxHsms.AddListItem(msgid, 1);
                        AxHsms.AddListItem(msgid, 2);
                        AxHsms.AddAsciiItem(msgid, GG.Equip.ModuleID, 40);

                        RmmEqpItem eqitem = changeRmmMgr.DicRmmEqp.Values.FirstOrDefault(f => f.Seq == item.EqpRecipeSeq - 1);
                        if (eqitem == null)
                            AxHsms.AddAsciiItem(msgid, "", 40);
                        else
                            AxHsms.AddAsciiItem(msgid, eqitem.Name, 40);


                        if (AxHsms.SendMsg(msgid) == 0)
                            Logger.Append(string.Format("S7F107, Send Report PPID Modification."));
                        else
                            Logger.Append(string.Format("S7F107, Send Report PPID Modification Fail."));

                    }

                }
            }

            // Eqp  event report
            if (changeRmmMgr.ActionType == 1)
            {
                ppIdType = 1;

                if (changeRmmMgr.DicRmmEqp.Count > 0)
                {
                    foreach (KeyValuePair<string, RmmEqpItem> dicKVP in changeRmmMgr.DicRmmEqp)
                    {
                        ppId = dicKVP.Key;
                        softRev = dicKVP.Value.SoftRev;

                        if (changeRmmMgr.ActionEqpPPID == ppId)
                        {
                            int msgid = AxHsms.CreateMsg(7, 107, 1);
                            AxHsms.AddListItem(msgid, 5);
                            AxHsms.AddU1Item(msgid, mode, 1);
                            AxHsms.AddAsciiItem(msgid, moduleId, 28);
                            AxHsms.AddAsciiItem(msgid, ppId, 20);
                            AxHsms.AddU1Item(msgid, ppIdType, 1);

                            AxHsms.AddListItem(msgid, 1);
                            AxHsms.AddListItem(msgid, 2);
                            AxHsms.AddU2Item(msgid, cCode, 1);
                            AxHsms.AddListItem(msgid, GG.Equip.RmmMgr.ParamNames.Count);

                            foreach (ParamInfo paramInfo in GG.Equip.RmmMgr.ParamNames)
                            {
                                AxHsms.AddListItem(msgid, 2);
                                AxHsms.AddAsciiItem(msgid, paramInfo.Name, 40);
                                AxHsms.AddAsciiItem(msgid, dicKVP.Value.Params[paramInfo.No].ToString(), 40);
                            }

                            if (AxHsms.SendMsg(msgid) == 0)
                                Logger.Append(string.Format("S7F107, Send Report PPID Modification."));
                            else
                                Logger.Append(string.Format("S7F107, Send Report PPID Modification Fail."));
                        }
                    }
                }
            }

            changeRmmMgr.Clear();

            RmmManager rmmMgr = GG.Equip.RmmMgr;
            if (!rmmMgr.LoadFromPlc())
            {
                Logger.Append(string.Format("S7F107 MODIFICAION, AFTER EVENT,  DIDN'T READ PLC"));
            }
        }
        /// <summary>
        /// 메소드 - PPID Create/Delete, PP Body Modify Report _ DELETE
        /// </summary>
        /// <param name="changeRmmMgr"></param>
        public static void S7F107_DELETE(RmmManager changeRmmMgr)
        {
            string moduleId = GG.Equip.ModuleID;

            string ppId = "";
            string softRev = "";
            short ppIdType = 0;
            int cCode = 0;
            short mode = 2;

            RmmManager rmmMgr = GG.Equip.RmmMgr;
            
            // host  event report
            if (changeRmmMgr.ActionType == 2)
            {
                string hostPPID = changeRmmMgr.ActionHostPPID.Trim();


                if (string.IsNullOrEmpty(hostPPID) == false)
                {
                    ppIdType = 2;

                    if (rmmMgr.DicRmmHost.ContainsKey(hostPPID))
                    {
                        RmmHostItem item = rmmMgr.DicRmmHost[hostPPID];
                        softRev = item.SoftRev;

                        int msgid = AxHsms.CreateMsg(7, 107, 1);

                        AxHsms.AddListItem(msgid, 5);
                        AxHsms.AddU1Item(msgid, mode, 1);
                        AxHsms.AddAsciiItem(msgid, moduleId, 28);
                        AxHsms.AddAsciiItem(msgid, ppId, 20);
                        AxHsms.AddU1Item(msgid, ppIdType, 1);
                        AxHsms.AddListItem(msgid, 1);

                        AxHsms.AddListItem(msgid, 2);
                        AxHsms.AddU2Item(msgid, cCode, 1);
                        AxHsms.AddListItem(msgid, 1);
                        AxHsms.AddListItem(msgid, 2);
                        AxHsms.AddAsciiItem(msgid, GG.Equip.ModuleID, 40);

                        RmmEqpItem eqitem = rmmMgr.DicRmmEqp.Values.FirstOrDefault(f => f.Seq == item.EqpRecipeSeq - 1);
                        if (eqitem == null)
                            AxHsms.AddAsciiItem(msgid, "", 40);
                        else
                            AxHsms.AddAsciiItem(msgid, eqitem.Name, 40);


                        if (AxHsms.SendMsg(msgid) == 0)
                            Logger.Append(string.Format("S7F107, Send Report PPID Delete."));
                        else
                            Logger.Append(string.Format("S7F107, Send Report PPID Delete Fail."));
                    }

                }
            }

            // Eqp  event report
            if (changeRmmMgr.ActionType == 1)
            {
                ppIdType = 1;

                if (rmmMgr.DicRmmEqp.Count > 0)
                {
                    foreach (KeyValuePair<string, RmmEqpItem> dicKVP in rmmMgr.DicRmmEqp)
                    {
                        ppId = dicKVP.Key;
                        softRev = dicKVP.Value.SoftRev;

                        if (changeRmmMgr.ActionEqpPPID == ppId)
                        {
                            int msgid = AxHsms.CreateMsg(7, 107, 1);
                            AxHsms.AddListItem(msgid, 5);
                            AxHsms.AddU1Item(msgid, mode, 1);
                            AxHsms.AddAsciiItem(msgid, moduleId, 28);
                            AxHsms.AddAsciiItem(msgid, ppId, 20);
                            AxHsms.AddU1Item(msgid, ppIdType, 1);

                            AxHsms.AddListItem(msgid, 1);
                            AxHsms.AddListItem(msgid, 2);
                            AxHsms.AddU2Item(msgid, ref cCode, 1);
                            AxHsms.AddListItem(msgid, GG.Equip.RmmMgr.ParamNames.Count);

                            foreach (ParamInfo paramInfo in GG.Equip.RmmMgr.ParamNames)
                            {
                                AxHsms.AddListItem(msgid, 2);
                                AxHsms.AddAsciiItem(msgid, paramInfo.Name, 40);
                                AxHsms.AddAsciiItem(msgid, dicKVP.Value.Params[paramInfo.No].ToString(), 40);
                            }

                            if (AxHsms.SendMsg(msgid) == 0)
                                Logger.Append(string.Format("S7F107, Send Report PPID Delete."));
                            else
                                Logger.Append(string.Format("S7F107, Send Report PPID Delete Fail."));
                        }

                    }
                }
            }
            changeRmmMgr.Clear();

            if (!rmmMgr.LoadFromPlc())
            {
                Logger.Append(string.Format("S7F107 DELETION, AFTER ENVET, DIDN'T READ PLC"));
            }
        }
        
        /// <summary>
        /// 메소드 - Current Running Equipment PPID Data
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="moduleid"></param>
        /// <param name="ppidType"></param>
        public static void S7F110(int msgID, string moduleid, short ppidType)
        {
            string ppid = "";
            short tmAck = 0;

            if (moduleid != GG.Equip.ModuleID)
            {
                tmAck = 1;
            }
            else
            {
                RmmManager rmmMgr = GG.Equip.RmmMgr;
                rmmMgr.LoadFromPlc();

                if (ppidType == 1)
                {
                    string currentPPID = rmmMgr.CurrentNewEqpId;
                    if (rmmMgr.DicRmmHost.ContainsKey(rmmMgr.CurrentNewPPID))
                        currentPPID = rmmMgr.DicRmmHost[rmmMgr.CurrentNewPPID].EqpPPID;

                    if (!rmmMgr.DicRmmEqp.ContainsKey(currentPPID))
                        tmAck = 26;
                    else
                        ppid = rmmMgr.DicRmmEqp[currentPPID].Name;
                }
                else if (ppidType == 2)
                {
                    ppid = rmmMgr.CurrentNewPPID;
                }
                else
                {
                    tmAck = 27;
                }
            }

            int msgid = AxHsms.CreateReplyMsg(msgID);

            AxHsms.AddListItem(msgid, 2);
            {
                AxHsms.AddU1Item(msgid, tmAck, 1);
                AxHsms.AddListItem(msgid, 3);
                {
                    AxHsms.AddAsciiItem(msgid, moduleid, 28);
                    AxHsms.AddAsciiItem(msgid, ppid, 20);
                    AxHsms.AddU1Item(msgid, ppidType, 1);
                }
            }


            if (AxHsms.SendMsg(msgid) == 0)
                Logger.Append(string.Format("Tx S7F110, Running PPID ACK={0}."));
            else
                Logger.Append(string.Format("Tx S7F110, Failed."));
        }

        #endregion

        #region S9

        /// <summary>
        /// 메소드 - Unrecognized Stream Type
        /// </summary>
        /// <param name="msgID"></param>
        public static void S9F3(int msgID)
        {
            AxHsms.SendS9F3(msgID);
        }

        #endregion
    }
}
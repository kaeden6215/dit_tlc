﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.HSMS
{
    public class S6F11_CEID
    {  
        //Related Panel(Glass,Cell) Process Event CEID          
        public const int GLASS_BROKEN = 14;
        public const int GLASS_UNBROKEN = 15;


        public const int PANEL_PROCESS_START = 16;
        public const int PANEL_PROCESS_END = 17;

        public const int EQ_MODULE_PROCESS_STATE_CHANGED = 51;
        public const int EQ_MODULE_PROCESS_STATE_TIME_OVER= 52;
        public const int EQ_MODULE_STATE_CHANGED = 53;


        public const int CHANGE_TO_ONLINE_LOCAL_MODE = 72;
        public const int CHANGE_TO_ONLINE_REMOTE_MODE = 73;


        ////Related Equipment Parameter Event CEID                
        public const int PARAMETER_CHANGED_EOID = 101;                       
        public const int PARAMETER_CHANGED_ECID = 102;



        public const int CURRENT_PPID_CHAGNE = 401;
        public const int GLASS_HAND_SHAKE_TIME_OVER = 402;

        public const int IMMEDIATELY_PAUSE_HAPPEN_EVENT = 403;
        public const int PANEL_TRANSFER_STOP_HAPPEN_EVENT = 407;
        public const int PANEL_LOADING_STOP_HAPPEN_EVENT = 408;


        public const int SPECIFIC_INTERLOCK_ON_EVENT = 409;
        public const int HNAD_SHAKE_VALID_CHECK_FAIL = 413;

        public const int NEQ_NETWORK_ERROR_EVENT = 171;

        public const int CELL_JOG_PROCESS_START = 2001;
        public const int CELL_JOG_PROCESS_CANCEL = 2002;
        public const int CELL_JOG_PROCESS_ABORT = 2003;
        public const int CELL_JOG_PROCESS_END = 2004;
        public const int CELL_CASSETTE_OUT = 2006;
        public const int CELL_CASSETTE_IN = 2007;
        public const int CELL_ID_READ = 2010;
        public const int CELL_PROCESS_START_FOR_MODULE = 2016;
        public const int CELL_PROCESS_END_FOR_MODULE = 2017;
        public const int CELL_SCRAP = 2018;
        public const int CELL_UNSCRAP = 2019;
        public const int CELL_LOAD_REQUEST = 2031;
        public const int CELL_PRE_LOAD_COMPLETE = 2032;
        public const int CELL_LOAD_COMPLETE = 2033;
        public const int CELL_UNLOAD_REQUEST = 2034;
        public const int CELL_LOT_UNLOAD_COMPLETE = 2035;
        public const int CELL_ID_READ_FAIL = 2111;
        public const int CELL_ID_READING_COMPLETE = 2133;


        //public const int CHANGE_TO_ONLINE_LOCAL_MODE = 72;
        //public const int CHANGE_TO_OFFLINE_MODE = 71;
        //public const int GLASS_JUDGEMENT_RESULT = 15;

        //public const int PROCESS_STEP_CHANGE = 23;

        ////
        //public const int UNIT_PROCESS_START = 26;
        //public const int UNIT_PROCESS_END = 27;

        //public const int JOB_START_PERMISSION_EVENT = 30;

        //public const int CANNOT_SAVE_RAW_DATA = 63;
        //public const int CANNOT_SAVE_IMAGE_DATA = 65;

        //////Related Port Event CEID                               
        //public const int CST_LOAD_REQUEST = 31;
        //public const int CST_PRE_LOAD = 32;
        //public const int CST_LOAD_COMPLETE = 33;
        //public const int CST_UNLOAD_REQUEST = 34;
        //public const int CST_UNLOAD_COMPLETE = 35;
        //public const int CST_LOAD_REJECTED = 36;
        //public const int PORT_PARAMETER_IS_CHANGED = 37;





        //public const int GLASSID_READ_FAIL = 111;
        //public const int H_GLASSID_SEARCH_FAIL = 112;
        //public const int GLASSID_MISMATCH= 113;
        //public const int CASSETTE_IN = 116;
        //public const int CASSETTE_OUT = 117;


        //public const int NETWORK_SERVER_NOT_CONNCECTED = 171;  

    }


}


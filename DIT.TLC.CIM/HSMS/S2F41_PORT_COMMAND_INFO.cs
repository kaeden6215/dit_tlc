﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.HSMS
{
    public class S2F41_PORT_COMMAND_INFO
    {
        public int PORT_COUNT;

        public string[] PORTID_NAME_ARR;
        public string[] PORTID_ARR;
        public short[] PORTID_CPACK_ARR;
        //public short[] PORTID_HCACK_ARR;

        public bool CPACK;
        public short HCACK;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Log;
using DitInlineCim.Common;
using DitInlineCim.Lib;
using DitInlineCim.Struct;
using DitInlineCim.Struct.RMM;
using DitInlineCim.Struct.SV;
using DitInlineCim.Struct.S2F41;
using DitInlineCim.Struct.EC;

namespace DitInlineCim.HSMS
{
    public class HSMS_RECV
    {
        private static List<int> LST_RCMDS = new List<int>()
            {     
                // Equipment Command
                (int)EMRCmd.EQUIPMENT_CYCLE_STOP ,
                (int)EMRCmd.EQUIPMENT_PAUSE ,
                (int)EMRCmd.EQUIPMENT_RESUME,
                (int)EMRCmd.EQUIPMENT_PM ,
                (int)EMRCmd.EQUIPMENT_NORMAL
            };

        public static AxEZNETLIGHTLib.AxEZNetLight AxHsms { get; set; }

        #region S1

        /// <summary>
        /// 메소드 - Are you there Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S1F1(int msgID)
        {
            Logger.Append("[HSMS H→E][S1F1] Are you there Request");
            
            HSMS_SEND.S1F2(msgID);
        }

        /// <summary>
        /// 메소드 - On Line Data
        /// </summary>
        /// <param name="msgID"></param>
        public static void S1F2(int msgID)
        {
            AxHsms.GetListItem(msgID);
            Logger.Append("[HSMS H→E][S1F2] On-line Data");

            if (GG.Equip.MCMD_OLD == GG.Equip.MCMD && GG.Equip.HostConnectedOneOff == true)
            {
                if (GG.Equip.MCMD == EMMCmd.ONLINE_LOCAL)
                {
                    GG.Equip.SetMCMD(EMMCmd.ONLINE_LOCAL, EMByWho.ByEqp);
                }
                else if (GG.Equip.MCMD == EMMCmd.ONLINE_REMOTE)
                {
                    GG.Equip.SetMCMD(EMMCmd.ONLINE_REMOTE, EMByWho.ByEqp);
                }
                else
                {
                    GG.Equip.CmdSetMCMD(EMMCmd.ONLINE_REMOTE, EMByWho.ByEqp);
                }
                GG.Equip.HostConnectedOneOff = false;
            }
        }
        
        /// <summary>
        /// 메소드 - Selected Equipment State Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S1F3(int msgID)
        {
            Logger.Append("[HSMS H→E][S1F3] Select Equipment State Request");
            List<int> lstSv = new List<int>();
            string moduleid = string.Empty;
            int svid = 0;

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleid);

            int nCount = AxHsms.GetListItem(msgID);
            for (int i = 0; i < nCount; ++i)
            {
                AxHsms.GetU2Item(msgID, ref svid);
                lstSv.Add(svid);
            }
            HSMS_SEND.S1F4(msgID, moduleid, lstSv);
        }

        /// <summary>
        /// 메소드 - Formatted State Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S1F5(int msgID)
        {
            string moduleID = string.Empty;
            short sfcd = 0;

            AxHsms.GetListItem(msgID);
            {
                AxHsms.GetAsciiItemTrim(msgID, ref moduleID);
                AxHsms.GetU1Item(msgID, ref sfcd);
            }

            Logger.Append("[HSMS H→E][S1F5][SFCD=" + sfcd + "][MODULEID=" + moduleID + " Formatted State Request");

            if (sfcd == 1)  // Equipment Online Parameter 
            {
                HSMS_SEND.S1F6_EqOnlineParameter(msgID, sfcd, moduleID);
            }
            else if (sfcd == 3)  // Glass Tracking 
            {
                HSMS_SEND.S1F6_GlassTracking(msgID, sfcd, moduleID);
            }
            else if (sfcd == 4)  // Module State 
            {
                HSMS_SEND.S1F6_ModuleState(msgID, sfcd, moduleID);
            }
            else
            {
                HSMS_SEND.S1F6_CfcdNotFound(msgID, sfcd, moduleID);
            }
        }
        
        /// <summary>
        /// 메소드 - Request ONLINE Mode Change
        /// </summary>
        /// <param name="msgID"></param>
        public static void S1F17(int msgID)
        {
            string moduleID = string.Empty;
            short mcmd = 0;

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleID);
            AxHsms.GetU1Item(msgID, ref mcmd);

            //ONLACK : Online Mode Acknowledgement
            //0 = Request accepted
            //1 = Equipment already in that state
            //2 = There is no such a MODULEID
            //3 = There is no such a MCMD

            Logger.Append("[HSMS H→E][S1F17][MCMD=" + mcmd + "] Request ON_LINE");

            short tmAck = 0;
            if (mcmd == (int)EMMCmd.ONLINE_LOCAL)
            {
                if (moduleID != GG.Equip.ModuleID)
                    tmAck = 7;
                else if (GG.Equip.MCMD == EMMCmd.ONLINE_LOCAL)
                    tmAck = 5;
                else
                    tmAck = 0;
            }
            else if (mcmd == (int)EMMCmd.ONLINE_REMOTE)
            {
                if (moduleID != GG.Equip.ModuleID)
                    tmAck = 7;
                else if (GG.Equip.MCMD == EMMCmd.ONLINE_REMOTE)
                    tmAck = 2;
                else
                    tmAck = 0;
            }
            else if (mcmd == (int)EMMCmd.OFFLINE)
            {
                tmAck = 5;
            }
            else
            {
                tmAck = 5;
            }

            HSMS_SEND.S1F18(msgID, mcmd, tmAck, GG.Equip.ModuleID);

            if (tmAck == 0)
                GG.Equip.CmdSetMCMD((EMMCmd)mcmd, EMByWho.ByHost);
        }

        #endregion

        #region S2

        /// <summary>
        /// 메소드 - (New Equipment Constant Send
        /// </summary>
        /// <param name="msgID"></param>
        public static void S2F15(int msgID)
        {
            Logger.Append("[HSMS H→E][S2F15] New Equipment Constant Send");

            List<EcidItem> lstEcItem = new List<EcidItem>();
            string moduleID = string.Empty;

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleID);
            int ecCount = AxHsms.GetListItem(msgID);

            for (int iPos = 0; iPos < ecCount; iPos++)
            {
                AxHsms.GetListItem(msgID);
                {
                    EcidItem item = new EcidItem();
                    item.EcID = AxHsms.GetU2Item(msgID);
                    item.EcName = AxHsms.GetAsciiItemTrim(msgID);
                    item.EcDefine = AxHsms.GetAsciiItemTrim(msgID);
                    item.EcSLL = AxHsms.GetAsciiItemTrim(msgID);
                    item.EcSUL = AxHsms.GetAsciiItemTrim(msgID);
                    item.EcWLL = AxHsms.GetAsciiItemTrim(msgID);
                    item.EcWUL = AxHsms.GetAsciiItemTrim(msgID);
                    lstEcItem.Add(item);
                }
            }

            short tmAck = S2F15_SUB1(lstEcItem);
            bool pmAck = S2F15_SUB2(lstEcItem);

            tmAck = 25;
            HSMS_SEND.S2F16(msgID, tmAck, lstEcItem, moduleID);

            if (tmAck == 0 && pmAck == true)
            {
                GG.Equip.EcIdMgr.SetECItems(lstEcItem);
            }
        }
        private static short S2F15_SUB1(List<EcidItem> lstEcItem)
        {
            return 25;
        }
        private static bool S2F15_SUB2(List<EcidItem> lstEcItem)
        {
            foreach (EcidItem item in lstEcItem)
            {
                if (GG.Equip.EcIdMgr.ContainsKey(item.EcID))
                    item.EcID_PMACK = 0;
                else
                    item.EcID_PMACK = 3;

                if (item.EcID_PMACK == 3)
                    item.EcName_PMACK = 3;
                else if (item.EcName != GG.Equip.EcIdMgr[item.EcID].EcName)
                    item.EcName_PMACK = 3;
                else
                    item.EcName_PMACK = 0;

                double value;
                if (double.TryParse(item.EcSLL, out value))
                {
                    item.EcSLL_PMACK = 0;
                }
                else
                {
                    item.EcSLL_PMACK = 2;
                }

                if (double.TryParse(item.EcSUL, out value))
                {
                    item.EcSUL_PMACK = 0;
                }
                else
                {
                    item.EcSUL_PMACK = 2;
                }

                if (double.TryParse(item.EcWLL, out value))
                {
                    item.EcWLL_PMACK = 0;
                }
                else
                {
                    item.EcWLL_PMACK = 2;
                }

                if (double.TryParse(item.EcWUL, out value))
                {
                    item.EcWUL_PMACK = 0;
                }
                else
                {
                    item.EcWUL_PMACK = 2;
                }
            }
            return true;
        }

        /// <summary>
        /// 메소드 - Trace Initialize Send
        /// </summary>
        /// <param name="msgID"></param>
        public static void S2F23(int msgID)
        {
            Logger.Append("[HSMS H→E][S2F23] Trace Initialize Send");
            string moduleid = "";
            int trid = 0;
            string smpTime = "";
            int totSmp = 0;
            int grSize = 0;
            int svCount = 0;
            int svID = 0;
            short tmAck = 0;

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleid);
            AxHsms.GetU2Item(msgID, ref trid);
            AxHsms.GetAsciiItem(msgID, ref smpTime);
            AxHsms.GetU2Item(msgID, ref totSmp);
            AxHsms.GetU2Item(msgID, ref grSize);

            svCount = AxHsms.GetListItem(msgID);

            if (moduleid != GG.Equip.ModuleID)
            {
                tmAck = 2;
            }
            else
            {
                if (trid == 0)
                {
                    GG.Equip.TdMgr.Clear();
                }
                else
                {
                    if (svCount == 0)
                    {
                        if (GG.Equip.TdMgr.ContainsKey(trid))
                            GG.Equip.TdMgr.Remove(trid);
                        else
                            tmAck = 1;
                    }
                    else
                    {
                        if (grSize == 0)
                            tmAck = 1;
                        else
                        {
                            int ST = 0;
                            string strSMP = "";
                            int nMin = 0;
                            int nSec = 0;
                            int nSMPTIME = 0;
                            int nHor = 0;

                            try
                            {
                                ST = (int)float.Parse(smpTime);
                                strSMP = string.Format("{0:000000}", ST);
                                nHor = int.Parse(smpTime.Substring(0, 2));
                                nMin = int.Parse(smpTime.Substring(2, 2));
                                nSec = int.Parse(smpTime.Substring(4, 2));
                                nSMPTIME = nHor * 60 * 60 * 1000 * nMin * 60 * 1000 + nSec * 1000;
                            }
                            catch (Exception /*ex*/)
                            {
                                tmAck = 1;
                            }
                            if (nMin > 59 || nSec > 59 || nSMPTIME < 100)
                            {
                                tmAck = 1;
                            }
                            else
                            {

                                TdItem tdItem = new TdItem();
                                tdItem.Init();
                                tdItem.TrID = trid;
                                tdItem.SamplingTime = nSMPTIME;
                                tdItem.TotalSmp = totSmp;
                                tdItem.GroupSize = grSize;

                                for (int i = 0; i < svCount; ++i)
                                {
                                    AxHsms.GetU2Item(msgID, ref svID);
                                    tdItem.LstSv.Add(svID);
                                }

                                if (GG.Equip.SvMgr.Count > 0)
                                {
                                    bool IsContainAll = true;
                                    foreach (int nSVID in tdItem.LstSv)
                                    {
                                        if (!GG.Equip.SvMgr.ContainsKey(nSVID))
                                        {
                                            IsContainAll = false;
                                            break;
                                        }
                                    }

                                    if (!IsContainAll)
                                    {
                                        tmAck = 1;
                                    }
                                    else
                                    {
                                        GG.Equip.TdMgr.AddTDItem(trid, tdItem);
                                    }
                                }

                            }
                        }
                    }
                }
            }

            HSMS_SEND.S2F24(msgID, tmAck);
        }

        /// <summary>
        /// 메소드 - Equipment Constant Namelist Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S2F29(int msgID)
        {
            Logger.Append("[HSMS H→E][S2F29] Equipment Constant Namelist Request");
            int ecId = 0;
            bool findNonEcid = false;
            string moduleId = "";
            List<int> lstEcItems = new List<int>();

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleId);

            int nCount = AxHsms.GetListItem(msgID);
            for (int i = 0; i < nCount; ++i)
            {
                AxHsms.GetU2Item(msgID, ref ecId);
                if (GG.Equip.EcIdMgr.ContainsKey(ecId) == false)
                {
                    findNonEcid = true;
                    break;
                }

                lstEcItems.Add(ecId);
            }

            HSMS_SEND.S2F30(msgID, moduleId, lstEcItems, findNonEcid);
        }

        /// <summary>
        /// 메소드 - Date and Time Set Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S2F31(int msgID)
        {
            string time = string.Empty;
            AxHsms.GetAsciiItemTrim(msgID, ref time);
            Logger.Append("[HSMS H→E][S2F31][TIME=" + time + "] Date and Time Set Request");

            short tmAck = 0;
            if (time.Length != 14)
            {
                tmAck = 1;
            }
            else
            {
                short nYY = 0;
                short nMM = 0;
                short nDD = 0;
                short nhh = 0;
                short nmm = 0;
                short nss = 0;

                try
                {
                    nYY = short.Parse(time.Substring(0, 4));
                    nMM = short.Parse(time.Substring(4, 2));
                    nDD = short.Parse(time.Substring(6, 2));
                    nhh = short.Parse(time.Substring(8, 2));
                    nmm = short.Parse(time.Substring(10, 2));
                    nss = short.Parse(time.Substring(12, 2));
                }
                catch (Exception /*ex*/)
                {
                    tmAck = 6;
                }

                if (nMM < 0 || nMM > 12) tmAck = 1;
                else if (nDD < 0 || nDD > 31) tmAck = 1;
                else if (nhh < 0 || nhh > 24) tmAck = 1;
                else if (nmm < 0 || nmm > 60) tmAck = 1;
                else if (nss < 0 || nss > 60) tmAck = 1;

                if (tmAck == 0)
                {
                    tmAck = GG.Equip.SetTime(nYY, nMM, nDD, nhh, nmm, nss);
                }
            }

            //컴퓨터 시간 변경 작업
            HSMS_SEND.S2F32(msgID, time, tmAck);
        }

        /// <summary>
        /// 메소드 - Host Command Send
        /// </summary>
        /// <param name="msgID"></param>
        public static void S2F41(int msgID)
        {
            short rcmd = 0;
            string key = string.Empty;

            AxHsms.GetListItem(msgID);
            AxHsms.GetU1Item(msgID, ref rcmd);

            Logger.Append("[HSMS H→E][S2F41][RCMD=" + rcmd + "] Host Command Send(HCS)");
            switch ((EMRCmd)rcmd)
            {
                default:
                case EMRCmd.EQUIPMENT_PAUSE:
                case EMRCmd.EQUIPMENT_RESUME:
                case EMRCmd.EQUIPMENT_PM:
                case EMRCmd.EQUIPMENT_NORMAL:
                    S2F41_EquipmentCommand(msgID, rcmd);
                    break;
                case EMRCmd.CELL_JOB_PROCESS_START:
                case EMRCmd.CELL_JOB_PROCESS_CANCLE:
                    S2F41_CellJobProcess(msgID, rcmd);
                    break;
            }
        }
        /// <summary>
        /// 메소드 - Host Command Send _ RCMD = 51, 52, 53, 54
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="rcmd"></param>
        private static void S2F41_EquipmentCommand(int msgID, short rcmd)
        {
            S2F41_EQUIP_COMMAND_INFO s2f41_equipCmd = new S2F41_EQUIP_COMMAND_INFO();
            s2f41_equipCmd.MODULE_COUNT = AxHsms.GetListItem(msgID);

            s2f41_equipCmd.MODULE_NAME_ARR = new string[s2f41_equipCmd.MODULE_COUNT];
            s2f41_equipCmd.MODULE_ARR = new string[s2f41_equipCmd.MODULE_COUNT];

            for (int iPos = 0; iPos < s2f41_equipCmd.MODULE_COUNT; iPos++)
            {
                AxHsms.GetListItem(msgID);
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_equipCmd.MODULE_NAME_ARR[iPos]);
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_equipCmd.MODULE_ARR[iPos]);
            }

            GetS2F41EquipCommand_TMACK(rcmd, ref s2f41_equipCmd);
            GetS2F41EquipCommand_PMACK(rcmd, ref s2f41_equipCmd);

            HSMS_SEND.S2F42_EquipCommand(msgID, rcmd, s2f41_equipCmd);

            if (s2f41_equipCmd.TMACK == 0 && s2f41_equipCmd.PMACK == true)
            {
                for (int iPos = 0; iPos < s2f41_equipCmd.MODULE_COUNT; iPos++)
                {
                    if (rcmd == (int)EMRCmd.EQUIPMENT_RESUME)
                    {
                        GG.Equip.SetRCMD(s2f41_equipCmd.MODULE_ARR[iPos], s2f41_equipCmd.RCODE_ARR[iPos], EMRCmd.EQUIPMENT_RESUME, EMByWho.ByHost);
                    }
                    else if (rcmd == (int)EMRCmd.EQUIPMENT_PAUSE)
                    {
                        GG.Equip.SetRCMD(s2f41_equipCmd.MODULE_ARR[iPos], s2f41_equipCmd.RCODE_ARR[iPos], EMRCmd.EQUIPMENT_PAUSE, EMByWho.ByHost);
                    }
                    else if (rcmd == (int)EMRCmd.EQUIPMENT_PM)
                    {
                        GG.Equip.SetRCMD(s2f41_equipCmd.MODULE_ARR[iPos], s2f41_equipCmd.RCODE_ARR[iPos], EMRCmd.EQUIPMENT_PM, EMByWho.ByHost);
                    }
                    else if (rcmd == (int)EMRCmd.EQUIPMENT_NORMAL)
                    {
                        GG.Equip.SetRCMD(s2f41_equipCmd.MODULE_ARR[iPos], s2f41_equipCmd.RCODE_ARR[iPos], EMRCmd.EQUIPMENT_NORMAL, EMByWho.ByHost);
                    }
                }
            }
        }
        /// <summary>
        /// 메소드 - Host Command Send _ RCMD = 201, 202
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="rcmd"></param>
        private static void S2F41_CellJobProcess(int msgID, short rcmd)
        {
            S2F41_PROCESS_COMMAND_INFO s2f41_ProCmd = new S2F41_PROCESS_COMMAND_INFO();


            AxHsms.GetListItem(msgID);
            AxHsms.GetListItem(msgID);
            {
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.ModuleIdName);
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.ModuleId);
            }
            AxHsms.GetListItem(msgID);
            {
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.LotIdName);
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.LotId);
            }
            AxHsms.GetListItem(msgID);
            {
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.ModelName);
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.Model);
            }
            AxHsms.GetListItem(msgID);
            {
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.PPIDName);
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.PPID);
            }
            AxHsms.GetListItem(msgID);
            {
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.CstIdName);
                AxHsms.GetAsciiItemTrim(msgID, ref s2f41_ProCmd.CstId);
            }

            GetS2F41PROCESSCommand_TMACK(rcmd, ref s2f41_ProCmd);
            GetS2F41PROCESSCommand_PMACK(rcmd, ref s2f41_ProCmd);

            HSMS_SEND.S2F42_CellLotProcessCommandReply(msgID, rcmd, s2f41_ProCmd);

            if (s2f41_ProCmd.HCACK == 0 && s2f41_ProCmd.CPACK == true)
            {
                if (rcmd == (int)EMRCmd.CELL_JOB_PROCESS_START)
                {
                    // CELL_JOB_PROCESS_START ...
                }
                else if (rcmd == (int)EMRCmd.CELL_JOB_PROCESS_CANCLE)
                {
                    // CELL_JOB_PROCESS_CANCLE ...
                }
            }
        }
        private static void GetS2F41EquipCommand_TMACK(int rcmd, ref S2F41_EQUIP_COMMAND_INFO s2f41_equipCmd)
        {
            short resutAck = 0;
            if (rcmd == (int)EMRCmd.EQUIPMENT_PAUSE)
            {
                if (GG.Equip.ProcState == EMProcState.Pause)
                    resutAck = 74;
                else
                    resutAck = 0;
            }
            else if (rcmd == (int)EMRCmd.EQUIPMENT_RESUME)
            {
                if (GG.Equip.EquipState == EMEquipState.PM)
                    resutAck = 2;
                else if (GG.Equip.EquipState == EMEquipState.Fault)
                    resutAck = 2;
                else
                    resutAck = 0;
            }
            else if (rcmd == (int)EMRCmd.EQUIPMENT_PM)
            {
                if (GG.Equip.EquipState == EMEquipState.PM)
                    resutAck = 74;
                else
                    resutAck = 0;
            }
            else if (rcmd == (int)EMRCmd.EQUIPMENT_NORMAL)
            {
                if (GG.Equip.EquipState == EMEquipState.Fault)
                    resutAck = 2;
                else
                    resutAck = 0;
            }
            else if (rcmd == (int)EMRCmd.EQUIPMENT_CYCLE_STOP)
            {
                resutAck = 0;
            }
            else
            {
                resutAck = 3;
            }

            foreach (string id in s2f41_equipCmd.MODULE_ARR)
            {
                if (id != GG.Equip.ModuleID)
                {
                    resutAck = 3;
                    break;
                }
            }

            s2f41_equipCmd.TMACK = resutAck;
        }
        private static void GetS2F41EquipCommand_PMACK(int rcmd, ref S2F41_EQUIP_COMMAND_INFO s2f41_equipCmd)
        {
            s2f41_equipCmd.PMACK = true;
            for (int iPos = 0; iPos < s2f41_equipCmd.MODULE_COUNT; iPos++)
            {
                s2f41_equipCmd.MODULE_PMACK_ARR[iPos] = 0;  
                s2f41_equipCmd.RCODE_PMACK_ARR[iPos] = 0; 
            }
        }
        private static void GetS2F41PROCESSCommand_TMACK(int rcmd, ref S2F41_PROCESS_COMMAND_INFO s2f41_ProCmd)
        {
            short resutAck = 0;
            if (rcmd == (int)EMRCmd.CELL_JOB_PROCESS_START)
            {
                // ...
            }
            else if (rcmd == (int)EMRCmd.CELL_JOB_PROCESS_CANCLE)
            {
                // ...
            }

            s2f41_ProCmd.TMACK = resutAck;
        }
        private static void GetS2F41PROCESSCommand_PMACK(int rcmd, ref S2F41_PROCESS_COMMAND_INFO s2f41_ProCmd)
        {
            s2f41_ProCmd.PMACK = true;
            for (int iPos = 0; iPos < s2f41_ProCmd.MODULE_COUNT; iPos++)
            {
                {
                    s2f41_ProCmd.MODULE_PMACK_ARR[iPos] = 0; 
                    s2f41_ProCmd.RCODE_PMACK_ARR[iPos] = 0;  
                }
            }
        }
        
        /// <summary>
        /// 메소드 - Operator Call
        /// </summary>
        /// <param name="msgID"></param>
        public static void S2F101(int msgID)
        {
            short ack = 0;
            short tID = 0;
            string[] msgs = new string[10];
            string msg = string.Empty;

            string moduleID = string.Empty;

            AxHsms.GetListItem(msgID);
            AxHsms.GetU1Item(msgID, ref tID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleID);
            AxHsms.GetAsciiItemTrim(msgID, ref msgs[0]);

            Logger.Append("[HSMS H→E][S2F101][TID=" + tID + "][MODULEID=" + moduleID + "][MSG=" + msg + "] Operator Call");

            if (moduleID != GG.Equip.ModuleID)
            {
                ack = 1; // Message will not be displayed  
            }
            else
            {
                if (tID == 0 || tID == 1 || tID == 2 || tID == 3)
                {
                    if (msgs.ToList().Count(f => string.IsNullOrEmpty(f)) == 0)
                        ack = 0; //Accepted for display 
                    else
                        ack = 1;  //Message will not be displayed
                }
                else
                {
                    ack = 2; //Terminal not available 
                }
            }

            HSMS_SEND.S2F102(msgID, tID, moduleID, msg, ack);

            if (ack == 0)
            {
                switch (tID)
                {
                    case 0: S2F101_SUB1(tID, msgs, moduleID); break; //ALL 
                    case 1: S2F101_SUB2(tID, msgs, moduleID); break; //EQ CONTROL PC
                    case 2: S2F101_SUB3(tID, msgs, moduleID); break; //EQ UI                    
                }
            }
        }
        private static void S2F101_SUB1(short tID, string[] msg, string moduleID)
        {
            S2F101_SUB2(tID, msg, moduleID);
            S2F101_SUB3(tID, msg, moduleID);
        }
        private static void S2F101_SUB2(short tID, string[] msg, string moduleID)
        {
            GG.Equip.AddOperatorCallToCimPC(tID, msg, moduleID);
        }
        private static void S2F101_SUB3(short tID, string[] msg, string moduleID)
        {
            GG.Equip.AddOperatorCallToEqpUi(tID, msg, moduleID);
        }

        /// <summary>
        /// 메소드 - Equipment Online Parameter Change
        /// </summary>
        /// <param name="msgID"></param>
        public static void S2F103(int msgID)
        {
            Logger.Append("[HSMS H→E][S2F103] Equipment Online Parameter Change");
            string moduleID = string.Empty;

            int eoidCount = 0;
            int eomdCount = 0;

            int eoid = 0;
            string eomd = string.Empty;
            short eov = 0;
            short tmAck = 0;
            List<EoidItem> s2f103EoidInfos = new List<EoidItem>();

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleID);
            eoidCount = AxHsms.GetListItem(msgID);   //EOID COUNT

            for (int iPos = 0; iPos < eoidCount; iPos++)
            {
                AxHsms.GetListItem(msgID);
                AxHsms.GetU2Item(msgID, ref eoid);
                eomdCount = AxHsms.GetListItem(msgID); //EOMD COUNT
                for (int jPos = 0; jPos < eomdCount; jPos++)
                {
                    AxHsms.GetListItem(msgID);
                    AxHsms.GetAsciiItemTrim(msgID, ref eomd);
                    AxHsms.GetU1Item(msgID, ref eov);

                    s2f103EoidInfos.Add(new EoidItem() { EOID = (short)eoid, EOMD = eomd, EOV = eov });
                }
            }

            tmAck = GetS2F103EquipOnlinParameterChangeTmAck(moduleID, ref s2f103EoidInfos);
            HSMS_SEND.S2F104(msgID, moduleID, tmAck, s2f103EoidInfos);

            if (tmAck == 0)
            {
                GG.Equip.CmdSetEoid(s2f103EoidInfos, EMByWho.ByHost);
            }
        }
        public static short GetS2F103EquipOnlinParameterChangeTmAck(string moduleId, ref List<EoidItem> s2f103EoidInfos)
        {
            int tmAck = 0;
            if (GG.Equip.ModuleID != moduleId)
                tmAck = 31;

            foreach (EoidItem eoidInfo in s2f103EoidInfos)
            {
                if (GG.Equip.EoidMgr.ContainEOID(eoidInfo.EOID) == false)
                {
                    eoidInfo.PmAck = 1;   // Denied, at least on constant dess not exit
                    tmAck = tmAck != 0 ? 7 : tmAck;
                }
                else
                {
                    if (s2f103EoidInfos.Count(f => f.EOID == eoidInfo.EOID && f.EOMD == eoidInfo.EOMD) > 1)
                    {
                        eoidInfo.PmAck = 3;  // 동일한 EOID, EOMD가 있는지 확인  
                    }
                    else
                    {
                        if (GG.Equip.EoidMgr.ContainEOID(eoidInfo.EOID, eoidInfo.EOMD) == false)
                        {
                            eoidInfo.PmAck = 1;   // Denied, at least on constant dess not exit
                            tmAck = tmAck == 0 ? 7 : tmAck;
                        }
                        else
                        {
                            if (GG.Equip.EoidMgr.ValidateEOV(eoidInfo.EOID, eoidInfo.EOMD, eoidInfo.EOV) == false)
                            {
                                eoidInfo.PmAck = 3; //나중에 3으로 변경
                                tmAck = tmAck == 0 ? 9 : tmAck;
                            }
                            else
                            {
                                // EOV 체크, EOV 값 동일.
                                // EAC = 3
                                short currEov = GG.Equip.EoidMgr.GetEov(eoidInfo.EOID, eoidInfo.EOMD);
                                if (eoidInfo.EOV == currEov)
                                {
                                    eoidInfo.PmAck = 3;//추후 3으로 변경
                                    tmAck = tmAck == 0 ? 4 : tmAck;
                                }
                                else
                                {
                                    eoidInfo.PmAck = 0;
                                }
                            }
                        }
                    }
                }
            }

            return (short)tmAck;
        }

        #endregion

        #region S3

        /// <summary>
        /// 메소드 - CELL Information
        /// </summary>
        /// <param name="msgID"></param>
        public static void S3F215(int msgID)
        {
            CassetteInfo cstInfo = new CassetteInfo();
            
            List<EoidItem> s2f103EoidInfos = new List<EoidItem>();
            string pnlSize = string.Empty;

            Logger.Append(string.Format("[HSMS H→E][S3F215] CELL Information"));

            AxHsms.GetListItem(msgID);
            {
                cstInfo.HotDevice = AxHsms.GetU1Item(msgID);
                cstInfo.HotLevel = AxHsms.GetU1Item(msgID);

                AxHsms.GetListItem(msgID);
                {
                    AxHsms.GetListItem(msgID);  //5 Port Object Set
                    {
                        cstInfo.PortID = AxHsms.GetAsciiItemTrim(msgID);
                        cstInfo.PortState = AxHsms.GetU1Item(msgID);
                        cstInfo.PortType = AxHsms.GetU1Item(msgID);
                        cstInfo.PortMode = AxHsms.GetAsciiItemTrim(msgID);
                        cstInfo.SortType = AxHsms.GetU1Item(msgID);
                    }

                    AxHsms.GetListItem(msgID);  //3 Cassette Object Set
                    {
                        cstInfo.CstID = AxHsms.GetAsciiItemTrim(msgID);
                        cstInfo.CstType = AxHsms.GetU1Item(msgID);
                        cstInfo.BatchOrder = AxHsms.GetU1Item(msgID);
                    }

                    AxHsms.GetListItem(msgID); // Number of LOT
                    {
                        AxHsms.GetListItem(msgID);
                        {
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.LotID);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.TotalCellQuantity);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Model);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.ProductId);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.ProductType);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.BatchId);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.RunSpecId);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.FlowId);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.StepId);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.PPID);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Result);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Code);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Comment);
                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.HGlassId);

                            int count = AxHsms.GetListItem(msgID);
                            for (int i = 0; i < count; i++)
                            {
                                AxHsms.GetListItem(msgID);
                                {
                                    AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].CellId);
                                    AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].VcrResult);
                                    AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].CJudgement);
                                    AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].SCode);
                                    AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].CCode);
                                    AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].Comment);

                                    int defectcount = AxHsms.GetListItem(msgID);

                                    for (int j = 0; j < defectcount; j++)
                                    {
                                        AxHsms.GetListItem(msgID);
                                        {
                                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].Defects[j].CoorX);
                                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].Defects[j].CoorY);
                                            AxHsms.GetAsciiItemTrim(msgID, ref cstInfo.Cells[i].Defects[j].DefectName);
                                        }
                                    }

                                }

                            }

                        }
                    }

                }
            }

            short ACKC3 = 0;
            HSMS_SEND.S3F216(msgID, ACKC3);
        }

        #endregion

        #region S5

        /// <summary>
        /// 메소드 - Acknowledgement of Alarm Report
        /// </summary>
        /// <param name="msgID"></param>
        public static void S5F2(int msgID)
        {
            short tmAck = 0;
            AxHsms.GetU1Item(msgID, ref tmAck);
            Logger.Append("[HSMS H→E][S5F2][ACKC5=" + tmAck + "] Acknowledgement of Alarm Report ");
        }

        /// <summary>
        /// 메소드 - Alarm List Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S5F5(int msgID)
        {
            Logger.Append("[HSMS H→E][S5F5] List Alarms Request (LAR)");
            List<double> lstReqAlarm = new List<double>();

            string moduleid = string.Empty;
            int alarmCnt = 0;
            double alarmid = 0;
            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItem(msgID, ref moduleid);
            alarmCnt = AxHsms.GetListItem(msgID);

            for (int iPos = 0; iPos < alarmCnt; iPos++)
            {
                AxHsms.GetU4Item(msgID, ref alarmid);
                lstReqAlarm.Add(alarmid);
            }
            moduleid = moduleid.Trim();

            HSMS_SEND.S5F6(msgID, moduleid, lstReqAlarm);
        }

        /// <summary>
        /// 메소드 - Waiting Reset Alarms List
        /// </summary>
        /// <param name="msgID"></param>
        public static void S5F101(int msgID)
        {
            Logger.Append("[HSMS H→E][S5F101] Waiting Reset Alarms List (WRAL)");
            string moduleid = string.Empty;
            AxHsms.GetAsciiItem(msgID, ref moduleid);
            moduleid = moduleid.Trim();

            HSMS_SEND.S5F102(msgID, moduleid);
        }

        #endregion

        #region S6

        /// <summary>
        /// 메소드 - Event Report Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        public static void S6F12(int msgID)
        {
            short ACKC6 = 0;
            AxHsms.GetU1Item(msgID, ref ACKC6);

            Logger.Append("[HSMS H→E][S6F12][ACKC6=" + ACKC6 + "] Event Report Acknowledge ");
        }

        /// <summary>
        /// 메소드 - (Annotated Event Report Acknowledge
        /// </summary>
        /// <param name="msgID"></param>
        public static void S6F14(int msgID)
        {
            short tmAck = 0;
            AxHsms.GetU1Item(msgID, ref tmAck);

            Logger.Append("[HSMS H→E][S6F14][TMACK=" + tmAck + "] Acknowledgement of Annotated Event Report ");
        }

        #endregion

        #region S7

        /// <summary>
        /// 메소드 -  Process Program Load Inquire
        /// </summary>
        /// <param name="msgID"></param>
        public static void S7F1(int msgID)
        {
            Logger.Append("[HSMS H→E][S7F1] Process Program Load Inquire");
            string moduleid = "";
            string ppid = "";
            short ppidType = 0;
            short tmAck = 0;

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleid);
            AxHsms.GetAsciiItemTrim(msgID, ref ppid);
            AxHsms.GetU1Item(msgID, ref ppidType);

            if (GG.Equip.ModuleID != moduleid)
                tmAck = 31;
            else if (ppidType != 1 && ppidType != 2)
                tmAck = 27;
            else if (ppidType == 1 && GG.Equip.RmmMgr.DicRmmEqp.ContainsKey(ppid) == false)
                tmAck = 26;
            else if (ppidType == 2 && GG.Equip.RmmMgr.DicRmmHost.ContainsKey(ppid) == false)
                tmAck = 26;
            else if (ppidType == 1 && ppid == GG.Equip.RmmMgr.ActionEqpPPID)
                tmAck = 74;
            else if (ppidType == 2 && ppid == GG.Equip.RmmMgr.ActionHostPPID)
                tmAck = 74;
            else
                tmAck = 25;
            
            HSMS_SEND.S7F2(msgID, tmAck);
        }

        /// <summary>
        /// 메소드 - Formatted Process Program Send
        /// </summary>
        /// <param name="msgID"></param>
        public static void S7F23(int msgID)
        {
            Logger.Append("[HSMS H→E][S7F23] Formatted Process Program Send");
            string moduleId = string.Empty;
            string ppid = string.Empty;
            string softRev = string.Empty;
            short ppidType = 0;
            int ccod = 0;
            string paramName = string.Empty;
            string parmValue = string.Empty;


            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleId);
            AxHsms.GetAsciiItemTrim(msgID, ref ppid);
            AxHsms.GetU1Item(msgID, ref ppidType);

            long nCount = AxHsms.GetListItem(msgID); //Process Commands

            NewPPIDItem newPpId = new NewPPIDItem();
            AxHsms.GetListItem(msgID);
            AxHsms.GetU2Item(msgID, ref ccod);
            newPpId.CCode = ccod;
            long nParmCount = AxHsms.GetListItem(msgID);
            for (int j = 0; j < nParmCount; ++j)
            {
                AxHsms.GetListItem(msgID);
                AxHsms.GetAsciiItemTrim(msgID, ref paramName);
                AxHsms.GetAsciiItemTrim(msgID, ref parmValue);

                newPpId.Params.Add(paramName, parmValue);
            }

            short tmAck = GetS7F23FormattedProcessProgramTmAck(moduleId, ppid, softRev, ppidType, newPpId);

            HSMS_SEND.S7F24(msgID, tmAck);

            if (tmAck == 0)
            {
                GG.Equip.CmdEquipModifyRecipe(ppid, softRev, ppidType, newPpId);
            }
        }
        private static short GetS7F23FormattedProcessProgramTmAck(string moduleId, string ppid, string softRev, short ppidType, NewPPIDItem newPpId)
        {
            short tmAck = 0;

            foreach (string paramName in newPpId.Params.Keys)
            {
                double paramValue = 0;
                ParamInfo info = GG.Equip.RmmMgr.ParamNames.FirstOrDefault(f => f.Name == paramName);
                if (info == null)
                {
                    tmAck = 7;
                    break;
                }

                if (double.TryParse(newPpId.Params[paramName], out paramValue))
                {
                    if (info.LSL > paramValue)
                    {
                        tmAck = 9;
                        break;
                    }
                    else if (info.USL < paramValue)
                    {
                        tmAck = 9;
                        break;
                    }
                }
                else
                {
                    tmAck = 9;
                }
            }

            if (tmAck == 0)
            {
                if (GG.Equip.ModuleID != moduleId)
                    tmAck = 31;
                else if (ppidType != 1 && ppidType != 2)
                    tmAck = 27;
                else if (ppidType == 1 && GG.Equip.RmmMgr.DicRmmEqp.ContainsKey(ppid) == false)
                    tmAck = 26;
                else if (ppidType == 2 && GG.Equip.RmmMgr.DicRmmHost.ContainsKey(ppid) == false)
                    tmAck = 26;
                else if (ppidType == 1 && ppid == GG.Equip.RmmMgr.CurrentNewEqpId)
                    tmAck = 74;
                else if (ppidType == 2 && ppid == GG.Equip.RmmMgr.CurrentNewPPID)
                    tmAck = 74;
                else if (GG.Equip.MCMD == EMMCmd.OFFLINE)
                    tmAck = 72;
                else
                    tmAck = 0;
            }

            return tmAck;
        }

        /// <summary>
        /// 메소드 - Formatted Process Program Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S7F25(int msgID)
        {
            Logger.Append("[HSMS H→E][S7F25] Formatted Process Program Request");
            string moduleid = "";
            string ppid = "";
            short ppidType = 0;

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleid);
            AxHsms.GetAsciiItemTrim(msgID, ref ppid);
            AxHsms.GetU1Item(msgID, ref ppidType);

            HSMS_SEND.S7F26(msgID, moduleid, ppid, ppidType);
        }

        /// <summary>
        /// 메소드 - Current Equipment PPID List Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S7F101(int msgID)
        {
            Logger.Append("[HSMS H→E][S7F101] Current EQ's PPID List Request");
            string moduleId = "";
            short ppidType = 0;
            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleId);
            AxHsms.GetU1Item(msgID, ref ppidType);

            HSMS_SEND.S7F102(msgID, moduleId, ppidType);
        }

        /// <summary>
        /// 메소드 - PPID Existence Check
        /// </summary>
        /// <param name="msgID"></param>
        public static void S7F103(int msgID)
        {
            Logger.Append("[HSMS H→E][S7F103] PPID Existence Check");
            string moduleid = "";
            string ppid = "";
            short ppidType = 0;

            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleid);
            AxHsms.GetAsciiItemTrim(msgID, ref ppid);
            AxHsms.GetU1Item(msgID, ref ppidType);

            HSMS_SEND.S7F104(msgID, moduleid, ppid, ppidType);
        }
        
        /// <summary>
        /// 메소드 - PPID Create/Delete, PP Body Modify Report
        /// </summary>
        /// <param name="msgID"></param>
        public static void S7F108(int msgID)
        {
            short tmAck = 0;
            AxHsms.GetU1Item(msgID, ref tmAck);

            Logger.Append(string.Format("Rx S7F108, PPID Creation/Deletion and PP Body Modify Report Acknowledge TMACK = {0} .", tmAck));
        }

        /// <summary>
        /// 메소드 - Current Running Equipment PPID Request
        /// </summary>
        /// <param name="msgID"></param>
        public static void S7F109(int msgID)
        {
            Logger.Append("[HSMS H→E][S7F109] Current Running Equipment PPID Request");
            string moduleid = "";
            short ppidType = 0;
            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItem(msgID, ref moduleid);
            AxHsms.GetU1Item(msgID, ref ppidType);


            HSMS_SEND.S7F110(msgID, moduleid.Trim(), ppidType);
        }

        #endregion

        #region S10

        /// <summary>
        /// 메소드 - Terminal Display Single
        /// </summary>
        /// <param name="msgID"></param>
        public static void S10F3(int msgID)
        {
            short ack = 0;
            short tID = 0;
            string msg = string.Empty;
            string moduleID = string.Empty;

            AxHsms.GetListItem(msgID);
            AxHsms.GetU1Item(msgID, ref tID);
            AxHsms.GetAsciiItemTrim(msgID, ref moduleID);
            AxHsms.GetListItem(msgID);
            AxHsms.GetAsciiItemTrim(msgID, ref msg);


            Logger.Append("[HSMS H→E][S10F3][TID=" + tID + "][MODULEID=" + moduleID + "][MSG=" + msg + "] Terminal Message");

            if (ack == 0)
            {
                switch (tID)
                {
                    case 0: S10F3_SUB1(tID, msg, moduleID); break; //ALL 
                    case 1: S10F3_SUB2(tID, msg, moduleID); break; //EQ CONTROL PC
                    case 2: S10F3_SUB3(tID, msg, moduleID); break; //EQ UI                    
                }
            }
        }
        private static void S10F3_SUB1(short tID, string msg, string moduleID)
        {
            S10F3_SUB2(tID, msg, moduleID);
            S10F3_SUB3(tID, msg, moduleID);
        }
        private static void S10F3_SUB2(short tID, string msg, string moduleID)
        {
            GG.Equip.AddTerminalMessageToCimPC(tID, msg, moduleID);
        }
        private static void S10F3_SUB3(short tID, string msg, string moduleID)
        {
            GG.Equip.AddTerminalMessageToEqpUi(tID, msg, moduleID);
        }

        #endregion
    }
}

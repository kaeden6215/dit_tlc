﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Common;

namespace DitInlineCim.HSMS
{
    //오정석
    public class S2F41_PROCESS_COMMAND_INFO
    {
        public int MODULE_COUNT;

        public string JobidName = string.Empty;
        public string Jobid = string.Empty;
        public short JobidCpack = 0;

        public string IpidName = string.Empty;
        public string Ipid = string.Empty;
        public short IpidCpack = 0;

        public string IcidName = string.Empty;
        public string Icid = string.Empty;
        public short IcidCpack = 0;

        public string OpidName = string.Empty;
        public string Opid = string.Empty;
        public short OpidCpack = 0;

        public string OcidName = string.Empty;
        public string Ocid = string.Empty;
        public short OcidCpack = 0;

        public string SlotInfoName = string.Empty;
        public string SlotInfo = string.Empty;
        public short SlotInfoCpack = 0;

        public string OrderName = string.Empty;
        public short OrderCpack = 0;

        public short[] SLOTNO_ARR = new short[GG.CST_SLOT_CNT];

        public short HCACK = 0;
        public bool CPACK = false;
        public int SLOT_COUNT = 0;

        public string ModuleIdName = string.Empty;
        public string ModuleId = string.Empty;
        public short ModuleIdCpack = 0;

        public string LotIdName = string.Empty;
        public string LotId = string.Empty;
        public short LotIdCpack = 0;

        public string ModelName = string.Empty;
        public string Model = string.Empty;
        public short ModelCpack = 0;

        public string PPIDName = string.Empty;
        public string PPID = string.Empty;
        public short PPIDCPack = 0;

        public string CstIdName = string.Empty;
        public string CstId = string.Empty;
        public short CstIdCpack = 0;

        public string[] MODULE_NAME_ARR;
        public string[] MODULE_ARR;
        public short[] MODULE_PMACK_ARR;

        public string[] RCODE_NAME_ARR;
        public string[] RCODE_ARR;
        public short[] RCODE_PMACK_ARR;

        public bool PMACK;
        public short TMACK;
    }
}

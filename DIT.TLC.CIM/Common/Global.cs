﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Struct;
using DitInlineCim.PLC;
using System.Windows.Forms;
using System.IO;
using Dit.Framework.PLC;

namespace DitInlineCim.Common
{

    public class GG
    {
        // PLC정보
        public static short NETWORK_NO = 1;
        public static short STATION_NO_CIM = 2;
        public static short STATION_NO_INDEX = 1;


        public static VirtualShare PLC = null;
        
        //public static short[] ARR_WORD_ZR = null;
        //public static bool[] ARR_BIT { get; set; }
        //public static short[] ARR_WORD { get; set; }
        
        //S1F2  Version
        public static string SPEC_CODE = "SDC_OLED_V235_UNI";
        public static string SOFT_REV = "20150902151515";        
                
        //운영 정보
        public const int CST_SLOT_CNT = 26;
        //public const int MAX_FULL_GLASS = 26;
        //public const int MAX_GLASS_CNT = 52;
        public static int MAX_GLASS = 28;
        public const int MAX_PORT = 4;
        public const string PASSWORD = "";
        
        //PATH 정보 
        public static string SETTING_PATH = Path.Combine(Application.StartupPath, "Setting");
        public static string RMM_PATH = @"D:\DITCIM\RMM\RMM_INFO.INI";

        //
        public static EquipmentInfo Equip { get; set; }
        

        public static int StandardTact = 200;  // 100msec unit
    }
}

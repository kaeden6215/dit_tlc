﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.Lib
{
    public class CimConvertCode
    {
        //메소드 - 변환 처리. 

        public static EMByWho GetByWho(short code)
        {
            EMByWho byWho = code == 1 ? EMByWho.ByHost :
                                  code == 2 ? EMByWho.ByOper :
                                  code == 3 ? EMByWho.ByEqp : EMByWho.Unknown;
            return byWho;
        }
        public static EMEquipState GetEquipState(short code)
        {
            //Equip State Change 
            EMEquipState equipState = code == 1 ? EMEquipState.Normal :
                                      code == 2 ? EMEquipState.Fault :
                                      code == 3 ? EMEquipState.PM : EMEquipState.Unknown;

            return equipState;
        }

        public static EMProcState GetProcState(short code)
        {
            //Process State Change 
            EMProcState equipProc = code == 1 ? EMProcState.Init :
                                    code == 2 ? EMProcState.Idle :
                                    code == 3 ? EMProcState.Setup :
                                    code == 4 ? EMProcState.Ready :
                                    code == 5 ? EMProcState.Execute :
                                    code == 6 ? EMProcState.Pause : EMProcState.Unknown;
            return equipProc;
        }
    }
}

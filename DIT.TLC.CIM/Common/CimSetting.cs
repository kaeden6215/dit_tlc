﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DitInlineCim.Lib;
using Dit.Framework.Ini;

namespace DitInlineCim.Common
{
    public class CimSetting
    {
        public const string FILE_NAME = "DitCim.Setting.ini";


        //HOST (HSMS Attrubutes)
        public short HSMS_HOST_MODE; //' 0 : Passive , 1 : Active
        public short HSMS_HOST_DEVICEID;

        public short HSMS_HOST_T3;
        public short HSMS_HOST_T5;
        public short HSMS_HOST_T6;
        public short HSMS_HOST_T7;
        public short HSMS_HOST_T8;
        public string HSMS_HOST_LOG_FILE_PATH;
        public string HSMS_HOST_REMOTE_ADDRESS;
        public short HSMS_HOST_CONNECT_PORT_NO;
        public short HSMS_HOST_LOG_KEEP_DAY;
        public short HSMS_LOG_ENABLE;

               

        //CIMPC INI     
        public string CIM_LOG_NAME;
        public string CIM_LOG_PATH;
        public short CIM_LOG_KEEP_DAY;

        public short CIM_MCMD;
        public string CIM_EQPID;
        public string CIM_FROMEQPID;
        public string CIM_TOEQPID;
        public string CIM_SOFTREV;


        public short HSMS_HOST_RETRY;
        public short HSMS_HOST_LINK_TEST_INTERVAL;

        public void Load(string settingPath)
        {
            string iniFileName = Path.Combine(settingPath, FILE_NAME);
            IniReader iniReader = new IniReader(iniFileName);

            // HSMS
            HSMS_HOST_DEVICEID = (short)iniReader.GetInteger("HSMS", "DEVID", 0);
            HSMS_HOST_T3 = (short)iniReader.GetInteger("HSMS", "T3", 45);
            HSMS_HOST_T5 = (short)iniReader.GetInteger("HSMS", "T5", 10);
            HSMS_HOST_T6 = (short)iniReader.GetInteger("HSMS", "T5", 5);
            HSMS_HOST_T7 = (short)iniReader.GetInteger("HSMS", "T7", 10);
            HSMS_HOST_T8 = (short)iniReader.GetInteger("HSMS", "T8", 5);
            HSMS_HOST_RETRY = (short)iniReader.GetInteger("HSMS", "RETRY", 3);
            HSMS_HOST_LINK_TEST_INTERVAL = (short)iniReader.GetInteger("HSMS", "LINKTEST", 45);

            HSMS_HOST_LOG_FILE_PATH = iniReader.GetString("HSMS", "LOG_PATH");
            HSMS_HOST_REMOTE_ADDRESS = iniReader.GetString("HSMS", "IP");
            HSMS_HOST_CONNECT_PORT_NO = (short)iniReader.GetInteger("HSMS", "PORT", 7000);
            HSMS_HOST_LOG_KEEP_DAY = (short)iniReader.GetInteger("HSMS", "LOG_RETENTION", 90);
            HSMS_LOG_ENABLE = (short)iniReader.GetInteger("HSMS", "LOG_ENABLE", 1);
            HSMS_HOST_MODE = (short)iniReader.GetInteger("HSMS", "PASSIVE", 1);


            // CIM
            CIM_LOG_NAME = iniReader.GetString("LOG", "LOG_NAME");
            CIM_LOG_PATH = iniReader.GetString("LOG", "LOG_PATH");
            CIM_LOG_KEEP_DAY = (short)iniReader.GetInteger("LOG", "LOG_RETENTION", 1);


            // EQPUIPMENT
            CIM_EQPID = iniReader.GetString("CIM", "EQPID");
            CIM_FROMEQPID = iniReader.GetString("CIM", "FROMEQPID");
            CIM_TOEQPID = iniReader.GetString("CIM", "TOEQPID");

            CIM_MCMD = (short)iniReader.GetInteger("CIM", "MCMD", 2);
            CIM_SOFTREV = iniReader.GetString("CIM", "SOFTREV ");
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AxEZNETLIGHTLib;
using System.Collections;

namespace DitInlineCim.Common
{
    public static class UserExtension
    {
        public static System.Boolean IsNumeric(this string it)
        {
            try
            {
                Double.Parse(it);
                return true;
            }
            catch { }
            return false;
        }

        public static string GetAsciiItemTrim(this AxEZNetLight it, int lMsgId)
        {
            string pszValue = string.Empty;
            int result = it.GetAsciiItem(lMsgId, ref pszValue);
            return pszValue.Trim();
        }
        public static short GetU1Item(this AxEZNetLight it, int lMsgId)
        {
            short refVal = 0;
            it.GetU1Item(lMsgId, ref refVal);
            return refVal;
        }
        public static int GetU2Item(this AxEZNetLight it, int lMsgId)
        {
            int refVal = 0;
            it.GetU2Item(lMsgId, ref refVal);
            return refVal;
        }
        public static int GetAsciiItemTrim(this AxEZNetLight it, int lMsgId, ref string pszValue)
        {
            int result = it.GetAsciiItem(lMsgId, ref pszValue);
            pszValue = pszValue.Trim();
            return result;
        }

        public static short AddU1Item(this AxEZNetLight it, int lMsgId, short pValue, int lCount)
        {
            short refVal = pValue;
            return it.AddU1Item(lMsgId, ref refVal, lCount);
        }
        public static short AddU2Item(this AxEZNetLight it, int lMsgId, int pValue, int lCount)
        {
            int refVal = pValue;
            return it.AddU2Item(lMsgId, ref refVal, lCount);
        }
        public static short AddU4Item(this AxEZNetLight it, int lMsgId, double pValue, int lCount)
        {
            double refVal = pValue;
            return it.AddU4Item(lMsgId, ref refVal, lCount);
        }
        public static short AddBoolItem(this AxEZNetLight it, int lMsgId, short pValue, int lCount)
        {
            short refVal = pValue;
            return it.AddBoolItem(lMsgId, ref refVal, lCount);
        }

        public static byte[] ToByteArray(this BitArray it)
        {
            int numBytes = it.Count / 8;
            if (it.Count % 8 != 0) numBytes++;

            byte[] bytes = new byte[numBytes];
            int byteIndex = 0, bitIndex = 0;

            for (int i = 0; i < it.Count; i++)
            {
                if (it[i])
                    bytes[byteIndex] |= (byte)(1 << (7 - bitIndex));

                bitIndex++;
                if (bitIndex == 8)
                {
                    bitIndex = 0;
                    byteIndex++;
                }
            }

            return bytes;
        }

        public static short SetBit(this short it, int bit, bool value)
        {
            BitArray bits = new BitArray(BitConverter.GetBytes(it));
            bits[bit] = value;
            short setValue = BitConverter.ToInt16(bits.ToByteArray(), 0);
            return setValue;
        }

        public static bool GetBit(this short it, int bit)
        {
            BitArray bitArray = new BitArray(BitConverter.GetBytes(it));
            return bitArray[bit];
        }
        public static short[] GetBits(this short it)
        {
            BitArray bitArray = new BitArray(BitConverter.GetBytes(it));
            short[] bits = new short[16];

            for (int iPos = 0; iPos < 16; iPos++)
                bits[iPos] = (short)(bitArray[iPos] ? 1 : 0);

            return bits;
        }
        public static BitArray ToBitArray(this short it)
        {
            BitArray bitArray = new BitArray(BitConverter.GetBytes(it));
            return bitArray;
        }
        public static string GetPlcAscii(this short[] it, int start, int length)
        {
            if ((it == null) || (it.Length < start + length))
            {
                return "";
            }

            byte[] data = new byte[length * 2];
            for (int i = 0; i < length; i++)
            {
                byte[] bytes = BitConverter.GetBytes(it[start + i]);
                data[i * 2] = bytes[0];
                data[i * 2 + 1] = bytes[1];
            }

            return Encoding.ASCII.GetString(data).Trim(new char[] { '\0', '\r', '\n', '\t', ' ' });
        }

        public static byte GetByte(this short it, int start)
        {
            byte[] bytes = BitConverter.GetBytes(it);
            return bytes[start];
        }

    }
}

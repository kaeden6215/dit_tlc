﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DitInlineCim.Ucrl
{
    public partial class FrmSemConfig : Form
    {
        public FrmSemConfig()
        {
            InitializeComponent();
        }

        public string UdpIp { get; set; }

        public string UdpPort { get; set; }

        public bool SemEnable { get; set; }
    }
}

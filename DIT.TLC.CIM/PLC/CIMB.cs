﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.PLC
{
    public class CIMB
    {
        //PLC ALIVE 
        public static PlcAddr PcAlive                              /*  */= new PlcAddr(PlcMemType.S, 20007, 0);

        //public static PlcAddr OnlineModeChangeEventAck = new PlcAddr(PlcMemType.ZR, 80011, 6);

        //EQUIP STATE CHANGE ACK
        public static PlcAddr StateChangEventAck                  /*  */= new PlcAddr(PlcMemType.S, 20008, 0);
         
        //EQUIP PROCESS CHANGE ACK
        public static PlcAddr ProcessStateChangEventAck           /*  */= new PlcAddr(PlcMemType.S, 20009, 0);



        public static PlcAddr CellJobProcessStartAck        /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessCancelAck       /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessAbortAck        /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessEndAck          /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellCassetteOutAck            /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellCassetteInAck             /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellIdReadAck                 /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellProcessStartForModuleAck  /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellProcessEndForModuleAck    /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellScrapAck                  /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellUnscrapAck                /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellLoadRequestAck            /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellPreLoadCompleteAck        /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellLoadCompleteAck           /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellUnloadRequestAck          /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellLotUnloadCompleteAck      /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellIdReadFailAck             /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellReadingCompleteAck        /*  */= new PlcAddr(PlcMemType.S, 20002, 0);

        public static PlcAddr CellInformationCmd            /*  */= new PlcAddr(PlcMemType.S, 20002, 0);

        public static PlcAddr CellJobProcessStartCmd        /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessCancelCmd       /*  */= new PlcAddr(PlcMemType.S, 20002, 0);


        //MODULE IN,OUT EVETNT ACK
        public static PlcAddr CellInEventAck                      /*  */= new PlcAddr(PlcMemType.S, 20010, 1);
        

        public static PlcAddr CellOutEventAck                     /*  */= new PlcAddr(PlcMemType.S, 20011, 1);
        

        //ALARM HAPPEN, CLEAR EVETNT ACK
        public static PlcAddr AlarmHappenEventAck                   /*  */= new PlcAddr(PlcMemType.S, 20012, 0);
        public static PlcAddr AlarmClearEventAck                    /*  */= new PlcAddr(PlcMemType.S, 20012, 1);

        //SPECIFIC EVENT
        public static PlcAddr SpecificEventAck                      /*  */= new PlcAddr(PlcMemType.S, 20012, 2);

        //EOID, ECID CHANGE EVENT ACK
        public static PlcAddr EqEcidChangeEventAck                  /*  */= new PlcAddr(PlcMemType.S, 20012, 3);

        //PPIC CRUD EVENT
        public static PlcAddr CurrentPpidChangeEventAck             /*  */= new PlcAddr(PlcMemType.S, 20012, 4);

        //PIO EVENT        
        public static PlcAddr PpidCreateDeleteModifyEventAck        /*  */= new PlcAddr(PlcMemType.S, 20012, 9);
        public static PlcAddr EqPioNetworkErrorEventAck             /*  */= new PlcAddr(PlcMemType.S, 20012, 10);

        public static PlcAddr CellScrapEventAck                    /*  */= new PlcAddr(PlcMemType.S, 20012, 11);
        public static PlcAddr CellUnScrapEventAck                  /*  */= new PlcAddr(PlcMemType.S, 20012, 12);
        public static PlcAddr Vy01ExchangeEventAck                  /*  */= new PlcAddr(PlcMemType.S, 20012, 13);
        public static PlcAddr SuttleUseNotUseEventAck               /*  */= new PlcAddr(PlcMemType.S, 20012, 14);

        //CMD
        public static PlcAddr EqpProcPauseCmd                       /*  */= new PlcAddr(PlcMemType.S, 20013, 0);
        public static PlcAddr EqpProcResumeCmd                      /*  */= new PlcAddr(PlcMemType.S, 20013, 1);

        public static PlcAddr EqpStatePmChangeCmd                   /*  */= new PlcAddr(PlcMemType.S, 20013, 2);
        public static PlcAddr EqpStateNormalChangeCmd               /*  */= new PlcAddr(PlcMemType.S, 20013, 3);
        public static PlcAddr EqpCycleStopCmd                       /*  */= new PlcAddr(PlcMemType.S, 20013, 4);

        public static PlcAddr TimeSetCmd                            /*  */= new PlcAddr(PlcMemType.S, 20013, 5);
        public static PlcAddr OpcallSetCmd                          /*  */= new PlcAddr(PlcMemType.S, 20013, 6);
        public static PlcAddr TerminalMessageSetCmd                 /*  */= new PlcAddr(PlcMemType.S, 20013, 7);
        public static PlcAddr EinModeChangeCmd                      /*  */= new PlcAddr(PlcMemType.S, 20013, 8);

        //Hidden Recipe
        public static PlcAddr H_RecipeCreateDeleteModifyEventAck    /*  */= new PlcAddr(PlcMemType.S, 20013, 9);

        static CIMB()
        {
        }
         
    }
}

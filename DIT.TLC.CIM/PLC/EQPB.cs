﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.PLC
{
    public class EQPB
    {
        static EQPB()
        {
        }

        //CIM 영역
        public static PlcAddr CIM_BIT_START_ADDR = new PlcAddr(PlcMemType.S, 20000, 0, 500);

        //PLC ALIVE EVETNT
        public static PlcAddr PlcAlive                          /*  */= new PlcAddr(PlcMemType.S, 20000, 0);
        
        //EQUIP STATE CHANGE EVETNT
        public static PlcAddr StateChangEvent                 /*  */= new PlcAddr(PlcMemType.S, 20001, 0);

        //EQUIP PROCESS CHANGE EVETNT
        public static PlcAddr ProcessStateChangEvent          /*  */= new PlcAddr(PlcMemType.S, 20002, 0);


        public static PlcAddr CellJobProcessStart           /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessCancel          /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessAbort           /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessEnd             /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellCassetteOut               /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellCassetteIn                /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellIdRead                    /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellProcessStartForModule     /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellProcessEndForModule       /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellScrap                     /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellUnscrap                   /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellLoadRequest               /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellPreLoadComplete           /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellLoadComplete              /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellUnloadRequest             /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellLotUnloadComplete         /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellIdReadFail                /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellReadingComplete           /*  */= new PlcAddr(PlcMemType.S, 20002, 0);

        public static PlcAddr CellInformationCmdAck         /*  */= new PlcAddr(PlcMemType.S, 20002, 0);

        public static PlcAddr CellJobProcessStartCmdAck     /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        public static PlcAddr CellJobProcessCancelCmdAck    /*  */= new PlcAddr(PlcMemType.S, 20002, 0);
        

        //MODULE IN,OUT EVETNT        
        public static PlcAddr CellInEvent                     /*  */= new PlcAddr(PlcMemType.S, 20003, 1);
        public static PlcAddr CellOutEvent                    /*  */= new PlcAddr(PlcMemType.S, 20004, 1);

        //ALARM EVETNT
        public static PlcAddr AlarmHappenEvent                  /*  */= new PlcAddr(PlcMemType.S, 20005, 0);
        public static PlcAddr AlarmClearEvent                   /*  */= new PlcAddr(PlcMemType.S, 20005, 1);

        //SPECIFIC EVENT
        public static PlcAddr SpecificEvent                     /*  */= new PlcAddr(PlcMemType.S, 20005, 2);

        //EOID, ECID CHANGE EVENT
        public static PlcAddr EqEcidChangeEvent                 /*  */= new PlcAddr(PlcMemType.S, 20005, 3);

        //PPIC CRUD EVENT
        public static PlcAddr CurrentPpidChangeEvent            /*  */= new PlcAddr(PlcMemType.S, 20005, 4);

        //PIO EVENT
        
        public static PlcAddr PpidCreateDeleteModifyEvent       /*  */= new PlcAddr(PlcMemType.S, 20005, 9);
        public static PlcAddr EqPioNetworkErrorEvent            /*  */= new PlcAddr(PlcMemType.S, 20005, 10);

        public static PlcAddr CellScrapEvent                   /*  */= new PlcAddr(PlcMemType.S, 20005, 11);
        public static PlcAddr CellUnScrapEvent                 /*  */= new PlcAddr(PlcMemType.S, 20005, 12);
        public static PlcAddr Vy01ExchangeEvent                 /*  */= new PlcAddr(PlcMemType.S, 20005, 13);
        public static PlcAddr SuttleUseNotUseEvent              /*  */= new PlcAddr(PlcMemType.S, 20005, 14);

        //CMD ACK
        public static PlcAddr EqpProcPauseCmdAck                /*  */= new PlcAddr(PlcMemType.S, 20006, 0);
        public static PlcAddr EqpProcResumeCmdAck               /*  */= new PlcAddr(PlcMemType.S, 20006, 1);
        public static PlcAddr EqpStatePmChangeCmdAck            /*  */= new PlcAddr(PlcMemType.S, 20006, 2);
        public static PlcAddr EqpStateNormalChangeCmdAck        /*  */= new PlcAddr(PlcMemType.S, 20006, 3);
        public static PlcAddr EqpCycleStopCmdAck                /*  */= new PlcAddr(PlcMemType.S, 20006, 4);
        public static PlcAddr TimeSetCmdAck                     /*  */= new PlcAddr(PlcMemType.S, 20006, 5);
        public static PlcAddr OpcallSetCmdAck                   /*  */= new PlcAddr(PlcMemType.S, 20006, 6);
        public static PlcAddr TerminalMessageSetCmdAck          /*  */= new PlcAddr(PlcMemType.S, 20006, 7);
        public static PlcAddr EinModeChangeCmdAck               /*  */= new PlcAddr(PlcMemType.S, 20006, 8);

        //Hidden Recipe
        public static PlcAddr H_RecipeCreateDeleteModifyEvent   /*  */= new PlcAddr(PlcMemType.S, 20006, 9);

        //ALARM 영역
        public static PlcAddr AlarmBitArea                      /*  */= new PlcAddr(PlcMemType.S, 20500, 0, 62, PlcValueType.NONO);
        public static int AlarmBitWordLength = 62 * 16;




    }
}

﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.PLC
{
    public class CIMW
    {
        //CIM 영역         
        public static PlcAddr TimeSet                           /*  */= new PlcAddr(PlcMemType.S, 20200, 0, 6, PlcValueType.SHORT);
        public static PlcAddr OperCallMsg                       /*  */= new PlcAddr(PlcMemType.S, 20206, 0, 40, PlcValueType.ASCII);
        public static PlcAddr TerminalCallMsg                   /*  */= new PlcAddr(PlcMemType.S, 20206, 0, 40, PlcValueType.ASCII);
        public static PlcAddr ReasonCode                        /*  */= new PlcAddr(PlcMemType.S, 20246, 0, 2, PlcValueType.ASCII);
        public static PlcAddr EinUse                            /*  */= new PlcAddr(PlcMemType.S, 20248, 0, 1, PlcValueType.SHORT);
        public static PlcAddr SemEcs01                          /*  */= new PlcAddr(PlcMemType.S, 20249, 0, 1, PlcValueType.SHORT);//문자 type 유형확인필요
        public static PlcAddr SemTemp1                          /*  */= new PlcAddr(PlcMemType.S, 20250, 0, 1, PlcValueType.SHORT);//문자 type 유형확인필요
        public static PlcAddr SemHum01                          /*  */= new PlcAddr(PlcMemType.S, 20251, 0, 1, PlcValueType.SHORT);//문자 type 유형확인필요

        //PPID
        public static PlcAddr HppidCreateDeleteModifyEvent      /*  */= new PlcAddr(PlcMemType.S, 20454, 0, 1, PlcValueType.SHORT);
        public static PlcAddr HppidCreateDeleteModifyPpid       /*  */= new PlcAddr(PlcMemType.S, 20455, 0, 8, PlcValueType.ASCII);
        public static PlcAddr HppidCreateDeleteModifyRecipe     /*  */= new PlcAddr(PlcMemType.S, 20463, 0, 8, PlcValueType.ASCII);
        public static PlcAddr CmdMccModeChange                  /*  */= new PlcAddr(PlcMemType.S, 20471, 0, 1, PlcValueType.SHORT);
        public static PlcAddr HrecipeDownloadRequest            /*  */= new PlcAddr(PlcMemType.S, 20472, 0, 1, PlcValueType.SHORT);
        public static PlcAddr HiddenRecipeNonexistentPanelid    /*  */= new PlcAddr(PlcMemType.S, 20473, 0, 8, PlcValueType.ASCII);
        public static PlcAddr HiddenRecipeNonexistentPpid       /*  */= new PlcAddr(PlcMemType.S, 20481, 0, 8, PlcValueType.ASCII);

        //RMS
        public static PlcAddr RMS_EQ_RECIPE                     /*  */= new PlcAddr(PlcMemType.ZR, 77000, 0, 8, PlcValueType.ASCII);
        public static PlcAddr RMS_EQ_VERSION                    /*  */= new PlcAddr(PlcMemType.ZR, 77008, 0, 1, PlcValueType.SHORT);
        public static PlcAddr RMS_RECIPE_EQ_DATE                /*  */= new PlcAddr(PlcMemType.ZR, 77009, 0, 6, PlcValueType.SHORT);
        public static PlcAddr RMS_RECIPE_PARAMS                 /*  */= new PlcAddr(PlcMemType.ZR, 77015, 0, 50, PlcValueType.SHORT);

        public static PlcAddr RMS_H_EQ_RECIPE                   /*  */= new PlcAddr(PlcMemType.S, 97000, 0, 8, PlcValueType.ASCII);
        public static PlcAddr RMS_H_EQ_VERSION                  /*  */= new PlcAddr(PlcMemType.S, 97008, 0, 1, PlcValueType.SHORT);
        public static PlcAddr RMS_H_RECIPE_EQ_DATE              /*  */= new PlcAddr(PlcMemType.S, 97009, 0, 6, PlcValueType.SHORT);
        public static PlcAddr RMS_H_RECIPE_PARAMS               /*  */= new PlcAddr(PlcMemType.S, 97015, 0, 50, PlcValueType.SHORT);

        static CIMW()
        {

        }
    }
}

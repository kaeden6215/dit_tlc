﻿using Dit.Framework.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DitInlineCim.PLC
{
    public class EQPW
    {
        //CIM 영역
        public static PlcAddr CIM_WORD_START_ADDR = new PlcAddr(PlcMemType.ZR, 55000, 0, 35000);

        //Eq State
        public static PlcAddr EquipState                              /*  */= new PlcAddr(PlcMemType.S, 20300, 0, 1, PlcValueType.SHORT);
        public static PlcAddr LdUnitState                             /*  */= new PlcAddr(PlcMemType.S, 20300, 0, 1, PlcValueType.SHORT);
        public static PlcAddr Proc01UnitState                         /*  */= new PlcAddr(PlcMemType.S, 20300, 0, 1, PlcValueType.SHORT);
        public static PlcAddr Proc02UnitState                         /*  */= new PlcAddr(PlcMemType.S, 20300, 0, 1, PlcValueType.SHORT);
        public static PlcAddr UdUnitState                             /*  */= new PlcAddr(PlcMemType.S, 20300, 0, 1, PlcValueType.SHORT);

        //Eq Process Change
        public static PlcAddr EquipProcState                          /*  */= new PlcAddr(PlcMemType.S, 20316, 0, 1, PlcValueType.SHORT);
        public static PlcAddr LdUnitProcState                         /*  */= new PlcAddr(PlcMemType.S, 20316, 0, 1, PlcValueType.SHORT);
        public static PlcAddr Proc01UnitProcState                     /*  */= new PlcAddr(PlcMemType.S, 20316, 0, 1, PlcValueType.SHORT);
        public static PlcAddr Proc02UnitProcState                     /*  */= new PlcAddr(PlcMemType.S, 20316, 0, 1, PlcValueType.SHORT);
        public static PlcAddr UdUnitProcState                         /*  */= new PlcAddr(PlcMemType.S, 20316, 0, 1, PlcValueType.SHORT);

        //Eq By Who
        public static PlcAddr BywhoEqState                              /*  */= new PlcAddr(PlcMemType.S, 20332, 0, 1, PlcValueType.SHORT);

        //MODULE IN
        public static PlcAddr CellInCellData                              /*  */= new PlcAddr(PlcMemType.S, 20349, 0, 1, PlcValueType.SHORT);

        //MODULE OUT
        public static PlcAddr CellOutCellData                             /*  */= new PlcAddr(PlcMemType.S, 20365, 0, 1, PlcValueType.SHORT);







        public static PlcAddr PM_CODE                                   /*  */= new PlcAddr(PlcMemType.S, 20396, 0, 2, PlcValueType.ASCII);
        public static PlcAddr PAUSE_CODE                                /*  */= new PlcAddr(PlcMemType.S, 20398, 0, 2, PlcValueType.ASCII);
        public static PlcAddr BROKEN_CODE                               /*  */= new PlcAddr(PlcMemType.S, 20400, 0, 2, PlcValueType.ASCII);
        public static PlcAddr SPECIFIC_CODE                             /*  */= new PlcAddr(PlcMemType.S, 20402, 0, 1, PlcValueType.SHORT);
        public static PlcAddr CurrentOldPPID                            /*  */= new PlcAddr(PlcMemType.S, 20403, 0, 8, PlcValueType.ASCII);
        public static PlcAddr CurrentNewPPID                            /*  */= new PlcAddr(PlcMemType.S, 20411, 0, 8, PlcValueType.ASCII);
        public static PlcAddr ECID_CHANGE_NO                            /*  */= new PlcAddr(PlcMemType.S, 20424, 0, 1, PlcValueType.SHORT); // 
        public static PlcAddr PanelScrapUnScrapPos                      /*  */= new PlcAddr(PlcMemType.S, 20425, 0, 1, PlcValueType.SHORT); // 
        public static PlcAddr PanelScrapUnScrapModuleNo                 /*  */= new PlcAddr(PlcMemType.S, 20426, 0, 1, PlcValueType.SHORT); // 

        public static PlcAddr PioEquipEvent                             /*  */= new PlcAddr(PlcMemType.S, 20427, 0, 1, PlcValueType.SHORT); // 
        public static PlcAddr EqIoDirection                             /*  */= new PlcAddr(PlcMemType.S, 20428, 0, 1, PlcValueType.SHORT); // 

        public static PlcAddr PpidCreateDeleteModifyEvent               /*  */= new PlcAddr(PlcMemType.S, 20429, 0, 1, PlcValueType.SHORT); //
        public static PlcAddr PpidCreateDeleteModifyPpidType            /*  */= new PlcAddr(PlcMemType.S, 20430, 0, 1, PlcValueType.SHORT); //
        public static PlcAddr PpidCreateDeleteModifyPpid                /*  */= new PlcAddr(PlcMemType.S, 20431, 0, 8, PlcValueType.ASCII); //
        public static PlcAddr PpidCreateDeleteModifyRecipe              /*  */= new PlcAddr(PlcMemType.S, 20439, 0, 8, PlcValueType.ASCII); //
        public static PlcAddr EinAppliedH_Panelid                       /*  */= new PlcAddr(PlcMemType.S, 20447, 0, 6, PlcValueType.ASCII); //
        public static PlcAddr SuttleNotUseNumber                        /*  */= new PlcAddr(PlcMemType.S, 20453, 0, 1, PlcValueType.ASCII); //

        //public static PlcAddr ResumeCheck = new PlcAddr(PlcMemType.S, 20489, 0, 1, PlcValueType.SHORT); //

        //Alarm 영역
        public static PlcAddr AlarmReading                              /*  */= new PlcAddr(PlcMemType.S, 20500, 0, 400, PlcValueType.SHORT); //

        //ECID
        public static PlcAddr ECID_001_DEF                              /*  */= new PlcAddr(PlcMemType.ZR, 77000, 0, 1, PlcValueType.SHORT); //
        public static PlcAddr ECID_001_SLL                              /*  */= new PlcAddr(PlcMemType.ZR, 77001, 0, 1, PlcValueType.SHORT); //
        public static PlcAddr ECID_001_SUL                              /*  */= new PlcAddr(PlcMemType.ZR, 77002, 0, 1, PlcValueType.SHORT); //
        public static PlcAddr ECID_001_WLL                              /*  */= new PlcAddr(PlcMemType.ZR, 77003, 0, 1, PlcValueType.SHORT); //
        public static PlcAddr ECID_001_WUL                              /*  */= new PlcAddr(PlcMemType.ZR, 77004, 0, 1, PlcValueType.SHORT); //

        //SVID
        public static PlcAddr SVID_001                                  /*  */= new PlcAddr(PlcMemType.ZR, 78000, 0, 1, PlcValueType.SHORT); //

        //DCOLL
        public static PlcAddr DCOLL_001                                 /*  */= new PlcAddr(PlcMemType.ZR, 78500, 0, 1, PlcValueType.SHORT); //


        //STATE EQUIP 
        public static PlcAddr STATE_VALUE                       /*  */= new PlcAddr(PlcMemType.ZR, 78000, 0, 500, PlcValueType.NONO);


        //ECID
        public static PlcAddr ECID_SYNC_ADDR                    /*  */= new PlcAddr(PlcMemType.ZR, 77000, 0, 250, PlcValueType.NONO);
        public static PlcAddr ECID_001_DEF_POS                  /*  */= new PlcAddr(PlcMemType.ZR, 77000, 0, 1, PlcValueType.NONO);
        public static PlcAddr ECID_001_SLL_POS                  /*  */= new PlcAddr(PlcMemType.ZR, 77001, 0, 1, PlcValueType.NONO);
        public static PlcAddr ECID_001_SUL_POS                  /*  */= new PlcAddr(PlcMemType.ZR, 77002, 0, 1, PlcValueType.NONO);
        public static PlcAddr ECID_001_WLL_POS                  /*  */= new PlcAddr(PlcMemType.ZR, 77003, 0, 1, PlcValueType.NONO);
        public static PlcAddr ECID_001_WUL_POS                  /*  */= new PlcAddr(PlcMemType.ZR, 77004, 0, 1, PlcValueType.NONO);



        //CELL DATA
        public static PlcAddr H_PANELID_POS                     /*  */= new PlcAddr(PlcMemType.ZR, 0, 0, 6);
        public static PlcAddr E_PANELID_POS                     /*  */= new PlcAddr(PlcMemType.ZR, 6, 0, 6);
        public static PlcAddr BATCHID_POS                       /*  */= new PlcAddr(PlcMemType.ZR, 12, 0, 6);
        public static PlcAddr PROD_TYPE_POS                     /*  */= new PlcAddr(PlcMemType.ZR, 18, 0, 1);
        public static PlcAddr PROD_KIND_POS                     /*  */= new PlcAddr(PlcMemType.ZR, 19, 0, 1);
        public static PlcAddr DEVICEID_POS                      /*  */= new PlcAddr(PlcMemType.ZR, 20, 0, 9);
        public static PlcAddr STEPID_POS                        /*  */= new PlcAddr(PlcMemType.ZR, 29, 0, 5);
        public static PlcAddr PPID_POS                          /*  */= new PlcAddr(PlcMemType.ZR, 34, 0, 8);
        public static PlcAddr THICKNESS_POS                     /*  */= new PlcAddr(PlcMemType.ZR, 42, 0, 1);
        public static PlcAddr INS_FLAG_POS                      /*  */= new PlcAddr(PlcMemType.ZR, 43, 0, 1);
        public static PlcAddr PANEL_SIZE_POS                    /*  */= new PlcAddr(PlcMemType.ZR, 44, 0, 2);
        public static PlcAddr PANEL_POSITION_POS                /*  */= new PlcAddr(PlcMemType.ZR, 46, 0, 1);
        public static PlcAddr COUNT1_POS                        /*  */= new PlcAddr(PlcMemType.ZR, 47, 0, 1);
        public static PlcAddr COUNT2_POS                        /*  */= new PlcAddr(PlcMemType.ZR, 48, 0, 1);
        public static PlcAddr GRADE_POS                         /*  */= new PlcAddr(PlcMemType.ZR, 49, 0, 3);
        public static PlcAddr COMMENT_POS                       /*  */= new PlcAddr(PlcMemType.ZR, 52, 0, 8);
        public static PlcAddr COMP_COUNT_POS                    /*  */= new PlcAddr(PlcMemType.ZR, 60, 0, 1);
        public static PlcAddr READING_FLAG_POS                  /*  */= new PlcAddr(PlcMemType.ZR, 61, 0, 1);
        public static PlcAddr PANEL_STATE_POS                   /*  */= new PlcAddr(PlcMemType.ZR, 62, 0, 1);
        public static PlcAddr JUDGEMENT_POS                     /*  */= new PlcAddr(PlcMemType.ZR, 63, 0, 2);
        public static PlcAddr CODE_POS                          /*  */= new PlcAddr(PlcMemType.ZR, 65, 0, 2);
        public static PlcAddr RUNLINE_POS                       /*  */= new PlcAddr(PlcMemType.ZR, 67, 0, 10);
        public static PlcAddr UNIQUEID_POS                      /*  */= new PlcAddr(PlcMemType.ZR, 77, 0, 2);
        public static PlcAddr PAIR_H_PANELID_POS                /*  */= new PlcAddr(PlcMemType.ZR, 79, 0, 6);
        public static PlcAddr PAIR_E_PANELID_POS                /*  */= new PlcAddr(PlcMemType.ZR, 85, 0, 6);
        public static PlcAddr PAIR_GRADE_POS                    /*  */= new PlcAddr(PlcMemType.ZR, 91, 0, 3);
        public static PlcAddr PAIR_UNIQUEID_POS                 /*  */= new PlcAddr(PlcMemType.ZR, 94, 0, 2);
        public static PlcAddr FLOW_RECIPE                       /*  */= new PlcAddr(PlcMemType.ZR, 96, 0, 1);
        public static PlcAddr RESERVED_POS                      /*  */= new PlcAddr(PlcMemType.ZR, 97, 0, 6);
        public static PlcAddr BITS_SIGNALS_POS                  /*  */= new PlcAddr(PlcMemType.ZR, 103, 0, 1);
        public static PlcAddr REFER_DATA_POS                    /*  */= new PlcAddr(PlcMemType.ZR, 104, 0, 2);
        public static PlcAddr DV_VALUE                          /*  */= new PlcAddr(PlcMemType.ZR, 78500, 0, 100, PlcValueType.NONO);

        //PORT 
        public static PlcAddr PORT_PORTID               /*  */= new PlcAddr(PlcMemType.S, 00, 0, 04);  //고정값
        public static PlcAddr PORT_PORT_STATE           /*  */= new PlcAddr(PlcMemType.S, 04, 0, 02);  //고정값
        public static PlcAddr PORT_PORT_TYPE            /*  */= new PlcAddr(PlcMemType.S, 06, 0, 02);  //고정값
        public static PlcAddr PORT_PORT_MODE            /*  */= new PlcAddr(PlcMemType.S, 08, 0, 02);  //고정값
        public static PlcAddr PORT_SORT_TYPE            /*  */= new PlcAddr(PlcMemType.S, 10, 0, 02);  //고정값

        //CST 정보.
        public static PlcAddr CST_CSTID                /*  */= new PlcAddr(PlcMemType.S, 00, 0, 20);  //고정값
        public static PlcAddr CST_CST_TYPE             /*  */= new PlcAddr(PlcMemType.S, 20, 0, 02);  //고정값
        public static PlcAddr CST_BATCH_ORDER          /*  */= new PlcAddr(PlcMemType.S, 22, 0, 02);  //고정값 

        //LOT INFO 
        public static PlcAddr LOT_LOT_ID                /*  */= new PlcAddr(PlcMemType.S, 000, 0, 20);  //고정값
        public static PlcAddr LOT_T_C_QTY               /*  */= new PlcAddr(PlcMemType.S, 020, 0, 05);  //고정값
        public static PlcAddr LOT_MODEL                 /*  */= new PlcAddr(PlcMemType.S, 025, 0, 20);  //고정값
        public static PlcAddr LOT_PRODUCTID             /*  */= new PlcAddr(PlcMemType.S, 045, 0, 20);  //고정값
        public static PlcAddr LOT_PRODUCT_TYPE          /*  */= new PlcAddr(PlcMemType.S, 065, 0, 05);  //고정값
        public static PlcAddr LOT_BATCHID               /*  */= new PlcAddr(PlcMemType.S, 070, 0, 12);  //고정값
        public static PlcAddr LOT_RUNSPECID             /*  */= new PlcAddr(PlcMemType.S, 082, 0, 20);  //고정값
        public static PlcAddr LOT_FLOWID                /*  */= new PlcAddr(PlcMemType.S, 102, 0, 10);  //고정값
        public static PlcAddr LOT_STEPID                /*  */= new PlcAddr(PlcMemType.S, 112, 0, 06);  //고정값
        public static PlcAddr LOT_PPID                  /*  */= new PlcAddr(PlcMemType.S, 118, 0, 20);  //고정값
        public static PlcAddr LOT_RESULT                /*  */= new PlcAddr(PlcMemType.S, 138, 0, 06);  //고정값
        public static PlcAddr LOT_CODE                  /*  */= new PlcAddr(PlcMemType.S, 144, 0, 10);  //고정값
        public static PlcAddr LOT_COMMENT               /*  */= new PlcAddr(PlcMemType.S, 154, 0, 20);  //고정값
        public static PlcAddr LOT_H_GLASSID             /*  */= new PlcAddr(PlcMemType.S, 174, 0, 20);  //고정값

        public static PlcAddr CELL_H_GLASSID             /*  */= new PlcAddr(PlcMemType.S, 000, 0, 20);  //고정값
        public static PlcAddr CELL_E_GLASSID             /*  */= new PlcAddr(PlcMemType.S, 020, 0, 05);  //고정값
        public static PlcAddr CELL_LOTID                 /*  */= new PlcAddr(PlcMemType.S, 025, 0, 20);  //고정값
        public static PlcAddr CELL_BATCHID               /*  */= new PlcAddr(PlcMemType.S, 045, 0, 20);  //고정값
        public static PlcAddr CELL_JOBID                 /*  */= new PlcAddr(PlcMemType.S, 065, 0, 05);  //고정값
        public static PlcAddr CELL_PROTID                /*  */= new PlcAddr(PlcMemType.S, 070, 0, 12);  //고정값
        public static PlcAddr CELL_SLOTNO                /*  */= new PlcAddr(PlcMemType.S, 082, 0, 20);  //고정값
        public static PlcAddr CELL_PRODUCT_TYPE          /*  */= new PlcAddr(PlcMemType.S, 102, 0, 10);  //고정값
        public static PlcAddr CELL_PRODUCT_KIND          /*  */= new PlcAddr(PlcMemType.S, 112, 0, 06);  //고정값
        public static PlcAddr CELL_PRODUCTID             /*  */= new PlcAddr(PlcMemType.S, 118, 0, 20);  //고정값
        public static PlcAddr CELL_RUNSPECID             /*  */= new PlcAddr(PlcMemType.S, 138, 0, 06);  //고정값
        public static PlcAddr CELL_LAYERID               /*  */= new PlcAddr(PlcMemType.S, 144, 0, 10);  //고정값
        public static PlcAddr CELL_STEPID                /*  */= new PlcAddr(PlcMemType.S, 154, 0, 20);  //고정값
        public static PlcAddr CELL_PPID                  /*  */= new PlcAddr(PlcMemType.S, 174, 0, 20);  //고정값
        public static PlcAddr CELL_FLOWID                /*  */= new PlcAddr(PlcMemType.S, 000, 0, 20);  //고정값
        public static PlcAddr CELL_GLASS_SIZE            /*  */= new PlcAddr(PlcMemType.S, 020, 0, 05);  //고정값
        public static PlcAddr CELL_GLASS_THICKNESS       /*  */= new PlcAddr(PlcMemType.S, 025, 0, 20);  //고정값
        public static PlcAddr CELL_GLASS_STATE           /*  */= new PlcAddr(PlcMemType.S, 045, 0, 20);  //고정값
        public static PlcAddr CELL_GLASS_ORDER           /*  */= new PlcAddr(PlcMemType.S, 065, 0, 05);  //고정값
        public static PlcAddr CELL_COMMENT               /*  */= new PlcAddr(PlcMemType.S, 070, 0, 12);  //고정값

        public static PlcAddr CELL_DV_DATA               /*  */= new PlcAddr(PlcMemType.S, 100, 0, 02);  //고정값


        static EQPW()
        {

        }

        public static PlcAddr CellUnscrapData  /*  */= new PlcAddr(PlcMemType.S, 100, 0, 02);  //고정값
        public static PlcAddr CellScrapData  /*  */= new PlcAddr(PlcMemType.S, 100, 0, 02);  //고정값

        public static PlcAddr CellJobProcessStartPort { get; internal set; }
        //오정석
        public static PlcAddr CellJobProcessCancel { get; internal set; }
        //오정석
        public static PlcAddr CellJobProcessAbort { get; internal set; }
        //오정석
        public static PlcAddr CellJobProcessEnd { get; internal set; }
        //오정석
        public static PlcAddr CellCassetteOut { get; internal set; }
        //오정석
        public static PlcAddr CellCassetteIn { get; internal set; }
        //오정석
        public static PlcAddr CellIdRead { get; internal set; }
        //오정석
        public static PlcAddr CellProcessStartForModule { get; internal set; }
        //오정석
        public static PlcAddr CellProcessEndForModule { get; internal set; }
        //오정석
        public static PlcAddr CellScrap { get; internal set; }
        //오정석
        public static PlcAddr CellUnscrap { get; internal set; }
        //오정석
        public static PlcAddr CellLoadRequest { get; internal set; }
        //오정석
        public static PlcAddr CellPreLoadComplete { get; internal set; }
        //오정석
        public static PlcAddr CellLoadComplete { get; internal set; }
        //오정석
        public static PlcAddr CellUnloadRequest { get; internal set; }
        //오정석
        public static PlcAddr CellLotUnloadComplete { get; internal set; }
        //오정석
        public static PlcAddr CellIdReadFail { get; internal set; }
        //오정석
        public static PlcAddr CellReadingComplete { get; internal set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Log;
using DitInlineCim.Common;
using Dit.Framework.PLC;

namespace DitInlineCim.PLC
{
    public class PlcCommand
    {
        public static int TimeOut = 3000;

        public string Name { get; set; }

        public PlcAddr OnAddr { get; set; }
        public PlcAddr AckAddr { get; set; }

        public int StepProc { get; set; }
        public bool IsInailzied { get; set; }
        public DateTime OnTime { get; set; }

        public Action OnCommand { get; set; }

        //메소드 생성자
        public PlcCommand()
        {
            IsInailzied = false;
            StepProc = 0;
        }
        public virtual void Initailzie()
        {
            GG.PLC.ClearBit(OnAddr);
            IsInailzied = true;
            StepProc = 0;
            Logger.Append("[EQ → CIM][{0}] {1}, Command Init", StepProc, Name);
        }
        public virtual void Process()
        {
            if (StepProc > 10 && (DateTime.Now - OnTime).TotalSeconds > TimeOut)
            {
                Logger.Append("[EQ → CIM][{0}] {1}, Command Time Out", StepProc, Name);
                IsInailzied = false;
            }

            if (IsInailzied == false)
            {
                Initailzie();
            }

            if (StepProc == 0)
            {
            }
            else if (StepProc == 10)
            {
                OnTime = DateTime.Now;
                
                Logger.Append("[CIM → EQ][{0}] {1}, Command Start", StepProc, Name);
                CommandRun();

                GG.PLC.SetBit(OnAddr);
                Logger.Append("[CIM → EQ ][{0}] {1}, Command Bit On ({2})", StepProc, Name, OnAddr.ToString());
                StepProc = 20;
            }
            else if (StepProc == 20)
            {
                if (GG.PLC.VirGetBit(AckAddr) == true)
                {
                    Logger.Append("[EQ → CIM][{0}] {1}, Command Ack Bit On ({2})", StepProc, Name, AckAddr.ToString());

                    GG.PLC.ClearBit(OnAddr);
                    Logger.Append("[CIM → EQ ][{0}] {1}, Command Bit Off ({2})", StepProc, Name, OnAddr.ToString());

                    StepProc = 0;
                    Logger.Append("[CIM → EQ][{0}] {1}, Command End ", StepProc, Name);
                }
            }
        }
        public virtual void CommandRun()
        {
            if (OnCommand != null)
                OnCommand();
        }
        public void RunCmd()
        {
            StepProc = 10;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DitInlineCim.Log;
using DitInlineCim.Common;
using Dit.Framework.PLC;

namespace DitInlineCim.PLC
{
    public class PlcEvent
    {
        public static int TimeOut = 1000;

        public string Name { get; set; }
        public PlcAddr OnAddr { get; set; }
        public PlcAddr AckAddr { get; set; }

        public int StepProc { get; set; }
        public bool IsInailzied { get; set; }
        public DateTime OnTime { get; set; }
        public object Tag { get; set; }

        public Action<string, object> OnEventRun { get; set; }

        //메소드 생성자
        public PlcEvent()
        {
            IsInailzied = false;
            StepProc = 0;
        }
        public virtual void Initailzie()
        {
            GG.PLC.ClearBit(AckAddr);
            IsInailzied = true;
            StepProc = 0;
            Logger.Append("[EQ → CIM][ {0}] {1}, Event Init", StepProc, Name);
        }
        public virtual void Process()
        {
            if (StepProc > 10 && (DateTime.Now - OnTime).TotalSeconds > TimeOut)
            {
                Logger.Append("[EQ → CIM][{0}] {1}, Event Time Out", StepProc, Name);
                IsInailzied = false;
            }

            if (IsInailzied == false)
            {
                Initailzie();
            }

            if (StepProc == 0)
            {
                if (GG.PLC.VirGetBit(OnAddr) == true)
                {
                    OnTime = DateTime.Now;
                    Logger.Append("[EQ → CIM][ {0}] {1}, Event Start", StepProc, Name);
                    Logger.Append("[EQ → CIM][ {0}] {1}, Event Bit On ({2})", StepProc, Name, OnAddr.ToString());

                    StepProc = 10;
                }
            }
            else if (StepProc == 10)
            {
                OnEvent();

                GG.PLC.SetBit(AckAddr);
                Logger.Append("[CIM → EQ][{0}] {1}, Event Ack Bit On ({2})", StepProc, Name, OnAddr.ToString());

                StepProc = 20;
            }
            else if (StepProc == 20)
            {
                if (GG.PLC.VirGetBit(OnAddr) == false)
                {
                    Logger.Append("[EQ → CIM][{0}] {1}, Event Bit Off ({2})", StepProc, Name, OnAddr.ToString());
                    GG.PLC.ClearBit(AckAddr);
                    Logger.Append("[EQ → CIM][{0}] {1}, Event End ({2})", StepProc, Name, OnAddr.ToString());

                    StepProc = 0;
                }
            }
        }
        public virtual void OnEvent()
        {
            if (OnEventRun != null)
                OnEventRun(Name, Tag);
        }
    }
}

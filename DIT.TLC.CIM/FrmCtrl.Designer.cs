﻿namespace DitInlineCim
{
    partial class FrmCtrl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCtrl));
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.colorPRST_DEFAULT = new System.Windows.Forms.Label();
            this.colorPRST_INIT = new System.Windows.Forms.Label();
            this.colorPRST_IDLE = new System.Windows.Forms.Label();
            this.colorPRST_SETUP = new System.Windows.Forms.Label();
            this.colorPRST_READY = new System.Windows.Forms.Label();
            this.colorPRST_EXECUTE = new System.Windows.Forms.Label();
            this.colorPRST_PAUSE = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.colorEQST_DEFAULT = new System.Windows.Forms.Label();
            this.colorEQST_PM = new System.Windows.Forms.Label();
            this.colorEQST_FAULT = new System.Windows.Forms.Label();
            this.colorEQST_NORMAL = new System.Windows.Forms.Label();
            this.ONOFF = new System.Windows.Forms.GroupBox();
            this.colorON = new System.Windows.Forms.Label();
            this.colorOFF = new System.Windows.Forms.Label();
            this.MCMD = new System.Windows.Forms.GroupBox();
            this.colorMCMD_DEFAULT = new System.Windows.Forms.Label();
            this.colorMCMD_REMOTE = new System.Windows.Forms.Label();
            this.colorMCMD_LOCAL = new System.Windows.Forms.Label();
            this.colorMCMD_OFFLINE = new System.Windows.Forms.Label();
            this.PNST = new System.Windows.Forms.GroupBox();
            this.colorPNST_CANCELED = new System.Windows.Forms.Label();
            this.colorPNST_EMPTY = new System.Windows.Forms.Label();
            this.colorPNST_IDLE = new System.Windows.Forms.Label();
            this.colorPNST_STP = new System.Windows.Forms.Label();
            this.colorPNST_PROCESSING = new System.Windows.Forms.Label();
            this.colorPNST_ABORTING = new System.Windows.Forms.Label();
            this.colorPNST_DONE = new System.Windows.Forms.Label();
            this.colorPNST_ABORTED = new System.Windows.Forms.Label();
            this.PTST = new System.Windows.Forms.GroupBox();
            this.colorPTST_EMPTY = new System.Windows.Forms.Label();
            this.colorPTST_IDLE = new System.Windows.Forms.Label();
            this.colorPTST_WAIT = new System.Windows.Forms.Label();
            this.colorPTST_RESERVE = new System.Windows.Forms.Label();
            this.colorPTST_BUSY = new System.Windows.Forms.Label();
            this.colorPTST_ABORT = new System.Windows.Forms.Label();
            this.colorPTST_COMPLEATE = new System.Windows.Forms.Label();
            this.colorPTST_CANCEL = new System.Windows.Forms.Label();
            this.colorPTST_PAUSED = new System.Windows.Forms.Label();
            this.colorPTST_ALARM = new System.Windows.Forms.Label();
            this.colorPTST_DISABLE = new System.Windows.Forms.Label();
            this.colorCONTROL = new System.Windows.Forms.Label();
            this.colorDis = new System.Windows.Forms.Label();
            this.colorCon = new System.Windows.Forms.Label();
            this.colorDEFAULT = new System.Windows.Forms.Label();
            this.colorBLACK = new System.Windows.Forms.Label();
            this.colorSKY = new System.Windows.Forms.Label();
            this.colorWATER = new System.Windows.Forms.Label();
            this.colorWHITE = new System.Windows.Forms.Label();
            this.colorRED = new System.Windows.Forms.Label();
            this.colorDGRAY = new System.Windows.Forms.Label();
            this.colorVIOLET = new System.Windows.Forms.Label();
            this.colorYELLOW = new System.Windows.Forms.Label();
            this.colorGRAY = new System.Windows.Forms.Label();
            this.colorBLUE = new System.Windows.Forms.Label();
            this.colorGREEN = new System.Windows.Forms.Label();
            
            this.GroupBox3.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.ONOFF.SuspendLayout();
            this.MCMD.SuspendLayout();
            this.PNST.SuspendLayout();
            this.PTST.SuspendLayout();
            
            this.SuspendLayout();
            // 
            // GroupBox3
            // 
            
            this.GroupBox3.Controls.Add(this.GroupBox4);
            this.GroupBox3.Controls.Add(this.GroupBox2);
            this.GroupBox3.Controls.Add(this.ONOFF);
            this.GroupBox3.Controls.Add(this.MCMD);
            this.GroupBox3.Controls.Add(this.PNST);
            this.GroupBox3.Controls.Add(this.PTST);
            this.GroupBox3.Controls.Add(this.colorCONTROL);
            this.GroupBox3.Controls.Add(this.colorDis);
            this.GroupBox3.Controls.Add(this.colorCon);
            this.GroupBox3.Controls.Add(this.colorDEFAULT);
            this.GroupBox3.Controls.Add(this.colorBLACK);
            this.GroupBox3.Controls.Add(this.colorSKY);
            this.GroupBox3.Controls.Add(this.colorWATER);
            this.GroupBox3.Controls.Add(this.colorWHITE);
            this.GroupBox3.Controls.Add(this.colorRED);
            this.GroupBox3.Controls.Add(this.colorDGRAY);
            this.GroupBox3.Controls.Add(this.colorVIOLET);
            this.GroupBox3.Controls.Add(this.colorYELLOW);
            this.GroupBox3.Controls.Add(this.colorGRAY);
            this.GroupBox3.Controls.Add(this.colorBLUE);
            this.GroupBox3.Controls.Add(this.colorGREEN);
            this.GroupBox3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.GroupBox3.Location = new System.Drawing.Point(12, 12);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(715, 447);
            this.GroupBox3.TabIndex = 4;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Color";
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.colorPRST_DEFAULT);
            this.GroupBox4.Controls.Add(this.colorPRST_INIT);
            this.GroupBox4.Controls.Add(this.colorPRST_IDLE);
            this.GroupBox4.Controls.Add(this.colorPRST_SETUP);
            this.GroupBox4.Controls.Add(this.colorPRST_READY);
            this.GroupBox4.Controls.Add(this.colorPRST_EXECUTE);
            this.GroupBox4.Controls.Add(this.colorPRST_PAUSE);
            this.GroupBox4.Location = new System.Drawing.Point(225, 227);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(101, 206);
            this.GroupBox4.TabIndex = 44;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "PRST";
            // 
            // colorPRST_DEFAULT
            // 
            this.colorPRST_DEFAULT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.colorPRST_DEFAULT.ForeColor = System.Drawing.Color.Black;
            this.colorPRST_DEFAULT.Location = new System.Drawing.Point(8, 138);
            this.colorPRST_DEFAULT.Margin = new System.Windows.Forms.Padding(1);
            this.colorPRST_DEFAULT.Name = "colorPRST_DEFAULT";
            this.colorPRST_DEFAULT.Size = new System.Drawing.Size(84, 16);
            this.colorPRST_DEFAULT.TabIndex = 32;
            this.colorPRST_DEFAULT.Text = "Default";
            this.colorPRST_DEFAULT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPRST_INIT
            // 
            this.colorPRST_INIT.BackColor = System.Drawing.Color.Gray;
            this.colorPRST_INIT.ForeColor = System.Drawing.Color.White;
            this.colorPRST_INIT.Location = new System.Drawing.Point(8, 18);
            this.colorPRST_INIT.Margin = new System.Windows.Forms.Padding(1);
            this.colorPRST_INIT.Name = "colorPRST_INIT";
            this.colorPRST_INIT.Size = new System.Drawing.Size(84, 16);
            this.colorPRST_INIT.TabIndex = 26;
            this.colorPRST_INIT.Text = "INIT";
            this.colorPRST_INIT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPRST_IDLE
            // 
            this.colorPRST_IDLE.BackColor = System.Drawing.Color.White;
            this.colorPRST_IDLE.Location = new System.Drawing.Point(8, 37);
            this.colorPRST_IDLE.Margin = new System.Windows.Forms.Padding(1);
            this.colorPRST_IDLE.Name = "colorPRST_IDLE";
            this.colorPRST_IDLE.Size = new System.Drawing.Size(84, 16);
            this.colorPRST_IDLE.TabIndex = 27;
            this.colorPRST_IDLE.Text = "IDLE";
            this.colorPRST_IDLE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPRST_SETUP
            // 
            this.colorPRST_SETUP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colorPRST_SETUP.Location = new System.Drawing.Point(8, 55);
            this.colorPRST_SETUP.Margin = new System.Windows.Forms.Padding(1);
            this.colorPRST_SETUP.Name = "colorPRST_SETUP";
            this.colorPRST_SETUP.Size = new System.Drawing.Size(84, 16);
            this.colorPRST_SETUP.TabIndex = 28;
            this.colorPRST_SETUP.Text = "SETUP";
            this.colorPRST_SETUP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPRST_READY
            // 
            this.colorPRST_READY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.colorPRST_READY.Location = new System.Drawing.Point(8, 73);
            this.colorPRST_READY.Margin = new System.Windows.Forms.Padding(1);
            this.colorPRST_READY.Name = "colorPRST_READY";
            this.colorPRST_READY.Size = new System.Drawing.Size(84, 16);
            this.colorPRST_READY.TabIndex = 29;
            this.colorPRST_READY.Text = "READY";
            this.colorPRST_READY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPRST_EXECUTE
            // 
            this.colorPRST_EXECUTE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.colorPRST_EXECUTE.ForeColor = System.Drawing.Color.White;
            this.colorPRST_EXECUTE.Location = new System.Drawing.Point(8, 91);
            this.colorPRST_EXECUTE.Margin = new System.Windows.Forms.Padding(1);
            this.colorPRST_EXECUTE.Name = "colorPRST_EXECUTE";
            this.colorPRST_EXECUTE.Size = new System.Drawing.Size(84, 16);
            this.colorPRST_EXECUTE.TabIndex = 30;
            this.colorPRST_EXECUTE.Text = "EXECUTE";
            this.colorPRST_EXECUTE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPRST_PAUSE
            // 
            this.colorPRST_PAUSE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(240)))), ((int)(((byte)(254)))));
            this.colorPRST_PAUSE.Location = new System.Drawing.Point(8, 109);
            this.colorPRST_PAUSE.Margin = new System.Windows.Forms.Padding(1);
            this.colorPRST_PAUSE.Name = "colorPRST_PAUSE";
            this.colorPRST_PAUSE.Size = new System.Drawing.Size(84, 16);
            this.colorPRST_PAUSE.TabIndex = 31;
            this.colorPRST_PAUSE.Text = "PAUSE";
            this.colorPRST_PAUSE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.colorEQST_DEFAULT);
            this.GroupBox2.Controls.Add(this.colorEQST_PM);
            this.GroupBox2.Controls.Add(this.colorEQST_FAULT);
            this.GroupBox2.Controls.Add(this.colorEQST_NORMAL);
            this.GroupBox2.Location = new System.Drawing.Point(117, 227);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(101, 206);
            this.GroupBox2.TabIndex = 43;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "EQST";
            // 
            // colorEQST_DEFAULT
            // 
            this.colorEQST_DEFAULT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.colorEQST_DEFAULT.ForeColor = System.Drawing.Color.Black;
            this.colorEQST_DEFAULT.Location = new System.Drawing.Point(4, 73);
            this.colorEQST_DEFAULT.Margin = new System.Windows.Forms.Padding(1);
            this.colorEQST_DEFAULT.Name = "colorEQST_DEFAULT";
            this.colorEQST_DEFAULT.Size = new System.Drawing.Size(84, 16);
            this.colorEQST_DEFAULT.TabIndex = 29;
            this.colorEQST_DEFAULT.Text = "Default";
            this.colorEQST_DEFAULT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorEQST_PM
            // 
            this.colorEQST_PM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colorEQST_PM.Location = new System.Drawing.Point(5, 51);
            this.colorEQST_PM.Margin = new System.Windows.Forms.Padding(1);
            this.colorEQST_PM.Name = "colorEQST_PM";
            this.colorEQST_PM.Size = new System.Drawing.Size(84, 16);
            this.colorEQST_PM.TabIndex = 28;
            this.colorEQST_PM.Text = "PM";
            this.colorEQST_PM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorEQST_FAULT
            // 
            this.colorEQST_FAULT.BackColor = System.Drawing.Color.Red;
            this.colorEQST_FAULT.ForeColor = System.Drawing.Color.White;
            this.colorEQST_FAULT.Location = new System.Drawing.Point(5, 35);
            this.colorEQST_FAULT.Margin = new System.Windows.Forms.Padding(1);
            this.colorEQST_FAULT.Name = "colorEQST_FAULT";
            this.colorEQST_FAULT.Size = new System.Drawing.Size(84, 14);
            this.colorEQST_FAULT.TabIndex = 27;
            this.colorEQST_FAULT.Text = "FAULT";
            this.colorEQST_FAULT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorEQST_NORMAL
            // 
            this.colorEQST_NORMAL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.colorEQST_NORMAL.ForeColor = System.Drawing.Color.White;
            this.colorEQST_NORMAL.Location = new System.Drawing.Point(6, 17);
            this.colorEQST_NORMAL.Margin = new System.Windows.Forms.Padding(1);
            this.colorEQST_NORMAL.Name = "colorEQST_NORMAL";
            this.colorEQST_NORMAL.Size = new System.Drawing.Size(83, 16);
            this.colorEQST_NORMAL.TabIndex = 22;
            this.colorEQST_NORMAL.Text = "NORMAL";
            this.colorEQST_NORMAL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ONOFF
            // 
            this.ONOFF.Controls.Add(this.colorON);
            this.ONOFF.Controls.Add(this.colorOFF);
            this.ONOFF.Location = new System.Drawing.Point(6, 227);
            this.ONOFF.Name = "ONOFF";
            this.ONOFF.Size = new System.Drawing.Size(105, 64);
            this.ONOFF.TabIndex = 42;
            this.ONOFF.TabStop = false;
            this.ONOFF.Text = "On / Off";
            // 
            // colorON
            // 
            this.colorON.BackColor = System.Drawing.Color.Lime;
            this.colorON.Location = new System.Drawing.Point(8, 17);
            this.colorON.Name = "colorON";
            this.colorON.Size = new System.Drawing.Size(81, 15);
            this.colorON.TabIndex = 37;
            this.colorON.Text = "ON";
            this.colorON.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorOFF
            // 
            this.colorOFF.BackColor = System.Drawing.Color.Gray;
            this.colorOFF.ForeColor = System.Drawing.Color.White;
            this.colorOFF.Location = new System.Drawing.Point(8, 37);
            this.colorOFF.Name = "colorOFF";
            this.colorOFF.Size = new System.Drawing.Size(82, 15);
            this.colorOFF.TabIndex = 38;
            this.colorOFF.Text = "OFF";
            this.colorOFF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MCMD
            // 
            this.MCMD.Controls.Add(this.colorMCMD_DEFAULT);
            this.MCMD.Controls.Add(this.colorMCMD_REMOTE);
            this.MCMD.Controls.Add(this.colorMCMD_LOCAL);
            this.MCMD.Controls.Add(this.colorMCMD_OFFLINE);
            this.MCMD.Location = new System.Drawing.Point(225, 23);
            this.MCMD.Name = "MCMD";
            this.MCMD.Size = new System.Drawing.Size(75, 198);
            this.MCMD.TabIndex = 41;
            this.MCMD.TabStop = false;
            this.MCMD.Text = "MCMD";
            // 
            // colorMCMD_DEFAULT
            // 
            this.colorMCMD_DEFAULT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.colorMCMD_DEFAULT.ForeColor = System.Drawing.Color.Black;
            this.colorMCMD_DEFAULT.Location = new System.Drawing.Point(10, 70);
            this.colorMCMD_DEFAULT.Name = "colorMCMD_DEFAULT";
            this.colorMCMD_DEFAULT.Size = new System.Drawing.Size(58, 16);
            this.colorMCMD_DEFAULT.TabIndex = 17;
            this.colorMCMD_DEFAULT.Text = "Default";
            this.colorMCMD_DEFAULT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorMCMD_REMOTE
            // 
            this.colorMCMD_REMOTE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.colorMCMD_REMOTE.ForeColor = System.Drawing.Color.White;
            this.colorMCMD_REMOTE.Location = new System.Drawing.Point(10, 19);
            this.colorMCMD_REMOTE.Name = "colorMCMD_REMOTE";
            this.colorMCMD_REMOTE.Size = new System.Drawing.Size(58, 16);
            this.colorMCMD_REMOTE.TabIndex = 14;
            this.colorMCMD_REMOTE.Text = "Remote";
            this.colorMCMD_REMOTE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorMCMD_LOCAL
            // 
            this.colorMCMD_LOCAL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.colorMCMD_LOCAL.ForeColor = System.Drawing.Color.White;
            this.colorMCMD_LOCAL.Location = new System.Drawing.Point(10, 36);
            this.colorMCMD_LOCAL.Name = "colorMCMD_LOCAL";
            this.colorMCMD_LOCAL.Size = new System.Drawing.Size(58, 16);
            this.colorMCMD_LOCAL.TabIndex = 15;
            this.colorMCMD_LOCAL.Text = "Local";
            this.colorMCMD_LOCAL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorMCMD_OFFLINE
            // 
            this.colorMCMD_OFFLINE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.colorMCMD_OFFLINE.Location = new System.Drawing.Point(10, 53);
            this.colorMCMD_OFFLINE.Name = "colorMCMD_OFFLINE";
            this.colorMCMD_OFFLINE.Size = new System.Drawing.Size(58, 16);
            this.colorMCMD_OFFLINE.TabIndex = 16;
            this.colorMCMD_OFFLINE.Text = "Offline";
            this.colorMCMD_OFFLINE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PNST
            // 
            this.PNST.Controls.Add(this.colorPNST_CANCELED);
            this.PNST.Controls.Add(this.colorPNST_EMPTY);
            this.PNST.Controls.Add(this.colorPNST_IDLE);
            this.PNST.Controls.Add(this.colorPNST_STP);
            this.PNST.Controls.Add(this.colorPNST_PROCESSING);
            this.PNST.Controls.Add(this.colorPNST_ABORTING);
            this.PNST.Controls.Add(this.colorPNST_DONE);
            this.PNST.Controls.Add(this.colorPNST_ABORTED);
            this.PNST.Location = new System.Drawing.Point(117, 23);
            this.PNST.Name = "PNST";
            this.PNST.Size = new System.Drawing.Size(101, 198);
            this.PNST.TabIndex = 40;
            this.PNST.TabStop = false;
            this.PNST.Text = "Panel State";
            // 
            // colorPNST_CANCELED
            // 
            this.colorPNST_CANCELED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.colorPNST_CANCELED.Location = new System.Drawing.Point(11, 132);
            this.colorPNST_CANCELED.Name = "colorPNST_CANCELED";
            this.colorPNST_CANCELED.Size = new System.Drawing.Size(84, 16);
            this.colorPNST_CANCELED.TabIndex = 32;
            this.colorPNST_CANCELED.Text = "CANCELED";
            this.colorPNST_CANCELED.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPNST_EMPTY
            // 
            this.colorPNST_EMPTY.BackColor = System.Drawing.Color.Gray;
            this.colorPNST_EMPTY.ForeColor = System.Drawing.Color.White;
            this.colorPNST_EMPTY.Location = new System.Drawing.Point(10, 20);
            this.colorPNST_EMPTY.Name = "colorPNST_EMPTY";
            this.colorPNST_EMPTY.Size = new System.Drawing.Size(84, 16);
            this.colorPNST_EMPTY.TabIndex = 25;
            this.colorPNST_EMPTY.Text = "EMPTY";
            this.colorPNST_EMPTY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPNST_IDLE
            // 
            this.colorPNST_IDLE.BackColor = System.Drawing.Color.White;
            this.colorPNST_IDLE.Location = new System.Drawing.Point(10, 36);
            this.colorPNST_IDLE.Name = "colorPNST_IDLE";
            this.colorPNST_IDLE.Size = new System.Drawing.Size(84, 16);
            this.colorPNST_IDLE.TabIndex = 26;
            this.colorPNST_IDLE.Text = "IDLE";
            this.colorPNST_IDLE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPNST_STP
            // 
            this.colorPNST_STP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.colorPNST_STP.Location = new System.Drawing.Point(11, 52);
            this.colorPNST_STP.Name = "colorPNST_STP";
            this.colorPNST_STP.Size = new System.Drawing.Size(84, 16);
            this.colorPNST_STP.TabIndex = 27;
            this.colorPNST_STP.Text = "STP";
            this.colorPNST_STP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPNST_PROCESSING
            // 
            this.colorPNST_PROCESSING.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.colorPNST_PROCESSING.ForeColor = System.Drawing.Color.White;
            this.colorPNST_PROCESSING.Location = new System.Drawing.Point(11, 68);
            this.colorPNST_PROCESSING.Name = "colorPNST_PROCESSING";
            this.colorPNST_PROCESSING.Size = new System.Drawing.Size(83, 16);
            this.colorPNST_PROCESSING.TabIndex = 28;
            this.colorPNST_PROCESSING.Text = "PROCESSING";
            this.colorPNST_PROCESSING.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPNST_ABORTING
            // 
            this.colorPNST_ABORTING.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.colorPNST_ABORTING.ForeColor = System.Drawing.Color.White;
            this.colorPNST_ABORTING.Location = new System.Drawing.Point(11, 100);
            this.colorPNST_ABORTING.Name = "colorPNST_ABORTING";
            this.colorPNST_ABORTING.Size = new System.Drawing.Size(84, 16);
            this.colorPNST_ABORTING.TabIndex = 29;
            this.colorPNST_ABORTING.Text = "ABORTING";
            this.colorPNST_ABORTING.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPNST_DONE
            // 
            this.colorPNST_DONE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colorPNST_DONE.Location = new System.Drawing.Point(11, 84);
            this.colorPNST_DONE.Name = "colorPNST_DONE";
            this.colorPNST_DONE.Size = new System.Drawing.Size(84, 16);
            this.colorPNST_DONE.TabIndex = 30;
            this.colorPNST_DONE.Text = "DONE";
            this.colorPNST_DONE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPNST_ABORTED
            // 
            this.colorPNST_ABORTED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.colorPNST_ABORTED.ForeColor = System.Drawing.Color.Black;
            this.colorPNST_ABORTED.Location = new System.Drawing.Point(11, 116);
            this.colorPNST_ABORTED.Name = "colorPNST_ABORTED";
            this.colorPNST_ABORTED.Size = new System.Drawing.Size(84, 16);
            this.colorPNST_ABORTED.TabIndex = 31;
            this.colorPNST_ABORTED.Text = "ABORTED";
            this.colorPNST_ABORTED.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PTST
            // 
            this.PTST.Controls.Add(this.colorPTST_EMPTY);
            this.PTST.Controls.Add(this.colorPTST_IDLE);
            this.PTST.Controls.Add(this.colorPTST_WAIT);
            this.PTST.Controls.Add(this.colorPTST_RESERVE);
            this.PTST.Controls.Add(this.colorPTST_BUSY);
            this.PTST.Controls.Add(this.colorPTST_ABORT);
            this.PTST.Controls.Add(this.colorPTST_COMPLEATE);
            this.PTST.Controls.Add(this.colorPTST_CANCEL);
            this.PTST.Controls.Add(this.colorPTST_PAUSED);
            this.PTST.Controls.Add(this.colorPTST_ALARM);
            this.PTST.Controls.Add(this.colorPTST_DISABLE);
            this.PTST.Location = new System.Drawing.Point(6, 23);
            this.PTST.Name = "PTST";
            this.PTST.Size = new System.Drawing.Size(105, 198);
            this.PTST.TabIndex = 39;
            this.PTST.TabStop = false;
            this.PTST.Text = "Port State";
            // 
            // colorPTST_EMPTY
            // 
            this.colorPTST_EMPTY.BackColor = System.Drawing.Color.Gray;
            this.colorPTST_EMPTY.ForeColor = System.Drawing.Color.White;
            this.colorPTST_EMPTY.Location = new System.Drawing.Point(8, 16);
            this.colorPTST_EMPTY.Name = "colorPTST_EMPTY";
            this.colorPTST_EMPTY.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_EMPTY.TabIndex = 17;
            this.colorPTST_EMPTY.Text = "EMPTY";
            this.colorPTST_EMPTY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_IDLE
            // 
            this.colorPTST_IDLE.BackColor = System.Drawing.Color.White;
            this.colorPTST_IDLE.Location = new System.Drawing.Point(8, 32);
            this.colorPTST_IDLE.Name = "colorPTST_IDLE";
            this.colorPTST_IDLE.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_IDLE.TabIndex = 18;
            this.colorPTST_IDLE.Text = "IDLE";
            this.colorPTST_IDLE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_WAIT
            // 
            this.colorPTST_WAIT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colorPTST_WAIT.Location = new System.Drawing.Point(8, 48);
            this.colorPTST_WAIT.Name = "colorPTST_WAIT";
            this.colorPTST_WAIT.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_WAIT.TabIndex = 19;
            this.colorPTST_WAIT.Text = "WAIT";
            this.colorPTST_WAIT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_RESERVE
            // 
            this.colorPTST_RESERVE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.colorPTST_RESERVE.Location = new System.Drawing.Point(9, 64);
            this.colorPTST_RESERVE.Name = "colorPTST_RESERVE";
            this.colorPTST_RESERVE.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_RESERVE.TabIndex = 20;
            this.colorPTST_RESERVE.Text = "RESERVE";
            this.colorPTST_RESERVE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_BUSY
            // 
            this.colorPTST_BUSY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.colorPTST_BUSY.ForeColor = System.Drawing.Color.White;
            this.colorPTST_BUSY.Location = new System.Drawing.Point(9, 80);
            this.colorPTST_BUSY.Name = "colorPTST_BUSY";
            this.colorPTST_BUSY.Size = new System.Drawing.Size(83, 16);
            this.colorPTST_BUSY.TabIndex = 21;
            this.colorPTST_BUSY.Text = "BUSY";
            this.colorPTST_BUSY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_ABORT
            // 
            this.colorPTST_ABORT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.colorPTST_ABORT.Location = new System.Drawing.Point(9, 112);
            this.colorPTST_ABORT.Name = "colorPTST_ABORT";
            this.colorPTST_ABORT.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_ABORT.TabIndex = 22;
            this.colorPTST_ABORT.Text = "ABORT";
            this.colorPTST_ABORT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_COMPLEATE
            // 
            this.colorPTST_COMPLEATE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colorPTST_COMPLEATE.Location = new System.Drawing.Point(9, 96);
            this.colorPTST_COMPLEATE.Name = "colorPTST_COMPLEATE";
            this.colorPTST_COMPLEATE.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_COMPLEATE.TabIndex = 23;
            this.colorPTST_COMPLEATE.Text = "COMPLEATE";
            this.colorPTST_COMPLEATE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_CANCEL
            // 
            this.colorPTST_CANCEL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.colorPTST_CANCEL.Location = new System.Drawing.Point(9, 128);
            this.colorPTST_CANCEL.Name = "colorPTST_CANCEL";
            this.colorPTST_CANCEL.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_CANCEL.TabIndex = 24;
            this.colorPTST_CANCEL.Text = "CANCEL";
            this.colorPTST_CANCEL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_PAUSED
            // 
            this.colorPTST_PAUSED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(240)))), ((int)(((byte)(254)))));
            this.colorPTST_PAUSED.Location = new System.Drawing.Point(9, 145);
            this.colorPTST_PAUSED.Name = "colorPTST_PAUSED";
            this.colorPTST_PAUSED.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_PAUSED.TabIndex = 25;
            this.colorPTST_PAUSED.Text = "PAUSED";
            this.colorPTST_PAUSED.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_ALARM
            // 
            this.colorPTST_ALARM.BackColor = System.Drawing.Color.Red;
            this.colorPTST_ALARM.ForeColor = System.Drawing.Color.White;
            this.colorPTST_ALARM.Location = new System.Drawing.Point(9, 177);
            this.colorPTST_ALARM.Name = "colorPTST_ALARM";
            this.colorPTST_ALARM.Size = new System.Drawing.Size(84, 14);
            this.colorPTST_ALARM.TabIndex = 26;
            this.colorPTST_ALARM.Text = "ALARM";
            this.colorPTST_ALARM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorPTST_DISABLE
            // 
            this.colorPTST_DISABLE.BackColor = System.Drawing.Color.LightGray;
            this.colorPTST_DISABLE.Location = new System.Drawing.Point(9, 161);
            this.colorPTST_DISABLE.Name = "colorPTST_DISABLE";
            this.colorPTST_DISABLE.Size = new System.Drawing.Size(84, 16);
            this.colorPTST_DISABLE.TabIndex = 27;
            this.colorPTST_DISABLE.Text = "DISABLE";
            this.colorPTST_DISABLE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorCONTROL
            // 
            this.colorCONTROL.BackColor = System.Drawing.Color.Silver;
            this.colorCONTROL.Location = new System.Drawing.Point(477, 21);
            this.colorCONTROL.Name = "colorCONTROL";
            this.colorCONTROL.Size = new System.Drawing.Size(84, 16);
            this.colorCONTROL.TabIndex = 36;
            this.colorCONTROL.Text = "gray(디폴트)";
            // 
            // colorDis
            // 
            this.colorDis.BackColor = System.Drawing.Color.Red;
            this.colorDis.Location = new System.Drawing.Point(573, 42);
            this.colorDis.Name = "colorDis";
            this.colorDis.Size = new System.Drawing.Size(84, 14);
            this.colorDis.TabIndex = 13;
            this.colorDis.Text = "끊김";
            // 
            // colorCon
            // 
            this.colorCon.BackColor = System.Drawing.Color.Blue;
            this.colorCon.Location = new System.Drawing.Point(573, 23);
            this.colorCon.Name = "colorCon";
            this.colorCon.Size = new System.Drawing.Size(84, 14);
            this.colorCon.TabIndex = 12;
            this.colorCon.Text = "연결";
            // 
            // colorDEFAULT
            // 
            this.colorDEFAULT.BackColor = System.Drawing.Color.LightGray;
            this.colorDEFAULT.Location = new System.Drawing.Point(432, 203);
            this.colorDEFAULT.Name = "colorDEFAULT";
            this.colorDEFAULT.Size = new System.Drawing.Size(84, 16);
            this.colorDEFAULT.TabIndex = 11;
            this.colorDEFAULT.Text = "디폴트(회색)";
            // 
            // colorBLACK
            // 
            this.colorBLACK.BackColor = System.Drawing.Color.Black;
            this.colorBLACK.ForeColor = System.Drawing.Color.White;
            this.colorBLACK.Location = new System.Drawing.Point(342, 203);
            this.colorBLACK.Name = "colorBLACK";
            this.colorBLACK.Size = new System.Drawing.Size(84, 16);
            this.colorBLACK.TabIndex = 10;
            this.colorBLACK.Text = "black(검정)";
            // 
            // colorSKY
            // 
            this.colorSKY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colorSKY.Location = new System.Drawing.Point(342, 108);
            this.colorSKY.Name = "colorSKY";
            this.colorSKY.Size = new System.Drawing.Size(84, 16);
            this.colorSKY.TabIndex = 9;
            this.colorSKY.Text = "sky(하늘)";
            // 
            // colorWATER
            // 
            this.colorWATER.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(240)))), ((int)(((byte)(254)))));
            this.colorWATER.Location = new System.Drawing.Point(432, 179);
            this.colorWATER.Name = "colorWATER";
            this.colorWATER.Size = new System.Drawing.Size(84, 16);
            this.colorWATER.TabIndex = 8;
            this.colorWATER.Text = "water(수색)";
            // 
            // colorWHITE
            // 
            this.colorWHITE.BackColor = System.Drawing.Color.White;
            this.colorWHITE.Location = new System.Drawing.Point(342, 179);
            this.colorWHITE.Name = "colorWHITE";
            this.colorWHITE.Size = new System.Drawing.Size(84, 16);
            this.colorWHITE.TabIndex = 7;
            this.colorWHITE.Text = "white(흰색)";
            // 
            // colorRED
            // 
            this.colorRED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.colorRED.Location = new System.Drawing.Point(342, 84);
            this.colorRED.Name = "colorRED";
            this.colorRED.Size = new System.Drawing.Size(84, 16);
            this.colorRED.TabIndex = 6;
            this.colorRED.Text = "red(빨강)";
            // 
            // colorDGRAY
            // 
            this.colorDGRAY.BackColor = System.Drawing.Color.Gray;
            this.colorDGRAY.Location = new System.Drawing.Point(432, 157);
            this.colorDGRAY.Name = "colorDGRAY";
            this.colorDGRAY.Size = new System.Drawing.Size(109, 16);
            this.colorDGRAY.TabIndex = 5;
            this.colorDGRAY.Text = "dgray(어두운회색)";
            // 
            // colorVIOLET
            // 
            this.colorVIOLET.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.colorVIOLET.Location = new System.Drawing.Point(342, 157);
            this.colorVIOLET.Name = "colorVIOLET";
            this.colorVIOLET.Size = new System.Drawing.Size(84, 16);
            this.colorVIOLET.TabIndex = 4;
            this.colorVIOLET.Text = "violet(보라)";
            // 
            // colorYELLOW
            // 
            this.colorYELLOW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colorYELLOW.Location = new System.Drawing.Point(342, 62);
            this.colorYELLOW.Name = "colorYELLOW";
            this.colorYELLOW.Size = new System.Drawing.Size(84, 16);
            this.colorYELLOW.TabIndex = 3;
            this.colorYELLOW.Text = "yellow(노랑)";
            // 
            // colorGRAY
            // 
            this.colorGRAY.BackColor = System.Drawing.Color.LightGray;
            this.colorGRAY.Location = new System.Drawing.Point(432, 137);
            this.colorGRAY.Name = "colorGRAY";
            this.colorGRAY.Size = new System.Drawing.Size(84, 16);
            this.colorGRAY.TabIndex = 2;
            this.colorGRAY.Text = "gray(회색)";
            // 
            // colorBLUE
            // 
            this.colorBLUE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.colorBLUE.Location = new System.Drawing.Point(342, 137);
            this.colorBLUE.Name = "colorBLUE";
            this.colorBLUE.Size = new System.Drawing.Size(84, 16);
            this.colorBLUE.TabIndex = 1;
            this.colorBLUE.Text = "blue(파랑)";
            // 
            // colorGREEN
            // 
            this.colorGREEN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colorGREEN.Location = new System.Drawing.Point(342, 42);
            this.colorGREEN.Name = "colorGREEN";
            this.colorGREEN.Size = new System.Drawing.Size(84, 16);
            this.colorGREEN.TabIndex = 0;
            this.colorGREEN.Text = "green(녹색)";            
            // 
            // FrmCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 474);
            this.Controls.Add(this.GroupBox3);
            this.Name = "FrmCtrl";
            this.Text = "CIM Color";
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox2.ResumeLayout(false);
            this.ONOFF.ResumeLayout(false);
            this.MCMD.ResumeLayout(false);
            this.PNST.ResumeLayout(false);
            this.PTST.ResumeLayout(false);            
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.Label colorPRST_DEFAULT;
        internal System.Windows.Forms.Label colorPRST_INIT;
        internal System.Windows.Forms.Label colorPRST_IDLE;
        internal System.Windows.Forms.Label colorPRST_SETUP;
        internal System.Windows.Forms.Label colorPRST_READY;
        internal System.Windows.Forms.Label colorPRST_EXECUTE;
        internal System.Windows.Forms.Label colorPRST_PAUSE;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label colorEQST_DEFAULT;
        internal System.Windows.Forms.Label colorEQST_PM;
        internal System.Windows.Forms.Label colorEQST_FAULT;
        internal System.Windows.Forms.Label colorEQST_NORMAL;
        internal System.Windows.Forms.GroupBox ONOFF;
        internal System.Windows.Forms.Label colorON;
        internal System.Windows.Forms.Label colorOFF;
        internal System.Windows.Forms.GroupBox MCMD;
        internal System.Windows.Forms.Label colorMCMD_DEFAULT;
        internal System.Windows.Forms.Label colorMCMD_REMOTE;
        internal System.Windows.Forms.Label colorMCMD_LOCAL;
        internal System.Windows.Forms.Label colorMCMD_OFFLINE;
        internal System.Windows.Forms.GroupBox PNST;
        internal System.Windows.Forms.Label colorPNST_CANCELED;
        internal System.Windows.Forms.Label colorPNST_EMPTY;
        internal System.Windows.Forms.Label colorPNST_IDLE;
        internal System.Windows.Forms.Label colorPNST_STP;
        internal System.Windows.Forms.Label colorPNST_PROCESSING;
        internal System.Windows.Forms.Label colorPNST_ABORTING;
        internal System.Windows.Forms.Label colorPNST_DONE;
        internal System.Windows.Forms.Label colorPNST_ABORTED;
        internal System.Windows.Forms.GroupBox PTST;
        internal System.Windows.Forms.Label colorPTST_EMPTY;
        internal System.Windows.Forms.Label colorPTST_IDLE;
        internal System.Windows.Forms.Label colorPTST_WAIT;
        internal System.Windows.Forms.Label colorPTST_RESERVE;
        internal System.Windows.Forms.Label colorPTST_BUSY;
        internal System.Windows.Forms.Label colorPTST_ABORT;
        internal System.Windows.Forms.Label colorPTST_COMPLEATE;
        internal System.Windows.Forms.Label colorPTST_CANCEL;
        internal System.Windows.Forms.Label colorPTST_PAUSED;
        internal System.Windows.Forms.Label colorPTST_ALARM;
        internal System.Windows.Forms.Label colorPTST_DISABLE;
        internal System.Windows.Forms.Label colorCONTROL;
        internal System.Windows.Forms.Label colorDis;
        internal System.Windows.Forms.Label colorCon;
        internal System.Windows.Forms.Label colorDEFAULT;
        internal System.Windows.Forms.Label colorBLACK;
        internal System.Windows.Forms.Label colorSKY;
        internal System.Windows.Forms.Label colorWATER;
        internal System.Windows.Forms.Label colorWHITE;
        internal System.Windows.Forms.Label colorRED;
        internal System.Windows.Forms.Label colorDGRAY;
        internal System.Windows.Forms.Label colorVIOLET;
        internal System.Windows.Forms.Label colorYELLOW;
        internal System.Windows.Forms.Label colorGRAY;
        internal System.Windows.Forms.Label colorBLUE;
        internal System.Windows.Forms.Label colorGREEN;        
    }
}